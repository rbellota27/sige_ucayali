﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTipoTablaValorTest y se pretende que
'''contenga todas las pruebas unitarias DAOTipoTablaValorTest.
'''</summary>
<TestClass()> _
Public Class DAOTipoTablaValorTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTipoTablaValor
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTipoTablaValorConstructorTest()
        Dim target As DAOTipoTablaValor = New DAOTipoTablaValor()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaTipoTablaValor
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaTipoTablaValorTest()
        Dim target As DAOTipoTablaValor = New DAOTipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim obj As TipoTablaValor = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaTipoTablaValor(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GenerarCodigo
    '''</summary>
    <TestMethod()> _
    Public Sub GenerarCodigoTest()
        Dim target As DAOTipoTablaValor = New DAOTipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim idtipotabla As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoTablaValor = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoTablaValor
        actual = target.GenerarCodigo(idtipotabla)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaTipoTablaValor
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTipoTablaValorTest()
        Dim target As DAOTipoTablaValor = New DAOTipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim obj As TipoTablaValor = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaTipoTablaValor(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxIdTipoTablaxEstado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxIdTipoTablaxEstadoTest()
        Dim target As DAOTipoTablaValor = New DAOTipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim idtipotabla As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoTablaValor)
        actual = target.SelectAllxIdTipoTablaxEstado(idtipotabla, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxIdTipoTablaxEstado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxIdTipoTablaxEstadoTest1()
        Dim target As DAOTipoTablaValor = New DAOTipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipotabla As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoTablaValor)
        actual = target.SelectAllxIdTipoTablaxEstado(idsublinea, idtipotabla, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxIdTipoTablaxnombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxIdTipoTablaxnombreTest()
        Dim target As DAOTipoTablaValor = New DAOTipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipotabla As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoTablaValor)
        actual = target.SelectAllxIdTipoTablaxnombre(idsublinea, idtipotabla, nombre, PageIndex, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de TipoTablaValorSelectAllxIdTipoTablaxEstado_Paginado
    '''</summary>
    <TestMethod()> _
    Public Sub TipoTablaValorSelectAllxIdTipoTablaxEstado_PaginadoTest()
        Dim target As DAOTipoTablaValor = New DAOTipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim idtipotabla As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoTablaValor)
        actual = target.TipoTablaValorSelectAllxIdTipoTablaxEstado_Paginado(idtipotabla, descripcion, estado, PageIndex, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
