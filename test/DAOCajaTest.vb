﻿Imports System.Data

Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCajaTest y se pretende que
'''contenga todas las pruebas unitarias DAOCajaTest.
'''</summary>
<TestClass()> _
Public Class DAOCajaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCaja
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCajaConstructorTest()
        Dim target As DAOCaja = New DAOCaja()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaCaja
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaCajaTest()
        Dim target As DAOCaja = New DAOCaja() ' TODO: Inicializar en un valor adecuado
        Dim caja As Caja = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaCaja(caja)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdTienda
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdTiendaTest()
        Dim target As DAOCaja = New DAOCaja() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdTienda(cn, IdTienda, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GrabaCajaT
    '''</summary>
    <TestMethod()> _
    Public Sub GrabaCajaTTest()
        Dim target As DAOCaja = New DAOCaja() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lcaja As List(Of Caja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.GrabaCajaT(cn, lcaja, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaCajaT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaCajaTTest()
        Dim target As DAOCaja = New DAOCaja() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lcaja As List(Of Caja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaCajaT(cn, lcaja, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOCaja = New DAOCaja() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Caja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Caja)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoInactivoTest()
        Dim target As DAOCaja = New DAOCaja() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Caja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Caja)
        actual = target.SelectAllActivoInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCajaxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCajaxIdTest()
        Dim target As DAOCaja = New DAOCaja() ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Caja = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Caja
        actual = target.SelectCajaxId(IdCaja)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdTienda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdTiendaTest()
        Dim target As DAOCaja = New DAOCaja() ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Caja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Caja)
        actual = target.SelectCboxIdTienda(idtienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaxIdTienda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaxIdTiendaTest()
        Dim target As DAOCaja = New DAOCaja() ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Caja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Caja)
        actual = target.SelectGrillaxIdTienda(idtienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectIdCajaxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectIdCajaxIdPersonaTest()
        Dim target As DAOCaja = New DAOCaja() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectIdCajaxIdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectResumenxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectResumenxParamsTest()
        Dim target As DAOCaja = New DAOCaja() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.SelectResumenxParams(IdEmpresa, IdTienda, IdCaja, IdMedioPago)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_LiquidacionCaja
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_LiquidacionCajaTest()
        Dim target As DAOCaja = New DAOCaja() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaIni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_LiquidacionCaja(IdEmpresa, IdTienda, IdCaja, FechaIni, FechaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
