﻿Imports System

Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoAjusteCuentaTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoAjusteCuentaTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoAjusteCuentaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoAjusteCuenta
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoAjusteCuentaConstructorTest()
        Dim target As DAODocumentoAjusteCuenta = New DAODocumentoAjusteCuenta()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoAjusteCuenta_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoAjusteCuenta_DeshacerMovTest()
        Dim target As DAODocumentoAjusteCuenta = New DAODocumentoAjusteCuenta() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleConcepto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim ValMovPosteriores As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim RestablecerSaldos As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoAjusteCuenta_DeshacerMov(IdDocumento, DeleteDetalleConcepto, DeleteObservacion, DeleteRelacionDocumento, ValMovPosteriores, RestablecerSaldos, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoAjusteCuenta_SelectDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoAjusteCuenta_SelectDetalleTest()
        Dim target As DAODocumentoAjusteCuenta = New DAODocumentoAjusteCuenta() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento_MovCuenta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento_MovCuenta)
        actual = target.DocumentoAjusteCuenta_SelectDetalle(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoAjusteCuenta_SelectMovCuentaCargoxParams
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoAjusteCuenta_SelectMovCuentaCargoxParamsTest()
        Dim target As DAODocumentoAjusteCuenta = New DAODocumentoAjusteCuenta() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento_MovCuenta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento_MovCuenta)
        actual = target.DocumentoAjusteCuenta_SelectMovCuentaCargoxParams(IdUsuario, IdPersona, Serie, Codigo, FechaInicio, FechaFin, IdTipoDocumento, IdTipoDocumentoRef)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
