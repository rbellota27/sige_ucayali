﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOPaisTest y se pretende que
'''contenga todas las pruebas unitarias DAOPaisTest.
'''</summary>
<TestClass()> _
Public Class DAOPaisTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOPais
    '''</summary>
    <TestMethod()> _
    Public Sub DAOPaisConstructorTest()
        Dim target As DAOPais = New DAOPais()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaUsuarioTest()
        Dim target As DAOPais = New DAOPais() ' TODO: Inicializar en un valor adecuado
        Dim usuario As Pais = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of Procedencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaUsuario(usuario, lista)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaPais
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaPaisTest()
        Dim target As DAOPais = New DAOPais() ' TODO: Inicializar en un valor adecuado
        Dim pais As Pais = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of Procedencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaPais(pais, lista)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOPais = New DAOPais() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Pais) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Pais)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoTodos
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTodosTest()
        Dim target As DAOPais = New DAOPais() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Pais) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Pais)
        actual = target.SelectAllActivoTodos
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoxIdProcedencia_Cbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoxIdProcedencia_CboTest()
        Dim target As DAOPais = New DAOPais() ' TODO: Inicializar en un valor adecuado
        Dim IdProcedencia As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Pais) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Pais)
        actual = target.SelectAllActivoxIdProcedencia_Cbo(IdProcedencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoxNoPais_Cbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoxNoPais_CboTest()
        Dim target As DAOPais = New DAOPais() ' TODO: Inicializar en un valor adecuado
        Dim NomPais As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Pais) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Pais)
        actual = target.SelectAllActivoxNoPais_Cbo(NomPais)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxEstadoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxEstadoxNombreTest()
        Dim target As DAOPais = New DAOPais() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim estado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Pais) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Pais)
        actual = target.SelectxEstadoxNombre(nombre, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPais
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPaisTest()
        Dim target As DAOPais = New DAOPais() ' TODO: Inicializar en un valor adecuado
        Dim IdPais As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Pais = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Pais
        actual = target.SelectxIdPais(IdPais)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProcedencia
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProcedenciaTest()
        Dim target As DAOPais = New DAOPais() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Pais = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Pais
        actual = target.SelectxIdProcedencia(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
