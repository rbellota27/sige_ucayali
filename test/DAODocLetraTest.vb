﻿Imports System.Data

Imports System

Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocLetraTest y se pretende que
'''contenga todas las pruebas unitarias DAODocLetraTest.
'''</summary>
<TestClass()> _
Public Class DAODocLetraTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocLetra
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocLetraConstructorTest()
        Dim target As DAODocLetra = New DAODocLetra()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Anexar_Letra
    '''</summary>
    <TestMethod()> _
    Public Sub Anexar_LetraTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoLetra As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocLetra) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocLetra)
        actual = target.Anexar_Letra(IdDocumentoLetra)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLetraAnularxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLetraAnularxIdDocumentoTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoLetra As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoLetraAnularxIdDocumento(IdDocumentoLetra, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLetraConsultarLetrasxIdEstadoCancelacion
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLetraConsultarLetrasxIdEstadoCancelacionTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEstadoCancelacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Top As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocLetra) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocLetra)
        actual = target.DocumentoLetraConsultarLetrasxIdEstadoCancelacion(IdPersona, IdEstadoCancelacion, Top)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLetraInsert
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLetraInsertTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim objDocLetra As DocLetra = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.DocumentoLetraInsert(objDocLetra, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLetraSelectCabtxId_Print
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLetraSelectCabtxId_PrintTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoLetra As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocLetra = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocLetra
        actual = target.DocumentoLetraSelectCabtxId_Print(IdDocumentoLetra)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLetraSelectCabxParams
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLetraSelectCabxParamsTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocLetra = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocLetra
        actual = target.DocumentoLetraSelectCabxParams(IdDocumento, IdSerie, Codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLetraSelectDocReferenciaxIdDocumentoLetra
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLetraSelectDocReferenciaxIdDocumentoLetraTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoLetra As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento_MovCuenta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento_MovCuenta)
        actual = target.DocumentoLetraSelectDocReferenciaxIdDocumentoLetra(IdDocumentoLetra, IdMonedaDestino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLetraUpdate
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLetraUpdateTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim objDocumentoLetra As DocLetra = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoLetraUpdate(objDocumentoLetra, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLetra_CanjearDocReferenciaxLetra
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLetra_CanjearDocReferenciaxLetraTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim IdMovCuentaDocRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoLetra As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim AbonoxLetra As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoLetra_CanjearDocReferenciaxLetra(IdMovCuentaDocRef, IdDocumentoRef, IdDocumentoLetra, AbonoxLetra, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLetra_DeshacerMov_Renegociar
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLetra_DeshacerMov_RenegociarTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoLetra As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoLetra_DeshacerMov_Renegociar(IdDocumentoLetra, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLetra_DeshacerMovimientos
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLetra_DeshacerMovimientosTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoLetra As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovCuenta As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePagoProgramado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservaciones As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteLetraCambio As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleConcepto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim valEstado_DC As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim valEstado_Renegociado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim valEstado_Protestado As Boolean = False ' TODO: Inicializar en un valor adecuado
        target.DocumentoLetra_DeshacerMovimientos(IdDocumentoLetra, DeleteMovCuenta, DeletePagoProgramado, DeleteObservaciones, DeleteLetraCambio, Anular, DeleteRelacionDocumento, DeleteDetalleConcepto, cn, tr, valEstado_DC, valEstado_Renegociado, valEstado_Protestado)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLetra_EditarLetraxConsulta
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLetra_EditarLetraxConsultaTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim LetraCancelada As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DebitoCuenta As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Protestado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim FechaPgo As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim nroOperacion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoLetra_EditarLetraxConsulta(IdDocumento, LetraCancelada, DebitoCuenta, Protestado, FechaPgo, nroOperacion, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLetra_VerificarEstadoCancelacionDocReferencia
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLetra_VerificarEstadoCancelacionDocReferenciaTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoLetra As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoLetra_VerificarEstadoCancelacionDocReferencia(IdDocumentoLetra, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_CR_DocumentoLetraCab
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_CR_DocumentoLetraCabTest()
        Dim target As DAODocLetra = New DAODocLetra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_CR_DocumentoLetraCab(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
