﻿Imports System

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMovBancoViewTest y se pretende que
'''contenga todas las pruebas unitarias DAOMovBancoViewTest.
'''</summary>
<TestClass()> _
Public Class DAOMovBancoViewTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMovBancoView
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMovBancoViewConstructorTest()
        Dim target As DAOMovBancoView = New DAOMovBancoView()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOMovBancoView = New DAOMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovBancoView)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOMovBancoView = New DAOMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovBancoView)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOMovBancoView = New DAOMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovBancoView)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectExcelttoGridView
    '''</summary>
    <TestMethod()> _
    Public Sub SelectExcelttoGridViewTest()
        Dim target As DAOMovBancoView = New DAOMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim namearchivo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nom_hoja As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdEstructuraBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovBancoView)
        actual = target.SelectExcelttoGridView(namearchivo, nom_hoja, IdEstructuraBanco)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectMovBancosxFechas
    '''</summary>
    <TestMethod()> _
    Public Sub SelectMovBancosxFechasTest()
        Dim target As DAOMovBancoView = New DAOMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim FechaInicial As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Fechafinal As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim NroOperacion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCuentaBancaria As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovBancoView)
        actual = target.SelectMovBancosxFechas(FechaInicial, Fechafinal, NroOperacion, IdBanco, IdCuentaBancaria)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectMovBancosxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectMovBancosxIdTest()
        Dim target As DAOMovBancoView = New DAOMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim IdMovBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As MovBancoView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As MovBancoView
        actual = target.SelectMovBancosxId(IdMovBanco)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectUltimoIdentificadorExport
    '''</summary>
    <TestMethod()> _
    Public Sub SelectUltimoIdentificadorExportTest()
        Dim target As DAOMovBancoView = New DAOMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectUltimoIdentificadorExport
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOMovBancoView = New DAOMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim Id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As MovBancoView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As MovBancoView
        actual = target.SelectxId(Id)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdBancoxIdCuentaBancaria
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdBancoxIdCuentaBancariaTest()
        Dim target As DAOMovBancoView = New DAOMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCuentaBancaria As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As MovBancoView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As MovBancoView
        actual = target.SelectxIdBancoxIdCuentaBancaria(IdBanco, IdCuentaBancaria)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoTest()
        Dim target As DAOMovBancoView = New DAOMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovBancoView)
        actual = target.SelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxParamsTest()
        Dim target As DAOMovBancoView = New DAOMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim x As MovBancoView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovBancoView)
        actual = target.SelectxParams(x)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
