﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAONaturalViewTest y se pretende que
'''contenga todas las pruebas unitarias DAONaturalViewTest.
'''</summary>
<TestClass()> _
Public Class DAONaturalViewTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAONaturalView
    '''</summary>
    <TestMethod()> _
    Public Sub DAONaturalViewConstructorTest()
        Dim target As DAONaturalView = New DAONaturalView()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de SelectxDNI
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxDNITest()
        Dim target As DAONaturalView = New DAONaturalView() ' TODO: Inicializar en un valor adecuado
        Dim dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As NaturalView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As NaturalView
        actual = target.SelectxDNI(dni)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAONaturalView = New DAONaturalView() ' TODO: Inicializar en un valor adecuado
        Dim id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As NaturalView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As NaturalView
        actual = target.SelectxId(id)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de _NaturalViewSelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub _NaturalViewSelectAllTest()
        Dim target As DAONaturalView = New DAONaturalView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of NaturalView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of NaturalView)
        actual = target._NaturalViewSelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de _NaturalViewSelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub _NaturalViewSelectAllxNombreTest()
        Dim target As DAONaturalView = New DAONaturalView() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of NaturalView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of NaturalView)
        actual = target._NaturalViewSelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
