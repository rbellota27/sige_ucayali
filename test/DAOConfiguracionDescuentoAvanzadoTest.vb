﻿Imports System

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOConfiguracionDescuentoAvanzadoTest y se pretende que
'''contenga todas las pruebas unitarias DAOConfiguracionDescuentoAvanzadoTest.
'''</summary>
<TestClass()> _
Public Class DAOConfiguracionDescuentoAvanzadoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOConfiguracionDescuentoAvanzado
    '''</summary>
    <TestMethod()> _
    Public Sub DAOConfiguracionDescuentoAvanzadoConstructorTest()
        Dim target As DAOConfiguracionDescuentoAvanzado = New DAOConfiguracionDescuentoAvanzado()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de InsertConfiguracionDescuento
    '''</summary>
    <TestMethod()> _
    Public Sub InsertConfiguracionDescuentoTest()
        Dim target As DAOConfiguracionDescuentoAvanzado = New DAOConfiguracionDescuentoAvanzado() ' TODO: Inicializar en un valor adecuado
        Dim objEnt As ConfiguracionDescuentoAvanzado = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertConfiguracionDescuento(objEnt)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertConfiguracionDescuentoAvanzado
    '''</summary>
    <TestMethod()> _
    Public Sub InsertConfiguracionDescuentoAvanzadoTest()
        Dim target As DAOConfiguracionDescuentoAvanzado = New DAOConfiguracionDescuentoAvanzado() ' TODO: Inicializar en un valor adecuado
        Dim objEnt As ConfiguracionDescuentoAvanzado = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertConfiguracionDescuentoAvanzado(objEnt)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectConfiguracionDescuento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectConfiguracionDescuentoTest()
        Dim target As DAOConfiguracionDescuentoAvanzado = New DAOConfiguracionDescuentoAvanzado() ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idMedPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTipoPv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idcondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim desc_PrecioBaseDscto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ConfiguracionDescuentoAvanzado) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ConfiguracionDescuentoAvanzado)
        actual = target.SelectConfiguracionDescuento(estado, idMedPago, idPerfil, idTipoPv, idcondicionPago, desc_PrecioBaseDscto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectConfiguracionDescuentoAvanzado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectConfiguracionDescuentoAvanzadoTest()
        Dim target As DAOConfiguracionDescuentoAvanzado = New DAOConfiguracionDescuentoAvanzado() ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idMedPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTipoPv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idcondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim desc_PrecioBaseDscto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ConfiguracionDescuentoAvanzado) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ConfiguracionDescuentoAvanzado)
        actual = target.SelectConfiguracionDescuentoAvanzado(estado, idMedPago, idPerfil, idTipoPv, idcondicionPago, desc_PrecioBaseDscto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateConfiguracionDescuento
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateConfiguracionDescuentoTest()
        Dim target As DAOConfiguracionDescuentoAvanzado = New DAOConfiguracionDescuentoAvanzado() ' TODO: Inicializar en un valor adecuado
        Dim objEnt As ConfiguracionDescuentoAvanzado = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateConfiguracionDescuento(objEnt)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getPorcentDctoMax
    '''</summary>
    <TestMethod()> _
    Public Sub getPorcentDctoMaxTest()
        Dim target As DAOConfiguracionDescuentoAvanzado = New DAOConfiguracionDescuentoAvanzado() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.getPorcentDctoMax(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getPorcentDctoMaxAvz
    '''</summary>
    <TestMethod()> _
    Public Sub getPorcentDctoMaxAvzTest()
        Dim target As DAOConfiguracionDescuentoAvanzado = New DAOConfiguracionDescuentoAvanzado() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodigoProd As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.getPorcentDctoMaxAvz(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago, IdTipoOperacion, CodigoProd)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getPrecioBaseDcto
    '''</summary>
    <TestMethod()> _
    Public Sub getPrecioBaseDctoTest()
        Dim target As DAOConfiguracionDescuentoAvanzado = New DAOConfiguracionDescuentoAvanzado() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.getPrecioBaseDcto(IdUsuario, IdTipoPV, IdCondicionPago, IdMedioPago)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
