﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCalidadLineaTest y se pretende que
'''contenga todas las pruebas unitarias DAOCalidadLineaTest.
'''</summary>
<TestClass()> _
Public Class DAOCalidadLineaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCalidadLinea
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCalidadLineaConstructorTest()
        Dim target As DAOCalidadLinea = New DAOCalidadLinea()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de GrabaCalidadLineaT
    '''</summary>
    <TestMethod()> _
    Public Sub GrabaCalidadLineaTTest()
        Dim target As DAOCalidadLinea = New DAOCalidadLinea() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaCalidadLinea As List(Of CalidadLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.GrabaCalidadLineaT(IdLinea, cn, listaCalidadLinea, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoxIdLineaIdTipoExistencia
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoxIdLineaIdTipoExistenciaTest()
        Dim target As DAOCalidadLinea = New DAOCalidadLinea() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipoexistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CalidadLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CalidadLinea)
        actual = target.SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoxIdLineaIdTipoExistenciav2
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoxIdLineaIdTipoExistenciav2Test()
        Dim target As DAOCalidadLinea = New DAOCalidadLinea() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipoexistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CalidadLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CalidadLinea)
        actual = target.SelectAllActivoxIdLineaIdTipoExistenciav2(idlinea, idtipoexistencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
