﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOPostViewTest y se pretende que
'''contenga todas las pruebas unitarias DAOPostViewTest.
'''</summary>
<TestClass()> _
Public Class DAOPostViewTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOPostView
    '''</summary>
    <TestMethod()> _
    Public Sub DAOPostViewConstructorTest()
        Dim target As DAOPostView = New DAOPostView()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOPostView = New DAOPostView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PostView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PostView)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOPostView = New DAOPostView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PostView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PostView)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOPostView = New DAOPostView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PostView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PostView)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdCaja
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdCajaTest()
        Dim target As DAOPostView = New DAOPostView() ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Post) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Post)
        actual = target.SelectCboxIdCaja(IdCaja)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectIdPosxTipoTarjetaCaja
    '''</summary>
    <TestMethod()> _
    Public Sub SelectIdPosxTipoTarjetaCajaTest()
        Dim target As DAOPostView = New DAOPostView() ' TODO: Inicializar en un valor adecuado
        Dim IdTarjeta As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoTarjeta As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectIdPosxTipoTarjetaCaja(IdTarjeta, IdTipoTarjeta, IdCaja)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdBanco
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdBancoTest()
        Dim target As DAOPostView = New DAOPostView() ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PostView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PostView)
        actual = target.SelectxIdBanco(IdBanco)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdBancoxIdCuentaBancaria
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdBancoxIdCuentaBancariaTest()
        Dim target As DAOPostView = New DAOPostView() ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCuentaBancaria As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PostView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PostView)
        actual = target.SelectxIdBancoxIdCuentaBancaria(IdBanco, IdCuentaBancaria)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdCuentaBancaria
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdCuentaBancariaTest()
        Dim target As DAOPostView = New DAOPostView() ' TODO: Inicializar en un valor adecuado
        Dim IdCuentaBancaria As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PostView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PostView)
        actual = target.SelectxIdCuentaBancaria(IdCuentaBancaria)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxParamsTest()
        Dim target As DAOPostView = New DAOPostView() ' TODO: Inicializar en un valor adecuado
        Dim x As PostView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PostView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PostView)
        actual = target.SelectxParams(x)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
