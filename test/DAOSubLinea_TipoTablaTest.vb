﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOSubLinea_TipoTablaTest y se pretende que
'''contenga todas las pruebas unitarias DAOSubLinea_TipoTablaTest.
'''</summary>
<TestClass()> _
Public Class DAOSubLinea_TipoTablaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOSubLinea_TipoTabla
    '''</summary>
    <TestMethod()> _
    Public Sub DAOSubLinea_TipoTablaConstructorTest()
        Dim target As DAOSubLinea_TipoTabla = New DAOSubLinea_TipoTabla()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de AnularSubLineaTipoTabla
    '''</summary>
    <TestMethod()> _
    Public Sub AnularSubLineaTipoTablaTest()
        Dim target As DAOSubLinea_TipoTabla = New DAOSubLinea_TipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.AnularSubLineaTipoTabla(idlinea, idsublinea, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GrabaSubLineaTipoTabla
    '''</summary>
    <TestMethod()> _
    Public Sub GrabaSubLineaTipoTablaTest()
        Dim target As DAOSubLinea_TipoTabla = New DAOSubLinea_TipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of SubLinea_TipoTabla) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.GrabaSubLineaTipoTabla(lista, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllTipoTablaxIdSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTipoTablaxIdSubLineaTest()
        Dim target As DAOSubLinea_TipoTabla = New DAOSubLinea_TipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea_TipoTabla) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea_TipoTabla)
        actual = target.SelectAllTipoTablaxIdSubLinea(idsublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxTipoExistxIdLineaxIdSubLineaxEstado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxTipoExistxIdLineaxIdSubLineaxEstadoTest()
        Dim target As DAOSubLinea_TipoTabla = New DAOSubLinea_TipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim idtipoexist As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea_TipoTabla) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea_TipoTabla)
        actual = target.SelectAllxTipoExistxIdLineaxIdSubLineaxEstado(idtipoexist, idlinea, idsublinea, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdLineaxIdSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdLineaxIdSubLineaTest()
        Dim target As DAOSubLinea_TipoTabla = New DAOSubLinea_TipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tipouso As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea_TipoTabla) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea_TipoTabla)
        actual = target.SelectxIdLineaxIdSubLinea(idlinea, idsublinea, tipouso)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdLineaxIdSubLineaxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdLineaxIdSubLineaxIdProductoTest()
        Dim target As DAOSubLinea_TipoTabla = New DAOSubLinea_TipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tipouso As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea_TipoTabla) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea_TipoTabla)
        actual = target.SelectxIdLineaxIdSubLineaxIdProducto(idlinea, idsublinea, tipouso, idproducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdTipoExistenciaxIdLineaxIdSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTipoExistenciaxIdLineaxIdSubLineaTest()
        Dim target As DAOSubLinea_TipoTabla = New DAOSubLinea_TipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea_TipoTabla) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea_TipoTabla)
        actual = target.SelectxIdTipoExistenciaxIdLineaxIdSubLinea(idlinea, idsublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de TipoTablaSublineaConFig_Paginado
    '''</summary>
    <TestMethod()> _
    Public Sub TipoTablaSublineaConFig_PaginadoTest()
        Dim target As DAOSubLinea_TipoTabla = New DAOSubLinea_TipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim idtipoexist As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea_TipoTabla) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea_TipoTabla)
        actual = target.TipoTablaSublineaConFig_Paginado(idtipoexist, idlinea, idsublinea, estado, PageIndex, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
