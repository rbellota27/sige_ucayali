﻿Imports System.Data

Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOPersonaTest y se pretende que
'''contenga todas las pruebas unitarias DAOPersonaTest.
'''</summary>
<TestClass()> _
Public Class DAOPersonaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOPersona
    '''</summary>
    <TestMethod()> _
    Public Sub DAOPersonaConstructorTest()
        Dim target As DAOPersona = New DAOPersona()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaPersonaTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim persona As Persona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ActualizaPersona(cn, persona, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaPersonaEstado
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaPersonaEstadoTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim persona As Persona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ActualizaPersonaEstado(persona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaPersonaT
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaPersonaTTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim persona As Persona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Nat As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim natural As Natural = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Jur As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Juridica As Juridica = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Rol As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim RolPersona As RolPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Dir As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim LDir As List(Of Direccion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Tel As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim LTel As List(Of Telefono) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Doi As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim LDId As List(Of DocumentoI) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cor As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim LCor As List(Of Correo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Age As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Agente As PersonaTipoAgente = Nothing ' TODO: Inicializar en un valor adecuado
        Dim profesion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim ProfesionPersona As ProfesionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ocupacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim OcupacionPersona As OcupacionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ActualizaPersonaT(persona, Nat, natural, Jur, Juridica, Rol, RolPersona, Dir, LDir, Tel, LTel, Doi, LDId, Cor, LCor, Age, Agente, profesion, ProfesionPersona, ocupacion, OcupacionPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizarPersona
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizarPersonaTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim tipoPersona As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim objPersona As Persona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objNatural As Natural = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objChofer As Chofer = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objEmpleado As Empleado = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objJuridica As Juridica = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListDireccion As List(Of Direccion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListTelefono As List(Of TelefonoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListCorreo As List(Of CorreoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListaDocumentoI As List(Of DocumentoI) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListRolPersona As List(Of RolPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListProfesion As List(Of ProfesionPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListOcupacion As List(Of OcupacionPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ListaContactoEmpresa As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim List_PersonaTipoAgente As List(Of PersonaTipoAgente) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.ActualizarPersona(tipoPersona, objPersona, objNatural, objChofer, objEmpleado, objJuridica, objListDireccion, objListTelefono, objListCorreo, objListaDocumentoI, objListRolPersona, objListProfesion, objListOcupacion, ListaContactoEmpresa, List_PersonaTipoAgente)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaBDPersona
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaBDPersonaTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objPersona As Persona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.InsertaBDPersona(cn, objPersona, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaPersonaTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim persona As Persona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaPersona(cn, persona, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaPersonaT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaPersonaTTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim persona As Persona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Nat As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim natural As Natural = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Jur As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Juridica As Juridica = Nothing ' TODO: Inicializar en un valor adecuado
        Dim rol As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim RolPersona As RolPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Dir As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim LDir As List(Of Direccion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tel As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim LTel As List(Of Telefono) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim doi As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim LDoid As List(Of DocumentoI) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cor As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim LCor As List(Of Correo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Age As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Agente As PersonaTipoAgente = Nothing ' TODO: Inicializar en un valor adecuado
        Dim profesion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim ProfesionPersona As ProfesionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ocupacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim OcupacionPersona As OcupacionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaPersonaT(persona, Nat, natural, Jur, Juridica, rol, RolPersona, Dir, LDir, tel, LTel, doi, LDoid, cor, LCor, Age, Agente, profesion, ProfesionPersona, ocupacion, OcupacionPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaPersonaT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaPersonaTTest1()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim persona As Persona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Nat As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim natural As Natural = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Jur As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Juridica As Juridica = Nothing ' TODO: Inicializar en un valor adecuado
        Dim rol As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim RolPersona As RolPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Dir As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim LDir As List(Of Direccion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tel As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim LTel As List(Of Telefono) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim doi As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim LDoid As List(Of DocumentoI) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cor As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim LCor As List(Of Correo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Age As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Agente As PersonaTipoAgente = Nothing ' TODO: Inicializar en un valor adecuado
        Dim profesion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim ProfesionPersona As ProfesionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ocupacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim OcupacionPersona As OcupacionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaPersonaT(cn, tr, persona, Nat, natural, Jur, Juridica, rol, RolPersona, Dir, LDir, tel, LTel, doi, LDoid, cor, LCor, Age, Agente, profesion, ProfesionPersona, ocupacion, OcupacionPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertarContactoEmpresa
    '''</summary>
    <TestMethod()> _
    Public Sub InsertarContactoEmpresaTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaContactoEmpresa As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertarContactoEmpresa(cn, listaContactoEmpresa, idPersona, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertarNuevaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub InsertarNuevaPersonaTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim tipoPersona As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim objPersona As Persona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objNatural As Natural = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objChofer As Chofer = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objEmpleado As Empleado = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objJuridica As Juridica = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListDireccion As List(Of Direccion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListTelefono As List(Of TelefonoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListCorreo As List(Of CorreoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListaDocumentoI As List(Of DocumentoI) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListRolPersona As List(Of RolPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListProfesion As List(Of ProfesionPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objListOcupacion As List(Of OcupacionPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ListaContactoEmpresa As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim List_PersonaTipoAgente As List(Of PersonaTipoAgente) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.InsertarNuevaPersona(tipoPersona, objPersona, objNatural, objChofer, objEmpleado, objJuridica, objListDireccion, objListTelefono, objListCorreo, objListaDocumentoI, objListRolPersona, objListProfesion, objListOcupacion, ListaContactoEmpresa, List_PersonaTipoAgente)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarPersonaComplemento
    '''</summary>
    <TestMethod()> _
    Public Sub ListarPersonaComplementoTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim idPerona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tipoComplemento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.ListarPersonaComplemento(idPerona, tipoComplemento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de PersonaDato
    '''</summary>
    <TestMethod()> _
    Public Sub PersonaDatoTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim ObjPersonaDato As PersonaDato = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaDato) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaDato)
        actual = target.PersonaDato(ObjPersonaDato)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RegistrarCliente_Ventas
    '''</summary>
    <TestMethod()> _
    Public Sub RegistrarCliente_VentasTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim objPersonaView As PersonaView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TipoPersona As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.RegistrarCliente_Ventas(objPersonaView, TipoPersona, IdEmpresa, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Registrar_MaestroObra
    '''</summary>
    <TestMethod()> _
    Public Sub Registrar_MaestroObraTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim objPersonaView As PersonaView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TipoPersona As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Registrar_MaestroObra(objPersonaView, TipoPersona, IdEmpresa, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPersonaTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Persona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Persona
        actual = target.SelectxIdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Selectx_IdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub Selectx_IdPersonaTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Persona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Persona
        actual = target.Selectx_IdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidancionPerfilNuevoxIdUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub ValidancionPerfilNuevoxIdUsuarioTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidancionPerfilNuevoxIdUsuario(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de deleteDatosPersonaxActualizacion
    '''</summary>
    <TestMethod()> _
    Public Sub deleteDatosPersonaxActualizacionTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim idPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.deleteDatosPersonaxActualizacion(idPersona, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetConsultaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetConsultaPersonaTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim nombres As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim numero As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim ordenar As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetConsultaPersona(nombres, numero, tipo, estado, ordenar)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDescripcionPersona
    '''</summary>
    <TestMethod()> _
    Public Sub getDescripcionPersonaTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.getDescripcionPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getRUC
    '''</summary>
    <TestMethod()> _
    Public Sub getRUCTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.getRUC(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarPersonaFRMproveedor
    '''</summary>
    <TestMethod()> _
    Public Sub listarPersonaFRMproveedorTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Natural = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Natural
        actual = target.listarPersonaFRMproveedor(dni)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarPersonaNaturalPersonaJuridica
    '''</summary>
    <TestMethod()> _
    Public Sub listarPersonaNaturalPersonaJuridicaTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim ruc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.listarPersonaNaturalPersonaJuridica(dni, ruc, nombre, tipo, pageindex, pagesize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarPersonaNoProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub listarPersonaNoProveedorTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim nrodoc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Persona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Persona
        actual = target.listarPersonaNoProveedor(nrodoc)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarxPersonaNaturalxPersonaJuridica
    '''</summary>
    <TestMethod()> _
    Public Sub listarxPersonaNaturalxPersonaJuridicaTest()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim ruc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim RazonApe As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idrol As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdNacionalidad As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, pageindex, pagesize, idrol, estado, IdNacionalidad)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarxPersonaNaturalxPersonaJuridica
    '''</summary>
    <TestMethod()> _
    Public Sub listarxPersonaNaturalxPersonaJuridicaTest1()
        Dim target As DAOPersona = New DAOPersona() ' TODO: Inicializar en un valor adecuado
        Dim dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim ruc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim RazonApe As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, pageindex, pagesize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
