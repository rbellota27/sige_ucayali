﻿Imports System.Data

Imports System

Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAO_ReposicionStockTest y se pretende que
'''contenga todas las pruebas unitarias DAO_ReposicionStockTest.
'''</summary>
<TestClass()> _
Public Class DAO_ReposicionStockTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAO_ReposicionStock
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_ReposicionStockConstructorTest()
        Dim target As DAO_ReposicionStock = New DAO_ReposicionStock()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de CrearCabeceraCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub CrearCabeceraCotizacionTest()
        Dim target As DAO_ReposicionStock = New DAO_ReposicionStock() ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.CrearCabeceraCotizacion(tr, cn, idUsuario, iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CrearDetalleCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub CrearDetalleCotizacionTest()
        Dim target As DAO_ReposicionStock = New DAO_ReposicionStock() ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of be_reposicionStock) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.CrearDetalleCotizacion(tr, cn, idUsuario, lista, idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarObjetosGenerales
    '''</summary>
    <TestMethod()> _
    Public Sub ListarObjetosGeneralesTest()
        Dim target As DAO_ReposicionStock = New DAO_ReposicionStock() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As be_reponerStockListarObjetos = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As be_reponerStockListarObjetos
        actual = target.ListarObjetosGenerales(cn, idUsuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de lista_reposicionStock_datatable
    '''</summary>
    <TestMethod()> _
    Public Sub lista_reposicionStock_datatableTest()
        Dim target As DAO_ReposicionStock = New DAO_ReposicionStock() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim fechainicial As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fechafinal As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacencomparativo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.lista_reposicionStock_datatable(cn, fechainicial, fechafinal, idTienda, idEmpresa, idalmacencomparativo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de lista_reposicionStock_lista
    '''</summary>
    <TestMethod()> _
    Public Sub lista_reposicionStock_listaTest()
        Dim target As DAO_ReposicionStock = New DAO_ReposicionStock() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim fechainicial As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fechafinal As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacencomparativo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of be_listaReponerStock) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of be_listaReponerStock)
        actual = target.lista_reposicionStock_lista(cn, fechainicial, fechafinal, idTienda, idEmpresa, idalmacencomparativo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
