﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTipoExistenciaTest y se pretende que
'''contenga todas las pruebas unitarias DAOTipoExistenciaTest.
'''</summary>
<TestClass()> _
Public Class DAOTipoExistenciaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTipoExistencia
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTipoExistenciaConstructorTest()
        Dim target As DAOTipoExistencia = New DAOTipoExistencia()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaTipoExistencia
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaTipoExistenciaTest()
        Dim target As DAOTipoExistencia = New DAOTipoExistencia() ' TODO: Inicializar en un valor adecuado
        Dim tipoexistencia As TipoExistencia = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaTipoExistencia(tipoexistencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaTipoExistencia
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTipoExistenciaTest()
        Dim target As DAOTipoExistencia = New DAOTipoExistencia() ' TODO: Inicializar en un valor adecuado
        Dim tipoexistencia As TipoExistencia = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaTipoExistencia(tipoexistencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNombreTest()
        Dim target As DAOTipoExistencia = New DAOTipoExistencia() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoExistencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoExistencia)
        actual = target.SelectActivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOTipoExistencia = New DAOTipoExistencia() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoExistencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoExistencia)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOTipoExistencia = New DAOTipoExistencia() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoExistencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoExistencia)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOTipoExistencia = New DAOTipoExistencia() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoExistencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoExistencia)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAOTipoExistencia = New DAOTipoExistencia() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoExistencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoExistencia)
        actual = target.SelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxNombreTest()
        Dim target As DAOTipoExistencia = New DAOTipoExistencia() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoExistencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoExistencia)
        actual = target.SelectInactivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxCodSunat
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxCodSunatTest()
        Dim target As DAOTipoExistencia = New DAOTipoExistencia() ' TODO: Inicializar en un valor adecuado
        Dim codsunat As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoExistencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoExistencia)
        actual = target.SelectxCodSunat(codsunat)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOTipoExistencia = New DAOTipoExistencia() ' TODO: Inicializar en un valor adecuado
        Dim id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoExistencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoExistencia)
        actual = target.SelectxId(id)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateTipoExistencia_Area
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTipoExistencia_AreaTest()
        Dim target As DAOTipoExistencia = New DAOTipoExistencia() ' TODO: Inicializar en un valor adecuado
        Dim tipoexistencia As TipoExistencia = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaLinea_Area As List(Of Linea_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tieneRelacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateTipoExistencia_Area(tipoexistencia, listaLinea_Area, tieneRelacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
