﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOSerieChequeViewTest y se pretende que
'''contenga todas las pruebas unitarias DAOSerieChequeViewTest.
'''</summary>
<TestClass()> _
Public Class DAOSerieChequeViewTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOSerieChequeView
    '''</summary>
    <TestMethod()> _
    Public Sub DAOSerieChequeViewConstructorTest()
        Dim target As DAOSerieChequeView = New DAOSerieChequeView()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOSerieChequeView = New DAOSerieChequeView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SerieChequeView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SerieChequeView)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOSerieChequeView = New DAOSerieChequeView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SerieChequeView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SerieChequeView)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOSerieChequeView = New DAOSerieChequeView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SerieChequeView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SerieChequeView)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdBancoxIdCuentaBancaria
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdBancoxIdCuentaBancariaTest()
        Dim target As DAOSerieChequeView = New DAOSerieChequeView() ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCuentaBancaria As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SerieChequeView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SerieChequeView)
        actual = target.SelectCboxIdBancoxIdCuentaBancaria(IdBanco, IdCuentaBancaria)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdCuentaBancaria
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdCuentaBancariaTest()
        Dim target As DAOSerieChequeView = New DAOSerieChequeView() ' TODO: Inicializar en un valor adecuado
        Dim IdCuentaBancaria As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SerieChequeView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SerieChequeView)
        actual = target.SelectCboxIdCuentaBancaria(IdCuentaBancaria)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOSerieChequeView = New DAOSerieChequeView() ' TODO: Inicializar en un valor adecuado
        Dim ID As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As SerieChequeView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As SerieChequeView
        actual = target.SelectxId(ID)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxParamsTest()
        Dim target As DAOSerieChequeView = New DAOSerieChequeView() ' TODO: Inicializar en un valor adecuado
        Dim x As SerieChequeView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SerieChequeView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SerieChequeView)
        actual = target.SelectxParams(x)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
