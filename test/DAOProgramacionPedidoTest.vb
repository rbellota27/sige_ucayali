﻿Imports System

Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOProgramacionPedidoTest y se pretende que
'''contenga todas las pruebas unitarias DAOProgramacionPedidoTest.
'''</summary>
<TestClass()> _
Public Class DAOProgramacionPedidoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOProgramacionPedido
    '''</summary>
    <TestMethod()> _
    Public Sub DAOProgramacionPedidoConstructorTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de GetSemanaDYear
    '''</summary>
    <TestMethod()> _
    Public Sub GetSemanaDYearTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProgramacionPedido) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProgramacionPedido)
        actual = target.GetSemanaDYear
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GetYear
    '''</summary>
    <TestMethod()> _
    Public Sub GetYearTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProgramacionPedido) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProgramacionPedido)
        actual = target.GetYear
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertDocumentoProgramacionPedido
    '''</summary>
    <TestMethod()> _
    Public Sub InsertDocumentoProgramacionPedidoTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim obj As ProgramacionPedido = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertDocumentoProgramacionPedido(obj, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertProgramacionPedido
    '''</summary>
    <TestMethod()> _
    Public Sub InsertProgramacionPedidoTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim obj As ProgramacionPedido = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertProgramacionPedido(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarDocProgPedidoxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub ListarDocProgPedidoxIdDocumentoTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As ProgramacionPedido = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As ProgramacionPedido
        actual = target.ListarDocProgPedidoxIdDocumento(idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarIdSemanaxIdYear
    '''</summary>
    <TestMethod()> _
    Public Sub ListarIdSemanaxIdYearTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim idyear As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.ListarIdSemanaxIdYear(idyear)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarProgPedido_A_Documento
    '''</summary>
    <TestMethod()> _
    Public Sub ListarProgPedido_A_DocumentoTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim idyear As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.ListarProgPedido_A_Documento(idyear, idTienda, idAlmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarProgramacionMantenimiento
    '''</summary>
    <TestMethod()> _
    Public Sub ListarProgramacionMantenimientoTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim idyear As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As ProgramacionPedido = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As ProgramacionPedido
        actual = target.ListarProgramacionMantenimiento(idyear, idsemana)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarProgramacionPedido
    '''</summary>
    <TestMethod()> _
    Public Sub ListarProgramacionPedidoTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim idyear As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.ListarProgramacionPedido(idyear)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ProgramacionPedidoSelectSemanaxYear
    '''</summary>
    <TestMethod()> _
    Public Sub ProgramacionPedidoSelectSemanaxYearTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim IdYear As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProgramacionPedido) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProgramacionPedido)
        actual = target.ProgramacionPedidoSelectSemanaxYear(IdYear)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ProgramacionPedidoSelectYear
    '''</summary>
    <TestMethod()> _
    Public Sub ProgramacionPedidoSelectYearTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProgramacionPedido) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProgramacionPedido)
        actual = target.ProgramacionPedidoSelectYear
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ProgramacionPedidoSelectxFecha
    '''</summary>
    <TestMethod()> _
    Public Sub ProgramacionPedidoSelectxFechaTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As ProgramacionPedido = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As ProgramacionPedido
        actual = target.ProgramacionPedidoSelectxFecha(fecha)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ProgramacionPedidoSelectxIdYearxIdSemana
    '''</summary>
    <TestMethod()> _
    Public Sub ProgramacionPedidoSelectxIdYearxIdSemanaTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim IdYear As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As ProgramacionPedido = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As ProgramacionPedido
        actual = target.ProgramacionPedidoSelectxIdYearxIdSemana(IdYear, IdSemana)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateDocumentoProgramacionPedido
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateDocumentoProgramacionPedidoTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim obj As ProgramacionPedido = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateDocumentoProgramacionPedido(obj, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateProgramacionPedido
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateProgramacionPedidoTest()
        Dim target As DAOProgramacionPedido = New DAOProgramacionPedido() ' TODO: Inicializar en un valor adecuado
        Dim obj As ProgramacionPedido = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateProgramacionPedido(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
