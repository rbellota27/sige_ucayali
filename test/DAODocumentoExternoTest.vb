﻿Imports System.Data

Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoExternoTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoExternoTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoExternoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoExterno
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoExternoConstructorTest()
        Dim target As DAODocumentoExterno = New DAODocumentoExterno()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoExterno_Buscar
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoExterno_BuscarTest()
        Dim target As DAODocumentoExterno = New DAODocumentoExterno() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoExterno_Buscar(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoExterno_BuscarReferencia
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoExterno_BuscarReferenciaTest()
        Dim target As DAODocumentoExterno = New DAODocumentoExterno() ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechas As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim doc_Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim doc_Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipodocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoExterno_BuscarReferencia(tipo, fechas, doc_Codigo, doc_Serie, idpersona, idtipodocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoExterno_Busqueda
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoExterno_BusquedaTest()
        Dim target As DAODocumentoExterno = New DAODocumentoExterno() ' TODO: Inicializar en un valor adecuado
        Dim fechas As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipodocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoExterno_Busqueda(fechas, idpersona, idtipodocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoExterno_ConsultarGuia
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoExterno_ConsultarGuiaTest()
        Dim target As DAODocumentoExterno = New DAODocumentoExterno() ' TODO: Inicializar en un valor adecuado
        Dim cadIdDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoExterno_ConsultarGuia(cadIdDocumento, idProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoExterno_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoExterno_DeshacerMovTest()
        Dim target As DAODocumentoExterno = New DAODocumentoExterno() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim deleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteAnexoDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteDetalleConcepto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteObservaciones As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteMovCuentaCXP As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteMontoRegimen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoExterno_DeshacerMov(IdDocumento, deleteRelacionDocumento, deleteAnexoDocumento, deleteDetalleConcepto, deleteObservaciones, deleteMovCuentaCXP, deleteMontoRegimen, anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoExterno_RelacionDoc
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoExterno_RelacionDocTest()
        Dim target As DAODocumentoExterno = New DAODocumentoExterno() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocumentoView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocumentoView
        actual = target.DocumentoExterno_RelacionDoc(idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoExterno_SerieNumero
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoExterno_SerieNumeroTest()
        Dim target As DAODocumentoExterno = New DAODocumentoExterno() ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim numero As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipodocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocumentoView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocumentoView
        actual = target.DocumentoExterno_SerieNumero(serie, numero, idtipodocumento, idpersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Documento_Externo_Insert
    '''</summary>
    <TestMethod()> _
    Public Sub Documento_Externo_InsertTest()
        Dim target As DAODocumentoExterno = New DAODocumentoExterno() ' TODO: Inicializar en un valor adecuado
        Dim obj As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista_detalle As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObservacion As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Documento_Externo_Insert(obj, lista_detalle, objObservacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Documento_Externo_Update
    '''</summary>
    <TestMethod()> _
    Public Sub Documento_Externo_UpdateTest()
        Dim target As DAODocumentoExterno = New DAODocumentoExterno() ' TODO: Inicializar en un valor adecuado
        Dim obj As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista_detalle As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObservacion As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Documento_Externo_Update(obj, lista_detalle, objObservacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_Cuentas_Por_Pagar_Cargos
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_Cuentas_Por_Pagar_CargosTest()
        Dim target As DAODocumentoExterno = New DAODocumentoExterno() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fecha As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_Cuentas_Por_Pagar_Cargos(IdEmpresa, IdMoneda, IdPersona, idTienda, fecha)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
