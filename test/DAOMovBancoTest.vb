﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMovBancoTest y se pretende que
'''contenga todas las pruebas unitarias DAOMovBancoTest.
'''</summary>
<TestClass()> _
Public Class DAOMovBancoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMovBanco
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMovBancoConstructorTest()
        Dim target As DAOMovBanco = New DAOMovBanco()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de GetVectorParametros
    '''</summary>
    <TestMethod()> _
    Public Sub GetVectorParametrosTest()
        Dim target As DAOMovBanco = New DAOMovBanco() ' TODO: Inicializar en un valor adecuado
        Dim x As MovBanco = Nothing ' TODO: Inicializar en un valor adecuado
        Dim op As DAOMantenedor.modo_query = New DAOMantenedor.modo_query() ' TODO: Inicializar en un valor adecuado
        Dim expected() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual() As SqlParameter
        actual = target.GetVectorParametros(x, op)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertAll
    '''</summary>
    <TestMethod()> _
    Public Sub InsertAllTest()
        Dim target As DAOMovBanco = New DAOMovBanco() ' TODO: Inicializar en un valor adecuado
        Dim x As MovBanco = Nothing ' TODO: Inicializar en un valor adecuado
        Dim L As List(Of Anexo_MovBanco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertAll(x, L)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertMovBancoExcel
    '''</summary>
    <TestMethod()> _
    Public Sub InsertMovBancoExcelTest()
        Dim target As DAOMovBanco = New DAOMovBanco() ' TODO: Inicializar en un valor adecuado
        Dim x As MovBancoView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertMovBancoExcel(x, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTTest()
        Dim target As DAOMovBanco = New DAOMovBanco() ' TODO: Inicializar en un valor adecuado
        Dim x As MovBanco = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertT(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertT_GetID
    '''</summary>
    <TestMethod()> _
    Public Sub InsertT_GetIDTest()
        Dim target As DAOMovBanco = New DAOMovBanco() ' TODO: Inicializar en un valor adecuado
        Dim x As MovBanco = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertT_GetID(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInsercionExcel
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInsercionExcelTest()
        Dim target As DAOMovBanco = New DAOMovBanco() ' TODO: Inicializar en un valor adecuado
        Dim IdentificadorExcel As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovBancoView)
        actual = target.SelectInsercionExcel(IdentificadorExcel)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectTiendaExcelImport
    '''</summary>
    <TestMethod()> _
    Public Sub SelectTiendaExcelImportTest()
        Dim target As DAOMovBanco = New DAOMovBanco() ' TODO: Inicializar en un valor adecuado
        Dim tie_Estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectTiendaExcelImport(tie_Estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateAll
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateAllTest()
        Dim target As DAOMovBanco = New DAOMovBanco() ' TODO: Inicializar en un valor adecuado
        Dim x As MovBanco = Nothing ' TODO: Inicializar en un valor adecuado
        Dim L As List(Of Anexo_MovBanco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateAll(x, L)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateT
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTTest()
        Dim target As DAOMovBanco = New DAOMovBanco() ' TODO: Inicializar en un valor adecuado
        Dim x As MovBanco = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateT(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
