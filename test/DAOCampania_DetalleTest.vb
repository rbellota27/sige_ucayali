﻿Imports System

Imports System.Collections.Generic

Imports System.Data

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCampania_DetalleTest y se pretende que
'''contenga todas las pruebas unitarias DAOCampania_DetalleTest.
'''</summary>
<TestClass()> _
Public Class DAOCampania_DetalleTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCampania_Detalle
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCampania_DetalleConstructorTest()
        Dim target As DAOCampania_Detalle = New DAOCampania_Detalle()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Campania_Detalle_DeletexParams
    '''</summary>
    <TestMethod()> _
    Public Sub Campania_Detalle_DeletexParamsTest()
        Dim target As DAOCampania_Detalle = New DAOCampania_Detalle() ' TODO: Inicializar en un valor adecuado
        Dim IdCampania As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.Campania_Detalle_DeletexParams(IdCampania, IdProducto, IdUnidadMedida, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Campania_Detalle_Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub Campania_Detalle_RegistrarTest()
        Dim target As DAOCampania_Detalle = New DAOCampania_Detalle() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objCampania_Detalle As Campania_Detalle = Nothing ' TODO: Inicializar en un valor adecuado
        target.Campania_Detalle_Registrar(cn, tr, objCampania_Detalle)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Campania_Detalle_Select
    '''</summary>
    <TestMethod()> _
    Public Sub Campania_Detalle_SelectTest()
        Dim target As DAOCampania_Detalle = New DAOCampania_Detalle() ' TODO: Inicializar en un valor adecuado
        Dim IdCampania As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim prod_codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim prod_nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim PageNumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Campania_Detalle) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Campania_Detalle)
        actual = target.Campania_Detalle_Select(IdCampania, IdTipoExistencia, IdLinea, IdSubLinea, prod_codigo, prod_nombre, PageNumber, PageSize, tabla)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Campania_Detalle_SelectxIdCampania
    '''</summary>
    <TestMethod()> _
    Public Sub Campania_Detalle_SelectxIdCampaniaTest()
        Dim target As DAOCampania_Detalle = New DAOCampania_Detalle() ' TODO: Inicializar en un valor adecuado
        Dim IdCampania As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Campania_Detalle) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Campania_Detalle)
        actual = target.Campania_Detalle_SelectxIdCampania(IdCampania)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Campania_Detalle_Vigente
    '''</summary>
    <TestMethod()> _
    Public Sub Campania_Detalle_VigenteTest()
        Dim target As DAOCampania_Detalle = New DAOCampania_Detalle() ' TODO: Inicializar en un valor adecuado
        Dim IdCampania As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Campania_Detalle = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Campania_Detalle
        actual = target.Campania_Detalle_Vigente(IdCampania, IdProducto, IdUnidadMedida)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Campania_Detalle_Vigente_SelectxParams_DT
    '''</summary>
    <TestMethod()> _
    Public Sub Campania_Detalle_Vigente_SelectxParams_DTTest()
        Dim target As DAOCampania_Detalle = New DAOCampania_Detalle() ' TODO: Inicializar en un valor adecuado
        Dim IdCampania As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.Campania_Detalle_Vigente_SelectxParams_DT(IdCampania, IdEmpresa, IdTienda, IdProducto, IdUnidadMedida, Fecha)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Campania_SelectProducto_AddDetallexParams
    '''</summary>
    <TestMethod()> _
    Public Sub Campania_SelectProducto_AddDetallexParamsTest()
        Dim target As DAOCampania_Detalle = New DAOCampania_Detalle() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Tabla_IdProducto As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Campania_Detalle) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Campania_Detalle)
        actual = target.Campania_SelectProducto_AddDetallexParams(IdLinea, IdSubLinea, Tabla_IdProducto, IdTienda, IdTipoPV)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
