﻿Imports System

Imports System.Data

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCxCREsumenTest y se pretende que
'''contenga todas las pruebas unitarias DAOCxCREsumenTest.
'''</summary>
<TestClass()> _
Public Class DAOCxCREsumenTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCxCREsumen
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCxCREsumenConstructorTest()
        Dim target As DAOCxCREsumen = New DAOCxCREsumen()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de GetDetalleAbonos
    '''</summary>
    <TestMethod()> _
    Public Sub GetDetalleAbonosTest()
        Dim target As DAOCxCREsumen = New DAOCxCREsumen() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CxCResumenView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CxCResumenView)
        actual = target.GetDetalleAbonos(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaCxCResumen
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaCxCResumenTest()
        Dim target As DAOCxCREsumen = New DAOCxCREsumen() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CxCResumenView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CxCResumenView)
        actual = target.SelectGrillaCxCResumen(idEmpresa, idTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaCxCResumenCliente
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaCxCResumenClienteTest()
        Dim target As DAOCxCREsumen = New DAOCxCREsumen() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CxCResumenView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CxCResumenView)
        actual = target.SelectGrillaCxCResumenCliente(idEmpresa, idtienda, idPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaCxCResumenDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaCxCResumenDocumentoTest()
        Dim target As DAOCxCREsumen = New DAOCxCREsumen() ' TODO: Inicializar en un valor adecuado
        Dim Idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CxCResumenView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CxCResumenView)
        actual = target.SelectGrillaCxCResumenDocumento(Idempresa, idtienda, idmoneda, idpersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getGuiaRecepcionOrdendespacho
    '''</summary>
    <TestMethod()> _
    Public Sub getGuiaRecepcionOrdendespachoTest()
        Dim target As DAOCxCREsumen = New DAOCxCREsumen() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipodocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getGuiaRecepcionOrdendespacho(idempresa, idalmacen, idtipodocumento, fechainicio, fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getInventarioValorizado
    '''</summary>
    <TestMethod()> _
    Public Sub getInventarioValorizadoTest()
        Dim target As DAOCxCREsumen = New DAOCxCREsumen() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getInventarioValorizado(IdEmpresa, IdAlmacen, IdLinea, IdSubLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getKardexValorizado
    '''</summary>
    <TestMethod()> _
    Public Sub getKardexValorizadoTest()
        Dim target As DAOCxCREsumen = New DAOCxCREsumen() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Signo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Cantidad As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getKardexValorizado(IdEmpresa, IdAlmacen, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto, Signo, Cantidad)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getRankingProductos
    '''</summary>
    <TestMethod()> _
    Public Sub getRankingProductosTest()
        Dim target As DAOCxCREsumen = New DAOCxCREsumen() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nprimeros As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdVendedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getRankingProductos(idempresa, idtienda, nprimeros, fechainicio, fechafin, idlinea, idsublinea, IdVendedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getUtilidadxProducto
    '''</summary>
    <TestMethod()> _
    Public Sub getUtilidadxProductoTest()
        Dim target As DAOCxCREsumen = New DAOCxCREsumen() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getUtilidadxProducto(idempresa, idtienda, fechainicio, fechafin, idlinea, idsublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
