﻿Imports System.Data

Imports System.Collections.Generic

Imports System

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOProductoTest y se pretende que
'''contenga todas las pruebas unitarias DAOProductoTest.
'''</summary>
<TestClass()> _
Public Class DAOProductoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOProducto
    '''</summary>
    <TestMethod()> _
    Public Sub DAOProductoConstructorTest()
        Dim target As DAOProducto = New DAOProducto()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaProducto
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaProductoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim producto As Producto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaProducto(producto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaProductoPrecioCompra
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaProductoPrecioCompraTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim precioCompra As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaProductoPrecioCompra(cn, tr, idProducto, precioCompra, IdMoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaProductoTipoMercaderia
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaProductoTipoMercaderiaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim prod As Producto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaProductoTipoMercaderia(prod, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaVehiculoxEstadoProxSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaVehiculoxEstadoProxSubLineaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim Vehiculo As Vehiculo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ActualizaVehiculoxEstadoProxSubLinea(Vehiculo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de BuscarProducto_Paginado
    '''</summary>
    <TestMethod()> _
    Public Sub BuscarProducto_PaginadoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEstadoProd As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodigoProveedor As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Tb As List(Of ProductoTipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim pageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of GrillaProducto_M) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of GrillaProducto_M)
        actual = target.BuscarProducto_Paginado(IdTipoExistencia, IdLinea, IdSubLinea, IdEstadoProd, Nombre, Codigo, CodigoProveedor, Tb, pageIndex, pagesize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de BuscarProducto_PaginadoConsulta
    '''</summary>
    <TestMethod()> _
    Public Sub BuscarProducto_PaginadoConsultaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Tb As List(Of ProductoTipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim pageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of GrillaProducto_M) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of GrillaProducto_M)
        actual = target.BuscarProducto_PaginadoConsulta(IdTipoExistencia, IdLinea, Nombre, Codigo, Tb, pageIndex, pagesize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeleteVehiculoxDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DeleteVehiculoxDocumentoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim Vehiculo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.DeleteVehiculoxDocumento(Vehiculo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de EliminarxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub EliminarxIdProductoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.EliminarxIdProducto(cn, tr, idproducto)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaProducto
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaProductoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim producto As Producto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaProducto(producto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaProductoTipoMercaderia
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaProductoTipoMercaderiaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim prod As Producto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaProductoTipoMercaderia(prod, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaVehiculoxEstadoProxSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaVehiculoxEstadoProxSubLineaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim Vehiculo As Vehiculo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaVehiculoxEstadoProxSubLinea(Vehiculo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de LongitudProducto
    '''</summary>
    <TestMethod()> _
    Public Sub LongitudProductoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim expected As Producto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Producto
        actual = target.LongitudProducto
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ProductoCostoCompraSelectxIdSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub ProductoCostoCompraSelectxIdSubLineaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Prod_Codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Prod_Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Tabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoUMCostoCompraView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoUMCostoCompraView)
        actual = target.ProductoCostoCompraSelectxIdSubLinea(IdTipoExistencia, IdLinea, IdSubLinea, Prod_Codigo, Prod_Nombre, Tabla)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ProductoMercaderiaSelectxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub ProductoMercaderiaSelectxIdProductoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Producto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Producto
        actual = target.ProductoMercaderiaSelectxIdProducto(IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ProductoSelectxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub ProductoSelectxIdProductoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As ProductoView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As ProductoView
        actual = target.ProductoSelectxIdProducto(IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ProductoValidadMovAlmacenIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub ProductoValidadMovAlmacenIdProductoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Producto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Producto
        actual = target.ProductoValidadMovAlmacenIdProducto(IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Producto_ConsultaEquivalenciaProductoxParams
    '''</summary>
    <TestMethod()> _
    Public Sub Producto_ConsultaEquivalenciaProductoxParamsTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida_Entrada As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida_Salida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Cantidad_Ingreso As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim TablaUM As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim utilizarRedondeoUp As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.Producto_ConsultaEquivalenciaProductoxParams(IdProducto, IdUnidadMedida_Entrada, IdUnidadMedida_Salida, Cantidad_Ingreso, TablaUM, utilizarRedondeoUp)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Producto_ListarProductosxParams
    '''</summary>
    <TestMethod()> _
    Public Sub Producto_ListarProductosxParamsTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPais As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdFabricante As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMarca As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdModelo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdFormato As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEstilo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTransito As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCalidad As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoView)
        actual = target.Producto_ListarProductosxParams(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdEmpresa, IdAlmacen, pagesize, pagenumber, IdPais, IdFabricante, IdMarca, IdModelo, IdFormato, IdProveedor, IdEstilo, IdTransito, IdCalidad)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Producto_ListarProductosxParamsReporte
    '''</summary>
    <TestMethod()> _
    Public Sub Producto_ListarProductosxParamsReporteTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPais As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdFabricante As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMarca As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdModelo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdFormato As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEstilo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTransito As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCalidad As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoView)
        actual = target.Producto_ListarProductosxParamsReporte(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdEmpresa, pagesize, pagenumber, IdPais, IdFabricante, IdMarca, IdModelo, IdFormato, IdProveedor, IdEstilo, IdTransito, IdCalidad)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Producto_ListarProductosxParams_V2
    '''</summary>
    <TestMethod()> _
    Public Sub Producto_ListarProductosxParams_V2Test()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tableTablaTipoValor As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoView)
        actual = target.Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdEmpresa, IdAlmacen, pagesize, pagenumber, codigoProducto, IdTipoExistencia, tableTablaTipoValor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Producto_ListarProductosxParams_V2Reporte
    '''</summary>
    <TestMethod()> _
    Public Sub Producto_ListarProductosxParams_V2ReporteTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tableTablaTipoValor As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoView)
        actual = target.Producto_ListarProductosxParams_V2Reporte(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdEmpresa, IdAlmacen, pagesize, pagenumber, codigoProducto, tableTablaTipoValor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReporteAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub ReporteAlmacenTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim Linea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim SubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Fecha As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Tienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ReporteAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ReporteAlmacen)
        actual = target.ReporteAlmacen(Linea, SubLinea, Fecha, Tienda, IdAlmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxIdProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxIdProveedorTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectActivoxIdProveedor(idProveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxLineaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectActivoxLinea(idLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNombreTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectActivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxSubLineaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectActivoxSubLinea(idSublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxIdProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxIdProveedorTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectAllxIdProveedor(idProveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxLineaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectAllxLinea(idLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxSubLineaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectAllxSubLinea(idSubLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectBusquedaProdxParams_Standar_Paginado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectBusquedaProdxParams_Standar_PaginadoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nomProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim codSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of GrillaProducto_M) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of GrillaProducto_M)
        actual = target.SelectBusquedaProdxParams_Standar_Paginado(idlinea, idsublinea, nomProducto, codSubLinea, pagenumber, pagesize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectEquivalenciaEntreUM
    '''</summary>
    <TestMethod()> _
    Public Sub SelectEquivalenciaEntreUMTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUM_Origen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUM_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Escalar As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectEquivalenciaEntreUM(IdProducto, IdUM_Origen, IdUM_Destino, Escalar)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectEquivalenciaUMPrincipal
    '''</summary>
    <TestMethod()> _
    Public Sub SelectEquivalenciaUMPrincipalTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim escalar As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectEquivalenciaUMPrincipal(cn, tr, IdProducto, IdUnidadMedida, escalar)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaProd_E
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaProd_ETest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim Serie As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdEstado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of GrillaProducto_E) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of GrillaProducto_E)
        actual = target.SelectGrillaProd_E(Serie, Nombre, IdEstado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaProd_M
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaProd_MTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEstadoProd As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Tb As List(Of ProductoTipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of GrillaProducto_M) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of GrillaProducto_M)
        actual = target.SelectGrillaProd_M(IdLinea, IdSubLinea, IdEstadoProd, Nombre, Codigo, Tb)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaProd_M
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaProd_MTest1()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEstadoProd As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of GrillaProducto_M) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of GrillaProducto_M)
        actual = target.SelectGrillaProd_M(IdLinea, IdSubLinea, IdProveedor, IdEstadoProd, Nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaVehiculo_Buscar
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaVehiculo_BuscarTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim Placa As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Modelo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Constancia As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim EstadoPro As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Vehiculo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Vehiculo)
        actual = target.SelectGrillaVehiculo_Buscar(Placa, Modelo, Constancia, EstadoPro)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxIdProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxIdProveedorTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectInactivoxIdProveedor(idProveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxLineaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectInactivoxLinea(idLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxNombreTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectInactivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxSubLineaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectInactivoxSubLinea(idSubLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectOrdenxIdSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectOrdenxIdSubLineaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectOrdenxIdSubLinea(idsublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectPrecioCompra
    '''</summary>
    <TestMethod()> _
    Public Sub SelectPrecioCompraTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectPrecioCompra(cn, IdProducto, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectPrecioCompra
    '''</summary>
    <TestMethod()> _
    Public Sub SelectPrecioCompraTest1()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectPrecioCompra(IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectProductoxCodigoxDescripcion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectProductoxCodigoxDescripcionTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim CodigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim NombreProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoView)
        actual = target.SelectProductoxCodigoxDescripcion(CodigoProducto, NombreProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectProductoxLineaSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectProductoxLineaSubLineaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectProductoxLineaSubLinea(idlinea, idsublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectUMPrincipal
    '''</summary>
    <TestMethod()> _
    Public Sub SelectUMPrincipalTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.SelectUMPrincipal(cn, tr, IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectUMPrincipalxIdSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectUMPrincipalxIdSubLineaTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nomProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of GrillaProducto_M) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of GrillaProducto_M)
        actual = target.SelectUMPrincipalxIdSubLinea(idlinea, idsublinea, nomProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectxId(id)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxSubLinea_Cbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxSubLinea_CboTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Producto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Producto)
        actual = target.SelectxSubLinea_Cbo(idSubLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateOrdenxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateOrdenxIdProductoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim orden As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.UpdateOrdenxIdProducto(cn, tr, idproducto, orden)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarAtributos
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarAtributosTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim NoVisible As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim prod_Codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim prod_Atributos As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Producto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Producto
        actual = target.ValidarAtributos(NoVisible, prod_Codigo, prod_Atributos)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_Get_CodProd_Lin_SubLin_AutoGen
    '''</summary>
    <TestMethod()> _
    Public Sub fx_Get_CodProd_Lin_SubLin_AutoGenTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Longitud As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.fx_Get_CodProd_Lin_SubLin_AutoGen(IdSublinea, Longitud)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_getValorEquivalenteProducto
    '''</summary>
    <TestMethod()> _
    Public Sub fx_getValorEquivalenteProductoTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida_Origen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim valor As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.fx_getValorEquivalenteProducto(IdProducto, IdUnidadMedida_Origen, IdUnidadMedida_Destino, valor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de productoAfectoSublineaForUpdate
    '''</summary>
    <TestMethod()> _
    Public Sub productoAfectoSublineaForUpdateTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.productoAfectoSublineaForUpdate(idsublinea, idproducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de productoAfectoSublineaforInsert
    '''</summary>
    <TestMethod()> _
    Public Sub productoAfectoSublineaforInsertTest()
        Dim target As DAOProducto = New DAOProducto() ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.productoAfectoSublineaforInsert(idsublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
