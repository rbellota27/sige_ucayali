﻿Imports System

Imports System.Data

Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOOrdenPedidoSucursalTest y se pretende que
'''contenga todas las pruebas unitarias DAOOrdenPedidoSucursalTest.
'''</summary>
<TestClass()> _
Public Class DAOOrdenPedidoSucursalTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOOrdenPedidoSucursal
    '''</summary>
    <TestMethod()> _
    Public Sub DAOOrdenPedidoSucursalConstructorTest()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DetalleDocumento_Ref
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleDocumento_RefTest()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DetalleDocumento_Ref(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoOrdenPedidoSucursal_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoOrdenPedidoSucursal_DeshacerMovTest()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim deleteDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteMovAlmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteRelacionDoc As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteObservaciones As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePuntoPartida As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deletePuntoLlegada As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteAnexoDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteAnexoDetalleD As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoOrdenPedidoSucursal_DeshacerMov(IdDocumento, deleteDetalleDocumento, deleteMovAlmacen, deleteRelacionDoc, deleteObservaciones, DeletePuntoPartida, deletePuntoLlegada, deleteAnexoDocumento, deleteAnexoDetalleD, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoOrdenPedidoSucursal_SelectDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoOrdenPedidoSucursal_SelectDetalleTest()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DocumentoOrdenPedidoSucursal_SelectDetalle(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoOrdenPedidoSucursal_SelectDetalle_Aprobacion_DT
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoOrdenPedidoSucursal_SelectDetalle_Aprobacion_DTTest()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.DocumentoOrdenPedidoSucursal_SelectDetalle_Aprobacion_DT(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoOrdenPedidoSucursal_ValDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoOrdenPedidoSucursal_ValDocumentoRefTest()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.DocumentoOrdenPedidoSucursal_ValDocumentoRef(IdDocumentoRef)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoOrdenPedido_AprobarCantidadOPInsert
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoOrdenPedido_AprobarCantidadOPInsertTest()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal() ' TODO: Inicializar en un valor adecuado
        Dim IdDetalleDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CantidadAprob As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoOrdenPedido_AprobarCantidadOPInsert(IdDetalleDocumento, CantidadAprob, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoOrdenPedido_DeshacerCantidadAprobada
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoOrdenPedido_DeshacerCantidadAprobadaTest()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoOrdenPedido_DeshacerCantidadAprobada(IdDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoOrdenPedido_SelectxParams_Aprobacion
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoOrdenPedido_SelectxParams_AprobacionTest()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim EstadoAprob As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.DocumentoOrdenPedido_SelectxParams_Aprobacion(IdEmpresa, IdAlmacen, FechaInicio, FechaFin, EstadoAprob, Serie, Codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Documento_BuscarDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub Documento_BuscarDocumentoRefTest()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdtipoDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.Documento_BuscarDocumentoRef(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, IdtipoDocumentoRef, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Documento_BuscarDocumentoRef2
    '''</summary>
    <TestMethod()> _
    Public Sub Documento_BuscarDocumentoRef2Test()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdtipoDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.Documento_BuscarDocumentoRef2(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, IdtipoDocumentoRef, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenPedidoAprobacion
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenPedidoAprobacionTest()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim yeari As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanai As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim yearf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanaF As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FiltraSemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tpAlmacen As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.OrdenPedidoAprobacion(idempresa, iddocumento, FechaInicio, Fechafin, yeari, semanai, yearf, semanaF, FiltraSemana, tpAlmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReporteAprobacionPedido
    '''</summary>
    <TestMethod()> _
    Public Sub ReporteAprobacionPedidoTest()
        Dim target As DAOOrdenPedidoSucursal = New DAOOrdenPedidoSucursal() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacenDA As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim yeari As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanai As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim yearf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanaF As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FiltraSemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim stocktransitoxtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tpAlmacen As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.ReporteAprobacionPedido(idempresa, idtienda, idalmacenDA, iddocumento, FechaInicio, Fechafin, yeari, semanai, yearf, semanaF, FiltraSemana, stocktransitoxtienda, tpAlmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
