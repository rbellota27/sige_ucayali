﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMedioP_CondicionPTest y se pretende que
'''contenga todas las pruebas unitarias DAOMedioP_CondicionPTest.
'''</summary>
<TestClass()> _
Public Class DAOMedioP_CondicionPTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMedioP_CondicionP
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMedioP_CondicionPConstructorTest()
        Dim target As DAOMedioP_CondicionP = New DAOMedioP_CondicionP()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeletexCondicionPago_MedioPagoBorrar
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexCondicionPago_MedioPagoBorrarTest()
        Dim target As DAOMedioP_CondicionP = New DAOMedioP_CondicionP() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexCondicionPago_MedioPagoBorrar(cn, tr, IdCondicionPago)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaListaCondicionPago
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaListaCondicionPagoTest()
        Dim target As DAOMedioP_CondicionP = New DAOMedioP_CondicionP() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of MedioPago_CondicionPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionP As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.InsertaListaCondicionPago(cn, tr, lista, IdCondicionP)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de _CondicionPago_MedioPagoSelectxIdCondicionPago
    '''</summary>
    <TestMethod()> _
    Public Sub _CondicionPago_MedioPagoSelectxIdCondicionPagoTest()
        Dim target As DAOMedioP_CondicionP = New DAOMedioP_CondicionP() ' TODO: Inicializar en un valor adecuado
        Dim IdMedioP As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago_CondicionPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago_CondicionPago)
        actual = target._CondicionPago_MedioPagoSelectxIdCondicionPago(IdMedioP)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
