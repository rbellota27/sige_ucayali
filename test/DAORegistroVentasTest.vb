﻿Imports System.Data

Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAORegistroVentasTest y se pretende que
'''contenga todas las pruebas unitarias DAORegistroVentasTest.
'''</summary>
<TestClass()> _
Public Class DAORegistroVentasTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAORegistroVentas
    '''</summary>
    <TestMethod()> _
    Public Sub DAORegistroVentasConstructorTest()
        Dim target As DAORegistroVentas = New DAORegistroVentas()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de NombreProcedure
    '''</summary>
    <TestMethod(), _
     DeploymentItem("DAO.dll")> _
    Public Sub NombreProcedureTest()
        Dim target As DAORegistroVentas_Accessor = New DAORegistroVentas_Accessor() ' TODO: Inicializar en un valor adecuado
        Dim percepcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim monbase As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.NombreProcedure(percepcion, monbase)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RegistroVentasxParams
    '''</summary>
    <TestMethod()> _
    Public Sub RegistroVentasxParamsTest()
        Dim target As DAORegistroVentas = New DAORegistroVentas() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechai As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fechaf As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim conPercep As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim monBase As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RegistroVentasxParams(idempresa, idtienda, fechai, fechaf, conPercep, monBase)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
