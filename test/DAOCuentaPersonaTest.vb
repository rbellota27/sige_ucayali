﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCuentaPersonaTest y se pretende que
'''contenga todas las pruebas unitarias DAOCuentaPersonaTest.
'''</summary>
<TestClass()> _
Public Class DAOCuentaPersonaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCuentaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCuentaPersonaConstructorTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaCuentaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaCuentaPersonaTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim cuentapersona As CuentaPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaCuentaPersona(cuentapersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CuentaPersonaCtaEmpresaTienda
    '''</summary>
    <TestMethod()> _
    Public Sub CuentaPersonaCtaEmpresaTiendaTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim ruc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim RazonApe As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idrol As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaPersona)
        actual = target.CuentaPersonaCtaEmpresaTienda(dni, ruc, RazonApe, tipo, pageindex, pagesize, idrol, estado, IdEmpresa, IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexCuentaPersonaxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexCuentaPersonaxIdPersonaTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexCuentaPersonaxIdPersona(idpersona, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdCuentaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdCuentaPersonaTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim idcuentapersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexIdCuentaPersona(idcuentapersona, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaCuentaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaCuentaPersonaTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim cuentapersona As CuentaPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaCuentaPersona(cuentapersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaCuentaPersonaT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaCuentaPersonaTTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim obj As CuentaPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaCuentaPersonaT(obj, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Mostrarventaxpersona
    '''</summary>
    <TestMethod()> _
    Public Sub MostrarventaxpersonaTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaPersona)
        actual = target.Mostrarventaxpersona(IdPersona, IdEmpresa, IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Mostrarventaxpersonaxaño
    '''</summary>
    <TestMethod()> _
    Public Sub MostrarventaxpersonaxañoTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaPersona)
        actual = target.Mostrarventaxpersonaxaño(IdPersona, IdEmpresa, IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllConsultaxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllConsultaxIdPersonaTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaPersona)
        actual = target.SelectAllConsultaxIdPersona(IdPersona, IdEmpresa, IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaCxCResumenDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaCxCResumenDocumentoTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CxCResumenView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CxCResumenView)
        actual = target.SelectGrillaCxCResumenDocumento(idpersona, idmoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdCtaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdCtaPersonaTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdCtaPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaPersona)
        actual = target.SelectxIdCtaPersona(IdCtaPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPersonaTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaPersona)
        actual = target.SelectxIdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxParamsTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaPersona)
        actual = target.SelectxParams(IdPersona, IdEmpresa, IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxcomprasParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxcomprasParamsTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaPersona)
        actual = target.SelectxcomprasParams(IdPersona, IdEmpresa, IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ULTIMAS_VENTASFUNCION
    '''</summary>
    <TestMethod()> _
    Public Sub ULTIMAS_VENTASFUNCIONTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaPersona)
        actual = target.ULTIMAS_VENTASFUNCION(IdPersona, IdEmpresa, IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateCuentaPersonaT
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateCuentaPersonaTTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim obj As CuentaPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateCuentaPersonaT(obj, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de fnInsTblCuentaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub fnInsTblCuentaPersonaTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim obj As CuentaPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.fnInsTblCuentaPersona(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fnUdpTblCuentaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub fnUdpTblCuentaPersonaTest()
        Dim target As DAOCuentaPersona = New DAOCuentaPersona() ' TODO: Inicializar en un valor adecuado
        Dim obj As CuentaPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.fnUdpTblCuentaPersona(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
