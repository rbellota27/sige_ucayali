﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODireccionTest y se pretende que
'''contenga todas las pruebas unitarias DAODireccionTest.
'''</summary>
<TestClass()> _
Public Class DAODireccionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODireccion
    '''</summary>
    <TestMethod()> _
    Public Sub DAODireccionConstructorTest()
        Dim target As DAODireccion = New DAODireccion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaDireccionT
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaDireccionTTest()
        Dim target As DAODireccion = New DAODireccion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Ldir As List(Of Direccion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.ActualizaDireccionT(cn, Ldir, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDireccion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDireccionTest()
        Dim target As DAODireccion = New DAODireccion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Ldir As List(Of Direccion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaDireccion(cn, Ldir, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPersonaTest()
        Dim target As DAODireccion = New DAODireccion() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Direccion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Direccion
        actual = target.SelectxIdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Selectx_IdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub Selectx_IdPersonaTest()
        Dim target As DAODireccion = New DAODireccion() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Direccion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Direccion)
        actual = target.Selectx_IdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
