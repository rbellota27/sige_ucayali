﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOUsuarioTiendaAreaTest y se pretende que
'''contenga todas las pruebas unitarias DAOUsuarioTiendaAreaTest.
'''</summary>
<TestClass()> _
Public Class DAOUsuarioTiendaAreaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOUsuarioTiendaArea
    '''</summary>
    <TestMethod()> _
    Public Sub DAOUsuarioTiendaAreaConstructorTest()
        Dim target As DAOUsuarioTiendaArea = New DAOUsuarioTiendaArea()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdPersonaTest()
        Dim target As DAOUsuarioTiendaArea = New DAOUsuarioTiendaArea() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.DeletexIdPersona(cn, tr, IdPersona)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Insert
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTest()
        Dim target As DAOUsuarioTiendaArea = New DAOUsuarioTiendaArea() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objUTA As UsuarioTiendaArea = Nothing ' TODO: Inicializar en un valor adecuado
        target.Insert(cn, tr, objUTA)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoDistinctTiendaxIdPersonaxIdPerfil
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoDistinctTiendaxIdPersonaxIdPerfilTest()
        Dim target As DAOUsuarioTiendaArea = New DAOUsuarioTiendaArea() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of UsuarioTiendaArea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of UsuarioTiendaArea)
        actual = target.SelectActivoDistinctTiendaxIdPersonaxIdPerfil(IdPersona, IdPerfil)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxIdPersonaTest()
        Dim target As DAOUsuarioTiendaArea = New DAOUsuarioTiendaArea() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of UsuarioTiendaArea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of UsuarioTiendaArea)
        actual = target.SelectAllxIdPersona(idpersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Update
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTest()
        Dim target As DAOUsuarioTiendaArea = New DAOUsuarioTiendaArea() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objUTA As UsuarioTiendaArea = Nothing ' TODO: Inicializar en un valor adecuado
        target.Update(cn, tr, objUTA)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub
End Class
