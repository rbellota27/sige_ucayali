﻿Imports System.Data

Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoCompPercepcionTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoCompPercepcionTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoCompPercepcionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoCompPercepcion
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoCompPercepcionConstructorTest()
        Dim target As DAODocumentoCompPercepcion = New DAODocumentoCompPercepcion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCompPercepcionAnticipo
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCompPercepcionAnticipoTest()
        Dim target As DAODocumentoCompPercepcion = New DAODocumentoCompPercepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PorcentPercepcion As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoCompPercepcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoCompPercepcion)
        actual = target.DocumentoCompPercepcionAnticipo(IdDocumento, IdMonedaDestino, PorcentPercepcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCompPercepcionSelectCab
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCompPercepcionSelectCabTest()
        Dim target As DAODocumentoCompPercepcion = New DAODocumentoCompPercepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocumentoCompPercepcion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocumentoCompPercepcion
        actual = target.DocumentoCompPercepcionSelectCab(IdSerie, Codigo, IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCompPercepcionSelectDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCompPercepcionSelectDetalleTest()
        Dim target As DAODocumentoCompPercepcion = New DAODocumentoCompPercepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoCompPercepcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoCompPercepcion)
        actual = target.DocumentoCompPercepcionSelectDetalle(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCompPercepcionSelectDocReferencia_Find
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCompPercepcionSelectDocReferencia_FindTest()
        Dim target As DAODocumentoCompPercepcion = New DAODocumentoCompPercepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoCompPercepcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoCompPercepcion)
        actual = target.DocumentoCompPercepcionSelectDocReferencia_Find(IdPersona, IdMonedaDestino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCompPercepcion_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCompPercepcion_DeshacerMovTest()
        Dim target As DAODocumentoCompPercepcion = New DAODocumentoCompPercepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeletePagoCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovCuenta As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleDocR As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteRelacionDoc As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim UpdateEstadoDoc_Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim ValAbonos As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim ValActivoToDeshacer As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoCompPercepcion_DeshacerMov(IdDocumento, DeletePagoCaja, DeleteMovCaja, DeleteMovCuenta, DeleteDetalleDocR, DeleteRelacionDoc, UpdateEstadoDoc_Anular, ValAbonos, ValActivoToDeshacer, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCompPercepcion_GenerarxIdDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCompPercepcion_GenerarxIdDocumentoRefTest()
        Dim target As DAODocumentoCompPercepcion = New DAODocumentoCompPercepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocumentoCompPercepcion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocumentoCompPercepcion
        actual = target.DocumentoCompPercepcion_GenerarxIdDocumentoRef(IdDocumentoRef, IdMonedaDestino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCompPercepcion_NC
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCompPercepcion_NCTest()
        Dim target As DAODocumentoCompPercepcion = New DAODocumentoCompPercepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PorcentPercepcion As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoCompPercepcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoCompPercepcion)
        actual = target.DocumentoCompPercepcion_NC(IdDocumento, IdMonedaDestino, PorcentPercepcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoPercepcionPrint
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoPercepcionPrintTest()
        Dim target As DAODocumentoCompPercepcion = New DAODocumentoCompPercepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.DocumentoPercepcionPrint(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
