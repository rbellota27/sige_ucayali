﻿Imports System.Collections.Generic

Imports System

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOKitTest y se pretende que
'''contenga todas las pruebas unitarias DAOKitTest.
'''</summary>
<TestClass()> _
Public Class DAOKitTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOKit
    '''</summary>
    <TestMethod()> _
    Public Sub DAOKitConstructorTest()
        Dim target As DAOKit = New DAOKit()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Kit_ActualizarxIdKitxIdComponente
    '''</summary>
    <TestMethod()> _
    Public Sub Kit_ActualizarxIdKitxIdComponenteTest()
        Dim target As DAOKit = New DAOKit() ' TODO: Inicializar en un valor adecuado
        Dim objKit As Kit = Nothing ' TODO: Inicializar en un valor adecuado
        target.Kit_ActualizarxIdKitxIdComponente(objKit)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Kit_DeletexIdKitxIdComponente
    '''</summary>
    <TestMethod()> _
    Public Sub Kit_DeletexIdKitxIdComponenteTest()
        Dim target As DAOKit = New DAOKit() ' TODO: Inicializar en un valor adecuado
        Dim IdKit As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdComponente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Kit_DeletexIdKitxIdComponente(IdKit, IdComponente, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Kit_Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub Kit_RegistrarTest()
        Dim target As DAOKit = New DAOKit() ' TODO: Inicializar en un valor adecuado
        Dim objKit As Kit = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.Kit_Registrar(objKit, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectComponenteDetallexParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectComponenteDetallexParamsTest()
        Dim target As DAOKit = New DAOKit() ' TODO: Inicializar en un valor adecuado
        Dim IdKit As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedidaKit As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CantidadKit As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaKit As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Kit) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Kit)
        actual = target.SelectComponenteDetallexParams(IdKit, IdUnidadMedidaKit, CantidadKit, IdMonedaKit, IdTienda, IdTipoPV)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectComponentexIdKit
    '''</summary>
    <TestMethod()> _
    Public Sub SelectComponentexIdKitTest()
        Dim target As DAOKit = New DAOKit() ' TODO: Inicializar en un valor adecuado
        Dim IdKit As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Kit) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Kit)
        actual = target.SelectComponentexIdKit(IdKit)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectComponentexIdKitDOC_OC
    '''</summary>
    <TestMethod()> _
    Public Sub SelectComponentexIdKitDOC_OCTest()
        Dim target As DAOKit = New DAOKit() ' TODO: Inicializar en un valor adecuado
        Dim IdKit As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Kit) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Kit)
        actual = target.SelectComponentexIdKitDOC_OC(IdKit, IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectComponentexIdKit_OC
    '''</summary>
    <TestMethod()> _
    Public Sub SelectComponentexIdKit_OCTest()
        Dim target As DAOKit = New DAOKit() ' TODO: Inicializar en un valor adecuado
        Dim IdKit As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Kit) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Kit)
        actual = target.SelectComponentexIdKit_OC(IdKit, IdMoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
