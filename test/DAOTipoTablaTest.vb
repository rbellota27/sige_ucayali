﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTipoTablaTest y se pretende que
'''contenga todas las pruebas unitarias DAOTipoTablaTest.
'''</summary>
<TestClass()> _
Public Class DAOTipoTablaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTipoTabla
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTipoTablaConstructorTest()
        Dim target As DAOTipoTabla = New DAOTipoTabla()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizarTipoTabla
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizarTipoTablaTest()
        Dim target As DAOTipoTabla = New DAOTipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim obj As TipoTabla = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizarTipoTabla(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaTipoTabla
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTipoTablaTest()
        Dim target As DAOTipoTabla = New DAOTipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim obj As TipoTabla = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaTipoTabla(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de LongitudTablasGeneral
    '''</summary>
    <TestMethod()> _
    Public Sub LongitudTablasGeneralTest()
        Dim target As DAOTipoTabla = New DAOTipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim idtipotabla As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoTabla = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoTabla
        actual = target.LongitudTablasGeneral(idtipotabla, idlinea, idsublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllTipoTablaxEstado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTipoTablaxEstadoTest()
        Dim target As DAOTipoTabla = New DAOTipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim Estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoTabla) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoTabla)
        actual = target.SelectAllTipoTablaxEstado(Estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllTipoTablaxLineaSub
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTipoTablaxLineaSubTest()
        Dim target As DAOTipoTabla = New DAOTipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim Estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Linea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Sublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoTabla) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoTabla)
        actual = target.SelectAllTipoTablaxLineaSub(Estado, Linea, Sublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectTipoTablaAllActivoxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectTipoTablaAllActivoxIdTest()
        Dim target As DAOTipoTabla = New DAOTipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim idtipotabla As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoTabla = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoTabla
        actual = target.SelectTipoTablaAllActivoxId(idtipotabla)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de TipoTablaSelectAllxEstado_Paginado
    '''</summary>
    <TestMethod()> _
    Public Sub TipoTablaSelectAllxEstado_PaginadoTest()
        Dim target As DAOTipoTabla = New DAOTipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoTabla) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoTabla)
        actual = target.TipoTablaSelectAllxEstado_Paginado(Descripcion, Estado, PageIndex, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidadNombres
    '''</summary>
    <TestMethod()> _
    Public Sub ValidadNombresTest()
        Dim target As DAOTipoTabla = New DAOTipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim nomTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim columna As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim columna2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoTabla = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoTabla
        actual = target.ValidadNombres(nomTabla, columna, valor, columna2, valor2)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidadNombres
    '''</summary>
    <TestMethod()> _
    Public Sub ValidadNombresTest1()
        Dim target As DAOTipoTabla = New DAOTipoTabla() ' TODO: Inicializar en un valor adecuado
        Dim nomTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim columna As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoTabla = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoTabla
        actual = target.ValidadNombres(nomTabla, columna, valor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
