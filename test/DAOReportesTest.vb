﻿Imports System

Imports System.Data

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOReportesTest y se pretende que
'''contenga todas las pruebas unitarias DAOReportesTest.
'''</summary>
<TestClass()> _
Public Class DAOReportesTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOReportes
    '''</summary>
    <TestMethod()> _
    Public Sub DAOReportesConstructorTest()
        Dim target As DAOReportes = New DAOReportes()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetBoleta
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetBoletaTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetBoleta(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetCatalogo
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetCatalogoTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetCatalogo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetCatalogoProducto_UM
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetCatalogoProducto_UMTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim UMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetCatalogoProducto_UM(IdLinea, IdSubLinea, UMedida)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetChequeEgreso
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetChequeEgresoTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetChequeEgreso(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetChequeImpresion
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetChequeImpresionTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetChequeImpresion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetCotizacionTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetCotizacion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetDocCancelBancos
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetDocCancelBancosTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetDocCancelBancos(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetDocCancelCaja
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetDocCancelCajaTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetDocCancelCaja(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetDocCancelacion
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetDocCancelacionTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim flag_banco As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetDocCancelacion(IdDocumento, flag_banco)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetDocCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetDocCotizacionTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetDocCotizacion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetDocEgresos
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetDocEgresosTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim flag_banco As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetDocEgresos(IdDocumento, flag_banco)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetDocEgresos
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetDocEgresosTest1()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetDocEgresos(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetDocGuiaRecepcion
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetDocGuiaRecepcionTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetDocGuiaRecepcion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetDocGuiaRemision
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetDocGuiaRemisionTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetDocGuiaRemision(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetDocInventarioInicial
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetDocInventarioInicialTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetDocInventarioInicial(idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetFactura
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetFacturaTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetFactura(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetInvInicial
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetInvInicialTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetInvInicial(idempresa, idalmacen, idlinea, idsublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetInvNOValorizado
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetInvNOValorizadoTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetInvNOValorizado(IdEmpresa, IdAlmacen, IdLinea, IdSubLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetMovCajaCantDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetMovCajaCantDocumentoTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idcaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmediopago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipomov As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaI As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fechaF As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetMovCajaCantDocumento(idempresa, idtienda, idcaja, idmediopago, idtipomov, fechaI, fechaF)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetMovCajaxParams
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetMovCajaxParamsTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idcaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmediopago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipomov As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaI As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fechaF As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetMovCajaxParams(idempresa, idtienda, idcaja, idmediopago, idtipomov, fechaI, fechaF)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetMovCajaxParams2
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetMovCajaxParams2Test()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipodocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idcaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmediopago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipomov As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaI As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fechaF As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetMovCajaxParams2(idempresa, idtienda, idtipodocumento, idcaja, idmediopago, idtipomov, fechaI, fechaF)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetOrdenDespacho
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetOrdenDespachoTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetOrdenDespacho(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetPedidoCliente
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetPedidoClienteTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetPedidoCliente(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetReciboIngreso
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetReciboIngresoTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetReciboIngreso(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getReporteComprobantePercepcion
    '''</summary>
    <TestMethod()> _
    Public Sub getReporteComprobantePercepcionTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getReporteComprobantePercepcion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getReporteDocCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub getReporteDocCotizacionTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tipoimpresion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getReporteDocCotizacion(IdDocumento, tipoimpresion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getResumenCXCResumen
    '''</summary>
    <TestMethod()> _
    Public Sub getResumenCXCResumenTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getResumenCXCResumen(IdEmpresa, idtienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getResumendiarioVentas
    '''</summary>
    <TestMethod()> _
    Public Sub getResumendiarioVentasTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaI As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechaF As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getResumendiarioVentas(idempresa, idtienda, fechaI, fechaF)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getResumidoGlobal
    '''</summary>
    <TestMethod()> _
    Public Sub getResumidoGlobalTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Fecha As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getResumidoGlobal(IdEmpresa, IdTienda, Fecha)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de rptListarProductos
    '''</summary>
    <TestMethod()> _
    Public Sub rptListarProductosTest()
        Dim target As DAOReportes = New DAOReportes() ' TODO: Inicializar en un valor adecuado
        Dim linea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim sublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim producto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.rptListarProductos(linea, sublinea, producto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
