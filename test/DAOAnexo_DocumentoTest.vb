﻿Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOAnexo_DocumentoTest y se pretende que
'''contenga todas las pruebas unitarias DAOAnexo_DocumentoTest.
'''</summary>
<TestClass()> _
Public Class DAOAnexo_DocumentoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOAnexo_Documento
    '''</summary>
    <TestMethod()> _
    Public Sub DAOAnexo_DocumentoConstructorTest()
        Dim target As DAOAnexo_Documento = New DAOAnexo_Documento()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaAnexoDocT
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaAnexoDocTTest()
        Dim target As DAOAnexo_Documento = New DAOAnexo_Documento() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim vIdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim objAnexoDoc As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaAnexoDocT(cn, vIdDocumento, objAnexoDoc, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Anexo_DocumentoSelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub Anexo_DocumentoSelectxIdDocumentoTest()
        Dim target As DAOAnexo_Documento = New DAOAnexo_Documento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Anexo_Documento
        actual = target.Anexo_DocumentoSelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdDocumentoTest()
        Dim target As DAOAnexo_Documento = New DAOAnexo_Documento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdDocumento(IdDocumento, cn, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de FacturacionDeleteMaestro
    '''</summary>
    <TestMethod()> _
    Public Sub FacturacionDeleteMaestroTest()
        Dim target As DAOAnexo_Documento = New DAOAnexo_Documento() ' TODO: Inicializar en un valor adecuado
        Dim IdCotizacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim sqlcn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqltr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.FacturacionDeleteMaestro(IdCotizacion, sqlcn, sqltr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de GetVectorParametros
    '''</summary>
    <TestMethod()> _
    Public Sub GetVectorParametrosTest()
        Dim target As DAOAnexo_Documento = New DAOAnexo_Documento() ' TODO: Inicializar en un valor adecuado
        Dim x As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim op As DAOMantenedor.modo_query = New DAOMantenedor.modo_query() ' TODO: Inicializar en un valor adecuado
        Dim useAlias_Random As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual() As SqlParameter
        actual = target.GetVectorParametros(x, op, useAlias_Random)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GetVectorParametros2
    '''</summary>
    <TestMethod()> _
    Public Sub GetVectorParametros2Test()
        Dim target As DAOAnexo_Documento = New DAOAnexo_Documento() ' TODO: Inicializar en un valor adecuado
        Dim x As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim op As DAOMantenedor.modo_query = New DAOMantenedor.modo_query() ' TODO: Inicializar en un valor adecuado
        Dim useAlias_Random As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual() As SqlParameter
        actual = target.GetVectorParametros2(x, op, useAlias_Random)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertAnexoDocT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertAnexoDocTTest()
        Dim target As DAOAnexo_Documento = New DAOAnexo_Documento() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim vIdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim objAnexoDoc As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertAnexoDocT(cn, vIdDocumento, objAnexoDoc, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertAnexo_Documento
    '''</summary>
    <TestMethod()> _
    Public Sub InsertAnexo_DocumentoTest()
        Dim target As DAOAnexo_Documento = New DAOAnexo_Documento() ' TODO: Inicializar en un valor adecuado
        Dim objEnt As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim useAlias_Random As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertAnexo_Documento(objEnt, cn, tr, useAlias_Random)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertAnexo_Documento2
    '''</summary>
    <TestMethod()> _
    Public Sub InsertAnexo_Documento2Test()
        Dim target As DAOAnexo_Documento = New DAOAnexo_Documento() ' TODO: Inicializar en un valor adecuado
        Dim objEnt As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim useAlias_Random As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertAnexo_Documento2(objEnt, cn, tr, useAlias_Random)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoTest()
        Dim target As DAOAnexo_Documento = New DAOAnexo_Documento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Anexo_Documento
        actual = target.SelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
