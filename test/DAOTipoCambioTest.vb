﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTipoCambioTest y se pretende que
'''contenga todas las pruebas unitarias DAOTipoCambioTest.
'''</summary>
<TestClass()> _
Public Class DAOTipoCambioTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTipoCambio
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTipoCambioConstructorTest()
        Dim target As DAOTipoCambio = New DAOTipoCambio()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaTipoCambio
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaTipoCambioTest()
        Dim target As DAOTipoCambio = New DAOTipoCambio() ' TODO: Inicializar en un valor adecuado
        Dim tipocambio As TipoCambio = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaTipoCambio(tipocambio)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaTipoCambio
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTipoCambioTest()
        Dim target As DAOTipoCambio = New DAOTipoCambio() ' TODO: Inicializar en un valor adecuado
        Dim tipocambio As TipoCambio = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaTipoCambio(tipocambio)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectComercial
    '''</summary>
    <TestMethod()> _
    Public Sub SelectComercialTest()
        Dim target As DAOTipoCambio = New DAOTipoCambio() ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoCambio = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoCambio
        actual = target.SelectComercial(IdMoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectOficial
    '''</summary>
    <TestMethod()> _
    Public Sub SelectOficialTest()
        Dim target As DAOTipoCambio = New DAOTipoCambio() ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoCambio = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoCambio
        actual = target.SelectOficial(IdMoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectTipoCambioActualxIdMoneda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectTipoCambioActualxIdMonedaTest()
        Dim target As DAOTipoCambio = New DAOTipoCambio() ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoCambio = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoCambio
        actual = target.SelectTipoCambioActualxIdMoneda(IdMoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectTipoCambioBuscarFecha
    '''</summary>
    <TestMethod()> _
    Public Sub SelectTipoCambioBuscarFechaTest()
        Dim target As DAOTipoCambio = New DAOTipoCambio() ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tc_Fecha As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoCambio) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoCambio)
        actual = target.SelectTipoCambioBuscarFecha(IdMoneda, tc_Fecha)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectVigentexIdMoneda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectVigentexIdMonedaTest()
        Dim target As DAOTipoCambio = New DAOTipoCambio() ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoCambio = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoCambio
        actual = target.SelectVigentexIdMoneda(IdMoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdMoneda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdMonedaTest()
        Dim target As DAOTipoCambio = New DAOTipoCambio() ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoCambio) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoCambio)
        actual = target.SelectxIdMoneda(IdMoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
