﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOProveedorTest y se pretende que
'''contenga todas las pruebas unitarias DAOProveedorTest.
'''</summary>
<TestClass()> _
Public Class DAOProveedorTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub DAOProveedorConstructorTest()
        Dim target As DAOProveedor = New DAOProveedor()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ListarProveedorOrdenCompra
    '''</summary>
    <TestMethod()> _
    Public Sub ListarProveedorOrdenCompraTest()
        Dim target As DAOProveedor = New DAOProveedor() ' TODO: Inicializar en un valor adecuado
        Dim idproveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As PersonaView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As PersonaView
        actual = target.ListarProveedorOrdenCompra(idproveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTest()
        Dim target As DAOProveedor = New DAOProveedor() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboImportado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboImportadoTest()
        Dim target As DAOProveedor = New DAOProveedor() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectCboImportado
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaProveedorTest()
        Dim target As DAOProveedor = New DAOProveedor() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaProveedor
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaProveedorxDNI
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaProveedorxDNITest()
        Dim target As DAOProveedor = New DAOProveedor() ' TODO: Inicializar en un valor adecuado
        Dim numero As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaProveedorxDNI(numero)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaProveedorxEstado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaProveedorxEstadoTest()
        Dim target As DAOProveedor = New DAOProveedor() ' TODO: Inicializar en un valor adecuado
        Dim estado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaProveedorxEstado(estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaProveedorxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaProveedorxIdPersonaTest()
        Dim target As DAOProveedor = New DAOProveedor() ' TODO: Inicializar en un valor adecuado
        Dim codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaProveedorxIdPersona(codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaProveedorxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaProveedorxNombreTest()
        Dim target As DAOProveedor = New DAOProveedor() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaProveedorxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaProveedorxRUC
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaProveedorxRUCTest()
        Dim target As DAOProveedor = New DAOProveedor() ' TODO: Inicializar en un valor adecuado
        Dim numero As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaProveedorxRUC(numero)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaProveedorxRazonSocial
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaProveedorxRazonSocialTest()
        Dim target As DAOProveedor = New DAOProveedor() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaProveedorxRazonSocial(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
