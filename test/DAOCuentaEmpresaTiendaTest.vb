﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCuentaEmpresaTiendaTest y se pretende que
'''contenga todas las pruebas unitarias DAOCuentaEmpresaTiendaTest.
'''</summary>
<TestClass()> _
Public Class DAOCuentaEmpresaTiendaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCuentaEmpresaTienda
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCuentaEmpresaTiendaConstructorTest()
        Dim target As DAOCuentaEmpresaTienda = New DAOCuentaEmpresaTienda()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de InsertarCuentaEmpresaTienda
    '''</summary>
    <TestMethod()> _
    Public Sub InsertarCuentaEmpresaTiendaTest()
        Dim target As DAOCuentaEmpresaTienda = New DAOCuentaEmpresaTienda() ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of CuentaEmpresaTienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idsupervisor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertarCuentaEmpresaTienda(lista, idsupervisor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCuentaEmpresaTienda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCuentaEmpresaTiendaTest()
        Dim target As DAOCuentaEmpresaTienda = New DAOCuentaEmpresaTienda() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaEmpresaTienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaEmpresaTienda)
        actual = target.SelectCuentaEmpresaTienda(IdEmpresa, IdTienda, Idmoneda, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdEmpresaxIdTiendaxIdMoneda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdEmpresaxIdTiendaxIdMonedaTest()
        Dim target As DAOCuentaEmpresaTienda = New DAOCuentaEmpresaTienda() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As CuentaEmpresaTienda = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As CuentaEmpresaTienda
        actual = target.SelectxIdEmpresaxIdTiendaxIdMoneda(IdEmpresa, IdTienda, IdMoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateCuentaEmpresaTienda
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateCuentaEmpresaTiendaTest()
        Dim target As DAOCuentaEmpresaTienda = New DAOCuentaEmpresaTienda() ' TODO: Inicializar en un valor adecuado
        Dim obj As CuentaEmpresaTienda = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idsupervisor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim sqlcn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqltr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateCuentaEmpresaTienda(obj, idsupervisor, sqlcn, sqltr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub
End Class
