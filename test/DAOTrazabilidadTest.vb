﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data

Imports System.Data.SqlClient

Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTrazabilidadTest y se pretende que
'''contenga todas las pruebas unitarias DAOTrazabilidadTest.
'''</summary>
<TestClass()> _
Public Class DAOTrazabilidadTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTrazabilidad
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTrazabilidadConstructorTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DAO_Insert_Kardex_detallado
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_Insert_Kardex_detalladoTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad() ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSector As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cantidad As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim idTono As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DAO_Insert_Kardex_detallado(idProducto, idAlmacen, idSector, cantidad, idTono)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_MANTENIMIENTO_TONO
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_MANTENIMIENTO_TONOTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad() ' TODO: Inicializar en un valor adecuado
        Dim nom_tono As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim descTono As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSector As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DAO_MANTENIMIENTO_TONO(nom_tono, descTono, idProducto, idAlmacen, idSector, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_Update_Stock
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_Update_StockTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad() ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSector_antiguo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSector_nuevo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cantidad As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim idTono As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DAO_Update_Stock(idProducto, idAlmacen, idSector_antiguo, idSector_nuevo, cantidad, idTono)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_insertTono
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_insertTonoTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad() ' TODO: Inicializar en un valor adecuado
        Dim nom_tono As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim descTono As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DAO_insertTono(nom_tono, descTono, idProducto, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_listarDocReferencia
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_listarDocReferenciaTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad() ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechaFin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.DAO_listarDocReferencia(serie, codigo, idTienda, fechaInicio, fechaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_listarProductos
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_listarProductosTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.DAO_listarProductos(idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_obtenerIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_obtenerIdDocumentoTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad() ' TODO: Inicializar en un valor adecuado
        Dim doc_serie As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim doc_codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.DAO_obtenerIdDocumento(doc_serie, doc_codigo, idTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_trazabilidad_GuardarDetalleSaldoKardex
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_trazabilidad_GuardarDetalleSaldoKardexTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad() ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSector As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cantidadSaldo As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTono As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.DAO_trazabilidad_GuardarDetalleSaldoKardex(idProducto, idSector, cantidadSaldo, idDocumento, idTono)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_trazabilidad_GuardarKardex
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_trazabilidad_GuardarKardexTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad() ' TODO: Inicializar en un valor adecuado
        Dim idDocumentoReferencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim usuRegistro As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim obj As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.DAO_trazabilidad_GuardarKardex(idDocumentoReferencia, usuRegistro, obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_trazabilidad_GuardarSaldoKardex
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_trazabilidad_GuardarSaldoKardexTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad() ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idUnidadMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idAfecto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim usuRegistro As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.DAO_trazabilidad_GuardarSaldoKardex(idProducto, idUnidadMedida, idAfecto, usuRegistro, idDocumento)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_trazabilidad_Select_Kardex
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_trazabilidad_Select_KardexTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad() ' TODO: Inicializar en un valor adecuado
        Dim idSector As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim prod_nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim prod_codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of be_productoStock) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of be_productoStock)
        actual = target.DAO_trazabilidad_Select_Kardex(idSector, idAlmacen, prod_nombre, prod_codigo, idTipoExistencia, idLinea, idSublinea, cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_trazabilidad_Ver_Registro
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_trazabilidad_Ver_RegistroTest()
        Dim target As DAOTrazabilidad = New DAOTrazabilidad() ' TODO: Inicializar en un valor adecuado
        Dim flag As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim serie As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTipodocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.DAO_trazabilidad_Ver_Registro(flag, serie, codigo, idempresa, idTienda, idTipodocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
