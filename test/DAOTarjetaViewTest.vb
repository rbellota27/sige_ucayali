﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTarjetaViewTest y se pretende que
'''contenga todas las pruebas unitarias DAOTarjetaViewTest.
'''</summary>
<TestClass()> _
Public Class DAOTarjetaViewTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTarjetaView
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTarjetaViewConstructorTest()
        Dim target As DAOTarjetaView = New DAOTarjetaView()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOTarjetaView = New DAOTarjetaView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TarjetaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TarjetaView)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOTarjetaView = New DAOTarjetaView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TarjetaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TarjetaView)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOTarjetaView = New DAOTarjetaView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TarjetaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TarjetaView)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdPost
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdPostTest()
        Dim target As DAOTarjetaView = New DAOTarjetaView() ' TODO: Inicializar en un valor adecuado
        Dim IdPost As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tarjeta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tarjeta)
        actual = target.SelectCboxIdPost(IdPost)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxTipoTarjetaCaja
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxTipoTarjetaCajaTest()
        Dim target As DAOTarjetaView = New DAOTarjetaView() ' TODO: Inicializar en un valor adecuado
        Dim IdTarjeta As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tarjeta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tarjeta)
        actual = target.SelectCboxTipoTarjetaCaja(IdTarjeta, IdCaja)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdTipoTarjeta
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTipoTarjetaTest()
        Dim target As DAOTarjetaView = New DAOTarjetaView() ' TODO: Inicializar en un valor adecuado
        Dim Id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TarjetaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TarjetaView)
        actual = target.SelectxIdTipoTarjeta(Id)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxParamsTest()
        Dim target As DAOTarjetaView = New DAOTarjetaView() ' TODO: Inicializar en un valor adecuado
        Dim x As TarjetaView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TarjetaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TarjetaView)
        actual = target.SelectxParams(x)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
