﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports System.Data

Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMovCuentaPorPagarTest y se pretende que
'''contenga todas las pruebas unitarias DAOMovCuentaPorPagarTest.
'''</summary>
<TestClass()> _
Public Class DAOMovCuentaPorPagarTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMovCuentaPorPagar
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMovCuentaPorPagarConstructorTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de CR_MovCuenta_CXP_DT
    '''</summary>
    <TestMethod()> _
    Public Sub CR_MovCuenta_CXP_DTTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim OpcionDeudas As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.CR_MovCuenta_CXP_DT(IdEmpresa, IdPersona, FechaInicio, FechaFin, OpcionDeudas)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CR_MovCuenta_CXP_Lista
    '''</summary>
    <TestMethod()> _
    Public Sub CR_MovCuenta_CXP_ListaTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim OpcionDeudas As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovCuentaPorPagar) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovCuentaPorPagar)
        actual = target.CR_MovCuenta_CXP_Lista(IdEmpresa, IdPersona, FechaInicio, FechaFin, OpcionDeudas)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GetVectorParametros
    '''</summary>
    <TestMethod()> _
    Public Sub GetVectorParametrosTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim x As MovCuentaPorPagar = Nothing ' TODO: Inicializar en un valor adecuado
        Dim op As DAOMantenedor.modo_query = New DAOMantenedor.modo_query() ' TODO: Inicializar en un valor adecuado
        Dim expected() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual() As SqlParameter
        actual = target.GetVectorParametros(x, op)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim x As MovCuentaPorPagar = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertT(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertT_GetID
    '''</summary>
    <TestMethod()> _
    Public Sub InsertT_GetIDTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim x As MovCuentaPorPagar = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertT_GetID(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de MovCuentaPorPagar_SelectxParams_DataTable
    '''</summary>
    <TestMethod()> _
    Public Sub MovCuentaPorPagar_SelectxParams_DataTableTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaVctoInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaVctoFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Opcion_Programacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Opcion_Deuda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Solicitud As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.MovCuentaPorPagar_SelectxParams_DataTable(IdEmpresa, IdTienda, IdMoneda, IdPersona, FechaVctoInicio, FechaVctoFin, Opcion_Programacion, Opcion_Deuda, PageIndex, PageSize, IdTipoDocumento, Solicitud)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de MovCuentaPorPagar_SelectxParams_DataTable2
    '''</summary>
    <TestMethod()> _
    Public Sub MovCuentaPorPagar_SelectxParams_DataTable2Test()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaVctoInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaVctoFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Opcion_Programacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Opcion_Deuda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Solicitud As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.MovCuentaPorPagar_SelectxParams_DataTable2(IdEmpresa, IdTienda, IdMoneda, IdPersona, FechaVctoInicio, FechaVctoFin, Opcion_Programacion, Opcion_Deuda, PageIndex, PageSize, IdTipoDocumento, Solicitud)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de MovCuentaPorPagar_SelectxParams_DataTableReport
    '''</summary>
    <TestMethod()> _
    Public Sub MovCuentaPorPagar_SelectxParams_DataTableReportTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaVctoInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaVctoFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Opcion_Programacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Opcion_Deuda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of be_Requerimiento_x_pagar) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of be_Requerimiento_x_pagar)
        'actual = target.MovCuentaPorPagar_SelectxParams_DataTableReport(IdEmpresa, IdTienda, IdMoneda, IdPersona, FechaVctoInicio, FechaVctoFin, serie, codigo, Opcion_Programacion, Opcion_Deuda, PageIndex, PageSize, IdTipoDocumento, cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub RegistrarTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim obj As MovCuentaPorPagar = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlcn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqltr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Registrar(obj, sqlcn, sqltr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReporteCancelacionBancos
    '''</summary>
    <TestMethod()> _
    Public Sub ReporteCancelacionBancosTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.ReporteCancelacionBancos(FechaInicio, FechaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReporteProgramaciones
    '''</summary>
    <TestMethod()> _
    Public Sub ReporteProgramacionesTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProgramacionReporte) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProgramacionReporte)
        actual = target.ReporteProgramaciones(FechaInicio, FechaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAbonosxIdMovCtaPP
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAbonosxIdMovCtaPPTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdMovCtaPP As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento_MovCuentaPorPagar) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento_MovCuentaPorPagar)
        actual = target.SelectAbonosxIdMovCtaPP(IdMovCtaPP)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDatosCancelacion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDatosCancelacionTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovCuentaPorPagar) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovCuentaPorPagar)
        actual = target.SelectDatosCancelacion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDeudaxIdProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDeudaxIdProveedorTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovCuentaPorPagar) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovCuentaPorPagar)
        actual = target.SelectDeudaxIdProveedor(IdProveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDeudaxIdProveedorxIdMoneda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDeudaxIdProveedorxIdMonedaTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento_MovCuentaPorPagar) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento_MovCuentaPorPagar)
        actual = target.SelectDeudaxIdProveedorxIdMoneda(IdProveedor, IdMonedaDestino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancel
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocCancel As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento_MovCuentaPorPagar) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento_MovCuentaPorPagar)
        actual = target.SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancel(IdProveedor, IdMonedaDestino, IdTipoDocCancel)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelRC
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelRCTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocCancel As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento_MovCuentaPorPagar) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento_MovCuentaPorPagar)
        actual = target.SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelRC(IdProveedor, IdMonedaDestino, IdTipoDocCancel, idTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelTwo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelTwoTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocCancel As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento_MovCuentaPorPagar) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento_MovCuentaPorPagar)
        actual = target.SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelTwo(IdProveedor, IdMonedaDestino, IdTipoDocCancel, fecha)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocumentosCancelacionBancosxSustentar
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocumentosCancelacionBancosxSustentarTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento_MovCuentaPorPagar) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento_MovCuentaPorPagar)
        actual = target.SelectDocumentosCancelacionBancosxSustentar(IdProveedor, FechaInicio, FechaFin, IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectMovCuentaporPagarxIddocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectMovCuentaporPagarxIddocumentoTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As MovCuentaPorPagar = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As MovCuentaPorPagar
        actual = target.SelectMovCuentaporPagarxIddocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectProgramacionPagoRQxFechas
    '''</summary>
    <TestMethod()> _
    Public Sub SelectProgramacionPagoRQxFechasTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim FechaVctoInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaVctoFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.SelectProgramacionPagoRQxFechas(FechaVctoInicio, FechaVctoFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovCuentaPorPagar) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovCuentaPorPagar)
        actual = target.SelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateSaldoxIdMovCuenta
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateSaldoxIdMovCuentaTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdMovCuentaCxP As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim saldo As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateSaldoxIdMovCuenta(IdMovCuentaCxP, saldo, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateT
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim x As MovCuentaPorPagar = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateT(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de _MovCuentaPorPagarRQCheckes
    '''</summary>
    <TestMethod()> _
    Public Sub _MovCuentaPorPagarRQCheckesTest()
        Dim target As DAOMovCuentaPorPagar = New DAOMovCuentaPorPagar() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaVctoInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaVctoFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Opcion_Programacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Opcion_Deuda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target._MovCuentaPorPagarRQCheckes(IdEmpresa, IdTienda, IdMoneda, IdPersona, FechaVctoInicio, FechaVctoFin, serie, codigo, Opcion_Programacion, Opcion_Deuda, PageIndex, PageSize, IdTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
