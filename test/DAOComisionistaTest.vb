﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOComisionistaTest y se pretende que
'''contenga todas las pruebas unitarias DAOComisionistaTest.
'''</summary>
<TestClass()> _
Public Class DAOComisionistaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOComisionista
    '''</summary>
    <TestMethod()> _
    Public Sub DAOComisionistaConstructorTest()
        Dim target As DAOComisionista = New DAOComisionista()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Comisionista_Insert
    '''</summary>
    <TestMethod()> _
    Public Sub Comisionista_InsertTest()
        Dim target As DAOComisionista = New DAOComisionista() ' TODO: Inicializar en un valor adecuado
        Dim objComisionista As Comisionista = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Comisionista_Insert(objComisionista, cnx, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Comisionista_Insert_por_grupo
    '''</summary>
    <TestMethod()> _
    Public Sub Comisionista_Insert_por_grupoTest()
        Dim target As DAOComisionista = New DAOComisionista() ' TODO: Inicializar en un valor adecuado
        Dim objComisionista As Comisionista = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Comisionista_Insert_por_grupo(objComisionista, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Comisionista_Select_Personas
    '''</summary>
    <TestMethod()> _
    Public Sub Comisionista_Select_PersonasTest()
        Dim target As DAOComisionista = New DAOComisionista() ' TODO: Inicializar en un valor adecuado
        Dim IdComisionCab As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdRol As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Comisionista = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Comisionista
        actual = target.Comisionista_Select_Personas(IdComisionCab, IdEmpresa, IdTienda, IdPersona, IdPerfil, IdRol)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Comisionista_Select_Personas_Lista
    '''</summary>
    <TestMethod()> _
    Public Sub Comisionista_Select_Personas_ListaTest()
        Dim target As DAOComisionista = New DAOComisionista() ' TODO: Inicializar en un valor adecuado
        Dim IdComisionCab As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdRol As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idConfigurado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Comisionista) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Comisionista)
        actual = target.Comisionista_Select_Personas_Lista(IdComisionCab, IdEmpresa, IdTienda, IdPerfil, IdRol, idConfigurado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Comisionista_SelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub Comisionista_SelectxParamsTest()
        Dim target As DAOComisionista = New DAOComisionista() ' TODO: Inicializar en un valor adecuado
        Dim IdComisionCab As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Comisionista) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Comisionista)
        actual = target.Comisionista_SelectxParams(IdComisionCab)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdComisionCabxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdComisionCabxIdPersonaTest()
        Dim target As DAOComisionista = New DAOComisionista() ' TODO: Inicializar en un valor adecuado
        Dim IdComisionCab As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexIdComisionCabxIdPersona(IdComisionCab, IdPersona, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub RegistrarTest()
        Dim target As DAOComisionista = New DAOComisionista() ' TODO: Inicializar en un valor adecuado
        Dim objComisionista As Comisionista = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Registrar(objComisionista, cnx, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
