﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoITest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoITest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoITest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoI
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoIConstructorTest()
        Dim target As DAODocumentoI = New DAODocumentoI()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaDocumentoI
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaDocumentoITest()
        Dim target As DAODocumentoI = New DAODocumentoI() ' TODO: Inicializar en un valor adecuado
        Dim documentoi As DocumentoI = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaDocumentoI(documentoi)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaDocumentoIT
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaDocumentoITTest()
        Dim target As DAODocumentoI = New DAODocumentoI() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Ldoi As List(Of DocumentoI) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.ActualizaDocumentoIT(cn, Ldoi, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdPersonaT
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdPersonaTTest()
        Dim target As DAODocumentoI = New DAODocumentoI() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdPersonaT(cn, IdPersona, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDocumentoI
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDocumentoITest()
        Dim target As DAODocumentoI = New DAODocumentoI() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim LDoi As List(Of DocumentoI) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaDocumentoI(cn, LDoi, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPersonaTest()
        Dim target As DAODocumentoI = New DAODocumentoI() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocumentoI = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocumentoI
        actual = target.SelectxIdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Selectx_IdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub Selectx_IdPersonaTest()
        Dim target As DAODocumentoI = New DAODocumentoI() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoI) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoI)
        actual = target.Selectx_IdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub listarTipoDocumentoTest()
        Dim target As DAODocumentoI = New DAODocumentoI() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumentoI) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumentoI)
        actual = target.listarTipoDocumento
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
