﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCuentaProveedorTest y se pretende que
'''contenga todas las pruebas unitarias DAOCuentaProveedorTest.
'''</summary>
<TestClass()> _
Public Class DAOCuentaProveedorTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCuentaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCuentaProveedorConstructorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizarCuentaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizarCuentaProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim cuentaproveedor As CuentaProveedor = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizarCuentaProveedor(cuentaproveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexCuentaProveedorxIdProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexCuentaProveedorxIdProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim idproveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexCuentaProveedorxIdProveedor(idproveedor, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdCtaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdCtaProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim idctaProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexIdCtaProveedor(idctaProveedor, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdCuentaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdCuentaProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim idcuentaProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexIdCuentaProveedor(idcuentaProveedor, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdProveedorCuentaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdProveedorCuentaProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdProveedorCuentaProveedor(IdProveedor, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocAsociadosCtaProvxPag
    '''</summary>
    <TestMethod()> _
    Public Sub DocAsociadosCtaProvxPagTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim idCtaProv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CxCResumenView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CxCResumenView)
        actual = target.DocAsociadosCtaProvxPag(idCtaProv)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaCuentaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaCuentaProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim CuentaProveedor As CuentaProveedor = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaCuentaProveedor(CuentaProveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaCuentaProveedorT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaCuentaProveedorTTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim obj As CuentaProveedor = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaCuentaProveedorT(obj, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoCuentaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoCuentaProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaProveedor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaProveedor)
        actual = target.SelectActivoCuentaProveedor
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllConsultaxHistCtaProv
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllConsultaxHistCtaProvTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim IdCtaProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaProveedor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaProveedor)
        actual = target.SelectAllConsultaxHistCtaProv(IdCtaProveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllConsultaxIdProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllConsultaxIdProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaProveedor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaProveedor)
        actual = target.SelectAllConsultaxIdProveedor(IdProveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllCuentaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllCuentaProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaProveedor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaProveedor)
        actual = target.SelectAllCuentaProveedor
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaCxCResumenDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaCxCResumenDocumentoTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CxCResumenView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CxCResumenView)
        actual = target.SelectGrillaCxCResumenDocumento(idpersona, idmoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaCxPResumenDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaCxPResumenDocumentoTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim idProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CxPResumenView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CxPResumenView)
        actual = target.SelectGrillaCxPResumenDocumento(idProveedor, idmoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoCuentaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoCuentaProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaProveedor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaProveedor)
        actual = target.SelectInactivoCuentaProveedor
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProv_IdPropCuentaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProv_IdPropCuentaProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPropietario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaProveedor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaProveedor)
        actual = target.SelectxIdProv_IdPropCuentaProveedor(IdProveedor, IdPropietario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaProveedor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaProveedor)
        actual = target.SelectxIdProveedor(IdProveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProveedorCuentaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProveedorCuentaProveedorTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaProveedor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaProveedor)
        actual = target.SelectxIdProveedorCuentaProveedor(IdProveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxParamsTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaPersona)
        actual = target.SelectxParams(IdPersona, IdEmpresa, IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateCuentaProveedorT
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateCuentaProveedorTTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim obj As CuentaProveedor = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateCuentaProveedorT(obj, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de fnInsTblCuentaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub fnInsTblCuentaPersonaTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim obj As CuentaPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.fnInsTblCuentaPersona(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fnUdpTblCuentaPersona
    '''</summary>
    <TestMethod()> _
    Public Sub fnUdpTblCuentaPersonaTest()
        Dim target As DAOCuentaProveedor = New DAOCuentaProveedor() ' TODO: Inicializar en un valor adecuado
        Dim obj As CuentaPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.fnUdpTblCuentaPersona(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
