﻿Imports System.Data.SqlClient

Imports System.Data

Imports System

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOChequeTest y se pretende que
'''contenga todas las pruebas unitarias DAOChequeTest.
'''</summary>
<TestClass()> _
Public Class DAOChequeTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCheque
    '''</summary>
    <TestMethod()> _
    Public Sub DAOChequeConstructorTest()
        Dim target As DAOCheque = New DAOCheque()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Cheque_SelectxIdCheque
    '''</summary>
    <TestMethod()> _
    Public Sub Cheque_SelectxIdChequeTest()
        Dim target As DAOCheque = New DAOCheque() ' TODO: Inicializar en un valor adecuado
        Dim IdCheque As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Cheque = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Cheque
        actual = target.Cheque_SelectxIdCheque(IdCheque)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Cheque_SelectxParams_DT
    '''</summary>
    <TestMethod()> _
    Public Sub Cheque_SelectxParams_DTTest()
        Dim target As DAOCheque = New DAOCheque() ' TODO: Inicializar en un valor adecuado
        Dim IdCheque As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuarioSupervisor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio_Find As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin_Find As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaCobrar_Inicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaCobrar_Fin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Estado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim EstadoMov As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.Cheque_SelectxParams_DT(IdCheque, IdCliente, IdBanco, IdUsuario, IdUsuarioSupervisor, FechaInicio_Find, FechaFin_Find, FechaCobrar_Inicio, FechaCobrar_Fin, IdEmpresa, IdTienda, Estado, EstadoMov)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Cheque_SelectxParams_ValBanco
    '''</summary>
    <TestMethod()> _
    Public Sub Cheque_SelectxParams_ValBancoTest()
        Dim target As DAOCheque = New DAOCheque() ' TODO: Inicializar en un valor adecuado
        Dim IdCheque As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuarioSupervisor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio_Find As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin_Find As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaCobrar_Inicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaCobrar_Fin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Estado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim EstadoMov As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.Cheque_SelectxParams_ValBanco(IdCheque, IdCliente, IdBanco, IdUsuario, IdUsuarioSupervisor, FechaInicio_Find, FechaFin_Find, FechaCobrar_Inicio, FechaCobrar_Fin, IdEmpresa, IdTienda, Estado, EstadoMov)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Cheque_ValSelect_AddDatoCancelacion_DT
    '''</summary>
    <TestMethod()> _
    Public Sub Cheque_ValSelect_AddDatoCancelacion_DTTest()
        Dim target As DAOCheque = New DAOCheque() ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaMov As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Val_DocumentoRelacionado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.Cheque_ValSelect_AddDatoCancelacion_DT(IdCliente, FechaMov, Val_DocumentoRelacionado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Finalize
    '''</summary>
    <TestMethod(), _
     DeploymentItem("DAO.dll")> _
    Public Sub FinalizeTest()
        Dim target As DAOCheque_Accessor = New DAOCheque_Accessor() ' TODO: Inicializar en un valor adecuado
        target.Finalize()
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub RegistrarTest()
        Dim target As DAOCheque = New DAOCheque() ' TODO: Inicializar en un valor adecuado
        Dim objCheque As Cheque = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Registrar(objCheque, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Update_Estado_EstadoMovxIdCheque
    '''</summary>
    <TestMethod()> _
    Public Sub Update_Estado_EstadoMovxIdChequeTest()
        Dim target As DAOCheque = New DAOCheque() ' TODO: Inicializar en un valor adecuado
        Dim IdCheque As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Estado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim EstadoMov As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Observacion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Update_Estado_EstadoMovxIdCheque(IdCheque, Estado, EstadoMov, Observacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
