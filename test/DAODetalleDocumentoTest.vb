﻿Imports System.Data

Imports System

Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODetalleDocumentoTest y se pretende que
'''contenga todas las pruebas unitarias DAODetalleDocumentoTest.
'''</summary>
<TestClass()> _
Public Class DAODetalleDocumentoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODetalleDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DAODetalleDocumentoConstructorTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaDetalleDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaDetalleDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim detalledocumento As DetalleDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaDetalleDocumento(detalledocumento, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaDetalleDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaDetalleDocumentoTest1()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim detalledocumento As DetalleDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaDetalleDocumento(detalledocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de BuscaCheckNotCred
    '''</summary>
    <TestMethod()> _
    Public Sub BuscaCheckNotCredTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.BuscaCheckNotCred(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de BuscaProdcompuestoo
    '''</summary>
    <TestMethod()> _
    Public Sub BuscaProdcompuestooTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.BuscaProdcompuestoo(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CambiarProdcompuesto
    '''</summary>
    <TestMethod()> _
    Public Sub CambiarProdcompuestoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.CambiarProdcompuesto(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdDetalleDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdDetalleDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDetalleDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexIdDetalleDocumento(IdDetalleDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdDocumento(IdDocumento, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleDocumentoRefxGuiaRecep
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleDocumentoRefxGuiaRecepTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DetalleDocumentoRefxGuiaRecep(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleDocumentoSelectDetallexAtenderxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleDocumentoSelectDetallexAtenderxIdDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DetalleDocumentoSelectDetallexAtenderxIdDocumento(IdDocumento, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleDocumentoSelectDetallexAtenderxIdDocumentoDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleDocumentoSelectDetallexAtenderxIdDocumentoDetalleTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DetalleDocumentoSelectDetallexAtenderxIdDocumentoDetalle(IdDocumento, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleDocumentoViewSelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleDocumentoViewSelectxIdDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumentoView)
        actual = target.DetalleDocumentoViewSelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoInvInicialDeleteDetallexIdDocumentoxIdSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoInvInicialDeleteDetallexIdDocumentoxIdSubLineaTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoInvInicialDeleteDetallexIdDocumentoxIdSubLinea(IdDocumento, IdSubLinea, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDetalleDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDetalleDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim detalledocumento As DetalleDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaDetalleDocumento(cn, detalledocumento, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDetalleDocumento_MovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDetalleDocumento_MovAlmacenTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalleDocumento As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim InsertaMovAlmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim ComprometeStock As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Factor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacenllegada As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.InsertaDetalleDocumento_MovAlmacen(cn, listaDetalleDocumento, tr, IdDocumento, InsertaMovAlmacen, ComprometeStock, Factor, IdAlmacen, IdAlmacenllegada)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaLDetalleDocumentoT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaLDetalleDocumentoTTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Ldetalledocumento As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaLDetalleDocumentoT(Cn, Ldetalledocumento, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de MovAlmacen_DetalleDocumentoDeletexIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub MovAlmacen_DetalleDocumentoDeletexIdDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.MovAlmacen_DetalleDocumentoDeletexIdDocumento(IdDocumento, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub RegistrarTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim objDetalleDocumento As DetalleDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Registrar(objDetalleDocumento, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCantxAtenderNoCeroxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCantxAtenderNoCeroxIdDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.SelectCantxAtenderNoCeroxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCantxAtenderxIdDetalleDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCantxAtenderxIdDetalleDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim iddetalledocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectCantxAtenderxIdDetalleDocumento(iddetalledocumento, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectComponenteKitxIdKitxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectComponenteKitxIdKitxIdDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdKit As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Kit) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Kit)
        actual = target.SelectComponenteKitxIdKitxIdDocumento(IdKit, IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectIdsxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectIdsxIdDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.SelectIdsxIdDocumento(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.SelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumentoValidacionTransito
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoValidacionTransitoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.SelectxIdDocumentoValidacionTransito(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumentoconcepto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoconceptoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.SelectxIdDocumentoconcepto(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdFact_NC
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdFact_NCTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdFactura As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdGuiaRecepcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.SelectxIdFact_NC(IdFactura, IdGuiaRecepcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdSubLineaxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdSubLineaxIdDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Prod_Codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Prod_Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Tabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumentoView)
        actual = target.SelectxIdSubLineaxIdDocumento(idsublinea, iddocumento, Prod_Codigo, Prod_Nombre, Tabla)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateCantxAtenderxIdDetalleDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateCantxAtenderxIdDetalleDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDetalleDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cantxatender As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateCantxAtenderxIdDetalleDocumento(IdDetalleDocumento, cantxatender, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fn_TotalPag_DetalleDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub fn_TotalPag_DetalleDocumentoTest()
        Dim target As DAODetalleDocumento = New DAODetalleDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.fn_TotalPag_DetalleDocumento(IdDocumento, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
