﻿Imports System

Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOProductoUMTest y se pretende que
'''contenga todas las pruebas unitarias DAOProductoUMTest.
'''</summary>
<TestClass()> _
Public Class DAOProductoUMTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOProductoUM
    '''</summary>
    <TestMethod()> _
    Public Sub DAOProductoUMConstructorTest()
        Dim target As DAOProductoUM = New DAOProductoUM()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaProductoUM
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaProductoUMTest()
        Dim target As DAOProductoUM = New DAOProductoUM() ' TODO: Inicializar en un valor adecuado
        Dim productoum As ProductoUM = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaProductoUM(productoum)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdProductoTest()
        Dim target As DAOProductoUM = New DAOProductoUM() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdProducto(IdProducto, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdProductoxIdUnidadMedida
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdProductoxIdUnidadMedidaTest()
        Dim target As DAOProductoUM = New DAOProductoUM() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexIdProductoxIdUnidadMedida(IdProducto, IdUnidadMedida, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaListaProductoUM
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaListaProductoUMTest()
        Dim target As DAOProductoUM = New DAOProductoUM() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of ProductoUMView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaListaProductoUM(IdProducto, lista, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaProductoUM
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaProductoUMTest()
        Dim target As DAOProductoUM = New DAOProductoUM() ' TODO: Inicializar en un valor adecuado
        Dim productoum As ProductoUM = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaProductoUM(productoum)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ProductoUMUpdatePorcentRetazo
    '''</summary>
    <TestMethod()> _
    Public Sub ProductoUMUpdatePorcentRetazoTest()
        Dim target As DAOProductoUM = New DAOProductoUM() ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idunidadmedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim porcentRetazo As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ProductoUMUpdatePorcentRetazo(idproducto, idunidadmedida, porcentRetazo, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOProductoUM = New DAOProductoUM() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoUM) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoUM)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectEquivalenciaxIdProductoxIdUnidadMedida
    '''</summary>
    <TestMethod()> _
    Public Sub SelectEquivalenciaxIdProductoxIdUnidadMedidaTest()
        Dim target As DAOProductoUM = New DAOProductoUM() ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectEquivalenciaxIdProductoxIdUnidadMedida(idProducto, IdUnidadMedida)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectIdUMPrincipalxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectIdUMPrincipalxIdProductoTest()
        Dim target As DAOProductoUM = New DAOProductoUM() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectIdUMPrincipalxIdProducto(IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProductoTest()
        Dim target As DAOProductoUM = New DAOProductoUM() ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoUM) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoUM)
        actual = target.SelectxIdProducto(idProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProductoxIdUM
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProductoxIdUMTest()
        Dim target As DAOProductoUM = New DAOProductoUM() ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idunidadmedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As ProductoUM = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As ProductoUM
        actual = target.SelectxIdProductoxIdUM(idproducto, idunidadmedida)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
