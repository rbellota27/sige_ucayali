﻿Imports System.Collections.Generic

Imports System.Data

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCuentaBancariaTest y se pretende que
'''contenga todas las pruebas unitarias DAOCuentaBancariaTest.
'''</summary>
<TestClass()> _
Public Class DAOCuentaBancariaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCuentaBancaria
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCuentaBancariaConstructorTest()
        Dim target As DAOCuentaBancaria = New DAOCuentaBancaria()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaCuentaBancaria
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaCuentaBancariaTest()
        Dim target As DAOCuentaBancaria = New DAOCuentaBancaria() ' TODO: Inicializar en un valor adecuado
        Dim cuentabancaria As CuentaBancaria = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim t As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaCuentaBancaria(cuentabancaria, cn, t)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeleteCuentaBancaria
    '''</summary>
    <TestMethod()> _
    Public Sub DeleteCuentaBancariaTest()
        Dim target As DAOCuentaBancaria = New DAOCuentaBancaria() ' TODO: Inicializar en un valor adecuado
        Dim idcuentaBancaria As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeleteCuentaBancaria(idcuentaBancaria, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaCuentaBancaria
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaCuentaBancariaTest()
        Dim target As DAOCuentaBancaria = New DAOCuentaBancaria() ' TODO: Inicializar en un valor adecuado
        Dim cuentabancaria As CuentaBancaria = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim t As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim esCuentaDetraccion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaCuentaBancaria(cuentabancaria, cn, t, esCuentaDetraccion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxIdPersona_DT
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxIdPersona_DTTest()
        Dim target As DAOCuentaBancaria = New DAOCuentaBancaria() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.SelectActivoxIdPersona_DT(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboBanco
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboBancoTest()
        Dim target As DAOCuentaBancaria = New DAOCuentaBancaria() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Banco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Banco)
        actual = target.SelectCboBanco(IdEmpresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdBanco
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdBancoTest()
        Dim target As DAOCuentaBancaria = New DAOCuentaBancaria() ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaBancaria) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaBancaria)
        actual = target.SelectCboxIdBanco(IdBanco)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdBancoxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdBancoxIdPersonaTest()
        Dim target As DAOCuentaBancaria = New DAOCuentaBancaria() ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaBancaria) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaBancaria)
        actual = target.SelectCboxIdBancoxIdPersona(IdBanco, IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectcuentaBancaria
    '''</summary>
    <TestMethod()> _
    Public Sub SelectcuentaBancariaTest()
        Dim target As DAOCuentaBancaria = New DAOCuentaBancaria() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idbanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Short = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Short = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaBancaria) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaBancaria)
        actual = target.SelectcuentaBancaria(idempresa, idbanco, estado, idmoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdBancoxIdCuentaBancaria
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdBancoxIdCuentaBancariaTest()
        Dim target As DAOCuentaBancaria = New DAOCuentaBancaria() ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCuentaBancaria As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As CuentaBancaria = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As CuentaBancaria
        actual = target.SelectxIdBancoxIdCuentaBancaria(IdBanco, IdCuentaBancaria)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de VerificarsiExisteCB
    '''</summary>
    <TestMethod()> _
    Public Sub VerificarsiExisteCBTest()
        Dim target As DAOCuentaBancaria = New DAOCuentaBancaria() ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As CuentaBancaria = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As CuentaBancaria
        actual = target.VerificarsiExisteCB(IdBanco, IdPersona, IdMoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de existeCuentaDetraccion
    '''</summary>
    <TestMethod()> _
    Public Sub existeCuentaDetraccionTest()
        Dim target As DAOCuentaBancaria = New DAOCuentaBancaria() ' TODO: Inicializar en un valor adecuado
        Dim idProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.existeCuentaDetraccion(idProveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
