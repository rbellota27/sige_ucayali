﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoChequeTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoChequeTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoChequeTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoCheque
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoChequeConstructorTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DAO_EmitirCheque
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_EmitirChequeTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdMovCtaPP As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoCheque) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoCheque)
        actual = target.DAO_EmitirCheque(cn, IdMovCtaPP)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCheque_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCheque_DeshacerMovTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim deleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoCheque_DeshacerMov(IdDocumento, deleteRelacionDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de GenerarDocCodigoChequexIdSerieCheque
    '''</summary>
    <TestMethod()> _
    Public Sub GenerarDocCodigoChequexIdSerieChequeTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim IdSerieCheque As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.GenerarDocCodigoChequexIdSerieCheque(IdSerieCheque)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GetVectorParametros
    '''</summary>
    <TestMethod()> _
    Public Sub GetVectorParametrosTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim x As DocumentoCheque = Nothing ' TODO: Inicializar en un valor adecuado
        Dim op As DAOMantenedor.modo_query = New DAOMantenedor.modo_query() ' TODO: Inicializar en un valor adecuado
        Dim expected() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual() As SqlParameter
        actual = target.GetVectorParametros(x, op)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoCheque) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoCheque)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxEstadoCancelacion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxEstadoCancelacionTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim Cancelado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoCheque) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoCheque)
        actual = target.SelectxEstadoCancelacion(Cancelado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocumentoCheque = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocumentoCheque
        actual = target.SelectxId(id)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdBeneficiario
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdBeneficiarioTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim IdBeneficiario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoCheque) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoCheque)
        actual = target.SelectxIdBeneficiario(IdBeneficiario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdBeneficiarioxEstadoCancelacion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdBeneficiarioxEstadoCancelacionTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim IdBeneficiario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Cancelado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoCheque) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoCheque)
        actual = target.SelectxIdBeneficiarioxEstadoCancelacion(IdBeneficiario, Cancelado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacionTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim IdBeneficiario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEstadoCancelacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoCheque) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoCheque)
        actual = target.SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacion(IdBeneficiario, IdEstadoCancelacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacionTest1()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim IdBeneficiario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Cancelado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoCheque) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoCheque)
        actual = target.SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacion(IdBeneficiario, Cancelado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxParamsTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim x As DocumentoCheque = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoCheque) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoCheque)
        actual = target.SelectxParams(x)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarNumeroCheque
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarNumeroChequeTest()
        Dim target As DAODocumentoCheque = New DAODocumentoCheque() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSerieCheque As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim ChequeNumero As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ValidarNumeroCheque(IdDocumento, IdSerieCheque, ChequeNumero)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
