﻿Imports System.Data.SqlClient

Imports System

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoCotizacionTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoCotizacionTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoCotizacionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoCotizacionConstructorTest()
        Dim target As DAODocumentoCotizacion = New DAODocumentoCotizacion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCotizacionSelectCab
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCotizacionSelectCabTest()
        Dim target As DAODocumentoCotizacion = New DAODocumentoCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nroDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocumentoCotizacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocumentoCotizacion
        actual = target.DocumentoCotizacionSelectCab(IdSerie, nroDocumento, IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCotizacionSelectCab22Factura
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCotizacionSelectCab22FacturaTest()
        Dim target As DAODocumentoCotizacion = New DAODocumentoCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nroDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocumentoCotizacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocumentoCotizacion
        actual = target.DocumentoCotizacionSelectCab22Factura(IdSerie, nroDocumento, IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCotizacionSelectDet
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCotizacionSelectDetTest()
        Dim target As DAODocumentoCotizacion = New DAODocumentoCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Consignacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DocumentoCotizacionSelectDet(IdDocumento, Consignacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCotizacionSelectDetalleConcepto
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCotizacionSelectDetalleConceptoTest()
        Dim target As DAODocumentoCotizacion = New DAODocumentoCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleConcepto)
        actual = target.DocumentoCotizacionSelectDetalleConcepto(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCotizacion_ConsultarTipoPrecioxParams
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCotizacion_ConsultarTipoPrecioxParamsTest()
        Dim target As DAODocumentoCotizacion = New DAODocumentoCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.DocumentoCotizacion_ConsultarTipoPrecioxParams(IdTienda, IdProducto, IdMoneda_Destino, Fecha)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCotizacion_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCotizacion_DeshacerMovTest()
        Dim target As DAODocumentoCotizacion = New DAODocumentoCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeleteAnexoDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteAnexoDetalle As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleConcepto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMontoRegimen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePuntoPartida As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePuntoLlegada As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoCotizacion_DeshacerMov(IdDocumento, DeleteAnexoDocumento, DeleteDetalleDocumento, DeleteAnexoDetalle, DeleteDetalleConcepto, DeleteMontoRegimen, DeleteObservacion, DeletePuntoPartida, DeletePuntoLlegada, deleteRelacionDocumento, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCotizacion_ValSelectTipoPrecioPV
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCotizacion_ValSelectTipoPrecioPVTest()
        Dim target As DAODocumentoCotizacion = New DAODocumentoCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Cad_IdProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Cad_IdUnidadMedida As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Cad_Cantidad As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Catalogo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Catalogo
        actual = target.DocumentoCotizacion_ValSelectTipoPrecioPV(IdUsuario, IdCondicionPago, IdMedioPago, fecha, IdMoneda, IdTienda, IdCliente, IdTipoPV_Destino, Cad_IdProducto, Cad_IdUnidadMedida, Cad_Cantidad, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoVenta_SelectDetxIdDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoVenta_SelectDetxIdDocumentoRefTest()
        Dim target As DAODocumentoCotizacion = New DAODocumentoCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdVendedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DocumentoVenta_SelectDetxIdDocumentoRef(IdDocumentoRef, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdPersona, IdMoneda, IdVendedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoVenta_SelectDetxIdDocumentoRef2
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoVenta_SelectDetxIdDocumentoRef2Test()
        Dim target As DAODocumentoCotizacion = New DAODocumentoCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdVendedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DocumentoVenta_SelectDetxIdDocumentoRef2(IdDocumentoRef, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdPersona, IdMoneda, IdVendedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoVenta_SelectDetxIdDocumentoRef3
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoVenta_SelectDetxIdDocumentoRef3Test()
        Dim target As DAODocumentoCotizacion = New DAODocumentoCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdVendedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DocumentoVenta_SelectDetxIdDocumentoRef3(IdDocumentoRef, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdPersona, IdMoneda, IdVendedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectValorEstimadoConcepto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectValorEstimadoConceptoTest()
        Dim target As DAODocumentoCotizacion = New DAODocumentoCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdConcepto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim UbigeoOrigen As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim UbigeoDestino As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PesoTotal As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim ImporteTotal As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim Fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectValorEstimadoConcepto(IdConcepto, IdTipoDocumento, UbigeoOrigen, UbigeoDestino, IdMoneda_Destino, PesoTotal, ImporteTotal, Fecha)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
