﻿Imports System

Imports System.Collections.Generic

Imports Entidades

Imports System.Data

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocOrdenPedidoTest y se pretende que
'''contenga todas las pruebas unitarias DAODocOrdenPedidoTest.
'''</summary>
<TestClass()> _
Public Class DAODocOrdenPedidoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocOrdenPedido
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocOrdenPedidoConstructorTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeleteDetallexID
    '''</summary>
    <TestMethod()> _
    Public Sub DeleteDetallexIDTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeleteDetallexID(iddocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleOrdenPedido
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleOrdenPedidoTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim year As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fsemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tipodocs As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.DetalleOrdenPedido(idempresa, idalmacen, year, semana, fechainicio, fechafin, fsemana, tipodocs)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertOrdenPedido
    '''</summary>
    <TestMethod()> _
    Public Sub InsertOrdenPedidoTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim ObjDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalle As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim obs As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim comprometerstock As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim objProgPedido As ProgramacionPedido = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.InsertOrdenPedido(ObjDocumento, listaDetalle, obs, comprometerstock, objProgPedido)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarProductosOrdenPedido
    '''</summary>
    <TestMethod()> _
    Public Sub ListarProductosOrdenPedidoTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim linea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim sublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codsubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim almacen1 As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.ListarProductosOrdenPedido(idalmacen, descripcion, linea, sublinea, codsubLinea, pageindex, pagesize, almacen1)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenPedidoConsultarRelacionDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenPedidoConsultarRelacionDocumentoTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.OrdenPedidoConsultarRelacionDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenPedidoConsultarStock
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenPedidoConsultarStockTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.OrdenPedidoConsultarStock(idalmacen, idproducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenPedido_ProgramacionPedido
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenPedido_ProgramacionPedidoTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.OrdenPedido_ProgramacionPedido(fecha, idEmpresa, idTienda, idAlmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenPedido_SemanaActual
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenPedido_SemanaActualTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.OrdenPedido_SemanaActual(fecha)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenPedido_Validar_ProgramacionPedido
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenPedido_Validar_ProgramacionPedidoTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.OrdenPedido_Validar_ProgramacionPedido(fecha, idEmpresa, idTienda, idAlmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenPedidoxSelectDetalleEntresucursales
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenPedidoxSelectDetalleEntresucursalesTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.OrdenPedidoxSelectDetalleEntresucursales(IdDocumento, idAlmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenPedidoxSelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenPedidoxSelectxIdDocumentoTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen2 As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.OrdenPedidoxSelectxIdDocumento(idDocumento, IdAlmacen, IdAlmacen2)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectOrdenPedido
    '''</summary>
    <TestMethod()> _
    Public Sub SelectOrdenPedidoTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idserie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectOrdenPedido(codigo, idserie)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateOrdenPedido
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateOrdenPedidoTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim objDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalle As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObs As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim comprometerStock As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim objProgPedido As ProgramacionPedido = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateOrdenPedido(objDocumento, listaDetalle, objObs, comprometerStock, objProgPedido)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de findDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub findDocumentoRefTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim serie As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.findDocumentoRef(serie, codigo, pageindex, pagesize, IdTienda, IdEmpresa, idalmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetOrdenPedido
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetOrdenPedidoTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetOrdenPedido(idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de insertBDOrdenPedido
    '''</summary>
    <TestMethod()> _
    Public Sub insertBDOrdenPedidoTest()
        Dim target As DAODocOrdenPedido = New DAODocOrdenPedido() ' TODO: Inicializar en un valor adecuado
        Dim obj As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.insertBDOrdenPedido(obj, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
