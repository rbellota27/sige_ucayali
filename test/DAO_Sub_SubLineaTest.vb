﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAO_Sub_SubLineaTest y se pretende que
'''contenga todas las pruebas unitarias DAO_Sub_SubLineaTest.
'''</summary>
<TestClass()> _
Public Class DAO_Sub_SubLineaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAO_Sub_SubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_Sub_SubLineaConstructorTest()
        Dim target As DAO_Sub_SubLinea = New DAO_Sub_SubLinea()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaSub_SubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaSub_SubLineaTest()
        Dim target As DAO_Sub_SubLinea = New DAO_Sub_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim Sub_SubLinea As Sub_SubLinea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaSub_SubLinea(Sub_SubLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaSub_SubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaSub_SubLineaTest()
        Dim target As DAO_Sub_SubLinea = New DAO_Sub_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim Sub_SubLinea As Sub_SubLinea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaSub_SubLinea(Sub_SubLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoSub_SubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoSub_SubLineaTest()
        Dim target As DAO_Sub_SubLinea = New DAO_Sub_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Sub_SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Sub_SubLinea)
        actual = target.SelectActivoSub_SubLinea
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoxIdLineav2
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoxIdLineav2Test()
        Dim target As DAO_Sub_SubLinea = New DAO_Sub_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Sub_SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Sub_SubLinea)
        actual = target.SelectAllActivoxIdLineav2(idsublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllSub_SubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllSub_SubLineaTest()
        Dim target As DAO_Sub_SubLinea = New DAO_Sub_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Sub_SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Sub_SubLinea)
        actual = target.SelectAllSub_SubLinea
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAO_Sub_SubLinea = New DAO_Sub_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Sub_SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Sub_SubLinea)
        actual = target.SelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoSub_SubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoSub_SubLineaTest()
        Dim target As DAO_Sub_SubLinea = New DAO_Sub_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Sub_SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Sub_SubLinea)
        actual = target.SelectInactivoSub_SubLinea
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectNombreActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectNombreActivoTest()
        Dim target As DAO_Sub_SubLinea = New DAO_Sub_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Sub_SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Sub_SubLinea)
        actual = target.SelectNombreActivo(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectNombreInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectNombreInactivoTest()
        Dim target As DAO_Sub_SubLinea = New DAO_Sub_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Sub_SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Sub_SubLinea)
        actual = target.SelectNombreInactivo(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectSub_SubLineaxSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectSub_SubLineaxSubLineaTest()
        Dim target As DAO_Sub_SubLinea = New DAO_Sub_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim IdSubLInea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Sub_SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Sub_SubLinea)
        actual = target.SelectSub_SubLineaxSubLinea(IdSubLInea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAO_Sub_SubLinea = New DAO_Sub_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Sub_SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Sub_SubLinea)
        actual = target.SelectxId(Codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
