﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOProfesionPersonaTest y se pretende que
'''contenga todas las pruebas unitarias DAOProfesionPersonaTest.
'''</summary>
<TestClass()> _
Public Class DAOProfesionPersonaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOProfesionPersona
    '''</summary>
    <TestMethod()> _
    Public Sub DAOProfesionPersonaConstructorTest()
        Dim target As DAOProfesionPersona = New DAOProfesionPersona()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaProfesionPersona
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaProfesionPersonaTest()
        Dim target As DAOProfesionPersona = New DAOProfesionPersona() ' TODO: Inicializar en un valor adecuado
        Dim profesionpersona As ProfesionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaProfesionPersona(profesionpersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaProfesionPersonaT
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaProfesionPersonaTTest()
        Dim target As DAOProfesionPersona = New DAOProfesionPersona() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim profesion As ProfesionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.ActualizaProfesionPersonaT(cn, profesion, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaListProfesionPersona
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaListProfesionPersonaTest()
        Dim target As DAOProfesionPersona = New DAOProfesionPersona() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim profesionpersona As List(Of ProfesionPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaListProfesionPersona(cn, profesionpersona, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaProfesionPersona
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaProfesionPersonaTest()
        Dim target As DAOProfesionPersona = New DAOProfesionPersona() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim profesionpersona As ProfesionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaProfesionPersona(cn, profesionpersona, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOProfesionPersona = New DAOProfesionPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As ProfesionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As ProfesionPersona
        actual = target.SelectxId(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Selectx_Id
    '''</summary>
    <TestMethod()> _
    Public Sub Selectx_IdTest()
        Dim target As DAOProfesionPersona = New DAOProfesionPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProfesionPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProfesionPersona)
        actual = target.Selectx_Id(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
