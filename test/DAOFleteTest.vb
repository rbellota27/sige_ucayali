﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOFleteTest y se pretende que
'''contenga todas las pruebas unitarias DAOFleteTest.
'''</summary>
<TestClass()> _
Public Class DAOFleteTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOFlete
    '''</summary>
    <TestMethod()> _
    Public Sub DAOFleteConstructorTest()
        Dim target As DAOFlete = New DAOFlete()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de InsertFlete
    '''</summary>
    <TestMethod()> _
    Public Sub InsertFleteTest()
        Dim target As DAOFlete = New DAOFlete() ' TODO: Inicializar en un valor adecuado
        Dim obj As Flete = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertFlete(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectFlete
    '''</summary>
    <TestMethod()> _
    Public Sub SelectFleteTest()
        Dim target As DAOFlete = New DAOFlete() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Flete) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Flete)
        actual = target.SelectFlete
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateFlete
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateFleteTest()
        Dim target As DAOFlete = New DAOFlete() ' TODO: Inicializar en un valor adecuado
        Dim obj As Flete = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateFlete(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de deleteFlete
    '''</summary>
    <TestMethod()> _
    Public Sub deleteFleteTest()
        Dim target As DAOFlete = New DAOFlete() ' TODO: Inicializar en un valor adecuado
        Dim idflete As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.deleteFlete(idflete)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
