﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports System

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocGuiaRecepcionTest y se pretende que
'''contenga todas las pruebas unitarias DAODocGuiaRecepcionTest.
'''</summary>
<TestClass()> _
Public Class DAODocGuiaRecepcionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocGuiaRecepcion
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocGuiaRecepcionConstructorTest()
        Dim target As DAODocGuiaRecepcion = New DAODocGuiaRecepcion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaRecepcionSelectCab
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaRecepcionSelectCabTest()
        Dim target As DAODocGuiaRecepcion = New DAODocGuiaRecepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocGuiaRecepcion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocGuiaRecepcion
        actual = target.DocumentoGuiaRecepcionSelectCab(IdSerie, Codigo, IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaRecepcionSelectCostoProducto
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaRecepcionSelectCostoProductoTest()
        Dim target As DAODocGuiaRecepcion = New DAODocGuiaRecepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.DocumentoGuiaRecepcionSelectCostoProducto(IdProducto, IdEmpresa, IdAlmacen, Fecha, IdDocumentoRef)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaRecepcion_BuscarDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaRecepcion_BuscarDocumentoRefTest()
        Dim target As DAODocGuiaRecepcion = New DAODocGuiaRecepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim TipoDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoGuiaRecepcion_BuscarDocumentoRef(IdEmpresa, IdTienda, IdAlmacen, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, TipoDocumentoRef, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaRecepcion_BuscarDocumentoRef2
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaRecepcion_BuscarDocumentoRef2Test()
        Dim target As DAODocGuiaRecepcion = New DAODocGuiaRecepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim TipoDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoGuiaRecepcion_BuscarDocumentoRef2(IdEmpresa, IdTienda, IdAlmacen, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, TipoDocumentoRef, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaRecepcion_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaRecepcion_DeshacerMovTest()
        Dim target As DAODocGuiaRecepcion = New DAODocGuiaRecepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim deleteDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteMovAlmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteSaldoKardex As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteDetalleMovAlmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteRelacionDoc As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteObservaciones As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePuntoPartida As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoGuiaRecepcion_DeshacerMov(IdDocumento, deleteDetalleDocumento, deleteMovAlmacen, deleteSaldoKardex, deleteDetalleMovAlmacen, deleteRelacionDoc, deleteObservaciones, DeletePuntoPartida, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaRecepcion_VerificarRegistro
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaRecepcion_VerificarRegistroTest()
        Dim target As DAODocGuiaRecepcion = New DAODocGuiaRecepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim TipoUso As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoGuiaRecepcion_VerificarRegistro(IdDocumento, TipoUso, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoSolicitudPago_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoSolicitudPago_DeshacerMovTest()
        Dim target As DAODocGuiaRecepcion = New DAODocGuiaRecepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim deleteRelacionDoc As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteObservaciones As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteDetalleConcepto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoSolicitudPago_DeshacerMov(IdDocumento, deleteRelacionDoc, deleteObservaciones, deleteDetalleConcepto, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDocumentoGuiaRecepcion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDocumentoGuiaRecepcionTest()
        Dim target As DAODocGuiaRecepcion = New DAODocGuiaRecepcion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim documento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaDocumentoGuiaRecepcion(cn, documento, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumentoGuiarecepcion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoGuiarecepcionTest()
        Dim target As DAODocGuiaRecepcion = New DAODocGuiaRecepcion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectxIdDocumentoGuiarecepcion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
