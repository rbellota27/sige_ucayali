﻿Imports System.Data.SqlClient

Imports System

Imports System.Collections.Generic

Imports Entidades

Imports System.Data

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoLiquidacionTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoLiquidacionTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoLiquidacionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoLiquidacion
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoLiquidacionConstructorTest()
        Dim target As DAODocumentoLiquidacion = New DAODocumentoLiquidacion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLiquidacionPrint
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLiquidacionPrintTest()
        Dim target As DAODocumentoLiquidacion = New DAODocumentoLiquidacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.DocumentoLiquidacionPrint(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLiquidacionSelectCabFind_Aplicacion
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLiquidacionSelectCabFind_AplicacionTest()
        Dim target As DAODocumentoLiquidacion = New DAODocumentoLiquidacion() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoLiquidacionSelectCabFind_Aplicacion(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLiquidacion_BuscarDocRef
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLiquidacion_BuscarDocRefTest()
        Dim target As DAODocumentoLiquidacion = New DAODocumentoLiquidacion() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoLiquidacion_BuscarDocRef(IdEmpresa, IdTienda, IdPersona, Serie, Codigo, IdTipoDocumentoRef, FechaInicio, FechaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLiquidacion_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLiquidacion_DeshacerMovTest()
        Dim target As DAODocumentoLiquidacion = New DAODocumentoLiquidacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeleteAnexoDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovAlmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePagoCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DocumentoLiquidacion_DeshacerMov(IdDocumento, DeleteAnexoDocumento, DeleteDetalleDocumento, DeleteMovAlmacen, DeleteMovCaja, DeletePagoCaja, DeleteObservacion, DeleteRelacionDocumento, Anular, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLiquidacion_SelectIdDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLiquidacion_SelectIdDocumentoRefTest()
        Dim target As DAODocumentoLiquidacion = New DAODocumentoLiquidacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of RelacionDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of RelacionDocumento)
        actual = target.DocumentoLiquidacion_SelectIdDocumentoRef(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoLiquidacion_UpdateSaldo
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoLiquidacion_UpdateSaldoTest()
        Dim target As DAODocumentoLiquidacion = New DAODocumentoLiquidacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoLiquidacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Monto As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim Factor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoLiquidacion_UpdateSaldo(IdDocumentoLiquidacion, Monto, Factor, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub
End Class
