﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOProgramacionPago_CXPTest y se pretende que
'''contenga todas las pruebas unitarias DAOProgramacionPago_CXPTest.
'''</summary>
<TestClass()> _
Public Class DAOProgramacionPago_CXPTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOProgramacionPago_CXP
    '''</summary>
    <TestMethod()> _
    Public Sub DAOProgramacionPago_CXPConstructorTest()
        Dim target As DAOProgramacionPago_CXP = New DAOProgramacionPago_CXP()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdProgramacionPagoCXP
    '''</summary>
    '<TestMethod()> _
    'Public Sub DeletexIdProgramacionPagoCXPTest()
    '    Dim target As DAOProgramacionPago_CXP = New DAOProgramacionPago_CXP() ' TODO: Inicializar en un valor adecuado
    '    Dim IdProgramacionPago_CXP As Integer = 0 ' TODO: Inicializar en un valor adecuado
    '    Dim idRequerimiento As Integer = 0 ' TODO: Inicializar en un valor adecuado
    '    Dim saldo As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
    '    target.DeletexIdProgramacionPagoCXP(IdProgramacionPago_CXP, idRequerimiento, saldo)
    '    Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    'End Sub

    '''<summary>
    '''Una prueba de Insert
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTest()
        Dim target As DAOProgramacionPago_CXP = New DAOProgramacionPago_CXP() ' TODO: Inicializar en un valor adecuado
        Dim cadenaDocumentoReferencia As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idRequerimiento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim objProgramacionPago_CXP As ProgramacionPago_CXP = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idEsDetraccion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim idEsRetencion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim idEsPercepcion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.Insert(cadenaDocumentoReferencia, idRequerimiento, objProgramacionPago_CXP, idEsDetraccion, idEsRetencion, idEsPercepcion, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    ' '''<summary>
    ' '''Una prueba de SelectxIdMovCuentaCXP
    ' '''</summary>
    '<TestMethod()> _
    'Public Sub SelectxIdMovCuentaCXPTest()
    '    Dim target As DAOProgramacionPago_CXP = New DAOProgramacionPago_CXP() ' TODO: Inicializar en un valor adecuado
    '    Dim IdMovCuentaCXP As Integer = 0 ' TODO: Inicializar en un valor adecuado
    '    Dim expected As List(Of ProgramacionPago_CXP) = Nothing ' TODO: Inicializar en un valor adecuado
    '    Dim actual As List(Of ProgramacionPago_CXP)
    '    actual = target.SelectxIdMovCuentaCXP(IdMovCuentaCXP)
    '    Assert.AreEqual(expected, actual)
    '    Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    'End Sub

    '''<summary>
    '''Una prueba de SelectxIdMovCuentaCXP2
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdMovCuentaCXP2Test()
        Dim target As DAOProgramacionPago_CXP = New DAOProgramacionPago_CXP() ' TODO: Inicializar en un valor adecuado
        Dim IdMovCuentaCXP As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProgramacionPago_CXP) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProgramacionPago_CXP)
        actual = target.SelectxIdMovCuentaCXP2(IdMovCuentaCXP)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProgramacionPago
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProgramacionPagoTest()
        Dim target As DAOProgramacionPago_CXP = New DAOProgramacionPago_CXP() ' TODO: Inicializar en un valor adecuado
        Dim IdProgramacionPago_CXP As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As ProgramacionPago_CXP = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As ProgramacionPago_CXP
        actual = target.SelectxIdProgramacionPago(IdProgramacionPago_CXP)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Update
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTest()
        Dim target As DAOProgramacionPago_CXP = New DAOProgramacionPago_CXP() ' TODO: Inicializar en un valor adecuado
        Dim objProgramacionPago_CXP As ProgramacionPago_CXP = Nothing ' TODO: Inicializar en un valor adecuado
        target.Update(objProgramacionPago_CXP)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub
End Class
