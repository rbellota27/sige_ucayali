﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOPerfilTest y se pretende que
'''contenga todas las pruebas unitarias DAOPerfilTest.
'''</summary>
<TestClass()> _
Public Class DAOPerfilTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOPerfil
    '''</summary>
    <TestMethod()> _
    Public Sub DAOPerfilConstructorTest()
        Dim target As DAOPerfil = New DAOPerfil()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de InsertaPerfil
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaPerfilTest()
        Dim target As DAOPerfil = New DAOPerfil() ' TODO: Inicializar en un valor adecuado
        Dim Perfil As Perfil = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaPerfil(Perfil, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de PerfilUpdate
    '''</summary>
    <TestMethod()> _
    Public Sub PerfilUpdateTest()
        Dim target As DAOPerfil = New DAOPerfil() ' TODO: Inicializar en un valor adecuado
        Dim Perfil As Perfil = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.PerfilUpdate(Perfil, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTest()
        Dim target As DAOPerfil = New DAOPerfil() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Perfil) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Perfil)
        actual = target.SelectCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectIdsxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectIdsxIdPersonaTest()
        Dim target As DAOPerfil = New DAOPerfil() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Perfil) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Perfil)
        actual = target.SelectIdsxIdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPerfilxNombrexEstado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPerfilxNombrexEstadoTest()
        Dim target As DAOPerfil = New DAOPerfil() ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Estado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Perfil) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Perfil)
        actual = target.SelectxIdPerfilxNombrexEstado(IdPerfil, Nombre, Estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
