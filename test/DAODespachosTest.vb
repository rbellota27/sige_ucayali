﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data

Imports System

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODespachosTest y se pretende que
'''contenga todas las pruebas unitarias DAODespachosTest.
'''</summary>
<TestClass()> _
Public Class DAODespachosTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODespachos
    '''</summary>
    <TestMethod()> _
    Public Sub DAODespachosConstructorTest()
        Dim target As DAODespachos = New DAODespachos()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DAO_BuscarDocumentoGeneral
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_BuscarDocumentoGeneralTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim nroSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nroDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As String = String.Empty ' TODO: Inicializar en un valor adecuado
        target.DAO_BuscarDocumentoGeneral(cn, nroSerie, nroDocumento, idTienda)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_Controladores
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_ControladoresTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim idDocumentos As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim flag As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idusuario As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim flagFiltro As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim fechaFiltro As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Trasnsportista As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.DAO_Controladores(idDocumentos, flag, idusuario, flagFiltro, fechaFiltro, Trasnsportista)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_Controladores_UPDATE
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_Controladores_UPDATETest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim id_despacho_controlador As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSector As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cantidadDespachada As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim idUnidadEntrada As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idUnidadSalida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTono As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.DAO_Controladores_UPDATE(id_despacho_controlador, idAlmacen, idProducto, idSector, cantidadDespachada, idUnidadEntrada, idUnidadSalida, idTono)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_DataSetTipados
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_DataSetTipadosTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim dt As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim flag As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim condicion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim documento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaEmision As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim check As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.DAO_DataSetTipados(dt, flag, condicion, tienda, serie, documento, fechaEmision, check)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_FiltrarGuias
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_FiltrarGuiasTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim filtro As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim flag As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim condicion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim documento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaEmision As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim check As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of be_guiaRemision) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of be_guiaRemision)
        actual = target.DAO_FiltrarGuias(filtro, flag, condicion, tienda, serie, documento, fechaEmision, check, cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_InsertarGuias
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_InsertarGuiasTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim peso As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim fechaInicioDespacho As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim capacidadAlcanzada As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cantidadDoc As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idEstadoDespacho As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idDocumentoRelacion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim entidades As Vehiculo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim flag As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idCronogramaDespacho As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idDocumentoDeselecion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nroVuelta As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.DAO_InsertarGuias(peso, fechaInicioDespacho, capacidadAlcanzada, cantidadDoc, idEstadoDespacho, idDocumentoRelacion, entidades, flag, idCronogramaDespacho, idDocumentoDeselecion, nroVuelta)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_ListaPicking
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_ListaPickingTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim paramidSector As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim flagTransporte As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim fecFiltro As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim idTransportista As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim turno As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of be_picking) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of be_picking)
        actual = target.DAO_ListaPicking(paramidSector, flagTransporte, fecFiltro, idTransportista, cn, turno)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_ListarDatosGenerales
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_ListarDatosGeneralesTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim filtro As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim dt As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim flag As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim condicion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim documento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaEmision As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim check As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.DAO_ListarDatosGenerales(filtro, dt, flag, condicion, tienda, serie, documento, fechaEmision, check)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_ListarDatosGenerales
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_ListarDatosGeneralesTest1()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim filtro As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim flag As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim condicion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim documento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaEmision As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim check As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of be_cronograma) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of be_cronograma)
        actual = target.DAO_ListarDatosGenerales(filtro, flag, condicion, tienda, serie, documento, fechaEmision, check, cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_ListarPersonasVehiculo
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_ListarPersonasVehiculoTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.DAO_ListarPersonasVehiculo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_Reporte
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_ReporteTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim fecInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fecFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim turno As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim placa As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idtienda As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.DAO_Reporte(fecInicio, fecFin, turno, placa, idtienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_Reporte_Distribucion
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_Reporte_DistribucionTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.DAO_Reporte_Distribucion(idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_UPDATE_FEC_DESPACHOS
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_UPDATE_FEC_DESPACHOSTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.DAO_UPDATE_FEC_DESPACHOS(idDocumento, fecha)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_UPDATE_PESO_DESPACHOS
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_UPDATE_PESO_DESPACHOSTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim peso As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.DAO_UPDATE_PESO_DESPACHOS(idDocumento, peso)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_UpdateProgramacion
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_UpdateProgramacionTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim idCronograma As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim id_anulacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim comentario As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.DAO_UpdateProgramacion(idCronograma, id_anulacion, comentario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_eventoLoad
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_eventoLoadTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Object) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Object)
        actual = target.DAO_eventoLoad(cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarVehiculos
    '''</summary>
    <TestMethod()> _
    Public Sub ListarVehiculosTest()
        Dim target As DAODespachos = New DAODespachos() ' TODO: Inicializar en un valor adecuado
        Dim identificador As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim filtro As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.ListarVehiculos(identificador, filtro)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
