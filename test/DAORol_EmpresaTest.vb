﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAORol_EmpresaTest y se pretende que
'''contenga todas las pruebas unitarias DAORol_EmpresaTest.
'''</summary>
<TestClass()> _
Public Class DAORol_EmpresaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAORol_Empresa
    '''</summary>
    <TestMethod()> _
    Public Sub DAORol_EmpresaConstructorTest()
        Dim target As DAORol_Empresa = New DAORol_Empresa()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaRol_Empresa
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaRol_EmpresaTest()
        Dim target As DAORol_Empresa = New DAORol_Empresa() ' TODO: Inicializar en un valor adecuado
        Dim rol_empresa As Rol_Empresa = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaRol_Empresa(rol_empresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GrabaRol_EmpresaT
    '''</summary>
    <TestMethod()> _
    Public Sub GrabaRol_EmpresaTTest()
        Dim target As DAORol_Empresa = New DAORol_Empresa() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lRolEmpresa As List(Of Rol_Empresa) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.GrabaRol_EmpresaT(cn, lRolEmpresa, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaRol_Empresa
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaRol_EmpresaTest()
        Dim target As DAORol_Empresa = New DAORol_Empresa() ' TODO: Inicializar en un valor adecuado
        Dim rol_empresa As Rol_Empresa = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaRol_Empresa(rol_empresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdEmpresa
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdEmpresaTest()
        Dim target As DAORol_Empresa = New DAORol_Empresa() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol_Empresa) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol_Empresa)
        actual = target.SelectxIdEmpresa(IdEmpresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
