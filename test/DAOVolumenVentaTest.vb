﻿Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOVolumenVentaTest y se pretende que
'''contenga todas las pruebas unitarias DAOVolumenVentaTest.
'''</summary>
<TestClass()> _
Public Class DAOVolumenVentaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOVolumenVenta
    '''</summary>
    <TestMethod()> _
    Public Sub DAOVolumenVentaConstructorTest()
        Dim target As DAOVolumenVenta = New DAOVolumenVenta()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaVolumenVenta
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaVolumenVentaTest()
        Dim target As DAOVolumenVenta = New DAOVolumenVenta() ' TODO: Inicializar en un valor adecuado
        Dim obj As VolumenVenta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaVolumenVenta(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeleteVolumenVenta
    '''</summary>
    <TestMethod()> _
    Public Sub DeleteVolumenVentaTest()
        Dim target As DAOVolumenVenta = New DAOVolumenVenta() ' TODO: Inicializar en un valor adecuado
        Dim obj As VolumenVenta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeleteVolumenVenta(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeleteVolumenVentaxParametros
    '''</summary>
    <TestMethod()> _
    Public Sub DeleteVolumenVentaxParametrosTest()
        Dim target As DAOVolumenVenta = New DAOVolumenVenta() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeleteVolumenVentaxParametros(IdLinea, IdSubLinea, IdTipoPV, IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaVolumenVenta
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaVolumenVentaTest()
        Dim target As DAOVolumenVenta = New DAOVolumenVenta() ' TODO: Inicializar en un valor adecuado
        Dim obj As VolumenVenta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaVolumenVenta(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaVolumenVentaxParametros
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaVolumenVentaxParametrosTest()
        Dim target As DAOVolumenVenta = New DAOVolumenVenta() ' TODO: Inicializar en un valor adecuado
        Dim obj As VolumenVenta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlcn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqltr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaVolumenVentaxParametros(obj, sqlcn, sqltr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
