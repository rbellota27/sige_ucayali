﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoFacturacionTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoFacturacionTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoFacturacionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoFacturacion
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoFacturacionConstructorTest()
        Dim target As DAODocumentoFacturacion = New DAODocumentoFacturacion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoFacturacionSelectPrintxIds
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoFacturacionSelectPrintxIdsTest()
        Dim target As DAODocumentoFacturacion = New DAODocumentoFacturacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoInicio As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoFin As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.DocumentoFacturacionSelectPrintxIds(IdDocumentoInicio, IdDocumentoFin, IdSerie)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoFacturacion_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoFacturacion_DeshacerMovTest()
        Dim target As DAODocumentoFacturacion = New DAODocumentoFacturacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteAnexoDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteAnexoDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovAlmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePuntoPartida As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePuntoLlegada As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleConcepto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePagoCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMontoRegimen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovCuenta As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleDocumentoR As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoFacturacion_DeshacerMov(IdDocumento, DeleteRelacionDocumento, DeleteAnexoDocumento, DeleteDetalleDocumento, DeleteAnexoDetalleDocumento, DeleteMovAlmacen, DeletePuntoPartida, DeletePuntoLlegada, DeleteDetalleConcepto, DeleteMovCaja, DeletePagoCaja, DeleteObservacion, DeleteMontoRegimen, DeleteMovCuenta, DeleteDetalleDocumentoR, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoSelectDocumentoAnticipo
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoSelectDocumentoAnticipoTest()
        Dim target As DAODocumentoFacturacion = New DAODocumentoFacturacion() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.DocumentoSelectDocumentoAnticipo(IdPersona, IdMoneda_Destino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de MovBancoInsert_Venta
    '''</summary>
    <TestMethod()> _
    Public Sub MovBancoInsert_VentaTest()
        Dim target As DAODocumentoFacturacion = New DAODocumentoFacturacion() ' TODO: Inicializar en un valor adecuado
        Dim objMovBanco As MovBancoView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.MovBancoInsert_Venta(objMovBanco, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
