﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTiendaTest y se pretende que
'''contenga todas las pruebas unitarias DAOTiendaTest.
'''</summary>
<TestClass()> _
Public Class DAOTiendaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTienda
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTiendaConstructorTest()
        Dim target As DAOTienda = New DAOTienda()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaTienda
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaTiendaTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim tienda As Tienda = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaTienda(tienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de BuscaAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub BuscaAllxNombreTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.BuscaAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de BuscaAllxNombreActivoInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub BuscaAllxNombreActivoInactivoTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.BuscaAllxNombreActivoInactivo(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertObjxTienda
    '''</summary>
    <TestMethod()> _
    Public Sub InsertObjxTiendaTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim obj As Tienda = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertObjxTienda(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaConfiguracionTiendaT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaConfiguracionTiendaTTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim lTiendaArea As List(Of TiendaArea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lTiendaAlmacen As List(Of TiendaAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaConfiguracionTiendaT(IdTienda, lTiendaArea, lTiendaAlmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaTienda
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTiendaTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim tienda As Tienda = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaTienda(tienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNombreTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectActivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllObjetivoxTienda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllObjetivoxTiendaTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim obj As Tienda = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectAllObjetivoxTienda(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxEmpresa
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxEmpresaTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectCboxEmpresa(idEmpresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdEmpresaxIdUsuario_Caja
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdEmpresaxIdUsuario_CajaTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectCboxIdEmpresaxIdUsuario_Caja(idEmpresa, IdUsuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxNombreTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectInactivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectxId(IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdTiendaObjetivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTiendaObjetivoTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim obj As Tienda = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Tienda = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Tienda
        actual = target.SelectxIdTiendaObjetivo(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateObjetivoxTienda
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateObjetivoxTiendaTest()
        Dim target As DAOTienda = New DAOTienda() ' TODO: Inicializar en un valor adecuado
        Dim obj As Tienda = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateObjetivoxTienda(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
