﻿Imports System.Data

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentosMercantilesTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentosMercantilesTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentosMercantilesTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentosMercantiles
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentosMercantilesConstructorTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ImpDocsEjm
    '''</summary>
    <TestMethod()> _
    Public Sub ImpDocsEjmTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocs As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ImpDocsEjm(IdTipoDocs)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RptCosteoImportacion
    '''</summary>
    <TestMethod()> _
    Public Sub RptCosteoImportacionTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RptCosteoImportacion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RptDocNotaCredito
    '''</summary>
    <TestMethod()> _
    Public Sub RptDocNotaCreditoTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RptDocNotaCredito(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RptDocReciboE
    '''</summary>
    <TestMethod()> _
    Public Sub RptDocReciboETest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RptDocReciboE(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RptDocReciboI
    '''</summary>
    <TestMethod()> _
    Public Sub RptDocReciboITest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RptDocReciboI(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de crpImprimir
    '''</summary>
    <TestMethod()> _
    Public Sub crpImprimirTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim idtipodocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Short = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Short
        actual = target.crpImprimir(idtipodocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetOrdenCompra
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetOrdenCompraTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetOrdenCompra(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetOrdenCompraNomComercial
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetOrdenCompraNomComercialTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetOrdenCompraNomComercial(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getReporteDocCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub getReporteDocCotizacionTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getReporteDocCotizacion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de rptDocControlInterno
    '''</summary>
    <TestMethod()> _
    Public Sub rptDocControlInternoTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.rptDocControlInterno(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de rptDocFactBol
    '''</summary>
    <TestMethod()> _
    Public Sub rptDocFactBolTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tipoimpresion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.rptDocFactBol(iddocumento, tipoimpresion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de rptDocGuias
    '''</summary>
    <TestMethod()> _
    Public Sub rptDocGuiasTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.rptDocGuias(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de rptDocNotaDebito
    '''</summary>
    <TestMethod()> _
    Public Sub rptDocNotaDebitoTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.rptDocNotaDebito(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de rptDocPedidoCliente
    '''</summary>
    <TestMethod()> _
    Public Sub rptDocPedidoClienteTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.rptDocPedidoCliente(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de rptGuiasxTonos
    '''</summary>
    <TestMethod()> _
    Public Sub rptGuiasxTonosTest()
        Dim target As DAODocumentosMercantiles = New DAODocumentosMercantiles() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.rptGuiasxTonos(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
