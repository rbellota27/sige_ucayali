﻿Imports System.Collections.Generic

Imports System

Imports Entidades

Imports System.Data

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOClienteTest y se pretende que
'''contenga todas las pruebas unitarias DAOClienteTest.
'''</summary>
<TestClass()> _
Public Class DAOClienteTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCliente
    '''</summary>
    <TestMethod()> _
    Public Sub DAOClienteConstructorTest()
        Dim target As DAOCliente = New DAOCliente()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DataSetCxCResumenCliente
    '''</summary>
    <TestMethod()> _
    Public Sub DataSetCxCResumenClienteTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.DataSetCxCResumenCliente(idEmpresa, idtienda, idmoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DataSetCxCResumenDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DataSetCxCResumenDocumentoTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.DataSetCxCResumenDocumento(idEmpresa, idtienda, idmoneda, idpersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleCxC
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleCxCTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim vopcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaIni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.DetalleCxC(idpersona, vopcion, idempresa, idtienda, FechaIni, FechaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleCxCDocCliente
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleCxCDocClienteTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim vopcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.DetalleCxCDocCliente(idpersona, vopcion, idempresa, idtienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleGuiasRemision
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleGuiasRemisionTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idAlmacenOrigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idAlmacenDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim vmercaTransito As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.DetalleGuiasRemision(idempresa, idAlmacenOrigen, idAlmacenDestino, fechaInicio, fechafin, vmercaTransito, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GetProcedureGuiaRemision
    '''</summary>
    <TestMethod(), _
     DeploymentItem("DAO.dll")> _
    Public Sub GetProcedureGuiaRemisionTest()
        Dim target As DAOCliente_Accessor = New DAOCliente_Accessor() ' TODO: Inicializar en un valor adecuado
        Dim mercaTransito As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.GetProcedureGuiaRemision(mercaTransito)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de PersonaUpdate_MO
    '''</summary>
    <TestMethod()> _
    Public Sub PersonaUpdate_MOTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim objPersonaView As PersonaView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.PersonaUpdate_MO(objPersonaView)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de PersonaUpdate_Ventas
    '''</summary>
    <TestMethod()> _
    Public Sub PersonaUpdate_VentasTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim objPersonaView As PersonaView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.PersonaUpdate_Ventas(objPersonaView)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RankingClientes
    '''</summary>
    <TestMethod()> _
    Public Sub RankingClientesTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idCondicionpago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTipopersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nprimeros As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RankingClientes(idempresa, idtienda, idCondicionpago, idmoneda, idTipopersona, fechaInicio, fechafin, nprimeros, idlinea, idsublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReporteDetalleDAOT
    '''</summary>
    <TestMethod()> _
    Public Sub ReporteDetalleDAOTTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim uit As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim doc As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.ReporteDetalleDAOT(idempresa, idtienda, idpersona, fechaInicio, fechafin, tipo, uit, doc)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReporteIngresosxTienda
    '''</summary>
    <TestMethod()> _
    Public Sub ReporteIngresosxTiendaTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.ReporteIngresosxTienda(idempresa, idtienda, fechaInicio, fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaCliente
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaClienteTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaCliente
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaClientexDNI
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaClientexDNITest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim numero As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaClientexDNI(numero)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaClientexEstado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaClientexEstadoTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim estado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaClientexEstado(estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaClientexIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaClientexIdPersonaTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaClientexIdPersona(codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaClientexNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaClientexNombreTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim BuscaPor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaClientexNombre(nombre, BuscaPor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaClientexRUC
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaClientexRUCTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim numero As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaClientexRUC(numero)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaClientexRazonSocial
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaClientexRazonSocialTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectGrillaClientexRazonSocial(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxDni
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxDniTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim Dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As PersonaView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As PersonaView
        actual = target.SelectxDni(Dni)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As PersonaView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As PersonaView
        actual = target.SelectxId(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId_Ventas
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxId_VentasTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As PersonaView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As PersonaView
        actual = target.SelectxId_Ventas(IdPersona, idempresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxNombreTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectxNombre(Nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxNombreComercial
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxNombreComercialTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim NombreComercial As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectxNombreComercial(NombreComercial)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxRazonSocial
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxRazonSocialTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim RazonSocial As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectxRazonSocial(RazonSocial)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxRuc
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxRucTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim Ruc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As PersonaView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As PersonaView
        actual = target.SelectxRuc(Ruc)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de VentasXTienda
    '''</summary>
    <TestMethod()> _
    Public Sub VentasXTiendaTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.VentasXTienda(idempresa, idLinea, idSublinea, idProducto, fechaInicio, fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de VentasXTiendaRSet
    '''</summary>
    <TestMethod()> _
    Public Sub VentasXTiendaRSetTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.VentasXTiendaRSet(idempresa, idLinea, idSublinea, idProducto, fechaInicio, fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getProcedCXC
    '''</summary>
    <TestMethod(), _
     DeploymentItem("DAO.dll")> _
    Public Sub getProcedCXCTest()
        Dim target As DAOCliente_Accessor = New DAOCliente_Accessor() ' TODO: Inicializar en un valor adecuado
        Dim idopcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.getProcedCXC(idopcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de gettable
    '''</summary>
    <TestMethod()> _
    Public Sub gettableTest()
        Dim target As DAOCliente = New DAOCliente() ' TODO: Inicializar en un valor adecuado
        Dim dt As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        target.gettable(dt)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub
End Class
