﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOEmpresaTiendaUsuarioTest y se pretende que
'''contenga todas las pruebas unitarias DAOEmpresaTiendaUsuarioTest.
'''</summary>
<TestClass()> _
Public Class DAOEmpresaTiendaUsuarioTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOEmpresaTiendaUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub DAOEmpresaTiendaUsuarioConstructorTest()
        Dim target As DAOEmpresaTiendaUsuario = New DAOEmpresaTiendaUsuario()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdUsuarioTest()
        Dim target As DAOEmpresaTiendaUsuario = New DAOEmpresaTiendaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexIdUsuario(IdUsuario, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Insert
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTest()
        Dim target As DAOEmpresaTiendaUsuario = New DAOEmpresaTiendaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim objETU As EmpresaTiendaUsuario = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.Insert(objETU, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectEmpresaxIdUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub SelectEmpresaxIdUsuarioTest()
        Dim target As DAOEmpresaTiendaUsuario = New DAOEmpresaTiendaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim Idusuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Propietario) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Propietario)
        actual = target.SelectEmpresaxIdUsuario(Idusuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectEscalaTienda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectEscalaTiendaTest()
        Dim target As DAOEmpresaTiendaUsuario = New DAOEmpresaTiendaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectEscalaTienda
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectTiendaObjetivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectTiendaObjetivoTest()
        Dim target As DAOEmpresaTiendaUsuario = New DAOEmpresaTiendaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectTiendaObjetivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectTiendaxIdEmpresaxIdUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub SelectTiendaxIdEmpresaxIdUsuarioTest()
        Dim target As DAOEmpresaTiendaUsuario = New DAOEmpresaTiendaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim Idusuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectTiendaxIdEmpresaxIdUsuario(Idusuario, IdEmpresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectTiendaxIdUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub SelectTiendaxIdUsuarioTest()
        Dim target As DAOEmpresaTiendaUsuario = New DAOEmpresaTiendaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim Idusuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda)
        actual = target.SelectTiendaxIdUsuario(Idusuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdUsuarioTest()
        Dim target As DAOEmpresaTiendaUsuario = New DAOEmpresaTiendaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim Idusuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of EmpresaTiendaUsuario) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of EmpresaTiendaUsuario)
        actual = target.SelectxIdUsuario(Idusuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fn_GetTiendaPrincipal
    '''</summary>
    <TestMethod()> _
    Public Sub fn_GetTiendaPrincipalTest()
        Dim target As DAOEmpresaTiendaUsuario = New DAOEmpresaTiendaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.fn_GetTiendaPrincipal(IdUsuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
