﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOOficinaTest y se pretende que
'''contenga todas las pruebas unitarias DAOOficinaTest.
'''</summary>
<TestClass()> _
Public Class DAOOficinaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOOficina
    '''</summary>
    <TestMethod()> _
    Public Sub DAOOficinaConstructorTest()
        Dim target As DAOOficina = New DAOOficina()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaOficina
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaOficinaTest()
        Dim target As DAOOficina = New DAOOficina() ' TODO: Inicializar en un valor adecuado
        Dim oficina As Oficina = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaOficina(oficina)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdBancoxIdOficina
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdBancoxIdOficinaTest()
        Dim target As DAOOficina = New DAOOficina() ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdOficina As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexIdBancoxIdOficina(IdBanco, IdOficina, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaListaOficina
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaListaOficinaTest()
        Dim target As DAOOficina = New DAOOficina() ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of Oficina) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaListaOficina(IdBanco, lista, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaOficina
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaOficinaTest()
        Dim target As DAOOficina = New DAOOficina() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of Oficina) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.InsertaOficina(cn, tr, lista, idBanco)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdBanco
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdBancoTest()
        Dim target As DAOOficina = New DAOOficina() ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Oficina) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Oficina)
        actual = target.SelectCboxIdBanco(IdBanco)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdBanco
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdBancoTest()
        Dim target As DAOOficina = New DAOOficina() ' TODO: Inicializar en un valor adecuado
        Dim IdBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Oficina) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Oficina)
        actual = target.SelectxIdBanco(IdBanco)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
