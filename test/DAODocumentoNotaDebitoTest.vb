﻿Imports System.Collections.Generic

Imports System

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoNotaDebitoTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoNotaDebitoTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoNotaDebitoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoNotaDebito
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoNotaDebitoConstructorTest()
        Dim target As DAODocumentoNotaDebito = New DAODocumentoNotaDebito()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNotaDebitoSelectCab
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNotaDebitoSelectCabTest()
        Dim target As DAODocumentoNotaDebito = New DAODocumentoNotaDebito() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocumentoNotaDebito = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocumentoNotaDebito
        actual = target.DocumentoNotaDebitoSelectCab(IdSerie, Codigo, IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNotaDebito_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNotaDebito_DeshacerMovTest()
        Dim target As DAODocumentoNotaDebito = New DAODocumentoNotaDebito() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim deleteDetalleConcepto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteMovCuenta As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteRelacionDoc As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoNotaDebito_DeshacerMov(IdDocumento, deleteDetalleConcepto, deleteMovCuenta, deleteObservacion, anular, deleteRelacionDoc, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocReferenciaxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocReferenciaxParamsTest()
        Dim target As DAODocumentoNotaDebito = New DAODocumentoNotaDebito() ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaI As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaF As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim opcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.SelectDocReferenciaxParams(IdCliente, FechaI, FechaF, IdTienda, serie, codigo, opcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
