﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOPostTest y se pretende que
'''contenga todas las pruebas unitarias DAOPostTest.
'''</summary>
<TestClass()> _
Public Class DAOPostTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOPost
    '''</summary>
    <TestMethod()> _
    Public Sub DAOPostConstructorTest()
        Dim target As DAOPost = New DAOPost()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de GetVectorParametros
    '''</summary>
    <TestMethod()> _
    Public Sub GetVectorParametrosTest()
        Dim target As DAOPost = New DAOPost() ' TODO: Inicializar en un valor adecuado
        Dim x As Post = Nothing ' TODO: Inicializar en un valor adecuado
        Dim op As DAOMantenedor.modo_query = New DAOMantenedor.modo_query() ' TODO: Inicializar en un valor adecuado
        Dim expected() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual() As SqlParameter
        actual = target.GetVectorParametros(x, op)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTTest()
        Dim target As DAOPost = New DAOPost() ' TODO: Inicializar en un valor adecuado
        Dim x As Post = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertT(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertT_AddListPos_TipoTarjeta
    '''</summary>
    <TestMethod()> _
    Public Sub InsertT_AddListPos_TipoTarjetaTest()
        Dim target As DAOPost = New DAOPost() ' TODO: Inicializar en un valor adecuado
        Dim x As Post = Nothing ' TODO: Inicializar en un valor adecuado
        Dim L As List(Of Pos_TipoTarjeta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertT_AddListPos_TipoTarjeta(x, L, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertT_GetID
    '''</summary>
    <TestMethod()> _
    Public Sub InsertT_GetIDTest()
        Dim target As DAOPost = New DAOPost() ' TODO: Inicializar en un valor adecuado
        Dim x As Post = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertT_GetID(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPost
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPostTest()
        Dim target As DAOPost = New DAOPost() ' TODO: Inicializar en un valor adecuado
        Dim IdPost As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Post = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Post
        actual = target.SelectxIdPost(IdPost)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateT
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTTest()
        Dim target As DAOPost = New DAOPost() ' TODO: Inicializar en un valor adecuado
        Dim x As Post = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateT(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateT_AddListPos_TipoTarjeta
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateT_AddListPos_TipoTarjetaTest()
        Dim target As DAOPost = New DAOPost() ' TODO: Inicializar en un valor adecuado
        Dim x As Post = Nothing ' TODO: Inicializar en un valor adecuado
        Dim L As List(Of Pos_TipoTarjeta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.UpdateT_AddListPos_TipoTarjeta(x, L, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
