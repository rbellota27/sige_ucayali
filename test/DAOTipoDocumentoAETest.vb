﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTipoDocumentoAETest y se pretende que
'''contenga todas las pruebas unitarias DAOTipoDocumentoAETest.
'''</summary>
<TestClass()> _
Public Class DAOTipoDocumentoAETest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTipoDocumentoAE
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTipoDocumentoAEConstructorTest()
        Dim target As DAOTipoDocumentoAE = New DAOTipoDocumentoAE()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaTipoDocumentoAE
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaTipoDocumentoAETest()
        Dim target As DAOTipoDocumentoAE = New DAOTipoDocumentoAE() ' TODO: Inicializar en un valor adecuado
        Dim TipoDocumentoAE As TipoDocumentoAE = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ActualizaTipoDocumentoAE(TipoDocumentoAE)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de EliminarxEmpresaxAreaxTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub EliminarxEmpresaxAreaxTipoDocumentoTest()
        Dim target As DAOTipoDocumentoAE = New DAOTipoDocumentoAE() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.EliminarxEmpresaxAreaxTipoDocumento(IdEmpresa, IdArea, IdTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaTipoDocumentoAE
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTipoDocumentoAETest()
        Dim target As DAOTipoDocumentoAE = New DAOTipoDocumentoAE() ' TODO: Inicializar en un valor adecuado
        Dim TipoDocumentoAE As TipoDocumentoAE = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.InsertaTipoDocumentoAE(TipoDocumentoAE)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllTipoDocumentoAE
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTipoDocumentoAETest()
        Dim target As DAOTipoDocumentoAE = New DAOTipoDocumentoAE() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumentoAE) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumentoAE)
        actual = target.SelectAllTipoDocumentoAE
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdEmpresaxIdArea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdEmpresaxIdAreaTest()
        Dim target As DAOTipoDocumentoAE = New DAOTipoDocumentoAE() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectCboxIdEmpresaxIdArea(IdEmpresa, IdArea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectValoresxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectValoresxParamsTest()
        Dim target As DAOTipoDocumentoAE = New DAOTipoDocumentoAE() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoDocumentoAE = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoDocumentoAE
        actual = target.SelectValoresxParams(IdEmpresa, IdTipoDocumento, IdMonedaDestino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIds
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdsTest()
        Dim target As DAOTipoDocumentoAE = New DAOTipoDocumentoAE() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoDocumentoAE = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoDocumentoAE
        actual = target.SelectxIds(IdEmpresa, IdTipoDocumento, IdArea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de TipoDocumentoAEBuscarxIdArea
    '''</summary>
    <TestMethod()> _
    Public Sub TipoDocumentoAEBuscarxIdAreaTest()
        Dim target As DAOTipoDocumentoAE = New DAOTipoDocumentoAE() ' TODO: Inicializar en un valor adecuado
        Dim IdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumentoAE) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumentoAE)
        actual = target.TipoDocumentoAEBuscarxIdArea(IdArea, IdEmpresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
