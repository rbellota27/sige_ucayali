﻿Imports System.Data.SqlClient

Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOUtilTest y se pretende que
'''contenga todas las pruebas unitarias DAOUtilTest.
'''</summary>
<TestClass()> _
Public Class DAOUtilTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOUtil
    '''</summary>
    <TestMethod()> _
    Public Sub DAOUtilConstructorTest()
        Dim target As DAOUtil = New DAOUtil()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de CalcularValorxTipoCambio
    '''</summary>
    <TestMethod()> _
    Public Sub CalcularValorxTipoCambioTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim valor As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaOrigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.CalcularValorxTipoCambio(valor, IdMonedaOrigen, IdMonedaDestino, codigo, fecha)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Fn_getStockTransito
    '''</summary>
    <TestMethod()> _
    Public Sub Fn_getStockTransitoTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.Fn_getStockTransito(IdAlmacen, IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectValorxIdMonedaOxIdMonedaD
    '''</summary>
    <TestMethod()> _
    Public Sub SelectValorxIdMonedaOxIdMonedaDTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaOrigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim ValorAConvertir As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CodigoManejo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectValorxIdMonedaOxIdMonedaD(IdMonedaOrigen, IdMonedaDestino, ValorAConvertir, cn, CodigoManejo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarExistenciaxTablax1Campo
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarExistenciaxTablax1CampoTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TR As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim nomTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarExistenciaxTablax1Campo(cn, TR, nomTabla, nomCampo, valor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarExistenciaxTablax1Campo
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarExistenciaxTablax1CampoTest1()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim nomTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarExistenciaxTablax1Campo(nomTabla, nomCampo, valor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarExistenciaxTablax2CamposForUpdate
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarExistenciaxTablax2CamposForUpdateTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim nomTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idCampo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idValor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarExistenciaxTablax2CamposForUpdate(nomTabla, nomCampo, valor, idCampo, idValor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarExistenciaxTablax3Campos
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarExistenciaxTablax3CamposTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim nomTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo3 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor3 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarExistenciaxTablax3Campos(nomTabla, nomCampo1, valor1, nomCampo2, valor2, nomCampo3, valor3)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarExistenciaxTablax3Campos
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarExistenciaxTablax3CamposTest1()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim nomTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo3 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor3 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarExistenciaxTablax3Campos(nomTabla, nomCampo1, valor1, nomCampo2, valor2, nomCampo3, valor3, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarExistenciaxTablax4Campos
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarExistenciaxTablax4CamposTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim nomTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo3 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor3 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo4 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor4 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarExistenciaxTablax4Campos(nomTabla, nomCampo1, valor1, nomCampo2, valor2, nomCampo3, valor3, nomCampo4, valor4, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarExistenciaxTablax4Campos
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarExistenciaxTablax4CamposTest1()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim nomTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo3 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor3 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo4 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor4 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarExistenciaxTablax4Campos(nomTabla, nomCampo1, valor1, nomCampo2, valor2, nomCampo3, valor3, nomCampo4, valor4)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarTiendaxUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarTiendaxUsuarioTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.ValidarTiendaxUsuario(cn, idUsuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarxDosParametros
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarxDosParametrosTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim nomTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarxDosParametros(nomTabla, nomCampo1, valor1, nomCampo2, valor2)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarxDosParametros
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarxDosParametrosTest1()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim nomTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarxDosParametros(cn, tr, nomTabla, nomCampo1, valor1, nomCampo2, valor2)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarxDosParametrosEnteros
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarxDosParametrosEnterosTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim nomTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nomCampo1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor1 As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nomCampo2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim valor2 As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarxDosParametrosEnteros(nomTabla, nomCampo1, valor1, nomCampo2, valor2)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fn_ProductoMedida
    '''</summary>
    <TestMethod()> _
    Public Sub fn_ProductoMedidaTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMagnitud As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUmMagnitudDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Valor As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.fn_ProductoMedida(IdProducto, IdUnidadMedida, IdMagnitud, IdUmMagnitudDestino, Valor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProductoTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(IdEmpresa, IdAlmacen, IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_StockDisponible
    '''</summary>
    <TestMethod()> _
    Public Sub fx_StockDisponibleTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.fx_StockDisponible(IdEmpresa, IdAlmacen, IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_StockReal
    '''</summary>
    <TestMethod()> _
    Public Sub fx_StockRealTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.fx_StockReal(IdEmpresa, IdAlmacen, IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_StockRealxIdEmpresaxIdAlmacenxIdProductoxFechaFin
    '''</summary>
    <TestMethod()> _
    Public Sub fx_StockRealxIdEmpresaxIdAlmacenxIdProductoxFechaFinTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.fx_StockRealxIdEmpresaxIdAlmacenxIdProductoxFechaFin(IdEmpresa, IdAlmacen, IdProducto, fechaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_getCantidadMovAlmacenxIdDetalleDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub fx_getCantidadMovAlmacenxIdDetalleDocumentoTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim IdDetalleDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.fx_getCantidadMovAlmacenxIdDetalleDocumento(IdDetalleDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_getCantidadxTono
    '''</summary>
    <TestMethod()> _
    Public Sub fx_getCantidadxTonoTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTono As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.fx_getCantidadxTono(idProducto, idTono)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_getCostoProducto
    '''</summary>
    <TestMethod()> _
    Public Sub fx_getCostoProductoTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.fx_getCostoProducto(IdEmpresa, IdAlmacen, IdProducto, fechaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_getMagnitudProducto
    '''</summary>
    <TestMethod()> _
    Public Sub fx_getMagnitudProductoTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUMProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Cantidad As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim IdMagnitud As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUMMagnitud_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.fx_getMagnitudProducto(IdProducto, IdUMProducto, Cantidad, IdMagnitud, IdUMMagnitud_Destino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_getParamVerInterfaceMaestroObra
    '''</summary>
    <TestMethod()> _
    Public Sub fx_getParamVerInterfaceMaestroObraTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim idparam As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.fx_getParamVerInterfaceMaestroObra(idparam)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_getValorEquivalenteProducto
    '''</summary>
    <TestMethod()> _
    Public Sub fx_getValorEquivalenteProductoTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUM_Origen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUM_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Cantidad As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.fx_getValorEquivalenteProducto(IdProducto, IdUM_Origen, IdUM_Destino, Cantidad)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fx_getValorParametroGeneralxIdparam
    '''</summary>
    <TestMethod()> _
    Public Sub fx_getValorParametroGeneralxIdparamTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim idparam As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.fx_getValorParametroGeneralxIdparam(idparam)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de valDocComprometerStock
    '''</summary>
    <TestMethod()> _
    Public Sub valDocComprometerStockTest()
        Dim target As DAOUtil = New DAOUtil() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.valDocComprometerStock(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
