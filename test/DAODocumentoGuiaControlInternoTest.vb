﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoGuiaControlInternoTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoGuiaControlInternoTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoGuiaControlInternoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoGuiaControlInterno
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoGuiaControlInternoConstructorTest()
        Dim target As DAODocumentoGuiaControlInterno = New DAODocumentoGuiaControlInterno()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaControlInterno_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaControlInterno_DeshacerMovTest()
        Dim target As DAODocumentoGuiaControlInterno = New DAODocumentoGuiaControlInterno() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovAlmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePuntoPartida As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePuntoLlegada As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteRelacionDoc As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoGuiaControlInterno_DeshacerMov(IdDocumento, DeleteDetalleDocumento, DeleteMovAlmacen, DeletePuntoPartida, DeletePuntoLlegada, DeleteObservacion, deleteRelacionDoc, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaControlInterno_SelectDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaControlInterno_SelectDetalleTest()
        Dim target As DAODocumentoGuiaControlInterno = New DAODocumentoGuiaControlInterno() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DocumentoGuiaControlInterno_SelectDetalle(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
