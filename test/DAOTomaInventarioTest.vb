﻿Imports System.Data

Imports System

Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTomaInventarioTest y se pretende que
'''contenga todas las pruebas unitarias DAOTomaInventarioTest.
'''</summary>
<TestClass()> _
Public Class DAOTomaInventarioTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTomaInventario
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTomaInventarioConstructorTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentTomaInventarioAnularxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentTomaInventarioAnularxIdDocumentoTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentTomaInventarioAnularxIdDocumento(IdDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoAjusteInventarioAnularxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoAjusteInventarioAnularxIdDocumentoTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DocumentoAjusteInventarioAnularxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoTomaInventarioSelectxIdAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoTomaInventarioSelectxIdAlmacenTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.DocumentoTomaInventarioSelectxIdAlmacen(IdAlmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoTomaInventario_CargarProductoDetallexParams
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoTomaInventario_CargarProductoDetallexParamsTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaTomaInv As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DocumentoTomaInventario_CargarProductoDetallexParams(IdEmpresa, IdAlmacen, IdLinea, IdSubLinea, IdProducto, IdDocumento, FechaTomaInv, PageIndex, PageSize, CodigoProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoTomaInventario_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoTomaInventario_DeshacerMovTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDocumento_Persona As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoTomaInventario_DeshacerMov(IdDocumento, DeleteDetalleDocumento, DeleteRelacionDocumento, DeleteDocumento_Persona, DeleteObservacion, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoTomaInventario_FiltroDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoTomaInventario_FiltroDetalleTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodigoSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Producto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdtipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DocumentoTomaInventario_FiltroDetalle(IdDocumento, IdLinea, IdSubLinea, CodigoSubLinea, Producto, CodigoProducto, IdtipoExistencia, pageIndex, pageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoTomaInventario_FiltroDetalle_Consolidado
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoTomaInventario_FiltroDetalle_ConsolidadoTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodigoSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Producto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdtipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.DocumentoTomaInventario_FiltroDetalle_Consolidado(IdDocumento, IdLinea, IdSubLinea, CodigoSubLinea, Producto, CodigoProducto, IdtipoExistencia, pageIndex, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoTomaInventario_GenerarConsolidado_DetalleInsert
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoTomaInventario_GenerarConsolidado_DetalleInsertTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoTomaInventario_GenerarConsolidado_DetalleInsert(IdDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoTomaInventario_RegistrarDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoTomaInventario_RegistrarDetalleTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim objDetalleDocumento As DetalleDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoTomaInventario_RegistrarDetalle(objDetalleDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoTomaInventario_SelectDocRef
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoTomaInventario_SelectDocRefTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoTomaInventario_SelectDocRef(IdEmpresa, IdTienda, IdAlmacen, Serie, Codigo, FechaInicio, FechaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de EliminarProcesoxIdProductoxIdDocumentoAjusteInv
    '''</summary>
    <TestMethod()> _
    Public Sub EliminarProcesoxIdProductoxIdDocumentoAjusteInvTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocAjusteInv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.EliminarProcesoxIdProductoxIdDocumentoAjusteInv(IdDocAjusteInv, IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GenerarDocTomaInventario
    '''</summary>
    <TestMethod()> _
    Public Sub GenerarDocTomaInventarioTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.GenerarDocTomaInventario(IdDocumento, IdEmpresa, IdAlmacen, IdLinea, IdSubLinea, FechaFin, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDetalleDocumentoAjusteInventario
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDetalleDocumentoAjusteInventarioTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim detalledocumento As DetalleDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaDetalleDocumentoAjusteInventario(cn, detalledocumento, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RecalcularSaldosDocTomaInv
    '''</summary>
    <TestMethod()> _
    Public Sub RecalcularSaldosDocTomaInvTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim NroDias_Add As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.RecalcularSaldosDocTomaInv(IdDocumento, FechaFin, NroDias_Add)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDetallexIdDocumentoxIdSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDetallexIdDocumentoxIdSubLineaTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.SelectDetallexIdDocumentoxIdSubLinea(IdDocumento, IdSubLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDetallexIdDocumentoxIdSubLinea_Ajuste
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDetallexIdDocumentoxIdSubLinea_AjusteTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoTomaInv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumentoTomaInventario) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumentoTomaInventario)
        actual = target.SelectDetallexIdDocumentoxIdSubLinea_Ajuste(IdDocumentoTomaInv, IdSubLinea, IdMonedaDestino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocAjusteInventarioxIdDocTomaInv
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocAjusteInventarioxIdDocTomaInvTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoTomaInv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectDocAjusteInventarioxIdDocTomaInv(IdDocumentoTomaInv)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocAjusteInventarioxIdSeriexCodigo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocAjusteInventarioxIdSeriexCodigoTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectDocAjusteInventarioxIdSeriexCodigo(IdSerie, Codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectLineaxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectLineaxIdDocumentoTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea)
        actual = target.SelectLineaxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectSubLineaxIdDocumentoxIdLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectSubLineaxIdDocumentoxIdLineaTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea)
        actual = target.SelectSubLineaxIdDocumentoxIdLinea(IdDocumento, IdLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdSeriexCodigo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdSeriexCodigoTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectxIdSeriexCodigo(IdSerie, codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_DocAjusteInventario
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_DocAjusteInventarioTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim opcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_DocAjusteInventario(IdDocumento, opcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_DocTomaInvFormato
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_DocTomaInvFormatoTest()
        Dim target As DAOTomaInventario = New DAOTomaInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim opcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_DocTomaInvFormato(IdDocumento, opcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
