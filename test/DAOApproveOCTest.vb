﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOApproveOCTest y se pretende que
'''contenga todas las pruebas unitarias DAOApproveOCTest.
'''</summary>
<TestClass()> _
Public Class DAOApproveOCTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOApproveOC
    '''</summary>
    <TestMethod()> _
    Public Sub DAOApproveOCConstructorTest()
        Dim target As DAOApproveOC = New DAOApproveOC()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de CancelarStockTransito
    '''</summary>
    <TestMethod()> _
    Public Sub CancelarStockTransitoTest()
        Dim target As DAOApproveOC = New DAOApproveOC() ' TODO: Inicializar en un valor adecuado
        Dim obj As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.CancelarStockTransito(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenCompra_Approve
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenCompra_ApproveTest()
        Dim target As DAOApproveOC = New DAOApproveOC() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Aprobacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Fechaini As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.OrdenCompra_Approve(IdEmpresa, IdTienda, IdArea, Aprobacion, IdSerie, Fechaini, Fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenCompra_MovCuentaPorPagar
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenCompra_MovCuentaPorPagarTest()
        Dim target As DAOApproveOC = New DAOApproveOC() ' TODO: Inicializar en un valor adecuado
        Dim obj As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.OrdenCompra_MovCuentaPorPagar(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
