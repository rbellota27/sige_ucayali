﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOUbicacionTest y se pretende que
'''contenga todas las pruebas unitarias DAOUbicacionTest.
'''</summary>
<TestClass()> _
Public Class DAOUbicacionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOUbicacion
    '''</summary>
    <TestMethod()> _
    Public Sub DAOUbicacionConstructorTest()
        Dim target As DAOUbicacion = New DAOUbicacion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaUbicacion
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaUbicacionTest()
        Dim target As DAOUbicacion = New DAOUbicacion() ' TODO: Inicializar en un valor adecuado
        Dim ubicacion As Ubicacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaUbicacion(ubicacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaUbicacion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaUbicacionTest()
        Dim target As DAOUbicacion = New DAOUbicacion() ' TODO: Inicializar en un valor adecuado
        Dim ubicacion As Ubicacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaUbicacion(ubicacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOUbicacion = New DAOUbicacion() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Ubicacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Ubicacion)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProductoTest()
        Dim target As DAOUbicacion = New DAOUbicacion() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Ubicacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Ubicacion)
        actual = target.SelectxIdProducto(IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdUbicacion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdUbicacionTest()
        Dim target As DAOUbicacion = New DAOUbicacion() ' TODO: Inicializar en un valor adecuado
        Dim IdUbicacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Ubicacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Ubicacion)
        actual = target.SelectxIdUbicacion(IdUbicacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
