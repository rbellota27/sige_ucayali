﻿Imports System.Data

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCostoFletexPesoTest y se pretende que
'''contenga todas las pruebas unitarias DAOCostoFletexPesoTest.
'''</summary>
<TestClass()> _
Public Class DAOCostoFletexPesoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCostoFletexPeso
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCostoFletexPesoConstructorTest()
        Dim target As DAOCostoFletexPeso = New DAOCostoFletexPeso()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de CostoFletexPeso_Delete
    '''</summary>
    <TestMethod()> _
    Public Sub CostoFletexPeso_DeleteTest()
        Dim target As DAOCostoFletexPeso = New DAOCostoFletexPeso() ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaOrigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.CostoFletexPeso_Delete(IdTiendaOrigen, IdTiendaDestino, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de CostoFletexPeso_Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub CostoFletexPeso_RegistrarTest()
        Dim target As DAOCostoFletexPeso = New DAOCostoFletexPeso() ' TODO: Inicializar en un valor adecuado
        Dim objCostoFletexPeso As CostoFletexPeso = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.CostoFletexPeso_Registrar(objCostoFletexPeso, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de CostoFletexPeso_SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub CostoFletexPeso_SelectxIdTest()
        Dim target As DAOCostoFletexPeso = New DAOCostoFletexPeso() ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaOrigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As CostoFletexPeso = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As CostoFletexPeso
        actual = target.CostoFletexPeso_SelectxId(IdTiendaOrigen, IdTiendaDestino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CostoFletexPeso_SelectxId_DT
    '''</summary>
    <TestMethod()> _
    Public Sub CostoFletexPeso_SelectxId_DTTest()
        Dim target As DAOCostoFletexPeso = New DAOCostoFletexPeso() ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaOrigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.CostoFletexPeso_SelectxId_DT(IdTiendaOrigen, IdTiendaDestino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
