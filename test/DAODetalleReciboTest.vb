﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODetalleReciboTest y se pretende que
'''contenga todas las pruebas unitarias DAODetalleReciboTest.
'''</summary>
<TestClass()> _
Public Class DAODetalleReciboTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODetalleRecibo
    '''</summary>
    <TestMethod()> _
    Public Sub DAODetalleReciboConstructorTest()
        Dim target As DAODetalleRecibo = New DAODetalleRecibo()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaDetalleRecibo
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaDetalleReciboTest()
        Dim target As DAODetalleRecibo = New DAODetalleRecibo() ' TODO: Inicializar en un valor adecuado
        Dim detallerecibo As DetalleRecibo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaDetalleRecibo(detallerecibo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleReciboSelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleReciboSelectxIdDocumentoTest()
        Dim target As DAODetalleRecibo = New DAODetalleRecibo() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleRecibo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleRecibo)
        actual = target.DetalleReciboSelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoReciboEgresoSelectDet
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoReciboEgresoSelectDetTest()
        Dim target As DAODetalleRecibo = New DAODetalleRecibo() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleReciboView_1) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleReciboView_1)
        actual = target.DocumentoReciboEgresoSelectDet(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoReciboIngresoSelectDet
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoReciboIngresoSelectDetTest()
        Dim target As DAODetalleRecibo = New DAODetalleRecibo() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleReciboView_1) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleReciboView_1)
        actual = target.DocumentoReciboIngresoSelectDet(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDetalleRecibo
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDetalleReciboTest()
        Dim target As DAODetalleRecibo = New DAODetalleRecibo() ' TODO: Inicializar en un valor adecuado
        Dim detallerecibo As DetalleRecibo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaDetalleRecibo(detallerecibo, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de fnInsTblDetalleRecibo
    '''</summary>
    <TestMethod()> _
    Public Sub fnInsTblDetalleReciboTest()
        Dim target As DAODetalleRecibo = New DAODetalleRecibo() ' TODO: Inicializar en un valor adecuado
        Dim poBETblDetalleRecibo As DetalleRecibo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.fnInsTblDetalleRecibo(poBETblDetalleRecibo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fnSelTblDetalleReciboDin
    '''</summary>
    <TestMethod()> _
    Public Sub fnSelTblDetalleReciboDinTest()
        Dim target As DAODetalleRecibo = New DAODetalleRecibo() ' TODO: Inicializar en un valor adecuado
        Dim psWhere As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim psOrder As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleRecibo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleRecibo)
        actual = target.fnSelTblDetalleReciboDin(psWhere, psOrder)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
