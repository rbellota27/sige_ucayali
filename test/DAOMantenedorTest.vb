﻿Imports System.Data

Imports System

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMantenedorTest y se pretende que
'''contenga todas las pruebas unitarias DAOMantenedorTest.
'''</summary>
<TestClass()> _
Public Class DAOMantenedorTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMantenedor
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMantenedorConstructorTest()
        Dim target As DAOMantenedor = New DAOMantenedor()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de QueryT
    '''</summary>
    <TestMethod()> _
    Public Sub QueryTTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim Prmx() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim modo As DAOMantenedor.modo_query = New DAOMantenedor.modo_query() ' TODO: Inicializar en un valor adecuado
        Dim sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.QueryT(Prmx, modo, sp, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UCBool
    '''</summary>
    <TestMethod()> _
    Public Sub UCBoolTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim obj As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UCBool(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UCDate
    '''</summary>
    <TestMethod()> _
    Public Sub UCDateTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim obj As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim actual As DateTime
        actual = target.UCDate(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UCDbl
    '''</summary>
    <TestMethod()> _
    Public Sub UCDblTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim obj As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Double = 0.0! ' TODO: Inicializar en un valor adecuado
        Dim actual As Double
        actual = target.UCDbl(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UCDec
    '''</summary>
    <TestMethod()> _
    Public Sub UCDecTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim obj As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.UCDec(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UCInt
    '''</summary>
    <TestMethod()> _
    Public Sub UCIntTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim obj As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.UCInt(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UCStr
    '''</summary>
    <TestMethod()> _
    Public Sub UCStrTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim obj As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.UCStr(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getParam
    '''</summary>
    <TestMethod()> _
    Public Sub getParamTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim a As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim parameterName As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim BDTipo As SqlDbType = New SqlDbType() ' TODO: Inicializar en un valor adecuado
        Dim expected As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As SqlParameter
        actual = target.getParam(a, parameterName, BDTipo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getParamValueChar
    '''</summary>
    <TestMethod()> _
    Public Sub getParamValueCharTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim a As Char = Global.Microsoft.VisualBasic.ChrW(0) ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.getParamValueChar(a)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getParamValueDate
    '''</summary>
    <TestMethod()> _
    Public Sub getParamValueDateTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim a As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.getParamValueDate(a)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getParamValueDec
    '''</summary>
    <TestMethod()> _
    Public Sub getParamValueDecTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim a As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.getParamValueDec(a)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getParamValueInt
    '''</summary>
    <TestMethod()> _
    Public Sub getParamValueIntTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim a As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.getParamValueInt(a)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getParamValueStr
    '''</summary>
    <TestMethod()> _
    Public Sub getParamValueStrTest()
        Dim target As DAOMantenedor = New DAOMantenedor() ' TODO: Inicializar en un valor adecuado
        Dim a As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.getParamValueStr(a)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
