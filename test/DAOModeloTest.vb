﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOModeloTest y se pretende que
'''contenga todas las pruebas unitarias DAOModeloTest.
'''</summary>
<TestClass()> _
Public Class DAOModeloTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOModelo
    '''</summary>
    <TestMethod()> _
    Public Sub DAOModeloConstructorTest()
        Dim target As DAOModelo = New DAOModelo()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaModelo
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaModeloTest()
        Dim target As DAOModelo = New DAOModelo() ' TODO: Inicializar en un valor adecuado
        Dim Modelo As Modelo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaModelo(Modelo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaModelo
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaModeloTest()
        Dim target As DAOModelo = New DAOModelo() ' TODO: Inicializar en un valor adecuado
        Dim Modelo As Modelo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaModelo(Modelo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoModelo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoModeloTest()
        Dim target As DAOModelo = New DAOModelo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Modelo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Modelo)
        actual = target.SelectAllActivoModelo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivoModelo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoModeloTest()
        Dim target As DAOModelo = New DAOModelo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Modelo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Modelo)
        actual = target.SelectAllInactivoModelo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllModelo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllModeloTest()
        Dim target As DAOModelo = New DAOModelo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Modelo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Modelo)
        actual = target.SelectAllModelo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAOModelo = New DAOModelo() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Modelo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Modelo)
        actual = target.SelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombreActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreActivoTest()
        Dim target As DAOModelo = New DAOModelo() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Modelo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Modelo)
        actual = target.SelectAllxNombreActivo(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombreInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreInactivoTest()
        Dim target As DAOModelo = New DAOModelo() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Modelo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Modelo)
        actual = target.SelectAllxNombreInactivo(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOModelo = New DAOModelo() ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Modelo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Modelo)
        actual = target.SelectxId(Codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
