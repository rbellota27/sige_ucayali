﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOSubLinea_TipoTablaValorTest y se pretende que
'''contenga todas las pruebas unitarias DAOSubLinea_TipoTablaValorTest.
'''</summary>
<TestClass()> _
Public Class DAOSubLinea_TipoTablaValorTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOSubLinea_TipoTablaValor
    '''</summary>
    <TestMethod()> _
    Public Sub DAOSubLinea_TipoTablaValorConstructorTest()
        Dim target As DAOSubLinea_TipoTablaValor = New DAOSubLinea_TipoTablaValor()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de AnularSubLineaTipoTablaValor
    '''</summary>
    <TestMethod()> _
    Public Sub AnularSubLineaTipoTablaValorTest()
        Dim target As DAOSubLinea_TipoTablaValor = New DAOSubLinea_TipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipotabla As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoTablaValor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.AnularSubLineaTipoTablaValor(idsublinea, idtipotabla, IdTipoTablaValor, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ConfigTablaValor_Paginado
    '''</summary>
    <TestMethod()> _
    Public Sub ConfigTablaValor_PaginadoTest()
        Dim target As DAOSubLinea_TipoTablaValor = New DAOSubLinea_TipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipoexistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea_TipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea_TipoTablaValor)
        actual = target.ConfigTablaValor_Paginado(idlinea, idsublinea, estado, idtipoexistencia, PageIndex, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GrabaSubLineaTipoTablaValor
    '''</summary>
    <TestMethod()> _
    Public Sub GrabaSubLineaTipoTablaValorTest()
        Dim target As DAOSubLinea_TipoTablaValor = New DAOSubLinea_TipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of SubLinea_TipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.GrabaSubLineaTipoTablaValor(lista, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxIdLineaxIdSubLineaxEstado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxIdLineaxIdSubLineaxEstadoTest()
        Dim target As DAOSubLinea_TipoTablaValor = New DAOSubLinea_TipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipoexistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea_TipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea_TipoTablaValor)
        actual = target.SelectAllxIdLineaxIdSubLineaxEstado(idlinea, idsublinea, estado, idtipoexistencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdLineaxIdSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdLineaxIdSubLineaTest()
        Dim target As DAOSubLinea_TipoTablaValor = New DAOSubLinea_TipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdtipoTabla As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea_TipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea_TipoTablaValor)
        actual = target.SelectxIdLineaxIdSubLinea(idlinea, idsublinea, IdtipoTabla, PageIndex, PageSize, Descripcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdLineaxIdSubLineaxIdTipoTabla
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdLineaxIdSubLineaxIdTipoTablaTest()
        Dim target As DAOSubLinea_TipoTablaValor = New DAOSubLinea_TipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipotabla As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea_TipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea_TipoTablaValor)
        actual = target.SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, idtipotabla, idproducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdLineaxIdSubLineaxIdTipoTabla
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdLineaxIdSubLineaxIdTipoTablaTest1()
        Dim target As DAOSubLinea_TipoTablaValor = New DAOSubLinea_TipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipotabla As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim textTabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea_TipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea_TipoTablaValor)
        actual = target.SelectxIdLineaxIdSubLineaxIdTipoTabla(idlinea, idsublinea, idtipotabla, textTabla)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdLineaxIdSubLineaxIdTipoTabla02
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdLineaxIdSubLineaxIdTipoTabla02Test()
        Dim target As DAOSubLinea_TipoTablaValor = New DAOSubLinea_TipoTablaValor() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipotabla As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea_TipoTablaValor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea_TipoTablaValor)
        actual = target.SelectxIdLineaxIdSubLineaxIdTipoTabla02(idlinea, idsublinea, idtipotabla, idproducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
