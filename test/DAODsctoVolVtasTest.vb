﻿Imports System.Data.SqlClient

Imports System

Imports System.Collections.Generic

Imports System.Data

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODsctoVolVtasTest y se pretende que
'''contenga todas las pruebas unitarias DAODsctoVolVtasTest.
'''</summary>
<TestClass()> _
Public Class DAODsctoVolVtasTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODsctoVolVtas
    '''</summary>
    <TestMethod()> _
    Public Sub DAODsctoVolVtasConstructorTest()
        Dim target As DAODsctoVolVtas = New DAODsctoVolVtas()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DsctoVolVtas_SelectD
    '''</summary>
    <TestMethod()> _
    Public Sub DsctoVolVtas_SelectDTest()
        Dim target As DAODsctoVolVtas = New DAODsctoVolVtas() ' TODO: Inicializar en un valor adecuado
        Dim objDscto As DsctoVolVtas = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DsctoVolVtas) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DsctoVolVtas)
        actual = target.DsctoVolVtas_SelectD(objDscto, tabla, PageIndex, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DsctoVolVtas_SelectG
    '''</summary>
    <TestMethod()> _
    Public Sub DsctoVolVtas_SelectGTest()
        Dim target As DAODsctoVolVtas = New DAODsctoVolVtas() ' TODO: Inicializar en un valor adecuado
        Dim objDscto As DsctoVolVtas = Nothing ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DsctoVolVtas) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DsctoVolVtas)
        actual = target.DsctoVolVtas_SelectG(objDscto, PageIndex, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DsctoVolVtas_SelectNew
    '''</summary>
    <TestMethod()> _
    Public Sub DsctoVolVtas_SelectNewTest()
        Dim target As DAODsctoVolVtas = New DAODsctoVolVtas() ' TODO: Inicializar en un valor adecuado
        Dim fecInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fecFinal As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim cantMinima As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cantMaxima As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Dscto As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Producto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DsctoVolVtas) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DsctoVolVtas)
        actual = target.DsctoVolVtas_SelectNew(fecInicio, fecFinal, cantMinima, cantMaxima, IdTienda, IdTipoPV, Dscto, IdLinea, IdSubLinea, IdProducto, CodigoProducto, Producto, tabla, PageIndex, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DsctoVolVtas_Transaction
    '''</summary>
    <TestMethod()> _
    Public Sub DsctoVolVtas_TransactionTest()
        Dim target As DAODsctoVolVtas = New DAODsctoVolVtas() ' TODO: Inicializar en un valor adecuado
        Dim objDsctoVolVtas As DsctoVolVtas = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlCN As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlTR As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DsctoVolVtas_Transaction(objDsctoVolVtas, sqlCN, sqlTR)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DsctoVolVtas_UpdateEst
    '''</summary>
    <TestMethod()> _
    Public Sub DsctoVolVtas_UpdateEstTest()
        Dim target As DAODsctoVolVtas = New DAODsctoVolVtas() ' TODO: Inicializar en un valor adecuado
        Dim objDsctoVolVtas As DsctoVolVtas = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlCN As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlTR As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DsctoVolVtas_UpdateEst(objDsctoVolVtas, sqlCN, sqlTR)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DsctoVolVtas_UpdateEstG
    '''</summary>
    <TestMethod()> _
    Public Sub DsctoVolVtas_UpdateEstGTest()
        Dim target As DAODsctoVolVtas = New DAODsctoVolVtas() ' TODO: Inicializar en un valor adecuado
        Dim objDsctoVolVtas As DsctoVolVtas = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlCN As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlTR As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DsctoVolVtas_UpdateEstG(objDsctoVolVtas, sqlCN, sqlTR)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Facturacion
    '''</summary>
    <TestMethod()> _
    Public Sub FacturacionTest()
        Dim target As DAODsctoVolVtas = New DAODsctoVolVtas() ' TODO: Inicializar en un valor adecuado
        Dim objDscto As DsctoVolVtas = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DsctoVolVtas) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DsctoVolVtas)
        actual = target.Facturacion(objDscto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
