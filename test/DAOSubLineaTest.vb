﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOSubLineaTest y se pretende que
'''contenga todas las pruebas unitarias DAOSubLineaTest.
'''</summary>
<TestClass()> _
Public Class DAOSubLineaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub DAOSubLineaConstructorTest()
        Dim target As DAOSubLinea = New DAOSubLinea()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaSubLineaTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sublinea As SubLinea = Nothing ' TODO: Inicializar en un valor adecuado
        target.ActualizaSubLinea(cn, tr, sublinea)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaSubLineaT
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaSubLineaTTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim objSubLinea As SubLinea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaRegimen As List(Of ProductoRegimen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaSubLineaT(objSubLinea, listaRegimen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaSubLineaTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sublinea As SubLinea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaSubLinea(cn, tr, sublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaSubLineaT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaSubLineaTTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim objSubLinea As SubLinea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaRegimen As List(Of ProductoRegimen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaSubLineaT(objSubLinea, listaRegimen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxLineaxTipoExistencia
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxLineaxTipoExistenciaTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim idLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTipoEx As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea)
        actual = target.SelectActivoxLineaxTipoExistencia(idLinea, idTipoEx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxLineaxTipoExistenciaCodigo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxLineaxTipoExistenciaCodigoTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim idLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTipoEx As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea)
        actual = target.SelectActivoxLineaxTipoExistenciaCodigo(idLinea, idTipoEx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoxLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoxLineaTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim idLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea)
        actual = target.SelectAllActivoxLinea(idLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea)
        actual = target.SelectCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdLineaTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim idLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea)
        actual = target.SelectCboxIdLinea(idLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCodigoxIdSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCodigoxIdSubLineaTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.SelectCodigoxIdSubLinea(IdSubLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectIdxCodigo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectIdxCodigoTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectIdxCodigo(Codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdLineaxEstado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdLineaxEstadoTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SubLinea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SubLinea)
        actual = target.SelectxIdLineaxEstado(idlinea, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdSubLineaTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As SubLinea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As SubLinea
        actual = target.SelectxIdSubLinea(idsublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidadLongitud
    '''</summary>
    <TestMethod()> _
    Public Sub ValidadLongitudTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim expected As SubLinea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As SubLinea
        actual = target.ValidadLongitud
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidadNombrexIdLinea
    '''</summary>
    <TestMethod()> _
    Public Sub ValidadNombrexIdLineaTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As SubLinea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As SubLinea
        actual = target.ValidadNombrexIdLinea(idlinea, nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fn_TotalPag_LineaxSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub fn_TotalPag_LineaxSubLineaTest()
        Dim target As DAOSubLinea = New DAOSubLinea() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.fn_TotalPag_LineaxSubLinea(IdLinea, IdSubLinea, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
