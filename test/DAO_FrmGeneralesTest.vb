﻿Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAO_FrmGeneralesTest y se pretende que
'''contenga todas las pruebas unitarias DAO_FrmGeneralesTest.
'''</summary>
<TestClass()> _
Public Class DAO_FrmGeneralesTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAO_FrmGenerales
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_FrmGeneralesConstructorTest()
        Dim target As DAO_FrmGenerales = New DAO_FrmGenerales()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de cls_frmCancelarProvisiones
    '''</summary>
    <TestMethod()> _
    Public Sub cls_frmCancelarProvisionesTest()
        Dim target As DAO_FrmGenerales = New DAO_FrmGenerales() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As be_frmCargaPrincipal = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As be_frmCargaPrincipal
        actual = target.cls_frmCancelarProvisiones(cn, idUsuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de validarEditar
    '''</summary>
    <TestMethod()> _
    Public Sub validarEditarTest()
        Dim target As DAO_FrmGenerales = New DAO_FrmGenerales() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.validarEditar(cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
