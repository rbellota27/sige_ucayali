﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCondicionComercialTest y se pretende que
'''contenga todas las pruebas unitarias DAOCondicionComercialTest.
'''</summary>
<TestClass()> _
Public Class DAOCondicionComercialTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCondicionComercial
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCondicionComercialConstructorTest()
        Dim target As DAOCondicionComercial = New DAOCondicionComercial()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaCondicionComercial
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaCondicionComercialTest()
        Dim target As DAOCondicionComercial = New DAOCondicionComercial() ' TODO: Inicializar en un valor adecuado
        Dim condicioncomercial As CondicionComercial = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaCondicionComercial(condicioncomercial)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaCondicionComercial
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaCondicionComercialTest()
        Dim target As DAOCondicionComercial = New DAOCondicionComercial() ' TODO: Inicializar en un valor adecuado
        Dim condicioncomercial As CondicionComercial = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaCondicionComercial(condicioncomercial)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNombreTest()
        Dim target As DAOCondicionComercial = New DAOCondicionComercial() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CondicionComercial) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CondicionComercial)
        actual = target.SelectActivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOCondicionComercial = New DAOCondicionComercial() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CondicionComercial) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CondicionComercial)
        actual = target.SelectAll(idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOCondicionComercial = New DAOCondicionComercial() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CondicionComercial) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CondicionComercial)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllCondicionCoimercial
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllCondicionCoimercialTest()
        Dim target As DAOCondicionComercial = New DAOCondicionComercial() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CondicionComercial) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CondicionComercial)
        actual = target.SelectAllCondicionCoimercial
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOCondicionComercial = New DAOCondicionComercial() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CondicionComercial) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CondicionComercial)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAOCondicionComercial = New DAOCondicionComercial() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CondicionComercial) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CondicionComercial)
        actual = target.SelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxNombreTest()
        Dim target As DAOCondicionComercial = New DAOCondicionComercial() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CondicionComercial) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CondicionComercial)
        actual = target.SelectInactivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOCondicionComercial = New DAOCondicionComercial() ' TODO: Inicializar en un valor adecuado
        Dim id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CondicionComercial) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CondicionComercial)
        actual = target.SelectxId(id)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdCondicionComercial
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdCondicionComercialTest()
        Dim target As DAOCondicionComercial = New DAOCondicionComercial() ' TODO: Inicializar en un valor adecuado
        Dim id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As CondicionComercial = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As CondicionComercial
        actual = target.SelectxIdCondicionComercial(id)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
