﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTipoDocumentoTest y se pretende que
'''contenga todas las pruebas unitarias DAOTipoDocumentoTest.
'''</summary>
<TestClass()> _
Public Class DAOTipoDocumentoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTipoDocumentoConstructorTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaTipoDocumentoTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim tipodocumento As TipoDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaTipoDocumento(tipodocumento, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTipoDocumentoTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim tipodocumento As TipoDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaTipoDocumento(tipodocumento, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNombreTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectActivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboTipoDocumentoIndep
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTipoDocumentoIndepTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectCboTipoDocumentoIndep
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboTipoDocumentoProgPago
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTipoDocumentoProgPagoTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectCboTipoDocumentoProgPago
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboTipoDocumentosCaja
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTipoDocumentosCajaTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectCboTipoDocumentosCaja
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboTipoOperacion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTipoOperacionTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento_TipoOperacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento_TipoOperacion)
        actual = target.SelectCboTipoOperacion
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdEmpresaxIdArea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdEmpresaxIdAreaTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectCboxIdEmpresaxIdArea(IdEmpresa, IdArea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectExportarContaxIdTiPoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectExportarContaxIdTiPoDocumentoTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.SelectExportarContaxIdTiPoDocumento(IdTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxNombreTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectInactivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectTipoDocumentoExternoActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectTipoDocumentoExternoActivoTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectTipoDocumentoExternoActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectTipoDocumentoxIdTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectTipoDocumentoxIdTipoDocumentoTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectTipoDocumentoxIdTipoDocumento(IdTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxCodSunat
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxCodSunatTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim codSunat As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectxCodSunat(codSunat)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim idTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoDocumento
        actual = target.SelectxId(idTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getListTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub getListTipoDocumentoTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim lector As SqlDataReader = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.getListTipoDocumento(lector)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub getTipoDocumentoTest()
        Dim target As DAOTipoDocumento = New DAOTipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim lector As SqlDataReader = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoDocumento
        actual = target.getTipoDocumento(lector)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
