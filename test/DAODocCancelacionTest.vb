﻿Imports System.Data

Imports System

Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocCancelacionTest y se pretende que
'''contenga todas las pruebas unitarias DAODocCancelacionTest.
'''</summary>
<TestClass()> _
Public Class DAODocCancelacionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocCancelacion
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocCancelacionConstructorTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoReciboEgreso_UpdateMontoxSustentar
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoReciboEgreso_UpdateMontoxSustentarTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRG As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoReciboEgreso_UpdateMontoxSustentar(IdDocumentoRG, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Insert
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim objDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalleConcepto As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaCancelacion As List(Of DatosCancelacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObservaciones As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaMovBanco As List(Of MovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objMovCaja As MovCaja = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaPagoCaja As List(Of PagoCaja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objAnexoDocumento As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaRelacionDocumento As List(Of RelacionDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objAsientoContable As be_asientoContable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        'actual = target.Insert(objDocumento, listaDetalleConcepto, listaCancelacion, objObservaciones, listaMovBanco, objMovCaja, listaPagoCaja, objAnexoDocumento, listaRelacionDocumento, objAsientoContable)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertDetalleConcepto_Cancelacion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertDetalleConcepto_CancelacionTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim obj As DetalleConcepto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertDetalleConcepto_Cancelacion(obj, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertDetalleConcepto_Cancelacion_v2
    '''</summary>
    <TestMethod()> _
    Public Sub InsertDetalleConcepto_Cancelacion_v2Test()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim obj As DetalleConcepto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertDetalleConcepto_Cancelacion_v2(obj, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertListDetalleConceptoxIdDocumento_Cancelacion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertListDetalleConceptoxIdDocumento_CancelacionTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim list As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertListDetalleConceptoxIdDocumento_Cancelacion(list, IdDocumento, cnx, trx)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertListDetalleConceptoxIdDocumento_Cancelacion_v2
    '''</summary>
    <TestMethod()> _
    Public Sub InsertListDetalleConceptoxIdDocumento_Cancelacion_v2Test()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim list As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertListDetalleConceptoxIdDocumento_Cancelacion_v2(list, IdDocumento, cnx, trx)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Sustento_Liq_SelectxParams_DT
    '''</summary>
    <TestMethod()> _
    Public Sub Sustento_Liq_SelectxParams_DTTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Opcion_Sustento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Opcion_Liq As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.Sustento_Liq_SelectxParams_DT(IdEmpresa, IdTienda, IdCaja, Opcion_Sustento, Opcion_Liq, FechaInicio, FechaFin, IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Update
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim objDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalleConcepto As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaCancelacion As List(Of DatosCancelacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObservaciones As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaMovBanco As List(Of MovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objMovCaja As MovCaja = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaPagoCaja As List(Of PagoCaja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objAnexoDocumento As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaRelacionDocumento As List(Of RelacionDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Update(objDocumento, listaDetalleConcepto, listaCancelacion, objObservaciones, listaMovBanco, objMovCaja, listaPagoCaja, objAnexoDocumento, listaRelacionDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateDocumentoSustento
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateDocumentoSustentoTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim list As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateDocumentoSustento(list, IdDocumento, cnx, trx)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateDocumentoSustentoMovCuenta
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateDocumentoSustentoMovCuentaTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim obj As MovCuentaPorPagar = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateDocumentoSustentoMovCuenta(obj, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateDocumentoSustentoMovCuenta
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateDocumentoSustentoMovCuentaTest1()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim list As List(Of MovCuentaPorPagar) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateDocumentoSustentoMovCuenta(list, IdDocumento, cnx, trx)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateDocumentoSustentox
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateDocumentoSustentoxTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim obj As DetalleConcepto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateDocumentoSustentox(obj, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateMontoxSustentar_Liquidacion
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateMontoxSustentar_LiquidacionTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim MontoxSustentar As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim Liquidado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateMontoxSustentar_Liquidacion(IdDocumento, MontoxSustentar, Liquidado, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateSustentoxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateSustentoxIdDocumentoTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim obj As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateSustentoxIdDocumento(obj)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de insertarAsientoContable
    '''</summary>
    <TestMethod()> _
    Public Sub insertarAsientoContableTest()
        Dim target As DAODocCancelacion = New DAODocCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objAsientoContable As be_asientoContable = Nothing ' TODO: Inicializar en un valor adecuado
        'target.insertarAsientoContable(cn, tr, objAsientoContable)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub
End Class
