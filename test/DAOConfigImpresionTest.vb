﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOConfigImpresionTest y se pretende que
'''contenga todas las pruebas unitarias DAOConfigImpresionTest.
'''</summary>
<TestClass()> _
Public Class DAOConfigImpresionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOConfigImpresion
    '''</summary>
    <TestMethod()> _
    Public Sub DAOConfigImpresionConstructorTest()
        Dim target As DAOConfigImpresion = New DAOConfigImpresion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de InsertConfigImpresion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertConfigImpresionTest()
        Dim target As DAOConfigImpresion = New DAOConfigImpresion() ' TODO: Inicializar en un valor adecuado
        Dim objImp As DocsImpresionView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertConfigImpresion(objImp)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOConfigImpresion = New DAOConfigImpresion() ' TODO: Inicializar en un valor adecuado
        Dim idTipodocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTipoimpresion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocsImpresionView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocsImpresionView)
        actual = target.SelectAll(idTipodocumento, idTipoimpresion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de llenaCbo
    '''</summary>
    <TestMethod()> _
    Public Sub llenaCboTest()
        Dim target As DAOConfigImpresion = New DAOConfigImpresion() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocsImpresionView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocsImpresionView)
        actual = target.llenaCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
