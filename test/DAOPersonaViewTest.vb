﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOPersonaViewTest y se pretende que
'''contenga todas las pruebas unitarias DAOPersonaViewTest.
'''</summary>
<TestClass()> _
Public Class DAOPersonaViewTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOPersonaView
    '''</summary>
    <TestMethod()> _
    Public Sub DAOPersonaViewConstructorTest()
        Dim target As DAOPersonaView = New DAOPersonaView()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNombreTest()
        Dim target As DAOPersonaView = New DAOPersonaView() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectActivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxParam
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxParamTest()
        Dim target As DAOPersonaView = New DAOPersonaView() ' TODO: Inicializar en un valor adecuado
        Dim texto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim opcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectActivoxParam(texto, opcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxParam_Paginado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxParam_PaginadoTest()
        Dim target As DAOPersonaView = New DAOPersonaView() ' TODO: Inicializar en un valor adecuado
        Dim texto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim opcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageNumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectActivoxParam_Paginado(texto, opcion, PageSize, PageNumber)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxParam_PaginadoUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxParam_PaginadoUsuarioTest()
        Dim target As DAOPersonaView = New DAOPersonaView() ' TODO: Inicializar en un valor adecuado
        Dim texto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim opcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageNumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectActivoxParam_PaginadoUsuario(texto, opcion, PageSize, PageNumber)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxPersonaRucDni
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxPersonaRucDniTest()
        Dim target As DAOPersonaView = New DAOPersonaView() ' TODO: Inicializar en un valor adecuado
        Dim texto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ruc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim opcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageNumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectActivoxPersonaRucDni(texto, Ruc, dni, opcion, PageSize, PageNumber)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxPersonaRucDniDetDoc
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxPersonaRucDniDetDocTest()
        Dim target As DAOPersonaView = New DAOPersonaView() ' TODO: Inicializar en un valor adecuado
        Dim texto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim opcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageNumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.SelectActivoxPersonaRucDniDetDoc(texto, opcion, PageSize, PageNumber)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPersonaTest()
        Dim target As DAOPersonaView = New DAOPersonaView() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As PersonaView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As PersonaView
        actual = target.SelectxIdPersona(idpersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPersonaMaestros
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPersonaMaestrosTest()
        Dim target As DAOPersonaView = New DAOPersonaView() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As PersonaView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As PersonaView
        actual = target.SelectxIdPersonaMaestros(idpersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
