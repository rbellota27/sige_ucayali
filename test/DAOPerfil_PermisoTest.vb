﻿Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOPerfil_PermisoTest y se pretende que
'''contenga todas las pruebas unitarias DAOPerfil_PermisoTest.
'''</summary>
<TestClass()> _
Public Class DAOPerfil_PermisoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOPerfil_Permiso
    '''</summary>
    <TestMethod()> _
    Public Sub DAOPerfil_PermisoConstructorTest()
        Dim target As DAOPerfil_Permiso = New DAOPerfil_Permiso()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdPerfilxIdArea
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdPerfilxIdAreaTest()
        Dim target As DAOPerfil_Permiso = New DAOPerfil_Permiso() ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexIdPerfilxIdArea(IdPerfil, IdArea, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaPerfil_Permiso
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaPerfil_PermisoTest()
        Dim target As DAOPerfil_Permiso = New DAOPerfil_Permiso() ' TODO: Inicializar en un valor adecuado
        Dim objPerfil_Permiso As Perfil_Permiso = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaPerfil_Permiso(objPerfil_Permiso, IdPerfil, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub
End Class
