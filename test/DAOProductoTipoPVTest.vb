﻿Imports System.Data

Imports System

Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOProductoTipoPVTest y se pretende que
'''contenga todas las pruebas unitarias DAOProductoTipoPVTest.
'''</summary>
<TestClass()> _
Public Class DAOProductoTipoPVTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOProductoTipoPV
    '''</summary>
    <TestMethod()> _
    Public Sub DAOProductoTipoPVConstructorTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaProductoTipoPV
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaProductoTipoPVTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim productotipopv As ProductoTipoPV = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.ActualizaProductoTipoPV(productotipopv, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdTiendaxIdProdxIdTipoPV
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdTiendaxIdProdxIdTipoPVTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdTiendaxIdProdxIdTipoPV(cn, tr, idproducto, idtienda, idtipopv)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdTiendaxIdProdxIdUMxIdTipoPV
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdTiendaxIdProdxIdUMxIdTipoPVTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objProductoTipoPV As ProductoTipoPV = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdTiendaxIdProdxIdUMxIdTipoPV(cn, tr, objProductoTipoPV)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaProductoTipoPV
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaProductoTipoPVTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim productotipopv As ProductoTipoPV = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CambiarxEquivalencia As Boolean = False ' TODO: Inicializar en un valor adecuado
        target.InsertaProductoTipoPV(cn, tr, productotipopv, CambiarxEquivalencia)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaProductoTipoPVT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaProductoTipoPVTTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of ProductoTipoPV) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CambiarxEquivalencia As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaProductoTipoPVT(lista, CambiarxEquivalencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaProductoTipoPVTxProducto
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaProductoTipoPVTxProductoTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of ProductoTipoPV) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaProductoTipoPVTxProducto(lista, idproducto, idtienda, idtipopv)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ProductoPrecioV
    '''</summary>
    <TestMethod()> _
    Public Sub ProductoPrecioVTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaOrigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV_Origen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PorcentPV As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim addCostoFletexPeso As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoTipoPV) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoTipoPV)
        actual = target.ProductoPrecioV(IdTiendaOrigen, IdTiendaDestino, IdTipoPV_Origen, IdTipoPV_Destino, IdProducto, PorcentPV, addCostoFletexPeso)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ProductoTipoPV_ReplicarPrecioPV
    '''</summary>
    <TestMethod()> _
    Public Sub ProductoTipoPV_ReplicarPrecioPVTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda_Origen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV_Origen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PorcentPV As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim DT_Atributos As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim addCostoFletexPeso As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.ProductoTipoPV_ReplicarPrecioPV(IdTienda_Origen, IdTienda_Destino, IdLinea, IdSubLinea, IdTipoPV_Origen, IdTipoPV_Destino, PorcentPV, DT_Atributos, addCostoFletexPeso, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de ReplicarPrecioPV_IdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub ReplicarPrecioPV_IdProductoTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda_Origen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV_Origen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PorcentPV As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim addCostoFletexPeso As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.ReplicarPrecioPV_IdProducto(IdTienda_Origen, IdTienda_Destino, IdProducto, IdTipoPV_Origen, IdTipoPV_Destino, PorcentPV, addCostoFletexPeso, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoTipoPV) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoTipoPV)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectPpublicoUMPrincipalxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectPpublicoUMPrincipalxParamsTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectPpublicoUMPrincipalxParams(IdTienda, IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectPrecioPublicoxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectPrecioPublicoxParamsTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectPrecioPublicoxParams(IdTienda, IdProducto, IdUnidadMedida)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectPrecioxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectPrecioxParamsTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectPrecioxParams(IdTienda, IdProducto, IdUnidadMedida, IdTipoPV)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectUM_Venta
    '''</summary>
    <TestMethod()> _
    Public Sub SelectUM_VentaTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of UnidadMedida) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of UnidadMedida)
        actual = target.SelectUM_Venta(IdProducto, idtienda, idtipopv, cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectUM_Venta
    '''</summary>
    <TestMethod()> _
    Public Sub SelectUM_VentaTest1()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of UnidadMedida) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of UnidadMedida)
        actual = target.SelectUM_Venta(IdProducto, idtienda, idtipopv)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectUmPrincipalxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectUmPrincipalxIdProductoTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoTipoPV) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoTipoPV)
        actual = target.SelectUmPrincipalxIdProducto(idproducto, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectValorCalculadoxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectValorCalculadoxParamsTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idunidadmedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectValorCalculadoxParams(idproducto, idtienda, idtipopv, idunidadmedida, idmoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectValor_Equivalencia_VentaxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectValor_Equivalencia_VentaxParamsTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim addPorcentRetazoAlways As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Catalogo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Catalogo
        actual = target.SelectValor_Equivalencia_VentaxParams(IdAlmacen, IdMoneda, IdUnidadMedida, IdProducto, idtienda, idtipopv, addPorcentRetazoAlways)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectValor_Equivalencia_VentaxParams_Cotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectValor_Equivalencia_VentaxParams_CotizacionTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim IdUnidadMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim cantidad As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim IdUMold As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Catalogo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Catalogo
        actual = target.SelectValor_Equivalencia_VentaxParams_Cotizacion(IdUnidadMedida, IdProducto, IdTipoPV, IdTienda, IdMoneda, IdAlmacen, IdEmpresa, Fecha, cantidad, IdUMold)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectValorxIdTiendaxIdProdxIdUMxIdTipoPV
    '''</summary>
    <TestMethod()> _
    Public Sub SelectValorxIdTiendaxIdProdxIdUMxIdTipoPVTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idunidadmedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.SelectValorxIdTiendaxIdProdxIdUMxIdTipoPV(idproducto, idtienda, idtipopv, idunidadmedida, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProductoTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoTipoPV) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoTipoPV)
        actual = target.SelectxIdProducto(idproducto, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProductoxIdTiendaxIdTipoPV
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProductoxIdTiendaxIdTipoPVTest()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoTipoPV) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoTipoPV)
        actual = target.SelectxIdProductoxIdTiendaxIdTipoPV(idproducto, idtienda, idtipopv, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProductoxIdTiendaxIdTipoPV
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProductoxIdTiendaxIdTipoPVTest1()
        Dim target As DAOProductoTipoPV = New DAOProductoTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoTipoPV) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoTipoPV)
        actual = target.SelectxIdProductoxIdTiendaxIdTipoPV(idproducto, idtienda, idtipopv)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
