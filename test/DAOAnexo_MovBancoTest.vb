﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOAnexo_MovBancoTest y se pretende que
'''contenga todas las pruebas unitarias DAOAnexo_MovBancoTest.
'''</summary>
<TestClass()> _
Public Class DAOAnexo_MovBancoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOAnexo_MovBanco
    '''</summary>
    <TestMethod()> _
    Public Sub DAOAnexo_MovBancoConstructorTest()
        Dim target As DAOAnexo_MovBanco = New DAOAnexo_MovBanco()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Anexo_MovBancoInsert
    '''</summary>
    <TestMethod()> _
    Public Sub Anexo_MovBancoInsertTest()
        Dim target As DAOAnexo_MovBanco = New DAOAnexo_MovBanco() ' TODO: Inicializar en un valor adecuado
        Dim objAnexoMovBanco As Anexo_MovBanco = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Anexo_MovBancoInsert(objAnexoMovBanco, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdMovBanco
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdMovBancoTest()
        Dim target As DAOAnexo_MovBanco = New DAOAnexo_MovBanco() ' TODO: Inicializar en un valor adecuado
        Dim IdMovBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdMovBanco(IdMovBanco, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoTest()
        Dim target As DAOAnexo_MovBanco = New DAOAnexo_MovBanco() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Anexo_MovBanco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Anexo_MovBanco)
        actual = target.SelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdMovBanco
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdMovBancoTest()
        Dim target As DAOAnexo_MovBanco = New DAOAnexo_MovBanco() ' TODO: Inicializar en un valor adecuado
        Dim IdMovBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Anexo_MovBanco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Anexo_MovBanco)
        actual = target.SelectxIdMovBanco(IdMovBanco)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
