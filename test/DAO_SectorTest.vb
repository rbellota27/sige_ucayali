﻿Imports System.Data

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAO_SectorTest y se pretende que
'''contenga todas las pruebas unitarias DAO_SectorTest.
'''</summary>
<TestClass()> _
Public Class DAO_SectorTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAO_Sector
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_SectorConstructorTest()
        Dim target As DAO_Sector = New DAO_Sector()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizarSector
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizarSectorTest()
        Dim target As DAO_Sector = New DAO_Sector() ' TODO: Inicializar en un valor adecuado
        Dim sector As Sector = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizarSector(sector)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_listarSectores
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_listarSectoresTest()
        Dim target As DAO_Sector = New DAO_Sector() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.DAO_listarSectores(idDocumento, idProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaRelacion_Sector_Sublinea
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaRelacion_Sector_SublineaTest()
        Dim target As DAO_Sector = New DAO_Sector() ' TODO: Inicializar en un valor adecuado
        Dim sector As Sector = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaRelacion_Sector_Sublinea(sector)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaSector
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaSectorTest()
        Dim target As DAO_Sector = New DAO_Sector() ' TODO: Inicializar en un valor adecuado
        Dim sector As Sector = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaSector(sector)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
