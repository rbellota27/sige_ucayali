﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMovBanco_ChequeTest y se pretende que
'''contenga todas las pruebas unitarias DAOMovBanco_ChequeTest.
'''</summary>
<TestClass()> _
Public Class DAOMovBanco_ChequeTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMovBanco_Cheque
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMovBanco_ChequeConstructorTest()
        Dim target As DAOMovBanco_Cheque = New DAOMovBanco_Cheque()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de MovBanco_Cheque_Select
    '''</summary>
    <TestMethod()> _
    Public Sub MovBanco_Cheque_SelectTest()
        Dim target As DAOMovBanco_Cheque = New DAOMovBanco_Cheque() ' TODO: Inicializar en un valor adecuado
        Dim Id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovBanco_Cheque) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovBanco_Cheque)
        actual = target.MovBanco_Cheque_Select(Id)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de MovBanco_Cheque_Transaction
    '''</summary>
    <TestMethod()> _
    Public Sub MovBanco_Cheque_TransactionTest()
        Dim target As DAOMovBanco_Cheque = New DAOMovBanco_Cheque() ' TODO: Inicializar en un valor adecuado
        Dim objMovBanco_Cheque As MovBanco_Cheque = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlCN As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlTR As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.MovBanco_Cheque_Transaction(objMovBanco_Cheque, sqlCN, sqlTR)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
