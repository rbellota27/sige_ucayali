﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOOcupacionPersonaTest y se pretende que
'''contenga todas las pruebas unitarias DAOOcupacionPersonaTest.
'''</summary>
<TestClass()> _
Public Class DAOOcupacionPersonaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOOcupacionPersona
    '''</summary>
    <TestMethod()> _
    Public Sub DAOOcupacionPersonaConstructorTest()
        Dim target As DAOOcupacionPersona = New DAOOcupacionPersona()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaOcupacionPersona
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaOcupacionPersonaTest()
        Dim target As DAOOcupacionPersona = New DAOOcupacionPersona() ' TODO: Inicializar en un valor adecuado
        Dim ocupacionpersona As OcupacionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaOcupacionPersona(ocupacionpersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaOcupacionPersonaT
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaOcupacionPersonaTTest()
        Dim target As DAOOcupacionPersona = New DAOOcupacionPersona() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ocupacionpersona As OcupacionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.ActualizaOcupacionPersonaT(cn, ocupacionpersona, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaListaOcupacionPersona
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaListaOcupacionPersonaTest()
        Dim target As DAOOcupacionPersona = New DAOOcupacionPersona() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim OcupacionPersona As List(Of OcupacionPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaListaOcupacionPersona(cn, OcupacionPersona, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaOcupacionPersona
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaOcupacionPersonaTest()
        Dim target As DAOOcupacionPersona = New DAOOcupacionPersona() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim OcupacionPersona As OcupacionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaOcupacionPersona(cn, OcupacionPersona, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOOcupacionPersona = New DAOOcupacionPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As OcupacionPersona = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As OcupacionPersona
        actual = target.SelectxId(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Selectx_Id
    '''</summary>
    <TestMethod()> _
    Public Sub Selectx_IdTest()
        Dim target As DAOOcupacionPersona = New DAOOcupacionPersona() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of OcupacionPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of OcupacionPersona)
        actual = target.Selectx_Id(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
