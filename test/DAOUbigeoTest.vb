﻿Imports System.Collections.Generic

Imports System.Data

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOUbigeoTest y se pretende que
'''contenga todas las pruebas unitarias DAOUbigeoTest.
'''</summary>
<TestClass()> _
Public Class DAOUbigeoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOUbigeo
    '''</summary>
    <TestMethod()> _
    Public Sub DAOUbigeoConstructorTest()
        Dim target As DAOUbigeo = New DAOUbigeo()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaUbigeo
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaUbigeoTest()
        Dim target As DAOUbigeo = New DAOUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim ubigeo As Ubigeo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaUbigeo(ubigeo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaUbigeoZona
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaUbigeoZonaTest()
        Dim target As DAOUbigeo = New DAOUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim ubigeo As Ubigeo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaUbigeoZona(ubigeo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaUbigeo
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaUbigeoTest()
        Dim target As DAOUbigeo = New DAOUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim ubigeo As Ubigeo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaUbigeo(ubigeo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllDatos
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllDatosTest()
        Dim target As DAOUbigeo = New DAOUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.SelectAllDatos
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllDepartamentos
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllDepartamentosTest()
        Dim target As DAOUbigeo = New DAOUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Ubigeo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Ubigeo)
        actual = target.SelectAllDepartamentos
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllDistritosxCodDptoxCodProv
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllDistritosxCodDptoxCodProvTest()
        Dim target As DAOUbigeo = New DAOUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim CodDpto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodProv As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Ubigeo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Ubigeo)
        actual = target.SelectAllDistritosxCodDptoxCodProv(CodDpto, CodProv)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllProvinciasxCodDpto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllProvinciasxCodDptoTest()
        Dim target As DAOUbigeo = New DAOUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim CodDpto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Ubigeo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Ubigeo)
        actual = target.SelectAllProvinciasxCodDpto(CodDpto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxCodDeptoxCodProvxCodDistrito
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxCodDeptoxCodProvxCodDistritoTest()
        Dim target As DAOUbigeo = New DAOUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim CodDepto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodProv As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodDist As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Ubigeo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Ubigeo
        actual = target.SelectxCodDeptoxCodProvxCodDistrito(CodDepto, CodProv, CodDist)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
