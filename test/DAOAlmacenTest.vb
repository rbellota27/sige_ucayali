﻿Imports System.Data

Imports System

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOAlmacenTest y se pretende que
'''contenga todas las pruebas unitarias DAOAlmacenTest.
'''</summary>
<TestClass()> _
Public Class DAOAlmacenTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub DAOAlmacenConstructorTest()
        Dim target As DAOAlmacen = New DAOAlmacen()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizarAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizarAlmacenTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim almacen As Almacen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizarAlmacen(almacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de AlmacenSelectActivoCentroDistribucionxIdEmpresa
    '''</summary>
    <TestMethod()> _
    Public Sub AlmacenSelectActivoCentroDistribucionxIdEmpresaTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.AlmacenSelectActivoCentroDistribucionxIdEmpresa(IdEmpresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaAlmacenTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim almacen As Almacen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaAlmacen(almacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNombreTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectActivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim estado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectAllActivo(estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoxIdTipoAlmacenxIdEmpresa
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoxIdTipoAlmacenxIdEmpresaTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectAllActivoxIdTipoAlmacenxIdEmpresa(IdTipoAlmacen, IdEmpresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAlmacenPrincipalxIdTienda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAlmacenPrincipalxIdTiendaTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectAlmacenPrincipalxIdTienda(idTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAlmacenTAPrincipal
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAlmacenTAPrincipalTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectAlmacenTAPrincipal
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdEmpresa
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdEmpresaTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectCboxIdEmpresa(IdEmpresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdTienda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdTiendaTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectCboxIdTienda(IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdTipoAlmacenxIdTienda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdTipoAlmacenxIdTiendaTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectCboxIdTipoAlmacenxIdTienda(IdTipoAlmacen, IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrilla
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectGrilla
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaActivoInactivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaActivoInactivoxNombreTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectGrillaActivoInactivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaActivoxNombreTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectGrillaActivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxNombreTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.SelectInactivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Almacen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Almacen
        actual = target.SelectxId(IdAlmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_AlmacenSaldoxFecha
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_AlmacenSaldoxFechaTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FiltrarSaldo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Saldo As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim Tabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_AlmacenSaldoxFecha(IdEmpresa, IdAlmacen, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto, FechaFin, FiltrarSaldo, Saldo, Tabla)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_CR_Comercializacion_Foshan02
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_CR_Comercializacion_Foshan02Test()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaIni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim YearIni As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim SemanaIni As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim YearFin As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim SemanaFin As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FiltrarSemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim AtributoProducto As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TipoAlmacen As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_CR_Comercializacion_Foshan02(IdEmpresa, IdTienda, IdTipoExistencia, IdLinea, IdSubLinea, FechaIni, FechaFin, YearIni, SemanaIni, YearFin, SemanaFin, FiltrarSemana, AtributoProducto, TipoAlmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_ComercializacionPorMes
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_ComercializacionPorMesTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaIni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim TipoAlmacen As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaPrecio As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim AtributoProducto As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_ComercializacionPorMes(IdEmpresa, IdTienda, IdAlmacen, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto, FechaIni, FechaFin, TipoAlmacen, IdDocumento, IdTiendaPrecio, AtributoProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_ComercializacionPorSemana
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_ComercializacionPorSemanaTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim YearIni As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim SemanaIni As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim YearFin As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim SemanaFin As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim TipoAlmacen As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaPrecio As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim AtributoProducto As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_ComercializacionPorSemana(IdEmpresa, IdTienda, IdAlmacen, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto, YearIni, SemanaIni, YearFin, SemanaFin, TipoAlmacen, IdDocumento, IdTiendaPrecio, AtributoProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_ProductoComprometido
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_ProductoComprometidoTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_ProductoComprometido(IdAlmacen, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_ProductoEnTransito
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_ProductoEnTransitoTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_ProductoEnTransito(IdAlmacen, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_ProductoPorEntregar
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_ProductoPorEntregarTest()
        Dim target As DAOAlmacen = New DAOAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_ProductoPorEntregar(IdEmpresa, IdAlmacen, IdDocumento, IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
