﻿Imports System.Collections.Generic

Imports Entidades

Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOValidDocRelacionadoTest y se pretende que
'''contenga todas las pruebas unitarias DAOValidDocRelacionadoTest.
'''</summary>
<TestClass()> _
Public Class DAOValidDocRelacionadoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOValidDocRelacionado
    '''</summary>
    <TestMethod()> _
    Public Sub DAOValidDocRelacionadoConstructorTest()
        Dim target As DAOValidDocRelacionado = New DAOValidDocRelacionado()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocRefValidNotaCredito
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocRefValidNotaCreditoTest()
        Dim target As DAOValidDocRelacionado = New DAOValidDocRelacionado() ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaI As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaF As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim opcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim ValDocRelacionado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoValid) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoValid)
        'actual = target.SelectDocRefValidNotaCredito(IdCliente, FechaI, FechaF, IdTienda, serie, codigo, opcion, ValDocRelacionado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
