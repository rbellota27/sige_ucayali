﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocImportacionTest y se pretende que
'''contenga todas las pruebas unitarias DAODocImportacionTest.
'''</summary>
<TestClass()> _
Public Class DAODocImportacionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocImportacion
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocImportacionConstructorTest()
        Dim target As DAODocImportacion = New DAODocImportacion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaImportacionT
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaImportacionTTest()
        Dim target As DAODocImportacion = New DAODocImportacion() ' TODO: Inicializar en un valor adecuado
        Dim objImportacion As DocImportacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaImportacionT(objImportacion, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de AnularImportacion
    '''</summary>
    <TestMethod()> _
    Public Sub AnularImportacionTest()
        Dim target As DAODocImportacion = New DAODocImportacion() ' TODO: Inicializar en un valor adecuado
        Dim objImportacion As DocImportacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.AnularImportacion(objImportacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleGastoImportacionSelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleGastoImportacionSelectxIdTest()
        Dim target As DAODocImportacion = New DAODocImportacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocImportacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocImportacion)
        actual = target.DetalleGastoImportacionSelectxId(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleGastoSelectxIdConcepto
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleGastoSelectxIdConceptoTest()
        Dim target As DAODocImportacion = New DAODocImportacion() ' TODO: Inicializar en un valor adecuado
        Dim IdConcepto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocImportacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocImportacion)
        actual = target.DetalleGastoSelectxIdConcepto(IdConcepto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ImportacionSelectAllxSeriexCodigoxProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub ImportacionSelectAllxSeriexCodigoxProveedorTest()
        Dim target As DAODocImportacion = New DAODocImportacion() ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocImportacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocImportacion)
        actual = target.ImportacionSelectAllxSeriexCodigoxProveedor(Serie, Codigo, IdProveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ImportacionSelectId
    '''</summary>
    <TestMethod()> _
    Public Sub ImportacionSelectIdTest()
        Dim target As DAODocImportacion = New DAODocImportacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocImportacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocImportacion
        actual = target.ImportacionSelectId(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertImportacionT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertImportacionTTest()
        Dim target As DAODocImportacion = New DAODocImportacion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objimportacion As DocImportacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertImportacionT(cn, objimportacion, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenCompraSelectNroOCxIdProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenCompraSelectNroOCxIdProveedorTest()
        Dim target As DAODocImportacion = New DAODocImportacion() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Correlativo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocImportacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocImportacion)
        actual = target.OrdenCompraSelectNroOCxIdProveedor(IdEmpresa, IdTienda, Serie, Correlativo, IdProveedor, pageindex, pagesize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTipoDocumentoTest()
        Dim target As DAODocImportacion = New DAODocImportacion() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocImportacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocImportacion)
        actual = target.SelectAllTipoDocumento
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCostosSinPasarxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCostosSinPasarxIdDocumentoTest()
        Dim target As DAODocImportacion = New DAODocImportacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectCostosSinPasarxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
