﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODetalleConceptoTest y se pretende que
'''contenga todas las pruebas unitarias DAODetalleConceptoTest.
'''</summary>
<TestClass()> _
Public Class DAODetalleConceptoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODetalleConcepto
    '''</summary>
    <TestMethod()> _
    Public Sub DAODetalleConceptoConstructorTest()
        Dim target As DAODetalleConcepto = New DAODetalleConcepto()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DetalleConceptoInsert
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleConceptoInsertTest()
        Dim target As DAODetalleConcepto = New DAODetalleConcepto() ' TODO: Inicializar en un valor adecuado
        Dim objDetalleConcepto As DetalleConcepto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim valConceptoTienda As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.DetalleConceptoInsert(objDetalleConcepto, cn, tr, valConceptoTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleConceptoInsertSolPago
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleConceptoInsertSolPagoTest()
        Dim target As DAODetalleConcepto = New DAODetalleConcepto() ' TODO: Inicializar en un valor adecuado
        Dim objDetalleConcepto As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim obj As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.DetalleConceptoInsertSolPago(objDetalleConcepto, obj, cn, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GrabarDetalleConceptoT
    '''</summary>
    <TestMethod()> _
    Public Sub GrabarDetalleConceptoTTest()
        Dim target As DAODetalleConcepto = New DAODetalleConcepto() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim vIdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim ldetalleconcepto As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim valConceptoAnticipo As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.GrabarDetalleConceptoT(cn, vIdDocumento, ldetalleconcepto, T, valConceptoAnticipo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoTest()
        Dim target As DAODetalleConcepto = New DAODetalleConcepto() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleConcepto)
        actual = target.SelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
