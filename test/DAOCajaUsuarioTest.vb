﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCajaUsuarioTest y se pretende que
'''contenga todas las pruebas unitarias DAOCajaUsuarioTest.
'''</summary>
<TestClass()> _
Public Class DAOCajaUsuarioTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCajaUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCajaUsuarioConstructorTest()
        Dim target As DAOCajaUsuario = New DAOCajaUsuario()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdCajaxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdCajaxIdPersonaTest()
        Dim target As DAOCajaUsuario = New DAOCajaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idcaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexIdCajaxIdPersona(idpersona, idcaja, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Insert
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTest()
        Dim target As DAOCajaUsuario = New DAOCajaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim objCajaUsuario As CajaUsuario = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.Insert(objCajaUsuario, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxIdPersonaTest()
        Dim target As DAOCajaUsuario = New DAOCajaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CajaUsuario) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CajaUsuario)
        actual = target.SelectAllxIdPersona(idpersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdTiendaxIdUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdTiendaxIdUsuarioTest()
        Dim target As DAOCajaUsuario = New DAOCajaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Caja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Caja)
        actual = target.SelectCboxIdTiendaxIdUsuario(IdUsuario, IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdTiendaxIdUsuario_Caja
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdTiendaxIdUsuario_CajaTest()
        Dim target As DAOCajaUsuario = New DAOCajaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Caja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Caja)
        actual = target.SelectCboxIdTiendaxIdUsuario_Caja(IdTienda, IdUsuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCountxIdTienda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCountxIdTiendaTest()
        Dim target As DAOCajaUsuario = New DAOCajaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectCountxIdTienda(idtienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPersonaTest()
        Dim target As DAOCajaUsuario = New DAOCajaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Caja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Caja)
        actual = target.SelectxIdPersona(idpersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Update
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTest()
        Dim target As DAOCajaUsuario = New DAOCajaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim objCajaUsuario As CajaUsuario = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.Update(objCajaUsuario, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarCajaActivoxIdPersonaxIdTiendaxIdCaja
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarCajaActivoxIdPersonaxIdTiendaxIdCajaTest()
        Dim target As DAOCajaUsuario = New DAOCajaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idcaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarCajaActivoxIdPersonaxIdTiendaxIdCaja(idpersona, idtienda, idcaja)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fnObtenerDeudasxIdCliente
    '''</summary>
    <TestMethod()> _
    Public Sub fnObtenerDeudasxIdClienteTest()
        Dim target As DAOCajaUsuario = New DAOCajaUsuario() ' TODO: Inicializar en un valor adecuado
        Dim _IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Cliente) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Cliente)
        actual = target.fnObtenerDeudasxIdCliente(_IdCliente)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
