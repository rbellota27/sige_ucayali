﻿Imports System

Imports System.Data

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOValorizadoCajasTest y se pretende que
'''contenga todas las pruebas unitarias DAOValorizadoCajasTest.
'''</summary>
<TestClass()> _
Public Class DAOValorizadoCajasTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOValorizadoCajas
    '''</summary>
    <TestMethod()> _
    Public Sub DAOValorizadoCajasConstructorTest()
        Dim target As DAOValorizadoCajas = New DAOValorizadoCajas()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DAO_CargarComboSecuencia
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_CargarComboSecuenciaTest()
        Dim target As DAOValorizadoCajas = New DAOValorizadoCajas() ' TODO: Inicializar en un valor adecuado
        Dim filtro As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tabla As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.DAO_CargarComboSecuencia(filtro, tabla)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DAO_ObtenerDatosGrilla
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_ObtenerDatosGrillaTest()
        Dim target As DAOValorizadoCajas = New DAOValorizadoCajas() ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fechaFinal As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim topNumero As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim opcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim opcFiltro As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim existencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim linea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim sublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pais As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idFabrica As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idTipoCliente As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.DAO_ObtenerDatosGrilla(idTienda, fechaInicio, fechaFinal, topNumero, opcion, opcFiltro, existencia, linea, sublinea, pais, idFabrica, idTipoCliente)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
