﻿Imports System

Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMovCuentaTest y se pretende que
'''contenga todas las pruebas unitarias DAOMovCuentaTest.
'''</summary>
<TestClass()> _
Public Class DAOMovCuentaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMovCuenta
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMovCuentaConstructorTest()
        Dim target As DAOMovCuenta = New DAOMovCuenta()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeleteMovCuenta_PagoProgramadoxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DeleteMovCuenta_PagoProgramadoxIdDocumentoTest()
        Dim target As DAOMovCuenta = New DAOMovCuenta() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeleteMovCuenta_PagoProgramadoxIdDocumento(IdDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaMovCuenta
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaMovCuentaTest()
        Dim target As DAOMovCuenta = New DAOMovCuenta() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim movcuenta As MovCuenta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaMovCuenta(cn, movcuenta, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAbonosxIdMovCuenta
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAbonosxIdMovCuentaTest()
        Dim target As DAOMovCuenta = New DAOMovCuenta() ' TODO: Inicializar en un valor adecuado
        Dim IdMovCuenta As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento_MovCuenta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento_MovCuenta)
        actual = target.SelectAbonosxIdMovCuenta(IdMovCuenta)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDeudaxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDeudaxIdPersonaTest()
        Dim target As DAOMovCuenta = New DAOMovCuenta() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento_MovCuenta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento_MovCuenta)
        actual = target.SelectDeudaxIdPersona(IdPersona, IdMonedaDestino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDeudaxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDeudaxIdPersonaTest1()
        Dim target As DAOMovCuenta = New DAOMovCuenta() ' TODO: Inicializar en un valor adecuado
        Dim idPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovCuenta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovCuenta)
        actual = target.SelectDeudaxIdPersona(idPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDeudaxIdPersona_NoLetras
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDeudaxIdPersona_NoLetrasTest()
        Dim target As DAOMovCuenta = New DAOMovCuenta() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento_MovCuenta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento_MovCuenta)
        actual = target.SelectDeudaxIdPersona_NoLetras(IdPersona, IdMonedaDestino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateSaldoxIdMovCuenta
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateSaldoxIdMovCuentaTest()
        Dim target As DAOMovCuenta = New DAOMovCuenta() ' TODO: Inicializar en un valor adecuado
        Dim IdMovCuenta As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim SaldoNew As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateSaldoxIdMovCuenta(IdMovCuenta, SaldoNew, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de fnInsTblMovCuenta
    '''</summary>
    <TestMethod()> _
    Public Sub fnInsTblMovCuentaTest()
        Dim target As DAOMovCuenta = New DAOMovCuenta() ' TODO: Inicializar en un valor adecuado
        Dim poBEMovCuenta As MovCuenta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.fnInsTblMovCuenta(poBEMovCuenta)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fnSelTblMovCuentaDin
    '''</summary>
    <TestMethod()> _
    Public Sub fnSelTblMovCuentaDinTest()
        Dim target As DAOMovCuenta = New DAOMovCuenta() ' TODO: Inicializar en un valor adecuado
        Dim psWhere As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim psOrder As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovCuenta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovCuenta)
        actual = target.fnSelTblMovCuentaDin(psWhere, psOrder)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fnUdpTblMovCuenta
    '''</summary>
    <TestMethod()> _
    Public Sub fnUdpTblMovCuentaTest()
        Dim target As DAOMovCuenta = New DAOMovCuenta() ' TODO: Inicializar en un valor adecuado
        Dim poBEMovCuenta As MovCuenta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.fnUdpTblMovCuenta(poBEMovCuenta)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
