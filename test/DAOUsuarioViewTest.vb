﻿Imports System.Data

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOUsuarioViewTest y se pretende que
'''contenga todas las pruebas unitarias DAOUsuarioViewTest.
'''</summary>
<TestClass()> _
Public Class DAOUsuarioViewTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOUsuarioView
    '''</summary>
    <TestMethod()> _
    Public Sub DAOUsuarioViewConstructorTest()
        Dim target As DAOUsuarioView = New DAOUsuarioView()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de FunVerTodasTiendas
    '''</summary>
    <TestMethod()> _
    Public Sub FunVerTodasTiendasTest()
        Dim target As DAOUsuarioView = New DAOUsuarioView() ' TODO: Inicializar en un valor adecuado
        Dim idusuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idpermiso As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.FunVerTodasTiendas(idusuario, idpermiso)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxEstadoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxEstadoxNombreTest()
        Dim target As DAOUsuarioView = New DAOUsuarioView() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim estado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of UsuarioView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of UsuarioView)
        actual = target.SelectxEstadoxNombre(nombre, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxEstadoxNombre_IdPerfil
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxEstadoxNombre_IdPerfilTest()
        Dim target As DAOUsuarioView = New DAOUsuarioView() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim estado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of UsuarioView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of UsuarioView)
        actual = target.SelectxEstadoxNombre_IdPerfil(nombre, estado, IdPerfil)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPersonaTest()
        Dim target As DAOUsuarioView = New DAOUsuarioView() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As UsuarioView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As UsuarioView
        actual = target.SelectxIdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDocumentosUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub getDocumentosUsuarioTest()
        Dim target As DAOUsuarioView = New DAOUsuarioView() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDocumentosUsuario(IdEmpresa, Idtienda, fechainicio, fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
