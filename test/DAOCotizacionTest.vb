﻿Imports System.Collections.Generic

Imports System

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCotizacionTest y se pretende que
'''contenga todas las pruebas unitarias DAOCotizacionTest.
'''</summary>
<TestClass()> _
Public Class DAOCotizacionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCotizacionConstructorTest()
        Dim target As DAOCotizacion = New DAOCotizacion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaCotizacionTest()
        Dim target As DAOCotizacion = New DAOCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim cotizacion As Cotizacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaCotizacion(cotizacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CotizacionesXAprobar
    '''</summary>
    <TestMethod()> _
    Public Sub CotizacionesXAprobarTest()
        Dim target As DAOCotizacion = New DAOCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim idAprobarCoti As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.CotizacionesXAprobar(cn, idEmpresa, idTienda, idPersona, fechaInicio, fechaFin, idAprobarCoti)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaCotizacionTest()
        Dim target As DAOCotizacion = New DAOCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim cotizacion As Cotizacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaCotizacion(cotizacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de aprobarCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub aprobarCotizacionTest()
        Dim target As DAOCotizacion = New DAOCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.aprobarCotizacion(cn, idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
