﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTiendaAreaTest y se pretende que
'''contenga todas las pruebas unitarias DAOTiendaAreaTest.
'''</summary>
<TestClass()> _
Public Class DAOTiendaAreaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTiendaArea
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTiendaAreaConstructorTest()
        Dim target As DAOTiendaArea = New DAOTiendaArea()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdTienda
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdTiendaTest()
        Dim target As DAOTiendaArea = New DAOTiendaArea() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdTienda(cn, IdTienda, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GrabaTiendaAreaT
    '''</summary>
    <TestMethod()> _
    Public Sub GrabaTiendaAreaTTest()
        Dim target As DAOTiendaArea = New DAOTiendaArea() ' TODO: Inicializar en un valor adecuado
        Dim CN As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ltiendarea As List(Of TiendaArea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.GrabaTiendaAreaT(CN, ltiendarea, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaTiendaAreaT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTiendaAreaTTest()
        Dim target As DAOTiendaArea = New DAOTiendaArea() ' TODO: Inicializar en un valor adecuado
        Dim CN As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ltiendarea As List(Of TiendaArea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaTiendaAreaT(CN, ltiendarea, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub
End Class
