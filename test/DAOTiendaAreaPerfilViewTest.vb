﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTiendaAreaPerfilViewTest y se pretende que
'''contenga todas las pruebas unitarias DAOTiendaAreaPerfilViewTest.
'''</summary>
<TestClass()> _
Public Class DAOTiendaAreaPerfilViewTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTiendaAreaPerfilView
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTiendaAreaPerfilViewConstructorTest()
        Dim target As DAOTiendaAreaPerfilView = New DAOTiendaAreaPerfilView()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboAreaxIdTienda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboAreaxIdTiendaTest()
        Dim target As DAOTiendaAreaPerfilView = New DAOTiendaAreaPerfilView() ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TiendaAreaPerfilView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TiendaAreaPerfilView)
        actual = target.SelectCboAreaxIdTienda(idtienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboPerfilxIdTiendaxIdArea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboPerfilxIdTiendaxIdAreaTest()
        Dim target As DAOTiendaAreaPerfilView = New DAOTiendaAreaPerfilView() ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idarea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TiendaAreaPerfilView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TiendaAreaPerfilView)
        actual = target.SelectCboPerfilxIdTiendaxIdArea(idtienda, idarea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboTienda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTiendaTest()
        Dim target As DAOTiendaAreaPerfilView = New DAOTiendaAreaPerfilView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TiendaAreaPerfilView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TiendaAreaPerfilView)
        actual = target.SelectCboTienda
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboTiendaxIdPerfilxIdArea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTiendaxIdPerfilxIdAreaTest()
        Dim target As DAOTiendaAreaPerfilView = New DAOTiendaAreaPerfilView() ' TODO: Inicializar en un valor adecuado
        Dim idperfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idarea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TiendaAreaPerfilView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TiendaAreaPerfilView)
        actual = target.SelectCboTiendaxIdPerfilxIdArea(idperfil, idarea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
