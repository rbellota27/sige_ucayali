﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODatosCancelacionTest y se pretende que
'''contenga todas las pruebas unitarias DAODatosCancelacionTest.
'''</summary>
<TestClass()> _
Public Class DAODatosCancelacionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODatosCancelacion
    '''</summary>
    <TestMethod()> _
    Public Sub DAODatosCancelacionConstructorTest()
        Dim target As DAODatosCancelacion = New DAODatosCancelacion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Anular
    '''</summary>
    <TestMethod()> _
    Public Sub AnularTest()
        Dim target As DAODatosCancelacion = New DAODatosCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Anular(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DeshacerMovTest()
        Dim target As DAODatosCancelacion = New DAODatosCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeshacerMovimientos As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovCuenta As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleConcepto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDatosCancelacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePagoCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovBanco As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteAnexoDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeshacerMov(IdDocumento, DeshacerMovimientos, DeleteMovCuenta, DeleteDetalleConcepto, DeleteDatosCancelacion, DeleteMovCaja, DeletePagoCaja, DeleteMovBanco, DeleteRelacionDocumento, DeleteObservacion, DeleteAnexoDocumento, Anular, cnx, trx)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeshacerMov_Edicion
    '''</summary>
    <TestMethod()> _
    Public Sub DeshacerMov_EdicionTest()
        Dim target As DAODatosCancelacion = New DAODatosCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeshacerMov_Edicion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GetVectorParametros
    '''</summary>
    <TestMethod()> _
    Public Sub GetVectorParametrosTest()
        Dim target As DAODatosCancelacion = New DAODatosCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim x As DatosCancelacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim op As DAOMantenedor.modo_query = New DAOMantenedor.modo_query() ' TODO: Inicializar en un valor adecuado
        Dim expected() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual() As SqlParameter
        actual = target.GetVectorParametros(x, op)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTTest()
        Dim target As DAODatosCancelacion = New DAODatosCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim x As DatosCancelacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertT(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertT_GetID
    '''</summary>
    <TestMethod()> _
    Public Sub InsertT_GetIDTest()
        Dim target As DAODatosCancelacion = New DAODatosCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim x As DatosCancelacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertT_GetID(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertT_GetID_V2
    '''</summary>
    <TestMethod()> _
    Public Sub InsertT_GetID_V2Test()
        Dim target As DAODatosCancelacion = New DAODatosCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim x As DatosCancelacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertT_GetID_V2(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectListxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectListxIdDocumentoTest()
        Dim target As DAODatosCancelacion = New DAODatosCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DatosCancelacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DatosCancelacion)
        actual = target.SelectListxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateT
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTTest()
        Dim target As DAODatosCancelacion = New DAODatosCancelacion() ' TODO: Inicializar en un valor adecuado
        Dim x As DatosCancelacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim CnxExpected As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim TrxExpected As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateT(x, Cnx, Trx)
        Assert.AreEqual(CnxExpected, Cnx)
        Assert.AreEqual(TrxExpected, Trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
