﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOZonaUbigeoTest y se pretende que
'''contenga todas las pruebas unitarias DAOZonaUbigeoTest.
'''</summary>
<TestClass()> _
Public Class DAOZonaUbigeoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOZonaUbigeo
    '''</summary>
    <TestMethod()> _
    Public Sub DAOZonaUbigeoConstructorTest()
        Dim target As DAOZonaUbigeo = New DAOZonaUbigeo()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaZonaUbigeo
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaZonaUbigeoTest()
        Dim target As DAOZonaUbigeo = New DAOZonaUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim zonaubigeo As ZonaUbigeo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaZonaUbigeo(zonaubigeo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de EliminaZonaUbigeo
    '''</summary>
    <TestMethod()> _
    Public Sub EliminaZonaUbigeoTest()
        Dim target As DAOZonaUbigeo = New DAOZonaUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim zonaubigeo As ZonaUbigeo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.EliminaZonaUbigeo(zonaubigeo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaZonaUbigeo
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaZonaUbigeoTest()
        Dim target As DAOZonaUbigeo = New DAOZonaUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim zonaubigeo As ZonaUbigeo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaZonaUbigeo(zonaubigeo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTest()
        Dim target As DAOZonaUbigeo = New DAOZonaUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ZonaUbigeo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ZonaUbigeo)
        actual = target.SelectCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxEstadoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxEstadoxNombreTest()
        Dim target As DAOZonaUbigeo = New DAOZonaUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim estado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ZonaUbigeo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ZonaUbigeo)
        actual = target.SelectxEstadoxNombre(estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdZona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdZonaTest()
        Dim target As DAOZonaUbigeo = New DAOZonaUbigeo() ' TODO: Inicializar en un valor adecuado
        Dim IdZona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As ZonaUbigeo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As ZonaUbigeo
        actual = target.SelectxIdZona(IdZona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
