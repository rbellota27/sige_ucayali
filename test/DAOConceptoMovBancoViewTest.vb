﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOConceptoMovBancoViewTest y se pretende que
'''contenga todas las pruebas unitarias DAOConceptoMovBancoViewTest.
'''</summary>
<TestClass()> _
Public Class DAOConceptoMovBancoViewTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOConceptoMovBancoView
    '''</summary>
    <TestMethod()> _
    Public Sub DAOConceptoMovBancoViewConstructorTest()
        Dim target As DAOConceptoMovBancoView = New DAOConceptoMovBancoView()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOConceptoMovBancoView = New DAOConceptoMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ConceptoMovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ConceptoMovBancoView)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOConceptoMovBancoView = New DAOConceptoMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ConceptoMovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ConceptoMovBancoView)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOConceptoMovBancoView = New DAOConceptoMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ConceptoMovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ConceptoMovBancoView)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdTipoConceptoBanco
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTipoConceptoBancoTest()
        Dim target As DAOConceptoMovBancoView = New DAOConceptoMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim Id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ConceptoMovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ConceptoMovBancoView)
        actual = target.SelectxIdTipoConceptoBanco(Id)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxParamsTest()
        Dim target As DAOConceptoMovBancoView = New DAOConceptoMovBancoView() ' TODO: Inicializar en un valor adecuado
        Dim x As ConceptoMovBancoView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ConceptoMovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ConceptoMovBancoView)
        actual = target.SelectxParams(x)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
