﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOEmpleadoTest y se pretende que
'''contenga todas las pruebas unitarias DAOEmpleadoTest.
'''</summary>
<TestClass()> _
Public Class DAOEmpleadoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOEmpleado
    '''</summary>
    <TestMethod()> _
    Public Sub DAOEmpleadoConstructorTest()
        Dim target As DAOEmpleado = New DAOEmpleado()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaEmpleado
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaEmpleadoTest()
        Dim target As DAOEmpleado = New DAOEmpleado() ' TODO: Inicializar en un valor adecuado
        Dim empleado As Empleado = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaEmpleado(empleado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaEmpleadoTransact
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaEmpleadoTransactTest()
        Dim target As DAOEmpleado = New DAOEmpleado() ' TODO: Inicializar en un valor adecuado
        Dim empleado As Empleado = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaEmpleadoTransact(empleado, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de EmpleadoSelectActivoxParams_Find
    '''</summary>
    <TestMethod()> _
    Public Sub EmpleadoSelectActivoxParams_FindTest()
        Dim target As DAOEmpleado = New DAOEmpleado() ' TODO: Inicializar en un valor adecuado
        Dim dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim ruc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim RazonApe As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idrol As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.EmpleadoSelectActivoxParams_Find(dni, ruc, RazonApe, tipo, pageindex, pagesize, idrol, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaEmpleado
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaEmpleadoTest()
        Dim target As DAOEmpleado = New DAOEmpleado() ' TODO: Inicializar en un valor adecuado
        Dim empleado As Empleado = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaEmpleado(empleado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaEmpleadoTransact
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaEmpleadoTransactTest()
        Dim target As DAOEmpleado = New DAOEmpleado() ' TODO: Inicializar en un valor adecuado
        Dim empleado As Empleado = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.InsertaEmpleadoTransact(empleado, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxParamsTest()
        Dim target As DAOEmpleado = New DAOEmpleado() ' TODO: Inicializar en un valor adecuado
        Dim opcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim textoBusqueda As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Empleado) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Empleado)
        actual = target.SelectActivoxParams(opcion, textoBusqueda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdEmpleado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdEmpleadoTest()
        Dim target As DAOEmpleado = New DAOEmpleado() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Empleado = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Empleado
        actual = target.SelectxIdEmpleado(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPersonaTest()
        Dim target As DAOEmpleado = New DAOEmpleado() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Empleado = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Empleado
        actual = target.SelectxIdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
