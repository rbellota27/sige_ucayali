﻿Imports System

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODetalleMovAlmacenTest y se pretende que
'''contenga todas las pruebas unitarias DAODetalleMovAlmacenTest.
'''</summary>
<TestClass()> _
Public Class DAODetalleMovAlmacenTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODetalleMovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub DAODetalleMovAlmacenConstructorTest()
        Dim target As DAODetalleMovAlmacen = New DAODetalleMovAlmacen()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaDetalleMovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaDetalleMovAlmacenTest()
        Dim target As DAODetalleMovAlmacen = New DAODetalleMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim detallemovalmacen As DetalleMovAlmacen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaDetalleMovAlmacen(detallemovalmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdDocumentoTest()
        Dim target As DAODetalleMovAlmacen = New DAODetalleMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdDocumento(cn, tr, iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDetalleMovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDetalleMovAlmacenTest()
        Dim target As DAODetalleMovAlmacen = New DAODetalleMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim detallemovalmacen As DetalleMovAlmacen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaDetalleMovAlmacen(cn, detallemovalmacen, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateCostoxIdMovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateCostoxIdMovAlmacenTest()
        Dim target As DAODetalleMovAlmacen = New DAODetalleMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idmovalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim costo As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateCostoxIdMovAlmacen(cn, tr, idmovalmacen, costo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
