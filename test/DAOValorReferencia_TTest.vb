﻿Imports System.Data

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOValorReferencia_TTest y se pretende que
'''contenga todas las pruebas unitarias DAOValorReferencia_TTest.
'''</summary>
<TestClass()> _
Public Class DAOValorReferencia_TTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOValorReferencia_T
    '''</summary>
    <TestMethod()> _
    Public Sub DAOValorReferencia_TConstructorTest()
        Dim target As DAOValorReferencia_T = New DAOValorReferencia_T()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub RegistrarTest()
        Dim target As DAOValorReferencia_T = New DAOValorReferencia_T() ' TODO: Inicializar en un valor adecuado
        Dim objValorReferenciaT As ValorReferencia_Transporte = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Registrar(objValorReferenciaT)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdValorReferenciaT
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdValorReferenciaTTest()
        Dim target As DAOValorReferencia_T = New DAOValorReferencia_T() ' TODO: Inicializar en un valor adecuado
        Dim IdValorReferenciaT As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As ValorReferencia_Transporte = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As ValorReferencia_Transporte
        actual = target.SelectxIdValorReferenciaT(IdValorReferenciaT)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxParams_DT
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxParams_DTTest()
        Dim target As DAOValorReferencia_T = New DAOValorReferencia_T() ' TODO: Inicializar en un valor adecuado
        Dim IdValorReferenciaT As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Estado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.SelectxParams_DT(IdValorReferenciaT, Estado, descripcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
