﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports System.Data

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOComision_CabTest y se pretende que
'''contenga todas las pruebas unitarias DAOComision_CabTest.
'''</summary>
<TestClass()> _
Public Class DAOComision_CabTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOComision_Cab
    '''</summary>
    <TestMethod()> _
    Public Sub DAOComision_CabConstructorTest()
        Dim target As DAOComision_Cab = New DAOComision_Cab()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Comision_Cab_SelectxIdComisionCab
    '''</summary>
    <TestMethod()> _
    Public Sub Comision_Cab_SelectxIdComisionCabTest()
        Dim target As DAOComision_Cab = New DAOComision_Cab() ' TODO: Inicializar en un valor adecuado
        Dim IdComisionCab As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Comision_Cab = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Comision_Cab
        actual = target.Comision_Cab_SelectxIdComisionCab(IdComisionCab)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Comision_Cab_SelectxParams_DT
    '''</summary>
    <TestMethod()> _
    Public Sub Comision_Cab_SelectxParams_DTTest()
        Dim target As DAOComision_Cab = New DAOComision_Cab() ' TODO: Inicializar en un valor adecuado
        Dim IdComisionCab As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.Comision_Cab_SelectxParams_DT(IdComisionCab)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Comision_Cab_cbo
    '''</summary>
    <TestMethod()> _
    Public Sub Comision_Cab_cboTest()
        Dim target As DAOComision_Cab = New DAOComision_Cab() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoComision As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Comision_Cab) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Comision_Cab)
        actual = target.Comision_Cab_cbo(IdTipoComision)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub RegistrarTest()
        Dim target As DAOComision_Cab = New DAOComision_Cab() ' TODO: Inicializar en un valor adecuado
        Dim objComisionCab As Comision_Cab = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Registrar(objComisionCab, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
