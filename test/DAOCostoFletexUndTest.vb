﻿Imports System.Collections.Generic

Imports System.Data

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCostoFletexUndTest y se pretende que
'''contenga todas las pruebas unitarias DAOCostoFletexUndTest.
'''</summary>
<TestClass()> _
Public Class DAOCostoFletexUndTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCostoFletexUnd
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCostoFletexUndConstructorTest()
        Dim target As DAOCostoFletexUnd = New DAOCostoFletexUnd()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de CostoFletexUnd_Delete
    '''</summary>
    <TestMethod()> _
    Public Sub CostoFletexUnd_DeleteTest()
        Dim target As DAOCostoFletexUnd = New DAOCostoFletexUnd() ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaOrigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.CostoFletexUnd_Delete(IdTiendaOrigen, IdTiendaDestino, IdProducto, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de CostoFletexUnd_Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub CostoFletexUnd_RegistrarTest()
        Dim target As DAOCostoFletexUnd = New DAOCostoFletexUnd() ' TODO: Inicializar en un valor adecuado
        Dim objCostoFletexUnd As CostoFletexUnd = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.CostoFletexUnd_Registrar(objCostoFletexUnd, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de CostoFletexUnd_SelectProductoBasexParams
    '''</summary>
    <TestMethod()> _
    Public Sub CostoFletexUnd_SelectProductoBasexParamsTest()
        Dim target As DAOCostoFletexUnd = New DAOCostoFletexUnd() ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaOrigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Producto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodigoProd As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CostoFletexUnd) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CostoFletexUnd)
        actual = target.CostoFletexUnd_SelectProductoBasexParams(IdTiendaOrigen, IdTiendaDestino, IdProducto, IdLinea, IdSubLinea, Producto, CodigoProd, IdTipoExistencia, tabla)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CostoFletexUnd_SelectxParams_DT
    '''</summary>
    <TestMethod()> _
    Public Sub CostoFletexUnd_SelectxParams_DTTest()
        Dim target As DAOCostoFletexUnd = New DAOCostoFletexUnd() ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaOrigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Producto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodigoProd As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.CostoFletexUnd_SelectxParams_DT(IdTiendaOrigen, IdTiendaDestino, IdProducto, IdLinea, IdSubLinea, Producto, CodigoProd, IdTipoExistencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
