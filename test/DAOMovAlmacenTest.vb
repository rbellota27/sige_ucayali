﻿Imports System.Data

Imports System

Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMovAlmacenTest y se pretende que
'''contenga todas las pruebas unitarias DAOMovAlmacenTest.
'''</summary>
<TestClass()> _
Public Class DAOMovAlmacenTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMovAlmacenConstructorTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaMovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaMovAlmacenTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim movalmacen As MovAlmacen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.ActualizaMovAlmacen(cn, movalmacen, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de CR_KardexProductoXMetVal
    '''</summary>
    <TestMethod()> _
    Public Sub CR_KardexProductoXMetValTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Kardex) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Kardex)
        actual = target.CR_KardexProductoXMetVal(idempresa, idalmacen, idproducto, fechainicio, fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CR_KardexProductoXMetVal
    '''</summary>
    <TestMethod()> _
    Public Sub CR_KardexProductoXMetValTest1()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Igv As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Kardex) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Kardex)
        actual = target.CR_KardexProductoXMetVal(idempresa, idalmacen, idproducto, fechainicio, fechafin, Igv)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdDocumentoTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.DeletexIdDocumento(cn, T, IdDocumento)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdDocumento_1
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdDocumento_1Test()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdDocumento_1(cn, tr, IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaLMovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaLMovAlmacenTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim LMovalmacen As List(Of MovAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaLMovAlmacen(cn, LMovalmacen, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaMovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaMovAlmacenTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim movalmacen As MovAlmacen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaMovAlmacen(cn, movalmacen, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de MovAlmacenUpdate_Cantidad_CostoxIdDetalleDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub MovAlmacenUpdate_Cantidad_CostoxIdDetalleDocumentoTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim Cantidad As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim Costo As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim IdDetalleDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.MovAlmacenUpdate_Cantidad_CostoxIdDetalleDocumento(Cantidad, Costo, IdDetalleDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de RptKardexMovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub RptKardexMovAlmacenTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaini As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RptKardexMovAlmacen(idempresa, idalmacen, idlinea, idsublinea, idproducto, fechaini, fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RptMovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub RptMovAlmacenTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idorigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim iddestino As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idlinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechaini As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RptMovAlmacen(idempresa, idorigen, iddestino, idlinea, idsublinea, fechaini, fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RptMovAlmacenxDoc
    '''</summary>
    <TestMethod()> _
    Public Sub RptMovAlmacenxDocTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idorigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim iddestino As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idlinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechaini As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idtipdoc As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RptMovAlmacenxDoc(idempresa, idorigen, iddestino, idlinea, idsublinea, fechaini, fechafin, idtipdoc)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectIdDocumentoInvInicial
    '''</summary>
    <TestMethod()> _
    Public Sub SelectIdDocumentoInvInicialTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectIdDocumentoInvInicial(IdEmpresa, IdAlmacen, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectMinIdMovAlmacenxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectMinIdMovAlmacenxIdDocumentoTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectMinIdMovAlmacenxIdDocumento(cn, tr, IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxEmpresaxAlmacenxProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxEmpresaxAlmacenxProductoTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovAlmacen)
        actual = target.SelectxEmpresaxAlmacenxProducto(cn, tr, idempresa, idalmacen, idproducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxEmpresaxAlmacenxProductoxMovAlmacenBase
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxEmpresaxAlmacenxProductoxMovAlmacenBaseTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmovalmacenbase As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovAlmacen)
        actual = target.SelectxEmpresaxAlmacenxProductoxMovAlmacenBase(cn, tr, idempresa, idalmacen, idproducto, idmovalmacenbase)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MovAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MovAlmacen)
        actual = target.SelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateCantxAtenderxIdDetalleDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateCantxAtenderxIdDetalleDocumentoTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdDetalleDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cantxatender As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim comprometido As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateCantxAtenderxIdDetalleDocumento(IdDetalleDocumento, cantxatender, comprometido, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de kardexporMetVal
    '''</summary>
    <TestMethod()> _
    Public Sub kardexporMetValTest()
        Dim target As DAOMovAlmacen = New DAOMovAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.kardexporMetVal(idempresa, idalmacen, idproducto, fechainicio, fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
