﻿Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para HelperDAOTest y se pretende que
'''contenga todas las pruebas unitarias HelperDAOTest.
'''</summary>
<TestClass()> _
Public Class HelperDAOTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor HelperDAO
    '''</summary>
    <TestMethod()> _
    Public Sub HelperDAOConstructorTest()
        Dim target As HelperDAO = New HelperDAO()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeletexParam1T
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexParam1TTest()
        Dim target As HelperDAO = New HelperDAO() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim P As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexParam1T(Cn, Sp, P, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexParams
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexParamsTest()
        Dim target As HelperDAO = New HelperDAO() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ap() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexParams(Cn, Sp, Ap)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Insert
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTest()
        Dim target As HelperDAO = New HelperDAO() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ap() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Insert(Cn, Sp, Ap)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Insert_ReturnParameter
    '''</summary>
    <TestMethod()> _
    Public Sub Insert_ReturnParameterTest()
        Dim target As HelperDAO = New HelperDAO() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ap() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Insert_ReturnParameter(Cn, Sp, Ap, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Inserta
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTest()
        Dim target As HelperDAO = New HelperDAO() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ap() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        target.Inserta(Cn, Sp, Ap)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTTest()
        Dim target As HelperDAO = New HelperDAO() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ap() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaT(Cn, Sp, Ap, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaTParameterOutPut
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTParameterOutPutTest()
        Dim target As HelperDAO = New HelperDAO() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ap() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaTParameterOutPut(Cn, Sp, Ap, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Insertation
    '''</summary>
    <TestMethod()> _
    Public Sub InsertationTest()
        Dim target As HelperDAO = New HelperDAO() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ap() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Insertation(Cn, Sp, Ap)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Update
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTest()
        Dim target As HelperDAO = New HelperDAO() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ap() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Update(Cn, Sp, Ap)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateT
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTTest()
        Dim target As HelperDAO = New HelperDAO() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ap() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateT(Cn, Sp, Ap, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateTi
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTiTest()
        Dim target As HelperDAO = New HelperDAO() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ap() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateTi(Cn, Sp, Ap)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarxUnParametro
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarxUnParametroTest()
        Dim target As HelperDAO = New HelperDAO() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Sp As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ap() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarxUnParametro(Cn, Sp, Ap)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
