﻿Imports System.Collections.Generic

Imports System.Data

Imports System

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocReciboCajaTest y se pretende que
'''contenga todas las pruebas unitarias DAODocReciboCajaTest.
'''</summary>
<TestClass()> _
Public Class DAODocReciboCajaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocReciboCaja
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocReciboCajaConstructorTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoReciboEgresoDeshacerMov
    '''</summary>
    <TestMethod(), _
     DeploymentItem("DAO.dll")> _
    Public Sub DocumentoReciboEgresoDeshacerMovTest()
        Dim target As DAODocReciboCaja_Accessor = New DAODocReciboCaja_Accessor() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleConcepto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePagoCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoReciboEgresoDeshacerMov(IdDocumento, DeleteDetalleConcepto, DeleteMovCaja, DeletePagoCaja, DeleteRelacionDocumento, DeleteObservacion, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoReciboIngresoAnular
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoReciboIngresoAnularTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DocumentoReciboIngresoAnular(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoReciboIngresoDeshacerMov_Edicion
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoReciboIngresoDeshacerMov_EdicionTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DocumentoReciboIngresoDeshacerMov_Edicion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoReciboIngreso_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoReciboIngreso_DeshacerMovTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeshacerMovimientos As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovCuenta As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleConcepto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePagoCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovBanco As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoReciboIngreso_DeshacerMov(IdDocumento, DeshacerMovimientos, DeleteMovCuenta, DeleteDetalleConcepto, DeleteMovCaja, DeletePagoCaja, DeleteMovBanco, DeleteRelacionDocumento, DeleteObservacion, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoReciboIngreso_InsertDetalleConcepto
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoReciboIngreso_InsertDetalleConceptoTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja() ' TODO: Inicializar en un valor adecuado
        Dim objDetalleConcepto As DetalleConcepto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoReciboIngreso_InsertDetalleConcepto(objDetalleConcepto, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de RPT_MovCajaxParams_DT
    '''</summary>
    <TestMethod()> _
    Public Sub RPT_MovCajaxParams_DTTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.RPT_MovCajaxParams_DT(IdEmpresa, Idtienda, IdCaja, FechaInicio, FechaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReciboCajaEgresoAnular
    '''</summary>
    <TestMethod()> _
    Public Sub ReciboCajaEgresoAnularTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ReciboCajaEgresoAnular(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReciboCajaEgresoInsert
    '''</summary>
    <TestMethod()> _
    Public Sub ReciboCajaEgresoInsertTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja() ' TODO: Inicializar en un valor adecuado
        Dim objDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalleConcepto As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objMovCaja As MovCaja = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaCancelacion As List(Of PagoCaja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objRelacionDocumento As RelacionDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObservaciones As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ReciboCajaEgresoInsert(objDocumento, listaDetalleConcepto, objMovCaja, listaCancelacion, objRelacionDocumento, objObservaciones)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReciboCajaEgresoUpdate
    '''</summary>
    <TestMethod()> _
    Public Sub ReciboCajaEgresoUpdateTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja() ' TODO: Inicializar en un valor adecuado
        Dim objDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalleConcepto As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objMovCaja As MovCaja = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaCancelacion As List(Of PagoCaja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objRelacionDocumento As RelacionDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObservaciones As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ReciboCajaEgresoUpdate(objDocumento, listaDetalleConcepto, objMovCaja, listaCancelacion, objRelacionDocumento, objObservaciones)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReciboCajaIngresoInsert
    '''</summary>
    <TestMethod()> _
    Public Sub ReciboCajaIngresoInsertTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja() ' TODO: Inicializar en un valor adecuado
        Dim objDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalleConcepto As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objMovCaja As MovCaja = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaCancelacion As List(Of PagoCaja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObservaciones As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaMovBanco As List(Of MovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaRelacionDocumento As List(Of RelacionDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ReciboCajaIngresoInsert(objDocumento, listaDetalleConcepto, objMovCaja, listaCancelacion, objObservaciones, listaMovBanco, listaRelacionDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReciboCajaIngresoUpdate
    '''</summary>
    <TestMethod()> _
    Public Sub ReciboCajaIngresoUpdateTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja() ' TODO: Inicializar en un valor adecuado
        Dim objDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalleConcepto As List(Of DetalleConcepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objMovCaja As MovCaja = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaCancelacion As List(Of PagoCaja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObservaciones As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaMovBanco As List(Of MovBancoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaRelacionDocumento As List(Of RelacionDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ReciboCajaIngresoUpdate(objDocumento, listaDetalleConcepto, objMovCaja, listaCancelacion, objObservaciones, listaMovBanco, listaRelacionDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectResumenMontoxSustentar_DT
    '''</summary>
    <TestMethod()> _
    Public Sub SelectResumenMontoxSustentar_DTTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.SelectResumenMontoxSustentar_DT(IdEmpresa, IdTienda, IdCaja)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_DocReciboEgreso
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_DocReciboEgresoTest()
        Dim target As DAODocReciboCaja = New DAODocReciboCaja() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_DocReciboEgreso(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
