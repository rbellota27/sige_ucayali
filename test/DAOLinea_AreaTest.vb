﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOLinea_AreaTest y se pretende que
'''contenga todas las pruebas unitarias DAOLinea_AreaTest.
'''</summary>
<TestClass()> _
Public Class DAOLinea_AreaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOLinea_Area
    '''</summary>
    <TestMethod()> _
    Public Sub DAOLinea_AreaConstructorTest()
        Dim target As DAOLinea_Area = New DAOLinea_Area()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de InsertLinea_Area
    '''</summary>
    <TestMethod()> _
    Public Sub InsertLinea_AreaTest()
        Dim target As DAOLinea_Area = New DAOLinea_Area() ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Lista As List(Of Linea_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertLinea_Area(cnx, IdLinea, Lista, trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Insert_TipoExist_Linea_Area
    '''</summary>
    <TestMethod()> _
    Public Sub Insert_TipoExist_Linea_AreaTest()
        Dim target As DAOLinea_Area = New DAOLinea_Area() ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Lista As List(Of Linea_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Insert_TipoExist_Linea_Area(cnx, IdTipoExistencia, Lista, trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarLineaxArea
    '''</summary>
    <TestMethod()> _
    Public Sub ListarLineaxAreaTest()
        Dim target As DAOLinea_Area = New DAOLinea_Area() ' TODO: Inicializar en un valor adecuado
        Dim idArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea)
        actual = target.ListarLineaxArea(idArea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarLineaxTipoexistenciaxArea
    '''</summary>
    <TestMethod()> _
    Public Sub ListarLineaxTipoexistenciaxAreaTest()
        Dim target As DAOLinea_Area = New DAOLinea_Area() ' TODO: Inicializar en un valor adecuado
        Dim idArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea)
        actual = target.ListarLineaxTipoexistenciaxArea(idArea, idTipoExistencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarTipoexistenciaxArea
    '''</summary>
    <TestMethod()> _
    Public Sub ListarTipoexistenciaxAreaTest()
        Dim target As DAOLinea_Area = New DAOLinea_Area() ' TODO: Inicializar en un valor adecuado
        Dim idArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoExistencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoExistencia)
        actual = target.ListarTipoexistenciaxArea(idArea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectLinea_Area
    '''</summary>
    <TestMethod()> _
    Public Sub SelectLinea_AreaTest()
        Dim target As DAOLinea_Area = New DAOLinea_Area() ' TODO: Inicializar en un valor adecuado
        Dim idLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea_Area)
        actual = target.SelectLinea_Area(idLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectLinea_Area_TipoExist
    '''</summary>
    <TestMethod()> _
    Public Sub SelectLinea_Area_TipoExistTest()
        Dim target As DAOLinea_Area = New DAOLinea_Area() ' TODO: Inicializar en un valor adecuado
        Dim idTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea_Area)
        actual = target.SelectLinea_Area_TipoExist(idTipoExistencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateLinea_Area
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateLinea_AreaTest()
        Dim target As DAOLinea_Area = New DAOLinea_Area() ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Lista As List(Of Linea_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateLinea_Area(cnx, idLinea, Lista, trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
