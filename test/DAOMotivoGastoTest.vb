﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMotivoGastoTest y se pretende que
'''contenga todas las pruebas unitarias DAOMotivoGastoTest.
'''</summary>
<TestClass()> _
Public Class DAOMotivoGastoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMotivoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMotivoGastoConstructorTest()
        Dim target As DAOMotivoGasto = New DAOMotivoGasto()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaMotivoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaMotivoGastoTest()
        Dim target As DAOMotivoGasto = New DAOMotivoGasto() ' TODO: Inicializar en un valor adecuado
        Dim MotivoGasto As MotivoGasto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaMotivoGasto(MotivoGasto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaMotivoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaMotivoGastoTest()
        Dim target As DAOMotivoGasto = New DAOMotivoGasto() ' TODO: Inicializar en un valor adecuado
        Dim MotivoGasto As MotivoGasto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaMotivoGasto(MotivoGasto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoMotivoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoMotivoGastoTest()
        Dim target As DAOMotivoGasto = New DAOMotivoGasto() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MotivoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MotivoGasto)
        actual = target.SelectAllActivoMotivoGasto
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivoMotivoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoMotivoGastoTest()
        Dim target As DAOMotivoGasto = New DAOMotivoGasto() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MotivoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MotivoGasto)
        actual = target.SelectAllInactivoMotivoGasto
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllMotivoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllMotivoGastoTest()
        Dim target As DAOMotivoGasto = New DAOMotivoGasto() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MotivoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MotivoGasto)
        actual = target.SelectAllMotivoGasto
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAOMotivoGasto = New DAOMotivoGasto() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MotivoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MotivoGasto)
        actual = target.SelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombreActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreActivoTest()
        Dim target As DAOMotivoGasto = New DAOMotivoGasto() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MotivoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MotivoGasto)
        actual = target.SelectAllxNombreActivo(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombreInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreInactivoTest()
        Dim target As DAOMotivoGasto = New DAOMotivoGasto() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MotivoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MotivoGasto)
        actual = target.SelectAllxNombreInactivo(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxConceptoxMotivoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxConceptoxMotivoGastoTest()
        Dim target As DAOMotivoGasto = New DAOMotivoGasto() ' TODO: Inicializar en un valor adecuado
        Dim IdConcepto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MotivoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MotivoGasto)
        actual = target.SelectxConceptoxMotivoGasto(IdConcepto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOMotivoGasto = New DAOMotivoGasto() ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MotivoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MotivoGasto)
        actual = target.SelectxId(Codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
