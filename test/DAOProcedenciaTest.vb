﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOProcedenciaTest y se pretende que
'''contenga todas las pruebas unitarias DAOProcedenciaTest.
'''</summary>
<TestClass()> _
Public Class DAOProcedenciaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOProcedencia
    '''</summary>
    <TestMethod()> _
    Public Sub DAOProcedenciaConstructorTest()
        Dim target As DAOProcedencia = New DAOProcedencia()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Insert
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTest()
        Dim target As DAOProcedencia = New DAOProcedencia() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim obj As Procedencia = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Insert(cn, obj, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de PerfilUsuarioSelectxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub PerfilUsuarioSelectxIdPersonaTest()
        Dim target As DAOProcedencia = New DAOProcedencia() ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Procedencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Procedencia)
        actual = target.PerfilUsuarioSelectxIdPersona(idpersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo_Cbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivo_CboTest()
        Dim target As DAOProcedencia = New DAOProcedencia() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Procedencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Procedencia)
        actual = target.SelectAllActivo_Cbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoxIdPais
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoxIdPaisTest()
        Dim target As DAOProcedencia = New DAOProcedencia() ' TODO: Inicializar en un valor adecuado
        Dim idpais As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Procedencia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Procedencia)
        actual = target.SelectAllActivoxIdPais(idpais)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
