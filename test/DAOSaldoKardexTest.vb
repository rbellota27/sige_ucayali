﻿Imports System

Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOSaldoKardexTest y se pretende que
'''contenga todas las pruebas unitarias DAOSaldoKardexTest.
'''</summary>
<TestClass()> _
Public Class DAOSaldoKardexTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOSaldoKardex
    '''</summary>
    <TestMethod()> _
    Public Sub DAOSaldoKardexConstructorTest()
        Dim target As DAOSaldoKardex = New DAOSaldoKardex()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaSaldoKardex
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaSaldoKardexTest()
        Dim target As DAOSaldoKardex = New DAOSaldoKardex() ' TODO: Inicializar en un valor adecuado
        Dim saldokardex As SaldoKardex = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaSaldoKardex(saldokardex)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdDocumentoTest()
        Dim target As DAOSaldoKardex = New DAOSaldoKardex() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdDocumento(cn, tr, iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaSaldoKardex
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaSaldoKardexTest()
        Dim target As DAOSaldoKardex = New DAOSaldoKardex() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim saldokardex As SaldoKardex = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaSaldoKardex(cn, saldokardex, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectUltimoSaldoKardexxIdMovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub SelectUltimoSaldoKardexxIdMovAlmacenTest()
        Dim target As DAOSaldoKardex = New DAOSaldoKardex() ' TODO: Inicializar en un valor adecuado
        Dim IdMovAlmacenBase As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As SaldoKardex = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As SaldoKardex
        actual = target.SelectUltimoSaldoKardexxIdMovAlmacen(IdMovAlmacenBase, IdProducto, IdEmpresa, IdAlmacen, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxEmpresaxAlmacenxProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxEmpresaxAlmacenxProductoTest()
        Dim target As DAOSaldoKardex = New DAOSaldoKardex() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As SaldoKardex = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As SaldoKardex
        actual = target.SelectxEmpresaxAlmacenxProducto(cn, IdEmpresa, IdAlmacen, IdProducto, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProductoTest()
        Dim target As DAOSaldoKardex = New DAOSaldoKardex() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of SaldoKardex) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of SaldoKardex)
        actual = target.SelectxIdProducto(cn, IdProducto, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateCantidadCostoxIdMovAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateCantidadCostoxIdMovAlmacenTest()
        Dim target As DAOSaldoKardex = New DAOSaldoKardex() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idmovalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cantidad As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim costo As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateCantidadCostoxIdMovAlmacen(cn, tr, idmovalmacen, cantidad, costo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
