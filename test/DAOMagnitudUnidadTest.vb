﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMagnitudUnidadTest y se pretende que
'''contenga todas las pruebas unitarias DAOMagnitudUnidadTest.
'''</summary>
<TestClass()> _
Public Class DAOMagnitudUnidadTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMagnitudUnidad
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMagnitudUnidadConstructorTest()
        Dim target As DAOMagnitudUnidad = New DAOMagnitudUnidad()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaMagnitudUnidad
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaMagnitudUnidadTest()
        Dim target As DAOMagnitudUnidad = New DAOMagnitudUnidad() ' TODO: Inicializar en un valor adecuado
        Dim UpdateMU As List(Of MagnitudUnidad) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaMagnitudUnidad(UpdateMU)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdMagnitud
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdMagnitudTest()
        Dim target As DAOMagnitudUnidad = New DAOMagnitudUnidad() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idMagnitud As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdMagnitud(cn, tr, idMagnitud)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaMagnitudUnidad
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaMagnitudUnidadTest()
        Dim target As DAOMagnitudUnidad = New DAOMagnitudUnidad() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of MagnitudUnidad) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdMagnitud As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.InsertaMagnitudUnidad(cn, tr, lista, IdMagnitud)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboUM
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboUMTest()
        Dim target As DAOMagnitudUnidad = New DAOMagnitudUnidad() ' TODO: Inicializar en un valor adecuado
        Dim IdMagnitud As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of UnidadMedida) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of UnidadMedida)
        actual = target.SelectCboUM(IdMagnitud)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de _MagnitudUnidadselectxIdMagnitud
    '''</summary>
    <TestMethod()> _
    Public Sub _MagnitudUnidadselectxIdMagnitudTest()
        Dim target As DAOMagnitudUnidad = New DAOMagnitudUnidad() ' TODO: Inicializar en un valor adecuado
        Dim IdMagnitud As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MagnitudUnidad) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MagnitudUnidad)
        actual = target._MagnitudUnidadselectxIdMagnitud(IdMagnitud)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
