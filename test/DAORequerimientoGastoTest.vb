﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data

Imports System

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAORequerimientoGastoTest y se pretende que
'''contenga todas las pruebas unitarias DAORequerimientoGastoTest.
'''</summary>
<TestClass()> _
Public Class DAORequerimientoGastoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAORequerimientoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub DAORequerimientoGastoConstructorTest()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeshacerDocumentoRequerimientoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub DeshacerDocumentoRequerimientoGastoTest()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeleteAnexoDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleConcepto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovCuentaCxP As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim listaRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeshacerDocumentoRequerimientoGasto(IdDocumento, DeleteAnexoDocumento, DeleteDetalleConcepto, DeleteMovCuentaCxP, DeleteObservacion, Anular, listaRelacionDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoRequerimientoGasto_SelectAprobacion
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoRequerimientoGasto_SelectAprobacionTest()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Opcion_Aprobacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim MontoTot As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.DocumentoRequerimientoGasto_SelectAprobacion(IdEmpresa, IdTienda, IdArea, Opcion_Aprobacion, FechaInicio, FechaFin, MontoTot)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoRequerimientoGasto_UpdateMontoxSustentar
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoRequerimientoGasto_UpdateMontoxSustentarTest()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRG As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoRequerimientoGasto_UpdateMontoxSustentar(IdDocumentoRG, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertDetRequerimientoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub InsertDetRequerimientoGastoTest()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Lista As List(Of RequerimientoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertDetRequerimientoGasto(idDocumento, Lista, cnx, trx)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertRequerimientoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub InsertRequerimientoGastoTest()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto() ' TODO: Inicializar en un valor adecuado
        Dim Obj As RequerimientoGasto.CabeceraRequerimiento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Lista As List(Of RequerimientoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObs As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.InsertRequerimientoGasto(Obj, Lista, objObs)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RequerimientoGasto_Select_Aprobar
    '''</summary>
    <TestMethod()> _
    Public Sub RequerimientoGasto_Select_AprobarTest()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idarea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim autorizadas As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of RequerimientoGasto.CabeceraRequerimiento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of RequerimientoGasto.CabeceraRequerimiento)
        actual = target.RequerimientoGasto_Select_Aprobar(idempresa, Idtienda, idarea, autorizadas)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateRequerimientoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateRequerimientoGastoTest()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto() ' TODO: Inicializar en un valor adecuado
        Dim Obj As RequerimientoGasto.CabeceraRequerimiento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Lista As List(Of RequerimientoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObs As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.UpdateRequerimientoGasto(Obj, Lista, objObs)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValSustento_RelacionDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub ValSustento_RelacionDocumentoTest()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRG As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.ValSustento_RelacionDocumento(IdDocumentoRG, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de fx_getTotalSustentadoxIdDocumentoReqGasto
    '''</summary>
    <TestMethod()> _
    Public Sub fx_getTotalSustentadoxIdDocumentoReqGastoTest()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento_RG As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.fx_getTotalSustentadoxIdDocumentoReqGasto(IdDocumento_RG)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSet_DocRequeGastoV2
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSet_DocRequeGastoV2Test()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSet_DocRequeGastoV2(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de selectListaRequerimientoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub selectListaRequerimientoGastoTest()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of RequerimientoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of RequerimientoGasto)
        actual = target.selectListaRequerimientoGasto(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de selectRequerimientoGasto
    '''</summary>
    <TestMethod()> _
    Public Sub selectRequerimientoGastoTest()
        Dim target As DAORequerimientoGasto = New DAORequerimientoGasto() ' TODO: Inicializar en un valor adecuado
        Dim idserie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As RequerimientoGasto.CabeceraRequerimiento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As RequerimientoGasto.CabeceraRequerimiento
        actual = target.selectRequerimientoGasto(idserie, codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
