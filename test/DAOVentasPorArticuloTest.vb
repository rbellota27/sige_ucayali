﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOVentasPorArticuloTest y se pretende que
'''contenga todas las pruebas unitarias DAOVentasPorArticuloTest.
'''</summary>
<TestClass()> _
Public Class DAOVentasPorArticuloTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOVentasPorArticulo
    '''</summary>
    <TestMethod()> _
    Public Sub DAOVentasPorArticuloConstructorTest()
        Dim target As DAOVentasPorArticulo = New DAOVentasPorArticulo()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de CRVentasPorArticulos
    '''</summary>
    <TestMethod()> _
    Public Sub CRVentasPorArticulosTest()
        Dim target As DAOVentasPorArticulo = New DAOVentasPorArticulo() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim yeari As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanai As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim yearf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanaf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim filtrarsemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.CRVentasPorArticulos(idEmpresa, idtienda, idlinea, idSublinea, idProducto, fechainicio, fechafin, yeari, semanai, yearf, semanaf, filtrarsemana)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CRVentasPorArticulosDetallladoValorizado
    '''</summary>
    <TestMethod()> _
    Public Sub CRVentasPorArticulosDetallladoValorizadoTest()
        Dim target As DAOVentasPorArticulo = New DAOVentasPorArticulo() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim yeari As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanai As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim yearf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanaf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim filtrarsemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.CRVentasPorArticulosDetallladoValorizado(idEmpresa, idtienda, IdTipoExistencia, idlinea, idSublinea, idProducto, fechainicio, fechafin, yeari, semanai, yearf, semanaf, filtrarsemana)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CRVentasPorArticulosResumido
    '''</summary>
    <TestMethod()> _
    Public Sub CRVentasPorArticulosResumidoTest()
        Dim target As DAOVentasPorArticulo = New DAOVentasPorArticulo() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim yeari As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanai As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim yearf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanaf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim filtrarsemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.CRVentasPorArticulosResumido(idEmpresa, idtienda, idlinea, idSublinea, idProducto, fechainicio, fechafin, yeari, semanai, yearf, semanaf, filtrarsemana)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CRVentasPorArticulosResumidoValorizado
    '''</summary>
    <TestMethod()> _
    Public Sub CRVentasPorArticulosResumidoValorizadoTest()
        Dim target As DAOVentasPorArticulo = New DAOVentasPorArticulo() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim yeari As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanai As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim yearf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanaf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim filtrarsemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.CRVentasPorArticulosResumidoValorizado(idEmpresa, idtienda, idlinea, idSublinea, idProducto, fechainicio, fechafin, yeari, semanai, yearf, semanaf, filtrarsemana)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CRVtasProductoProm
    '''</summary>
    <TestMethod()> _
    Public Sub CRVtasProductoPromTest()
        Dim target As DAOVentasPorArticulo = New DAOVentasPorArticulo() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim ListaProducto As List(Of ProductoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim yeari As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanai As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim yearf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanaf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim filtrarsemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.CRVtasProductoProm(idEmpresa, idtienda, idDocumento, ListaProducto, fechainicio, fechafin, yeari, semanai, yearf, semanaf, filtrarsemana, idpersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReporteListaPrecios
    '''</summary>
    <TestMethod()> _
    Public Sub ReporteListaPreciosTest()
        Dim target As DAOVentasPorArticulo = New DAOVentasPorArticulo() ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idtipoMedida As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.ReporteListaPrecios(idtienda, idlinea, idSublinea, codSublinea, idproducto, descripcion, idtipoMedida, idmoneda, tabla)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getNameProcedure
    '''</summary>
    <TestMethod()> _
    Public Sub getNameProcedureTest()
        Dim target As DAOVentasPorArticulo = New DAOVentasPorArticulo() ' TODO: Inicializar en un valor adecuado
        Dim idtipoprecio As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.getNameProcedure(idtipoprecio)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
