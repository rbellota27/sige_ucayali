﻿Imports System.Data

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMovCajaTest y se pretende que
'''contenga todas las pruebas unitarias DAOMovCajaTest.
'''</summary>
<TestClass()> _
Public Class DAOMovCajaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMovCaja
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMovCajaConstructorTest()
        Dim target As DAOMovCaja = New DAOMovCaja()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaMovCaja
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaMovCajaTest()
        Dim target As DAOMovCaja = New DAOMovCaja() ' TODO: Inicializar en un valor adecuado
        Dim movcaja As MovCaja = Nothing ' TODO: Inicializar en un valor adecuado
        target.ActualizaMovCaja(movcaja)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdDocumentoTest()
        Dim target As DAOMovCaja = New DAOMovCaja() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.DeletexIdDocumento(cn, T, IdDocumento)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de IngresosDiariosxParams
    '''</summary>
    <TestMethod()> _
    Public Sub IngresosDiariosxParamsTest()
        Dim target As DAOMovCaja = New DAOMovCaja() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.IngresosDiariosxParams(IdTienda, IdEmpresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaMovCaja
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaMovCajaTest()
        Dim target As DAOMovCaja = New DAOMovCaja() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim movcaja As MovCaja = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaMovCaja(Cn, movcaja, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de fnInsTblMovCaja
    '''</summary>
    <TestMethod()> _
    Public Sub fnInsTblMovCajaTest()
        Dim target As DAOMovCaja = New DAOMovCaja() ' TODO: Inicializar en un valor adecuado
        Dim poBETblMovCaja As MovCaja = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.fnInsTblMovCaja(poBETblMovCaja)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
