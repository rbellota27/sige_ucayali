﻿Imports System

Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoAjusteInventarioTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoAjusteInventarioTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoAjusteInventarioTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoAjusteInventario
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoAjusteInventarioConstructorTest()
        Dim target As DAODocumentoAjusteInventario = New DAODocumentoAjusteInventario()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoAjusteInventario_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoAjusteInventario_DeshacerMovTest()
        Dim target As DAODocumentoAjusteInventario = New DAODocumentoAjusteInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMovAlmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDocumento_Persona As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservaciones As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoAjusteInventario_DeshacerMov(IdDocumento, DeleteDetalleDocumento, DeleteMovAlmacen, DeleteDocumento_Persona, DeleteRelacionDocumento, DeleteObservaciones, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoAjusteInventario_FiltroDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoAjusteInventario_FiltroDetalleTest()
        Dim target As DAODocumentoAjusteInventario = New DAODocumentoAjusteInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoTomaInv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodigoSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Producto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdtipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumentoTomaInventario) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumentoTomaInventario)
        actual = target.DocumentoAjusteInventario_FiltroDetalle(IdDocumento, IdDocumentoTomaInv, IdLinea, IdSubLinea, CodigoSubLinea, Producto, CodigoProducto, IdtipoExistencia, PageIndex, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoAjusteInventario_SelectDocRef
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoAjusteInventario_SelectDocRefTest()
        Dim target As DAODocumentoAjusteInventario = New DAODocumentoAjusteInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim iDTIPOdocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoAjusteInventario_SelectDocRef(IdEmpresa, IdTienda, IdAlmacen, Serie, Codigo, FechaInicio, FechaFin, iDTIPOdocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de EliminarProcesoxIdProductoxIdDocumentoAjusteInv
    '''</summary>
    <TestMethod()> _
    Public Sub EliminarProcesoxIdProductoxIdDocumentoAjusteInvTest()
        Dim target As DAODocumentoAjusteInventario = New DAODocumentoAjusteInventario() ' TODO: Inicializar en un valor adecuado
        Dim IdDocAjusteInv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.EliminarProcesoxIdProductoxIdDocumentoAjusteInv(IdDocAjusteInv, IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
