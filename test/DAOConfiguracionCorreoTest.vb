﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOConfiguracionCorreoTest y se pretende que
'''contenga todas las pruebas unitarias DAOConfiguracionCorreoTest.
'''</summary>
<TestClass()> _
Public Class DAOConfiguracionCorreoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOConfiguracionCorreo
    '''</summary>
    <TestMethod()> _
    Public Sub DAOConfiguracionCorreoConstructorTest()
        Dim target As DAOConfiguracionCorreo = New DAOConfiguracionCorreo()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ConfiguracionCorreo_select
    '''</summary>
    <TestMethod()> _
    Public Sub ConfiguracionCorreo_selectTest()
        Dim target As DAOConfiguracionCorreo = New DAOConfiguracionCorreo() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ConfiguracionCorreo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ConfiguracionCorreo)
        actual = target.ConfiguracionCorreo_select(IdEmpresa, IdTienda, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de _ConfiguracionCorreoTransaction
    '''</summary>
    <TestMethod()> _
    Public Sub _ConfiguracionCorreoTransactionTest()
        Dim target As DAOConfiguracionCorreo = New DAOConfiguracionCorreo() ' TODO: Inicializar en un valor adecuado
        Dim obj As ConfiguracionCorreo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim getList As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ConfiguracionCorreo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ConfiguracionCorreo)
        actual = target._ConfiguracionCorreoTransaction(obj, getList)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
