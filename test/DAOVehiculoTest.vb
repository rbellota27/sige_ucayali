﻿Imports System.Data

Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOVehiculoTest y se pretende que
'''contenga todas las pruebas unitarias DAOVehiculoTest.
'''</summary>
<TestClass()> _
Public Class DAOVehiculoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOVehiculo
    '''</summary>
    <TestMethod()> _
    Public Sub DAOVehiculoConstructorTest()
        Dim target As DAOVehiculo = New DAOVehiculo()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaVehiculo
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaVehiculoTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim vehiculo As Vehiculo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaVehiculo(vehiculo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaVehiculoValidadoxPlaca
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaVehiculoValidadoxPlacaTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim Vehiculo As Vehiculo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ActualizaVehiculoValidadoxPlaca(Vehiculo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeleteVehiculoxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DeleteVehiculoxIdDocumentoTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim Vehiculo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.DeleteVehiculoxIdDocumento(Vehiculo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaEstadoVehiculo
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaEstadoVehiculoTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim EstadoVehiculo As Vehiculo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaEstadoVehiculo(EstadoVehiculo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaIdProductoVehiculo
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaIdProductoVehiculoTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim EstVehiculo As Vehiculo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaIdProductoVehiculo(EstVehiculo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaVehiculo
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaVehiculoTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim vehiculo As Vehiculo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaVehiculo(vehiculo, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaVehiculoValidadoxPlaca
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaVehiculoValidadoxPlacaTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim Vehiculo As Vehiculo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaVehiculoValidadoxPlaca(Vehiculo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelecFiltradoGrilla
    '''</summary>
    <TestMethod()> _
    Public Sub SelecFiltradoGrillaTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim placa As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Vehiculo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Vehiculo)
        actual = target.SelecFiltradoGrilla(placa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SeleccionarTransporte
    '''</summary>
    <TestMethod()> _
    Public Sub SeleccionarTransporteTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of be_transportista) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of be_transportista)
        actual = target.SeleccionarTransporte(cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SeleccionarTransportistas
    '''</summary>
    <TestMethod()> _
    Public Sub SeleccionarTransportistasTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.SeleccionarTransportistas
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SeleccionarTransportistas_prueba
    '''</summary>
    <TestMethod()> _
    Public Sub SeleccionarTransportistas_pruebaTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim inicial As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nroPaginas As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.SeleccionarTransportistas_prueba(inicial, nroPaginas)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNroPlaca
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNroPlacaTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim placa As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Vehiculo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Vehiculo)
        actual = target.SelectActivoxNroPlaca(placa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllVehiculos
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllVehiculosTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Vehiculo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Vehiculo)
        actual = target.SelectAllVehiculos
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllVehiculosXtipoexistenciaXlineaXsublinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllVehiculosXtipoexistenciaXlineaXsublineaTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Vehiculo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Vehiculo)
        actual = target.SelectAllVehiculosXtipoexistenciaXlineaXsublinea
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboEstadoVehiculo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboEstadoVehiculoTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of EstadoComboVehiculo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of EstadoComboVehiculo)
        actual = target.SelectCboEstadoVehiculo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboTipoExistencia
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTipoExistenciaTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Vehiculo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Vehiculo)
        actual = target.SelectCboTipoExistencia
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaVehiculo_Buscar
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaVehiculo_BuscarTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim Placa As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Modelo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Constancia As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim EstadoPro As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Vehiculo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Vehiculo)
        actual = target.SelectGrillaVehiculo_Buscar(Placa, Modelo, Constancia, EstadoPro)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdVehiculo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdVehiculoTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim IdVehiculo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Vehiculo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Vehiculo
        actual = target.SelectxIdVehiculo(IdVehiculo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de seleccionarPlacasActivas
    '''</summary>
    <TestMethod()> _
    Public Sub seleccionarPlacasActivasTest()
        Dim target As DAOVehiculo = New DAOVehiculo() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Vehiculo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Vehiculo)
        actual = target.seleccionarPlacasActivas(cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
