﻿Imports System.Data

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoViewTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoViewTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoViewTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoView
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoViewConstructorTest()
        Dim target As DAODocumentoView = New DAODocumentoView()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoBuscarDocxSeriexCodigo
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoBuscarDocxSeriexCodigoTest()
        Dim target As DAODocumentoView = New DAODocumentoView() ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoBuscarDocxSeriexCodigo(serie, codigo, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoDetalleTest()
        Dim target As DAODocumentoView = New DAODocumentoView() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipodoc As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idestadoDoc As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim persona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim rango As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim resumen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FiltrarSemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim YearIni As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim SemanaIni As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim YearFin As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim SemanaFin As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.DocumentoDetalle(idempresa, idtienda, idmoneda, idtipodoc, idestadoDoc, fechainicio, fechafin, persona, serie, codigo, rango, resumen, FiltrarSemana, YearIni, SemanaIni, YearFin, SemanaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoDetallexMes
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoDetallexMesTest()
        Dim target As DAODocumentoView = New DAODocumentoView() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipodoc As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idestadoDoc As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim persona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim rango As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim resumen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FiltrarSemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim YearIni As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim SemanaIni As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim YearFin As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim SemanaFin As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.DocumentoDetallexMes(idempresa, idtienda, idmoneda, idtipodoc, idestadoDoc, fechainicio, fechafin, persona, serie, codigo, rango, resumen, FiltrarSemana, YearIni, SemanaIni, YearFin, SemanaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Procedimiento
    '''</summary>
    <TestMethod(), _
     DeploymentItem("DAO.dll")> _
    Public Sub ProcedimientoTest()
        Dim target As DAODocumentoView_Accessor = New DAODocumentoView_Accessor() ' TODO: Inicializar en un valor adecuado
        Dim idtipodoc As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim resumen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.Procedimiento(idtipodoc, tipo, resumen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ProcedimientoxMes
    '''</summary>
    <TestMethod(), _
     DeploymentItem("DAO.dll")> _
    Public Sub ProcedimientoxMesTest()
        Dim target As DAODocumentoView_Accessor = New DAODocumentoView_Accessor() ' TODO: Inicializar en un valor adecuado
        Dim idtipodoc As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim resumen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.ProcedimientoxMes(idtipodoc, tipo, resumen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCantidadFilasDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCantidadFilasDocumentoTest()
        Dim target As DAODocumentoView = New DAODocumentoView() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectCantidadFilasDocumento(idEmpresa, idTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCantidadFilasDocumentoProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCantidadFilasDocumentoProductoTest()
        Dim target As DAODocumentoView = New DAODocumentoView() ' TODO: Inicializar en un valor adecuado
        Dim IdsProductos As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectCantidadFilasDocumentoProducto(IdsProductos)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocumMontDiferentes
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocumMontDiferentesTest()
        Dim target As DAODocumentoView = New DAODocumentoView() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.SelectDocumMontDiferentes(idEmpresa, idTienda, FechaInicio, fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocumUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocumUsuarioTest()
        Dim target As DAODocumentoView = New DAODocumentoView() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idtipodocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim ascdes As Short = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.SelectDocumUsuario(idEmpresa, idTienda, FechaInicio, fechafin, idtipodocumento, idTipo, ascdes)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocumentMediosPagos
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocumentMediosPagosTest()
        Dim target As DAODocumentoView = New DAODocumentoView() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.SelectDocumentMediosPagos(idEmpresa, idTienda, FechaInicio, fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
