﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOPermisoTest y se pretende que
'''contenga todas las pruebas unitarias DAOPermisoTest.
'''</summary>
<TestClass()> _
Public Class DAOPermisoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOPermiso
    '''</summary>
    <TestMethod()> _
    Public Sub DAOPermisoConstructorTest()
        Dim target As DAOPermiso = New DAOPermiso()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de PermisoSelectxNombrePermiso
    '''</summary>
    <TestMethod()> _
    Public Sub PermisoSelectxNombrePermisoTest()
        Dim target As DAOPermiso = New DAOPermiso() ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Perfil) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Perfil)
        actual = target.PermisoSelectxNombrePermiso(IdPerfil)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboArea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboAreaTest()
        Dim target As DAOPermiso = New DAOPermiso() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Area)
        actual = target.SelectCboArea
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdAreaxIdPerfil
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdAreaxIdPerfilTest()
        Dim target As DAOPermiso = New DAOPermiso() ' TODO: Inicializar en un valor adecuado
        Dim IdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Perfil_Permiso) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Perfil_Permiso)
        actual = target.SelectxIdAreaxIdPerfil(IdArea, IdPerfil)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
