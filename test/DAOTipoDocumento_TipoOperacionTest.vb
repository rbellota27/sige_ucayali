﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTipoDocumento_TipoOperacionTest y se pretende que
'''contenga todas las pruebas unitarias DAOTipoDocumento_TipoOperacionTest.
'''</summary>
<TestClass()> _
Public Class DAOTipoDocumento_TipoOperacionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTipoDocumento_TipoOperacion
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTipoDocumento_TipoOperacionConstructorTest()
        Dim target As DAOTipoDocumento_TipoOperacion = New DAOTipoDocumento_TipoOperacion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaTipoDocumento_TipoOperacion
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaTipoDocumento_TipoOperacionTest()
        Dim target As DAOTipoDocumento_TipoOperacion = New DAOTipoDocumento_TipoOperacion() ' TODO: Inicializar en un valor adecuado
        Dim UpdateTipoDocOpera As List(Of TipoDocumento_TipoOperacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaTipoDocumento_TipoOperacion(UpdateTipoDocOpera)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdDocumentoTest()
        Dim target As DAOTipoDocumento_TipoOperacion = New DAOTipoDocumento_TipoOperacion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdDocumento(cn, tr, idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaTipoDocumento_TipoOperacion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTipoDocumento_TipoOperacionTest()
        Dim target As DAOTipoDocumento_TipoOperacion = New DAOTipoDocumento_TipoOperacion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of TipoDocumento_TipoOperacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.InsertaTipoDocumento_TipoOperacion(cn, tr, lista, IdTipoDocumento)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboTipoOperacion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTipoOperacionTest()
        Dim target As DAOTipoDocumento_TipoOperacion = New DAOTipoDocumento_TipoOperacion() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento_TipoOperacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento_TipoOperacion)
        actual = target.SelectCboTipoOperacion
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de _TipoDocumento_TipoOperacionSelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub _TipoDocumento_TipoOperacionSelectxIdDocumentoTest()
        Dim target As DAOTipoDocumento_TipoOperacion = New DAOTipoDocumento_TipoOperacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento_TipoOperacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento_TipoOperacion)
        actual = target._TipoDocumento_TipoOperacionSelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
