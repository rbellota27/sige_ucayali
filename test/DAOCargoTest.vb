﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCargoTest y se pretende que
'''contenga todas las pruebas unitarias DAOCargoTest.
'''</summary>
<TestClass()> _
Public Class DAOCargoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCargo
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCargoConstructorTest()
        Dim target As DAOCargo = New DAOCargo()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaCargo
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaCargoTest()
        Dim target As DAOCargo = New DAOCargo() ' TODO: Inicializar en un valor adecuado
        Dim cargo As Cargo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaCargo(cargo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaCargo
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaCargoTest()
        Dim target As DAOCargo = New DAOCargo() ' TODO: Inicializar en un valor adecuado
        Dim cargo As Cargo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaCargo(cargo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNombreTest()
        Dim target As DAOCargo = New DAOCargo() ' TODO: Inicializar en un valor adecuado
        Dim Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Cargo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Cargo)
        actual = target.SelectActivoxNombre(Nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOCargo = New DAOCargo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Cargo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Cargo)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOCargo = New DAOCargo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Cargo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Cargo)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOCargo = New DAOCargo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Cargo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Cargo)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAOCargo = New DAOCargo() ' TODO: Inicializar en un valor adecuado
        Dim Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Cargo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Cargo)
        actual = target.SelectAllxNombre(Nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTest()
        Dim target As DAOCargo = New DAOCargo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Cargo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Cargo)
        actual = target.SelectCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxNombreTest()
        Dim target As DAOCargo = New DAOCargo() ' TODO: Inicializar en un valor adecuado
        Dim Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Cargo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Cargo)
        actual = target.SelectInactivoxNombre(Nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOCargo = New DAOCargo() ' TODO: Inicializar en un valor adecuado
        Dim IdCargo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Cargo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Cargo)
        actual = target.SelectxId(IdCargo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
