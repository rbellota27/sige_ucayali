﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOBancoTest y se pretende que
'''contenga todas las pruebas unitarias DAOBancoTest.
'''</summary>
<TestClass()> _
Public Class DAOBancoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOBanco
    '''</summary>
    <TestMethod()> _
    Public Sub DAOBancoConstructorTest()
        Dim target As DAOBanco = New DAOBanco()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaBanco
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaBancoTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim banco As Banco = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaBanco(banco, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Banco_CuentaBancaria
    '''</summary>
    <TestMethod()> _
    Public Sub Banco_CuentaBancariaTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Banco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Banco)
        actual = target.Banco_CuentaBancaria(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaBanco
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaBancoTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim Banco As Banco = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaBanco(Banco, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNombreTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Banco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Banco)
        actual = target.SelectActivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Banco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Banco)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Banco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Banco)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Banco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Banco)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Banco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Banco)
        actual = target.SelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Banco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Banco)
        actual = target.SelectCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxNombreTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Banco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Banco)
        actual = target.SelectInactivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxCodSunat
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxCodSunatTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim codSunat As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Banco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Banco)
        actual = target.SelectxCodSunat(codSunat)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim idbanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Banco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Banco)
        actual = target.SelectxId(idbanco)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de selectCuentaDetraccion
    '''</summary>
    <TestMethod()> _
    Public Sub selectCuentaDetraccionTest()
        Dim target As DAOBanco = New DAOBanco() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Banco) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Banco)
        actual = target.selectCuentaDetraccion
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
