﻿Imports System.Collections.Generic

Imports System.Data

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTipoAlmacenTest y se pretende que
'''contenga todas las pruebas unitarias DAOTipoAlmacenTest.
'''</summary>
<TestClass()> _
Public Class DAOTipoAlmacenTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTipoAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTipoAlmacenConstructorTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaTipoAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaTipoAlmacenTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim talmacen As TipoAlmacen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaTipoAlmacen(talmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaTipoAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTipoAlmacenTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim talmacen As TipoAlmacen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaTipoAlmacen(talmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReporteRotacionInvxAnioxMes
    '''</summary>
    <TestMethod()> _
    Public Sub ReporteRotacionInvxAnioxMesTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cadenaalmacenado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim anio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim mes As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.ReporteRotacionInvxAnioxMes(cadenaalmacenado, anio, mes)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ReporteRotacionInvxAnioxMesLineaSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub ReporteRotacionInvxAnioxMesLineaSubLineaTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cadenaalmacenado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim anio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim mes As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Linea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim SubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Pais As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.ReporteRotacionInvxAnioxMesLineaSubLinea(cadenaalmacenado, anio, mes, Linea, SubLinea, Pais)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ResumenIndicadores
    '''</summary>
    <TestMethod()> _
    Public Sub ResumenIndicadoresTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim objx As TipoAlmacen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.ResumenIndicadores(objx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ResumenIndicadoresGrillaIRI
    '''</summary>
    <TestMethod()> _
    Public Sub ResumenIndicadoresGrillaIRITest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim obj As TipoAlmacen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoAlmacen)
        actual = target.ResumenIndicadoresGrillaIRI(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ResumenIndicadoresGrillaIRILineaSubLineaAnio
    '''</summary>
    <TestMethod()> _
    Public Sub ResumenIndicadoresGrillaIRILineaSubLineaAnioTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim Anio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim MesInicial As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim MesFinal As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CadenaTipoAlmacen As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Linea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Sublinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Pais As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoAlmacen)
        actual = target.ResumenIndicadoresGrillaIRILineaSubLineaAnio(Anio, MesInicial, MesFinal, CadenaTipoAlmacen, Linea, Sublinea, Pais)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ResumenIndicadoresLineasubLineaAnio
    '''</summary>
    <TestMethod()> _
    Public Sub ResumenIndicadoresLineasubLineaAnioTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim Anio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim MesInicial As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim MesFinal As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CadenaTipoAlmacen As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Linea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Sublinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Pais As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.ResumenIndicadoresLineasubLineaAnio(Anio, MesInicial, MesFinal, CadenaTipoAlmacen, Linea, Sublinea, Pais)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RotacionInventarioAllLineaSubLineaAnio
    '''</summary>
    <TestMethod()> _
    Public Sub RotacionInventarioAllLineaSubLineaAnioTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim Anio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim MesInicial As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim MesFinal As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CadenaTipoAlmacen As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Linea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Sublinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Pais As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RotacionInventarioAllLineaSubLineaAnio(Anio, MesInicial, MesFinal, CadenaTipoAlmacen, Linea, Sublinea, Pais, idTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RotacioninventarioAll
    '''</summary>
    <TestMethod()> _
    Public Sub RotacioninventarioAllTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim objx As TipoAlmacen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RotacioninventarioAll(objx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoAlmacen)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoInactivoTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoAlmacen)
        actual = target.SelectAllActivoInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdTipoAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTipoAlmacenTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoAlmacen)
        actual = target.SelectxIdTipoAlmacen
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdTipoAlmacenAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTipoAlmacenAllTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim IdtipoAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoAlmacen)
        actual = target.SelectxIdTipoAlmacenAll(IdtipoAlmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxTipoAlmacenxanioxmes
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxTipoAlmacenxanioxmesTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cadenaalmacenado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim anio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim mes As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoAlmacen)
        actual = target.SelectxTipoAlmacenxanioxmes(cadenaalmacenado, anio, mes)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxTipoAlmacenxanioxmesLineaSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxTipoAlmacenxanioxmesLineaSubLineaTest()
        Dim target As DAOTipoAlmacen = New DAOTipoAlmacen() ' TODO: Inicializar en un valor adecuado
        Dim cadenaalmacenado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim anio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim mes As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Linea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim SubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Pais As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoAlmacen)
        actual = target.SelectxTipoAlmacenxanioxmesLineaSubLinea(cadenaalmacenado, anio, mes, Linea, SubLinea, Pais)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
