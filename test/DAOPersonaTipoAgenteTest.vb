﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOPersonaTipoAgenteTest y se pretende que
'''contenga todas las pruebas unitarias DAOPersonaTipoAgenteTest.
'''</summary>
<TestClass()> _
Public Class DAOPersonaTipoAgenteTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOPersonaTipoAgente
    '''</summary>
    <TestMethod()> _
    Public Sub DAOPersonaTipoAgenteConstructorTest()
        Dim target As DAOPersonaTipoAgente = New DAOPersonaTipoAgente()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaPersonaTipoAgenteT
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaPersonaTipoAgenteTTest()
        Dim target As DAOPersonaTipoAgente = New DAOPersonaTipoAgente() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim agente As PersonaTipoAgente = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.ActualizaPersonaTipoAgenteT(cn, agente, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaPersonaTipoAgente
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaPersonaTipoAgenteTest()
        Dim target As DAOPersonaTipoAgente = New DAOPersonaTipoAgente() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim personatipoagente As PersonaTipoAgente = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaPersonaTipoAgente(cn, personatipoagente, T)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de PersonaTipoAgente_Select
    '''</summary>
    <TestMethod()> _
    Public Sub PersonaTipoAgente_SelectTest()
        Dim target As DAOPersonaTipoAgente = New DAOPersonaTipoAgente() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaTipoAgente) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaTipoAgente)
        actual = target.PersonaTipoAgente_Select(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectIdAgenteTasaxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectIdAgenteTasaxIdPersonaTest()
        Dim target As DAOPersonaTipoAgente = New DAOPersonaTipoAgente() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As TipoAgente = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As TipoAgente
        actual = target.SelectIdAgenteTasaxIdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOPersonaTipoAgente = New DAOPersonaTipoAgente() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As PersonaTipoAgente = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As PersonaTipoAgente
        actual = target.SelectxId(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
