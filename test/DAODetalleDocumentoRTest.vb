﻿Imports System.Data.SqlClient

Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODetalleDocumentoRTest y se pretende que
'''contenga todas las pruebas unitarias DAODetalleDocumentoRTest.
'''</summary>
<TestClass()> _
Public Class DAODetalleDocumentoRTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODetalleDocumentoR
    '''</summary>
    <TestMethod()> _
    Public Sub DAODetalleDocumentoRConstructorTest()
        Dim target As DAODetalleDocumentoR = New DAODetalleDocumentoR()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDetalleDocumentoR
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDetalleDocumentoRTest()
        Dim target As DAODetalleDocumentoR = New DAODetalleDocumentoR() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaEmisionRef As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim CodigoRef As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim SerieRef As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim TotalDocumentoRef As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim PorcentPercepcion As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim TipoCambio As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim ImportePercepcion As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim ImporteFinal As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaDetalleDocumentoR(IdDocumento, IdDocumentoRef, IdMonedaRef, fechaEmisionRef, CodigoRef, SerieRef, TotalDocumentoRef, PorcentPercepcion, TipoCambio, ImportePercepcion, ImporteFinal, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub
End Class
