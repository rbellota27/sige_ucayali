﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOChoferTest y se pretende que
'''contenga todas las pruebas unitarias DAOChoferTest.
'''</summary>
<TestClass()> _
Public Class DAOChoferTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOChofer
    '''</summary>
    <TestMethod()> _
    Public Sub DAOChoferConstructorTest()
        Dim target As DAOChofer = New DAOChofer()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaChofer
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaChoferTest()
        Dim target As DAOChofer = New DAOChofer() ' TODO: Inicializar en un valor adecuado
        Dim chofer As Chofer = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaChofer(chofer)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaChoferTransact
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaChoferTransactTest()
        Dim target As DAOChofer = New DAOChofer() ' TODO: Inicializar en un valor adecuado
        Dim chofer As Chofer = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaChoferTransact(chofer, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ChoferSelectActivoxParams
    '''</summary>
    <TestMethod()> _
    Public Sub ChoferSelectActivoxParamsTest()
        Dim target As DAOChofer = New DAOChofer() ' TODO: Inicializar en un valor adecuado
        Dim dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim ruc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim RazonApe As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.ChoferSelectActivoxParams(dni, ruc, RazonApe, tipo, pageindex, pagesize, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaChofer
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaChoferTest()
        Dim target As DAOChofer = New DAOChofer() ' TODO: Inicializar en un valor adecuado
        Dim chofer As Chofer = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaChofer(chofer, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaChofer
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaChoferTest1()
        Dim target As DAOChofer = New DAOChofer() ' TODO: Inicializar en un valor adecuado
        Dim chofer As Chofer = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaChofer(chofer)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaChoferTransact
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaChoferTransactTest()
        Dim target As DAOChofer = New DAOChofer() ' TODO: Inicializar en un valor adecuado
        Dim chofer As Chofer = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaChoferTransact(chofer, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNombreTest()
        Dim target As DAOChofer = New DAOChofer() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Chofer) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Chofer)
        actual = target.SelectActivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de deleteChofer
    '''</summary>
    <TestMethod()> _
    Public Sub deleteChoferTest()
        Dim target As DAOChofer = New DAOChofer() ' TODO: Inicializar en un valor adecuado
        Dim idchofer As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.deleteChofer(idchofer, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarChoferxID
    '''</summary>
    <TestMethod()> _
    Public Sub listarChoferxIDTest()
        Dim target As DAOChofer = New DAOChofer() ' TODO: Inicializar en un valor adecuado
        Dim idChofer As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Chofer = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Chofer
        actual = target.listarChoferxID(idChofer)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
