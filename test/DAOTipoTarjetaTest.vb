﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTipoTarjetaTest y se pretende que
'''contenga todas las pruebas unitarias DAOTipoTarjetaTest.
'''</summary>
<TestClass()> _
Public Class DAOTipoTarjetaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTipoTarjeta
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTipoTarjetaConstructorTest()
        Dim target As DAOTipoTarjeta = New DAOTipoTarjeta()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de InsertT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTTest()
        Dim target As DAOTipoTarjeta = New DAOTipoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim x As TipoTarjeta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertT(x)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertT_GetID
    '''</summary>
    <TestMethod()> _
    Public Sub InsertT_GetIDTest()
        Dim target As DAOTipoTarjeta = New DAOTipoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim x As TipoTarjeta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertT_GetID(x)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOTipoTarjeta = New DAOTipoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoTarjeta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoTarjeta)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOTipoTarjeta = New DAOTipoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoTarjeta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoTarjeta)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOTipoTarjeta = New DAOTipoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoTarjeta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoTarjeta)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdCaja
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdCajaTest()
        Dim target As DAOTipoTarjeta = New DAOTipoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoTarjeta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoTarjeta)
        actual = target.SelectCboxIdCaja(IdCaja)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxParamsTest()
        Dim target As DAOTipoTarjeta = New DAOTipoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim x As TipoTarjeta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoTarjeta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoTarjeta)
        actual = target.SelectxParams(x)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateT
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTTest()
        Dim target As DAOTipoTarjeta = New DAOTipoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim x As TipoTarjeta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateT(x)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
