﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMotivoT_TipoOperacionTest y se pretende que
'''contenga todas las pruebas unitarias DAOMotivoT_TipoOperacionTest.
'''</summary>
<TestClass()> _
Public Class DAOMotivoT_TipoOperacionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMotivoT_TipoOperacion
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMotivoT_TipoOperacionConstructorTest()
        Dim target As DAOMotivoT_TipoOperacion = New DAOMotivoT_TipoOperacion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaMotivoTraslado_TipoOperacion
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaMotivoTraslado_TipoOperacionTest()
        Dim target As DAOMotivoT_TipoOperacion = New DAOMotivoT_TipoOperacion() ' TODO: Inicializar en un valor adecuado
        Dim UpdateTipoOperacion As List(Of MotivoT_TipoOperacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaMotivoTraslado_TipoOperacion(UpdateTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdMotivoT
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdMotivoTTest()
        Dim target As DAOMotivoT_TipoOperacion = New DAOMotivoT_TipoOperacion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idTraslado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdMotivoT(cn, tr, idTraslado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaMotivoT_TipoOperacion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaMotivoT_TipoOperacionTest()
        Dim target As DAOMotivoT_TipoOperacion = New DAOMotivoT_TipoOperacion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of MotivoT_TipoOperacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdMotivoT As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.InsertaMotivoT_TipoOperacion(cn, tr, lista, IdMotivoT)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboTipoOperacion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTipoOperacionTest()
        Dim target As DAOMotivoT_TipoOperacion = New DAOMotivoT_TipoOperacion() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MotivoT_TipoOperacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MotivoT_TipoOperacion)
        actual = target.SelectCboTipoOperacion
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdTipoOperacion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdTipoOperacionTest()
        Dim target As DAOMotivoT_TipoOperacion = New DAOMotivoT_TipoOperacion() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MotivoTraslado) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MotivoTraslado)
        actual = target.SelectCboxIdTipoOperacion(IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de _MotivoT_TipoOperacionSelectxIdTraslado
    '''</summary>
    <TestMethod()> _
    Public Sub _MotivoT_TipoOperacionSelectxIdTrasladoTest()
        Dim target As DAOMotivoT_TipoOperacion = New DAOMotivoT_TipoOperacion() ' TODO: Inicializar en un valor adecuado
        Dim IdTraslado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MotivoT_TipoOperacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MotivoT_TipoOperacion)
        actual = target._MotivoT_TipoOperacionSelectxIdTraslado(IdTraslado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
