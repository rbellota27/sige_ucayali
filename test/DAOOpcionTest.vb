﻿Imports System.Collections.Generic

Imports System.Data

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOOpcionTest y se pretende que
'''contenga todas las pruebas unitarias DAOOpcionTest.
'''</summary>
<TestClass()> _
Public Class DAOOpcionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOOpcion
    '''</summary>
    <TestMethod()> _
    Public Sub DAOOpcionConstructorTest()
        Dim target As DAOOpcion = New DAOOpcion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaOpcion
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaOpcionTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim opcion As Opcion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaOpcion(opcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de AplicarCambiosAllUsers
    '''</summary>
    <TestMethod()> _
    Public Sub AplicarCambiosAllUsersTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim t As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idperfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.AplicarCambiosAllUsers(cn, t, idperfil)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de CargaNodoTreeView
    '''</summary>
    <TestMethod()> _
    Public Sub CargaNodoTreeViewTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.CargaNodoTreeView
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletePerfilOpcion
    '''</summary>
    <TestMethod()> _
    Public Sub DeletePerfilOpcionTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim Idperfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletePerfilOpcion(Idperfil, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeletePerfilOpcionUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub DeletePerfilOpcionUsuarioTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim Idperfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.DeletePerfilOpcionUsuario(Idperfil, cn, tr, idUsuario)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Elimina
    '''</summary>
    <TestMethod()> _
    Public Sub EliminaTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idopcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim deleteAll As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Elimina(cn, tr, idopcion, deleteAll)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaOpcion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaOpcionTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim opcion As Opcion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaOpcion(opcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaPerfilOpcion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaPerfilOpcionTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim listaOpcion As List(Of Opcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim t As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idperfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaPerfilOpcion(listaOpcion, cn, t, idperfil)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaPerfilOpcionUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaPerfilOpcionUsuarioTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim listaOpcion As List(Of Opcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim t As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idperfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaPerfilOpcionUsuario(listaOpcion, cn, t, idperfil, idUsuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllOpcion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllOpcionTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Opcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Opcion)
        actual = target.SelectAllOpcion
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectOpcionMantenimiento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectOpcionMantenimientoTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Opcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Opcion)
        actual = target.SelectOpcionMantenimiento
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectOpcionxDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub SelectOpcionxDocumentoRefTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Opcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Opcion)
        actual = target.SelectOpcionxDocumentoRef(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectOpcionxPerfil
    '''</summary>
    <TestMethod()> _
    Public Sub SelectOpcionxPerfilTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim idperfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Opcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Opcion)
        actual = target.SelectOpcionxPerfil(idperfil)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectOpcionxUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub SelectOpcionxUsuarioTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim idusuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Opcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Opcion)
        actual = target.SelectOpcionxUsuario(idusuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectOpcionxUsuarioxPerfil
    '''</summary>
    <TestMethod()> _
    Public Sub SelectOpcionxUsuarioxPerfilTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim idperfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idusuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Opcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Opcion)
        actual = target.SelectOpcionxUsuarioxPerfil(idperfil, idusuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectPerfilxUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub SelectPerfilxUsuarioTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim idusuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Perfil) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Perfil)
        actual = target.SelectPerfilxUsuario(idusuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdOpcion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdOpcionTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim IdOpcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Opcion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Opcion
        actual = target.SelectxIdOpcion(IdOpcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateInsertaOpcion
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateInsertaOpcionTest()
        Dim target As DAOOpcion = New DAOOpcion() ' TODO: Inicializar en un valor adecuado
        Dim Listaopcion As List(Of Opcion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim t As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateInsertaOpcion(Listaopcion, cn, t)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub
End Class
