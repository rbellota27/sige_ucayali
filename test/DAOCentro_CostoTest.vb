﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCentro_CostoTest y se pretende que
'''contenga todas las pruebas unitarias DAOCentro_CostoTest.
'''</summary>
<TestClass()> _
Public Class DAOCentro_CostoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCentro_Costo
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCentro_CostoConstructorTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de BuscarAutogeneradoXCodSubArea1
    '''</summary>
    <TestMethod()> _
    Public Sub BuscarAutogeneradoXCodSubArea1Test()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim CodUnidadNegocio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodsubCodigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.BuscarAutogeneradoXCodSubArea1(CodUnidadNegocio, CodsubCodigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de BuscarAutogeneradoXCodSubArea2
    '''</summary>
    <TestMethod()> _
    Public Sub BuscarAutogeneradoXCodSubArea2Test()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim CodUnidadNegocio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodsubCodigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodSubArea1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.BuscarAutogeneradoXCodSubArea2(CodUnidadNegocio, CodsubCodigo, CodSubArea1)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de BuscarAutogeneradoXCodSubArea3
    '''</summary>
    <TestMethod()> _
    Public Sub BuscarAutogeneradoXCodSubArea3Test()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim CodUnidadNegocio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodsubCodigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodSubArea1 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodSubArea2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.BuscarAutogeneradoXCodSubArea3(CodUnidadNegocio, CodsubCodigo, CodSubArea1, CodSubArea2)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de BuscarAutogeneradoXCodSubcodigo
    '''</summary>
    <TestMethod()> _
    Public Sub BuscarAutogeneradoXCodSubcodigoTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim CodUnidadNegocio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.BuscarAutogeneradoXCodSubcodigo(CodUnidadNegocio)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de BuscarAutogeneradoXCodUniNegocio
    '''</summary>
    <TestMethod()> _
    Public Sub BuscarAutogeneradoXCodUniNegocioTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim CodUnidadNegocio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.BuscarAutogeneradoXCodUniNegocio(CodUnidadNegocio)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CentroCosto_idTienda_IdUsuario_IdArea
    '''</summary>
    <TestMethod()> _
    Public Sub CentroCosto_idTienda_IdUsuario_IdAreaTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idusuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idarea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.CentroCosto_idTienda_IdUsuario_IdArea(idtienda, idusuario, idarea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GastoPorCentroCosto_Insert
    '''</summary>
    <TestMethod()> _
    Public Sub GastoPorCentroCosto_InsertTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Lista As List(Of Centro_costo) = Nothing ' TODO: Inicializar en un valor adecuado
        target.GastoPorCentroCosto_Insert(cnx, trx, iddocumento, Lista)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de GastoPorCentroCosto_Select_IdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub GastoPorCentroCosto_Select_IdDocumentoTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Centro_costo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Centro_costo)
        actual = target.GastoPorCentroCosto_Select_IdDocumento(idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Insert_Centro_Costo
    '''</summary>
    <TestMethod()> _
    Public Sub Insert_Centro_CostoTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim obj As Centro_costo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Insert_Centro_Costo(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Listar_Cod_DepFunc_Centro_costo
    '''</summary>
    <TestMethod()> _
    Public Sub Listar_Cod_DepFunc_Centro_costoTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim Cod_UniNeg As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Centro_costo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Centro_costo)
        actual = target.Listar_Cod_DepFunc_Centro_costo(Cod_UniNeg, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Listar_Cod_SubCodigo2_Centro_costo
    '''</summary>
    <TestMethod()> _
    Public Sub Listar_Cod_SubCodigo2_Centro_costoTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim Cod_UniNeg As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Cod_DepFunc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Centro_costo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Centro_costo)
        actual = target.Listar_Cod_SubCodigo2_Centro_costo(Cod_UniNeg, Cod_DepFunc, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Listar_Cod_SubCodigo3_Centro_costo
    '''</summary>
    <TestMethod()> _
    Public Sub Listar_Cod_SubCodigo3_Centro_costoTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim Cod_UniNeg As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Cod_DepFunc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Cod_SubCodigo2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Centro_costo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Centro_costo)
        actual = target.Listar_Cod_SubCodigo3_Centro_costo(Cod_UniNeg, Cod_DepFunc, Cod_SubCodigo2, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Listar_Cod_SubCodigo4_Centro_costo
    '''</summary>
    <TestMethod()> _
    Public Sub Listar_Cod_SubCodigo4_Centro_costoTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim Cod_UniNeg As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Cod_DepFunc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Cod_SubCodigo2 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Cod_SubCodigo3 As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Centro_costo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Centro_costo)
        actual = target.Listar_Cod_SubCodigo4_Centro_costo(Cod_UniNeg, Cod_DepFunc, Cod_SubCodigo2, Cod_SubCodigo3, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Listar_Cod_UniNeg_Centro_Costo
    '''</summary>
    <TestMethod()> _
    Public Sub Listar_Cod_UniNeg_Centro_CostoTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Centro_costo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Centro_costo)
        actual = target.Listar_Cod_UniNeg_Centro_Costo(estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Listar_centro_costo
    '''</summary>
    <TestMethod()> _
    Public Sub Listar_centro_costoTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim obj As Centro_costo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Centro_costo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Centro_costo)
        actual = target.Listar_centro_costo(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Update_Centro_Costo
    '''</summary>
    <TestMethod()> _
    Public Sub Update_Centro_CostoTest()
        Dim target As DAOCentro_Costo = New DAOCentro_Costo() ' TODO: Inicializar en un valor adecuado
        Dim obj As Centro_costo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Update_Centro_Costo(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
