﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAORolTest y se pretende que
'''contenga todas las pruebas unitarias DAORolTest.
'''</summary>
<TestClass()> _
Public Class DAORolTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAORol
    '''</summary>
    <TestMethod()> _
    Public Sub DAORolConstructorTest()
        Dim target As DAORol = New DAORol()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaRol
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaRolTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim rol As Rol = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaRol(rol)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaRol
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaRolTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim rol As Rol = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaRol(rol)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNombreTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol)
        actual = target.SelectActivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol)
        actual = target.SelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol)
        actual = target.SelectCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboCliente
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboClienteTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol)
        actual = target.SelectCboCliente
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboProved
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboProvedTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol)
        actual = target.SelectCboProved
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxNombreTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol)
        actual = target.SelectInactivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol)
        actual = target.SelectxId(id)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPersonaTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim idPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of RolPersona) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of RolPersona)
        actual = target.SelectxIdPersona(idPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarExisteRolEmpresa
    '''</summary>
    <TestMethod()> _
    Public Sub listarExisteRolEmpresaTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol_Empresa) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol_Empresa)
        actual = target.listarExisteRolEmpresa
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarMotivoBaja
    '''</summary>
    <TestMethod()> _
    Public Sub listarMotivoBajaTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MotivoBaja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MotivoBaja)
        actual = target.listarMotivoBaja
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarRolxEmpresa
    '''</summary>
    <TestMethod()> _
    Public Sub listarRolxEmpresaTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol_Empresa) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol_Empresa)
        actual = target.listarRolxEmpresa(idempresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarRolxEmpresaProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub listarRolxEmpresaProveedorTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol_Empresa) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol_Empresa)
        actual = target.listarRolxEmpresaProveedor(idempresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarRolxEmpresaUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub listarRolxEmpresaUsuarioTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol_Empresa) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol_Empresa)
        actual = target.listarRolxEmpresaUsuario(idempresa, idPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarRolxEmpresaUsuarioProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub listarRolxEmpresaUsuarioProveedorTest()
        Dim target As DAORol = New DAORol() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Rol_Empresa) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Rol_Empresa)
        actual = target.listarRolxEmpresaUsuarioProveedor(idempresa, idPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
