﻿Imports System

Imports System.Collections.Generic

Imports System.Data

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOOrdenCompraTest y se pretende que
'''contenga todas las pruebas unitarias DAOOrdenCompraTest.
'''</summary>
<TestClass()> _
Public Class DAOOrdenCompraTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOOrdenCompra
    '''</summary>
    <TestMethod()> _
    Public Sub DAOOrdenCompraConstructorTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizarDocCondiconComercial
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizarDocCondiconComercialTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim t As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.ActualizarDocCondiconComercial(cn, t, iddocumento)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizarOrdenCompra
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizarOrdenCompraTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim obj As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.ActualizarOrdenCompra(cn, obj, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Anular
    '''</summary>
    <TestMethod()> _
    Public Sub AnularTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeletePuntoLlegada As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteCondicionC As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteMontoRegimen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteCuentaPorPagar As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim sqlcn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqltr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.Anular(IdDocumento, DeleteDetalleDocumento, DeletePuntoLlegada, DeleteCondicionC, DeleteObservacion, DeleteMontoRegimen, DeleteCuentaPorPagar, Anular, sqlcn, sqltr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de ComprasxProveedor
    '''</summary>
    <TestMethod()> _
    Public Sub ComprasxProveedorTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim linea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim sublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.ComprasxProveedor(idempresa, idtienda, idpersona, fechaInicio, fechafin, tipo, linea, sublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CuentaProveedor_SelectActivo
    '''</summary>
    <TestMethod()> _
    Public Sub CuentaProveedor_SelectActivoTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPropietario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CuentaProveedor) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CuentaProveedor)
        actual = target.CuentaProveedor_SelectActivo(IdProveedor, IdPropietario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeleteDetallexID
    '''</summary>
    <TestMethod()> _
    Public Sub DeleteDetallexIDTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeleteDetallexID(iddocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DetalleDocumentoViewSelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleDocumentoViewSelectxIdDocumentoTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumentoView)
        actual = target.DetalleDocumentoViewSelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertBD_OrdenCompra
    '''</summary>
    <TestMethod()> _
    Public Sub InsertBD_OrdenCompraTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim obj As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.InsertBD_OrdenCompra(obj, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertarOrdenCompra
    '''</summary>
    <TestMethod()> _
    Public Sub InsertarOrdenCompraTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim ObjDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalle As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim obs As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objMonto As MontoRegimen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaCondicionComercial As List(Of CondicionComercial) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.InsertarOrdenCompra(ObjDocumento, listaDetalle, obs, objMonto, listaCondicionComercial)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarPrecioXidMoneda
    '''</summary>
    <TestMethod()> _
    Public Sub ListarPrecioXidMonedaTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.ListarPrecioXidMoneda(idmoneda, idproducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenCompra_Datos_Contacto
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenCompra_Datos_ContactoTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.OrdenCompra_Datos_Contacto(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenCompra_Select_Tipo
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenCompra_Select_TipoTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim tipoOrden As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim aprobado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.OrdenCompra_Select_Tipo(tipoOrden, aprobado, idempresa, idtienda, pageIndex, pageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RankingProvedores
    '''</summary>
    <TestMethod()> _
    Public Sub RankingProvedoresTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idCondicionpago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTipopersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nprimeros As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RankingProvedores(idempresa, idtienda, idCondicionpago, idmoneda, idTipopersona, fechaInicio, fechafin, nprimeros, idlinea, idsublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectOrdenCompra
    '''</summary>
    <TestMethod()> _
    Public Sub SelectOrdenCompraTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim idserie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectOrdenCompra(idserie, codigo, idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateDocumentoTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim objDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalle As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObs As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objMonto As MontoRegimen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaCondicionComercial As List(Of CondicionComercial) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateDocumento(objDocumento, listaDetalle, objObs, objMonto, listaCondicionComercial)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Validar_CosteoImportacion
    '''</summary>
    <TestMethod()> _
    Public Sub Validar_CosteoImportacionTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Nullable(Of Integer) = New Nullable(Of Integer)() ' TODO: Inicializar en un valor adecuado
        Dim actual As Nullable(Of Integer)
        actual = target.Validar_CosteoImportacion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetComparativo
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetComparativoTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim year As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim columna As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idproveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idpropietario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetComparativo(year, columna, idproveedor, idmoneda, idpropietario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getDataSetOrdenCompra
    '''</summary>
    <TestMethod()> _
    Public Sub getDataSetOrdenCompraTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getDataSetOrdenCompra(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getReporteVentasStock
    '''</summary>
    <TestMethod()> _
    Public Sub getReporteVentasStockTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim yeari As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanai As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim yearf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim semanaf As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim filtrarsemana As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim ltProd As List(Of ProductoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ltTAlmacen As List(Of TipoAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim stockxtienda As Short = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.getReporteVentasStock(idEmpresa, idtienda, idlinea, idSublinea, fechainicio, fechafin, yeari, semanai, yearf, semanaf, filtrarsemana, ltProd, ltTAlmacen, stockxtienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarContactoxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub listarContactoxIdPersonaTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim idPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim addIdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.listarContactoxIdPersona(idPersona, addIdUsuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarDireccionAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub listarDireccionAlmacenTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Almacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Almacen)
        actual = target.listarDireccionAlmacen
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarOrdenesCompraxFiltro
    '''</summary>
    <TestMethod()> _
    Public Sub listarOrdenesCompraxFiltroTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaIni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdEstadoCan As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEstadoEnt As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.listarOrdenesCompraxFiltro(IdEmpresa, IdTienda, IdProveedor, FechaIni, FechaFin, IdEstadoCan, IdEstadoEnt, PageIndex, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarPersonaJuridicaxRolxEmpresa
    '''</summary>
    <TestMethod()> _
    Public Sub listarPersonaJuridicaxRolxEmpresaTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim NomPersona As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Ruc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdRol As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Condicion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.listarPersonaJuridicaxRolxEmpresa(NomPersona, Ruc, Tipo, IdRol, Condicion, Estado, PageIndex, PageSize, Dni)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarPreciosCompra
    '''</summary>
    <TestMethod()> _
    Public Sub listarPreciosCompraTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.listarPreciosCompra(idproducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarProductoPedidoSucursal
    '''</summary>
    <TestMethod()> _
    Public Sub listarProductoPedidoSucursalTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim codsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.listarProductoPedidoSucursal(idtienda, idlinea, idsublinea, descripcion, codsublinea, pageIndex, pageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarProductosProveedorOrdenCompra
    '''</summary>
    <TestMethod()> _
    Public Sub listarProductosProveedorOrdenCompraTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAgenteProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageSise As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim vertodo As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim IdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim codigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim codigoProveedor As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IncluyeIgv As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.listarProductosProveedorOrdenCompra(idempresa, idproveedor, IdAgenteProveedor, descripcion, idlinea, idsublinea, codsublinea, idmoneda, pageindex, pageSise, vertodo, IdArea, tabla, codigoProducto, codigoProveedor, IncluyeIgv)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarProductosProveedorOrdenCompraCodBarraFab
    '''</summary>
    <TestMethod()> _
    Public Sub listarProductosProveedorOrdenCompraCodBarraFabTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idproveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAgenteProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IncluyeIgv As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim CodbarraFab As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.listarProductosProveedorOrdenCompraCodBarraFab(idempresa, idproveedor, IdAgenteProveedor, idmoneda, IdArea, IncluyeIgv, CodbarraFab)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de updatePrecioCompraProducto
    '''</summary>
    <TestMethod()> _
    Public Sub updatePrecioCompraProductoTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim idmoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.updatePrecioCompraProducto(idmoneda, lista, cnx, trx)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de ventasxVendedor
    '''</summary>
    <TestMethod()> _
    Public Sub ventasxVendedorTest()
        Dim target As DAOOrdenCompra = New DAOOrdenCompra() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idpersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaInicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim tipo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim linea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim sublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.ventasxVendedor(idempresa, idtienda, idpersona, fechaInicio, fechafin, tipo, linea, sublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
