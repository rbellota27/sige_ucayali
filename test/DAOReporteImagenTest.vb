﻿Imports System.Data

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOReporteImagenTest y se pretende que
'''contenga todas las pruebas unitarias DAOReporteImagenTest.
'''</summary>
<TestClass()> _
Public Class DAOReporteImagenTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOReporteImagen
    '''</summary>
    <TestMethod()> _
    Public Sub DAOReporteImagenConstructorTest()
        Dim target As DAOReporteImagen = New DAOReporteImagen()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de InsertaImagenReporte
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaImagenReporteTest()
        Dim target As DAOReporteImagen = New DAOReporteImagen() ' TODO: Inicializar en un valor adecuado
        Dim objReporteImagen As ReporteImagen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaImagenReporte(objReporteImagen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListNomImagen
    '''</summary>
    <TestMethod()> _
    Public Sub ListNomImagenTest()
        Dim target As DAOReporteImagen = New DAOReporteImagen() ' TODO: Inicializar en un valor adecuado
        Dim expected As ReporteImagen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As ReporteImagen
        actual = target.ListNomImagen
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RptNomImagen
    '''</summary>
    <TestMethod()> _
    Public Sub RptNomImagenTest()
        Dim target As DAOReporteImagen = New DAOReporteImagen() ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RptNomImagen
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
