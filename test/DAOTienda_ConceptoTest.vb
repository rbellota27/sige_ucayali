﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTienda_ConceptoTest y se pretende que
'''contenga todas las pruebas unitarias DAOTienda_ConceptoTest.
'''</summary>
<TestClass()> _
Public Class DAOTienda_ConceptoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTienda_Concepto
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTienda_ConceptoConstructorTest()
        Dim target As DAOTienda_Concepto = New DAOTienda_Concepto()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de InsertTienda_Concepto
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTienda_ConceptoTest()
        Dim target As DAOTienda_Concepto = New DAOTienda_Concepto() ' TODO: Inicializar en un valor adecuado
        Dim idConcepto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Lista As List(Of Tienda_Concepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertTienda_Concepto(idConcepto, Lista, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectTienda_Concepto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectTienda_ConceptoTest()
        Dim target As DAOTienda_Concepto = New DAOTienda_Concepto() ' TODO: Inicializar en un valor adecuado
        Dim idConcepto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Tienda_Concepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Tienda_Concepto)
        actual = target.SelectTienda_Concepto(idConcepto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateTienda_Concepto
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTienda_ConceptoTest()
        Dim target As DAOTienda_Concepto = New DAOTienda_Concepto() ' TODO: Inicializar en un valor adecuado
        Dim idconcepto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateTienda_Concepto(idconcepto, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
