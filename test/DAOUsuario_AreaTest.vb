﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOUsuario_AreaTest y se pretende que
'''contenga todas las pruebas unitarias DAOUsuario_AreaTest.
'''</summary>
<TestClass()> _
Public Class DAOUsuario_AreaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOUsuario_Area
    '''</summary>
    <TestMethod()> _
    Public Sub DAOUsuario_AreaConstructorTest()
        Dim target As DAOUsuario_Area = New DAOUsuario_Area()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaUsuario_Area
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaUsuario_AreaTest()
        Dim target As DAOUsuario_Area = New DAOUsuario_Area() ' TODO: Inicializar en un valor adecuado
        Dim Usuario_Area As Usuario_Area = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaUsuario_Area(Usuario_Area)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdUsuarioxIdArea
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdUsuarioxIdAreaTest()
        Dim target As DAOUsuario_Area = New DAOUsuario_Area() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim obj As Usuario_Area = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdUsuarioxIdArea(cn, obj, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Insert
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTest()
        Dim target As DAOUsuario_Area = New DAOUsuario_Area() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim obj As Usuario_Area = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Insert(cn, obj, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaUsuario_Area
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaUsuario_AreaTest()
        Dim target As DAOUsuario_Area = New DAOUsuario_Area() ' TODO: Inicializar en un valor adecuado
        Dim Usuario_Area As Usuario_Area = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaUsuario_Area(Usuario_Area)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarAreaxIdusuario
    '''</summary>
    <TestMethod()> _
    Public Sub ListarAreaxIdusuarioTest()
        Dim target As DAOUsuario_Area = New DAOUsuario_Area() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim addIdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Usuario_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Usuario_Area)
        actual = target.ListarAreaxIdusuario(IdUsuario, addIdArea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOUsuario_Area = New DAOUsuario_Area() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Usuario_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Usuario_Area)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOUsuario_Area = New DAOUsuario_Area() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Usuario_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Usuario_Area)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOUsuario_Area = New DAOUsuario_Area() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Usuario_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Usuario_Area)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdArea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdAreaTest()
        Dim target As DAOUsuario_Area = New DAOUsuario_Area() ' TODO: Inicializar en un valor adecuado
        Dim IdArea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Usuario_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Usuario_Area)
        actual = target.SelectxIdArea(IdArea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdUsuarioTest()
        Dim target As DAOUsuario_Area = New DAOUsuario_Area() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Usuario_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Usuario_Area)
        actual = target.SelectxIdUsuario(IdUsuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Update
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateTest()
        Dim target As DAOUsuario_Area = New DAOUsuario_Area() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim obj As Usuario_Area = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Update(cn, obj, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
