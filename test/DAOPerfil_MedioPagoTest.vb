﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOPerfil_MedioPagoTest y se pretende que
'''contenga todas las pruebas unitarias DAOPerfil_MedioPagoTest.
'''</summary>
<TestClass()> _
Public Class DAOPerfil_MedioPagoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOPerfil_MedioPago
    '''</summary>
    <TestMethod()> _
    Public Sub DAOPerfil_MedioPagoConstructorTest()
        Dim target As DAOPerfil_MedioPago = New DAOPerfil_MedioPago()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdPerfil
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdPerfilTest()
        Dim target As DAOPerfil_MedioPago = New DAOPerfil_MedioPago() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdPerfil(cn, tr, IdPerfil)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Insert
    '''</summary>
    <TestMethod()> _
    Public Sub InsertTest()
        Dim target As DAOPerfil_MedioPago = New DAOPerfil_MedioPago() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim obj As Perfil_MedioPago = Nothing ' TODO: Inicializar en un valor adecuado
        target.Insert(cn, tr, obj)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertList
    '''</summary>
    <TestMethod()> _
    Public Sub InsertListTest()
        Dim target As DAOPerfil_MedioPago = New DAOPerfil_MedioPago() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of Perfil_MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertList(cn, tr, lista)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertListxIdPerfil
    '''</summary>
    <TestMethod()> _
    Public Sub InsertListxIdPerfilTest()
        Dim target As DAOPerfil_MedioPago = New DAOPerfil_MedioPago() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of Perfil_MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.InsertListxIdPerfil(cn, tr, lista, IdPerfil)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuarioTest()
        Dim target As DAOPerfil_MedioPago = New DAOPerfil_MedioPago() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Perfil_MedioPago_ValidarxIdMedioPagoxIdUsuario(IdUsuario, IdMedioPago)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdPerfil
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdPerfilTest()
        Dim target As DAOPerfil_MedioPago = New DAOPerfil_MedioPago() ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Perfil_MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Perfil_MedioPago)
        actual = target.SelectxIdPerfil(IdPerfil)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
