﻿Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoInvInicialTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoInvInicialTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoInvInicialTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoInvInicial
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoInvInicialConstructorTest()
        Dim target As DAODocumentoInvInicial = New DAODocumentoInvInicial()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoInvInicial_RegistrarDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoInvInicial_RegistrarDetalleTest()
        Dim target As DAODocumentoInvInicial = New DAODocumentoInvInicial() ' TODO: Inicializar en un valor adecuado
        Dim objDetalle As DetalleDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim InsertaMovAlmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Factor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoInvInicial_RegistrarDetalle(objDetalle, IdDocumento, InsertaMovAlmacen, Factor, IdEmpresa, IdAlmacen, IdTipoOperacion, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoInventarioInicialSelectIdDocumentoxParams
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoInventarioInicialSelectIdDocumentoxParamsTest()
        Dim target As DAODocumentoInvInicial = New DAODocumentoInvInicial() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.DocumentoInventarioInicialSelectIdDocumentoxParams(IdEmpresa, IdAlmacen, IdTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
