﻿Imports System

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOProductoMedidaTest y se pretende que
'''contenga todas las pruebas unitarias DAOProductoMedidaTest.
'''</summary>
<TestClass()> _
Public Class DAOProductoMedidaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOProductoMedida
    '''</summary>
    <TestMethod()> _
    Public Sub DAOProductoMedidaConstructorTest()
        Dim target As DAOProductoMedida = New DAOProductoMedida()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaProductoMedida
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaProductoMedidaTest()
        Dim target As DAOProductoMedida = New DAOProductoMedida() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim productomedida As ProductoMedida = Nothing ' TODO: Inicializar en un valor adecuado
        target.ActualizaProductoMedida(cn, tr, productomedida)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdProductoTest()
        Dim target As DAOProductoMedida = New DAOProductoMedida() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.DeletexIdProducto(cn, tr, idProducto)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdProductoTest1()
        Dim target As DAOProductoMedida = New DAOProductoMedida() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdProducto(IdProducto, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaProductoMedida
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaProductoMedidaTest()
        Dim target As DAOProductoMedida = New DAOProductoMedida() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim productomedida As ProductoMedida = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertaProductoMedida(cn, tr, productomedida)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de getMagnitudProducto
    '''</summary>
    <TestMethod()> _
    Public Sub getMagnitudProductoTest()
        Dim target As DAOProductoMedida = New DAOProductoMedida() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUMProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Cantidad As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim IdMagnitud As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUMMagnitud_Destino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.getMagnitudProducto(IdProducto, IdUMProducto, Cantidad, IdMagnitud, IdUMMagnitud_Destino)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
