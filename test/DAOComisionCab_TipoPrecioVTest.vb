﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOComisionCab_TipoPrecioVTest y se pretende que
'''contenga todas las pruebas unitarias DAOComisionCab_TipoPrecioVTest.
'''</summary>
<TestClass()> _
Public Class DAOComisionCab_TipoPrecioVTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOComisionCab_TipoPrecioV
    '''</summary>
    <TestMethod()> _
    Public Sub DAOComisionCab_TipoPrecioVConstructorTest()
        Dim target As DAOComisionCab_TipoPrecioV = New DAOComisionCab_TipoPrecioV()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ComisionCab_TipoPrecioV_SelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub ComisionCab_TipoPrecioV_SelectxParamsTest()
        Dim target As DAOComisionCab_TipoPrecioV = New DAOComisionCab_TipoPrecioV() ' TODO: Inicializar en un valor adecuado
        Dim IdComisionCab As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ComisionCab_TipoPrecioV) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ComisionCab_TipoPrecioV)
        actual = target.ComisionCab_TipoPrecioV_SelectxParams(IdComisionCab)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdComisionCabxIdTipoPv
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdComisionCabxIdTipoPvTest()
        Dim target As DAOComisionCab_TipoPrecioV = New DAOComisionCab_TipoPrecioV() ' TODO: Inicializar en un valor adecuado
        Dim IdComisionCab As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeletexIdComisionCabxIdTipoPv(IdComisionCab, IdTipoPv, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub RegistrarTest()
        Dim target As DAOComisionCab_TipoPrecioV = New DAOComisionCab_TipoPrecioV() ' TODO: Inicializar en un valor adecuado
        Dim objComisionCab_TipoPV As ComisionCab_TipoPrecioV = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Registrar(objComisionCab_TipoPV, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
