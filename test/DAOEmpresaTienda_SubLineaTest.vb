﻿Imports System.Data

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOEmpresaTienda_SubLineaTest y se pretende que
'''contenga todas las pruebas unitarias DAOEmpresaTienda_SubLineaTest.
'''</summary>
<TestClass()> _
Public Class DAOEmpresaTienda_SubLineaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOEmpresaTienda_SubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub DAOEmpresaTienda_SubLineaConstructorTest()
        Dim target As DAOEmpresaTienda_SubLinea = New DAOEmpresaTienda_SubLinea()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeletexParams
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexParamsTest()
        Dim target As DAOEmpresaTienda_SubLinea = New DAOEmpresaTienda_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexParams(IdEmpresa, IdTienda, IdSubLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de EmpresaTienda_SubLinea_Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub EmpresaTienda_SubLinea_RegistrarTest()
        Dim target As DAOEmpresaTienda_SubLinea = New DAOEmpresaTienda_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim objETS As EmpresaTienda_SubLinea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.EmpresaTienda_SubLinea_Registrar(objETS, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdEmpresaxIdTiendaxIdSubLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdEmpresaxIdTiendaxIdSubLineaTest()
        Dim target As DAOEmpresaTienda_SubLinea = New DAOEmpresaTienda_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As EmpresaTienda_SubLinea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As EmpresaTienda_SubLinea
        actual = target.SelectxIdEmpresaxIdTiendaxIdSubLinea(IdEmpresa, IdTienda, IdSubLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxParams_DT
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxParams_DTTest()
        Dim target As DAOEmpresaTienda_SubLinea = New DAOEmpresaTienda_SubLinea() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.SelectxParams_DT(IdEmpresa, IdTienda, IdLinea, IdSubLinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
