﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCaja_AperturaCierreTest y se pretende que
'''contenga todas las pruebas unitarias DAOCaja_AperturaCierreTest.
'''</summary>
<TestClass()> _
Public Class DAOCaja_AperturaCierreTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCaja_AperturaCierre
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCaja_AperturaCierreConstructorTest()
        Dim target As DAOCaja_AperturaCierre = New DAOCaja_AperturaCierre()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Caja_AperturaCierre_Select
    '''</summary>
    <TestMethod()> _
    Public Sub Caja_AperturaCierre_SelectTest()
        Dim target As DAOCaja_AperturaCierre = New DAOCaja_AperturaCierre() ' TODO: Inicializar en un valor adecuado
        Dim Id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cac_FechaApertura As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim cac_FechaCierre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Caja_AperturaCierre) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Caja_AperturaCierre)
        actual = target.Caja_AperturaCierre_Select(Id, IdEmpresa, IdTienda, IdCaja, IdUsuario, cac_FechaApertura, cac_FechaCierre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Caja_AperturaCierre_SelectEstado
    '''</summary>
    <TestMethod()> _
    Public Sub Caja_AperturaCierre_SelectEstadoTest()
        Dim target As DAOCaja_AperturaCierre = New DAOCaja_AperturaCierre() ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cac_FechaApertura As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.Caja_AperturaCierre_SelectEstado(IdCaja, cac_FechaApertura)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Caja_AperturaCierre_Transaction
    '''</summary>
    <TestMethod()> _
    Public Sub Caja_AperturaCierre_TransactionTest()
        Dim target As DAOCaja_AperturaCierre = New DAOCaja_AperturaCierre() ' TODO: Inicializar en un valor adecuado
        Dim objCaja_AperturaCierre As Caja_AperturaCierre = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlCN As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlTR As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Caja_AperturaCierre_Transaction(objCaja_AperturaCierre, sqlCN, sqlTR)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
