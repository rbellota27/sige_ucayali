﻿Imports Microsoft.VisualBasic.ApplicationServices

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO.My



'''<summary>
'''Se trata de una clase de prueba para MyProjectTest y se pretende que
'''contenga todas las pruebas unitarias MyProjectTest.
'''</summary>
<TestClass()> _
Public Class MyProjectTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Application
    '''</summary>
    <TestMethod()> _
    Public Sub ApplicationTest()
        Dim actual As MyApplication
        actual = MyProject.Application
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Computer
    '''</summary>
    <TestMethod()> _
    Public Sub ComputerTest()
        Dim actual As MyComputer
        actual = MyProject.Computer
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de User
    '''</summary>
    <TestMethod()> _
    Public Sub UserTest()
        Dim actual As User
        actual = MyProject.User
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de WebServices
    '''</summary>
    <TestMethod()> _
    Public Sub WebServicesTest()
        Dim actual As MyProject.MyWebServices
        actual = MyProject.WebServices
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
