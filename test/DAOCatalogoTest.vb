﻿Imports System

Imports System.Data

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCatalogoTest y se pretende que
'''contenga todas las pruebas unitarias DAOCatalogoTest.
'''</summary>
<TestClass()> _
Public Class DAOCatalogoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCatalogo
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCatalogoConstructorTest()
        Dim target As DAOCatalogo = New DAOCatalogo()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de CatalogoProductoSelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub CatalogoProductoSelectxParamsTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim addListaUM_Venta As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Catalogo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Catalogo
        actual = target.CatalogoProductoSelectxParams(IdProducto, IdTienda, IdTipoPV, IdMonedaDestino, IdAlmacen, addListaUM_Venta)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CatalogoProducto_PV
    '''</summary>
    <TestMethod()> _
    Public Sub CatalogoProducto_PVTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.CatalogoProducto_PV(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdAlmacen, idtipopv)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CatalogoProducto_PV_Cotizacion_V2CodBarras
    '''</summary>
    <TestMethod()> _
    Public Sub CatalogoProducto_PV_Cotizacion_V2CodBarrasTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tableTipoTabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim codigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim filtroProductoCampania As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim codbarras As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.CatalogoProducto_PV_Cotizacion_V2CodBarras(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania, codbarras)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CatalogoProducto_PV_Paginado
    '''</summary>
    <TestMethod()> _
    Public Sub CatalogoProducto_PV_PaginadoTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.CatalogoProducto_PV_Paginado(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CatalogoProducto_PV_Paginado_Cotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub CatalogoProducto_PV_Paginado_CotizacionTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPais As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdFabricante As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMarca As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdModelo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdFormato As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProveedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEstilo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTransito As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCalidad As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.CatalogoProducto_PV_Paginado_Cotizacion(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, IdPais, IdFabricante, IdMarca, IdModelo, IdFormato, IdProveedor, IdEstilo, IdTransito, IdCalidad)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CatalogoProducto_PV_Paginado_Cotizacion_V2
    '''</summary>
    <TestMethod()> _
    Public Sub CatalogoProducto_PV_Paginado_Cotizacion_V2Test()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tableTipoTabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim codigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim filtroProductoCampania As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.CatalogoProducto_PV_Paginado_Cotizacion_V2(IdExistencia, IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarras
    '''</summary>
    <TestMethod()> _
    Public Sub CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tableTipoTabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim codigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim filtroProductoCampania As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim codbarras As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigoProveedor As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarras(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania, codbarras, IdTipoExistencia, codigoProveedor)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzado
    '''</summary>
    <TestMethod()> _
    Public Sub CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzadoTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tableTipoTabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim codigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim filtroProductoCampania As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim codbarras As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigoProveedor As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzado(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania, codbarras, IdTipoExistencia, codigoProveedor, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzadoV2
    '''</summary>
    <TestMethod()> _
    Public Sub CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzadoV2Test()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tableTipoTabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim codigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim filtroProductoCampania As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim codbarras As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigoProveedor As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarrasAvanzadoV2(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania, codbarras, IdTipoExistencia, codigoProveedor, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GetDescuentoxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub GetDescuentoxIdProductoTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Catalogo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Catalogo
        actual = target.GetDescuentoxIdProducto(IdUsuario, IdCondicionPago, IdMedioPago, IdTipoPV, IdTipoOperacion, IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Producto_Consultar_Stock_Precio_Venta
    '''</summary>
    <TestMethod()> _
    Public Sub Producto_Consultar_Stock_Precio_VentaTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoAlmancen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.Producto_Consultar_Stock_Precio_Venta(IdProducto, IdTienda, IdTipoPV, IdMonedaDestino, Fecha, IdEmpresa, IdTipoAlmancen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RptCatalogo
    '''</summary>
    <TestMethod()> _
    Public Sub RptCatalogoTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RptCatalogo(IdTienda, IdSublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RptCatalogoProductoPVxParametros
    '''</summary>
    <TestMethod()> _
    Public Sub RptCatalogoProductoPVxParametrosTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim umprincipal As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim umretazo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.RptCatalogoProductoPVxParametros(IdTienda, idtipopv, Idlinea, idsublinea, umprincipal, umretazo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectProductoxCodigoxDescripcion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectProductoxCodigoxDescripcionTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim NombreProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.SelectProductoxCodigoxDescripcion(IdTienda, IdTipoPV, CodigoProducto, NombreProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectProductoxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectProductoxIdProductoTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Catalogo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Catalogo
        actual = target.SelectProductoxIdProducto(IdProducto, IdTienda, IdTipoPV)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProductoDetalladoDescripcion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProductoDetalladoDescripcionTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Catalogo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Catalogo
        actual = target.SelectxIdProductoDetalladoDescripcion(IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdSublineaxIds
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdSublineaxIdsTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.SelectxIdSublineaxIds(IdEmpresa, IdTienda, IdAlmacen, IdSublinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdSublineaxIdsxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdSublineaxIdsxNombreTest()
        Dim target As DAOCatalogo = New DAOCatalogo() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim NombreProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.SelectxIdSublineaxIdsxNombre(IdEmpresa, IdTienda, IdAlmacen, IdSublinea, NombreProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
