﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOTipoDocumentoRefTest y se pretende que
'''contenga todas las pruebas unitarias DAOTipoDocumentoRefTest.
'''</summary>
<TestClass()> _
Public Class DAOTipoDocumentoRefTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOTipoDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub DAOTipoDocumentoRefConstructorTest()
        Dim target As DAOTipoDocumentoRef = New DAOTipoDocumentoRef()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeleteTipoDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub DeleteTipoDocumentoRefTest()
        Dim target As DAOTipoDocumentoRef = New DAOTipoDocumentoRef() ' TODO: Inicializar en un valor adecuado
        Dim TipoDocumentoRef As TipoDocumentoRef = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeleteTipoDocumentoRef(TipoDocumentoRef)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaTipoDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaTipoDocumentoRefTest()
        Dim target As DAOTipoDocumentoRef = New DAOTipoDocumentoRef() ' TODO: Inicializar en un valor adecuado
        Dim TipoDocumentoRef As TipoDocumentoRef = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaTipoDocumentoRef(TipoDocumentoRef)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdTipoDoc
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTipoDocTest()
        Dim target As DAOTipoDocumentoRef = New DAOTipoDocumentoRef() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDoc As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumentoRef) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumentoRef)
        actual = target.SelectxIdTipoDoc(IdTipoDoc)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdtipoDocRef
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdtipoDocRefTest()
        Dim target As DAOTipoDocumentoRef = New DAOTipoDocumentoRef() ' TODO: Inicializar en un valor adecuado
        Dim idTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento)
        actual = target.SelectxIdtipoDocRef(idTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
