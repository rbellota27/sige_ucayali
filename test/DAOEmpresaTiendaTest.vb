﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOEmpresaTiendaTest y se pretende que
'''contenga todas las pruebas unitarias DAOEmpresaTiendaTest.
'''</summary>
<TestClass()> _
Public Class DAOEmpresaTiendaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOEmpresaTienda
    '''</summary>
    <TestMethod()> _
    Public Sub DAOEmpresaTiendaConstructorTest()
        Dim target As DAOEmpresaTienda = New DAOEmpresaTienda()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaEmpresaTienda
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaEmpresaTiendaTest()
        Dim target As DAOEmpresaTienda = New DAOEmpresaTienda() ' TODO: Inicializar en un valor adecuado
        Dim empresatienda As EmpresaTienda = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaEmpresaTienda(empresatienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeletexIdEmpresa
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexIdEmpresaTest()
        Dim target As DAOEmpresaTienda = New DAOEmpresaTienda() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexIdEmpresa(cn, IdEmpresa, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GrabaEmpresaTiendaT
    '''</summary>
    <TestMethod()> _
    Public Sub GrabaEmpresaTiendaTTest()
        Dim target As DAOEmpresaTienda = New DAOEmpresaTienda() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lempresatienda As List(Of EmpresaTienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.GrabaEmpresaTiendaT(cn, lempresatienda, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaEmpresaTiendaT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaEmpresaTiendaTTest()
        Dim target As DAOEmpresaTienda = New DAOEmpresaTienda() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lempresatienda As List(Of EmpresaTienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaEmpresaTiendaT(cn, lempresatienda, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectGrillaxEmpresaTienda
    '''</summary>
    <TestMethod()> _
    Public Sub SelectGrillaxEmpresaTiendaTest()
        Dim target As DAOEmpresaTienda = New DAOEmpresaTienda() ' TODO: Inicializar en un valor adecuado
        Dim idEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of EmpresaTienda) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of EmpresaTienda)
        actual = target.SelectGrillaxEmpresaTienda(idEmpresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
