﻿Imports System.Data.SqlClient

Imports System

Imports System.Collections.Generic

Imports Entidades

Imports System.Data

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocGuiaRemisionTest y se pretende que
'''contenga todas las pruebas unitarias DAODocGuiaRemisionTest.
'''</summary>
<TestClass()> _
Public Class DAODocGuiaRemisionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocGuiaRemision
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocGuiaRemisionConstructorTest()
        Dim target As DAODocGuiaRemision = New DAODocGuiaRemision()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de CostoFlete_AnalisisCostoFlete_PrecioVenta
    '''</summary>
    <TestMethod()> _
    Public Sub CostoFlete_AnalisisCostoFlete_PrecioVentaTest()
        Dim target As DAODocGuiaRemision = New DAODocGuiaRemision() ' TODO: Inicializar en un valor adecuado
        Dim Tabla_IdDocumento As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Producto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTiendaOrigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.CostoFlete_AnalisisCostoFlete_PrecioVenta(Tabla_IdDocumento, IdLinea, IdSubLinea, Producto, CodigoProducto, IdTiendaDestino, IdTipoPV, IdTiendaOrigen, IdMoneda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaRemisionSelectDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaRemisionSelectDetalleTest()
        Dim target As DAODocGuiaRemision = New DAODocGuiaRemision() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.DocumentoGuiaRemisionSelectDetalle(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaRemisionSelectDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaRemisionSelectDocumentoRefTest()
        Dim target As DAODocGuiaRemision = New DAODocGuiaRemision() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.DocumentoGuiaRemisionSelectDocumentoRef(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaRemisionSelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaRemisionSelectxParamsTest()
        Dim target As DAODocGuiaRemision = New DAODocGuiaRemision() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocGuiaRemision = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocGuiaRemision
        actual = target.DocumentoGuiaRemisionSelectxParams(IdSerie, codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaRemision_CostoFlete_SelectxParams
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaRemision_CostoFlete_SelectxParamsTest()
        Dim target As DAODocGuiaRemision = New DAODocGuiaRemision() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacenOrigen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacenDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.DocumentoGuiaRemision_CostoFlete_SelectxParams(IdEmpresa, IdAlmacenOrigen, IdAlmacenDestino, FechaInicio, FechaFin, Serie, Codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaRemision_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaRemision_DeshacerMovTest()
        Dim target As DAODocGuiaRemision = New DAODocGuiaRemision() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim deleteDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteMovAlmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deletePuntoPartida As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deletePuntoLlegada As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteObservaciones As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteRelacionDoc As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim validar_Despacho As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoGuiaRemision_DeshacerMov(IdDocumento, deleteDetalleDocumento, deleteMovAlmacen, deletePuntoPartida, deletePuntoLlegada, deleteObservaciones, deleteRelacionDoc, validar_Despacho, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoGuiaRemision_DetalleInsert
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoGuiaRemision_DetalleInsertTest()
        Dim target As DAODocGuiaRemision = New DAODocGuiaRemision() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalleDocumento As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Factor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim MoverStockFisico As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim ValidarDespacho As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.DocumentoGuiaRemision_DetalleInsert(cn, listaDetalleDocumento, tr, IdDocumento, Factor, MoverStockFisico, ValidarDespacho)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoSolicitudPagpSelectDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoSolicitudPagpSelectDocumentoRefTest()
        Dim target As DAODocGuiaRemision = New DAODocGuiaRemision() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.DocumentoSolicitudPagpSelectDocumentoRef(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
