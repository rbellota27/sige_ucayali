﻿Imports System.Data

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOComparativoVentasAnualTest y se pretende que
'''contenga todas las pruebas unitarias DAOComparativoVentasAnualTest.
'''</summary>
<TestClass()> _
Public Class DAOComparativoVentasAnualTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOComparativoVentasAnual
    '''</summary>
    <TestMethod()> _
    Public Sub DAOComparativoVentasAnualConstructorTest()
        Dim target As DAOComparativoVentasAnual = New DAOComparativoVentasAnual()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ComparativoVentasAnual
    '''</summary>
    <TestMethod()> _
    Public Sub ComparativoVentasAnualTest()
        Dim target As DAOComparativoVentasAnual = New DAOComparativoVentasAnual() ' TODO: Inicializar en un valor adecuado
        Dim year As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim variable As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idpropietario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.ComparativoVentasAnual(year, variable, IdPersona, idMoneda, idpropietario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de reporteComisiones
    '''</summary>
    <TestMethod()> _
    Public Sub reporteComisionesTest()
        Dim target As DAOComparativoVentasAnual = New DAOComparativoVentasAnual() ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechafin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim empresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim perfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim rol As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim usuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.reporteComisiones(fechainicio, fechafin, empresa, tienda, perfil, rol, usuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
