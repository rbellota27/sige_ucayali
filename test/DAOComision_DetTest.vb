﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports System.Data

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOComision_DetTest y se pretende que
'''contenga todas las pruebas unitarias DAOComision_DetTest.
'''</summary>
<TestClass()> _
Public Class DAOComision_DetTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOComision_Det
    '''</summary>
    <TestMethod()> _
    Public Sub DAOComision_DetConstructorTest()
        Dim target As DAOComision_Det = New DAOComision_Det()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de CR_ComisionxVendedor
    '''</summary>
    <TestMethod()> _
    Public Sub CR_ComisionxVendedorTest()
        Dim target As DAOComision_Det = New DAOComision_Det() ' TODO: Inicializar en un valor adecuado
        Dim IdComisionCab As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdVendedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaIni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Tabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.CR_ComisionxVendedor(IdComisionCab, IdEmpresa, IdTienda, IdVendedor, FechaIni, FechaFin, IdTipoExistencia, IdLinea, IdSubLinea, IdProducto, Tabla)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CR_DetalleComision
    '''</summary>
    <TestMethod()> _
    Public Sub CR_DetalleComisionTest()
        Dim target As DAOComision_Det = New DAOComision_Det() ' TODO: Inicializar en un valor adecuado
        Dim IdComisionCab As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdVendedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaIni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As DataSet = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataSet
        actual = target.CR_DetalleComision(IdComisionCab, IdEmpresa, IdTienda, IdVendedor, FechaIni, FechaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Comision_SelectProducto_AddDetallexParams
    '''</summary>
    <TestMethod()> _
    Public Sub Comision_SelectProducto_AddDetallexParamsTest()
        Dim target As DAOComision_Det = New DAOComision_Det() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Tabla_IdProducto As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdComisionCab As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FiltrarComsion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Comision_Det) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Comision_Det)
        actual = target.Comision_SelectProducto_AddDetallexParams(IdLinea, IdSubLinea, Tabla_IdProducto, IdTienda, IdComisionCab, FiltrarComsion, Descripcion, CodigoProducto, PageIndex, PageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Registrar
    '''</summary>
    <TestMethod()> _
    Public Sub RegistrarTest()
        Dim target As DAOComision_Det = New DAOComision_Det() ' TODO: Inicializar en un valor adecuado
        Dim objComisionDet As Comision_Det = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Registrar(objComisionDet, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
