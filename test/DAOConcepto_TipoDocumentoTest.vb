﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOConcepto_TipoDocumentoTest y se pretende que
'''contenga todas las pruebas unitarias DAOConcepto_TipoDocumentoTest.
'''</summary>
<TestClass()> _
Public Class DAOConcepto_TipoDocumentoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOConcepto_TipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DAOConcepto_TipoDocumentoConstructorTest()
        Dim target As DAOConcepto_TipoDocumento = New DAOConcepto_TipoDocumento()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeletexCpto_TipoDoc
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexCpto_TipoDocTest()
        Dim target As DAOConcepto_TipoDocumento = New DAOConcepto_TipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdConcepto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexCpto_TipoDoc(cn, tr, IdConcepto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaListaConcepto_TipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaListaConcepto_TipoDocumentoTest()
        Dim target As DAOConcepto_TipoDocumento = New DAOConcepto_TipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of Concepto_TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdConcepto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.InsertaListaConcepto_TipoDocumento(cn, tr, lista, IdConcepto)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdTipoDocumentoTest()
        Dim target As DAOConcepto_TipoDocumento = New DAOConcepto_TipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Concepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Concepto)
        actual = target.SelectCboxIdTipoDocumento(IdTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectConceptoDetraccionxIdTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectConceptoDetraccionxIdTipoDocumentoTest()
        Dim target As DAOConcepto_TipoDocumento = New DAOConcepto_TipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Concepto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Concepto)
        actual = target.SelectConceptoDetraccionxIdTipoDocumento(IdTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectConceptoTipoDocxIdTipoGastoxNomConcepto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectConceptoTipoDocxIdTipoGastoxNomConceptoTest()
        Dim target As DAOConcepto_TipoDocumento = New DAOConcepto_TipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoGasto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim NomConcepto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Concepto_TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Concepto_TipoDocumento)
        actual = target.SelectConceptoTipoDocxIdTipoGastoxNomConcepto(IdTipoGasto, NomConcepto, pageindex, pagesize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de _Concepto_TipoDocumentoEditarxIdConcepto
    '''</summary>
    <TestMethod()> _
    Public Sub _Concepto_TipoDocumentoEditarxIdConceptoTest()
        Dim target As DAOConcepto_TipoDocumento = New DAOConcepto_TipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdConcepto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Concepto_TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Concepto_TipoDocumento)
        actual = target._Concepto_TipoDocumentoEditarxIdConcepto(IdConcepto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarMoneda
    '''</summary>
    <TestMethod()> _
    Public Sub listarMonedaTest()
        Dim target As DAOConcepto_TipoDocumento = New DAOConcepto_TipoDocumento() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Concepto_TipoDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Concepto_TipoDocumento)
        actual = target.listarMoneda
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
