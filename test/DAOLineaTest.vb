﻿Imports System.Collections.Generic

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOLineaTest y se pretende que
'''contenga todas las pruebas unitarias DAOLineaTest.
'''</summary>
<TestClass()> _
Public Class DAOLineaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOLinea
    '''</summary>
    <TestMethod()> _
    Public Sub DAOLineaConstructorTest()
        Dim target As DAOLinea = New DAOLinea()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaLinea
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaLineaTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim objLinea As Linea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaLinea(objLinea, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaLinea
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaLineaTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim linea As Linea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaLinea(linea, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxTipoExistencia
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxTipoExistenciaTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim idTipoEx As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea)
        actual = target.SelectActivoxTipoExistencia(idTipoEx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxTipoExistenciaCodigo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxTipoExistenciaCodigoTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim idTipoEx As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea)
        actual = target.SelectActivoxTipoExistenciaCodigo(idTipoEx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAtributoxIdLineaxEstado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAtributoxIdLineaxEstadoTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idexistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea)
        actual = target.SelectAtributoxIdLineaxEstado(idlinea, idexistencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboLineaxIdtipoexistencia
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboLineaxIdtipoexistenciaTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim idexistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea)
        actual = target.SelectCboLineaxIdtipoexistencia(idexistencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectFlagActivoxIdLinea
    '''</summary>
    <TestMethod()> _
    Public Sub SelectFlagActivoxIdLineaTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Linea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Linea
        actual = target.SelectFlagActivoxIdLinea(idlinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectValidarIdLineaxProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectValidarIdLineaxProductoTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Linea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Linea
        actual = target.SelectValidarIdLineaxProducto(idlinea)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdLineaxEstado
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdLineaxEstadoTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim estado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idexistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea)
        actual = target.SelectxIdLineaxEstado(idlinea, estado, idexistencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidadLongitud
    '''</summary>
    <TestMethod()> _
    Public Sub ValidadLongitudTest()
        Dim target As DAOLinea = New DAOLinea() ' TODO: Inicializar en un valor adecuado
        Dim expected As Linea = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Linea
        actual = target.ValidadLongitud
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
