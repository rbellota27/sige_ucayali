﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOOrdenTrabajoTest y se pretende que
'''contenga todas las pruebas unitarias DAOOrdenTrabajoTest.
'''</summary>
<TestClass()> _
Public Class DAOOrdenTrabajoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOOrdenTrabajo
    '''</summary>
    <TestMethod()> _
    Public Sub DAOOrdenTrabajoConstructorTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizarDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizarDocumentoTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim objProductoOt As List(Of VehiculoOt) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObs As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objFinanciamientoOt As FinanciamientoOT = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizarDocumento(objProductoOt, objObs, objFinanciamientoOt)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizarDocumentoAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizarDocumentoAlmacenTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ActualizarDocumentoAlmacen(cn, tr, iddocumento, idalmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Actualizar_FinanciamientoOT
    '''</summary>
    <TestMethod()> _
    Public Sub Actualizar_FinanciamientoOTTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim obj As FinanciamientoOT = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.Actualizar_FinanciamientoOT(obj, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Actualizar_ProductoOT
    '''</summary>
    <TestMethod()> _
    Public Sub Actualizar_ProductoOTTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.Actualizar_ProductoOT(iddocumento, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de AnularOrdenTrabajo
    '''</summary>
    <TestMethod()> _
    Public Sub AnularOrdenTrabajoTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim idDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.AnularOrdenTrabajo(idDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de inserta_vehiculoOT
    '''</summary>
    <TestMethod()> _
    Public Sub inserta_vehiculoOTTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim objvehiculo As VehiculoOt = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.inserta_vehiculoOT(objvehiculo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de insertarDetallesOrdenTrabajo
    '''</summary>
    <TestMethod()> _
    Public Sub insertarDetallesOrdenTrabajoTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim listProdusctos As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim OBJDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim moverAlmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim comprometerStock As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim operativo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.insertarDetallesOrdenTrabajo(listProdusctos, OBJDocumento, moverAlmacen, comprometerStock, operativo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de insertarOrdenTrabajo
    '''</summary>
    <TestMethod()> _
    Public Sub insertarOrdenTrabajoTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim objDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objProductoOt As List(Of VehiculoOt) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim obs As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objFinanciamientoOt As FinanciamientoOT = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.insertarOrdenTrabajo(objDocumento, objProductoOt, obs, objFinanciamientoOt)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de insertarProductoOT
    '''</summary>
    <TestMethod()> _
    Public Sub insertarProductoOTTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim obj As VehiculoOt = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.insertarProductoOT(obj, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de insertarVehiculo
    '''</summary>
    <TestMethod()> _
    Public Sub insertarVehiculoTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim obj As VehiculoOt = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.insertarVehiculo(obj, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de insertar_FinanciamientoOT
    '''</summary>
    <TestMethod(), _
     DeploymentItem("DAO.dll")> _
    Public Sub insertar_FinanciamientoOTTest()
        Dim target As DAOOrdenTrabajo_Accessor = New DAOOrdenTrabajo_Accessor() ' TODO: Inicializar en un valor adecuado
        Dim obj As FinanciamientoOT = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim t As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.insertar_FinanciamientoOT(obj, cn, t)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarDetalleServicio
    '''</summary>
    <TestMethod()> _
    Public Sub listarDetalleServicioTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim idproducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.listarDetalleServicio(idproducto, idtienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarLineaMarca
    '''</summary>
    <TestMethod()> _
    Public Sub listarLineaMarcaTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim mante As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Marca) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Marca)
        actual = target.listarLineaMarca(idlinea, mante)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarModeloMarcaLinea
    '''</summary>
    <TestMethod()> _
    Public Sub listarModeloMarcaLineaTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim idlineaMarca As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ModeloLineaMarca) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ModeloLineaMarca)
        actual = target.listarModeloMarcaLinea(idlineaMarca)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarOrdenTrabajo
    '''</summary>
    <TestMethod()> _
    Public Sub listarOrdenTrabajoTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim idfinanciamiento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As OrdenTrabajoView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As OrdenTrabajoView
        actual = target.listarOrdenTrabajo(idfinanciamiento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarOrdenTrabajo
    '''</summary>
    <TestMethod()> _
    Public Sub listarOrdenTrabajoTest1()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim nomcli As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim doccli As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim placa As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim nrodoc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of OrdenTrabajoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of OrdenTrabajoView)
        actual = target.listarOrdenTrabajo(nomcli, doccli, placa, nrodoc, idtienda, pageindex, pagesize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarOrdenTrabajoL
    '''</summary>
    <TestMethod()> _
    Public Sub listarOrdenTrabajoLTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of OrdenTrabajoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of OrdenTrabajoView)
        actual = target.listarOrdenTrabajoL(idtienda, pageindex, pagesize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarPersonaNaturalJuridica
    '''</summary>
    <TestMethod()> _
    Public Sub listarPersonaNaturalJuridicaTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim ruc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.listarPersonaNaturalJuridica(nombre, dni, ruc, pageindex, pagesize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarPersonaNaturalJuridicaX
    '''</summary>
    <TestMethod()> _
    Public Sub listarPersonaNaturalJuridicaXTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim idvehiculo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim dni As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim ruc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim pageindex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PersonaView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PersonaView)
        actual = target.listarPersonaNaturalJuridicaX(idvehiculo, nombre, dni, ruc, pageindex, pagesize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarProductoOT
    '''</summary>
    <TestMethod()> _
    Public Sub listarProductoOTTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim idfinanciamiento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumentoView)
        actual = target.listarProductoOT(idfinanciamiento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarProductoOTL
    '''</summary>
    <TestMethod()> _
    Public Sub listarProductoOTLTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumentoView)
        actual = target.listarProductoOTL(iddocumento, idtienda, tipoPV)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarProductosLiquidacion
    '''</summary>
    <TestMethod()> _
    Public Sub listarProductosLiquidacionTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim tipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idlinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim codsublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.listarProductosLiquidacion(tipoPV, idtienda, idalmacen, idlinea, idsublinea, descripcion, codsublinea, pageIndex, pageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarVehiculoxPlaca
    '''</summary>
    <TestMethod()> _
    Public Sub listarVehiculoxPlacaTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim placa As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As VehiculoOt = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As VehiculoOt
        actual = target.listarVehiculoxPlaca(placa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de llenarLineaxTipoExistencia
    '''</summary>
    <TestMethod()> _
    Public Sub llenarLineaxTipoExistenciaTest()
        Dim target As DAOOrdenTrabajo = New DAOOrdenTrabajo() ' TODO: Inicializar en un valor adecuado
        Dim idexistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Linea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Linea)
        actual = target.llenarLineaxTipoExistencia(idexistencia)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
