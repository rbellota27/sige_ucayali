﻿Imports System.Collections.Generic

Imports System.Data

Imports System

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para daCotizacionTest y se pretende que
'''contenga todas las pruebas unitarias daCotizacionTest.
'''</summary>
<TestClass()> _
Public Class daCotizacionTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor daCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub daCotizacionConstructorTest()
        Dim target As daCotizacion = New daCotizacion()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de GetVectorParametros
    '''</summary>
    <TestMethod()> _
    Public Sub GetVectorParametrosTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim x As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected() As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual() As SqlParameter
        actual = target.GetVectorParametros(x)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UCBool
    '''</summary>
    <TestMethod()> _
    Public Sub UCBoolTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim obj As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UCBool(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UCDate
    '''</summary>
    <TestMethod()> _
    Public Sub UCDateTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim obj As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim actual As DateTime
        actual = target.UCDate(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UCDbl
    '''</summary>
    <TestMethod()> _
    Public Sub UCDblTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim obj As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Double = 0.0! ' TODO: Inicializar en un valor adecuado
        Dim actual As Double
        actual = target.UCDbl(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UCDec
    '''</summary>
    <TestMethod()> _
    Public Sub UCDecTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim obj As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim actual As [Decimal]
        actual = target.UCDec(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UCInt
    '''</summary>
    <TestMethod()> _
    Public Sub UCIntTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim obj As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.UCInt(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UCStr
    '''</summary>
    <TestMethod()> _
    Public Sub UCStrTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim obj As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.UCStr(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de actualizar
    '''</summary>
    <TestMethod()> _
    Public Sub actualizarTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim con As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Documento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim AnexoDocumento As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista_DetalleDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim lista_DetalleConcepto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim objObservacion As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objPuntoPartida As PuntoPartida = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objPuntoLlegada As PuntoLlegada = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objMontoRegimenPercepcion As MontoRegimen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista_RelacionDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.actualizar(con, Documento, AnexoDocumento, lista_DetalleDocumento, lista_DetalleConcepto, objObservacion, objPuntoPartida, objPuntoLlegada, objMontoRegimenPercepcion, lista_RelacionDocumento, trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de adicionar
    '''</summary>
    <TestMethod()> _
    Public Sub adicionarTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim con As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Documento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim AnexoDocumento As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista_DetalleDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim lista_DetalleConcepto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim objObservacion As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objPuntoPartida As PuntoPartida = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objPuntoLlegada As PuntoLlegada = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objMontoRegimenPercepcion As MontoRegimen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista_RelacionDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.adicionar(con, Documento, AnexoDocumento, lista_DetalleDocumento, lista_DetalleConcepto, objObservacion, objPuntoPartida, objPuntoLlegada, objMontoRegimenPercepcion, lista_RelacionDocumento, trx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de buscarCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub buscarCotizacionTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nroDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Consignacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim IdMagnitudPeso As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PesoTotal As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumentoMP As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim con As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As beCotizacionBuscar = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As beCotizacionBuscar
        actual = target.buscarCotizacion(IdSerie, nroDocumento, IdDocumento, IdTipoDocumento, Consignacion, IdMagnitudPeso, PesoTotal, IdTipoDocumentoMP, con)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de cargarDocumentoReferencia
    '''</summary>
    <TestMethod()> _
    Public Sub cargarDocumentoReferenciaTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoReferencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Fecha As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoPV As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdVendedor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim con As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As beCotizacionReferencia = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As beCotizacionReferencia
        actual = target.cargarDocumentoReferencia(IdDocumentoReferencia, IdEmpresa, IdTienda, Fecha, IdTipoPV, IdMoneda, IdVendedor, IdTipoDocumento, CondicionPago, con)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getParam
    '''</summary>
    <TestMethod()> _
    Public Sub getParamTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim a As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim parameterName As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim BDTipo As SqlDbType = New SqlDbType() ' TODO: Inicializar en un valor adecuado
        Dim expected As SqlParameter = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As SqlParameter
        actual = target.getParam(a, parameterName, BDTipo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getParamValueChar
    '''</summary>
    <TestMethod()> _
    Public Sub getParamValueCharTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim a As Char = Global.Microsoft.VisualBasic.ChrW(0) ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.getParamValueChar(a)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getParamValueDate
    '''</summary>
    <TestMethod()> _
    Public Sub getParamValueDateTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim a As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.getParamValueDate(a)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getParamValueDec
    '''</summary>
    <TestMethod()> _
    Public Sub getParamValueDecTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim a As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.getParamValueDec(a)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getParamValueInt
    '''</summary>
    <TestMethod()> _
    Public Sub getParamValueIntTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim a As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.getParamValueInt(a)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de getParamValueStr
    '''</summary>
    <TestMethod()> _
    Public Sub getParamValueStrTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim a As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Object = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Object
        actual = target.getParamValueStr(a)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de nuevaCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub nuevaCotizacionTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim con As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As beCotizacionNuevo = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As beCotizacionNuevo
        actual = target.nuevaCotizacion(IdTienda, IdSerie, con)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de obtenerListas
    '''</summary>
    <TestMethod()> _
    Public Sub obtenerListasTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodDep As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim CodProv As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMangitud As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Permisos As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Parametros As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim con As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As beCotizacionListas = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As beCotizacionListas
        actual = target.obtenerListas(IdUsuario, IdEmpresa, IdTienda, IdTipoDocumento, IdTipoOperacion, IdCondicionPago, CodDep, CodProv, IdTipoExistencia, IdMangitud, Permisos, Parametros, con)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de obtenerProductos
    '''</summary>
    <TestMethod()> _
    Public Sub obtenerProductosTest()
        Dim target As daCotizacion = New daCotizacion() ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodSubLinea As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Descripcion As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMonedaDestino As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idtipopv As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagesize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pagenumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tableTipoTabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim codigoProducto As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim filtroProductoCampania As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim codbarras As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigoProveedor As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim con As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Catalogo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Catalogo)
        actual = target.obtenerProductos(IdLinea, IdSubLinea, CodSubLinea, Descripcion, IdTienda, IdMonedaDestino, IdEmpresa, IdAlmacen, idtipopv, pagesize, pagenumber, IdUsuario, IdCondicionPago, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, filtroProductoCampania, codbarras, IdTipoExistencia, codigoProveedor, IdTipoOperacion, con)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
