﻿Imports System

Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoNotaCreditoTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoNotaCreditoTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoNotaCreditoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumentoNotaCredito
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoNotaCreditoConstructorTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNC_Deshacer_MovFact
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNC_Deshacer_MovFactTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoNC_Deshacer_MovFact(IdDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNotaCreditoAplicacion_DeshacerAplicacion
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNotaCreditoAplicacion_DeshacerAplicacionTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim deshacer_AppDeudas As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deshacer_AppEfectivoCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoNotaCreditoAplicacion_DeshacerAplicacion(IdDocumento, deshacer_AppDeudas, deshacer_AppEfectivoCaja, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNotaCreditoAplicacion_ValSaldoRestante
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNotaCreditoAplicacion_ValSaldoRestanteTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoNotaCredito As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Opcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoNotaCreditoAplicacion_ValSaldoRestante(IdDocumentoNotaCredito, Opcion, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNotaCreditoDeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNotaCreditoDeshacerMovTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim DeleteRelacionDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim DeleteObservacion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteDetalleConcepto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoNotaCreditoDeshacerMov(IdDocumento, DeleteRelacionDocumento, DeleteDetalleDocumento, DeleteObservacion, deleteDetalleConcepto, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNotaCreditoSelectCab
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNotaCreditoSelectCabTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Entidades.DocumentoNotaCredito = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Entidades.DocumentoNotaCredito
        actual = target.DocumentoNotaCreditoSelectCab(IdSerie, Codigo, IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNotaCreditoSelectCabFind_Aplicacion
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNotaCreditoSelectCabFind_AplicacionTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Filtro As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoNotaCreditoSelectCabFind_Aplicacion(IdPersona, IdTienda, Filtro)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNotaCredito_AplicacionDeudas
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNotaCredito_AplicacionDeudasTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoNotaCredito As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Abono As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim IdMovCuentaDocRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoNotaCredito_AplicacionDeudas(IdDocumentoNotaCredito, IdDocumentoRef, Abono, IdMovCuentaDocRef, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNotaCredito_AplicacionEfectivoCaja
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNotaCredito_AplicacionEfectivoCajaTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCaja As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoMov As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Factor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Monto As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoNotaCredito_AplicacionEfectivoCaja(IdDocumento, IdEmpresa, IdTienda, IdCaja, IdUsuario, IdTipoMov, IdMedioPago, IdMoneda, Factor, Monto, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNotaCredito_DisminuirSaldo_Ventas
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNotaCredito_DisminuirSaldo_VentasTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim monto As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoNotaCredito_DisminuirSaldo_Ventas(IdDocumento, monto, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNotaCredito_UpdateSaldoFavorCliente
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNotaCredito_UpdateSaldoFavorClienteTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim monto As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoNotaCredito_UpdateSaldoFavorCliente(IdDocumento, monto, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoNotaCredito_ValidarDocumentoNC_Rel
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoNotaCredito_ValidarDocumentoNC_RelTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMoneda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Monto_NC As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoNotaCredito_ValidarDocumentoNC_Rel(IdDocumentoRef, IdMoneda, Monto_NC, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Registrar_DetalleDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub Registrar_DetalleDocumentoTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim ListaDetalleDocumento As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.Registrar_DetalleDocumento(ListaDetalleDocumento, IdDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocReferenciaxParams
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocReferenciaxParamsTest()
        Dim target As DAODocumentoNotaCredito = New DAODocumentoNotaCredito() ' TODO: Inicializar en un valor adecuado
        Dim IdCliente As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaI As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaF As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim opcion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim ValDocRelacionado As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        'actual = target.SelectDocReferenciaxParams(IdCliente, FechaI, FechaF, IdTienda, serie, codigo, opcion, ValDocRelacionado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
