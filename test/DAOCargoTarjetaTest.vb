﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCargoTarjetaTest y se pretende que
'''contenga todas las pruebas unitarias DAOCargoTarjetaTest.
'''</summary>
<TestClass()> _
Public Class DAOCargoTarjetaTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCargoTarjeta
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCargoTarjetaConstructorTest()
        Dim target As DAOCargoTarjeta = New DAOCargoTarjeta()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaCargoTarjeta
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaCargoTarjetaTest()
        Dim target As DAOCargoTarjeta = New DAOCargoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim CargoTarjeta As CargoTarjeta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaCargoTarjeta(CargoTarjeta)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaCargoTarjeta
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaCargoTarjetaTest()
        Dim target As DAOCargoTarjeta = New DAOCargoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim CargoTarjeta As CargoTarjeta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaCargoTarjeta(CargoTarjeta)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivoxNombre_Cbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoxNombre_CboTest()
        Dim target As DAOCargoTarjeta = New DAOCargoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim Nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CargoTarjeta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CargoTarjeta)
        actual = target.SelectAllActivoxNombre_Cbo(Nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCargoTarjeta
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCargoTarjetaTest()
        Dim target As DAOCargoTarjeta = New DAOCargoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim estado As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CargoTarjeta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CargoTarjeta)
        actual = target.SelectCargoTarjeta(estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCodigoxIdCargoTarjeta
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCodigoxIdCargoTarjetaTest()
        Dim target As DAOCargoTarjeta = New DAOCargoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim IdCargoTarjeta As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.SelectCodigoxIdCargoTarjeta(IdCargoTarjeta)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxEstadoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxEstadoxNombreTest()
        Dim target As DAOCargoTarjeta = New DAOCargoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim estado As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of CargoTarjeta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of CargoTarjeta)
        actual = target.SelectxEstadoxNombre(nombre, estado)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdCargoTarjeta
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdCargoTarjetaTest()
        Dim target As DAOCargoTarjeta = New DAOCargoTarjeta() ' TODO: Inicializar en un valor adecuado
        Dim idCargoTarjeta As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As CargoTarjeta = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As CargoTarjeta
        actual = target.SelectxIdCargoTarjeta(idCargoTarjeta)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
