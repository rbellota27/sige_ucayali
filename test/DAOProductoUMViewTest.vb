﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOProductoUMViewTest y se pretende que
'''contenga todas las pruebas unitarias DAOProductoUMViewTest.
'''</summary>
<TestClass()> _
Public Class DAOProductoUMViewTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOProductoUMView
    '''</summary>
    <TestMethod()> _
    Public Sub DAOProductoUMViewConstructorTest()
        Dim target As DAOProductoUMView = New DAOProductoUMView()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboxIdProductoTest()
        Dim target As DAOProductoUMView = New DAOProductoUMView() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoUMView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoUMView)
        actual = target.SelectCboxIdProducto(IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectUMPrincipalxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectUMPrincipalxIdProductoTest()
        Dim target As DAOProductoUMView = New DAOProductoUMView() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As ProductoUMView = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As ProductoUMView
        actual = target.SelectUMPrincipalxIdProducto(IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdProducto
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdProductoTest()
        Dim target As DAOProductoUMView = New DAOProductoUMView() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ProductoUMView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ProductoUMView)
        actual = target.SelectxIdProducto(IdProducto)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
