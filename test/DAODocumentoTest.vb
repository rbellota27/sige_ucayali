﻿Imports System.Data

Imports System

Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocumentoTest y se pretende que
'''contenga todas las pruebas unitarias DAODocumentoTest.
'''</summary>
<TestClass()> _
Public Class DAODocumentoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocumentoConstructorTest()
        Dim target As DAODocumento = New DAODocumento()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizarDocumentoGuia
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizarDocumentoGuiaTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim documento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim LDet As List(Of DetalleGuia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoOld As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim factorMov As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMetValuacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim inventarioInicial As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ActualizarDocumentoGuia(documento, LDet, IdDocumentoOld, factorMov, IdAlmacen, IdMetValuacion, inventarioInicial)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de AgregarCheckDocumentoxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub AgregarCheckDocumentoxIdDocumentoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.AgregarCheckDocumentoxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de AnulaT
    '''</summary>
    <TestMethod()> _
    Public Sub AnulaTTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.AnulaT(cn, T, IdDocumento)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de AnularGuiaRecepcionxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub AnularGuiaRecepcionxIdDocumentoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.AnularGuiaRecepcionxIdDocumento(IdDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de AutoGenerarDocumentos_Salteados
    '''</summary>
    <TestMethod()> _
    Public Sub AutoGenerarDocumentos_SalteadosTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodigoInicio As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CodigoFin As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.AutoGenerarDocumentos_Salteados(IdSerie, CodigoInicio, CodigoFin, IdUsuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de CambiarPuntoLLegada
    '''</summary>
    <TestMethod()> _
    Public Sub CambiarPuntoLLegadaTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.CambiarPuntoLLegada(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DeleteDocSalteadoxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DeleteDocSalteadoxIdDocumentoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeleteDocSalteadoxIdDocumento(IdDocumento, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoCabSelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoCabSelectxIdTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.DocumentoCabSelectxId(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoOCSelect
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoOCSelectTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechaini As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fechafin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.DocumentoOCSelect(serie, codigo, fechaini, fechafin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoOCSelectDT
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoOCSelectDTTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim obj As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.DocumentoOCSelectDT(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoSelectDocxDespachar_V2
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoSelectDocxDespachar_V2Test()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoSelectDocxDespachar_V2(IdEmpresa, IdAlmacen, FechaInicio, FechaFin, Serie, Codigo, IdPersona, IdTipoDocumento, IdTipoOperacion, IdTipoDocumentoRef)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoSelectDocxDespachar_V2_Detalle
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoSelectDocxDespachar_V2_DetalleTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoSelectDocxDespachar_V2_Detalle(IdEmpresa, IdAlmacen, FechaInicio, FechaFin, Serie, Codigo, IdPersona, IdTipoDocumento, IdTipoOperacion, IdTipoDocumentoRef)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerieTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(IdTipoDocumento, IdSerie, IdPersona, FechaInicio, FechaFin)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoUpdate
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoUpdateTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim documento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoUpdate(documento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoVenta_DeshacerMov
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoVenta_DeshacerMovTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim deleteObservaciones As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteDetalleDocumento As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteMovALmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteMontoRegimen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deletePuntoLlegada As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteMovCaja As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim deleteMovCuenta As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim Anular As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoVenta_DeshacerMov(IdDocumento, deleteObservaciones, deleteDetalleDocumento, deleteMovALmacen, deleteMontoRegimen, deletePuntoLlegada, deleteMovCaja, deleteMovCuenta, Anular, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Documento_Buscar
    '''</summary>
    <TestMethod()> _
    Public Sub Documento_BuscarTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Val_FechaVcto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim IdEstadoDoc As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.Documento_Buscar(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, Val_FechaVcto, IdEstadoDoc)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Documento_BuscarDocumentoRef
    '''</summary>
    <TestMethod()> _
    Public Sub Documento_BuscarDocumentoRefTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Val_FechaVcto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim val_CotizacionCanjeUnico As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim IdtipoDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.Documento_BuscarDocumentoRef(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, Val_FechaVcto, val_CotizacionCanjeUnico, IdtipoDocumentoRef, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Documento_BuscarDocumentoRef2
    '''</summary>
    <TestMethod()> _
    Public Sub Documento_BuscarDocumentoRef2Test()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Val_FechaVcto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim val_CotizacionCanjeUnico As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim IdtipoDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.Documento_BuscarDocumentoRef2(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, Val_FechaVcto, val_CotizacionCanjeUnico, IdtipoDocumentoRef, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Documento_BuscarDocumentoRef_Externo
    '''</summary>
    <TestMethod()> _
    Public Sub Documento_BuscarDocumentoRef_ExternoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Val_FechaVcto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim val_CotizacionCanjeUnico As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim IdtipoDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.Documento_BuscarDocumentoRef_Externo(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, Val_FechaVcto, val_CotizacionCanjeUnico, IdtipoDocumentoRef)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Documento_BuscarxTraza
    '''</summary>
    <TestMethod()> _
    Public Sub Documento_BuscarxTrazaTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Val_FechaVcto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim PageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim PageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEstadoEnt As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEstadoCan As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.Documento_BuscarxTraza(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, Val_FechaVcto, PageIndex, PageSize, IdEstadoEnt, IdEstadoCan)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de EliminarDocumentoxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub EliminarDocumentoxIdDocumentoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.EliminarDocumentoxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de EliminarPuntoLlegada
    '''</summary>
    <TestMethod()> _
    Public Sub EliminarPuntoLlegadaTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.EliminarPuntoLlegada(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de FacturaSelectCab_Impresion
    '''</summary>
    <TestMethod()> _
    Public Sub FacturaSelectCab_ImpresionTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.FacturaSelectCab_Impresion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de FacturaSelectDet_Impresion
    '''</summary>
    <TestMethod()> _
    Public Sub FacturaSelectDet_ImpresionTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.FacturaSelectDet_Impresion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GenerarDocTomaInventario
    '''</summary>
    <TestMethod()> _
    Public Sub GenerarDocTomaInventarioTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSubLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.GenerarDocTomaInventario(IdDocumento, IdEmpresa, IdAlmacen, IdLinea, IdSubLinea, FechaFin, cn, tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GenerarNroDocumentoxIdSerie
    '''</summary>
    <TestMethod()> _
    Public Sub GenerarNroDocumentoxIdSerieTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.GenerarNroDocumentoxIdSerie(IdSerie)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GenerarPaseVentasCompras
    '''</summary>
    <TestMethod()> _
    Public Sub GenerarPaseVentasComprasTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim FechaInicioPase As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaFinPase As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Anio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.GenerarPaseVentasCompras(FechaInicioPase, FechaFinPase, Anio, IdUsuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertListCancelacion_DatosCancelacion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertListCancelacion_DatosCancelacionTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim list As List(Of DatosCancelacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertListCancelacion_DatosCancelacion(list, IdDocumento, cnx, trx)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertListCancelacion_PagoCaja
    '''</summary>
    <TestMethod()> _
    Public Sub InsertListCancelacion_PagoCajaTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim list As List(Of PagoCaja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertListCancelacion_PagoCaja(list, IdDocumento, cnx, trx)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertMovCajaxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub InsertMovCajaxIdDocumentoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim obj As MovCaja = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertMovCajaxIdDocumento(obj, IdDocumento, cnx, trx)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertObservacion
    '''</summary>
    <TestMethod()> _
    Public Sub InsertObservacionTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim obj As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cnx As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim trx As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertObservacion(obj, IdDocumento, cnx, trx)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDocumentoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim documento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaDocumento(cn, documento, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDocumentoT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDocumentoTTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim documento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim LDet As List(Of DetalleGuia) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim afectaMovAlmacen As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim factorMov As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMetValuacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim inventarioInicial As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaDocumentoT(documento, LDet, afectaMovAlmacen, factorMov, IdAlmacen, IdMetValuacion, inventarioInicial)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDocumentoVentaT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDocumentoVentaTTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim Documento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim LDetalle As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim MontoRegimen As MontoRegimen = Nothing ' TODO: Inicializar en un valor adecuado
        Dim LMovAlmacen As List(Of MovAlmacen) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim PLlegada As PuntoLlegada = Nothing ' TODO: Inicializar en un valor adecuado
        Dim MovCaja As MovCaja = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaPagoCaja As List(Of PagoCaja) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaMovCuenta As List(Of MovCuenta) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaDocumentoVentaT(Documento, LDetalle, MontoRegimen, LMovAlmacen, PLlegada, MovCaja, listaPagoCaja, listaMovCuenta)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaDocumento_Venta
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaDocumento_VentaTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim documento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim autoGenerarCodigo As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim comprometerPercepcion As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaDocumento_Venta(cn, documento, T, autoGenerarCodigo, comprometerPercepcion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaPuntoLlegadaT
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaPuntoLlegadaTTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim puntollegada As PuntoLlegada = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaPuntoLlegadaT(puntollegada)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de MovAlmacen_ExtornoxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub MovAlmacen_ExtornoxIdDocumentoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.MovAlmacen_ExtornoxIdDocumento(IdDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de QuitarCheckDocumentoxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub QuitarCheckDocumentoxIdDocumentoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.QuitarCheckDocumentoxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxIdDocumentoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectActivoxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDetallexIdPase
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDetallexIdPaseTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdPase As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.SelectDetallexIdPase(IdPase)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocCancelacionBxIdRQ
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocCancelacionBxIdRQTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.SelectDocCancelacionBxIdRQ(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocEOCxCodigoSerie
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocEOCxCodigoSerieTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim serie As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fechafin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.SelectDocEOCxCodigoSerie(serie, codigo, fechainicio, fechafin, IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocExternoxCodigoSerie
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocExternoxCodigoSerieTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim serie As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fechafin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.SelectDocExternoxCodigoSerie(serie, codigo, fechainicio, fechafin, IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocExtxIdRQinProgPago
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocExtxIdRQinProgPagoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.SelectDocExtxIdRQinProgPago(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocumentoOCxIdDocRef
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocumentoOCxIdDocRefTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectDocumentoOCxIdDocRef(IdDocumentoRef)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocumentotoInsertRQ
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocumentotoInsertRQTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectDocumentotoInsertRQ(IdDocumentoRef)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDocxDespachar
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDocxDespacharTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target.SelectDocxDespachar(IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectIdCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectIdCotizacionTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectIdCotizacion(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectIdDocumentoxIdSeriexCodigo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectIdDocumentoxIdSeriexCodigoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectIdDocumentoxIdSeriexCodigo(IdSerie, codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInvInicialCabxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInvInicialCabxIdDocumentoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectInvInicialCabxIdDocumento(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInvInicialIdDocxEmpresaxAlmacen
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInvInicialIdDocxEmpresaxAlmacenTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim idempresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idalmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectInvInicialIdDocxEmpresaxAlmacen(idempresa, idalmacen)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectMaxCodxIdSerie
    '''</summary>
    <TestMethod()> _
    Public Sub SelectMaxCodxIdSerieTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.SelectMaxCodxIdSerie(IdSerie)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectPasesxIdEmpresa
    '''</summary>
    <TestMethod()> _
    Public Sub SelectPasesxIdEmpresaTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Pase) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Pase)
        actual = target.SelectPasesxIdEmpresa(IdEmpresa)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectPermisosRQxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectPermisosRQxIdPersonaTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectPermisosRQxIdPersona(IdUsuario)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectReciboEgresoxIdRQ
    '''</summary>
    <TestMethod()> _
    Public Sub SelectReciboEgresoxIdRQTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.SelectReciboEgresoxIdRQ(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectReporteDocExterno
    '''</summary>
    <TestMethod()> _
    Public Sub SelectReporteDocExternoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim fechainicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fechafin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Idtienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.SelectReporteDocExterno(serie, codigo, fechainicio, fechafin, Idtienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectxIdDocumento(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumentoDocReferencia
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoDocReferenciaTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectxIdDocumentoDocReferencia(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumentoExterno
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoExternoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.SelectxIdDocumentoExterno(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumentoExternoLista
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoExternoListaTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.SelectxIdDocumentoExternoLista(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumentoExternoOne
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoExternoOneTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectxIdDocumentoExternoOne(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumentoExternotwo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoExternotwoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectxIdDocumentoExternotwo(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumentoProgramacionPago
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoProgramacionPagoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectxIdDocumentoProgramacionPago(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumentoReferenciaCheckes
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoReferenciaCheckesTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectxIdDocumentoReferenciaCheckes(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdSeriexCodigo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdSeriexCodigoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdSerie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.SelectxIdSeriexCodigo(IdSerie, codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdTipoDocumentoxIdPersonaxSeriexCodigo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTipoDocumentoxIdPersonaxSeriexCodigoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.SelectxIdTipoDocumentoxIdPersonaxSeriexCodigo(IdTipoDocumento, IdPersona, Serie, Codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdTipoDocumentoxIdPersonaxSeriexCodigoExt
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTipoDocumentoxIdPersonaxSeriexCodigoExtTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim pageIndex As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.SelectxIdTipoDocumentoxIdPersonaxSeriexCodigoExt(IdTipoDocumento, IdPersona, Serie, Codigo, pageIndex, pageSize)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateFechaEmision
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateFechaEmisionTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaEmisionNew As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateFechaEmision(IdDocumento, FechaEmisionNew, cn)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateVendedorBolFact
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateVendedorBolFactTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim objx As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateVendedorBolFact(objx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateVendedorCotizacion
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateVendedorCotizacionTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim objx As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.UpdateVendedorCotizacion(objx)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarDocExterno
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarDocExternoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim serie As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Documento
        actual = target.ValidarDocExterno(serie, codigo, IdTipoDocumento, IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de VerificandoHistorialPases
    '''</summary>
    <TestMethod()> _
    Public Sub VerificandoHistorialPasesTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim FechaInicioPase As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaFinPase As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Anio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.VerificandoHistorialPases(FechaInicioPase, FechaFinPase, Anio)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de VerificandoSinPasar
    '''</summary>
    <TestMethod()> _
    Public Sub VerificandoSinPasarTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim FechaInicioPase As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim FechaFinPase As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Anio As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.VerificandoSinPasar(FechaInicioPase, FechaFinPase, Anio)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de VerificandoTiendaOPVP
    '''</summary>
    <TestMethod()> _
    Public Sub VerificandoTiendaOPVPTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.VerificandoTiendaOPVP(IdTienda)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de _Documento_BuscarDocumentoRefDocExterno
    '''</summary>
    <TestMethod()> _
    Public Sub _Documento_BuscarDocumentoRefDocExternoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim Val_FechaVcto As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim val_CotizacionCanjeUnico As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim IdtipoDocumentoRef As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DocumentoView) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DocumentoView)
        actual = target._Documento_BuscarDocumentoRefDocExterno(IdEmpresa, IdTienda, IdPersona, IdTipoDocumento, Serie, Codigo, FechaInicio, FechaFin, Val_FechaVcto, val_CotizacionCanjeUnico, IdtipoDocumentoRef, IdTipoOperacion)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fnInsTblDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub fnInsTblDocumentoTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim documento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.fnInsTblDocumento(documento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de fnSelTblDocumentoDin
    '''</summary>
    <TestMethod()> _
    Public Sub fnSelTblDocumentoDinTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim psWhere As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim psOrder As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.fnSelTblDocumentoDin(psWhere, psOrder)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de selectxidval_req_aprob
    '''</summary>
    <TestMethod()> _
    Public Sub selectxidval_req_aprobTest()
        Dim target As DAODocumento = New DAODocumento() ' TODO: Inicializar en un valor adecuado
        Dim obj As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Anexo_Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Anexo_Documento
        actual = target.selectxidval_req_aprob(obj)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
