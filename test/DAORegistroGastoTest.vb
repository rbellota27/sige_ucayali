﻿Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAORegistroGastoTest y se pretende que
'''contenga todas las pruebas unitarias DAORegistroGastoTest.
'''</summary>
<TestClass()> _
Public Class DAORegistroGastoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAORegistroGasto
    '''</summary>
    <TestMethod()> _
    Public Sub DAORegistroGastoConstructorTest()
        Dim target As DAORegistroGasto = New DAORegistroGasto()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de InsertRegistroGasto
    '''</summary>
    <TestMethod()> _
    Public Sub InsertRegistroGastoTest()
        Dim target As DAORegistroGasto = New DAORegistroGasto() ' TODO: Inicializar en un valor adecuado
        Dim Obj As RegistroGasto.CabeceraRegistroGasto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Lista As List(Of RequerimientoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ListaCentroCosto As List(Of Centro_costo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObs As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.InsertRegistroGasto(Obj, Lista, ListaCentroCosto, objObs)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateRegistroGasto
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateRegistroGastoTest()
        Dim target As DAORegistroGasto = New DAORegistroGasto() ' TODO: Inicializar en un valor adecuado
        Dim Obj As RegistroGasto.CabeceraRegistroGasto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Lista As List(Of RequerimientoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ListaCentroCosto As List(Of Centro_costo) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objObs As Observacion = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim actual As String
        actual = target.UpdateRegistroGasto(Obj, Lista, ListaCentroCosto, objObs)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de selectListaRegistroGasto
    '''</summary>
    <TestMethod()> _
    Public Sub selectListaRegistroGastoTest()
        Dim target As DAORegistroGasto = New DAORegistroGasto() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of RequerimientoGasto) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of RequerimientoGasto)
        actual = target.selectListaRegistroGasto(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de selectRegistroGasto
    '''</summary>
    <TestMethod()> _
    Public Sub selectRegistroGastoTest()
        Dim target As DAORegistroGasto = New DAORegistroGasto() ' TODO: Inicializar en un valor adecuado
        Dim idserie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As RegistroGasto.CabeceraRegistroGasto = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As RegistroGasto.CabeceraRegistroGasto
        actual = target.selectRegistroGasto(idserie, codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
