﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOMedioPagoTest y se pretende que
'''contenga todas las pruebas unitarias DAOMedioPagoTest.
'''</summary>
<TestClass()> _
Public Class DAOMedioPagoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOMedioPago
    '''</summary>
    <TestMethod()> _
    Public Sub DAOMedioPagoConstructorTest()
        Dim target As DAOMedioPago = New DAOMedioPago()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizarMedioPago_ListaTipoDoc
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizarMedioPago_ListaTipoDocTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim obj As MedioPago = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ListaTipoDoc_MedioP As List(Of TipoDocumento_MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizarMedioPago_ListaTipoDoc(obj, ListaTipoDoc_MedioP)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaListaTipoDoc_MedioP
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaListaTipoDoc_MedioPTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdMedioPAgo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim lista_TipoDoc_MedioP As List(Of TipoDocumento_MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim Tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaListaTipoDoc_MedioP(cn, IdMedioPAgo, lista_TipoDoc_MedioP, Tr)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertarMedioPago_ListaTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub InsertarMedioPago_ListaTipoDocumentoTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim obj As MedioPago = Nothing ' TODO: Inicializar en un valor adecuado
        Dim ListaTipoDoc_MedioP As List(Of TipoDocumento_MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertarMedioPago_ListaTipoDocumento(obj, ListaTipoDoc_MedioP)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ListarTipoDoc_MedioP
    '''</summary>
    <TestMethod()> _
    Public Sub ListarTipoDoc_MedioPTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim idMedioPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of TipoDocumento_MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of TipoDocumento_MedioPago)
        actual = target.ListarTipoDoc_MedioP(idMedioPago)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de MedioPago_MovBanco
    '''</summary>
    <TestMethod()> _
    Public Sub MedioPago_MovBancoTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoConceptoBanco As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago)
        actual = target.MedioPago_MovBanco(IdTipoConceptoBanco)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectActivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectActivoxNombreTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago)
        actual = target.SelectActivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAll
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago)
        actual = target.SelectAll
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllActivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllActivoTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago)
        actual = target.SelectAllActivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllInactivo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllInactivoTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago)
        actual = target.SelectAllInactivo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllxNombreTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago)
        actual = target.SelectAllxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago)
        actual = target.SelectCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboMpInterfaz
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboMpInterfazTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago)
        actual = target.SelectCboMpInterfaz
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectIdMedioPagoPrincipal
    '''</summary>
    <TestMethod()> _
    Public Sub SelectIdMedioPagoPrincipalTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.SelectIdMedioPagoPrincipal
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectInactivoxNombre
    '''</summary>
    <TestMethod()> _
    Public Sub SelectInactivoxNombreTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago)
        actual = target.SelectInactivoxNombre(nombre)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxCodSunat
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxCodSunatTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim codSunat As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago)
        actual = target.SelectxCodSunat(codSunat)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxId
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim id As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago)
        actual = target.SelectxId(id)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdTipoDocumentoxIdCondicionPago
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdTipoDocumentoxIdCondicionPagoTest()
        Dim target As DAOMedioPago = New DAOMedioPago() ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCondicionPago As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of MedioPago) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of MedioPago)
        actual = target.SelectxIdTipoDocumentoxIdCondicionPago(IdTipoDocumento, IdCondicionPago)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
