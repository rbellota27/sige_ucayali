﻿Imports System.Collections.Generic

Imports System.Data

Imports System.Data.SqlClient

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOCampania_DetalleTGTest y se pretende que
'''contenga todas las pruebas unitarias DAOCampania_DetalleTGTest.
'''</summary>
<TestClass()> _
Public Class DAOCampania_DetalleTGTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOCampania_DetalleTG
    '''</summary>
    <TestMethod()> _
    Public Sub DAOCampania_DetalleTGConstructorTest()
        Dim target As DAOCampania_DetalleTG = New DAOCampania_DetalleTG()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Campania_DetalleTG_Transaction
    '''</summary>
    <TestMethod()> _
    Public Sub Campania_DetalleTG_TransactionTest()
        Dim target As DAOCampania_DetalleTG = New DAOCampania_DetalleTG() ' TODO: Inicializar en un valor adecuado
        Dim objCampania_DetalleTG As Campania_DetalleTG = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlCN As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlTR As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.Campania_DetalleTG_Transaction(objCampania_DetalleTG, sqlCN, sqlTR)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Campania_DetalleTG_TransactionALL
    '''</summary>
    <TestMethod()> _
    Public Sub Campania_DetalleTG_TransactionALLTest()
        Dim target As DAOCampania_DetalleTG = New DAOCampania_DetalleTG() ' TODO: Inicializar en un valor adecuado
        Dim objCampania_DetalleTG As Campania_DetalleTG = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlCN As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqlTR As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.Campania_DetalleTG_TransactionALL(objCampania_DetalleTG, sqlCN, sqlTR)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Campania_DetalleTGxDelete
    '''</summary>
    <TestMethod()> _
    Public Sub Campania_DetalleTGxDeleteTest()
        Dim target As DAOCampania_DetalleTG = New DAOCampania_DetalleTG() ' TODO: Inicializar en un valor adecuado
        Dim IdCampania As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCampaniaDetalle As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim sqlcn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim sqltr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.Campania_DetalleTGxDelete(IdCampania, IdProducto, IdCampaniaDetalle, sqlcn, sqltr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de Campania_DetalleTGxIdCampania
    '''</summary>
    <TestMethod()> _
    Public Sub Campania_DetalleTGxIdCampaniaTest()
        Dim target As DAOCampania_DetalleTG = New DAOCampania_DetalleTG() ' TODO: Inicializar en un valor adecuado
        Dim IdProducto As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdCampania As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoExistencia As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdLinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdSublinea As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim prod_codigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim prod_nombre As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim pageNumber As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim pageSize As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim tabla As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Campania_DetalleTG) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Campania_DetalleTG)
        actual = target.Campania_DetalleTGxIdCampania(IdProducto, IdCampania, IdTipoExistencia, IdLinea, IdSublinea, prod_codigo, prod_nombre, pageNumber, pageSize, tabla)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Campania_DetalleTGxIdCampaniaDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub Campania_DetalleTGxIdCampaniaDetalleTest()
        Dim target As DAOCampania_DetalleTG = New DAOCampania_DetalleTG() ' TODO: Inicializar en un valor adecuado
        Dim IdCampaniaDetalle As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Campania_DetalleTG) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Campania_DetalleTG)
        actual = target.Campania_DetalleTGxIdCampaniaDetalle(IdCampaniaDetalle)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
