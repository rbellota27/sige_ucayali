﻿Imports System

Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAODocOrdenDespachoTest y se pretende que
'''contenga todas las pruebas unitarias DAODocOrdenDespachoTest.
'''</summary>
<TestClass()> _
Public Class DAODocOrdenDespachoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAODocOrdenDespacho
    '''</summary>
    <TestMethod()> _
    Public Sub DAODocOrdenDespachoConstructorTest()
        Dim target As DAODocOrdenDespacho = New DAODocOrdenDespacho()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaOrdenDespacho
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaOrdenDespachoTest()
        Dim target As DAODocOrdenDespacho = New DAODocOrdenDespacho() ' TODO: Inicializar en un valor adecuado
        Dim listaDetalle As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaOrdenDespacho(listaDetalle, objDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de AnularxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub AnularxIdDocumentoTest()
        Dim target As DAODocOrdenDespacho = New DAODocOrdenDespacho() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.AnularxIdDocumento(iddocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DeleteDetalle_Movimiento
    '''</summary>
    <TestMethod()> _
    Public Sub DeleteDetalle_MovimientoTest()
        Dim target As DAODocOrdenDespacho = New DAODocOrdenDespacho() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DeleteDetalle_Movimiento(iddocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoOrdenDespacho_DetalleInsert
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoOrdenDespacho_DetalleInsertTest()
        Dim target As DAODocOrdenDespacho = New DAODocOrdenDespacho() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim listaDetalleDocumento As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim Factor As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim MoverStockFisico As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim ValidarDespacho As Boolean = False ' TODO: Inicializar en un valor adecuado
        target.DocumentoOrdenDespacho_DetalleInsert(cn, listaDetalleDocumento, tr, IdDocumento, Factor, MoverStockFisico, ValidarDespacho)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoOrdenDespacho_GenerarDespachoAutomatico
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoOrdenDespacho_GenerarDespachoAutomaticoTest()
        Dim target As DAODocOrdenDespacho = New DAODocOrdenDespacho() ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento_OD As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion_OD As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoOrdenDespacho_GenerarDespachoAutomatico(IdAlmacen, FechaFin, IdUsuario, IdTipoDocumento_OD, IdTipoOperacion_OD, IdDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de DocumentoOrdenDespacho_GenerarDespachoAutomaticoxParams
    '''</summary>
    <TestMethod()> _
    Public Sub DocumentoOrdenDespacho_GenerarDespachoAutomaticoxParamsTest()
        Dim target As DAODocOrdenDespacho = New DAODocOrdenDespacho() ' TODO: Inicializar en un valor adecuado
        Dim IdAlmacen As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim FechaInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim FechaFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento_OD As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoOperacion_OD As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.DocumentoOrdenDespacho_GenerarDespachoAutomaticoxParams(IdAlmacen, FechaInicio, FechaFin, IdUsuario, IdTipoDocumento_OD, IdTipoOperacion_OD, IdDocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaOrdenDespacho
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaOrdenDespachoTest()
        Dim target As DAODocOrdenDespacho = New DAODocOrdenDespacho() ' TODO: Inicializar en un valor adecuado
        Dim listaDetalle As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim objDocumento As Documento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumentoOld As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdMetodoVal As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.InsertaOrdenDespacho(listaDetalle, objDocumento, IdDocumentoOld, IdMetodoVal)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectDetalleOrdenDespachoxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectDetalleOrdenDespachoxIdDocumentoTest()
        Dim target As DAODocOrdenDespacho = New DAODocOrdenDespacho() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of DetalleDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of DetalleDocumento)
        actual = target.SelectDetalleOrdenDespachoxIdDocumento(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectOrdenDespachoxIdSeriexCodigo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectOrdenDespachoxIdSeriexCodigoTest()
        Dim target As DAODocOrdenDespacho = New DAODocOrdenDespacho() ' TODO: Inicializar en un valor adecuado
        Dim idserie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocOrdenDespacho = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocOrdenDespacho
        actual = target.SelectOrdenDespachoxIdSeriexCodigo(idserie, codigo)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoTest()
        Dim target As DAODocOrdenDespacho = New DAODocOrdenDespacho() ' TODO: Inicializar en un valor adecuado
        Dim iddocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DocOrdenDespacho = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DocOrdenDespacho
        actual = target.SelectxIdDocumento(iddocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
