﻿Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAORelacionDocumentoTest y se pretende que
'''contenga todas las pruebas unitarias DAORelacionDocumentoTest.
'''</summary>
<TestClass()> _
Public Class DAORelacionDocumentoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAORelacionDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub DAORelacionDocumentoConstructorTest()
        Dim target As DAORelacionDocumento = New DAORelacionDocumento()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de Documento_Referencia
    '''</summary>
    <TestMethod()> _
    Public Sub Documento_ReferenciaTest()
        Dim target As DAORelacionDocumento = New DAORelacionDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of RelacionDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of RelacionDocumento)
        actual = target.Documento_Referencia(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GrabaRelacionDocumentoT
    '''</summary>
    <TestMethod()> _
    Public Sub GrabaRelacionDocumentoTTest()
        Dim target As DAORelacionDocumento = New DAORelacionDocumento() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim vIdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim objRelacDoc As RelacionDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.GrabaRelacionDocumentoT(cn, vIdDocumento, objRelacDoc, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertRelacionChequeProgramacionPago
    '''</summary>
    <TestMethod()> _
    Public Sub InsertRelacionChequeProgramacionPagoTest()
        Dim target As DAORelacionDocumento = New DAORelacionDocumento() ' TODO: Inicializar en un valor adecuado
        Dim relaciondocumento As RelacionDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.InsertRelacionChequeProgramacionPago(relaciondocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaRelacionDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaRelacionDocumentoTest()
        Dim target As DAORelacionDocumento = New DAORelacionDocumento() ' TODO: Inicializar en un valor adecuado
        Dim relaciondocumento As RelacionDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        'target.InsertaRelacionDocumento(relaciondocumento, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de RelacionDocumentoSelectxIdDocumento2xIdTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub RelacionDocumentoSelectxIdDocumento2xIdTipoDocumentoTest()
        Dim target As DAORelacionDocumento = New DAORelacionDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento2 As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of RelacionDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of RelacionDocumento)
        actual = target.RelacionDocumentoSelectxIdDocumento2xIdTipoDocumento(IdDocumento2, IdTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de RelacionDocumento_SelectDocumentoRefxIdDocumento2
    '''</summary>
    <TestMethod()> _
    Public Sub RelacionDocumento_SelectDocumentoRefxIdDocumento2Test()
        Dim target As DAORelacionDocumento = New DAORelacionDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento2 As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.RelacionDocumento_SelectDocumentoRefxIdDocumento2(IdDocumento2)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectAllIdDoc2xIdDoc1
    '''</summary>
    <TestMethod()> _
    Public Sub SelectAllIdDoc2xIdDoc1Test()
        Dim target As DAORelacionDocumento = New DAORelacionDocumento() ' TODO: Inicializar en un valor adecuado
        Dim Cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento1 As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of RelacionDocumento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of RelacionDocumento)
        actual = target.SelectAllIdDoc2xIdDoc1(Cn, T, IdDocumento1)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectxIdDocumentoxIdTipoDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub SelectxIdDocumentoxIdTipoDocumentoTest()
        Dim target As DAORelacionDocumento = New DAORelacionDocumento() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTipoDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.SelectxIdDocumentoxIdTipoDocumento(IdDocumento, IdTipoDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de UpdateRelacionDocumento
    '''</summary>
    <TestMethod()> _
    Public Sub UpdateRelacionDocumentoTest()
        Dim target As DAORelacionDocumento = New DAORelacionDocumento() ' TODO: Inicializar en un valor adecuado
        Dim obj As RelacionDocumento = Nothing ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        target.UpdateRelacionDocumento(obj, cn, tr)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub
End Class
