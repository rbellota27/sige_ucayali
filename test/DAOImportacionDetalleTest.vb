﻿Imports System

Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOImportacionDetalleTest y se pretende que
'''contenga todas las pruebas unitarias DAOImportacionDetalleTest.
'''</summary>
<TestClass()> _
Public Class DAOImportacionDetalleTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOImportacionDetalle
    '''</summary>
    <TestMethod()> _
    Public Sub DAOImportacionDetalleConstructorTest()
        Dim target As DAOImportacionDetalle = New DAOImportacionDetalle()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DetalleImportacionSelectId
    '''</summary>
    <TestMethod()> _
    Public Sub DetalleImportacionSelectIdTest()
        Dim target As DAOImportacionDetalle = New DAOImportacionDetalle() ' TODO: Inicializar en un valor adecuado
        Dim IdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ImportacionDetalle) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ImportacionDetalle)
        actual = target.DetalleImportacionSelectId(IdDocumento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de GrabarDetalleImportacionT
    '''</summary>
    <TestMethod()> _
    Public Sub GrabarDetalleImportacionTTest()
        Dim target As DAOImportacionDetalle = New DAOImportacionDetalle() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim vIdDocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim UpdPrecioOC As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim ldetalleimportacion As List(Of ImportacionDetalle) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim T As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.GrabarDetalleImportacionT(cn, vIdDocumento, UpdPrecioOC, ldetalleimportacion, T)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de OrdenCompraSelectIdOC
    '''</summary>
    <TestMethod()> _
    Public Sub OrdenCompraSelectIdOCTest()
        Dim target As DAOImportacionDetalle = New DAOImportacionDetalle() ' TODO: Inicializar en un valor adecuado
        Dim IdDoC As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim TipoCambio As [Decimal] = New [Decimal]() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of ImportacionDetalle) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of ImportacionDetalle)
        actual = target.OrdenCompraSelectIdOC(IdDoC, TipoCambio)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
