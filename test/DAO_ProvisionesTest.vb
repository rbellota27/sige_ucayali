﻿Imports System.Collections.Generic

Imports Entidades

Imports System

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAO_ProvisionesTest y se pretende que
'''contenga todas las pruebas unitarias DAO_ProvisionesTest.
'''</summary>
<TestClass()> _
Public Class DAO_ProvisionesTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAO_Provisiones
    '''</summary>
    <TestMethod()> _
    Public Sub DAO_ProvisionesConstructorTest()
        Dim target As DAO_Provisiones = New DAO_Provisiones()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de listaProvisiones
    '''</summary>
    <TestMethod()> _
    Public Sub listaProvisionesTest()
        Dim target As DAO_Provisiones = New DAO_Provisiones() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim docSerie As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim docCodigo As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim fecInicio As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim fecFin As DateTime = New DateTime() ' TODO: Inicializar en un valor adecuado
        Dim flagFiltro As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim nroruc As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.listaProvisiones(cn, docSerie, docCodigo, fecInicio, fecFin, flagFiltro, nroruc)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de listarRelacionRequetimento_Provision
    '''</summary>
    <TestMethod()> _
    Public Sub listarRelacionRequetimento_ProvisionTest()
        Dim target As DAO_Provisiones = New DAO_Provisiones() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim idRequerimiento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Documento) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Documento)
        actual = target.listarRelacionRequetimento_Provision(cn, idRequerimiento)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
