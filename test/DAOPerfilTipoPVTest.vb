﻿Imports System.Collections.Generic

Imports Entidades

Imports System.Data.SqlClient

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOPerfilTipoPVTest y se pretende que
'''contenga todas las pruebas unitarias DAOPerfilTipoPVTest.
'''</summary>
<TestClass()> _
Public Class DAOPerfilTipoPVTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOPerfilTipoPV
    '''</summary>
    <TestMethod()> _
    Public Sub DAOPerfilTipoPVConstructorTest()
        Dim target As DAOPerfilTipoPV = New DAOPerfilTipoPV()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de DeletexPerfilTipoPVBorrar
    '''</summary>
    <TestMethod()> _
    Public Sub DeletexPerfilTipoPVBorrarTest()
        Dim target As DAOPerfilTipoPV = New DAOPerfilTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.DeletexPerfilTipoPVBorrar(cn, tr, IdPerfil)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaListaPerfilTipoPV
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaListaPerfilTipoPVTest()
        Dim target As DAOPerfilTipoPV = New DAOPerfilTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim tr As SqlTransaction = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of PerfilTipoPV) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        target.InsertaListaPerfilTipoPV(cn, tr, lista, IdPerfil)
        Assert.Inconclusive("Un método que no devuelve ningún valor no se puede comprobar.")
    End Sub

    '''<summary>
    '''Una prueba de _PerfilTipoPVEditarxIdPerfil
    '''</summary>
    <TestMethod()> _
    Public Sub _PerfilTipoPVEditarxIdPerfilTest()
        Dim target As DAOPerfilTipoPV = New DAOPerfilTipoPV() ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of PerfilTipoPV) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of PerfilTipoPV)
        actual = target._PerfilTipoPVEditarxIdPerfil(IdPerfil)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
