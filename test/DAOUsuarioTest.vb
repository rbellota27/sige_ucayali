﻿Imports System.Data

Imports System.Data.SqlClient

Imports System.Collections.Generic

Imports Entidades

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports DAO



'''<summary>
'''Se trata de una clase de prueba para DAOUsuarioTest y se pretende que
'''contenga todas las pruebas unitarias DAOUsuarioTest.
'''</summary>
<TestClass()> _
Public Class DAOUsuarioTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Obtiene o establece el contexto de la prueba que proporciona
    '''la información y funcionalidad para la ejecución de pruebas actual.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Atributos de prueba adicionales"
    '
    'Puede utilizar los siguientes atributos adicionales mientras escribe sus pruebas:
    '
    'Use ClassInitialize para ejecutar código antes de ejecutar la primera prueba en la clase 
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup para ejecutar código después de haber ejecutado todas las pruebas en una clase
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize para ejecutar código antes de ejecutar cada prueba
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup para ejecutar código después de que se hayan ejecutado todas las pruebas
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''Una prueba de Constructor DAOUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub DAOUsuarioConstructorTest()
        Dim target As DAOUsuario = New DAOUsuario()
        Assert.Inconclusive("TODO: Implementar código para comprobar el destino")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaUsuarioTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim usuario As Usuario = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of PerfilUsuario) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista_area As List(Of Usuario_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim UpdatePassword As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaUsuario(usuario, lista, lista_area, UpdatePassword)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ActualizaUsuarioAtencionPago
    '''</summary>
    <TestMethod()> _
    Public Sub ActualizaUsuarioAtencionPagoTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim ObjUsu As Usuario = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ActualizaUsuarioAtencionPago(ObjUsu)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ChangePassword
    '''</summary>
    <TestMethod()> _
    Public Sub ChangePasswordTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim ClaveOld As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim ClaveNew As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim ClaveNewConfirm As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.ChangePassword(IdUsuario, ClaveOld, ClaveNew, ClaveNewConfirm, cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de InsertaUsuario
    '''</summary>
    <TestMethod()> _
    Public Sub InsertaUsuarioTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim usuario As Usuario = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista As List(Of PerfilUsuario) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim lista_area As List(Of Usuario_Area) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Boolean = False ' TODO: Inicializar en un valor adecuado
        Dim actual As Boolean
        actual = target.InsertaUsuario(usuario, lista, lista_area)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCbo
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Natural) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Natural)
        actual = target.SelectCbo
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectCboAtencion
    '''</summary>
    <TestMethod()> _
    Public Sub SelectCboAtencionTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of Usuario) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of Usuario)
        actual = target.SelectCboAtencion
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de SelectUsuarioTiendaAreaxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub SelectUsuarioTiendaAreaxIdPersonaTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of UsuarioTiendaArea) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of UsuarioTiendaArea)
        actual = target.SelectUsuarioTiendaAreaxIdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de TodosUsuarios
    '''</summary>
    <TestMethod()> _
    Public Sub TodosUsuariosTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Usuario = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Usuario
        actual = target.TodosUsuarios(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_DT
    '''</summary>
    <TestMethod()> _
    Public Sub Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_DTTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_DT(IdEmpresa, IdTienda, IdPerfil)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_Distinct
    '''</summary>
    <TestMethod()> _
    Public Sub Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_DistinctTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdEmpresa As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdTienda As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPerfil As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As DataTable = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As DataTable
        actual = target.Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_Distinct(IdEmpresa, IdTienda, IdPerfil)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de Usuario_SelectxIdPersona
    '''</summary>
    <TestMethod()> _
    Public Sub Usuario_SelectxIdPersonaTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdPersona As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim expected As Comisionista = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As Comisionista
        actual = target.Usuario_SelectxIdPersona(IdPersona)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarEdicionDocxIdUsuarioxIdPermiso
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarEdicionDocxIdUsuarioxIdPermisoTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPermiso As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim doc_serie As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim doc_codigo As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim idTIpodocumento As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarEdicionDocxIdUsuarioxIdPermiso(IdUsuario, IdPermiso, doc_serie, doc_codigo, idTIpodocumento, cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarIdentidad
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarIdentidadTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim Login As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Clave As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarIdentidad(Login, Clave)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarIdentidadPerfil
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarIdentidadPerfilTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim Login As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim Clave As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarIdentidadPerfil(Login, Clave)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarPermisoxIdUsuarioxIdPermiso
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarPermisoxIdUsuarioxIdPermisoTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim IdPermiso As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim actual As Integer
        actual = target.ValidarPermisoxIdUsuarioxIdPermiso(IdUsuario, IdPermiso, cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub

    '''<summary>
    '''Una prueba de ValidarPermisoxIdUsuarioxIdPermisoxTipoOperacion
    '''</summary>
    <TestMethod()> _
    Public Sub ValidarPermisoxIdUsuarioxIdPermisoxTipoOperacionTest()
        Dim target As DAOUsuario = New DAOUsuario() ' TODO: Inicializar en un valor adecuado
        Dim IdUsuario As Integer = 0 ' TODO: Inicializar en un valor adecuado
        Dim CadenaIdPermiso As String = String.Empty ' TODO: Inicializar en un valor adecuado
        Dim cn As SqlConnection = Nothing ' TODO: Inicializar en un valor adecuado
        Dim expected As List(Of be_validacion) = Nothing ' TODO: Inicializar en un valor adecuado
        Dim actual As List(Of be_validacion)
        actual = target.ValidarPermisoxIdUsuarioxIdPermisoxTipoOperacion(IdUsuario, CadenaIdPermiso, cn)
        Assert.AreEqual(expected, actual)
        Assert.Inconclusive("Compruebe la exactitud de este método de prueba.")
    End Sub
End Class
