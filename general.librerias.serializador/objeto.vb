﻿Imports System.Text
Imports System.Reflection
Imports System.IO

Public Class objeto

    Public Shared Sub Grabar(Of T)(ByVal archivo As String, ByVal obj As T)
        Dim propiedades As PropertyInfo() = obj.[GetType]().GetProperties()
        Using sw As New StreamWriter(archivo, True)
            Dim valor As Object
            For Each propiedad As PropertyInfo In propiedades
                valor = propiedad.GetValue(obj, Nothing)
                sw.WriteLine("{0} = {1}", propiedad.Name, (If(valor IsNot Nothing, valor.ToString(), "")))
            Next
        End Using
    End Sub

    Public Shared Function SerializarLista(Of T)(ByVal lista As List(Of T), Optional ByVal separadorCampo As Char = "|"c, Optional ByVal separadorRegistro As Char = ";"c, Optional ByVal incluirCabecera As Boolean = False, Optional ByVal archivo As String = "", Optional ByVal incluirCifrado As Boolean = False) As String
        Dim sb As New StringBuilder()
        If lista IsNot Nothing AndAlso lista.Count > 0 Then
            Dim propiedades As PropertyInfo() = lista(0).[GetType]().GetProperties()
            If archivo.Equals("") Then
                If incluirCabecera Then
                    For Each propiedad As PropertyInfo In propiedades
                        sb.Append(propiedad.Name)
                        sb.Append(separadorCampo)
                    Next
                    sb = sb.Remove(sb.Length - 1, 1)
                    sb.Append(separadorRegistro)
                End If
                Dim tipo As String
                Dim valor As Object
                For i As Integer = 0 To lista.Count - 1
                    For Each propiedad As PropertyInfo In propiedades
                        tipo = propiedad.PropertyType.ToString().ToLower()
                        valor = propiedad.GetValue(lista(i), Nothing)
                        If valor IsNot Nothing Then
                            If tipo.Contains("byte[]") Then
                                sb.Append(Convert.ToBase64String(DirectCast(valor, Byte())))
                            Else
                                sb.Append(valor.ToString())
                            End If
                        Else
                            sb.Append("")
                        End If
                        sb.Append(separadorCampo)
                    Next
                    sb = sb.Remove(sb.Length - 1, 1)
                    sb.Append(separadorRegistro)
                Next
                sb = sb.Remove(sb.Length - 1, 1)
            Else
                If File.Exists(archivo) Then
                    Dim lineas As String() = File.ReadAllLines(archivo)
                    Dim listaColumnas As New List(Of String)()
                    For Each propiedad As PropertyInfo In propiedades
                        listaColumnas.Add(propiedad.Name)
                    Next
                    Dim listaCampos As New List(Of String)()
                    Dim listaTitulos As New List(Of String)()
                    Dim listaFormatos As New List(Of String)()
                    Dim campos As String()
                    For Each linea As String In lineas
                        campos = linea.Split(","c)
                        If campos.Length > 0 Then
                            listaCampos.Add(campos(0))
                        Else
                            listaCampos.Add("")
                        End If
                        If campos.Length > 1 Then
                            listaTitulos.Add(campos(1))
                        Else
                            listaTitulos.Add("")
                        End If
                        If campos.Length > 2 Then
                            listaFormatos.Add(campos(2))
                        Else
                            listaFormatos.Add("")
                        End If
                    Next
                    If incluirCabecera Then
                        Dim pos As Integer
                        Dim n As Integer = listaCampos.Count
                        Dim campo As String
                        For i As Integer = 0 To n - 1
                            campo = listaCampos(i)
                            pos = listaColumnas.FindIndex(Function(x) x.Equals(campo))
                            If pos > -1 Then
                                If listaTitulos(i).Equals("") Then
                                    sb.Append(campo)
                                Else
                                    sb.Append(listaTitulos(i))
                                End If
                                sb.Append(separadorCampo)
                            End If
                        Next
                        sb = sb.Remove(sb.Length - 1, 1)
                        sb.Append(separadorRegistro)
                        Dim m As Integer = lista.Count
                        Dim valor As Object
                        Dim formato As String()
                        For j As Integer = 0 To m - 1
                            For i As Integer = 0 To n - 1
                                campo = listaCampos(i)
                                pos = listaColumnas.FindIndex(Function(x) x.Equals(campo))
                                If pos > -1 Then
                                    formato = listaFormatos(i).Split("|"c)
                                    valor = lista(j).[GetType]().GetProperty(campo).GetValue(lista(j), Nothing)
                                    If formato(0) = "Decimal" AndAlso lista(j).[GetType]().GetProperty(campo).ToString().ToLower().Contains(formato(0)) Then
                                        valor = Math.Round(CDec(valor), Integer.Parse(formato(1)))
                                    End If
                                    If formato(0) = "Date" Then
                                        valor = [String].Format("{0:d}", valor)
                                    End If
                                    If valor IsNot Nothing Then
                                        sb.Append(valor.ToString())
                                    Else
                                        sb.Append("")
                                    End If
                                    sb.Append(separadorCampo)
                                End If
                            Next
                            sb = sb.Remove(sb.Length - 1, 1)
                            sb.Append(separadorRegistro)
                        Next
                    End If
                    sb = sb.Remove(sb.Length - 1, 1)
                End If
            End If
            If incluirCifrado Then
                Dim k As Integer
                For i As Integer = 0 To sb.Length - 1
                    k = CInt(Val(sb(i)))
                    If k < 255 Then
                        k += 1
                    Else
                        k = 0
                    End If
                    sb(i) = CChar(CChar(CStr(k)))
                Next
            End If
        End If
        Return sb.ToString()
    End Function

    Public Shared Function SerializarObjeto(Of T)(ByVal obj As T, ByVal separadorCampo As Char, Optional ByVal archivo As String = "") As String
        Dim sb As New StringBuilder()
        Dim propiedades As PropertyInfo() = obj.[GetType]().GetProperties()
        Dim tipo As String
        If archivo = "" Then
            For i As Integer = 0 To propiedades.Length - 1
                tipo = propiedades(i).PropertyType.ToString()
                If propiedades(i).GetValue(obj, Nothing) IsNot Nothing Then
                    If tipo.Contains("Byte[]") Then
                        Dim buffer As Byte() = DirectCast(propiedades(i).GetValue(obj, Nothing), Byte())
                        sb.Append(Convert.ToBase64String(buffer))
                    Else
                        sb.Append(propiedades(i).GetValue(obj, Nothing).ToString())
                    End If
                Else
                    sb.Append("")
                End If
                If i < propiedades.Length - 1 Then
                    sb.Append(separadorCampo)
                End If
            Next
        Else
            If File.Exists(archivo) Then
                Dim campos As String() = File.ReadAllLines(archivo)
                Dim valor As Object
                Dim props As New List(Of String)()
                For i As Integer = 0 To propiedades.Length - 1
                    props.Add(propiedades(i).Name)
                Next
                For i As Integer = 0 To campos.Length - 1
                    If props.IndexOf(campos(i)) > -1 Then
                        tipo = obj.[GetType]().GetProperty(campos(i)).PropertyType.ToString()
                        valor = obj.[GetType]().GetProperty(campos(i)).GetValue(obj, Nothing)
                        If valor IsNot Nothing Then
                            If tipo.Contains("Byte[]") Then
                                Dim buffer As Byte() = DirectCast(valor, Byte())
                                sb.Append(Convert.ToBase64String(buffer))
                            Else
                                sb.Append(valor.ToString())
                            End If
                        Else
                            sb.Append("")
                        End If
                        If i < propiedades.Length - 1 Then
                            sb.Append(separadorCampo)
                        End If
                    End If
                Next
            End If
        End If
        Return sb.ToString()
    End Function

End Class