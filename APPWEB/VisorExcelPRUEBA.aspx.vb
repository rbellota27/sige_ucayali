﻿Imports ADODB
Imports Microsoft.Office.Interop
Partial Public Class VisorExcelPRUEBA
    Inherits System.Web.UI.Page
    Dim objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        reporteVentasxTiendaExportExcel()
    End Sub
    Private Sub reporteVentasxTiendaExportExcel()
        Try
            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente
            'Dim Rutaxlt As String = Server.MapPath("\PlantillasXLT\VtasProductoTiendaRpt")
            Dim Rutaxlt As String = Server.MapPath("\PlantillasXLT\VtasProductoTiendaRpt")
            ds = objReporte.VentasXTiendaExcel(CInt(1), CInt(1), CInt(0), CInt(0), CStr("01/01/2010"), CStr("05/04/2010"))

            Dim oRs As Recordset
            oRs = New Recordset
            oRs = ConvertToRecordset(ds.Tables(0))

            Dim oExcel As Excel.ApplicationClass
            Dim oBook As Excel.WorkbookClass
            Dim oBooks As Excel.Workbooks

            oExcel = CType(CreateObject("excel.application"), Excel.ApplicationClass)
            oBooks = oExcel.Workbooks
            oBook = CType(oBooks.Open(CurDir() + Rutaxlt), Excel.WorkbookClass)
            oExcel.Visible = True
            oExcel.DisplayAlerts = False
            oExcel.Run("reporte", oRs, "PRUEBA EXCEL")

            oExcel.Quit()
            oExcel = Nothing
        Catch ex As Exception
            oExcel.Quit()
            oExcel = Nothing
            Response.Write(ex.Message)
        End Try

    End Sub

#Region " Convertir Datatable a Recordset "
    Function ConvertToRecordset(ByVal inTable As DataTable) As ADODB.Recordset
        Try
            Dim result As New ADODB.Recordset()
            result.CursorLocation = ADODB.CursorLocationEnum.adUseClient
            Dim resultFields As ADODB.Fields = result.Fields
            Dim inColumns As System.Data.DataColumnCollection = inTable.Columns

            For Each inColumn As DataColumn In inColumns
                resultFields.Append(inColumn.ColumnName, TranslateType(inColumn.DataType), inColumn.MaxLength, CType(IIf(inColumn.AllowDBNull, ADODB.FieldAttributeEnum.adFldIsNullable, ADODB.FieldAttributeEnum.adFldUnspecified), FieldAttributeEnum), Nothing)
            Next

            result.Open(System.Reflection.Missing.Value, System.Reflection.Missing.Value, ADODB.CursorTypeEnum.adOpenStatic, _
                     ADODB.LockTypeEnum.adLockOptimistic, 0)

            For Each dr As DataRow In inTable.Rows
                result.AddNew(System.Reflection.Missing.Value, System.Reflection.Missing.Value)
                For columnIndex As Integer = 0 To inColumns.Count - 1
                    resultFields(columnIndex).Value = dr(columnIndex)
                Next
            Next
            Return result
        Catch xErr As Exception
            Dim result As ADODB.Recordset() = Nothing
            'MsgBox(xErr.Message, MsgBoxStyle.Critical)
            objScript.mostrarMsjAlerta(Me, xErr.Message)
            ' Return result
        End Try
    End Function

    Function TranslateType(ByVal columnType As Type) As ADODB.DataTypeEnum
        Select Case columnType.UnderlyingSystemType.ToString()
            Case "System.Boolean"
                Return ADODB.DataTypeEnum.adBoolean
            Case "System.Byte"
                Return ADODB.DataTypeEnum.adUnsignedTinyInt
            Case "System.Char"
                Return ADODB.DataTypeEnum.adChar
            Case "System.DateTime"
                Return ADODB.DataTypeEnum.adDate
            Case "System.TimeSpan"
                Return ADODB.DataTypeEnum.adDate
            Case "System.Decimal"
                Return ADODB.DataTypeEnum.adCurrency
            Case "System.Double"
                Return ADODB.DataTypeEnum.adDouble
            Case "System.Int16"
                Return ADODB.DataTypeEnum.adSmallInt
            Case "System.Int32"
                Return ADODB.DataTypeEnum.adInteger
            Case "System.Int64"
                Return ADODB.DataTypeEnum.adBigInt
            Case "System.SByte"
                Return ADODB.DataTypeEnum.adTinyInt
            Case "System.Single"
                Return ADODB.DataTypeEnum.adSingle
            Case "System.UInt16"
                Return ADODB.DataTypeEnum.adUnsignedSmallInt
            Case "System.UInt32"
                Return ADODB.DataTypeEnum.adUnsignedInt
            Case "System.UInt64"
                Return ADODB.DataTypeEnum.adUnsignedBigInt
            Case "System.String"
                Return ADODB.DataTypeEnum.adVarChar
            Case Else
                Return ADODB.DataTypeEnum.adVarChar
        End Select
    End Function
#End Region
End Class