﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmDocumentoAjusteCuenta
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Private listaDocumento_MovCuenta As List(Of Entidades.Documento_MovCuenta)
    Private listaDocumento_MovCuenta_Find As List(Of Entidades.Documento_MovCuenta)

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            ConfigurarDatos()
            inicializarFrm()
        End If
    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo
            With objCbo
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(CboTipoDocumento, CInt(Me.hddIdTipoDocumento.Value), 2, False)
                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

                .LLenarCboEstadoDocumento(Me.cboEstado)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True)
                .LlenarCboConceptoxIdTipoDocumento(Me.cboConcepto, CInt(Me.hddIdTipoDocumento.Value), False)

            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text

            Me.txtFechaInicio_BA.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_BA.Text = Me.txtFechaEmision.Text


            actualizarOpcionesBusquedaDocRef(1, False)

            ValidarPermisos()
            verFrm(FrmModo.Nuevo, True, True, True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / FECHA EMISION  / CONSULTAR
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {138, 139, 140, 141, 142})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try


            Dim objCbo As New Combo

            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged


        Try

            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            GenerarCodigoDocumento()


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub

    Private Sub limpiarFrm()

        '*************** CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

        If (Me.cboEstado.Items.FindByValue("1") IsNot Nothing) Then
            Me.cboEstado.SelectedValue = "1"
        End If

        Me.cboTipoOperacion.SelectedIndex = 0

        '***************** PERSONA
        Me.txtDescripcionPersona.Text = ""
        Me.txtDNI.Text = ""
        Me.txtRUC.Text = ""

        '*************** DETALLE
        Me.cboConcepto.SelectedIndex = 0
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()

        '*************** OBSERVACIONES
        Me.txtObservaciones.Text = ""

        '************* HIDDEN
        Me.hddIdDocumento.Value = ""
        Me.hddIdPersona.Value = ""
        Me.hddCodigoDocumento.Value = ""

        '*********** BUSQUEDA DOCUMENTO
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

    End Sub

    Private Sub verFrm(ByVal modoFrm As Integer, ByVal generarCodigoDoc As Boolean, ByVal limpiarSessionViewState As Boolean, ByVal initListasSession As Boolean, ByVal limpiarControles As Boolean, ByVal cargarParametrosGral As Boolean)

        If (limpiarControles) Then
            limpiarFrm()
        End If


        If (limpiarSessionViewState) Then
            Session.Remove("listaDocumento_MovCuenta")
            Session.Remove("listaDocumento_MovCuenta_Find")
        End If


        If (initListasSession) Then
            Me.listaDocumento_MovCuenta = New List(Of Entidades.Documento_MovCuenta)
            setListaDocumento_MovCuenta(Me.listaDocumento_MovCuenta)

            Me.listaDocumento_MovCuenta_Find = New List(Of Entidades.Documento_MovCuenta)
            setListaDocumento_MovCuenta_Find(Me.listaDocumento_MovCuenta_Find)
        End If

        If (generarCodigoDoc) Then
            GenerarCodigoDocumento()
        End If

        If (cargarParametrosGral) Then

            Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {6})
            Me.hddDiferenciaMaxSaldos.Value = CStr(listaParametros(0))

        End If

        '************** MODO FRM
        hddFrmModo.Value = CStr(modoFrm)
        ActualizarBotonesControl()

    End Sub

    Private Sub ActualizarBotonesControl()

        Select Case CInt(hddFrmModo.Value)

            Case 0 '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                'Me.GV_Detalle.Columns(7).Visible = False

            Case 1 '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                'Me.GV_Detalle.Columns(7).Visible = False

            Case 2 '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

                'Me.GV_Detalle.Columns(7).Visible = True

            Case 3 '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = True
                Me.btnBuscarDocumentoRef.Visible = False

                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()

                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False

                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                'Me.GV_Detalle.Columns(7).Visible = False

            Case 4 '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                '********** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

                'Me.GV_Detalle.Columns(7).Visible = True

            Case 5 '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '********* Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

                'Me.GV_Detalle.Columns(7).Visible = False

            Case 6  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '********* Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

                'Me.GV_Detalle.Columns(7).Visible = True

        End Select
    End Sub

    Private Function getListaDocumento_MovCuenta() As List(Of Entidades.Documento_MovCuenta)
        Return CType(Session.Item("listaDocumento_MovCuenta"), List(Of Entidades.Documento_MovCuenta))
    End Function

    Private Sub setListaDocumento_MovCuenta(ByVal lista As List(Of Entidades.Documento_MovCuenta))
        Session.Remove("listaDocumento_MovCuenta")
        Session.Add("listaDocumento_MovCuenta", lista)
    End Sub

    Private Function getListaDocumento_MovCuenta_Find() As List(Of Entidades.Documento_MovCuenta)
        Return CType(Session.Item("listaDocumento_MovCuenta_Find"), List(Of Entidades.Documento_MovCuenta))
    End Function

    Private Sub setListaDocumento_MovCuenta_Find(ByVal lista As List(Of Entidades.Documento_MovCuenta))
        Session.Remove("listaDocumento_MovCuenta_Find")
        Session.Add("listaDocumento_MovCuenta_Find", lista)
    End Sub

#Region "****************Buscar Personas Mantenimiento"
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

            Me.listaDocumento_MovCuenta = New List(Of Entidades.Documento_MovCuenta)
            setListaDocumento_MovCuenta(Me.listaDocumento_MovCuenta)

            Me.listaDocumento_MovCuenta_Find = New List(Of Entidades.Documento_MovCuenta)
            setListaDocumento_MovCuenta_Find(Me.listaDocumento_MovCuenta_Find)

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersonaView IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersonaView.Descripcion
            Me.txtDNI.Text = objPersonaView.Dni
            Me.txtRUC.Text = objPersonaView.Ruc
            Me.hddIdPersona.Value = CStr(objPersonaView.IdPersona)

        End If
    End Sub
    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 1)  '******************** CLIENTE

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdPersona.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaInicio_BA.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaFin_BA.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            If (Me.GV_BusquedaAvanzado.Rows.Count <= 0) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapa('capaBusquedaAvanzado');  alert('No se hallaron registros.');    ", True)
            Else
                objScript.onCapa(Me, "capaBusquedaAvanzado")
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            cargarDocumentoAjusteCuenta(Nothing, Nothing, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region




#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
                If (CInt(Me.hddIdPersona.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdPersona.Value)
                End If
            End If

            If (IdPersona = 0) Then
                Throw New Exception("DEBE SELECCIONAR UN CLIENTE. NO SE PERMITE LA OPERACIÓN.")
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            '*************  IdTienda = 0  (BUSCA DE TODAS LAS TIENDAS)
            Me.listaDocumento_MovCuenta_Find = (New Negocio.DocumentoAjusteCuenta).DocumentoAjusteCuenta_SelectMovCuentaCargoxParams(CInt(Session("IdUsuario")), IdPersona, serie, codigo, fechaInicio, fechafin, CInt(Me.hddIdTipoDocumento.Value), CInt(Me.CboTipoDocumento.SelectedValue))

            If (Me.listaDocumento_MovCuenta_Find.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = Me.listaDocumento_MovCuenta_Find
                Me.GV_DocumentosReferencia_Find.DataBind()
                objScript.onCapa(Me, "capaDocumentosReferencia")
            Else

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDocumentosReferencia');    alert('NO SE HALLARON REGISTROS.');     ", True)

            End If

            setListaDocumento_MovCuenta_Find(Me.listaDocumento_MovCuenta_Find)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoRef(Me.GV_DocumentosReferencia_Find.SelectedIndex)

    End Sub

    Private Sub cargarDocumentoRef(ByVal Index As Integer)

        Try

            actualizarListaDocumentoRef_Detalle()

            Me.listaDocumento_MovCuenta_Find = getListaDocumento_MovCuenta_Find()
            Me.listaDocumento_MovCuenta = getListaDocumento_MovCuenta()

            For i As Integer = 0 To Me.listaDocumento_MovCuenta.Count - 1

                If (Me.listaDocumento_MovCuenta(i).Id = Me.listaDocumento_MovCuenta_Find(Index).Id) Then
                    Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
                End If

            Next

            Me.listaDocumento_MovCuenta_Find(Index).Descuento = Me.listaDocumento_MovCuenta_Find(Index).Saldo
            Me.listaDocumento_MovCuenta.Add(Me.listaDocumento_MovCuenta_Find(Index))

            Me.GV_DocumentosReferencia_Find.DataSource = Me.listaDocumento_MovCuenta_Find
            Me.GV_DocumentosReferencia_Find.DataBind()

            Me.GV_Detalle.DataSource = Me.listaDocumento_MovCuenta
            Me.GV_Detalle.DataBind()

            setListaDocumento_MovCuenta(Me.listaDocumento_MovCuenta)
            setListaDocumento_MovCuenta_Find(Me.listaDocumento_MovCuenta_Find)

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try


    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

#End Region


    Protected Sub GV_Detalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Detalle.SelectedIndexChanged

        quitarDocumentoDetalle(Me.GV_Detalle.SelectedIndex)

    End Sub

    Private Sub quitarDocumentoDetalle(ByVal Index As Integer)
        Try

            actualizarListaDocumentoRef_Detalle()

            Me.listaDocumento_MovCuenta = getListaDocumento_MovCuenta()

            Me.listaDocumento_MovCuenta.RemoveAt(Index)

            Me.GV_Detalle.DataSource = Me.listaDocumento_MovCuenta
            Me.GV_Detalle.DataBind()

            setListaDocumento_MovCuenta(Me.listaDocumento_MovCuenta)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub actualizarListaDocumentoRef_Detalle()

        Me.listaDocumento_MovCuenta = getListaDocumento_MovCuenta()

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            Me.listaDocumento_MovCuenta(i).Descuento = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtSaldo_Nuevo"), TextBox).Text)

        Next

        setListaDocumento_MovCuenta(Me.listaDocumento_MovCuenta)

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        registrarDocumento()
    End Sub

    Private Sub validarFrm()

        Me.listaDocumento_MovCuenta = getListaDocumento_MovCuenta()

        Dim valorDifMaximo As Decimal = CDec(Me.hddDiferenciaMaxSaldos.Value)

        For i As Integer = 0 To Me.listaDocumento_MovCuenta.Count - 1
            Dim ingresado As Decimal = (Me.listaDocumento_MovCuenta(i).Descuento)
            Dim diferencia As Decimal = Math.Abs(Me.listaDocumento_MovCuenta(i).Saldo)
            If (ingresado > diferencia) Then
                Dim cad As String = "INGRESE UN MONTO MENOR AL SALDO ACTUAL. NO SE PERMITE LA OPERACIÓN."
                Throw New Exception(cad)
            End If




        Next

    End Sub
    Private Sub registrarDocumento()

        Try

            actualizarListaDocumentoRef_Detalle()

            validarFrm()


            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
            Dim listaDetalleConcepto As List(Of Entidades.DetalleConcepto) = obtenerListaDetalleConcepto()
            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo

                    objDocumento.Id = (New Negocio.DocumentoAjusteCuenta).registrarDocumento(objDocumento, listaDetalleConcepto, objObservaciones, listaRelacionDocumento)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")

                Case FrmModo.Editar

                    objDocumento.Id = (New Negocio.DocumentoAjusteCuenta).actualizarDocumento(objDocumento, listaDetalleConcepto, objObservaciones, listaRelacionDocumento)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")

            End Select

            verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)

        Me.listaDocumento_MovCuenta = getListaDocumento_MovCuenta()

        For i As Integer = 0 To Me.listaDocumento_MovCuenta.Count - 1

            Dim obj As New Entidades.RelacionDocumento

            With obj

                Select Case CInt(Me.hddFrmModo.Value)

                    Case FrmModo.Nuevo

                        .IdDocumento2 = Nothing

                    Case FrmModo.Editar

                        .IdDocumento2 = CInt(Me.hddIdDocumento.Value)

                End Select

                .IdDocumento1 = Me.listaDocumento_MovCuenta(i).Id

            End With

            lista.Add(obj)

        Next

        Return lista

    End Function
    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdPersona = CInt(Me.hddIdPersona.Value)
            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)

        End With

        Return objDocumento

    End Function
    Private Function obtenerListaDetalleConcepto() As List(Of Entidades.DetalleConcepto)

        Dim listaDetalleConcepto As New List(Of Entidades.DetalleConcepto)

        Me.listaDocumento_MovCuenta = getListaDocumento_MovCuenta()

        For i As Integer = 0 To Me.listaDocumento_MovCuenta.Count - 1

            Dim obj As New Entidades.DetalleConcepto

            With obj

                Select Case CInt(Me.hddFrmModo.Value)

                    Case FrmModo.Nuevo

                        .IdDocumento = Nothing

                    Case FrmModo.Editar

                        .IdDocumento = CInt(Me.hddIdDocumento.Value)

                End Select

                .IdTipoDocumentoRef = Me.listaDocumento_MovCuenta(i).IdTipoDocumento
                .NroDocumento = Me.listaDocumento_MovCuenta(i).getNroDocumento
                .Concepto = Me.cboConcepto.SelectedItem.ToString
                .IdMoneda = Me.listaDocumento_MovCuenta(i).IdMoneda
                .Monto = Me.listaDocumento_MovCuenta(i).Descuento
                .IdConcepto = CInt(Me.cboConcepto.SelectedValue)

                .IdDocumentoRef = Me.listaDocumento_MovCuenta(i).Id
                .Monto2 = Me.listaDocumento_MovCuenta(i).Saldo

                .IdMovCuentaRef = Me.listaDocumento_MovCuenta(i).IdMovCuenta


            End With

            listaDetalleConcepto.Add(obj)

        Next

        Return listaDetalleConcepto

    End Function


    Private Sub cargarDocumentoAjusteCuenta(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)

        '***************** LIMPIAR FRM
        verFrm(FrmModo.Inicio, False, True, True, True, True)

        '****************** CABECERA
        Dim objDocumento As Entidades.Documento = Nothing
        If (IdDocumento <> Nothing) Then
            objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
        Else
            objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
        End If

        '***************** DETALLE
        Me.listaDocumento_MovCuenta = (New Negocio.DocumentoAjusteCuenta).DocumentoAjusteCuenta_SelectDetalle(objDocumento.Id)

        '***************** OBSERVACIONES
        Dim objObservacion As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

        '******************** PERSONA
        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)

        '****************** LISTA DETALLE CONCEPTO
        Dim listaDetalleConcepto As List(Of Entidades.DetalleConcepto) = (New Negocio.DetalleConcepto).SelectxIdDocumento(objDocumento.Id)

        '*************** CARGAMOS LA INTERFACES
        cargarDocumentoAjusteCuenta_GUI(objDocumento, Me.listaDocumento_MovCuenta, objObservacion, objPersona, listaDetalleConcepto)

        '**************** GUARDO EN SESSIÓN
        setListaDocumento_MovCuenta(Me.listaDocumento_MovCuenta)


        verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, False)

    End Sub

    Private Sub cargarDocumentoAjusteCuenta_GUI(ByVal objDocumento As Entidades.Documento, ByVal listaDocumento_MovCuenta As List(Of Entidades.Documento_MovCuenta), ByVal objObservacion As Entidades.Observacion, ByVal objPersona As Entidades.PersonaView, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto))

        '********************    CARGAMOS LA CABECERA
        With objDocumento

            If (Me.cboEstado.Items.FindByValue(CStr(.IdEstadoDoc)) IsNot Nothing) Then
                Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)
            End If

            Me.txtCodigoDocumento.Text = .Codigo

            If (.FechaEmision <> Nothing) Then
                Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
            End If

            Me.hddIdDocumento.Value = CStr(.Id)
            Me.hddCodigoDocumento.Value = .Codigo

        End With


        '*********************** PERSONA
        If (objPersona IsNot Nothing) Then
            Me.txtDescripcionPersona.Text = objPersona.Descripcion
            Me.txtRUC.Text = objPersona.Ruc
            Me.txtDNI.Text = objPersona.Dni
            Me.hddIdPersona.Value = CStr(objPersona.IdPersona)
        End If

        '****************  DETALLE
        Me.GV_Detalle.DataSource = listaDocumento_MovCuenta
        Me.GV_Detalle.DataBind()


        '************** OBSERVACIONES
        If (objObservacion IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservacion.Observacion
        End If

        '************** COMBO CONCEPTO
        For i As Integer = 0 To listaDetalleConcepto.Count - 1

            If (Me.cboConcepto.Items.FindByValue(CStr(listaDetalleConcepto(i).IdConcepto)) IsNot Nothing) Then

                Me.cboConcepto.SelectedValue = CStr(listaDetalleConcepto(i).IdConcepto)
                Exit For

            End If


        Next

    End Sub

    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        Try

            cargarDocumentoAjusteCuenta(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value), Nothing)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        Try
            verFrm(FrmModo.Nuevo, True, True, True, True, True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        anularDocumento()
    End Sub
    Private Sub anularDocumento()

        Try

            If ((New Negocio.DocumentoAjusteCuenta).anularDocumento(CInt(Me.hddIdDocumento.Value))) Then

                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False, False)
                objScript.mostrarMsjAlerta(Me, "El documento se anuló con éxito.")

            Else

                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click

        Try

            verFrm(FrmModo.Editar, False, False, False, False, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
End Class