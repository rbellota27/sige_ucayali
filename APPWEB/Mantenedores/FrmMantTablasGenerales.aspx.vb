
Partial Public Class FrmMantTablasGenerales
    Inherits System.Web.UI.Page

    Private colorReadOnly As Drawing.Color = Drawing.Color.LightGray
    Private colorHabilitado As Drawing.Color = Drawing.Color.White
    Private objDataSource As Object
    Private listaUMedida As List(Of Entidades.MagnitudUnidad)
    Private listaMagnitud As List(Of Entidades.MagnitudUnidad)
    Private listaMagnitudUnidad As List(Of Entidades.MagnitudUnidad)
    Private listaTipoOperacion As List(Of Entidades.TipoDocumento_TipoOperacion)
    Private ListaTipoDocRef As List(Of Entidades.TipoDocRef_TipoDoc_View)
    Private ListaMotivoTipoOperacion As List(Of Entidades.MotivoT_TipoOperacion)
    Private ListaBancoOficina As New List(Of Entidades.Oficina)
    Private ListaMedioP_CondicionP As List(Of Entidades.MedioPago_CondicionPago)
    Private ListaCpto_TipoDoc As List(Of Entidades.Concepto_TipoDocumento)
    Private ListaTiendaConcepto As List(Of Entidades.Tienda_Concepto)
    Private ListaPerfilTipoPV As List(Of Entidades.PerfilTipoPV)
    Private ListaPerfilMedioPago As List(Of Entidades.Perfil_MedioPago)

    Private objScript As New ScriptManagerClass
    Private cbo As New Combo
    'SE CAMBIO EL CONTROL cmbTabla LOS ITEM SIGUIENTE: Material x Formato
    Private Lista_Linea_Area As List(Of Entidades.Linea_Area)
    Private Obj_Linea_Area As Entidades.Linea_Area
    Private Lista_Area As List(Of Entidades.Area)
    Private Obj_Area As Entidades.Area
    Private Lista_TipoDocumento_MP As List(Of Entidades.TipoDocumento_MedioPago)
    Private Obj_TipoDocumento_MP As Entidades.TipoDocumento_MedioPago

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        inicializar()
    End Sub
    Private Sub inicializar()
        If Not Me.IsPostBack Then
            CargarColumnasSector_sublinea()
            cargarControladores(ddlControlador)
            cargarChofer(ddlChofer)

            ConfigurarDatos()
            txttextoBusqueda.AutoPostBack = False
            Me.objDataSource = New Object
            Session.Add("objDataSource", Me.objDataSource)
            frmVisible(False)
            hddModo.Value = "0"
            hddTabla.Value = "Null"
            mostrarMensajeT(False, "")
            mostrarMensajeB(False, "")
            mostrarMensajeMotivoTraslado(False, "")
            cargarDatosUnidMedida(Me.M_cmbUMedida)
            CargarDatosCmbTipoOpera(Me.cmb_TipoOpera)
            CargarDatosCmbTipoDocRef(Me.cmb_TipoDocRef)
            cargarDatosMotivoTipoOpera(Me.cmb_MotivoTipoOperacion)
            CargardatosMedioPago(cbo_CondicionPago)
            CargarDatosTipoDocumento(cbo_TipoDocumento)
            CargarDatosTipoGasto(CboTipoGasto)
            CargardatosMedioPago(cbo_CondicionPago)
            CargarDatosPrecioventa(cboPerfilTV)
            CargardatosMedioPago(cboPerfilMP)
            CargarComboTipoDocumento(CboTipoDocumento)

            CargarDatosRegimen(cboRegimen)
            CargarDatosTipoOperacion(cboTipoOperacion)
            CargarDatosConcepto(dlConcepto) 'llama a la funcion para cargar los conceptos en el combo
            CargarDatosPais(dlPais) 'llama a la funcion para cargar los conceptos en el combo

            Me.Panel_Datos.Visible = False
            Me.Panel_MU.Visible = False
            Me.Panel_TipoDocOpera.Visible = False
            Me.Panel_TipoDocRef.Visible = False
            Me.Panel_Banco.Visible = False
            Me.Panel_MotivoTraslado.Visible = False
            Me.Panel_CondicionPago.Visible = False
            Me.Panel_TipoDocRef.Visible = False
            Panel_TipoDocumento.Visible = False
            PanelPerfilTipoPV.Visible = False
            PanelPerfilMedioPago.Visible = False
            Dim listaTipoOperacion As New List(Of Entidades.TipoDocumento_TipoOperacion)
            setlistTipoOperacion(listaTipoOperacion)
            Dim ListaTipoDocRef As New List(Of Entidades.TipoDocRef_TipoDoc_View)
            setlistTipoDocRef(ListaTipoDocRef)
            Dim listaMotivoTipoOperacion As New List(Of Entidades.MotivoT_TipoOperacion)
            setListaMotivoTras_TipoOpera(listaMotivoTipoOperacion)
            Dim ListaMedioP_CondicionP As New List(Of Entidades.MedioPago_CondicionPago)
            setListaMedioP_CondicionP(ListaMedioP_CondicionP)
        End If
    End Sub
    Private Sub mostrarMensajeT(ByVal isVisible As Boolean, ByVal texto As String)
        lblMensaje.Visible = isVisible
        lblMensaje.Text = texto
    End Sub
    Private Sub mostrarMensajeB(ByVal isVisible As Boolean, ByVal texto As String)
        lblMsjB.Visible = isVisible
        lblMsjB.Text = texto
    End Sub
    Private Sub mostrarMensajeMotivoTraslado(ByVal isVisible As Boolean, ByVal texto As String)
        LblMsj.Visible = isVisible
        LblMsj.Text = texto
    End Sub

    Protected Sub rdbBase_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBase.SelectedIndexChanged
        If hddTabla.Value = "Moneda" Then
            Dim obj As New Negocio.Moneda
            If rdbBase.SelectedIndex = 0 Then
                If obj.existeMonedaBase Then
                    'mostrar mensaje de error
                    rdbBase.SelectedIndex = 1
                End If
            End If
        End If
    End Sub
    Private Sub setDataSource(ByVal obj As Object)
        Session.Remove("objDataSource")
        Session.Add("objDataSource", obj)
    End Sub
    Private Function getDataSource() As Object
        Return Session.Item("objDataSource")
    End Function
    Private Sub frmVisible(ByVal flag As Boolean)
        lblBuscarPor.Visible = flag
        lblCodigo.Visible = flag
        lblDescBreve.Visible = flag
        lblDescripcion.Visible = flag
        lblEstado.Visible = flag
        lblEstadoB.Visible = flag

        lblpermisomasivo.Visible = flag

        lblTextoBusqueda.Visible = flag
        lblNomTabla.Visible = flag
        txtCodigo.Visible = flag
        txt2.Visible = flag
        txt1.Visible = flag
        txttextoBusqueda.Visible = flag
        cmbBuscarPor.Visible = flag
        rdbEstados.Visible = flag
        rdbEstadosB.Visible = flag

        rdbpermisomasivo.Visible = flag

        btnCancelar.Visible = flag
        btnFiltrar.Visible = flag
        btnGuardar.Visible = flag
        btnNuevo.Visible = flag
        DGV1.Visible = flag
        lbl4.Visible = flag
        txt3.Visible = flag

        'lbl5.Visible = flag
        'rdbBase.Visible = flag
        tr_MonedaBase.Visible = flag

        'btaddarea.Visible = flag
        tr_gv_area.Visible = flag

        'lblDocumentoReferente.Visible = flag
        'rdbDocumentoReferente.Visible = flag
        tr_DocumentoReferente.Visible = flag
        txt2.Visible = flag
        'lblTipoGasto.Visible = flag
        'CboTipoGasto.Visible = flag
        tr_TipoGasto.Visible = flag

        'lblTipoDocumento.Visible = flag
        'CboTipoDocumento.Visible = flag
        tr_TipoDocumento.Visible = flag

        'lblTipoOperacion.Visible = flag
        'cboTipoOperacion.Visible = flag
        tr_TipoOperacion.Visible = flag

        'lblRegimen.Visible = flag
        'cboRegimen.Visible = flag
        tr_Regimen.Visible = flag

        'llbConcepto.Visible = flag 'si flag es true muestra el control visible sino no lo muestra el control
        'dlConcepto.Visible = flag 'si flag es true muestra el control visible sino no lo muestra el control
        tr_Concepto.Visible = flag
        'lblPais.Visible = flag 'si flag es true muestra el control visible sino no lo muestra el control
        'dlPais.Visible = flag 'si flag es true muestra el control visible sino no lo muestra el control
        tr_Pais.Visible = flag

        'lblinterfazmp.Visible = flag
        'dlinterfazmp.Visible = flag
        tr_interfazmp.Visible = flag
        'lblprincipalmp.Visible = flag
        'ckprincipalmp.Visible = flag
        tr_principalmp.Visible = flag

        'dlTipoConceptoBanco.Visible = flag : lblTipoConceptoBanco.Visible = flag
        tr_TipoConceptoBanco.Visible = flag
        'dlConceptoMovBanco.Visible = flag : lblConceptoMovBanco.Visible = flag
        tr_ConceptoMovBanco.Visible = flag
        'lblcuentaContablemp.Visible = flag
        'txtcuentacontablemp.Visible = flag
        tr_CtaContablemp.Visible = flag

        pnltipodocumetomp.Visible = flag

        'pnlcentroCosto.Visible = flag
        'lblcentrocosto.Visible = flag
        tr_centrocosto.Visible = flag

        'lblCheck1.Visible = flag
        'chkCheck1.Visible = flag
        'lblCheck1Message.Visible = flag
        tr_check1.Visible = flag
        tr_check2.Visible = flag
        tr_check3.Visible = flag
        tr_check4.Visible = flag

        txtAbrev.Visible = flag
        lblAbrev.Visible = flag
        tr_combo1.Visible = flag

    End Sub
    Protected Sub cmbTabla_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTabla.SelectedIndexChanged

        limpiarFrm()
        hddModo.Value = "0"
        If cmbTabla.SelectedIndex = 0 Then
            hddTabla.Value = "Null"
            frmVisible(False)
        Else
            hddTabla.Value = cmbTabla.SelectedValue
            cargarCombo()
            verFrmInicio()
        End If
        mostrarMensajeB(False, "")
        mostrarMensajeT(False, "")
        mostrarMensajeMotivoTraslado(False, "")
    End Sub
    Private Sub limpiarFrm()
        txt3.Text = ""
        txtCodigo.Text = ""
        txt2.Text = ""
        txt1.Text = ""
        txt2.Text = ""
        txttextoBusqueda.Text = ""
        rdbEstadosB.SelectedIndex = 1
        rdbBase.SelectedIndex = 1
        rdbDocumentoReferente.SelectedIndex = 1
        DGV_UM.DataSource = Nothing
        DGV_UM.DataBind()
        DGV_TipoOpera.DataSource = Nothing
        DGV_TipoOpera.DataBind()

        DGV_TipoDocRef.DataSource = Nothing
        DGV_TipoDocRef.DataBind()

        DGV_MotivoTipoOperacion.DataSource = Nothing
        DGV_MotivoTipoOperacion.DataBind()
        DGV_CondicionPago.DataSource = Nothing
        DGV_CondicionPago.DataBind()
        DGV_TipoDocumento.DataSource = Nothing
        DGV_TipoDocumento.DataBind()
        gvConpTienda.DataSource = Nothing
        gvConpTienda.DataBind()
        DGVPerfilTipoPV.DataSource = Nothing
        DGVPerfilTipoPV.DataBind()

        lblCombo1.Text = ""
        cboCombo1.DataSource = Nothing 'Revisar
        lblCombo1Message.Text = ""

        lblCheck1.Text = ""
        chkCheck1.Text = ""
        chkCheck1.Checked = False
        lblCheck1Message.Text = ""

        Me.txtcaja1.Text = ""
        Me.txtcaja2.Text = ""
    End Sub
    Private Sub verCodigo(ByVal visible1 As Boolean, ByVal texto As String, ByVal readOnly1 As Boolean, ByVal textoCaja As String, ByVal maxLength As Integer)
        lblCodigo.Text = texto
        lblCodigo.Visible = visible1
        txtCodigo.Visible = visible1
        txtCodigo.ReadOnly = visible1
        txtCodigo.Text = textoCaja
        txtCodigo.MaxLength = maxLength
        If readOnly1 = True Then
            txtCodigo.BackColor = Me.colorReadOnly
        Else
            txtCodigo.BackColor = Me.colorHabilitado
        End If
    End Sub
    Private Sub verTxt1(ByVal visible1 As Boolean, ByVal texto As String, ByVal readOnly1 As Boolean, ByVal textoCaja As String, ByVal maxLength As Integer)
        lblDescripcion.Text = texto
        lblDescripcion.Visible = visible1
        txt1.Visible = visible1
        txt1.ReadOnly = readOnly1
        txt1.Text = HttpUtility.HtmlDecode(textoCaja)
        txt1.MaxLength = maxLength
        If readOnly1 = True Then
            txt1.BackColor = Me.colorReadOnly
        Else
            txt1.BackColor = Me.colorHabilitado
        End If
    End Sub

    Private Sub verTxt11(ByVal visible1 As Boolean, ByVal texto As String, ByVal readOnly1 As Boolean, ByVal textoCaja As String, ByVal maxLength As Integer)
        lblestadomasivo.Text = texto
        lblestadomasivo.Visible = visible1
        txt11.Visible = visible1
        txt11.ReadOnly = readOnly1
        txt11.Text = HttpUtility.HtmlDecode(textoCaja)
        txt11.MaxLength = maxLength
        If readOnly1 = True Then
            txt11.BackColor = Me.colorReadOnly
        Else
            txt11.BackColor = Me.colorHabilitado
        End If
    End Sub
    Private Sub verTxt2(ByVal visible1 As Boolean, ByVal texto As String, ByVal readOnly1 As Boolean, ByVal textoCaja As String, ByVal maxLength As Integer)
        lblDescBreve.Text = texto
        lblDescBreve.Visible = visible1
        txt2.Visible = visible1
        txt2.ReadOnly = readOnly1
        txt2.Text = HttpUtility.HtmlDecode(textoCaja)
        txt2.MaxLength = maxLength
        If readOnly1 = True Then
            txt2.BackColor = Me.colorReadOnly
        Else
            txt2.BackColor = Me.colorHabilitado
        End If
    End Sub
    Private Sub verTxtAbv(ByVal visible1 As Boolean, ByVal texto As String, ByVal readOnly1 As Boolean, ByVal textoCaja As String, ByVal maxLength As Integer)

        Me.lblAbrev.Text = texto
        lblAbrev.Visible = visible1
        txtAbrev.Visible = visible1
        txtAbrev.Text = HttpUtility.HtmlDecode(textoCaja)
        txtAbrev.MaxLength = maxLength
        If readOnly1 = True Then
            txtAbrev.BackColor = Me.colorReadOnly
        Else
            txtAbrev.BackColor = Me.colorHabilitado
        End If
    End Sub


    Private Sub verTxt3(ByVal flag As Boolean, ByVal texto As String, ByVal readOnly1 As Boolean, ByVal textoCaja As String, ByVal maxLength As Integer)
        lbl4.Text = texto
        lbl4.Visible = flag
        txt3.Visible = flag
        txt3.ReadOnly = readOnly1
        txt3.Text = HttpUtility.HtmlDecode(textoCaja).Trim
        txt3.MaxLength = maxLength
        If readOnly1 = True Then
            txt3.BackColor = Me.colorReadOnly
        Else
            txt3.BackColor = Me.colorHabilitado
        End If
    End Sub
    Private Sub verNomTabla(ByVal visible1 As Boolean, ByVal texto As String)
        lblNomTabla.Text = texto
        lblNomTabla.Visible = visible1
    End Sub
    Private Sub verEstado(ByVal visible1 As Boolean, ByVal readOnly1 As Boolean, ByVal indexSelected As Integer)
        lblEstado.Visible = visible1
        rdbEstados.Visible = visible1
        rdbEstados.Enabled = Not readOnly1
        rdbEstados.SelectedIndex = indexSelected
    End Sub

    'Private Sub VerPermiso(ByVal visible1 As Boolean, ByVal readOnly1 As Boolean, ByVal indexSelected As Integer)
    '    lblpermisomasivo.Visible = visible1
    '    rdbpermisomasivo.Visible = visible1
    '    rdbpermisomasivo.Enabled = Not readOnly1
    '    rdbpermisomasivo.SelectedIndex = indexSelected
    'End Sub

    Private Sub verRdbBase(ByVal visible1 As Boolean, ByVal readOnly1 As Boolean, ByVal indexSelected As Integer, Optional ByVal texto As String = "Moneda Base: ")
        'lbl5.Visible = visible1
        'rdbBase.Visible = visible1
        tr_MonedaBase.Visible = visible1
        rdbBase.Enabled = Not readOnly1
        rdbBase.SelectedIndex = indexSelected
        lbl5.Text = texto
    End Sub
    Private Sub verRdbDocumentoReferente(ByVal visible1 As Boolean, ByVal readonly1 As Boolean, ByVal valor As String)
        'lblDocumentoReferente.Visible = visible1
        'rdbDocumentoReferente.Visible = visible1
        tr_DocumentoReferente.Visible = visible1
        rdbDocumentoReferente.Enabled = Not readonly1

        If valor = "No Compromete Stock" Then valor = "0"
        If valor = "Si Compromete Stock" Then valor = "1"
        rdbDocumentoReferente.SelectedValue = valor
    End Sub
    Private Sub verBotones(ByVal flag As Boolean)
        Select Case CInt(hddModo.Value)
            Case 0 'Modo Inicio
                btnNuevo.Visible = flag
                btnGuardar.Visible = Not flag
                btnCancelar.Visible = Not flag
            Case Else 'Modo Nuevo
                btnNuevo.Visible = Not flag
                btnGuardar.Visible = flag
                btnCancelar.Visible = flag
        End Select
    End Sub
    Private Sub verControlesBusqueda(ByVal flag As Boolean, ByVal indexSelected As Integer)
        lblBuscarPor.Visible = flag
        cmbBuscarPor.Visible = flag
        lblTextoBusqueda.Visible = flag
        txttextoBusqueda.Visible = flag
        lblEstadoB.Visible = flag
        btnFiltrar.Visible = flag
        DGV1.Visible = flag
        rdbEstadosB.Visible = flag
        rdbEstadosB.SelectedIndex = indexSelected
        rdbDocumentoReferente.Visible = flag
        rdbDocumentoReferente.SelectedIndex = indexSelected
    End Sub
    Protected Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        mostrarMensajeT(False, "")
        mostrarMensajeMotivoTraslado(False, "")
        hddModo.Value = "0"
        verFrmInicio()
        If dlunidadnegocio.Items.Count > 0 Then
            dlunidadnegocio.SelectedValue = "00"
            dlunidadnegocio_SelectedIndexChanged(sender, e)
        End If
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click

        mostrarMensajeT(False, "")
        hddModo.Value = "1"

        Me.txtcaja1.Attributes.Remove("validarNumeroPuntoPositivo('event')")
        Me.txtcaja2.Attributes.Remove("validarNumeroPuntoPositivo('event')")


        verFrmNuevo()

        DGV_UM.DataSource = Nothing
        DGV_UM.DataBind()

        DGV_TipoOpera.DataSource = Nothing
        DGV_TipoOpera.DataBind()

        DGV_TipoDocRef.DataSource = Nothing
        DGV_TipoDocRef.DataBind()

        DGV_MotivoTipoOperacion.DataSource = Nothing
        DGV_MotivoTipoOperacion.DataBind()
        DGV_CondicionPago.DataSource = Nothing
        DGV_CondicionPago.DataBind()
        DGV_TipoDocumento.DataSource = Nothing
        DGV_TipoDocumento.DataBind()
        gvConpTienda.DataSource = Nothing
        gvConpTienda.DataBind()
        DGVPerfilTipoPV.DataSource = Nothing
        DGVPerfilTipoPV.DataBind()
        Dim lista As New List(Of Entidades.MagnitudUnidad)
        setListaUM(lista)
        Dim listaBancoOficina As New List(Of Entidades.Oficina)
        setListaBancoOficina(listaBancoOficina)
        Dim listaMedioPago As New List(Of Entidades.MedioPago_CondicionPago)
        setListaMedioP_CondicionP(listaMedioPago)
        Dim listaMotivoTipoOperacion As New List(Of Entidades.MotivoT_TipoOperacion)
        setListaMotivoTras_TipoOpera(listaMotivoTipoOperacion)
        Dim listaTipoDocumento_TipoOperacion As New List(Of Entidades.TipoDocumento_TipoOperacion)
        setlistTipoOperacion(listaTipoDocumento_TipoOperacion)
        Dim ListaTipoDocRef As New List(Of Entidades.TipoDocRef_TipoDoc_View)
        setlistTipoDocRef(ListaTipoDocRef)
        If dlunidadnegocio.Items.Count > 0 Then
            dlunidadnegocio.SelectedValue = "00"
            dlunidadnegocio_SelectedIndexChanged(sender, e)
        End If
        If cmbTabla.SelectedItem.Text = "Motivo de Traslado" Then
            Dim msj As String = "Si la opci�n ""No Comprometer Stock"" est� activo, se deber� proporcionar un documento referente."
            mostrarMensajeMotivoTraslado(True, msj)
        End If
    End Sub
    Protected Sub DGV1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV1.SelectedIndexChanged
        hddModo.Value = "2"
        verFrmEditar(DGV1.SelectedIndex)

        Select Case hddTabla.Value

            Case "Area"
                Dim cad As String = DGV1.SelectedRow.Cells(5).Text
                If cad = "---" Then
                    dlunidadnegocio.SelectedValue = "00" : dldptofuncional.SelectedValue = "00"
                    dlsubarea1.SelectedValue = "00" : dlsubarea2.SelectedValue = "00" : dlsubarea3.SelectedValue = "00"
                Else
                    dlunidadnegocio.SelectedValue = cad.Substring(0, 2) : dlunidadnegocio_SelectedIndexChanged(sender, e)
                    dldptofuncional.SelectedValue = cad.Substring(2, 2) : dldptofuncional_SelectedIndexChanged(sender, e)
                    dlsubarea1.SelectedValue = cad.Substring(4, 2) : dlsubarea1_SelectedIndexChanged(sender, e)
                    dlsubarea2.SelectedValue = cad.Substring(6, 2) : dlsubarea2_SelectedIndexChanged(sender, e)
                    dlsubarea3.SelectedValue = cad.Substring(8, 2)
                End If
            Case "Medio de Pago"
                If CInt(DGV1.SelectedRow.Cells(9).Text) <> 0 Then
                    dlTipoConceptoBanco.SelectedValue = DGV1.SelectedRow.Cells(10).Text
                    dlTipoConceptoBanco_SelectedIndexChanged(sender, e)
                    dlConceptoMovBanco.SelectedValue = DGV1.SelectedRow.Cells(9).Text
                Else
                    dlConceptoMovBanco.SelectedIndex = 0
                End If

        End Select
    End Sub
    Private Sub HabilitarBusqueda(ByVal flag As Boolean)
        cmbBuscarPor.Enabled = flag
        txttextoBusqueda.Enabled = flag
        btnFiltrar.Enabled = flag
        rdbEstadosB.Enabled = flag
        rdbDocumentoReferente.Enabled = flag
        DGV1.Enabled = flag
    End Sub
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Dim objScript As New ScriptManagerClass
        Try
            registrarDatos(hddTabla.Value, CInt(hddModo.Value))
            mostrarMensajeMotivoTraslado(True, "")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
    Private Function obtenerEstado(ByVal index As Integer) As String
        Dim estado As String = "0"
        If index = 0 Then
            estado = "1"
        End If
        Return estado
    End Function

    Private Function obtenerPermisoMasivo(ByVal index As Integer) As String
        Dim permisomasivo As String = "0"
        If index = 0 Then
            permisomasivo = "1"
        End If
        Return permisomasivo
    End Function

    Private Function obtenerEstadoBit(ByVal index As Integer) As Boolean
        Dim estado As Boolean = CBool(0)
        If index = 0 Then
            estado = CBool(1)
        End If
        Return estado
    End Function

    Private Function obtenerEstadoBool(ByVal est As Boolean) As String
        'index_estado=0 indica al item 0 'Activo', entonces si est=true, index_estado=0 
        Dim index_estado As String = "0"
        If Not est Then
            index_estado = "1"
        End If
        Return index_estado
    End Function

    Private Function obtenerBaseMoneda(ByVal index As Integer) As Boolean
        Dim base As Boolean = False
        If index = 0 Then
            base = True
        End If
        Return base
    End Function
    Private Sub HabilitarSeleccionTabla(ByVal flag As Boolean)
        cmbTabla.Enabled = flag
    End Sub
    Private Sub verFrmInicio()

        verNomTabla(True, hddTabla.Value)
        verBotones(True)
        verCodigo(False, "C�digo: ", True, "", 50)
        verEstado(False, True, 0)
        llenarGrillaInicio(True)
        verControlesBusqueda(True, 1)
        HabilitarBusqueda(True)
        HabilitarSeleccionTabla(True)
        verTxt1(False, "", True, "", 50)
        verTxt11(False, "", True, "", 1)
        verTxt2(False, "", True, "", 20)
        verTxt3(False, "", True, "", 50)
        verTxtAbv(False, "", True, "", 30)

        verRdbBase(False, True, 1)
        verRdbDocumentoReferente(False, True, "1")
        Me.Panel_MU.Visible = False
        Me.Panel_TipoDocOpera.Visible = False
        Me.Panel_TipoDocRef.Visible = False
        Me.Panel_Banco.Visible = False
        Me.Panel_MotivoTraslado.Visible = False
        Panel_CondicionPago.Visible = False
        Panel_TipoDocumento.Visible = False
        pnltipodocumetomp.Visible = False
        PanelPerfilTipoPV.Visible = False
        PanelPerfilMedioPago.Visible = False
        'lblTipoGasto.Visible = False
        'CboTipoGasto.Visible = False
        tr_TipoGasto.Visible = False

        'lblTipoDocumento.Visible = False
        'CboTipoDocumento.Visible = False
        tr_TipoDocumento.Visible = False

        'lblRegimen.Visible = False
        'cboRegimen.Visible = False
        tr_Regimen.Visible = False

        'lblTipoOperacion.Visible = False
        'cboTipoOperacion.Visible = False
        tr_TipoOperacion.Visible = False

        'btaddarea.Visible = False
        'gvarea.DataBind()
        tr_gv_area.Visible = False

        gvtipodocmp.DataBind()

        'lblinterfazmp.Visible = False
        'dlinterfazmp.Visible = False
        tr_interfazmp.Visible = False

        'lblprincipalmp.Visible = False
        'ckprincipalmp.Visible = False
        tr_principalmp.Visible = False
        'lblcuentaContablemp.Visible = False
        'txtcuentacontablemp.Visible = False
        tr_CtaContablemp.Visible = False

        'lblcentrocosto.Visible = False
        'pnlcentroCosto.Visible = False
        tr_centrocosto.Visible = False

        'dlTipoConceptoBanco.Visible = False : lblTipoConceptoBanco.Visible = False
        tr_TipoConceptoBanco.Visible = False
        'dlConceptoMovBanco.Visible = False : lblConceptoMovBanco.Visible = False
        tr_ConceptoMovBanco.Visible = False
        'dlConcepto.Visible = False : llbConcepto.Visible = False 'despues del cancelar los controles no aparecen
        tr_Concepto.Visible = False
        'dlPais.Visible = False : lblPais.Visible = False
        tr_Pais.Visible = False

        tr_Chofer.Visible = False
        tr_controlador.Visible = False
        'lblCheck1.Visible = False
        'chkCheck1.Visible = False
        'lblCheck1Message.Visible = False
        tr_check1.Visible = False
        tr_check2.Visible = False
        tr_check3.Visible = False
        tr_check4.Visible = False

        Me.tr_caja1.Visible = False
        Me.tr_caja2.Visible = False

        tr_combo1.Visible = False

        Panel_Datos.Visible = False

        tr_rel_Sector.Visible = False
        tr_rel_Sector_sublinea.Visible = False
        tr_gvSector_rel_Sublinea.Visible = False
    End Sub

    'CARGA LOS DATOS A LA GRILLA DEPENDIENDO DEL ESTADO SELECCIONADO
    Protected Sub rdbEstados_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbEstadosB.SelectedIndexChanged
        Dim nGrilla As New Grilla
        Select Case hddTabla.Value
            Case "Area"
                Dim objArea As New Negocio.Area
                nGrilla.llenarGrillaArea(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = objArea.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = objArea.SelectAllActivo
                Else
                    DGV1.DataSource = objArea.SelectAllInactivo
                End If

            Case "Banco"
                Dim objBanco As New Negocio.Banco
                nGrilla.llenarGrillaBanco(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = objBanco.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = objBanco.SelectAllActivo
                Else
                    DGV1.DataSource = objBanco.SelectAllInactivo
                End If

            Case "Calidad"
                Dim obj As New Negocio.Calidad
                nGrilla.llenarGrillaCalidad(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Cargo"
                Dim objCargo As New Negocio.Cargo
                nGrilla.llenarGrillaCargo(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = objCargo.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = objCargo.SelectAllActivo
                Else
                    DGV1.DataSource = objCargo.SelectAllInactivo
                End If
            Case "Categor�a de Empleado"
                Dim obj As New Negocio.CatEmpleado
                nGrilla.llenarGrillaCatEmpleado(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Chofer"
                Dim obj As New Negocio.LNValorizadoCajas
                nGrilla.llenarGrillaChofer(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then 'selecciona todos
                    DGV1.DataSource = obj.LN_ReturnDataTable("", "CHOFER_TODOS")
                ElseIf rdbEstadosB.SelectedIndex = 1 Then 'selecciona activos
                    DGV1.DataSource = obj.LN_ReturnDataTable("", "CHOFER_ACTIVO")
                Else
                    DGV1.DataSource = obj.LN_ReturnDataTable("", "CHOFER_INACTIVO") ' selecciona inactivos
                End If
            Case "Color"
                Dim obj As New Negocio.Color
                nGrilla.llenarGrillaColor(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Concepto"
                Dim obj As New Negocio.Concepto
                nGrilla.llenarGrillaConcepto(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If

            Case "Condici�n Comercial"
                Dim obj As New Negocio.CondicionComercial
                nGrilla.llenarGrillaCondicionComercial(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Condici�n de Pago"
                Dim obj As New Negocio.CondicionPago
                nGrilla.llenarGrillaCondicionPago(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Condici�n de Trabajo"
                Dim obj As New Negocio.CondTrabajo
                nGrilla.llenarGrillaCondicionTrabajo(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Estado Cargo Programado"
                Dim obj As New Negocio.EstadoCargoP
                nGrilla.llenarGrillaEstadoCargoP(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Estado Civil"
                Dim obj As New Negocio.EstadoCivil
                nGrilla.llenarGrillaEstadoCivil(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Estado del Documento"
                Dim obj As New Negocio.EstadoDocumento
                nGrilla.llenarGrillaEstadoDocumento(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Estado del Producto"
                Dim obj As New Negocio.EstadoProd
                nGrilla.llenarGrillaEstadoProd(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Fabricante"
                Dim obj As New Negocio.Fabricante
                nGrilla.llenarGrillaFabricante(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Giro"
                Dim obj As New Negocio.Giro
                nGrilla.llenarGrillaGiro(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If

            Case "Impuesto"
                nGrilla.llenarGrillaImpuesto(DGV1)
                Select Case rdbEstadosB.SelectedIndex
                    Case 0 '                       +++     Todos
                        DGV1.DataSource = (New Negocio.Impuesto).impuesto_select()
                    Case 1 '                       +++      Activo
                        DGV1.DataSource = (New Negocio.Impuesto).impuesto_select(IdEstado:="1")
                    Case 2 '                       +++      Inactivo
                        DGV1.DataSource = (New Negocio.Impuesto).impuesto_select(IdEstado:="0")
                End Select

            Case "Magnitud"
                Dim obj As New Negocio.Magnitud
                nGrilla.llenarGrillaMagnitud(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Estilo"
                Dim obj As New Negocio.Estilo
                nGrilla.llenarGrillaEstilo(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Marca"
                Dim obj As New Negocio.Marca
                nGrilla.llenarGrillaMarca(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
                'Case "Material"
            Case "Formato"
                Dim obj As New Negocio.Material
                nGrilla.llenarGrillaMaterial(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Medio de Pago"
                Dim obj As New Negocio.MedioPago
                nGrilla.llenarGrillaMedioPago(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Moneda"
                Dim obj As New Negocio.Moneda
                nGrilla.llenarGrillaMoneda(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Motivo de Baja"
                Dim obj As New Negocio.MotivoBaja
                nGrilla.llenarGrillaMotivoBaja(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Motivo de Traslado"
                Dim obj As New Negocio.MotivoTraslado
                nGrilla.llenarGrillaMotivoTraslado(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Movimiento Cuenta Tipo"
                Dim obj As New Negocio.MovCuentaTipo
                nGrilla.llenarGrillaMovCuentaTipo(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Nacionalidad"
                Dim obj As New Negocio.Nacionalidad
                nGrilla.llenarGrillaNacionalidad(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Perfil"
                Dim obj As New Negocio.Perfil
                nGrilla.llenarGrillaPerfil(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectxIdPerfilxNombrexEstado(0, "", "", "")
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectxIdPerfilxNombrexEstado(0, "", "1", "1")
                Else
                    DGV1.DataSource = obj.SelectxIdPerfilxNombrexEstado(0, "", "0", "0")
                End If
            Case "Profesi�n"
                Dim obj As New Negocio.Profesion
                nGrilla.llenarGrillaProfesion(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "R�gimen"
                Dim obj As New Negocio.Regimen
                nGrilla.llenarGrillaRegimen(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "R�gimen Operaci�n"
                Dim obj As New Negocio.RegimenOperacion
                nGrilla.llenarGrillaRegimenOperacion(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Rol"
                Dim obj As New Negocio.Rol
                nGrilla.llenarGrillaRol(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Sabor"
                Dim obj As New Negocio.Sabor
                nGrilla.llenarGrillaSabor(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Sector"
                Dim obj As New Negocio.LNValorizadoCajas
                nGrilla.llenarGrillaSector(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then 'selecciona todos
                    DGV1.DataSource = obj.LN_ReturnDataTable("", "TBL_ALMACEN_SECTOR_TODOS")
                ElseIf rdbEstadosB.SelectedIndex = 1 Then 'selecciona activos
                    DGV1.DataSource = obj.LN_ReturnDataTable("", "TBL_ALMACEN_SECTOR_MANTENIMIENTO")
                Else
                    DGV1.DataSource = obj.LN_ReturnDataTable("", "TBL_ALMACEN_SECTOR_INACTIVOS") ' selecciona inactivos
                End If
            Case "Talla"
                Dim obj As New Negocio.Talla
                nGrilla.llenarGrillaTalla(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tama�o"
                Dim obj As New Negocio.Tamanio
                nGrilla.llenarGrillaTamanio(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tarjeta"
                Dim obj As New Negocio.Tarjeta
                nGrilla.llenarGrillaTarjeta(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If

            Case "Tipo de Agente"
                Dim obj As New Negocio.TipoAgente
                nGrilla.llenarGrillaTipoAgente(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tipo de Comision"
                Dim obj As New Negocio.TipoComision
                nGrilla.llenarGrillaTipoComision(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tipo de Concepto Banco"
                Dim objTipoConceptoBanco As New Negocio.TipoConceptoBanco
                nGrilla.llenarGrillaTipoConceptoBanco(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = objTipoConceptoBanco.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = objTipoConceptoBanco.SelectAllActivo
                Else
                    DGV1.DataSource = objTipoConceptoBanco.SelectAllInactivo
                End If
            Case "Tipo de Correo"
                Dim obj As New Negocio.TipoCorreo
                nGrilla.llenarGrillaTipoCorreo(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tipo de Direcci�n"
                Dim obj As New Negocio.TipoDireccion
                nGrilla.llenarGrillaTipoDireccion(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tipo de Documento"
                Dim obj As New Negocio.TipoDocumento
                nGrilla.llenarGrillaTipoDocumento(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tipo de Documento Identidad"
                Dim obj As New Negocio.TipoDocumentoI
                nGrilla.llenarGrillaTipoDocumentoI(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If

            Case "Tipo de Existencia"
                Dim obj As New Negocio.TipoExistencia
                nGrilla.llenarGrillaTipoExistencia(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tipo de Gasto"
                Dim obj As New Negocio.TipoGasto
                nGrilla.llenarGrillaTipoGasto(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tipo de Movimiento"
                Dim obj As New Negocio.TipoMovimiento
                nGrilla.llenarGrillaTipoMovimiento(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tipo de Operaci�n"
                Dim obj As New Negocio.TipoOperacion
                nGrilla.llenarGrillaTipoOperacion(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tipo de Precio de Importaci�n"
                Dim obj As New Negocio.TipoPrecioImportacion
                nGrilla.llenarGrillaTipoPrecioImportacion(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tipo de Precio de Venta"
                Dim obj As New Negocio.TipoPrecioV
                nGrilla.llenarGrillaTipoPrecioV(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tipo de Tel�fono"
                Dim obj As New Negocio.TipoTelefono
                nGrilla.llenarGrillaTipoTelefono(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tipo de Tienda"
                Dim obj As New Negocio.TipoTienda
                nGrilla.llenarGrillaTipoTienda(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Tr�nsito"
                Dim obj As New Negocio.Transito
                nGrilla.llenarGrillaTransito(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If

            Case "Unidad de Medida"
                Dim obj As New Negocio.UnidadMedida
                nGrilla.llenarGrillaUnidadMedida(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "V�a Tipo"
                Dim obj As New Negocio.ViaTipo
                nGrilla.llenarGrillaViaTipo(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If
            Case "Zona Tipo"
                Dim obj As New Negocio.ZonaTipo
                nGrilla.llenarGrillaZonaTipo(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If

            Case "Motivo Gasto"
                Dim obj As New Negocio.MotivoGasto
                nGrilla.llenarGrillaMotivoGasto(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivoMotivoGasto
                Else
                    DGV1.DataSource = obj.SelectAllInactivoMotivoGasto
                End If
                'Busca por el estado
            Case "Modelo"
                Dim obj As New Negocio.Modelo
                nGrilla.llenarGrillaModelo(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivoModelo
                End If

            Case "Alias"
                Dim obj As New Negocio.AliasMant
                nGrilla.llenarGrillaAlias(DGV1)
                If rdbEstadosB.SelectedIndex = 0 Then
                    DGV1.DataSource = obj.SelectAll
                ElseIf rdbEstadosB.SelectedIndex = 1 Then
                    DGV1.DataSource = obj.SelectAllActivo
                Else
                    DGV1.DataSource = obj.SelectAllInactivo
                End If

        End Select
        DGV1.DataBind()
        Me.setDataSource(DGV1.DataSource)
        mostrarMensajeT(False, "")
        mostrarMensajeMotivoTraslado(False, "")
        If DGV1.Rows.Count = 0 Then
            mostrarMensajeB(True, "No se hallaron registros")
        Else
            mostrarMensajeB(False, "")
        End If
    End Sub
    'MUESTRA EL FRM PARA LA EDICION
    Private Sub verFrmEditar(ByVal index As Integer)
        Panel_Datos.Visible = True
        HabilitarBusqueda(False)
        HabilitarSeleccionTabla(False)
        verCodigo(True, "C�digo: ", True, DGV1.Rows(index).Cells(1).Text, 50)
        Select Case hddTabla.Value
            Case "Area"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "Descripci�n Breve: ", False, DGV1.Rows(index).Cells(3).Text, 20)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))
                Dim cad As String = DGV1.Rows(index).Cells(5).Text
                'lblcentrocosto.Visible = True : pnlcentroCosto.Visible = True
                tr_centrocosto.Visible = True

            Case "Banco"
                verTxt1(True, "C�digo Sunat: ", False, DGV1.Rows(index).Cells(2).Text, 3)
                verTxt2(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(3).Text, 50)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))
                verTxtAbv(True, "Abrev", False, DGV1.Rows(index).Cells(6).Text, 20)
                '************  Cargo los datos (Editar la grilla)
                ' Session.Remove("ListaBancoOficina")
                Me.ListaBancoOficina = (New Negocio.Oficina).SelectxIdBanco(CInt(DGV1.Rows(index).Cells(1).Text))
                Me.DGV_Oficina.DataSource = Me.ListaBancoOficina
                Me.DGV_Oficina.DataBind()

                Me.Panel_Banco.Visible = True
                Dim ListaOficios As New List(Of Entidades.Oficina)
                setListaBancoOficina(ListaBancoOficina)




            Case "C�lculo"
                verTxt1(True, "Descripci�n:", False, DGV1.Rows(index).Cells(2).Text, 30)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Calidad"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(3).Text, 30)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(4).Text, 10)
                verTxt3(True, "C�d. Calidad: ", False, DGV1.Rows(index).Cells(2).Text, 2)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(5).Text))))

            Case "Cargo"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))

            Case "Categor�a de Empleado"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Chofer"
                tr_Chofer.Visible = True
                Me.ddlChofer.SelectedIndex = -1
                If IsNothing(Me.ddlChofer.Items.FindByValue(DGV1.Rows(index).Cells(1).Text)) Then
                    objScript.mostrarMsjAlerta(Me, "La persona selecionada actualmente no tiene el cargo de chofer. Si insiste en editar este Usuario, deber� cambiar el cargo a Chofer desde el men� mantenimiento.")                    
                Else
                    Me.ddlChofer.Items.FindByValue(DGV1.Rows(index).Cells(1).Text).Selected = True
                End If
                verTxt1(True, "Licencia: ", False, DGV1.Rows(index).Cells(3).Text, 100)
                verTxt2(True, "Categoria: ", False, DGV1.Rows(index).Cells(4).Text, 100)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(5).Text))))
                
            Case "Color"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(3).Text, 30)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(4).Text, 10)
                verTxt3(True, "C�d. Color: ", False, DGV1.Rows(index).Cells(2).Text, 2)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(5).Text))))

            Case "Concepto"
                verTxt1(True, "Descripci�n: ", False, HttpUtility.HtmlDecode(DGV1.Rows(index).Cells(2).Text).Trim, 50)
                verTxt2(True, "Cuenta Contable: ", False, DGV1.Rows(index).Cells(3).Text, 12)
                CboTipoGasto.SelectedValue = HttpUtility.HtmlDecode(DGV1.Rows(index).Cells(4).Text)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(6).Text))))
                Panel_TipoDocumento.Visible = True
                tr_TipoGasto.Visible = True

                tr_check1.Visible = True : tr_check2.Visible = True

                chkCheck1.Checked = CBool(CType(DGV1.Rows(index).Cells(8).Controls(0), CheckBox).Checked)
                chkCheck2.Checked = CBool(CType(DGV1.Rows(index).Cells(11).Controls(0), CheckBox).Checked)

                Me.tr_caja1.Visible = True
                Me.tr_caja2.Visible = True
                If IsNumeric(DGV1.Rows(index).Cells(9).Text) Then Me.txtcaja1.Text = CStr(Math.Round(CDec(DGV1.Rows(index).Cells(9).Text), 2)) Else Me.txtcaja1.Text = ""
                If IsNumeric(DGV1.Rows(index).Cells(10).Text) Then Me.txtcaja2.Text = CStr(Math.Round(CDec(DGV1.Rows(index).Cells(10).Text), 2)) Else Me.txtcaja2.Text = ""

                '************ Edita la Lista Condici�n de Pago
                ListaCpto_TipoDoc = (New Negocio.Concepto)._Concepto_TipoDocumentoEditarxIdConcepto(CInt(DGV1.Rows(index).Cells(1).Text))
                DGV_TipoDocumento.DataSource = ListaCpto_TipoDoc
                DGV_TipoDocumento.DataBind()

                ListaTiendaConcepto = (New Negocio.Tienda_Concepto).SelectTienda_Concepto(CInt(DGV1.Rows(index).Cells(1).Text))
                gvConpTienda.DataSource = ListaTiendaConcepto
                gvConpTienda.DataBind()
                'setListaCpto_TipoDoc(ListaCpto_TipoDoc)
            Case "Condici�n Comercial"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
                CboTipoDocumento.SelectedValue = CStr(DGV1.Rows(index).Cells(5).Text)
                'CboTipoDocumento.Visible = True
                'lblTipoDocumento.Visible = True
                tr_TipoDocumento.Visible = True
            Case "Condici�n de Pago"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
                Me.Panel_CondicionPago.Visible = True

                '************ Edita la Lista Condici�n de Pago
                ListaMedioP_CondicionP = (New Negocio.MedioPago)._CondicionPago_MedioPagoSelectxIdCondicionPago(CInt(DGV1.Rows(index).Cells(1).Text))
                DGV_CondicionPago.DataSource = ListaMedioP_CondicionP
                DGV_CondicionPago.DataBind()
                setListaMedioP_CondicionP(ListaMedioP_CondicionP)

            Case "Condici�n de Trabajo"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 20)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Estado Cargo Programado"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 20)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Estado Civil"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 20)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Estado del Documento"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 25)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Estado del Producto"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 20)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Fabricante"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(3).Text, 20)
                verTxt3(True, "C�digo Fab.: ", False, DGV1.Rows(index).Cells(4).Text, 2)
                verEstado(True, False, CInt(CStr(obtenerEstado(CInt(DGV1.Rows(index).Cells(5).Text)))))
                'verEstado(True, False, CInt(CStr(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text)))))
                'dlPais.Visible = True : dlPais.Visible = True
                tr_Pais.Visible = True

                dlPais.SelectedValue = CStr(CInt(DGV1.Rows(index).Cells(7).Text))


            Case "Giro"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))

            Case "Impuesto"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(3).Text, 10)
                verTxt3(True, "Tasa: ", False, DGV1.Rows(index).Cells(4).Text, 5)
                tr_CtaContablemp.Visible = True
                txtcuentacontablemp.Text = HttpUtility.HtmlDecode(DGV1.Rows(index).Cells(5).Text)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(6).Text))))

            Case "Magnitud"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(3).Text, 10)
                verEstado(True, False, CInt(IIf(DGV1.Rows(index).Cells(6).Text = "Activo", 0, 1)))

                '************  Cargo los datos (Editar la grilla Magnitud Unidad)

                Me.listaMagnitudUnidad = (New Negocio.MagnitudUnidad)._MagnitudUnidadselectxIdMagnitud(CInt(DGV1.Rows(index).Cells(1).Text))
                Me.DGV_UM.DataSource = Me.listaMagnitudUnidad
                Me.DGV_UM.DataBind()
                setListaUM(Me.listaMagnitudUnidad)
                Me.Panel_MU.Visible = True
            Case "Estilo"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(3).Text, 30)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(4).Text, 10)
                verTxt3(True, "C�d. Estilo: ", False, DGV1.Rows(index).Cells(2).Text, 2)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(5).Text))))

            Case "Marca"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(3).Text, 30)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(4).Text, 10)
                verTxt3(True, "C�d. Marca: ", False, DGV1.Rows(index).Cells(2).Text, 2)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(5).Text))))
                'Case "Material"
            Case "Formato"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(3).Text, 30)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(4).Text, 10)
                verTxt3(True, "C�d Formato: ", False, DGV1.Rows(index).Cells(2).Text, 2)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(5).Text))))

            Case "Medio de Pago"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 100)
                verTxt2(True, "C�d. Sunat:", False, DGV1.Rows(index).Cells(3).Text, 3)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))
                verTxtAbv(True, "Abrev:", False, DGV1.Rows(index).Cells(11).Text, 20)



                'lblinterfazmp.Visible = True
                'dlinterfazmp.Visible = True
                tr_interfazmp.Visible = True
                pnltipodocumetomp.Visible = True
                'lblprincipalmp.Visible = True
                'ckprincipalmp.Visible = True
                tr_principalmp.Visible = True

                'lblcuentaContablemp.Visible = True
                'txtcuentacontablemp.Visible = True
                tr_CtaContablemp.Visible = True
                'dlTipoConceptoBanco.Visible = True : lblTipoConceptoBanco.Visible = True
                tr_TipoConceptoBanco.Visible = True
                'dlConceptoMovBanco.Visible = True : lblConceptoMovBanco.Visible = True
                tr_ConceptoMovBanco.Visible = True

                Me.tr_check1.Visible = True

                ckprincipalmp.Checked = CBool(CType(DGV1.Rows(index).Cells(6).Controls(0), CheckBox).Checked)
                Me.chkCheck1.Checked = CBool(CType(Me.DGV1.Rows(index).Cells(12).Controls(0), CheckBox).Checked)

                dlinterfazmp.SelectedValue = DGV1.Rows(index).Cells(7).Text : txtcuentacontablemp.Text = HttpUtility.HtmlDecode(DGV1.Rows(index).Cells(8).Text)
                Lista_TipoDocumento_MP = cargarTipoDoc_MedioP(CInt(txtCodigo.Text))
                gvtipodocmp.DataSource = Lista_TipoDocumento_MP
                gvtipodocmp.DataBind()

            Case "Moneda"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 20)
                verTxt2(True, "S�mbolo: ", False, DGV1.Rows(index).Cells(3).Text, 4)
                verTxt3(True, "C�d. Sunat: ", False, DGV1.Rows(index).Cells(4).Text, 2)
                verRdbBase(True, False, CInt(IIf(CBool(DGV1.Rows(index).Cells(5).Text) = True, 0, 1)))
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(6).Text))))
            Case "Motivo de Baja"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 20)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Motivo de Traslado"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
                verRdbDocumentoReferente(True, False, DGV1.Rows(index).Cells(4).Text)

                '*********** Lbl-Mensajes
                If cmbTabla.SelectedItem.Text = "Motivo de Traslado" Then
                    Dim msj As String = "Si la opci�n ""No Comprometer Stock en Transito"" est� activo, se deber� proporcionar un documento referente."
                    mostrarMensajeMotivoTraslado(True, msj)
                End If

                '************  Cargo los datos (Editar la grilla)
                Me.ListaMotivoTipoOperacion = (New Negocio.MotivoT_TipoOperacion)._MotivoT_TipoOperacioNSelectxIdMotivoT(CInt(DGV1.Rows(index).Cells(1).Text))
                Me.DGV_MotivoTipoOperacion.DataSource = Me.ListaMotivoTipoOperacion
                Me.DGV_MotivoTipoOperacion.DataBind()
                Me.Panel_MotivoTraslado.Visible = True
                Dim ListaTipoOperacion As New List(Of Entidades.MotivoT_TipoOperacion)
                setListaMotivoTras_TipoOpera(ListaMotivoTipoOperacion)

            Case "Movimiento Cuenta Tipo"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 20)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Nacionalidad"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))
                chkCheck1.Checked = CBool(CType(DGV1.Rows(index).Cells(3).Controls(0), CheckBox).Checked)
                tr_check1.Visible = True
                lblCheck1.Text = "Nacional:"
            Case "Perfil"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verEstado(True, False, CInt(obtenerEstadoBool(CBool(CType(DGV1.Rows(index).Cells(3).Controls(0), CheckBox).Checked))))
                verTxt11(True, "Estado Masivo: ", False, DGV1.Rows(index).Cells(4).Text, 1)
                PanelPerfilTipoPV.Visible = True
                PanelPerfilMedioPago.Visible = True

                '************ Edita la Lista PerfilTotalPV
                ListaPerfilTipoPV = (New Negocio.PerfilTipoPV)._PerfilTipoPVselectxIdPerfil(CInt(DGV1.Rows(index).Cells(1).Text))
                DGVPerfilTipoPV.DataSource = ListaPerfilTipoPV
                DGVPerfilTipoPV.DataBind()
                '************ Edita la Lista PerfilMedioPago
                ListaPerfilMedioPago = (New Negocio.Perfil_MedioPago).SelectxIdPerfil(CInt(DGV1.Rows(index).Cells(1).Text))
                DGVPerfilMedioPago.DataSource = ListaPerfilMedioPago
                DGVPerfilMedioPago.DataBind()

            Case "Profesi�n"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "R�gimen"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verTxt2(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(3).Text, 10)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))
            Case "R�gimen Operaci�n"
                '
                verTxt1(True, "Cuenta Contable: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                'verTxt2(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(3).Text, 10)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))

                'lblTipoOperacion.Visible = True
                'cboTipoOperacion.Visible = True
                tr_TipoOperacion.Visible = True


                'lblRegimen.Visible = True
                'cboRegimen.Visible = True
                tr_Regimen.Visible = True

                cboRegimen.SelectedValue = DGV1.Rows(index).Cells(1).Text
                cboTipoOperacion.SelectedValue = DGV1.Rows(index).Cells(3).Text

                cboTipoOperacion.Enabled = False
                cboRegimen.Enabled = False
                '
            Case "Rol"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Sabor"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(3).Text, 10)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))
            Case "Sector"
                verTxt1(True, "Nombre: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verTxt2(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(3).Text, 10)
                tr_controlador.Visible = True
                Me.ddlControlador.SelectedIndex = -1
                Me.ddlControlador.Items.FindByText(DGV1.Rows(index).Cells(4).Text.Trim()).Selected = True
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(5).Text))))
            Case "Talla"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(3).Text, 10)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))
            Case "Tama�o"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Tarjeta"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Tipo de Agente"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "Tasa: ", False, DGV1.Rows(index).Cells(3).Text, 50)
                verEstado(True, False, CInt(obtenerEstadoBool(CBool(CType(DGV1.Rows(index).Cells(5).Controls(0), CheckBox).Checked))))
                tr_check1.Visible = True
                chkCheck1.Checked = CBool(CType(DGV1.Rows(index).Cells(4).Controls(0), CheckBox).Checked)

            Case "Tipo de Comision"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Tipo de Concepto Banco"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "Factor", False, DGV1.Rows(index).Cells(3).Text, 20)
                verEstado(True, False, CInt(obtenerEstadoBool(CBool(DGV1.Rows(index).Cells(4).Text))))
            Case "Tipo de Correo"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 20)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Tipo de Direcci�n"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Tipo de Documento"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(3).Text, 30)
                verTxt3(True, "C�d. Sunat: ", False, DGV1.Rows(index).Cells(4).Text, 3)
                'verRdbBase(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(5).Text))), "Exportar a Contab.: ")
                verEstado(True, False, CInt(obtenerEstadoBool(CBool(CType(DGV1.Rows(index).Cells(8).Controls(0), CheckBox).Checked))))
                'Export Cont.
                tr_check1.Visible = True
                chkCheck1.Checked = CBool(CType(DGV1.Rows(index).Cells(5).Controls(0), CheckBox).Checked)
                'Externo
                tr_check2.Visible = True
                chkCheck2.Checked = CBool(CType(DGV1.Rows(index).Cells(6).Controls(0), CheckBox).Checked)
                'Cancelable
                tr_check3.Visible = True
                chkCheck3.Checked = CBool(CType(DGV1.Rows(index).Cells(7).Controls(0), CheckBox).Checked)

                '************  Cargo los datos (Editar la grilla)
                Me.listaTipoOperacion = (New Negocio.TipoDocumento_TipoOperacion)._TipoDocumento_TipoOperacionSelectxIdDocumento(CInt(DGV1.Rows(index).Cells(1).Text))
                Me.ListaTipoDocRef = (New Negocio.TipoDocRef_TipoDoc_View).SelectxIdTipoDoc(CInt(DGV1.Rows(index).Cells(1).Text))
                Me.DGV_TipoOpera.DataSource = Me.listaTipoOperacion
                Me.DGV_TipoOpera.DataBind()
                Me.DGV_TipoDocRef.DataSource = Me.ListaTipoDocRef
                Me.DGV_TipoDocRef.DataBind()

                ' Dim ListaTipoOperacion As New List(Of Entidades.TipoDocumento_TipoOperacion)
                setlistTipoOperacion(listaTipoOperacion)
                setlistTipoDocRef(ListaTipoDocRef)
                Me.Panel_TipoDocOpera.Visible = True
                Me.Panel_TipoDocRef.Visible = True

            Case "Tipo de Documento Identidad"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(3).Text, 15)
                verTxt3(True, "C�d. Sunat: ", False, DGV1.Rows(index).Cells(4).Text, 2)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(5).Text))))

            Case "Tipo de Existencia"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "C�d. Sunat: ", False, DGV1.Rows(index).Cells(3).Text, 3)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))
                hdd_tienerelacion.Value = CStr(DGV1.Rows(index).Cells(6).Text.Trim)
                'btaddarea.Visible = True
                tr_gv_area.Visible = True
                Lista_Linea_Area = cargarLinea_Area(CInt(txtCodigo.Text))
                gvarea.DataSource = Lista_Linea_Area
                gvarea.DataBind()

            Case "Tipo de Gasto"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Tipo de Movimiento"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 20)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Tipo de Operaci�n"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "C�d. Sunat: ", False, DGV1.Rows(index).Cells(3).Text, 3)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))
            Case "Tipo de Precio de Importaci�n"
                verTxt1(True, "Nombre: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "Nombre Corto: ", False, DGV1.Rows(index).Cells(3).Text, 20)
                verEstado(True, False, CInt(obtenerEstadoBool(CType(DGV1.Rows(index).Cells(4).Controls(0), CheckBox).Checked)))
            Case "Tipo de Precio de Venta"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(3).Text, 10)
                verEstado(True, False, CInt(obtenerEstadoBool(CBool(CType(DGV1.Rows(index).Cells(4).Controls(0), CheckBox).Checked))))
                'Se carga el default
                tr_check1.Visible = True
                chkCheck1.Checked = CBool(CType(DGV1.Rows(index).Cells(6).Controls(0), CheckBox).Checked)

                tr_combo1.Visible = True
                'AfectoCargoTarjeta
                tr_check2.Visible = True
                chkCheck2.Checked = CBool(CType(DGV1.Rows(index).Cells(5).Controls(0), CheckBox).Checked)


            Case "Tipo de Tel�fono"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 20)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Tipo de Tienda"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(3).Text))))
            Case "Tr�nsito"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(3).Text, 30)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(4).Text, 10)
                verTxt3(True, "C�d. Transito: ", False, DGV1.Rows(index).Cells(2).Text, 2)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(5).Text))))
            Case "Unidad de Medida"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(3).Text, 5)
                verTxt3(True, "C�d. Sunat: ", False, DGV1.Rows(index).Cells(4).Text, 3)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(5).Text))))
            Case "V�a Tipo"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 30)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(3).Text, 5)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))
            Case "Zona Tipo"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(3).Text, 10)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))

            Case "Modelo"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(3).Text, 50)
                verTxt3(True, "C�d. Modelo: ", False, DGV1.Rows(index).Cells(2).Text, 2)
                verEstado(True, False, CInt(CStr(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text)))))

            Case "Motivo Gasto"
                verTxt1(True, "Descripci�n: ", False, DGV1.Rows(index).Cells(2).Text, 50)
                verTxt2(True, "Abv.: ", False, DGV1.Rows(index).Cells(3).Text, 20)
                verEstado(True, False, CInt(CStr(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text)))))
                'dlConcepto.Visible = True : llbConcepto.Visible = True
                tr_Concepto.Visible = True
                dlConcepto.SelectedValue = CStr(CInt(DGV1.Rows(index).Cells(6).Text))

            Case "Alias"
                verTxt2(True, "C�d. Persona:", False, DGV1.Rows(index).Cells(3).Text, 9)
                verTxt3(True, "Persona:", True, HttpUtility.HtmlDecode(DGV1.Rows(index).Cells(2).Text).Trim, 150)
                verEstado(True, False, CInt(obtenerEstado(CInt(DGV1.Rows(index).Cells(4).Text))))
                verTxt1(True, "Observaci�n: ", False, HttpUtility.HtmlDecode(DGV1.Rows(index).Cells(6).Text).Trim, 30)

        End Select
        verBotones(True)
    End Sub


    'FUNCION EL CUAL REALIZA LA TRANSACCION
    Private Sub registrarDatos(ByVal tabla As String, ByVal modo As Integer)
        Dim msjExito As String = "Operaci�n finalizada con �xito."
        Dim msjError As String = "Problemas en la operaci�n."
        Dim exito As Boolean = False
        Dim ValidoRep As New Negocio.Util

        Try
            Select Case tabla
                Case "Area"
                    Dim nArea As New Negocio.Area
                    Dim eArea As New Entidades.Area
                    eArea.Descripcion = txt1.Text.Trim
                    eArea.DescripcionCorta = txt2.Text.Trim
                    eArea.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    eArea.IdCentroCosto = getCentroCosto()
                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Area", "ar_NombreLargo", eArea.Descripcion) <= 0 Then
                            exito = nArea.InsertaArea(eArea)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el area.")
                        End If

                    Else 'Editar
                        eArea.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Area", "ar_NombreLargo", eArea.Descripcion, "IdArea", eArea.Id) <= 0 Then
                            exito = nArea.ActualizaArea(eArea)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el area.")
                        End If

                    End If
                Case "Banco"
                    Dim nBanco As New Negocio.Banco
                    Dim eBanco As New Entidades.Banco
                    eBanco.CodigoSunat = txt1.Text.Trim
                    eBanco.Nombre = txt2.Text.Trim
                    eBanco.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    eBanco.Abrev = txtAbrev.Text.Trim

                    If DGV_Oficina IsNot Nothing Then
                        saveListaBancoOficina()
                        Me.ListaBancoOficina = getListaBancoOficina()
                    End If

                    If modo = 1 Then 'Nuevo
                        If DGV_Oficina IsNot Nothing Then
                            exito = nBanco.InsertaBancoOficina(eBanco, Me.ListaBancoOficina)
                            Session.Remove("ListaBancoOficina")
                        Else
                            exito = nBanco.InsertaBanco(eBanco)
                        End If

                    Else 'Editar
                        eBanco.Id = CInt(txtCodigo.Text)
                        exito = nBanco.ActualizaBancoOficina(eBanco, ListaBancoOficina)
                        Session.Remove("ListaBancoOficina")

                    End If



                Case "Calidad"
                    If txt3.Text = "" Then
                        objScript.mostrarMsjAlerta(Me, "Ingrese c�digo calidad.")
                        txt3.Focus()
                        Exit Sub
                        Exit Select
                    End If

                    Dim nobj As New Negocio.Calidad
                    Dim eobj As New Entidades.Calidad
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.Cal_Codigo = Me.txt3.Text.Trim
                    eobj.Estado = CBool(obtenerEstado(rdbEstados.SelectedIndex))

                    If ValidoRep.ValidarExistenciaxTablax1Campo("calidad", "Cal_Nombre", eobj.Nombre) <= 0 Then
                        If modo = 1 Then 'Nuevo
                            exito = nobj.InsertaCalidad(eobj)
                        Else 'Editar
                            eobj.IdCalidad = CInt(txtCodigo.Text)
                            exito = nobj.ActualizaCalidad(eobj)
                        End If
                    Else
                        objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                    End If


                Case "Cargo"
                    Dim nCargo As New Negocio.Cargo
                    Dim eCargo As New Entidades.Cargo
                    eCargo.Nombre = txt1.Text.Trim
                    eCargo.Estado = obtenerEstado(rdbEstados.SelectedIndex)

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Cargo", "Car_Nombre", eCargo.Nombre) <= 0 Then
                            exito = nCargo.InsertaCargo(eCargo)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eCargo.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Cargo", "Car_Nombre", eCargo.Nombre, "IdCargo", eCargo.Id) <= 0 Then
                            exito = nCargo.ActualizaCargo(eCargo)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Categor�a de Empleado"
                    Dim nobj As New Negocio.CatEmpleado
                    Dim eobj As New Entidades.CatEmpleado
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("CatEmpleado", "cate_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaCatEmpleado(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.IdCatEmpleado = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("CatEmpleado", "cate_Nombre", eobj.Nombre, "IdCatEmpleado", eobj.IdCatEmpleado) <= 0 Then
                            exito = nobj.ActualizaCatEmpleado(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If
                Case "Chofer"
                    Dim nobj As New Negocio.Chofer
                    Dim objValida As New Negocio.LNValorizadoCajas
                    Dim eobj As New Entidades.Chofer
                    eobj.Id = ddlChofer.SelectedValue
                    eobj.Licencia = txt1.Text.Trim()
                    eobj.Categoria = txt2.Text.Trim()
                    eobj.estado = obtenerEstado(rdbEstados.SelectedIndex)
                    If modo = 1 Then ' nuevo
                        If objValida.LN_ReturnDataTable(ddlChofer.SelectedValue, "VALIDA_CHOFER").Rows.Count = 0 Then
                            exito = nobj.LN_InsertarChofer(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    Else 'Editar
                        exito = nobj.LN_ActualizaChofer(eobj)
                    End If

                Case "Color"
                    If txt3.Text = "" Then
                        objScript.mostrarMsjAlerta(Me, "Ingrese c�digo color.")
                        txt3.Focus()
                        Exit Sub
                        Exit Select
                    End If

                    Dim nobj As New Negocio.Color
                    Dim eobj As New Entidades.Color
                    eobj.Nombre = txt1.Text.Trim
                    eobj.NombreCorto = txt2.Text.Trim
                    eobj.Col_Codigo = txt3.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)

                    If modo = 1 Then 'Nuevo
                        exito = nobj.InsertaColor(eobj)
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        exito = nobj.ActualizaColor(eobj)
                    End If


                Case "Concepto"
                    Dim nobj As New Negocio.Concepto
                    Dim eobj As New Entidades.Concepto
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    eobj.CuentaContable = txt2.Text.Trim
                    eobj.con_IdTipoGasto = CInt(CboTipoGasto.SelectedValue)
                    eobj.ConceptoAdelanto = chkCheck1.Checked
                    eobj.con_NoAfectoIGV = chkCheck2.Checked

                    If IsNumeric(Me.txtcaja1.Text.Trim) Then eobj.PorcentDetraccion = CDec(Me.txtcaja1.Text.Trim)
                    If IsNumeric(Me.txtcaja2.Text.Trim) Then eobj.MontoMinimoDetraccion = CDec(Me.txtcaja2.Text.Trim)
                    ListaCpto_TipoDoc = getListaCpto_TipoDoc()
                    ListaTiendaConcepto = getListaTiendaConcepto()


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("concepto", "con_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaCpto_TipoDoc(eobj, ListaCpto_TipoDoc, ListaTiendaConcepto)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("concepto", "con_Nombre", eobj.Nombre, "IdConcepto", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaConcepto_TipoDocumento(eobj, ListaCpto_TipoDoc, ListaTiendaConcepto)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Condici�n Comercial"
                    Dim nobj As New Negocio.CondicionComercial
                    Dim eobj As New Entidades.CondicionComercial
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    eobj.IdTipoDocumento = CInt(CboTipoDocumento.SelectedValue)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("CondicionComercial", "cct_Nombre", eobj.Descripcion) <= 0 Then
                            exito = nobj.InsertaCondicionComercial(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("CondicionComercial", "cct_Nombre", eobj.Descripcion, "IdCondicionCC", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaCondicionComercial(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Condici�n de Pago"
                    Dim nobj As New Negocio.CondicionPago
                    Dim eobj As New Entidades.CondicionPago
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)

                    saveListaMedioP_CondicionP()
                    ListaMedioP_CondicionP = getListaMedioP_CondicionP()
                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("CondicionPago", "cp_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaMedioP_CondicionP(eobj, ListaMedioP_CondicionP)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("CondicionPago", "cp_Nombre", eobj.Nombre, "IdCondicionPago", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaMedioPago_CondicionP(eobj, ListaMedioP_CondicionP)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If


                Case "Condici�n de Trabajo"
                    Dim nobj As New Negocio.CondTrabajo
                    Dim eobj As New Entidades.CondTrabajo
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("CondTrabajo", "ct_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaCondTrabajo(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("CondTrabajo", "ct_Nombre", eobj.Nombre, "IdCondTrabajo", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaCondTrabajo(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If


                Case "Estado Cargo Programado"
                    Dim nobj As New Negocio.EstadoCargoP
                    Dim eobj As New Entidades.EstadoCargoP
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)

                    If modo = 1 Then 'Nuevo
                        exito = nobj.InsertaEstadoCargoP(eobj)
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)

                        exito = nobj.ActualizaEstadoCargoP(eobj)
                    End If

                Case "Estado Civil"
                    Dim nobj As New Negocio.EstadoCivil
                    Dim eobj As New Entidades.EstadoCivil
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("EstadoCivil", "ec_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaEstadoCivil(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("EstadoCivil", "ec_Nombre", eobj.Nombre, "IdEstadoCivil", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaEstadoCivil(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If



                Case "Estado del Documento"
                    Dim nobj As New Negocio.EstadoDocumento
                    Dim eobj As New Entidades.EstadoDocumento
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("EstadoDocumento", "edoc_Nombre", eobj.Descripcion) <= 0 Then
                            exito = nobj.InsertaEstadoDocumento(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("EstadoDocumento", "edoc_Nombre", eobj.Descripcion, "IdEstadoDoc", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaEstadoDocumento(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Estado del Producto"
                    Dim nobj As New Negocio.EstadoProd
                    Dim eobj As New Entidades.EstadoProd
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("EstadoProd", "ep_Nombre", eobj.Descripcion) <= 0 Then
                            exito = nobj.InsertaEstadoProd(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("EstadoProd", "ep_Nombre", eobj.Descripcion, "IdEstadoProd", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaEstadoProd(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Fabricante"
                    If txt3.Text = "" Then
                        objScript.mostrarMsjAlerta(Me, "Ingrese c�digo fabricante.")
                        txt3.Focus()
                        Exit Sub
                        Exit Select
                    End If
                    Dim nobj As New Negocio.Fabricante
                    Dim eobj As New Entidades.Fabricante
                    eobj.NombreLargo = txt1.Text.Trim
                    eobj.NombreCorto = txt2.Text.Trim
                    eobj.fab_codigo = txt3.Text.Trim
                    eobj.IdPais = CStr(Me.dlPais.SelectedValue)
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    If modo = 1 Then 'Nuevo
                        exito = nobj.InsertaFabricante(eobj)
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        exito = nobj.ActualizaFabricante(eobj)
                    End If
                Case "Giro"
                    Dim nobj As New Negocio.Giro
                    Dim eobj As New Entidades.Giro
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Giro", "giro_Nombre", eobj.Descripcion) <= 0 Then
                            exito = nobj.InsertaGiro(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Giro", "giro_Nombre", eobj.Descripcion, "IdGiro", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaGiro(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    End If

                Case "Impuesto"
                    Dim eobj As New Entidades.Impuesto
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Abv = HttpUtility.HtmlDecode(txt2.Text)
                    eobj.Tasa = CDec(txt3.Text)
                    eobj.CuentaContable = txtcuentacontablemp.Text
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Impuesto", "imp_Nombre", eobj.Descripcion) <= 0 Then
                            exito = (New Negocio.Impuesto).InsertaImpuesto(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Impuesto", "imp_Nombre", eobj.Descripcion, "IdImpuesto", eobj.Id) <= 0 Then
                            exito = (New Negocio.Impuesto).ActualizaImpuesto(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    End If

                Case "Magnitud"
                    Dim nobj As New Negocio.Magnitud
                    Dim eobj As New Entidades.Magnitud
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    saveListaUMedida()
                    Me.listaUMedida = getListaUM()
                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Magnitud", "mag_Nombre", eobj.Descripcion) <= 0 Then
                            exito = nobj.InsertaMagnitud(eobj, listaUMedida)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Magnitud", "mag_Nombre", eobj.Descripcion, "IdMagnitud", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaMagnitudUnidad(eobj, Me.listaUMedida)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If

                Case "Estilo"
                    If txt3.Text = "" Then
                        objScript.mostrarMsjAlerta(Me, "Ingrese c�digo estilo.")
                        txt3.Focus()
                        Exit Sub
                        Exit Select
                    End If
                    Dim nobj As New Negocio.Estilo
                    Dim eobj As New Entidades.Estilo
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.Est_Codigo = txt3.Text.Trim
                    eobj.EstadoBit = CBool(obtenerEstado(rdbEstados.SelectedIndex))
                    If modo = 1 Then 'Nuevo
                        exito = nobj.InsertaEstilo(eobj)
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        exito = nobj.ActualizaEstilo(eobj)
                    End If
                Case "Marca"
                    If txt3.Text = "" Then
                        objScript.mostrarMsjAlerta(Me, "Ingrese c�digo marca.")
                        txt3.Focus()
                        Exit Sub
                        Exit Select
                    End If
                    Dim nobj As New Negocio.Marca
                    Dim eobj As New Entidades.Marca
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.mar_Codigo = txt3.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    If modo = 1 Then 'Nuevo
                        exito = nobj.InsertaMarca(eobj)
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        exito = nobj.ActualizaMarca(eobj)
                    End If
                    'Case "Material"
                Case "Formato"
                    If txt3.Text = "" Then
                        objScript.mostrarMsjAlerta(Me, "Ingrese c�digo formato.")
                        txt3.Focus()
                        Exit Sub
                        Exit Select
                    End If
                    Dim nobj As New Negocio.Material
                    Dim eobj As New Entidades.Material
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.For_Codigo = txt3.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    If modo = 1 Then 'Nuevo
                        exito = nobj.InsertaMaterial(eobj)
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        exito = nobj.ActualizaMaterial(eobj)
                    End If
                Case "Medio de Pago"
                    Dim nobj As New Negocio.MedioPago
                    Dim eobj As New Entidades.MedioPago
                    eobj.Nombre = txt1.Text.Trim
                    eobj.CodigoSunat = txt2.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    eobj.Principal = ckprincipalmp.Checked
                    eobj.IdMedioPagoInterfaz = CInt(dlinterfazmp.SelectedValue)
                    eobj.mp_CuentaContable = txtcuentacontablemp.Text
                    eobj.IdConceptoMovBanco = CInt(dlConceptoMovBanco.SelectedValue)
                    eobj.Abrev = Me.txtAbrev.Text
                    eobj.mp_autoMovbanco = Me.chkCheck1.Checked
                    Lista_TipoDocumento_MP = getLista_TipoDocumento_MP()


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("MedioPago", "mp_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertarMedioPago_ListaTipoDocumento(eobj, Lista_TipoDocumento_MP)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("MedioPago", "mp_Nombre", eobj.Nombre, "IdMedioPago", eobj.Id) <= 0 Then
                            exito = nobj.ActualizarMedioPago_ListaTipoDoc(eobj, Lista_TipoDocumento_MP)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Moneda"
                    Dim nobj As New Negocio.Moneda
                    Dim eobj As New Entidades.Moneda
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Simbolo = txt2.Text.Trim
                    eobj.CodigoSunat = txt3.Text.Trim
                    eobj.Base = obtenerBaseMoneda(rdbBase.SelectedIndex)
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("moneda", "mon_Nombre", eobj.Descripcion) <= 0 Then
                            exito = nobj.InsertaMoneda(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("moneda", "mon_Nombre", eobj.Descripcion, "IdMoneda", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaMoneda(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If


                Case "Motivo de Baja"
                    Dim nobj As New Negocio.MotivoBaja
                    Dim eobj As New Entidades.MotivoBaja
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    'mt_Nombre

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("MotivoBaja", "mt_Nombre", eobj.Descripcion) <= 0 Then
                            exito = nobj.InsertaMotivoBaja(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("MotivoBaja", "mt_Nombre", eobj.Descripcion, "IdMotivoBaja", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaMotivoBaja(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If


                Case "Motivo de Traslado"
                    Dim nobj As New Negocio.MotivoTraslado
                    Dim eobj As New Entidades.MotivoTraslado
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    saveListaMotivoTraslado_TipoOperacion()
                    eobj.DocumentoReferente = CBool(rdbDocumentoReferente.SelectedValue)
                    Me.ListaMotivoTipoOperacion = getListaMotivoTras_TipoOpera()


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("MotivoTraslado", "mt_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaMotivoTraslado_tipoOpera(eobj, ListaMotivoTipoOperacion)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("MotivoTraslado", "mt_Nombre", eobj.Nombre, "IdMotivoT", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaMotivoT_TipoOperacion(eobj, ListaMotivoTipoOperacion)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If



                Case "Movimiento Cuenta Tipo"
                    Dim nobj As New Negocio.MovCuentaTipo
                    Dim eobj As New Entidades.MovCuentaTipo
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    'mct_Nombre

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("MovCuentaTipo", "mct_Nombre", eobj.Descripcion) <= 0 Then
                            exito = nobj.InsertaMovCuentaTipo(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.IdMovCuentaTipo = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("MovCuentaTipo", "mct_Nombre", eobj.Descripcion, "IdMovCuentaTipo", eobj.IdMovCuentaTipo) <= 0 Then
                            exito = nobj.ActualizaMovCuentaTipo(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If

                Case "Nacionalidad"
                    Dim nobj As New Negocio.Nacionalidad
                    Dim eobj As New Entidades.Nacionalidad
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    eobj.IdPaisBase = chkCheck1.Checked

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Nacionalidad", "nac_Nombre", eobj.Descripcion) <= 0 Then
                            exito = nobj.InsertaNacionalidad(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Nacionalidad", "nac_Nombre", eobj.Descripcion, "IdNacionalidad", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaNacionalidad(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    End If

                Case "Perfil"
                    Dim nobj As New Negocio.Perfil
                    Dim eobj As New Entidades.Perfil
                    eobj.perm_Descripcion = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    eobj.EstadoMasivo = txt11.Text.Trim
                    'eobj.CuentaContabl = txt2.Text.Trim
                    eobj.IdPerfil = CInt(cboPerfilTV.SelectedValue)
                    ListaPerfilTipoPV = getListaPerfilTipoPV()
                    ListaPerfilMedioPago = getListaPerfilMedioPago()


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Perfil", "perf_Nombre", eobj.perm_Descripcion) <= 0 Then
                            exito = nobj.InsertaPerfilTipoPV(eobj, ListaPerfilTipoPV, ListaPerfilMedioPago)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.IdPerfil = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Perfil", "perf_Nombre", eobj.perm_Descripcion, "IdPerfil", eobj.IdPerfil) <= 0 Then
                            exito = nobj.PerfilTipoPVUpdate(eobj, ListaPerfilTipoPV, ListaPerfilMedioPago)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If


                Case "Profesi�n"
                    Dim nobj As New Negocio.Profesion
                    Dim eobj As New Entidades.Profesion
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    'prof_Nombre

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Profesion", "prof_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaProfesion(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Profesion", "prof_Nombre", eobj.Nombre, "IdProfesion", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaProfesion(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If

                Case "R�gimen"
                    Dim nobj As New Negocio.Regimen
                    Dim eobj As New Entidades.Regimen
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Regimen", "reg_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaRegimen(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Regimen", "reg_Nombre", eobj.Nombre, "reg_Id", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaRegimen(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If


                Case "R�gimen Operaci�n"
                    Dim nobj As New Negocio.RegimenOperacion
                    Dim eobj As New Entidades.RegimenOperacion
                    eobj.CuentaContable = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    eobj.IdRegimen = CInt(cboRegimen.SelectedValue)
                    eobj.IdTipoOperacion = CInt(cboTipoOperacion.SelectedValue)

                    If modo = 1 Then 'Nuevo
                        exito = nobj.InsertaRegimenOperacion(eobj)
                    Else 'Editar
                        'eobj.IdRegimen = CInt(txtCodigo.Text)
                        'eobj.IdTipoOperacion = CInt(cboTipoOperacion.SelectedValue)
                        exito = nobj.ActualizaRegimenOperacion(eobj)
                    End If
                Case "Rol"
                    Dim nobj As New Negocio.Rol
                    Dim eobj As New Entidades.Rol
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Rol", "rol_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaRol(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Rol", "rol_Nombre", eobj.Nombre, "IdRol", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaRol(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If


                Case "Sabor"
                    Dim nobj As New Negocio.Sabor
                    Dim eobj As New Entidades.Sabor
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    If modo = 1 Then 'Nuevo
                        exito = nobj.InsertaSabor(eobj)
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        exito = nobj.ActualizaSabor(eobj)
                    End If
                Case "Sector"
                    Dim objEntidadSector As New Entidades.Sector
                    Dim objNegocioSector As New Negocio.LN_Sector
                    With objEntidadSector
                        .nombre = Me.txt1.Text.Trim()
                        .descripcion = Me.txt2.Text.Trim()
                        .idControlador = Me.ddlControlador.SelectedValue
                        .estado = obtenerEstado(rdbEstados.SelectedIndex)
                    End With

                    If modo = 1 Then 'nuevo
                        exito = objNegocioSector.LN_InsertaSector(objEntidadSector)
                    Else 'Editar
                        objEntidadSector.idSector = CInt(Me.txtCodigo.Text.Trim())
                        exito = objNegocioSector.LN_ActualizarSector(objEntidadSector)
                    End If

                Case "Sector-Sublinea"
                    Dim objnegocioSector As New Negocio.LN_Sector
                    Dim objEntidadSector As New Entidades.Sector
                    Dim cadenaIds As String = String.Empty
                    With objEntidadSector

                        .idSector = Me.ddlSector_rel.SelectedValue
                        For Each row As GridViewRow In Me.gvSector_rel_Sublinea.Rows
                            cadenaIds = row.Cells(1).Text.Trim().ToString() & "," & cadenaIds
                        Next
                        .idSublineas = cadenaIds.Remove(cadenaIds.Length - 1)
                        If modo = 1 Then 'Nuevo
                            exito = objnegocioSector.LN_InsertaRelacion_Sector_Sublinea(objEntidadSector)
                            ViewState("Sublinea_tabla") = Nothing
                        End If
                    End With
                Case "Talla"
                    Dim nobj As New Negocio.Talla
                    Dim eobj As New Entidades.Talla
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    If modo = 1 Then 'Nuevo
                        exito = nobj.InsertaTalla(eobj)
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        exito = nobj.ActualizaTalla(eobj)
                    End If
                Case "Tama�o"
                    Dim nobj As New Negocio.Tamanio
                    Dim eobj As New Entidades.Tamanio
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    If modo = 1 Then 'Nuevo
                        exito = nobj.InsertaTamanio(eobj)
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        exito = nobj.ActualizaTamanio(eobj)
                    End If
                Case "Tarjeta"
                    Dim nobj As New Negocio.Tarjeta
                    Dim eobj As New Entidades.Tarjeta
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = CBool(obtenerEstado(rdbEstados.SelectedIndex))

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("tarjeta", "t_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaTarjeta(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.IdTarjeta = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("tarjeta", "t_Nombre", eobj.Nombre, "IdTarjeta", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaTarjeta(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If



                Case "Tipo de Agente"
                    Dim nobj As New Negocio.TipoAgente
                    Dim eobj As New Entidades.TipoAgente
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Tasa = CDec(IIf(IsNumeric(txt2.Text.Trim), txt2.Text.Trim, 0))
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    eobj.SujetoAPercepcion = chkCheck1.Checked


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoAgente", "ag_Nombre", eobj.Nombre) <= 0 Then

                            exito = nobj.InsertaTipoAgente(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.IdAgente = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoAgente", "ag_Nombre", eobj.Nombre, "IdAgente", eobj.IdAgente) <= 0 Then
                            exito = nobj.ActualizaTipoAgente(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    End If



                Case "Tipo de Comision"
                    Dim nobj As New Negocio.TipoComision
                    Dim eobj As New Entidades.TipoComision
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = CBool(obtenerEstado(rdbEstados.SelectedIndex))

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoComision", "tcom_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaTipoComision(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.IdTipoComision = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoComision", "tcom_Nombre", eobj.Nombre, "IdTipoComision", eobj.IdTipoComision) <= 0 Then
                            exito = nobj.ActualizaTipoComision(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If

                Case "Tipo de Concepto Banco"

                    Dim nTipoConceptoBanco As New Negocio.TipoConceptoBanco
                    Dim eTipoConceptoBanco As New Entidades.TipoConceptoBanco
                    eTipoConceptoBanco.Descripcion = txt1.Text.Trim
                    eTipoConceptoBanco.Factor = CInt(txt2.Text.Trim)
                    eTipoConceptoBanco.Estado = CBool(obtenerEstado(rdbEstados.SelectedIndex))


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoConceptoBanco", "tcba_Descripcion", eTipoConceptoBanco.Descripcion) <= 0 Then
                            exito = nTipoConceptoBanco.InsertaTipoConceptoBanco(eTipoConceptoBanco)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eTipoConceptoBanco.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoConceptoBanco", "tcba_Descripcion", eTipoConceptoBanco.Descripcion, "IdTipoConceptoBanco", eTipoConceptoBanco.Id) <= 0 Then
                            exito = nTipoConceptoBanco.ActualizaTipoConceptoBanco(eTipoConceptoBanco)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If

                Case "Tipo de Correo"
                    Dim nobj As New Negocio.TipoCorreo
                    Dim eobj As New Entidades.TipoCorreo
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoCorreo", "tcorr_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaTipoCorreo(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoCorreo", "tcorr_Nombre", eobj.Nombre, "IdTipoCorreo", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaTipoCorreo(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If



                Case "Tipo de Direcci�n"
                    Dim nobj As New Negocio.TipoDireccion
                    Dim eobj As New Entidades.TipoDireccion
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    If modo = 1 Then 'Nuevo
                        exito = nobj.InsertaTipoDireccion(eobj)
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        exito = nobj.ActualizaTipoDireccion(eobj)
                    End If
                Case "Tipo de Documento"
                    Dim nobj As New Negocio.TipoDocumento
                    Dim eobj As New Entidades.TipoDocumento
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.DescripcionCorto = txt2.Text.Trim
                    eobj.CodigoSunat = txt3.Text.Trim
                    'eobj.ExportarConta = obtenerEstado(rdbBase.SelectedIndex)
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    eobj.ExportarContaBool = chkCheck1.Checked
                    eobj.Externo = chkCheck2.Checked
                    eobj.Cancelable = chkCheck3.Checked
                    saveListaTipoOperacion()
                    saveListaTipoDocRef()
                    Me.listaTipoOperacion = GetListaTipoDocOpera()
                    Me.ListaTipoDocRef = GetListaTipoDocRef()


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoDocumento", "tdoc_NombreLargo", eobj.Descripcion) <= 0 Then
                            exito = nobj.InsertaTipoDocumento(eobj, listaTipoOperacion, ListaTipoDocRef)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoDocumento", "tdoc_NombreLargo", eobj.Descripcion, "IdTipoDocumento", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaTipoDocTipoOpera(eobj, listaTipoOperacion, ListaTipoDocRef)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Tipo de Documento Identidad"
                    Dim nobj As New Negocio.TipoDocumentoI
                    Dim eobj As New Entidades.TipoDocumentoI
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.CodigoSunat = txt3.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoDocumentoI", "tdoc_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaTipoDocumentoI(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoDocumentoI", "tdoc_Nombre", eobj.Nombre, "IdTipoDocumentoI", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaTipoDocumentoI(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If



                Case "Tipo de Existencia"
                    Dim nobj As New Negocio.TipoExistencia
                    Dim eobj As New Entidades.TipoExistencia
                    eobj.Nombre = txt1.Text.Trim
                    eobj.CodigoSunat = txt2.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    Lista_Linea_Area = getLista_Linea_Area()


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoExistencia", "tex_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaTipoExistencia(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        'exito = nobj.ActualizaTipoExistencia(eobj)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoExistencia", "tex_Nombre", eobj.Nombre, "IdTipoExistencia", eobj.Id) <= 0 Then
                            exito = nobj.UpdateTipoExistencia_Area(eobj, Lista_Linea_Area, CInt(hdd_tienerelacion.Value))
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Tipo de Gasto"
                    Dim nobj As New Negocio.TipoGasto
                    Dim eobj As New Entidades.TipoGasto
                    eobj.TG_Nombre = txt1.Text.Trim
                    eobj.Estado = CBool(obtenerEstado(rdbEstados.SelectedIndex))


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoGasto", "tg_Nombre", eobj.TG_Nombre) <= 0 Then
                            exito = nobj.InsertaTipoGasto(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.IdTipoGasto = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoGasto", "tg_Nombre", eobj.TG_Nombre, "IdTipoGasto", eobj.IdTipoGasto) <= 0 Then
                            exito = nobj.ActualizaTipoGasto(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Tipo de Movimiento"
                    Dim nobj As New Negocio.TipoMovimiento
                    Dim eobj As New Entidades.TipoMovimiento
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoMovimiento", "tm_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaTipoMovimiento(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoMovimiento", "tm_Nombre", eobj.Nombre, "IdTipoMovimiento", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaTipoMovimiento(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If

                Case "Tipo de Operaci�n"
                    Dim nobj As New Negocio.TipoOperacion
                    Dim eobj As New Entidades.TipoOperacion
                    eobj.Nombre = txt1.Text.Trim
                    eobj.CodigoSunat = txt2.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoOperacion", "top_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaTipoOperacion(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoOperacion", "top_Nombre", eobj.Nombre, "IdTipoOperacion", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaTipoOperacion(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If


                Case "Tipo de Precio de Importaci�n"
                    Dim nobj As New Negocio.TipoPrecioImportacion
                    Dim eobj As New Entidades.TipoPrecioImportacion
                    eobj.Nombre = txt1.Text.Trim
                    eobj.NomCorto = txt2.Text.Trim


                    If rdbEstados.SelectedIndex = 0 Then
                        eobj.Estado = True
                    Else
                        eobj.Estado = False
                    End If
                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoPrecioImportacion", "Nombre_TPImp", eobj.Nombre) <= 0 Then
                            exito = nobj.Insert(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoPrecioImportacion", "Nombre_TPImp", eobj.Nombre, "IdTPImp", eobj.Id) <= 0 Then
                            exito = nobj.Update(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If




                Case "Tipo de Precio de Venta"
                    Dim nobj As New Negocio.TipoPrecioV
                    Dim eobj As New Entidades.TipoPrecioV
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    eobj.TipoPvDefault = chkCheck1.Checked
                    eobj.AfectoCargoTarjeta = chkCheck2.Checked
                    eobj.IdTipoPvPredecesor = CInt(cboCombo1.SelectedValue)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoPrecioV", "pv_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaTipoPrecioV(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.IdTipoPv = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoPrecioV", "pv_Nombre", eobj.Nombre, "IdTipoPV", eobj.IdTipoPv) <= 0 Then
                            exito = nobj.ActualizaTipoPrecioV(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Tipo de Tel�fono"
                    Dim nobj As New Negocio.TipoTelefono
                    Dim eobj As New Entidades.TipoTelefono
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoTelefono", "ttel_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaTipoTelefono(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoTelefono", "ttel_Nombre", eobj.Nombre, "IdTipoTelefono", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaTipoTelefono(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Tipo de Tienda"
                    Dim nobj As New Negocio.TipoTienda
                    Dim eobj As New Entidades.TipoTienda
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("TipoTienda", "tipt_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaTipoTienda(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)

                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoTienda", "tipt_Nombre", eobj.Nombre, "IdTipoTienda", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaTipoTienda(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Tr�nsito"
                    If txt3.Text = "" Then
                        objScript.mostrarMsjAlerta(Me, "Ingrese c�digo tr�nsito.")
                        txt3.Focus()
                        Exit Sub
                        Exit Select
                    End If
                    Dim nobj As New Negocio.Transito
                    Dim eobj As New Entidades.Transito
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.Tran_Codigo = txt3.Text.Trim
                    eobj.Estado = CBool(obtenerEstado(rdbEstados.SelectedIndex))


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Transito", "Tran_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaTransito(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.IdTransito = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Transito", "Tran_Nombre", eobj.Nombre, "IdTransito", eobj.IdTransito) <= 0 Then
                            exito = nobj.ActualizaTransito(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If

                Case "Unidad de Medida"
                    Dim nobj As New Negocio.UnidadMedida
                    Dim eobj As New Entidades.UnidadMedida
                    eobj.Descripcion = txt1.Text.Trim
                    eobj.DescripcionCorto = txt2.Text.Trim
                    eobj.CodigoSunat = txt3.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)
                    'um_NombreLargo

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("UnidadMedida", "um_NombreLargo", eobj.Descripcion) <= 0 Then
                            exito = nobj.InsertaUnidadMedida(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("UnidadMedida", "um_NombreLargo", eobj.Descripcion, "IdUnidadMedida", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaUnidadMedida(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If

                    End If


                Case "V�a Tipo"
                    Dim nobj As New Negocio.ViaTipo
                    Dim eobj As New Entidades.ViaTipo
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("ViaTipo", "vt_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaViaTipo(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("ViaTipo", "vt_Nombre", eobj.Nombre, "IdViaTipo", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaViaTipo(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Zona Tipo"
                    Dim nobj As New Negocio.ZonaTipo
                    Dim eobj As New Entidades.ZonaTipo
                    eobj.Nombre = txt1.Text.Trim
                    eobj.Abv = txt2.Text.Trim
                    eobj.Estado = obtenerEstado(rdbEstados.SelectedIndex)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("ZonaTipo", "zt_Nombre", eobj.Nombre) <= 0 Then
                            exito = nobj.InsertaZonaTipo(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("ZonaTipo", "zt_Nombre", eobj.Nombre, "IdZonaTipo", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaZonaTipo(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If


                Case "Motivo Gasto"
                    Dim nobj As New Negocio.MotivoGasto
                    Dim eobj As New Entidades.MotivoGasto
                    eobj.Nombre_MG = txt1.Text.Trim
                    eobj.NombreCorto_MG = txt2.Text.Trim
                    eobj.Estado_MG = obtenerEstado(rdbEstados.SelectedIndex)
                    eobj.IdConcepto = CInt(dlConcepto.SelectedValue)


                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Motivo_Gasto", "Nombre_MG", eobj.Nombre_MG) <= 0 Then
                            exito = nobj.InsertaMotivoGasto(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Motivo_Gasto", "Nombre_MG", eobj.Nombre_MG, "IdMotGasto", eobj.Id) <= 0 Then
                            exito = nobj.ActualizaMotivoGasto(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "ya existe el nombre.")
                        End If


                    End If



                Case "Modelo"
                    If txt3.Text = "" Then
                        objScript.mostrarMsjAlerta(Me, "Ingrese c�digo modelo.")
                        txt3.Focus()
                        Exit Sub
                        Exit Select
                    End If
                    Dim nobj As New Negocio.Modelo
                    Dim eobj As New Entidades.Modelo
                    eobj.m_Nombre = txt1.Text.Trim
                    eobj.m_Codigo = txt3.Text.Trim

                    'eobj.Estado = CBool(IIf(rdbEstados.SelectedValue = "Activo", True, False))
                    eobj.Estado = CBool(obtenerEstado(rdbEstados.SelectedIndex))
                    If modo = 1 Then 'Nuevo
                        exito = nobj.InsertaModelo(eobj)
                    Else 'Editar
                        eobj.Id = CInt(txtCodigo.Text)

                        exito = nobj.ActualizaModelo(eobj)

                    End If

                Case "Alias"
                    Dim nobj As New Negocio.AliasMant
                    Dim eobj As New Entidades.AliasMant
                    eobj.Descripcion = HttpUtility.HtmlDecode(txt1.Text).Trim
                    If Not IsNumeric(txt2.Text.Trim) Then
                        Throw New Exception("Ingrese un codigo de persona valido.")
                    End If
                    eobj.IdPersona = CInt(txt2.Text.Trim)
                    eobj.Estado = CBool(obtenerEstadoBit(rdbEstados.SelectedIndex))

                    If modo = 1 Then 'Nuevo
                        If ValidoRep.ValidarExistenciaxTablax1Campo("Alias", "IdPersona", CStr(eobj.IdPersona)) <= 0 Then
                            exito = nobj.InsertaAlias(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "Ya existe el c�digo de la persona.")
                        End If
                    Else 'Editar
                        eobj.IdAlias = CInt(txtCodigo.Text)
                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Alias", "IdPersona", CStr(eobj.IdPersona), "IdAlias", eobj.IdAlias) <= 0 Then
                            exito = nobj.ActualizaAlias(eobj)
                        Else
                            objScript.mostrarMsjAlerta(Me, "Ya existe el c�digo de la persona.")
                        End If

                    End If

            End Select
        Catch ex As Exception
            msjError = ex.Message '"Problemas en la Operaci�n"
            If ckprincipalmp.Visible = True Then msjError = ex.Message
        End Try
        If exito = True Then   'imprimir mensaje            
            mostrarMensajeT(True, msjExito)
            objScript.mostrarMsjAlerta(Me, msjExito)
            hddModo.Value = "0"
            verFrmInicio()
        Else
            mostrarMensajeT(True, msjError)
            objScript.mostrarMsjAlerta(Me, msjError)
        End If
        'hddModo.Value = "0"
        'verFrmInicio()
    End Sub


    'CARGA EL COMBO DE BUSQUEDA DEPENDIENDO DE LA TABLA SELECCIONADA
    Private Sub cargarCombo()
        cmbBuscarPor.Items.Clear()
        cmbBuscarPor.Items.Add("C�digo")
        cmbBuscarPor.Items.Add("Descripci�n")
        Select Case hddTabla.Value
            Case "Banco"
                cmbBuscarPor.Items.Add("C�digo Sunat")
            Case "Medio de Pago"
                cmbBuscarPor.Items.Add("C�digo Sunat")
            Case "Moneda"
                cmbBuscarPor.Items.Add("C�digo Sunat")
            Case "Tipo de Documento"
                cmbBuscarPor.Items.Add("C�digo Sunat")
            Case "Tipo de Documento Identidad"
                cmbBuscarPor.Items.Add("C�digo Sunat")
            Case "Tipo de Existencia"
                cmbBuscarPor.Items.Add("C�digo Sunat")
            Case "Tipo de Operaci�n"
                cmbBuscarPor.Items.Add("C�digo Sunat")
            Case "Unidad de Medida"
                cmbBuscarPor.Items.Add("C�digo Sunat")
        End Select
        cmbBuscarPor.SelectedIndex = 1
    End Sub
    'LLENA LA GRILLA INICIO
    Private Sub llenarGrillaInicio(ByVal flag As Boolean)
        If flag = True Then
            Select Case hddTabla.Value
                Case "Area"
                    Dim objArea As New Negocio.Area
                    Dim objGrilla As New Grilla
                    objGrilla.llenarGrillaArea(DGV1)
                    DGV1.DataSource = objArea.SelectAllActivo
                    If dlunidadnegocio.Items.Count = 0 Then
                        cbo.LlenarCboCod_UniNeg(dlunidadnegocio, True) : dldptofuncional.Items.Insert(0, New ListItem("------", "00")) : dlsubarea1.Items.Insert(0, New ListItem("------", "00"))
                        dlsubarea2.Items.Insert(0, New ListItem("------", "00")) : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
                    End If
                Case "Banco"
                    Dim nBanco As New Negocio.Banco
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaBanco(DGV1)
                    DGV1.DataSource = nBanco.SelectAllActivo
                Case "Calidad"
                    Dim nobj As New Negocio.Calidad
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaCalidad(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo

                Case "Cargo"
                    Dim nCargo As New Negocio.Cargo
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaCargo(DGV1)
                    DGV1.DataSource = nCargo.SelectAllActivo
                Case "Categor�a de Empleado"
                    Dim nCat As New Negocio.CatEmpleado
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaCatEmpleado(DGV1)
                    DGV1.DataSource = nCat.SelectAllActivo
                Case "Chofer"
                    Dim obj As New Negocio.LNValorizadoCajas
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaChofer(DGV1)
                    DGV1.DataSource = obj.LN_ReturnDataTable("", "CHOFER_ACTIVO")
                Case "Color"
                    Dim nColor As New Negocio.Color
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaColor(DGV1)
                    DGV1.DataSource = nColor.SelectAllActivo
                Case "Concepto"
                    Dim nobj As New Negocio.Concepto
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaConcepto(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo

                    lblCheck1.Text = "Concepto Adelanto:"
                    lblCheck1Message.Text = "*Solo un Concepto puede tener este Valor activado"

                    lblCheck2.Text = "No afecto"
                    lblCheck2Message.Text = "I.G.V."

                    Me.lblcaja1.Text = "Detraccion( % ):"
                    Me.lblcaja2.Text = "Monto Min. Detraccion( S/. ):"


                Case "Condici�n Comercial"
                    Dim nobj As New Negocio.CondicionComercial
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaCondicionComercial(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Condici�n de Pago"
                    Dim nobj As New Negocio.CondicionPago
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaCondicionPago(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Condici�n de Trabajo"
                    Dim nobj As New Negocio.CondTrabajo
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaCondicionTrabajo(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Estado Cargo Programado"
                    Dim nobj As New Negocio.EstadoCargoP
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaEstadoCargoP(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Estado Civil"
                    Dim nobj As New Negocio.EstadoCivil
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaEstadoCivil(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Estado del Documento"
                    Dim nobj As New Negocio.EstadoDocumento
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaEstadoDocumento(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Estado del Producto"
                    Dim nobj As New Negocio.EstadoProd
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaEstadoProd(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Fabricante"
                    Dim nobj As New Negocio.Fabricante
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaFabricante(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Giro"
                    Dim nobj As New Negocio.Giro
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaGiro(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo

                Case "Impuesto"
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaImpuesto(DGV1)
                    DGV1.DataSource = (New Negocio.Impuesto).impuesto_select(IdEstado:="1")

                Case "Magnitud"
                    Dim nobj As New Negocio.Magnitud
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaMagnitud(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Estilo"
                    Dim nobj As New Negocio.Estilo
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaEstilo(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Marca"
                    Dim nobj As New Negocio.Marca
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaMarca(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                    'Case "Material"
                Case "Formato"
                    Dim nobj As New Negocio.Material
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaMaterial(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Medio de Pago"
                    Dim nobj As New Negocio.MedioPago
                    Dim nGrilla As New Grilla
                    If dlinterfazmp.Items.Count = 0 Then cbo.LlenarCboMedioPagoInterfaz(dlinterfazmp, True)
                    If dlTipoConceptoBanco.Items.Count = 0 Then
                        cbo.LlenarCboTipoConceptoBanco(dlTipoConceptoBanco)
                        If dlTipoConceptoBanco.Items.Count > 0 Then cbo.LlenarCboConceptoMovBancoxIdTipoConceptoBanco(dlConceptoMovBanco, CInt(dlTipoConceptoBanco.SelectedValue), True)
                    End If

                    Me.lblCheck1.Text = "Env�o Autom�tico:"
                    Me.chkCheck1.Text = "Movimiento Bancario"
                    nGrilla.llenarGrillaMedioPago(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Moneda"
                    Dim nobj As New Negocio.Moneda
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaMoneda(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Motivo de Baja"
                    Dim nobj As New Negocio.MotivoBaja
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaMotivoBaja(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Motivo de Traslado"

                    Dim nobj As New Negocio.MotivoTraslado
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaMotivoTraslado(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Movimiento Cuenta Tipo"
                    Dim nobj As New Negocio.MovCuentaTipo
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaMovCuentaTipo(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Nacionalidad"
                    Dim nobj As New Negocio.Nacionalidad
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaNacionalidad(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Perfil"
                    Dim nobj As New Negocio.Perfil
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaPerfil(DGV1)
                    DGV1.DataSource = nobj.SelectxIdPerfilxNombrexEstado(0, "", "1", "")
                Case "Profesi�n"
                    Dim nobj As New Negocio.Profesion
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaProfesion(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "R�gimen"
                    Dim nobj As New Negocio.Regimen
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaRegimen(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "R�gimen Operaci�n"
                    Dim nobj As New Negocio.RegimenOperacion
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaRegimenOperacion(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Rol"
                    Dim nobj As New Negocio.Rol
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaRol(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Sabor"
                    Dim nobj As New Negocio.Sabor
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaSabor(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Sector-Sublinea"
                    Dim nobj As New Negocio.LNValorizadoCajas
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaSector_Sublinea(DGV1)
                    DGV1.DataSource = nobj.LN_ReturnDataTable("", "LISTAR_SECTOR_SUBLINEA")
                Case "Sector"
                    Dim obj As New Negocio.LNValorizadoCajas
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaSector(DGV1)
                    DGV1.DataSource = obj.LN_ReturnDataTable("", "TBL_ALMACEN_SECTOR_MANTENIMIENTO")
                Case "Talla"
                    Dim nobj As New Negocio.Talla
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTalla(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tama�o"
                    Dim nobj As New Negocio.Tamanio
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTamanio(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tarjeta"
                    Dim nobj As New Negocio.Tarjeta
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTarjeta(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tipo de Agente"
                    Dim nobj As New Negocio.TipoAgente
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoAgente(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo

                    lblCheck1.Text = "Sujeto a Percepci�n:"

                Case "Tipo de Comision"
                    Dim nobj As New Negocio.TipoComision
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoComision(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tipo de Concepto Banco"
                    Dim objTipoConceptoBanco As New Negocio.TipoConceptoBanco
                    Dim objGrilla As New Grilla
                    objGrilla.llenarGrillaTipoConceptoBanco(DGV1)
                    DGV1.DataSource = objTipoConceptoBanco.SelectAllActivo
                Case "Tipo de Correo"
                    Dim nobj As New Negocio.TipoCorreo
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoCorreo(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tipo de Direcci�n"
                    Dim nobj As New Negocio.TipoDireccion
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoDireccion(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tipo de Documento"
                    Dim nobj As New Negocio.TipoDocumento
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoDocumento(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo

                    lblCheck1.Text = "Exportar Cont.:"
                    lblCheck2.Text = "Tipo Doc. Externo:"
                    lblCheck3.Text = "Tipo Doc. Cancelable:"

                Case "Tipo de Documento Identidad"
                    Dim nobj As New Negocio.TipoDocumentoI
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoDocumentoI(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tipo de Existencia"
                    Dim nobj As New Negocio.TipoExistencia
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoExistencia(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tipo de Gasto"
                    Dim nobj As New Negocio.TipoGasto
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoGasto(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tipo de Movimiento"
                    Dim nobj As New Negocio.TipoMovimiento
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoMovimiento(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tipo de Operaci�n"
                    Dim nobj As New Negocio.TipoOperacion
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoOperacion(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tipo de Precio de Importaci�n"
                    Dim nobj As New Negocio.TipoPrecioImportacion
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoPrecioImportacion(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tipo de Precio de Venta"
                    Dim nobj As New Negocio.TipoPrecioV
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoPrecioV(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo

                    lblCheck1.Text = "Predeterminado"
                    lblCheck1Message.Text = "*Solo un Tipo de Precio de Venta puede ser Predeterminado"
                    lblCombo1.Text = "Predecesor"
                    cbo.LlenarCboTipoPV(cboCombo1)
                    lblCheck2.Text = "Afecto Cargo Tarjeta"
                Case "Tipo de Tel�fono"
                    Dim nobj As New Negocio.TipoTelefono
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoTelefono(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tipo de Tienda"
                    Dim nobj As New Negocio.TipoTienda
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTipoTienda(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Tr�nsito"
                    Dim nobj As New Negocio.Transito
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaTransito(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Unidad de Medida"
                    Dim nobj As New Negocio.UnidadMedida
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaUnidadMedida(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "V�a Tipo"
                    Dim nobj As New Negocio.ViaTipo
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaViaTipo(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo
                Case "Zona Tipo"
                    Dim nobj As New Negocio.ZonaTipo
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaZonaTipo(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo

                Case "Motivo Gasto" 'Llama a la funcion para llenar la grilla de motivo gasto
                    Dim nobj As New Negocio.MotivoGasto
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaMotivoGasto(DGV1)
                    DGV1.DataSource = nobj.SelectAll 'Muestra todos los datos en la grilla

                Case "Modelo" 'Llama a la funcion para llenar la grilla de motivo gasto
                    Dim nobj As New Negocio.Modelo
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaModelo(DGV1)
                    DGV1.DataSource = nobj.SelectAll 'Muestra todos los datos en la grilla

                Case "Alias"
                    Dim nobj As New Negocio.AliasMant
                    Dim nGrilla As New Grilla
                    nGrilla.llenarGrillaAlias(DGV1)
                    DGV1.DataSource = nobj.SelectAllActivo

            End Select
            DGV1.DataBind()
            Me.setDataSource(DGV1.DataSource)
        End If
    End Sub
    'MUESTRA EL FRM PARA EL INGRESO DE UN REGISTRO EN UNA TABLA
    Private Sub verFrmNuevo()
        Me.Panel_Datos.Visible = True
        HabilitarBusqueda(False)
        HabilitarSeleccionTabla(False)
        verBotones(True)
        verEstado(True, False, 0)
        verCodigo(True, "C�digo: ", True, "", 50)
        Select Case hddTabla.Value
            Case "Area"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "Descripci�n Breve: ", False, "", 20)
                'lblcentrocosto.Visible = True : pnlcentroCosto.Visible = True
                tr_centrocosto.Visible = True
            Case "Banco"
                verTxt1(True, "C�digo Sunat: ", False, "", 3)
                verTxt2(True, "Descripci�n: ", False, "", 50)
                verTxtAbv(True, "Abreviatura", False, "", 20)

                Me.Panel_Banco.Visible = True
            Case "C�lculo"
                verTxt1(True, "Nombre: ", False, "", 30)
            Case "Cargo"
                verTxt1(True, "Descripci�n: ", False, "", 50)
            Case "Categor�a de Empleado"
                verTxt1(True, "Descripci�n: ", False, "", 50)
            Case "Chofer"
                tr_Chofer.Visible = True
                verTxt1(True, "Licencia: ", False, "", 100)
                verTxt2(True, "Categoria: ", False, "", 100)
            Case "Color"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                verTxt2(True, "Abv.: ", False, "", 10)
                verTxt3(True, "C�d. Color: ", False, "", 2)
            Case "Concepto"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "Cuenta Contable: ", False, "", 12)
                'CboTipoGasto.Visible = True
                'lblTipoGasto.Visible = True
                tr_TipoGasto.Visible = True
                Panel_TipoDocumento.Visible = True

                tr_check1.Visible = True : tr_check2.Visible = True

                Me.tr_caja1.Visible = True : Me.txtcaja1.Text = "" : Me.txtcaja1.Attributes.Add("onKeyPress", "return validarNumeroPuntoPositivo('event')")
                Me.tr_caja2.Visible = True : Me.txtcaja2.Text = "" : Me.txtcaja2.Attributes.Add("onKeyPress", "return validarNumeroPuntoPositivo('event')")

            Case "Condici�n Comercial"
                verTxt1(True, "Descripci�n", False, "", 50)
                'lblTipoDocumento.Visible = True
                'CboTipoDocumento.Visible = True
                tr_TipoDocumento.Visible = True
            Case "Condici�n de Pago"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                Panel_CondicionPago.Visible = True
            Case "Condici�n de Trabajo"
                verTxt1(True, "Descripci�n: ", False, "", 20)
            Case "Estado Cargo Programado"
                verTxt1(True, "Descripci�n: ", False, "", 20)
            Case "Estado Civil"
                verTxt1(True, "Descripci�n: ", False, "", 20)
            Case "Estado del Documento"
                verTxt1(True, "Descripci�n: ", False, "", 25)
            Case "Estado del Producto"
                verTxt1(True, "Descripci�n: ", False, "", 20)
            Case "Fabricante"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "Abv.: ", False, "", 20)
                verTxt3(True, "C�digo Fab.: ", False, "", 20)
                lbl4.Visible = True
                'dlPais.Visible = True
                'dlPais.Enabled = True
                tr_Pais.Visible = True
            Case "Giro"
                verTxt1(True, "Descripci�n: ", False, "", 50)

            Case "Impuesto"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "Abv.: ", False, "", 10)
                verTxt3(True, "Tasa: ", False, "", 5)
                tr_CtaContablemp.Visible = True

            Case "Magnitud"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                verTxt2(True, "Abv.: ", False, "", 10)
                Me.Panel_MU.Visible = True
            Case "Estilo"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                verTxt2(True, "Abv.: ", False, "", 10)
                verTxt3(True, "C�d. Estilo: ", False, "", 2)

            Case "Marca"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                verTxt2(True, "Abv.: ", False, "", 10)
                verTxt3(True, "C�d. Marca: ", False, "", 2)

                'Case "Material"
            Case "Formato"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                verTxt2(True, "Abv.: ", False, "", 10)
                verTxt3(True, "C�d. Formato: ", False, "", 2)

            Case "Medio de Pago"
                verTxt1(True, "Descripci�n: ", False, "", 100)
                verTxt2(True, "C�d. Sunat: ", False, "", 3)
                verTxtAbv(True, "Abrev:", False, "", 20)

                pnltipodocumetomp.Visible = True
                'lblinterfazmp.Visible = True
                'dlinterfazmp.Visible = True
                tr_interfazmp.Visible = True
                'lblprincipalmp.Visible = True
                'ckprincipalmp.Visible = True
                tr_principalmp.Visible = True
                'lblcuentaContablemp.Visible = True
                'txtcuentacontablemp.Visible = True
                tr_CtaContablemp.Visible = True
                'dlTipoConceptoBanco.Visible = True : lblTipoConceptoBanco.Visible = True
                tr_TipoConceptoBanco.Visible = True
                'dlConceptoMovBanco.Visible = True : lblConceptoMovBanco.Visible = True
                tr_ConceptoMovBanco.Visible = True

                tr_check1.Visible = True


            Case "Moneda"
                verTxt1(True, "Descripci�n: ", False, "", 20)
                verTxt2(True, "S�mbolo: ", False, "", 4)
                verTxt3(True, "C�d. Sunat: ", False, "", 2)
                verRdbBase(True, False, 1)
            Case "Motivo de Baja"
                verTxt1(True, "Descripci�n: ", False, "", 20)
            Case "Motivo de Traslado"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verRdbDocumentoReferente(True, False, "1")
                Me.Panel_MotivoTraslado.Visible = True
            Case "Movimiento Cuenta Tipo"
                verTxt1(True, "Descripci�n: ", False, "", 20)
            Case "Nacionalidad"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                tr_check1.Visible = True
                lblCheck1.Text = "Nacional:"
            Case "Perfil"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt11(True, "Estado Masivo:", False, "", 1)
                PanelPerfilTipoPV.Visible = True
                PanelPerfilMedioPago.Visible = True
            Case "Profesi�n"
                verTxt1(True, "Descripci�n: ", False, "", 50)
            Case "R�gimen"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                verTxt2(True, "Abv.: ", False, "", 10)
            Case "R�gimen Operaci�n"
                verTxt1(True, "Cuenta Contable: ", False, "", 50)
                'verTxt2(True, "Cuenta Contable: ", False, "", 12)
                'cboRegimen.Visible = True
                'lblRegimen.Visible = True
                'cboRegimen.Enabled = True
                tr_Regimen.Visible = True
                'CboTipoDocumento.Visible = True
                'lblTipoDocumento.Visible = True
                'CboTipoDocumento.Enabled = True
                tr_TipoDocumento.Visible = True

            Case "Rol"
                verTxt1(True, "Descripci�n: ", False, "", 30)
            Case "Sabor"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                verTxt2(True, "Abv.: ", False, "", 10)
            Case "Sector"
                verTxt1(True, "Nombre: ", False, "", 40)
                verTxt2(True, "Descripci�n: ", False, "", 150)
                tr_controlador.Visible = True
            Case "Sector-Sublinea"
                Me.ddlSubLinea_rel.SelectedValue = "-1"
                gvSector_rel_Sublinea.DataSource = Nothing
                gvSector_rel_Sublinea.DataBind()
                cargarSectores(ddlSector_rel)
                cargarLinea(ddlLinea_rel)
                tr_rel_Sector.Visible = True
                tr_rel_Sector_sublinea.Visible = True
                tr_gvSector_rel_Sublinea.Visible = True
                tr_controlador.Visible = False
                'qwe
            Case "Talla"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                verTxt2(True, "Abv.: ", False, "", 10)
            Case "Tama�o"
                verTxt1(True, "Descripci�n: ", False, "", 30)
            Case "Tarjeta"
                verTxt1(True, "Nombre: ", False, "", 30)
            Case "Tipo de Agente"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "Tasa: ", False, "", 50)
                tr_check1.Visible = True
            Case "Tipo de Comision"
                verTxt1(True, "Nombre: ", False, "", 30)
            Case "Tipo de Concepto Banco"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "Factor: ", False, "", 20)
            Case "Tipo de Correo"
                verTxt1(True, "Descripci�n: ", False, "", 20)
            Case "Tipo de Direcci�n"
                verTxt1(True, "Descripci�n: ", False, "", 30)
            Case "Tipo de Documento"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "Abv.: ", False, "", 30)
                verTxt3(True, "C�d. Sunat: ", False, "", 3)
                'verRdbBase(True, False, 1, "Exportar a Contab.: ")
                tr_check1.Visible = True
                tr_check2.Visible = True
                tr_check3.Visible = True

                Me.Panel_TipoDocOpera.Visible = True
                Me.Panel_TipoDocRef.Visible = True
            Case "Tipo de Documento Identidad"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "Abv.: ", False, "", 15)
                verTxt3(True, "C�d. Sunat: ", False, "", 2)
            Case "Tipo de Existencia"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "C�d. Sunat: ", False, "", 3)
            Case "Tipo de Gasto"
                verTxt1(True, "Nombre: ", False, "", 30)
            Case "Tipo de Movimiento"
                verTxt1(True, "Descripci�n: ", False, "", 20)
            Case "Tipo de Operaci�n"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "C�d. Sunat: ", False, "", 3)
            Case "Tipo de Precio de Importaci�n"
                verTxt1(True, "Nombre: ", False, "", 50)
                verTxt2(True, "Nombre Corto: ", False, "", 20)
            Case "Tipo de Precio de Venta"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                verTxt2(True, "Abv.: ", False, "", 10)
                tr_check1.Visible = True
                tr_combo1.Visible = True
                tr_check2.Visible = True
            Case "Tipo de Tel�fono"
                verTxt1(True, "Descripci�n: ", False, "", 20)
            Case "Tipo de Tienda"
                verTxt1(True, "Descripci�n: ", False, "", 30)
            Case "Tr�nsito"
                verTxt1(True, "Nombre: ", False, "", 30)
                verTxt2(True, "Abv.: ", False, "", 10)
                verTxt3(True, "C�d. Tr�nsito: ", False, "", 2)

            Case "Unidad de Medida"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                verTxt2(True, "Abv.: ", False, "", 5)
                verTxt3(True, "C�d. SUNAT: ", False, "", 3)
            Case "V�a Tipo"
                verTxt1(True, "Descripci�n: ", False, "", 30)
                verTxt2(True, "Abv.: ", False, "", 5)
            Case "Zona Tipo"
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "Abv.: ", False, "", 10)
            Case "Motivo Gasto" 'Asigno el tama�o de caracteres a ingresar en cada caja texto
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "Abv.: ", False, "", 20)
                'llbConcepto.Visible = True
                'dlConcepto.Visible = True
                'dlConcepto.Enabled = True
                tr_Concepto.Visible = True

            Case "Modelo" 'Asigno el tama�o de caracteres a ingresar en cada caja texto
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt3(True, "C�d. Modelo: ", False, "", 3)

            Case "Calidad" 'Asigno el tama�o de caracteres a ingresar en cada caja texto
                verTxt1(True, "Descripci�n: ", False, "", 50)
                verTxt2(True, "Abv.: ", False, "", 20)
                verTxt3(True, "C�d. Calidad: ", False, "", 2)

            Case "Alias"
                verTxt1(True, "Observaci�n: ", False, "", 250)
                verTxt2(True, "C�d. Persona: ", False, "", 9)
        End Select
    End Sub
    'FUNCION LA CUAL SE ENCARGA DEL FILTRADO DE DATOS
    Protected Sub btnFiltrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFiltrar.Click
        buscarxTexto()
    End Sub
    Private Sub buscarxTexto()
        Try
            Dim nGrilla As New Grilla
            Dim textoB As String = txttextoBusqueda.Text.Trim
            Select Case hddTabla.Value
                Case "Area"
                    Dim objArea As New Negocio.Area
                    nGrilla.llenarGrillaArea(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = objArea.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = objArea.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = objArea.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = objArea.SelectInactivoxNombre(textoB)
                            End Select
                    End Select

                Case "Banco"
                    Dim nBanco As New Negocio.Banco
                    nGrilla.llenarGrillaBanco(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nBanco.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nBanco.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nBanco.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nBanco.SelectInactivoxNombre(textoB)
                            End Select
                        Case 2 'codigo sunat
                            DGV1.DataSource = nBanco.SelectxCodSunat(textoB)
                    End Select

                Case "Calidad"
                    Dim nobj As New Negocio.Calidad
                    nGrilla.llenarGrillaCalidad(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Cargo"
                    Dim nCargo As New Negocio.Cargo
                    nGrilla.llenarGrillaCargo(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nCargo.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nCargo.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nCargo.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nCargo.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Categor�a de Empleado"
                    Dim nCat As New Negocio.CatEmpleado
                    nGrilla.llenarGrillaCatEmpleado(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nCat.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nCat.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nCat.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nCat.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Color"
                    Dim nColor As New Negocio.Color
                    nGrilla.llenarGrillaColor(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nColor.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nColor.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nColor.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nColor.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Concepto"
                    Dim nobj As New Negocio.Concepto
                    nGrilla.llenarGrillaConcepto(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Condici�n Comercial"
                    Dim nobj As New Negocio.CondicionComercial
                    nGrilla.llenarGrillaCondicionComercial(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'Descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Condici�n de Pago"
                    Dim nobj As New Negocio.CondicionPago
                    nGrilla.llenarGrillaCondicionPago(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Condici�n de Trabajo"
                    Dim nobj As New Negocio.CondTrabajo
                    nGrilla.llenarGrillaCondicionTrabajo(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Estado Cargo Programado"
                    Dim nobj As New Negocio.EstadoCargoP
                    nGrilla.llenarGrillaEstadoCargoP(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Estado Civil"
                    Dim nobj As New Negocio.EstadoCivil
                    nGrilla.llenarGrillaEstadoCivil(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Estado del Documento"
                    Dim nobj As New Negocio.EstadoDocumento
                    nGrilla.llenarGrillaEstadoDocumento(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Estado del Producto"
                    Dim nobj As New Negocio.EstadoProd
                    nGrilla.llenarGrillaEstadoProd(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Fabricante"
                    Dim nobj As New Negocio.Fabricante
                    nGrilla.llenarGrillaFabricante(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Giro"
                    Dim nobj As New Negocio.Giro
                    nGrilla.llenarGrillaGiro(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select

                Case "Impuesto"
                    nGrilla.llenarGrillaImpuesto(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = (New Negocio.Impuesto).impuesto_select(id)
                        Case 1 'descripcion                            
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 '                       +++     Todos x Nombre
                                    DGV1.DataSource = (New Negocio.Impuesto).impuesto_select(Descripcion:=textoB)
                                Case 1 '                       +++      Activo 
                                    DGV1.DataSource = (New Negocio.Impuesto).impuesto_select(IdEstado:="1", Descripcion:=textoB)
                                Case 2 '                       +++      Inactivo
                                    DGV1.DataSource = (New Negocio.Impuesto).impuesto_select(IdEstado:="0", Descripcion:=textoB)
                            End Select
                    End Select

                Case "Magnitud"
                    Dim nobj As New Negocio.Magnitud
                    nGrilla.llenarGrillaMagnitud(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Estilo"
                    Dim nobj As New Negocio.Estilo
                    nGrilla.llenarGrillaEstilo(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Marca"
                    Dim nobj As New Negocio.Marca
                    nGrilla.llenarGrillaMarca(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                    'Case "Material"
                Case "Formato"
                    Dim nobj As New Negocio.Material
                    nGrilla.llenarGrillaMaterial(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Medio de Pago"
                    Dim nobj As New Negocio.MedioPago
                    nGrilla.llenarGrillaMedioPago(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                        Case 2 'cod sunat
                            DGV1.DataSource = nobj.SelectxCodSunat(textoB)
                    End Select
                Case "Moneda"
                    Dim nobj As New Negocio.Moneda
                    nGrilla.llenarGrillaMoneda(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                        Case 2 'cod sunat
                            DGV1.DataSource = nobj.SelectxCodSunat(textoB)
                    End Select
                Case "Motivo de Baja"
                    Dim nobj As New Negocio.MotivoBaja
                    nGrilla.llenarGrillaMotivoBaja(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Motivo de Traslado"
                    Dim nobj As New Negocio.MotivoTraslado
                    nGrilla.llenarGrillaMotivoTraslado(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Movimiento Cuenta Tipo"
                    Dim nobj As New Negocio.MovCuentaTipo
                    nGrilla.llenarGrillaMovCuentaTipo(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Nacionalidad"
                    Dim nobj As New Negocio.Nacionalidad
                    nGrilla.llenarGrillaNacionalidad(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Perfil"
                    Dim nobj As New Negocio.Perfil
                    nGrilla.llenarGrillaPerfil(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxIdPerfilxNombrexEstado(id, "", "", "")
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectxIdPerfilxNombrexEstado(0, Me.txttextoBusqueda.Text, "", "")
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectxIdPerfilxNombrexEstado(0, Me.txttextoBusqueda.Text, "1", "1")
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectxIdPerfilxNombrexEstado(0, Me.txttextoBusqueda.Text, "0", "0")
                            End Select
                    End Select
                Case "Profesi�n"
                    Dim nobj As New Negocio.Profesion
                    nGrilla.llenarGrillaProfesion(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "R�gimen"
                    Dim nobj As New Negocio.Regimen
                    nGrilla.llenarGrillaRegimen(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "R�gimen Operaci�n"
                    Dim nobj As New Negocio.RegimenOperacion
                    nGrilla.llenarGrillaRegimenOperacion(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxIdRegimen(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Rol"
                    Dim nobj As New Negocio.Rol
                    nGrilla.llenarGrillaRol(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Sabor"
                    Dim nobj As New Negocio.Sabor
                    nGrilla.llenarGrillaSabor(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Sector"
                    Dim obj As New Negocio.LNValorizadoCajas
                    nGrilla.llenarGrillaSector(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = obj.LN_ReturnDataTable(id, "TBL_ALMACEN_SECTOR_BUSQUEDA_X_ID")
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = obj.LN_ReturnDataTable(textoB, "TBL_ALMACEN_SECTOR_BUSQUEDA_X_NOMBRE_TODOS")
                                Case 1 'activos
                                    DGV1.DataSource = obj.LN_ReturnDataTable(textoB, "TBL_ALMACEN_SECTOR_BUSQUEDA_X_NOMBRE_ACTIVOS")
                                Case 2 'inactivos
                                    DGV1.DataSource = obj.LN_ReturnDataTable(textoB, "TBL_ALMACEN_SECTOR_BUSQUEDA_X_NOMBRE_INACTIVOS")
                            End Select
                    End Select
                Case "Talla"
                    Dim nobj As New Negocio.Talla
                    nGrilla.llenarGrillaTalla(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Tama�o"
                    Dim nobj As New Negocio.Tamanio
                    nGrilla.llenarGrillaTamanio(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Tarjeta"
                    Dim nobj As New Negocio.Tarjeta
                    nGrilla.llenarGrillaTarjeta(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Tipo de Agente"
                    Dim nobj As New Negocio.TipoAgente
                    nGrilla.llenarGrillaTipoAgente(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Tipo de Comision"
                    Dim nobj As New Negocio.TipoComision
                    nGrilla.llenarGrillaTipoComision(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select

                Case "Tipo de Concepto Banco"
                    Dim objTipoConceptoBanco As New Negocio.TipoConceptoBanco
                    nGrilla.llenarGrillaTipoConceptoBanco(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = objTipoConceptoBanco.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = objTipoConceptoBanco.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = objTipoConceptoBanco.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = objTipoConceptoBanco.SelectInactivoxNombre(textoB)
                            End Select
                    End Select

                Case "Tipo de Correo"
                    Dim nobj As New Negocio.TipoCorreo
                    nGrilla.llenarGrillaTipoCorreo(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Tipo de Direcci�n"
                    Dim nobj As New Negocio.TipoDireccion
                    nGrilla.llenarGrillaTipoDireccion(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Tipo de Documento"
                    Dim nobj As New Negocio.TipoDocumento
                    nGrilla.llenarGrillaTipoDocumento(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            Dim lista As New List(Of Entidades.TipoDocumento)
                            lista.Add(nobj.SelectxId(id))
                            DGV1.DataSource = lista
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                        Case 2
                            DGV1.DataSource = nobj.SelectxCodSunat(textoB)
                    End Select
                Case "Tipo de Documento Identidad"
                    Dim nobj As New Negocio.TipoDocumentoI
                    nGrilla.llenarGrillaTipoDocumentoI(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                        Case 2
                            DGV1.DataSource = nobj.SelectxCodSunat(textoB)
                    End Select

                Case "Tipo de Gasto"
                    Dim nobj As New Negocio.TipoGasto
                    nGrilla.llenarGrillaTipoGasto(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Tipo de Existencia"
                    Dim nobj As New Negocio.TipoExistencia
                    nGrilla.llenarGrillaTipoExistencia(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                        Case 2
                            DGV1.DataSource = nobj.SelectxCodSunat(textoB)
                    End Select

                Case "Tipo de Movimiento"
                    Dim nobj As New Negocio.TipoMovimiento
                    nGrilla.llenarGrillaTipoMovimiento(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Tipo de Operaci�n"
                    Dim nobj As New Negocio.TipoOperacion
                    nGrilla.llenarGrillaTipoOperacion(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                        Case 2
                            DGV1.DataSource = nobj.SelectxCodSunat(textoB)
                    End Select
                Case "Tipo de Precio de Importaci�n"
                    Dim nobj As New Negocio.TipoPrecioImportacion
                    nGrilla.llenarGrillaTipoPrecioImportacion(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Tipo de Precio de Venta"
                    Dim nobj As New Negocio.TipoPrecioV
                    nGrilla.llenarGrillaTipoPrecioV(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Tipo de Tel�fono"
                    Dim nobj As New Negocio.TipoTelefono
                    nGrilla.llenarGrillaTipoTelefono(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Tipo de Tienda"
                    Dim nobj As New Negocio.TipoTienda
                    nGrilla.llenarGrillaTipoTienda(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Tr�nsito"
                    Dim nobj As New Negocio.Transito
                    nGrilla.llenarGrillaTransito(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Unidad de Medida"
                    Dim nobj As New Negocio.UnidadMedida
                    nGrilla.llenarGrillaUnidadMedida(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                        Case 2
                            DGV1.DataSource = nobj.SelectxCodSunat(textoB)
                    End Select
                Case "V�a Tipo"
                    Dim nobj As New Negocio.ViaTipo
                    nGrilla.llenarGrillaViaTipo(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select
                Case "Zona Tipo"
                    Dim nobj As New Negocio.ZonaTipo
                    nGrilla.llenarGrillaZonaTipo(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select
                    End Select


                Case "Motivo Gasto"
                    Dim nobj As New Negocio.MotivoGasto
                    nGrilla.llenarGrillaMotivoGasto(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectAllxNombreActivo(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectAllxNombreInactivo(textoB)
                            End Select
                    End Select

                    'Para las busquedas
                Case "Modelo"
                    Dim nobj As New Negocio.Modelo
                    nGrilla.llenarGrillaModelo(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion
                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectAllxNombreActivo(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectAllxNombreInactivo(textoB)
                            End Select
                    End Select

                Case "Alias"
                    Dim nobj As New Negocio.AliasMant
                    nGrilla.llenarGrillaAlias(DGV1)
                    Select Case cmbBuscarPor.SelectedIndex
                        Case 0 'id
                            Dim id As Integer = 0
                            If IsNumeric(textoB) Then id = CInt(textoB)
                            DGV1.DataSource = nobj.SelectxId(id)
                        Case 1 'descripcion

                            Select Case rdbEstadosB.SelectedIndex
                                Case 0 'todos
                                    DGV1.DataSource = nobj.SelectAllxNombre(textoB)
                                Case 1 'activos
                                    DGV1.DataSource = nobj.SelectActivoxNombre(textoB)
                                Case 2 'inactivos
                                    DGV1.DataSource = nobj.SelectInactivoxNombre(textoB)
                            End Select

                    End Select

            End Select

            DGV1.DataBind()
            Me.setDataSource(DGV1.DataSource)
            mostrarMensajeT(False, "")
            mostrarMensajeMotivoTraslado(False, "")
            If DGV1.Rows.Count = 0 Then
                mostrarMensajeB(True, "No se hallaron registros")
            Else
                mostrarMensajeB(False, "")
            End If
        Catch ex As Exception
            mostrarMensajeB(True, ex.Message)
        End Try
    End Sub
    Protected Sub DGV1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV1.PageIndexChanging
        DGV1.PageIndex = e.NewPageIndex
        DGV1.DataSource = Me.getDataSource
        DGV1.DataBind()
    End Sub

    Private Sub cargarControladores(ByVal combo As DropDownList)
        Dim objSector As New Negocio.LNValorizadoCajas
        Dim dt As New DataTable
        dt = objSector.LN_ReturnDataTable("", "CONTROLADOR")
        ddlControlador.DataSource = dt
        ddlControlador.DataTextField = "nombre"
        ddlControlador.DataValueField = "IdPersona"
        ddlControlador.DataBind()
        ddlControlador.Items.Insert(0, New ListItem("Seleccione un Controlador", "-1"))
    End Sub

    Private Sub cargarChofer(ByVal combo As DropDownList)
        Dim objSector As New Negocio.LNValorizadoCajas
        Dim dt As New DataTable
        dt = objSector.LN_ReturnDataTable("", "TBL_CHOFER")
        ddlChofer.DataSource = dt
        ddlChofer.DataTextField = "Nombre"
        ddlChofer.DataValueField = "IdPersona"
        ddlChofer.DataBind()
        ddlChofer.Items.Insert(0, New ListItem("Seleccione un Chofer", "-1"))
    End Sub

    Private Sub cargarSectores(ByVal combo As DropDownList)
        Dim objSector As New Negocio.LNValorizadoCajas
        Dim dt As New DataTable
        dt = objSector.LN_ReturnDataTable("", "TBL_ALMACEN_SECTOR")
        ddlSector_rel.DataSource = dt
        ddlSector_rel.DataTextField = "nom_sector"
        ddlSector_rel.DataValueField = "id_sector"
        ddlSector_rel.DataBind()
        ddlSector_rel.Items.Insert(0, New ListItem("Seleccione un Sector", "-1"))
    End Sub

    Private Sub cargarLinea(ByVal combo As DropDownList)
        Dim objSector As New Negocio.LNValorizadoCajas
        Dim dt As New DataTable
        dt = objSector.LN_ReturnDataTable("", "LINEA_MERCADERIA")
        ddlLinea_rel.DataSource = dt
        ddlLinea_rel.DataTextField = "lin_Nombre"
        ddlLinea_rel.DataValueField = "IdLinea"
        ddlLinea_rel.DataBind()
        ddlLinea_rel.Items.Insert(0, New ListItem("Seleccione una Linea", "-1"))
    End Sub

    Private Sub cargarSubLinea(ByVal combo As DropDownList)
        Dim objSector As New Negocio.LNValorizadoCajas
        Dim dt As New DataTable
        dt = objSector.LN_ReturnDataTable(Me.ddlLinea_rel.SelectedValue, "SUBLINEA")
        ddlSubLinea_rel.DataSource = dt
        ddlSubLinea_rel.DataTextField = "sl_Nombre"
        ddlSubLinea_rel.DataValueField = "IdSubLInea"
        ddlSubLinea_rel.DataBind()
        ddlSubLinea_rel.Items.Insert(0, New ListItem("---", "-1"))
    End Sub

    Private Sub cargarDatosUnidMedida(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim UnidMedida As New Negocio.UnidadMedida
        Dim nComboBox As New Negocio.ComboBox
        nComboBox.llenarComboBoxUMedida(combo)
        combo.DataSource = UnidMedida.SelectAllActivo
        combo.DataBind()
    End Sub
    Private Function getListaUM() As List(Of Entidades.MagnitudUnidad)
        Return CType(Session.Item("listaUM"), List(Of Entidades.MagnitudUnidad))
    End Function

    Private Sub saveListaUMedida()
        Me.listaUMedida = getListaUM()
        For i As Integer = 0 To DGV_UM.Rows.Count - 1
            Me.listaUMedida(i).Equivalencia = CDec(CType(DGV_UM.Rows(i).Cells(2).FindControl("txtEquivalencia_UM"), TextBox).Text)
            Me.listaUMedida(i).UnidadPrincipal = CBool(CType(Me.DGV_UM.Rows(i).Cells(3).FindControl("chb_umPrincipal"), CheckBox).Checked)
        Next
        setListaUM(listaUMedida)
    End Sub

    Private Sub mostrarMensaje(ByVal isVisible As Boolean, ByVal texto As String)
        lblMensaje.Visible = isVisible
        lblMensaje.Text = texto
    End Sub
    Private Sub setListaUM(ByVal lista As List(Of Entidades.MagnitudUnidad))
        Session.Remove("listaUM")
        Session.Add("listaUM", lista)
    End Sub

    Private Function getCentroCosto() As String
        Dim ID As String = String.Empty
        ID = dlunidadnegocio.SelectedValue + dldptofuncional.SelectedValue
        ID &= dlsubarea1.SelectedValue + dlsubarea2.SelectedValue + dlsubarea3.SelectedValue
        Return ID
    End Function

    Private Sub cargarDatosGrillaUMedida()

        saveListaUMedida()
        Dim objMagnitudUM As New Entidades.MagnitudUnidad

        With objMagnitudUM

            .idUnidadMedida = CInt(Me.M_cmbUMedida.SelectedValue)
            .NombreCortoUM = Me.M_cmbUMedida.SelectedItem.ToString
            .UnidadPrincipal = False
            .Equivalencia = 0

        End With

        Me.listaUMedida.Add(objMagnitudUM)

        setListaUM(listaUMedida)
        DGV_UM.DataSource = Me.listaUMedida
        DGV_UM.DataBind()
    End Sub


    Protected Sub btnAgregarUM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarUM.Click
        cargarDatosGrillaUMedida()
    End Sub

    Private Sub quitarUMedida_Magnitud()

        Try
            saveListaUMedida()

            Me.listaUMedida = getListaUM()
            Me.listaUMedida.RemoveAt(Me.DGV_UM.SelectedIndex)

            setListaUM(Me.listaUMedida)

            DGV_UM.DataSource = Me.listaUMedida
            DGV_UM.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminaci�n del Regitro.")
        End Try

    End Sub

    Protected Sub DGV_UM_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_UM.SelectedIndexChanged
        quitarUMedida_Magnitud()
    End Sub
    Private Function Guardar_MU() As Entidades.MagnitudUnidad
        Dim LGuarMU As New List(Of Entidades.MagnitudUnidad)
        Dim objG_mu As New Entidades.MagnitudUnidad

        'GUARDAR LOS DATOS DEL DGV A LA TABLA MAGNITUD UNIDAD
        If Me.DGV_UM.Rows.Count <> 0 Then

            Me.listaUMedida = Me.getListaUM

            For i As Integer = 0 To Me.listaUMedida.Count - 1
                With objG_mu

                    .idUnidadMedida = Me.listaUMedida.Item(i).idUnidadMedida
                    .Equivalencia = Me.listaUMedida.Item(i).Equivalencia
                    .UnidadPrincipal = Me.listaUMedida.Item(i).UnidadPrincipal
                End With
                LGuarMU.Add(objG_mu)
            Next

            Dim obj As New Negocio.MagnitudUnidad

        End If

        'GUARDAR EL DATO DEL TXT1 (CODIGO MAGNITUD) A LA TABLA MAGNITUD UNIDAD
        objG_mu.idMagnitud = CInt(IIf(IsNumeric(txtCodigo.Text) = True, txtCodigo.Text, 0))
        Return objG_mu
    End Function
    '************************* Tabla: Tipo Documento  ***************************************
    Private Sub CargarDatosCmbTipoOpera(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Try
            Dim nTipoOpera As New Negocio.TipoOperacion
            Dim nComboBox As New Negocio.ComboBox
            nComboBox.llenarCboTipoOperacion(combo)
            combo.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub CargarDatosCmbTipoDocRef(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Try
            Dim nComboBox As New Combo
            nComboBox.LlenarCboTipoDocumentoIndep(combo)
            combo.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function GetListaTipoDocOpera() As List(Of Entidades.TipoDocumento_TipoOperacion)
        Return CType(Session.Item("listaTipoOpera"), List(Of Entidades.TipoDocumento_TipoOperacion))
    End Function
    Private Function GetListaTipoDocRef() As List(Of Entidades.TipoDocRef_TipoDoc_View)
        Return CType(Session.Item("ListaTipoDocRef"), List(Of Entidades.TipoDocRef_TipoDoc_View))
    End Function
    Private Sub setlistTipoOperacion(ByVal lista As List(Of Entidades.TipoDocumento_TipoOperacion))
        Session.Remove("listaTipoOpera")
        Session.Add("listaTipoOpera", lista)
    End Sub
    Private Sub setlistTipoDocRef(ByVal lista As List(Of Entidades.TipoDocRef_TipoDoc_View))
        Session.Remove("ListaTipoDocRef")
        Session.Add("ListaTipoDocRef", lista)
    End Sub

    Private Sub saveListaTipoOperacion()
        Me.listaTipoOperacion = GetListaTipoDocOpera()
        setlistTipoOperacion(listaTipoOperacion)
    End Sub

    Private Sub saveListaTipoDocRef()
        Me.ListaTipoDocRef = GetListaTipoDocRef()
        setlistTipoDocRef(ListaTipoDocRef)
    End Sub

    Private Sub CargarDatosGrillaTipoOpera()
        saveListaTipoOperacion()
        Dim objTipoDocOpera As New Entidades.TipoDocumento_TipoOperacion
        With objTipoDocOpera
            .IdTipoOperacion = CInt(Me.cmb_TipoOpera.SelectedValue)
            .Nombre = Me.cmb_TipoOpera.SelectedItem.ToString
        End With
        Me.listaTipoOperacion.Add(objTipoDocOpera)
        setlistTipoOperacion(listaTipoOperacion)
        DGV_TipoOpera.DataSource = Me.listaTipoOperacion
        DGV_TipoOpera.DataBind()
    End Sub
    Private Sub CargarDatosGrillaTipoDocRef()
        saveListaTipoDocRef()
        Dim objTipoDocRef As New Entidades.TipoDocRef_TipoDoc_View
        With objTipoDocRef
            .IdTipoDocRef = CInt(Me.cmb_TipoDocRef.SelectedValue)
            .Nombre = Me.cmb_TipoDocRef.SelectedItem.ToString
        End With
        Me.ListaTipoDocRef.Add(objTipoDocRef)
        setlistTipoDocRef(ListaTipoDocRef)
        DGV_TipoDocRef.DataSource = Me.ListaTipoDocRef
        DGV_TipoDocRef.DataBind()
    End Sub
    Protected Sub btnAgregarTipoOpera_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarTipoOpera.Click
        CargarDatosGrillaTipoOpera()
    End Sub

    Protected Sub btnAgregarTipoDocRef_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarTipoDocRef.Click
        CargarDatosGrillaTipoDocRef()
    End Sub
    Private Sub quitarTipoDocOpera()
        Try
            saveListaTipoOperacion()
            Me.listaTipoOperacion = GetListaTipoDocOpera()
            Me.listaTipoOperacion.RemoveAt(Me.DGV_TipoOpera.SelectedIndex)
            setlistTipoOperacion(Me.listaTipoOperacion)
            DGV_TipoOpera.DataSource = Me.listaTipoOperacion
            DGV_TipoOpera.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminaci�n del Registro.")
        End Try
    End Sub
    Private Sub quitarTipoDocRef()
        Try
            saveListaTipoDocRef()
            Me.ListaTipoDocRef = GetListaTipoDocRef()
            Me.ListaTipoDocRef.RemoveAt(Me.DGV_TipoDocRef.SelectedIndex)
            setlistTipoDocRef(Me.ListaTipoDocRef)
            DGV_TipoDocRef.DataSource = Me.ListaTipoDocRef
            DGV_TipoDocRef.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminaci�n del Registro.")
        End Try
    End Sub

    Protected Sub DGV_TipoOpera_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_TipoOpera.SelectedIndexChanged
        quitarTipoDocOpera()
    End Sub
    Protected Sub DGV_TipoDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_TipoDocRef.SelectedIndexChanged
        quitarTipoDocRef()
    End Sub
    '********************************************  Banco  -  Oficina  *****************************
    Private Sub setListaBancoOficina(ByVal lista As List(Of Entidades.Oficina))
        Session.Remove("ListaBancoOficina")
        Session.Add("ListaBancoOficina", lista)
    End Sub
    Private Function getListaBancoOficina() As List(Of Entidades.Oficina)
        Return CType(Session.Item("ListaBancoOficina"), List(Of Entidades.Oficina))

    End Function
    Private Sub saveListaBancoOficina()
        Me.ListaBancoOficina = getListaBancoOficina()
        For i As Integer = 0 To DGV_Oficina.Rows.Count - 1
            Me.ListaBancoOficina(i).Descripcion = CStr(CType(DGV_Oficina.Rows(i).Cells(1).FindControl("txtBoxDescrOficina"), TextBox).Text)
            Dim EST As Boolean = CBool(CType(Me.DGV_Oficina.Rows(i).Cells(2).FindControl("chb_EstadoOficina"), CheckBox).Checked)
            Me.ListaBancoOficina(i).Estado = CStr(IIf(EST = True, "1", "0"))
        Next
        setListaBancoOficina(ListaBancoOficina)
    End Sub
    Private Sub CargarDatosGrillaBancoOficina()
        saveListaBancoOficina()
        Dim objBancoOficina As New Entidades.Oficina
        With objBancoOficina
            .Id = 0
            .Descripcion = ""
            .Estado = "1"
        End With
        Me.ListaBancoOficina.Add(objBancoOficina)
        setListaBancoOficina(ListaBancoOficina)
        DGV_Oficina.DataSource = Me.ListaBancoOficina
        DGV_Oficina.DataBind()
    End Sub
    Protected Sub btnAgregarTipoOpera0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarOficina.Click
        CargarDatosGrillaBancoOficina()
    End Sub
    Private Function ObtenerListaOficinaFrmGrilla() As List(Of Entidades.Oficina)
        Dim lista As New List(Of Entidades.Oficina)
        For i As Integer = 0 To DGV_Oficina.Rows.Count - 1
            With DGV_Oficina.Rows(i)
                Dim estado As String = "0"
                If CType(.Cells(2).FindControl("chb_EstadoOficina"), CheckBox).Checked Then
                    estado = "1"
                End If
                Dim txtNombre As TextBox = (CType(.Cells(1).FindControl("txtBoxDescrOficina"), TextBox))
                Dim obj As New Entidades.Oficina(CInt(ViewState.Item("Id")), CStr(txtNombre.Text), estado)
                obj.Id = CInt(DGV_Oficina.Rows(i).Cells(3).Text)
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Private Sub quitarRegistroOficina()
        Dim objUtil As New Negocio.Util
        Try
            '****************** validamos que no tenga registros relacionados
            If (objUtil.ValidarxDosParametros("LetraCambio", "IdOficina", DGV_Oficina.SelectedRow.Cells(3).Text, "IdBanco", txtCodigo.Text)) > 0 Then
                objScript.mostrarMsjAlerta(Me, "La cuenta no puede ser eliminada por que posee movimientos relacionados.")
                Return
            End If

            '**************** quitamos de la lista
            Dim lista As List(Of Entidades.Oficina) = ObtenerListaOficinaFrmGrilla()

            lista.RemoveAt(DGV_Oficina.SelectedIndex)
            DGV_Oficina.DataSource = lista
            DGV_Oficina.DataBind()
            setListaBancoOficina(lista)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminaci�n del registro.")
        End Try
    End Sub
    Protected Sub DGV_Oficina_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_Oficina.SelectedIndexChanged
        quitarRegistroOficina()
    End Sub
    '**********************************************  Motivo Traslado - Tipo Operacion  *********************************************************************
    'Cargar Combo
    Private Sub cargarDatosMotivoTipoOpera(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim MtipoOpera As New Negocio.TipoOperacion
        Dim nComboBox As New Negocio.ComboBox
        nComboBox.llenarCboTipoOperacion(combo)
        combo.DataSource = MtipoOpera.SelectAllActivo
        combo.DataBind()
    End Sub
    'Llenar la grilla  
    Private Sub setListaMotivoTras_TipoOpera(ByVal lista As List(Of Entidades.MotivoT_TipoOperacion))

        Session.Remove("ListaMotivoTipoOperacion")
        Session.Add("ListaMotivoTipoOperacion", lista)

    End Sub
    Private Function getListaMotivoTras_TipoOpera() As List(Of Entidades.MotivoT_TipoOperacion)
        Return CType(Session.Item("ListaMotivoTipoOperacion"), List(Of Entidades.MotivoT_TipoOperacion))
    End Function
    Private Sub saveListaMotivoTraslado_TipoOperacion()
        Me.ListaMotivoTipoOperacion = getListaMotivoTras_TipoOpera()
        setListaMotivoTras_TipoOpera(ListaMotivoTipoOperacion)
    End Sub
    Private Sub CargarDatosGrillaMotivoTraslado_TipoOperacion()
        saveListaMotivoTraslado_TipoOperacion()
        Dim objMotivoTraslado As New Entidades.MotivoT_TipoOperacion
        With objMotivoTraslado
            .idTipoOperac = CInt(Me.cmb_MotivoTipoOperacion.SelectedValue)
            .NombreTipoOperacion = Me.cmb_MotivoTipoOperacion.SelectedItem.ToString
        End With
        Me.ListaMotivoTipoOperacion.Add(objMotivoTraslado)
        setListaMotivoTras_TipoOpera(ListaMotivoTipoOperacion)
        DGV_MotivoTipoOperacion.DataSource = Me.ListaMotivoTipoOperacion
        DGV_MotivoTipoOperacion.DataBind()
    End Sub

    Protected Sub btnAgregarMotivoTraslado_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarMotivoTipoOperacion.Click
        CargarDatosGrillaMotivoTraslado_TipoOperacion()
    End Sub
    'Quitar
    Private Sub quitarTipoOperacion()
        Try
            saveListaMotivoTraslado_TipoOperacion()

            Me.ListaMotivoTipoOperacion = getListaMotivoTras_TipoOpera()
            Me.ListaMotivoTipoOperacion.RemoveAt(Me.DGV_MotivoTipoOperacion.SelectedIndex)

            setListaMotivoTras_TipoOpera(Me.ListaMotivoTipoOperacion)

            DGV_MotivoTipoOperacion.DataSource = Me.ListaMotivoTipoOperacion
            DGV_MotivoTipoOperacion.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminaci�n del Regitro.")
        End Try

    End Sub
    Protected Sub DGV_MotivoTraslado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_MotivoTipoOperacion.SelectedIndexChanged
        quitarTipoOperacion()
    End Sub
    '****************** Medio Pago - Condici�n de Pago
    'Cargar el Combo
    Private Sub CargardatosMedioPago(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim nCombo As New Combo
        nCombo.LlenarCboMedioPago(combo)
    End Sub
    'Llenar la grilla
    Private Sub setListaMedioP_CondicionP(ByVal lista As List(Of Entidades.MedioPago_CondicionPago))
        Session.Remove("ListaMedioP_CondicionP")
        Session.Add("ListaMedioP_CondicionP", lista)
    End Sub
    Private Function getListaMedioP_CondicionP() As List(Of Entidades.MedioPago_CondicionPago)
        Return CType(Session.Item("ListaMedioP_CondicionP"), List(Of Entidades.MedioPago_CondicionPago))
    End Function
    Private Sub saveListaMedioP_CondicionP()
        ListaMedioP_CondicionP = getListaMedioP_CondicionP()
        setListaMedioP_CondicionP(ListaMedioP_CondicionP)
    End Sub
    Private Sub CargarGrillaMedioPago()
        saveListaMedioP_CondicionP()
        Dim objMedioPago As New Entidades.MedioPago_CondicionPago
        With objMedioPago
            .IdMedioPago = CInt(cbo_CondicionPago.SelectedValue)
            .NomMedioPago = cbo_CondicionPago.SelectedItem.ToString
        End With
        'ListaMedioP_CondicionP = New List(Of Entidades.MedioPago)
        ListaMedioP_CondicionP.Add(objMedioPago)
        setListaMedioP_CondicionP(ListaMedioP_CondicionP)
        DGV_CondicionPago.DataSource = ListaMedioP_CondicionP
        DGV_CondicionPago.DataBind()
    End Sub
    Protected Sub BtonA�adir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtonA�adir.Click
        CargarGrillaMedioPago()
    End Sub
    'quitar
    Private Sub quitarmedioPago()
        saveListaMedioP_CondicionP()
        ListaMedioP_CondicionP = getListaMedioP_CondicionP()
        ListaMedioP_CondicionP.RemoveAt(DGV_CondicionPago.SelectedIndex)
        setListaMedioP_CondicionP(ListaMedioP_CondicionP)
        DGV_CondicionPago.DataSource = ListaMedioP_CondicionP
        DGV_CondicionPago.DataBind()
    End Sub
    Protected Sub DGV_CondicionPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_CondicionPago.SelectedIndexChanged
        quitarmedioPago()
    End Sub
    '************************************** Conceto - Tipo de Documento *************************************************
    'Private Sub setListaCpto_TipoDoc(ByVal lista As List(Of Entidades.Concepto_TipoDocumento))
    '    Session.Remove("ListaCpto_TipoDoc")
    '    Session.Add("ListaCpto_TipoDoc", lista)
    'End Sub
    'Private Function getListaCpto_TipoDoc1() As List(Of Entidades.Concepto_TipoDocumento)
    '    Return CType(Session.Item("ListaCpto_TipoDoc"), List(Of Entidades.Concepto_TipoDocumento))
    'End Function
    'Private Sub saveListaCpto_TipoDoc()
    '    ListaCpto_TipoDoc = getListaCpto_TipoDoc1()
    '    setListaCpto_TipoDoc(ListaCpto_TipoDoc)
    'End Sub
    'Cargar el combo Tipo de Documento
    Private Sub CargarDatosTipoDocumento(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim nCombo As New Combo
        nCombo.LlenarComboTipoDocumento(combo)
    End Sub
    'Cargar el combo Tipo de Gasto
    Private Sub CargarDatosTipoGasto(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim nCombo As New Combo
        nCombo.LlenarCboTipoGasto(combo, True)
    End Sub
    Private Sub CargarDatosRegimen(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim nCombo As New Combo
        nCombo.llenarCboRegimen(combo, False)
    End Sub
    Private Sub CargarDatosTipoOperacion(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim nCombo As New Combo
        nCombo.llenarCboTipoOperacion(combo, False)
    End Sub
    'Carga todos los conceptos para registrar un motivo de gasto
    Private Sub CargarDatosConcepto(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim nCombo As New Combo
        nCombo.LlenarCboConcepto(combo, True)
    End Sub
    Private Sub CargarDatosPais(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim nCombo As New Combo
        nCombo.LlenarCboPais(combo, True)
    End Sub
    Private Sub llenargrillaConceptoTipoDocumento(ByVal gv As GridView)
        ListaCpto_TipoDoc = getListaCpto_TipoDoc()

        Dim obj As New Entidades.Concepto_TipoDocumento
        With obj
            .IdTipoDocumento = CInt(cbo_TipoDocumento.SelectedValue)
            .DescTipoDocumento = cbo_TipoDocumento.SelectedItem.Text
            .IdMoneda = 0
            If DGV_TipoDocumento.Rows.Count = 0 Then
                .NombreMoneda = (New Negocio.Concepto).listarMoneda()
            Else
                .NombreMoneda = getDropMoneda()
            End If
            .CtDoc_Valor = 0
            .fletexkilofijo = "NULL"
            .con_TipoCalculo = "NULL"
        End With
        ListaCpto_TipoDoc.Add(obj)
        DGV_TipoDocumento.DataSource = ListaCpto_TipoDoc
        DGV_TipoDocumento.DataBind()
    End Sub
    Function getDropMoneda() As List(Of Entidades.Concepto_TipoDocumento)
        Dim cboMoneda As DropDownList = CType(DGV_TipoDocumento.Rows(0).Cells(2).FindControl("dlmoneda"), DropDownList)
        Dim listaConcepto_TipoDocumento As New List(Of Entidades.Concepto_TipoDocumento)
        For x As Integer = 0 To cboMoneda.Items.Count - 1
            Dim objTConcepto_TipoDocumento As New Entidades.Concepto_TipoDocumento
            With objTConcepto_TipoDocumento
                .IdMoneda = CInt(cboMoneda.Items(x).Value)
                .SimboloMoneda = cboMoneda.Items(x).Text
            End With
            listaConcepto_TipoDocumento.Add(objTConcepto_TipoDocumento)
        Next
        Return listaConcepto_TipoDocumento
    End Function

    'Llenar la grilla 
    Private Function getListaCpto_TipoDoc() As List(Of Entidades.Concepto_TipoDocumento)
        ListaCpto_TipoDoc = New List(Of Entidades.Concepto_TipoDocumento)
        For Each fila As GridViewRow In DGV_TipoDocumento.Rows
            Dim obj As New Entidades.Concepto_TipoDocumento
            With obj
                .IdTipoDocumento = CInt(CType(fila.Cells(1).FindControl("HddIdTipoDocumento"), HiddenField).Value)
                .DescTipoDocumento = CStr(CType(fila.Cells(1).FindControl("lblNomTipoDoc"), Label).Text)
                .NombreMoneda = getDropMoneda()
                .IdMoneda = CInt(CType(fila.Cells(2).FindControl("dlmoneda"), DropDownList).SelectedValue)
                .CtDoc_Valor = CDec(CType(fila.Cells(3).FindControl("txtCtDocValor"), TextBox).Text)
                .fletexkilofijo = CStr(CType(fila.Cells(4).FindControl("rb_fletexkilofijo"), RadioButtonList).SelectedValue)
                .con_TipoCalculo = CStr(CType(fila.Cells(5).FindControl("rb_con_TipoCalculo"), RadioButtonList).SelectedValue)
                .con_ZonaUbigeo = CType(fila.Cells(6).FindControl("ckZona"), CheckBox).Checked
            End With
            ListaCpto_TipoDoc.Add(obj)
        Next
        Return ListaCpto_TipoDoc
    End Function
    Protected Sub BotonA�adirTipoDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BotonA�adirTipoDoc.Click
        Try
            llenargrillaConceptoTipoDocumento(DGV_TipoDocumento)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    'quitar
    Private Sub quitarRegistroTipopDoc(ByVal idempresa As Integer, ByVal idrol As Integer, ByVal index As Integer)
        Try
            ListaCpto_TipoDoc = getListaCpto_TipoDoc()
            ListaCpto_TipoDoc.RemoveAt(index)
            DGV_TipoDocumento.DataSource = ListaCpto_TipoDoc
            DGV_TipoDocumento.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub DGV_TipoDocumento_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_TipoDocumento.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim moneda As DropDownList = CType(e.Row.Cells(2).FindControl("dlmoneda"), DropDownList)
                Dim fletexkilofijo As RadioButtonList = CType(e.Row.Cells(4).FindControl("rb_fletexkilofijo"), RadioButtonList)
                Dim con_TipoCalculo As RadioButtonList = CType(e.Row.Cells(5).FindControl("rb_con_TipoCalculo"), RadioButtonList)
                If ListaCpto_TipoDoc(e.Row.RowIndex).IdMoneda <> 0 Then
                    moneda.SelectedValue = CStr(ListaCpto_TipoDoc(e.Row.RowIndex).IdMoneda)
                End If
                fletexkilofijo.SelectedValue = CStr(ListaCpto_TipoDoc(e.Row.RowIndex).fletexkilofijo)
                con_TipoCalculo.SelectedValue = CStr(ListaCpto_TipoDoc(e.Row.RowIndex).con_TipoCalculo)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_TipoDocumento.SelectedIndexChanged
        Dim codigoCpto% = CInt((IIf(Me.txtCodigo.Text = "", 0, Me.txtCodigo.Text)))
        Me.quitarRegistroTipopDoc(CInt(CType(DGV_TipoDocumento.SelectedRow.Cells(1).FindControl("HddIdTipoDocumento"),  _
                                 HiddenField).Value), CInt(codigoCpto), DGV_TipoDocumento.SelectedIndex)
    End Sub
    '************************ TIENDA CONCEPTO ********************************************

    Public Function getListaTiendaConcepto() As List(Of Entidades.Tienda_Concepto)
        ListaTiendaConcepto = New List(Of Entidades.Tienda_Concepto)
        For Each fila As GridViewRow In Me.gvConpTienda.Rows
            Dim obj As New Entidades.Tienda_Concepto
            With obj
                .IdTienda = CInt(CType(fila.Cells(1).FindControl("dlTiendaTie"), DropDownList).SelectedValue)
                .ObjTienda = getDropTiendaGrillaTienda()
                .IdMoneda = CInt(CType(fila.Cells(2).FindControl("dlMonedaTie"), DropDownList).SelectedValue)
                .ObjMoneda = getDropMonedaGrillaTienda()
                .tie_monto = CInt(CType(fila.Cells(3).FindControl("tbmonto"), TextBox).Text)
                .tie_estado = CBool(CType(fila.Cells(4).FindControl("ckestado"), CheckBox).Checked)
            End With
            ListaTiendaConcepto.Add(obj)
        Next
        Return ListaTiendaConcepto
    End Function

    Private Function getDropMonedaGrillaTienda() As List(Of Entidades.Moneda)
        Dim cboMoneda As DropDownList = CType(gvConpTienda.Rows(0).Cells(2).FindControl("dlMonedaTie"), DropDownList)
        Dim Lista As New List(Of Entidades.Moneda)
        For x As Integer = 0 To cboMoneda.Items.Count - 1
            Dim obj As New Entidades.Moneda
            With obj
                .Id = CInt(cboMoneda.Items(x).Value)
                .Simbolo = cboMoneda.Items(x).Text
            End With
            Lista.Add(obj)
        Next
        Return Lista
    End Function

    Private Function getDropTiendaGrillaTienda() As List(Of Entidades.Tienda)
        Dim cboMoneda As DropDownList = CType(gvConpTienda.Rows(0).Cells(1).FindControl("dlTiendaTie"), DropDownList)
        Dim Lista As New List(Of Entidades.Tienda)
        For x As Integer = 0 To cboMoneda.Items.Count - 1
            Dim obj As New Entidades.Tienda
            With obj
                .Id = CInt(cboMoneda.Items(x).Value)
                .Nombre = cboMoneda.Items(x).Text
            End With
            Lista.Add(obj)
        Next
        Return Lista
    End Function

    Private Sub llenarGrillaTiendaConcepto()
        ListaTiendaConcepto = getListaTiendaConcepto()

        Dim obj As New Entidades.Tienda_Concepto
        With obj
            .IdTienda = 0
            If gvConpTienda.Rows.Count = 0 Then
                .ObjMoneda = (New Negocio.Moneda).SelectCbo()
                .ObjTienda = (New Negocio.Tienda).SelectCbo()
            Else
                .ObjMoneda = getDropMonedaGrillaTienda()
                .ObjTienda = getDropTiendaGrillaTienda()
            End If
            .tie_monto = 0
            .tie_estado = True
        End With
        ListaTiendaConcepto.Add(obj)
        gvConpTienda.DataSource = ListaTiendaConcepto
        gvConpTienda.DataBind()
    End Sub

    Protected Sub btaddTiendaConcepto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btaddTiendaConcepto.Click
        Try
            llenarGrillaTiendaConcepto()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvConpTienda_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvConpTienda.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If ListaTiendaConcepto(e.Row.RowIndex).IdTienda <> 0 Then
                    Dim tienda As DropDownList = CType(e.Row.Cells(1).FindControl("dlTiendaTie"), DropDownList)
                    tienda.SelectedValue = CStr(ListaTiendaConcepto(e.Row.RowIndex).IdTienda)
                End If
                If ListaTiendaConcepto(e.Row.RowIndex).IdMoneda <> 0 Then
                    Dim moneda As DropDownList = CType(e.Row.Cells(2).FindControl("dlMonedaTie"), DropDownList)
                    moneda.SelectedValue = CStr(ListaTiendaConcepto(e.Row.RowIndex).IdMoneda)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvConpTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvConpTienda.SelectedIndexChanged
        Try
            ListaTiendaConcepto = getListaTiendaConcepto()
            ListaTiendaConcepto.RemoveAt(gvConpTienda.SelectedIndex)
            gvConpTienda.DataSource = ListaTiendaConcepto
            gvConpTienda.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#Region "Perfil - Perfil Tipo Precio de Venta"

    '************************ Perfil - Perfil Tipo Precio de Venta ****************************************************
    Private Sub CargarDatosPrecioventa(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim nCombo As New Combo
        nCombo.LlenarCboTipoPV1(combo)
    End Sub
    'Llenar la grilla 
    Private Function getListaPerfilTipoPV() As List(Of Entidades.PerfilTipoPV)
        'aqui lo distancio para q me carge a la grilla
        ListaPerfilTipoPV = New List(Of Entidades.PerfilTipoPV)
        For Each fila As GridViewRow In Me.DGVPerfilTipoPV.Rows
            Dim obj As New Entidades.PerfilTipoPV
            obj.IdTipoPV = CInt(CType(fila.Cells(1).FindControl("HddIdPerfil"), HiddenField).Value)
            obj.NombreTipoPV = CStr(CType(fila.Cells(1).FindControl("lblNombrePerfil"), Label).Text)
            Dim Estado As Boolean = CType(fila.Cells(2).FindControl("ChboxEstado"), CheckBox).Checked
            'Dim Estado As Boolean = CType(fila.Cells(2).FindControl("Chboxpermisomasivo"), CheckBox).Checked
            obj.PerPV_Estado = CChar(IIf(Estado = True, "1", "0"))
            'Dim EST As Boolean = CBool(CType(Me.DGV_Oficina.Rows(i).Cells(2).FindControl("chb_EstadoOficina"), CheckBox).Checked)
            'Me.ListaBancoOficina(i).Estado = CStr(IIf(EST = True, "1", "0"))
            ListaPerfilTipoPV.Add(obj)
        Next
        Return ListaPerfilTipoPV
    End Function

    'Boton A�ADIR
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        CargarDatosGrillaPerfilTipoPV(DGVPerfilTipoPV)
    End Sub
    Private Sub CargarDatosGrillaPerfilTipoPV(ByVal gv As GridView)
        Dim obj As New Entidades.PerfilTipoPV
        'Como ya esta instanciado, ahora qiero q me carge + de una fila
        ListaPerfilTipoPV = getListaPerfilTipoPV()
        obj.IdTipoPV = CInt(cboPerfilTV.SelectedValue)
        obj.NombreTipoPV = (cboPerfilTV.SelectedItem.ToString)
        obj.PerPV_Estado = CChar("1")
        ListaPerfilTipoPV.Add(obj)
        DGVPerfilTipoPV.DataSource = ListaPerfilTipoPV
        DGVPerfilTipoPV.DataBind()
    End Sub
    'quitar
    Private Sub quitarRegistroPerfilTipoPV(ByVal IdPerfil As Integer, ByVal IdTipoPV As Integer, ByVal index As Integer)
        Try
            ListaPerfilTipoPV = getListaPerfilTipoPV()
            ListaPerfilTipoPV.RemoveAt(index)
            DGVPerfilTipoPV.DataSource = ListaPerfilTipoPV
            DGVPerfilTipoPV.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminaci�n del registro.")
        End Try
    End Sub
    Protected Sub DGVPerfilTipoPV_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGVPerfilTipoPV.SelectedIndexChanged
        Dim codigoPerfilTipoPV% = CInt(IIf(txtCodigo.Text = "", 0, txtCodigo.Text))
        quitarRegistroPerfilTipoPV(CInt(CType(DGVPerfilTipoPV.SelectedRow.Cells(1).FindControl("HddIdPerfil"),  _
                                    HiddenField).Value), CInt(codigoPerfilTipoPV), DGVPerfilTipoPV.SelectedIndex)
    End Sub

#End Region
#Region "Perfil - Perfil Medio Pago"

    '************************ Perfil - Perfil Medio Pago ****************************************************
    'Private Sub CargarDatosMedioPago(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
    '    Dim nCombo As New Combo
    '    nCombo.LlenarCboMedioPago(combo)
    'End Sub
    'Llenar la grilla 
    Private Function getListaPerfilMedioPago() As List(Of Entidades.Perfil_MedioPago)
        'aqui lo distancio para q me carge a la grilla
        ListaPerfilMedioPago = New List(Of Entidades.Perfil_MedioPago)
        For Each fila As GridViewRow In Me.DGVPerfilMedioPago.Rows
            Dim obj As New Entidades.Perfil_MedioPago

            obj.IdMedioPago = CInt(CType(fila.Cells(1).FindControl("HddIdPerfilMP"), HiddenField).Value)
            obj.MedioPago = CStr(CType(fila.Cells(1).FindControl("lblNombrePerfilMP"), Label).Text)
            obj.Estado = CType(fila.Cells(2).FindControl("ChboxEstado"), CheckBox).Checked

            ListaPerfilMedioPago.Add(obj)
        Next
        Return ListaPerfilMedioPago
    End Function

    'Boton A�ADIR
    Protected Sub btnAgregarPerfilMedioPago_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarPerfilMedioPago.Click
        CargarDatosGrillaPerfilMedioPago(DGVPerfilMedioPago)
    End Sub
    Private Sub CargarDatosGrillaPerfilMedioPago(ByVal gv As GridView)
        Dim obj As New Entidades.Perfil_MedioPago
        'Como ya esta instanciado, ahora qiero q me carge + de una fila
        ListaPerfilMedioPago = getListaPerfilMedioPago()
        obj.IdMedioPago = CInt(cboPerfilMP.SelectedValue)
        obj.MedioPago = (cboPerfilMP.SelectedItem.ToString)
        obj.Estado = True
        ListaPerfilMedioPago.Add(obj)
        DGVPerfilMedioPago.DataSource = ListaPerfilMedioPago
        DGVPerfilMedioPago.DataBind()
    End Sub
    'quitar
    Private Sub quitarRegistroPerfilMedioPago(ByVal IdPerfil As Integer, ByVal IdMedioPago As Integer, ByVal index As Integer)
        Try
            ListaPerfilMedioPago = getListaPerfilMedioPago()
            ListaPerfilMedioPago.RemoveAt(index)
            DGVPerfilMedioPago.DataSource = ListaPerfilMedioPago
            DGVPerfilMedioPago.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminaci�n del registro.")
        End Try
    End Sub
    Protected Sub DGVPerfilMedioPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGVPerfilMedioPago.SelectedIndexChanged
        Dim codigoPerfilMedioPago% = CInt(IIf(txtCodigo.Text = "", 0, txtCodigo.Text))
        quitarRegistroPerfilMedioPago(CInt(CType(DGVPerfilMedioPago.SelectedRow.Cells(1).FindControl("HddIdPerfilMP"),  _
                                    HiddenField).Value), CInt(codigoPerfilMedioPago), DGVPerfilMedioPago.SelectedIndex)
    End Sub

#End Region
    '************************ Condici�n Comercial ****************************************************
    'Cargar el Combo
    Private Sub CargarComboTipoDocumento(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim ncombo As New Combo
        ncombo.LlenarComboTipoDocumento(combo)
    End Sub

#Region "Seccion de TipoExistencia-Area"
    Protected Sub btaddarea_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btaddarea.Click
        Try
            LLenarGrillaArea()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Function cargarLinea_Area(ByVal idTipoExistencia As Integer) As List(Of Entidades.Linea_Area)
        Lista_Linea_Area = (New Negocio.Linea_Area).SelectLinea_Area_TipoExist(idTipoExistencia)
        If Lista_Linea_Area.Count > 0 Then
            If Lista_Linea_Area(0).objArea Is Nothing Then Lista_Linea_Area(0).objArea = (New Negocio.Area).SelectCbo
        End If
        If Lista_Linea_Area.Count > 0 Then
            For x As Integer = 1 To Lista_Linea_Area.Count - 1
                Lista_Linea_Area(x).objArea = Lista_Linea_Area(x - 1).objArea
            Next
        End If
        Return Lista_Linea_Area
    End Function

    Private Function getLista_Linea_Area() As List(Of Entidades.Linea_Area)
        Lista_Linea_Area = New List(Of Entidades.Linea_Area)
        For Each fila As GridViewRow In gvarea.Rows
            Obj_Linea_Area = New Entidades.Linea_Area
            With Obj_Linea_Area

                Dim cboArea As DropDownList = CType(fila.Cells(1).FindControl("dlarea"), DropDownList)
                .IdArea = CInt(cboArea.SelectedValue)

                If fila.RowIndex = 0 Then
                    Lista_Area = New List(Of Entidades.Area)
                    For x As Integer = 0 To cboArea.Items.Count - 1
                        Obj_Area = New Entidades.Area
                        With Obj_Area
                            .Id = CInt(cboArea.Items(x).Value)
                            .DescripcionCorta = cboArea.Items(x).Text
                        End With
                        Lista_Area.Add(Obj_Area)
                    Next
                    .objArea = Lista_Area
                End If
                If Lista_Linea_Area.Count > 0 Then .objArea = Lista_Linea_Area(0).objArea
                .LAEstado = CBool(CType(fila.Cells(2).FindControl("ckestado"), CheckBox).Checked)
            End With
            Lista_Linea_Area.Add(Obj_Linea_Area)
        Next
        Return Lista_Linea_Area
    End Function

    Private Sub LLenarGrillaArea()
        Lista_Linea_Area = getLista_Linea_Area()
        Obj_Linea_Area = New Entidades.Linea_Area
        With Obj_Linea_Area
            .IdArea = 0
            If Lista_Linea_Area.Count = 0 Then .objArea = (New Negocio.Area).SelectCbo()
            If Lista_Linea_Area.Count > 0 Then .objArea = Lista_Linea_Area(0).objArea
            .LAEstado = True
        End With
        Lista_Linea_Area.Add(Obj_Linea_Area)
        gvarea.DataSource = Lista_Linea_Area
        gvarea.DataBind()

    End Sub

    'Protected Sub validarAreaxTExistencia()
    '    Lista_Linea_Area = getLista_Linea_Area()
    '    If Lista_Linea_Area.Count > 1 Then

    '        Dim L As List(Of Entidades.Linea_Area)
    '        L = Lista_Linea_Area

    '        Dim cont, i, j As Integer
    '        cont = 0

    '        For i = 0 To L.Count - 1
    '            For j = 0 To L.Count - 1
    '                If i <> j Then
    '                    If L(i).IdArea = L(j).IdArea Then
    '                        cont = cont + 1
    '                        If cont > 0 Then
    '                            Exit For
    '                        End If
    '                    End If
    '                End If
    '            Next
    '        Next

    '        If cont > 0 Then
    '            LblMsjGrillaAreasxTExistencia.Text = "Hay �reas repetidas"
    '        Else
    '            LblMsjGrillaAreasxTExistencia.Text = ""
    '        End If
    '    End If
    'End Sub

    Private Sub gvArea_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvarea.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(1).FindControl("dlarea"), DropDownList)
                Dim ck As CheckBox = CType(e.Row.Cells(2).FindControl("ckestado"), CheckBox)
                If Lista_Linea_Area(e.Row.RowIndex).IdArea <> 0 Then
                    cbo.SelectedValue = CStr(Lista_Linea_Area(e.Row.RowIndex).IdArea)
                End If
                ck.Checked = Lista_Linea_Area(e.Row.RowIndex).LAEstado
            End If
            'If e.Row.RowIndex = gvarea.Rows.Count - 1 Then     'para que trabaje al final
            'validarAreaxTExistencia()
            'End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub gvArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvarea.SelectedIndexChanged
        Try
            Lista_Linea_Area = getLista_Linea_Area()
            Lista_Linea_Area.RemoveAt(gvarea.SelectedIndex)
            gvarea.DataSource = Lista_Linea_Area
            gvarea.DataBind()
            'validarAreaxTExistencia()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "Seccion de Medio Pago Interfaz y Tipo Documento"

    Private Sub dlTipoConceptoBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlTipoConceptoBanco.SelectedIndexChanged
        cbo.LlenarCboConceptoMovBancoxIdTipoConceptoBanco(dlConceptoMovBanco, CInt(dlTipoConceptoBanco.SelectedValue), True)
    End Sub

    Function cargarTipoDoc_MedioP(ByVal idMedioPago As Integer) As List(Of Entidades.TipoDocumento_MedioPago)
        Lista_TipoDocumento_MP = (New Negocio.MedioPago).ListarTipoDoc_MedioP(idMedioPago)
        If Lista_TipoDocumento_MP.Count > 0 Then
            If Lista_TipoDocumento_MP(0).ObjTipoDocumento Is Nothing Then Lista_TipoDocumento_MP(0).ObjTipoDocumento = (New Negocio.TipoDocumento).SelectCbo
        End If
        If Lista_TipoDocumento_MP.Count > 0 Then
            For x As Integer = 1 To Lista_TipoDocumento_MP.Count - 1
                Lista_TipoDocumento_MP(x).ObjTipoDocumento = Lista_TipoDocumento_MP(x - 1).ObjTipoDocumento
            Next
        End If
        Return Lista_TipoDocumento_MP
    End Function

    Private Sub llenarGrillatipoDocumentoMP()
        Lista_TipoDocumento_MP = getLista_TipoDocumento_MP()
        Obj_TipoDocumento_MP = New Entidades.TipoDocumento_MedioPago
        With Obj_TipoDocumento_MP
            .IdTipoDocumento = 0
            If Lista_TipoDocumento_MP.Count = 0 Then .ObjTipoDocumento = (New Negocio.TipoDocumento).SelectCbo
            If Lista_TipoDocumento_MP.Count > 0 Then .ObjTipoDocumento = Lista_TipoDocumento_MP(0).ObjTipoDocumento
            .Estado = True
        End With
        Lista_TipoDocumento_MP.Add(Obj_TipoDocumento_MP)
        gvtipodocmp.DataSource = Lista_TipoDocumento_MP
        gvtipodocmp.DataBind()
    End Sub

    Private Function getLista_TipoDocumento_MP() As List(Of Entidades.TipoDocumento_MedioPago)
        Lista_TipoDocumento_MP = New List(Of Entidades.TipoDocumento_MedioPago)
        For Each fila As GridViewRow In gvtipodocmp.Rows
            Obj_TipoDocumento_MP = New Entidades.TipoDocumento_MedioPago
            With Obj_TipoDocumento_MP

                Dim cboTipoDocumento As DropDownList = CType(fila.Cells(1).FindControl("dltipodocumentomp"), DropDownList)
                .IdTipoDocumento = CInt(cboTipoDocumento.SelectedValue)

                If fila.RowIndex = 0 Then
                    Dim ListaTipoDoc As New List(Of Entidades.TipoDocumento)
                    For x As Integer = 0 To cboTipoDocumento.Items.Count - 1
                        Dim objTipoDoc As New Entidades.TipoDocumento
                        With objTipoDoc
                            .Id = CInt(cboTipoDocumento.Items(x).Value)
                            .Descripcion = cboTipoDocumento.Items(x).Text
                        End With
                        ListaTipoDoc.Add(objTipoDoc)
                    Next
                    .ObjTipoDocumento = ListaTipoDoc
                End If
                If Lista_TipoDocumento_MP.Count > 0 Then .ObjTipoDocumento = Lista_TipoDocumento_MP(0).ObjTipoDocumento
                .Estado = CBool(CType(fila.Cells(2).FindControl("ckestadomp"), CheckBox).Checked)
            End With
            Lista_TipoDocumento_MP.Add(Obj_TipoDocumento_MP)
        Next
        Return Lista_TipoDocumento_MP
    End Function

    Protected Sub btaddtipodocumento_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btaddtipodocumento.Click
        Try
            llenarGrillatipoDocumentoMP()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvtipodocmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvtipodocmp.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cbo As DropDownList = CType(e.Row.Cells(1).FindControl("dltipodocumentomp"), DropDownList)
            Dim ck As CheckBox = CType(e.Row.Cells(2).FindControl("ckestadomp"), CheckBox)
            If Lista_TipoDocumento_MP(e.Row.RowIndex).IdTipoDocumento <> 0 Then
                cbo.SelectedValue = CStr(Lista_TipoDocumento_MP(e.Row.RowIndex).IdTipoDocumento)
            End If
            ck.Checked = Lista_TipoDocumento_MP(e.Row.RowIndex).Estado
        End If
    End Sub

    Private Sub gvtipodocmp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvtipodocmp.SelectedIndexChanged
        Lista_TipoDocumento_MP = getLista_TipoDocumento_MP()
        Lista_TipoDocumento_MP.RemoveAt(gvtipodocmp.SelectedIndex)
        gvtipodocmp.DataSource = Lista_TipoDocumento_MP
        gvtipodocmp.DataBind()
    End Sub

#End Region

#Region "seccion de area y centro de costo"

    Protected Sub dlunidadnegocio_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlunidadnegocio.SelectedIndexChanged
        Try
            If dlunidadnegocio.SelectedValue <> "00" Then
                cbo.LlenarCboCod_DepFunc(dldptofuncional, dlunidadnegocio.SelectedValue, True)
            Else
                dldptofuncional.Items.Clear() : dldptofuncional.Items.Insert(0, New ListItem("------", "00"))
            End If
            dlsubarea1.Items.Clear() : dlsubarea1.Items.Insert(0, New ListItem("------", "00"))
            dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
            dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub dldptofuncional_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dldptofuncional.SelectedIndexChanged
        Try
            If dldptofuncional.SelectedValue <> "00" Then
                cbo.LlenarCboCod_SubCodigo2(dlsubarea1, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, True)
            Else
                dlsubarea1.Items.Clear() : dlsubarea1.Items.Insert(0, New ListItem("------", "00"))
            End If
            dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
            dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub dlsubarea1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlsubarea1.SelectedIndexChanged
        Try
            If dlsubarea1.SelectedValue <> "00" Then
                cbo.LlenarCboCod_SubCodigo3(dlsubarea2, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, dlsubarea1.SelectedValue, True)
            Else
                dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
            End If
            dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub dlsubarea2_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlsubarea2.SelectedIndexChanged
        Try
            If dlsubarea2.SelectedValue <> "00" Then
                cbo.LlenarCboCod_SubCodigo4(dlsubarea3, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, dlsubarea1.SelectedValue, dlsubarea2.SelectedValue, True)
            Else
                dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

    Private Sub ddlLinea_rel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLinea_rel.SelectedIndexChanged
        cargarSubLinea(ddlLinea_rel)
    End Sub

    Private Sub CargarColumnasSector_sublinea()
        Dim dt As New DataTable
        Dim dr As DataRow = Nothing
        dt.Columns.Add(New DataColumn("IdSubLInea", GetType(Int32)))
        dt.Columns.Add(New DataColumn("sl_Nombre", GetType(String)))        
        ViewState("Sublinea_tabla") = dt
    End Sub

    Private Sub btnAgreagarSublinea_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgreagarSublinea.Click
        Dim objSector As New Negocio.LNValorizadoCajas        
        If Me.ddlLinea_rel.SelectedItem.Text <> "---" Then
            Dim dt As New DataTable
            dt = ViewState("Sublinea_tabla")
            Dim row_busqueda As DataRow() = dt.Select("IdSubLInea = " & Me.ddlSubLinea_rel.SelectedValue)
            If row_busqueda.Length = 0 Then
                Dim dr As DataRow = dt.NewRow
                dr("IdSubLInea") = Me.ddlSubLinea_rel.SelectedValue
                dr("sl_Nombre") = Me.ddlSubLinea_rel.SelectedItem.Text.ToString()
                dt.Rows.Add(dr)
                ViewState("Sublinea_tabla") = dt
                Me.gvSector_rel_Sublinea.DataSource = dt
                Me.gvSector_rel_Sublinea.DataBind()
            End If
        End If
    End Sub

    Private Sub gvSector_rel_Sublinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSector_rel_Sublinea.SelectedIndexChanged
        Dim indice As Integer = Me.gvSector_rel_Sublinea.SelectedIndex
        Dim dt As New DataTable
        dt = ViewState("Sublinea_tabla")
        If dt.Rows.Count > 0 Then
            dt.Rows.RemoveAt(indice)
            Me.gvSector_rel_Sublinea.DataSource = dt
            Me.gvSector_rel_Sublinea.DataBind()
        End If
    End Sub

    Private Sub ddlSector_rel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSector_rel.SelectedIndexChanged
        Dim obj As New Negocio.LNValorizadoCajas
        Dim dt As New DataTable
        dt = obj.LN_ReturnDataTable(Me.ddlSector_rel.SelectedValue, "LISTAR_SUBLINEA_X_SECTOR")
        ViewState("Sublinea_tabla") = dt
        Me.gvSector_rel_Sublinea.DataSource = dt
        Me.gvSector_rel_Sublinea.DataBind()
    End Sub
End Class
