﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmCentroCosto
    Inherits System.Web.UI.Page

#Region "variables"
    Private objscript As New ScriptManagerClass
    Private Lista_centro_costo As List(Of Entidades.Centro_costo)
    Private Obj_centro_costo As Entidades.Centro_costo
    Private drop As Combo
    Private ValidoRep As New Negocio.Util

    Private Enum operativo
        ninguno = 0
        guardarNuevo = 1
        actualizar = 2
    End Enum
#End Region

#Region "Procedimientos"

    Private Sub agregarElementos()
        dlunidadnegocio.Items.Insert(0, New ListItem("------", "00"))
        dldptofuncional.Items.Insert(0, New ListItem("------", "00"))
        dlsubarea1.Items.Insert(0, New ListItem("------", "00"))
        dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
        dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
        '**************
        dlunidad.Items.Insert(0, New ListItem("------", "00"))
        dlfuncional.Items.Insert(0, New ListItem("------", "00"))
        dlarea1.Items.Insert(0, New ListItem("------", "00"))
        dlarea2.Items.Insert(0, New ListItem("------", "00"))
        dlarea3.Items.Insert(0, New ListItem("------", "00"))
        '**************
        tbdescripcion.Focus()
    End Sub

    Private Sub llenarGrillaBusqueda()
        Obj_centro_costo = New Entidades.Centro_costo
        With Obj_centro_costo
            .Estado = CInt(rbver.SelectedValue)
            .Cod_UniNeg = dlunidad.SelectedValue
            .Cod_DepFunc = dlfuncional.SelectedValue
            .Cod_SubCodigo2 = dlarea1.SelectedValue
            .Cod_SubCodigo3 = dlarea2.SelectedValue
            .Cod_SubCodigo4 = dlarea3.SelectedValue
        End With
        Lista_centro_costo = (New Negocio.Centro_Costo).Listar_centro_costo(Obj_centro_costo)
        gvbusqueda.DataSource = Lista_centro_costo
        setDataSource(Lista_centro_costo)
        gvbusqueda.DataBind()
    End Sub

    Private Sub cargarAlIniciar()
        agregarElementos()
        'llenarGrillaBusqueda()

        drop = New Combo
        'If gvbusqueda.Rows.Count > 0 Then
        drop.LlenarCboCod_UniNeg(dlunidadnegocio, True)
        drop.LlenarCboCod_UniNeg(dlunidad, True, 0)
     
        'End If
    End Sub

    Private Sub LimpiarForm()
        tbcodigo.Text = ""
        tbdescripcion.Text = ""
        hdd_idcentrocosto.Value = "0"
        hdd_modo.Value = CStr(operativo.ninguno)
    End Sub

    Private Sub VerBotones(ByVal modo As String)
        Select Case modo
            Case "Load"
                btNuevo.Visible = True
                btGuardar.Visible = False
                btCancelar.Visible = False
                pnlDatos.Enabled = False
                pnlBusqueda.Enabled = True
            Case "Nuevo"
                btNuevo.Visible = False
                btGuardar.Visible = True
                btCancelar.Visible = True
                pnlDatos.Enabled = True
                pnlBusqueda.Enabled = False
        End Select
    End Sub

    Private Sub Mantenimiento(ByVal tipo As Integer)

        Try
            Obj_centro_costo = New Entidades.Centro_costo
            With Obj_centro_costo
                .IdCentroCosto = CInt(hdd_idcentrocosto.Value)
                .Cod_UniNeg = CStr(dlunidadnegocio.SelectedValue)
                .Cod_DepFunc = CStr(dldptofuncional.SelectedValue)
                .Cod_SubCodigo2 = CStr(dlsubarea1.SelectedValue)
                .Cod_SubCodigo3 = CStr(dlsubarea2.SelectedValue)
                '.Nombre_CC = HttpUtility.HtmlDecode(tbdescripcion.Text.Trim)
                .Nombre_CC = CStr(tbdescripcion.Text.Trim)
                .Cod_SubCodigo4 = "00"
                '.codigo = HttpUtility.HtmlDecode(tbcodigo.Text.Trim)
                .codigo = CStr(tbcodigo.Text.Trim)
                .Estado_CC = CBool(rbestado.SelectedValue)
            End With
            Dim resultado As Boolean = False
            'Dim mensaje As String = String.Empty


            Select Case tipo
                Case operativo.guardarNuevo
                    If ValidoRep.ValidarExistenciaxTablax1Campo("Centro_Costo", "Nombre_CC", Obj_centro_costo.Nombre_CC) <= 0 Then
                        resultado = (New Negocio.Centro_Costo).Insert_Centro_Costo(Obj_centro_costo)
                        objscript.mostrarMsjAlerta(Me, "El registro ha sido guardado correctamente")
                    Else
                        objscript.mostrarMsjAlerta(Me, "El nombre del registro ya existe. No procede la operación.")
                    End If
                Case operativo.actualizar
                    If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Centro_Costo", "Nombre_CC", Obj_centro_costo.Nombre_CC, "IdCentroCosto", Obj_centro_costo.IdCentroCosto) <= 0 Then
                        Obj_centro_costo.Cod_SubCodigo4 = dlsubarea3.SelectedValue
                        resultado = (New Negocio.Centro_Costo).Update_Centro_Costo(Obj_centro_costo)
                        objscript.mostrarMsjAlerta(Me, "El registro ha sido actualizado correctamente")
                    Else
                        objscript.mostrarMsjAlerta(Me, "El nombre del registro ya existe. No procede la operación.")
                    End If


            End Select
            If resultado = True Then
                'objscript.mostrarMsjAlerta(Me, "El registro ha sido " + mensaje)
                hdd_modo.Value = CStr(operativo.ninguno)
                'llenarGrillaBusqueda()
                VerBotones("Load")
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Function getDataSource() As Object
        Return CType(Session.Item("Data"), Object)
    End Function

    Private Sub setDataSource(ByVal obj As Object)
        Session.Remove("Data")
        Session.Add("Data", obj)
    End Sub

    Private Sub recargarCod_UniNeg()
        drop = New Combo
        drop.LlenarCboCod_UniNeg(dlunidadnegocio, True)
        drop.LlenarCboCod_UniNeg(dlunidad, True, 0)
    End Sub

    Private Sub HabilitarEdicion(ByVal valor As Boolean)
        dlunidadnegocio.Enabled = Not valor
        dldptofuncional.Enabled = Not valor
        dlsubarea1.Enabled = Not valor
        dlsubarea2.Enabled = Not valor
        dlsubarea3.Enabled = Not valor
    End Sub

#End Region

#Region "Eventos Principales"
    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                cargarAlIniciar()
                VerBotones("Load")
                hdd_modo.Value = CStr(operativo.ninguno)
                ConfigurarDatos()
            End If
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btNuevo.Click
        Try
            HabilitarEdicion(False)
            VerBotones("Nuevo")
            recargarCod_UniNeg()
            LimpiarForm()
            dlunidadnegocio_SelectedIndexChanged(sender, e)
            dlunidad_SelectedIndexChanged(sender, e)
            hdd_modo.Value = CStr(operativo.guardarNuevo)
            tbcodigo.Focus()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btGuardar.Click
        Try
            Mantenimiento(CInt(hdd_modo.Value))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btCancelar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btCancelar.Click
        Try
            VerBotones("Load")
            LimpiarForm()
            If dlunidadnegocio.Items.Count > 0 Then
                dlunidadnegocio.SelectedValue = "00"
                dlunidadnegocio_SelectedIndexChanged(sender, e)
            End If
            If dlunidad.Items.Count > 0 Then
                dlunidad.SelectedValue = "00"
                dlunidad_SelectedIndexChanged(sender, e)
            End If
            hdd_modo.Value = CStr(operativo.ninguno)
            gvbusqueda.DataSource = getDataSource()
            gvbusqueda.DataBind()
            dlunidadnegocio_SelectedIndexChanged(sender, e)
            dlunidad_SelectedIndexChanged(sender, e)
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btbuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btbuscar.Click
        Try
            llenarGrillaBusqueda()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvbusqueda_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvbusqueda.PageIndexChanging
        gvbusqueda.PageIndex = e.NewPageIndex
        gvbusqueda.DataSource = getDataSource()
        gvbusqueda.DataBind()
    End Sub

    Private Sub gvbusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvbusqueda.SelectedIndexChanged
        Try

            hdd_modo.Value = CStr(operativo.actualizar)

            dlunidadnegocio.SelectedValue = CType(gvbusqueda.SelectedRow.Cells(2).FindControl("hddCod_UniNeg"), HiddenField).Value
            dlunidadnegocio_SelectedIndexChanged(sender, e)

            dldptofuncional.SelectedValue = CType(gvbusqueda.SelectedRow.Cells(2).FindControl("hddCod_DepFunc"), HiddenField).Value
            dldptofuncional_SelectedIndexChanged(sender, e)

            dlsubarea1.SelectedValue = CType(gvbusqueda.SelectedRow.Cells(2).FindControl("hddCod_SubCodigo2"), HiddenField).Value
            dlsubarea1_SelectedIndexChanged(sender, e)

            dlsubarea2.SelectedValue = CType(gvbusqueda.SelectedRow.Cells(2).FindControl("hddCod_SubCodigo3"), HiddenField).Value
            dlsubarea2_SelectedIndexChanged(sender, e)

            dlsubarea3.SelectedValue = CType(gvbusqueda.SelectedRow.Cells(2).FindControl("hddCod_SubCodigo4"), HiddenField).Value
            hdd_idcentrocosto.Value = CType(gvbusqueda.SelectedRow.Cells(2).FindControl("hddidcentrocosto"), HiddenField).Value
            If dlsubarea3.SelectedValue <> "00" _
            And dlsubarea2.SelectedValue <> "00" _
            And dlsubarea1.SelectedValue <> "00" _
            And dldptofuncional.SelectedValue <> "00" _
            And dlunidadnegocio.SelectedValue <> "00" Then
                tbcodigo.Text = dlsubarea3.SelectedValue
                tbdescripcion.Text = HttpUtility.HtmlDecode(gvbusqueda.SelectedRow.Cells(7).Text)
            End If

            If dlsubarea3.SelectedValue = "00" _
            And dlsubarea2.SelectedValue <> "00" _
            And dlsubarea1.SelectedValue <> "00" _
            And dldptofuncional.SelectedValue <> "00" _
            And dlunidadnegocio.SelectedValue <> "00" Then
                tbcodigo.Text = dlsubarea2.SelectedValue
                tbdescripcion.Text = HttpUtility.HtmlDecode(gvbusqueda.SelectedRow.Cells(6).Text)
            End If
            If dlsubarea3.SelectedValue = "00" _
           And dlsubarea2.SelectedValue = "00" _
           And dlsubarea1.SelectedValue <> "00" _
           And dldptofuncional.SelectedValue <> "00" _
           And dlunidadnegocio.SelectedValue <> "00" Then
                tbcodigo.Text = dlsubarea1.SelectedValue
                tbdescripcion.Text = HttpUtility.HtmlDecode(gvbusqueda.SelectedRow.Cells(5).Text)
            End If
            If dlsubarea3.SelectedValue = "00" _
          And dlsubarea2.SelectedValue = "00" _
          And dlsubarea1.SelectedValue = "00" _
          And dldptofuncional.SelectedValue <> "00" _
          And dlunidadnegocio.SelectedValue <> "00" Then
                tbcodigo.Text = dldptofuncional.SelectedValue
                tbdescripcion.Text = HttpUtility.HtmlDecode(gvbusqueda.SelectedRow.Cells(4).Text)
            End If
            If dlsubarea3.SelectedValue = "00" _
          And dlsubarea2.SelectedValue = "00" _
          And dlsubarea1.SelectedValue = "00" _
          And dldptofuncional.SelectedValue = "00" _
          And dlunidadnegocio.SelectedValue <> "00" Then
                tbcodigo.Text = dlunidadnegocio.SelectedValue
                tbdescripcion.Text = HttpUtility.HtmlDecode(gvbusqueda.SelectedRow.Cells(3).Text)
            End If

            Dim std As Boolean = CType(gvbusqueda.SelectedRow.Cells(8).FindControl("ckestado"), CheckBox).Checked
            rbestado.SelectedValue = CStr(IIf(std = True, "1", "0"))

            VerBotones("Nuevo")
            HabilitarEdicion(True)

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub rbver_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbver.SelectedIndexChanged
        llenarGrillaBusqueda()
    End Sub

#End Region

#Region "seccion de cbo para mantenimiento"
    Protected Sub dlunidadnegocio_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlunidadnegocio.SelectedIndexChanged
        Dim ValorTexto As String = ""
        Try
            If dlunidadnegocio.SelectedValue <> "00" Then
                drop = New Combo
                If hdd_modo.Value = CStr(operativo.actualizar) Then
                    drop.LlenarCboCod_DepFunc(dldptofuncional, dlunidadnegocio.SelectedValue, True, 0)
                Else
                    drop.LlenarCboCod_DepFunc(dldptofuncional, dlunidadnegocio.SelectedValue, True)
                    ValorTexto = New Negocio.Centro_Costo().BuscarAutogeneradoXCodSubcodigo(dlunidadnegocio.SelectedValue.ToString)
                    If (CInt(ValorTexto) < 10) Then
                        tbcodigo.Text = "0" + ValorTexto
                    Else
                        tbcodigo.Text = ValorTexto
                    End If
                End If
            Else
                dldptofuncional.Items.Clear() : dldptofuncional.Items.Insert(0, New ListItem("------", "00"))
                ValorTexto = New Negocio.Centro_Costo().BuscarAutogeneradoXCodUniNegocio(dlunidadnegocio.SelectedValue.ToString)
                If (CInt(ValorTexto) < 10) Then
                    tbcodigo.Text = "0" + ValorTexto
                Else
                    tbcodigo.Text = ValorTexto
                End If

            End If
            dlsubarea1.Items.Clear() : dlsubarea1.Items.Insert(0, New ListItem("------", "00"))
            dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
            dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub dldptofuncional_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dldptofuncional.SelectedIndexChanged
        Try
            Dim ValorTexto As String = ""
            If dldptofuncional.SelectedValue <> "00" Then
                drop = New Combo
                If hdd_modo.Value = CStr(operativo.actualizar) Then
                    drop.LlenarCboCod_SubCodigo2(dlsubarea1, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, True, 0)
                Else
                    drop.LlenarCboCod_SubCodigo2(dlsubarea1, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, True)
                    ValorTexto = New Negocio.Centro_Costo().BuscarAutogeneradoXCodSubArea1(dlunidadnegocio.SelectedValue.ToString, dldptofuncional.SelectedValue.ToString)
                    If (CInt(ValorTexto) < 10) Then
                        tbcodigo.Text = "0" + ValorTexto
                    Else
                        tbcodigo.Text = ValorTexto
                    End If
                End If

            Else
                dlsubarea1.Items.Clear() : dlsubarea1.Items.Insert(0, New ListItem("------", "00"))
                ValorTexto = New Negocio.Centro_Costo().BuscarAutogeneradoXCodSubcodigo(dlunidadnegocio.SelectedValue.ToString)
                If (CInt(ValorTexto) < 10) Then
                    tbcodigo.Text = "0" + ValorTexto
                Else
                    tbcodigo.Text = ValorTexto
                End If
            End If
            dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
            dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub dlsubarea1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlsubarea1.SelectedIndexChanged
        Try
            Dim ValorTexto As String = ""
            If dlsubarea1.SelectedValue <> "00" Then
                drop = New Combo
                If hdd_modo.Value = CStr(operativo.actualizar) Then
                    drop.LlenarCboCod_SubCodigo3(dlsubarea2, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, dlsubarea1.SelectedValue, True, 0)
                Else
                    drop.LlenarCboCod_SubCodigo3(dlsubarea2, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, dlsubarea1.SelectedValue, True)
                    ValorTexto = New Negocio.Centro_Costo().BuscarAutogeneradoXCodSubArea2(dlunidadnegocio.SelectedValue.ToString, dldptofuncional.SelectedValue.ToString, dlsubarea1.SelectedValue.ToString)
                    If (CInt(ValorTexto) < 10) Then
                        tbcodigo.Text = "0" + ValorTexto
                    Else
                        tbcodigo.Text = ValorTexto
                    End If

                    End If
            Else
                dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
                ValorTexto = New Negocio.Centro_Costo().BuscarAutogeneradoXCodSubArea1(dlunidadnegocio.SelectedValue.ToString, dldptofuncional.SelectedValue.ToString)
                If (CInt(ValorTexto) < 10) Then
                    tbcodigo.Text = "0" + ValorTexto
                Else
                    tbcodigo.Text = ValorTexto
                End If
            End If
            dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub dlsubarea2_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlsubarea2.SelectedIndexChanged
        Try
            Dim ValorTexto As String = ""
            If dlsubarea2.SelectedValue <> "00" Then
                drop = New Combo
                If hdd_modo.Value = CStr(operativo.actualizar) Then
                    drop.LlenarCboCod_SubCodigo4(dlsubarea3, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, dlsubarea1.SelectedValue, dlsubarea2.SelectedValue, True, 0)
                Else
                    drop.LlenarCboCod_SubCodigo4(dlsubarea3, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, dlsubarea1.SelectedValue, dlsubarea2.SelectedValue, True)
                    ValorTexto = New Negocio.Centro_Costo().BuscarAutogeneradoXCodSubArea3(dlunidadnegocio.SelectedValue.ToString, dldptofuncional.SelectedValue.ToString, dlsubarea1.SelectedValue.ToString, dlsubarea2.SelectedValue.ToString)
                    If (CInt(ValorTexto) < 10) Then
                        tbcodigo.Text = "0" + ValorTexto
                    Else
                        tbcodigo.Text = ValorTexto
                    End If
                End If

            Else
                dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
                ValorTexto = New Negocio.Centro_Costo().BuscarAutogeneradoXCodSubArea2(dlunidadnegocio.SelectedValue.ToString, dldptofuncional.SelectedValue.ToString, dlsubarea1.SelectedValue.ToString)
                If (CInt(ValorTexto) < 10) Then
                    tbcodigo.Text = "0" + ValorTexto
                Else
                    tbcodigo.Text = ValorTexto
                End If
            End If
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

#Region "Seccion de cbo para busqueda"
    Protected Sub dlunidad_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlunidad.SelectedIndexChanged
        Try
            If dlunidad.SelectedValue <> "00" Then
                drop = New Combo
                drop.LlenarCboCod_DepFunc(dlfuncional, dlunidad.SelectedValue, True, 0)
            Else
                dlfuncional.Items.Clear() : dlfuncional.Items.Insert(0, New ListItem("------", "00"))
            End If
            dlarea1.Items.Clear() : dlarea1.Items.Insert(0, New ListItem("------", "00"))
            dlarea2.Items.Clear() : dlarea2.Items.Insert(0, New ListItem("------", "00"))
            dlarea3.Items.Clear() : dlarea3.Items.Insert(0, New ListItem("------", "00"))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub dlfuncional_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlfuncional.SelectedIndexChanged
        Try
            If dlfuncional.SelectedValue <> "00" Then
                drop = New Combo
                drop.LlenarCboCod_SubCodigo2(dlarea1, dlunidad.SelectedValue, dlfuncional.SelectedValue, True, 0)
            Else
                dlarea1.Items.Clear() : dlarea1.Items.Insert(0, New ListItem("------", "00"))
            End If
            dlarea2.Items.Clear() : dlarea2.Items.Insert(0, New ListItem("------", "00"))
            dlarea3.Items.Clear() : dlarea3.Items.Insert(0, New ListItem("------", "00"))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub dlarea1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlarea1.SelectedIndexChanged
        Try
            If dlarea1.SelectedValue <> "00" Then
                drop = New Combo
                drop.LlenarCboCod_SubCodigo3(dlarea2, dlunidad.SelectedValue, dlfuncional.SelectedValue, dlarea1.SelectedValue, True, 0)
            Else
                dlarea2.Items.Clear() : dlarea2.Items.Insert(0, New ListItem("------", "00"))
            End If
            dlarea3.Items.Clear() : dlarea3.Items.Insert(0, New ListItem("------", "00"))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub dlarea2_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlarea2.SelectedIndexChanged
        Try
            If dlarea2.SelectedValue <> "00" Then
                drop = New Combo
                drop.LlenarCboCod_SubCodigo4(dlarea3, dlunidad.SelectedValue, dlfuncional.SelectedValue, dlarea1.SelectedValue, dlarea2.SelectedValue, True, 0)
            Else
                dlarea3.Items.Clear() : dlarea3.Items.Insert(0, New ListItem("------", "00"))
            End If
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

End Class