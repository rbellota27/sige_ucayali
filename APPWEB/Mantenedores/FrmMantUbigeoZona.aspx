<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantUbigeoZona.aspx.vb" Inherits="APPWEB.FrmMantUbigeoZona" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 80%;">
        <tr>
            <td class ="Label"  >
                <label>
                    Departamento:
                </label>
            </td>
            <td>
                <asp:DropDownList ID="cboDpto" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td class ="Label">
                <label>
                    Provincia:
                </label>
            </td>
            <td>
                <asp:DropDownList ID="cboProv" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td class ="Label">
                <label>
                    Distrito:
                </label>
            </td>
            <td>
                <asp:DropDownList ID="cboDist" runat="server" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class ="Label">
                <label>
                    Zona:
                </label>
            </td>
            <td>
                <asp:DropDownList ID="cboZona" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                <asp:ImageButton ID="btnAceptar" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                    onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
            </td>
            <td>
                <asp:Label ID="lblMensaje" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:ListView ID="lvUbigeo" runat="server">
                    <LayoutTemplate>
                        <table class="GrillaPager">
                            <tr class="GrillaHeader">
                                <th>
                                    Id Ubigeo
                                </th>
                                <th>
                                    Departamento
                                </th>
                                <th>
                                    Provincia
                                </th>
                                <th>
                                    Distrito
                                </th>
                                <th>
                                    Zona
                                </th>
                            </tr>
                            <tr id="itemPlaceholder" runat="server" />
                        </table>
                        <asp:DataPager ID="dp_lvUbigeo" runat="server" PagedControlID="lvUbigeo" PageSize="20">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowNextPageButton="False"
                                    ShowPreviousPageButton="False" />
                                <asp:NumericPagerField />
                                <asp:NextPreviousPagerField ButtonType="Button" ShowLastPageButton="True" ShowNextPageButton="False"
                                    ShowPreviousPageButton="False" />
                            </Fields>
                        </asp:DataPager>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class="GrillaRow">
                            <td>
                                <%#Eval("IdUbigeo")%>
                            </td>
                            <td>
                                <%#Eval("Dpto")%>
                            </td>
                            <td>
                                <%#Eval("Prov")%>
                            </td>
                            <td>
                                <%#Eval("Dist")%>
                            </td>
                            <td>
                                <%#Eval("zo_nombre")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr class="GrillaRowAlternating">
                            <td>
                                <%#Eval("IdUbigeo")%>
                            </td>
                            <td>
                                <%#Eval("Dpto")%>
                            </td>
                            <td>
                                <%#Eval("Prov")%>
                            </td>
                            <td>
                                <%#Eval("Dist")%>
                            </td>
                            <td>
                                <%#Eval("zo_nombre")%>
                            </td>
                        </tr>
                    </AlternatingItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
