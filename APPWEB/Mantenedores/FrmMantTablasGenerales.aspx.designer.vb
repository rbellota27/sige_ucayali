﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmMantTablasGenerales
    
    '''<summary>
    '''Control UpdatePanel1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Control lbltablaS.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbltablaS As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cmbTabla.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmbTabla As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control lblNomTabla.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblNomTabla As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblMensaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMensaje As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control btnNuevo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnNuevo As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control btnGuardar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnGuardar As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control btnCancelar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCancelar As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control Panel_Datos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Datos As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control lblCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCodigo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txtCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCodigo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control tr_Chofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_Chofer As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblChofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblChofer As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control ddlChofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlChofer As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control lblDescripcion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblDescripcion As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txt1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt1 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control lblestadomasivo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblestadomasivo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txt11.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt11 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control lbl4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbl4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txt3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt3 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control lblDescBreve.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblDescBreve As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txt2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control lblAbrev.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblAbrev As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txtAbrev.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtAbrev As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control tr_TipoGasto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_TipoGasto As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblTipoGasto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTipoGasto As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control CboTipoGasto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CboTipoGasto As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control tr_CtaContablemp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_CtaContablemp As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblcuentaContablemp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblcuentaContablemp As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txtcuentacontablemp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtcuentacontablemp As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control tr_TipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_TipoDocumento As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblTipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTipoDocumento As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control CboTipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CboTipoDocumento As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control tr_principalmp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_principalmp As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblprincipalmp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblprincipalmp As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control ckprincipalmp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ckprincipalmp As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''Control tr_Regimen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_Regimen As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblRegimen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblRegimen As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cboRegimen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboRegimen As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control tr_TipoConceptoBanco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_TipoConceptoBanco As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblTipoConceptoBanco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTipoConceptoBanco As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control dlTipoConceptoBanco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlTipoConceptoBanco As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control tr_ConceptoMovBanco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_ConceptoMovBanco As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblConceptoMovBanco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblConceptoMovBanco As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control dlConceptoMovBanco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlConceptoMovBanco As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control tr_Concepto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_Concepto As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control llbConcepto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents llbConcepto As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control dlConcepto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlConcepto As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control tr_Pais.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_Pais As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblPais.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblPais As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control dlPais.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlPais As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control tr_TipoOperacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_TipoOperacion As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblTipoOperacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTipoOperacion As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cboTipoOperacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboTipoOperacion As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control tr_interfazmp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_interfazmp As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblinterfazmp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblinterfazmp As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control dlinterfazmp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlinterfazmp As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control tr_centrocosto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_centrocosto As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblcentrocosto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblcentrocosto As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control dlunidadnegocio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlunidadnegocio As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control dldptofuncional.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dldptofuncional As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control dlsubarea1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlsubarea1 As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control dlsubarea2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlsubarea2 As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control dlsubarea3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dlsubarea3 As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control tr_controlador.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_controlador As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control ddlControlador.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlControlador As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control tr_rel_Sector.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_rel_Sector As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control ddlSector_rel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlSector_rel As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control tr_rel_Sector_sublinea.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_rel_Sector_sublinea As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control ddlLinea_rel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlLinea_rel As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control ddlSubLinea_rel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ddlSubLinea_rel As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control btnAgreagarSublinea.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAgreagarSublinea As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control tr_gvSector_rel_Sublinea.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_gvSector_rel_Sublinea As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control gvSector_rel_Sublinea.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvSector_rel_Sublinea As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control tr_check1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_check1 As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblCheck1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCheck1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control chkCheck1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents chkCheck1 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''Control lblCheck1Message.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCheck1Message As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control tr_check2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_check2 As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblCheck2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCheck2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control chkCheck2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents chkCheck2 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''Control lblCheck2Message.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCheck2Message As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control tr_check3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_check3 As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblCheck3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCheck3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control chkCheck3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents chkCheck3 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''Control lblCheck3Message.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCheck3Message As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control tr_check4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_check4 As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblCheck4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCheck4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control chkCheck4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents chkCheck4 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''Control lblCheck4Message.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCheck4Message As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control tr_caja1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_caja1 As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblcaja1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblcaja1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txtcaja1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtcaja1 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control tr_caja2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_caja2 As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblcaja2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblcaja2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txtcaja2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtcaja2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control tr_combo1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_combo1 As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblCombo1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCombo1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cboCombo1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboCombo1 As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control lblCombo1Message.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCombo1Message As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control tr_permiso.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_permiso As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblpermisomasivo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblpermisomasivo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control rdbpermisomasivo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rdbpermisomasivo As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''Control tr_estado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_estado As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblEstado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEstado As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control rdbEstados.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rdbEstados As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''Control tr_MonedaBase.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_MonedaBase As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lbl5.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbl5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control rdbBase.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rdbBase As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''Control tr_gv_area.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_gv_area As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control btaddarea.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btaddarea As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control hdd_tienerelacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdd_tienerelacion As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control gvarea.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvarea As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control LblMsjGrillaAreasxTExistencia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents LblMsjGrillaAreasxTExistencia As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control tr_DocumentoReferente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tr_DocumentoReferente As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Control lblDocumentoReferente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblDocumentoReferente As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control rdbDocumentoReferente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rdbDocumentoReferente As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''Control LblMsj.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents LblMsj As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control Panel_MU.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_MU As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control Label4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control M_cmbUMedida.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents M_cmbUMedida As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control btnAgregarUM.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAgregarUM As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control DGV_UM.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DGV_UM As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control Panel_TipoDocOpera.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_TipoDocOpera As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control Label42.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label42 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cmb_TipoOpera.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmb_TipoOpera As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control btnAgregarTipoOpera.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAgregarTipoOpera As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control DGV_TipoOpera.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DGV_TipoOpera As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control Panel_TipoDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_TipoDocRef As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control LabelTipoDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents LabelTipoDocRef As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cmb_TipoDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmb_TipoDocRef As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control btnAgregarTipoDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAgregarTipoDocRef As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control DGV_TipoDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DGV_TipoDocRef As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control Panel_Banco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Banco As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control btnAgregarOficina.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAgregarOficina As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control DGV_Oficina.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DGV_Oficina As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control Panel_MotivoTraslado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_MotivoTraslado As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control Label47.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label47 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cmb_MotivoTipoOperacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmb_MotivoTipoOperacion As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control btnAgregarMotivoTipoOperacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAgregarMotivoTipoOperacion As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control DGV_MotivoTipoOperacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DGV_MotivoTipoOperacion As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control Panel_CondicionPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_CondicionPago As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control Label1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cbo_CondicionPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cbo_CondicionPago As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control BtonAñadir.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents BtonAñadir As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control DGV_CondicionPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DGV_CondicionPago As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control Panel_TipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_TipoDocumento As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control Label2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cbo_TipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cbo_TipoDocumento As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control BotonAñadirTipoDoc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents BotonAñadirTipoDoc As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control DGV_TipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DGV_TipoDocumento As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control btaddTiendaConcepto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btaddTiendaConcepto As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control gvConpTienda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvConpTienda As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control pnltipodocumetomp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnltipodocumetomp As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control gvtipodocmp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvtipodocmp As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control btaddtipodocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btaddtipodocumento As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control PanelPerfilTipoPV.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents PanelPerfilTipoPV As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control Label3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cboPerfilTV.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboPerfilTV As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control ImageButton1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton1 As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control DGVPerfilTipoPV.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DGVPerfilTipoPV As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control PanelPerfilMedioPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents PanelPerfilMedioPago As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control Label5.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cboPerfilMP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboPerfilMP As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control btnAgregarPerfilMedioPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAgregarPerfilMedioPago As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control DGVPerfilMedioPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DGVPerfilMedioPago As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control lblBuscarPor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblBuscarPor As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cmbBuscarPor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmbBuscarPor As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control lblTextoBusqueda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTextoBusqueda As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txttextoBusqueda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txttextoBusqueda As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control btnFiltrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnFiltrar As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Control DGV1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DGV1 As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control lblMsjB.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMsjB As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblEstadoB.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEstadoB As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control rdbEstadosB.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rdbEstadosB As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''Control hddTabla.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddTabla As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hddModo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddModo As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hddConfigurarDatos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddConfigurarDatos As Global.System.Web.UI.WebControls.HiddenField
End Class
