﻿<%@ Page Title="Permisos" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmMantPermisos.aspx.vb" Inherits="APPWEB.FrmMantPermisos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <asp:Panel ID="Panel_Permisos" runat="server" Width="994px">
   <table style="width: 100%" >
    <tr>
    <td class="TituloCelda" style="height: 21px">Perfil Permisos</td>
    </tr>
    <tr>
    <td>
    <asp:ImageButton ID="btnGuardar" runat="server"  OnClientClick="return(   valSave()    );"
                        ImageUrl="~/Imagenes/Guardar_B.JPG" 
                        onmouseover="this.src='/Imagenes/Guardar_A.JPG';"  
                        onmouseout="this.src='/Imagenes/Guardar_B.JPG';"  />
    </td>
    </tr>
   <tr>
   <td>
   <table>
   <tr>
   <td class="Texto">Area:</td>
   <td>
       <asp:DropDownList ID="cboArea" runat="server" AutoPostBack="True">
       </asp:DropDownList>
   </td>
   <td class="Texto">Perfil:</td>
   <td><asp:DropDownList ID="cboPerfil" runat="server" AutoPostBack="True">
       </asp:DropDownList></td>
   </tr>
   </table>
   </td>
   </tr>
   <tr>
   <td>
   <asp:GridView ID="DGV_PerfilPermisos" runat="server" AutoGenerateColumns="False" Width="650px">                                            
                                            <Columns>
                                                <asp:TemplateField HeaderText="Permisos" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"  >
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:HiddenField ID="HDD_IdPermiso" runat="server" 
                                                                        Value='<%# DataBinder.Eval(Container.DataItem,"IdPermiso") %>' />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LblNombrePermiso" runat="server" 
                                                                        Text='<%# DataBinder.Eval(Container.DataItem,"DescPermiso") %>'></asp:Label>
                                                                </td>   
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Activos"  HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" >
                                                    <ItemTemplate >
                                                       <asp:CheckBox ID="Chb_Estado" runat="server"                                                            
                                                            Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                                    </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            <RowStyle CssClass="GrillaRow" />
                                        </asp:GridView>
   </td>
   </tr>
       <tr class="LabelRojo" style="font-weight:bold">
           <td>
               *** Los Permisos Activos, serán aplicados a todos los Usuarios que 
               posean el Perfil seleccionado.</td>
       </tr>
   </table>            
    </asp:Panel>
                        
<script language="javascript" type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
    Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
        if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
            offsetX = Math.floor(offsetX);
            offsetY = Math.floor(offsetY);
        }
        this._origOnFormActiveElement(element, offsetX, offsetY);
    };
		
    function valSave() {        
        return confirm('Desea continuar con la Operación ?');
    }
</script>

</asp:Content>
