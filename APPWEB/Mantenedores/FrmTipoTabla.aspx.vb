﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmTipoTabla
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Private cbo As New Combo
    Private colorReadOnly As Drawing.Color = Drawing.Color.LightGray
    Private colorHabilitado As Drawing.Color = Drawing.Color.White
    Private listaSubLineaTipoTablaValor As New List(Of Entidades.SubLinea_TipoTablaValor)

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ConfigurarDatos()
            '**** TIPO TABLA
            HabilitarBotonesTT(True, False, False, False)
            HabilitarPanelesTT(False, True)
            Me.rbEstadoBusquedaTT.SelectedValue = "1"
            'SelectAllTipoTablaxEstado(CInt(Me.rbEstadoBusquedaTT.SelectedValue), dgvTipoTabla)
            onClick_Buscar_tt()

            hddIdTipoTabla.Value = "0"
            '**** TABLA
            HabilitarBotonesTTV(True, False, False, False)
            HabilitarPanelesTTV(False, True)
            Me.rbEstadoBusquedaTTV.SelectedValue = "1"
            cbo.LlenarCboTipoTabla(1, cboTablaTTV, True)

            hddIdTipoTablaValorTTV.Value = "0"

            '**** CONFIGURACION TABLA
            HabilitarBotonesConfig(True, False, False, False)
            HabilitarPanelesConfig(False, True)
            Me.rbEstadoBusquedaConfig.Visible = False
            Me.rbEstadoBusquedaConfig.SelectedValue = "1"
            LlenarCboTipoTabla(1, cboTipoTablaConfig, True)

            cbo.llenarCboTipoExistencia(cboTipoExistenciaConfig, True)
            cbo.llenarCboLineaxTipoExistencia(cboLineaConfig, CInt(cboTipoExistenciaConfig.SelectedValue), True)
            cbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLineaConfig, CInt(cboLineaConfig.SelectedValue), CInt(cboTipoExistenciaConfig.SelectedValue), True)

            Me.rbEstadoConfig.SelectedValue = "1"
            Me.rbTipoUsoConfig.SelectedValue = "OR"
            PanelEstadoConfig.Visible = False

            '**** CONFIGURACION TABLA VALOR
            setListaSTTVConfigxValor(Me.listaSubLineaTipoTablaValor)
            Me.dgvListConfigxValor.DataSource = Me.listaSubLineaTipoTablaValor
            Me.dgvListConfigxValor.DataBind()
            rbEstadoBusquedaConfigxValor.SelectedValue = "1"
            rbEstadoBusquedaConfigxValor.Visible = False
            HabilitarBotonesConfigxValor(True, False, False, False)
            HabilitarPanelesConfigxValor(False, True)
            cbo.llenarCboTipoExistencia(cboTipoExistenciaConfigxValor, True)
            cbo.llenarCboLineaxTipoExistencia(cboLineaConfigxValor, CInt(cboTipoExistenciaConfigxValor.SelectedValue), True)
            cbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLineaConfigxValor, CInt(cboLineaConfigxValor.SelectedValue), CInt(cboTipoExistenciaConfigxValor.SelectedValue), True)
            PanelBuscarConfigTablaxValor.Visible = False
        End If
    End Sub


#Region "Mantenimiento TIPO TABLA"
    Private Sub HabilitarBotonesTT(ByVal btnnuevo As Boolean, ByVal btngrabar As Boolean, ByVal btneditar As Boolean, ByVal btncancelar As Boolean)
        Me.btnNuevoTT.Visible = btnnuevo
        Me.btnGrabarTT.Visible = btngrabar
        Me.btnEditarTT.Visible = btneditar
        Me.btnCancelarTT.Visible = btncancelar
    End Sub
    Private Sub HabilitarPanelesTT(ByVal panelnuevo As Boolean, ByVal panelbusqueda As Boolean)
        Me.PanelNuevoTT.Visible = panelnuevo
        Me.PanelBuscarTT.Visible = panelbusqueda
    End Sub
    Private Sub ClearControlesTT()
        Me.txtNombreTT.Text = ""
        Me.txtAbvTT.Text = ""
        rbEstadoTT.SelectedValue = "1"
        hddIdTipoTabla.Value = "0"
        rbLongitudTT.SelectedValue = "1"
    End Sub
    Protected Sub btnNuevoTT_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevoTT.Click
        Me.hddModoTT.Value = "1"
        HabilitarBotonesTT(False, True, False, True)
        HabilitarPanelesTT(True, False)
        ClearControlesTT()
        Me.rbEstadoTT.Visible = False
    End Sub
    Protected Sub btnGrabarTT_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGrabarTT.Click
        Dim ngcValTabla As New Negocio.TipoTabla
        If Me.hddModoTT.Value = "1" Then
            Dim objValTabla As Entidades.TipoTabla = ngcValTabla.ValidadNombres("TipoTabla", "tt_Nombre", Me.txtNombreTT.Text.Trim)
            If objValTabla.Item >= 1 Then
                objScript.mostrarMsjAlerta(Me, "Ya existe el nombre " + Me.txtNombreTT.Text.Trim + " ingrese un nombre diferente" + "\n ó este inactivo.")
                txtNombreTT.Focus()
                Exit Sub
            End If
        End If
        If Me.hddModoTT.Value = "1" Then
            Dim objValTabla As Entidades.TipoTabla = ngcValTabla.ValidadNombres("TipoTabla", "tt_Abv", Me.txtAbvTT.Text.Trim)
            If objValTabla.Item >= 1 Then
                objScript.mostrarMsjAlerta(Me, "Ya existe una abv. " + Me.txtAbvTT.Text.Trim + " ingrese un abv. diferente." + "\n ó este inactivo.")
                txtAbvTT.Focus()
                Exit Sub
            End If
        End If

        GrabarTT(CInt(Me.hddModoTT.Value))
        HabilitarBotonesTT(True, False, False, False)
        HabilitarPanelesTT(False, True)
        ClearControlesTT()
        'SelectAllTipoTablaxEstado(CInt(Me.rbEstadoBusquedaTT.SelectedValue), dgvTipoTabla)
        Buscar_tt(0)

        cbo.LlenarCboTipoTabla(1, cboTablaTTV, True)
        LlenarCboTipoTabla(1, cboTipoTablaConfig, True)
        cbo.LlenarCboTipoTabla(1, cboTipoTablaConfigxValor, True)
        UpdatePanel5.Update()
    End Sub
    Private Sub GrabarTT(ByVal Flag As Integer)
        Try
            Dim ngcTipoTabla As New Negocio.TipoTabla
            Dim objTipoTabla As New Entidades.TipoTabla
            With objTipoTabla
                .IdTipoTabla = CInt(hddIdTipoTabla.Value)
                .Nombre = Me.txtNombreTT.Text.Trim
                .Abv = Me.txtAbvTT.Text.Trim
                .Longitud = CInt(rbLongitudTT.SelectedValue)
                .Estado = CBool(rbEstadoTT.SelectedValue)
            End With
            Select Case Flag
                Case 1
                    If ngcTipoTabla.InsertaTipoTabla(objTipoTabla) = True Then
                        objScript.mostrarMsjAlerta(Me, "El tipo tabla se registró con éxito.")
                    End If
                Case 2
                    If ngcTipoTabla.ActualizarTipoTabla(objTipoTabla) = True Then
                        objScript.mostrarMsjAlerta(Me, "El tipo tabla se actualizo con éxito.")
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub btnEditarTT_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditarTT.Click
        Me.hddModoTT.Value = "2"
        HabilitarBotonesTT(False, True, False, True)
        HabilitarPanelesTT(True, False)
        Me.PanelNuevoTT.Enabled = True
    End Sub
    Protected Sub btnCancelarTT_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelarTT.Click
        Me.hddModoTT.Value = "0"
        HabilitarBotonesTT(True, False, False, False)
        HabilitarPanelesTT(False, True)
        ClearControlesTT()
    End Sub
    'Private Sub SelectAllTipoTablaxEstado(ByVal estado As Integer, ByVal grilla As GridView)
    '    Dim ngcTipoTabla As New Negocio.TipoTabla
    '    Dim listaTT As List(Of Entidades.TipoTabla) = ngcTipoTabla.SelectAllTipoTablaxEstado(estado)
    '    grilla.DataSource = listaTT
    '    grilla.DataBind()
    'End Sub
    Protected Sub rbEstadoBusquedaTT_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rbEstadoBusquedaTT.SelectedIndexChanged
        'SelectAllTipoTablaxEstado(CInt(Me.rbEstadoBusquedaTT.SelectedValue), dgvTipoTabla)
        Buscar_t(0)
    End Sub
    Protected Sub dgvTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvTipoTabla.SelectedIndexChanged
        Dim hddestado As HiddenField = CType(dgvTipoTabla.SelectedRow.Cells(5).FindControl("hddEstado"), HiddenField)
        hddIdTipoTabla.Value = dgvTipoTabla.SelectedRow.Cells(1).Text
        txtNombreTT.Text = HttpUtility.HtmlDecode(dgvTipoTabla.SelectedRow.Cells(2).Text).Trim
        txtAbvTT.Text = HttpUtility.HtmlDecode(dgvTipoTabla.SelectedRow.Cells(3).Text).Trim

        If dgvTipoTabla.SelectedRow.Cells(4).Text = "1" Then
            rbLongitudTT.SelectedValue = "1"
        ElseIf dgvTipoTabla.SelectedRow.Cells(4).Text = "2" Then
            rbLongitudTT.SelectedValue = "2"
        ElseIf dgvTipoTabla.SelectedRow.Cells(4).Text = "3" Then
            rbLongitudTT.SelectedValue = "3"
        End If

        If CBool(hddestado.Value) = True Then
            rbEstadoTT.SelectedValue = "1"
        ElseIf CBool(hddestado.Value) = False Then
            rbEstadoTT.SelectedValue = "0"
        End If
        HabilitarBotonesTT(False, False, True, True)
        HabilitarPanelesTT(True, False)
        Me.PanelNuevoTT.Enabled = False
        Me.rbEstadoTT.Visible = True
    End Sub


#Region "Paginacion de busqueda"

    Protected Sub btn_buscar_tt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_buscar_tt.Click
        onClick_Buscar_tt()
    End Sub

    Private Sub onClick_Buscar_tt()
        ViewState.Add("descripcion_tt", Trim(txt_buscar_tt.Text))
        ViewState.Add("estado_tt", CInt(rbEstadoBusquedaTT.SelectedValue))
        Buscar_tt(0)
    End Sub

    Private Sub Buscar_tt(ByVal tipoMov As Integer)
        TipoTabla_Paginado(CStr(ViewState("descripcion_tt")), CInt(ViewState("estado_tt")), tipoMov, dgvTipoTabla)
    End Sub
    Private Sub TipoTabla_Paginado(ByVal Descripcion As String, _
                           ByVal Estado As Integer, ByVal TipoMov As Integer, _
                           ByVal Grilla As GridView)

        Dim index As Integer = 0
        Select Case TipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(txt_PageIndex_tt.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(txt_PageIndex_tt.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(txt_PageIndexGO_tt.Text) - 1)
        End Select

        Dim listaTT As List(Of Entidades.TipoTabla) = _
        (New Negocio.TipoTabla).TipoTablaSelectAllxEstado_Paginado( _
                                                Descripcion, Estado, _
                                                index, Grilla.PageSize)
        If listaTT.Count > 0 Then
            txt_PageIndex_tt.Text = CStr(index + 1)
            Grilla.DataSource = listaTT
            Grilla.DataBind()
        Else
            objScript.mostrarMsjAlerta(Me, "No se hallaron registros")
        End If

    End Sub


    Protected Sub btn_anterior_tt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_anterior_tt.Click
        Buscar_tt(1)
    End Sub
    Protected Sub btn_siguiente_tt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_siguiente_tt.Click
        Buscar_tt(2)
    End Sub
    Protected Sub btn_Ir_tt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Ir_tt.Click
        Buscar_tt(3)
    End Sub

#End Region

#End Region

#Region "Mantenimiento TIPO TABLA VALOR"
    Private Sub HabilitarBotonesTTV(ByVal btnnuevo As Boolean, ByVal btngrabar As Boolean, ByVal btneditar As Boolean, ByVal btncancelar As Boolean)
        Me.btnNuevoTTV.Visible = btnnuevo
        Me.btnGrabarTTV.Visible = btngrabar
        Me.btnEditarTTV.Visible = btneditar
        Me.btnCancelarTTV.Visible = btncancelar
    End Sub
    Private Sub HabilitarPanelesTTV(ByVal panelnuevo As Boolean, ByVal panelbusqueda As Boolean)
        Me.PanelNuevoTTV.Visible = panelnuevo
        Me.PanelBuscarTTV.Visible = panelbusqueda
    End Sub
    Private Sub ClearControlesTTV()
        Me.txtCodigoTTV.Text = ""
        Me.txtDescripcionTTV.Text = ""
        Me.txtAbvTTV.Text = ""
        rbEstadoTTV.SelectedValue = "1"
        hddIdTipoTablaValorTTV.Value = "0"
        lblMsjCodigoTTV.Text = ""
    End Sub
    Private Sub verTextBox(ByVal control As TextBox, ByVal readOnly1 As Boolean, ByVal DataFildCaja As String, ByVal maxLength As Integer)
        control.ReadOnly = readOnly1
        control.Text = HttpUtility.HtmlDecode(DataFildCaja).Trim
        control.MaxLength = maxLength
        If readOnly1 = True Then
            control.BackColor = Me.colorReadOnly
        Else
            control.BackColor = Me.colorHabilitado
        End If
    End Sub
    Private Sub GrabaTTV(ByVal Flag As Integer)
        Try
            Dim ngcTipoTablaValor As New Negocio.TipoTablaValor
            Dim objTipoTablaValor As New Entidades.TipoTablaValor
            With objTipoTablaValor
                .IdTipoTablaValor = CInt(hddIdTipoTablaValorTTV.Value)
                .IdTipoTabla = CInt(cboTablaTTV.SelectedValue)
                .Codigo = txtCodigoTTV.Text.Trim
                .Nombre = txtDescripcionTTV.Text.Trim
                .Abv = txtAbvTTV.Text.Trim
                .Estado = CBool(rbEstadoTTV.SelectedValue)
            End With
            Select Case Flag
                Case 1
                    If ngcTipoTablaValor.InsertaTipoTablaValor(objTipoTablaValor) = True Then
                        objScript.mostrarMsjAlerta(Me, "El " + cboTablaTTV.SelectedItem.Text + " se registró con éxito.")
                    End If
                Case 2
                    If ngcTipoTablaValor.ActualizaTipoTablaValor(objTipoTablaValor) = True Then
                        objScript.mostrarMsjAlerta(Me, "El " + cboTablaTTV.SelectedItem.Text + " se actualizo con éxito.")
                    End If
            End Select
            HabilitarBotonesTTV(True, False, False, False)
            HabilitarPanelesTTV(False, True)
            ClearControlesTTV()
            'SelectAllTipoTablaValorxEstado(CInt(cboTablaTTV.SelectedValue), CInt(Me.rbEstadoBusquedaTTV.SelectedValue), dgvTablaTTV)
            Buscar_t(0)
            hddModoTTV.Value = "0"
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GenCodigo(ByVal idtipotabla As Integer, ByVal control As TextBox, ByVal Msj As String)
        Try
            Dim ngcTipoTablaValor As New Negocio.TipoTablaValor
            Dim objTipoTablaValor As New Entidades.TipoTablaValor
            objTipoTablaValor = ngcTipoTablaValor.GenerarCodigo(idtipotabla)
            control.Text = objTipoTablaValor.Codigo
            lblMsjCodigoTTV.Text = ""
        Catch ex As Exception
            lblMsjCodigoTTV.Text = Msj
        End Try
    End Sub
    Private Sub btnNuevoTTV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevoTTV.Click
        Me.hddModoTTV.Value = "1"
        HabilitarBotonesTTV(False, True, False, True)
        HabilitarPanelesTTV(True, False)
        ClearControlesTTV()
        Me.rbEstadoTTV.Visible = False
        GenCodigo(CInt(cboTablaTTV.SelectedValue), txtCodigoTTV, "Ingrese código [Número o Letra].")
    End Sub
    Private Sub btnGrabarTTV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabarTTV.Click
        Dim ngcValTabla As New Negocio.TipoTabla
        If Me.hddModoTTV.Value = "1" Then
            Dim objValTabla As Entidades.TipoTabla = ngcValTabla.ValidadNombres("TipoTablaValor", "ttv_Nombre", Me.txtDescripcionTTV.Text.Trim, "IdTipoTabla", Me.cboTablaTTV.SelectedValue)
            If objValTabla.Item >= 1 Then
                objScript.mostrarMsjAlerta(Me, "Ya existe el nombre " + Me.txtDescripcionTTV.Text.Trim + " ingrese un nombre diferente" + "\n ó este inactivo.")
                txtDescripcionTTV.Focus()
                Exit Sub
            End If
        End If
        If Me.hddModoTTV.Value = "1" Then
            Dim objValTabla As Entidades.TipoTabla = ngcValTabla.ValidadNombres("TipoTablaValor", "ttv_Abv", Me.txtAbvTTV.Text.Trim, "IdTipoTabla", Me.cboTablaTTV.SelectedValue)
            If objValTabla.Item >= 1 Then
                objScript.mostrarMsjAlerta(Me, "Ya existe una abv. " + Me.txtAbvTTV.Text.Trim + " ingrese un abv. diferente" + "\n ó este inactivo.")
                txtAbvTTV.Focus()
                Exit Sub
            End If
        End If

        GrabaTTV(CInt(Me.hddModoTTV.Value))
    End Sub
    Private Sub btnEditarTTV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditarTTV.Click
        Me.hddModoTTV.Value = "2"
        HabilitarBotonesTTV(False, True, False, True)
        HabilitarPanelesTTV(True, False)
        Me.PanelNuevoTTV.Enabled = True
    End Sub
    Private Sub btnCancelarTTV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelarTTV.Click
        Me.hddModoTTV.Value = "0"
        HabilitarBotonesTTV(True, False, False, False)
        HabilitarPanelesTTV(False, True)
        ClearControlesTTV()
    End Sub
    Private Sub cboTablaTTV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTablaTTV.SelectedIndexChanged
        Dim ngcTipoTabla As New Negocio.TipoTabla
        Dim objTipoTabla As New Entidades.TipoTabla
        objTipoTabla = ngcTipoTabla.SelectTipoTablaAllActivoxId(CInt(cboTablaTTV.SelectedValue))
        verTextBox(Me.txtCodigoTTV, False, "", objTipoTabla.Longitud)
        GenCodigo(CInt(cboTablaTTV.SelectedValue), txtCodigoTTV, "Ingrese código [Número o Letra].")
        If hddModoTTV.Value <> "1" Then
            'SelectAllTipoTablaValorxEstado(CInt(cboTablaTTV.SelectedValue), CInt(Me.rbEstadoBusquedaTTV.SelectedValue), dgvTablaTTV)
            onClick_buscar_t()
        End If
    End Sub
    'Private Sub SelectAllTipoTablaValorxEstado(ByVal idtipotabla As Integer, ByVal estado As Integer, ByVal grilla As GridView)
    '    Dim ngcTipoTablaValor As New Negocio.TipoTablaValor
    '    Dim listaTTV As List(Of Entidades.TipoTablaValor) = ngcTipoTablaValor.SelectAllxIdTipoTablaxEstado(idtipotabla, estado)
    '    grilla.DataSource = listaTTV
    '    grilla.DataBind()
    'End Sub
    Private Sub rbEstadoBusquedaTTV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbEstadoBusquedaTTV.SelectedIndexChanged
        'SelectAllTipoTablaValorxEstado(CInt(cboTablaTTV.SelectedValue), CInt(Me.rbEstadoBusquedaTTV.SelectedValue), dgvTablaTTV)
        Buscar_t(0)
    End Sub
    Private Sub dgvTablaTTV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvTablaTTV.SelectedIndexChanged
        Dim hddIdTipoTabla As HiddenField = CType(dgvTablaTTV.SelectedRow.Cells(1).FindControl("hddIdTipoTabla"), HiddenField)
        Dim hhdidtipotablavalor As HiddenField = CType(dgvTablaTTV.SelectedRow.Cells(1).FindControl("hddIdTipoTablaValor"), HiddenField)
        Dim lblcodigo As Label = CType(dgvTablaTTV.SelectedRow.Cells(1).FindControl("lblCodigo"), Label)
        Dim hddestado As HiddenField = CType(dgvTablaTTV.SelectedRow.Cells(4).FindControl("hddEstado"), HiddenField)
        cboTablaTTV.SelectedValue = hddIdTipoTabla.Value
        hddIdTipoTablaValorTTV.Value = hhdidtipotablavalor.Value
        Me.txtCodigoTTV.Text = HttpUtility.HtmlDecode(lblcodigo.Text).Trim
        txtDescripcionTTV.Text = HttpUtility.HtmlDecode(dgvTablaTTV.SelectedRow.Cells(2).Text).Trim
        Me.txtAbvTTV.Text = HttpUtility.HtmlDecode(dgvTablaTTV.SelectedRow.Cells(3).Text).Trim
        If CBool(hddestado.Value) = True Then
            Me.rbEstadoTTV.SelectedValue = "1"
        ElseIf CBool(hddestado.Value) = False Then
            Me.rbEstadoTTV.SelectedValue = "0"
        End If
        HabilitarBotonesTTV(False, False, True, True)
        HabilitarPanelesTTV(True, False)
        Me.PanelNuevoTTV.Enabled = False
        Me.rbEstadoTTV.Visible = True
    End Sub

#Region "Paginacion de busqueda"

    Protected Sub btn_buscar_t_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_buscar_t.Click
        onClick_buscar_t()
    End Sub

    Private Sub onClick_buscar_t()
        ViewState.Add("IdTipoTabla_t", CInt(cboTablaTTV.SelectedValue))
        ViewState.Add("descripcion_t", Trim(txt_buscar_t.Text))
        ViewState.Add("estado_t", CInt(rbEstadoBusquedaTTV.SelectedValue))
        Buscar_t(0)
    End Sub

    Private Sub Buscar_t(ByVal tipoMov As Integer)
        TipoTablaV_Paginado(CInt(ViewState("IdTipoTabla_t")), CStr(ViewState("descripcion_t")), CInt(ViewState("estado_t")), tipoMov, dgvTablaTTV)
    End Sub
    Private Sub TipoTablaV_Paginado(ByVal IdtipoTabla As Integer, ByVal Descripcion As String, _
                           ByVal Estado As Integer, ByVal TipoMov As Integer, _
                           ByVal Grilla As GridView)

        Dim index As Integer = 0
        Select Case TipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(txt_PageIndex_t.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(txt_PageIndex_t.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(txt_PageIndexGo_t.Text) - 1)
        End Select

        Dim listaTTV As List(Of Entidades.TipoTablaValor) = _
        (New Negocio.TipoTablaValor).TipoTablaValorSelectAllxIdTipoTablaxEstado_Paginado( _
                                                IdtipoTabla, Descripcion, Estado, _
                                                index, Grilla.PageSize)
        If listaTTV.Count > 0 Then
            txt_PageIndex_t.Text = CStr(index + 1)
            Grilla.DataSource = listaTTV
            Grilla.DataBind()
        Else
            objScript.mostrarMsjAlerta(Me, "No se hallaron registros")
        End If

    End Sub


    Protected Sub btn_anterior_t_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_anterior_t.Click
        Buscar_t(1)
    End Sub
    Protected Sub btn_siguiente_t_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_siguiente_t.Click
        Buscar_t(2)
    End Sub
    Protected Sub btn_Ir_t_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_ir_t.Click
        Buscar_t(3)
    End Sub

#End Region

#End Region

#Region "Mantenimiento CONFIGURACION TABLA"
    Public Sub LlenarCboTipoTabla(ByVal estado As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.TipoTabla) = (New Negocio.TipoTabla).SelectAllTipoTablaxEstado(estado)
        If addElement Then
            Dim objtipotabla3 As New Entidades.TipoTabla(-3, "Seleccione Tabla", True)
            lista.Insert(0, objtipotabla3)

            Dim objtipotabla2 As New Entidades.TipoTabla(-2, "Linea", True)
            lista.Insert(1, objtipotabla2)

            Dim objtipotabla1 As New Entidades.TipoTabla(-1, "Sub Linea", True)
            lista.Insert(2, objtipotabla1)
        End If

        'For Each tt As Entidades.TipoTabla In listabtnAgregarConfig
        '    tt.Nombre = Left(tt.Nombre, Len(tt.Nombre) - 2)
        'Next

        cbo.DataSource = lista
        ListaTablas = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdTipoTabla"
        cbo.DataBind()

    End Sub

    Private Sub HabilitarBotonesConfig(ByVal btnnuevo As Boolean, ByVal btngrabar As Boolean, ByVal btneditar As Boolean, ByVal btncancelar As Boolean)
        Me.btnNuevoConfig.Visible = btnnuevo
        Me.btnGrabarConfig.Visible = btngrabar
        Me.btnEditarConfig.Visible = btneditar
        Me.btnCancelarConfig.Visible = btncancelar
    End Sub
    Private Sub HabilitarPanelesConfig(ByVal panelnuevo As Boolean, ByVal panelbusqueda As Boolean)
        Me.PanelNuevoConfig.Visible = panelnuevo
        Me.PanelBuscarConfig.Visible = panelbusqueda
    End Sub
    Private Sub ClearControlesConfig()
        cboTipoExistenciaConfig.SelectedValue = "1"
        cbo.llenarCboLineaxTipoExistencia(cboLineaConfig, CInt(0), True)
        cbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLineaConfig, CInt(0), CInt(0), True)
        Me.txtNroOrdenConfig.Text = ""
        rbEstadoConfig.SelectedValue = "1"
        rbTipoUsoConfig.SelectedValue = "OR"
        chkParteNombreConfig.Checked = False
        dgvSubLineaTipoTabla.DataBind()
    End Sub
    Private Sub btnNuevoConfig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevoConfig.Click
        hddModoConfig.Value = "1"
        HabilitarBotonesConfig(False, True, False, True)
        HabilitarPanelesConfig(True, False)
        ClearControlesConfig()
        PanelEstadoConfig.Visible = False
        'cbo.llenarCboLineaxTipoExistencia(cboLineaConfig, CInt(cboTipoExistenciaConfig.SelectedValue), True)
    End Sub
    Private Sub btnGrabarConfig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabarConfig.Click
        GrabarConfig()
    End Sub
    Private Sub GrabarConfig()
        Try
            Dim ngcSubLineaTipoTabla As New Negocio.SubLinea_TipoTabla
            Dim lSubLineaTipoTabla As List(Of Entidades.SubLinea_TipoTabla) = obtenerConfiguracionFrmGrilla()
            If ngcSubLineaTipoTabla.GrabaSubLineaTipoTabla(CInt(cboLineaConfig.SelectedValue), CInt(cboSubLineaConfig.SelectedValue), lSubLineaTipoTabla) = True Then
                Dim msjsublinea As String
                If lSubLineaTipoTabla.Count = 0 Then
                    If cboSubLineaConfig.SelectedValue = "0" Then
                        msjsublinea = "."
                    Else
                        msjsublinea = " y la Sub Linea " + "[" + CStr(cboSubLineaConfig.SelectedItem.Text) + "]" + "."
                    End If
                    objScript.mostrarMsjAlerta(Me, "se anulo con éxito la configuración para la Linea " + "[" + cboLineaConfig.SelectedItem.Text + "]" + msjsublinea)
                Else
                    If cboSubLineaConfig.SelectedValue = "0" Then
                        msjsublinea = "."
                    Else
                        msjsublinea = " y la Sub Linea " + "[" + CStr(cboSubLineaConfig.SelectedItem.Text) + "]" + "."
                    End If
                    objScript.mostrarMsjAlerta(Me, "se registro con éxito la configuración para la Linea " + "[" + cboLineaConfig.SelectedItem.Text + "]" + msjsublinea)
                End If

                HabilitarBotonesConfig(True, False, False, False)
                HabilitarPanelesConfig(False, True)
                hddModoConfig.Value = "0"
                onClick_TipoTablaSublineaConFig_Paginado()
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnEditarConfig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditarConfig.Click
        hddModoConfig.Value = "2"
        HabilitarBotonesConfig(False, True, False, True)
        HabilitarPanelesConfig(True, False)
        Me.PanelNuevoConfig.Enabled = True
        PanelEstadoConfig.Visible = False
    End Sub
    Private Sub btnCancelarConfig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelarConfig.Click
        hddModoConfig.Value = "0"
        HabilitarBotonesConfig(True, False, False, False)
        HabilitarPanelesConfig(False, True)
        ClearControlesConfig()
        cbo.llenarCboLineaxTipoExistencia(cboLineaConfig, CInt(cboTipoExistenciaConfig.SelectedValue), True)
    End Sub

    Private Function obtenerConfiguracionFrmGrilla() As List(Of Entidades.SubLinea_TipoTabla)
        Dim lista As New List(Of Entidades.SubLinea_TipoTabla)
        If cboSubLineaConfig.SelectedValue = "0" Then
            For ii As Integer = 0 To dgvSubLineaTipoTabla.Rows.Count - 1
                With dgvSubLineaTipoTabla.Rows(ii)
                    Dim obj As New Entidades.SubLinea_TipoTabla( _
                    CInt(CType(dgvSubLineaTipoTabla.Rows(ii).Cells(1).FindControl("hddIdLinea"), HiddenField).Value), _
                    CInt(0), _
                    CInt(HttpUtility.HtmlDecode(CType(dgvSubLineaTipoTabla.Rows(ii).Cells(1).FindControl("lblIdTipoTabla"), Label).Text)), CStr(HttpUtility.HtmlDecode(.Cells(2).Text)), _
                    CStr(CType(dgvSubLineaTipoTabla.Rows(ii).Cells(3).FindControl("hddTipoUso"), HiddenField).Value), _
                    CStr(HttpUtility.HtmlDecode(CType(dgvSubLineaTipoTabla.Rows(ii).Cells(3).FindControl("lblNomTipoUso"), Label).Text)), CInt(.Cells(4).Text), _
                    CType(Me.dgvSubLineaTipoTabla.Rows(ii).Cells(5).FindControl("chkParteNombre"), CheckBox).Checked, _
                    CType(Me.dgvSubLineaTipoTabla.Rows(ii).Cells(6).FindControl("chkEstado"), CheckBox).Checked, _
                    CBool(CType(Me.dgvSubLineaTipoTabla.Rows(ii).Cells(6).FindControl("hddNoDele"), HiddenField).Value), _
                    CStr(CType(Me.dgvSubLineaTipoTabla.Rows(ii).Cells(6).FindControl("hddLongitud"), HiddenField).Value))
                    lista.Add(obj)
                End With
            Next
        Else
            For i As Integer = 0 To dgvSubLineaTipoTabla.Rows.Count - 1
                With dgvSubLineaTipoTabla.Rows(i)
                    Dim obj As New Entidades.SubLinea_TipoTabla( _
                    CInt(CType(dgvSubLineaTipoTabla.Rows(i).Cells(1).FindControl("hddIdLinea"), HiddenField).Value), _
                    CInt(CType(dgvSubLineaTipoTabla.Rows(i).Cells(1).FindControl("hddIdSubLinea"), HiddenField).Value), _
                    CInt(HttpUtility.HtmlDecode(CType(dgvSubLineaTipoTabla.Rows(i).Cells(1).FindControl("lblIdTipoTabla"), Label).Text)), CStr(HttpUtility.HtmlDecode(.Cells(2).Text)), _
                    CStr(CType(dgvSubLineaTipoTabla.Rows(i).Cells(3).FindControl("hddTipoUso"), HiddenField).Value), _
                    CStr(HttpUtility.HtmlDecode(CType(dgvSubLineaTipoTabla.Rows(i).Cells(3).FindControl("lblNomTipoUso"), Label).Text)), CInt(.Cells(4).Text), _
                    CType(Me.dgvSubLineaTipoTabla.Rows(i).Cells(5).FindControl("chkParteNombre"), CheckBox).Checked, _
                    CType(Me.dgvSubLineaTipoTabla.Rows(i).Cells(6).FindControl("chkEstado"), CheckBox).Checked, _
                    CBool(CType(Me.dgvSubLineaTipoTabla.Rows(i).Cells(6).FindControl("hddNoDele"), HiddenField).Value), _
                    CStr(CType(Me.dgvSubLineaTipoTabla.Rows(i).Cells(6).FindControl("hddLongitud"), HiddenField).Value))
                    lista.Add(obj)
                End With
            Next
        End If
        Return lista
    End Function
    Private Sub addConfiguracionGrilla(ByVal idtipotabla As Integer)
        Dim objlongProd As New Entidades.Producto
        Dim ngclonprod As New Negocio.Producto
        objlongProd = ngclonprod.LongitudProducto

        Dim lista As List(Of Entidades.SubLinea_TipoTabla) = obtenerConfiguracionFrmGrilla()
        Dim MsjLongProd As String = ""
        Dim Indicador As Integer = 0
        Dim SumLong1, SumLong2 As Integer

        Dim cont As Integer = 0
        Select Case rbTipoUsoConfig.SelectedValue
            Case "OR"
                For Each sltt As Entidades.SubLinea_TipoTabla In lista
                    If sltt.TipoUso = "OR" Then
                        cont = cont + 1
                    End If
                    If cont >= 2 Then
                        objScript.mostrarMsjAlerta(Me, "2 tablas como maximo para Origen")
                        Exit Sub
                    End If
                Next
                For Each TT As Entidades.TipoTabla In ListaTablas
                    If TT.IdTipoTabla = idtipotabla Then
                        If TT.Longitud > 2 Then
                            objScript.mostrarMsjAlerta(Me, "tablas de origen debe ser menor igual 2.")
                            Exit Sub
                        End If
                    End If
                Next
            Case "CO"
                For Each sltt As Entidades.SubLinea_TipoTabla In lista
                    If sltt.TipoUso = "CO" Then
                        cont = cont + 1
                    End If
                    If cont >= 4 Then
                        objScript.mostrarMsjAlerta(Me, "4 tablas como maximo para Codigo")
                        Exit Sub
                    End If
                Next
                For Each TT As Entidades.TipoTabla In ListaTablas
                    If TT.IdTipoTabla = idtipotabla Then
                        If TT.Longitud > 2 Then
                            objScript.mostrarMsjAlerta(Me, "tablas de codigo debe ser menor igual 2.")
                            Exit Sub
                        End If
                    End If
                Next
            Case "AT"
                For Each sltt As Entidades.SubLinea_TipoTabla In lista
                    If sltt.TipoUso = "AT" Then
                        cont = cont + 1
                    End If
                    If cont >= 3 Then
                        objScript.mostrarMsjAlerta(Me, "3 tablas como maximo para Atributos")
                        Exit Sub
                    End If
                Next
                For Each TT As Entidades.TipoTabla In ListaTablas
                    If TT.IdTipoTabla = idtipotabla Then
                        If TT.Longitud > 3 Then
                            objScript.mostrarMsjAlerta(Me, "tablas de atributos debe ser menor igual 3.")
                            Exit Sub
                        End If
                    End If
                Next
        End Select


        Try
            For x As Integer = 0 To lista.Count - 1
                If lista.Item(x).TipoUso = "CO" Then
                    SumLong1 = lista.Item(x).Longitud
                    SumLong2 = SumLong1 + SumLong2
                Else
                    Indicador = 0
                End If
            Next

            Select Case rbTipoUsoConfig.SelectedValue
                Case "CO"
                    If (CInt(SumLong2) + CInt(ViewState("ValLongitud"))) > CInt(objlongProd.LongitudProd) Then
                        MsjLongProd = "La Longitud para el TIPO DE USO CÓDIGO ya fue definido [ LONGITUD INICIAL " + CStr(objlongProd.LongitudProd) + " ], \n" + _
                                "sobrepaso los limites [ LONGITUD ACTUAL " + CStr(CInt(SumLong2) + CInt(ViewState("ValLongitud"))) + " ]. \n " + _
                                "la longitud inicial no debe ser mayor a la longitud actual \n" + _
                                "verificar configuración."
                        Indicador = 1
                    End If
            End Select
        Catch ex As Exception
            'Indicador = 0
        End Try

        If Indicador = 0 Then
            Dim obj As New Entidades.SubLinea_TipoTabla(CInt(Me.cboLineaConfig.SelectedValue), CInt(cboSubLineaConfig.SelectedValue), _
                CInt(cboTipoTablaConfig.SelectedValue), CStr(HttpUtility.HtmlDecode(cboTipoTablaConfig.SelectedItem.Text)), _
                CStr(rbTipoUsoConfig.SelectedValue), CStr(HttpUtility.HtmlDecode(rbTipoUsoConfig.SelectedItem.Text)), _
                CInt(txtNroOrdenConfig.Text), CBool(chkParteNombreConfig.Checked), CBool(rbEstadoConfig.SelectedValue), False, _
                CStr(ViewState("ValLongitud")))
            lista.Add(obj)
        ElseIf Indicador = 1 Then
            objScript.mostrarMsjAlerta(Me, MsjLongProd)
            Exit Sub
        End If



        dgvSubLineaTipoTabla.DataSource = lista
        dgvSubLineaTipoTabla.DataBind()
    End Sub

    Private Sub btnAgregarConfig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregarConfig.Click
        If CInt(Me.cboLineaConfig.SelectedValue) = 0 Then
            objScript.mostrarMsjAlerta(Me, "Seleccione Linea.")
            Me.cboLineaConfig.Focus()
            Exit Sub
        End If
        If CInt(Me.cboSubLineaConfig.SelectedValue) = 0 Then
            objScript.mostrarMsjAlerta(Me, "Seleccione SubLinea.")
            Me.cboSubLineaConfig.Focus()
            Exit Sub
        End If

        addConfiguracionGrilla(CInt(cboTipoTablaConfig.SelectedValue))
        Me.txtNroOrdenConfig.Text = ""
        chkParteNombreConfig.Checked = False
    End Sub
    Public Property ListaTablas() As List(Of Entidades.TipoTabla)
        Get
            Return CType(Session("ListaTablas"), List(Of Entidades.TipoTabla))
        End Get
        Set(ByVal value As List(Of Entidades.TipoTabla))
            Session.Remove("ListaTablas")
            Session.Add("ListaTablas", value)
        End Set
    End Property

    Private Sub quitarRegistroConfiguracion()
        Dim lista As List(Of Entidades.SubLinea_TipoTabla) = obtenerConfiguracionFrmGrilla()
        lista.RemoveAt(dgvSubLineaTipoTabla.SelectedIndex)
        dgvSubLineaTipoTabla.DataSource = lista
        dgvSubLineaTipoTabla.DataBind()
    End Sub
    Private Sub dgvSubLineaTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSubLineaTipoTabla.SelectedIndexChanged
        Dim hddNoDele As HiddenField = CType(dgvSubLineaTipoTabla.SelectedRow.Cells(6).FindControl("hddNoDele"), HiddenField)
        If CBool(hddNoDele.Value) = True Then
            objScript.mostrarMsjAlerta(Me, "No puede quitar el Tipo Tabla.")
        Else
            quitarRegistroConfiguracion()
        End If
    End Sub

    Private Sub dgvListConfig_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvListConfig.SelectedIndexChanged
        Dim hddIdLinea As HiddenField = CType(dgvListConfig.SelectedRow.Cells(1).FindControl("hddIdLinea"), HiddenField)
        Dim hddIdSubLinea As HiddenField = CType(dgvListConfig.SelectedRow.Cells(2).FindControl("hddIdSubLinea"), HiddenField)
        EditarConfigTipoTabla(CInt(hddIdLinea.Value), CInt(hddIdSubLinea.Value), dgvSubLineaTipoTabla)
        HabilitarBotonesConfig(False, False, True, True)
        HabilitarPanelesConfig(True, False)
        Me.PanelNuevoConfig.Enabled = False
        Me.rbEstadoConfig.Visible = False
    End Sub
    Private Sub EditarConfigTipoTabla(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal grilla As GridView)
        Dim ngcSubLineaTipoTabla As New Negocio.SubLinea_TipoTabla
        Dim lista As List(Of Entidades.SubLinea_TipoTabla) = ngcSubLineaTipoTabla.SelectxIdTipoExistenciaxIdLineaxIdSubLinea(idlinea, idsublinea)
        cbo.llenarCboLineaxTipoExistencia(cboLineaConfig, CInt(cboTipoExistenciaConfig.SelectedValue), True)
        cboLineaConfig.SelectedValue = CStr(lista.Item(0).IdLinea)
        cbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLineaConfig, CInt(cboLineaConfig.SelectedValue), CInt(cboTipoExistenciaConfig.SelectedValue), True)
        cboSubLineaConfig.SelectedValue = CStr(lista.Item(0).IdSubLinea)
        grilla.DataSource = lista
        grilla.DataBind()
    End Sub

    Private Sub cboTipoExistenciaConfig_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistenciaConfig.SelectedIndexChanged
        cbo.llenarCboLineaxTipoExistencia(cboLineaConfig, CInt(cboTipoExistenciaConfig.SelectedValue), True)
        cbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLineaConfig, CInt(cboLineaConfig.SelectedValue), CInt(cboTipoExistenciaConfig.SelectedValue), True)

        If Me.PanelNuevoConfig.Visible = False Then
            onClick_TipoTablaSublineaConFig_Paginado()
        End If
    End Sub
    Private Sub cboLineaConfig_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLineaConfig.SelectedIndexChanged
        cbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLineaConfig, CInt(cboLineaConfig.SelectedValue), CInt(cboTipoExistenciaConfig.SelectedValue), True)

        If Me.PanelNuevoConfig.Visible = False Then
            onClick_TipoTablaSublineaConFig_Paginado()
        End If
    End Sub
    Private Sub cboSubLineaConfig_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubLineaConfig.SelectedIndexChanged
        If Me.PanelNuevoConfig.Visible = False Then
            onClick_TipoTablaSublineaConFig_Paginado()
        End If
    End Sub


#Region "Paginacion de Busqueda"

    Private Sub onClick_TipoTablaSublineaConFig_Paginado()
        ViewState.Add("_IdTipoexistencia_slconfig", CInt(cboTipoExistenciaConfig.SelectedValue))
        ViewState.Add("_IdLinea_slconfig", CInt(cboLineaConfig.SelectedValue))
        ViewState.Add("_IdSubLinea_slconfig", CInt(cboSubLineaConfig.SelectedValue))
        ViewState.Add("_Estado_slconfig", CInt(rbEstadoBusquedaConfig.SelectedValue))

        Buscar_TipoTablaSublineaConFig_Paginado(0)
    End Sub
    Private Sub Buscar_TipoTablaSublineaConFig_Paginado(ByVal tipoMov As Integer)
        TipoTablaSublineaConFig_Paginado(CInt(ViewState("_IdTipoexistencia_slconfig")), CInt(ViewState("_IdLinea_slconfig")), CInt(ViewState("_IdSubLinea_slconfig")), CInt(ViewState("_Estado_slconfig")), tipoMov, dgvListConfig)
    End Sub
    Private Sub TipoTablaSublineaConFig_Paginado(ByVal IdTipoExistencia As Integer, ByVal IdLinea As Integer, _
                           ByVal IdSubLinea As Integer, ByVal Estado As Integer, ByVal TipoMov As Integer, _
                           ByVal Grilla As GridView)

        Dim index As Integer = 0
        Select Case TipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(txt_pageIndex_slConfig.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(txt_pageIndex_slConfig.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(txt_PageIndexGo_slConfig.Text) - 1)
        End Select

        Dim lista As List(Of Entidades.SubLinea_TipoTabla) = _
           (New Negocio.SubLinea_TipoTabla).TipoTablaSublineaConFig_Paginado(IdTipoExistencia, IdLinea, IdSubLinea, Estado, index, Grilla.PageSize)


        If lista.Count > 0 Then
            txt_pageIndex_slConfig.Text = CStr(index + 1)
            Grilla.DataSource = lista
            Grilla.DataBind()
        Else
            objScript.mostrarMsjAlerta(Me, "No se hallaron registros")
        End If
    End Sub

    Private Sub btn_anterior_slConfig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_anterior_slConfig.Click
        Buscar_TipoTablaSublineaConFig_Paginado(1)
    End Sub

    Private Sub btn_siguiente_slConfig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_siguiente_slConfig.Click
        Buscar_TipoTablaSublineaConFig_Paginado(2)
    End Sub

    Private Sub btn_ir_slConfig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_ir_slConfig.Click
        Buscar_TipoTablaSublineaConFig_Paginado(3)
    End Sub


#End Region

#End Region

#Region "Mantenimiento CONFIGURACION TABLA POR VALOR"
    Private Sub HabilitarBotonesConfigxValor(ByVal btnnuevo As Boolean, ByVal btngrabar As Boolean, ByVal btneditar As Boolean, ByVal btncancelar As Boolean)
        Me.btnNuevoConfigTablaxValor.Visible = btnnuevo
        Me.btnGrabarConfigTablaxValor.Visible = btngrabar
        Me.btnEditarConfigTablaxValor.Visible = btneditar
        Me.btnCancelarConfigTablaxValor.Visible = btncancelar
    End Sub
    Private Sub HabilitarPanelesConfigxValor(ByVal panelnuevo As Boolean, ByVal panelbusqueda As Boolean)
        Me.PanelNuevoConfigTablaxValor.Visible = panelnuevo
        Me.PanelBuscarConfigTablaxValor.Visible = panelbusqueda
    End Sub
    Private Sub ClearControlesConfigxValor()
        setListaSTTVConfigxValor(listaSubLineaTipoTablaValor)
        rbEstadoBusquedaConfigxValor.SelectedValue = "1"
        dgvTipoTablaConfigxValor.DataBind()
        lblMsjRegistroTipoTabla.Text = ""
        hddModoConfigValor.Value = "0"
    End Sub

    Private Sub btnNuevoConfigTablaxValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevoConfigTablaxValor.Click
        hddModoConfigValor.Value = "1"
        HabilitarBotonesConfigxValor(False, True, False, True)
        HabilitarPanelesConfigxValor(True, False)
        ClearControlesConfigxValor()
        Panel6.Visible = False
        Me.PanelNuevoConfigTablaxValor.Enabled = True
    End Sub
    Private Sub btnGrabarConfigTablaxValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabarConfigTablaxValor.Click
        GrabarConfigxValor()
    End Sub
    Private Sub GrabarConfigxValor()
        Try
            Dim ngcSubLineaTipoTablaValor As New Negocio.SubLinea_TipoTablaValor
            Dim lista As List(Of Entidades.SubLinea_TipoTablaValor) = getListaSTTVConfigxValor()

            If ngcSubLineaTipoTablaValor.GrabaSubLineaTipoTablaValor(CInt(cboSubLineaConfigxValor.SelectedValue), CInt(cboTipoTablaConfigxValor.SelectedValue), lista) Then
                If lista.Count = 0 Then
                    objScript.mostrarMsjAlerta(Me, "se anulo con éxito la configuración.")
                Else
                    objScript.mostrarMsjAlerta(Me, "se registro con éxito la configuración.")
                End If
                HabilitarBotonesConfigxValor(True, False, False, False)
                HabilitarPanelesConfigxValor(False, True)
                'SelectIdLineaxIdSubLineaxEstado(CInt(cboLineaConfigxValor.SelectedValue), CInt(cboSubLineaConfigxValor.SelectedValue), CInt(rbEstadoBusquedaConfigxValor.SelectedValue), dgvBusquedaConfigxValor, CInt(cboTipoExistenciaConfigxValor.SelectedValue))
                OnClick_ConfigTablaValor_Paginado()
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnEditarConfigTablaxValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditarConfigTablaxValor.Click
        hddModoConfigValor.Value = "2"
        HabilitarBotonesConfigxValor(False, True, False, True)
        HabilitarPanelesConfigxValor(True, False)
        Me.PanelNuevoConfigTablaxValor.Enabled = True
    End Sub
    Private Sub btnCancelarConfigTablaxValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelarConfigTablaxValor.Click
        hddModoConfigValor.Value = "0"
        HabilitarBotonesConfigxValor(True, False, False, False)
        HabilitarPanelesConfigxValor(False, True)
        ClearControlesConfigxValor()
    End Sub
    'Private Sub SelectAllTipoTablaValorxEstado(ByVal idsublinea As Integer, ByVal idtipotabla As Integer, ByVal estado As Integer, ByVal grilla As GridView)
    '    Dim ngcTipoTablaValor As New Negocio.TipoTablaValor
    '    Dim listaTTV As List(Of Entidades.TipoTablaValor) = ngcTipoTablaValor.SelectAllxIdTipoTablaxEstado(idsublinea, idtipotabla, estado)
    '    grilla.DataSource = listaTTV
    '    grilla.DataBind()
    'End Sub
    Private Sub cboTipoTablaConfigxValor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoTablaConfigxValor.SelectedIndexChanged
        OnClick_cboTipoTablaConfigxValor()
    End Sub
    Private Sub OnClick_cboTipoTablaConfigxValor()

        If cboSubLineaConfigxValor.SelectedValue = "0" Then
            lblMsjRegistroTipoTabla.Text = ""
            objScript.mostrarMsjAlerta(Me, "Seleccione Sub Linea.")
            Exit Sub
        End If

        txtBuscarCttv.Text = ""
        dgvTipoTablaConfigxValor.DataBind()

    End Sub
    Private Function getListaSTTVConfigxValor() As List(Of Entidades.SubLinea_TipoTablaValor)
        Return CType(Session.Item("listaSTTVConfigxValor"), List(Of Entidades.SubLinea_TipoTablaValor))
    End Function
    Private Sub setListaSTTVConfigxValor(ByVal lista As List(Of Entidades.SubLinea_TipoTablaValor))
        Session.Remove("listaSTTVConfigxValor")
        Session.Add("listaSTTVConfigxValor", lista)
    End Sub
    Private Sub addGrillaSubLineaTipoTablaValor()
        Try
            Me.listaSubLineaTipoTablaValor = Me.getListaSTTVConfigxValor
            For i As Integer = 0 To dgvTipoTablaConfigxValor.Rows.Count - 1
                Dim chbAdd As CheckBox = CType(Me.dgvTipoTablaConfigxValor.Rows(i).Cells(0).FindControl("ChkSeleccionar"), CheckBox)
                If chbAdd.Checked Then
                    Dim obj As New Entidades.SubLinea_TipoTablaValor
                    obj.IdSubLinea = CInt(cboSubLineaConfigxValor.SelectedValue)
                    obj.NomSubLinea = HttpUtility.HtmlDecode(CStr(cboSubLineaConfigxValor.SelectedItem.Text).Trim)
                    obj.IdTipoTabla = CInt(cboTipoTablaConfigxValor.SelectedValue)
                    obj.NomTipoTabla = HttpUtility.HtmlDecode(CStr(cboTipoTablaConfigxValor.SelectedItem.Text).Trim)
                    obj.Codigo = CStr(dgvTipoTablaConfigxValor.Rows(i).Cells(1).Text)
                    obj.IdTipoTablaValor = CInt(CType(dgvTipoTablaConfigxValor.Rows(i).Cells(2).FindControl("hddIdTipoTablaValor"), HiddenField).Value)
                    obj.Nombre = HttpUtility.HtmlDecode(CStr(CType(dgvTipoTablaConfigxValor.Rows(i).Cells(2).FindControl("lblNomTipoTablaValor"), Label).Text).Trim)
                    obj.TipoUso = HttpUtility.HtmlDecode(CStr(dgvTipoTablaConfigxValor.Rows(i).Cells(3).Text).Trim)
                    obj.Estado = CBool(CType(dgvTipoTablaConfigxValor.Rows(i).Cells(4).FindControl("hddEstado"), HiddenField).Value)

                    obj.Indicador = False

                    Me.listaSubLineaTipoTablaValor.Add(obj)
                End If
            Next
            dgvListConfigxValor.DataSource = Me.listaSubLineaTipoTablaValor
            dgvListConfigxValor.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub quitarSubLineaTipoTablaValor(ByVal Index As Integer)
        Try

            Me.listaSubLineaTipoTablaValor = Me.getListaSTTVConfigxValor

            If Me.listaSubLineaTipoTablaValor(Index).IdTipoTablaValor > 0 And _
                    Me.listaSubLineaTipoTablaValor(Index).IdSubLinea > 0 And _
                    Me.listaSubLineaTipoTablaValor(Index).IdTipoTabla > 0 Then

                If (New Negocio.SubLinea_TipoTablaValor).AnularSubLineaTipoTablaValor(Me.listaSubLineaTipoTablaValor) Then
                    '  objScript.mostrarMsjAlerta(Me, "La operación finalizó con éxito.")
                End If

            End If

            Me.listaSubLineaTipoTablaValor.RemoveAt(Index)
            Me.setListaSTTVConfigxValor(Me.listaSubLineaTipoTablaValor)

            dgvListConfigxValor.DataSource = Me.listaSubLineaTipoTablaValor
            dgvListConfigxValor.DataBind()

        Catch ex As Exception
        Finally
        End Try
    End Sub
    Private Sub btnAgregarConfiggxValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregarConfiggxValor.Click
        addGrillaSubLineaTipoTablaValor()
        Panel6.Visible = True
    End Sub

    'Private Sub SelectIdLineaxIdSubLineaxEstado(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal estado As Integer, ByVal grilla As GridView, ByVal idtipoexistencia As Integer)
    '    Dim ngcSubLineaTipoTablaValor As New Negocio.SubLinea_TipoTablaValor
    '    Dim lista As List(Of Entidades.SubLinea_TipoTablaValor) = ngcSubLineaTipoTablaValor.SelectAllxIdLineaxIdSubLineaxEstado(idlinea, idsublinea, estado, idtipoexistencia)
    '    grilla.DataSource = lista
    '    grilla.DataBind()
    'End Sub

    Private Sub dgvBusquedaConfigxValor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvBusquedaConfigxValor.SelectedIndexChanged
        Dim hddIdLinea As HiddenField = CType(dgvBusquedaConfigxValor.SelectedRow.Cells(1).FindControl("hddIdLinea"), HiddenField)
        Dim hddIdSubLinea As HiddenField = CType(dgvBusquedaConfigxValor.SelectedRow.Cells(2).FindControl("hddIdSubLinea"), HiddenField)
        EditarSubLineaTipoTablaValor(CInt(hddIdLinea.Value), CInt(hddIdSubLinea.Value), dgvListConfigxValor)
        HabilitarBotonesConfigxValor(False, False, True, True)
        HabilitarPanelesConfigxValor(True, False)
        Me.PanelNuevoConfigTablaxValor.Visible = True
        Me.PanelNuevoConfigTablaxValor.Enabled = False
        Panel6.Visible = True

        dgvTipoTablaConfigxValor.DataBind()
        lblMsjRegistroTipoTabla.Text = ""
    End Sub
    Private Sub EditarSubLineaTipoTablaValor(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal grilla As GridView)
        Dim ngcSubLineaTipoTablaValor As New Negocio.SubLinea_TipoTablaValor

        If cboLineaConfigxValor.Items.FindByValue(CStr(idlinea)) Is Nothing Then
            cbo.llenarCboLineaxTipoExistencia(cboLineaConfigxValor, CInt(cboTipoExistenciaConfigxValor.SelectedValue), True)
        End If
        cboLineaConfigxValor.SelectedValue = CStr(idlinea)

        If cboSubLineaConfigxValor.Items.FindByValue(CStr(idsublinea)) Is Nothing Then
            cbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLineaConfigxValor, CInt(cboLineaConfigxValor.SelectedValue), CInt(cboTipoExistenciaConfigxValor.SelectedValue), True)
        End If
        cboSubLineaConfigxValor.SelectedValue = CStr(idsublinea)

        cbo.LlenarCboTipoTabla2(1, idlinea, idsublinea, cboTipoTablaConfigxValor, False)

    End Sub

    Private Sub verCtrlBusqTTv(ByVal ver As Boolean)
        Me.txtBuscarCttv.Visible = ver
        Me.btnBuscarCttv.Visible = ver
    End Sub

    Private Sub cboTipoExistenciaConfigxValor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistenciaConfigxValor.SelectedIndexChanged
        cbo.llenarCboLineaxTipoExistencia(cboLineaConfigxValor, CInt(cboTipoExistenciaConfigxValor.SelectedValue), True)
        cbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLineaConfigxValor, CInt(0), CInt(0), True)

        If PanelNuevoConfigTablaxValor.Visible = False Then
            OnClick_ConfigTablaValor_Paginado()
        End If

        cboTipoTablaConfigxValor.Items.Clear()
        dgvTipoTablaConfigxValor.DataBind()
        dgvListConfigxValor.DataBind()

    End Sub
    Private Sub cboLineaConfigxValor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLineaConfigxValor.SelectedIndexChanged
        cbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cboSubLineaConfigxValor, CInt(cboLineaConfigxValor.SelectedValue), CInt(cboTipoExistenciaConfigxValor.SelectedValue), True)

        If PanelNuevoConfigTablaxValor.Visible = False Then
            OnClick_ConfigTablaValor_Paginado()
        End If
        cboTipoTablaConfigxValor.Items.Clear()
        dgvTipoTablaConfigxValor.DataBind()
        dgvListConfigxValor.DataBind()

    End Sub
    Private Sub cboSubLineaConfigxValor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubLineaConfigxValor.SelectedIndexChanged

        If PanelNuevoConfigTablaxValor.Visible = False Then
            OnClick_ConfigTablaValor_Paginado()
        End If

        cbo.LlenarCboTipoTabla2(1, CInt(cboLineaConfigxValor.SelectedValue), CInt(cboSubLineaConfigxValor.SelectedValue), cboTipoTablaConfigxValor, True)

    End Sub


#Region "Paginacion de Registro de Tabla"

    Private Sub OnClick_ConfigTablaValor_Paginado()
        ViewState.Add("_LineaConfigxValor", CInt(cboLineaConfigxValor.SelectedValue))
        ViewState.Add("_SubLineaConfigxValor", CInt(cboSubLineaConfigxValor.SelectedValue))
        ViewState.Add("_EstadoBusquedaConfigxValor", CInt(rbEstadoBusquedaConfigxValor.SelectedValue))
        ViewState.Add("_TipoExistenciaConfigxValor", CInt(cboTipoExistenciaConfigxValor.SelectedValue))

        Buscar_ConfigTablaValor_Paginado(0)
    End Sub

    Private Sub Buscar_ConfigTablaValor_Paginado(ByVal TipoMov As Integer)
        ConfigTablaValor_Paginado(CInt(ViewState("_LineaConfigxValor")), CInt(ViewState("_SubLineaConfigxValor")), CInt(ViewState("_EstadoBusquedaConfigxValor")), CInt(ViewState("_TipoExistenciaConfigxValor")), TipoMov, dgvBusquedaConfigxValor)
    End Sub

    Private Sub ConfigTablaValor_Paginado(ByVal idlinea As Integer, _
                         ByVal idsublinea As Integer, ByVal Estado As Integer, ByVal idtipoexistencia As Integer, _
                         ByVal TipoMov As Integer, ByVal Grilla As GridView)

        Dim index As Integer = 0
        Select Case TipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(txt_PageIndex_ConfigValor.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(txt_PageIndex_ConfigValor.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(txt_PageIndexGo_ConfigValor.Text) - 1)
        End Select

        Dim lista As List(Of Entidades.SubLinea_TipoTablaValor) = _
        (New Negocio.SubLinea_TipoTablaValor).ConfigTablaValor_Paginado(idlinea, idsublinea, Estado, idtipoexistencia, index, Grilla.PageSize)

        If lista.Count > 0 Then
            PanelBuscarConfigTablaxValor.Visible = True
            txt_PageIndex_ConfigValor.Text = CStr(index + 1)
            Grilla.DataSource = lista
            Grilla.DataBind()
        Else
            objScript.mostrarMsjAlerta(Me, "No se hallaron registros")
        End If

    End Sub

    Private Sub btn_anterior_ConfigValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_anterior_ConfigValor.Click
        Buscar_ConfigTablaValor_Paginado(1)
    End Sub

    Private Sub btn_siguiente_ConfigValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_siguiente_ConfigValor.Click
        Buscar_ConfigTablaValor_Paginado(2)
    End Sub

    Private Sub btn_Ir_ConfigValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Ir_ConfigValor.Click
        Buscar_ConfigTablaValor_Paginado(3)
    End Sub

    Protected Sub btnBuscarCttv_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarCttv.Click
        If cboSubLineaConfigxValor.SelectedValue = "0" Then
            objScript.mostrarMsjAlerta(Me, "Seleccione una Sublinea.")
        End If
        If cboTipoTablaConfigxValor.SelectedValue = "0" Or cboTipoTablaConfigxValor.SelectedValue = "" Then
            objScript.mostrarMsjAlerta(Me, "Seleccione una TipoTabla.")
        End If
        OnClick_SetTablaValor_Paginado()
    End Sub

    Private Sub OnClick_SetTablaValor_Paginado()
        ViewState.Add("_SetSubLinea", CInt(cboSubLineaConfigxValor.SelectedValue))
        ViewState.Add("_SetTipoTablaValor", CInt(cboTipoTablaConfigxValor.SelectedValue))
        ViewState.Add("_SetNombre", CStr(txtBuscarCttv.Text.Trim))

        Buscar_SetTablaValor_Paginado(0)
    End Sub
    Private Sub Buscar_SetTablaValor_Paginado(ByVal TipoMov As Integer)
        SetTablaValor_Paginado(CInt(ViewState("_SetSubLinea")), CInt(ViewState("_SetTipoTablaValor")), CStr(ViewState("_SetNombre")), TipoMov, dgvTipoTablaConfigxValor)
    End Sub
    Private Sub SetTablaValor_Paginado(ByVal idsublinea As Integer, _
                                       ByVal IdTipoTabla As Integer, ByVal descripcion As String, _
                                       ByVal TipoMov As Integer, ByVal Grilla As GridView)

        Dim index As Integer = 0
        Select Case TipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(txt_PageIndex_SetValor.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(txt_PageIndex_SetValor.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(txt_PageIndexGo_SetValor.Text) - 1)
        End Select

        Dim listaTTV As List(Of Entidades.TipoTablaValor) = (New Negocio.TipoTablaValor).SelectAllxIdTipoTablaxnombre(idsublinea, IdTipoTabla, descripcion, index, Grilla.PageSize)

        If listaTTV.Count > 0 Then
            txtBuscarCttv.Text = ""
            txt_PageIndex_SetValor.Text = CStr(index + 1)
            Grilla.DataSource = listaTTV
            Grilla.DataBind()
        Else
            objScript.mostrarMsjAlerta(Me, "No se hallaron registros")
        End If

    End Sub
    Protected Sub btn_anterior_SetValor_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_anterior_SetValor.Click
        Buscar_SetTablaValor_Paginado(1)
    End Sub

    Protected Sub btn_siguiente_SetValor_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_siguiente_SetValor.Click
        Buscar_SetTablaValor_Paginado(2)
    End Sub

    Protected Sub btn_Ir_SetValor_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Ir_SetValor.Click
        Buscar_SetTablaValor_Paginado(3)
    End Sub
#End Region

#End Region

    Private Sub cboTipoTablaConfig_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoTablaConfig.SelectedIndexChanged
        Try
            Dim ngcTipoTabla As New Negocio.TipoTabla
            Dim objTipoTabla As New Entidades.TipoTabla
            objTipoTabla = ngcTipoTabla.LongitudTablasGeneral(CInt(cboTipoTablaConfig.SelectedValue), CInt(Me.cboLineaConfig.SelectedValue), CInt(Me.cboSubLineaConfig.SelectedValue))
            ViewState.Add("ValLongitud", objTipoTabla.Longitud)
            'objScript.mostrarMsjAlerta(Me, CStr(ViewState("ValLongitud")))
        Catch ex As Exception
        Finally
        End Try
    End Sub

    Private Sub btnBuscarTValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarTValor.Click

        ViewState.Add("DescripcionTValor", Me.txtBuscarTValor.Text.Trim)

        BuscarTValor(0)
    End Sub
    Private Sub BuscarTValor(ByVal TipoMov As Integer)



        Dim index As Integer = 0
        Select Case TipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(txt_PageIndex_ConfigValor.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(txt_PageIndex_ConfigValor.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(txt_PageIndexGo_ConfigValor.Text) - 1)
        End Select

        listaSubLineaTipoTablaValor = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLinea(CInt(cboLineaConfigxValor.SelectedValue), CInt(cboSubLineaConfigxValor.SelectedValue), CInt(cboTipoTablaConfigxValor.SelectedValue), index, 20, CStr(ViewState("DescripcionTValor")))
        setListaSTTVConfigxValor(listaSubLineaTipoTablaValor)

        dgvListConfigxValor.DataSource = listaSubLineaTipoTablaValor
        dgvListConfigxValor.DataBind()


    End Sub



    Private Sub btnAnteriorTValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnteriorTValor.Click
        BuscarTValor(1)
    End Sub

    Private Sub btnSiguienteTValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguienteTValor.Click
        BuscarTValor(2)
    End Sub

    Private Sub btnIrTValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIrTValor.Click
        BuscarTValor(3)
    End Sub

    Protected Sub lkbQuitar_Click(ByVal sender As Object, ByVal e As EventArgs)
        quitarDetalleTablaValor(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub quitarDetalleTablaValor(ByVal Index As Integer)
        Dim hddNoDele As HiddenField = CType(dgvListConfigxValor.Rows(Index).FindControl("hddNoDele"), HiddenField)
        If CBool(hddNoDele.Value) = True Then
            objScript.mostrarMsjAlerta(Me, "No puede quitar el Tipo Tabla y el registro seleccionado.")
        Else
            quitarSubLineaTipoTablaValor(Index)
        End If

        If dgvListConfigxValor.Rows.Count = 0 Then
            Panel6.Visible = False
        Else
            Panel6.Visible = True
        End If
    End Sub

End Class