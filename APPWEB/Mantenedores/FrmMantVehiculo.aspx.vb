﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



Partial Public Class FrmMantVehiculo
    Inherits System.Web.UI.Page
#Region "variables"
    Private objScript As New ScriptManagerClass
    Private colorReadOnly As Drawing.Color = Drawing.Color.LightGray
    Private colorHabilitado As Drawing.Color = Drawing.Color.White
    Private ListaVehiculo As List(Of Entidades.Vehiculo)
    Private Enum operativo
        GuardarNuevo = 1
        Actualizar = 2
        Ninguno = 3
    End Enum

#End Region

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ConfigurarDatos()
                CargaEstado()
                GrillaVehiculos()
                btnCancelar.Visible = False
                btnGuardar.Visible = False
                lbl_MensajeBusq.Visible = False
                Panel_Vehiculo.Visible = False
                Panel_Busqueda.Visible = True
                gvVehiculos.Visible = True
                CargardatosTipoExistencia(Me.cboTipoExistencia, CStr(True))
                cboTipoExistencia_SelectedIndexChanged(sender, e)
                ViewState.Add("operativo", operativo.Ninguno)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    'Cargar el combo TIPO EXISTENCIA
    Private Sub CargardatosTipoExistencia(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim nTipoExistencia As New Negocio.TipoExistencia
        Dim nCombo As New Combo
        nCombo.LlenarCboTipoExistenciaxIdVehiculo(combo)
    End Sub
    Private cbo As New Combo
    'Cargas el combo LINEA dependiendo del combo TIPO EXISTENCIA
    Protected Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTipoExistencia.SelectedIndexChanged
        cbo.llenarCboLineaxTipoExistencia(cboLinea, CInt(cboTipoExistencia.SelectedValue), True)
    End Sub
    'Cargar el combo Sub Linea
    Private Sub cargarDatosSubLinea(ByVal cboVehic As DropDownList, ByVal idLinea As Integer, ByVal idTipoExistencia As Integer)
        Dim nSubLinea As New Negocio.SubLinea
        Dim nComboBox As New Negocio.ComboBox
        nComboBox.llenarComboBoxSubLinea(cboVehic)
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectActivoxLineaxTipoExistencia(idLinea, idTipoExistencia)
        cboVehic.DataSource = lista
        cboVehic.DataBind()
    End Sub
    ''Cargas el combo SUB LINEA dependiendo del combo LINEA
    Protected Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboLinea.SelectedIndexChanged
        cargarDatosSubLinea((Me.cboSubLinea), CInt(cboLinea.SelectedValue), CInt(cboTipoExistencia.SelectedValue))
    End Sub
    'Cargar el combo ESTADO
    Private Sub CargaEstado()
        Try
            Dim objComboEstado As List(Of Entidades.EstadoComboVehiculo) = (New Negocio.Vehiculo).SelectCboEstadoVehiculo
            cboEstado.DataSource = objComboEstado
            cboEstado.DataValueField = "IdCboVehiculo"
            cboEstado.DataTextField = "NombreEstadoVahiculo"
            cboEstado.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    '**** Cargar a la Grilla
    Private Sub GrillaVehiculos()
        Try
            Dim objVehiculo As List(Of Entidades.Vehiculo) = (New Negocio.Vehiculo).SelectAllVehiculosXtipoexistenciaXlineaXsublinea
            gvVehiculos.DataSource = objVehiculo
            gvVehiculos.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub Clear()
        TxtPlaca.Text = ""
        TxtModelo.Text = ""
        TxtConstancia.Text = ""
        TxtConfeccion.Text = ""
        TxtTara.Text = ""
        V_txtTextoBusqueda.Text = ""
    End Sub
    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        Try
            Clear()
            limpiarCombo()
            ViewState.Add("operativo", operativo.GuardarNuevo)
            CargardatosTipoExistencia(Me.cboTipoExistencia, CStr(True))
            cboTipoExistencia_SelectedIndexChanged(sender, e)
            Me.btnNuevo.Visible = False
            btnGuardar.Visible = True
            btnCancelar.Visible = True
            Panel_Vehiculo.Visible = True
            Panel_Busqueda.Enabled = False
            gvVehiculos.Enabled = False
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        Clear()
        ViewState.Add("operativo", operativo.Ninguno)
        Me.btnNuevo.Visible = True
        btnGuardar.Visible = False
        btnCancelar.Visible = False
        Panel_Vehiculo.Visible = False
        Panel_Busqueda.Visible = True
        gvVehiculos.Visible = True
        Panel_Busqueda.Enabled = True
        gvVehiculos.Enabled = True
        lbl_MensajeBusq.Visible = False
        GrillaVehiculos()
        'No_Visible()
    End Sub
    '**** Boton Guardar  ---> INSERTAR Vehiculo,EstadoProd y Sub Línea
    '                    ---> ACTUALIZA Vehiculo,EstadoProd y Sub Línea
    Private Sub ValidarGuardar()
        btnGuardar.Visible = False
        btnCancelar.Visible = False
        btnNuevo.Visible = True
        Panel_Busqueda.Visible = True
        Panel_Busqueda.Enabled = True
        Panel_Vehiculo.Visible = False
        Panel_Vehiculo.Enabled = True
        gvVehiculos.Visible = True
        gvVehiculos.Enabled = True
    End Sub
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Try
            Select Case CInt(ViewState("operativo"))

                Case operativo.GuardarNuevo

                    Dim objVehiculo As New Entidades.Vehiculo
                    '*** los combos de Tipo Existencia y Línea son referenciales para llegar a SUB LÍNEA
                    objVehiculo.IdEstadoProd = CInt(cboEstado.SelectedValue)
                    objVehiculo.IdSubLinea = CInt(cboSubLinea.SelectedValue)
                    objVehiculo.Placa = TxtPlaca.Text.Trim
                    objVehiculo.Modelo = CStr(IIf(TxtModelo.Text = "", "----", TxtModelo.Text.Trim))
                    objVehiculo.ConstanciaIns = CStr(IIf(TxtConstancia.Text = "", "----", TxtConstancia.Text.Trim))
                    objVehiculo.ConfVeh = CStr(IIf(TxtConfeccion.Text = "", "----", TxtConfeccion.Text.Trim))
                    objVehiculo.Tara = CDec(IIf(TxtTara.Text = "", 0.0, TxtTara.Text))
                    objVehiculo.veh_Capacidad = Val(Me.txtCapacidad.Text)
                    If (New Negocio.Vehiculo).InsertaVehiculoValidadoxPlaca(objVehiculo) > 0 Then
                        objScript.mostrarMsjAlerta(Me, "La operación Guardar, se finalizó con éxito.")
                    End If
                    GrillaVehiculos()
                    ViewState.Add("operativo", operativo.Ninguno)
                    ValidarGuardar()

                Case operativo.Actualizar
                    Dim objVehiculo As New Entidades.Vehiculo
                    ''los combos de Tipo Existencia y Línea son referenciales para llegar a SUB LÍNEA
                    objVehiculo.IdEstadoProd = CInt(cboEstado.SelectedValue)
                    objVehiculo.IdSubLinea = CInt(cboSubLinea.SelectedValue)
                    objVehiculo.Placa = CStr(IIf(TxtPlaca.Text = "", 0, TxtPlaca.Text))
                    objVehiculo.Modelo = CStr(IIf(TxtModelo.Text = "", "----", TxtModelo.Text.Trim))
                    objVehiculo.ConstanciaIns = CStr(IIf(TxtConstancia.Text = "", "----", TxtConstancia.Text.Trim))
                    objVehiculo.ConfVeh = CStr(IIf(TxtConfeccion.Text = "", "----", TxtConfeccion.Text.Trim))
                    objVehiculo.Tara = CDec(IIf(TxtTara.Text = "", 0.0, TxtTara.Text))
                    objVehiculo.veh_Capacidad = CDec(IIf(txtCapacidad.Text = "", 0.0, txtCapacidad.Text))
                    objVehiculo.IdProducto = CInt(ViewState("idproducto"))

                    If (New Negocio.Vehiculo).ActualizaVehiculoValidadoxPlaca(objVehiculo) = 0 Then
                        objScript.mostrarMsjAlerta(Me, "Problemas en la Actualización del Regitro.")
                    Else
                        objScript.mostrarMsjAlerta(Me, "La operación Actualizar, se finalizó con éxito.")
                    End If
                    GrillaVehiculos()
                    ViewState.Add("operativo", operativo.Ninguno)
                    ValidarGuardar()
            End Select
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    '**** ELIMINAR ---> Vehiculo pero si tiene el mismo idVehiculo de la tabla DOCUMENTO no se eliminara
    Protected Sub Click_Eliminar(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lb As LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lb.NamingContainer, GridViewRow)

            HDD_IdProducto.Value = CType(fila.Cells(5).FindControl("HDD_PlacaxIdProducto"), HiddenField).Value.Trim

            If (New Negocio.Vehiculo).DeleteVehiculoxIdDocumento(CInt(HDD_IdProducto.Value)) = 1 Then

                objScript.mostrarMsjAlerta(Me, "La Eliminación se ejecuto con éxito.")

            Else
                objScript.mostrarMsjAlerta(Me, "El Vehículo no puede ser eliminado porque posee registros relacionados en la Tabla Documento.")
            End If
            GrillaVehiculos()
            ViewState.Add("operativo", operativo.Ninguno)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la ejecución.")
        End Try
    End Sub
    '**** EDITAR
    Private Sub limpiarCombo()
        'cboTipoExistencia.SelectedIndex = 0
        cboLinea.Items.Clear()
        cboSubLinea.Items.Clear()
    End Sub
    Protected Sub Click_Editar(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lb As LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lb.NamingContainer, GridViewRow)
            HDD_IdProducto.Value = CType(fila.Cells(5).FindControl("HDD_PlacaxIdProducto"), HiddenField).Value.Trim
            ViewState("idproducto") = HDD_IdProducto.Value
            limpiarCombo()

            'La declaracion "Fila" es igual q pongamos "gvVehiculos(cbo)"
            'La declaracion "Fila" es igual q pongamos "gvVehiculos.SelectedRow(Txt)"
            'Lo pongo de esta manera porq en mi grilla tengo "Eliminar" y "Editar"
            Panel_Vehiculo.Visible = True
            Panel_Busqueda.Enabled = False
            gvVehiculos.Enabled = False
            btnNuevo.Visible = False
            btnGuardar.Visible = True
            btnCancelar.Visible = True



            cboTipoExistencia.SelectedValue = CStr(IIf(CType(fila.Cells(2).FindControl("HDD_IdTipoExistencia"), HiddenField).Value.Trim = "" _
                                                         , "0", CType(fila.Cells(2).FindControl("HDD_IdTipoExistencia"), HiddenField).Value.Trim))
            cboTipoExistencia_SelectedIndexChanged(sender, e)


            cboLinea.SelectedValue = CStr(IIf(CType(fila.Cells(3).FindControl("hdd_IdLinea"), HiddenField).Value.Trim = "" _
                                                        , "0", CType(fila.Cells(3).FindControl("hdd_IdLinea"), HiddenField).Value.Trim))
            cboLinea_SelectedIndexChanged(sender, e)

            cboSubLinea.SelectedValue = CType(fila.Cells(4).FindControl("hdd_IdSubLinea"), HiddenField).Value.Trim
            cboEstado.SelectedValue = CType(fila.Cells(10).FindControl("hdd_IdEstadoProd"), HiddenField).Value.Trim

            TxtPlaca.Text = CStr(IIf(CType(fila.Cells(5).FindControl("Lbl_NomPlaca"), Label).Text.Trim = "----", "", CType(fila.Cells(5).FindControl("Lbl_NomPlaca"), Label).Text.Trim))
            TxtModelo.Text = CStr(IIf(HttpUtility.HtmlDecode(fila.Cells(6).Text).Trim = "----", "", HttpUtility.HtmlDecode(fila.Cells(6).Text).Trim))
            TxtConstancia.Text = CStr(IIf(HttpUtility.HtmlDecode(fila.Cells(7).Text).Trim = "----", "", HttpUtility.HtmlDecode(fila.Cells(7).Text).Trim))
            TxtConfeccion.Text = CStr(IIf(HttpUtility.HtmlDecode(fila.Cells(8).Text).Trim = "----", "", HttpUtility.HtmlDecode(fila.Cells(8).Text).Trim))
            TxtTara.Text = CStr(IIf(HttpUtility.HtmlDecode(fila.Cells(9).Text).Trim = "0.000000", "", HttpUtility.HtmlDecode(fila.Cells(9).Text).Trim))
            txtCapacidad.Text = CStr(IIf(HttpUtility.HtmlDecode(fila.Cells(11).Text).Trim = "0.000000", "", HttpUtility.HtmlDecode(fila.Cells(11).Text).Trim))
            HDD_IdProducto.Value = CType(fila.Cells(5).FindControl("HDD_PlacaxIdProducto"), HiddenField).Value.Trim
            ViewState.Add("operativo", operativo.Actualizar)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la edición del Regitro.")
        End Try
    End Sub
    '**** Texto de BUSQUEDA
    Private Sub mostrarMensajeBusqueda(ByVal isVisible As Boolean)
        lbl_MensajeBusq.Visible = isVisible
    End Sub
    Private Sub cargarDatosGrilla_Vehiculo()
        Dim nGrilla As New Grilla
        Dim nVehiculo As New Negocio.Vehiculo
        Dim lista As List(Of Entidades.Vehiculo)
        'x Placa, x Modelo, xConstancia, x EstadoProd
        If cboFiltro.SelectedIndex = 0 Then lista = nVehiculo.SelectGrillaVehiculo_Buscar(V_txtTextoBusqueda.Text.Trim, "", "", "")
        If cboFiltro.SelectedIndex = 1 Then lista = nVehiculo.SelectGrillaVehiculo_Buscar("", V_txtTextoBusqueda.Text.Trim, "", "")
        If cboFiltro.SelectedIndex = 2 Then lista = nVehiculo.SelectGrillaVehiculo_Buscar("", "", V_txtTextoBusqueda.Text.Trim, "")
        If cboFiltro.SelectedIndex = 3 Then lista = nVehiculo.SelectGrillaVehiculo_Buscar("", "", "", V_txtTextoBusqueda.Text.Trim)
        If lista.Count = 0 Then
            mostrarMensajeBusqueda(True)
        Else
            mostrarMensajeBusqueda(False)
        End If
        gvVehiculos.DataSource = lista
        gvVehiculos.DataBind()
    End Sub
    Protected Sub E_btnFiltrar_B_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles E_btnFiltrar_B.Click
        cargarDatosGrilla_Vehiculo()
        btnCancelar.Visible = True
    End Sub

End Class
