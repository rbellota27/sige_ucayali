﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class frmProgramacionPedido
    Inherits System.Web.UI.Page


#Region "variable"
    Private objScript As New ScriptManagerClass
    Private fecha As Negocio.FechaActual
    Private Enum operativo
        Ninguno = 0
        GuardarNuevo = 1
        Actualizar = 2
        Buscar = 3
    End Enum
    Private objProgramacionPedido As Entidades.ProgramacionPedido
#End Region

#Region "procedimiento"


    Private Sub llenarCboSemana(ByVal dl As DropDownList, ByVal idyear As Integer, ByVal tipoOperativo As Integer)
        dlweek.Items.Clear()
        Dim idSemana() As String

        If tipoOperativo = operativo.Ninguno Or tipoOperativo = operativo.GuardarNuevo Then
            idSemana = (New Negocio.ProgramacionPedido).ListarIdSemanaxIdYear(idyear).Split(CChar(","))
            dlweek.Items.Add(New ListItem(idSemana(0), idSemana(0)))
            tbfechaini.Text = idSemana(1)
            tbfechafin.Text = CStr(DateAdd(DateInterval.Day, 7, CDate(idSemana(1))))
            tbutilday.Text = "0"
            dlmonth.SelectedValue = CDate(tbfechafin.Text).Month.ToString
            tbutilday.Focus()
        End If

        If tipoOperativo = operativo.Buscar Then
            idSemana = (New Negocio.ProgramacionPedido).ListarProgramacionPedido(idyear).Split(CChar(","))
            If idSemana(0) <> "" Then
                For p As Integer = 0 To idSemana.Length - 1
                    dlweek.Items.Add(New ListItem(idSemana(p), idSemana(p)))
                Next
            End If
            dlweek.Items.Insert(0, New ListItem("-Seleccionar-", "0"))
        End If
    End Sub

    Private Sub cargarAlIniciar()
        fecha = New Negocio.FechaActual
        Dim val_fecha As Date = fecha.SelectFechaActual

        Dim recorreYear% = (val_fecha.Year - 3)
        Dim finrecorreYear% = (val_fecha.Year + 2)
        For x As Integer = recorreYear To finrecorreYear
            dlyear.Items.Add(New ListItem(CStr(x), CStr(x)))
        Next
        dlyear.Items.Insert(0, New ListItem("-Seleccionar-", "0"))

        For x As Integer = 1 To 12
            dlmonth.Items.Add(New ListItem(String.Format("{0:MMMM}", MonthName(x)).ToUpper, CStr(x)))
        Next
        dlmonth.Items.Insert(0, New ListItem("-SELECCIONAR-", "0"))
    End Sub

    Private Sub MostrarBotones(ByVal modo As String)
        Select Case modo
            Case "Load"
                btNuevo.Visible = True
                btBuscar.Visible = True
                btEditar.Visible = False
                btCancelar.Visible = False
                btGuardar.Visible = False
                pnlPrincipal.Enabled = True
                pnlSecundario.Enabled = False

            Case "Nuevo"
                btNuevo.Visible = False
                btBuscar.Visible = False
                btEditar.Visible = False
                btCancelar.Visible = True
                btGuardar.Visible = True
                pnlPrincipal.Enabled = False
                pnlSecundario.Enabled = True

            Case "Buscar"
                btNuevo.Visible = False
                btBuscar.Visible = False
                btEditar.Visible = True
                btCancelar.Visible = True
                btGuardar.Visible = False
                pnlPrincipal.Enabled = True
                pnlSecundario.Enabled = False

            Case "Editar"
                btNuevo.Visible = False
                btBuscar.Visible = False
                btEditar.Visible = False
                btCancelar.Visible = True
                btGuardar.Visible = True
                pnlPrincipal.Enabled = False
                pnlSecundario.Enabled = True
            Case "Guardar"
                btNuevo.Visible = False
                btBuscar.Visible = False
                btEditar.Visible = False
                btCancelar.Visible = True
                btGuardar.Visible = False
                pnlPrincipal.Enabled = False
                pnlSecundario.Enabled = False
        End Select

    End Sub

    Private Sub Mantenimiento(ByVal tipo As Integer)
        objProgramacionPedido = New Entidades.ProgramacionPedido
        With objProgramacionPedido
            .IdYear = CInt(dlyear.SelectedValue)
            .IdSemana = CInt(dlweek.SelectedValue)
            .cal_Mes = CInt(dlmonth.SelectedValue)
            .cal_FechaIni = CDate(tbfechaini.Text)
            .cal_FechaFin = CDate(tbfechafin.Text)
            .cal_DiasUtil = CInt(tbutilday.Text)
            .cal_Estado = CBool(IIf(rbestado.SelectedValue = "1", True, False))
        End With
        Dim ok As Boolean = False

        Select Case tipo
            Case operativo.GuardarNuevo
                ok = (New Negocio.ProgramacionPedido).InsertProgramacionPedido(objProgramacionPedido)
                objScript.mostrarMsjAlerta(Me, "El registro ha sido Guardado")
            Case operativo.Actualizar
                ok = (New Negocio.ProgramacionPedido).UpdateProgramacionPedido(objProgramacionPedido)
                objScript.mostrarMsjAlerta(Me, "El registro ha sido Actualizado")
        End Select

        MostrarBotones("Guardar")

    End Sub

    Private Sub limpiarForm()
        tbfechaini.Text = ""
        tbfechafin.Text = ""
        tbutilday.Text = ""
        dlyear.SelectedValue = "0"
        dlweek.Items.Clear()
        dlmonth.SelectedValue = "0"
        rbestado.SelectedValue = "1"
    End Sub


#End Region

#Region "eventos Principales"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                cargarAlIniciar()
                MostrarBotones("Load")
                ViewState.Add("operativo", operativo.Ninguno)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btNuevo.Click
        Try
            MostrarBotones("Nuevo")
            ViewState.Add("operativo", operativo.GuardarNuevo)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        Try
            Mantenimiento(CInt(ViewState("operativo")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btEditar.Click
        Try
            MostrarBotones("Editar")
            ViewState.Add("operativo", operativo.Actualizar)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btCancelar.Click
        Try
            limpiarForm()
            MostrarBotones("Load")
            ViewState.Add("operativo", operativo.Ninguno)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btBuscar.Click
        Try
            limpiarForm()
            MostrarBotones("Buscar")
            ViewState.Add("operativo", operativo.Buscar)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

    Private Sub dlyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlyear.SelectedIndexChanged
        Try
            If dlyear.SelectedValue <> "0" Then llenarCboSemana(dlweek, CInt(dlyear.SelectedValue), CInt(ViewState("operativo")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub dlweek_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlweek.SelectedIndexChanged
        Try
            If CInt(ViewState("operativo")) = operativo.Buscar Then
                objProgramacionPedido = (New Negocio.ProgramacionPedido).ListarProgramacionMantenimiento(CInt(dlyear.SelectedValue), CInt(dlweek.SelectedValue))
                With objProgramacionPedido
                    dlmonth.SelectedValue = CStr(.cal_Mes)
                    tbfechaini.Text = .cal_FechaIni.Date.ToShortDateString
                    tbfechafin.Text = .cal_FechaFin.Date.ToShortDateString
                    tbutilday.Text = CStr(.cal_DiasUtil)
                    rbestado.SelectedValue = CStr(IIf(.cal_Estado = True, "1", "0"))
                End With
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class