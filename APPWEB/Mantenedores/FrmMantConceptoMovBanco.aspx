﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantConceptoMovBanco.aspx.vb" Inherits="APPWEB.FrmMantConceptoMovBanco" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscar">
        <table width="100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='/Imagenes/Nuevo_A.JPG';"
                                    onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" />
                                <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                    onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                    OnClientClick="return(validarSave());" CausesValidation="true" />
                                <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                    onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';"
                                    OnClientClick="return(confirm('Desea cancelar el proceso?'));" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    Conceptos de Movimientos Bancarios
                </td>
            </tr>
            <tr>
                <td align="center">
                    <br />
                    <fieldset class="FieldSetPanel">
                        <legend>
                            <asp:Label ID="lblModo" runat="server" Text="Búsqueda"></asp:Label></legend>
                        <table cellpadding="2" style="width: 90%;">
                            <tr>
                                <td class="Label_fsp">
                                    <asp:Label ID="lblTipoConceptoBanco" runat="server" Text="Tipo Concepto Banco:"></asp:Label>
                                </td>
                                <td style="text-align :left;">
                                    <asp:DropDownList ID="cboTipoConceptoBanco" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>                                
                                <td class="Label_fsp">
                                    <asp:Label ID="lblEstadoAutoAprobacion" runat="server" Text="Estado Auto-Aprobación:"></asp:Label>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rbtlEstadoAutoAprobacion" runat="server" CssClass="Label_fsp"
                                        RepeatDirection="Horizontal ">
                                        <asp:ListItem Selected="True" Text="Todos" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Activo" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td class="Label_fsp">
                                    <asp:Label ID="lblDescripcionBreve" runat="server" Text="Descripción Breve:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="txtDescripcionBreve" runat="server" Width="100%"></asp:TextBox>
                                </td>
                                
                                <td class="Label_fsp">
                                    <asp:Label ID="lblEstado" runat="server" Text="Estado:"></asp:Label>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rbtlEstado" runat="server" CssClass="Label_fsp" RepeatDirection="Horizontal ">
                                        <asp:ListItem Text="Todos" Value="2"></asp:ListItem>
                                        <asp:ListItem Selected="True" Text="Activo" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td class="Label_fsp">
                                    <asp:Label ID="lbldescripcion" runat="server" Text="Descripción:"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="txtDescripcion" runat="server" Width="100%"></asp:TextBox>
                                </td>
                                
                                <td>
                                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" onmouseout="this.src='/Imagenes/Buscar_B.JPG';" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <br />
                    <asp:UpdateProgress ID="upGrilla_Progress" runat="server" DisplayAfter="1000" AssociatedUpdatePanelID="upGrilla">
                        <ProgressTemplate>
                            <asp:Image ID="ImgProgress" runat="server" ImageUrl="~/Imagenes/ajax.gif" /></ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upGrilla" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="dgvConceptoMovBanco" runat="server" AutoGenerateColumns="False"
                                CellPadding="2" HorizontalAlign="Center" Width="80%">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnEditar" runat="server" OnClick="lbtnEditar_Click">Editar</asp:LinkButton>
                                            <asp:HiddenField ID="hddId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                            <asp:HiddenField ID="hddIdTipoConceptoBanco" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoConceptoBanco") %>' />
                                            <asp:HiddenField ID="hddIdEstadoAutoAprobacion" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"EstadoAutoAprobacion") %>' />
                                            <asp:HiddenField ID="hddIdEstado" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminar_Click">Eliminar</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción" NullDisplayText="" />
                                    <asp:BoundField DataField="DescripcionBreve" HeaderText="Desc. Breve" NullDisplayText="" />
                                    <asp:BoundField DataField="TipoConceptoBanco" HeaderText="Tipo Cpto. Banco" NullDisplayText="" />
                                    <asp:CheckBoxField DataField="EstadoAutoAprobacion" HeaderText="Auto-Aprobación" />
                                    <asp:CheckBoxField DataField="Estado" HeaderText="Activo" />
                                    <%--<asp:BoundField DataField="DescEstadoAutoAprobacion" HeaderText="Estado Auto-Aprobación"
                                NullDisplayText="" />
                            <asp:BoundField DataField="DescEstado" HeaderText="Estado" NullDisplayText="" />--%>
                                </Columns>
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
    
    <script language="javascript" type="text/javascript">
        function validarSave() {
            var TipoConceptoBanco = document.getElementById('<%=cboTipoConceptoBanco.ClientID%>');
            var idTipoConceptoBanco = TipoConceptoBanco.value;
            if (idTipoConceptoBanco == 0) {
                alert('Seleccione un TipoConceptoBanco.');

                return false;
            }

            var descripcion = document.getElementById('<%=txtdescripcion.ClientID%>');
            if (descripcion.value.length == 0) {
                alert('Debe Ingresar una Descripción.');
                descripcion.select();
                descripcion.focus();
                return false;
            }

            return (confirm('Desea Continuar Con la Operacion'));
        }
    </script>
   
   <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
