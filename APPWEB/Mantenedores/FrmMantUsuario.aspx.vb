﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmMantUsuario
    Inherits System.Web.UI.Page
    Private objDSBPersona As Object
    Private objDSUsuario As Object
    Private objScript As New ScriptManagerClass
    'Private listaTiendaAreaPerfil As List(Of Entidades.TiendaAreaPerfilView)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
            ValidarPermisos()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try
            Me.objDSBPersona = New Object
            Me.objDSUsuario = New Object
            Session.Add("objDSBPersona", objDSBPersona)
            Session.Add("objDSUsuario", objDSUsuario)
            cargarDatos()
            verFrmInicio()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub ValidarPermisos()
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {31})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.lblPasswordActual.Visible = True
            Me.lblPasswordActual_Text.Visible = True
        Else
            Me.lblPasswordActual.Visible = False
            Me.lblPasswordActual_Text.Visible = False
        End If

    End Sub
#Region "VISTAS FRM"
    Private Sub verFrmInicio()
        hddModo.Value = "I"
        hddLogin.Value = ""
        Panel_Usuario.Visible = False
        Panel_Busqueda.Enabled = True
        mostrarBotonesControl()
        buscarxEstado()
        btnBuscarPersona.Visible = True
        limpiarFrm()
        'DGV_TiendaAreaPerfil.DataSource = Nothing
        'DGV_TiendaAreaPerfil.DataBind()
    End Sub
    Private Sub verFrmNuevo()
        mostrarFechaBaja(False)
        activarEstado(False, 0)
        mostrarMotivoBaja(False)
        Panel_Usuario.Visible = True
        Panel_Busqueda.Enabled = False
        mostrarBotonesControl()

        Me.chbUpdatePassword.Checked = True
        Me.chbUpdatePassword.Enabled = False

    End Sub
    Private Sub verFrmEditar()
        Panel_Usuario.Visible = True
        Panel_Busqueda.Enabled = False
        mostrarBotonesControl()
        mostrarFechaBaja(True)
        activarEstado(True, rdbEstado.SelectedIndex)
        btnBuscarPersona.Visible = False

        Me.chbUpdatePassword.Checked = False
        Me.chbUpdatePassword.Enabled = True

    End Sub
#End Region
#Region "BOTONES DE CONTROL"
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        hddModo.Value = "I"
        verFrmInicio()
    End Sub
    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        hddModo.Value = "N"
        Dim objFecha As New Negocio.FechaActual
        Dim fecha As Date = objFecha.SelectFechaActual
        lblFechaAlta.Text = Format(fecha, "dd/MM/yyyy")
        verFrmNuevo()
    End Sub
    Private Sub limpiarFrm()
        txtClave.Text = ""
        txtCodigo.Text = ""
        txtCodigoPersona.Text = ""
        txtConfirmarPassword.Text = ""
        txtDNI.Text = ""
        lblFechaAlta.Text = ""
        lblFechaBaja.Text = ""
        txtPersona.Text = ""
        txtUsuario.Text = ""
        Me.lblPasswordActual.Text = ""
        DGV_PerfilUsuario.DataSource = Nothing
        DGV_PerfilUsuario.DataBind()
        DGV_Usuario_Area.DataSource = Nothing
        DGV_Usuario_Area.DataBind()
    End Sub
    Private Sub mostrarBotonesControl()
        If hddModo.Value = "I" Then
            btnNuevo.Visible = True
            btnGuardar.Visible = False
            btnCancelar.Visible = False
        Else
            btnNuevo.Visible = False
            btnGuardar.Visible = True
            btnCancelar.Visible = True
        End If
    End Sub
#End Region
#Region "CARGA DE DATOS BD"
    Private Sub cargarDatos()
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboMotivoBaja(Me.cmbMotivoBaja)
            cargarDatosPerfil(Me.cmbPerfil_Usuario)
            objCbo.LlenarCboArea(Me.cmbArea_Usuario, False)
        Catch ex As Exception
            Dim objScrip As New ScriptManagerClass
            objScrip.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosPerfil(ByVal cbo As DropDownList)
        Dim objCombo As New Combo
        objCombo.LlenarCboPerfil(cbo)
    End Sub
    Private Sub cargarDatosCaja(ByVal cbo As DropDownList, ByVal IdTienda As Integer)
        Dim objcbo As New Combo
        objcbo.LlenarCboCajaxIdTienda(cbo, IdTienda)
    End Sub
    Private Sub cargarDatosTienda()
        Dim obj As New Negocio.TiendaAreaPerfilView
        'cmbTienda_usuario.DataSource = obj.SelectCboTienda
        'cmbTienda_usuario.DataBind()
    End Sub
    Private Sub cargardatosAreaxIdTienda()
        Dim obj As New Negocio.TiendaAreaPerfilView
        'cmbArea_usuario.DataSource = obj.SelectCboAreaxIdTienda(CInt(cmbTienda_usuario.SelectedValue))
        'cmbArea_usuario.DataBind()
    End Sub
    Private Sub cargarDatosPerfilxIdTiendaxIdArea()
        Dim obj As New Negocio.TiendaAreaPerfilView
        'cmbPerfil_Usuario.DataSource = obj.SelectCboPerfilxIdTiendaxIdArea(CInt(cmbTienda_usuario.SelectedValue), CInt(cmbArea_usuario.SelectedValue))
        'cmbPerfil_Usuario.DataBind()
    End Sub
#End Region
#Region "MANEJO DE CONTROLES"
    Private Sub mostrarMotivoBaja(ByVal isVisible As Boolean)
        lblMotivoBaja.Visible = isVisible
        cmbMotivoBaja.Visible = isVisible
    End Sub
    Private Sub activarEstado(ByVal enabled As Boolean, ByVal index As Integer)
        lblEstado.Enabled = enabled
        rdbEstado.Enabled = enabled
        rdbEstado.SelectedIndex = index
    End Sub
    Private Sub mostrarFechaBaja(ByVal isVisible As Boolean)
        lblFechaBaja.Visible = isVisible
    End Sub
#End Region
#Region "BUSCAR PERSONA"
    Protected Sub btnBuscarPersonaCapa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarPersonaCapa.Click
        Dim objScript As New ScriptManagerClass
        Try
            Dim obj As New Negocio.NaturalView
            Me.gvBusquedaClientes.DataSource = obj._NaturalViewSelectAllxNombre(txtTextoBusqCliente.Text)
            Me.gvBusquedaClientes.DataBind()
            Me.setDSPersona(gvBusquedaClientes.DataSource)
            If gvBusquedaClientes.Rows.Count = 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If
            objScript.onCapa(Me, "capaBuscarP")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region
#Region "GESTION DATASOURCE"
    Private Function getDSPersona() As Object
        Return CObj(Session.Item("objDSBPersona"))
    End Function
    Private Sub setDSPersona(ByVal objDSBPersona1 As Object)
        Session.Remove("objDSBPersona")
        Session.Add("objDSBPersona", objDSBPersona1)
    End Sub
    Private Function getDSUsuario() As Object
        Return CObj(Session.Item("objDSUsuario"))
    End Function
    Private Sub setDSUsuario(ByVal objDSBusuario1 As Object)
        Session.Remove("objDSUsuario")
        Session.Add("objDSUsuario", objDSBusuario1)
    End Sub
#End Region

    Protected Sub gvBusquedaClientes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBusquedaClientes.PageIndexChanging
        Me.gvBusquedaClientes.PageIndex = e.NewPageIndex
        Me.gvBusquedaClientes.DataSource = Me.getDSPersona
        Me.gvBusquedaClientes.DataBind()
    End Sub

    Protected Sub gvBusquedaClientes_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvBusquedaClientes.SelectedIndexChanged
        Dim objScrip As New ScriptManagerClass
        Try
            txtCodigo.Text = HttpUtility.HtmlDecode(gvBusquedaClientes.SelectedRow.Cells(4).Text).Trim
            txtCodigoPersona.Text = HttpUtility.HtmlDecode(gvBusquedaClientes.SelectedRow.Cells(1).Text).Trim
            txtDNI.Text = HttpUtility.HtmlDecode(gvBusquedaClientes.SelectedRow.Cells(4).Text).Trim
            'txtRUC.Text = gvBusquedaClientes.SelectedRow.Cells(6).Text
            txtPersona.Text = HttpUtility.HtmlDecode(gvBusquedaClientes.SelectedRow.Cells(2).Text).Trim
            objScrip.offCapa(Me, "capaBuscarP")
        Catch ex As Exception
            objScrip.mostrarMsjAlerta(Me, "Problemas en la carga de datos")
        End Try
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Dim objScript As New ScriptManagerClass
        Dim objUsuario As New Negocio.Usuario
        Try
            Select Case hddModo.Value
                Case "N"
                    If validarExistenciaPersona() Then
                        objScript.mostrarMsjAlerta(Me, "La persona ha registrar ya posee un usuario.")
                        txtCodigo.Focus()
                        Return
                    End If
                    If validarExistenciaLogin() Then
                        objScript.mostrarMsjAlerta(Me, "El Login ingresado ya ha sido registrado. Intente con otro.")
                        txtUsuario.Focus()
                        Return
                    End If
                Case "E"
                    If hddLogin.Value.ToUpper <> txtUsuario.Text.ToUpper Then
                        If validarExistenciaLogin() Then
                            objScript.mostrarMsjAlerta(Me, "El Login ingresado ya ha sido registrado. Intente con otro.")
                            txtUsuario.Focus()
                            Return
                        End If
                    End If
            End Select
            Dim obj As New Entidades.Usuario
            obj.Clave = txtClave.Text
            obj.Estado = rdbEstado.SelectedValue
            obj.IdPersona = CInt(txtCodigoPersona.Text)
            obj.Login = txtUsuario.Text
            obj.IdMotivoBaja = CInt(cmbMotivoBaja.SelectedValue)
            If obj.Estado = "0" Then
                '************** obtengo la fecha actual
                obj.FechaBaja = (New Negocio.FechaActual).SelectFechaActual
            Else
                obj.IdMotivoBaja = 0
                obj.FechaBaja = Nothing
            End If

            '************ obtengo el perfil
            Dim lista As List(Of Entidades.PerfilUsuario) = obtenerListaPerfilUsuarioFrmGrilla()

            '************ obtengo el perfil
            Dim lista_area As List(Of Entidades.Usuario_Area) = obtenerListaAreaUsuarioFrmGrilla()

            Dim hecho As Boolean = False
            Select Case hddModo.Value
                Case "N"
                    hecho = objUsuario.InsertaUsuario(obj, lista, lista_area)
                Case "E"
                    hecho = objUsuario.ActualizaUsuario(obj, lista, lista_area, Me.chbUpdatePassword.Checked)
            End Select
            If hecho Then
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            Else
                Throw New Exception
            End If
            verFrmInicio()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Ocurrieron problemas en la operación. Asegúrese que el login sea único y/o que la persona no posea un usuario")
        End Try
    End Sub

    Protected Sub btnBuscarPersona0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarPersona0.Click
        Dim obj As New ScriptManagerClass
        Try
            Dim objUV As New Negocio.UsuarioView
            DGVUsuario.DataSource = objUV.SelectxEstadoxNombre(txtTextoABuscar.Text, rdbestadoBusq.SelectedValue)
            DGVUsuario.DataBind()
            Me.setDSUsuario(DGVUsuario.DataSource)
            If DGVUsuario.Rows.Count = 0 Then
                obj.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If
        Catch ex As Exception
            obj.mostrarMsjAlerta(Me, "Problemas en la búsqueda.")
        End Try
    End Sub

    Protected Sub DGVUsuario_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGVUsuario.PageIndexChanging
        DGVUsuario.PageIndex = e.NewPageIndex
        DGVUsuario.DataSource = Me.getDSUsuario
        DGVUsuario.DataBind()
    End Sub

    Protected Sub rdbestadoBusq_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbestadoBusq.SelectedIndexChanged
        buscarxEstado()
    End Sub
    Private Sub buscarxEstado()
        Dim obj As New ScriptManagerClass
        Try
            Dim objUV As New Negocio.UsuarioView
            DGVUsuario.DataSource = objUV.SelectxEstadoxNombre("", rdbestadoBusq.SelectedValue)
            DGVUsuario.DataBind()
            Me.setDSUsuario(DGVUsuario.DataSource)
            If DGVUsuario.Rows.Count = 0 Then
                obj.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If
        Catch ex As Exception
            obj.mostrarMsjAlerta(Me, "Problemas en la búsqueda.")
        End Try
    End Sub

    Protected Sub DGVUsuario_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGVUsuario.SelectedIndexChanged
        cargarDatosusuario()
    End Sub
    Private Sub cargarDatosusuario()
        hddLogin.Value = DGVUsuario.SelectedRow.Cells(4).Text
        Try
            Dim objUsuario As Entidades.UsuarioView = (New Negocio.UsuarioView).SelectxIdPersona(CInt(DGVUsuario.SelectedRow.Cells(1).Text))

            txtClave.Text = objUsuario.Clave
            txtConfirmarPassword.Text = objUsuario.Clave

            Me.lblPasswordActual.Text = objUsuario.Clave

            txtPersona.Text = objUsuario.Nombre
            txtCodigo.Text = objUsuario.DNI
            txtCodigoPersona.Text = objUsuario.IdPersona.ToString
            txtDNI.Text = objUsuario.DNI
            txtUsuario.Text = objUsuario.Login
            rdbEstado.SelectedValue = objUsuario.Estado

            If objUsuario.fechaAlta = Nothing Then
                lblFechaAlta.Text = ""
            Else
                lblFechaAlta.Text = Format(CDate(objUsuario.fechaAlta), "dd/MM/yyyy")
            End If
            If objUsuario.fechaBaja = Nothing Then
                lblFechaBaja.Text = ""
            Else
                lblFechaBaja.Text = Format(CDate(objUsuario.fechaBaja), "dd/MM/yyyy")
            End If

            cmbMotivoBaja.SelectedValue = objUsuario.IdMotivoBaja.ToString

            If objUsuario.Estado = "0" Then
                mostrarMotivoBaja(True)
            Else
                mostrarMotivoBaja(False)
            End If

            '*************** obtengo los datos del perfil usuario
            Dim objPerfilUsuario As New Negocio.PerfilUsuario
            Dim listaPerfilUsuario As List(Of Entidades.PerfilUsuario) = objPerfilUsuario.PerfilUsuarioSelectxIdPersona(CInt(txtCodigoPersona.Text))


            DGV_PerfilUsuario.DataSource = listaPerfilUsuario
            DGV_PerfilUsuario.DataBind()

            '*************** obtengo los datos del area usuario
            Dim objUsuario_Area As New Negocio.Usuario_Area
            Dim listaUsuario_Area As List(Of Entidades.Usuario_Area) = objUsuario_Area.SelectxIdUsuario(CInt(txtCodigoPersona.Text))

            DGV_Usuario_Area.DataSource = listaUsuario_Area
            DGV_Usuario_Area.DataBind()


            hddModo.Value = "E"

            verFrmEditar()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos")
        End Try
    End Sub

    Protected Sub rdbEstado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbEstado.SelectedIndexChanged
        If rdbEstado.SelectedValue = "1" Then
            mostrarMotivoBaja(False)
        Else
            mostrarMotivoBaja(True)
        End If
    End Sub

    Protected Sub btnBuscarCosPersona_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarCosPersona.Click
        Dim objScript As New ScriptManagerClass
        Dim cod As String = ""
        Try
            cod = txtCodigo.Text
            Dim objNaturalView As New Negocio.NaturalView
            Dim objNatural As Entidades.NaturalView = objNaturalView.SelectxDNI(cod)
            txtPersona.Text = objNatural.Nombre
            txtCodigoPersona.Text = CStr(objNatural.IdPersona)
            txtDNI.Text = objNatural.DNI
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "No existe una persona con D.N.I.=" & cod)
        End Try
    End Sub

    Protected Sub btnValidarLogin_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidarLogin.Click
        Dim obj As New ScriptManagerClass
        If validarExistenciaLogin() Then
            obj.mostrarMsjAlerta(Me, "El login ya ha sido registrado. Intente con otro.")
            txtUsuario.Focus()
        Else
            obj.mostrarMsjAlerta(Me, "El login ingresado se encuentra disponible.")
        End If
    End Sub
    Private Function validarExistenciaLogin() As Boolean
        Dim flag As Boolean = False
        Try
            Dim objUtil As New Negocio.Util
            If objUtil.ValidarExistenciaxTablax1Campo("Usuario", "us_Login", txtUsuario.Text) > 0 Then
                flag = True
            End If
        Catch ex As Exception
            flag = False
        End Try
        Return flag
    End Function
    Private Function validarExistenciaPersona() As Boolean
        Dim flag As Boolean = False
        Try
            Dim objUtil As New Negocio.Util
            If objUtil.ValidarExistenciaxTablax1Campo("Usuario", "IdPersona", txtCodigoPersona.Text) > 0 Then
                flag = True
            End If
        Catch ex As Exception
            flag = False
        End Try
        Return flag
    End Function

    Private Sub btnGuardar_RegPersona_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar_RegPersona.Click
        registrarPersona()
    End Sub
    Private Sub registrarPersona()
        Dim idPersona As Integer = -1
        Try
            Dim objNatural As New Entidades.Natural
            Dim objDocumentoI As New Entidades.DocumentoI
            objDocumentoI.Numero = txtDNI_RegPersona.Text.Trim
            objDocumentoI.IdTipoDocumentoI = 1
            With objNatural
                .ApellidoMaterno = txtApMaterno_RegPersona.Text.Trim
                .ApellidoPaterno = txtApPaterno_RegPersona.Text.Trim
                .Nombres = txtNombres_RegPersona.Text.Trim
            End With
            Dim objNPersona As New Negocio.Persona
            Dim lista As New List(Of Entidades.DocumentoI)
            lista.Add(objDocumentoI)
            Dim objPersona As New Entidades.Persona
            objPersona.Estado = "1"
            idPersona = objNPersona.InsertaPersonaT(objPersona, True, objNatural, False, Nothing, False, Nothing, False, Nothing, False, Nothing, True, lista, False, Nothing, False, Nothing)
            If idPersona > 0 Then
                cargarDatosPersona(idPersona)
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Ocurrieron problemas en la operaciòn.")
        End Try
    End Sub
    Private Sub cargarDatosPersona(ByVal idpersona As Integer)
        txtCodigo.Text = txtDNI_RegPersona.Text
        txtCodigoPersona.Text = idpersona.ToString
        txtPersona.Text = txtApPaterno_RegPersona.Text & " " & txtApMaterno_RegPersona.Text & ", " & txtNombres_RegPersona.Text
        txtDNI.Text = txtDNI_RegPersona.Text
        'limpio los campos
        txtApMaterno_RegPersona.Text = ""
        txtApPaterno_RegPersona.Text = ""
        txtNombres_RegPersona.Text = ""
        txtDNI_RegPersona.Text = ""
        objScript.offCapa(Me, "capaRegistrarP")
    End Sub
    Protected Sub btnAgregarTAP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarTAP.Click
        addPerfilGrilla()
    End Sub

    Protected Sub btnAgregarTAA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarTAA.Click
        addAreaGrilla()
    End Sub
    Private Sub addPerfilGrilla()
        Try
            Dim lista As List(Of Entidades.PerfilUsuario) = obtenerListaPerfilUsuarioFrmGrilla()

            '********** creo un nuevo objeto
            Dim objPerfilusuario As New Entidades.PerfilUsuario(Nothing, CInt(cmbPerfil_Usuario.SelectedValue), Me.cmbPerfil_Usuario.SelectedItem.ToString, False)
            lista.Add(objPerfilusuario)

            DGV_PerfilUsuario.DataSource = lista
            DGV_PerfilUsuario.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga del Perfil")
        End Try
    End Sub

    Private Sub addAreaGrilla()
        Try
            Dim lista As List(Of Entidades.Usuario_Area) = obtenerListaAreaUsuarioFrmGrilla()

            '********** creo un nuevo objeto
            Dim objAreausuario As New Entidades.Usuario_Area(Nothing, CInt(cmbArea_Usuario.SelectedValue), False, Me.cmbArea_Usuario.SelectedItem.ToString)
            lista.Add(objAreausuario)

            DGV_Usuario_Area.DataSource = lista
            DGV_Usuario_Area.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga del Area")
        End Try
    End Sub

    Private Function obtenerListaPerfilUsuarioFrmGrilla() As List(Of Entidades.PerfilUsuario)

        Dim lista As New List(Of Entidades.PerfilUsuario)
        Dim idPersona As Integer = 0
        If IsNumeric(txtCodigoPersona.Text) Then
            idPersona = CInt(txtCodigoPersona.Text)
        End If

        For i As Integer = 0 To DGV_PerfilUsuario.Rows.Count - 1
            Dim obj As New Entidades.PerfilUsuario
            obj.IdPerfil = CInt(CType(DGV_PerfilUsuario.Rows(i).FindControl("hddIdPerfil"), HiddenField).Value)
            obj.NomPerfil = (New Util).decodificarTexto(CStr(CType(DGV_PerfilUsuario.Rows(i).FindControl("lblPerfil"), Label).Text))
            obj.Estado = CType(DGV_PerfilUsuario.Rows(i).FindControl("chbEstado_UsuarioPerfil"), CheckBox).Checked
            obj.IdPersona = idPersona
            lista.Add(obj)
        Next

        Return lista

    End Function

    Private Function obtenerListaAreaUsuarioFrmGrilla() As List(Of Entidades.Usuario_Area)

        Dim lista As New List(Of Entidades.Usuario_Area)
        Dim idPersona As Integer = 0
        If IsNumeric(txtCodigoPersona.Text) Then
            idPersona = CInt(txtCodigoPersona.Text)
        End If

        For i As Integer = 0 To DGV_Usuario_Area.Rows.Count - 1
            Dim obj As New Entidades.Usuario_Area
            obj.IdArea = CInt(CType(DGV_Usuario_Area.Rows(i).FindControl("hddIdArea"), HiddenField).Value)
            obj.NombreArea = (New Util).decodificarTexto(CStr(CType(DGV_Usuario_Area.Rows(i).FindControl("lblArea"), Label).Text))
            obj.Estado = CType(DGV_Usuario_Area.Rows(i).FindControl("chbEstado_UsuarioArea"), CheckBox).Checked
            obj.IdUsuario = idPersona
            lista.Add(obj)
        Next

        Return lista

    End Function

    Protected Sub DGV_PerfilUsuario_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_PerfilUsuario.SelectedIndexChanged
        quitarRegistroPerfilUsuarioFromGrilla()
    End Sub
    Protected Sub DGV_Usuario_Area_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_Usuario_Area.SelectedIndexChanged
        quitarRegistroAreaUsuarioFromGrilla()
    End Sub
    Private Sub quitarRegistroPerfilUsuarioFromGrilla()
        Try
            Dim idPersona As Integer = 0
            If IsNumeric(txtCodigoPersona.Text) Then
                idPersona = CInt(txtCodigoPersona.Text)
            End If

            '******************* valido que no haya registros relacionados
            Dim objUtil As New Negocio.Util
            If objUtil.ValidarxDosParametros("PerfilUsuarioOpcion", "IdPerfil", CType(DGV_PerfilUsuario.SelectedRow.FindControl("hddIdPerfil"), HiddenField).Value, "IdUsuario", idPersona.ToString) > 0 Then
                objScript.mostrarMsjAlerta(Me, "No se puede quitar el perfil-usuario porque posee registros relacionados en la Tabla [ Perfil Usuario Opción ]")
                Return
            End If

            '*********************** quitamos el registro
            Dim lista As List(Of Entidades.PerfilUsuario) = obtenerListaPerfilUsuarioFrmGrilla()
            lista.RemoveAt(DGV_PerfilUsuario.SelectedIndex)
            DGV_PerfilUsuario.DataSource = lista
            DGV_PerfilUsuario.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminación de un registro.")
        End Try
    End Sub

    Private Sub quitarRegistroAreaUsuarioFromGrilla()
        Try
            Dim idPersona As Integer = 0
            If IsNumeric(txtCodigoPersona.Text) Then
                idPersona = CInt(txtCodigoPersona.Text)
            End If
            'Desactivado por el momento
            ''******************* valido que no haya registros relacionados
            'Dim objUtil As New Negocio.Util
            'If objUtil.ValidarxDosParametrosEnteros("TablaX a verificar", "IdArea", CInt(CType(DGV_Usuario_Area.SelectedRow.FindControl("hddIdArea"), HiddenField).Value), "IdUsuario", idPersona) > 0 Then
            '    objScript.mostrarMsjAlerta(Me, "No se puede quitar el Area-Usuario porque posee registros relacionados en la Tabla [ TablaX ]")
            '    Return
            'End If

            '*********************** quitamos el registro
            Dim lista As List(Of Entidades.Usuario_Area) = obtenerListaAreaUsuarioFrmGrilla()
            lista.RemoveAt(DGV_Usuario_Area.SelectedIndex)
            DGV_Usuario_Area.DataSource = lista
            DGV_Usuario_Area.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminación de un registro.")
        End Try
    End Sub

End Class
