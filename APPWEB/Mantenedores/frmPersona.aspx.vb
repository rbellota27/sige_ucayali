﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class frmPersona
    Inherits System.Web.UI.Page

#Region "variables"
    Private Enum operativo
        GuardarNuevo = 1
        Actualizar = 2
        Ninguno = 0
    End Enum
    Private drop As Combo
    Private objNatural As Entidades.Natural
    Private objChofer As Entidades.Chofer
    Private objEmpleado As Entidades.Empleado
    Private objPersona As Entidades.Persona
    Private objJuridica As Entidades.Juridica
    Private objScript As New ScriptManagerClass
    Private ListaRolPersona As List(Of Entidades.RolPersona)
    Private ListaDocumentoPersona As List(Of Entidades.DocumentoI)
    Private ListaOcupacionPersona As List(Of Entidades.OcupacionPersona)
    Private ListaTelefonoPersona As List(Of Entidades.TelefonoView)
    Private ListaCorreoPersona As List(Of Entidades.CorreoView)
    Private ListaProfesionPersona As List(Of Entidades.ProfesionPersona)
    Private ListaBuscarPersona As List(Of Entidades.PersonaView)
    Private ListaContactoEmpresa As List(Of Entidades.PersonaView)
    Private ListaDireccionPersona As List(Of Entidades.Direccion)
    Private ListaAuxContacto As List(Of Entidades.PersonaView)
    Private ValidoRep As New Negocio.Util
    Private listaRol As List(Of Entidades.Rol)


#End Region

#Region "Procesos "

    Public Sub llenarCboCatEmpleado(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        cbo.DataSource = (New Negocio.CatEmpleado).SelectAllActivo
        cbo.DataValueField = "IdCatEmpleado"
        cbo.DataTextField = "Nombre"
        cbo.DataBind()
        If addElement Then
            cbo.Items.Insert(0, New ListItem("-----", "0"))
        End If
    End Sub
    Private Sub BuscarPerfil()

        Dim IdUsuario As Integer = CInt(Session("IdUsuario"))

        Dim IdPerfil As Integer = CInt(Session("IdPerfil"))

        'If (IdPerfil = 5) Then
        '    Me.dltipoPV.Enabled = False
        'End If

    End Sub


    Private Sub ValidarPermisos(ByVal IdUsuario As Integer)
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / DESPACHAR / EMITIR GUIA REM / FECHA EMISION / COMPROMETER PERCEPCION
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(IdUsuario, New Integer() {33, 34, 35, 36, 37, 38, 260})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.gvBuscar.Enabled = True
        Else
            Me.gvBuscar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.rbestado.Enabled = True
        Else
            Me.rbestado.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* REGISTRAR PROPIETARIO
            Me.ckpropietario.Enabled = True
        Else
            Me.ckpropietario.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* REGISTRAR EMPLEADO
            Me.tr_empleado.Visible = True
        Else
            Me.tr_empleado.Visible = False
        End If

        If listaPermisos(5) > 0 Then  '********* REGISTRAR CHOFER
            Me.tr_chofer.Visible = True
        Else
            Me.tr_chofer.Visible = False
        End If

        If listaPermisos(6) > 0 Then  '********* TIPO PRECIO VENTA
            Me.dltipoPV.Enabled = True
        Else
            Me.dltipoPV.Enabled = False
        End If

    End Sub

#End Region

#Region "procedimientos"

    Private Sub llenarDropdownList()
        drop = New Combo
        drop.LlenarTipoVia(dlTipoVia)
        drop.LlenarTipoZona(dlZonificacion, 2, True)
        drop.LLenarCboDepartamento(dlDepartamento)
        drop.LlenarTipoDireccion(dlTipoDireccion)

        drop.LlenarGiro(dlgiro)
        drop.LlenarCboNacionalidad(dlnacionalidad)
        drop.LlenarCboTipoPV(dltipoPV)
        drop.LlenarCboTipoAgente(dlTipoAgente, True)
        drop.LlenarCboRol(dlRol, True)
        drop.LlenarCboCargo(dlCargo)
        drop.LlenarCboCondTrabajo(dlCondicionTrabajo)
        drop.LlenarCboEstadoCivil(dlEstadoCivil)
        drop.LlenarCboMotivoBaja(dlMotivoBaja)
        drop.LlenarCboMotivoBaja(dlEmpMotivoBaja)
        llenarCboCatEmpleado(dlEmpCategoria, True)
    End Sub
    Private Sub validarIngreso()
        Dim tipopv As Integer
        Dim idrolp As Integer
        tipopv = CInt(dltipoPV.SelectedValue)
        If (tipopv <> 1) Then
            ListaRolPersona = getListaRol()

            For i As Integer = 0 To ListaRolPersona.Count - 1

                idrolp = ListaRolPersona(i).IdRol

                If (idrolp = 1) Then

                    If (tipopv <> 1) Then
                        objScript.mostrarMsjAlerta(Me, "EL PRECIO SOLO DEBE SER PUBLICO")
                        Exit Sub


                    End If
                End If


            Next

        End If



    End Sub

    Private Sub Mantenimiento(ByVal tipo As Integer)
        Dim tipoPersona$ = rbPersona.SelectedValue
        objNatural = New Entidades.Natural
        objPersona = New Entidades.Persona
        objJuridica = New Entidades.Juridica
        objEmpleado = New Entidades.Empleado
        objChofer = New Entidades.Chofer
        With objPersona
            .Id = CInt(hddIdPersona.Value)
            .WebSite = tbwebSite.Text.Trim
            .Estado = rbestado.SelectedValue
            .IdNacionalidad = CInt(dlNacionalidad.SelectedValue)
            .IdMotivoBaja = CInt(dlMotivoBaja.SelectedValue)
            .IdTipoPV = CInt(dltipoPV.SelectedValue)


            Dim tipopv As Integer
            Dim idrolp As Integer
            tipopv = CInt(dltipoPV.SelectedValue)

            If (tipopv <> 1) Then
                ListaRolPersona = getListaRol()
                For i As Integer = 0 To ListaRolPersona.Count - 1
                    idrolp = ListaRolPersona(i).IdRol
                    If (idrolp = 1) Then
                        If (tipopv <> 1) Then
                            'objScript.mostrarMsjAlerta(Me, "EL PRECIO SOLO PUEDE SER PUBLICO POR SER CLIENTE FINAL")
                            .IdTipoPV = 1
                            'Exit Sub
                        End If
                    End If
                Next
            End If



            If tbfechaBaja.Text <> "" Then .FechaBaja = CDate(tbfechaBaja.Text.Trim)
            .NComercial = tbnombreComercial.Text.Trim

            If tipoPersona = "J" Then

                .IdGiro = CInt(dlgiro.SelectedValue)
                .Propietario = ckpropietario.Checked

                .IdRepresentanteL = CInt(hddidpersonaR.Value)

                With objJuridica
                    .RazonSocial = tbrazonSocial.Text.Trim
                    .IdAgente = Nothing 'CInt(dlTipoAgente.SelectedValue)
                    .RUC = tbrucjuridco.Text.Trim
                End With

                Select Case tipo '======== Validadacion de la persona juridica
                    Case operativo.GuardarNuevo

                        If ValidoRep.ValidarExistenciaxTablax1Campo("Juridica", "jur_Rsocial", CStr(IIf(objJuridica.RazonSocial Is Nothing, "xzxzxz", objJuridica.RazonSocial))) > 0 Then
                            objScript.mostrarMsjAlerta(Me, "La Razón social ya existe.")
                            Exit Sub
                        End If

                    Case operativo.Actualizar

                        If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("Juridica", "jur_Rsocial", CStr(IIf(objJuridica.RazonSocial Is Nothing, "xzxzxz", objJuridica.RazonSocial)), "IdPersona", objPersona.Id) > 0 Then
                            objScript.mostrarMsjAlerta(Me, "La Razón social ya existe.")
                            Exit Sub
                        End If

                End Select

                ListaContactoEmpresa = getListaContactoEmpresa()

            Else
                .IdCentroLaboral = CInt(hddidCentroLaboral.Value)
                .IdContacto = CInt(hddidPersonaE.Value)

                With objNatural
                    .ApellidoMaterno = tbmaterno.Text.Trim
                    .ApellidoPaterno = tbpaterno.Text.Trim
                    .Nombres = tbnombres.Text.Trim
                    .IdCargo = CInt(dlCargo.SelectedValue)
                    .IdEstadoCivil = CInt(dlEstadoCivil.SelectedValue)
                    .IdCondTrabajo = CInt(dlCondicionTrabajo.SelectedValue)

                    If (IsDate(Me.txtFechaNac.Text) And Me.txtFechaNac.Text.Trim.Length > 0) Then
                        .FechaNac = CDate(Me.txtFechaNac.Text)
                    End If

                    If (Me.cboSexo.SelectedValue <> "") Then
                        .Sexo = Me.cboSexo.SelectedValue
                    End If

                End With

                With objChofer
                    .esChofer = ckChofer.Checked
                    .Licencia = tbnroLicencia.Text.Trim
                    .Categoria = tbcategoria.Text.Trim
                    .TotalRegEnDocumento = CInt(hddChoferReg.Value)
                End With

                With objEmpleado
                    .EsEmpleado = ckEmpleado.Checked
                    .IdCatEmpleado = CInt(dlEmpCategoria.SelectedValue)
                    .Estado = rbEmpEstado.SelectedValue
                    .EstaReg = CInt(hddEsempleado.Value)
                    If rbEmpEstado.SelectedValue = "0" Then
                        .IdMotivoBaja = CInt(dlEmpMotivoBaja.SelectedValue)
                        If tbEmpFechaBaja.Text <> "" Then
                            .FechaBaja = CDate(tbEmpFechaBaja.Text)
                        End If
                    End If
                End With


                ListaDocumentoPersona = getListaTipoDocumento()
                ListaProfesionPersona = getListaProfesion()
                ListaOcupacionPersona = getListaOcupacion()
            End If
        End With

        List_PersonaTipoAgente = getList_PersonaTipoAgente()
        ListaRolPersona = getListaRol()
        ListaDireccionPersona = getListaDireccion()
        ListaTelefonoPersona = getListaTelefono()
        ListaCorreoPersona = getListaCorreo()


        Select Case tipo
            Case operativo.GuardarNuevo
                Dim Datos() As String = (New Negocio.Persona).InsertarNuevaPersona(tipoPersona, objPersona, objNatural, _
                                            objChofer, objEmpleado, objJuridica, ListaDireccionPersona, ListaTelefonoPersona, _
                                            ListaCorreoPersona, ListaDocumentoPersona, ListaRolPersona, _
                                            ListaProfesionPersona, ListaOcupacionPersona, ListaContactoEmpresa, List_PersonaTipoAgente).Split(CChar(","))
                If Datos.Length > 1 Then
                    objScript.mostrarMsjAlerta(Me, "El registro ha sido Guardado con exito")
                    tbcodigo.Text = Datos(0)
                    tbfechaAlta.Text = Datos(1)
                    If Datos.Length > 2 Then tbEmpFechaAlta.Text = Datos(2)
                    pnlPersona.Enabled = False
                    btnGuardar.Visible = False
                Else
                    objScript.mostrarMsjAlerta(Me, Datos(0))
                End If
            Case operativo.Actualizar


                Dim Dato As String = (New Negocio.Persona).ActualizarPersona(tipoPersona, objPersona, objNatural, _
                    objChofer, objEmpleado, objJuridica, ListaDireccionPersona, ListaTelefonoPersona, _
                    ListaCorreoPersona, ListaDocumentoPersona, ListaRolPersona, _
                    ListaProfesionPersona, ListaOcupacionPersona, ListaContactoEmpresa, List_PersonaTipoAgente)

                objScript.mostrarMsjAlerta(Me, "El registro ha sido Actulizado con exito")

                tbEmpFechaAlta.Text = Dato
                pnlPersona.Enabled = False
                btnGuardar.Visible = False
        End Select
    End Sub

    Private Sub CargarFormulario(ByVal IdPersona As Integer)
        BuscarPerfil()

        Dim IdPerfil As Integer = CInt(Session("IdPerfil"))
        Dim IdUsuario As Integer = CInt(Session("IdUsuario"))
        objPersona = New Entidades.Persona
        objPersona = (New Negocio.Persona).Selectx_IdPersona(IdPersona)

        With objPersona
            tbcodigo.Text = CStr(.Id)
            tbwebSite.Text = .WebSite
            tbfechaAlta.Text = .FechadeAlta
            tbfechaBaja.Text = .FechadeBaja
            rbestado.SelectedValue = CStr(.Estado)
            If dlnacionalidad.Items.FindByValue(CStr(.IdNacionalidad)) IsNot Nothing Then dlnacionalidad.SelectedValue = CStr(.IdNacionalidad)

            dlMotivoBaja.SelectedValue = CStr(.IdMotivoBaja)

            dlgiro.SelectedValue = CStr(.IdGiro)
            ckpropietario.Checked = .Propietario
            dltipoPV.SelectedValue = CStr(.IdTipoPV)


            Dim listaPermisosRolTipoPV As Integer = (New Negocio.Util).ValidarPermisosRolTipoPv(IdUsuario)
            If listaPermisosRolTipoPV > 0 Then
               gvRol.Enabled = True
            Else
                gvRol.Enabled = False
            End If

            hddIdTipoPv.Value = CStr(.IdTipoPV)
            'validar para que cuando se edite los datos del cliente verifique si el cliente tiene tipo de precio publico solo le 
            'aparezcan los roles que deberian aparacerle

            If CStr(.IdTipoPV) <> "1" And (IdPerfil = 1 Or IdPerfil = 5 Or IdPerfil = 4) Then
                hddIdTipoPv.Value = CStr(.IdTipoPV)
                'gvRol.Enabled = False
                'btnagregarrol.Enabled = False
            Else
                hddIdTipoPv.Value = CStr("0")
                'gvRol.Enabled = True
                'btnagregarrol.Enabled = True
            End If

            ''Validando que solo los perfiles gerentes de tienda tenda la opcion de cambiar el perfil del cliente.
            'If (IdPerfil = 200000004 Or IdPerfil = 4) Then
            '    gvRol.Enabled = True
            'Else
            '    gvRol.Enabled = False
            'End If

            tbnombreComercial.Text = CStr(.NComercial)

            hddidpersonaR.Value = CStr(.IdRepresentanteL)
            cargarContactoXJuridica(IdPersona, .IdRepresentanteL)

            hddidCentroLaboral.Value = CStr(.IdCentroLaboral)
            hddidPersonaE.Value = CStr(.IdContacto)

            cargarContactosNaturales(.IdCentroLaboral, .IdContacto)

        End With

        objNatural = New Entidades.Natural
        objNatural = (New Negocio.Natural).SelectxId(IdPersona)
        With objNatural
            tbpaterno.Text = .ApellidoPaterno

            If .ApellidoPaterno <> "" Or .Nombres <> "" Then
                gvProfesion.DataSource = cargarProfesion(IdPersona)
                gvProfesion.DataBind()

                gvOcupacion.DataSource = cargarOcupacion(IdPersona)
                gvOcupacion.DataBind()

                gvTipoDocumento.DataSource = cargarTipoDocumento(IdPersona)
                gvTipoDocumento.DataBind()

                If (Me.cboSexo.Items.FindByValue(CStr(.Sexo)) IsNot Nothing) Then
                    Me.cboSexo.SelectedValue = .Sexo
                End If

                If (.FechaNac <> Nothing) Then
                    Me.txtFechaNac.Text = Format(.FechaNac, "dd/MM/yyyy")
                Else
                    Me.txtFechaNac.Text = ""
                End If

                objChofer = (New Negocio.Chofer).listarChoferxID(IdPersona)
                If Not objChofer Is Nothing Then
                    tbcategoria.Text = objChofer.Categoria
                    tbnroLicencia.Text = objChofer.Licencia
                    ckChofer.Checked = objChofer.esChofer
                    hddChoferReg.Value = CStr(objChofer.TotalRegEnDocumento)
                Else
                    hddChoferReg.Value = "-1"
                End If

                objEmpleado = (New Negocio.Empleado).SelectxIdEmpleado(IdPersona)
                If Not objEmpleado Is Nothing Then
                    With objEmpleado
                        dlEmpCategoria.SelectedValue = CStr(.IdCatEmpleado)
                        tbEmpFechaAlta.Text = .FechadeAlta
                        tbEmpFechaBaja.Text = .FechadeBaja
                        rbEmpEstado.SelectedValue = .Estado
                        dlEmpMotivoBaja.SelectedValue = CStr(.IdMotivoBaja)
                        ckEmpleado.Checked = .EsEmpleado
                        hddEsempleado.Value = "1"
                    End With
                Else
                    hddEsempleado.Value = "-1"
                End If

            End If

            tbmaterno.Text = .ApellidoMaterno
            tbnombres.Text = .Nombres
            dlCargo.SelectedValue = CStr(.IdCargo)
            dlEstadoCivil.SelectedValue = CStr(.IdEstadoCivil)
            dlCondicionTrabajo.SelectedValue = CStr(.IdCondTrabajo)
        End With

        objJuridica = New Entidades.Juridica
        objJuridica = (New Negocio.Juridica).Selectx_Id(IdPersona)
        With objJuridica
            tbrazonSocial.Text = .RazonSocial
            'dlTipoAgente.SelectedValue = CStr(.IdAgente)
            tbrucjuridco.Text = .RUC
        End With


        gv_tipoAgente.DataSource = (New Negocio.PersonaTipoAgente).PersonaTipoAgente_Select(IdPersona)
        gv_tipoAgente.DataBind()

        gvTelefono.DataSource = cargarTelefono(IdPersona)
        gvTelefono.DataBind()

        gvCorreo.DataSource = cargarCorreo(IdPersona)
        gvCorreo.DataBind()

        gvDireccion.DataSource = cargarDireccion(IdPersona)
        gvDireccion.DataBind()

        gvRol.DataSource = cargarRol(IdPersona)
        gvRol.DataBind()

    End Sub

    Private Sub verBotones(ByVal modo As String)
        Select Case modo
            Case "N"
                btnNuevo.Visible = False
                btnGuardar.Visible = True
                btnCancelar.Visible = True
                rbPersona.Enabled = False
                rbestado.SelectedValue = "1"
                '' rbestado.Enabled = False
                rbEmpEstado.SelectedValue = "1"
                '' rbEmpEstado.Enabled = False
                pnlBusquedaPersona.Visible = False
                pnlPersona.Visible = True
                pnlPersona.Enabled = True
                gvRol.Enabled = True
                btnagregarrol.Enabled = True
            Case "L"
                btnNuevo.Visible = True
                btnGuardar.Visible = False
                btnCancelar.Visible = False
                rbPersona.Enabled = True
                rbEmpEstado.Enabled = True
                pnlBusquedaPersona.Visible = True
                pnlPersona.Visible = False
                pnlPersona.Enabled = False
            Case "G"
                btnNuevo.Visible = True
                btnGuardar.Visible = False
                btnCancelar.Visible = False
        End Select
    End Sub
#End Region

#Region "Limpiar Formulario"

    Private Sub limpiarFormulario()
        hddIdPersona.Value = "0"

        gvTipoDocumento.DataBind()
        gvProfesion.DataBind()
        gvOcupacion.DataBind()
        gvTelefono.DataBind()
        gvCorreo.DataBind()
        gvDireccion.DataBind()
        gvRol.DataBind()
        gvContacto.DataBind()
        gv_tipoAgente.DataBind()

        tbcodigo.Text = ""
        tbwebSite.Text = ""
        tbfechaAlta.Text = ""
        tbfechaBaja.Text = ""
        tbnombreComercial.Text = ""
        tbpaterno.Text = ""
        tbmaterno.Text = ""
        Me.txtFechaNac.Text = ""

        If (Me.cboSexo.Items.FindByValue(CStr("")) IsNot Nothing) Then
            Me.cboSexo.SelectedValue = ""
        End If

        tbnombres.Text = ""
        tbrazonSocial.Text = ""
        tbrucjuridco.Text = ""
        tbcategoria.Text = ""
        tbnroLicencia.Text = ""
        tbEmpFechaAlta.Text = ""
        tbEmpFechaBaja.Text = ""


        dlEmpMotivoBaja.SelectedIndex = 0
        dlEmpCategoria.SelectedIndex = 0
        dlMotivoBaja.SelectedIndex = 0
        dlgiro.SelectedIndex = 0
        dltipoPV.SelectedIndex = 0
        dlCargo.SelectedIndex = 0
        dlEstadoCivil.SelectedIndex = 0
        dlCondicionTrabajo.SelectedIndex = 0
        dlTipoAgente.SelectedIndex = 0

        ckpropietario.Checked = False
        ckEmpleado.Checked = False
        ckChofer.Checked = False

        hddChoferReg.Value = "-1"
        hddEsempleado.Value = "-1"
        LimpiarTablaDireccion()
        limpiarContacto()
        LimpiarTablaBusqueda()
        LimpiarBuscarPersona()
    End Sub

    Private Sub LimpiarTablaDireccion()
        dlTipoDireccion.SelectedIndex = 0
        dlZonificacion.SelectedIndex = 0
        dlTipoVia.SelectedIndex = 0
        tbnroExterior.Text = ""
        tbEtapa.Text = ""
        tbManzana.Text = ""
        tbnroInterior.Text = ""
        tbLote.Text = ""
        tbzona.Text = ""
        tbVia.Text = ""
        tbDireccion.Text = ""
        tbreferencia.Text = ""
    End Sub

    Private Sub LimpiarTablaBusqueda()
        tbRazonApe.Text = ""
        tbbuscarDni.Text = ""
        tbbuscarRuc.Text = ""
        gvBuscar.DataSource = Nothing
        gvBuscar.DataBind()
    End Sub

    Private Sub limpiarContacto()
        hddidpersonaR.Value = "0"
        hddidCentroLaboral.Value = "0"
        hddidPersonaE.Value = "0"
        tbrepresentanteLegal.Text = ""
        tbnrodocRepresentanteLegal.Text = ""
        tbCentroLaboral.Text = ""
        tbCentroLaboralRuc.Text = ""
        tbEmpresaContacto.Text = ""
        tbRucEmpresa.Text = ""
    End Sub

    Private Sub LimpiarBuscarPersona()
        gvBuscarPersona.DataBind()
        tbbuscarPersona.Text = ""
    End Sub

    Private Sub btlimpiarCentroLaboral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlimpiarCentroLaboral.Click
        hddidCentroLaboral.Value = "0"
        tbCentroLaboral.Text = ""
        tbCentroLaboralRuc.Text = ""
    End Sub

    Private Sub btlimpiarContactoEmpresa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlimpiarContactoEmpresa.Click
        hddidPersonaE.Value = "0"
        tbEmpresaContacto.Text = ""
        tbRucEmpresa.Text = ""
    End Sub

    Private Sub btlimpiarRepresentanteLegal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlimpiarRepresentanteLegal.Click
        hddidpersonaR.Value = "0"
        tbrepresentanteLegal.Text = ""
        tbnrodocRepresentanteLegal.Text = ""
    End Sub
#End Region

#Region "Cargar Datos de la Persona"

    Function cargarTipoDocumento(ByVal idPersona As Integer) As List(Of Entidades.DocumentoI)
        ListaDocumentoPersona = (New Negocio.DocumentoI).Selectx_IdPersona(idPersona)
        For x As Integer = 0 To ListaDocumentoPersona.Count - 1
            If x = 0 Then
                ListaDocumentoPersona(x).objDocumento = (New Negocio.DocumentoI).listarTipoDocumento()
            Else
                ListaDocumentoPersona(x).objDocumento = ListaDocumentoPersona(0).objDocumento
            End If
        Next
        Return ListaDocumentoPersona
    End Function
    Function cargarProfesion(ByVal idPersona As Integer) As List(Of Entidades.ProfesionPersona)
        ListaProfesionPersona = (New Negocio.ProfesionPersona).Selectx_Id(idPersona)
        For x As Integer = 0 To ListaProfesionPersona.Count - 1
            If x = 0 Then
                ListaProfesionPersona(x).objProfesion = (New Negocio.Profesion).SelectCbo
            Else
                ListaProfesionPersona(x).objProfesion = ListaProfesionPersona(0).objProfesion
            End If
        Next
        Return ListaProfesionPersona
    End Function
    Function cargarOcupacion(ByVal idPersona As Integer) As List(Of Entidades.OcupacionPersona)
        ListaOcupacionPersona = (New Negocio.OcupacionPersona).Selectx_Id(idPersona)

        For x As Integer = 0 To ListaOcupacionPersona.Count - 1
            If x = 0 Then
                ListaOcupacionPersona(x).objOcupacion = (New Negocio.Ocupacion).SelectCbo
                ListaOcupacionPersona(x).objGiro = (New Negocio.Giro).SelectCbo
            Else
                ListaOcupacionPersona(x).objOcupacion = ListaOcupacionPersona(0).objOcupacion
                ListaOcupacionPersona(x).objGiro = ListaOcupacionPersona(0).objGiro
            End If
        Next

        Return ListaOcupacionPersona
    End Function
    Function cargarTelefono(ByVal idPersona As Integer) As List(Of Entidades.TelefonoView)
        ListaTelefonoPersona = (New Negocio.TelefonoView).SelectxIdPersona(idPersona)
        For x As Integer = 0 To ListaTelefonoPersona.Count - 1
            If x = 0 Then
                ListaTelefonoPersona(x).objTelefono = (New Negocio.TipoTelefono).SelectCbo()
            Else
                ListaTelefonoPersona(x).objTelefono = ListaTelefonoPersona(0).objTelefono
            End If
        Next
        Return ListaTelefonoPersona
    End Function
    Function cargarCorreo(ByVal idPersona As Integer) As List(Of Entidades.CorreoView)
        ListaCorreoPersona = (New Negocio.CorreoView).SelectxIdPersona(idPersona)
        For x As Integer = 0 To ListaCorreoPersona.Count - 1
            If x = 0 Then
                ListaCorreoPersona(x).objCorreo = (New Negocio.TipoCorreo).SelectCbo()
            Else
                ListaCorreoPersona(x).objCorreo = ListaCorreoPersona(0).objCorreo
            End If
        Next
        Return ListaCorreoPersona
    End Function
    Function cargarDireccion(ByVal idPersona As Integer) As List(Of Entidades.Direccion)
        ListaDireccionPersona = (New Negocio.Direccion).Selectx_IdPersona(idPersona)
        Return ListaDireccionPersona
    End Function
    Function cargarRol(ByVal idPersona As Integer) As List(Of Entidades.RolPersona)
        ListaRolPersona = (New Negocio.Rol).SelectxIdPersona(idPersona)
        Dim IdPerfil As Integer = CInt(Session("IdPerfil"))
        For x As Integer = 0 To ListaRolPersona.Count - 1
            If x = 0 Then
                ListaRolPersona(x).objEmpresa = (New Negocio.Rol).listarExisteRolEmpresa()
                ListaRolPersona(x).objMotivoBaja = (New Negocio.Rol).listarMotivoBaja()
                'ListaRolPersona(x).objRol = (New Negocio.Rol).listarRolxEmpresa(ListaRolPersona(x).IdEmpresa)
                ListaRolPersona(x).objRol = (New Negocio.Rol).listarRolxEmpresaUsuario(ListaRolPersona(x).IdEmpresa, CInt(hddIdPersona.Value))

                ''valido para que cuando sea el perfil de vendedor y el perfil de jefe de ventas y el tipo de precio sea diferente a publico se inhabilite
                'If IdPerfil = 1 Or IdPerfil = 5 And CStr(hddIdTipoPv.Value) <> "1" Then
                '    If CStr(hddIdTipoPv.Value) = "0" Then
                '        gvRol.Enabled = True
                '        btnagregarrol.Enabled = True
                '    Else
                '        gvRol.Enabled = False
                '        btnagregarrol.Enabled = False
                '    End If

                'Else
                '    gvRol.Enabled = True
                '    btnagregarrol.Enabled = True
                'End If
            Else
                ListaRolPersona(x).objEmpresa = ListaRolPersona(0).objEmpresa
                ListaRolPersona(x).objMotivoBaja = ListaRolPersona(0).objMotivoBaja

                For i As Integer = 0 To ListaRolPersona.Count - 1
                    If ListaRolPersona(x).IdEmpresa = ListaRolPersona(i).IdEmpresa Then
                        ListaRolPersona(x).objRol = ListaRolPersona(i).objRol
                        Exit For
                    End If
                Next
                If ListaRolPersona(x).objRol Is Nothing Then
                    'ListaRolPersona(x).objRol = (New Negocio.Rol).listarRolxEmpresa(ListaRolPersona(x).IdEmpresa)
                    ListaRolPersona(x).objRol = (New Negocio.Rol).listarRolxEmpresaUsuario(ListaRolPersona(x).IdEmpresa, CInt(hddIdPersona.Value))


                End If
            End If
        Next

        Return ListaRolPersona
    End Function
    Private Sub cargarContactoXJuridica(ByVal IdPersona As Integer, ByVal IdRepresentanteLegal As Integer)
        If IdPersona <> 0 Then '************************ Contactos de la Empresa
            ListaContactoEmpresa = (New Negocio.Persona).ListarPersonaComplemento(IdPersona, "C")
            gvContacto.DataSource = ListaContactoEmpresa
            gvContacto.DataBind()
        End If

        If IdRepresentanteLegal <> 0 Then '************* RepresentanteLegal
            ListaAuxContacto = (New Negocio.Persona).ListarPersonaComplemento(IdRepresentanteLegal, "R")
            hddidpersonaR.Value = CStr(ListaAuxContacto(0).IdPersona)
            tbrepresentanteLegal.Text = ListaAuxContacto(0).Nombre
            tbnrodocRepresentanteLegal.Text = ListaAuxContacto(0).Dni
        End If

    End Sub
    Private Sub cargarContactosNaturales(ByVal IdCentroLaboral As Integer, ByVal IdEmpresaYoContacto As Integer)
        If IdEmpresaYoContacto <> 0 Then '********************** Empresa de la cual la persona es contacto
            ListaAuxContacto = (New Negocio.Persona).ListarPersonaComplemento(IdEmpresaYoContacto, "E")
            hddidPersonaE.Value = CStr(ListaAuxContacto(0).IdPersona)
            tbEmpresaContacto.Text = ListaAuxContacto(0).Nombre
            tbRucEmpresa.Text = ListaAuxContacto(0).Ruc
        End If
        If IdCentroLaboral <> 0 Then '*********************** Centro Laboral de la Persona
            ListaAuxContacto = (New Negocio.Persona).ListarPersonaComplemento(IdCentroLaboral, "L")
            hddidCentroLaboral.Value = CStr(ListaAuxContacto(0).IdPersona)
            tbCentroLaboral.Text = ListaAuxContacto(0).Nombre
            tbCentroLaboralRuc.Text = ListaAuxContacto(0).Ruc
        End If
    End Sub
   
#End Region

#Region "Buscar Personas Complemento"
    Private Sub BuscarPersona(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, ByVal nombre As String, _
                              ByVal tipo As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(tbPageIndex_Persona.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(tbPageIndex_Persona.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(tbPageIndexGO_Persona.Text) - 1)
        End Select

        ListaBuscarPersona = (New Negocio.Persona).listarPersonaNaturalPersonaJuridica(dni, ruc, nombre, tipo, index, gv.PageSize)
        If ListaBuscarPersona.Count > 0 Then
            gv.DataSource = ListaBuscarPersona
            gv.DataBind()
            tbPageIndex_Persona.Text = CStr(index + 1)
            objScript.onCapa(Me, "verCapaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('verCapaPersona'); alert('No se hallaron registros');", True)
        End If

    End Sub

    Private Sub btAnterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior_Persona.Click
        Try
            Select Case CInt(ViewState("cboFiltro"))
                Case 0 '********* Nombre
                    BuscarPersona(gvBuscarPersona, "", "", CStr(ViewState("tbBuscar")), CInt(ViewState("tipo")), 1)
                Case 1 '********* Dni
                    BuscarPersona(gvBuscarPersona, CStr(ViewState("tbBuscar")), "", "", CInt(ViewState("tipo")), 1)
                Case 2 '********* Ruc
                    BuscarPersona(gvBuscarPersona, "", CStr(ViewState("tbBuscar")), "", CInt(ViewState("tipo")), 1)
            End Select
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente_Persona.Click
        Try
            Select Case CInt(ViewState("cboFiltro"))
                Case 0 '********* Nombre
                    BuscarPersona(gvBuscarPersona, "", "", CStr(ViewState("tbBuscar")), CInt(ViewState("tipo")), 2)
                Case 1 '********* Dni
                    BuscarPersona(gvBuscarPersona, CStr(ViewState("tbBuscar")), "", "", CInt(ViewState("tipo")), 2)
                Case 2 '********* Ruc
                    BuscarPersona(gvBuscarPersona, "", CStr(ViewState("tbBuscar")), "", CInt(ViewState("tipo")), 2)
            End Select

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr_Persona.Click
        Try
            Select Case CInt(ViewState("cboFiltro"))
                Case 0 '********* Nombre
                    BuscarPersona(gvBuscarPersona, "", "", CStr(ViewState("tbBuscar")), CInt(ViewState("tipo")), 3)
                Case 1 '********* Dni
                    BuscarPersona(gvBuscarPersona, CStr(ViewState("tbBuscar")), "", "", CInt(ViewState("tipo")), 3)
                Case 2 '********* Ruc
                    BuscarPersona(gvBuscarPersona, "", CStr(ViewState("tbBuscar")), "", CInt(ViewState("tipo")), 3)
            End Select
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Function getListaContactoEmpresa() As List(Of Entidades.PersonaView)
        ListaContactoEmpresa = New List(Of Entidades.PersonaView)
        For Each fila As GridViewRow In gvContacto.Rows
            Dim obj As New Entidades.PersonaView
            With obj
                .IdPersona = CInt(CType(fila.Cells(1).FindControl("hddidPersonaC"), HiddenField).Value.Trim)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(2).FindControl("gvlbContacto"), Label).Text.Trim)
                .Dni = HttpUtility.HtmlDecode(fila.Cells(2).Text)
            End With
            ListaContactoEmpresa.Add(obj)
        Next
        Return ListaContactoEmpresa
    End Function

    Private Sub gvBuscarPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscarPersona.SelectedIndexChanged
        Try
            Select Case hddtipoBusqueda.Value.Substring(2, 1)
                Case "R" '************************ Representante Legal
                    hddidpersonaR.Value = gvBuscarPersona.SelectedRow.Cells(1).Text.Trim
                    tbrepresentanteLegal.Text = HttpUtility.HtmlDecode(gvBuscarPersona.SelectedRow.Cells(2).Text)
                    tbnrodocRepresentanteLegal.Text = HttpUtility.HtmlDecode(gvBuscarPersona.SelectedRow.Cells(4).Text)

                Case "C" '************************ Contacto de la empresa
                    ListaContactoEmpresa = getListaContactoEmpresa()
                    Dim obj As New Entidades.PersonaView
                    With obj
                        .IdPersona = CInt(gvBuscarPersona.SelectedRow.Cells(1).Text.Trim)
                        .Nombre = HttpUtility.HtmlDecode(gvBuscarPersona.SelectedRow.Cells(2).Text)
                        .Dni = HttpUtility.HtmlDecode(gvBuscarPersona.SelectedRow.Cells(4).Text)
                    End With
                    ListaContactoEmpresa.Add(obj)
                    gvContacto.DataSource = ListaContactoEmpresa
                    gvContacto.DataBind()

                Case "E" '************************ Empresa yo soy Contacto
                    hddidPersonaE.Value = gvBuscarPersona.SelectedRow.Cells(1).Text.Trim
                    tbEmpresaContacto.Text = HttpUtility.HtmlDecode(gvBuscarPersona.SelectedRow.Cells(2).Text)
                    tbRucEmpresa.Text = HttpUtility.HtmlDecode(gvBuscarPersona.SelectedRow.Cells(3).Text)

                Case "L" '*********************** Mi centro Laboral
                    hddidCentroLaboral.Value = gvBuscarPersona.SelectedRow.Cells(1).Text.Trim
                    tbCentroLaboral.Text = HttpUtility.HtmlDecode(gvBuscarPersona.SelectedRow.Cells(2).Text)
                    tbCentroLaboralRuc.Text = HttpUtility.HtmlDecode(gvBuscarPersona.SelectedRow.Cells(3).Text)
            End Select
            objScript.offCapa(Me, "verCapaPersona")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvContacto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvContacto.SelectedIndexChanged
        Try
            ListaContactoEmpresa = getListaContactoEmpresa()
            ListaContactoEmpresa.RemoveAt(gvContacto.SelectedIndex)
            gvContacto.DataSource = ListaContactoEmpresa
            gvContacto.DataBind()
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "Buscar Personas Mantenimiento"
    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(tbPageIndexGO.Text) - 1)
        End Select

        ListaBuscarPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado, CInt(ViewState("IdNacionalidad")))

        If ListaBuscarPersona.Count > 0 Then
            gv.DataSource = ListaBuscarPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
        Else

            objScript.mostrarMsjAlerta(Me, "No se encontraron registros")
        End If

    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged
        Try

            hddIdPersona.Value = gvBuscar.SelectedRow.Cells(1).Text
            verBotones("N")
            CargarFormulario(CInt(hddIdPersona.Value))

            If dlDistrito.DataSource Is Nothing Then
                drop = New Combo
                dlDepartamento.SelectedValue = "15"
                drop.LLenarCboProvincia(dlProvincia, dlDepartamento.SelectedValue)
                dlProvincia.SelectedValue = "01"
                drop.LLenarCboDistrito(dlDistrito, dlDepartamento.SelectedValue, dlProvincia.SelectedValue)
                dlDistrito.SelectedValue = "01"
            End If

            rbPersona.SelectedValue = CStr(IIf(CInt(ViewState("tipo")) = 1, "N", "J"))
            rbPersona_SelectedIndexChanged(sender, e)
            rbestado.Enabled = True
            rbEmpEstado.Enabled = True
            hddoperativo.Value = CStr(operativo.Actualizar)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "eventos principales"
    Private Sub ConfigurarDatos()
        hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Try

                llenarDropdownList()
                rbPersona_SelectedIndexChanged(sender, e)
                hddoperativo.Value = CStr(operativo.Ninguno)
                verBotones("L")
                ValidarPermisos(CInt(Session("IdUsuario")))
                ConfigurarDatos()

                If Request.QueryString("IdRol") IsNot Nothing Then
                    hdd_idrolurl.Value = CStr(Request.QueryString("IdRol"))
                    Try
                        Dim cad() As String = hdd_idrolurl.Value.Split(CChar(","))
                        dlRol.SelectedValue = cad(0)
                    Catch ex As Exception

                    End Try
                End If

            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message)
            End Try

        End If
    End Sub

    Private Sub rbPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPersona.SelectedIndexChanged
        SelectTipoesona(rbPersona.SelectedValue)
    End Sub
    Private Sub SelectTipoesona(ByVal valor As String)
        pnlJuridica.Visible = False
        pnlNatural.Visible = False
        Select Case valor
            Case "N" 'Natural
                pnlNatural.Visible = True
            Case "J" 'Juridica
                pnlJuridica.Visible = True
        End Select
    End Sub

#End Region

#Region "agregar a las cuadriculas"

    Private Sub btnagregarTipoDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnagregarTipoDoc.Click
        Try
            llenargrillaTipoDocumento(gvTipoDocumento)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btagregarRol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnagregarrol.Click
        Try
            AddRol()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub AddRol()
        llenargrillaRol(gvRol)
    End Sub

    Private Sub btagregarCorreo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnagregarCorreo.Click
        Try
            llenargrillaCorreo(gvCorreo)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btagregarOcupacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnagregarOcupacion.Click
        Try
            llenargrillaOcupacion(gvOcupacion)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btagregarTelefono_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnagregarTelefono.Click
        Try
            llenargrillaTelefono(gvTelefono)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btagregarDireccion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnagregarDireccion.Click
        Try
            llenargrillaDireccion(gvDireccion)
            LimpiarTablaDireccion()
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btagregarProfesion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnagregarProfesion.Click
        Try
            llenargrillaProfesion(gvProfesion)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#End Region

#Region "Seccion de Rol"

    Dim objRol As Entidades.RolPersona

    Private Sub llenargrillaRol(ByVal gv As GridView)
        Dim lista_rol_empresa As List(Of Entidades.Rol_Empresa)
        Dim IdUsuario As Integer = CInt(Session("IdUsuario"))
        ListaRolPersona = getListaRol()
        objRol = New Entidades.RolPersona

        With objRol
            .IdEmpresa = 0
            .IdMotivoBaja = 0
            .IdRol = 0

            If ListaRolPersona.Count = 0 Then
                .objEmpresa = (New Negocio.Rol).listarExisteRolEmpresa()
                .objMotivoBaja = (New Negocio.Rol).listarMotivoBaja()
            Else
                .objEmpresa = ListaRolPersona(0).objEmpresa
                .objMotivoBaja = ListaRolPersona(0).objMotivoBaja
            End If
            lista_rol_empresa = CType(.objEmpresa, List(Of Entidades.Rol_Empresa))

            '.objRol = (New Negocio.Rol).listarRolxEmpresa(lista_rol_empresa(0).IdEmpresa)
            .objRol = (New Negocio.Rol).listarRolxEmpresaUsuario(lista_rol_empresa(0).IdEmpresa, CInt(hddIdPersona.Value))
            '.objRol = (New Negocio.Rol).listarRolxEmpresaUsuario(lista_rol_empresa(0).IdEmpresa, IdUsuario)
            .Estado = True
        End With

        ListaRolPersona.Add(objRol)
        gv.DataSource = ListaRolPersona
        gv.DataBind()

      
    End Sub

    Function getListaRol() As List(Of Entidades.RolPersona)

        ListaRolPersona = New List(Of Entidades.RolPersona)

        For Each filas As GridViewRow In gvRol.Rows
            objRol = New Entidades.RolPersona

            With objRol
                .IdEmpresa = CInt(CType(filas.Cells(1).FindControl("gvdlEmpresa"), DropDownList).SelectedValue)
                .IdMotivoBaja = CInt(CType(filas.Cells(4).FindControl("gvdlMotivoBaja"), DropDownList).SelectedValue)

                If ListaRolPersona.Count = 0 Then
                    .objEmpresa = getDropEmpresa()
                    .objMotivoBaja = getDropMotivoBaja()
                Else
                    .objEmpresa = ListaRolPersona(0).objEmpresa
                    .objMotivoBaja = ListaRolPersona(0).objMotivoBaja
                End If

                Dim cboRol As DropDownList = CType(filas.Cells(2).FindControl("gvdlRol"), DropDownList)
                .IdRol = CInt(IIf(cboRol.SelectedValue = "", 0, cboRol.SelectedValue))
                Dim listaRol As New List(Of Entidades.Rol_Empresa)
                For x As Integer = 0 To cboRol.Items.Count - 1
                    Dim objRol As New Entidades.Rol_Empresa
                    With objRol
                        .IdRol = CInt(cboRol.Items(x).Value)
                        .Rol = cboRol.Items(x).Text
                    End With
                    listaRol.Add(objRol)
                Next
                .objRol = listaRol

                .Estado = CType(filas.Cells(3).FindControl("gvckestado"), CheckBox).Checked
            End With

            ListaRolPersona.Add(objRol)
        Next

        Return ListaRolPersona
    End Function
    Function getDropEmpresa() As List(Of Entidades.Rol_Empresa)
        Dim cboEmpresa As DropDownList = CType(gvRol.Rows(0).Cells(1).FindControl("gvdlEmpresa"), DropDownList)
        Dim listaEmpresa As New List(Of Entidades.Rol_Empresa)
        Dim objEmpresa As Entidades.Rol_Empresa

        For x As Integer = 0 To cboEmpresa.Items.Count - 1
            objEmpresa = New Entidades.Rol_Empresa
            With objEmpresa
                .IdEmpresa = CInt(cboEmpresa.Items(x).Value)
                .Empresa = cboEmpresa.Items(x).Text
            End With
            listaEmpresa.Add(objEmpresa)
        Next

        Return listaEmpresa
    End Function
    Function getDropMotivoBaja() As List(Of Entidades.MotivoBaja)
        Dim cboMotivoBaja As DropDownList = CType(gvRol.Rows(0).Cells(4).FindControl("gvdlMotivoBaja"), DropDownList)
        Dim listaMotivoBaja As New List(Of Entidades.MotivoBaja)
        Dim objMotivoBaja As Entidades.MotivoBaja

        For x As Integer = 0 To cboMotivoBaja.Items.Count - 1
            objMotivoBaja = New Entidades.MotivoBaja
            With objMotivoBaja
                .Id = CInt(cboMotivoBaja.Items(x).Value)
                .Descripcion = cboMotivoBaja.Items(x).Text
            End With
            listaMotivoBaja.Add(objMotivoBaja)
        Next

        Return listaMotivoBaja
    End Function
    Protected Sub gvdlEmpresa_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim cbo As DropDownList = CType(sender, DropDownList)
            Dim fila As GridViewRow = CType(cbo.NamingContainer, GridViewRow)
            Dim cboRol As DropDownList = CType(fila.Cells(2).FindControl("gvdlRol"), DropDownList)
            cboRol.Items.Clear()
            If CInt(cbo.SelectedValue) <> 0 Then
                Dim Lista As List(Of Entidades.Rol_Empresa) = (New Negocio.Rol).listarRolxEmpresa(CInt(cbo.SelectedValue))
                'Dim Lista As List(Of Entidades.Rol_Empresa) = (New Negocio.Rol).listarRolxEmpresaUsuario(CInt(cbo.SelectedValue), CInt(hddIdPersona.Value))

                For x As Integer = 0 To Lista.Count - 1
                    cboRol.Items.Add(New ListItem(Lista(x).Rol, CStr(Lista(x).IdRol)))
                Next
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvRol_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRol.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If (Me.ListaRolPersona(e.Row.RowIndex).IdEmpresa <> 0) Then
                    CType(e.Row.Cells(1).FindControl("gvdlEmpresa"), DropDownList).SelectedValue = CStr(Me.ListaRolPersona(e.Row.RowIndex).IdEmpresa)
                    Dim cboRol As DropDownList = CType(e.Row.Cells(2).FindControl("gvdlRol"), DropDownList)
                    If cboRol.Items.Count > 0 Then
                        cboRol.SelectedValue = CStr(Me.ListaRolPersona(e.Row.RowIndex).IdRol)
                    End If
                End If

                Dim cboMotivoBaja As DropDownList = CType(e.Row.Cells(4).FindControl("gvdlMotivoBaja"), DropDownList)

                If (Me.ListaRolPersona(e.Row.RowIndex).IdMotivoBaja <> 0) Then
                    cboMotivoBaja.SelectedValue = CStr(Me.ListaRolPersona(e.Row.RowIndex).IdMotivoBaja)
                End If

                If (ListaRolPersona(e.Row.RowIndex).Estado) Then
                    cboMotivoBaja.Enabled = False
                Else
                    cboMotivoBaja.Enabled = True
                End If
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub gvRol_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvRol.SelectedIndexChanged
        ListaRolPersona = getListaRol()
        ListaRolPersona.RemoveAt(gvRol.SelectedIndex)
        gvRol.DataSource = ListaRolPersona
        gvRol.DataBind()
    End Sub

#End Region

#Region "Seccion de Documento"

    Function getDropTipoDocumento() As List(Of Entidades.TipoDocumentoI)
        Dim cboTipoDocumento As DropDownList = CType(gvTipoDocumento.Rows(0).Cells(1).FindControl("gvdltipoDoc"), DropDownList)
        Dim listaTipoDocumentoI As New List(Of Entidades.TipoDocumentoI)
        For x As Integer = 0 To cboTipoDocumento.Items.Count - 1
            Dim objTipoDocumentoI As New Entidades.TipoDocumentoI
            With objTipoDocumentoI
                .Id = CInt(cboTipoDocumento.Items(x).Value)
                .Abv = cboTipoDocumento.Items(x).Text
            End With
            listaTipoDocumentoI.Add(objTipoDocumentoI)
        Next
        Return listaTipoDocumentoI
    End Function

    Private Sub llenargrillaTipoDocumento(ByVal gv As GridView)
        Dim obj As New Entidades.DocumentoI
        If gv.Rows.Count = 0 Then
            ListaDocumentoPersona = New List(Of Entidades.DocumentoI)

            obj.IdTipoDocumentoI = 0
            obj.objDocumento = (New Negocio.DocumentoI).listarTipoDocumento()
            obj.Numero = String.Empty
        Else
            ListaDocumentoPersona = getListaTipoDocumento()
            obj.IdTipoDocumentoI = 0
            obj.objDocumento = getDropTipoDocumento()
            obj.Numero = String.Empty
        End If
        ListaDocumentoPersona.Add(obj)
        gv.DataSource = ListaDocumentoPersona
        gv.DataBind()
    End Sub

    Function getListaTipoDocumento() As List(Of Entidades.DocumentoI)
        ListaDocumentoPersona = New List(Of Entidades.DocumentoI)

        For Each filas As GridViewRow In gvTipoDocumento.Rows
            Dim obj As New Entidades.DocumentoI

            With obj
                Dim cboTipoDocumento As DropDownList = CType(filas.Cells(1).FindControl("gvdltipoDoc"), DropDownList)
                .IdTipoDocumentoI = CInt(IIf(cboTipoDocumento.SelectedValue = "", 0, cboTipoDocumento.SelectedValue))
                .objDocumento = getDropTipoDocumento()
                .Numero = CType(filas.Cells(1).FindControl("gvtbnroDoc"), TextBox).Text
            End With
            ListaDocumentoPersona.Add(obj)
        Next

        Return ListaDocumentoPersona
    End Function

    Private Sub gvTipoDocumento_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTipoDocumento.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboTipoDocumento As DropDownList = CType(e.Row.Cells(1).FindControl("gvdltipoDoc"), DropDownList)

                If ListaDocumentoPersona(e.Row.RowIndex).IdTipoDocumentoI <> 0 Then
                    cboTipoDocumento.SelectedValue = CStr(ListaDocumentoPersona(e.Row.RowIndex).IdTipoDocumentoI)
                End If
            End If
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTipoDocumento.SelectedIndexChanged
        Try
            ListaDocumentoPersona = getListaTipoDocumento()
            ListaDocumentoPersona.RemoveAt(gvTipoDocumento.SelectedIndex)
            gvTipoDocumento.DataSource = ListaDocumentoPersona
            gvTipoDocumento.DataBind()
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

#Region "Seccion de Profesion y/o Ocupacion"

    Function getDropProfesion() As List(Of Entidades.Profesion)
        Dim cboProfesion As DropDownList = CType(gvProfesion.Rows(0).Cells(1).FindControl("gvdlProfesion"), DropDownList)
        Dim ListaProfesion As New List(Of Entidades.Profesion)
        For x As Integer = 0 To cboProfesion.Items.Count - 1
            Dim objProfesion As New Entidades.Profesion
            With objProfesion
                .Id = CInt(cboProfesion.Items(x).Value)
                .Nombre = cboProfesion.Items(x).Text
            End With
            ListaProfesion.Add(objProfesion)
        Next
        Return ListaProfesion
    End Function

    Private Sub llenargrillaProfesion(ByVal gv As GridView)
        Dim obj As New Entidades.ProfesionPersona
        If gv.Rows.Count = 0 Then
            ListaProfesionPersona = New List(Of Entidades.ProfesionPersona)
            With obj
                .IdPersona = 0
                .IdProfesion = 0
                .objProfesion = (New Negocio.Profesion).SelectCbo
                .Principal = False
            End With
        Else
            ListaProfesionPersona = getListaProfesion()
            With obj
                .IdPersona = 0
                .IdProfesion = 0
                .objProfesion = getDropProfesion()
                .Principal = False
            End With
        End If

        ListaProfesionPersona.Add(obj)
        gv.DataSource = ListaProfesionPersona
        gv.DataBind()
    End Sub

    Private Function getListaProfesion() As List(Of Entidades.ProfesionPersona)
        ListaProfesionPersona = New List(Of Entidades.ProfesionPersona)
        For Each fila As GridViewRow In gvProfesion.Rows
            Dim obj As New Entidades.ProfesionPersona
            With obj
                .IdPersona = 0
                Dim cboProfesion As DropDownList = CType(fila.Cells(1).FindControl("gvdlProfesion"), DropDownList)
                .IdProfesion = CInt(IIf(cboProfesion.SelectedValue = "", 0, cboProfesion.SelectedValue))
                .objProfesion = getDropProfesion()
                .Principal = CType(fila.Cells(2).FindControl("gvckPrincipal"), CheckBox).Checked
            End With

            ListaProfesionPersona.Add(obj)
        Next
        Return ListaProfesionPersona
    End Function

    Private Sub gvProfesion_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvProfesion.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cboProfesion As DropDownList = CType(e.Row.Cells(1).FindControl("gvdlProfesion"), DropDownList)
                If ListaProfesionPersona(e.Row.RowIndex).IdProfesion <> 0 Then
                    cboProfesion.SelectedValue = CStr(ListaProfesionPersona(e.Row.RowIndex).IdProfesion)
                End If

                Dim ckPrincipal As CheckBox = CType(e.Row.Cells(2).FindControl("gvckPrincipal"), CheckBox)
                If ListaProfesionPersona(e.Row.RowIndex).Principal <> False Then
                    ckPrincipal.Checked = ListaProfesionPersona(e.Row.RowIndex).Principal
                End If

            End If
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvProfesion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvProfesion.SelectedIndexChanged
        Try
            ListaProfesionPersona = getListaProfesion()
            ListaProfesionPersona.RemoveAt(gvProfesion.SelectedIndex)
            gvProfesion.DataSource = ListaProfesionPersona
            gvProfesion.DataBind()
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Function getDropOcupacion() As List(Of Entidades.Ocupacion)
        Dim cboOcupacion As DropDownList = CType(gvOcupacion.Rows(0).Cells(1).FindControl("gvdlOcupacion"), DropDownList)
        Dim ListaOcupacion As New List(Of Entidades.Ocupacion)
        For x As Integer = 0 To cboOcupacion.Items.Count - 1
            Dim objOcupacion As New Entidades.Ocupacion
            With objOcupacion
                .Id = CInt(cboOcupacion.Items(x).Value)
                .Nombre = cboOcupacion.Items(x).Text
            End With
            ListaOcupacion.Add(objOcupacion)
        Next
        Return ListaOcupacion
    End Function

    Function getDropGiro() As List(Of Entidades.Giro)
        Dim cboGiro As DropDownList = CType(gvOcupacion.Rows(0).Cells(3).FindControl("gvdlGiro"), DropDownList)
        Dim ListaGiro As New List(Of Entidades.Giro)
        For x As Integer = 0 To cboGiro.Items.Count - 1
            Dim objGiro As New Entidades.Giro
            With objGiro
                .Id = CInt(cboGiro.Items(x).Value)
                .Descripcion = cboGiro.Items(x).Text
            End With
            ListaGiro.Add(objGiro)
        Next
        Return ListaGiro
    End Function

    Private Sub llenargrillaOcupacion(ByVal gv As GridView)
        Dim obj As New Entidades.OcupacionPersona

        If gv.Rows.Count = 0 Then
            ListaOcupacionPersona = New List(Of Entidades.OcupacionPersona)
            With obj
                .IdOcupacion = 0
                .objOcupacion = (New Negocio.Ocupacion).SelectCbo
                .IdGiro = 0
                .objGiro = (New Negocio.Giro).SelectCbo
                .Principal = False
            End With
        Else
            ListaOcupacionPersona = getListaOcupacion()
            With obj
                .IdOcupacion = 0
                .objOcupacion = getDropOcupacion()
                .IdGiro = 0
                .objGiro = getDropGiro()
                .Principal = False
            End With
        End If
        ListaOcupacionPersona.Add(obj)
        gv.DataSource = ListaOcupacionPersona
        gv.DataBind()
    End Sub

    Private Function getListaOcupacion() As List(Of Entidades.OcupacionPersona)
        ListaOcupacionPersona = New List(Of Entidades.OcupacionPersona)

        For Each fila As GridViewRow In gvOcupacion.Rows
            Dim obj As New Entidades.OcupacionPersona
            With obj
                .IdOcupacion = CInt(CType(fila.Cells(1).FindControl("gvdlOcupacion"), DropDownList).SelectedValue)
                .objOcupacion = getDropOcupacion()
                .IdGiro = CInt(CType(fila.Cells(3).FindControl("gvdlGiro"), DropDownList).SelectedValue)
                .objGiro = getDropGiro()
                .Principal = CType(fila.Cells(2).FindControl("gvckPrincipal"), CheckBox).Checked
            End With
            ListaOcupacionPersona.Add(obj)
        Next
        Return ListaOcupacionPersona
    End Function

    Private Sub gvOcupacion_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOcupacion.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboOcupacion As DropDownList = CType(e.Row.Cells(1).FindControl("gvdlOcupacion"), DropDownList)
                If ListaOcupacionPersona(e.Row.RowIndex).IdOcupacion <> 0 Then
                    cboOcupacion.SelectedValue = CStr(ListaOcupacionPersona(e.Row.RowIndex).IdOcupacion)
                End If

                Dim cboGiro As DropDownList = CType(e.Row.Cells(3).FindControl("gvdlGiro"), DropDownList)
                If ListaOcupacionPersona(e.Row.RowIndex).IdGiro <> 0 Then
                    cboGiro.SelectedValue = CStr(ListaOcupacionPersona(e.Row.RowIndex).IdGiro)
                End If

                Dim ckPrincipal As CheckBox = CType(e.Row.Cells(2).FindControl("gvckPrincipal"), CheckBox)
                If ListaOcupacionPersona(e.Row.RowIndex).Principal <> False Then
                    ckPrincipal.Checked = ListaOcupacionPersona(e.Row.RowIndex).Principal
                End If

            End If
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvOcupacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvOcupacion.SelectedIndexChanged
        Try
            ListaOcupacionPersona = getListaOcupacion()
            ListaOcupacionPersona.RemoveAt(gvOcupacion.SelectedIndex)
            gvOcupacion.DataSource = ListaOcupacionPersona
            gvOcupacion.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

#Region "Seccion de Telefono"

    Function getDropTelefono() As List(Of Entidades.TipoTelefono)
        Dim cboTelefono As DropDownList = CType(gvTelefono.Rows(0).Cells(1).FindControl("gvdlTipoTelefono"), DropDownList)
        Dim ListaTelefono As New List(Of Entidades.TipoTelefono)
        For x As Integer = 0 To cboTelefono.Items.Count - 1
            Dim objTelefono As New Entidades.TipoTelefono
            With objTelefono
                .Id = CInt(cboTelefono.Items(x).Value)
                .Nombre = cboTelefono.Items(x).Text
            End With
            ListaTelefono.Add(objTelefono)
        Next
        Return ListaTelefono
    End Function

    Private Sub llenargrillaTelefono(ByVal gv As GridView)
        Dim obj As New Entidades.TelefonoView

        If gv.Rows.Count = 0 Then
            ListaTelefonoPersona = New List(Of Entidades.TelefonoView)
            With obj
                .IdPersona = 0
                .IdTipoTelefono = 0
                .objTelefono = (New Negocio.TipoTelefono).SelectCbo()
                .Prioridad = False
            End With
        Else
            ListaTelefonoPersona = getListaTelefono()
            With obj
                .IdTipoTelefono = 0
                .objTelefono = getDropTelefono()
                .Prioridad = False
            End With
        End If

        ListaTelefonoPersona.Add(obj)
        gv.DataSource = ListaTelefonoPersona
        gv.DataBind()
    End Sub

    Private Function getListaTelefono() As List(Of Entidades.TelefonoView)
        ListaTelefonoPersona = New List(Of Entidades.TelefonoView)
        For Each fila As GridViewRow In Me.gvTelefono.Rows
            Dim obj As New Entidades.TelefonoView
            With obj
                .IdPersona = 0

                Dim cboTipoTelefono As DropDownList = CType(fila.Cells(1).FindControl("gvdlTipoTelefono"), DropDownList)
                .IdTipoTelefono = CInt(IIf(cboTipoTelefono.SelectedValue = "", 0, cboTipoTelefono.SelectedValue))
                .objTelefono = getDropTelefono()

                .Numero = CType(fila.Cells(2).FindControl("gvtbNroTelefono"), TextBox).Text.Trim
                .Anexo = CType(fila.Cells(3).FindControl("gvtbAnexo"), TextBox).Text.Trim
                .Prioridad = CType(fila.Cells(4).FindControl("gvckPrioridad"), CheckBox).Checked

            End With
            ListaTelefonoPersona.Add(obj)
        Next
        Return ListaTelefonoPersona
    End Function

    Private Sub gvTelefono_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTelefono.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboTelefono As DropDownList = CType(e.Row.Cells(1).FindControl("gvdlTipoTelefono"), DropDownList)
                If ListaTelefonoPersona(e.Row.RowIndex).IdTipoTelefono <> 0 Then
                    cboTelefono.SelectedValue = CStr(ListaTelefonoPersona(e.Row.RowIndex).IdTipoTelefono)
                End If

                Dim ckPrioridad As CheckBox = CType(e.Row.Cells(4).FindControl("gvckPrioridad"), CheckBox)
                If ListaTelefonoPersona(e.Row.RowIndex).Prioridad <> False Then
                    ckPrioridad.Checked = ListaTelefonoPersona(e.Row.RowIndex).Prioridad
                End If

            End If
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvTelefono_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTelefono.SelectedIndexChanged
        Try
            ListaTelefonoPersona = getListaTelefono()
            ListaTelefonoPersona.RemoveAt(gvTelefono.SelectedIndex)
            gvTelefono.DataSource = ListaTelefonoPersona
            gvTelefono.DataBind()
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

#Region "seccion de Correo"

    Private Sub llenargrillaCorreo(ByVal gv As GridView)
        Dim obj As New Entidades.CorreoView
        If gv.Rows.Count = 0 Then
            ListaCorreoPersona = New List(Of Entidades.CorreoView)
            obj.IdPersona = 0
            obj.IdTipoCorreo = 0
            obj.objCorreo = (New Negocio.TipoCorreo).SelectCbo
        Else
            ListaCorreoPersona = getListaCorreo()
            obj.IdPersona = 0
            obj.IdTipoCorreo = 0
            obj.objCorreo = getDropTipoCorreo()
        End If
        ListaCorreoPersona.Add(obj)
        gv.DataSource = ListaCorreoPersona
        gv.DataBind()
    End Sub

    Function getDropTipoCorreo() As List(Of Entidades.TipoCorreo)
        Dim cboTipoCorreo As DropDownList = CType(gvCorreo.Rows(0).Cells(1).FindControl("gvdlTipoCorreo"), DropDownList)
        Dim listaTipoCorreo As New List(Of Entidades.TipoCorreo)
        For x As Integer = 0 To cboTipoCorreo.Items.Count - 1
            Dim objTipoCorreo As New Entidades.TipoCorreo
            With objTipoCorreo
                .Id = CInt(cboTipoCorreo.Items(x).Value)
                .Nombre = cboTipoCorreo.Items(x).Text
            End With
            listaTipoCorreo.Add(objTipoCorreo)
        Next
        Return listaTipoCorreo
    End Function

    Private Function getListaCorreo() As List(Of Entidades.CorreoView)
        ListaCorreoPersona = New List(Of Entidades.CorreoView)
        For Each fila As GridViewRow In gvCorreo.Rows
            Dim obj As New Entidades.CorreoView
            With obj

                Dim cboTipoCorreo As DropDownList = CType(fila.Cells(1).FindControl("gvdlTipoCorreo"), DropDownList)
                .IdTipoCorreo = CInt(IIf(cboTipoCorreo.SelectedValue = "", 0, cboTipoCorreo.SelectedValue))
                .objCorreo = getDropTipoCorreo()

                .Nombre = CType(fila.Cells(1).FindControl("gvtbCorreo"), TextBox).Text
            End With
            ListaCorreoPersona.Add(obj)
        Next
        Return ListaCorreoPersona
    End Function

    Private Sub gvCorreo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCorreo.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboCorreo As DropDownList = CType(e.Row.Cells(1).FindControl("gvdlTipoCorreo"), DropDownList)
                If ListaCorreoPersona(e.Row.RowIndex).IdTipoCorreo <> 0 Then
                    cboCorreo.SelectedValue = CStr(ListaCorreoPersona(e.Row.RowIndex).IdTipoCorreo)
                End If

            End If
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvCorreo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCorreo.SelectedIndexChanged
        Try
            ListaCorreoPersona = getListaCorreo()
            ListaCorreoPersona.RemoveAt(gvCorreo.SelectedIndex)
            gvCorreo.DataSource = ListaCorreoPersona
            gvCorreo.DataBind()
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "Seccion de Direccion"

    Function getDireccion() As String
        Dim cad$ = dlTipoVia.SelectedItem.Text.Trim + " " + tbVia.Text.Trim + _
        CStr(IIf(tbnroExterior.Text = "", "", " Nº " + tbnroExterior.Text.Trim + CStr(IIf(tbnroInterior.Text = "", "", "-" + tbnroInterior.Text.Trim)))) + _
        CStr(IIf(tbManzana.Text = "", "", ", Mz. " + tbManzana.Text.Trim)) + _
        CStr(IIf(tbLote.Text = "", "", ", Lt " + tbLote.Text)) + _
        CStr(IIf(tbEtapa.Text = "", "", ", " + tbEtapa.Text.Trim + " Et.")) + _
        CStr(IIf(tbzona.Text = "", "", CStr(IIf(dlZonificacion.SelectedValue = "0", "", ", " + dlZonificacion.SelectedItem.Text.Trim)) + " " + tbzona.Text.Trim))
        If dlDistrito.SelectedValue.Length > 0 And dlDistrito.SelectedValue <> "00" Then cad = cad + ", " + dlDistrito.SelectedItem.Text

        Return cad
    End Function

    Private Sub llenargrillaDireccion(ByVal gv As GridView)
        ListaDireccionPersona = getListaDireccion()
        Dim obj As New Entidades.Direccion

        With obj
            .TipoDireccion = CStr(dlTipoDireccion.SelectedItem.Text)
            .IdTipoDireccion = CInt(dlTipoDireccion.SelectedValue)
            .IdZonaTipo = CInt(dlZonificacion.SelectedValue)
            .IdViaTipo = CInt(dlTipoVia.SelectedValue)

            If dlDepartamento.SelectedValue <> "00" Then
                .Ubigeo = dlDepartamento.SelectedValue + dlProvincia.SelectedValue + _
                                    CStr(IIf(dlDistrito.Items.Count = 0, "00", dlDistrito.SelectedValue))
            Else
                .Ubigeo = "000000"
            End If

            .ViaNumero = tbnroExterior.Text.Trim
            .Etapa = tbEtapa.Text.Trim
            .Mz = tbManzana.Text.Trim
            .Interior = CStr(IIf(tbnroExterior.Text.Trim <> "", tbnroInterior.Text.Trim, ""))
            .Lt = tbLote.Text.Trim
            .Direccion = HttpUtility.HtmlDecode(CStr(IIf(tbDireccion.Text = "", getDireccion(), tbDireccion.Text)))
            .ZonaNombre = tbzona.Text.Trim
            .ViaNombre = tbVia.Text.Trim
            .Referencia = tbreferencia.Text.Trim
        End With
        ListaDireccionPersona.Add(obj)
        gv.DataSource = ListaDireccionPersona
        gv.DataBind()
    End Sub

    Function getListaDireccion() As List(Of Entidades.Direccion)
        ListaDireccionPersona = New List(Of Entidades.Direccion)
        For Each fila As GridViewRow In gvDireccion.Rows
            Dim obj As New Entidades.Direccion
            With obj
                .TipoDireccion = CStr(HttpUtility.HtmlDecode(fila.Cells(1).Text))
                .IdTipoDireccion = CInt(CType(fila.Cells(2).FindControl("hddIdTipoDireccion"), HiddenField).Value.Trim)
                .IdZonaTipo = CInt(CType(fila.Cells(2).FindControl("hddidzonatipo"), HiddenField).Value.Trim)
                .IdViaTipo = CInt(CType(fila.Cells(2).FindControl("hddIdViaTipo"), HiddenField).Value.Trim)
                .Ubigeo = CStr(CType(fila.Cells(2).FindControl("hddubigeo"), HiddenField).Value.Trim)
                .ViaNumero = CStr(HttpUtility.HtmlDecode(CType(fila.Cells(2).FindControl("hddvianumero"), HiddenField).Value.Trim))
                .Etapa = CStr(HttpUtility.HtmlDecode(CType(fila.Cells(2).FindControl("hddetapa"), HiddenField).Value.Trim))
                .Mz = CStr(HttpUtility.HtmlDecode(CType(fila.Cells(2).FindControl("hddmz"), HiddenField).Value.Trim))
                .Interior = CStr(HttpUtility.HtmlDecode(CType(fila.Cells(2).FindControl("hddInterior"), HiddenField).Value.Trim))
                .Lt = CStr(CType(fila.Cells(2).FindControl("hddlt"), HiddenField).Value.Trim)
                .Direccion = CStr(HttpUtility.HtmlDecode(CType(fila.Cells(2).FindControl("gvlbDireccion"), Label).Text.Trim))
                .ZonaNombre = CStr(HttpUtility.HtmlDecode(fila.Cells(3).Text))
                .ViaNombre = CStr(HttpUtility.HtmlDecode(fila.Cells(4).Text))
                .Referencia = CStr(HttpUtility.HtmlDecode(fila.Cells(5).Text))
            End With
            ListaDireccionPersona.Add(obj)
        Next
        Return ListaDireccionPersona
    End Function

    Protected Sub lbQuitar_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim quitar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow
            fila = CType(quitar.NamingContainer, GridViewRow)

            ListaDireccionPersona = getListaDireccion()
            ListaDireccionPersona.RemoveAt(fila.RowIndex)
            gvDireccion.DataSource = ListaDireccionPersona
            gvDireccion.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lbEditar_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim editar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow
            fila = CType(editar.NamingContainer, GridViewRow)
            ListaDireccionPersona = getListaDireccion()
            gvDireccion.DataSource = EditarDireccion(ListaDireccionPersona, fila.RowIndex)
            gvDireccion.DataBind()
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function EditarDireccion(ByVal Lista As List(Of Entidades.Direccion), ByVal index As Integer) As List(Of Entidades.Direccion)
        dlTipoDireccion.SelectedValue = CStr(Lista(index).IdTipoDireccion)
        dlZonificacion.SelectedValue = CStr(Lista(index).IdZonaTipo)
        dlTipoVia.SelectedValue = CStr(Lista(index).IdViaTipo)

        If Lista(index).Ubigeo.Length = 6 Then

            drop = New Combo

            dlDepartamento.SelectedValue = CStr(Lista(index).Ubigeo).Substring(0, 2)
            If CStr(Lista(index).Ubigeo).Substring(0, 2) <> "00" Then
                drop.LLenarCboProvincia(dlProvincia, dlDepartamento.SelectedValue)
                dlProvincia.SelectedValue = CStr(Lista(index).Ubigeo).Substring(2, 2)
            End If

            If CStr(Lista(index).Ubigeo).Substring(2, 2) <> "00" Then
                drop.LLenarCboDistrito(dlDistrito, dlDepartamento.SelectedValue, dlProvincia.SelectedValue)
                dlDistrito.SelectedValue = CStr(Lista(index).Ubigeo).Substring(4, 2)
            End If
        End If

        tbnroExterior.Text = Lista(index).ViaNumero
        tbEtapa.Text = Lista(index).Etapa
        tbManzana.Text = Lista(index).Mz
        tbnroInterior.Text = Lista(index).Interior
        tbLote.Text = Lista(index).Lt
        tbzona.Text = Lista(index).ZonaNombre
        tbVia.Text = Lista(index).ViaNombre
        tbreferencia.Text = Lista(index).Referencia
        tbDireccion.Text = Lista(index).Direccion
        ListaDireccionPersona.RemoveAt(index)
        Return ListaDireccionPersona
    End Function

    Private Sub dlDepartamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlDepartamento.SelectedIndexChanged
        Try
            drop = New Combo
            drop.LLenarCboProvincia(dlProvincia, dlDepartamento.SelectedValue)
            dlDistrito.Items.Clear()
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub dlProvincia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlProvincia.SelectedIndexChanged
        Try
            drop = New Combo
            drop.LLenarCboDistrito(dlDistrito, dlDepartamento.SelectedValue, dlProvincia.SelectedValue)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "Seccion de Tipo de Agente"

    Private List_PersonaTipoAgente As List(Of Entidades.PersonaTipoAgente)
    Private Obj_PersonaTipoAgente As Entidades.PersonaTipoAgente

    Private Sub gv_tipoAgente_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv_tipoAgente.SelectedIndexChanged
        List_PersonaTipoAgente = getList_PersonaTipoAgente()

        List_PersonaTipoAgente.RemoveAt(gv_tipoAgente.SelectedIndex)
        gv_tipoAgente.DataSource = List_PersonaTipoAgente
        gv_tipoAgente.DataBind()
    End Sub

    Private Sub btn_AddTipoAgente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_AddTipoAgente.Click
        Add_TipoAgente()
    End Sub
    Private Sub Add_TipoAgente()
        List_PersonaTipoAgente = getList_PersonaTipoAgente()

        Obj_PersonaTipoAgente = New Entidades.PersonaTipoAgente
        With Obj_PersonaTipoAgente
            .IdAgente = CInt(dlTipoAgente.SelectedValue)
            .Descripcion = HttpUtility.HtmlDecode(dlTipoAgente.SelectedItem.Text)
        End With

        List_PersonaTipoAgente.Add(Obj_PersonaTipoAgente)

        gv_tipoAgente.DataSource = List_PersonaTipoAgente
        gv_tipoAgente.DataBind()
    End Sub
    Private Function getList_PersonaTipoAgente() As List(Of Entidades.PersonaTipoAgente)
        List_PersonaTipoAgente = New List(Of Entidades.PersonaTipoAgente)

        For Each row As GridViewRow In Me.gv_tipoAgente.Rows
            Obj_PersonaTipoAgente = New Entidades.PersonaTipoAgente
            With Obj_PersonaTipoAgente
                .IdAgente = CInt(CType(row.FindControl("hdd_idTipoAgente"), HiddenField).Value)
                .Descripcion = HttpUtility.HtmlDecode(CType(row.FindControl("gv_lblAgente"), Label).Text)
            End With
            List_PersonaTipoAgente.Add(Obj_PersonaTipoAgente)
        Next

        Return List_PersonaTipoAgente
    End Function
#End Region

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Try
            hddoperativo.Value = CStr(operativo.GuardarNuevo)
            verBotones("N")
            AddRol()
            If dlDistrito.DataSource Is Nothing Then
                drop = New Combo
                dlDepartamento.SelectedValue = "15"
                drop.LLenarCboProvincia(dlProvincia, dlDepartamento.SelectedValue)
                dlProvincia.SelectedValue = "01"
                drop.LLenarCboDistrito(dlDistrito, dlDepartamento.SelectedValue, dlProvincia.SelectedValue)
                dlDistrito.SelectedValue = "01"
            End If
            Dim validacion As Integer = New Negocio.Persona().ValidancionPerfilNuevoxIdUsuario(CInt(Session("IdUsuario")))
            If (validacion = 0) Then
                gvRol.Enabled = False
            Else
                gvRol.Enabled = True
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Try
            Mantenimiento(CInt(hddoperativo.Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Try
            hddoperativo.Value = CStr(operativo.Ninguno)
            verBotones("L")
            limpiarFormulario()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(rbPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("rol", dlRol.SelectedValue)
            ViewState.Add("estado", rbbuscarEstado.SelectedValue)
            ViewState.Add("IdNacionalidad", CInt(ddlNacionalidadBuscar.SelectedValue))

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarPersona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarPersona.Click
        Try
            ViewState.Add("cboFiltro", cbbuscarpersona.SelectedValue)
            ViewState.Add("tbBuscar", tbbuscarPersona.Text.Trim)
            ViewState.Add("tipo", hddtipoBusqueda.Value.Substring(0, 1))
            Select Case CInt(ViewState("cboFiltro"))
                Case 0 '********* Nombre
                    BuscarPersona(gvBuscarPersona, "", "", CStr(ViewState("tbBuscar")), CInt(ViewState("tipo")), 0)
                Case 1 '********* Dni
                    BuscarPersona(gvBuscarPersona, CStr(ViewState("tbBuscar")), "", "", CInt(ViewState("tipo")), 0)
                Case 2 '********* Ruc
                    BuscarPersona(gvBuscarPersona, "", CStr(ViewState("tbBuscar")), "", CInt(ViewState("tipo")), 0)
            End Select
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

End Class