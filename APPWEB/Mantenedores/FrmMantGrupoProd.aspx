<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantGrupoProd.aspx.vb" Inherits="APPWEB.FrmMantGrupoProd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <table width="100%">
                    <tr valign="top">
                        <td class="TituloCelda">
                            GRUPO DE PRODUCTOS
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseout="this.src='/Imagenes/Nuevo_b.JPG';"
                                onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" />
                            <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                onmouseout="this.src='/Imagenes/Guardar_B.JPG';" onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                                OnClientClick="return(  valSave());" />
                            <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                                onmouseout="this.src='/Imagenes/Arriba_B.JPG';" onmouseover="this.src='/Imagenes/Arriba_A.JPG';"
                                ToolTip="Regresar a Administración Sistema" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlCtrl" runat="server">
                    <table>
                        <tr>
                            <td class="Label">
                                <asp:Label ID="lblId" Text="Codigo" runat="server"   ></asp:Label>
                            </td>
                            <td>
                            <asp:TextBox ID="txtCodigo" runat ="server" ReadOnly ="true" CssClass ="TextBox_ReadOnly"   ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Label">
                                <asp:Label Text="Nombre Grupo" ID="lblNombreGrupo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNombreGrupo" runat="server" Width="500px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Label">
                                <asp:Label ID="lblAbrev" runat="server" Text="Abrev"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNombreAbrev" runat="server" Width="500px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Label">
                                <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                            </td>
                            <td class="Label" align ="left" >
                                <asp:RadioButtonList ID="rblEstado" runat="server" RepeatDirection ="Horizontal" >
                                    <asp:ListItem Value="1" Text="Activo" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Inactivo"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlCtrlBus" runat="server">
                    <table>
                        <tr>
                            <td class="Label">
                                <asp:Label ID="lblNombreGrupoBusqueda" runat="server" Text="Nombre Grupo"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtnombreBusGrupo" runat="server" Width="500px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarProducto" runat="server" Text="Buscar" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:GridView ID="gvwGrupoProductos" Width="100%" runat="server" AllowPaging="True"
                                AutoGenerateColumns="False"  ForeColor="#333333">
                                <Columns>
                                    <asp:CommandField SelectText="Editar" ShowSelectButton="True" />
                                    <asp:BoundField DataField="id" HeaderText="Codigo" NullDisplayText="-----" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Descripcion" NullDisplayText="-----" />
                                    <asp:BoundField DataField="abv" HeaderText="Abrev" NullDisplayText="-----" />
                                    <asp:BoundField DataField="DescEstado" HeaderText="Estado" NullDisplayText="-----" />
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" CssClass="GrillaFooter" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" CssClass="GrillaPager" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" CssClass="GrillaSelectedRow" Font-Bold="True"
                                    ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" CssClass="GrillaHeader" Font-Bold="True" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" CssClass="GrillaEditRow" />
                                <AlternatingRowStyle BackColor="White" CssClass="GrillaRowAlternating" ForeColor="#284775" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
        function valSave() {

            var txtNombre = document.getElementById('<%=txtNombreGrupo.ClientID %>');
            if (txtNombre.value.length <= 0) {
                alert('Ingrese una Descripción');
                txtNombre.select();
                txtNombre.focus();
                return false;
            }
                
            var txtabv= document.getElementById('<%=txtNombreAbrev.ClientID %>');
            if (txtabv.value.length <= 0) {
                alert('Ingrese una abreviatura');
                txtabv.select();
                txtabv.focus();
                return false;
            }
            
            
        
        }
    
    </script>
</asp:Content>


