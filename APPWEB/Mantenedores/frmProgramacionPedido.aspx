<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmProgramacionPedido.aspx.vb" Inherits="APPWEB.frmProgramacionPedido" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table cellspacing="2" cellpadding="2">
        <tr>
            <td>
                <asp:Button ID="btNuevo" Width="80px" runat="server" Text="Nuevo" OnClientClick="return(validarNuevo());"
                    Style="cursor: hand;" />
                <asp:Button ID="btBuscar" Width="80px" runat="server" Text="Buscar" Style="cursor: hand;" />
                <asp:Button ID="btGuardar" Width="80px" runat="server" Text="Guardar" OnClientClick="return(validarGuardar());"
                    Style="cursor: hand;" />
                <asp:Button ID="btCancelar" OnClientClick="return(confirm('Desea ir al modo Inicio?'));"
                    Width="80px" runat="server" Text="Cancelar" Style="cursor: hand;" />
                <asp:Button ID="btEditar" Width="80px" runat="server" Text="Editar" OnClientClick="return(validarNuevo());"
                    Style="cursor: hand;" />
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Programaci&oacute;n de Semana
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td align="center">
                <table width="100%">
                    <tr>
                        <td>
                            <asp:Panel ID="pnlPrincipal" runat="server">
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold">
                                            A�o:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlyear" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            &nbsp; Semana:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlweek" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="Texto" style="font-weight: bold">
                                        Estado:
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rbestado" class="Texto" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                            <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlSecundario" runat="server">
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold">
                                            Mes:
                                        </td>
                                        <td colspan="3">
                                            <asp:DropDownList ID="dlmonth" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </td>                                      
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold">
                                            &nbsp; Fecha de Inicio:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbfechaini" onFocus="return(validarSeleccionarFecha());" CssClass="TextBox_Fecha"
                                                onblur="return(  valFecha_Blank(this) );" runat="server"></asp:TextBox>
                                            <ajax:MaskedEditExtender ID="MaskedEditExtender_tbfechaini" runat="server" ClearMaskOnLostFocus="false"
                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="tbfechaini">
                                            </ajax:MaskedEditExtender>
                                            <ajax:CalendarExtender ID="CalendarINI" Format="d" runat="server" TargetControlID="tbfechaini">
                                            </ajax:CalendarExtender>
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            &nbsp; Fecha de Fin:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbfechafin" onFocus="return(validarSeleccionarFecha());" CssClass="TextBox_Fecha"
                                                onblur="return(  valFecha_Blank(this) );" runat="server"></asp:TextBox>
                                            <ajax:MaskedEditExtender ID="MaskedEditExtender_tbfechafin" runat="server" ClearMaskOnLostFocus="false"
                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="tbfechafin">
                                            </ajax:MaskedEditExtender>
                                            <ajax:CalendarExtender ID="CalendarFin" Format="d" runat="server" TargetControlID="tbfechafin">
                                            </ajax:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold">
                                            Dias Util:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbutilday" runat="server" onKeypress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hdd_operativo" Value="0" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        function validarGuardar() {
            var mes = document.getElementById('<%=dlmonth.ClientID %>');
            var fechaini = document.getElementById('<%=tbfechaini.ClientID %>');
            var fechafin = document.getElementById('<%=tbfechafin.ClientID %>');
            var dias = document.getElementById('<%=tbutilday.ClientID %>');
            var radio = document.getElementById('<%=rbestado.ClientID %>');
            var operativo = document.getElementById('<%=hdd_operativo.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[0].checked == false) {
                alert('El estado no puede ser inactivo para la generacion de fechas');
                return false;
            }

            if (dias.value == '' || dias.value == '' || isNaN(parseInt(dias.value))) {
                alert('Los dias utiles estan en un rango de 0 - 7');
                dias.focus();
                dias.select();
                return false;
            }

            if (mes.value == '0') {
                alert('No ha seleccionado el mes');
                mes.focus();
                return false;
            }

            if (operativo.value == '2') {//actualizar
                if (compararFechas(fechaini, fechafin)) {
                    alert('La fecha inicio no puede ser mayor a la fecha fin');
                    return false;
                }
                if (fechaini == fechafin) {
                    alert('La fecha inicio no puede ser igual a la fecha fin');
                    return false;
                }
            }

            if (parseInt(dias.value) > 7) {
                alert('Los dias utiles estan en un rango de 0 - 7');
                dias.focus();
                dias.select();
                return false;
            }

            return confirm("Esta seguro de continuar");
        }
        //++++++++++++
        function validarSeleccionarFecha() {
            var mes = document.getElementById('<%=dlmonth.ClientID %>');
            if (mes.value == '0') {
                alert('No ha seleccionado el mes');
                mes.focus();
                return false;
            }
        }

        //++++++++++++++
        function compararFechas(fecha, fecha2) {
            var xMonth = fecha.value.substring(3, 5);
            var xDay = fecha.value.substring(0, 2);
            var xYear = fecha.value.substring(6, 10);
            var yMonth = fecha2.value.substring(3, 5);
            var yDay = fecha2.value.substring(0, 2);
            var yYear = fecha2.value.substring(6, 10);

            if (xYear > yYear) {
                return (true)
            }
            else {
                if (xYear == yYear) {
                    if (xMonth > yMonth) {
                        return (true)
                    }
                    else {
                        if (xMonth == yMonth) {
                            if (xDay > yDay)
                                return (true);
                            else
                                return (false);
                        }
                        else
                            return (false);
                    }
                }
                else
                    return (false);
            }
        }
        //++++++++++++++++++++++++++++++
        function validarNuevo() {
            var year = document.getElementById('<%=dlyear.ClientID %>');
            var week = document.getElementById('<%=dlweek.ClientID %>');
            if (year.value == '0') {
                alert('NO HA SELECCIONADO EL A�O.');
                year.focus();
                return false;
            }
            if (week.value == '0') {
                alert('NO HA SELECCIONADO EL NUMERO DE SEMANA.');
                week.focus();
                return false;
            }
            if (week.value == '52') {
                alert('EL A�O YA HA SIDO GENERADO.');
                week.focus();
                return false;
            }
            if (week.value == '') {
                return false;
            }

            return true;
        }
        
    </script>

</asp:Content>
