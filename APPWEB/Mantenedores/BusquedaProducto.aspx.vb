﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class BusquedaProducto
    Inherits System.Web.UI.Page


    Private listaCatalogo As List(Of Entidades.Catalogo)
    Private objScript As New ScriptManagerClass


    Private Function getListaCatalogo() As List(Of Entidades.Catalogo)
        Return CType(Session.Item("listaCatalogox"), List(Of Entidades.Catalogo))
    End Function
    Private Sub setListaCatalogo(ByVal lista As List(Of Entidades.Catalogo))
        Session.Remove("listaCatalogox")
        Session.Add("listaCatalogox", lista)
    End Sub

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        onLoad_Producto()
    End Sub

    Private Sub onLoad_Producto()

        If Not IsPostBack Then

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)

                .llenarCboAlmacenxIdTienda(Me.cboAlmacenReferencia, CInt(Me.cboTienda.SelectedValue), False)

                .llenarCboTipoExistencia(cboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)
                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

                .LlenarCboTipoPV(Me.cboTipoPrecioV, False)
                .LlenarCboTipoAlmacen(Me.ddlTipoAlmacen, False)
            End With

            Me.hddIdTipoPVDefault.Value = CStr((New Negocio.TipoPrecioV).SelectIdTipoPrecioVDefault())

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            ConfigurarDatos()

        End If

    End Sub

#Region "************************** BUSQUEDA PRODUCTO"

    Private Sub btnBuscarGrilla_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarGrilla_AddProducto.Click
        BuscarProductoCatalogo(0)
        Me.txtCodBarrasCapa.Text = ""
    End Sub
    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try
            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)
            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                        If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                            IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                        End If
                    End If

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacenReferencia.SelectedValue)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("IdTienda_BuscarProducto", Me.cboTienda.SelectedValue)
                    ViewState.Add("IdMoneda_BuscarProducto", 1)
                    ViewState.Add("IdTipoPV_BuscarProducto", IdTipoPV)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("CodigoBarras", Me.txtCodBarrasCapa.Text)
                    ViewState.Add("IdTipoExistencia_BuscarProducto", Me.cboTipoExistencia.SelectedValue)

                Case 1
                    '******************* Cuando se cambia el almacen de busqueda
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacenReferencia.SelectedValue)

                Case 2
                    '******************** cuando se cambia la tienda de precios
                    ViewState.Add("IdTienda_BuscarProducto", Me.cboTienda.SelectedValue)

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CStr(ViewState("CodigoBarras")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, ByVal nomProducto As String, ByVal IdAlmacen As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer, ByVal IdMoneda As Integer, ByVal tipoMov As Integer, ByVal IdEmpresa As Integer, ByVal codbarras As String, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        '************ Esto ha sido comentado a pedido de cristhian panta 
        Dim IdMedioPago As Integer = 1

        Dim IdCliente As Integer = 0
        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

        Dim filtroProductoCampania As Boolean = Me.chb_FiltroProductoCampania.Checked

        'Me.listaCatalogo = (New Negocio.Catalogo).CatalogoProducto_PV_Paginado_Cotizacion(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, grilla.PageSize, index, CInt(Session("IdUsuario")), CInt(Me.cboCondicionPago.SelectedValue), IdMedioPago, IdCliente, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaCatalogo = (New Negocio.Catalogo).CatalogoProducto_PV_Paginado_Cotizacion_V2ParCodBarras(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdTienda, IdMoneda, IdEmpresa, IdAlmacen, IdTipoPV, grilla.PageSize, index, CInt(Session("IdUsuario")), 1, IdMedioPago, IdCliente, tableTipoTabla, codigoProducto, codbarras, filtroProductoCampania, IdTipoExistencia)

        If Me.listaCatalogo.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('No se hallaron registros.');        ", True)
            Return

        Else
            grilla.DataSource = Me.listaCatalogo
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)
            setListaCatalogo(Me.listaCatalogo)
        End If

        Me.GV_ComponenteKit_Find.DataBind()

    End Sub


    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CStr(ViewState("CodigoBarras")), CInt(ViewState("IdExistencia_BuscarProducto")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CStr(ViewState("CodigoBarras")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(ViewState("IdTienda_BuscarProducto")), CInt(ViewState("IdTipoPV_BuscarProducto")), CInt(ViewState("IdMoneda_BuscarProducto")), 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CStr(ViewState("CodigoBarras")), CInt(ViewState("IdTipoExistencia_BuscarProducto")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub DGV_AddProd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_AddProd.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                'Se cambia el icono del boton
                Dim lb As ImageButton = DirectCast(e.Row.FindControl("btnConsultarStockAlmacenes_BuscarProd"), ImageButton)
                lb.Attributes.Add("onClientClick", "this.src='../Imagenes/loader.gif'")

                Dim gvrow As GridViewRow = CType(e.Row.Cells(3).NamingContainer, GridViewRow)
                Dim cboUM As DropDownList = CType(gvrow.FindControl("cmbUnidadMedida_AddProd"), DropDownList)
                Me.listaCatalogo(e.Row.RowIndex).ListaUM_Venta = (New Negocio.ProductoTipoPV).SelectUM_Venta(Me.listaCatalogo(e.Row.RowIndex).IdProducto, CInt(Me.cboTienda.SelectedValue), Me.listaCatalogo(e.Row.RowIndex).IdTipoPV)
                cboUM.DataSource = Me.listaCatalogo(e.Row.RowIndex).ListaUM_Venta
                cboUM.DataBind()

                If (cboUM.Items.FindByValue(CStr(Me.listaCatalogo(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then
                    cboUM.SelectedValue = Me.listaCatalogo(e.Row.RowIndex).IdUnidadMedida.ToString
                End If


                setListaCatalogo(Me.listaCatalogo)

                If (Me.listaCatalogo(e.Row.RowIndex).Kit) Then  '********* KIT

                    CType(e.Row.FindControl("btnMostrarComponenteKit_Find"), ImageButton).Visible = True

                Else

                    CType(e.Row.FindControl("btnMostrarComponenteKit_Find"), ImageButton).Visible = False

                End If

                Dim btnVerCampania As ImageButton = CType(e.Row.FindControl("btnViewCampania"), ImageButton)
                If (Me.listaCatalogo(e.Row.RowIndex).ExisteCampania_Producto) Then
                    btnVerCampania.Visible = True
                Else
                    btnVerCampania.Visible = False
                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub cmbUnidadMedidaVenta_Catalogo_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try

            ActualizarListaCatalogo()

            Me.listaCatalogo = Me.getListaCatalogo

            Dim IdTipoPV As Integer = CInt(Me.hddIdTipoPVDefault.Value)

            If IsNumeric(cboTipoPrecioV.SelectedValue) And cboTipoPrecioV.SelectedValue.Trim.Length > 0 Then
                If CInt(cboTipoPrecioV.SelectedValue) > 0 Then
                    IdTipoPV = CInt(cboTipoPrecioV.SelectedValue)
                End If
            End If

            Dim cboUM As DropDownList = CType(sender, DropDownList)
            Dim i As Integer = CType(cboUM.NamingContainer, GridViewRow).RowIndex

            Dim objCatalogo As Entidades.Catalogo = (New Negocio.ProductoTipoPV).SelectValor_Equivalencia_VentaxParams_Cotizacion(Me.listaCatalogo(i).IdUnidadMedida, Me.listaCatalogo(i).IdProducto, _
             IdTipoPV, Me.listaCatalogo(i).IdTienda, CInt(1), CInt(Me.cboAlmacenReferencia.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), CDate(Me.txtFechaEmision.Text), _
             )

            If objCatalogo IsNot Nothing Then

                Me.listaCatalogo(i).PrecioSD = objCatalogo.PrecioSD
                Me.listaCatalogo(i).PrecioLista = objCatalogo.PrecioLista
                Me.listaCatalogo(i).StockDisponibleN = objCatalogo.StockDisponibleN
                Me.listaCatalogo(i).pvComercial = objCatalogo.pvComercial



            End If



            DGV_AddProd.DataSource = Me.listaCatalogo
            DGV_AddProd.DataBind()


            setListaCatalogo(Me.listaCatalogo)


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub ActualizarListaCatalogo()

        Me.listaCatalogo = getListaCatalogo()

        For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

            Me.listaCatalogo(i).IdUnidadMedida = CInt(CType(DGV_AddProd.Rows(i).Cells(3).FindControl("cmbUnidadMedida_AddProd"), DropDownList).SelectedValue)

        Next

        setListaCatalogo(Me.listaCatalogo)

    End Sub

    Protected Sub mostrarCapaStockPrecioxProducto(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProductoConsultarStockPrecioxProd.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)
            Dim IdTipoPv As Integer = CInt(Me.hddIdTipoPVDefault.Value)

            If Me.ddlTipoAlmacen.Items.Count > 0 Then Me.ddlTipoAlmacen.SelectedIndex = 0
            Dim IdTipoAlmacen As Integer = 0

            If (IsNumeric(Me.cboTipoPrecioV.SelectedValue) And Me.cboTipoPrecioV.SelectedValue.Trim.Length > 0) Then
                If (CInt(Me.cboTipoPrecioV.SelectedValue) > 0) Then
                    IdTipoPv = CInt(Me.cboTipoPrecioV.SelectedValue)
                End If
            End If


            If IsNumeric(Me.ddlTipoAlmacen.SelectedValue) Then IdTipoAlmacen = CInt(Me.ddlTipoAlmacen.SelectedValue)

            hddIdProductoStockPrecio.Value = CStr(IdProducto)


            Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, IdTipoPv, CInt(1), (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue), IdTipoAlmacen)
            Me.GV_ConsultarStockPrecioxProd.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaConsultarStockPrecioxProducto');  CalcularConsultarStockPrecioxProd(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub ddlTipoAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoAlmacen.SelectedIndexChanged
        onChange_TipoAlmacen()
    End Sub
    Private Sub onChange_TipoAlmacen()
        Dim IdProducto As Integer = CInt(hddIdProductoStockPrecio.Value)
        Dim IdTipoPv As Integer = CInt(Me.hddIdTipoPVDefault.Value)
        Dim IdTipoAlmacen As Integer = 0
        If (IsNumeric(Me.cboTipoPrecioV.SelectedValue) And Me.cboTipoPrecioV.SelectedValue.Trim.Length > 0) Then
            If (CInt(Me.cboTipoPrecioV.SelectedValue) > 0) Then
                IdTipoPv = CInt(Me.cboTipoPrecioV.SelectedValue)
            End If
        End If

        If IsNumeric(Me.ddlTipoAlmacen.SelectedValue) Then IdTipoAlmacen = CInt(Me.ddlTipoAlmacen.SelectedValue)

        Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, IdTipoPv, CInt(1), (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue), IdTipoAlmacen)
        Me.GV_ConsultarStockPrecioxProd.DataBind()

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaConsultarStockPrecioxProducto'); CalcularConsultarStockPrecioxProd(); ", True)

    End Sub

    Protected Sub mostrarCapaConsultarTipoPrecio(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProducto_ConsultarTipoPrecio.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Me.lblTienda_ConsultarTipoPrecio.Text = CStr(Me.cboTienda.SelectedItem.ToString)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)
            Dim IdTienda As Integer = CInt(Me.cboTienda.SelectedValue)

            Me.GV_ConsultarTipoPrecio.DataSource = (New Negocio.DocumentoCotizacion).DocumentoCotizacion_ConsultarTipoPrecioxParams(IdTienda, IdProducto, CInt(1), CDate(Me.txtFechaEmision.Text))
            Me.GV_ConsultarTipoPrecio.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaConsultarTipoPrecio'); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnMostrarComponenteKit_Find_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim index As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex

            Me.listaCatalogo = getListaCatalogo()

            '****************** MOSTRAMOS LOS COMPONENTES DEL KIT
            Me.GV_ComponenteKit_Find.DataSource = (New Negocio.Kit).SelectComponentexIdKit(Me.listaCatalogo(index).IdProducto)
            Me.GV_ComponenteKit_Find.DataBind()



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


#End Region

#Region "Filtro de busquedad avanza de productos"

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0

            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1
            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)
        Next

        Return dt
    End Function

    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function

#End Region

#Region "******************************* PRINCIPALES COMBOS"


    Private Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub cmbSubLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region



#Region "*********************************** CAMPAÑA"

    Protected Sub valOnClick_btnViewCampania(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        visualizarCampania_Producto_Find(CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex)

    End Sub
    Private Sub visualizarCampania_Producto_Find(ByVal index As Integer)
        Try

            Me.listaCatalogo = getListaCatalogo()

            Dim IdProducto As Integer = Me.listaCatalogo(index).IdProducto

            Me.lblProducto_capaCampania_Producto.Text = Me.listaCatalogo(index).Descripcion
            Me.lblCodigoProducto_capaCampania_Producto.Text = Me.listaCatalogo(index).CodigoProducto

            Me.GV_Campania_Producto.DataSource = (New Negocio.Campania_Detalle).Campania_Detalle_Vigente_SelectxParams_DT(Nothing, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdProducto, Nothing, CDate(Me.txtFechaEmision.Text))
            Me.GV_Campania_Producto.DataBind()

            Me.hddIndex_Campania_Producto.Value = CStr(index)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapa('capaCampania_Producto');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub btnVerCampania_Consulta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerCampania_Consulta.Click
        valOnClick_btnVerCampania_Consulta()
    End Sub
    Private Sub valOnClick_btnVerCampania_Consulta()
        Try

            Dim fechaActual As Date = (New Negocio.FechaActual).SelectFechaActual

            Me.GV_Campania_Cab.DataSource = (New Negocio.Campania).Campania_SelectxParams(Nothing, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), Nothing, fechaActual, fechaActual, True)
            Me.GV_Campania_Cab.DataBind()

            Me.GV_Campania_Det.DataSource = Nothing
            Me.GV_Campania_Det.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " onCapa('capaConsultar_Campania');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#End Region



    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Dim objCbo As New Combo

        With objCbo
            .llenarCboAlmacenxIdTienda(Me.cboAlmacenReferencia, CInt(Me.cboTienda.SelectedValue), False)
        End With
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Dim objCbo As New Combo

        With objCbo
            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            .llenarCboAlmacenxIdTienda(Me.cboAlmacenReferencia, CInt(Me.cboTienda.SelectedValue), False)
        End With
    End Sub








End Class