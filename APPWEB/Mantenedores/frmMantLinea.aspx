<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmMantLinea.aspx.vb" Inherits="APPWEB.frmMantLinea" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseout="this.src='/Imagenes/Nuevo_b.JPG';"
                    onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" />
                <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                    onmouseout="this.src='/Imagenes/Guardar_B.JPG';" onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                    OnClientClick="return(  valSave());" />
                <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                    onmouseout="this.src='/Imagenes/Arriba_B.JPG';" onmouseover="this.src='/Imagenes/Arriba_A.JPG';"
                    ToolTip="Regresar a Administraci�n Sistema" />
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                L&iacute;nea
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto">
                            Tipo de Existencia:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_tipoExistencia" runat="server" Width="400px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_LineaEdicion" runat="server" Width="100%">
                    <table cellpadding="0" cellspacing="4">
                        <tr>
                            <td class="Texto">
                                C&oacute;digo:
                            </td>
                            <td>
                                <asp:TextBox ID="txtCodigoLinea" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                    Width="80px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td colspan="3">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Longitud:
                            </td>
                            <td>
                                <asp:TextBox ID="txtLongitud" runat="server" MaxLength="12" Width="100px" onKeyup="return(valLongitudColor(this));"
                                    OnKeypress="return (onKeyPressEsNumero('event'));"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                C&oacute;d. Linea:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtCodAntiguo" runat="server" MaxLength="30" Width="200px" onKeyPress="return (valCodLinea(this));"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Nombre:
                            </td>
                            <td>
                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                    ID="txtnomdescripcion" runat="server" MaxLength="50" Width="331px"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Cta. Debe Compra:
                            </td>
                            <td>
                                <asp:TextBox ID="txtCtaDebeCompra0" runat="server" MaxLength="12" Width="100px"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Cta. Haber Compra:
                            </td>
                            <td>
                                <asp:TextBox ID="txtCtaHaberCompra0" runat="server" MaxLength="12" Width="100px"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Cta. Debe Venta:
                            </td>
                            <td>
                                <asp:TextBox ID="txtCtaDebeVenta" runat="server" MaxLength="12" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Cta. Haber Venta:
                            </td>
                            <td>
                                <asp:TextBox ID="txtCtaHaberVenta0" runat="server" MaxLength="12" Width="100px"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Costo de Compra:
                            </td>
                            <td>
                                <asp:TextBox ID="txtCostoCompra0" runat="server" MaxLength="12" Width="100px"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Costo de Venta:
                            </td>
                            <td>
                                <asp:TextBox ID="txtCostoVenta0" runat="server" MaxLength="12" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rdbEstado" runat="server" CssClass="Label" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="true" Value="1">Activo</asp:ListItem>
                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" valign="top">
                                &Aacute;rea:
                            </td>
                            <td>
                                <asp:GridView ID="gvarea" runat="server" GridLines="None" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                        <asp:TemplateField HeaderText="�rea">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="dlarea" runat="server" DataValueField="Id" DataTextField="DescripcionCorta"
                                                    DataSource='<%# DataBinder.Eval(Container.DataItem,"objArea") %>' Width="220px">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Estado">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ckestado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"LAEstado") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="GrillaRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaCabecera" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                            <td valign="top">
                                <asp:Button ID="btaddarea" runat="server" Text="Agregar" OnClientClick="return (onClick_btaddarea());" />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Busqueda" runat="server" Width="100%">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label25" runat="server" CssClass="Label" Text="Estado:"></asp:Label>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rdbEstado_Busqueda" runat="server" CssClass="Label" RepeatDirection="Horizontal"
                                    AutoPostBack="True">
                                    <asp:ListItem Value="">Todos</asp:ListItem>
                                    <asp:ListItem Selected="true" Value="1">Activo</asp:ListItem>
                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="DGV_LineaBusqueda" Width="100%" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False" CellPadding="2" EmptyDataText="No existe informaci�n."
                    ForeColor="#333333" GridLines="None">
                    <RowStyle BackColor="#F7F6F3" CssClass="GrillaRow" ForeColor="#333333" />
                    <Columns>
                        <asp:CommandField SelectText="Editar" ShowSelectButton="True" />
                        <asp:BoundField DataField="Id" HeaderText="C�digo" NullDisplayText="0">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Nombre" NullDisplayText="-----" />
                        <asp:BoundField DataField="DescEstado" HeaderText="Estado" NullDisplayText="-----" />
                        <asp:BoundField DataField="TipoExist" HeaderText="Existencia" NullDisplayText="-----" />
                        <asp:BoundField DataField="CtaDebeCompra" HeaderText="Debe_Compra" NullDisplayText="-----" />
                        <asp:BoundField DataField="CtaHaberCompra" HeaderText="Haber_Compra" NullDisplayText="-----" />
                        <asp:BoundField DataField="CostoCompra" HeaderText="Costo_Compra" NullDisplayText="-----" />
                        <asp:BoundField DataField="CtaDebeVenta" HeaderText="Debe_Venta" NullDisplayText="-----" />
                        <asp:BoundField DataField="CtaHaberVenta" HeaderText="Haber_Venta" NullDisplayText="-----" />
                        <asp:BoundField DataField="CostoVenta" HeaderText="Costo_Venta" NullDisplayText="-----" />
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" CssClass="GrillaFooter" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" CssClass="GrillaPager" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#E2DED6" CssClass="GrillaSelectedRow" Font-Bold="True"
                        ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" CssClass="GrillaHeader" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#999999" CssClass="GrillaEditRow" />
                    <AlternatingRowStyle BackColor="White" CssClass="GrillaRowAlternating" ForeColor="#284775" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddValMsjLinea" runat="server" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">
        function valSave() {
            //************ validamos que tenga un Tipo de existencia
            var cboTipoE = document.getElementById('<%=ddl_tipoexistencia.ClientID %>');
            if (isNaN(cboTipoE.value) || (cboTipoE.value.length == 0) || parseInt(cboTipoE.value) <= 0) {
                alert('Seleccione un tipo de Existencia');
                return false;
            }

            //************ Validamos un Nombre
            var txtNombreLinea = document.getElementById('<%=txtnomdescripcion.ClientID %>');
            if (txtNombreLinea.value.length <= 0) {
                alert('Ingrese una Descripci�n');
                txtNombreLinea.select();
                txtNombreLinea.focus();
                return false;
            }
            var txtCodLinea = document.getElementById('<%=txtCodAntiguo.ClientID %>');
            if (txtCodLinea.value.length <= 0) {
                alert('Ingrese c�d. linea.');
                txtCodLinea.select();
                txtCodLinea.focus();
                return false;
            }

            var grilla = document.getElementById('<%=gvarea.ClientID %>');
            if (grilla != null) {

                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var cont = 0;
                    for (var r = 1; r < grilla.rows.length; r++) {
                        var row = grilla.rows[r];
                        if (rowElem.cells[1].children[0].value == row.cells[1].children[0].value) {
                            cont = cont + 1;
                        }
                        if (cont > 1) {
                            alert("Ha seleccionado 2 veces la misma area");
                            return false;
                        }
                    }
                }

            }

            return (confirm('Desea continuar con la operaci�n ?'));
        }

        function valCodLinea(caja) {
            var cajaLongitud = document.getElementById('<%=txtLongitud.ClientID %>');
            if (cajaLongitud.value.length > 0) {
                if (parseInt(cajaLongitud.value) <= caja.value.length) {
                    return false;
                }
                return true;
            }
            return false;
        }

        function valLongitudColor(caja) {
            var cajaLongitud = document.getElementById('<%=txtCodAntiguo.ClientID %>');
            if (caja.value.length > 0) {
                cajaLongitud.style.backgroundColor = 'white';
            }
            else {
                cajaLongitud.style.backgroundColor = '#E2E2E2';
                cajaLongitud.value = '';
            }

            return true;
        }
        //
        function onClick_btaddarea() {
            var gvarea = document.getElementById('<%=gvarea.ClientID %>');

            if (gvarea != null) {
                for (var i = 1; i < gvarea.rows.length; i++) {
                    var rowElem = gvarea.rows[i];
                    if (rowElem.cells[1].children[0].options.length == gvarea.rows.length) {
                        alert('No se puede agregar mas �reas de las que estan registradas.');
                        return false;
                    }
                }
            }
        }
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
