﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



Partial Public Class frmMantLinea
    Inherits System.Web.UI.Page


#Region "variables"
    Dim listaLineaConsulta As List(Of Entidades.Linea)
    Private Lista_Linea_Area As List(Of Entidades.Linea_Area)
    Private Obj_Linea_Area As Entidades.Linea_Area
    Private Lista_Area As List(Of Entidades.Area)
    Private Obj_Area As Entidades.Area
    Private objScript As New ScriptManagerClass
    Private cbo As New Combo
    Private Enum operativo
        Ninguno = 0
        InsertUpdate = 1
    End Enum
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            ValidarPermisos()
            ConfigurarDatos()
            frm_State(operativo.Ninguno)
            frm_OnLoad()
        End If
    End Sub
    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / FECHA VCTO / NRO DIAS VIGENCIA / REGISTRAR CLIENTE / EDITAR CLIENTE
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {132, 133})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* BUSCAR
            'Me.cmbTipoExistencia0.Enabled = True
            rdbEstado_Busqueda.Enabled = True
        Else
            'Me.cmbTipoExistencia0.Enabled = False
            rdbEstado_Busqueda.Enabled = False
        End If

    End Sub
    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Private Sub frm_State(ByVal modo As Integer)
        limpiarControles()

        Select Case modo
            Case operativo.Ninguno
                ddl_tipoExistencia.Enabled = True
                ddl_tipoExistencia.AutoPostBack = True
                Panel_LineaEdicion.Visible = False
                Panel_Busqueda.Visible = True
                btnNuevo.Visible = True
                btnGuardar.Visible = False
                btnCancelar.Visible = False

            Case operativo.InsertUpdate
                ddl_tipoExistencia.AutoPostBack = False
                btnNuevo.Visible = False
                btnGuardar.Visible = True
                btnCancelar.Visible = True
                Panel_LineaEdicion.Visible = True
                Panel_Busqueda.Visible = False

        End Select
    End Sub
    Private Sub frm_OnLoad()
        Try
            cbo.llenarCboTipoExistencia(Me.ddl_tipoExistencia, True)
            getLineaByParameters()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub getLineaByParameters()
        Try
            Me.listaLineaConsulta = (New Negocio.Linea).SelectxIdLineaxEstado(0, rdbEstado_Busqueda.SelectedValue, _
                                    CInt(ddl_tipoExistencia.SelectedValue))
            setlistaLinea(listaLineaConsulta)
            DGV_LineaBusqueda.DataSource = listaLineaConsulta
            DGV_LineaBusqueda.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub DGV_LineaBusqueda_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_LineaBusqueda.PageIndexChanging
        DGV_LineaBusqueda.PageIndex = e.NewPageIndex
        DGV_LineaBusqueda.DataSource = getlistaLinea()
        DGV_LineaBusqueda.DataBind()
    End Sub
    Protected Sub DGV_LineaBusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_LineaBusqueda.SelectedIndexChanged
        cargarDatosLineaEdicion(CInt(DGV_LineaBusqueda.SelectedRow.Cells(1).Text))
    End Sub

    Private Sub cargarDatosLineaEdicion(ByVal idLinea As Integer)
        Try
            frm_State(operativo.InsertUpdate)
            ddl_tipoExistencia.Enabled = False

            Dim objLinea As List(Of Entidades.Linea) = (New Negocio.Linea).SelectxIdLineaxEstado(idLinea, "", 0)
            If objLinea.Count > 0 Then
                ddl_tipoExistencia.SelectedValue = CStr(objLinea.Item(0).IdTipoExistencia)
                txtCodigoLinea.Text = CStr(objLinea.Item(0).Id)
                txtnomdescripcion.Text = objLinea.Item(0).Descripcion
                txtCtaDebeCompra0.Text = objLinea.Item(0).CtaDebeCompra
                txtCtaHaberCompra0.Text = objLinea.Item(0).CtaHaberCompra
                txtCostoCompra0.Text = objLinea.Item(0).CostoCompra
                txtCtaDebeVenta.Text = objLinea.Item(0).CtaDebeVenta
                txtCtaHaberVenta0.Text = objLinea.Item(0).CtaHaberVenta
                txtCostoVenta0.Text = objLinea.Item(0).CostoVenta
                Me.txtCodAntiguo.Text = objLinea.Item(0).CodigoAntiguo
            End If
            'CARGAR LINEA AREA
            Lista_Linea_Area = cargarLinea_Area(idLinea)
            gvarea.DataSource = Lista_Linea_Area
            gvarea.DataBind()
            Try
                Dim objNLinea As New Negocio.Linea
                Dim objLineaLong As Entidades.Linea = objNLinea.ValidadLongitud
                Me.txtLongitud.Text = CStr(objLineaLong.Longitud)
                'If Me.txtLongitud.Text = "" Or Me.txtLongitud.Text = "0" Then
                If objLineaLong.Cantidad <= 1 Then
                    txtLongitud.Enabled = True
                ElseIf objLineaLong.Cantidad >= 2 Then
                    txtLongitud.Enabled = False
                End If
            Catch ex As Exception
                txtLongitud.Enabled = True
            Finally

            End Try
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#Region "funciones"

    Private Sub setlistaLinea(ByVal lista As List(Of Entidades.Linea))
        Session.Remove("lineaConsulta")
        Session.Add("lineaConsulta", lista)
    End Sub
    Private Function getlistaLinea() As List(Of Entidades.Linea)
        Return CType(Session.Item("lineaConsulta"), List(Of Entidades.Linea))
    End Function

#End Region

#Region "VISTAS FORMULARIOS"

    Private Sub limpiarControles()
        txtCodigoLinea.Text = ""
        txtnomdescripcion.Text = ""
        txtCostoCompra0.Text = ""
        txtCostoVenta0.Text = ""
        txtCtaDebeCompra0.Text = ""
        txtCtaHaberCompra0.Text = ""
        txtCtaDebeVenta.Text = ""
        txtCtaHaberVenta0.Text = ""
        txtCodAntiguo.Text = ""
        gvarea.DataBind()
    End Sub

#End Region

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click

        frm_State(operativo.InsertUpdate)

        Try
            Dim objNLinea As New Negocio.Linea
            Dim objLineaLong As Entidades.Linea = objNLinea.ValidadLongitud
            Me.txtLongitud.Text = CStr(objLineaLong.Longitud)
            If objLineaLong.Cantidad <= 1 Then
                'If Me.txtLongitud.Text = "" Or Me.txtLongitud.Text = "0" Then
                txtLongitud.Enabled = True
            ElseIf objLineaLong.Cantidad >= 2 Then
                txtLongitud.Enabled = False
            End If
            Me.txtCodAntiguo.Text = ""
        Catch ex As Exception
            txtLongitud.Enabled = True
            Me.txtCodAntiguo.Text = ""
        End Try

    End Sub
    Private Function getLinea() As Entidades.Linea
        Dim objLinea As New Entidades.Linea
        With objLinea
            .CostoCompra = txtCostoCompra0.Text.Trim
            .CostoVenta = txtCostoVenta0.Text.Trim
            .CtaDebeCompra = txtCtaDebeCompra0.Text.Trim
            .CtaDebeVenta = txtCtaDebeVenta.Text.Trim
            .CtaHaberCompra = txtCtaHaberCompra0.Text.Trim
            .CtaHaberVenta = txtCtaHaberVenta0.Text.Trim
            .Estado = rdbEstado.SelectedValue
            .Id = CInt(IIf(IsNumeric(txtCodigoLinea.Text) = True, txtCodigoLinea.Text, 0))
            .IdTipoExistencia = CInt(ddl_tipoExistencia.SelectedValue)
            .Descripcion = txtnomdescripcion.Text.Trim
            If IsNumeric(txtCodigoLinea.Text) Then .Id = CInt(txtCodigoLinea.Text)
            .CodigoAntiguo = Me.txtCodAntiguo.Text.Trim
            .Longitud = CInt(txtLongitud.Text)
        End With
        Return objLinea
    End Function
    Private Sub registrarLinea()
        Dim hecho As Boolean = False
        Try
            Dim objLinea As Entidades.Linea = getLinea()
            Dim objNLinea As New Negocio.Linea
            Lista_Linea_Area = getLista_Linea_Area()
            If txtCodigoLinea.Text = "" Then
                hecho = objNLinea.InsertaLinea(objLinea, Lista_Linea_Area)
            Else
                hecho = objNLinea.ActualizaLinea(objLinea, Lista_Linea_Area)
            End If
            If hecho Then
                frm_State(operativo.Ninguno)
                getLineaByParameters()
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        frm_State(operativo.Ninguno)
    End Sub
    Protected Sub rdbEstado_Busqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbEstado_Busqueda.SelectedIndexChanged
        getLineaByParameters()
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Dim ngcValTabla As New Negocio.TipoTabla
        If CStr(ViewState("Modo")) = "1" Then
            Dim objValTabla As Entidades.TipoTabla = ngcValTabla.ValidadNombres("Linea", "lin_Nombre", Me.txtnomdescripcion.Text.Trim)
            If objValTabla.Item >= 1 Then
                objScript.mostrarMsjAlerta(Me, "Ya existe el nombre " + Me.txtnomdescripcion.Text.Trim + " ingrese un nombre diferente" + "\n ó este inactivo.")
                txtnomdescripcion.Focus()
                Exit Sub
            End If
        End If

        registrarLinea()

    End Sub

    Private Sub actualizarLinea()
        listaLineaConsulta = getlistaLinea()
        setlistaLinea(listaLineaConsulta)
    End Sub


#Region "Seccion de area"

    Function cargarLinea_Area(ByVal Idlinea As Integer) As List(Of Entidades.Linea_Area)
        Lista_Linea_Area = (New Negocio.Linea_Area).SelectLinea_Area(Idlinea)
        If Lista_Linea_Area.Count > 0 Then
            If Lista_Linea_Area(0).objArea Is Nothing Then Lista_Linea_Area(0).objArea = (New Negocio.Area).SelectCbo
        End If
        If Lista_Linea_Area.Count > 0 Then
            For x As Integer = 1 To Lista_Linea_Area.Count - 1
                Lista_Linea_Area(x).objArea = Lista_Linea_Area(x - 1).objArea
            Next
        End If
        Return Lista_Linea_Area
    End Function
    Private Function getLista_Linea_Area() As List(Of Entidades.Linea_Area)
        Lista_Linea_Area = New List(Of Entidades.Linea_Area)
        For Each fila As GridViewRow In gvarea.Rows
            Obj_Linea_Area = New Entidades.Linea_Area
            With Obj_Linea_Area

                Dim cboArea As DropDownList = CType(fila.Cells(1).FindControl("dlarea"), DropDownList)
                .IdArea = CInt(cboArea.SelectedValue)

                If fila.RowIndex = 0 Then
                    Lista_Area = New List(Of Entidades.Area)
                    For x As Integer = 0 To cboArea.Items.Count - 1
                        Obj_Area = New Entidades.Area
                        With Obj_Area
                            .Id = CInt(cboArea.Items(x).Value)
                            .DescripcionCorta = cboArea.Items(x).Text
                        End With
                        Lista_Area.Add(Obj_Area)
                    Next
                    .objArea = Lista_Area
                End If
                If Lista_Linea_Area.Count > 0 Then .objArea = Lista_Linea_Area(0).objArea
                .LAEstado = CBool(CType(fila.Cells(2).FindControl("ckestado"), CheckBox).Checked)
            End With
            Lista_Linea_Area.Add(Obj_Linea_Area)
        Next
        Return Lista_Linea_Area
    End Function
    Private Sub LLenarGrillaArea()
        Lista_Linea_Area = getLista_Linea_Area()
        Obj_Linea_Area = New Entidades.Linea_Area
        With Obj_Linea_Area
            .IdArea = 0
            If Lista_Linea_Area.Count = 0 Then .objArea = (New Negocio.Area).SelectCbo()
            If Lista_Linea_Area.Count > 0 Then .objArea = Lista_Linea_Area(0).objArea
            .LAEstado = True
        End With
        Lista_Linea_Area.Add(Obj_Linea_Area)
        gvarea.DataSource = Lista_Linea_Area
        gvarea.DataBind()

    End Sub
    Private Sub gvArea_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvarea.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(1).FindControl("dlarea"), DropDownList)
                Dim ck As CheckBox = CType(e.Row.Cells(1).FindControl("ckestado"), CheckBox)
                If Lista_Linea_Area(e.Row.RowIndex).IdArea <> 0 Then
                    cbo.SelectedValue = CStr(Lista_Linea_Area(e.Row.RowIndex).IdArea)
                End If
                ck.Checked = Lista_Linea_Area(e.Row.RowIndex).LAEstado
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub gvArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvarea.SelectedIndexChanged
        Try
            Lista_Linea_Area = getLista_Linea_Area()
            Lista_Linea_Area.RemoveAt(gvarea.SelectedIndex)
            gvarea.DataSource = Lista_Linea_Area
            gvarea.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btaddarea_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btaddarea.Click
        Try
            LLenarGrillaArea()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

    Private Sub ddl_tipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_tipoExistencia.SelectedIndexChanged
        getLineaByParameters()
    End Sub
End Class
