﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmMantSerieCheque
    Inherits System.Web.UI.Page

#Region "Atributos"

    Private FuncCombo As New Combo
    Private objScript As New ScriptManagerClass
    Private objNegSerieChequeView As New Negocio.SerieChequeView
    Private p As New Entidades.SerieChequeView

    Public Property SLista() As List(Of Entidades.SerieChequeView)
        Get
            Return CType(Session.Item("ListSerieChequeView"), List(Of Entidades.SerieChequeView))
        End Get
        Set(ByVal value As List(Of Entidades.SerieChequeView))
            Session.Remove("ListSerieChequeView")
            Session.Add("ListSerieChequeView", value)
        End Set
    End Property
    Public Property SModo() As modo_menu
        Get
            Return CType(Session.Item("Modo"), modo_menu)
        End Get
        Set(ByVal value As modo_menu)
            Session.Remove("Modo")
            Session.Add("Modo", value)
        End Set
    End Property
    Public Property SId() As Integer
        Get
            Return CType(Session.Item("IdRegistroFila"), Integer)
        End Get
        Set(ByVal value As Integer)
            Session.Remove("IdRegistroFila")
            Session.Add("IdRegistroFila", value)
        End Set
    End Property

    Enum modo_menu
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Insertar = 3
        Actualizar = 4
    End Enum

#End Region

    Protected Function ListaGetIndiceEntidadxId(ByVal id As Integer) As Integer
        For i As Integer = 0 To SLista.Count - 1
            If SLista(i).Id = id Then
                Return i
            End If
        Next
        Return -1
    End Function

    Protected Sub cargar_recargar_datos()
        SLista = objNegSerieChequeView.SelectAll()
    End Sub

    Protected Sub cargar_dgv()
        actualizar_atributos()
        dgvSerieCheque.DataSource = objNegSerieChequeView.FiltrarLista(p, SLista)
        dgvSerieCheque.DataBind()
    End Sub

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            inicializarFRM()
            SModo = modo_menu.Inicio
            HabilitarControles(SModo)
            ConfigurarDatos()
        Else
            actualizar_atributos()
        End If
    End Sub

    Private Sub inicializarFRM()
        Dim objScript As New ScriptManagerClass
        Try
            cargar_recargar_datos()
            FuncCombo.LlenarCboBanco(cboBanco, True)
            FuncCombo.LlenarCboCuentaBancaria(cboCuentaBancaria, CInt(cboBanco.SelectedValue), True)
            cargar_dgv()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizar_atributos()
        Try
            'SLista = CType(Session.Item("SLista"), List(Of Entidades.SerieChequeView))
            'modo = CType(Session.Item("modo"), modo_menu)
            'p.Id = CType(Session.Item("IdRegistroFila"), Integer)
            p.Id = SId
            p.IdBanco = CInt(Me.cboBanco.SelectedValue)
            p.IdCuentaBancaria = CInt(IIf(CStr(Me.cboCuentaBancaria.SelectedValue) <> "", Me.cboCuentaBancaria.SelectedValue, 0))
            'p.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)
            p.Descripcion = DecodeCadena(txtDescripcion.Text)
            p.Serie = DecodeCadena(txtSerie.Text)
            'p.Estado = CStr(rbtlEstado.SelectedValue)
            Select Case CInt(rbtlEstado.SelectedValue)
                Case 2
                    p.Estado = Nothing
                Case 1
                    p.Estado = True
                Case 0
                    p.Estado = False
            End Select

            p.NroInicio = CInt(IIf(txtNroInicio.Text.Trim = "", 0, txtNroInicio.Text))
            p.NroFin = CInt(IIf(txtNroFin.Text.Trim = "", 0, txtNroFin.Text))
            p.Longitud = CInt(IIf(txtLongitud.Text.Trim = "", 0, txtLongitud.Text))
            

        Catch ex As Exception
        End Try
    End Sub

    Public Sub limpiarCtrl()

        Me.cboBanco.SelectedIndex = 0
        Me.cboCuentaBancaria.SelectedIndex = 0
        Me.txtDescripcion.Text = ""
        Me.txtSerie.Text = ""
        Me.rbtlEstado.SelectedValue = "1" 'Activos
        Me.txtLongitud.Text = ""
        Me.txtNroInicio.Text = ""
        Me.txtNroFin.Text = ""
    End Sub

    'Private Sub cargar_entidad_en_controles(ByVal x As Entidades.SerieChequeView)
    '    cboBanco.SelectedValue = CStr(x.IdBanco)
    '    txtDescripcion.Text = x.Descripcion
    '    rbtlEstado.SelectedValue = x.Estado
    'End Sub
    Public Sub HabilitarControles(ByVal x As modo_menu)
        Select Case x
            Case modo_menu.Inicio

                Me.btnNuevo.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscar.Visible = True

                'Me.cboBanco.Enabled = True
                'Me.cboCuentaBancaria.Enabled = True
                'Me.txtDescripcion.Visible = True
                'Me.txtSerie.Visible = True
                Me.rbtlEstado.Items(0).Enabled = True

                'Me.lblBanco.Visible = True
                'Me.lblDescripcion.Visible = True
                'Me.lblEstado.Visible = True

                Me.dgvSerieCheque.Enabled = True

            Case modo_menu.Nuevo

                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False

                'Me.cboBanco.Enabled = True
                'Me.cboCuentaBancaria.Enabled = True

                'Me.txtDescripcion.Visible = True
                'Me.txtSerie.Visible = True
                Me.rbtlEstado.Items(0).Enabled = False

                'Me.lblBanco.Visible = True
                'Me.lblDescripcion.Visible = True
                'Me.lblEstado.Visible = True

                Me.dgvSerieCheque.Enabled = False

            Case modo_menu.Editar

                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False

                'Me.cboBanco.Enabled = False
                'Me.cboCuentaBancaria.Enabled = False
                'Me.txtDescripcion.Visible = True
                'Me.txtSerie.Visible = True
                Me.rbtlEstado.Items(0).Enabled = False

                'Me.lblBanco.Visible = True
                'Me.lblDescripcion.Visible = True
                'Me.lblEstado.Visible = True

                Me.dgvSerieCheque.Enabled = False

            Case modo_menu.Actualizar
            Case modo_menu.Insertar
        End Select
    End Sub

    Public Function DecodeCadena(ByVal cadena As String) As String
        Dim cadenavalida As String = ""
        cadenavalida = HttpUtility.HtmlDecode(cadena)
        Return cadenavalida
    End Function


    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        cargar_dgv()
    End Sub

    Protected Sub lbtnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ind As Integer 'indice de la lista correspondiente al registro a modificar

            SModo = modo_menu.Editar

            'Se obtiene el Id del Registro a editar 
            'ini
            Dim lbtnEditar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lbtnEditar.NamingContainer, GridViewRow)
            SId = CInt(CType(fila.Cells(0).FindControl("hddId"), HiddenField).Value.Trim())
            'fin

            'se obtiene el indice de la lista, para asi obtener la entidad a editar 
            ind = ListaGetIndiceEntidadxId(SId)

            'se carga los controles
            'ini
            Me.cboBanco.SelectedValue = CStr(SLista(ind).IdBanco)
            FuncCombo.LlenarCboCuentaBancaria(cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), True)
            Me.cboCuentaBancaria.SelectedValue = CStr(SLista(ind).IdCuentaBancaria)
            Me.rbtlEstado.SelectedValue = SLista(ind).DescEstadoNum

            Me.txtDescripcion.Text = SLista(ind).Descripcion
            Me.txtSerie.Text = SLista(ind).Serie
            Me.txtLongitud.Text = CStr(SLista(ind).Longitud)
            Me.txtNroInicio.Text = CStr(SLista(ind).NroInicio)
            Me.txtNroFin.Text = CStr(SLista(ind).NroFin)
            'fin


            HabilitarControles(SModo)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        'cargar_recargar_modo(modo_menu.Nuevo)
        SModo = modo_menu.Nuevo
        HabilitarControles(SModo)
        'limpiarCtrl()  'se deben dejar los datos escritos
        cargar_dgv()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        'cargar_recargar_Id(0)   'Limpia el ID
        SId = 0         'Limpia el ID
        'cargar_recargar_modo(modo_menu.Inicio)
        SModo = modo_menu.Inicio
        HabilitarControles(modo_menu.Inicio)
        limpiarCtrl()
        cargar_dgv()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click

        Dim mensaje As String
        Dim exito As Boolean
        exito = True
        mensaje = "Se Guardó Correctamente"
        'Solo entran los modos editar y nuevo, en esta zona
        'opcional ini
        cargar_recargar_datos() 'Los datos disponibles se actualizan antes de realizar la validacion
        'opcional fin

        Select Case SModo
            Case modo_menu.Nuevo
                If Not objNegSerieChequeView.InsertT(p) Then
                    exito = False
                    mensaje = "Problema en la Inserción"
                End If
            Case modo_menu.Editar
                If Not objNegSerieChequeView.UpdateT(p) Then
                    exito = False
                    mensaje = "Problema en la Actualización"
                End If
        End Select

        objScript.mostrarMsjAlerta(Me, mensaje)
        If exito Then
            'cargar_recargar_Id(0)   'Limpia el ID
            SId = 0                 'Limpia el ID
            cargar_recargar_datos()
            'cargar_recargar_modo(modo_menu.Inicio)
            SModo = modo_menu.Inicio
            HabilitarControles(SModo)
            cargar_dgv()    'Muestra el registro ingresado

            limpiarCtrl()   'Limpia los controles
        End If
    End Sub

    Protected Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboBanco.SelectedIndexChanged
        FuncCombo.LlenarCboCuentaBancaria(cboCuentaBancaria, p.IdBanco, True)
    End Sub

    Protected Sub dgvSerieCheque_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvSerieCheque.RowDataBound
        'LinkButton Editar esta en un UpdatePanel y para que este no atrape su SerieChequeback, se registra en el ScriptManager de la página Maestra
        Try
            If e.Row.RowType = DataControlRowType.DataRow And e.Row.RowState = DataControlRowState.Normal Or _
            e.Row.RowState = DataControlRowState.Alternate Or e.Row.RowState = DataControlRowState.Selected Then
                Dim sm As ScriptManager
                sm = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                sm.RegisterPostBackControl(DirectCast(e.Row.FindControl("lbtnEditar"), LinkButton))
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class