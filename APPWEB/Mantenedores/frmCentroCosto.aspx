﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmCentroCosto.aspx.vb" Inherits="APPWEB.frmCentroCosto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td  class ="TituloCelda" >                               
                    Centro de Costo
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btNuevo" Width="80px" runat="server" Text="Nuevo" />
                <asp:Button ID="btGuardar" Width="80px" runat="server" Text="Guardar" OnClientClick="return(validarGuardar());" />
                <asp:Button ID="btCancelar" OnClientClick="return(confirm('Desea ir al modo Inicio?'));"
                    Width="80px" runat="server" Text="Cancelar" />
                <asp:HiddenField ID="hdd_modo" runat="server" Value="0" />
                <hr />
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlDatos" runat="server">
    <table>
        <tr>
            <td>
                <table style="width: 100%;">
                    <tr>
                        <td class="LabelTdLeft">
                            &nbsp;Unidad Negocio:<br />
                            <asp:DropDownList ID="dlunidadnegocio" runat="server" Width="150px" 
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="LabelTdLeft">
                            &nbsp;Dpto Funcional:<br />
                            <asp:DropDownList ID="dldptofuncional" runat="server" Width="150px" 
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="LabelTdLeft">
                            &nbsp;Sub area 1:<br />
                            <asp:DropDownList ID="dlsubarea1" runat="server" Width="150px" 
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="LabelTdLeft">
                            &nbsp;Sub area 2:<br />
                            <asp:DropDownList ID="dlsubarea2" runat="server" Width="150px" 
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="LabelTdLeft">
                            &nbsp;Sub area 3:<br />
                            <asp:DropDownList ID="dlsubarea3" runat="server" Width="150px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 50%;">
                    <tr>
                        <td class="LabelTdLeft">
                            &nbsp;Codigo:
                        </td>
                        <td>
                            <asp:TextBox ID="tbcodigo" MaxLength="2" Enabled="false" onkeyPress="return ( onKeyPressEsNumero('event') );" Width="50px" runat="server"></asp:TextBox>                            
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelTdLeft">
                            &nbsp;Descripcion:
                        </td>
                        <td>
                            <asp:TextBox ID="tbdescripcion" runat="server" Width="350px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelTdLeft">
                            &nbsp;Estado:
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rbestado" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <hr />
    </asp:Panel>
    <table style="width: 100%;">
        <tr>
            <td class="TituloCeldaLeft">
                &nbsp; Busqueda
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlBusqueda" runat="server">
        <table>
            <tr>
                <td>
                    <table style="width: 100%;">
                        <tr>
                            <td class="LabelTdLeft">
                                &nbsp;Unidad Negocio:<br />
                                <asp:DropDownList ID="dlunidad" runat="server" DataTextField="Nombre_CC" 
                                    DataValueField ="Cod_UniNeg" Width="150px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="LabelTdLeft">
                                &nbsp;Dpto Funcional:<br />
                                <asp:DropDownList ID="dlfuncional" runat="server" Width="150px" 
                                    AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="LabelTdLeft">
                                &nbsp;Sub area 1:<br />
                                <asp:DropDownList ID="dlarea1" runat="server" Width="150px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="LabelTdLeft">
                                &nbsp;Sub area 2:<br />
                                <asp:DropDownList ID="dlarea2" runat="server" Width="150px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="LabelTdLeft">
                            &nbsp;Sub area 3:<br />
                            <asp:DropDownList ID="dlarea3" runat="server" Width="150px">
                            </asp:DropDownList>
                        </td>
                            <td>
                                <asp:Button ID="btbuscar" runat="server" Text="Buscar" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvbusqueda" runat="server" GridLines="None" Width="100%" 
                        AutoGenerateColumns="False" AllowPaging="True">
                        <RowStyle CssClass="GrillaRow" />
                        <Columns>
                            <asp:CommandField SelectText="Editar" ShowSelectButton="True" />
                            <asp:BoundField DataField="strCodigo" HeaderText="Codigo" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddCod_UniNeg" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"Cod_UniNeg") %>' />
                                    <asp:HiddenField ID="hddCod_DepFunc" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"Cod_DepFunc") %>' />
                                    <asp:HiddenField ID="hddCod_SubCodigo2" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"Cod_SubCodigo2") %>' />
                                    <asp:HiddenField ID="hddCod_SubCodigo3" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"Cod_SubCodigo3") %>' />
                                    <asp:HiddenField ID="hddCod_SubCodigo4" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"Cod_SubCodigo4") %>' />
                                    <asp:HiddenField ID="hddidcentrocosto" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"IdCentroCosto") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="strCod_UniNeg" HeaderText="Unidad Negocio" />
                            <asp:BoundField DataField="strCod_DepFunc" HeaderText="Dpto Funcional" />
                            <asp:BoundField DataField="strCod_SubCodigo2" HeaderText="Subarea 1" />
                            <asp:BoundField DataField="strCod_SubCodigo3" HeaderText="Subarea 2" />
                            <asp:BoundField DataField="strCod_SubCodigo4" HeaderText="Subarea 3" />
                            <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:CheckBox ID="ckestado" runat="server" 
                                        Checked='<%# DataBinder.Eval(Container.DataItem,"Estado_CC") %>' 
                                        Enabled="False" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButtonList ID="rbver" runat="server" AutoPostBack="True" CssClass="LabelTab"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="2">Todos</asp:ListItem>
                        <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                        <asp:ListItem Value="0">Inactivo</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID ="hddConfigurarDatos" runat ="server" Value = "0" />
    <asp:HiddenField ID="hdd_idcentrocosto" Value="0" runat="server" />
    
    <script language="javascript" type="text/javascript">

        function validarGuardar() {
            var codigo = document.getElementById('<%=tbcodigo.ClientID %>');
            if (codigo.value == '0' | codigo.value == '00' | codigo.value == ' 0' | codigo.value == '0 ') {
                alert('El valor ingresado no es valido');
                codigo.select();
                codigo.focus();
                return false;
            }
            if (codigo.value == ' ' | codigo.value == '  ') {
                alert('El valor ingresado no es valido');
                codigo.select();
                codigo.focus();
                return false;
            }
            if (codigo.value.length != '2') {
                alert('El codigo debe tener una longitud de 2 caracteres');
                codigo.select();
                codigo.focus();
                return false;
            }
            var descripcion = document.getElementById('<%=tbdescripcion.ClientID %>');
            if (descripcion.value == '' | descripcion.value == ' ') {
                alert('Debe ingresar una descripcion');
                descripcion.select();
                descripcion.focus();
                return false;
            }
            var radio = document.getElementById('<%=rbestado.ClientID %>');
            var modo = document.getElementById('<%=hdd_modo.ClientID %>');
            if (modo.value == '1') 
            {
                control = radio.getElementsByTagName('input');
                if (control[0].checked == false ){
                    alert('El nuevo registro no puede tener estado inactivo');
                    return false;
                }            
            }
            
            
            return confirm('Desea continuar con la operacion ?');
        }
    </script>

<script language ="javascript" type="text/javascript">
    //////////////////////////////////////////
    var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
    /////////////////////////////////////////
</script>

</asp:Content>
