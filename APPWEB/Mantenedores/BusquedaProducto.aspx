<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="BusquedaProducto.aspx.vb" Inherits="APPWEB.BusquedaProducto" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../css/buttons.css" rel="stylesheet" type="text/css" />
    <table style="width: 100%;">
        <tr>
            <td align="right">
                <table>
                    <tr>
                        <td class="Texto">
                            Fecha Actual:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                Width="100px" onFocus=" return( Readonly(thfis) );" ></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Almac&eacute;n:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboAlmacenReferencia" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto">
                            Tipo Existencia:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Tipo Precio:"></asp:Label>
                            <asp:DropDownList TabIndex="202" ID="cboTipoPrecioV" AutoPostBack="true" runat="server"
                                DataTextField="Nombre" DataValueField="Id">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                DataTextField="Descripcion" DataValueField="Id">
                            </asp:DropDownList>
                            <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                            <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" AutoPostBack="true" runat="server"
                                DataTextField="Nombre" DataValueField="Id">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">
                                        <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                            onKeypress="return( valKeyPressDescripcionProd(this) );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                            onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                            </asp:Panel>
                                    </td>
                                    <td class="Texto">
                                        C�d.:
                                    </td>
                                    <td>
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">
                                        <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                            onKeypress="return( valKeyPressDescripcionProd(this) );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                            onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" TabIndex="205"></asp:TextBox>
                                            </asp:Panel>
                                    </td>
                                    <td>
                                        <%--<asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                            TabIndex="207" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />--%>
                                        <asp:Button ID="btnBuscarGrilla_AddProducto" runat="server" Text="Buscar" CssClass="btnBuscar"
                                        OnClientClick="this.disabled=true;this.value= 'Procesando...'" UseSubmitBehavior="false" />
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnVerCampania_Consulta" runat="server" Text="Campa�as" Width="80px" CssClass="btnCampa�a"
                                            ToolTip="Ver Campa�as Vigentes" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            &nbsp;
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chb_FiltroProductoCampania" runat="server" Checked="false" CssClass="Texto"
                                            Font-Bold="true" Text="Filtrar Productos en Campa�a." />
                                    </td>
                                    <td class="Label">
                                        Cod. Barras:
                                    </td>
                                    <td>
                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">
                                        <asp:TextBox ID="txtCodBarrasCapa" runat="server" onKeypress="return( valKeyPressDescripcionProd(this) );"
                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus=" onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                    </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                    CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                    CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                    CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                    CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                    SuppressPostBack="true">
                </cc1:CollapsiblePanelExtender>
                <asp:Image ID="Image21_11" runat="server" Height="16px" />
                <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                    <table width="100">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold">
                                            Atributo:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoTabla" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                    Width="650px">
                                    <Columns>
                                        <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Width="75px" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            SelectText="Quitar" ShowSelectButton="True" />
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Atributo" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Valor" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboTTV" runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>'
                                                    DataTextField="Nombre" DataValueField="IdTipoTablaValor" Width="200px">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="GrillaRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%"
                    PageSize="20">
                    <Columns>
                        <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" Visible="false">
                            <ItemTemplate>
                                <asp:TextBox ID="txtCantidad_AddProd" TabIndex="210" Width="65px" onKeyup="return(  valStockDisponible_AddProd() );"
                                    onblur="return( valBlurClear('0.00',event) );" onfocus="return(read_Lista(this));"
                                    onKeypress="return( valKeyPressCantidadAddProd()  );" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>'
                                    runat="server"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" NullDisplayText="---" />
                        <asp:TemplateField HeaderText="U.M.">
                            <ItemTemplate>
                                <asp:DropDownList ID="cmbUnidadMedida_AddProd" runat="server" OnSelectedIndexChanged="cmbUnidadMedidaVenta_Catalogo_SelectedIndexChanged"
                                    AutoPostBack="true" TabIndex="211" DataTextField="DescripcionCorto" DataValueField="Id">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="NomTipoPV" HeaderText="Tipo P.V." NullDisplayText="---" />
                        <asp:BoundField DataField="SimbMoneda" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"
                            NullDisplayText="---" />
                        <asp:BoundField DataField="PrecioSD" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"
                            HeaderText="P. Venta" NullDisplayText="0" DataFormatString="{0:F2}" />
                        <asp:BoundField DataField="StockDisponibleN" HeaderText="Stock Disponible" NullDisplayText="0"
                            DataFormatString="{0:F4}" />
                        <asp:BoundField DataField="Percepcion" HeaderText="Percepci�n (%)" NullDisplayText="0"
                            DataFormatString="{0:F2}" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="btnConsultarStockAlmacenes_BuscarProd" runat="server" OnClick="mostrarCapaStockPrecioxProducto"
                                                ImageUrl="~/Imagenes/Ok_b.bmp" ToolTip="Consultar Stock y Precios por Tienda / Almac�n."
                                                OnClientClick="this.src='../Imagenes/loader.gif'" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnConsultarTipoPrecio" runat="server" OnClick="mostrarCapaConsultarTipoPrecio"
                                                ImageUrl="~/Imagenes/Ok_b.bmp" ToolTip="Consultar Tipo Precios de Venta." />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddIdProducto_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddPrecioComercial" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"pvComercial","{0:F2}")%>' />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnMostrarComponenteKit_Find" runat="server" ImageUrl="~/Imagenes/search_add.ico"
                                                Width="20px" ToolTip="Visualizar Componentes del Kit" OnClick="btnMostrarComponenteKit_Find_Click" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnViewCampania" runat="server" ImageUrl="~/Imagenes/regalo.jpg"
                                                Width="20px" ToolTip="Producto en Campa�a" OnClick="valOnClick_btnViewCampania" />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddCodBarraProdCapa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Prod_CodigoBarras")%>' />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GrillaHeader" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GV_ComponenteKit_Find" runat="server" AutoGenerateColumns="false"
                    Width="100%">
                    <Columns>
                        <asp:BoundField DataField="CodigoProd_Comp" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                            ItemStyle-Font-Bold="true" />
                        <asp:BoundField DataField="Componente" HeaderText="Componente" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                            ItemStyle-Font-Bold="true" />
                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCantidad_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad_Comp","{0:F3}")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblUnidadMedida_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida_Comp")%>'></asp:Label>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Precio" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMonedaPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaPrecio_Comp")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Precio_Comp","{0:F3}")%>'></asp:Label>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GrillaHeader" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                    Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                    Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                    CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                    Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                    runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdTipoPVDefault" runat="server" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hddIndex_Campania_Producto" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <div id="capaConsultarStockPrecioxProducto" style="border: 3px solid blue; padding: 8px;
        width: 800px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaConsultarStockPrecioxProducto')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProductoConsultarStockPrecioxProd" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIdProductoStockPrecio" runat="server" />
                                <asp:HiddenField ID="hddDescripcionFlete" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Tipo Almac&eacute;n:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTipoAlmacen" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ConsultarStockPrecioxProd" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Tienda" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:HiddenField ID="hddIdTienda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTienda")%>' />

                                                <asp:Label ID="lblTienda" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Almac�n" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                <asp:Label ID="lblAlmacen" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Almacen")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Precio Lista" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SimbMoneda")%>'></asp:Label>

                                                <asp:Label ID="lblPrecioLista" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioLista","{0:F3}")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="U.M." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblUM" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cadenaUM")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Real" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblStockReal" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StockAReal","{0:F3}")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Comprometido" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblStockComprometido" ForeColor="Red" Font-Bold="true" runat="server"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"StockComprometido","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Disponible" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblStockDisponible" ForeColor="Red" Font-Bold="true" runat="server"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"StockDisponibleN","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock-Tr�nsito">
                                <ItemTemplate>
                                                <asp:Label ID="lblStockEnTransito" runat="server" ForeColor="Red" Font-Bold="true"
                                                    Text='<%# DataBinder.Eval(Container.DataItem,"StockEnTransito","{0:F3}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <table>
                        <tr>
                        <td class="Texto">
                                Stock Real:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStockReal" runat="server" Width="70px" onFocus="return(onFocus_ReadOnly(this));"
                                    CssClass="TextBoxReadOnly"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Stock Comprometido:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStockComprometido" runat="server" Width="70px" onFocus="return(onFocus_ReadOnly(this));"
                                    CssClass="TextBoxReadOnly"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Stock Disponible:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStockDisponible" runat="server" Width="70px" onFocus="return(onFocus_ReadOnly(this));"
                                    CssClass="TextBoxReadOnly"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Stock Tr&aacute;nsito:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStockTransito" runat="server" Width="70px" onFocus="return(onFocus_ReadOnly(this));"
                                    CssClass="TextBoxReadOnly"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnAddProducto_ConsultarStockPrecio" runat="server" Text="Aceptar"
                        Width="90px" OnClientClick="return(  onClick_AddProducto_ConsultarStockPrecio()  );" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaConsultarTipoPrecio" style="border: 3px solid blue; padding: 8px; width: 500px;
        height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton4" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaConsultarTipoPrecio')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold" align="right">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProducto_ConsultarTipoPrecio" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold" align="right">
                                Tienda:
                            </td>
                            <td>
                                <asp:Label ID="lblTienda_ConsultarTipoPrecio" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ConsultarTipoPrecio" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="NomTipoPV" HeaderText="Tipo Precio" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="cadenaUM" HeaderText="U. M." HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Precio Lista" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMoneda" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SimbMoneda")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPrecioLista" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioLista","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnAceptar_ConsultarTipoPrecio" runat="server" Text="Aceptar" Width="90px"
                        OnClientClick="return(  offCapa('capaConsultarTipoPrecio')   );" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaCampania_Producto" style="border: 3px solid blue; padding: 8px; width: 800px;
        height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton14" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaCampania_Producto')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblCodigoProducto_capaCampania_Producto" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                            <td style="width: 15px">
                            </td>
                            <td>
                                <asp:Label ID="lblProducto_capaCampania_Producto" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_Campania_Producto" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Height="25px"
                                Visible="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="Campa�a" DataField="Campania" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Inicio" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFechaInicio" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FechaInicio","{0:dd/MM/yyyy}")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdCampania" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCampania")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdUnidadMedida" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fin" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFechaFin" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FechaFin","{0:dd/MM/yyyy}")%>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="U.M." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblUnidadMedida" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida")%>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Precio" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMoneda" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPrecioCampania" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCampania","{0:F3}")%>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cantidad M�n." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCantidadMin" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CantidadMin","{0:F3}")%>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnCerrar_capaCampania_Producto" runat="server" Text="Cerrar" Width="70px"
                        OnClientClick="return(  offCapa('capaCampania_Producto')   );" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaConsultar_Campania" style="border: 3px solid blue; padding: 8px; width: 800px;
        height: auto; position: absolute; top: 100px; left: 20px; background-color: white;
        z-index: 6; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_capaConsultar_Campania" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImageButton15" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(  offCapa('capaConsultar_Campania')   );" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_Campania_Cab" runat="server" AutoGenerateColumns="false" Width="100%">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Campa�a" DataField="cam_Descripcion" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="Inicio" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblFechaInicio" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cam_FechaInicio","{0:dd/MM/yyyy}")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hddIdCampania" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCampania")%>' />
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fin" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblFechaFin" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cam_FechaFin","{0:dd/MM/yyyy}")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Empresa" DataField="per_NComercial" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Tienda" DataField="tie_Nombre" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_Campania_Det" runat="server" AutoGenerateColumns="false" Width="100%"
                                AllowPaging="true" PageSize="20">
                                <Columns>
                                    <asp:BoundField HeaderText="C�digo" DataField="CodigoProducto" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Producto" DataField="Producto" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="U.M." DataField="UnidadMedida" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Cantidad M�n." DataFormatString="{0:F3}" DataField="CantidadMin"
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="Precio" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMoneda" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda_Campania")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblPrecio" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Precio","{0:F3}")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnCerrar_capaConsultar_Campania" runat="server" Text="Cerrar" Width="70px"
                                OnClientClick="return(  offCapa('capaConsultar_Campania')   );" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;

        function CalcularConsultarStockPrecioxProd() {


            var GV_ConsultarStockPrecioxProd = document.getElementById('<%=GV_ConsultarStockPrecioxProd.ClientID %>');
            
            var StockRealTotal = 0;
            var StockComprometidoTotal = 0;
            var StockDisponibleTotal = 0;
            var StockTransitoTotal = 0;

            if (GV_ConsultarStockPrecioxProd != null) {
                for (var i = 1; i < GV_ConsultarStockPrecioxProd.rows.length; i++) {
                    var rowElem = GV_ConsultarStockPrecioxProd.rows[i];

                    var StockReal = parseFloat(rowElem.cells[4].children[0].innerHTML);
                    if (isNaN(StockReal)) { StockReal = 0; }
                    StockRealTotal = StockRealTotal + StockReal;

                    var StockComprometido = parseFloat(rowElem.cells[5].children[0].innerHTML);
                    if (isNaN(StockComprometido)) { StockComprometido = 0; }
                    StockComprometidoTotal = StockComprometidoTotal + StockComprometido;

                    var StockDisponible = parseFloat(rowElem.cells[6].children[0].innerHTML);
                    if (isNaN(StockDisponible)) { StockDisponible = 0; }
                    StockDisponibleTotal = StockDisponibleTotal + StockDisponible;

                    var StockTransito = parseFloat(rowElem.cells[7].children[0].innerHTML);
                    if (isNaN(StockTransito)) { StockTransito = 0; }
                    StockTransitoTotal = StockTransitoTotal + StockTransito;

                } //end for
            } //end if
            document.getElementById('<%=txtStockReal.ClientID %>').value = redondear(StockRealTotal, 4)
            document.getElementById('<%=txtStockComprometido.ClientID %>').value = redondear(StockComprometidoTotal, 4)
            document.getElementById('<%=txtStockDisponible.ClientID %>').value = redondear(StockDisponibleTotal, 4)
            document.getElementById('<%=txtStockTransito.ClientID %>').value = redondear(StockTransitoTotal, 4)
            return false;
        }
        //
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //
        function valKeyPressDescripcionProd(obj) {
            if (event.keyCode == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscarGrilla_AddProducto.ClientID%>').focus();
            }
            return true;
        }
        //
        function onClick_AddProducto_ConsultarStockPrecio() {

            offCapa('capaConsultarStockPrecioxProducto');
            return false;

        }
        //
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        //
    </script>

</asp:Content>
