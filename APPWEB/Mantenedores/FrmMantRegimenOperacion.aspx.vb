﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmMantRegimenOperacion
    Inherits System.Web.UI.Page
    Private FuncCombo As New Combo
    Private objScript As New ScriptManagerClass
    Private objNegRegOpView As New Negocio.RegimenOperacionView
    Private objNegRegOp As New Negocio.RegimenOperacion
    Private ListRegimenOperacionView As New List(Of Entidades.RegimenOperacionView)
    Private p As New Entidades.RegimenOperacionView
    Private modo As New modo_menu

    Enum modo_menu
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Insertar = 3
        Actualizar = 4
    End Enum
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            inicializarFRM()
            modo = modo_menu.Inicio
            HabilitarControles(modo)
        Else
            actualizar_atributos()
        End If
    End Sub
    Private Sub inicializarFRM()
        Dim objScript As New ScriptManagerClass
        Try
            cargar_recargar_datos()
            cargar_controles()
            cargar_dgv()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub actualizar_atributos()
        Try
            ListRegimenOperacionView = CType(Session.Item("ListRegimenOperacionView"), List(Of Entidades.RegimenOperacionView))
            modo = CType(Session.Item("modo"), modo_menu)
            'p.IdRegimen=CInt(ViewState.Item("IdRegimen"))
            p.IdRegimen = CInt(Me.cboRegimen.SelectedValue)
            p.IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            p.CuentaContable = DecodeCadena(txtCtaContable.Text)
            p.Estado = CStr(rbtlEstado.SelectedValue)
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub cargar_recargar_datos()
        ListRegimenOperacionView = objNegRegOpView.SelectAll()
        Session.Remove("ListRegimenOperacionView")
        Session.Add("ListRegimenOperacionView", ListRegimenOperacionView)
    End Sub
    Sub cargar_recargar_modo(ByVal x As modo_menu)
        modo = x
        Session.Remove("modo")
        Session.Add("modo", modo)
    End Sub
    Private Sub cargar_controles()
        FuncCombo.llenarCboRegimen(cboRegimen, True)
        FuncCombo.llenarCboTipoOperacion(cboTipoOperacion, True)
    End Sub
    Protected Sub cargar_dgv()
        actualizar_atributos()
        dgvRegimenOperacion.DataSource = objNegRegOpView.FiltrarLista(p, ListRegimenOperacionView)
        dgvRegimenOperacion.DataBind()
    End Sub
    Public Sub limpiarCtrl()
        Me.cboRegimen.SelectedIndex = 0
        Me.cboTipoOperacion.SelectedIndex = 0
        Me.txtCtaContable.Text = ""
        Me.rbtlEstado.SelectedValue = "1"
    End Sub
    Public Sub HabilitarControles(ByVal x As modo_menu)
        Select Case x
            Case modo_menu.Inicio

                Me.btnNuevo.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscar.Visible = True
                Me.cboRegimen.Enabled = True
                Me.cboTipoOperacion.Enabled = True
                Me.txtCtaContable.Enabled = True
                Me.rbtlEstado.Visible = True
                Me.rbtlEstado.Items(0).Enabled = True
                Me.dgvRegimenOperacion.Enabled = True

            Case modo_menu.Nuevo

                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False
                Me.cboTipoOperacion.Enabled = True
                Me.cboRegimen.Enabled = True
                Me.rbtlEstado.Items(0).Enabled = False
                Me.dgvRegimenOperacion.Enabled = False

            Case modo_menu.Editar

                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False
                Me.cboRegimen.Enabled = True
                Me.cboTipoOperacion.Enabled = True
                Me.txtCtaContable.Enabled = True
                Me.rbtlEstado.Items(0).Enabled = False
                Me.dgvRegimenOperacion.Enabled = True

            Case modo_menu.Actualizar

        End Select
    End Sub
    Public Function DecodeCadena(ByVal cadena As String) As String
        Dim cadenavalida As String = ""
        cadenavalida = HttpUtility.HtmlDecode(cadena)
        Return cadenavalida
    End Function
    
#Region "Botones"
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        cargar_dgv()
    End Sub
    Protected Sub lbtnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            cargar_recargar_modo(modo_menu.Editar)
            Dim lbtnEditar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)

            Dim fila As GridViewRow = CType(lbtnEditar.NamingContainer, GridViewRow)

            Me.cboRegimen.SelectedValue = CStr(CType(fila.Cells(0).FindControl("hddIdRegimen"), HiddenField).Value.Trim())
            Me.cboTipoOperacion.SelectedValue = CStr(CType(fila.Cells(0).FindControl("hddIdTipoOperacion"), HiddenField).Value.Trim())
            Me.rbtlEstado.SelectedValue = CType(fila.Cells(0).FindControl("hddIdEstado"), HiddenField).Value
            Me.txtCtaContable.Text = DecodeCadena(fila.Cells(3).Text)

            HabilitarControles(modo)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        cargar_recargar_modo(modo_menu.Nuevo)
        HabilitarControles(modo)
        'limpiarCtrl()
        txtCtaContable.Enabled = True
        cargar_dgv()
    End Sub
    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click

        cargar_recargar_modo(modo_menu.Inicio)
        HabilitarControles(modo_menu.Inicio)
        limpiarCtrl()
        cargar_dgv()
    End Sub
    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click

        Dim mensaje As String
        Dim exito As Boolean
        exito = True
        mensaje = "Se Guardó Correctamente"

        Select Case modo
            Case modo_menu.Nuevo
                If objNegRegOpView.ValidarEntidad(p, ListRegimenOperacionView) Then
                    If objNegRegOp.InsertaRegimenOperacion(p) Then
                        'btnNuevo.Enabled = True
                        'btnNuevo.Visible = True
                        'txtCtaContable.Enabled = False
                        'cboRegimen.Enabled = False
                        'cboTipoOperacion.Enabled = False
                        'dgvRegimenOperacion.Enabled = True
                    End If
                Else
                    exito = False
                    mensaje = "Ya existe un registro con ese Régimen y Tipo de Operación"

                    'limpiarCtrl()

                    'btnGuardar.Visible = False
                    'btnNuevo.Visible = True
                    'btnNuevo.Enabled = True
                    'btnCancelar.Enabled = True
                    'txtCtaContable.Enabled = False
                    'cboRegimen.Enabled = False
                    'cboTipoOperacion.Enabled = False

                End If
            Case modo_menu.Editar
                If objNegRegOp.ActualizaRegimenOperacion(p) Then
                    mensaje = "Se Actualizó Correctamente"

                    'txtCtaContable.Enabled = False
                    'cboRegimen.Enabled = False
                    'cboTipoOperacion.Enabled = False
                    'Me.btnNuevo.Visible = True
                    'Me.btnNuevo.Enabled = True
                End If
        End Select


        objScript.mostrarMsjAlerta(Me, mensaje)
        If exito Then
            'cargar_recargar_Id(0)   'Limpia el ID
            cargar_recargar_datos()
            cargar_recargar_modo(modo_menu.Inicio)
            HabilitarControles(modo)
            cargar_dgv()    'Muestra el registro ingresado

            limpiarCtrl()   'Limpia los controles
        End If
    End Sub
#End Region


End Class