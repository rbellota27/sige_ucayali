﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




Partial Public Class FrmMantCuentaBancaria
    Inherits System.Web.UI.Page
    Private objcbo As New Combo
    Private objScript As New ScriptManagerClass
    Private objNegCta As New Negocio.CuentaBancaria

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        valOnLoad_Frm()
    End Sub
    Private Sub valOnLoad_Frm()
        Try
            If Not IsPostBack Then
                ConfigurarDatos()
                inicializarFrm()
                muestraSaldos(False, True)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub inicializarFrm()

        Dim objCbo As New Combo
        With objCbo
            .LlenarCboBanco(Me.cboBanco, True)
            .LlenarCboMoneda(Me.cboMoneda, True)
        End With

        verFrm(FrmModo.Inicio, True, True, True)

    End Sub

    Private Sub verFrm(ByVal modo As Integer, ByVal limpiarFrm As Boolean, ByVal cargarGrillaDefault As Boolean, ByVal cargarPropietarioDefault As Boolean)

        If (limpiarFrm) Then
            limpiarCtrl()
        End If

        If (cargarPropietarioDefault) Then
            '******************* PERSONA PROPIETARIO
            Dim listaPropietario As List(Of Entidades.Propietario) = (New Negocio.Propietario).SelectCbo
            If (listaPropietario.Count > 0) Then
                Dim IdEmpresa As Integer = listaPropietario(0).Id
                cargarPersona(IdEmpresa)
            End If
        End If

        If (cargarGrillaDefault) Then
            '*************** CARGAMOS LAS CUENTAS BANCARIAS
            Dim IdPersona As Integer = 0
            If (IsNumeric(Me.hddIdPersona.Value) And Me.hddIdPersona.Value.Trim.Length > 0) Then
                IdPersona = CInt(Me.hddIdPersona.Value)
            End If
            Me.dgvCuentaBancaria.DataSource = objNegCta.SelectcuentaBancaria(IdPersona, 0, 1, 0)
            Me.dgvCuentaBancaria.DataBind()
        End If

        Me.hddFrmModo.Value = CStr(modo)
        actualizarControles_Frm()

    End Sub

    Private Sub actualizarControles_Frm()

        Me.btnNuevo.Visible = False
        Me.btnGuardar.Visible = False
        Me.btnBuscar_CB.Visible = False
        Me.btnBuscarPersona.Visible = False
        Me.cboBanco.Enabled = True

        Me.txtCtaContable.Enabled = True
        Me.txtNumero.Enabled = True
        Me.btnCancelar.Visible = False
        Me.dgvCuentaBancaria.Enabled = True

        Me.cboMoneda.Enabled = True

        Select Case CInt(Me.hddFrmModo.Value)
            Case FrmModo.Inicio

                Me.btnNuevo.Visible = True
                Me.btnBuscar_CB.Visible = True
                Me.btnBuscarPersona.Visible = True

                Me.txtCtaContable.Enabled = False
                Me.txtNumero.Enabled = False

            Case FrmModo.Nuevo

                Me.btnGuardar.Visible = True
                Me.btnBuscarPersona.Visible = True

                Me.dgvCuentaBancaria.Enabled = False
                Me.btnCancelar.Visible = True

            Case FrmModo.Editar


                Me.btnGuardar.Visible = True
                Me.cboBanco.Enabled = False
                Me.btnCancelar.Visible = True
                Me.dgvCuentaBancaria.Enabled = False
                Me.cboMoneda.Enabled = False

        End Select


    End Sub


    Protected Sub lbtnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        valOnClick_btnEditar(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub valOnClick_btnEditar(ByVal index As Integer)

        Try

            'Dim edita As Int16 = CShort(CType(Me.dgvCuentaBancaria.Rows(index).FindControl("hddEdita"), HiddenField).Value)
            'If edita = 1 Then

            Dim IdCuentaBancaria As Integer = CInt(CType(Me.dgvCuentaBancaria.Rows(index).FindControl("hddIdCuentaBancaria"), HiddenField).Value)
            Dim IdBanco As Integer = CInt(CType(Me.dgvCuentaBancaria.Rows(index).FindControl("hddIdBanco"), HiddenField).Value)

            Me.hddIdCuentaBancaria.Value = CStr(IdCuentaBancaria)

            Dim objCuentaBancaria As Entidades.CuentaBancaria = (New Negocio.CuentaBancaria).SelectxIdBancoxIdCuentaBancaria(IdBanco, IdCuentaBancaria)

            If (objCuentaBancaria IsNot Nothing) Then
                cargarCuentaBancaria_GUI(objCuentaBancaria)
            Else
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If
            verFrm(FrmModo.Editar, False, False, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarCuentaBancaria_GUI(ByVal objCuentaBancaria As Entidades.CuentaBancaria)

        '************** PERSONA
        cargarPersona(objCuentaBancaria.IdPersona)

        With objCuentaBancaria

            If (Me.cboBanco.Items.FindByValue(CStr(.IdBanco)) IsNot Nothing) Then

                Me.cboBanco.SelectedValue = CStr(.IdBanco)

            End If

            If (Me.cboMoneda.Items.FindByValue(CStr(.IdMoneda)) IsNot Nothing) Then

                Me.cboMoneda.SelectedValue = CStr(.IdMoneda)

            End If

            Me.txtNumero.Text = .NroCuentaBancaria
            Me.txtCtaContable.Text = .NroCuentaContable

            If .Edita = 1 Then
                muestraSaldos(True, False)
            End If

            Me.txtswiftbanca.Text = .SwiftBancaria
            Me.txtcuentaInter.Text = .NroCuentaInterbanca
            'Me.txtSaldoDisponible.Text = CStr(.SaldoDisponible)
            'Me.txtSaldoContable.Text = CStr(.Saldocontable)

            If (.Estado = "1") Then
                Me.rbtlEstado.SelectedValue = "1"
            Else
                Me.rbtlEstado.SelectedValue = "2"
            End If

        End With

    End Sub

    Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As EventArgs)
        valOnClick_btnEliminar(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub valOnClick_btnEliminar(ByVal index As Integer)

        Try
            Dim IdCuentaBancaria As Integer = CInt(CType(Me.dgvCuentaBancaria.Rows(index).FindControl("hddIdCuentaBancaria"), HiddenField).Value)
            Dim IdBanco As Integer = CInt(CType(Me.dgvCuentaBancaria.Rows(index).FindControl("hddIdBanco"), HiddenField).Value)

            If ((New Negocio.CuentaBancaria).DeleteCtaBancaria(IdCuentaBancaria)) Then

                verFrm(FrmModo.Inicio, False, True, False)
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")

            Else
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
        


    End Sub


    Public Function DecodeCadena(ByVal cadena As String) As String
        Dim cadenavalida As String = ""
        cadenavalida = HttpUtility.HtmlDecode(cadena)
        Return cadenavalida
    End Function

    Public Sub limpiarCtrl()

        '*************** PERSONA
        Me.txtDescripcionPersona.Text = ""
        Me.txtDNI.Text = ""
        Me.txtRUC.Text = ""
        Me.hddIdPersona.Value = ""

        Me.txtCtaContable.Text = ""
        Me.txtNumero.Text = ""
        Me.rbtlEstado.SelectedValue = "1" '*** ACTIVO
        Me.txtcuentaInter.Text = ""
        Me.txtswiftbanca.Text = ""
        If (Me.cboBanco.Items.FindByValue("0") IsNot Nothing) Then
            Me.cboBanco.SelectedValue = "0"
        End If

        If (Me.cboMoneda.Items.FindByValue("0") IsNot Nothing) Then
            Me.cboMoneda.SelectedValue = "0"
        End If

        Me.dgvCuentaBancaria.DataSource = Nothing
        Me.dgvCuentaBancaria.DataBind()

        Me.hddIdCuentaBancaria.Value = ""

    End Sub

    Public Sub muestraSaldos(ByVal v As Boolean, ByVal modoI As Boolean)
        'Me.lblSaldoContable.Enabled = v
        'Me.txtSaldoContable.Enabled = v
        'Me.lblSaldoDisponible.Enabled = v
        'Me.txtSaldoDisponible.Enabled = v
        'If modoI = True Then
        '    lblSaldoDisponible.Text = "Saldo Disponible Inicial"
        '    lblSaldoContable.Text = "Saldo Contable Inicial"
        'Else
        '    lblSaldoDisponible.Text = "Saldo Disponible"
        '    lblSaldoContable.Text = "Saldo Contable"
        'End If

    End Sub

#Region "****************Buscar Personas Mantenimiento"
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   offCapa('capaPersona');     ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersonaView IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersonaView.Descripcion
            Me.txtDNI.Text = objPersonaView.Dni
            Me.txtRUC.Text = objPersonaView.Ruc
            Me.hddIdPersona.Value = CStr(objPersonaView.IdPersona)

        End If
    End Sub
    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 0)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        valOnClick_btnNuevo()
    End Sub
    Private Sub valOnClick_btnNuevo()

        Try
            verFrm(FrmModo.Nuevo, True, False, True)
            muestraSaldos(True, True)
            limpiatxt()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Public Sub limpiatxt()
        'Me.txtSaldoContable.Text = ""
        'Me.txtSaldoDisponible.Text = ""
        Me.txtDescripcionPersona.Text = ""
        Me.txtDNI.Text = ""
        Me.txtRUC.Text = ""
        Me.hddIdPersona.Value = ""
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs)
        registrar_CuentaBancaria()
    End Sub

    Private Sub registrar_CuentaBancaria()
        Try

            Dim objCuentaBancaria As Entidades.CuentaBancaria = obtenerCuentaBancaria()

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo

                    If ((New Negocio.CuentaBancaria).InsertaCuentaBancaria(objCuentaBancaria)) Then
                        verFrm(FrmModo.Inicio, True, True, True)
                        objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")
                    Else
                        objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")

                    End If


                Case FrmModo.Editar

                    If ((New Negocio.CuentaBancaria).UpdateCuentaBancaria(objCuentaBancaria)) Then


                        verFrm(FrmModo.Inicio, True, True, True)
                        muestraSaldos(True, False)
                        objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")
                    Else

                        objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")

                    End If

            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerCuentaBancaria() As Entidades.CuentaBancaria

        Dim obj As New Entidades.CuentaBancaria

        With obj

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdCuentaBancaria = Nothing
                Case FrmModo.Editar
                    .IdCuentaBancaria = CInt(Me.hddIdCuentaBancaria.Value)
            End Select

            .IdBanco = CInt(Me.cboBanco.SelectedValue)
            .IdPersona = CInt(Me.hddIdPersona.Value)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)

            '.Saldocontable = CDec(Me.txtSaldoContable.Text)
            '.SaldoDisponible = CDec(Me.txtSaldoDisponible.Text)


            Select Case CInt(Me.rbtlEstado.SelectedValue)
                Case 1  '********* ACTIVO
                    .Estado = "1"
                Case 2  '********* INACTIVO
                    .Estado = "0"
            End Select

            .NroCuentaBancaria = Me.txtNumero.Text
            .NroCuentaContable = Me.txtCtaContable.Text
            .NroCuentaInterbanca = Me.txtcuentaInter.Text
            .SwiftBancaria = Me.txtswiftbanca.Text
        End With

        Return obj

    End Function

    Private Sub btnBuscar_CB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar_CB.Click
        valOnClick_btnBuscar_CB()
        muestraSaldos(False, False)

    End Sub

    Private Sub valOnClick_btnBuscar_CB()

        Try

            Dim IdPersona As Integer = 0
            If (IsNumeric(Me.hddIdPersona.Value) And Me.hddIdPersona.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdPersona.Value) <> 0) Then

                    IdPersona = CInt(Me.hddIdPersona.Value)

                End If
            End If

            Dim estado As Short = 1

            Select Case CInt(Me.rbtlEstado.SelectedValue)
                Case 1 '********** ACTIVO
                    estado = 1
                Case 2 '********** INACTIVO
                    estado = 0
            End Select

            Me.dgvCuentaBancaria.DataSource = objNegCta.SelectcuentaBancaria(IdPersona, CInt(Me.cboBanco.SelectedValue), estado, CShort(Me.cboMoneda.SelectedValue))
            Me.dgvCuentaBancaria.DataBind()

            If (Me.dgvCuentaBancaria.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        valOnClick_btnCancelar()
        muestraSaldos(False, False)
        limpiatxt()

    End Sub
    Private Sub valOnClick_btnCancelar()

        Try

            verFrm(FrmModo.Inicio, True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnGuardar_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        If (cboBanco.Enabled = True) Then


            Dim objCuentaBancaria As Entidades.CuentaBancaria = obtenerCuentaBancaria()

            Dim objCuenta As Entidades.CuentaBancaria = New Negocio.CuentaBancaria().VerificarsiExisteCB(objCuentaBancaria.IdBanco, objCuentaBancaria.IdPersona, objCuentaBancaria.IdMoneda)

            If (objCuenta.IdCuentaBancaria = 0) Then
                registrar_CuentaBancaria()
                muestraSaldos(False, False)
                limpiatxt()
            Else
                objScript.mostrarMsjAlerta(Me, "La Cuenta Bancaria ingresada ya se encuentra Registrada. No procede la operación.")
            End If
        Else
            registrar_CuentaBancaria()
            muestraSaldos(False, False)
            limpiatxt()
        End If


    End Sub

    Private Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        Me.txtDescripcionPersona.Text = ""
        Me.txtDNI.Text = ""
        Me.txtRUC.Text = ""
        Me.hddIdPersona.Value = "0"
    End Sub
End Class
