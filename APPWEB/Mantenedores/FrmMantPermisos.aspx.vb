﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.




Partial Public Class FrmMantPermisos
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Private ListaPermisos As List(Of Entidades.Perfil)
    Private objCombo As New Combo
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            inicializarFrm()

        End If
    End Sub

    Private Sub inicializarFrm()
        Try
            objCombo.LlenarCboPerfil(Me.cboPerfil, False)
            objCombo.LlenarCboArea_Permiso(Me.cboArea, True)

            cargarGV_Permisos(CInt(Me.cboArea.SelectedValue), CInt(Me.cboPerfil.SelectedValue))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cargarGV_Permisos(ByVal IdArea As Integer, ByVal IdPerfil As Integer)

        Me.DGV_PerfilPermisos.DataSource = (New Negocio.Permiso).SelectxIdAreaxIdPerfil(IdArea, IdPerfil)
        Me.DGV_PerfilPermisos.DataBind()

    End Sub






    'llenar Combo
    Private Sub CargarCboPerfil(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim nCombo As New Combo
        nCombo.LlenarCboPerfil(combo, True)
    End Sub
    'llenar Grilla sin parametros
    Private Sub CargarDGVDescripcionPermisos(ByVal IdPerfil As Integer)
        Try
            ListaPermisos = (New Negocio.Permiso).PermisoSelectxNombrePermiso(IdPerfil)
            DGV_PerfilPermisos.DataSource = ListaPermisos
            DGV_PerfilPermisos.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        registrarPerfil_Permisos()
    End Sub

    Private Sub registrarPerfil_Permisos()

        Try
            If ((New Negocio.Perfil).InsertaPerfilPermiso(Guardar_Perfil_Permiso(), CInt(Me.cboPerfil.SelectedValue), CInt(Me.cboArea.SelectedValue))) Then
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")
            Else
                Throw New Exception("Problemas en la Operación")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function Guardar_Perfil_Permiso() As List(Of Entidades.Perfil_Permiso)

        Dim lista As New List(Of Entidades.Perfil_Permiso)

        For i As Integer = 0 To Me.DGV_PerfilPermisos.Rows.Count - 1

            If (CType(DGV_PerfilPermisos.Rows(i).FindControl("Chb_Estado"), CheckBox).Checked) Then

                Dim obj As New Entidades.Perfil_Permiso
                obj.IdPerfil = CInt(Me.cboPerfil.SelectedValue)
                obj.IdPermiso = CInt(CType(DGV_PerfilPermisos.Rows(i).FindControl("HDD_IdPermiso"), HiddenField).Value)
                obj.Estado = CType(DGV_PerfilPermisos.Rows(i).FindControl("Chb_Estado"), CheckBox).Checked

                lista.Add(obj)


            End If


        Next

        Return lista

    End Function

    Protected Sub cboArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboArea.SelectedIndexChanged
        Try
            cargarGV_Permisos(CInt(Me.cboArea.SelectedValue), CInt(Me.cboPerfil.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub cboPerfil_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboPerfil.SelectedIndexChanged
        Try
            cargarGV_Permisos(CInt(Me.cboArea.SelectedValue), CInt(Me.cboPerfil.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class
