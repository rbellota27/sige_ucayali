﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmMantTarjeta
    Inherits System.Web.UI.Page
    Private FuncCombo As New Combo
    Private objScript As New ScriptManagerClass
    Private objNegTarjeta As New Negocio.TarjetaView
    Private ListTarjeta As New List(Of Entidades.TarjetaView)
    Private p As New Entidades.TarjetaView
    Private modo As New modo_menu

    Enum modo_menu
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Insertar = 3
        Actualizar = 4
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            inicializarFRM()
            modo = modo_menu.Inicio
            HabilitarControles(modo)
            ConfigurarDatos()
        Else
            actualizar_atributos()
        End If
    End Sub

    Private Sub inicializarFRM()
        Dim objScript As New ScriptManagerClass
        Try
            cargar_recargar_datos()
            cargar_controles()
            cargar_dgv()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizar_atributos()
        Try
            ListTarjeta = CType(Session.Item("ListTarjeta"), List(Of Entidades.TarjetaView))
            modo = CType(Session.Item("modo"), modo_menu)
            p.Id = CType(Session.Item("IdRegistroFila"), Integer)
            p.Nombre = txtNombre.Text
            p.IdTipoTarjeta = CInt(Me.cboTipoTarjeta.SelectedValue)

            Select Case CInt(rbtlEstado.SelectedValue)
                Case 2
                    p.Estado = Nothing
                Case 1
                    p.Estado = True
                Case 0
                    p.Estado = False
            End Select
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub cargar_recargar_datos()
        ListTarjeta = objNegTarjeta.SelectAll()
        Session.Remove("ListTarjeta")
        Session.Add("ListTarjeta", ListTarjeta)
    End Sub
    Sub cargar_recargar_modo(ByVal x As modo_menu)
        modo = x
        Session.Remove("modo")
        Session.Add("modo", modo)
    End Sub
    Sub cargar_recargar_Id(ByVal x As Integer)  'almacena el Id del Registro a editar
        p.Id = x
        Session.Remove("IdRegistroFila")
        Session.Add("IdRegistroFila", p.Id)
    End Sub
    Private Sub cargar_controles()

        FuncCombo.LlenarCboTipoTarjeta(cboTipoTarjeta, True)
    End Sub

    Protected Sub cargar_dgv()
        actualizar_atributos()
        dgvTarjeta.DataSource = objNegTarjeta.FiltrarLista(p, ListTarjeta)
        dgvTarjeta.DataBind()
    End Sub

    Public Sub limpiarCtrl()

        Me.cboTipoTarjeta.SelectedIndex = 0
        Me.txtNombre.Text = ""
        Me.rbtlEstado.SelectedValue = "1" 'Activos

    End Sub

    'Private Sub cargar_entidad_en_controles(ByVal x As Entidades.TarjetaView)
    '    cboBanco.SelectedValue = CStr(x.IdBanco)
    '    cboTipoTarjeta.SelectedValue = CStr(x.IdTipoTarjeta)
    '    txtNombre.Text = x.Nombre
    '    rbtlEstado.SelectedValue = x.Estado
    'End Sub
    Public Sub HabilitarControles(ByVal x As modo_menu)
        Select Case x
            Case modo_menu.Inicio

                Me.btnNuevo.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscar.Visible = True

                Me.cboTipoTarjeta.Enabled = True
              
                Me.rbtlEstado.Items(0).Enabled = True

                Me.dgvTarjeta.Enabled = True

            Case modo_menu.Nuevo

                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False

                Me.cboTipoTarjeta.Enabled = True

                Me.rbtlEstado.Items(0).Enabled = False

                Me.dgvTarjeta.Enabled = False

            Case modo_menu.Editar

                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False

                Me.cboTipoTarjeta.Enabled = False

                Me.rbtlEstado.Items(0).Enabled = False

                Me.dgvTarjeta.Enabled = False

            Case modo_menu.Actualizar
            Case modo_menu.Insertar
        End Select
    End Sub

    Public Function DecodeCadena(ByVal cadena As String) As String
        Dim cadenavalida As String = ""
        cadenavalida = HttpUtility.HtmlDecode(cadena)
        Return cadenavalida
    End Function


    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        cargar_dgv()
    End Sub

    Protected Sub lbtnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            cargar_recargar_modo(modo_menu.Editar)
            Dim lbtnEditar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)

            Dim fila As GridViewRow = CType(lbtnEditar.NamingContainer, GridViewRow)

            cargar_recargar_Id(CInt(CType(fila.Cells(0).FindControl("hddId"), HiddenField).Value.Trim()))

            Me.cboTipoTarjeta.SelectedValue = CStr(CType(fila.Cells(0).FindControl("hddIdTipoTarjeta"), HiddenField).Value.Trim())
            Me.rbtlEstado.SelectedValue = CStr(IIf(CBool((CType(fila.Cells(0).FindControl("hddIdEstado"), HiddenField).Value)), 1, 0))

            Me.txtNombre.Text = DecodeCadena(fila.Cells(2).Text)

            HabilitarControles(modo)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        cargar_recargar_modo(modo_menu.Nuevo)
        HabilitarControles(modo)
        'limpiarCtrl()  'se deben dejar los datos escritos
        cargar_dgv()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        cargar_recargar_Id(0)   'Limpia el ID
        cargar_recargar_modo(modo_menu.Inicio)
        HabilitarControles(modo_menu.Inicio)
        limpiarCtrl()
        cargar_dgv()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Dim mensaje As String
        Dim exito As Boolean
        exito = True
        mensaje = ""
        'opcional ini
        cargar_recargar_datos() 'Los datos disponibles se actualizan antes de realizar la validacion
        'opcional fin

        Select Case modo
            Case modo_menu.Nuevo
                If objNegTarjeta.ValidarEntidadxNombre(p, ListTarjeta) Then
                    If objNegTarjeta.InsertaTarjeta(p) Then
                        mensaje = "Se Guardó Correctamente"
                    End If
                Else
                    exito = False
                    mensaje = "Ya existe un registro con ese Nombre de Tarjeta"
                End If
            Case modo_menu.Editar
                If objNegTarjeta.ActualizaTarjeta(p) Then
                    mensaje = "Se Actualizó Correctamente"
                End If
        End Select

        objScript.mostrarMsjAlerta(Me, mensaje)
        If exito Then
            cargar_recargar_Id(0)   'Limpia el ID
            cargar_recargar_datos()
            cargar_recargar_modo(modo_menu.Inicio)
            HabilitarControles(modo)
            cargar_dgv()    'Muestra el registro ingresado

            limpiarCtrl()   'Limpia los controles
        End If
    End Sub

    Protected Sub dgvTarjeta_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvTarjeta.RowDataBound
        'LinkButton Editar esta en un UpdatePanel y para que este no atrape su postback, se registra en el ScriptManager de la página Maestra
        Try
            If e.Row.RowType = DataControlRowType.DataRow And e.Row.RowState = DataControlRowState.Normal Or _
            e.Row.RowState = DataControlRowState.Alternate Or e.Row.RowState = DataControlRowState.Selected Then
                Dim sm As ScriptManager
                sm = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                sm.RegisterPostBackControl(DirectCast(e.Row.FindControl("lbtnEditar"), LinkButton))
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class