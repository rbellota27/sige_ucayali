<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmProveedores.aspx.vb" Inherits="APPWEB.frmProveedores" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table width="100%">
<tr>
  <td class="SubTituloCelda">
                             MANTENIMIENTO PROVEEDORES
                            </td>
</tr>
</table>
    <table width="100%">
        <tr>
            <td>
                <asp:Button Style="cursor: hand;" Width="85px" ID="btnNuevo" runat="server" Text="Nuevo" />
                <asp:Button Style="cursor: hand;" Width="85px" ID="btnGuardar" runat="server" Text="Guardar"
                    OnClientClick="return(Guardar());" />
                <asp:Button Style="cursor: hand;" Width="85px" ID="btnCancelar" runat="server" Text="Cancelar"
                    OnClientClick="return(  confirm('Desea Cancelar la Operaci�n ?')  );" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:RadioButtonList ID="rbPersona" runat="server" Font-Bold="true" class="Texto"
                    RepeatDirection="Horizontal" AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="N">NATURAL</asp:ListItem>
                    <asp:ListItem Value="J">JUR&Iacute;DICA</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlPersona" runat="server">
                    <table width="100%">
                        <tr>
                            <td class="TituloCelda">
                                DATOS GENERALES
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="Texto" style="width: 100px;">
                                            C&oacute;digo:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbcodigo" runat="server" onKeypress="return(false);" CssClass="TextBoxReadOnly_Right"
                                                onFocus="return(SalirdeEnfoque(this));"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="width: 100px;">
                                            Fecha Alta:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbfechaAlta" runat="server" MaxLength="10" CssClass="TextBox_Fecha"
                                                onKeypress="return(false);" onFocus="return(SalirdeEnfoque(this));" Width="100px"></asp:TextBox>
                                        </td>
                                        <td style="width: 100px;">
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="ckpropietario" runat="server" CssClass="Texto" Text="Propietario" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="width: 100px;">
                                            Nacionalidad:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlnacionalidad" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" style="width: 100px;">
                                            Fecha Baja:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbfechaBaja" runat="server" onFocus="javascript:habilitarControles();"
                                                MaxLength="10" Width="100px"></asp:TextBox>
                                            <cc1:CalendarExtender ID="cefechabaja" runat="server" Format="d" TargetControlID="tbfechaBaja">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td class="Texto" style="width: 100px;">
                                            Estado:
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rbestado" runat="server" class="Texto" onClick="javascript:validarEstadoPersona();"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="width: 100px;">
                                            Web Site:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbwebSite" runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                onFocus="return ( onFocusTextTransform(this,configurarDatos) );" Width="99%"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="width: 100px;">
                                            Precio Venta:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dltipoPV" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" style="width: 100px;">
                                            Motivo Baja:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlMotivoBaja" runat="server" onFocus="javascript:habilitarControles();"
                                                Font-Italic="False">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="width: 100px;">
                                            Nom. Comercial:
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="tbnombreComercial" runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                onFocus="return ( onFocusTextTransform(this,configurarDatos) ); " Width="99%"
                                                MaxLength="99"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" valign="top">
                                            Tipo de Agente:
                                        </td>
                                        <td colspan="3">
                                            <asp:DropDownList ID="dlTipoAgente" runat="server">
                                            </asp:DropDownList>
                                            <asp:Button ID="btn_AddTipoAgente" runat="server" Text="Agregar" ToolTip="[ Agregar Tipo de Agente ]"
                                                OnClientClick="return ( onclick_btn_AddTipoAgente() );" Style="cursor: hand;" />
                                            <br />
                                            <asp:GridView ID="gv_tipoAgente" runat="server" AlternatingRowStyle-CssClass="GrillaRowAlternating"
                                                AutoGenerateColumns="False" EditRowStyle-CssClass="GrillaEditRow" GridLines="None"
                                                HeaderStyle-CssClass="GrillaCabecera" RowStyle-CssClass="GrillaRow" SelectedRowStyle-CssClass="GrillaSelectedRow"
                                                Width="99%">
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True">
                                                        <ItemStyle />
                                                    </asp:CommandField>
                                                    <asp:TemplateField HeaderText="Descripci�n">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hdd_idTipoAgente" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdAgente") %>' />
                                                            <asp:Label ID="gv_lblAgente" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Descripcion") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--Rol--%>
                        <tr>
                            <td class="TituloCeldaLeft">
                                ROL
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td align="center">
                                            <asp:GridView ID="gvRol" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                                                HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow">
                                                <RowStyle CssClass="GrillaRow" />
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                    <asp:TemplateField HeaderText="Empresa">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="gvdlEmpresa" runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"objEmpresa") %>'
                                                                DataTextField="Empresa" DataValueField="IdEmpresa" AutoPostBack="true" OnSelectedIndexChanged="gvdlEmpresa_OnSelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Rol">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="gvdlRol" runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"objRol") %>'
                                                                DataTextField="Rol" DataValueField="IdRol">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Estado">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="gvckestado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>'
                                                                onClick="return(ckestadorol(this));" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Motivo Baja">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="gvdlMotivoBaja" runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"objMotivoBaja") %>'
                                                                DataTextField="Descripcion" DataValueField="Id">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <HeaderStyle CssClass="GrillaCabecera" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            </asp:GridView>
                                        </td>
                                        <td valign="top">
                                            <asp:Button ID="btnagregarrol" runat="server" Text="Agregar Rol" ToolTip="[ Agregar Rol ]"
                                                Style="cursor: hand;" />
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--Persona juridica persona natural--%>
                        <tr>
                            <td>
                                <asp:Panel ID="pnlJuridica" runat="server" ScrollBars="Auto" Width="100%">
                                    <table width="100%">
                                        <tr>
                                            <td class="TituloCeldaLeft">
                                                PERSONA JUR&Iacute;DICA
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="Texto" style="width: 125px;">
                                                            Raz&oacute;n Social:
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:TextBox ID="tbrazonSocial" runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                onFocus="return ( onFocusTextTransform(this,configurarDatos) );" Width="99%"
                                                                MaxLength="99"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" style="width: 125px;">
                                                            R.U.C.:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbrucjuridco" runat="server" MaxLength="20" onKeypress="return(validarNumeroPunto(event));"
                                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto">
                                                            Giro:
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:DropDownList ID="dlgiro" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" style="width: 125px;">
                                                            Representante Legal:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbrepresentanteLegal" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                                        </td>
                                                        <td class="Texto">
                                                            Contactos:
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnBuscarContacto" runat="server" OnClientClick="return(verCapaBuscarPersonas('1,C'));"
                                                                Style="cursor: hand;" Text="Buscar" ToolTip="[ Buscar Contacto ]" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" valign="top">
                                                            D.N.I.:
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbnrodocRepresentanteLegal" runat="server" Enabled="false"></asp:TextBox>
                                                            <asp:Button ID="btnbuscarRepresentante" runat="server" OnClientClick="return(verCapaBuscarPersonas('1,R'));"
                                                                Style="cursor: hand;" Text="Buscar" ToolTip="[ Buscar Representante Legal ]" />
                                                            <asp:Button ID="btnlimpiarRepresentanteLegal" runat="server" OnClientClick="return(limpiarRepresentante());"
                                                                Style="cursor: hand;" Text="Limpiar" ToolTip="[ Quitar Representante Legal ]" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:GridView ID="gvContacto" runat="server" AlternatingRowStyle-CssClass="GrillaRowAlternating"
                                                                AutoGenerateColumns="False" EditRowStyle-CssClass="GrillaEditRow" GridLines="None"
                                                                HeaderStyle-CssClass="GrillaCabecera" RowStyle-CssClass="GrillaRow" SelectedRowStyle-CssClass="GrillaSelectedRow"
                                                                Width="99%">
                                                                <Columns>
                                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True">
                                                                        <ItemStyle />
                                                                    </asp:CommandField>
                                                                    <asp:TemplateField HeaderText="Nombres">
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="hddidPersonaC" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdPersona") %>' />
                                                                            <asp:Label ID="gvlbContacto" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="D.N.I.">
                                                                        <ItemStyle />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlNatural" runat="server" ScrollBars="Auto" Width="100%">
                                    <table width="100%">
                                        <tr>
                                            <td class="TituloCeldaLeft">
                                                PERSONA NATURAL
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="Texto" style="width: 120px;">
                                                            Apellido Paterno:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbpaterno" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                onFocus="return ( onFocusTextTransform(this,configurarDatos) );" runat="server"
                                                                Width="99%"></asp:TextBox>
                                                        </td>
                                                        <td class="Texto" style="width: 120px;">
                                                            Apellido Materno:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbmaterno" runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                onFocus="return ( onFocusTextTransform(this,configurarDatos) );" Width="99%"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" style="width: 120px;">
                                                            Nombres:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbnombres" runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                onFocus="return ( onFocusTextTransform(this,configurarDatos) );" Width="99%"></asp:TextBox>
                                                        </td>
                                                        <td class="Texto" style="width: 120px;">
                                                            Sexo:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboSexo" runat="server" Font-Bold="true">
                                                                <asp:ListItem Value="" Selected="True">-----</asp:ListItem>
                                                                <asp:ListItem Value="M">Masculino</asp:ListItem>
                                                                <asp:ListItem Value="F">Femenino</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" style="width: 120px;">
                                                            Estado Civil:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="dlEstadoCivil" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Texto" style="width: 120px;">
                                                            Fecha Nacimiento:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFechaNac" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                                                Width="100px"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="CalendarExtender_txtFechaNac" runat="server" Enabled="True"
                                                                Format="dd/MM/yyyy" TargetControlID="txtFechaNac">
                                                            </cc1:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" style="width: 120px;">
                                                            <br />
                                                            Cargo:
                                                        </td>
                                                        <td>
                                                            <br />
                                                            <asp:DropDownList ID="dlCargo" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Texto" style="width: 120px;">
                                                            <br />
                                                            Condici&oacute;n Trabajo:
                                                        </td>
                                                        <td>
                                                            <br />
                                                            <asp:DropDownList ID="dlCondicionTrabajo" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" style="width: 120px;">
                                                            Centro Laboral:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbCentroLaboral" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                                        </td>
                                                        <td class="Texto" style="width: 120px;">
                                                            Contacto Empresa:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbEmpresaContacto" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" style="width: 120px;">
                                                            R.U.C.:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbCentroLaboralRuc" runat="server" Enabled="false"></asp:TextBox>
                                                            <asp:Button ID="btnBuscarCentroLaboral" runat="server" OnClientClick="return(verCapaBuscarPersonas('2,L'));"
                                                                Style="cursor: hand;" Text="Buscar" ToolTip="[ Buscar Centro Laboral ]" />
                                                            <asp:Button ID="btnlimpiarCentroLaboral" runat="server" OnClientClick="return(LimpiarCentroLaboral());"
                                                                Style="cursor: hand;" Text="Limpiar" ToolTip="[ Quitar Centro Laboral ]" />
                                                        </td>
                                                        <td class="Texto" style="width: 120px;">
                                                            R.U.C.:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbRucEmpresa" runat="server" Enabled="false"></asp:TextBox>
                                                            <asp:Button ID="btnbuscarEmpresa" runat="server" OnClientClick="return(verCapaBuscarPersonas('2,E'));"
                                                                Style="cursor: hand;" Text="Buscar" ToolTip="[ Buscar Empresa ]" />
                                                            <asp:Button ID="btnlimpiarContactoEmpresa" runat="server" OnClientClick="return(LimpiarEmpresa());"
                                                                Style="cursor: hand;" Text="Limpiar" ToolTip="[ Quitar Contacto Empresa ]" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%--Empleado--%>
                                        <tr runat="server" id="tr_empleado">
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="TituloCeldaLeft">
                                                            empleado
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 100px;">
                                                                        <asp:CheckBox ID="ckEmpleado" runat="server" CssClass="Texto" onClick="return(esEmpleado());"
                                                                            Text="Registrar" />
                                                                    </td>
                                                                    <td class="Texto" style="width: 100px;">
                                                                        Categor&iacute;a:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="dlEmpCategoria" onFocus="return (ValidarEmpleado());" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td class="Texto" style="width: 100px;">
                                                                        Fecha Alta:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbEmpFechaAlta" runat="server" CssClass="TextBox_Fecha" Width="100px"
                                                                            onkeyDown="return(false);"></asp:TextBox>
                                                                    </td>
                                                                    <td class="Texto" style="width: 100px;">
                                                                        Estado:
                                                                    </td>
                                                                    <td>
                                                                        <asp:RadioButtonList ID="rbEmpEstado" runat="server" class="Texto" onClick="return (ValidarEmpleado());"
                                                                            RepeatDirection="Horizontal">
                                                                            <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                                                            <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Motivo Baja:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="dlEmpMotivoBaja" runat="server" onFocus="return (ValidarEstadoEmp());">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Fecha Baja:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbEmpFechaBaja" runat="server" onFocus="return (ValidarEstadoEmp());"
                                                                            CssClass="TextBox_Fecha" Width="100px"></asp:TextBox>
                                                                        <cc1:CalendarExtender ID="ceFechaEmp" runat="server" Format="d" TargetControlID="tbEmpFechaBaja">
                                                                        </cc1:CalendarExtender>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="tr_chofer">
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="TituloCeldaLeft">
                                                            chofer
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="width: 100px;">
                                                                        <asp:CheckBox ID="ckChofer" CssClass="Texto" Text="Registrar" runat="server" onClick="return( ValidarChofer());" />
                                                                    </td>
                                                                    <td class="Texto" style="width: 100px;">
                                                                        Nro Licencia:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbnroLicencia" runat="server" onKeyPress="return(esChofer());"></asp:TextBox>
                                                                    </td>
                                                                    <td class="Texto" style="width: 100px;">
                                                                        Categor&iacute;a:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbcategoria" runat="server" onKeyPress="return(esChofer());"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="TituloCeldaLeft">
                                                DOCUMENTOS DE IDENTIDAD
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvTipoDocumento" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                                                                HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow">
                                                                <Columns>
                                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                                    <asp:TemplateField HeaderText="Tipo Documento">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="gvdltipoDoc" runat="server" DataTextField="Abv" DataValueField="Id"
                                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"objDocumento") %>'>
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Nro Documento">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="gvtbnroDoc" runat="server" MaxLength="20" onfocus="return(aceptarFoco(this));"
                                                                                onKeypress="return(validarNumeroPunto(event));" Text='<%# DataBinder.Eval(Container.DataItem,"Numero") %>'></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:Button ID="btnagregarTipoDoc" runat="server" Text="Agregar" ToolTip="[ Agregar Documento ]"
                                                                Style="cursor: hand;" />
                                                            <span class="Texto">Documento</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="TituloCeldaLeft">
                                                PROFESI&Oacute;N Y/O OCUPACI&Oacute;N
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Panel ID="pnlProfesion" runat="server" ScrollBars="Auto" Width="950px">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="width: 50%;">
                                                                <asp:Button ID="btnagregarProfesion" runat="server" Text="Agregar" ToolTip="[ Agregar Profesi�n ]"
                                                                    Style="cursor: hand;" />
                                                                <span class="Texto">Profesi&oacute;n</span>
                                                            </td>
                                                            <td style="width: 50%;">
                                                                <asp:Button ID="btnagregarOcupacion" runat="server" Text="Agregar" ToolTip="[ Agregar Ocupaci�n ]"
                                                                    Style="cursor: hand;" />
                                                                <span class="Texto">Ocupaci&oacute;n</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <asp:GridView ID="gvProfesion" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                    AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                                                                    HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow">
                                                                    <Columns>
                                                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                                        <asp:TemplateField HeaderText="Profesi�n">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="gvdlProfesion" runat="server" DataValueField="Id" DataTextField="Nombre"
                                                                                    DataSource='<%# DataBinder.Eval(Container.DataItem,"objProfesion") %>'>
                                                                                </asp:DropDownList>
                                                                            </ItemTemplate>
                                                                            <ItemStyle />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Principal">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="gvckPrincipal" runat="server" onClick="return(validarPrioridadProfesion(this));" />
                                                                            </ItemTemplate>
                                                                            <ItemStyle />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                            <td valign="top">
                                                                <asp:GridView ID="gvOcupacion" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                    AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                                                                    HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow">
                                                                    <Columns>
                                                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                                        <asp:TemplateField HeaderText="Ocupaci�n">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="gvdlOcupacion" runat="server" DataValueField="Id" DataTextField="Nombre"
                                                                                    DataSource='<%# DataBinder.Eval(Container.DataItem,"objOcupacion") %>'>
                                                                                </asp:DropDownList>
                                                                            </ItemTemplate>
                                                                            <ItemStyle />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Principal">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="gvckPrincipal" runat="server" onClick="return(validarPrioridadOcupacion(this));" />
                                                                            </ItemTemplate>
                                                                            <ItemStyle />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Giro">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="gvdlGiro" runat="server" DataValueField="Id" DataTextField="Descripcion"
                                                                                    DataSource='<%# DataBinder.Eval(Container.DataItem,"objGiro") %>'>
                                                                                </asp:DropDownList>
                                                                            </ItemTemplate>
                                                                            <ItemStyle />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td class="TituloCeldaLeft">
                                            TEL&Eacute;FONO Y CORREO
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Panel ID="pnlTelefono" runat="server" ScrollBars="Auto" Width="100%">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="width: 50%;">
                                                            <asp:Button ID="btnagregarTelefono" runat="server" Text="Agregar" ToolTip="[ Agregar Tel�fono ]"
                                                                Style="cursor: hand;" />
                                                            <span class="Texto">T&eacute;lefono</span>
                                                        </td>
                                                        <td style="width: 50%;">
                                                            <asp:Button ID="btnagregarCorreo" runat="server" Text="Agregar" ToolTip="[ Agregar Correo ]"
                                                                Style="cursor: hand;" />
                                                            <span class="Texto">Correo</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <asp:GridView ID="gvTelefono" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                                                                HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow">
                                                                <Columns>
                                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                                    <asp:TemplateField HeaderText="Tipo Telefono">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="gvdlTipoTelefono" runat="server" DataValueField="Id" DataTextField="Nombre"
                                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"objTelefono") %>'>
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Nro Telefono">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="gvtbNroTelefono" runat="server" MaxLength="15" onfocus="return(aceptarFoco(this));"
                                                                                Text='<%# DataBinder.Eval(Container.DataItem,"Numero") %>'></asp:TextBox></ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Anexo">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="gvtbAnexo" runat="server" MaxLength="10" Width="50px" onKeypress="return(validarNumeroPunto(event));"
                                                                                Text='<%# DataBinder.Eval(Container.DataItem,"Anexo") %>'></asp:TextBox></ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Prioridad">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="gvckPrioridad" OnClick="return(validarPrioridadTelefono(this));"
                                                                                runat="server" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:GridView ID="gvCorreo" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                                AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                                                                HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow">
                                                                <Columns>
                                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                                    <asp:TemplateField HeaderText="Tipo Correo">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="gvdlTipoCorreo" runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"objCorreo") %>'
                                                                                DataTextField="Nombre" DataValueField="Id">
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Correo">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="gvtbCorreo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'
                                                                                MaxLength="50" Width="350px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                                onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox></ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td class="TituloCeldaLeft">
                                            DIRECCI&Oacute;N
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="LabelTdLeft" style="width: 25%">
                                                                    Domicilio:
                                                                </td>
                                                                <td class="LabelTdLeft" style="width: 25%">
                                                                    Departamento:
                                                                </td>
                                                                <td class="LabelTdLeft" style="width: 25%">
                                                                    Provincia:
                                                                </td>
                                                                <td class="LabelTdLeft" style="width: 25%">
                                                                    Distrito:
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="dlTipoDireccion" runat="server" Width="99%">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="dlDepartamento" runat="server" AutoPostBack="True" Width="99%">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="dlProvincia" runat="server" AutoPostBack="True" Width="99%">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="dlDistrito" runat="server" Width="99%">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" align="left">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="LabelTdLeft">
                                                                                Tipo V&iacute;a:
                                                                            </td>
                                                                            <td class="LabelTdLeft">
                                                                                V&iacute;a:
                                                                            </td>
                                                                            <td class="LabelTdLeft">
                                                                                N� Exterior:
                                                                            </td>
                                                                            <td class="LabelTdLeft">
                                                                                N� Interior:
                                                                            </td>
                                                                            <td class="LabelTdLeft">
                                                                                Zonificaci&oacute;n:
                                                                            </td>
                                                                            <td class="LabelTdLeft">
                                                                                Zona:
                                                                            </td>
                                                                            <td class="LabelTdLeft">
                                                                                Etapa:
                                                                            </td>
                                                                            <td class="LabelTdLeft">
                                                                                Mz:
                                                                            </td>
                                                                            <td class="LabelTdLeft">
                                                                                Lote:
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:DropDownList ID="dlTipoVia" runat="server">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbVia" runat="server" MaxLength="35" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                                    onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbnroExterior" runat="server" MaxLength="6" Width="70px"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbnroInterior" runat="server" MaxLength="6" Width="70px"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="dlZonificacion" runat="server">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbzona" runat="server" MaxLength="50" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                                    onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbEtapa" runat="server" MaxLength="2" Width="40px"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbManzana" runat="server" MaxLength="3" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                                    onFocus="return ( onFocusTextTransform(this,configurarDatos) );" Width="40px"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbLote" runat="server" MaxLength="3" Width="40px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="Texto" style="width: 100px">
                                                                    Direcci&oacute;n:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbDireccion" runat="server" MaxLength="120" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                        onFocus="return ( onFocusTextTransform(this,configurarDatos) );" Width="600px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="Texto" style="width: 100px">
                                                                    Referencia:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbreferencia" runat="server" MaxLength="100" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                        onFocus="return ( onFocusTextTransform(this,configurarDatos) );" Width="600px"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:Button ID="btnagregarDireccion" runat="server" OnClientClick="return(validarDireccion());"
                                                                        Style="cursor: hand;" Text="Agregar" ToolTip="[ Agregar Direccion ]" />
                                                                </td>
                                                                <td class="Texto">
                                                                    &nbsp;Direcci&oacute;n
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:GridView ID="gvDireccion" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                            AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                                                            HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow"
                                                            Width="99%">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lbQuitar" runat="server" OnClick="lbQuitar_Click">Quitar</asp:LinkButton></ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="TipoDireccion" HeaderText="Tipo Direcci�n" />
                                                                <asp:TemplateField HeaderText="Direcci�n">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hddIdTipoDireccion" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoDireccion") %>' />
                                                                        <asp:Label ID="gvlbDireccion" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Direccion") %>'></asp:Label><asp:HiddenField
                                                                            ID="hddIdViaTipo" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdViaTipo") %>' />
                                                                        <asp:HiddenField ID="hddidzonatipo" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdZonaTipo") %>' />
                                                                        <asp:HiddenField ID="hddetapa" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Etapa") %>' />
                                                                        <asp:HiddenField ID="hddinterior" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Interior") %>' />
                                                                        <asp:HiddenField ID="hddlt" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Lt") %>' />
                                                                        <asp:HiddenField ID="hddmz" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Mz") %>' />
                                                                        <asp:HiddenField ID="hddubigeo" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Ubigeo") %>' />
                                                                        <asp:HiddenField ID="hddvianumero" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"ViaNumero") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="ZonaNombre" HeaderText="Zona" />
                                                                <asp:BoundField DataField="ViaNombre" HeaderText="V�a" />
                                                                <asp:BoundField DataField="Referencia" HeaderText="Referencia" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lbEditar" runat="server" OnClick="lbEditar_Click">Editar</asp:LinkButton></ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlBusquedaPersona" runat="server">
        <table width="100%">
            <tr>
                <td class="TituloCeldaLeft">
                    BUSQUEDA DE PERSONAS
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="Texto">
                                Raz�n Social / Nombres:
                            </td>
                            <td colspan="3">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscar">                                
                                <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                    Width="100%" onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbbuscarEstado" runat="server" CssClass="Texto" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                    <asp:ListItem Value="2">Todos</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                D.N.I.:
                            </td>
                            <td align="left">
                                <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscar">                                
                                <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                    MaxLength="8" runat="server"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td class="Texto">
                                R.U.C.:
                            </td>
                            <td align="left">
                                <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscar">                                
                                <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                    MaxLength="11"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td>
                                <asp:Button Style="cursor: hand;" ID="btnBuscar" runat="server" Text="Buscar" Width="85px"
                                    OnClientClick="return ( onclick_btnBuscar() ); " />
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Rol:
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="dlRol" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Nacionalidad:
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlNacionalidadBuscar" runat="server">
                                    <asp:ListItem Value="0" Text="------"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="NACIONAL"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="EXTRANJERO"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvBuscar" runat="server" AutoGenerateColumns="False" GridLines="None"
                        AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                        HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow"
                        Width="99%" PageSize="20">
                        <RowStyle CssClass="GrillaRow" />
                        <Columns>
                            <asp:CommandField ButtonType="Link" SelectText="Editar" ShowSelectButton="true" />
                            <asp:BoundField DataField="idpersona" HeaderText="C�d." NullDisplayText="0" />
                            <asp:BoundField DataField="Nombre" HeaderText="Raz�n Social / Nombres" NullDisplayText="---">
                                <ItemStyle Width="500px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C" NullDisplayText="---" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I" NullDisplayText="---" />
                            <asp:BoundField DataField="Nacionalidad" HeaderText="Nacionalidad" />
                            <asp:BoundField DataField="Estado" HeaderText="Estado" />
                        </Columns>
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button Style="cursor: hand;" ID="btAnterior" runat="server" Width="50px" Text="<"
                        ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                    <asp:Button Style="cursor: hand;" ID="btSiguiente" runat="server" Width="50px" Text=">"
                        ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                    <asp:TextBox ID="tbPageIndex" Width="50px" CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox><asp:Button
                        Style="cursor: hand;" ID="btIr" runat="server" Width="50px" Text="Ir" ToolTip="Ir a la P�gina"
                        OnClientClick="return(valNavegacion('2'));" />
                    <asp:TextBox ID="tbPageIndexGO" Width="50px" runat="server" CssClass="TextBoxReadOnly"
                        onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdPersona" runat="server" Value="0" />
                <asp:HiddenField ID="hddidpersonaR" runat="server" Value="0" />
                <asp:HiddenField ID="hddidCentroLaboral" runat="server" Value="0" />
                <asp:HiddenField ID="hddidPersonaE" runat="server" Value="0" />
                <asp:HiddenField ID="hddChoferReg" runat="server" Value="0" />
                <asp:HiddenField ID="hddEsempleado" runat="server" Value="-1" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hddoperativo" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_idrolurl" runat="server" Value="" />
                <asp:HiddenField ID="hddIdTipoPv" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <div id="verCapaPersona" style="border: 3px solid blue; padding: 4px; width: 850px;
        height: auto; position: absolute; top: 239px; left: 35px; background-color: white;
        z-index: 2; display: none;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button Style="cursor: hand;" ID="btcerrarPersona" runat="server" Text="Cerrar"
                        OnClientClick="return(offCapa('verCapaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Filtro:
                            </td>
                            <td>
                                <asp:DropDownList ID="cbbuscarpersona" Width="150px" runat="server" CssClass="Texto">
                                    <asp:ListItem Selected="True" Value="0">Nombre</asp:ListItem>
                                    <asp:ListItem Value="1">D.N.I.</asp:ListItem>
                                    <asp:ListItem Value="2">R.U.C.</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Texto:
                            </td>
                            <td>
                                <asp:Panel ID="Panel4" runat="server" DefaultButton="btnBuscarPersona">
                                <asp:TextBox ID="tbbuscarPersona" Width="300px" runat="server"></asp:TextBox>
                                <asp:Button Style="cursor: hand;" ID="btnBuscarPersona" runat="server" Text="Buscar" />
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvBuscarPersona" runat="server" AutoGenerateColumns="False" GridLines="None"
                        AlternatingRowStyle-CssClass="GrillaRowAlternating" SelectedRowStyle-CssClass="GrillaSelectedRow"
                        HeaderStyle-CssClass="GrillaCabecera" EditRowStyle-CssClass="GrillaEditRow" RowStyle-CssClass="GrillaRow"
                        Width="99%" PageSize="20">
                        <Columns>
                            <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                            <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                            <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                <ItemStyle Width="500px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                <ItemStyle />
                            </asp:BoundField>
                            <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                <ItemStyle />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button Style="cursor: hand;" ID="btAnterior_Persona" runat="server" Width="50px"
                        Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionPersona('0'));" />
                    <asp:Button Style="cursor: hand;" ID="btSiguiente_Persona" runat="server" Width="50px"
                        Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionPersona('1'));" />
                    <asp:TextBox ID="tbPageIndex_Persona" Width="50px" CssClass="TextBoxReadOnly_Right"
                        runat="server"></asp:TextBox><asp:Button Style="cursor: hand;" ID="btIr_Persona"
                            runat="server" Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionPersona('2'));" />
                    <asp:TextBox ID="tbPageIndexGO_Persona" Width="50px" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox><asp:HiddenField
                        ID="hddtipoBusqueda" runat="server" Value="0" />
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">

        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        function valNavegacionPersona(tipoMov) {

            tipoMov = tipoMov.substring(0, 1);

            var index = parseInt(document.getElementById('<%=tbPageIndex_Persona.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbbuscarpersona.ClientID%>').select();
                document.getElementById('<%=tbbuscarpersona.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO_Persona.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        function verCapaBuscarPersonas(valor) {
            document.getElementById('<%=hddtipoBusqueda.ClientID %>').value = valor;
            onCapa('verCapaPersona');
            return false;
        }

        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function valBlur(event) {
            var id = event.srcElement.id;
            var caja = document.getElementById(id);
            if (CajaEnBlanco(caja)) {
                caja.focus();
                alert('Debe ingresar un valor.');
                return false;
            }
            return true;
        }
        function aceptarFoco(caja) {
            caja.select();
            caja.focus();
            return true;
        }
        function validarNumeroPunto(event) {
            var key = event.keyCode;
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++   
        function Guardar() {

            var grillaDocumento = document.getElementById('<%=gvTipoDocumento.ClientID %>');
            if (grillaDocumento != null) {
                for (var i = 1; i < grillaDocumento.rows.length; i++) {
                    var rowElem = grillaDocumento.rows[i];

                    var cantidad = redondear(parseFloat(rowElem.cells[2].children[0].value), 2);
                    if (isNaN(cantidad)) {
                        cantidad = 0;
                    }
                    if (cantidad == 0) {
                        alert('Ingrese el Nro de documento ' + rowElem.cells[1].children[0].options[rowElem.cells[1].children[0].selectedIndex].text + '.');
                        return false;
                    }

                    if ((rowElem.cells[2].children[0].value.length < 8 || rowElem.cells[2].children[0].value.length > 8) && rowElem.cells[1].children[0].options[rowElem.cells[1].children[0].selectedIndex].text == 'DNI') {
                        alert("El numero de DNI debe tener una Longuitud de 8 caracteres");
                        return false;
                    }

                    if ((rowElem.cells[2].children[0].value.length < 11 || rowElem.cells[2].children[0].value.length > 11) && rowElem.cells[1].children[0].options[rowElem.cells[1].children[0].selectedIndex].text == 'RUC') {
                        alert("El numero de RUC debe tener una Longuitud de 11 caracteres");
                        return false;
                    }


                    if (rowElem.cells[1].children[0].options[rowElem.cells[1].children[0].selectedIndex].text == 'RUC') {
                        var valido = false;
                        if (rowElem.cells[2].children[0].value.substring(0, 1) == '1') { valido = true; }
                        if (valido == false) {
                            alert("verifique el numero de ruc");
                            return false;
                        }
                    }

                    var cont = 0;
                    for (var r = 1; r < grillaDocumento.rows.length; r++) {
                        var row = grillaDocumento.rows[r];
                        if (rowElem.cells[1].children[0].value == row.cells[1].children[0].value) {
                            cont = cont + 1;
                        }
                        if (cont > 1) {
                            alert("Ha seleccionado 2 veces el mismo tipo de Documento");
                            return false;
                        }
                    }
                }
            }
            var empleado = document.getElementById('<%=ckEmpleado.ClientID %>');

            var grillaRol = document.getElementById('<%=gvRol.ClientID %>');

            if (grillaRol != null) {
                for (var i = 1; i < grillaRol.rows.length; i++) {
                    var rowElem = grillaRol.rows[i];
                    var empresa = rowElem.cells[1].children[0].value;
                    if (empresa == 0) {
                        alert('Seleccione una empresa en la seccion de Rol.');
                        return false;
                    }
                                        
                    var rol = rowElem.cells[2].children[0].value;
                    if (isNaN(rol)) {
                        rol = 0;
                    }
                    
                    if (rol == 0) {
                        alert('Seleccione una empresa ' + rowElem.cells[1].children[0].options[rowElem.cells[1].children[0].selectedIndex].text + '.');
                        return false;
                    }
                }
            }
            else {
                var ckpropietario = document.getElementById('<%=ckpropietario.ClientID %>');
                if (ckpropietario.checked == false) {
                    if (grillaRol == null && empleado.checked == false) {
                        alert("Debe ingresar uno o mas roles para la persona");
                        return false;
                    }
                }
            }

            var grillaCorreo = document.getElementById('<%=gvCorreo.ClientID %>');
            if (grillaCorreo != null) {
                for (var i = 1; i < grillaCorreo.rows.length; i++) {
                    var rowElem = grillaCorreo.rows[i];
                    var cont = 0;
                    for (var r = 1; r < grillaCorreo.rows.length; r++) {
                        var row = grillaCorreo.rows[r];
                        if (rowElem.cells[1].children[0].value == row.cells[1].children[0].value) {
                            cont = cont + 1;
                        }
                        if (cont > 1) {
                            alert("Ha seleccionado 2 veces el mismo tipo de correo");
                            return false;
                        }
                    }
                }
            }

            var grillaOcupacion = document.getElementById('<%=gvOcupacion.ClientID %>');
            if (grillaOcupacion != null) {
                for (var i = 1; i < grillaOcupacion.rows.length; i++) {
                    var rowElem = grillaOcupacion.rows[i];
                    var cont = 0;
                    for (var r = 1; r < grillaOcupacion.rows.length; r++) {
                        var row = grillaOcupacion.rows[r];
                        if (rowElem.cells[1].children[0].value == row.cells[1].children[0].value) {
                            cont = cont + 1;
                        }
                        if (cont > 1) {
                            alert("Ha seleccionado 2 veces el mismo tipo de ocupacion");
                            return false;
                        }
                    }
                }
            }

            var grillaTelefono = document.getElementById('<%=gvTelefono.ClientID %>');
            if (grillaTelefono != null) {
                for (var i = 1; i < grillaTelefono.rows.length; i++) {
                    var rowElem = grillaTelefono.rows[i];

                    var cantidad = redondear(parseFloat(rowElem.cells[2].children[0].value), 2);
                    if (isNaN(cantidad)) {
                        cantidad = 0;
                    }
                    if (cantidad == 0) {
                        alert('Ingrese el Nro de telefono ' + rowElem.cells[1].children[0].options[rowElem.cells[1].children[0].selectedIndex].text + '.');
                        return false;
                    }

                    var cont = 0;
                    for (var r = 1; r < grillaTelefono.rows.length; r++) {
                        var row = grillaTelefono.rows[r];
                        if (rowElem.cells[1].children[0].value == row.cells[1].children[0].value) {
                            cont = cont + 1;
                        }
                        if (cont > 1) {
                            alert("Ha seleccionado 2 veces el mismo tipo de telefono");
                            return false;
                        }
                    }
                }
            }

            var grillaProfesion = document.getElementById('<%=gvProfesion.ClientID %>');
            if (grillaProfesion != null) {
                for (var i = 1; i < grillaProfesion.rows.length; i++) {
                    var rowElem = grillaProfesion.rows[i];
                    var cont = 0;
                    for (var r = 1; r < grillaProfesion.rows.length; r++) {
                        var row = grillaProfesion.rows[r];
                        if (rowElem.cells[1].children[0].value == row.cells[1].children[0].value) {
                            cont = cont + 1;
                        }
                        if (cont > 1) {
                            alert("Ha seleccionado 2 veces el mismo tipo de profesion");
                            return false;
                        }
                    }
                }
            }

            var radioPersona = document.getElementById('<%=rbPersona.ClientID %>');
            var controlPersona = radioPersona.getElementsByTagName('input');
            var radioEstado = document.getElementById('<%=rbestado.ClientID %>');
            var controlEstado = radioEstado.getElementsByTagName('input');
            var cbnacionalidad = document.getElementById('<%=dlNacionalidad.ClientID %>');

            if (controlPersona[0].checked == true && cbnacionalidad.selectedIndex == 0) { //Natural
                var paterno = document.getElementById('<%=tbpaterno.ClientID %>');
                if (paterno.value == '') {
                    alert("Debe ingresar el apellido paterno de la persona natural");
                    return false;
                }
                var nombre = document.getElementById('<%=tbnombres.ClientID %>');
                if (nombre.value == '') {
                    alert("Debe ingresar el nombre de la persona natural");
                    return false;
                }
            }
            if (controlPersona[0].checked == true) { //Natural
                var paterno = document.getElementById('<%=tbpaterno.ClientID %>');
                if (paterno.value == '') {
                    alert("Debe ingresar el apellido paterno de la persona natural");
                    return false;
                }
                var nombre = document.getElementById('<%=tbnombres.ClientID %>');
                if (nombre.value == '') {
                    alert("Debe ingresar el nombre de la persona natural");
                    return false;
                }
            }
            if (controlPersona[1].checked == true && cbnacionalidad.selectedIndex == 0) { //juridica
                var ruc = document.getElementById('<%=tbrucjuridco.ClientID %>');


                if (ruc.value == '') {
                    alert("Debe ingresar el numero ruc de la persona juridica");
                    return false;
                }

                if (ruc.value.length < 11 || ruc.value.length > 11) {
                    alert("El numero ruc de la persona juridica debe tener 11 digitos");
                    return false;
                }

                if (ruc.value.substring(0, 1) != '2') {
                    alert("Verifique el numero de ruc");
                    return false;
                }

                var razonsocial = document.getElementById('<%=tbrazonSocial.ClientID %>');
                if (razonsocial.value == '') {
                    alert("Debe ingresar la razon social de la persona juridica");
                    return false;
                }
            }
            if (controlPersona[1].checked == true) { //juridica
                var razonsocial = document.getElementById('<%=tbrazonSocial.ClientID %>');
                if (razonsocial.value == '') {
                    alert("Debe ingresar la razon social de la persona juridica");
                    return false;
                }
            }
            if (controlEstado[1].checked == true) { //Inactivo
                var idpersona = document.getElementById('<%=hddIdPersona.ClientId %>');
                if (idpersona.value == '0') {
                    alert('El estado no puede ser inactivo cuando registra a una nueva persona');
                    return false;
                }
                var motivo = document.getElementById('<%=dlMotivoBaja.ClientID %>');
                if (motivo.value == 0) {
                    alert('Debe seleccionar un motivo de baja de la persona');
                    return false;
                }
                var fechabaja = document.getElementById('<%=tbfechaBaja.ClientID %>');
                if (valFecha(fechabaja) == false) {
                    fechabaja.select();
                    fechabaja.focus();
                    return false;
                }
            }

            return confirm("Desea continuar con la operaci�n ?");
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++
        function ckestadorol(valor) {
            var grilla = document.getElementById('<%=gvRol.ClientID %>');
            for (var i = 1; i < grilla.rows.length; i++) {
                var row = grilla.rows[i];
                if (row.cells[3].children[0].id == valor.id) {

                    row.cells[4].children[0].disabled = row.cells[3].children[0].checked;
                }
            }
            return true;
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++++++
        function validarDireccion() {
            var cboDireccion = document.getElementById('<%=dlTipoDireccion.ClientID %>');
            if (cboDireccion.value == '0') {
                alert("Debe seleccionar un tipo de domicilio");
                return false;
            }
            var cboTipoVia = document.getElementById('<%=dlTipoVia.ClientID %>');
            if (cboTipoVia.value == '0') {
                alert("Debe seleccionar el tipo de via");
                return false;
            }
            var txtVia = document.getElementById('<%=tbVia.ClientID %>');
            if (txtVia.value == '') {
                alert("Debe ingresar el nombre de la via");
                return false;
            }

            var grillaDireccion = document.getElementById('<%=gvDireccion.ClientID %>');
            if (grillaDireccion != null) {
                for (var i = 1; i < grillaDireccion.rows.length; i++) {
                    var rowElem = grillaDireccion.rows[i];
                    if (rowElem.cells[2].children[0].value == cboDireccion.value) {
                        alert("No puede agregar el mismo tipo de direccion " + ".");
                        return false;
                    }
                }
            }
            return true;
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function SalirdeEnfoque(valor) {
            var cbo = document.getElementById('<%=dlNacionalidad.ClientID %>');
            cbo.focus();
            return true;
        }
        //+++++++++++++++++++++++++++++
        function habilitarControles() {
            var radio = document.getElementById('<%=rbestado.ClientID %>');
            var cbo = document.getElementById('<%=dlNacionalidad.ClientID %>');
            var inputs = radio.getElementsByTagName("input");
            if (inputs[0].checked == true) {
                alert("El estado es activo");
                cbo.focus();
                return false;
            }
            return true;
        }
        //++++++++++++++++++++++++++++
        function validarPrioridadTelefono(obj) {
            var grilla = document.getElementById('<%=gvTelefono.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[4].children[0].id != obj.id) {
                        rowElem.cells[4].children[0].status = false;
                    }
                }
            }
            return true;
        }
        function validarPrioridadOcupacion(obj) {
            var grilla = document.getElementById('<%=gvOcupacion.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[2].children[0].id != obj.id) {
                        rowElem.cells[2].children[0].status = false;
                    }
                }
            }
            return true;
        }
        function validarPrioridadProfesion(obj) {
            var grilla = document.getElementById('<%=gvProfesion.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[2].children[0].id != obj.id) {
                        rowElem.cells[2].children[0].status = false;
                    }
                }
            }
            return true;
        }
        //++++++++++++++++++++++++++++++++++++
        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rbPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        //+++++++++++++++++++++++++++++++++++
        function validarCajaBusqueda(obj) {
            var key = event.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btnBuscar.ClientID %>');
                boton.focus();
                return true;
            }
        }
        //+++++++++++++++++++++++++++++++++++
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btnBuscar.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        //+++++++++++++++++++++++++++++++++++
        function esEmpleado() {
            var esEmp = document.getElementById('<%=hddEsempleado.ClientID %>');
            var empleado = document.getElementById('<%=ckEmpleado.ClientID %>');
            if (esEmp.value >= 0 && empleado.checked == false) {
                return false;
            }
            return true;
        }
        //+++++++++++++++++++++++++++++++++++++
        function ValidarEmpleado() {
            var esEmp = document.getElementById('<%=hddEsempleado.ClientID %>');
            var empleado = document.getElementById('<%=ckEmpleado.ClientID %>');
            var radio = document.getElementById('<%=rbEmpEstado.ClientID %>');
            var inputs = radio.getElementsByTagName("input");
            if (empleado.checked == false && esEmp.value == '-1') {
                alert("Debe habilitar el registro de empleado");
                inputs[0].checked = true;
                empleado.focus();
                return false;
            }
            return true;
        }

        //+++++++++++++++++++++++++++++++++++++++
        function ValidarEstadoEmp() {
            var radio = document.getElementById('<%=rbEmpEstado.ClientID %>');
            var inputs = radio.getElementsByTagName("input");
            //for (var i = 0; i < inputs.length; i++) {
            if (inputs[0].checked == true) {
                alert("El estado es Activo");
                inputs[0].focus();
                return false;
            }
            var esEmp = document.getElementById('<%=hddEsempleado.ClientID %>');
            if (inputs[0].checked == false && esEmp.value == '-1') {
                alert("Va a registrar a un nuevo empleado no puede ser inactivo el estado");
                inputs[0].checked = true;
                inputs[0].focus();
                return false;
            }
            //}            
            return true;
        }

        //+++++++++++++++++++++++++++++++++++++++
        function esChofer() {
            var chofer = document.getElementById('<%=ckChofer.ClientID %>');
            if (chofer.checked == false) {
                return false;
            }
        }

        //+++++++++++++++++++++++++++++++++++++++
        function ValidarChofer() {
            var esChofer = document.getElementById('<%=hddChoferReg.ClientID %>');
            if (esChofer.value > 0) {
                alert("Esta persona como chofer, tiene registros relacionados");
                return false;
            }
        }
        //++++++++++++++++++++++++++++++++++++++++++
        function validarEstadoPersona() {
            var radio = document.getElementById('<%=rbestado.ClientID %>');
            var control = radio.getElementsByTagName('input');
            var txtfecha = document.getElementById('<%=tbfechaBaja.ClientID%>');
            var motivo = document.getElementById('<%=dlMotivoBaja.ClientID %>');
            var fecha = new Date();
            var fechaCompleta = fecha.format("dd/MM/yyyy");
            if (control[0].checked == true) {
                txtfecha.value = "";
                motivo.selectedIndex = 0;
                return false;
            }
            if (control[1].checked == true) {
                txtfecha.value = fechaCompleta;
                return false;
            }
            return true;
        }
        ///////////////////////////////////
        function LimpiarCentroLaboral() {
            document.getElementById('<%=tbCentroLaboralRuc.ClientID %>').value = '';
            document.getElementById('<%=tbCentroLaboral.ClientID %>').value = '';
            document.getElementById('<%=hddidCentroLaboral.ClientID %>').value = '0';
            return false;
        }
        //////////////////////////////////////
        function LimpiarEmpresa() {
            document.getElementById('<%=tbRucEmpresa.ClientID %>').value = '';
            document.getElementById('<%=tbEmpresaContacto.ClientID %>').value = '';
            document.getElementById('<%=hddidPersonaE.ClientID %>').value = '0';
            return false;
        }
        function limpiarRepresentante() {
            document.getElementById('<%=tbnrodocRepresentanteLegal.ClientID %>').value = '';
            document.getElementById('<%=tbrepresentanteLegal.ClientID %>').value = '';
            document.getElementById('<%=hddidpersonaR.ClientID %>').value = '0';
            return false;
        }

        function onclick_btn_AddTipoAgente() {
            var dlTipoAgente = document.getElementById('<%=dlTipoAgente.ClientID %>');
            if (dlTipoAgente.value == '0') {
                return false;
            }
            var gv_TipoAgente = document.getElementById('<%=gv_TipoAgente.ClientID %>');
            var cont = 0;
            if (gv_TipoAgente != null) {
                for (var i = 1; i < gv_TipoAgente.rows.length; i++) {
                    var rowElem = gv_TipoAgente.rows[i];
                    if (dlTipoAgente.value == rowElem.cells[1].children[0].value) {
                        alert('EL ELEMENTO ' + dlTipoAgente.options[dlTipoAgente.selectedIndex].text + ' HA SIDO INGRESADO');
                        return false;
                    }
                }
            }
            return true;
        }
        //
        function onclick_btnBuscar() {
            var hdd_idrolurl = document.getElementById('<%=hdd_idrolurl.ClientID %>');
            var dlRol = document.getElementById('<%=dlRol.ClientID %>');
            var existe = 0;
            if (hdd_idrolurl.value != '') {
                var lista = hdd_idrolurl.value.split(',');
                for (var i = 0; i < lista.length; i++) {
                    if (dlRol.value == lista[i]) {
                        existe = 1;
                    }
                }
                if (existe == 0) {
                    alert('NO PUEDE HACER BUSQUEDAS POR EL ROL ' + dlRol.options[dlRol.selectedIndex].text + '.');
                    return false;
                }
            }
            return true;
        }


        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
        
        
    </script>

</asp:Content>

