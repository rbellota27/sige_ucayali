<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmMantCuentaBancaria.aspx.vb" Inherits="APPWEB.FrmMantCuentaBancaria" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table  width ="100%">
    <tr>
        <td>
            
            <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" Width="80px" runat="server" Text="Nuevo" ToolTip="Generar un Doc. Ajuste de Inventario desde un Doc. Toma de Inventario." />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" OnClientClick="return( validarSave()  );" Width="80px" runat="server" Text="Guardar" ToolTip="Guardar" />
                        </td>
                        <td>
                        
                            <asp:Button ID="btnCancelar"  Width="80px" runat="server" Text="Cancelar" ToolTip="Cancelar" />
                        
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td style="width:100%" align="right">
                        <table>
                <tr>
                    <td class="Texto">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
                        </td>
                    </tr>
                </table>
            
        </td>
    </tr>
<tr><td class ="TituloCelda" >Cuenta Bancaria</td></tr>
<tr>
<td>
    
    <asp:Panel ID="Panel_Persona" runat="server"  >
    <fieldset title="Busqueda de Personas" class="FieldSetPanel_OnlyBorder">
                                <legend class="Texto"  style="font-weight:bold;text-align:left" >Persona</legend>
    <table >
        <tr>
            <td class="Texto" style="font-weight:bold;text-align:right">
                Descripci�n:</td>
            <td colspan="3">
                <asp:TextBox ID="txtDescripcionPersona" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" Width="400px" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnBuscarPersona" OnClientClick="return( valBuscarPersona()  );" runat="server" Text="Buscar" ToolTip="Buscar Cliente" Width="70px" />
            </td>
               <td>
                <asp:Button ID="btnLimpiar"  runat="server" Text="Limpiar" ToolTip="limpiar Cliente" Width="70px" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="Texto" style="font-weight:bold;text-align:right" >
                D.N.I.:</td>
            <td>
                <asp:TextBox ID="txtDNI" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
            </td>
            <td class="Texto" style="font-weight:bold;text-align:right" >
                R.U.C.:</td>
            <td>
                <asp:TextBox ID="txtRUC"  ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>                                
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    </fieldset>
    </asp:Panel>

    



    </td>
</tr>
<tr>
<td>
    <table>
        <tr>
       
            <td  class="Texto" style="font-weight:bold;text-align:right" >
                Banco:</td>
            <td>
            <asp:DropDownList ID="cboBanco" runat="server" >
            </asp:DropDownList>
            </td>
            
            <td class="Texto" style="font-weight:bold;text-align:right" >
                Moneda:</td>
            <td >
            <asp:DropDownList ID="cboMoneda" runat="server" >
            </asp:DropDownList>
            </td>
            <td class="Texto" style="font-weight:bold;text-align:right" >
                Nro. Cuenta Bancaria:</td>
            <td>
            <asp:TextBox ID="txtNumero" runat="server" Width="200px" ></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
         <td class="Texto" style="font-weight:bold;text-align:right" >
                Cuenta Contable:</td><td>
            <asp:TextBox ID="txtCtaContable" runat="server" Width="100%" ></asp:TextBox>
            </td >
            <td class="Texto" style="font-weight:bold;text-align:right">
                    Swift Bancario:
            </td>
            <td >
            <asp:TextBox ID="txtswiftbanca" runat="server" Text=""></asp:TextBox>
            </td>
            
            <td class="Texto" style="font-weight:bold;text-align:right">
             Nro. Cuenta Interbancaria:
            </td>
            
            <td> 
            <asp:TextBox ID="txtcuentaInter" runat="server"  Width="200px">
            </asp:TextBox></td>
        </tr>
        
        <tr>

            <td class="Texto" style="font-weight:bold;text-align:right" >
                Estado:</td>
             <td colspan="3">
            <table>
            <tr>
            <td>
            <asp:RadioButtonList ID="rbtlEstado" runat="server" RepeatDirection="Horizontal " CssClass ="Label">
                <asp:ListItem Text="Activo" Selected="True" Value="1"></asp:ListItem>
                <asp:ListItem Text="Inactivo" Value="2"></asp:ListItem>
                </asp:RadioButtonList></td>
                
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                <asp:Button ID="btnBuscar_CB" runat="server" Text="Buscar" Width="70px" ToolTip="Buscar" />
            
            </td>
            </tr>
            </table>
            </td>
            <td>
                &nbsp;</td><td>
                &nbsp;</td></tr></table></td></tr><tr>
    <td style="text-align: center">
         <asp:GridView ID="dgvCuentaBancaria" runat="server" AutoGenerateColumns="False" Width="100%" >
                <Columns>                  
                   <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-Height="25px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtnMostrar" runat="server" OnClick ="lbtnEditar_Click">Editar</asp:LinkButton><asp:HiddenField ID ="hddIdEstado" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>'  runat ="server"  />
                            <asp:HiddenField ID ="hddIdCuentaBancaria" Value='<%# DataBinder.Eval(Container.DataItem,"IdCuentaBancaria") %>'  runat ="server"  />
                            <asp:HiddenField ID ="hddIdBanco" Value='<%# DataBinder.Eval(Container.DataItem,"IdBanco") %>'  runat ="server"  />
                            <asp:HiddenField ID ="hddEdita" Value='<%# DataBinder.Eval(Container.DataItem,"Edita") %>'  runat ="server"  />
                        </ItemTemplate>
                    </asp:TemplateField>
                  <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="btnEliminar" ForeColor="Red"  OnClientClick="   return(  valOnClick_btnEliminar() );  " runat="server" OnClick ="btnEliminar_Click" >Eliminar</asp:LinkButton></ItemTemplate></asp:TemplateField><asp:BoundField DataField="Empresa" HeaderText="Empresa" >
                  </asp:BoundField>
                  <asp:BoundField DataField="Banco" HeaderText="Banco" NullDisplayText="0" />
                  <asp:BoundField DataField="MonSimbolo" HeaderText="Moneda" NullDisplayText="0" />
                  <asp:BoundField DataField="NroCuentaBancaria" HeaderText="Numero" NullDisplayText="0" />
                  <asp:BoundField DataField="Estado" HeaderText="Estado" NullDisplayText="0" />
                  <asp:BoundField DataField ="NroCuentaContable"  HeaderText ="Cuenta Contable" />
                  <asp:BoundField DataField ="SaldoDisponible"  HeaderText ="Saldo Disponible" />
                  <asp:BoundField DataField ="Saldocontable"  HeaderText ="Saldo Contable" />
                  
                </Columns>
                
                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                <EditRowStyle CssClass="GrillaEditRow" />
                <FooterStyle CssClass="GrillaFooter" />
                <HeaderStyle CssClass="GrillaHeader" />
                <PagerStyle CssClass="GrillaPager" />
                <RowStyle CssClass="GrillaRow" />
                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
    </td>
</tr>
<tr>
    <td >
        <asp:HiddenField ID="hddIdPersona" runat="server" Value="" />
        <asp:HiddenField ID="hddFrmModo" runat="server" Value="" />
        <asp:HiddenField ID="hddIdCuentaBancaria" runat="server" Value="" />        
        <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
    </td>
</tr>
</table>
<div  id="capaPersona"   style="border: 3px solid blue; padding: 8px; width:900px; height:auto; position:absolute; top:100px; left:21px; background-color:white; z-index:2; display :none; ">                                        
                        <table style="width: 100%;">                                    
                                <tr><td align="right">
                                       <asp:ImageButton ID="btnCerrar_Capa" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar.GIF"                                                
                                        OnClientClick="return(offCapa('capaPersona'));" />   
                                </td>
                                </tr>   
                                <tr>
                                <td>
                                        <asp:Panel ID="pnlBusquedaPersona" runat="server">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table width="100%">                                            
                                            <tr>
                                                <td>
                                                    <table width="100%">                                              
                                                        <tr>
                                                            <td>
                                                                <table >
                                                                <tr>
                                                                <td colspan="5">
                                                                  <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal" AutoPostBack="false">
                                                                <asp:ListItem  Value="N">Natural</asp:ListItem><asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem></asp:RadioButtonList></td></tr><tr>
                                                                        <td class="Texto">
                                                                            Razon Social / Nombres:                                                                            
                                                                        </td>
                                                                        <td colspan="4" >
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda());" runat="server"
                                                                                Width="450px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox></td></tr><tr>
                                                                        <td class="Texto">                                                                                                                                                    
                                                                        D.N.I.:
                                                                        </td>
                                                                        <td >                                                                            
                                                                            <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                                MaxLength="8" runat="server"></asp:TextBox></td><td class="Texto" >
                                                                          Ruc:                                                                            
                                                                        </td>
                                                                        <td>
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                                MaxLength="11"></asp:TextBox></td><td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                                onmouseout="this.src='/Imagenes/Buscar_b.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                    PageSize="20" Width="100%">
                                                                    <Columns>
                                                                        <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                        <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                        <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                            <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>                                                                            
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                                    <FooterStyle CssClass="GrillaFooter" />
                                                                    <PagerStyle CssClass="GrillaPager" />
                                                                    <RowStyle CssClass="GrillaRow" />
                                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                    ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                                <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                    ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                                <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                    runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                        Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                                <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                    onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox></td></tr></table></td></tr></table></td></tr></table></asp:Panel></td></tr></table></div>
                                                                    <script language ="javascript" type ="text/javascript" >

    //************************** BUSQUEDA PERSONA
    //***************************************************** BUSQUEDA PERSONA
                                                                        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
                                                                        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
                                                                            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                                                                                offsetX = Math.floor(offsetX);
                                                                                offsetY = Math.floor(offsetY);
                                                                            }
                                                                            this._origOnFormActiveElement(element, offsetX, offsetY);
                                                                        };
    function validarbusqueda() {
        var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
        var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
        var control = radio.getElementsByTagName('input');
        if (control[1].checked == true) {
            tb.select();
            tb.focus();
            return false;
        }
        return true;
    }
    function validarCajaBusquedaNumero() {
        var key = event.keyCode;
        if (key == 13) {
            var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
            boton.focus();
            return true;
        }
        if (key == 46) {
            return true;
        } else {
            if (!onKeyPressEsNumero('event')) {
                return false;
            }
        }
    }
    function validarCajaBusqueda() {        
        var key = event.keyCode;
        if (key == 13) {
            var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
            boton.focus();
            return true;
        }
    }
    function valNavegacion(tipoMov) {

        var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
        if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
            alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }
        switch (tipoMov) {
            case '0':   //************ anterior
                if (index <= 1) {
                    alert('No existen p�ginas con �ndice menor a uno.');
                    return false;
                }
                break;
            case '1':
                break;
            case '2': //************ ir
                index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                if (isNaN(index) || index == null || index.length == 0) {
                    alert('Ingrese una P�gina de navegaci�n.');
                    document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                    document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                    return false;
                }
                if (index < 1) {
                    alert('No existen p�ginas con �ndice menor a uno.');
                    document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                    document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                    return false;
                }
                break;
        }
        return true;
    }
    function mostrarCapaPersona() {
        onCapa('capaPersona');
        document.getElementById('<%=tbRazonApe.ClientID%>').select();
        document.getElementById('<%=tbRazonApe.ClientID%>').focus();
        return false;
    }


    function validarSave() {
    
        var empresa = document.getElementById('<%=hddIdPersona.ClientID%>');
        var idEmpresa = empresa.value;
        if (isNaN(parseInt(idEmpresa)) || idEmpresa.length <= 0 || parseInt(idEmpresa) <= 0) {
            alert('Seleccione Una Empresa.');       
            return false;
        }
        var banco=document.getElementById('<%=cboBanco.ClientID%>');
        var idbanco = banco.value;
        if (idbanco == 0) {
            alert('Seleccione Un Banco.');

            return false;
        }
              
        
        var moneda = document.getElementById('<%=cboMoneda.ClientID%>');
        var idmoneda = moneda.value;
        if (idmoneda==0) {
            alert('Seleccione una Moneda');

            return false;
        }
        var numero = document.getElementById('<%=txtNumero.ClientID%>');
        if (numero.value.length == 0) {
            alert('Debe Ingrese Numero Cuenta.');
            numero.select();
            numero.focus();
            return false;
        }
        var CtaContable = document.getElementById('<%=txtCtaContable.ClientID%>');
        /*if (CtaContable.value.length == 0) {
            alert('Debe Ingrese Numero Cuenta Contable.');
            CtaContable.select();
            CtaContable.focus();
            return false;
        }*/


        
        return(confirm('Desea Continuar Con la Operacion'));
      
    }

    function validarSaveActualizar() {

        var numero = document.getElementById('<%=txtNumero.ClientID%>');
        if (numero.value.length == 0) {
            alert('Debe Ingrese Numero Cuenta.');
            numero.select();
            numero.focus();
            return false;
        }
        var CtaContable = document.getElementById('<%=txtCtaContable.ClientID%>');
        if (CtaContable.value.length == 0) {
            alert('Debe Ingrese Numero Cuenta Contable.');
            CtaContable.select();
            CtaContable.focus();
            return false;
        }
 
           
        
      return (confirm('Desea Continuar con  la Operacion?'));
      
    }
    function valOnClick_btnEliminar() {
        return confirm('Desea continuar con la Operaci�n ?');
    }
    function valBuscarPersona() {
        mostrarCapaPersona();
        return false;
    }

    function onKeyPressEsNumeroSalCon() {
        return onKeyPressEsNumero('event');
  } 
</script>
<script language="javascript" type="text/javascript">
    //////////////////////////////////////////
    var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
    /////////////////////////////////////////
    </script>
</asp:Content>