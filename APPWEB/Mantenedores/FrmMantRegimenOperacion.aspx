﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantRegimenOperacion.aspx.vb" Inherits="APPWEB.FrmMantRegimenOperacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                onmouseover="this.src='/Imagenes/Buscar_A.JPG';" onmouseout="this.src='/Imagenes/Buscar_B.JPG';" />
                            <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='/Imagenes/Nuevo_A.JPG';"
                                onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" />
                            <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                OnClientClick="return(validarSave());" CausesValidation="true" />
                            <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';"
                                OnClientClick="return(confirm('Desea cancelar el proceso?'));" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Régimen Operación
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <table style="width: 90%">
                    <tr>
                        <td class="Label">
                            <asp:Label ID="lblRegimen" runat="server" Text="Régimen:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="cboRegimen" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            <asp:Label ID="lblTipoOperacion" runat="server" Text="Tipo Operación:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTipoOperacion" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            <asp:Label ID="lblEstado" runat="server" Text="Estado:"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rbtlEstado" runat="server" RepeatDirection="Horizontal "
                                CssClass="Label">
                                <asp:ListItem Text="Todos" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Activo" Selected="True" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label">
                            <asp:Label ID="lblctaContable" runat="server" Text="Cuenta Contable:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCtaContable" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="dgvRegimenOperacion" runat="server" AutoGenerateColumns="False"
                    Width="750px">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEditar" runat="server" OnClick="lbtnEditar_Click">Editar</asp:LinkButton>
                                <asp:HiddenField ID="hddIdRegimen" Value='<%# DataBinder.Eval(Container.DataItem,"IdRegimen") %>'
                                    runat="server" />
                                <asp:HiddenField ID="hddIdTipoOperacion" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoOperacion") %>'
                                    runat="server" />
                                <asp:HiddenField ID="hddIdEstado" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="RegimenNombre" HeaderText="Régimen" />
                        <asp:BoundField DataField="TipoOperacionNombre" HeaderText="Tipo Operación" NullDisplayText="0" />
                        <asp:BoundField DataField="CuentaContable" HeaderText="Cuenta Contable" NullDisplayText="" />
                        <asp:BoundField DataField="DescEstado" HeaderText="Estado" NullDisplayText="0" />
                    </Columns>
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <HeaderStyle CssClass="GrillaHeader" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">
        function validarSave() {
            var Regimen = document.getElementById('<%=cboRegimen.ClientID%>');
            var idRegimen = Regimen.value;
            if (idRegimen == 0) {
                alert('Seleccione un Régimen.');

                return false;
            }
            var TipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            var idTipoOperacion = TipoOperacion.value;
            if (idTipoOperacion == 0) {
                alert('Seleccione un Tipo Operación.');

                return false;
            }

            var CtaContable = document.getElementById('<%=txtCtaContable.ClientID%>');
            if (CtaContable.value.length == 0) {
                alert('Debe Ingrese Numero Cuenta Contable.');
                CtaContable.select();
                CtaContable.focus();
                return false;
            }

            return (confirm('Desea Continuar Con la Operacion'));
        }

        function validarSaveActualizar() {

            var CtaContable = document.getElementById('<%=txtCtaContable.ClientID%>');
            if (CtaContable.value.length == 0) {
                alert('Debe Ingrese Numero Cuenta Contable.');
                CtaContable.select();
                CtaContable.focus();
                return false;
            }

            return (confirm('Desea Continuar con  la Operacion?'));
        }
    
    </script>

</asp:Content>
