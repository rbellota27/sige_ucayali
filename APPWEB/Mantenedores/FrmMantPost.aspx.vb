﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmMantPost
    Inherits System.Web.UI.Page

    Private objCbo As New Combo
    Private objScript As New ScriptManagerClass
    Private objNegPostView As New Negocio.PostView
    'Private ListPostView As New List(Of Entidades.PostView)
    Private p As New Entidades.PostView
    'Private modo As New modo_menu

    Public Property Id() As Integer
        Get
            Return CInt(ViewState("Id"))
        End Get
        Set(ByVal value As Integer)
            ViewState.Remove("Id")
            ViewState.Add("Id", value)
        End Set
    End Property
    Private Property modo() As modo_menu
        Get
            Return CType(ViewState("modo"), modo_menu)
        End Get
        Set(ByVal value As modo_menu)
            ViewState.Remove("modo")
            ViewState.Add("modo", value)
        End Set
    End Property
    Private Property ListPostView() As List(Of Entidades.PostView)
        Get
            Return CType(Session("ListPostView"), List(Of Entidades.PostView))
        End Get
        Set(ByVal value As List(Of Entidades.PostView))
            Session.Remove("ListPostView")
            Session.Add("ListPostView", value)
        End Set
    End Property
    Private Property ListTipoTarjeta() As List(Of Entidades.TipoTarjeta)
        Get
            Return CType(Session("ListTipoTarjeta"), List(Of Entidades.TipoTarjeta))
        End Get
        Set(ByVal value As List(Of Entidades.TipoTarjeta))
            Session.Remove("ListTipoTarjeta")
            Session.Add("ListTipoTarjeta", value)
        End Set
    End Property

    Private Property ListTipoTarjeta_In() As List(Of Entidades.TipoTarjeta)
        Get
            Return CType(Session("ListTipoTarjeta_In"), List(Of Entidades.TipoTarjeta))
        End Get
        Set(ByVal value As List(Of Entidades.TipoTarjeta))
            Session.Remove("ListTipoTarjeta_In")
            Session.Add("ListTipoTarjeta_In", value)
        End Set
    End Property
    Private Property ListTipoTarjeta_Out() As List(Of Entidades.TipoTarjeta)
        Get
            Return CType(Session("ListTipoTarjeta_Out"), List(Of Entidades.TipoTarjeta))
        End Get
        Set(ByVal value As List(Of Entidades.TipoTarjeta))
            Session.Remove("ListTipoTarjeta_Out")
            Session.Add("ListTipoTarjeta_Out", value)
        End Set
    End Property

    Enum modo_menu
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Insertar = 3
        Actualizar = 4
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            inicializarFRM()
            modo = modo_menu.Inicio
            HabilitarControles(modo)
            ConfigurarDatos()
        Else
            actualizar_atributos()
        End If
    End Sub

    Private Sub inicializarFRM()
        Dim objScript As New ScriptManagerClass
        Try
            cargar_recargar_datos()
            With objCbo
                .LlenarCboBanco(cboBanco, True)
                .LlenarCboCuentaBancaria(cboCuentaBancaria, CInt(cboBanco.SelectedValue), True)
                '.LlenarCboTipoTarjeta(cboTipoTarjeta, True)
            End With

            ListPostView = objNegPostView.SelectAll()

            'Datos Adicionales - Tipo Tarjeta
            'ini
            ListTipoTarjeta = (New Negocio.TipoTarjeta).SelectAllActivo
            ListTipoTarjeta_Out = (New Negocio.TipoTarjeta).SelectAllActivo
            ListTipoTarjeta_In = New List(Of Entidades.TipoTarjeta)
            LlenarCboTipoTarjeta()
            'fin

            Id = 0
            cargar_dgv()
            Panel_DatosAdicionales.Visible = False

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizar_atributos()
        Try
            p.Id = Id
            p.IdBanco = CInt(Me.cboBanco.SelectedValue)
            p.IdCuentaBancaria = CInt(IIf(CStr(Me.cboCuentaBancaria.SelectedValue) <> "", Me.cboCuentaBancaria.SelectedValue, 0))
            'p.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)
            p.Descripcion = DecodeCadena(txtDescripcion.Text)
            p.Identificador = DecodeCadena(txtIdentificador.Text)

            Select Case CInt(rbtlEstado.SelectedValue)
                Case 2
                    p.Estado = Nothing
                Case 1
                    p.Estado = True
                Case 0
                    p.Estado = False
            End Select
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub cargar_recargar_datos()
        ListPostView = objNegPostView.SelectAll()
    End Sub

    Protected Sub cargar_dgv()
        actualizar_atributos()
        gvDatos.DataSource = objNegPostView.FiltrarLista(p, ListPostView)
        gvDatos.DataBind()
    End Sub

    Public Sub limpiarCtrl()

        Me.cboBanco.SelectedIndex = 0
        Me.cboCuentaBancaria.SelectedIndex = 0
        Me.txtDescripcion.Text = ""
        Me.txtIdentificador.Text = ""
        Me.rbtlEstado.SelectedValue = "1" 'Activos
        Me.ListTipoTarjeta_In.Clear()
        Me.ListTipoTarjeta_Out = (New Negocio.TipoTarjeta).SelectAllActivo
        Me.ListTipoTarjeta = (New Negocio.TipoTarjeta).SelectAllActivo
        actualizarDatosTipoTarjeta()
    End Sub

    'Private Sub cargar_entidad_en_controles(ByVal x As Entidades.PostView)
    '    cboBanco.SelectedValue = CStr(x.IdBanco)
    '    txtDescripcion.Text = x.Descripcion
    '    rbtlEstado.SelectedValue = x.Estado
    'End Sub
    Public Sub HabilitarControles(ByVal x As modo_menu)
        Select Case x
            Case modo_menu.Inicio

                Me.btnNuevo.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscar.Visible = True

                'Me.cboBanco.Enabled = True
                'Me.cboCuentaBancaria.Enabled = True
                'Me.txtDescripcion.Visible = True
                'Me.txtIdentificador.Visible = True
                Me.rbtlEstado.Items(0).Enabled = True

                'Me.lblBanco.Visible = True
                'Me.lblDescripcion.Visible = True
                'Me.lblEstado.Visible = True

                Me.gvDatos.Enabled = True
                lblModo.Text = "Búsqueda"
                Panel_DatosAdicionales.Visible = False
            Case modo_menu.Nuevo

                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False

                'Me.cboBanco.Enabled = True
                'Me.cboCuentaBancaria.Enabled = True

                'Me.txtDescripcion.Visible = True
                'Me.txtIdentificador.Visible = True
                Me.rbtlEstado.Items(0).Enabled = False

                'Me.lblBanco.Visible = True
                'Me.lblDescripcion.Visible = True
                'Me.lblEstado.Visible = True

                Me.gvDatos.Enabled = False
                lblModo.Text = "Nuevo"
                Panel_DatosAdicionales.Visible = True
            Case modo_menu.Editar

                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False

                'Me.cboBanco.Enabled = False
                'Me.cboCuentaBancaria.Enabled = False
                'Me.txtDescripcion.Visible = True
                'Me.txtIdentificador.Visible = True
                Me.rbtlEstado.Items(0).Enabled = False

                'Me.lblBanco.Visible = True
                'Me.lblDescripcion.Visible = True
                'Me.lblEstado.Visible = True

                Me.gvDatos.Enabled = False
                lblModo.Text = "Edición"
                Panel_DatosAdicionales.Visible = True
            Case modo_menu.Actualizar
            Case modo_menu.Insertar
        End Select
    End Sub

    Public Function DecodeCadena(ByVal cadena As String) As String
        Dim cadenavalida As String = ""
        cadenavalida = HttpUtility.HtmlDecode(cadena)
        Return cadenavalida
    End Function


    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        cargar_dgv()
    End Sub

    Protected Sub lbtnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            modo = modo_menu.Editar

            Dim lbtnEditar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lbtnEditar.NamingContainer, GridViewRow)

            Id = CInt(CType(fila.Cells(0).FindControl("hddId"), HiddenField).Value.Trim())

            Me.cboBanco.SelectedValue = CStr(CType(fila.Cells(0).FindControl("hddIdBanco"), HiddenField).Value.Trim())
            objCbo.LlenarCboCuentaBancaria(cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), True)

            Me.cboCuentaBancaria.SelectedValue = CStr(CType(fila.Cells(0).FindControl("hddIdCuentaBancaria"), HiddenField).Value.Trim())
            Me.rbtlEstado.SelectedValue = CStr(IIf(CBool((CType(fila.Cells(0).FindControl("hddIdEstado"), HiddenField).Value)), 1, 0))

            Me.txtDescripcion.Text = DecodeCadena(fila.Cells(4).Text)
            Me.txtIdentificador.Text = DecodeCadena(fila.Cells(5).Text)

            ListTipoTarjeta_In = getListaIn(ListTipoTarjeta, (New Negocio.Pos_TipoTarjeta).SelectxIdPos(Id))
            ListTipoTarjeta_Out = getListaOut(ListTipoTarjeta, ListTipoTarjeta_In)
            actualizarDatosTipoTarjeta()

            HabilitarControles(modo)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click

        modo = modo_menu.Nuevo
        HabilitarControles(modo)
        'limpiarCtrl()  'se deben dejar los datos escritos
        cargar_dgv()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        Id = 0      'Limpia el ID
        modo = modo_menu.Inicio
        HabilitarControles(modo_menu.Inicio)
        limpiarCtrl()
        cargar_dgv()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click

        Dim mensaje As String
        Dim exito As Boolean
        exito = True
        mensaje = ""
        'Solo entran los modos editar y nuevo, en esta zona
        'opcional ini
        cargar_recargar_datos() 'Los datos disponibles se actualizan antes de realizar la validacion
        'opcional fin
        If objNegPostView.ValidarEntidadxNombre(p, ListPostView) Then
            Select Case modo
                Case modo_menu.Nuevo
                    If objNegPostView.InsertT_AddListPos_TipoTarjeta(p, getListPos_TipoTarjeta(ListTipoTarjeta_In)) > 0 Then
                        mensaje = "Se Guardó Correctamente"
                    End If
                Case modo_menu.Editar
                    If objNegPostView.UpdateT_AddListPos_TipoTarjeta(p, getListPos_TipoTarjeta(ListTipoTarjeta_In)) > 0 Then
                        mensaje = "Se Guardó Correctamente"
                    End If
            End Select
        Else
            exito = False
            mensaje = "Ya existe un registro con ese Identificador"
        End If

        objScript.mostrarMsjAlerta(Me, mensaje)
        If exito Then
            Id = 0   'Limpia el ID
            cargar_recargar_datos()
            modo = modo_menu.Inicio
            HabilitarControles(modo)
            cargar_dgv()    'Muestra el registro ingresado

            limpiarCtrl()   'Limpia los controles
        End If
    End Sub

    Protected Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboBanco.SelectedIndexChanged
        objCbo.LlenarCboCuentaBancaria(cboCuentaBancaria, p.IdBanco, True)
    End Sub

    Protected Sub gvDatos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDatos.RowDataBound
        'LinkButton Editar esta en un UpdatePanel y para que este no atrape su postback, se registra en el ScriptManager de la página Maestra
        Try
            If e.Row.RowType = DataControlRowType.DataRow And e.Row.RowState = DataControlRowState.Normal Or _
            e.Row.RowState = DataControlRowState.Alternate Or e.Row.RowState = DataControlRowState.Selected Then
                Dim sm As ScriptManager
                sm = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                sm.RegisterPostBackControl(DirectCast(e.Row.FindControl("lbtnEditar"), LinkButton))
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "Datos Adicionales - Tipo Tarjeta"

    Enum Operacion
        agregar = 1
        quitar = 2
    End Enum

    Private Sub LlenarCboTipoTarjeta()
        cboTipoTarjeta.Items.Clear()
        If ListTipoTarjeta_Out.Count > 0 Then
            If btnAgregarTipoTarjeta.Enabled <> True Then
                cboTipoTarjeta.Enabled = True
                btnAgregarTipoTarjeta.Enabled = True
            End If
            cboTipoTarjeta.DataSource = ListTipoTarjeta_Out
            cboTipoTarjeta.DataValueField = "Id"
            cboTipoTarjeta.DataTextField = "Nombre"
            cboTipoTarjeta.DataBind()
        Else
            If btnAgregarTipoTarjeta.Enabled <> False Then
                cboTipoTarjeta.Enabled = False
                btnAgregarTipoTarjeta.Enabled = False
            End If
        End If
    End Sub

    Protected Sub btnAgregarTipoTarjeta_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarTipoTarjeta.Click
        Actualizar_Datos_TipoTarjeta(CInt(cboTipoTarjeta.SelectedValue), Operacion.agregar)
    End Sub

    Protected Sub lbtnQuitarTipoTarjeta_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim lbtnQuitar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
        Dim fila As GridViewRow = CType(lbtnQuitar.NamingContainer, GridViewRow)

        Dim IdTTarj As Integer

        IdTTarj = CInt(CType(fila.Cells(0).FindControl("hddIdTipoTarjeta"), HiddenField).Value.Trim())

        Actualizar_Datos_TipoTarjeta(IdTTarj, Operacion.quitar)

    End Sub

    Private Sub Actualizar_Datos_TipoTarjeta(ByVal IdTTarj As Integer, ByVal op As Operacion)
        Try
            Dim Ind As Integer
            Dim list1, list2 As List(Of Entidades.TipoTarjeta)

            Select Case op
                Case Operacion.agregar
                    list1 = ListTipoTarjeta_In
                    list2 = ListTipoTarjeta_Out
                Case Operacion.quitar
                    list1 = ListTipoTarjeta_Out
                    list2 = ListTipoTarjeta_In
            End Select

            'Se obtiene el indice de la lista, para el id enviado
            Ind = ListaGetIndiceEntidadxId(list2, IdTTarj)

            'la entidad correspondiente al id enviado, se extrae y se pasa a la otra lista.
            'ini
            If Ind > -1 Then
                list1.Add(list2(Ind).getClone)
                list2.RemoveAt(Ind)
            End If
            'fin
            actualizarDatosTipoTarjeta()
            
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub actualizarDatosTipoTarjeta()

        gvTipoTarjeta.DataSource = ListTipoTarjeta_In
        gvTipoTarjeta.DataBind()
        LlenarCboTipoTarjeta()
    End Sub

    Protected Function ListaGetIndiceEntidadxId(ByVal Lista As List(Of Entidades.TipoTarjeta), ByVal id As Integer) As Integer
        For i As Integer = 0 To Lista.Count - 1
            If Lista(i).Id = id Then
                Return i
            End If
        Next
        Return -1
    End Function

    Protected Function getListaIn(ByVal L As List(Of Entidades.TipoTarjeta), ByVal LPTT As List(Of Entidades.Pos_TipoTarjeta)) As List(Of Entidades.TipoTarjeta)

        Dim LIn As New List(Of Entidades.TipoTarjeta)
        If LPTT IsNot Nothing Then
            Dim ind As Integer

            For i As Integer = 0 To LPTT.Count - 1
                ind = ListaGetIndiceEntidadxId(L, LPTT(i).IdTipoTarjeta)
                If ind > -1 Then
                    LIn.Add(L(ind).getClone)
                End If
            Next
        End If
        Return LIn

    End Function

    Protected Function getListaOut(ByVal L As List(Of Entidades.TipoTarjeta), ByVal LIn As List(Of Entidades.TipoTarjeta)) As List(Of Entidades.TipoTarjeta)

        Dim LOut As New List(Of Entidades.TipoTarjeta)

        For i As Integer = 0 To L.Count - 1
            If ListaGetIndiceEntidadxId(LIn, L(i).Id) = -1 Then
                LOut.Add(L(i).getClone)
            End If
        Next

        Return LOut

    End Function

    Protected Function getListPos_TipoTarjeta(ByVal LTT As List(Of Entidades.TipoTarjeta)) As List(Of Entidades.Pos_TipoTarjeta)

        Dim L As New List(Of Entidades.Pos_TipoTarjeta)

        For i As Integer = 0 To LTT.Count - 1
            Dim obj As New Entidades.Pos_TipoTarjeta
            obj.IdPos = Id
            obj.IdTipoTarjeta = LTT(i).Id
            L.Add(obj)
        Next

        Return L
    End Function

    Protected Sub gvTipoTarjeta_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTipoTarjeta.RowDataBound
        'LinkButton QuitarTipoTarjeta se registra en el ScriptManager de la página Maestra
        Try
            If e.Row.RowType = DataControlRowType.DataRow And e.Row.RowState = DataControlRowState.Normal Or _
            e.Row.RowState = DataControlRowState.Alternate Or e.Row.RowState = DataControlRowState.Selected Then
                Dim sm As ScriptManager
                sm = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                sm.RegisterPostBackControl(DirectCast(e.Row.FindControl("lbtnQuitarTipoTarjeta"), LinkButton))
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

    

    
End Class