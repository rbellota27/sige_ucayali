﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmMantSerie
    Inherits System.Web.UI.Page
    Private listaSerie As List(Of Entidades.Serie)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            InicializarFrm()
        End If
    End Sub
    Private Function getListaSerie() As List(Of Entidades.Serie)
        Return CType(Session.Item("listaSerie"), List(Of Entidades.Serie))
    End Function
    Private Sub setListaSerie(ByVal lista As List(Of Entidades.Serie))
        Session.Remove("listaSerie")
        Session.Add("listaSerie", lista)
    End Sub
    Private Sub InicializarFrm()
        listaSerie = New List(Of Entidades.Serie)
        Session.Add("listaSerie", Me.listaSerie)
        cargarControles()
        rdbEstado_B.SelectedIndex = 1
        Me.buscarSerieGrilla(0, 0, 0, "1")
        verFrmInicio()
    End Sub
#Region "CARGAR CONTROLES"
    Private Sub cargarControles()
        Dim objScript As New ScriptManagerClass
        Try
            cargarCboTipoDocumento()
            cargarCboEmpresa()
            cargarCboTienda()
            cargarCboTienda_B()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub cargarCboTienda()
        Dim obj As New Negocio.Tienda
        Me.cmbTienda.DataSource = obj.SelectCboxEmpresa(CInt(Me.cmbEmpresa.SelectedValue))
        Me.cmbTienda.DataBind()
    End Sub
    Private Sub cargarCboTienda_B()
        Dim obj As New Negocio.Tienda
        Dim lista As List(Of Entidades.Tienda) = obj.SelectCboxEmpresa(CInt(Me.cmbEmpresa_B.SelectedValue))
        Dim objTienda As New Entidades.Tienda
        objTienda.Id = 0
        objTienda.Nombre = "-----"
        lista.Insert(0, objTienda)
        Me.cmbTienda_B.DataSource = lista
        Me.cmbTienda_B.DataBind()
    End Sub
    Private Sub cargarCboEmpresa()
        Dim obj As New Negocio.Propietario
        Dim lista As List(Of Entidades.Propietario) = obj.SelectCbo
        Me.cmbEmpresa.DataSource = lista
        Me.cmbEmpresa.DataBind()
        Dim objProp As New Entidades.Propietario
        objProp.Id = 0
        objProp.NombreComercial = "-----"
        lista.Insert(0, objProp)
        cmbEmpresa_B.DataSource = lista
        cmbEmpresa_B.DataBind()
    End Sub
    Private Sub cargarCboTipoDocumento()
        Dim obj As New Negocio.TipoDocumento
        Dim lista As List(Of Entidades.TipoDocumento) = obj.SelectCbo
        Me.cmbTipoDoc.DataSource = lista
        Me.cmbTipoDoc.DataBind()
        Dim objTDoc As New Entidades.TipoDocumento
        objTDoc.Id = 0
        objTDoc.Descripcion = "-----"
        lista.Insert(0, objTDoc)
        cmbTipoDoc_B.DataSource = lista
        cmbTipoDoc_B.DataBind()
    End Sub
#End Region
    Protected Sub cmbEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbEmpresa.SelectedIndexChanged
        Me.cargarCboTienda()
    End Sub
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        registrar()
    End Sub
    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        verFrmNuevo()
    End Sub
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        verFrmInicio()
    End Sub
    Private Sub registrar()
        Dim objNScript As New ScriptManagerClass
        Dim objNSerie As New Negocio.Serie
        Dim hecho As Boolean = False
        Try
            Dim objESerie As New Entidades.Serie
            objESerie.Cantidad = CInt(txtCantidad.Text.Trim)
            objESerie.Estado = rdbEstado.SelectedValue
            objESerie.IdEmpresa = CInt(cmbEmpresa.SelectedValue)
            objESerie.IdTienda = CInt(cmbTienda.SelectedValue)
            objESerie.IdTipoDocumento = CInt(cmbTipoDoc.SelectedValue)
            objESerie.Inicio = CInt(txtInicio.Text)
            objESerie.Longitud = txtNumero.Text.Trim.Length
            objESerie.Numero = txtNumero.Text
            Select Case hddModo.Value
                Case "N"
                    hecho = objNSerie.InsertaSerie(objESerie)
                Case "E"
                    objESerie.IdSerie = CInt(txtID.Text)
                    hecho = objNSerie.ActualizaSerie(objESerie)
            End Select
            If hecho Then
                objNScript.mostrarMsjAlerta(Me, "La operación se realizó con éxito")
                verFrmInicio()
            Else
                objNScript.mostrarMsjAlerta(Me, "Ocurrieron problemas en la operación.")
            End If
        Catch ex As Exception
            objNScript.mostrarMsjAlerta(Me, "Ocurrieron problemas en la operación")
        End Try
        buscarSerieGrilla(CInt(cmbEmpresa_B.SelectedValue), CInt(cmbTienda_B.SelectedValue), CInt(cmbTipoDoc_B.SelectedValue), rdbEstado_B.SelectedValue)
    End Sub
#Region "VER FORMULARIOS"
    Private Sub verFrmInicio()
        Panel_Serie.Visible = False
        Panel_Filtro.Enabled = True
        hddModo.Value = "I"
        actualizarBotonesControl()
        limpiarFrm()
    End Sub
    Private Sub verFrmEditar()
        Panel_Serie.Visible = True
        Panel_Filtro.Enabled = False
        hddModo.Value = "E"
        actualizarBotonesControl()
    End Sub
    Private Sub verFrmNuevo()
        Panel_Serie.Visible = True
        Panel_Filtro.Enabled = False
        hddModo.Value = "N"
        actualizarBotonesControl()
    End Sub
    Private Sub actualizarBotonesControl()
        If hddModo.Value = "I" Then
            btnNuevo.Visible = True
            btnCancelar.Visible = False
            btnGuardar.Visible = False
        Else
            btnNuevo.Visible = False
            btnCancelar.Visible = True
            btnGuardar.Visible = True
        End If
    End Sub
    Private Sub limpiarFrm()
        txtCantidad.Text = ""
        txtFechaRegistro.Text = ""
        txtInicio.Text = ""
        txtLongitud.Text = ""
        txtNumero.Text = ""
        txtID.Text = ""
        rdbEstado.SelectedIndex = 0
    End Sub
#End Region
    Private Sub buscarSerieGrilla(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDoc As Integer, ByVal estado As String)
        Dim objScript As New ScriptManagerClass
        Try
            Dim objNSerie As New Negocio.Serie
            Me.listaSerie = objNSerie.SelectxEmpresaxTiendaxTipoDocxEstado(IdEmpresa, IdTienda, IdTipoDoc, estado)
            DGVSerie_B.DataSource = Me.listaSerie
            DGVSerie_B.DataBind()
            Me.setListaSerie(listaSerie)
            If DGVSerie_B.Rows.Count = 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la búsqueda.")
        End Try
    End Sub
    Protected Sub rdbEstado_B_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbEstado_B.SelectedIndexChanged
        Me.buscarSerieGrilla(0, 0, 0, Me.rdbEstado_B.SelectedValue)
    End Sub
    Protected Sub cmbEmpresa_B_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbEmpresa_B.SelectedIndexChanged
        Me.cargarCboTienda_B()
    End Sub
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        buscarSerieGrilla(CInt(cmbEmpresa_B.SelectedValue), CInt(cmbTienda_B.SelectedValue), CInt(cmbTipoDoc_B.SelectedValue), rdbEstado_B.SelectedValue)
    End Sub
    Protected Sub DGVSerie_B_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGVSerie_B.PageIndexChanging
        DGVSerie_B.PageIndex = e.NewPageIndex
        DGVSerie_B.DataSource = Me.getListaSerie
        DataBind()
    End Sub
    Protected Sub DGVSerie_B_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGVSerie_B.SelectedIndexChanged
        hddModo.Value = "E"
        Dim idSerie As Integer = CInt(DGVSerie_B.SelectedRow.Cells(1).Text)
        Dim objScript As New ScriptManagerClass
        Try
            Dim objNSerie As New Negocio.Serie
            Dim objSerie As Entidades.Serie = objNSerie.SelectxIdSerie(idSerie)
            txtCantidad.Text = objSerie.Cantidad.ToString
            txtFechaRegistro.Text = objSerie.DescFechaAlta
            txtInicio.Text = objSerie.Inicio.ToString
            txtLongitud.Text = objSerie.Longitud.ToString
            txtNumero.Text = objSerie.Numero
            txtID.Text = objSerie.IdSerie.ToString
            rdbEstado.SelectedValue = objSerie.Estado
            cmbEmpresa.SelectedValue = objSerie.IdEmpresa.ToString
            cmbTienda.SelectedValue = objSerie.IdTienda.ToString
            cmbTipoDoc.SelectedValue = objSerie.IdTipoDocumento.ToString
            verFrmEditar()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
End Class