﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports Neodynamic.SDK.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Drawing
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports CrystalDecisions.ReportSource
Imports System.IO


Partial Public Class VisorDocMercantiles
    Inherits System.Web.UI.Page
    Private objnegDocMer As Negocio.DocumentosMercantiles
    Dim reporte As ReportDocument

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If WebClientPrint.ProcessPrintJob(Request) Then
            'rptDocBol()
            'load and set report's data source
            'Dim ds As New DataSet()
            'ds.ReadXml(Server.MapPath("~/NorthwindProducts.xml"))

            ''create and load rpt in memory
            'myCrystalReport.Load(Server.MapPath("~/MyProducts.rpt"))
            'myCrystalReport.SetDataSource(ds.Tables(0))
            ObtenerVariables()
            mostrarReporte()
            '==================
            'Export rpt to a temp PDF and get binary content            
            Dim pdfContent As Byte() = Nothing
            '=============================
            Dim osStream As Stream = reporte.ExportToStream(ExportFormatType.PortableDocFormat)
            pdfContent = New Byte(osStream.Length - 1) {}
            osStream.Read(pdfContent, 0, Convert.ToInt32(osStream.Length - 1))            

            'Using ms As MemoryStream = DirectCast(myCrystalReport.ExportToStream(ExportFormatType.PortableDocFormat), MemoryStream)
            '    pdfContent = ms.ToArray()
            'End Using

            'Now send this file to the client side for printing
            'IMPORTANT: Adobe Reader needs to be installed at the client side

            'get selected printer
            Dim printerName As String = Server.UrlDecode(Request("printerName"))

            'create a temp file name for our PDF report...
            Dim fileName As String = Guid.NewGuid().ToString("N") + ".pdf"

            'Create a PrintFile object with the pdf report
            Dim file As New PrintFile(pdfContent, fileName)
            'Create a ClientPrintJob and send it back to the client!
            Dim cpj As New ClientPrintJob()
            'set file to print...
            cpj.PrintFile = file
            'set client printer...
            If printerName = "Default Printer" Then
                cpj.ClientPrinter = New DefaultPrinter()
            Else
                cpj.ClientPrinter = New InstalledPrinter(printerName)
            End If
            'send it...
            cpj.SendToClient(Response)

            'Response.ClearContent()
            'Response.ClearHeaders()
            'Response.ContentType = "application / pdf"
            'Response.BinaryWrite(pdfContent)
            'Response.Flush()
            'Response.Close()
        End If
    End Sub
    Private Sub ObtenerVariables()
        Dim iReporte As String = Request.QueryString("iReporte")

        If iReporte Is Nothing Then
            iReporte = ""
        End If
        ViewState.Add("iReporte", iReporte)
        Select Case iReporte
            Case "1" 'dEtalles de documentos tipoimpresion
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                ViewState.Add("tipoimpresion", Request.QueryString("tipoimpresion"))

            Case "2"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "3"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                ViewState.Add("tipoimpresion", Request.QueryString("tipoimpresion"))
            Case "4"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "5"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "6"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                ViewState.Add("tipoimpresion", Request.QueryString("tipoimpresion"))
            Case "7"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "8"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "9"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "10"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "11"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "12"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "13"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "14"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
                ViewState.Add("tipoimpresion", Request.QueryString("tipoimpresion"))
            Case ("15")
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "16"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "99"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "17"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "18"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "25"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "33"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "56"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "47"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))
            Case "2101100003"
                ViewState.Add("IdDocumento", Request.QueryString("IdDocumento"))

        End Select
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            ObtenerVariables()            
        End If
        'Response.Write("<script>alert('" + Request.QueryString("IdDocumento") + "');</script>")
        mostrarReporte()
    End Sub
    'hddTipoDiseno.Value = CStr(objDocMer.crpImprimir(14))

    Private Sub mostrarReporte()
        Dim objDocMer As New Negocio.DocumentosMercantiles
        Select Case ViewState.Item("iReporte").ToString
            Case "16"
                Dim idImp As Integer = objDocMer.crpImprimir(16)
                If idImp = 1 Or idImp = 0 Then ''estandar
                    RptOrdenCompra()
                Else 'personalizado falta
                    RptOrdenCompra()
                End If
            Case "99"
                Dim idImp As Integer = objDocMer.crpImprimir(16)
                If idImp = 1 Or idImp = 0 Then ''estandar
                    RptOrdenCompraNomComercial()
                Else 'personalizado falta
                    RptOrdenCompraNomComercial()
                End If

            Case "47"
                Dim idImp As Integer = objDocMer.crpImprimir(47)
                If idImp = 1 Or idImp = 0 Then ''estandar
                    rptDocControlInterno()
                Else 'personalizado falta
                    rptDocControlInterno()
                End If

            Case "17"
                Dim idImp As Integer = objDocMer.crpImprimir(17)
                If idImp = 1 Or idImp = 0 Then ''estandar
                    rptReciboI()
                Else 'personalizado falta
                    rptReciboI()
                End If
            Case "18"
                Dim idImp As Integer = objDocMer.crpImprimir(18)
                If idImp = 1 Or idImp = 0 Then ''estandar
                    rptReciboE()
                Else 'personalizado falta
                    rptReciboE()
                End If

            Case "4"
                Dim idImp As Integer = objDocMer.crpImprimir(4)
                If idImp = 1 Or idImp = 0 Then ''estandar
                    RptNotaCredito()
                Else 'personalizado falta
                    RptNotaCredito()
                End If

            Case "1"
                'rptDocFact(objDocMer.crpImprimir(1))
                rptDocFact(CInt(ViewState("tipoimpresion")))
            Case "3"
                'rptDocBol(objDocMer.crpImprimir(3))
                rptDocBol(CInt(ViewState("tipoimpresion")))

            Case "5"
                Dim idImp As Integer = objDocMer.crpImprimir(5)
                rptDocNotaDebito(idImp)

            Case "15"
                Dim idImp As Integer = objDocMer.crpImprimir(15)
                If idImp = 1 Or idImp = 0 Then ''estandar
                    rptDocPedidoCliente(15)
                Else ' personalizado falta
                    rptDocPedidoCliente(15)
                End If
            Case "56"
                Dim idImp As Integer = objDocMer.crpImprimir(56)
                If idImp = 1 Or idImp = 0 Then ''estandar
                    rptDocPedidoCliente(56)
                Else ' personalizado falta
                    rptDocPedidoCliente(56)
                End If

            Case "14"
                RptCotizacion(CInt(ViewState("tipoimpresion")))

            Case "6"
                ''Dim idImp As Integer = objDocMer.crpImprimir(6)
                rptDocGuiasRemitente(CInt(ViewState("tipoimpresion")))


            Case "25"
                Dim idImp As Integer = objDocMer.crpImprimir(25)
                If idImp = 1 Or idImp = 0 Then ''estandar
                    rptDocGuiasRecepcion()
                Else 'personalizado falta
                    rptDocGuiasRecepcion()
                End If

            Case "33"
                RptCosteoImportacion()

            Case "2101100003"
                RptCosteoProyectadoImportacion()

        End Select
    End Sub


    Private Sub RptCosteoImportacion()
        Try

            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            reporte = New CR_PRUEBA

            reporte.SetDataSource((New Negocio.DocumentosMercantiles).RptCosteoImportacion(IdDocumento))
            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()

        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message + "');</script>")

        End Try
    End Sub


    Private Sub RptCosteoProyectadoImportacion()
        Try

            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            reporte = New CR_PRUEBA

            reporte.SetDataSource((New Negocio.DocumentosMercantiles).RptCosteoProyectadoImportacion(IdDocumento))
            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()

        Catch ex As Exception
            Response.Write("<script>alert('" + ex.Message + "');</script>")

        End Try
    End Sub

    Private Sub RptKardexValorizado()
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            reporte = New CR_DocReciboI

            reporte.SetDataSource((New Negocio.DocumentosMercantiles).ReporteDocReciboI(IdDocumento))

            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub rptKardexFisico()

    End Sub

    Private Sub rptReciboI()
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            reporte = New CR_DocReciboI

            reporte.SetDataSource((New Negocio.DocumentosMercantiles).ReporteDocReciboI(IdDocumento))

            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")



        End Try
    End Sub
    Private Sub rptReciboE()
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            reporte = New CR_DocReciboE

            reporte.SetDataSource((New Negocio.DocumentosMercantiles).ReporteDocReciboE(IdDocumento))

            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")



        End Try

    End Sub
    Private Sub RptCotizacion(ByVal tipoImpresion As Integer)
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            Select Case tipoImpresion
                Case 1 Or 0
                    reporte = New CR_DoctCotizacion
                    reporte.SetDataSource((New Negocio.DocumentosMercantiles).getReporteDocCotizacion(IdDocumento))
                Case 2
                    reporte = New CR_rptCotizacion
                    reporte.SetDataSource((New Negocio.Reportes).getReporteDocCotizacion(IdDocumento, tipoImpresion))
                Case 3
                    reporte = New CR_DoctCotizacionAgrup
                    reporte.SetDataSource((New Negocio.Reportes).getReporteDocCotizacion(IdDocumento, tipoImpresion))
            End Select

            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")

        End Try
    End Sub
    Private Sub RptNotaCredito()
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            reporte = New CR_DocNotaCreditoPers
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).RptDocNotaCredito(IdDocumento))

            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")



        End Try
    End Sub
    Private Sub RptOrdenCompraNomComercial()
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            reporte = New CR_DocOrdenCompraM
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).getDataSetOrdenCompraNomComercial(IdDocumento))

            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")



        End Try
    End Sub
    Private Sub RptOrdenCompra()
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            reporte = New CR_DocOrdenCompraM
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).getDataSetOrdenCompra(IdDocumento))

            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")



        End Try
    End Sub

    Private Sub rptDocFact(ByVal tipoimpresion As Integer)
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            Select Case tipoimpresion
                Case 1 Or 0
                    reporte = New CR_DocFactBolPers2
                Case 2
                    reporte = New CR_DocFactBolPers
                Case 3
                    reporte = New CR_DocFactBolAgrup
            End Select

            Dim ds As DataSet = (New Negocio.DocumentosMercantiles).rptDocFactBol(IdDocumento, tipoimpresion)

            reporte.SetDataSource(ds)

            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")


        End Try
    End Sub
    Private Sub rptDocBol(ByVal tipoImpresion As Integer)
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            Select Case tipoImpresion
                Case 1 Or 0 'estandar
                    reporte = New CR_DocBol
                Case 2 'personalizado
                    reporte = New CR_DocFactBolPers1  'reporte = New CR_DocBolPers

                Case 3
                    reporte = New CR_DocBolAgrup
            End Select
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocFactBol(IdDocumento, tipoImpresion))
            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")


        End Try
    End Sub

    Private Sub rptDocNotaDebito(ByVal tipoImpresion As Integer)
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))
            reporte = New ReportDocument
            Select Case tipoImpresion

                ' Case 1 Or 0 'estandar
                ' reporte = New CR_DocNotaDebito1

                Case 1 Or 0 'personalizado
                    reporte = New CR_DocNotaDebitoEst


            End Select
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocNotaDebito(IdDocumento))

            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")



        End Try
    End Sub
    Private Sub rptDocPedidoCliente(ByVal IdtipoDocumento As Integer)


        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))
            Dim Etiqueta As String = ""

            reporte = New CR_DocPedidoCliente
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocPedidoCliente(IdDocumento))

            Select Case IdtipoDocumento
                Case 15
                    Etiqueta = "Documento Referencia"
                Case 56
                    Etiqueta = "Programación Calendario"
            End Select

            reporte.SetParameterValue("@Etiqueta", Etiqueta)
            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")



        End Try
    End Sub
    Private Sub rptDocGuiasRemitente(ByVal timp As Integer)
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))
            Select Case timp
                Case 1 '
                    reporte = New CR_DocGuiaRemisionRemitentePers2
                    reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocGuias(IdDocumento))
                Case 2 'personalizado
                    reporte = New CR_DocGuiaRemisionRemitentePers
                    reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocGuias(IdDocumento))
                Case 3 'por tonos
                    reporte = New CR_GuiaxTonos
                    reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptGuiaxTono(IdDocumento))
                Case 4 'por tonos
                    reporte = New CR_DocGuiaRemisionRemitentePers3
                    reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocGuias(IdDocumento))

            End Select

            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")



        End Try

    End Sub
    Private Sub rptDocControlInterno()
        Try
            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            reporte = New CR_DocControlInterno
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocControlInterno(IdDocumento))

            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")



        End Try

    End Sub

    Private Sub rptDocGuiasRecepcion()
        Try

            Dim IdDocumento As Integer = CInt(ViewState("IdDocumento"))

            reporte = New CR_DocGuiaRecepcionn
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocGuias(IdDocumento))

            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()

        Catch ex As Exception
            Response.Write("<script>alert(" + ex.Message.ToString() + ");</script>")

        End Try

    End Sub

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reporte.Close()
        reporte.Dispose()
    End Sub

End Class