<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmEmisionCheque.aspx.vb" Inherits="APPWEB.FrmEmisionCheque" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscar">
       <%-- <table width="100%">
            
        </table>--%>
        <table style="width: 100%;">
            <tr>
                <td class="TituloCelda"> 
                Emisi�n de Cheques
                </td>
            </tr>
        </table>
        <br />
        <div>
            <div style="text-align: center;">
                <label class="Label">
             <b>Cuentas Bancarias</b></label>
            </div>
            
            <asp:UpdateProgress ID="UpdateProgressSerieCheque" runat="server" DisplayAfter="1000"
                AssociatedUpdatePanelID="upSerieCheque">
                <ProgressTemplate>
                <label class="Label">Cargando...     
                </label>
                </ProgressTemplate>
                </asp:UpdateProgress>
                
                <asp:UpdatePanel  ID="upSerieCheque" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="overflow: auto; width: 100%; height: 100px; text-align: center;">
                        <asp:GridView ID="dgvSerieCheque" runat="server" AutoGenerateColumns="False" CellPadding="2"
                            HorizontalAlign="Center" EnableViewState="True">
                            <Columns>
                                <asp:BoundField DataField="Banco" HeaderText="Banco" />
                                <asp:BoundField DataField="DescCuentaBancaria" HeaderText="Cuenta Bancaria" />
                                <asp:BoundField DataField="Serie" HeaderText="Serie" NullDisplayText="" />
                                <asp:BoundField DataField="DescNroInicio" HeaderText="Nro. Inicio" />
                                <asp:BoundField DataField="DescNroFin" HeaderText="Nro. Fin" />
                                <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" 
                                    NullDisplayText="" />
                                <asp:BoundField HeaderText="Saldo Contable" DataField="DescSaldoContable" SortExpression="DescSaldoContable" DataFormatString="{0:n}"
                                    NullDisplayText="" />
                                <asp:BoundField DataField="DescSaldoDisponible" HeaderText="Saldo Disponible" 
                                    NullDisplayText="" />
                                <%--<asp:BoundField DataField="DescEstado" HeaderText="Estado" NullDisplayText="0" />--%>
                            </Columns>
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <HeaderStyle CssClass="GrillaHeader" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
               <asp:AsyncPostBackTrigger ControlID="cboBanco" EventName="SelectedIndexChanged" />
               <asp:AsyncPostBackTrigger ControlID="cboCuentaBancaria" EventName="SelectedIndexChanged" />
               <asp:AsyncPostBackTrigger ControlID="cboSerieNumeracion" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <table style="width: 100%;">
                        <tr class="SubTituloCelda">
                            <td>
                            B�SQUEDA DE CHEQUES
                            </td>
                        </tr>
               <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='/Imagenes/Nuevo_A.JPG';"
                                    onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" />  
                                <asp:ImageButton ID="btnBuscar" runat="server" 
                                    ImageUrl="~/Imagenes/Buscar_b.JPG"
                                    onmouseover="this.src='/Imagenes/Buscar_A.JPG';" 
                                    onmouseout="this.src='/Imagenes/Buscar_B.JPG';" />
                                  <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                    onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                    OnClientClick="return(validarSave());" CausesValidation="true" />
                                <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                    onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';"
                                    OnClientClick="return(confirm('Desea cancelar el proceso?'));" />
                                <asp:ImageButton ID="btnImprimir" runat="server" ImageUrl="~/Imagenes/Imprimir_B.JPG"
                                    onmouseover="this.src='/Imagenes/Imprimir_A.JPG';" onmouseout="this.src='/Imagenes/Imprimir_B.JPG';"
                                    OnClientClick="return(  valOnClick_btnImprimir()  );" 
                                    ToolTip="Imprimir Documento" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>
        <fieldset class="FieldSetPanel ">
         <legend>
                <asp:Label ID="lblTituloPnlDatos" runat="server" Text="B�squeda"></asp:Label>
            </legend>
            <table style="width: 100%;">
                <tr>
                    <td>
                    </td>
                    <td colspan="5">
                        <asp:Button ID="btnAbrirCapaProgramacionPago" runat="server" Text="Programacion de Pagos" Visible="false" 
                            ToolTip="Buscar Programaciones de Pago" OnClientClick="return( valOnClick_btnBuscar_DocumentoRef() );" />
                    </td>
                </tr>
                        <tr>
                    <td class="Label_fsp">
                        <asp:Label ID="lblFechaACobrarInicial" runat="server" 
                            Text="Fecha de Cobro Inicial:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFechaACobrarInicial" runat="server" 
                            onblur="return(valFecha(this));"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="txtFechaACobrarInicial_MaskedEditExtender" runat="server"
                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                            CultureTimePlaceholder="" Enabled="True" TargetControlID="txtFechaACobrarInicial"
                            ClearMaskOnLostFocus="false" Mask="99/99/9999">
                        </cc1:MaskedEditExtender>
                        <cc1:CalendarExtender ID="txtFechaACobrarInicial_CalendarExtender" runat="server"
                            TargetControlID="txtFechaACobrarInicial" Enabled="True" 
                            Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                    </td>
                    
                    <td class="Label_fsp">
                        <asp:Label ID="lblFechaACobrarFinal" runat="server" 
                            Text="Fecha de Cobro Final:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFechaACobrarFinal" runat="server" 
                            onblur="return(valFecha(this));"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="txtFechaACobrarFinal_MaskedEditExtender" runat="server"
                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                            CultureTimePlaceholder="" Enabled="True" TargetControlID="txtFechaACobrarFinal"
                            ClearMaskOnLostFocus="false" Mask="99/99/9999">
                        </cc1:MaskedEditExtender>
                        <cc1:CalendarExtender ID="txtFechaACobrarFinal_CalendarExtender" runat="server" TargetControlID="txtFechaACobrarFinal"
                            Enabled="True" Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                    </td>
                    <td>
                    </td>
                    <td></td>
           
                </tr>
                <tr>
                    <td class="Label_fsp">
                        <asp:Label ID="lblBanco" runat="server" Text="Banco:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="cboBanco" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td class="Label_fsp">
                        <asp:Label ID="lblCuentaBancaria" runat="server" Text="Cuenta Bancaria:"></asp:Label>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="upCuentaBancaria" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:DropDownList ID="cboCuentaBancaria" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="cboBanco" 
                            EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                    <td class="Label_fsp">
                        <asp:Label ID="lblEstadoDoc" runat="server" Text="Estado del Documento:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="cboEstadoDoc" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="Label_fsp">
                        <asp:Label ID="lblcboSerieNumeracion" runat="server" 
                            Text="Serie /  Numeraci�n:"></asp:Label>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="upcboSerieNumeracion" runat="server" 
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:DropDownList ID="cboSerieNumeracion" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="cboCuentaBancaria" 
                                    EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="cboBanco" 
                            EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                    <td class="Label_fsp">
                        <asp:Label ID="lblNumeroCheque" runat="server" Text="Nro. Cheque:"></asp:Label>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="upNumeroCheque" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="txtNumeroCheque" runat="server"></asp:TextBox>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="cboCuentaBancaria" 
                                    EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="cboSerieNumeracion" 
                                    EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                    <td class="Label_fsp">
                        <asp:Label ID="lblEstadoEntrega" runat="server" Text="Estado de Entrega:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="cboEstadoEntregado" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="Label_fsp">
                        <asp:Label ID="lblMonto" runat="server" Text="Monto:"></asp:Label>
                        <asp:UpdatePanel ID="upMonedaSymbol" runat="server" UpdateMode="Conditional" 
                            RenderMode="Inline">
                            <ContentTemplate>
                                <asp:Label ID="lblMoneda" runat="server" Text=""></asp:Label>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="cboCuentaBancaria" 
                                    EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                    <td>
                        <asp:TextBox ID="txtMonto" runat="server" onblur="return(valBlur(event));"></asp:TextBox>
                    </td>
                    <td class="Label_fsp">
                        <asp:Label ID="lblNumeroVoucher" runat="server" Text="Nro. Voucher:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNumeroVoucher" runat="server"></asp:TextBox>
                    </td>
                    <td class="Label_fsp">
                        <asp:Label ID="lblEstadoCancelacion" runat="server" 
                            Text="Estado de Cancelaci�n:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="cboEstadoCancelacion" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="Label_fsp">
                        <asp:Label ID="lblFechaACobrar" runat="server" Text="Fecha de Cobro:"></asp:Label>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="upFechaACobrar" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="txtFechaACobrar" runat="server" 
                                    onblur="return(valFecha(this));"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaACobrar_MaskedEditExtender" runat="server" CultureAMPMPlaceholder=""
                                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                    CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                    Enabled="True" TargetControlID="txtFechaACobrar" ClearMaskOnLostFocus="false"
                                    Mask="99/99/9999">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaACobrar_CalendarExtender" runat="server" TargetControlID="txtFechaACobrar"
                                    Enabled="True" Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="Label_fsp">
                        <asp:Label ID="lblFechaEmision" runat="server" Text="Fecha de Emision:"></asp:Label>
                    </td>
                    <td>
                        <asp:UpdatePanel ID="upFechaEmision" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="txtFechaEmision" runat="server" onblur="return(valFecha(this));"
                                    BackColor="White"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" CultureAMPMPlaceholder=""
                                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                    CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                    Enabled="True" TargetControlID="txtFechaEmision" ClearMaskOnLostFocus="false"
                                    Mask="99/99/9999">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" TargetControlID="txtFechaEmision"
                                    Enabled="True" Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
        
                <tr>
                    <td colspan="6">
                        <asp:Label ID="lblMensajeInformativo" runat="server" Text=""></asp:Label>
                    </td>
                    </tr>
               <%-- </tr>
                <table style="width: 100%;">
                    <tr>
                        <td class="TituloCelda">
                            BENEFICIARIO
                        </td>
                    </tr>
                </table>
                </tr>--%><tr>
                    <td colspan="6">
                        <asp:Panel ID="Panel_PersonaBenef" runat="server">
                            <asp:UpdatePanel ID="upPersonaBenef" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <fieldset class="FieldSetPanel">
                                        <legend>Beneficiario</legend>
                                        <table style="width: 60%">
                                            <tr>
                                                <td class="Label_fsp">
                                                    <asp:Label ID="lblTextBeneficiario" runat="server" Text="Beneficiario:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblBeneficiario" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                </td>
                                             
                                               <%-- <td>
                                                <asp:Button ID="btnBuscarCliente" runat="server" Text="Documento Beneficiario" Width="120px"
                                                    ToolTip="Buscar Beneficiario" OnClientClick=" return( mostrarCapaPersona() ); " />
                                                </td>--%><td>
                                                    <asp:Button ID="btnBuscarCliente" runat="server" Text="Buscar Beneficiario"
                                                        ToolTip="Buscar Beneficiario"  
                                                           OnClientClick=" return( mostrarCapaPersona() ); "/>
                                                    </td>
                                                 <%--  <td>
                                                    <asp:ImageButton ID="btnBuscarCliente" runat="server" 
                                                        ImageUrl="../Imagenes/Buscar_b.JPG" 
                                                        OnClientClick="return(mostrarCapaPersona());" 
                                                        onmouseout="this.src='../Imagenes/Buscar_b.JPG';" 
                                                        onmouseover="this.src='../Imagenes/Buscar_A.JPG';" TabIndex="13" />
                                                </td>--%></tr>
                                            <tr>
                                                <td class="Label_fsp">
                                                    <asp:Label ID="lblTextDNI" runat="server" Text="DNI:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDNI" runat="server"></asp:Label>
                                                    <%--<asp:TextBox ID="txtDNI" runat="server"></asp:TextBox>--%></td>
                                            </tr>
                                            <tr>
                                                <td class="Label_fsp">
                                                    <asp:Label ID="lblTextRUC" runat="server" Text="RUC:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblRUC" runat="server"></asp:Label>
                                                    <%--<asp:TextBox ID="txtRUC" runat="server"></asp:TextBox>--%></td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="gvBuscar" 
                                        EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </fieldset>
        <table width="100%">
          <tr class="SubTituloCelda">
                                <td>
                            DOCUMENTO REFERENCIADO
                                </td>
                        </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocumentoRef" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:Button ID="btnBuscarDocumento_Ref" runat="server" Text="Documento Ref." Width="120px"
                                        ToolTip="Buscar documento de referencia." 
                                        OnClientClick=" return( valOnClick_btnBuscarDocumento_Ref() ); " />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" 
                                        Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SelectText="Quitar" 
                                                ShowSelectButton="True" />
                                            <asp:BoundField DataField="NomTipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Documento" ItemStyle-Font-Bold="true" 
                                                ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                                ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDocumento" runat="server" 
                                                                    Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                                            </td>
                                                             <td>
                                                                <asp:HiddenField ID="hddIddo" runat="server" 
                                                                     Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Emisi�n" 
                                                ItemStyle-HorizontalAlign="Center" />
                                                
                                                     <asp:BoundField DataField="FechaVenc" 
                                                DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Venc." 
                                                ItemStyle-HorizontalAlign="Center" />
                                                
                                                
                                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMoneda2" runat="server" Font-Bold="true" ForeColor="Red" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbltotal" runat="server"  Font-Bold="true" ForeColor="Red"  
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Total","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                                  <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Saldo" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" ForeColor="Red" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblsaldo" runat="server"  Font-Bold="true" ForeColor="Red" 
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                
                                            
                                                
                                            <asp:BoundField DataField="DescripcionPersona" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Descripci�n" ItemStyle-HorizontalAlign="Center" />
                                                 <asp:BoundField DataField="NomEstadoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Estado" ItemStyle-HorizontalAlign="Center" />
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
      <%--  <table width="100%">
              <tr class="SubTituloCelda">
                            <td>
                               PROGRAMACI�N DE PAGO REFERENCIADO
                            </td>
                        </tr>
        </table>--%>
        <asp:Panel ID="Panel_Observacion" runat="server">
            <table width="100%">
                <tr>
                    <td align="left" class="TituloCeldaLeft">
                        <asp:Panel ID="pnlObservacion_Cabecera" runat="server">
                            <asp:Image ID="ImgObservacion" runat="server" />Detalle
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="pnlObservacion"
                                ImageControlID="ImgObservacion" ExpandedImage="../Imagenes/Menos_B.JPG" CollapsedImage="../Imagenes/Mas_B.JPG"
                                Collapsed="true" CollapseControlID="ImgObservacion" 
                                ExpandControlID="ImgObservacion">
                            </cc1:CollapsiblePanelExtender>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlObservacion" runat="server">
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine" Style="width: 100%;
                                            height: 58px" MaxLength="400"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        
         <table style="width: 100%;">
            <tr>
                <td class="TituloCelda">
                Lista de Cheques Emitidos
                </td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td>
                    <asp:UpdateProgress ID="upGrilla_Progress" runat="server" DisplayAfter="1000" 
                        AssociatedUpdatePanelID="upGrilla">
                        <ProgressTemplate>
                            <label class="Label">
                                    Cargando...</label></ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <asp:UpdatePanel 
                        ID="upGrilla" runat="server" UpdateMode="Conditional"><ContentTemplate>
                            <div style="overflow: auto; width: 1300px; height: 350px">
                                <asp:GridView ID="dgvDatos" runat="server" AutoGenerateColumns="False" Width="100%"
                                    EnableViewState="True">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnEditar" runat="server" OnClick="lbtnEditar_Click">Editar</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                <%--        <asp:BoundField DataField="Banco" HeaderText="Banco" NullDisplayText="" 
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"  />--%>
                                            
                                           <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                              HeaderText="Banco" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblBanco" runat="server" 
                                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Banco")%>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                        
                                        <asp:BoundField DataField="CuentaBancaria" HeaderText="Cuenta Bancaria" 
                                            NullDisplayText="" />
                                     
                                           <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                              HeaderText="Serie" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblNroDocumento" runat="server"  ForeColor="Red" 
                                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Serie")%>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:HiddenField ID="hddId" runat="server" 
                                                                        Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                                </td>
                                                              
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                        <asp:BoundField DataField="ChequeNumero" HeaderText="Nro. Cheque" ItemStyle-ForeColor="Red" 
                                            NullDisplayText="" ItemStyle-HorizontalAlign="Center" 
                                            HeaderStyle-HorizontalAlign="Center"  />
                                        
                                        <asp:BoundField DataField="MonedaSimbolo" HeaderText="Mon." NullDisplayText="" 
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"  />
                                       
                                        <asp:BoundField DataField="DescMonto" HeaderText="Monto" NullDisplayText="" 
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"  
                                             />
                                        
                                        <asp:BoundField DataField="Beneficiario" HeaderText="Beneficiario" 
                                            NullDisplayText=""   />
                                        
                                        <asp:BoundField DataField="DescFechaACobrar" HeaderText="Fecha a Cobrar" ItemStyle-ForeColor="Red" 
                                            NullDisplayText="" ItemStyle-HorizontalAlign="Center" 
                                            HeaderStyle-HorizontalAlign="Center"  />
                                        
                                        <asp:BoundField DataField="DescFechaEmision" HeaderText="Fecha de Emisi�n" ItemStyle-ForeColor="Red" 
                                            NullDisplayText=""  ItemStyle-HorizontalAlign="Center" 
                                            HeaderStyle-HorizontalAlign="Center" />
                                        
                                        <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Est. Documento" 
                                            NullDisplayText=""  ItemStyle-HorizontalAlign="Center" 
                                            HeaderStyle-HorizontalAlign="Center" />
                                        
                                        <asp:BoundField DataField="NomEstadoEntregado" HeaderText="Est. Entrega" 
                                            NullDisplayText="" ItemStyle-HorizontalAlign="Center" 
                                            HeaderStyle-HorizontalAlign="Center"  />
                                        
                                        <asp:BoundField DataField="NomEstadoCancelacion" HeaderText="Est. Cancelaci�n" 
                                            NullDisplayText="" ItemStyle-HorizontalAlign="Center" 
                                            HeaderStyle-HorizontalAlign="Center"  />
                                
                                 
                                         <asp:TemplateField>
                                        <ItemTemplate>
                                        <table>
                                        <tr>
                                        <td>
                                                <asp:Button ID="btnImprimir" Text="Imprimir"   
                                                    OnClientClick="return(  valOnClick_btnImprimir2(this)  );" 
                                                    ToolTip="Imprimir Documento" runat="server" ItemStyle-HorizontalAlign="Center" 
                                                    HeaderStyle-HorizontalAlign="Center" />
                                       </td>
                                        </tr>
                                        </table>
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                      <asp:TemplateField>
                                        <ItemTemplate>
                                        <table>
                                        <tr>
                                        <td>
                                                <asp:Button ID="btnVoucher" Text="Voucher"   
                                                    OnClientClick="return(  valOnClick_btnImprimirVoucher(this)  );" 
                                                    ToolTip="Imprimir Voucher" runat="server" ItemStyle-HorizontalAlign="Center" 
                                                    HeaderStyle-HorizontalAlign="Center" />
                                       </td>
                                        </tr>
                                        </table>
                                        </ItemTemplate>
                                   </asp:TemplateField>
                                    </Columns>
                                    
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
                 <%--                    <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminar_Click">Eliminar</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%><%--  <asp:HiddenField ID="hddId" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>'
                                                    runat="server" />--%>
                                                    </table>
        <asp:HiddenField ID="hddIdDocumento" runat="server" Value="0" />
        
          <asp:HiddenField ID="hddIdProgramacionPago" runat="server" Value="0" />
        
    </asp:Panel>
<%--    <cc1:ModalPopupExtender ID="ModalPopup_ProgramacionPago" runat="server" TargetControlID="btnAbrir_ProgramacionPago"
        PopupControlID="Panel_ProgramacionPago" BackgroundCssClass="modalBackground"
        Enabled="true" RepositionMode="None" CancelControlID="btnClose_ProgramacionPago"  Y="250">
    </cc1:ModalPopupExtender>--%>
    
    
        <div id="CapaProgramacionpPago" style="border: 3px solid blue; padding: 8px; width: 1250px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;">
        <table style="width: 100%;">
            <tr>
                    <td align="right">

                    <asp:ImageButton ID="btnClose_ProgramacionPago" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                    OnClientClick="return(  offCapa('CapaProgramacionpPago')   );" />
                    </td>
                  
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Empresa:
                                 </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa_PP" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                     Tienda:
                                    </td>
                            <td>
                                <asp:DropDownList ID="cboTienda_PP" runat="server">
                                </asp:DropDownList>
                            </td>
                                <td class="Texto">
                                        Moneda:
                                         </td>
                            <td>
                                <asp:DropDownList ID="cboMoneda_PP" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                               
                                       <table>
                                        <tr>
                                            <td class="Texto">
                                            Inicio:
                                            </td>
                                            <td>
                                                <cc1:MaskedEditExtender ID="MaskedEdit_FechaIni_PP" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txt_FechaIni_PP">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="Calendar_FechaIni_PP" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txt_FechaIni_PP">
                                                </cc1:CalendarExtender>
                                                <asp:TextBox ID="txt_FechaIni_PP" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                            </td>
                                            
                                            <td class="Texto">Fin:
                                            </td>
                                          <td>
                                                <cc1:MaskedEditExtender ID="MaskedEdit_FechaFin_PP" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txt_FechaFin_PP">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="Calendar_FechaFin_PP" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txt_FechaFin_PP">
                                                </cc1:CalendarExtender>
                                                <asp:TextBox ID="txt_FechaFin_PP" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnBuscar_PP" runat="server" Text="Buscar" ToolTip="Buscar Programacion de pago" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
<%--                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>--%>
                       <asp:Panel ID="Panel_ProgramacionPago" runat="server">
                       <table>
                       <tr>
                       <td>
                     
                            <asp:GridView ID="GV_ProgramacionPago" runat="server" AutoGenerateColumns="False"
                                Width="96%">
                                <HeaderStyle CssClass="GrillaHeader" />
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    
                                          <asp:BoundField DataField="pp_FechaPagoProg" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha de Pago" 
                                                        ItemStyle-ForeColor="Red" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                        
                                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMoneda_MontoProg" runat="server" ForeColor="Red"
                                                                            Text='<%#DataBinder.Eval(Container.DataItem,"MonedaPP")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblMontoProg" runat="server"  ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"pp_MontoPagoProg","{0:F3}")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdProgramacionPago_CXP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProgramacionPago")%>' />
                                                                    </td>
                                                                      </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                          <asp:BoundField DataField="mp_Nombre" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Medio de Pago" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="ban_Nombre" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Banco" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="cb_numero" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Cuenta Bancaria" ItemStyle-HorizontalAlign="Center" />
                                                        
                                        <asp:BoundField DataField="TipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Tipo Documento"  ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblNroDocumento" runat="server"  ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hddIdMovCtaPP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCtaPP")%>' />
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hddIdProveedor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProveedor")%>' />
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                                    </td>
                                                   <%-- <td>
                                                     <asp:HiddenField  ID="hddIdDocRef" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumentoRef")%>'/>
                                                    </td>--%><td>
                                                <asp:HiddenField ID="hddIdUsuario" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUsuario")%>' />
                                            </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="doc_FechaEmision" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n" 
                                        ItemStyle-HorizontalAlign="Center" />
                                        
                                <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Fecha Vcto." ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblfechaVencimiento" runat="server"  Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaVenc","{0:d}")%>'></asp:Label>
                                            </td>
                                       </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                                     
                                    <asp:BoundField DataField="Proveedor" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Proveedor"  ItemStyle-HorizontalAlign="Center" />
                                        
                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMoneda_Monto"  runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaDoc")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblMonto"  runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Monto","{0:F3}")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Saldo" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMoneda_Saldo" ForeColor="Red"  runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaDoc")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSaldo"  ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Saldo","{0:F3}")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                            
                                <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Ver Doc. Relacionado" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button id="btnMostrar"  runat="server" ToolTip="Ver R/Q" OnClientClick=" return( valOnClick_btnMostrarDetalleOC(this)  ); " Text="Ver Requerimiento" Width="120px" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                                </Columns>
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                              </td>
                       </tr>
                       </table>
                       </asp:Panel>
                            </td>
<%--                        </ContentTemplate>
                      <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar_PP" EventName="Click" />
                        </Triggers>--%>
                 <%--   </asp:UpdatePanel>--%>
  
            </tr>
            <tr>
                <td>
                  <%--  <asp:Image ID="btnOpen_ListaPP" runat="server" Width="1px" Height="1px" />
                    <cc1:ModalPopupExtender ID="ModalPopup_ListaPP" runat="server" TargetControlID="btnOpen_ListaPP"
                        PopupControlID="Panel_ListaPP" BackgroundCssClass="modalBackground" Enabled="true"
                        RepositionMode="None" CancelControlID="btnClose_ListaPP" Y="255">
                    </cc1:ModalPopupExtender>--%>
                    
                    <%--AQUI ESTA LISTA DE PROGRAMACI�N DE PAGO--%>
                <%--    <asp:Panel ID="Panel_ListaPP" runat="server" CssClass="modalPopup" Style="display: none;"
                        Width="600px">
                        <table width="100%">
                            <tr>
                                <td align="right">
                                    <asp:Image ID="btnClose_ListaPP" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="GV_CXP_PP" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SelectText="Seleccionar" ShowSelectButton="True" />
                                                    <asp:BoundField DataField="FechaPagoProg" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha de Pago" ItemStyle-Font-Bold="true"
                                                        ItemStyle-ForeColor="Red" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMoneda_MontoProg" runat="server" Font-Bold="true" ForeColor="Red"
                                                                            Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblMontoProg" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"MontoPagoProg","{0:F3}")%>'></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdProgramacionPago_CXP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProgramacionPago")%>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdMovCtaPP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCtaPP")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="MedioPago" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Medio de Pago" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Banco" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Banco" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NroCuentaBancaria" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Cuenta Bancaria" ItemStyle-HorizontalAlign="Center" />
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="GV_ProgramacionPago" EventName="SelectedIndexChanged" />
                                            <asp:PostBackTrigger ControlID="GV_CXP_PP" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>--%>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Value="J">Jur�dica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Rol:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:DropDownList ID="cboRol" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Raz�n Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda());" runat="server"
                                                                            Width="450px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        R.U.C.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%" BackColor="Silver">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="C�d." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Raz�n Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="cbotipoDocumento" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisi�n</asp:ListItem>
                        <asp:ListItem Value="1" Selected="True">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hddIdPersona_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                </td>
                                                
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                 <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Venc."
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" /> 
                                    
                                <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblmoneda_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                </td>
                                                 <td>
                                                    <asp:Label ID="lblMonto" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Total","{0:F3}")%>'></asp:Label>
                                                </td>
                                               
                                                
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                    
                                    
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Descripci�n" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />  
                                    
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    
    
    <script language="javascript" type="text/javascript">

        function valOnClick_btnBuscar_DocumentoRef() {

            onCapa('CapaProgramacionpPago');
                return false;
        }

        function valOnClick_btnMostrarDetalle(control) {
            var grilla = document.getElementById('<%=GV_ProgramacionPago.ClientID%>');
            var IdDocumento = 0;
            var IdUsuario = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var boton = rowElem.cells[8].children[0].cells[0].children[0];
                    var hddIdDocumento = rowElem.cells[2].children[0].cells[3].children[0];
                    var hddIdUsuario = rowElem.cells[2].children[0].cells[5].children[0];
                    if (control.id == boton.id) {
                        IdDocumento = parseInt(hddIdDocumento.value);
                        IdUsuario = parseInt(hddIdUsuario.value)
                        break;
                    }


                }
            }

            }
            function valOnClick_btnMostrarDetalleOC(control) {

                var grilla = document.getElementById('<%=GV_ProgramacionPago.ClientID%>');
                var IdDocumento = 0;

                var IdUsuario = '<%= Session("IdUsuario") %>';

                if (grilla != null) {
                    for (var i = 1; i < grilla.rows.length; i++) {
                        var rowElem = grilla.rows[i];
                        var boton = rowElem.cells[13].children[0].cells[0].children[0];
                        var hddIdDocumento = rowElem.cells[7].children[0].cells[3].children[0];
                        if (control.id == boton.id) {
                            IdDocumento = parseInt(hddIdDocumento.value);
                            break;
                        }
                    }
                }
                if (IdDocumento == 0 || isNaN(IdDocumento)) {
                    alert('El documento no tiene Documentos Referenciados. No procede la Operaci�n.');
                    return false;
                } else {
                    window.open('../../Finanzas/FrmRequerimientoGasto_V2.aspx?IdDocumento=' + IdDocumento + '&IdUsuario=' + IdUsuario, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                    return false;
                }
            }

            function valOnClick_btnImprimir2(control) {
                var grilla = document.getElementById('<%=dgvDatos.ClientID%>');
                var IdDocumento = 0;
                var Banco = "";
                if (grilla != null) {
                    for (var i = 1; i < grilla.rows.length; i++) {
                        var rowElem = grilla.rows[i];
                        var boton = rowElem.cells[13].children[0].cells[0].children[0];
                        var hddIdDocumento = rowElem.cells[3].children[0].cells[1].children[0];
                        var Banco = rowElem.cells[1].children[0].cells[0].children[0];
                        
                        if (control.id == boton.id) {
                            IdDocumento = parseInt(hddIdDocumento.value);
                            Banco = Banco.innerText;
                            break;
                        }
                    }
                }

                if (IdDocumento == 0 || isNaN(IdDocumento)) {
                    alert('El documento no tiene Documentos Referenciados. No procede la Operaci�n.');
                    return false;
                } else {
                    window.open('../../Caja/Reportes/Visor1.aspx?iReporte=19&IdDocumento=' + IdDocumento+'&Banco='+Banco , null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                    return false;
                }
            }


            function valOnClick_btnImprimirVoucher(control) {
                var grilla = document.getElementById('<%=dgvDatos.ClientID%>');
                var IdDocumento = 0;
                var Banco = "";
                if (grilla != null) {
                    for (var i = 1; i < grilla.rows.length; i++) {
                        var rowElem = grilla.rows[i];
                        var boton = rowElem.cells[14].children[0].cells[0].children[0];
                        var hddIdDocumento = rowElem.cells[3].children[0].cells[1].children[0];
                        var Banco = rowElem.cells[1].children[0].cells[0].children[0];

                        if (control.id == boton.id) {
                            IdDocumento = parseInt(hddIdDocumento.value);
                            Banco = Banco.innerText;
                            break;
                        }
                    }
                }

                if (IdDocumento == 0 || isNaN(IdDocumento)) {
                    alert('El documento no tiene Documentos Referenciados. No procede la Operaci�n.');
                    return false;
                } else {
                    window.open('../../Caja/Reportes/Visor1.aspx?iReporte=20&IdDocumento=' + IdDocumento + '&Banco=' + Banco, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                    return false;
                }
            }







            function validarSave() {
                var Banco = document.getElementById('<%=cboBanco.ClientID%>');
                var idBanco = Banco.value;
                if (idBanco == 0) {
                    alert('Seleccione un Banco.');

                    return false;
                }
                var CuentaBancaria = document.getElementById('<%=cboCuentaBancaria.ClientID%>');
                var idCuentaBancaria = CuentaBancaria.value;
                if (idCuentaBancaria == 0) {
                    alert('Seleccione una Cuenta Bancaria.');

                    return false;
                }

                var SN = document.getElementById('<%=cboSerieNumeracion.ClientID%>');
                var idSN = SN.value;
                if (idSN == 0) {
                    alert('Seleccione una Serie-Numeraci�n.');

                    return false;
                }

                var Monto = document.getElementById('<%=txtMonto.ClientID%>');
                if (Monto.value.length == 0 || parseInt(Monto.value) == 0) {
                    alert('Debe Ingresar un Monto.');
                    Monto.select();
                    Monto.focus();
                    return false;
                }

                var Beneficiario = document.getElementById('<%=lblBeneficiario.ClientID%>').innerText;
                /*Funciona con IE*/
                if (Beneficiario.length == 0) {
                    var btnBuscarBeneficiario = document.getElementById('<%=btnBuscarCliente.ClientID%>');
                    alert('Debe Ingresar un Beneficiario.');
                    btnBuscarBeneficiario.select();
                    btnBuscarBeneficiario.focus();
                    return false;
                }

                var EstadoEntregado = document.getElementById('<%=cboEstadoEntregado.ClientID%>');
                var idEE = EstadoEntregado.value;
                if (idEE == 0) {
                    alert('Seleccione un Estado de Entrega.');

                    return false;
                }

                var EstadoCancelacion = document.getElementById('<%=cboEstadoCancelacion.ClientID%>');
                var idEC = EstadoCancelacion.value;
                if (idEC == 0) {
                    alert('Seleccione un Estado de Cancelaci�n.');

                    return false;
                }

                return (confirm('Desea Continuar Con la Operaci�n'));
            }

            function valOnClick_btnImprimir() {
                var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
                if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                    alert('No se tiene ning�n Documento para Impresi�n.');
                    return false;
                }
                window.open('../../Caja/Reportes/Visor1.aspx?iReporte=5&IdDocumento=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                return false;
            }
            /*Busqueda Personas ini*/

            function mostrarCapaPersona() {
                onCapa('capaPersona');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }

            function validarbusqueda() {
                var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
                var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
                var control = radio.getElementsByTagName('input');
                if (control[1].checked == true) {
                    tb.select();
                    tb.focus();
                    return false;
                }
                return true;
            }

            function validarCajaBusquedaNumero() {
                var key = event.keyCode;
                if (key == 13) {
                    var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                    boton.focus();
                    return true;
                }
                if (key == 46) {
                    return true;
                } else {
                    if (!onKeyPressEsNumero('event')) {
                        return false;
                    }
                }
            }
            function validarCajaBusqueda() {
                var key = event.keyCode;
                if (key == 13) {
                    var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                    boton.focus();
                    return true;
                }
            }
            function valNavegacion(tipoMov) {

                var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
                if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                    alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                    document.getElementById('<%=tbRazonApe.ClientID%>').select();
                    document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                    return false;
                }
                switch (tipoMov) {
                    case '0':   //************ anterior
                        if (index <= 1) {
                            alert('No existen p�ginas con �ndice menor a uno.');
                            return false;
                        }
                        break;
                    case '1':
                        break;
                    case '2': //************ ir
                        index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                        if (isNaN(index) || index == null || index.length == 0) {
                            alert('Ingrese una P�gina de navegaci�n.');
                            document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                            document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                            return false;
                        }
                        if (index < 1) {
                            alert('No existen p�ginas con �ndice menor a uno.');
                            document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                            document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                            return false;
                        }
                        break;
                }
                return true;
            }
            /*Busqueda Personas fin*/

            function valOnClick_btnBuscarDocumento_Ref() {

                var lblDescripcionPersona = document.getElementById('<%=lblBeneficiario.ClientID%>');

                if (lblDescripcionPersona.innerHTML.length <= 0) {
                    alert('DEBE SELECCIONAR UN BENEFICIARIO. NO SE PERMITE LA OPERACI�N.');
                    return false;
                } else {
                    onCapa('capaDocumentosReferencia');
                    return false;
                }

                return false;
            }
            function valOnClick_btnAceptarBuscarDocRefxCodigo() {
                var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
                if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                    alert('Ingrese un Nro. de Serie para la b�squeda.');
                    txtSerie.select();
                    txtSerie.focus();
                    return false;
                }
                var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
                if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                    alert('Ingrese un Nro. de Documento para la b�squeda.');
                    txtCodigo.select();
                    txtCodigo.focus();
                    return false;
                }
                return true;
            }

//            function onClick_ProgramacionPago() {
//                var modalPopupBehavior = $find('ctl00_ContentPlaceHolder1_ModalPopup_ProgramacionPago');
//                modalPopupBehavior.show();
//                return false;
//            }
    </script>

</asp:Content>
