﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmListaDocumentoCancSustentar
    Inherits System.Web.UI.Page


    Private objScript As New ScriptManagerClass




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            ConfigurarDatos()
            inicializarFrm()
        End If

    End Sub

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Private Property IdPersona() As Integer
        Get
            Return CInt(hddIdPersona.Value)
        End Get
        Set(ByVal value As Integer)
            hddIdPersona.Value = CStr(value)
        End Set
    End Property
#Region "****************Buscar Personas Mantenimiento"
    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo
                ''

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)

            End With

            Me.txtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaFin.Text = Me.txtFechaInicio.Text
            Me.IdPersona = 0
            verFrm()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub verFrm()

        LimpiarFrm()

    End Sub
    Private Sub LimpiarFrm()

        ''************* CABECERA

        Me.txtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaFin.Text = Me.txtFechaInicio.Text

    End Sub
    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 0)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)
        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersona IsNot Nothing) Then
            Me.txtDescripcionPersona.Text = objPersona.Descripcion
            Me.txtDni.Text = objPersona.Dni
            Me.txtRuc.Text = objPersona.Ruc
            Me.IdPersona = objPersona.IdPersona
        Else
            Throw New Exception("NO SE HALLARON REGISTROS.")
        End If
        objScript.offCapa(Me, "capaPersona")
    End Sub

#End Region


    Protected Sub btnBuscarBanco_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarBanco.Click

        Dim obj As Entidades.Documento_MovCuentaPorPagar = New Entidades.Documento_MovCuentaPorPagar
        obj.FechaInicio = CDate(txtFechaInicio.Text)
        obj.FechaFin = CDate(txtFechaFin.Text)
        obj.IdTienda = CInt(cboTienda.SelectedValue)
        Dim lista As List(Of Entidades.Documento_MovCuentaPorPagar) = (New Negocio.MovCuentaPorPagar).SelectDocumentosCancelacionBancosxSustentar(IdPersona, obj.FechaInicio, obj.FechaFin, obj.IdTienda)
        If (lista.Count <= 0) Then
            objScript.mostrarMsjAlerta(Me, "No se encontrarón registros..")
            Me.GV_Deudas.DataSource = Nothing
            Me.GV_Deudas.DataBind()
        Else
            Me.GV_Deudas.DataSource = lista
            Me.GV_Deudas.DataBind()
        End If


    End Sub

    Protected Sub GV_Deudas_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GV_Deudas.RowCommand

        If (e.CommandName = "Insert") Then
            Dim index As Integer = CInt(e.CommandArgument)
            visualizarCapaAprobacion(index, 1)
            registrarAprobacion()

        End If
    End Sub
    Private Sub registrarAprobacion()

        Try
            Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)
            Dim objDocumento As Entidades.Documento = New Entidades.Documento
            objDocumento.IdDocumento = IdDocumento
            objDocumento.FactorMov = 1
            Dim objx As Boolean = (New Negocio.DocCancelacion).UpdateSustentoxIdDocumento(objDocumento)

            If (objx = False) Then
                objScript.mostrarMsjAlerta(Me, "El documento se sustento correctamente.")

                Dim obj As Entidades.Documento_MovCuentaPorPagar = New Entidades.Documento_MovCuentaPorPagar
                obj.FechaInicio = CDate(txtFechaInicio.Text)
                obj.FechaFin = CDate(txtFechaFin.Text)
                obj.IdTienda = CInt(cboTienda.SelectedValue)

                GV_Deudas.DataSource = Nothing
                GV_Deudas.DataBind()


                Dim lista As List(Of Entidades.Documento_MovCuentaPorPagar) = (New Negocio.MovCuentaPorPagar).SelectDocumentosCancelacionBancosxSustentar(IdPersona, obj.FechaInicio, obj.FechaFin, obj.IdTienda)

                If (lista.Count <= 0) Then
                    objScript.mostrarMsjAlerta(Me, "No se encontrarón registros..")
                Else
                    Me.GV_Deudas.DataSource = lista
                    Me.GV_Deudas.DataBind()
                End If
            Else
                objScript.mostrarMsjAlerta(Me, "No procede la operación. Error de inserción.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub visualizarCapaAprobacion(ByVal index As Integer, ByVal codigo As Integer)
        Try

            Dim IdDocumento As Integer = CInt(CType(Me.GV_Deudas.Rows(index).FindControl("hddIdDocumentoRef"), HiddenField).Value)
            Me.hddIdDocumento.Value = CStr(IdDocumento)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class