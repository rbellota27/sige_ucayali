﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmExportarDocProvisionados.aspx.vb" Inherits="APPWEB.frmExportarDocProvisionados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    <table>
        <tr>
            <td class="TituloCelda" colspan="10">
                <span>EXPORTAR DOCUMENTOS PROVISIONADOS AL SISTEMA SIGE</span>
            </td>
        </tr>
        <tr>
            <td class="Texto">Empresa: </td>
            <td>
                <asp:DropDownList ID="ddlEmpresa" runat="server" Height="16px">
                <asp:ListItem Value="001" Text="Sanicenter"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="Texto" style="width: 80px">Periodo:</td>
            <td style="width: 139px"><asp:TextBox ID="txtPeriodo" runat="server" Width="58px"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="Texto">Fecha Inicio:</td>
            <td>
            <asp:TextBox ID="txtFechaInicio" runat="server" Text=""></asp:TextBox>
            <cc1:MaskedEditExtender ID="txtDesde_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                                </cc1:MaskedEditExtender>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaInicio" Format="dd/MM/yyyy" >
            </cc1:CalendarExtender>
            </td>
            <td class="Texto" style="width: 80px">Fecha Fin:</td>
            <td style="width: 139px">
            <asp:TextBox ID="txtFechaFinal" runat="server" Text=""></asp:TextBox>
            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFinal">
                                </cc1:MaskedEditExtender>
                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtFechaFinal" Format="dd/MM/yyyy" >
            </cc1:CalendarExtender>
            </td>
            <td>
<%--                <asp:Button ID="btnExportar" runat="server" Text="Exportar" OnClientClick="this.disabled=true;this.value='Procesando...'" 
                UseSubmitBehavior="false" CssClass="btnBuscar" />--%>
                <input id="btnbuscarprovisiconados" type="button" 
                    value="Buscar Documentos Provisionados" class="btnBuscar" 
                    onclick="return enviarDatos(this,'L');" />
                <input id="btntransferirDocumentos" type="button" value="Importar Documentos Provisionados" class="btnBuscar" onclick="return ImportarDocumentos(this,'I');"/>
            </td>
        </tr>
    </table>
</div>
<div>
<table>
    <tr>
        <td class="SubTituloCelda">
            <span>LISTA DE DOCUMENTOS PROVISIONADOS PROCESADOS CORRECTAMENTE</span>
        </td>
    </tr>
    <tr>
        <td>
            <span class="TextoRojoMedium">Nro Registros:</span><span id="nroFilas" style="font-weight:bold"></span>
            <div id="docProvisionados"></div>
        </td>
    </tr>
</table>
</div>
<script type="text/javascript">
    var filas;
    var matriz = [];
   
    function enviarDatos(objeto, flag) {
      
        var empresa = '001';
        var fechaInicio = document.getElementById("<%=txtFechaInicio.ClientID %>").value;
        var FechaFinal = document.getElementById("<%=txtFechaFinal.ClientID %>").value;
        var periodo = document.getElementById("<%=txtPeriodo.ClientID %>").value;

        var xhrT = new XMLHttpRequest();
        xhrT.open("POST", "frmExportarDocProvisionados.aspx?flag=" + flag + "&empresa=" + empresa + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&periodo=" + periodo, true);
        xhrT.onloadstart = function () { objeto.disabled = true; objeto.value = "Procesando..."; }
        xhrT.onloadend = function () { objeto.disabled = false; objeto.value = "Buscar Documentos Provisionados "; }

        xhrT.onreadystatechange = function () {
            if (xhrT.readyState == 4 && xhrT.status == 200) {
                fun_lista(xhrT.responseText);
            }
        }
        xhrT.send();
        return false;
    }

    function ImportarDocumentos(objeto, flag) {
        var empresa = '001';
        var fechaInicio = document.getElementById("<%=txtFechaInicio.ClientID %>").value;
        var FechaFinal = document.getElementById("<%=txtFechaFinal.ClientID %>").value;
        var periodo = document.getElementById("<%=txtPeriodo.ClientID %>").value;

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "frmExportarDocProvisionados.aspx?flag=" + flag + "&empresa=" + empresa + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&periodo=" + periodo, true);
        xhr.onloadstart = function () { objeto.disabled = true; objeto.value = "Procesando..."; }
        xhr.onloadend = function () { objeto.disabled = false; objeto.value = "Buscar Documentos Provisionados "; }

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                fun_lista(xhr.responseText);
            }
        }
        xhr.send();
        return false;
    }

    function fun_lista(lista) {
        filas = lista.split("▼");
        crearTabla();
        crearMatriz();
        mostrarMatriz();
        configurarFiltros();
    }
    function crearTabla() {
        var nRegistros = filas.length;
        var cabeceras = ["RUC", "RAZÓN SOCIAL", "NÚMERO", "FECHA","TC","CONCEPTO","DETALLE","PERIODO","VOUCHER","DETRACCIÓN","IDDOCUMENTO"];
        var nCabeceras = cabeceras.length;
        var contenido = "<table><thead>";
        //Crear la Fila con la Cabecera
        contenido += "<tr class='GrillaHeader'>";
        for (var j = 0; j < nCabeceras; j++) {
            contenido += "<th>";
            //contenido += cabeceras[j];
            contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
            contenido += "</th>";
        }
        contenido += "</tr></thead><tbody id='tbDocumentos' class='GrillaRow'></tbody>";
        contenido += "</table>";
        var div = document.getElementById("docProvisionados");
        div.innerHTML = contenido;
    }

    function crearMatriz() {
        matriz = [];
        var nRegistros = filas.length;
        var nCampos;
        var campos;
        var c = 0;
        var exito;
        var textos = document.getElementsByClassName("texto");
        var nTextos = textos.length;
        var texto;
        var x;
        for (var i = 0; i < nRegistros; i++) {
            exito = true;
            campos = filas[i].split("|");
            nCampos = campos.length;
            for (var j = 0; j < nTextos; j++) {
                texto = textos[j];
                x = j;
                //if (j > 1) { x = j + 2; }
                //else {x = j; }
                //x = j + 2;
                //else x = j;
                if (x < 10) {
                    if (texto.value.length > 0) {
                        exito = campos[x].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                    }
                }
                //exito = exito && campos[j].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                if (!exito) break;
            }
            if (exito) {
                matriz[c] = [];
                for (var j = 0; j < nCampos; j++) {
                    if (isNaN(campos[j])) matriz[c][j] = campos[j];
                    else matriz[c][j] = campos[j] * 1;
                }
                c++;
            }
        }
    }

    function mostrarMatriz() {
        var nRegistros = matriz.length;
        var contenido = "";
        if (nRegistros > 0) {
            var nCampos = matriz[0].length;
            for (var i = 0; i < nRegistros; i++) {
                contenido += "<tr class='GrillaRow'>";
                for (var j = 0; j < nCampos; j++) {
                    //if (!(j == 2 || j == 3)) {
                        contenido += "<td>";
                      //  if (j == 0) {
                            //contenido += "<a href='#''' id='" + matriz[i][j] + "' class='links'>";
                            contenido += matriz[i][j];
                            //contenido += "</a>";
                        //}
                        //else contenido += matriz[i][j];
                        contenido += "</td>";
                    //}
                }
                contenido += "</tr>";
            }
        }
        var spnMensaje = document.getElementById("nroFilas");
        spnMensaje.innerHTML = "<b>Total de Registros: " + (nRegistros) + "</b>";
        var tabla = document.getElementById("tbDocumentos");
        tabla.innerHTML = contenido;
    }

    function configurarFiltros() {
        var textos = document.getElementsByClassName("texto");
        var nTextos = textos.length;
        var texto;
        for (var j = 0; j < nTextos; j++) {
            texto = textos[j];
            texto.onkeyup = function () {
                crearMatriz();
                mostrarMatriz();
            }
        }
    }
function btntransferirDocumentos_onclick() {

}

function btntransferirDocumentos_onclick() {

}

function btntransferirDocumentos_onclick() {

}

function btntransferirDocumentos_onclick() {

}

function btntransferirDocumentos_onclick() {

}

</script>
</asp:Content>

