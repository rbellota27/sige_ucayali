﻿Imports Entidades
Imports Negocio
Imports general.librerias.serializador
Imports System.Linq
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports
Imports Entidades.RequerimientoGasto

Imports System.Net
Imports System.Net.Mail

Partial Public Class FrmAprobacionPagoRequerimientos
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
  
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.TxtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.TxtFechaFin.Text = Me.TxtFechaInicio.Text

        Dim flag As String = Request.QueryString("flag")
        Select Case flag

            Case "llenarComboTienda"
                Dim IdEmpresa As String = Request.QueryString("IdEmpresa")
                Dim rpta As String = ""


                Dim lista As New List(Of be_Tienda)
                Try

                    lista = (New bl_Tienda).selectTiendasN(Convert.ToInt32(IdEmpresa))
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rpta)
                    Response.End()
                Else
                    Response.Write(rpta)
                    Response.End()
                End If

            Case "llenarComboArea"
                Dim IdEmpresa As String = Request.QueryString("IdEmpresa")
                Dim rptaA As String = ""
                Dim listaA As New List(Of be_Area)
                Try
                    listaA = (New bl_Area).selectArea(Convert.ToInt32(IdEmpresa))
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If listaA.Count > 0 Then
                    Try
                        rptaA = (New objeto).SerializarLista(listaA, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rptaA)
                    Response.End()
                Else
                    Response.Write(rptaA)
                    Response.End()
                End If
            Case "ConsultaDocumentosaExportar"

                Dim FechaInicio As String = Request.QueryString("FechaInicio")
                Dim FechaFinal As String = Request.QueryString("FechaFinal")
                Dim idtienda As String = Request.QueryString("idtienda")
                Dim idAprobado As String = Request.QueryString("idAprobado")
                Dim idArea As String = Request.QueryString("idArea")

                Dim rptaDoc As String = ""
                Dim listaDoc As New List(Of be_PagosDocumentosExport)
                Try
                    'If idAprobado <> 4 Then
                    '    listaDoc = (New bl_Pagosdocumentos).selectDocumentos(1, idtienda, idArea, idAprobado, FechaInicio, FechaFinal, 0)
                    'End If

                    If idAprobado = 4 Then
                        listaDoc = (New bl_Pagosdocumentos).selectDocumentosPagadosaExportar(1, idtienda, idArea, idAprobado, FechaInicio, FechaFinal, 0)
                    End If

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If listaDoc.Count > 0 Then
                    Try
                        rptaDoc = (New objeto).SerializarLista(listaDoc, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rptaDoc)
                    Response.End()
                Else
                    Response.Write(rptaDoc)
                    Response.End()
                End If
            Case "ConsultaDocumentos"

                Dim FechaInicio As String = Request.QueryString("FechaInicio")
                Dim FechaFinal As String = Request.QueryString("FechaFinal")
                Dim idtienda As String = Request.QueryString("idtienda")
                Dim idAprobado As String = Request.QueryString("idAprobado")
                Dim idArea As String = Request.QueryString("idArea")

                Dim rptaDoc As String = ""
                Dim listaDoc As New List(Of be_PagosDocumentos)
                Try
                    If idAprobado <> 4 Then
                        listaDoc = (New bl_Pagosdocumentos).selectDocumentos(1, idtienda, idArea, idAprobado, FechaInicio, FechaFinal, 0)
                    End If
                    If idAprobado = 4 Then
                        listaDoc = (New bl_Pagosdocumentos).selectDocumentosPagados(1, idtienda, idArea, idAprobado, FechaInicio, FechaFinal, 0)
                    End If

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If listaDoc.Count > 0 Then
                    Try
                        rptaDoc = (New objeto).SerializarLista(listaDoc, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rptaDoc)
                    Response.End()
                Else
                    Response.Write(rptaDoc)
                    Response.End()
                End If
            Case "ConsultarRegistrosSumar"

                Dim FechaInicio As String = Request.QueryString("FechaInicio")
                Dim FechaFinal As String = Request.QueryString("FechaFinal")
                Dim idtienda As String = Request.QueryString("idtienda")
                Dim idAprobado As String = Request.QueryString("idAprobado")
                Dim idArea As String = Request.QueryString("idArea")

                Dim rptaDoc As String = ""
                Dim listaDoc As New List(Of be_Montos)
                Try
                    listaDoc = (New bl_Montos).selectSumarTotales(1, idtienda, idArea, idAprobado, FechaInicio, FechaFinal, 0)

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If listaDoc.Count > 0 Then
                    Try
                        rptaDoc = (New objeto).SerializarLista(listaDoc, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rptaDoc)
                    Response.End()
                Else
                    Response.Write(rptaDoc)
                    Response.End()
                End If

            Case "UpdateRequerimientoRetencion"
                Dim iddocumento As String = Request.QueryString("iddocumento")
                Dim idrequerimiento As String = Request.QueryString("idrequerimiento")

                Dim rptaDoc As String = ""
                Dim listaDoc As New List(Of be_PagosDocumentos)
                Try
                    If (iddocumento > 0) Then
                        listaDoc = (New bl_Pagosdocumentos).UpdateDocumentoRetencion(iddocumento, idrequerimiento)
                    End If
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If listaDoc.Count > 0 Then
                    Try
                        rptaDoc = (New objeto).SerializarLista(listaDoc, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rptaDoc)
                    Response.End()
                Else
                    Response.Write(rptaDoc)
                    Response.End()
                End If

            Case "UpdateRequerimiento"

                Dim FechaInicio As String = Request.QueryString("FechaInicio")
                Dim FechaFinal As String = Request.QueryString("FechaFinal")
                Dim idtienda As String = Request.QueryString("idtienda")
                Dim idAprobado As String = Request.QueryString("idAprobado")
                Dim idArea As String = Request.QueryString("idArea")
                Dim iddocumento As String = Request.QueryString("iddocumento")
                Dim idrequerimiento As String = Request.QueryString("idrequerimiento")

                Dim rptaDoc As String = ""
                Dim listaDoc As New List(Of be_PagosDocumentos)
                Try
                    If (iddocumento > 0) Then
                        listaDoc = (New bl_Pagosdocumentos).UpdateDocumento(iddocumento, idrequerimiento)
                    End If
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If listaDoc.Count > 0 Then
                    Try
                        rptaDoc = (New objeto).SerializarLista(listaDoc, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rptaDoc)
                    Response.End()
                Else
                    Response.Write(rptaDoc)
                    Response.End()
                End If

            Case "ReprogramarPago"
                Dim FechaInicio As String = Request.QueryString("FechaInicio")
                Dim FechaFinal As String = Request.QueryString("FechaFinal")
                Dim idtienda As String = Request.QueryString("idtienda")
                Dim idAprobado As String = Request.QueryString("idAprobado")
                Dim idArea As String = Request.QueryString("idArea")
                Dim iddocumento As String = Request.QueryString("iddocumento")
                Dim idrequerimiento As String = Request.QueryString("idrequerimiento")
                Dim Fecha As String = Request.QueryString("FechaReprogramada")

                Dim rptaDoc As String = ""
                Dim listaDoc As New List(Of be_PagosDocumentos)
                Try
                    listaDoc = (New bl_Pagosdocumentos).UpdateDocumentoReprogramados(Fecha, Convert.ToInt32(idrequerimiento))
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If listaDoc.Count > 0 Then
                    Try
                        rptaDoc = (New objeto).SerializarLista(listaDoc, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rptaDoc)
                    Response.End()
                Else
                    Response.Write(rptaDoc)
                    Response.End()
                End If

        End Select
            
    End Sub




   
    


End Class