<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmReporteProgramacionPago.aspx.vb" Inherits="APPWEB.FrmReporteProgramacionPago" 
    title="Reporte Programaci�n de Pagos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   <table class="style1" width="100%">


           <tr>
            <td class="TituloCelda">
               REPORTE DE PROGRAMACIONES DE PAGO
            </td>
        </tr>
        <tr>
        <td>
        <asp:Panel ID="Panel_Cab" runat="server">
                   
                   <table >
                     <tr>    <td></td>
                            <td align="right" class="Texto">
                                Fecha Inicio:
                            </td>
                            <td >
                                <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="150px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                                </cc1:CalendarExtender>
                           </td>
                           <td></td>
                          <td align="right" class="Texto">
                                Fecha de Fin:
                            </td>
                            <td>
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="150px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaFin" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaFin" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                                </cc1:CalendarExtender>
                            </td>
                            <td></td>
                            <td></td>
                             <td></td>
                                <td></td>
                                <td></td>
                        </tr>
                        <tr>
                        <td></td>
                     <%-- <td align="right" class="Texto">
                               Programaci�n:
                            </td>
                      <td>
                            <asp:DropDownList ID="cboFiltro_Opcion_Programacion" runat="server" Width="100%"
                                Font-Bold="true">
                                <asp:ListItem Value="0" Selected="True">Sin Programar</asp:ListItem>
                                <asp:ListItem Value="1">Programados</asp:ListItem>
                                <asp:ListItem Value="2">Todos</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td></td>
                          <td></td>--%>
                    
                        
                          
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                        </tr>
                   </table>
                   <table>
                  
                   <tr>
                   <td></td>
                   <td></td>
         

                   <td>
                                <asp:Button id="btnBuscar" runat="server" Text="Buscar" Width="160px" />
                                </td>
                                <td></td>
                                <td>
                                <asp:Button id="btnExport" runat="server" Text="Exportar" Width="140px"/>
                                </td>
                   </tr>
                   </table>
                   <br />
                   <br />
                                <table width="100%">
                            
                                  <tr>
                                 
                                <td class="TituloCelda">
                                   RESULTADO DE LA B�SQUEDA
                                </td>
                            </tr>
                                <tr>
                                
                                <td>  
                                <asp:GridView ID="GV_Inventrio" runat="server" AutoGenerateColumns="False" 
                                        Width="96%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" 
                                        BorderWidth="1px" CellPadding="3"  >
                                       
                                <Columns>
                                  <%--  <asp:ButtonField Text = "Seleccionar" CommandName = "Select"  />--%>
                                   <asp:TemplateField HeaderText="Proveedor" >
                                   <ItemTemplate>
                                    <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblProveedor"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Proveedor")%>'></asp:Label></td> 
                                             </tr>
                                             </table>                                        
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fecha de Pago Programado" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                         <asp:Label ID="lblpp_FechaPagoProg"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"pp_FechaPagoProg","{0:d}")%>'></asp:Label>
                                   </ItemTemplate>
                                   
                                                                       
                                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                   
                                                                       
                                   </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Moneda Prog." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                            <table>
                                            <tr>
                                            <td>   <asp:Label ID="lblmonedapago"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"monedapago")%>'></asp:Label> </td>
                                          
                                            </table>
                                   </ItemTemplate>                      
                                         <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                         <ItemStyle HorizontalAlign="Center" />
                                   </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Monto Programado" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                            <table>
                                            <tr>
                                            <td> <asp:Label ID="lblMontoProgramado"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"pp_MontoPagoProg","{0:F}")%>'></asp:Label></td>
                                            </tr>
                                            </table>
                                   </ItemTemplate>                      
                                         <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                         <ItemStyle HorizontalAlign="Center" />
                                   </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Moneda Tot." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                              <table>
                                            <tr>
                                            <td>
                                             <asp:Label ID="lblmon_Simbolo1"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"mon_Simbolo")%>'></asp:Label></td>
                                            </tr>
                                            </table>
                                   </ItemTemplate>
                                                                    
                                   <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                   <ItemStyle HorizontalAlign="Center" />
                                                                    
                                   </asp:TemplateField>
                                   
                               <asp:TemplateField HeaderText="Monto Total" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                              <table>
                                            <tr>
                                           <td>
                                         <asp:Label ID="lblMontoTotal"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Monto","{0:F}")%>'></asp:Label>
                                            </td></tr>
                                            </table>
                                   </ItemTemplate>
                                                                    
                                   <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                   <ItemStyle HorizontalAlign="Center" />
                                                                    
                                   </asp:TemplateField>
                                   
                                  <%--   <asp:TemplateField HeaderText="Moneda Saldo" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                              <table>
                                            <tr>
                                            <td>
                                    <asp:Label ID="lblmon_Simbolo2"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"mon_Simbolo")%>'></asp:Label></td>
                                           </tr>
                                            </table>
                                   </ItemTemplate>                  
                                          <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                          <ItemStyle HorizontalAlign="Center" />
                                   </asp:TemplateField>
                                   --%>
                                     <%-- <asp:TemplateField HeaderText="Saldo" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                              <table>
                                            <tr>
                                           <td>
                                         <asp:Label ID="lblSaldo"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Saldo","{0:F}")%>'></asp:Label>
                                            </td></tr>
                                            </table>
                                   </ItemTemplate>                  
                                          <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                          <ItemStyle HorizontalAlign="Center" />
                                   </asp:TemplateField>--%>
                                   
                                     <asp:TemplateField HeaderText="Observaci�n" >
                                   <ItemTemplate>
                                         <asp:Label ID="lblObservacion"  runat="server"  ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"pp_Observacion")%>'></asp:Label>
                                        
                                   </ItemTemplate>
                               
                                  </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Medio de Pago">
                                   <ItemTemplate>
                                    <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblMedioPago"  runat="server"  ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"mp_Nombre")%>'></asp:Label>
                                           </td>
                                           </tr>
                                   </table>
                                   </ItemTemplate>
                               
                                  </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Banco">
                                   <ItemTemplate>
                                    <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblBanco"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"ban_Nombre")%>'></asp:Label> </td>
                                        
                                            </tr>
                                           </table>
                                     </ItemTemplate>
                                   </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Cuenta Bancaria">
                                   <ItemTemplate>
                                      <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblCuentaBancaria"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"cb_numero")%>'></asp:Label> </td>
                                         
                                                </tr>
                                                </table>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                   
<%--                                    <asp:TemplateField HeaderText="Tipo de Documento Ref." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                   <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblTipoDocumento"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"TipoDocumento")%>'></asp:Label> </td>
                                   </tr>
                                   </table>
                                   </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                   </asp:TemplateField>--%>
                                 <%--     <asp:TemplateField HeaderText="Doc. Referencia" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                      <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblNumeroDocumento"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"NumeroDocumento")%>'></asp:Label> </td>
                                     </tr>
                                     </table>
                                   </ItemTemplate>
                                          <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                          <ItemStyle HorizontalAlign="Center" />
                                     
                                   </asp:TemplateField>--%>
                                   
                                  <%-- <asp:TemplateField HeaderText="Fecha Emisi�n Doc." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                         <asp:Label ID="lblFechaEmision"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Fecha","{0:d}")%>'></asp:Label>
                                   </ItemTemplate>                          
                                       <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                       <ItemStyle HorizontalAlign="Center" />
                                   </asp:TemplateField>
                                   
                                      <asp:TemplateField HeaderText="Fecha de Vencimiento Doc." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                         <asp:Label ID="lblFechaVenci"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaVenc","{0:d}")%>'></asp:Label>
                                   </ItemTemplate>
                                          <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                          <ItemStyle HorizontalAlign="Center" />
                                   </asp:TemplateField>--%>
                                       

                                    <%--    <asp:TemplateField HeaderText="Banco Proveedor">
                                   <ItemTemplate>
                                         <asp:Label ID="lbljulio"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Julio","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                   
                                   </asp:TemplateField>
                                   
                                        <asp:TemplateField HeaderText="Cuenta Proveedor">
                                   <ItemTemplate>
                                         <asp:Label ID="lblagosto"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Agosto","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                     
                                   </asp:TemplateField>--%>
                                   
                                       
                                   
                                       <%-- <asp:TemplateField HeaderText="Estado" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                         <asp:Label ID="lblestado"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"edoc_Nombre")%>'></asp:Label>
                                   </ItemTemplate>
                                     
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                     
                                   </asp:TemplateField>--%>
                                </Columns>
                                <RowStyle CssClass="GrillaRow" ForeColor="#000066" />
                                <FooterStyle CssClass="GrillaFooter" BackColor="White" ForeColor="#000066" />
                                <PagerStyle CssClass="GrillaPager"
                                        HorizontalAlign="Left" BackColor="White" ForeColor="#000066" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" BackColor="#669999" Font-Bold="True" 
                                        ForeColor="White" />
                                <HeaderStyle CssClass="GrillaHeader" BackColor="#006699" Font-Bold="True" 
                                        ForeColor="White" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            </asp:GridView></td>
                                </tr>
                                
                                <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                              
                                </tr>
                                  <tr>
                                <td></td>
                                
                                
                                </tr>
                                    </table>
                                                       
             </asp:Panel>
        </td>
          
      
        </tr>
              
</table>


 <script language="javascript" type="text/javascript">

 
        
 </script>

</asp:Content>