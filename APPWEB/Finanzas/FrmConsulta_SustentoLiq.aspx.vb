﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.



Partial Public Class FrmConsulta_SustentoLiq
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            ConfigurarDatos()
            inicializarFrm()
        End If
    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboRol(Me.cboRol, True)

            End With
            Me.txtFecha_Inicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFecha_Fin.Text = Me.txtFecha_Inicio.Text

            '******************* CARGAMOS EL TEXTO DEL RESUMEN
            Dim IdMedioPagoPrincipal As Integer = (New Negocio.MedioPago).SelectIdMedioPagoPrincipal
            Dim objMedioPago As Entidades.MedioPago = (New Negocio.MedioPago).SelectxId(IdMedioPagoPrincipal)(0)
            Me.lblTotalFisico.Text = "Total " + objMedioPago.Nombre + ":"

            Me.hddIdMedioPagoPrincipal.Value = CStr(IdMedioPagoPrincipal)


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        valOnChange_cboEmpresa()
    End Sub

    Private Sub valOnChange_cboEmpresa()

        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub valOnChange_cboTienda()

        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        valOnChange_cboTienda()
    End Sub

    Protected Sub btnAceptar_ViewRpt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptar_ViewRpt.Click

        valOnClick_btnAceptar_ViewRpt()

    End Sub
    Private Sub valOnClick_btnAceptar_ViewRpt()
        Try

            Dim fechaInicio As Date = Nothing
            Dim fechaFin As Date = Nothing

            If (IsDate(Me.txtFecha_Inicio.Text) And Me.txtFecha_Inicio.Text.Trim.Length > 0) Then
                fechaInicio = CDate(Me.txtFecha_Inicio.Text)
            End If
            If (IsDate(Me.txtFecha_Fin.Text) And Me.txtFecha_Fin.Text.Trim.Length > 0) Then
                fechaFin = CDate(Me.txtFecha_Fin.Text)
            End If

            Dim IdPersona As Integer = 0
            If (IsNumeric(Me.hddIdPersona.Value) And Me.hddIdPersona.Value.Trim.Length > 0) Then
                IdPersona = CInt(Me.hddIdPersona.Value)
            End If

            '********************* CARGAMOS LA GRILLA DOCUMENTO
            Me.GV_DocumentoReqGasto.DataSource = (New Negocio.DocCancelacion).Sustento_Liq_SelectxParams_DT(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboCaja.SelectedValue), CInt(Me.cboOpcion_Sustento.SelectedValue), CInt(Me.cboOpcion_Liquidacion.SelectedValue), fechaInicio, fechaFin, IdPersona)
            Me.GV_DocumentoReqGasto.DataBind()

            '****************** CARGAMOS EL RESUMEN CAJA
            Me.GV_CajaResumen_Fisico.DataSource = (New Negocio.Caja).SelectResumenxParams(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboCaja.SelectedValue), CInt(Me.hddIdMedioPagoPrincipal.Value))
            Me.GV_CajaResumen_Fisico.DataBind()

            '****************** CARGAMOS EL RESUMEN MONTO X SUSTENTAR
            Me.GV_TotalxSustentar.DataSource = (New Negocio.DocReciboCaja).SelectResumenMontoxSustentar_DT(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboCaja.SelectedValue))
            Me.GV_TotalxSustentar.DataBind()

            If (Me.GV_CajaResumen_Fisico.Rows.Count > 0) Then
                Me.GV_CajaResumen_Fisico.Rows(Me.GV_CajaResumen_Fisico.Rows.Count - 1).BackColor = Drawing.Color.Yellow
            End If

            If (Me.GV_TotalxSustentar.Rows.Count > 0) Then
                Me.GV_TotalxSustentar.Rows(Me.GV_TotalxSustentar.Rows.Count - 1).BackColor = Drawing.Color.Yellow
            End If

            If (Me.GV_DocumentoReqGasto.Rows.Count <= 0) Then
                Me.btnGuardar.Visible = False
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS.")
            Else
                Me.btnGuardar.Visible = True
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click

        valOnClick_btnGuardar()

    End Sub
    Private Sub limpiarGrilla()
        Me.GV_DocumentoReqGasto.DataSource = Nothing
        Me.GV_DocumentoReqGasto.DataBind()

        Me.GV_CajaResumen_Fisico.DataSource = Nothing
        Me.GV_CajaResumen_Fisico.DataBind()

        Me.GV_TotalxSustentar.DataSource = Nothing
        Me.GV_TotalxSustentar.DataBind()

        Me.GV_RptMovCaja.DataSource = Nothing
        Me.GV_RptMovCaja.DataBind()

        Me.btnGuardar.Visible = False

    End Sub
    Private Sub valOnClick_btnGuardar()
        Try

            Dim lista As List(Of Entidades.Anexo_Documento) = obtenerListaAnexoDocumento()

            If ((New Negocio.DocCancelacion).UpdateMontoxSustentar_Liquidacion(lista)) Then
                limpiarGrilla()
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")
            Else
                objScript.mostrarMsjAlerta(Me, "Problemas en la Operación.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaAnexoDocumento() As List(Of Entidades.Anexo_Documento)

        Dim lista As New List(Of Entidades.Anexo_Documento)

        For i As Integer = 0 To Me.GV_DocumentoReqGasto.Rows.Count - 1

            Dim obj As New Entidades.Anexo_Documento
            With obj
                .IdDocumento = CInt(CType(Me.GV_DocumentoReqGasto.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value)
                .MontoxSustentar = CDec(CType(Me.GV_DocumentoReqGasto.Rows(i).FindControl("txtMontoxSustentar"), TextBox).Text)
                .Liquidado = CBool(CType(Me.GV_DocumentoReqGasto.Rows(i).FindControl("chb_Liquidado"), CheckBox).Checked)
            End With

            lista.Add(obj)

        Next

        Return lista

    End Function

    Protected Sub Click_btnSustentar(ByVal sender As Object, ByVal e As EventArgs)
        valOnClick_btnSustentar(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub
    Private Sub valOnClick_btnSustentar(ByVal index As Integer)

        Try

            Dim IdDocumento As Integer = CInt(CType(Me.GV_DocumentoReqGasto.Rows(index).FindControl("hddIdDocumento"), HiddenField).Value)

            'Dim listaDocumentoRef As List(Of Entidades.Documento) = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(IdDocumento)

            'Dim IdDocumentoReqGasto As Integer = Nothing

            'For i As Integer = 0 To listaDocumentoRef.Count - 1

            '    '******************** BUSCAMOS EL REQ DE GASTO
            '    If (listaDocumentoRef(i).IdTipoDocumento = 36) Then

            '        IdDocumentoReqGasto = listaDocumentoRef(i).Id
            '        Exit For

            '    End If

            'Next

            If (IdDocumento <> Nothing) Then
                limpiarGrilla()
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  visualizarDocumento_ReqGasto(" + CStr(IdDocumento) + ");   ", True)
            Else
                objScript.mostrarMsjAlerta(Me, "No se hallaron documentos asociados para sustento. < Requerimiento de Gasto. >")
            End If

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Sub btnMostrarRpt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrarRpt.Click
        valOnClick_btnMostrarRpt()
    End Sub
    Private Sub valOnClick_btnMostrarRpt()
        Try

            Dim fechaInicio, fechaFin As Date

            If (IsDate(Me.txtFecha_Inicio.Text) And Me.txtFecha_Inicio.Text.Trim.Length > 0) Then
                fechaInicio = CDate(Me.txtFecha_Inicio.Text)
            End If

            If (IsDate(Me.txtFecha_Fin.Text) And Me.txtFecha_Fin.Text.Trim.Length > 0) Then
                fechaFin = CDate(Me.txtFecha_Fin.Text)
            End If

            Me.GV_RptMovCaja.DataSource = (New Negocio.DocReciboCaja).RPT_MovCajaxParams_DT(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboCaja.SelectedValue), fechaInicio, fechaFin)
            Me.GV_RptMovCaja.DataBind()

            If (Me.GV_RptMovCaja.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)
        listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try

            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", Me.cboRol.SelectedValue)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")

        With objPersona
            Me.hddIdPersona.Value = CStr(objPersona.IdPersona)
            Me.txtDescripcionPersona.Text = objPersona.Descripcion
            Me.txtDni.Text = objPersona.Dni
        End With
        
        objScript.offCapa(Me, "capaPersona")

    End Sub

#End Region

    Protected Sub btnLimpiar_Persona_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLimpiar_Persona.Click
        valOnClick_btnLimpiar_Persona()
    End Sub
    Private Sub valOnClick_btnLimpiar_Persona()
        Try

            Me.txtDescripcionPersona.Text = ""
            Me.txtDni.Text = ""
            Me.hddIdPersona.Value = ""

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class