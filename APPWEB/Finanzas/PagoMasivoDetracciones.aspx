﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="PagoMasivoDetracciones.aspx.vb" Inherits="APPWEB.PagoMasivoDetracciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table align="center" border="solid"  cellpadding="0" cellspacing="0" Width="1400px" border="1px">
<tr>
<td style="height: 34px; text-align: center">
<span style="text-align: center"><strong>PAGO MASIVO DETRACCIONES</strong></span>
</td>
    </tr>
    <tr>
<td>

    <table class="style1">
        <tr>
            <td style="width: 181px; text-align: right;">
                Fecha Inicial :</td>
            <td style="width: 100px">
                <asp:TextBox ID="TxtFechaInicio" runat="server" style="margin-left: 0px" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="txtfecha_CalendarExtender" runat="server" 
                                           TargetControlID="TxtFechaInicio"  Format="dd/MM/yyyy"  /></td>
            <td style="width: 154px; text-align: right;">

                Fecha Final :</td>
            <td style="width: 116px">
                  <asp:TextBox ID="TxtFechaFin" runat="server" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="Calendarextender1" runat="server" 
                                           TargetControlID="TxtFechaFin"  Format="dd/MM/yyyy"  /></td>
            <td style="width: 163px">
                <input id="btnBuscar" style="width: 174px" type="button" 
                    value="Mostrar Detracciones" onclick="return ConsultarRegistros()" /></td>
            <td style="width: 45px">
                &nbsp;</td>
            <td style="width: 128px">
                &nbsp;</td>
             <td style="width: 44px">
            </td>
             <td style="width: 91px">
            </td>
             <td>
            </td>
        </tr>
        <tr>
            <td style="width: 181px">
                <input 
        id="btnexportardetracciones" style="width: 194px" type="button" 
        value="Generar Lote de Detracciones" 
                    onclick="return btnexportardetracciones_onclick()"/></td>
            <td style="width: 100px">
                <input id="txtlote" type="text" /></td>
            <td style="width: 154px">

   <input id="btnExportarArchivo" style="width: 170px" type="button" 
        value="Exportar Archivo TXT" 
        onclick="return btnExportarArchivo_onclick()" /></td>
            <td style="width: 116px">
                &nbsp;</td>
            <td style="width: 163px">
                <input id="btnExportarReportePagado" style="width: 174px" type="button" onclick="return btnExportarReportePagado_onclick()"
                    value="Exportar Reporte por Lote" /></td>
            <td style="width: 45px">
                &nbsp;</td>
            <td style="width: 128px">
                <input id="btncerrarlote" style="width: 158px" type="button" 
                    value="Cerrar Lote" onclick="return btncerrarlote_onclick()"/></td>
            <td style="width: 44px">
            </td>
             <td style="width: 91px">
                 <input id="btnConsulta" style="width: 118px" type="button" 
                     value="Cancelaciones" onclick="return btnCancelaciones_onclick()" /></td>
             <td>
                 &nbsp;</td>
        </tr>
    </table>
        </td>

       
</tr>
<tr>
<td style="height: 19px">
<div id="GrillaDetracciones">
</div>
</td>
</tr>
<tr>
<td style="height: 19px">
 
</td>
</tr>
<tr>
<td>
    <asp:HiddenField ID="hddIdPersona" runat="server" />
</td>
</tr>
</table>
<div id="CapaMantenimiento" style="border: 3px solid blue; padding: 4px; width: 850px;
            height: 600px; position: absolute; top: 0px; left: 450px; background-color: #EFFBFB;
            z-index: 3; display:none;">
                   <table border="0" cellspacing="0px" style="width: 750px">
                   <tr>
                    <td>
                          <table border="0">
                                        <tr class="BarraTitulo">
                                        <td style="width:90%; background-color: #0066CC;"><span id="TituloPopup"></span></td>
                                        <td style="width:4%"><img src='../ImagenesForm/imgcerrar.png' class="Iconoventanas" 
                                                title="Cerrar Ventana" onclick="return cerrardetalle()"/></td>
                                        </tr>
                          </table>
                    </td>
                    </tr>
                    <tr>
                    <td>
                       <div id="TablaMantenimientoDetraccion">
                           <table class="style1">
                               <tr>
                                   <td style="width: 254px; text-align: right">
                                       <b>Tipo Operacion :</b></td>
                                   <td colspan="2">
                                       <select id="cboTipoOperacion" name="D1" style="width:461px; margin-left: 0px;">
                                       </select></td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right; height: 16px;">
                                       </td>
                                   <td style="width: 267px; height: 16px;">
                                       </td>
                                   <td style="height: 16px">
                                       </td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right">
                                       <b>Tipo Bien o Servicio :</b></td>
                                   <td colspan="2">
                                       <select id="cbobienServicio" name="D2" style="width:467px; margin-left: 0px;">
                                       </select></td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right; height: 8px;">
                                       </td>
                                   <td style="width: 267px; height: 8px;">
                                       </td>
                                   <td style="height: 8px">
                                       </td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right">
                                       <b>Razon Proveedor :</b></td>
                                   <td colspan="2">
                                       <input id="txtnombreproveedor" onkeypress="return isNumberKey(event)" 
                                           style="width: 461px" type="text" /></td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right; height: 14px;">
                                       </td>
                                   <td style="width: 267px; height: 14px;">
                                       </td>
                                   <td style="height: 14px">
                                       </td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right">
                                       <b>Ruc del Proveedor :</b></td>
                                   <td style="width: 267px">
                                       <input id="txtrucproveedor" onkeypress="return isNumberKey(event)" 
                                           style="width: 182px" type="text" maxlength="11" /></td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right; height: 7px;">
                                       </td>
                                   <td style="width: 267px; height: 7px;">
                                       </td>
                                   <td style="height: 7px">
                                       </td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right">
                                       <b>Tipo Documento Adquiriente:</b></td>
                                   <td style="width: 267px">
                                       <select ID="cboTipoDoc" style="width:234px; margin-left: 0px;" name="D2">
                                       <option value="01">RUC</option>
                                        </select>
                                   </td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right; height: 15px;">
                                       </td>
                                   <td style="width: 267px; height: 15px;">
                                       </td>
                                   <td style="height: 15px">
                                       </td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right">
                                       <b>Numero de Documento Adquiriente :</b></td>
                                   <td style="width: 267px">
                                       <input id="txtrucAdquiriente" onkeypress="return isNumberKey(event)" 
                                           style="width: 299px" type="text" maxlength="11" /></td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right; height: 16px;">
                                       </td>
                                   <td style="width: 267px; height: 16px;">
                                       </td>
                                   <td style="height: 16px">
                                       </td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right">
                                       <b>Nombre Razon Social Adquiriente :</b></td>
                                   <td style="width: 267px">
                                       <input id="txtrazonsocialadquiriente" onkeypress="return isNumberKey(event)" 
                                           style="width: 328px" type="text" /></td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right">
                                       &nbsp;</td>
                                   <td style="width: 267px">
                                       &nbsp;</td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right">
                                       <b>Monto Deposito:</b></td>
                                   <td style="width: 267px">
                                       <input id="txtmontodeposito" onkeypress="return isNumberKey(event)" 
                                           type="text" /><input id="btnCambiarMonto" style="width: 104px" 
                                           type="button" value="Cambiar Monto" onclick="return btnCambiarMonto_onclick()" /></td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right">
                                       <b>Periodo Tributario :</b></td>
                                   <td style="width: 267px">
                                       <input id="txtperiodotributo" onkeypress="return isNumberKey(event)" 
                                           type="text" /></td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: center">
                                       <b>Tipo Comprobante :</b></td>
                                   <td style="text-align: center; width: 267px">
                                       <b>Serie :</b></td>
                                   <td style="text-align: left">
                                       <b>Nro Documento :</b></td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: center;">
                                  <select ID="cbotipocompb" style="width:234px; margin-left: 0px;" name="D2">
                                 <option value="01">01 - FACTURA</option>
                                        </select>
                                       </td>

                                     
                              
                                       

                                   <td style="width: 267px; text-align: center;">
                                       <input id="txtseriedoc" 
                                           type="text" maxlength="4" /></td>
                                   <td style="text-align: left">
                                       <input id="txtnrodocumento" onkeypress="return isNumberKey(event)" 
                                           type="text" maxlength="8" /></td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right; height: 52px;">
                                       <b style="text-align: right">Nro Cuenta Detraccion :</b></td>
                                   <td style="height: 52px">
                                       <input id="txtnumerodetraccion" onkeypress="return isNumberKey(event)" 
                                           style="width: 316px" type="text" maxlength="11" />
                                  </td>
                                  <td>
                                  
                                       <input 
                                           id="btncuentadetraccion" style="width: 139px" type="button" 
                                           value="Grabar Cta Detraccion"  onclick="return btnguardarCta_onclick()"/>
                                           </td>
                               </tr>
                               <tr>
                                   <td style="width: 254px; text-align: right">
                                       <input 
                                           id="txtidcuenta" onkeypress="return isNumberKey(event)" 
                                           style="width: 5px" type="text" maxlength="11" readonly="readonly" /><input 
                                           id="txtiddocumento" onkeypress="return isNumberKey(event)" 
                                           style="width: 5px" type="text" maxlength="11" readonly="readonly" /><input 
                                           id="txtidpersona" onkeypress="return isNumberKey(event)" 
                                           style="width: 5px" type="text" maxlength="11" readonly="readonly" /></td>
                                   <td colspan="2">
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="text-align: right" colspan="3">
                                       &nbsp;<table class="style1">
                                           <tr>
                                               <td style="width: 111px">
                                                   &nbsp;</td>
                                               <td style="width: 111px">
                                       <input id="btnguardarDetraccion" style="width: 134px" type="button"  onclick="return btnguardarDetraccion_onclick()"
                                           value="Guardar Detraccion" /></td>
                                               <td style="width: 33px">
                                                   &nbsp;</td>
                                               <td style="width: 95px">
                                                   <input id="btndesactivar" style="width: 143px" type="button" onclick="return btncambiarEstado_onclick()"
                                                       value="Cambiar a Pendiente" /></td>
                                               <td class="style2" style="width: 40px">
                                                   &nbsp;</td>
                                               <td style="width: 156px">
                                                   <input id="btncancelar" style="width: 118px; margin-left: 0px;" 
                                           type="button" value="Cancelar" onclick="return cerrardetalle()"/></td>
                                               <td>
                                                   &nbsp;</td>
                                               <td>
                                                   &nbsp;</td>
                                           </tr>
                                       </table>
                                   </td>
                               </tr>
                           </table>
                    </td>
                    </tr>
                    </table>
                

</div>

<div id="CapaCancelacionDetracciones" style="border: 3px solid blue; padding: 4px; width: 1050px;
            height: 2000px; position: absolute; top: 0px; left:350px; background-color: #FFFFFF;
            z-index: 3; display:none;">
                   <table border="0" cellspacing="0px" style="width: 960px">
                   <tr>
                    <td style="height: 32px">
                          <table border="0">
                                        <tr class="BarraTitulo">
                                        <td style="width:105%; background-color: #0066CC; text-align: center;"><span id="TituloPopup">
                                            <span style="color: #FFFFFF"><strong>Consulta de Lote de Detracciones</strong></span> </span></td>
                                        <td style="width:4%"><img src='../ImagenesForm/imgcerrar.png' class="Iconoventanas" 
                                                title="Cerrar Ventana" onclick="return cerrarVentana()"/></td>
                                        </tr>
                          </table>
                    </td>
                    </tr>
                    <tr>
                    <td>
                       <div id="TablaCancelacion">

                           <table class="style1" style="height: 85px; width: 109%;">
                               <tr>
                                   <td style="width: 121px; text-align: right; color: #FFFFFF; background-color: #3399FF;">
                                       <strong>Lote :</strong></td>
                                   <td style="width: 154px; background-color: #3399FF;">
                                       <input id="txtloteDetracciones" style="width: 125px" type="text" /></td>
                                   <td colspan="2">
                                       <input id="btnConsult" style="width: 275px; text-align: center" type="button" 
                                           value="Consultar" onclick="return ConsultarRegistrosSegunLote()"/></td>
                               </tr>
                               <tr>
                                   <td style="width: 121px; height: 25px; text-align: right; color: #0000FF; background-color: #CCFFFF; font-size: x-small;">
                                       <b>Nro Constancia :</b></td>
                                   <td style="width: 154px; height: 25px; background-color: #CCFFFF;">
                        <input id="txtNroOperacion" type="text" /></td>
                                   <td style="width: 271px; " rowspan="3">
                                       <div  id="MantenimientoConstancia" style="display:none;" >
                                           <table class="style1" style="width: 126%">
                                               <tr>
                                                   <td style="width: 110px; font-size: x-small; text-align: right; color: #006600; background-color: #FFFFCC;">
                                                       <b>Nro Constancia:</b></td>
                                                   <td style="background-color: #FFFFCC">
                                                       <input id="txtconstancia" type="text" style="background-color: #66FF99" /></td>
                                               </tr>
                                               <tr>
                                                   <td style="width: 110px; font-size: x-small; text-align: right; color: #006600; background-color: #FFFFCC;">
                                                       <b>Nro Documento:</b></td>
                                                   <td style="background-color: #FFFFCC">
                                                       <input id="txtnrodoc" type="text" /></td>
                                               </tr>
                                               <tr>
                                                   <td style="width: 110px; font-size: x-small; text-align: right; color: #006600; background-color: #FFFFCC;">
                                                       <b>IdDocumento:</b></td>
                                                   <td style="background-color: #FFFFCC">
                                                       <input id="txtiddoc" type="text" /></td>
                                               </tr>
                                               <tr>
                                                   <td style="width: 110px; background-color: #FFFFCC;">
                                                       &nbsp;</td>
                                                   <td style="background-color: #FFFFCC">
                                                       <input id="btnguardarconstancia" style="width: 127px" type="button" value="Guardar Constancia"  onclick="return btnguardarconstancia_onclick(this)" /></td>
                                               </tr>
                                           </table>
                                       </div>
                                       </td>
                                   <td style="height: 25px">
                                       </td>
                               </tr>
                               <tr>
                                   <td style="width: 121px; height: 25px; text-align: right; color: #0000FF; background-color: #CCFFFF; font-size: x-small;">
                                       <b>Monto Deposito :</b></td>
                                   <td style="width: 154px; height: 25px; background-color: #CCFFFF;">
                        <input id="txtMontoDeposito" type="text" /></td>
                                   <td style="height: 25px">
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 121px; height: 25px; text-align: right; color: #0000FF; background-color: #CCFFFF; font-size: x-small;">
                                       <b>Banco :</b></td>
                                   <td style="width: 154px; height: 25px;">
                        <select id="cboBanco" name="D3" style="width: 343px; background-color: #CCFFFF;">
                            <option></option>
                        </select></td>
                                   <td style="height: 25px">
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 121px; height: 25px; text-align: right; color: #0000FF; background-color: #CCFFFF; font-size: x-small;">
                                       <b><span style="background-color: #CCFFFF; font-size: x-small;">Fecha Pago :</span></b></td>
                                   <td style="width: 154px; height: 25px; background-color: #CCFFFF;">
                                           <asp:TextBox ID="txtFechaPago" runat="server" Text=""></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtFechaPago">
                        </cc1:CalendarExtender></td>
                                   <td style="height: 25px;" colspan="2">
                                       <input id="btncancelacion" style="width: 267px" type="button" 
                                           value="Cancelar Detraccion x Lote" 
                                           onclick="return btncancelacion_onclick(this)" /><input id="txtnrovouch" 
                                           type="text" 
                                           style="color: #FF00FF; font-size: small; font-weight: 700; width: 180px;"/></td>

                               </tr>
                               <tr>
                                   <td colspan="4">
                                    <div id="GrillaDetraccion"></div></td>
                               </tr>
                               </table>

                      </div>
                    </td>
                    </tr>
                    </table>
                

</div>

<script type="text/javascript" language="javascript">

  window.onload = varios();


  function varios() {
      ConsultarRegistros();
      llenarcombos();
      llenarcomboservicio();
      ConsultaCombos();
  }


  function ConsultaCombos() {

      var opcion = "llenarcomboBanco";
      var IdEstadoc = 1;
      var datacombob = new FormData();
      datacombob.append('flag', 'llenarCombo');
      datacombob.append('IdEstadoc', IdEstadoc);

      var xhrrep = new XMLHttpRequest();
      xhrrep.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&IdEstadoc=" + IdEstadoc, true);
      xhrrep.onreadystatechange = function () {
          if (xhrrep.readyState == 4 && xhrrep.status == 200) {
              mostrarListasCombos(xhrrep.responseText);
          }
      }
      xhrrep.send(datacombob);
      return false;
  }


  function mostrarListasCombos(rptacombo) {
      if (rptacombo != "") {
          var listas2 = rptacombo;
          data1 = crearArray(listas2, "▼");
          listarcombos();
      }
      return false;
  }
  function listarcombos() {
      var TipoServicio = [];
      var nRegistros = data1.length;
      var campos2;
      var TipoServ;
      for (var i = 0; i < nRegistros; i++) {
          campos2 = data1[i].split("|");
          {
              TipoServ = campos2[0];
              TipoServ += "|";
              TipoServ += campos2[1];
              TipoServicio.push(TipoServicio);
          }
      }
      crearCombo(data1, "cboBanco", "|");
  }

  function crearCombo(lista, nombreCombo, separador, nombreItem, valorItem) {
      var contenido = "";
      if (nombreItem != null && valorItem != null) {
          contenido += "<option value='";
          contenido += valorItem;
          contenido += "'>";
          contenido += nombreItem;
          contenido += "</option>";
      }
      var nRegistros = lista.length;
      if (nRegistros > 0) {
          var campos;
          for (var i = 0; i < nRegistros; i++) {
              campos = lista[i].split(separador);
              contenido += "<option value='";
              contenido += campos[0];
              contenido += "'>";
              contenido += campos[1];
              contenido += "</option>";
          }
      }
      var cbo = document.getElementById(nombreCombo);
      if (cbo != null) cbo.innerHTML = contenido;

  }


  function btnexportardetracciones_onclick() {
      var r = confirm('Esta Seguro de Guardar Lote para Exportar Archivo');
      if (r == true) {
          GenerarLoteDetraccion();
          return false; 
      } else {
          alert("No se genero ningun Lote");
          return false;
      }
  }

  function btnCancelaciones_onclick() {
      document.getElementById('CapaCancelacionDetracciones').style.display = 'block';  
  }

  function btncancelacion_onclick(objeto) {

      var txtNroOperacion = document.getElementById("txtNroOperacion").value;
      var txtMontoDeposito = document.getElementById("txtMontoDeposito").value;

      if (txtNroOperacion == "") {
          return false;
      }
      if (txtMontoDeposito == "") {
          return false;
      }


      var r = confirm('Esta Seguro en Registrar la Cancelacion');
      if (r == true) {
         GuardarCancelacionLote(objeto);
          return false; ;
      } else {
          alert("No se Realizo ninguna Cancelacion");
          return false;
      }
  }

  function btnCambiarMonto_onclick()
   {
       var r = confirm('Esta Seguro de Cambiar el Monto de la detraccion');
       if (r == true) {
           CambiarMontoDetraccion();
           return false; ;
       } else {
           alert("No se Realizo ningun cambio");
           return false;
       }
   }

  function btnguardarconstancia_onclick(objeto) { 
          Actualizar(objeto);
  }

  function Actualizar(objeto) {
      var opcion = "ActualizarConstancia";
      var estado = 2;

      var iddocumento = document.getElementById("txtiddoc").value;
      var constancia = document.getElementById("txtconstancia").value;
      var lote = document.getElementById("txtloteDetracciones").value;
      var FechaFinal = document.getElementById('<%= TxtFechaFin.ClientID%>').value;

      var parts = FechaFinal.split('/');
      var mydate = parts[2];
      var periodo = mydate;

      var dataDetraccion = new FormData();

      dataDetraccion.append('flag', 'ActualizarConstancia');
      dataDetraccion.append('iddocumento', iddocumento);
      dataDetraccion.append('constancia', constancia);
      dataDetraccion.append('lote', lote);
      dataDetraccion.append('periodo', periodo);

      var xhrDetxmonto = new XMLHttpRequest();
      xhrDetxmonto.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&iddocumento=" + iddocumento + "&Constancia=" + constancia + "&lote=" + lote + "&periodo=" + periodo, true);

      xhrDetxmonto.onloadstart = function () { objeto.disabled = true; objeto.value = "Procesando..."; }
      xhrDetxmonto.onloadend = function () { objeto.disabled = false; objeto.value = "Guardar Constancia"; }
     
      xhrDetxmonto.onreadystatechange = function () {
          if (xhrDetxmonto.readyState == 4 && xhrDetxmonto.status == 200) {
              //fun_lista(xhrDetxmonto.responseText);
              fun_listaLote(xhrDetxmonto.responseText);
          }
      }
      xhrDetxmonto.send(dataDetraccion);
      return false;
  
  }

  function CambiarMontoDetraccion(objeto, flag) {
      var opcion = "CambiarMontoDetraccion";
      var estado = 2;
      var iddocumento = document.getElementById("txtiddocumento").value;
      var MontoN = document.getElementById("txtmontodeposito").value;
      var dataDetraccion = new FormData();
      dataDetraccion.append('flag', 'CambiarMontoDetraccion');
      dataDetraccion.append('iddocumento', iddocumento);
      dataDetraccion.append('estado', estado);
      dataDetraccion.append('MontoN', MontoN);
      var xhrDetxmonto = new XMLHttpRequest();
      xhrDetxmonto.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&iddocumento=" + iddocumento + "&estado=" + estado + "&MontoN=" + MontoN, true);
      xhrDetxmonto.onreadystatechange = function () {
          if (xhrDetxmonto.readyState == 4 && xhrDetxmonto.status == 200) {
              fun_lista(xhrDetxmonto.responseText);
          }
      }
      xhrDetxmonto.send(dataDetraccion);
      return false;
  }

  function GuardarCancelacionLote(objeto, flag) {

      var opcion = "CancelarLoteDetracciones";
      //      var FechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
      //      var FechaFin = document.getElementById('<%= TxtFechaFin.ClientID%>').value;    
      var lote = document.getElementById("txtloteDetracciones").value;  
      var idBancoCanc = document.getElementById("cboBanco").value;
      var nrooperacion = document.getElementById("txtNroOperacion").value;
      var montocancelado = document.getElementById("txtMontoDeposito").value;
      var FechaCancelacion = document.getElementById('<%=txtFechaPago.ClientID%>').value;


      var FechaFinal = document.getElementById('<%= txtFechaPago.ClientID%>').value;
      var parts = FechaFinal.split('/');
      var mydate = parts[2];
      var periodo = mydate;
      
      var dataCancLote = new FormData();
      dataCancLote.append('flag', 'CancelarLoteDetracciones');
      dataCancLote.append('lote', lote);
      dataCancLote.append('periodo', periodo);
      dataCancLote.append('idBancoCanc', idBancoCanc);
      dataCancLote.append('nrooperacion', nrooperacion);
      dataCancLote.append('montocancelado', montocancelado);
      dataCancLote.append('FechaCancelacion', FechaCancelacion);

      var xhrcanc = new XMLHttpRequest();
      xhrcanc.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&lote=" + lote + "&periodo=" + periodo + "&idBancoCanc=" + idBancoCanc + "&nrooperacion=" + nrooperacion + "&montocancelado=" + montocancelado + "&FechaCancelacion=" + FechaCancelacion, true);


      xhrcanc.onloadstart = function () { objeto.disabled = true; objeto.value = "Procesando..."; }
      xhrcanc.onloadend = function () { objeto.disabled = false; objeto.value  = "Cancelacion Detracciones x Lote"; }

      xhrcanc.onreadystatechange = function () {
          if (xhrcanc.readyState == 4 && xhrcanc.status == 200) {
              document.getElementById("txtnrovouch").value = xhrcanc.responseText;
              fun_lista(xhrcanc.responseText);

//              document.getElementById("txtMontoDeposito").value = "";
//              document.getElementById("txtNroOperacion").value = "";
//              document.getElementById("txtmontoTotal").value = "0";
              
          }
      }
      xhrcanc.send(dataCancLote);
      return false;

  }


  function btnExportarReportePagado_onclick() {
      var r = confirm('Generar Reporte de Detracciones canceladas segun Lote:');
      var txtlote = document.getElementById('txtlote').value;
      if (txtlote.value == "") {
          alert('Verifique Nro Lote correcto');
          return false;
      }
      if (r == true) {

          GenerarReporteDetraccionLote();
          return false; ;
      } else {
          alert("Error al Generar el reporte");
          return false;
      }
  }

  function GenerarReporteDetraccionLote(objeto, flag) {

      var opcion = "GenerarReporteLoteDetraccion";
      var IdEstado = null;

      var lote = document.getElementById('txtlote').value;
      var datarep = new FormData();

      datarep.append('flag', 'GenerarReporteLoteDetraccion');
      datarep.append('lote', lote);

      var xhrrep = new XMLHttpRequest();
      xhrrep.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&lote=" + lote, true);

      xhrrep.onreadystatechange = function () {
          if (xhrrep.readyState == 4 && xhrrep.status == 200) {
              fun_listaExport(xhrrep.responseText);
          }
      }
      xhrrep.send(datarep);
      return false;
  }



  function btncambiarEstado_onclick() {

              var r = confirm('Desea Cambiar a Estado Pendiente?');
              if (r == true) {
                  CambiarEstadoDetraccion();
                  return false; ;
              } else {
                  alert("No se ingreso ningun registro");
                  return false;
              }
          }

        

        function btnguardarDetraccion_onclick() {
            return (validarguardarDetraccion(0))
        }

        function btnguardarCta_onclick() {
            return (validarguardar(0))
        }

        function validarguardarDetraccion(tipod) {
          
            var ctadetraccion = document.getElementById('txtnumerodetraccion').value;
            if (ctadetraccion == "")
            {
                alert('Verifique Cuenta Detraccion');
                return false;
            }

            if (ctadetraccion == "0") {
                alert('Verifique Cuenta Detraccion');
                return false;
            }

            var r = confirm('Desea continuar con la operación?');           
            if (r == true) {
                enviarDatosGuardarDetraccion();
                return false; ;
            } else {
                alert("No se ingreso ningun registro");
                return false;
            }

        }

        function validarguardar(tipo) {
            var id = document.getElementById("txtnumerodetraccion").value;            
            if (id.value == "") {
                alert('Verifique ID');
                return false;
            }
            var r = confirm('Desea continuar con la operación?');
            if (r == true) {
                enviarDatosGuardar();
                return false; ;
            } else {
                alert("No se ingreso ningun registro");
                return false;
            }
        }
       
        function btncerrarlote_onclick(objeto, flag) {
            var opcion = "CierreLoteDetracciones";
            var IdEstado = null;

            var fechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
            var FechaFinal = document.getElementById('<%= TxtFechaFin.ClientID%>').value;

            var parts = FechaFinal.split('/');
            var mydate = parts[2];
            var periodo = mydate;

            var lote = document.getElementById("txtlote").value;

            var datacierre = new FormData();
            datacierre.append('flag', 'CierreLoteDetracciones');
            datacierre.append('fechaInicio', fechaInicio);
            datacierre.append('FechaFinal', FechaFinal);
            datacierre.append('lote', lote);

            var xhrCierreLote = new XMLHttpRequest();
            xhrCierreLote.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&lote=" + lote, true);
            
            xhrCierreLote.onreadystatechange = function () {
                if (xhrCierreLote.readyState == 4 && xhrCierreLote.status == 200) {
                    fun_lista(xhrCierreLote.responseText);
                }
            }
            xhrCierreLote.send(datacierre);
            return false;
        }


        function GenerarLoteDetraccion(objeto, flag) {
            var opcion = "GenerarLoteDetraccion";
            var IdEstado = null;
            var fechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
            var FechaFinal = document.getElementById('<%= TxtFechaFin.ClientID%>').value;

            var parts = FechaFinal.split('/');
            var mydate = parts[2];
            var periodo = mydate;

            var dataDetraccion = new FormData();

            dataDetraccion.append('flag', 'GenerarLoteDetraccion');
            dataDetraccion.append('fechaInicio', fechaInicio);
            dataDetraccion.append('FechaFinal', FechaFinal);
            dataDetraccion.append('periodo', periodo);

            var xhrGenera = new XMLHttpRequest();
            xhrGenera.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&periodo=" + periodo, true);
   
            xhrGenera.onreadystatechange = function () {
                if (xhrGenera.readyState == 4 && xhrGenera.status == 200) {
                    fun_lista(xhrGenera.responseText);
                }
            }
            xhrGenera.send(dataDetraccion);
            return false;

        }


        function CambiarEstadoDetraccion(objeto, flag) {
            var opcion = "GuardarEstadoDetraccion";
            var IdEstado = null;
            var iddocumento = document.getElementById("txtiddocumento").value;
            var dataDetraccion = new FormData();
            dataDetraccion.append('flag', 'GuardarEstadoDetraccion');         
            dataDetraccion.append('Estado', IdEstado);
            dataDetraccion.append('iddocumento', iddocumento);
            var xhrDetx = new XMLHttpRequest();
            xhrDetx.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&Estado=" + IdEstado + "&iddocumento=" + iddocumento, true);
            xhrDetx.onreadystatechange = function () {
                if (xhrDetx.readyState == 4 && xhrDetx.status == 200) {
                    fun_lista(xhrDetx.responseText);

                }
            }
            xhrDetx.send(dataDetraccion);
            return false;
        }

        function enviarDatosGuardarDetraccion(objeto, flag) {

            var opcion = "GuardarDetraccion";

            var IdConcepto = document.getElementById("cbobienServicio").value;
            var IdTipoOperacion = document.getElementById("cboTipoOperacion").value;
            var Serie_Doc = document.getElementById("txtseriedoc").value;
            var Anexo_Doc = document.getElementById("txtnrodocumento").value;
            var MontoDetracSunat = document.getElementById("txtmontodeposito").value;
            var IdEstado = 2;
            var iddocumento = document.getElementById("txtiddocumento").value;
            var dataDetraccion = new FormData();
            dataDetraccion.append('flag', 'GuardarDetraccion');
            dataDetraccion.append('IdServicio', IdConcepto);
            dataDetraccion.append('IdOperacion', IdTipoOperacion);
            dataDetraccion.append('Serie', Serie_Doc);
            dataDetraccion.append('Codigo', Anexo_Doc);
            dataDetraccion.append('MontoDetraccion', MontoDetracSunat);
            dataDetraccion.append('Estado', IdEstado);
            dataDetraccion.append('IDDocumentoGenera', iddocumento);            
           
            var xhrDet = new XMLHttpRequest();
           
            xhrDet.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&IdServicio=" + IdConcepto + "&IdOperacion=" + IdTipoOperacion + "&Serie=" + Serie_Doc + "&Codigo=" + Anexo_Doc + "&MontoDetraccion=" + MontoDetracSunat + "&Estado=" + IdEstado + "&IDDocumentoGenera=" + iddocumento, true);
            
            xhrDet.onreadystatechange = function () {             
                if (xhrDet.readyState == 4 && xhrDet.status == 200) {
                   (xhrDet.responseText);
                    if (xhrDet.toString = "ok") {
                        alert("Se guardo Correctamente");                        
                    }
                }
            }

            xhrDet.send(dataDetraccion);
            return false;
        }
     
        function enviarDatosGuardar(objeto, flag) {
            var opcion = "Guardar";
            var ctadetraccion = document.getElementById("txtnumerodetraccion").value;

            var iddocumento = document.getElementById("txtiddocumento").value;
            var idpersona = document.getElementById("txtidpersona").value;
            var idcuentadetraccion = document.getElementById("txtidcuenta").value;

            var dataproy = new FormData();
            dataproy.append('flag', 'Guardar');
            dataproy.append('CtaDetraccion', ctadetraccion);
            dataproy.append('iddocumento', iddocumento);
            dataproy.append('idpersona', idpersona);
            dataproy.append('idcuentadetraccion', idcuentadetraccion);
            var xhr2 = new XMLHttpRequest();

            xhr2.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&CtaDetraccion=" + ctadetraccion + "&iddocumento=" + iddocumento + "&idpersona=" + idpersona + "&idcuentadetraccion=" + idcuentadetraccion, true);
            xhr2.onreadystatechange = function () {
                if (xhr2.readyState == 4 && xhr2.status == 200) {

                    (xhr2.responseText);
                    if (xhr2.toString = "ok") {
                        alert("Se guardo Correctamente");
                    }
                }
            }
            xhr2.send(dataproy);
            return false;
        }




        function ConsultarRegistrosSegunLote(objeto, flag) {

            var opcion = "ConsultaDetraccionesporLote";
            var lote = document.getElementById("txtloteDetracciones").value;
            var FechaFinal = document.getElementById('<%= TxtFechaFin.ClientID%>').value;
            var parts = FechaFinal.split('/');
            var mydate = parts[2];
            var periodo = mydate;
            var datadet = new FormData();
            datadet.append('flag', 'ConsultaDetraccionesporLote');
            datadet.append('lote', lote);
            datadet.append('periodo', periodo);
            var xhrDet = new XMLHttpRequest();
            xhrDet.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&lote=" + lote + "&periodo=" + periodo, true);
            xhrDet.onreadystatechange = function () {
                if (xhrDet.readyState == 4 && xhrDet.status == 200) {
                    fun_listaLote(xhrDet.responseText);
                }
            }
            xhrDet.send(datadet);
            return false;
        }



        function ConsultarRegistros(objeto, flag) {
            var opcion = "ConsultaDetracciones";         

            var fechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
            var FechaFinal = document.getElementById('<%= TxtFechaFin.ClientID%>').value;

            var parts = FechaFinal.split('/');
            var mydate = parts[2];
            var periodo = mydate;

            var xhr = new XMLHttpRequest();
            xhr.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&periodo=" + periodo, true);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    fun_lista(xhr.responseText);
                }
            }
            xhr.send(null);
            return false;
        }





        function llenarcombos(objeto, flag) {
            var opcion = "llenarcombos";
            var xhrtop = new XMLHttpRequest();
            xhrtop.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion, true);
            xhrtop.onreadystatechange = function () {
                if (xhrtop.readyState == 4 && xhrtop.status == 200) {
                    mostrarCombos(xhrtop.responseText);
                }
            }
            xhrtop.send(null);
            return false;
        }

        

        function llenarcomboservicio(objeto, flag) {
            var opcion = "llenarcomboservicio";
            var xhrtopserv = new XMLHttpRequest();
            xhrtopserv.open("POST", "PagoMasivoDetracciones.aspx?flag=" + opcion, true);
            xhrtopserv.onreadystatechange = function () {
                if (xhrtopserv.readyState == 4 && xhrtopserv.status == 200) {
                    mostrarComboserv(xhrtopserv.responseText);
                }
            }
            xhrtopserv.send(null);
            return false;
        }


        function mostrarComboserv(rptacomboserv) {
            if (rptacomboserv != "") {
                var listas2 = rptacomboserv;
                data1 = crearArray(listas2, "▼");
                listarcomboservicio();
            }
            return false;
        }

        function mostrarCombos(rptacombos) {
            if (rptacombos != "") 
            {
                
                var listas = rptacombos;
                data = crearArray(listas, "▼");
                listarcomboTipoExhibicion();

            }
            return false;
        }

        function crearArray(lista, separador) {
            var data = lista.split(separador);
            if (data.length === 1 && data[0] === "") data = [];
            return data;
        }

        function listarcomboservicio() {
            var TipoServicio = [];
            var nRegistros = data1.length;
            var campos2;
            var TipoServ;
            for (var i = 0; i < nRegistros; i++) {
                campos2 = data1[i].split("|");
                {
                    TipoServ = campos2[0];
                    TipoServ += "|";
                    TipoServ += campos2[1];
                    TipoServicio.push(TipoServicio);
                }
            }
            crearCombo(data1, "cbobienServicio", "|");
        }

        function listarcomboTipoExhibicion() {
            var TipoExhibicion = [];
            var nRegistros = data.length;
            var campos;
            var TipoExh;
            for (var i = 0; i < nRegistros; i++) {
                campos = data[i].split("|");
                {
                    TipoExh = campos[0];
                    TipoExh += "|";
                    TipoExh += campos[1];
                    TipoExhibicion.push(TipoExhibicion);
                }
            }
            crearCombo(data, "cboTipoOperacion", "|");
        }


        function crearCombo(lista, nombreCombo, separador, nombreItem, valorItem) {
            var contenido = "";
            if (nombreItem != null && valorItem != null) {
                contenido += "<option value='";
                contenido += valorItem;
                contenido += "'>";
                contenido += nombreItem;
                contenido += "</option>";
            }
            var nRegistros = lista.length;
            if (nRegistros > 0) {
                var campos;
                for (var i = 0; i < nRegistros; i++) {
                    campos = lista[i].split(separador);
                    contenido += "<option value='";
                    contenido += campos[0];
                    contenido += "'>";
                    contenido += campos[1];
                    contenido += "</option>";
                }
            }
            var cbo = document.getElementById(nombreCombo);
            if (cbo != null) cbo.innerHTML = contenido;

        }

        function fun_listaLote(lista) {
            filas = lista.split("▼");
            crearTablaConsulta();
            crearMatrizConsulta();
            mostrarMatrizConsulta();
            //configurarFiltros();
        }

        function fun_lista(lista) {
            filas = lista.split("▼");
            crearTabla();
            crearMatriz();
            mostrarMatriz();
            configurarFiltros();
        }

        function configurarFiltros() {
            var textos = document.getElementsByClassName("texto");
            var nTextos = textos.length;
            var texto;
            for (var j = 0; j < nTextos; j++) {
                texto = textos[j];
                texto.onkeyup = function () {
                    crearMatriz();
                    mostrarMatriz();
                }
            }
        }

        
        function crearTabla() {
            var nRegistros = filas.length;
   
            var cabeceras = ["", "IdDocumento", "doc_FechaEmision", "doc_NroVoucherConta", "Ruc", "NombreProveedor", "Serie", "codigo", "Moneda", "montoRegimen", "TC", "MontoEnSoles", "can_nroOperacion", "can_montoCancelado", "NumeroCuenta"];
            var nCabeceras = cabeceras.length;
            var contenido = "<table><thead>";
            contenido += "<tr class='GrillaHeader'>";
            for (var j = 0; j < nCabeceras; j++) {

                    contenido += "<th>";
                    contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
                    contenido += "</th>";
                
            }
            contenido += "</tr></thead><tbody id='tbDocumentos' class='GrillaRow'></tbody>";
            contenido += "</table>";
            var div = document.getElementById("GrillaDetracciones");
            div.innerHTML = contenido;
        }



        function crearTablaConsulta() {
            var nRegistros = filas.length;

            var cabeceras = ["", "IdDocumento", "doc_FechaEmision", "doc_NroVoucherConta", "Ruc", "NombreProveedor", "Serie", "codigo", "Moneda", "montoRegimen", "TC", "MontoEnSoles", "can_nroOperacion", "can_montoCancelado", "NumeroCuenta"];
            var nCabeceras = cabeceras.length;
            var contenido = "<table><thead>";
            contenido += "<tr class='GrillaHeader'>";
            for (var j = 0; j < nCabeceras; j++) {

                contenido += "<th>";
                contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
                contenido += "</th>";
            }
            contenido += "</tr></thead><tbody id='tbDocumentosConsulta' class='GrillaRow'></tbody>";
            contenido += "</table>";
            var div = document.getElementById("GrillaDetraccion");
            div.innerHTML = contenido;
        }



        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function crearMatrizConsulta() {
            matrizConsulta = [];
            var nRegistros = filas.length;
            var nCampos;
            var campos;
            var c = 0;
            var exito;
            var textos = document.getElementsByClassName("texto");
            var nTextos = textos.length;
            var texto;
            var x;
            for (var i = 0; i < nRegistros; i++) {
                exito = true;
                campos = filas[i].split("|");
                nCampos = campos.length;
                for (var j = 0; j < nTextos; j++) {
                    texto = textos[j];
                    x = j;

                    if (x < 10) {
                        if (texto.value.length > 0) {
                            if (campos[x] != campos[0]) {
                                exito = campos[x - 1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                            }
                         }
                    }
                    if (x == 10 && x < 13) {
                        if (texto.value.length > 0) {
                            if (campos[x] != campos[0]) {
                                exito = campos[x - 1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                            }
                        }
                    }

                    if (!exito) break;
                }
                if (exito) {
                    matrizConsulta[c] = [];
                    for (var j = 0; j < nCampos; j++) {
                        if (isNaN(campos[j])) matrizConsulta[c][j] = campos[j];

                        else matrizConsulta[c][j] = campos[j];
                    }
                    c++;
                }
            }
        }

        function crearMatriz() {
            matriz = [];
            var nRegistros = filas.length;
            var nCampos;
            var campos;
            var c = 0;
            var exito;
            var textos = document.getElementsByClassName("texto");
            var nTextos = textos.length;
            var texto;
            var x;
            for (var i = 0; i < nRegistros; i++) {
                exito = true;
                campos = filas[i].split("|");
                nCampos = campos.length;
                for (var j =0; j < nTextos; j++) {
                    texto = textos[j];
                    x = j;
        
                    if ( x < 10) {
                        if (texto.value.length > 0) {
                            if (campos[x] != campos[0]) {
                                exito = campos[x - 1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                            }
                        
                        }
                    }
                    if (x == 10 && x<13) {
                        if (texto.value.length > 0) {
                            if (campos[x] != campos[0]) {
                            exito = campos[x-1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        }
                        }
                    }

                    if (!exito) break;
                }
                if (exito) {
                    matriz[c] = [];
                    for (var j = 0; j < nCampos; j++) {
                        if (isNaN(campos[j])) matriz[c][j] = campos[j];

                        else matriz[c][j] = campos[j];
                    }
                    c++;
                }
            }
        }

        function mostrarMatriz() {
            var nRegistros = matriz.length;
            var contenido = "";
            if (nRegistros > 0) {
                var nCampos = matriz[0].length;
                for (var i = 0; i < nRegistros; i++) {
                    
                    if (matriz[i][2] == "POR EXPORTAR") {
                        contenido += "<tr bgcolor='#00FF99'>";
                    }
                    if (matriz[i][2] != "POR EXPORTAR") {
                        contenido += "<tr class='GrillaRow'>";
                    } 


                    for (var j = 0; j < nCampos-8; j++) {
                        if (j == 0) {
                            contenido += "<td>";
                            contenido += "<input id='btnmas' type='image' img src='../ImagenesForm/imgeditar.png' onclick='return MostrarDetalle(";
                            contenido +=  matriz[i][0] ;  
                            contenido += ',"' + matriz[i][4] + '"';                           
                            contenido += "," +'"'+ matriz[i][3] + '"';
                            contenido += "," +'"'+"20138651917"+'"';
                            contenido += "," +'"'+ "SANICENTER S.A.C."+'"';
                            contenido += "," + matriz[i][10];
                            contenido += "," + '"' + matriz[i][1] + '"';
                            contenido += "," + '"' + matriz[i][5] + '"';
                            contenido += "," + '"' + matriz[i][6] + '"';
                            contenido += "," + '"' + matriz[i][13] + '"';
                            contenido += "," + '"' + matriz[i][18] + '"';
                            contenido += "," + '"' + matriz[i][19] + '"';
                            contenido += "," + '"' + matriz[i][20] + '"';
                            contenido += "," + '"' + matriz[i][21] + '"';                              
                            contenido += ")' >";
                            contenido += "</td>";
                            contenido += "<td>";
                            contenido += matriz[i][j];
                            contenido += "</td>";                            
                        }
                        if (j > 0) {
                                contenido += "<td>";
                                contenido += matriz[i][j];
                                contenido += "</td>";  
                        }                    
                    }
                        contenido += "</tr>";
                   
                }
            }
            var spnMensaje = document.getElementById("nroFilas");
            var tabla = document.getElementById("tbDocumentos");
            tabla.innerHTML = contenido;
        }

        function mostrarMatrizConsulta() {
            var nRegistros = matrizConsulta.length;
            var contenido = "";
            if (nRegistros > 0) {
                var nCampos = matrizConsulta[0].length;
                for (var i = 0; i < nRegistros; i++) {

              


                    for (var j = 0; j < nCampos - 8; j++) {
                        if (j == 0) {
                            contenido += "<td>";
                             contenido += "<input id='btnmas' type='image' img src='../ImagenesForm/imgeditar.png' onclick='return Mantenimiento(";
                             contenido += matrizConsulta[i][0];
                             contenido += "," + '"' + matrizConsulta[i][5] + '"';
                             contenido += "," + '"' + matrizConsulta[i][6] + '"';
                             contenido += "," + '"' + matrizConsulta[i][13] + '"';
                          contenido += ")' >";
                          contenido += "</td>";

                            contenido += "<td>";
                            contenido += matrizConsulta[i][j];
                            contenido += "</td>";
                        }
                     

                        if ((j > 0 )&&(j<2)) {

                            contenido += "<td>";
                            contenido += matrizConsulta[i][j];
                            contenido += "</td>";
                        }

                        if (j == 2) {
                            contenido += "<td bgcolor='#F4E39E'>";
                            contenido += matrizConsulta[i][j];
                            contenido += "</td>";
                        }

                        if ((j > 2) && (j < 13)) {

                            contenido += "<td>";
                            contenido += matrizConsulta[i][j];
                            contenido += "</td>";
                        }

                        if (j == 13) 
                        {
                            contenido += "<td bgcolor='#00FF99'>";
                            contenido += matrizConsulta[i][j];
                            contenido += "</td>";
                        }

                        
                    }
                    contenido += "</tr>";
                }
            }
           // var spnMensaje = document.getElementById("nroFilas");
            var tabla = document.getElementById("tbDocumentosConsulta");
            tabla.innerHTML = contenido;
        }




        function Mantenimiento(iddocumento,serie,codigo,constancia) {
            document.getElementById('MantenimientoConstancia').style.display = 'block';
            document.getElementById('txtiddoc').value = iddocumento;
            document.getElementById('txtnrodoc').value = serie + '-' + codigo;
            document.getElementById('txtconstancia').value = constancia;
            return false;
        }

      function MostrarDetalle(iddocumento,nombreproveedor,rucproveedor,rucadquiriente,razonadquiriente,monto,periodo,serie,numero,cta,idpersona,idcuenta,IdTipoOperacion,IdConcepto) {
            document.getElementById('CapaMantenimiento').style.display = 'block';
            document.getElementById('txtidcuenta').value = idcuenta;
            document.getElementById('txtidpersona').value = idpersona;
            document.getElementById('txtiddocumento').value = iddocumento;
            document.getElementById('txtnombreproveedor').value = nombreproveedor;
            document.getElementById('txtrucproveedor').value = rucproveedor;
            document.getElementById('txtrucAdquiriente').value = rucadquiriente;
            document.getElementById('txtrazonsocialadquiriente').value = razonadquiriente;
            document.getElementById('txtmontodeposito').value = monto;
            document.getElementById('txtperiodotributo').value = periodo;
            document.getElementById('txtseriedoc').value = serie;
            document.getElementById('txtnrodocumento').value = numero;
            document.getElementById('txtnumerodetraccion').value = cta;
            if (IdTipoOperacion > 0)
              {
             document.getElementById('cboTipoOperacion').value = IdTipoOperacion;
             }
            if (IdConcepto > 0) 
             {
             document.getElementById('cbobienServicio').value = IdConcepto;
             }
            return false;

        }


        function cerrardetalle() {
            document.getElementById('CapaMantenimiento').style.display = 'none';
            ConsultarRegistros();
            return false;

        }


        function cerrarVentana() {
            document.getElementById('CapaCancelacionDetracciones').style.display = 'none';
         
            return false;

        }

       
function btnExportarArchivo_onclick() {
    var opcion = "ExportarFormato";
 
    var fechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
    var FechaFinal = document.getElementById('<%= TxtFechaFin.ClientID%>').value;

var parts = FechaFinal.split('/');
var mydate = parts[2];
var periodo = mydate;


    var xhrexp1 = new XMLHttpRequest();
    xhrexp1.open("post", "PagoMasivoDetracciones.aspx?flag=" + opcion + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&periodo=" + periodo, true);
    xhrexp1.onreadystatechange = function () {
        if (xhrexp1.readyState == 4 && xhrexp1.status == 200) {
            fun_listaExport(xhrexp1.responseText);
        }
    }
    xhrexp1.send(null);
    return false;

}


function fun_listaExport(listaex) {
    filasex = listaex.split("▼");
    crearTablaEx();
    crearExcelExportar();
    reportegenerado();
}

function crearTablaEx() {
    
    var nRegistrosex = filasex.length;
    var cabecerasex = ["Cadena"];
    var nCabecerasex = cabecerasex.length;
    var contenidoex = "";
    contenidoex = "<table><thead>";
    contenidoex += "<tr class='GrillaHeader'>";
    
    for (var j = 0; j < nCabecerasex; j++) {
        contenidoex += "<th>";
        contenidoex += "<input type='text' class='texto' style='width:95%' placeholder='" + cabecerasex[j] + "'/>";
        contenidoex += "</th>";
    }
    contenidoex += "</tr></thead><tbody id='tbDocumentos' class='GrillaRow'></tbody>";
    contenidoex += "</table>";


}



function crearExcelExportar() {
    crearMatrizReporteExcel()
    excel = ""
    var nRegistros = matrizex.length;
    if (nRegistros > 0) {
        var cabecera = ["cadena"];
        var nCampos = cabecera.length;

        
        for (var i = 0; i < nRegistros; i++) {
            
            for (var j = 0; j < nCampos; j++) {

                excel += matrizex[i][j] + "\r\n";
          }
           
        }
        texto = "";
    }
}

function reportegenerado() {
    var blob = new Blob([excel], { type: 'application/text' });
    //    var blob = new Blob([excel], { type: 'application/vnd.text' });

    if (navigator.appVersion.toString().indexOf('.NET') > 0) {
        window.navigator.msSaveBlob(blob, "Reporte.txt");
    }
    else {
        this.download = "Reporte.txt";
        //this.href = window.URL.createObjectURL(blob);
        this.href = (window.URL || window.webkitURL).createObjectURL(blob);
    }
    excel = "";
    matrizex = []
}

var matrizex = [];

function crearMatrizReporteExcel() {

// var  matrizex = [];    

    var nRegistrosex = filasex.length;
    var nCamposex;
    var camposex;
    var c = 0;
    var exitoex;
    var textos = document.getElementsByClassName("texto");
    var nTextos = textos.length;
    var texto;
    var x;

    for (var i = 0; i < nRegistrosex; i++) {
        exitoex = true;
        camposex = filasex[i].split("|");
        nCamposex = camposex.length;
        for (var j = 0; j < nTextos; j++) {
            texto = textos[j];
            x = j;
            if (x < 10) {
                if (texto.value.length > 0) {
                    exitoex = camposex[x].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                }
            }
            if (x == 10 && x < 13) {
                if (texto.value.length > 0) {
                    exitoex = camposex[x].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                }
            }

            if (!exitoex) break;
        }

        if (exitoex) {
            matrizex[c] = [];
            for (var j = 0; j < nCamposex; j++) {
                if (isNaN(camposex[j])) matrizex[c][j] = camposex[j];
                else matrizex[c][j] = camposex[j];
            }
            c++;
        }
    }
}





</script>

 
</asp:Content>




