﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmRequerimientoGasto_V2
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private listaDetalleConcepto As List(Of Entidades.DetalleConcepto)
    Private listaDocumentoRef_Sustento As List(Of Entidades.Documento)
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private listaCentroCosto As List(Of Entidades.GastoCentroCosto)
    Dim valorx As Integer = 0

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
        Documento_Sustento = 7
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then

            If (Request.QueryString("IdUsuario") <> Nothing And Request.QueryString("IdUsuario") <> " ") Then
                Session("IdUsuario") = CInt(Request.QueryString("IdUsuario"))
            End If

            ConfigurarDatos()
            ValidarPermisos()
            inicializarFrm()
            valOnLoadPage_Request()
        End If
    End Sub

    Private Sub ValidarPermisos()



        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {111, 112, 113, 114, 115, 116})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
            Me.btnImprimir.Visible = False
            'Me.btnVistaPrevia.Visible = False
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
            Me.btnImprimir.Visible = False
            'Me.btnVistaPrevia.Visible = False
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
            Me.btnImprimir.Visible = True
            'Me.btnVistaPrevia.Visible = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* Consultar
            Me.btnBuscar.Enabled = True
            Me.btnImprimir.Visible = False
            'Me.btnVistaPrevia.Visible = False
        Else
            Me.btnBuscar.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* AUTORIZAR (jefe Inmediato)
            Me.Panel_DatosAprobacion.Enabled = True
            Me.btnImprimir.Visible = True
            'Me.btnVistaPrevia.Visible = True
        Else
            Me.Panel_DatosAprobacion.Enabled = False
        End If

        'If listaPermisos(5) > 0 Then  '********* Generar Req. Gasto
        'Me.btnSustentar.Enabled = True
        'Else
        'Me.btnSustentar.Enabled = False
        ' End If
        Me.btnSustentar.Visible = False

    End Sub

    Private Sub valOnLoadPage_Request()

        If (Request.QueryString("IdDocumento") IsNot Nothing) Then

            '********************** CARGA EL DOCUMENTO
            cargarDocumentoREQNav(CInt(Request.QueryString("IdDocumento")), Nothing, Nothing)

        End If

        'If (Request.QueryString("IdUsuario") <> Nothing And Request.QueryString("IdUsuario") <> " ") Then

        '    (Session("IdUsuario") = CInt(Request.QueryString("IdUsuario"))
        'Else
        '    ValidarPermisos()
        'End If



        If (Request.QueryString("IdDocumentoRef") <> Nothing) Then
            cargarDocumentoReferencia(CInt(Request.QueryString("IdDocumentoRef")))
        End If

        If (Request.QueryString("IdDocumento_Sustentar") IsNot Nothing) Then

            '********************** CARGA EL DOCUMENTO
            cargarDocumentoREQ(CInt(Request.QueryString("IdDocumento_Sustentar")), Nothing, Nothing)
            valOnClick_btnSustentar()

        End If

    End Sub

#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

   

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)


        If (objPersona IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersona.Descripcion
            Me.txtDni.Text = objPersona.Dni
            Me.txtRuc.Text = objPersona.Ruc
            Me.hddIdPersona.Value = CStr(objPersona.IdPersona)

            Me.GV_CuentaBancaria.DataSource = (New Negocio.CuentaBancaria).SelectActivoxIdPersona_DT(objPersona.IdPersona)
            Me.GV_CuentaBancaria.DataBind()

        Else

            Throw New Exception("NO SE HALLARON REGISTROS.")

        End If


        objScript.offCapa(Me, "capaPersona")



    End Sub

#End Region

    Private Sub visualizarMoneda()

        If (Me.cboMoneda.SelectedItem IsNot Nothing) Then

            Me.lblMonedaTotal.Text = Me.cboMoneda.SelectedItem.ToString
            Me.lblMoneda_TotalSustentado.Text = Me.cboMoneda.SelectedItem.ToString

        End If

    End Sub

    Private Function getListaDetalleConcepto() As List(Of Entidades.DetalleConcepto)
        Return CType(Session.Item("listaDetalleConceptoGasto"), List(Of Entidades.DetalleConcepto))
    End Function
    Private Sub setListaDetalleConcepto(ByVal lista As List(Of Entidades.DetalleConcepto))
        Session.Remove("listaDetalleConceptoGasto")
        Session.Add("listaDetalleConceptoGasto", lista)
    End Sub

    Private Function getListaCentroCosto() As List(Of Entidades.GastoCentroCosto)
        Return CType(Session.Item("listaCentroCostoGasto"), List(Of Entidades.GastoCentroCosto))
    End Function
    Private Sub setListaCentroCosto(ByVal lista As List(Of Entidades.GastoCentroCosto))
        Session.Remove("listaCentroCostoGasto")
        Session.Add("listaCentroCostoGasto", lista)
    End Sub

    Private Function getListaDocumentoRef_Sustento() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRef_SustentoGasto"), List(Of Entidades.Documento))
    End Function
    Private Sub setListaDocumentoRef_Sustento(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRef_SustentoGasto")
        Session.Add("listaDocumentoRef_SustentoGasto", lista)
    End Sub
    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item(CStr(ViewState("listaDocumentoRef"))), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove(CStr(ViewState("listaDocumentoRef")))
        Session.Add(CStr(ViewState("listaDocumentoRef")), lista)
    End Sub
    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo
                ''
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(CboTipoDocumento, CInt(36), 2, False)
                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
                .LlenarCboMoneda(Me.cboMoneda, False)
                .LLenarCboEstadoDocumento(Me.cboEstado)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True)
                .LlenarCboAreaxIdusuario(Me.cboArea, CInt(Session("IdUsuario")), Nothing, False)
                .LlenarCboAtencion(Me.cboAtencion)

                '*************** COMBOS CENTRO DE COSTO
                .LlenarCboUnidadNegocio(Me.cboUnidadNegocio, False)
                .LlenarCboDeptoFuncional(Me.cboDeptoFuncional, Me.cboUnidadNegocio.SelectedValue, True)
                .LlenarCboSubArea1(Me.cboSubArea1, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, True)
                .LlenarCboSubArea2(Me.cboSubArea2, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, True)
                .LlenarCboSubArea3(Me.cboSubArea3, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, Me.cboSubArea2.SelectedValue, True)



            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaVcto.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaPago.Text = Me.txtFechaEmision.Text
            actualizarOpcionesBusquedaDocRef(0, False)

            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            'Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            'Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text

            '******************   actualizarOpcionesBusquedaDocRef(0, False)

            verFrm(FrmModo.Nuevo, True, True, True, True, True)

            visualizarMoneda()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Protected Sub btnAddConcepto_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddConcepto.Click

        agregarConcepto()

    End Sub

    Private Sub actualizarListaDetalleConcepto()

        Me.listaDetalleConcepto = getListaDetalleConcepto()

        If (GV_DetalleConcepto.Rows.Count > 1) Then
            For i As Integer = 0 To Me.GV_DetalleConcepto.Rows.Count - 1

                Me.listaDetalleConcepto(i).IdConcepto = CInt(CType(Me.GV_DetalleConcepto.Rows(i).FindControl("cboConcepto"), DropDownList).SelectedValue)
                Me.listaDetalleConcepto(i).IdMotivoGasto = CInt(CType(Me.GV_DetalleConcepto.Rows(i).FindControl("cboMotivoGasto"), DropDownList).SelectedValue)
                Me.listaDetalleConcepto(i).Monto = CDec(CType(Me.GV_DetalleConcepto.Rows(i).FindControl("txtMonto"), TextBox).Text)
                Me.listaDetalleConcepto(i).Concepto = CStr(CType(Me.GV_DetalleConcepto.Rows(i).FindControl("txtDescripcionConcepto"), TextBox).Text)
                Me.listaDetalleConcepto(i).IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                'Me.listaDetalleConcepto(i).Monto = CDec(CType(Me.GV_DetalleConcepto.Rows(i).FindControl("txtMonto"), TextBox).Text)
            Next

            setListaDetalleConcepto(Me.listaDetalleConcepto)

        Else

            For i As Integer = 0 To Me.GV_DetalleConcepto.Rows.Count - 1

                Me.listaDetalleConcepto(i).IdConcepto = CInt(CType(Me.GV_DetalleConcepto.Rows(i).FindControl("cboConcepto"), DropDownList).SelectedValue)
                Me.listaDetalleConcepto(i).IdMotivoGasto = CInt(CType(Me.GV_DetalleConcepto.Rows(i).FindControl("cboMotivoGasto"), DropDownList).SelectedValue)
                'Me.listaDetalleConcepto(i).Monto = CDec(CType(Me.GV_DetalleConcepto.Rows(i).FindControl("txtMonto"), TextBox).Text)
                Me.listaDetalleConcepto(i).Concepto = CStr(CType(Me.GV_DetalleConcepto.Rows(i).FindControl("txtDescripcionConcepto"), TextBox).Text)
                Me.listaDetalleConcepto(i).IdMoneda = CInt(Me.cboMoneda.SelectedValue)

                If (CDec(txtMontoTotal.Text) <> 0) Then
                    Me.listaDetalleConcepto(i).Monto = CDec(txtMontoTotal.Text)
                Else
                    '(CDec(txtTotal.Text) > 0) Then
                    Me.listaDetalleConcepto(i).Monto = CDec(txtTotal.Text)
                End If



            Next

            setListaDetalleConcepto(Me.listaDetalleConcepto)
        End If

    End Sub

    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer)
        Dim valorvalidacion As Integer = 3
        Try

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectDocumentoOCxIdDocRef(IdDocumentoRef)

            '*************** GUARDAMOS EN SESSION
            Me.listaDocumentoRef = getlistaDocumentoRef()
            For i As Integer = 0 To Me.listaDocumentoRef.Count - 1
                If (objDocumento.Id = listaDocumentoRef(i).Id) Then
                    valorvalidacion = 0
                    Exit For
                ElseIf (objDocumento.NomMoneda <> listaDocumentoRef(i).NomMoneda) Then
                    valorvalidacion = 4
                Else
                    valorvalidacion = 1

                End If
            Next

            If (valorvalidacion = 0 And listaDocumentoRef.Count > 0) Then
                objScript.mostrarMsjAlerta(Me, "Ya agregó el documento seleccionado.")
                valorvalidacion = 3
            ElseIf (valorvalidacion = 3 And listaDocumentoRef.Count = 0) Then
                Me.listaDocumentoRef.Add(objDocumento)
                setlistaDocumentoRef(Me.listaDocumentoRef)
                Me.GV_DocumentoRef.DataSource = listaDocumentoRef
                Me.GV_DocumentoRef.DataBind()
                valorvalidacion = 3
                Dim TotalAcumulado0 As Decimal = CDec(txtMontoTotal.Text) + (objDocumento.TotalAPagar)
                txtMontoTotal.Text = CStr(Math.Round(TotalAcumulado0, 3))
                lblMoneda.Text = objDocumento.NomMoneda

            ElseIf (valorvalidacion = 4 And listaDocumentoRef.Count > 0) Then
                objScript.mostrarMsjAlerta(Me, "Agrege una O/C con el mismo Tipo de Moneda. No se permite la operación.")
                valorvalidacion = 3
            Else

                Dim TotalAcumulado As Decimal = CDec(txtMontoTotal.Text) + (objDocumento.TotalAPagar)

                txtMontoTotal.Text = CStr(Math.Round(TotalAcumulado, 3))

                Me.listaDocumentoRef.Add(objDocumento)
                setlistaDocumentoRef(Me.listaDocumentoRef)
                With objDocumento
                    Me.hddIdDocumento.Value = CStr(.Id)
                    Me.hddCodigoDocumento.Value = .Codigo
                End With
                Me.GV_DocumentoRef.DataSource = listaDocumentoRef
                Me.GV_DocumentoRef.DataBind()
                valorvalidacion = 3
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub agregarConcepto()
        Try

            If (CInt(Me.hddFrmModo.Value) = FrmModo.Documento_Sustento) Then
                Throw New Exception("NO SE PERMITE LA OPERACIÓN.")
            End If

            actualizarListaDetalleConcepto()

            Me.listaDetalleConcepto = getListaDetalleConcepto()

            Dim objDetalleConcepto As New Entidades.DetalleConcepto
            With objDetalleConcepto

                Dim lista As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.hddIdTipoDocumento.Value))
                lista.Insert(0, (New Entidades.Concepto(0, "-----")))

                Dim listaMotivoGasto As List(Of Entidades.MotivoGasto) = (New Negocio.MotivoGasto).SelectxConceptoxMotivoGasto(.IdConcepto)
                listaMotivoGasto.Insert(0, (New Entidades.MotivoGasto(0, "-----")))

                .IdConcepto = Nothing
                .IdMotivoGasto = Nothing
                .ListaConcepto = lista
                .ListaMotivoGasto = listaMotivoGasto
                .Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
                .Monto = 0

            End With

            Me.listaDetalleConcepto.Add(objDetalleConcepto)

            setListaDetalleConcepto(Me.listaDetalleConcepto)

            Me.GV_DetalleConcepto.DataSource = Me.listaDetalleConcepto
            Me.GV_DetalleConcepto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   calcularTotales_GV_DetalleConcepto();    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub GV_DetalleConcepto_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_DetalleConcepto.RowDataBound
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboConcepto As DropDownList = CType(e.Row.FindControl("cboConcepto"), DropDownList)

                cboConcepto.DataSource = Me.listaDetalleConcepto(e.Row.RowIndex).ListaConcepto
                cboConcepto.DataBind()

                If (cboConcepto.Items.FindByValue(CStr(Me.listaDetalleConcepto(e.Row.RowIndex).IdConcepto)) IsNot Nothing) Then
                    cboConcepto.SelectedValue = CStr(Me.listaDetalleConcepto(e.Row.RowIndex).IdConcepto)
                End If

                Dim cboMotivoGasto As DropDownList = CType(e.Row.FindControl("cboMotivoGasto"), DropDownList)

                cboMotivoGasto.DataSource = Me.listaDetalleConcepto(e.Row.RowIndex).ListaMotivoGasto
                cboMotivoGasto.DataBind()

                If (cboMotivoGasto.Items.FindByValue(CStr(Me.listaDetalleConcepto(e.Row.RowIndex).IdMotivoGasto)) IsNot Nothing) Then
                    cboMotivoGasto.SelectedValue = CStr(Me.listaDetalleConcepto(e.Row.RowIndex).IdMotivoGasto)
                End If

                Dim txtDescripcion As TextBox = CType(e.Row.FindControl("txtDescripcionConcepto"), TextBox)


            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub

    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean, ByVal generarCodigo As Boolean)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then
            Session.Remove("listaDetalleConceptoGasto")
            Session.Remove("listaDocumentoRef_SustentoGasto")
            Session.Remove("listaCentroCostoGasto")
            Session.Remove(CStr(ViewState("listaDocumentoRef")))
        End If

        If (initSession) Then
            Me.listaDetalleConcepto = New List(Of Entidades.DetalleConcepto)
            setListaDetalleConcepto(Me.listaDetalleConcepto)

            Me.listaCentroCosto = New List(Of Entidades.GastoCentroCosto)
            setListaCentroCosto(Me.listaCentroCosto)

            Me.listaDocumentoRef_Sustento = New List(Of Entidades.Documento)
            setListaDocumentoRef_Sustento(Me.listaDocumentoRef_Sustento)

            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setlistaDocumentoRef(Me.listaDocumentoRef)
        End If

        If (initParametrosGral) Then

            '******* FECHA DE VCTO
            'Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {1, 2})

        End If

        If (generarCodigo) Then
            GenerarCodigoDocumento()
        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub
    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaVcto.Text = Me.txtFechaEmision.Text
        Me.txtFechaPago.Text = Me.txtFechaEmision.Text
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto
        Me.txtSerie_BuscarDocRef.Text = ""
        'Me.txtbeneficiario.Text = " "
        Me.txtCodigo_BuscarDocRef.Text = " "
        Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
        Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text
        '************ BENEFICIARIO
        Me.txtDescripcionPersona.Text = ""
        Me.txtDni.Text = ""
        Me.txtRuc.Text = ""
        Me.txtMontoTotal.Text = "0.00"

        '**************** APROBACION
        Me.rdbAutorizado.SelectedValue = "0"  '*********** NO APROBADO
        Me.txtFechaAprobacion.Text = ""
        Me.chbEnviarCxP.Checked = False

        '*********** DETALLES
        Me.GV_DetalleConcepto.DataSource = Nothing
        Me.GV_DetalleConcepto.DataBind()
        Me.GV_Documento_Sustento.DataSource = Nothing
        Me.GV_Documento_Sustento.DataBind()
        Me.GV_CentroCosto.DataSource = Nothing
        Me.GV_CentroCosto.DataBind()
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""

        '************* HIDDEN
        Me.hddCodigoDocumento.Value = ""
        Me.hddIdDocumento.Value = ""
        Me.hddIndex.Value = ""
        Me.hddIdPersona.Value = ""


        '************* OTROS        
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.gvBuscar.DataSource = Nothing
        Me.gvBuscar.DataBind()

        Me.GV_CuentaBancaria.DataSource = Nothing
        Me.GV_CuentaBancaria.DataBind()

    End Sub
    Private Sub ActualizarBotonesControl()

        Me.Panel_Sustento.Visible = False
        Me.Panel_CentroCosto_Grilla.Visible = False
        Me.Panel_CentroCosto_Combos.Visible = False
        Me.Panel_CentroCosto_Grilla.Enabled = False
        Me.Panel_CentroCosto_Combos.Enabled = False
        Me.Panel_CentroCosto.Visible = False

        Me.Panel_SustentoResumen.Visible = False
        Me.Panel_SustentoResumen.Enabled = False

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnSustentar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                'Me.btnVistaPrevia.Visible = False


                Me.Panel_Cab.Enabled = True
                Me.Panel_Persona.Enabled = False
                Me.Panel_Aprobacion.Enabled = False
                Me.Panel_DetalleConcepto.Enabled = False
                Me.Panel_Obs.Enabled = False


                'Me.Panel_Beneficiario.Enabled = False
                Me.Panel_BuscarDocRefxFecha.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Referenciar.Enabled = True

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"

                '************* GENERAMOS CODIGO
                GenerarCodigoDocumento()

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnSustentar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Persona.Enabled = True
                Me.Panel_DetalleConcepto.Enabled = True
                Me.Panel_Obs.Enabled = True
                Me.Panel_Aprobacion.Enabled = True


                Me.Panel_Referenciar.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_BuscarDocRefxFecha.Enabled = True
                'Me.Panel_Beneficiario.Enabled = True
                Me.Panel_BuscarDocRefxCodigo.Enabled = True

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"


                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True

            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnSustentar.Visible = False
                'Me.btnVistaPrevia.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Persona.Enabled = True
                Me.Panel_DetalleConcepto.Enabled = True
                Me.Panel_Obs.Enabled = True
                Me.Panel_Aprobacion.Enabled = True
                'Me.Panel_Beneficiario.Enabled = True
                Me.Panel_BuscarDocRefxCodigo.Enabled = True
                Me.Panel_BuscarDocRefxFecha.Enabled = True
                Me.Panel_Referenciar.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True


                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

                Me.Panel_CentroCosto_Grilla.Visible = True
                Me.Panel_CentroCosto.Visible = True

            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnSustentar.Visible = False
                'Me.btnVistaPrevia.Visible = False
                Me.Panel_DocumentoRef.Enabled = False
                'Me.Panel_Beneficiario.Enabled = False
                Me.Panel_BuscarDocRefxCodigo.Enabled = False
                Me.Panel_BuscarDocRefxFecha.Enabled = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Persona.Enabled = False
                Me.Panel_Aprobacion.Enabled = False
                Me.Panel_DetalleConcepto.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True


            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True
                'Me.btnSustentar.Visible = True
                'Me.btnVistaPrevia.Visible = True


                Me.Panel_Cab.Enabled = False
                Me.Panel_Persona.Enabled = False
                Me.Panel_Aprobacion.Enabled = False
                Me.Panel_DetalleConcepto.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.Panel_DocumentoRef.Enabled = False
                'Me.Panel_Beneficiario.Enabled = False
                Me.Panel_BuscarDocRefxCodigo.Enabled = False
                Me.Panel_BuscarDocRefxFecha.Enabled = False
                Me.Panel_Referenciar.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

                Me.Panel_CentroCosto_Grilla.Visible = True
                Me.Panel_CentroCosto.Visible = True

                Me.Panel_SustentoResumen.Visible = False


            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnSustentar.Visible = False
                'Me.btnVistaPrevia.Visible = True

                Me.btnImprimir.Visible = True
                Me.Panel_Aprobacion.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Persona.Enabled = False
                Me.Panel_DetalleConcepto.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                'Me.Panel_Beneficiario.Enabled = False
                Me.Panel_BuscarDocRefxCodigo.Enabled = False

                Me.Panel_Referenciar.Enabled = False
                Me.Panel_CentroCosto_Grilla.Visible = True
                Me.Panel_CentroCosto.Visible = True

                Me.Panel_SustentoResumen.Visible = False


            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True
                Me.btnSustentar.Visible = False
                'Me.btnVistaPrevia.Visible = True
                Me.Panel_Aprobacion.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Persona.Enabled = False
                Me.Panel_DetalleConcepto.Enabled = False
                Me.Panel_Obs.Enabled = False
                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboEstado.SelectedValue = "2"

                Me.Panel_CentroCosto_Grilla.Visible = True
                Me.Panel_CentroCosto.Visible = True
                Me.Panel_SustentoResumen.Visible = False
                Me.Panel_DocumentoRef.Enabled = False

            Case FrmModo.Documento_Sustento

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnSustentar.Visible = False
                'Me.btnVistaPrevia.visible = True
                Me.btnImprimir.Visible = True
                Me.Panel_Aprobacion.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Persona.Enabled = False
                Me.Panel_DetalleConcepto.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

                Me.Panel_CentroCosto_Grilla.Visible = True
                Me.Panel_CentroCosto_Combos.Visible = True
                Me.Panel_CentroCosto_Grilla.Enabled = True
                Me.Panel_CentroCosto_Combos.Enabled = True
                Me.Panel_CentroCosto.Visible = True

                Me.Panel_SustentoResumen.Visible = False
                Me.Panel_SustentoResumen.Enabled = False

        End Select
    End Sub


    Protected Sub cboConcepto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            actualizarListaDetalleConcepto()

            cargarCboMotivoGasto(CInt(CType(CType(sender, DropDownList).NamingContainer, GridViewRow).RowIndex))

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   calcularTotales_GV_DetalleConcepto();    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarCboMotivoGasto(ByVal index As Integer)

        Me.listaDetalleConcepto = getListaDetalleConcepto()

        Dim listaMotivoGasto As List(Of Entidades.MotivoGasto) = (New Negocio.MotivoGasto).SelectxConceptoxMotivoGasto(Me.listaDetalleConcepto(index).IdConcepto)
        listaMotivoGasto.Insert(0, (New Entidades.MotivoGasto(0, "-----")))
        Me.listaDetalleConcepto(index).IdMotivoGasto = Nothing
        Me.listaDetalleConcepto(index).ListaMotivoGasto = listaMotivoGasto

        setListaDetalleConcepto(Me.listaDetalleConcepto)

        Me.GV_DetalleConcepto.DataSource = Me.listaDetalleConcepto
        Me.GV_DetalleConcepto.DataBind()

    End Sub

    Protected Sub GV_DetalleConcepto_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_DetalleConcepto.SelectedIndexChanged

        quitarDetalleConcepto(Me.GV_DetalleConcepto.SelectedIndex)

    End Sub
    Private Sub quitarDetalleConcepto(ByVal index As Integer)

        Try

            If (CInt(Me.hddFrmModo.Value) = FrmModo.Documento_Sustento) Then
                Throw New Exception("NO SE PERMITE LA OPERACIÓN.")
            End If

            actualizarListaDetalleConcepto()

            Me.listaDetalleConcepto = getListaDetalleConcepto()
            Me.listaDetalleConcepto.RemoveAt(index)

            setListaDetalleConcepto(Me.listaDetalleConcepto)

            Me.GV_DetalleConcepto.DataSource = Me.listaDetalleConcepto
            Me.GV_DetalleConcepto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   calcularTotales_GV_DetalleConcepto();    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub



#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click
        mostrarDocumentosRef_Find()
    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            'Dim IdPersona As Integer = 0


            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                    'IdPersona = CInt(hddIdPersona.Value)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
                    'IdPersona = CInt(hddIdPersona.Value)
            End Select

            '*************  IdTienda = 0  (BUSCA DE TODAS LAS TIENDAS)
            'Dim lista As List(Of Entidades.Documento) = (New Negocio.Documento).DocumentoOCSelectList(serie, codigo, IdPersona, fechaInicio, fechafin)
            Dim lista As List(Of Entidades.Documento) = (New Negocio.Documento).DocumentoOCSelectList(serie, codigo, fechaInicio, fechafin)

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
                Me.Panel_DocRef_Find.Enabled = True
            Else
                Throw New Exception("No se hallaron registros.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged
        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value))
    End Sub

    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()
        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If
        Next

        Return True
    End Function




    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
                'Me.Panel_Beneficiario.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
                'Me.Panel_Beneficiario.Visible = False
                'Case 2
                '    Me.Panel_Beneficiario.Visible = True
                '    Me.Panel_BuscarDocRefxCodigo.Visible = False
                '    Me.Panel_BuscarDocRefxFecha.Visible = False
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)


    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)
        Try
            Me.listaDocumentoRef = getlistaDocumentoRef()

            '*************** QUITAMOS EL DETALLE DEL DOCUMENTO DE REFERENCIA
            Dim i As Integer = (Me.listaDocumentoRef.Count - 1)
            While i >= 0

                If (CInt(Me.listaDocumentoRef(i).IdDocumento) = Me.listaDocumentoRef(index).Id) Then
                    Me.listaDocumentoRef.RemoveAt(i)
                End If
                i = i - 1
            End While

            Me.listaDocumentoRef.RemoveAt(index)
            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)

            Dim Total As Decimal = 0
            txtMontoTotal.Text = " "
            For ii As Integer = 0 To Me.listaDocumentoRef.Count - 1
                Total += CDec(listaDocumentoRef(ii).TotalAPagar)
            Next
            txtMontoTotal.Text = CStr(Math.Round(Total, 3))

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region


    Protected Sub cboMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMoneda.SelectedIndexChanged

        valOnChange_CboMoneda()

    End Sub
    Private Sub valOnChange_CboMoneda()
        Try

            actualizarListaDetalleConcepto()
            'actualizarListaCentroCosto()
            visualizarMoneda()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click

        Try

            verFrm(FrmModo.Nuevo, True, True, True, True, True)
            Me.Panel_Referenciar.Visible = False
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click
        valOnClick_Guardar()
    End Sub
    Private Sub valOnClick_Guardar()

        Dim mensj As Integer = 0
        btnGuardar.Visible = False
        Dim contador As Integer = 0
        Dim Valor As Boolean

        For Each row As GridViewRow In Me.GV_CuentaBancaria.Rows
            Valor = CBool(CType(row.FindControl("chb_Select_CB"), CheckBox).Checked)
            If (Valor = True) Then
                contador += 1
            Else
                contador = contador
            End If

        Next

        If (GV_DetalleConcepto.Rows.Count > 0 And cboTipoOperacion.SelectedValue = "20000001" And contador = 1) Then
            mensj = 3
        ElseIf (GV_DetalleConcepto.Rows.Count > 0 And cboTipoOperacion.SelectedValue <> "20000001" And contador = 0) Then
            mensj = 5
        ElseIf (GV_DetalleConcepto.Rows.Count > 0 And cboTipoOperacion.SelectedValue = "20000001" And contador = 0) Then
            mensj = 8
        ElseIf (contador > 1 And GV_DetalleConcepto.Rows.Count > 0) Then
            mensj = 4
        ElseIf (GV_DetalleConcepto.Rows.Count > 0 And cboTipoOperacion.SelectedValue <> "20000001" And contador > 0) Then
            mensj = 5
        ElseIf (GV_DetalleConcepto.Rows.Count < 0) Then
            mensj = 1
        ElseIf (contador > 1 And GV_DetalleConcepto.Rows.Count < 0) Then
            mensj = 7
        End If


        If (mensj = 4) Then
            objScript.mostrarMsjAlerta(Me, "Debe de relacionar solo una cuenta bancaria al Requerimiento de Gasto. No procede la operación.")
        ElseIf (mensj = 1) Then
            objScript.mostrarMsjAlerta(Me, "Debe de agregar por lo menos un Concepto de gasto al Requerimiento. No procede la operación.")
        ElseIf (mensj = 3 Or mensj = 5) Then
            registrarDocumento()
        ElseIf (mensj = 7) Then
            objScript.mostrarMsjAlerta(Me, "Debe de agregar por lo menos un Concepto  y seleccionar un Banco. No procede la operación.")
        ElseIf (mensj = 8) Then
            objScript.mostrarMsjAlerta(Me, "El beneficiario no posee una cuenta bancaria seleccionada. No procede la operación.")
        End If

        If (valorx <> 2) Then
            btnGuardar.Visible = True
        End If

    End Sub
    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)
        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            Dim objRelacionDocumento As New Entidades.RelacionDocumento
            With objRelacionDocumento
                .IdDocumento1 = Me.listaDocumentoRef(i).Id
                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento2 = Nothing
                    Case FrmModo.Editar
                        .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                End Select

            End With
            lista.Add(objRelacionDocumento)
        Next

        Return lista

    End Function
    

    Private Sub registrarDocumento()

        Try

            actualizarListaDetalleConcepto()

            '**************   validarFrm()

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Dim objAnexoDocumento As Entidades.Anexo_Documento = obtenerAnexoDocumento()
            Dim objMovCuentaCxP As Entidades.MovCuentaPorPagar = obtenerMovCuentaxPagar()
            Me.listaDetalleConcepto = getListaDetalleConcepto()
            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo

                    objDocumento.Id = (New Negocio.RequerimientoGasto).registrarDocumento(objDocumento, objAnexoDocumento, Me.listaDetalleConcepto, objMovCuentaCxP, objObservaciones, listaRelacionDocumento)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")

                Case FrmModo.Editar

                    objDocumento.Id = (New Negocio.RequerimientoGasto).actualizarDocumento(objDocumento, objAnexoDocumento, Me.listaDetalleConcepto, objMovCuentaCxP, objObservaciones, listaRelacionDocumento)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")

            End Select

            verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .FechaCancelacion = CDate(Me.txtFechaPago.Text)
            .FechaVenc = CDate(Me.txtFechaVcto.Text)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdArea = CInt(Me.cboArea.SelectedValue)

            If (CDec(txtTotal.Text) <= 0) Then
                .TotalAPagar = CDec(Me.txtMontoTotal.Text)
                .Total = CDec(Me.txtMontoTotal.Text)
                .ImporteTotal = CDec(Me.txtMontoTotal.Text)
                .SubTotal = CDec(Me.txtMontoTotal.Text)
            Else
                .TotalAPagar = CDec(Me.txtTotal.Text)
                .Total = CDec(Me.txtTotal.Text)
                .ImporteTotal = CDec(Me.txtTotal.Text)
                .SubTotal = CDec(Me.txtTotal.Text)
            End If

        
            .IdPersona = CInt(hddIdPersona.Value)
            .IdUsuarioComision = CInt(cboAtencion.SelectedValue)
            .EstadoRequerimiento = 0
            .TotalLetras = (New Negocio.ALetras).Letras(CStr(Math.Round(.Total, 2))) + "/100 " + (New Negocio.Moneda).SelectxId(.IdMoneda)(0).Descripcion
            .IdRemitente = (CInt(Session("IdUsuario")))

        End With

        Return objDocumento

    End Function
    Private Function obtenerAnexoDocumento() As Entidades.Anexo_Documento

        Dim obj As New Entidades.Anexo_Documento
        Dim Valor As Boolean = False

        With obj

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdArea = CInt(Me.cboArea.SelectedValue)

            Select Case CInt(Me.rdbAutorizado.SelectedValue)
                Case 0  '************ NO APROBADO

                    .Aprobar = False
                    .FechaAprobacion = Nothing
                    .EstadoRequerimiento = 0
                Case 1  '************* APROBADO

                    .Aprobar = True
                    .FechaAprobacion = CDate(Me.txtFechaAprobacion.Text)
                    .EstadoRequerimiento = 1
                    .IdUsuarioSupervisor = CInt(Session("IdUsuario"))
                Case Else
                    Throw New Exception("SE HA SELECCIONADO UNA OPCIÓN DE APROBACIÓN NO EXISTENTE.")
            End Select

            For Each row As GridViewRow In Me.GV_CuentaBancaria.Rows
                Valor = CBool(CType(row.FindControl("chb_Select_CB"), CheckBox).Checked)
                If (Valor = True) Then
                    .Banco = CStr(CType(row.FindControl("lblBanco"), Label).Text)
                    .NroCuentaBancaria = CStr(CType(row.FindControl("lblNroCuentaBancaria"), Label).Text)
                    .IdCuentaBancaria = CInt(CType(row.FindControl("hddIdCuentaBancaria"), HiddenField).Value)
                    .cb_CuentaInterbancaria = CStr(CType(row.FindControl("lblcuentainterbanca"), Label).Text)
                    .cb_SwiftBancario = CStr(CType(row.FindControl("lblswift"), Label).Text)
                    Exit For
                Else
                    .Banco = ""
                    .NroCuentaBancaria = ""
                    .IdCuentaBancaria = 0
                    .cb_CuentaInterbancaria = ""
                    .cb_SwiftBancario = ""
                End If

            Next
        End With

        Return obj

    End Function

    Private Function obtenerMovCuentaxPagar() As Entidades.MovCuentaPorPagar


        Dim objMovCuentaCxP As Entidades.MovCuentaPorPagar = Nothing

        If (Me.chbEnviarCxP.Checked) Then
            objMovCuentaCxP = New Entidades.MovCuentaPorPagar

            With objMovCuentaCxP

                .Monto = CDec(Me.txtTotal.Text)
                .Saldo = CDec(Me.txtTotal.Text)
                .Factor = 1
                .IdProveedor = CInt(hddIdPersona.Value)
                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo

                        .IdDocumento = Nothing

                    Case FrmModo.Editar

                        .IdDocumento = CInt(Me.hddIdDocumento.Value)

                End Select

                .IdMovCuentaTipo = 1

            End With

        End If
        Return objMovCuentaCxP

    End Function

    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservaciones As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservaciones = New Entidades.Observacion

            With objObservaciones

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento = Nothing
                    Case FrmModo.Editar
                        .IdDocumento = CInt(Me.hddIdDocumento.Value)
                End Select

                .Observacion = Me.txtObservaciones.Text.Trim

            End With
        End If

        Return objObservaciones


    End Function
    Protected Sub rdbAutorizado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbAutorizado.SelectedIndexChanged
        valOnChange_rdbAutorizado()
    End Sub
    Private Sub valOnChange_rdbAutorizado()
        Try

            Select Case CInt(Me.rdbAutorizado.SelectedValue)
                Case 0  '*********** NO APROBAR

                    Me.txtFechaAprobacion.Text = ""
                    Me.chbEnviarCxP.Checked = False
                    Me.chbEnviarCxP.Enabled = False

                Case 1

                    Me.txtFechaAprobacion.Text = CStr((New Negocio.FechaActual).SelectFechaActual)
                    Me.chbEnviarCxP.Enabled = False
                    Me.chbEnviarCxP.Checked = True


            End Select


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        anularDocumento()
    End Sub
    Private Sub anularDocumento()
        Try
            Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)
            If ((New Negocio.RequerimientoGasto).anularDocumento(IdDocumento)) Then
                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")
                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False, False)
            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    'Protected Sub btnAddSustento_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    '    valOnClick_btnAddSustento(CType(CType(sender, Button).NamingContainer, GridViewRow).RowIndex)

    'End Sub

    'Private Sub valOnClick_btnAddSustento(ByVal index As Integer)
    '    Try

    '        Me.hddIndex.Value = CStr(index)

    '        Me.listaDetalleConcepto = getListaDetalleConcepto()

    '        Me.listaDocumentoRef_Sustento = (New Negocio.DetalleConcepto_Anexo).DetalleDocumento_Anexo_SelectDocumentoxIdDocumentoxIdDetalleConcepto(CInt(Me.hddIdDocumento.Value), Me.listaDetalleConcepto(index).IdDetalleConcepto)
    '        setListaDocumentoRef_Sustento(Me.listaDocumentoRef_Sustento)

    '        Me.GV_Documento_Sustento.DataSource = Me.listaDocumentoRef_Sustento
    '        Me.GV_Documento_Sustento.DataBind()

    '        Me.Panel_Sustento.Visible = True

    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub btnBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarDocRef.Click

    '    buscarDocumentoReferenciaxParams()

    'End Sub

    'Private Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBuscarDocRef.Click

    '    buscarDocumentoReferenciaxParams()

    'End Sub

    'Private Sub buscarDocumentoReferenciaxParams()
    '    Try

    '        Dim serie As String = ""
    '        Dim codigo As String = ""
    '        Dim fechaIni As Date = Nothing
    '        Dim fechaFin As Date = Nothing

    '        '*******  0: por fechas
    '        '*******  1: por nro documento

    '        Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
    '            Case 0
    '                fechaIni = CDate(Me.txtFechaInicio_DocRef.Text.Trim)
    '                fechaFin = CDate(Me.txtFechaFin_DocRef.Text.Trim)
    '            Case 1
    '                serie = Me.txtSerie_BuscarDocRef.Text.Trim
    '                codigo = Me.txtCodigo_BuscarDocRef.Text.Trim
    '        End Select

    '        '*************  IdTienda = 0  (BUSCA DE TODAS LAS TIENDAS)
    '        '****  Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocGuiaRecepcion).DocumentoGuiaRecepcion_BuscarDocumentoRef(CInt(Me.cboEmpresa.SelectedValue), 0, CInt(Me.cboAlmacen.SelectedValue), IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, fechaInicio, fechafin)
    '        Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef_Externo(0, 0, 0, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, CInt(Me.CboTipoDocumento.SelectedValue), fechaIni, fechaFin, False)

    '        If (lista.Count > 0) Then
    '            Me.GV_DocumentoCab_BuscarDocumento.DataSource = lista
    '            Me.GV_DocumentoCab_BuscarDocumento.DataBind()
    '        Else
    '            Throw New Exception("No se hallaron registros.")
    '        End If

    '        objScript.onCapa(Me, "capaDocumento_Sustento")

    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub

    'Protected Sub GV_DocumentoCab_BuscarDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_DocumentoCab_BuscarDocumento.SelectedIndexChanged

    '    cargarDocumentoRef_Sustento(CInt(CType(Me.GV_DocumentoCab_BuscarDocumento.SelectedRow.FindControl("hddIdDocumento"), HiddenField).Value))

    'End Sub

    'Private Sub cargarDocumentoRef_Sustento(ByVal IdDocumento As Integer)

    '    Try

    '        Me.listaDocumentoRef_Sustento = getListaDocumentoRef_Sustento()

    '        For i As Integer = 0 To Me.listaDocumentoRef_Sustento.Count - 1
    '            If (Me.listaDocumentoRef_Sustento(i).Id = IdDocumento) Then
    '                Throw New Exception("EL DOCUMENTO YA HA SIDO AGREGADO. NO SE PERMITE LA OPERACIÓN.")
    '            End If
    '        Next

    '        Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)

    '        Me.listaDocumentoRef_Sustento.Add(objDocumento)

    '        setListaDocumentoRef_Sustento(Me.listaDocumentoRef_Sustento)

    '        Me.GV_Documento_Sustento.DataSource = Me.listaDocumentoRef_Sustento
    '        Me.GV_Documento_Sustento.DataBind()

    '        objScript.onCapa(Me, "capaDocumento_Sustento")

    '    Catch ex As Exception

    '        objScript.mostrarMsjAlerta(Me, ex.Message)

    '    End Try

    'End Sub



    'Private Sub registrarSustento_CentroCosto()
    '    Try

    '        Dim index As Integer = -1

    '        If (IsNumeric(Me.hddIndex.Value) And Me.hddIndex.Value.Trim.Length > 0) Then
    '            If (CInt(Me.hddIndex.Value) >= 0) Then
    '                index = CInt(Me.hddIndex.Value)
    '            End If
    '        End If

    '        Me.listaDetalleConcepto = getListaDetalleConcepto()
    '        Me.listaDocumentoRef_Sustento = getListaDocumentoRef_Sustento()

    '        Dim listaDetalleConcepto_Anexo As List(Of Entidades.DetalleConcepto_Anexo) = obtenerListaDetalleConcepto_Anexo(index)

    '        '***************** CENTRO DE COSTO
    '        'actualizarListaCentroCosto()
    '        Me.listaCentroCosto = getListaCentroCosto()

    '        '***************** OBSERVACIONES
    '        Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()

    '        Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)
    '        Dim IdDetalleConcepto As Integer = Nothing
    '        If (index >= 0) Then

    '            IdDetalleConcepto = Me.listaDetalleConcepto(index).IdDetalleConcepto

    '        End If

    '        If ((New Negocio.RequerimientoGasto).actualizarSustento_CentroCosto_Documento(IdDocumento, IdDetalleConcepto, listaDetalleConcepto_Anexo, Me.listaCentroCosto, objObservaciones)) Then
    '            objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")

    '            Me.GV_Documento_Sustento.DataSource = Nothing
    '            Me.GV_Documento_Sustento.DataBind()

    '            Me.listaDocumentoRef_Sustento = New List(Of Entidades.Documento)
    '            setListaDocumentoRef_Sustento(Me.listaDocumentoRef_Sustento)

    '            Me.hddIndex.Value = ""
    '            Me.Panel_Sustento.Visible = False

    '            '*******************  MANEJO DE LOS BOTONES DE REC INGRESO Y EGRESO
    '            actualizarControles_ReciboIE()

    '        Else

    '            Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")

    '        End If



    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub
    Private Sub actualizarControles_ReciboIE()

        '*******************  MANEJO DE LOS BOTONES DE REC INGRESO Y EGRESO
        Dim totalSustentado As Decimal = (New Negocio.RequerimientoGasto).fx_getTotalSustentadoxIdDocumentoReqGasto(CInt(Me.hddIdDocumento.Value))
        Dim totalDocumento As Decimal = CDec(Me.txtTotal.Text)
        Me.txtTotalSustento.Text = Format(totalSustentado, "F3")

        Me.btn_EmitirReciboIngreso.Visible = False
        Me.btn_EmitirReciboEgreso.Visible = False

        If (totalDocumento > totalSustentado) Then
            Me.btn_EmitirReciboIngreso.Visible = True
        ElseIf (totalDocumento < totalSustentado) Then
            Me.btn_EmitirReciboEgreso.Visible = True
        End If

    End Sub
    Private Function obtenerListaDetalleConcepto_Anexo(ByVal index As Integer) As List(Of Entidades.DetalleConcepto_Anexo)

        Dim lista As New List(Of Entidades.DetalleConcepto_Anexo)
        Me.listaDetalleConcepto = getListaDetalleConcepto()
        Me.listaDocumentoRef_Sustento = getListaDocumentoRef_Sustento()

        For i As Integer = 0 To Me.listaDocumentoRef_Sustento.Count - 1

            Dim obj As New Entidades.DetalleConcepto_Anexo

            With obj
                .IdDetalleConcepto = Me.listaDetalleConcepto(index).IdDetalleConcepto
                .IdDetalleConcepto_Anexo = Nothing
                .IdDocumento = CInt(Me.hddIdDocumento.Value)
                .IdDocumentoRef = Me.listaDocumentoRef_Sustento(i).Id

            End With

            lista.Add(obj)

        Next

        Return lista

    End Function
    Private Sub btnSustentar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSustentar.Click

        valOnClick_btnSustentar()

    End Sub
    Private Sub valOnClick_btnSustentar()
        Try

            If (Me.rdbAutorizado.SelectedValue = "1" And CInt(Me.cboEstado.SelectedValue) = 1) Then

                Me.listaDetalleConcepto = getListaDetalleConcepto()
                Me.listaCentroCosto = getListaCentroCosto()

                verFrm(FrmModo.Documento_Sustento, False, False, False, False, False)

                Me.hddIndex.Value = ""

                Me.GV_DetalleConcepto.DataSource = Me.listaDetalleConcepto
                Me.GV_DetalleConcepto.DataBind()

                Me.GV_CentroCosto.DataSource = Me.listaCentroCosto
                Me.GV_CentroCosto.DataBind()

            Else

                Throw New Exception("EL DOCUMENTO NO HA SIDO APROBADO Y/O SE ENCUENTRA ANULADO. NO SE PERMITE LA OPERACIÓN.")

            End If


        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub
    Private Sub cargarDocumentoREQNav(ByVal IdDocumento As Integer, ByVal IdSerie As Integer, ByVal codigo As Integer)

        Try

            Dim objDocumento As Entidades.Documento = Nothing

            If (IdDocumento = Nothing) Then
                objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
            Else
                objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
            End If

            If (objDocumento Is Nothing) Then
                Throw New Exception("NO SE HALLARON REGISTROS.")
            End If

            '***************** DETALLE CONCEPTO
            Me.listaDetalleConcepto = obtenerListaDetalleConcepto_Load(objDocumento.Id)
            Me.listaDocumentoRef = (New Negocio.DocGuiaRemision).DocumentoSolicitudPagpSelectDocumentoRef(objDocumento.Id)
            '***************** ANEXO DOCUMENTO
            Dim objAnexoDocumento As Entidades.Anexo_Documento = (New Negocio.Anexo_Documento).Anexo_DocumentoSelectxIdDocumento(objDocumento.Id)

            Dim objPersona As Entidades.PersonaView = New Entidades.PersonaView
            '****************** PERSONA 
            objPersona = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)


            Dim objAtencion As Entidades.PersonaView = New Entidades.PersonaView
            objAtencion = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdUsuarioComision)

            '****************** OBSERVACIONES
            Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

            '********************* GASTO CENTRO COSTO
            Me.listaCentroCosto = (New Negocio.GastoCentroCosto).SelectxIdDocumento(objDocumento.Id)

            '*********************** REGISTRADO EN MOV CUENTA POR PAGAR
            Dim movCuentaCXP As Boolean = False
            If (((New Negocio.Util).ValidarExistenciaxTablax1Campo("MovCuentaPorPagar", "IdDocumento", CStr(objDocumento.Id))) > 0) Then
                movCuentaCXP = True
            End If
            setlistaDocumentoRef(Me.listaDocumentoRef)


            cargarDocumentoREQ_GUINav(objDocumento, objPersona, objAnexoDocumento, objObservaciones, movCuentaCXP, Me.listaDetalleConcepto, Me.listaCentroCosto, Me.listaDocumentoRef, objAtencion)

            '********************* ALMACENA EN SESSION
            setListaDetalleConcepto(Me.listaDetalleConcepto)
            setListaCentroCosto(Me.listaCentroCosto)
            setlistaDocumentoRef(Me.listaDocumentoRef)
            '*******************  MANEJO DE LOS BOTONES DE REC INGRESO Y EGRESO
            actualizarControles_ReciboIE()

            '****************** EDITAR
            verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, False)

            Me.Panel_Referenciar.Visible = False


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub
    Private Sub cargarDocumentoREQ(ByVal IdDocumento As Integer, ByVal IdSerie As Integer, ByVal codigo As Integer)

        Try

            Dim objDocumento As Entidades.Documento = Nothing

            If (IdDocumento = Nothing) Then
                objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
            Else
                objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
            End If

            If (objDocumento Is Nothing) Then
                Throw New Exception("NO SE HALLARON REGISTROS.")
            End If

            '***************** DETALLE CONCEPTO
            Me.listaDetalleConcepto = obtenerListaDetalleConcepto_Load(objDocumento.Id)
            Me.listaDocumentoRef = (New Negocio.DocGuiaRemision).DocumentoSolicitudPagpSelectDocumentoRef(objDocumento.Id)
            '***************** ANEXO DOCUMENTO
            Dim objAnexoDocumento As Entidades.Anexo_Documento = (New Negocio.Anexo_Documento).Anexo_DocumentoSelectxIdDocumento(objDocumento.Id)

            Dim objPersona As Entidades.PersonaView = New Entidades.PersonaView
            '****************** PERSONA 
            objPersona = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)


            Dim objAtencion As Entidades.PersonaView = New Entidades.PersonaView
            objAtencion = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdUsuarioComision)

            '****************** OBSERVACIONES
            Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

            '********************* GASTO CENTRO COSTO
            Me.listaCentroCosto = (New Negocio.GastoCentroCosto).SelectxIdDocumento(objDocumento.Id)

            '*********************** REGISTRADO EN MOV CUENTA POR PAGAR
            Dim movCuentaCXP As Boolean = False
            If (((New Negocio.Util).ValidarExistenciaxTablax1Campo("MovCuentaPorPagar", "IdDocumento", CStr(objDocumento.Id))) > 0) Then
                movCuentaCXP = True
            End If
            setlistaDocumentoRef(Me.listaDocumentoRef)


            cargarDocumentoREQ_GUI(objDocumento, objPersona, objAnexoDocumento, objObservaciones, movCuentaCXP, Me.listaDetalleConcepto, Me.listaCentroCosto, Me.listaDocumentoRef, objAtencion)

            '********************* ALMACENA EN SESSION
            setListaDetalleConcepto(Me.listaDetalleConcepto)
            setListaCentroCosto(Me.listaCentroCosto)
            setlistaDocumentoRef(Me.listaDocumentoRef)
            '*******************  MANEJO DE LOS BOTONES DE REC INGRESO Y EGRESO
            actualizarControles_ReciboIE()

            '****************** EDITAR
            verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, False)

            Me.Panel_Referenciar.Visible = False


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub
    Private Function obtenerListaDetalleConcepto_Load(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleConcepto)

        Dim listaDetalleConcepto As List(Of Entidades.DetalleConcepto) = (New Negocio.DetalleConcepto).SelectxIdDocumento(IdDocumento)

        For i As Integer = 0 To listaDetalleConcepto.Count - 1

            Dim lista As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(CInt(Me.hddIdTipoDocumento.Value))
            lista.Insert(0, (New Entidades.Concepto(0, "-----")))

            Dim listaMotivoGasto As List(Of Entidades.MotivoGasto) = (New Negocio.MotivoGasto).SelectxConceptoxMotivoGasto(listaDetalleConcepto(i).IdConcepto)
            listaMotivoGasto.Insert(0, (New Entidades.MotivoGasto(0, "-----")))

            listaDetalleConcepto(i).ListaConcepto = lista
            listaDetalleConcepto(i).ListaMotivoGasto = listaMotivoGasto

        Next

        Return listaDetalleConcepto

    End Function
    Private Sub cargarDocumentoREQ_GUINav(ByVal objDocumento As Entidades.Documento, ByVal objPersona As Entidades.PersonaView, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal objObservaciones As Entidades.Observacion, ByVal movCuentaCXP As Boolean, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal listaGastoCentroCosto As List(Of Entidades.GastoCentroCosto), ByVal listaDocumento As List(Of Entidades.Documento), ByVal objAtencion As Entidades.PersonaView)

        Dim objCbo As New Combo

        '************************ CABECERA
        If (objDocumento IsNot Nothing) Then


            Me.txtCodigoDocumento.Text = objDocumento.Codigo
            Me.cboSerie.SelectedItem.Text = CStr(objDocumento.Serie)

            If (objDocumento.FechaEmision <> Nothing) Then
                Me.txtFechaEmision.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
            End If

            If (objDocumento.FechaVenc <> Nothing) Then
                Me.txtFechaVcto.Text = Format(objDocumento.FechaVenc, "dd/MM/yyyy")
            Else
                Me.txtFechaVcto.Text = ""
            End If
            If (objDocumento.FechaCancelacion <> Nothing) Then
                Me.txtFechaPago.Text = Format(objDocumento.FechaCancelacion, "dd/MM/yyyy")
            Else
                Me.txtFechaPago.Text = ""
            End If
            If (Me.cboTienda.Items.FindByValue(CStr(objDocumento.IdTienda)) IsNot Nothing) Then
                Me.cboTienda.SelectedValue = CStr(objDocumento.IdTienda)
            End If





            If (Me.cboEstado.Items.FindByValue(CStr(objDocumento.IdEstadoDoc)) IsNot Nothing) Then

                Me.cboEstado.SelectedValue = CStr(objDocumento.IdEstadoDoc)

            End If

            If (Me.cboMoneda.Items.FindByValue(CStr(objDocumento.IdMoneda)) IsNot Nothing) Then

                Me.cboMoneda.SelectedValue = CStr(objDocumento.IdMoneda)

            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then

                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)

            End If




            '******************* TOTALES



            Me.txtMontoTotal.Text = CStr(Math.Round(objDocumento.TotalAPagar, 3))

            Me.txtTotal.Text = CStr(Math.Round(objDocumento.TotalAPagar, 3))


            Me.hddIdDocumento.Value = CStr(objDocumento.Id)

            Me.hddCodigoDocumento.Value = (objDocumento.Codigo)
        End If

        'Dim Total As Decimal = 0

        'For i As Integer = 0 To Me.listaDocumentoRef.Count - 1
        '    Total += CDec(listaDocumentoRef(i).TotalAPagar)
        'Next
        'txtMontoTotal.Text = CStr(Math.Round(Total, 3))


        '****************** PROVEEDOR
        If (objPersona IsNot Nothing) Then

            cargarPersona(objPersona.IdPersona)

        End If

        '********************** ANEXO DOCUMENTO - APROBACION - MONTOS 
        If (objAnexoDocumento IsNot Nothing) Then

            With objAnexoDocumento

                If (.Aprobar) Then
                    Me.rdbAutorizado.SelectedValue = "1"
                    Me.txtFechaAprobacion.Text = CStr(.FechaAprobacion)
                    Me.chbEnviarCxP.Enabled = True
                Else
                    Me.rdbAutorizado.SelectedValue = "0"
                    Me.txtFechaAprobacion.Text = ""
                    Me.chbEnviarCxP.Enabled = False
                End If
                Me.chbEnviarCxP.Checked = movCuentaCXP
            End With

            If (objAnexoDocumento.NroCuentaBancaria <> Nothing And objAnexoDocumento.IdCuentaBancaria <> 0 And objAnexoDocumento.Banco <> Nothing) Then
                Dim lista As List(Of Entidades.Anexo_Documento) = New List(Of Entidades.Anexo_Documento)
                objAnexoDocumento.IdBanco = 0
                lista.Add(objAnexoDocumento)

                GV_CuentaBancaria.DataSource = lista
                GV_CuentaBancaria.DataBind()

                For Each row As GridViewRow In Me.GV_CuentaBancaria.Rows
                    Dim check As CheckBox = (CType(row.FindControl("chb_Select_CB"), CheckBox))
                    check.Checked = True
                    Exit For
                Next
            Else
                GV_CuentaBancaria.DataSource = Nothing
                GV_CuentaBancaria.DataBind()
            End If



            If (Me.cboArea.Items.FindByValue(CStr(objAnexoDocumento.IdArea)) IsNot Nothing) Then

                Me.cboArea.SelectedValue = CStr(objAnexoDocumento.IdArea)

            End If



            If (objAtencion IsNot Nothing) Then
                If (Me.cboAtencion.Items.FindByValue(CStr(objAtencion.IdPersona)) IsNot Nothing) Then
                    Me.cboAtencion.SelectedValue = CStr(objAtencion.IdPersona)
                End If
            End If


            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then

                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)

            End If



        End If

        '************************** LISTA DETALLE CONCEPTO
        Me.GV_DetalleConcepto.DataSource = listaDetalleConcepto
        Me.GV_DetalleConcepto.DataBind()

        '************************** LISTA GASTO CENTRO COSTO
        Me.GV_CentroCosto.DataSource = listaGastoCentroCosto
        Me.GV_CentroCosto.DataBind()


        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()
        '**************************** OBSERVACIONES
        If (objObservaciones IsNot Nothing) Then

            Me.txtObservaciones.Text = objObservaciones.Observacion

        End If

    End Sub
    Private Sub cargarDocumentoREQ_GUI(ByVal objDocumento As Entidades.Documento, ByVal objPersona As Entidades.PersonaView, ByVal objAnexoDocumento As Entidades.Anexo_Documento, ByVal objObservaciones As Entidades.Observacion, ByVal movCuentaCXP As Boolean, ByVal listaDetalleConcepto As List(Of Entidades.DetalleConcepto), ByVal listaGastoCentroCosto As List(Of Entidades.GastoCentroCosto), ByVal listaDocumento As List(Of Entidades.Documento), ByVal objAtencion As Entidades.PersonaView)

        Dim objCbo As New Combo

        '************************ CABECERA
        If (objDocumento IsNot Nothing) Then
            Me.txtCodigoDocumento.Text = objDocumento.Codigo
            If (objDocumento.FechaEmision <> Nothing) Then
                Me.txtFechaEmision.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
            End If
            If (objDocumento.FechaVenc <> Nothing) Then
                Me.txtFechaVcto.Text = Format(objDocumento.FechaVenc, "dd/MM/yyyy")
            Else
                Me.txtFechaVcto.Text = ""
            End If
            If (objDocumento.FechaCancelacion <> Nothing) Then
                Me.txtFechaPago.Text = Format(objDocumento.FechaCancelacion, "dd/MM/yyyy")
            Else
                Me.txtFechaPago.Text = ""
            End If
            If (Me.cboEstado.Items.FindByValue(CStr(objDocumento.IdEstadoDoc)) IsNot Nothing) Then
                Me.cboEstado.SelectedValue = CStr(objDocumento.IdEstadoDoc)
            End If
            If (Me.cboMoneda.Items.FindByValue(CStr(objDocumento.IdMoneda)) IsNot Nothing) Then
                Me.cboMoneda.SelectedValue = CStr(objDocumento.IdMoneda)
            End If
            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
            End If




            '******************* TOTALEs
            Me.txtMontoTotal.Text = CStr(Math.Round(objDocumento.TotalAPagar, 3))
            Me.txtTotal.Text = CStr(Math.Round(objDocumento.TotalAPagar, 3))
            Me.hddIdDocumento.Value = CStr(objDocumento.Id)
            Me.hddCodigoDocumento.Value = (objDocumento.Codigo)
        End If

        'Dim Total As Decimal = 0

        'For i As Integer = 0 To Me.listaDocumentoRef.Count - 1
        '    Total += CDec(listaDocumentoRef(i).TotalAPagar)
        'Next
        'txtMontoTotal.Text = CStr(Math.Round(Total, 3))


        '****************** PROVEEDOR
        If (objPersona IsNot Nothing) Then
            cargarPersona(objPersona.IdPersona)
        End If

        '********************** ANEXO DOCUMENTO - APROBACION - MONTOS 
        If (objAnexoDocumento IsNot Nothing) Then
            With objAnexoDocumento
                If (.Aprobar) Then
                    Me.rdbAutorizado.SelectedValue = "1"
                    Me.txtFechaAprobacion.Text = CStr(.FechaAprobacion)
                    Me.chbEnviarCxP.Enabled = True
                Else
                    Me.rdbAutorizado.SelectedValue = "0"
                    Me.txtFechaAprobacion.Text = ""
                    Me.chbEnviarCxP.Enabled = False
                End If
                Me.chbEnviarCxP.Checked = movCuentaCXP
            End With

            If (objAnexoDocumento.NroCuentaBancaria <> Nothing And objAnexoDocumento.IdCuentaBancaria <> 0 And objAnexoDocumento.Banco <> Nothing) Then
                Dim lista As List(Of Entidades.Anexo_Documento) = New List(Of Entidades.Anexo_Documento)
                objAnexoDocumento.IdBanco = 0
                lista.Add(objAnexoDocumento)
                GV_CuentaBancaria.DataSource = lista
                GV_CuentaBancaria.DataBind()
                For Each row As GridViewRow In Me.GV_CuentaBancaria.Rows
                    Dim check As CheckBox = (CType(row.FindControl("chb_Select_CB"), CheckBox))
                    check.Checked = True
                    Exit For
                Next

                'For Each row As GridViewRow In Me.GV_CuentaBancaria.Rows
                '    Dim validando As Integer = CInt(CType(row.FindControl("hddIdCuentaBancaria"), HiddenField).Value)
                'Next


            Else
                GV_CuentaBancaria.DataSource = Nothing
                GV_CuentaBancaria.DataBind()
            End If
            If (Me.cboArea.Items.FindByValue(CStr(objAnexoDocumento.IdArea)) IsNot Nothing) Then
                Me.cboArea.SelectedValue = CStr(objAnexoDocumento.IdArea)
            End If
            If (objAtencion IsNot Nothing) Then
                If (Me.cboAtencion.Items.FindByValue(CStr(objAtencion.IdPersona)) IsNot Nothing) Then
                    Me.cboAtencion.SelectedValue = CStr(objAtencion.IdPersona)
                End If
            End If
            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
            End If
        End If
        '************************** LISTA DETALLE CONCEPTO
        Me.GV_DetalleConcepto.DataSource = listaDetalleConcepto
        Me.GV_DetalleConcepto.DataBind()

        '************************** LISTA GASTO CENTRO COSTO
        Me.GV_CentroCosto.DataSource = listaGastoCentroCosto
        Me.GV_CentroCosto.DataBind()


        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()
        '**************************** OBSERVACIONES
        If (objObservaciones IsNot Nothing) Then

            Me.txtObservaciones.Text = objObservaciones.Observacion

        End If

    End Sub
    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click

        cargarDocumentoREQ(Nothing, CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value))

    End Sub
    'Protected Sub GV_Documento_Sustento_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Documento_Sustento.SelectedIndexChanged
    '    quitarDocumento_Sustento(Me.GV_Documento_Sustento.SelectedIndex)
    'End Sub
    'Private Sub quitarDocumento_Sustento(ByVal index As Integer)
    '    Try


    '        Me.listaDocumentoRef_Sustento = getListaDocumentoRef_Sustento()
    '        Me.listaDocumentoRef_Sustento.RemoveAt(index)

    '        setListaDocumentoRef_Sustento(Me.listaDocumentoRef_Sustento)

    '        Me.GV_Documento_Sustento.DataSource = Me.listaDocumentoRef_Sustento
    '        Me.GV_Documento_Sustento.DataBind()

    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub
#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdPersona.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub GV_BusquedaAvanzado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_BusquedaAvanzado.PageIndexChanging
        Me.GV_BusquedaAvanzado.PageIndex = e.NewPageIndex
        busquedaAvanzado()
    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            cargarDocumentoREQ(CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value), 0, 0)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region

    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditar.Click
        valOnClick_btnEditar()
    End Sub
    Private Sub valOnClick_btnEditar()
        Try
            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDetalleConcepto = getListaDetalleConcepto()
            Me.listaCentroCosto = getListaCentroCosto()

            Dim valor As Entidades.Anexo_Documento = New Entidades.Anexo_Documento
            valor.IdDocumento = CInt(hddIdDocumento.Value)
            
            Dim objDocumento As Entidades.Anexo_Documento = (New Negocio.RequerimientoGasto).selectxidval_req_aprob(valor)
            If (objDocumento.EstadoRequerimiento = 1 Or objDocumento.EstadoRequerimiento = 2) Then
                objScript.mostrarMsjAlerta(Me, "No se puede modificar ya que el documento ya fue Aprobado/Desaprobado.")
            Else
                verFrm(FrmModo.Editar, False, False, False, False, False)

                Me.GV_DetalleConcepto.DataSource = Me.listaDetalleConcepto
                Me.GV_DetalleConcepto.DataBind()

                Me.GV_CentroCosto.DataSource = Me.listaCentroCosto
                Me.GV_CentroCosto.DataBind()

                Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
                Me.GV_DocumentoRef.DataBind()

                Me.Panel_Referenciar.Visible = True
               
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
    Private Sub cboUnidadNegocio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUnidadNegocio.SelectedIndexChanged
        valOnChange_cboUnidadNegocio()
    End Sub
    Private Sub valOnChange_cboUnidadNegocio()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboDeptoFuncional(Me.cboDeptoFuncional, Me.cboUnidadNegocio.SelectedValue, True)
                .LlenarCboSubArea1(Me.cboSubArea1, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, True)
                .LlenarCboSubArea2(Me.cboSubArea2, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, True)
                .LlenarCboSubArea3(Me.cboSubArea3, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, Me.cboSubArea2.SelectedValue, True)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub valOnChange_cboDeptoFuncional()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboSubArea1(Me.cboSubArea1, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, True)
                .LlenarCboSubArea2(Me.cboSubArea2, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, True)
                .LlenarCboSubArea3(Me.cboSubArea3, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, Me.cboSubArea2.SelectedValue, True)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub valOnChange_cboSubArea1()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboSubArea2(Me.cboSubArea2, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, True)
                .LlenarCboSubArea3(Me.cboSubArea3, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, Me.cboSubArea2.SelectedValue, True)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub valOnChange_cboSubArea2()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboSubArea3(Me.cboSubArea3, Me.cboUnidadNegocio.SelectedValue, Me.cboDeptoFuncional.SelectedValue, Me.cboSubArea1.SelectedValue, Me.cboSubArea2.SelectedValue, True)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboDeptoFuncional_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDeptoFuncional.SelectedIndexChanged
        valOnChange_cboDeptoFuncional()
    End Sub
    Private Sub cboSubArea1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubArea1.SelectedIndexChanged
        valOnChange_cboSubArea1()
    End Sub
    Private Sub cboSubArea2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubArea2.SelectedIndexChanged
        valOnChange_cboSubArea2()
    End Sub
    'Private Sub btnAdd_CentroCosto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd_CentroCosto.Click
    '    add_CentroCosto()
    'End Sub

    'Private Sub valAddCentroCosto()

    '    Dim IdCentroCosto As String = Me.cboUnidadNegocio.SelectedValue + Me.cboDeptoFuncional.SelectedValue + Me.cboSubArea1.SelectedValue + Me.cboSubArea2.SelectedValue + Me.cboSubArea3.SelectedValue
    '    Me.listaCentroCosto = getListaCentroCosto()

    '    For i As Integer = 0 To Me.listaCentroCosto.Count - 1

    '        If (Me.listaCentroCosto(i).IdCentroCosto = IdCentroCosto) Then
    '            Throw New Exception("EL CENTRO DE COSTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
    '        End If

    '    Next


    'End Sub

    'Private Sub add_CentroCosto()
    '    Try

    '        valAddCentroCosto()

    '        actualizarListaCentroCosto()

    '        Me.listaCentroCosto = getListaCentroCosto()

    '        Dim objCentroCosto As New Entidades.GastoCentroCosto
    '        With objCentroCosto

    '            .IdCentroCosto = Me.cboUnidadNegocio.SelectedValue + Me.cboDeptoFuncional.SelectedValue + Me.cboSubArea1.SelectedValue + Me.cboSubArea2.SelectedValue + Me.cboSubArea3.SelectedValue
    '            .Monto = 0
    '            .Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
    '            .UnidadNegocio = CStr(Me.cboUnidadNegocio.SelectedItem.ToString)
    '            .DeptoFuncional = CStr(Me.cboDeptoFuncional.SelectedItem.ToString)
    '            .SubArea1 = CStr(Me.cboSubArea1.SelectedItem.ToString)
    '            .SubArea2 = CStr(Me.cboSubArea2.SelectedItem.ToString)
    '            .SubArea3 = CStr(Me.cboSubArea3.SelectedItem.ToString)

    '        End With
    '        Me.listaCentroCosto.Add(objCentroCosto)
    '        setListaCentroCosto(Me.listaCentroCosto)

    '        Me.GV_CentroCosto.DataSource = Me.listaCentroCosto
    '        Me.GV_CentroCosto.DataBind()

    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub

    'Private Sub GV_CentroCosto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_CentroCosto.SelectedIndexChanged
    '    quitarCentroCosto(Me.GV_CentroCosto.SelectedIndex)
    'End Sub
    'Private Sub quitarCentroCosto(ByVal index As Integer)
    '    Try

    '        actualizarListaCentroCosto()

    '        Me.listaCentroCosto = getListaCentroCosto()

    '        Me.listaCentroCosto.RemoveAt(index)

    '        Me.GV_CentroCosto.DataSource = Me.listaCentroCosto
    '        Me.GV_CentroCosto.DataBind()

    '        setListaCentroCosto(Me.listaCentroCosto)

    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub

    'Private Sub actualizarListaCentroCosto()

    '    Me.listaCentroCosto = getListaCentroCosto()

    '    For i As Integer = 0 To Me.GV_CentroCosto.Rows.Count - 1

    '        Me.listaCentroCosto(i).Moneda = CStr(Me.cboMoneda.SelectedItem.ToString)
    '        Me.listaCentroCosto(i).Monto = CDec(CType(Me.GV_CentroCosto.Rows(i).FindControl("txtMonto"), TextBox).Text)

    '    Next

    '    setListaCentroCosto(Me.listaCentroCosto)

    'End Sub


    'Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
    '    actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    'End Sub
    'Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

    '    '*******  0: por fechas
    '    '*******  1: por nro documento

    '    Select Case opcion
    '        Case 0
    '            Me.Tr_fecha.Visible = True
    '            Me.Tr_serie_codigo.Visible = False
    '        Case 1
    '            Me.Tr_fecha.Visible = False
    '            Me.Tr_serie_codigo.Visible = True
    '    End Select

    '    Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
    '    If (onCapa) Then
    '        objScript.onCapa(Me, "capaDocumento_Sustento")
    '    End If

    'End Sub

    'Private Sub btnConsultarDocumentoExterno_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConsultarDocumentoExterno.Click
    '    Try
    '        If Not Session("IdDocumentoExt") Is Nothing Then
    '            cargarDocumentoRef_Sustento(CInt(Session("IdDocumentoExt")))
    '        Else
    '            objScript.mostrarMsjAlerta(Me, "No ha registrado un documento.")
    '        End If
    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub
    Protected Sub btnMostrarReferencia_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMostrarReferencia.Click
        Panel_Referenciar.Visible = True
    End Sub
    Protected Sub btnOcultarReferencia_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnOcultarReferencia.Click
        Panel_Referenciar.Visible = False
    End Sub

    Private Sub btBuscarPersonaGrillas_Click(sender As Object, e As System.EventArgs) Handles btBuscarPersonaGrillas.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 0)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class