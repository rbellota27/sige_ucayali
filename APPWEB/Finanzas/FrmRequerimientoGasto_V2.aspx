<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmRequerimientoGasto_V2.aspx.vb" Inherits="APPWEB.FrmRequerimientoGasto_V2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table class="style1 ">
        <tr>
            <td>
                <div id="divEspera" style="display:none;text-align:center;position:fixed;left:0px;top:0px;background-color:White;">
                <h1>Espera un momento...</h1>
                <img src="../Imagenes/loader.gif" alt="" />
            </div>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" ToolTip="Nuevo" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                                Text="Editar" ToolTip="Editar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                                Text="Anular" ToolTip="Anular" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" OnClientClick="return( valSaveDocumento(this));"
                                Text="Guardar" ToolTip="Guardar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  valOnClick_btnImprimir()  );"
                                Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnSustentar" runat="server" Text="Sustento" ToolTip="Ingresar Sustento"
                                Width="80px" />
                        </td>
                        <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                REQUERIMIENTO DE GASTO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True" >
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarDocumentoxCodigo">                                
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( valOnKeyPressCodigoDoc(this)  );"
                                    ReadOnly="true" Width="100px"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" ToolTip="Buscar Documento por [ Serie - N�mero ]." />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="B�squeda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender0" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender0" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Fecha Vcto.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaVcto" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaVcto" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaVcto" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaVcto">
                                </cc1:CalendarExtender>
                            </td>
                          
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                         <td align="right" class="Texto">
                                Fecha de Pago:
                            </td>
                            <td>
                            <asp:TextBox ID="txtFechaPago" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaPago">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaPago" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaPago">
                                </cc1:CalendarExtender>
                            </td>
                              <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Moneda:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboMoneda" runat="server" AutoPostBack="True" CssClass="LabelRojo"
                                    Font-Bold="true" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Tipo Operaci�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                �rea:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboArea" Width="100%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                          <tr>
                        <td align="right" class="Texto">
                            Atenci�n:
                        </td>
                  
                        <td colspan="5">
                            <asp:DropDownList ID="cboAtencion" runat="server" AutoPostBack="True" CssClass="LabelRojo"
                                Font-Bold="true" Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                BENEFICIARIO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Persona" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Descripci�n:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionPersona" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                    Width="400px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersona" OnClientClick="return( mostrarCapaPersona()  );"
                                    runat="server" Text="Buscar" ToolTip="Buscar beneficiario" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDni" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRuc" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto" colspan="4">
                                <asp:GridView ID="GV_CuentaBancaria" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:CheckBox ID="chb_Select_CB" runat="server" onclick=" return(  valOnClick_chb_Select_CB(this)  ); " />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Banco" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:HiddenField ID="hddIdCuentaBancaria" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCuentaBancaria") %>' />

                                                            <asp:Label ID="lblBanco" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Banco") %>'></asp:Label>

                                                            <asp:HiddenField ID="hddIdBanco" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdBanco") %>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Nro. Cuenta" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblNroCuentaBancaria" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NroCuentaBancaria") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Nro. Cuenta Interbancaria" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblcuentainterbanca" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"cb_CuentaInterbancaria") %>'></asp:Label>
 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                         <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Swift" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
  
                                                            <asp:Label ID="lblswift" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"cb_SwiftBancario") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="GrillaRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaCabecera" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
        <td>
        
        <asp:Button id="btnMostrarReferencia" runat="server"  Text="Mostrar Opciones para Referenciar Documento" Width="30%" ToolTip="Seleccione para Referenciar un Documento, una vez seleccionado podr� buscar el documentos por serie y c�digo o por rango de fechas." />
         <asp:Button id="btnOcultarReferencia" runat="server"  Text="Ocultar Opciones para Referenciar Documento" Width="30%" ToolTip="Seleccione para Ocutar la opcion Referenciar un Documento." />
        </td>

        <%--<td>
       <a CssClass="tooltip" href="#demo2_tip" onmouseover="tooltip.pop(this, '#demo2_tip')">Hover me</a>
         <div style="display:none;">
            <div id="demo2_tip">
              
                <b>SEO</b><br /><br />
                The tooltip content comes from an element on the page. So this approach is <strong>Search Engine Friendly</strong>.
            </div>
        </div>
        </td>--%>
    
        </tr>
         <tr>
                        <td class="SubTituloCelda">
                          DOCUMENTO DE REFERENCIA
                        </td>
                    </tr>
         <tr>
                        <td>
                            <asp:Panel ID="Panel_DocumentoRef" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                              <Columns>
                         <asp:CommandField SelectText="Quitar" ShowSelectButton="True" ItemStyle-HorizontalAlign="Center"
                               HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" >
                             <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                             </asp:CommandField>
                        
                           <asp:TemplateField ShowHeader="False" HeaderText="Documento"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" >
                            <ItemTemplate>
                                           <asp:Label ID="lbldocumento" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>' Font-Bold="true" style="color:Red " ></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            
                        </asp:TemplateField> 
                        
                        <asp:TemplateField HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" HeaderText="Nro. Documento" 
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                             <asp:Label ID="lblnrodoc" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>' style="color:Red "  Font-Bold="true"></asp:Label>

                                            <asp:HiddenField ID="hddIdDocumentoRef" runat="server" 
                                                Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                            </ItemTemplate>

                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                     </asp:TemplateField>
                       <asp:TemplateField HeaderStyle-Height="25px" 
                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Monto" 
                                        ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                    <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" 
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblMonto" runat="server" Font-Bold="true" 
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>

                                              <asp:HiddenField ID="hddIdMoneda2" runat="server"  Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                </ItemTemplate>

                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                   </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" HeaderText="Fecha de Emisi�n">
                           <ItemTemplate>
                              <asp:Label ID="lblfechaemision" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FechaEmision","{0:dd/MM/yyyy}")%>' ></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField> 
                        <asp:TemplateField ShowHeader="False" HeaderText="Fecha de Vcto.">
                            <ItemTemplate>
                                           <asp:Label ID="lblfechavenci" runat="server" Font-Bold="true" 
                                            Text='<%#DataBinder.Eval(Container.DataItem,"FechaVenc","{0:dd/MM/yyyy}" )%>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField> 

                  <asp:TemplateField ShowHeader="False" HeaderText="Beneficiario">
                            <ItemTemplate>
                                           <asp:Label ID="lbldescpersona" runat="server" Font-Bold="true" 
                                            Text='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>     
                          
 
                       <asp:TemplateField ShowHeader="False" HeaderText="Estado" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                           <ItemTemplate>
                              <asp:Label ID="lblestado" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomEstadoDocumento")%>' ></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>   
                         


                                        </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    
                                </table>
                                <table >
                              
                                </table>
                                <table border="2px"  >
                                <tr>
                                <td class="SubTituloCelda"  >
                                       MONTO TOTAL :             
                                </td>
                                 <td>
                                  <asp:Label ID="lblMoneda" Text=" " runat="server" style="color:Red ;font-weight:bolder " ></asp:Label>
                                    
                                </td>
                                
                                <td>
                                          <asp:TextBox ID="txtMontoTotal" runat="server" Text="0.00" ReadOnly="true" ToolTip="Monto Calculado" style="background-color:#F3F781;color:Red ;font-weight:bolder;text-align:center  "  ></asp:TextBox>
                                </td>
                                 </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    
                    <tr>
            <td class="TituloCelda">
                REFERENCIAR ORDEN DE COMPRA
            </td>
             
            
        </tr>
        <tr>
        <td>
            <asp:Panel ID="Panel_Referenciar" runat="server" Visible="false" >
             
           <table style="width: 100%;">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoDocumento" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisi�n</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    <%--<asp:ListItem Value="2">Por Beneficiario</asp:ListItem>--%>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos" runat="server" Text="Buscar" CssClass="btnBuscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:Panel ID="Panel5" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                            <asp:Panel ID="Panel6" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                            
                        </tr>
                       <%-- <tr>
                        <td>
                         <asp:Panel ID="Panel_Beneficiario" runat="server">
                                    <table>
                                       <tr>
                                          <td class="Texto" align="right">
                                                 <asp:Label ID="lblbeneficiario" runat="server" Text="Beneficiario: "></asp:Label>  
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtbeneficiario" runat="server" style="background-color:#FFFFCC" Width="400px"  ></asp:TextBox>
                                            </td>
                                              <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxBeneficiario" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                      </tr>
                                    </table>
                                </asp:Panel>
                                </td>
                                </tr>--%>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" Height="250px" ScrollBars="Vertical" Width="1200px">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="1150px">
                             <Columns>
                        
                        <asp:CommandField  HeaderText="Opciones" ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center" 
                                    HeaderStyle-Width="50px" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ButtonType="Button"  />
                                    
                           <asp:TemplateField ShowHeader="False" HeaderText="Documento"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                           <asp:Label ID="lbltipodoc" runat="server" Font-Bold="true" 
                                            Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            
                        </asp:TemplateField> 
                        
                       
                        <asp:TemplateField HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" HeaderText="Nro. Documento" 
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                            <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                            <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" 
                                                Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                              <asp:HiddenField ID="hddIdPersona" runat="server"  Value='<%#DataBinder.Eval(Container.DataItem,"IdUsuario")%>' />

                                            <asp:HiddenField ID="hddNumDocu" runat="server" 
                                                Value='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>' />

                                         <asp:HiddenField ID="hddbeneficiario" runat="server" 
                                         Value='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'/>
                            </ItemTemplate>

                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                     </asp:TemplateField>
                     
                        <asp:TemplateField ShowHeader="False" HeaderText="Fecha de Emisi�n">
                            <ItemTemplate>
                                           <asp:Label ID="lblfechaemision" runat="server" Font-Bold="true" 
                                            Text='<%#DataBinder.Eval(Container.DataItem,"FechaEmision","{0:dd/MM/yyyy}" )%>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField> 
                     
                         <asp:TemplateField ShowHeader="False" HeaderText="Fecha de Vcto.">
                            <ItemTemplate>
                                           <asp:Label ID="lblfechavenci" runat="server" Font-Bold="true" 
                                            Text='<%#DataBinder.Eval(Container.DataItem,"FechaVenc","{0:dd/MM/yyyy}" )%>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField> 
                     
                     
                     <asp:TemplateField HeaderStyle-Height="25px" 
                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Monto" 
                                        ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                    <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" 
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                              <asp:HiddenField ID="hddIdMoneda" runat="server"  Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                    <asp:Label ID="lblMonto" runat="server" Font-Bold="true" 
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>

                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                   </asp:TemplateField>
                          
                          
                        <asp:TemplateField ShowHeader="False" HeaderText="Beneficiario">
                            <ItemTemplate>
                                           <asp:Label ID="lbldescpersona" runat="server" Font-Bold="true" 
                                            Text='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>     
                          
 
                       <asp:TemplateField ShowHeader="False" HeaderText="Estado" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                           <asp:Label ID="lblestadodoc" runat="server" Font-Bold="true" 
                                            Text='<%#DataBinder.Eval(Container.DataItem,"NomEstadoDocumento")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>   
                         
                      <asp:TemplateField HeaderStyle-Height="25px" 
                                         HeaderStyle-HorizontalAlign="Center" HeaderText="" 
                                          ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                                  <asp:ImageButton ID="btnMostrarDetalle" runat="server" 
                                                                    ImageUrl="~/Imagenes/search_add.ico"  OnClientClick=" return( valOnClick_btnMostrarDetalle(this)  ); "
                                                                    ToolTip="Visualizar Detalle" Width="20px" />
                                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                      </asp:TemplateField>
                      
                        <asp:TemplateField ShowHeader="False" HeaderText="Impresi�n">
                            <ItemTemplate>
                                <asp:Button ID="btnimprimir" runat="server" 
                                    Text="Imprimir" OnClientClick="return(valOnClick_btnImprimirExpo(this));" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        
                           <asp:TemplateField ShowHeader="False" HeaderText="Id DocAutog.">
                            <ItemTemplate>
                               
                                       <asp:Label ID="lblIddocumentoo" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text='<%#DataBinder.Eval(Container.DataItem,"Id")%>'></asp:Label>
                                       
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" HeaderText="Tienda">
                            <ItemTemplate>
                               
                                       <asp:Label ID="lblTienda" runat="server" Font-Bold="true"
                                                Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>
                                       
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        
                        
                        
                                        </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        </asp:Panel>
         </td>
        </tr>
        
        <tr>
            <td align="center">
                <asp:Panel ID="Panel_Aprobacion" runat="server">
                    <table>
                        <tr>
                            <td class="SubTituloCelda">
                                DATOS DE APROBACI�N
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_DatosAprobacion" runat="server">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:RadioButtonList ID="rdbAutorizado" runat="server" RepeatDirection="Horizontal"
                                                TextAlign="Right" CssClass="Texto" Font-Bold="true" AutoPostBack="True">
                                                <asp:ListItem Value="1">S�</asp:ListItem>
                                                <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td style="width: 30px">
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            Fecha Aprobaci�n:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaAprobacion" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                                Width="180px" runat="server"></asp:TextBox>
                                        </td>
                                        <td style="width: 30px">
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chbEnviarCxP" runat="server" Text="Enviar a Cuentas por Pagar"
                                                CssClass="Texto" Font-Bold="true" Enabled="false" />
                                        </td>
                                    </tr>
                                </table>
                                </asp:Panel> 
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                CONCEPTOS
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_DetalleConcepto" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Button ID="btnAddConcepto" runat="server" Text="Agregar Concepto" Width="130px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DetalleConcepto" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                        <asp:TemplateField HeaderText="Concepto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="25px">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboConcepto" runat="server" DataTextField="Nombre" DataValueField="Id"
                                                    OnSelectedIndexChanged="cboConcepto_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Motivo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="25px">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboMotivoGasto" runat="server" DataTextField="Nombre_MG" DataValueField="Id">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Descripci�n" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="25px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDescripcionConcepto" runat="server" Width="200px" TabIndex="200"
                                                    Text='<%# DataBinder.Eval(Container.DataItem,"Concepto") %>' onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Monto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="25px">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMoneda_MontoConcepto" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Moneda") %>'
                                                                ForeColor="Red" Font-Bold="true"></asp:Label>

                                                            <asp:TextBox ID="txtMonto" Width="90px" onfocus="return(aceptarFoco(this));" runat="server"
                                                                onKeyUp=" return( calcularTotales_GV_DetalleConcepto()  ); " onblur="  return( valBlur(event) );  "
                                                                TabIndex="250" Text='<%# DataBinder.Eval(Container.DataItem,"Monto","{0:F3}") %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                   
                                    </Columns>
                                    <RowStyle CssClass="GrillaRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaCabecera" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaTotal" runat="server" Text="-" Font-Bold="true" CssClass="LabelRojo"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTotal" runat="server" CssClass="TextBox_ReadOnly" onFocus="return( lostFocus() );"
                                                onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Panel ID="Panel_Sustento" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: center">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnAddDocumento_Sustento" runat="server" OnClientClick=" return(  valOnClick_btnAddDocumento_Sustento()  ); "
                                                                Text="Agregar Documento de Sustento" Width="220px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GV_Documento_Sustento" runat="server" AutoGenerateColumns="False"
                                                    Width="100%">
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <Columns>
                                                        <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" SelectText="Quitar" ShowSelectButton="True" />
                                                        <asp:BoundField DataField="NomTipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                                            <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Height="25px"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Emisi�n" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="DescripcionPersona" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderText="Proveedor" ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                                            <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Panel ID="Panel_SustentoResumen" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Total Sustentado:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMoneda_TotalSustentado" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                    Text="-"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTotalSustento" Width="80px" CssClass="TextBox_ReadOnly" ReadOnly="true"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                           
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr id="Tr1" runat="server" visible = "false">
                                            <td>
                                                <asp:Button ID="btn_EmitirReciboIngreso" runat="server" Text="Ingreso" ToolTip="Emitir Recibo de Ingreso"
                                                    Width="80px" OnClientClick=" return( valOnClick_btn_EmitirReciboIngreso() );  " />
                                            </td>
                                            <td>
                                                <asp:Button ID="btn_EmitirReciboEgreso" runat="server" Text="Egreso" ToolTip="Emitir Recibo de Egreso"
                                                    Width="80px" OnClientClick=" return( valOnClick_btn_EmitirReciboEgreso() );  " />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_CentroCosto" runat="server">
                    <table width="100%">
                        <tr>
                            <td class="SubTituloCelda">
                                CENTRO DE COSTOS
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_CentroCosto_Combos" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto" style="text-align: right; font-weight: bold">
                                                Centro de Costo:
                                            </td>
                                            <td style="width: 400px">
                                                <asp:DropDownList ID="cboUnidadNegocio" runat="server" Width="100%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAdd_CentroCosto" runat="server" Text="Agregar" Width="70px" ToolTip="Agregar Centro de Costo." />
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto" style="text-align: right; font-weight: bold">
                                                Sub Area Funcional:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboDeptoFuncional" runat="server" Width="100%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto" style="text-align: right; font-weight: bold">
                                                Concepto Gasto:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboSubArea1" runat="server" Width="100%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto" style="text-align: right; font-weight: bold">
                                                Detalle Gasto:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboSubArea2" runat="server" Width="100%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto" style="text-align: right; font-weight: bold">
                                                Sub Detalle Gasto:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboSubArea3" runat="server" Width="100%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_CentroCosto_Grilla" runat="server">
                                    <asp:GridView ID="GV_CentroCosto" runat="server" AutoGenerateColumns="False" Width="100%">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Und. Negocio" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-Height="25px">
                                                <ItemTemplate>
                                                                <asp:Label ID="lblUnidadNegocio" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadNegocio")%>'></asp:Label>

                                                                <asp:HiddenField ID="hddIdCentroCosto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCentroCosto")%>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="DeptoFuncional" HeaderText="Depto. Funcional" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:BoundField DataField="SubArea1" HeaderText="Sub �rea 1" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:BoundField DataField="SubArea2" HeaderText="Sub �rea 2" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:BoundField DataField="SubArea3" HeaderText="Sub �rea 3" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Monto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-Height="25px">
                                                <ItemTemplate>
                                                                <asp:Label ID="lblMoneda_Monto" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>

                                                                <asp:TextBox ID="txtMonto" TabIndex="500" Width="70px" Font-Bold="true" onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); "
                                                                    onFocus=" return( aceptarFoco(this) ); " onblur=" return( valBlur(event) ); " runat="server"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaCabecera" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                OBSERVACIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" Width="100%" Font-Bold="true"
                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                    runat="server" Height="100px" Style="margin-bottom: 0px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="36" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddIndex" runat="server" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                  <asp:HiddenField ID="hdd_IdUsuario" runat="server" />
                
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos"  CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%" AllowPaging="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de b�squeda realiza un filtrado de acuerdo al Beneficiario seleccionado.
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarPersonaGrillas">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrillas">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel4" runat="server" DefaultButton="btBuscarPersonaGrillas">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />--%>
                                                                            <asp:Button ID="btBuscarPersonaGrillas" runat="server" Text="Buscar" CssClass="btnBuscar"
                                                                        OnClientClick="this.disabled;this.value='Procesando...'" UseSubmitBehavior="false"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
 <%--   <div id="capaDocumento_Sustento" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumento_Sustento')   );" />
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    BUSCAR DOCUMENTO
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoDocumento" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRefx" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisi�n</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr id="Tr_fecha" runat="server">
                <td>
                    <table>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="Texto">
                                Fecha Inicio:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtFechaInicio_DocRef">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="Texto">
                                Fecha Fin:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtFechaFin_DocRef">
                                </cc1:CalendarExtender>
                            </td>
                            <td>
                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos"
                                    runat="server" Text="Buscar" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="Tr_serie_codigo" runat="server">
                <td>
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Serie:
                            </td>
                            <td>
                                <asp:TextBox ID="txtSerie_BuscarDocRef" Width="70px" Font-Bold="true" onFocus=" return( aceptarFoco(this) ); "
                                    runat="server"></asp:TextBox>
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                Nro.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtCodigo_BuscarDocRef" Width="80px" Font-Bold="true" onFocus=" return( aceptarFoco(this) ); "
                                    runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarDocRef" Width="80px" runat="server" Text="Buscar" OnClientClick=" return( valOnClick_btnBuscarDocRef() ); " />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_DocumentoCab_BuscarDocumento" runat="server" AutoGenerateColumns="False"
                        Width="100%">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderText=""
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Proveedor" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:LinkButton ID="btnIrPagina_RegistrarDocumentoExterno" OnClientClick="return( valOnClick_btnIrPagina_RegistrarDocumentoExterno() );"
                                    runat="server" CssClass="Texto" Font-Bold="true">Registrar Documento ...</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnConsultarDocumentoExterno"
                                    runat="server" CssClass="Texto" Font-Bold="true">Agregar el Documento Registrado por esta interfaz ...</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>--%>

     
    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };

////        $(function() {

////            $("#lnk").tooltip({ placement: 'Top' });

////        });


//        $(function() {
//        $("#lnk").popover({ title: 'Twitter Bootstrap Popover', content: "It's so simple to create a tooltop for my website!" });
//        });


        function ReplaceTags(xStr) {
            var regExp = /<\/?[^>]+>/gi;
            xStr = xStr.replace(regExp, "");
            return xStr;
        }
        
        function valOnClick_btnImprimirExpo(control) {
            var IdDocumento = "";
            var row = control.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var hddIdDocumento = row.cells[10].innerHTML;
            IdDocumento = ReplaceTags(hddIdDocumento);

            var iddoc = parseInt(IdDocumento);

            window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=16&IdDocumento=' + iddoc, 'Compras', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        } 





        function valOnClick_btnMostrarDetalle(control) {

            var grilla = document.getElementById('<%=GV_DocumentosReferencia_Find.ClientID%>');
            var IdDocumento = 0;
            var IdUsuario = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var boton = rowElem.cells[8].children[0];
                    var hddIdDocumento = rowElem.cells[2].children[1];
                    var hddIdUsuario = rowElem.cells[2].children[2]; 
                    if (control.id == boton.id) {
                        IdDocumento = parseInt(hddIdDocumento.value);
                        IdUsuario = parseInt(hddIdUsuario.value)
                        break;
                    }


                }
            }

            window.open('../../Compras/frmOrdenCompra1.aspx?IdDocumento=' + IdDocumento + '&IdUsuario=' + IdUsuario, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }
        


        //***************************************************** BUSQUEDA PERSONA
        function valOnClick_VistaPrevia() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Impresi�n.');
                return false;
            }
            window.open('../../Caja/Reportes/Visor1.aspx?iReporte=8&IdDocumento=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }
        function valOnClick_btnImprimir() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Impresi�n.');
                return false;
            }
            window.open('../../Caja/Reportes/Visor1.aspx?iReporte=9&IdDocumento=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrillas.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj) {
            var key = event.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrillas.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }


        function calcularTotales_GV_DetalleConcepto() {

            var grilla = document.getElementById('<%=GV_DetalleConcepto.ClientID%>');
            var txtMonto;
            var monto = 0;
            var montoAcumulado = 0;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    txtMonto = rowElem.cells[4].children[1];
                    monto = parseFloat(txtMonto.value);
                    if (isNaN(monto)) { monto = 0; }

                    montoAcumulado = montoAcumulado + monto;


                } //********* FIN FOR                                                                
            }

            document.getElementById('<%=txtTotal.ClientID%>').value = montoAcumulado;

            return false;
        }

        function lostFocus() {
            document.getElementById('<%=btnAddConcepto.ClientID%>').focus();
            return false;
        }
        function ocultarDivCarga() {
            var espera = document.getElementById("divEspera");
            espera.style.display = "none";
        }
        function valSaveDocumento(btn) {
            
            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie de Documento.');
                return false;
            }

            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }

            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            if (isNaN(parseInt(cboMoneda.value)) || cboMoneda.value.length <= 0) {
                alert('Debe seleccionar una Moneda de Emisi�n.');
                return false;
            }

            var cboArea = document.getElementById('<%=cboArea.ClientID%>');
            if (isNaN(parseInt(cboArea.value)) || cboArea.value.length <= 0 || parseInt(cboArea.value) <= 0) {
                alert('Debe seleccionar un �rea.');
                return false;
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0 || parseInt(hddIdPersona.value) <= 0) {
                alert('Debe seleccionar un Remitente.');
                return false;
            }

            var grilla = document.getElementById('<%=GV_DetalleConcepto.ClientID%>');
            var txtMonto;
            var cboConcepto;
            var cboMotivoGasto;
            var monto = 0;


            if (grilla == null) {

                alert('DEBE SELECCIONAR UN CONCEPTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    txtMonto = rowElem.cells[4].children[1];
                    monto = parseFloat(txtMonto.value);
                    if (isNaN(monto)) { monto = 0; }

                    if (monto <= 0) {
                        alert('DEBE INGRESAR UN VALOR V�LIDO. NO SE PERMITE LA OPERACI�N.');
                        txtMonto.select();
                        txtMonto.focus();
                        return false;
                    }

                    cboConcepto = rowElem.cells[1].children[0];
                    if (isNaN(parseInt(cboConcepto.value)) || cboConcepto.value.length <= 0 || parseInt(cboConcepto.value) <= 0) {
                        alert('DEBE SELECCIONAR UN CONCEPTO. NO SE PERMITE LA OPERACI�N.');
                        cboConcepto.focus();
                        return false;
                    }

                    cboMotivoGasto = rowElem.cells[2].children[0];
                    //                    if (isNaN(parseInt(cboMotivoGasto.value)) || cboMotivoGasto.value.length <= 0 || parseInt(cboMotivoGasto.value) <= 0) {
                    //                        alert('DEBE SELECCIONAR UN MOTIVO DE GASTO. NO SE PERMITE LA OPERACI�N.');
                    //                        cboMotivoGasto.focus();
                    //                        return false;
                    //                    }

                } //********* FIN FOR                                                                
            }

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
            var mensaje = '';
            //            switch (parseInt(hddFrmModo.value)) {
            //                case 1: //******** NUEVO
            //                    mensaje = 'Desea continuar con la Operaci�n ?';
            //                    break;
            //                case 2: //******** EDITAR
            //                    mensaje = 'Desea continuar con la Operaci�n ?';
            //                    break;
            //            }

            mensaje = 'Desea continuar con la Operaci�n ?';

//            var espera = document.getElementById("divEspera");

//            var respuesta = confirm(mensaje);
//            if (respuesta) {
//                btn.disabled = true;
//                btn.value = "Procesando...";
//                return true;
//            } else {
//                return false;
            //            }
            return confirm(mensaje);
        }

        function valOnClick_btnBuscarDocRef() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseFloat(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('INGRESE UN VALOR V�LIDO. NO SE PERMITE LA OPERACI�N.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseFloat(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('INGRESE UN VALOR V�LIDO. NO SE PERMITE LA OPERACI�N.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }
        function valOnClick_btnAddDocumento_Sustento() {
            onCapa('capaDocumento_Sustento');
            return false;
        }
        function valOnClick_btnIrPagina_RegistrarDocumentoExterno() {

            window.open('FrmDocumentoExterno_V2.aspx', null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }
        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }
        function valOnKeyPressCodigoDoc(txtCodigoDocumento) {
            var key = event.keyCode;
            if (key == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }

        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor v�lido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }
        function valOnClickEditar() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Edici�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite su edici�n.');
                return false;
            }
            return true;
        }
        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para su Anulaci�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya est� ANULADO. No se permite su anulaci�n.');
                return false;
            }
            return confirm('Desea continuar con el Proceso de ANULACI�N del Documento ?');
        }

        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');

        }

        function valOnClick_btn_EmitirReciboIngreso() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            window.open('FrmPage_Load.aspx?Tipo=1&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }

        function valOnClick_btn_EmitirReciboEgreso() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            window.open('FrmPage_Load.aspx?Tipo=2&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }

        function valOnClick_chb_Select_CB(checkBox) {

            var grilla = document.getElementById('<%=GV_CuentaBancaria.ClientID%>');
            var txtObservaciones = document.getElementById('<%=txtObservaciones.ClientID%>');
            var cad = '';
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[0].id == checkBox.id) {

                        if (checkBox.checked) {
                            var banco = rowElem.cells[1].children[1].innerHTML;
                            var nroCuenta = rowElem.cells[2].children[0].innerHTML;
                            cad = cad + 'Banco: ' + banco + '. Cuenta Bancaria:' + nroCuenta;
                            break;
                        }

                    }

                }
            }

            txtObservaciones.value = txtObservaciones.value + ' / ' + cad;

            return true;
        }
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>


</asp:Content>
