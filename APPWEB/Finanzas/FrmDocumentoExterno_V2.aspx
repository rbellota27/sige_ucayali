<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmDocumentoExterno_V2.aspx.vb" Inherits="APPWEB.FrmDocumentoExterno_V2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" ToolTip="Nuevo" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                                Text="Editar" ToolTip="Editar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                                Text="Anular" ToolTip="Anular" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" OnClientClick="return( valSaveDocumento()  );"
                                Text="Guardar" ToolTip="Guardar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  valOnClick_btnImprimir()  );"
                                Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" Visible="false" />
                        </td>
                        <td>
                           <asp:Button id="btnReporteDocExternos" runat="server" Text="Buscar Doc. Externos" Width="160px" OnClientClick="return(MostrarCapaReporteDocExterno());"/>
                        </td>
                            <td>
                           <asp:Button id="btnCambiarCC" runat="server" Text="Cambiar Centro de Costos" Width="160px"/>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DOCUMENTOS EXTERNOS
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboEmpresa" runat="server" Width="100%" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTienda" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Tipo Documento:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoDocumento" runat="server" AutoPostBack="true" CssClass="LabelRojo"
                                    Font-Bold="true" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                Serie:
                            </td>
                            <td>
                                <asp:TextBox ID="txtSerie" onFocus=" return(  aceptarFoco(this)  ); " runat="server"
                                    Width="150px" Text="" Font-Bold="true"></asp:TextBox>
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender1" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Fecha Vcto.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaVcto" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaVcto0" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaVcto0" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaVcto">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto" style="font-weight: bold">
                                &nbsp; Nro.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNroDocumento" runat="server" Font-Bold="true" onFocus=" return(  aceptarFoco(this)  ); "
                                    onKeypress=" return( valOnKeyPress_txtNroDocumento(event) ); " Text="" Width="150px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" ToolTip="Buscar Documento por [ Serie - N�mero ]." />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Moneda:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboMoneda" runat="server" Width="100%" AutoPostBack="True"
                                    CssClass="LabelRojo" Font-Bold="true">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Tipo Operaci�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                PROVEEDOR
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Persona" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Descripci�n:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionPersona" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                    Width="400px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersona" OnClientClick="return( mostrarCapaPersona()  );"
                                    runat="server" Text="Buscar" ToolTip="Buscar beneficiario" />
                            </td>
                            <td>
                                <asp:Button ID="btnLimpiar" 
                                    runat="server" Text="Limpiar" ToolTip="Limpiar Beneficiario" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDni" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRuc" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center">
                                <asp:GridView ID="GV_TipoAgente" runat="server" RowStyle-Height="25px" HeaderStyle-Height="25px"
                                    AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tipo Agente">
                                            <ItemTemplate>
                                                            <asp:HiddenField ID="hddIdAgente" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdAgente") %>' />

                                                            <asp:Label ID="lblAgente" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Descripcion") %>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Factor">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblAgenteTaza" Font-Bold="true" ForeColor="Red" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Tasa","{0:F4}") %>'></asp:Label>

                                                            <asp:HiddenField ID="hddSujetoAPercepcion" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"SujetoAPercepcion") %>' />

                                                            <asp:HiddenField ID="hddSujetoARetencion" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"SujetoARetencion") %>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="GrillaRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaCabecera" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto" colspan="4">
                                <asp:GridView ID="GV_CuentaBancaria" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:CheckBox ID="chb_Select_CB" runat="server" onClick=" return(  valOnClick_chb_Select_CB(this)  ); " />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Banco" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:HiddenField ID="hddIdCuentaBancaria" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCuentaBancaria") %>' />
                                                            <asp:Label ID="lblBanco" runat="server" Font-Bold="true" Text='<%# DataBinder.Eval(Container.DataItem,"Banco") %>'></asp:Label>
                                                            <asp:HiddenField ID="hddIdBanco" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdBanco") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Nro. Cuenta" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblNroCuentaBancaria" runat="server" Font-Bold="true" Text='<%# DataBinder.Eval(Container.DataItem,"NroCuentaBancaria") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="GrillaRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaCabecera" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel ID="Panel_Aprobacion" runat="server">
                    <table>
                        <tr>
                            <td class="SubTituloCelda">
                                DATOS DE APROBACI�N
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:RadioButtonList ID="rdbAutorizado" runat="server" RepeatDirection="Horizontal"
                                                TextAlign="Right" CssClass="Texto" Font-Bold="true" AutoPostBack="True">
                                                <asp:ListItem Value="1">S�</asp:ListItem>
                                                <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td style="width: 30px">
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            Fecha Aprobaci�n:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaAprobacion" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                                Width="180px" runat="server"></asp:TextBox>
                                        </td>
                                        <td style="width: 30px">
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chbEnviarCxP" runat="server" Text="Enviar a Cuenta por Pagar" CssClass="Texto"
                                                Font-Bold="true" Enabled="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <asp:GridView ID="GV_CuentaProveedor" runat="server" AutoGenerateColumns="False"
                                                Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblMoneda_Monto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Mon_Simbolo")%>'
                                                                            Font-Bold="true"></asp:Label>

                                                                        <asp:Label ID="lblMonto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cprov_CargoMax","{0:F3}")%>'
                                                                            Font-Bold="true"></asp:Label>

                                                                        <asp:HiddenField ID="hddIdMoneda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                                        <asp:HiddenField ID="hddIdCuentaProveedor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                                        <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProveedor")%>' />

                                                                        <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPropietario")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Disponible" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblMoneda_Disponible" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Mon_Simbolo")%>'
                                                                            Font-Bold="true"></asp:Label>
                                                                        <asp:Label ID="lblDisponible" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cprov_Saldo","{0:F3}")%>'
                                                                            Font-Bold="true"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
              BENEFICIARIO DEL DOC. DE REFERENCIA
            </td>
        </tr>
         <tr>
            <td>
                <asp:Panel ID="PanelBeneficiario" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Descripci�n:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtApellidoBen" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                    Width="400px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarBene" OnClientClick="return( mostrarCapaPersona2()  );"
                                    runat="server" Text="Buscar Beneficiario" ToolTip="Buscar beneficiario" />
                            </td>
                            <td>
                                <asp:Button ID="btnLimpiarBene" 
                                    runat="server" Text="Limpiar Beneficiario" ToolTip="Limpiar Beneficiario" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDniBene" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRucBene" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        
        
        <tr>
            <td class="TituloCelda">
                DOCUMENTOS DE REFERENCIA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_DocumentoRef" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnBuscar_DocumentoRef" runat="server" Text="Documento Ref." Width="120px"
                                                OnClientClick="return( valOnClick_btnBuscar_DocumentoRef() );" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnLimpiarDetalle_DocumentoRef" runat="server" Text="Limpiar Detalle"
                                                Width="120px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" SelectText="Quitar" ShowSelectButton="True" />
                                        <asp:BoundField DataField="NomTipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                            <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Emisi�n" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:ImageButton ID="btnMostrarDetalleDocumentoRef" runat="server" ImageUrl="~/Imagenes/search_add.ico"
                                                                OnClick="btnMostrarDetalleDocumentoRef" ToolTip="Visualizar Detalle" Width="20px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                           <asp:BoundField DataField="FechaAEntregar" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Fecha a Entregar" DataFormatString="{0:dd/MM/yyy}" ItemStyle-Font-Bold="true"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="FechaVenc" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Fecha Vcto." DataFormatString="{0:dd/MM/yyy}" ItemStyle-Font-Bold="true"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonedaCab" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                            <asp:Label ID="lblMontoCab" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              <%--  <asp:GridView ID="GV_DocumentoRef_Cab" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField DataField="FechaAEntregar" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Fecha a Entregar" DataFormatString="{0:dd/MM/yyy}" ItemStyle-Font-Bold="true"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="FechaVenc" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Fecha Vcto." DataFormatString="{0:dd/MM/yyy}" ItemStyle-Font-Bold="true"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblMonedaCab" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblMontoCab" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocumentoRef_Detalle" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField DataField="CodigoProducto" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="C�d." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="NomProducto" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Producto" ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Cantidad" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Cantidad" ItemStyle-Font-Bold="true"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="CantidadTransito" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Pendiente" ItemStyle-Font-Bold="true"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="UMedida" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="U.M." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="PrecioCD" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" HeaderText="P. Venta" ItemStyle-Font-Bold="true"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Importe" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                            <asp:Label ID="lblImporte" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Importe","{0:F3}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocumentoRef_DetalleConcepto" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField DataField="Descripcion" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Concepto" ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Total" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                            <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocumentoRef_CondicionC" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField DataField="Descripcion" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Condici�n Comercial" ItemStyle-Font-Bold="true" ItemStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="TituloCelda">
            <td>
                CONCEPTOS
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GV_DetalleConcepto" runat="server" AutoGenerateColumns="False"
                    Width="100%">
                    <HeaderStyle CssClass="GrillaHeader" />
                    <Columns>
                        <asp:BoundField DataField="Descripcion" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Concepto" ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Total" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                            <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                            <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                TOTALES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Totales" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            Monto Afecto I.G.V.:
                                        </td>
                                        <td style="width: 20px">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMoneda_MontoAfectoIgv" runat="server" Font-Bold="true" CssClass="LabelRojo"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMontoAfectoIgv_Monto" runat="server" Width="70px" onKeypress=" return( validarNumeroPuntoPositivo('event') ); "
                                                onKeyUp=" return( valOnKeyUp_txtMontoAfectoIgv_Monto(this) ); " onFocus=" return( aceptarFoco(this) ); "
                                                onblur="  return( valBlur(event) ); " Font-Bold="true" Text="0"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            I.S.C.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtIsc" runat="server" Width="70px" onKeypress=" return( validarNumeroPuntoPositivo('event') ); "
                                                onKeyUp=" return( valOnKeyUp_txtMontoAfectoIgv_Isc(this) ); " onFocus=" return( aceptarFoco(this) ); "
                                                onblur="  return( valBlur(event) ); " Font-Bold="true" Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            Monto No Afecto I.G.V.:
                                        </td>
                                        <td style="width: 20px">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMoneda_MontoNoAfectoIgv_Monto" runat="server" Font-Bold="true"
                                                CssClass="LabelRojo" Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMontoNoAfectoIgv_Monto" runat="server" Width="70px" onKeypress=" return( validarNumeroPuntoPositivo('event') ); "
                                                onKeyUp=" return( valOnKeyUp_txtMontoNoAfectoIgv_Monto(this) ); " onFocus=" return( aceptarFoco(this) ); "
                                                onblur="  return( valBlur(event) ); " Font-Bold="true" Text="0"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            Retenci&oacute;n de Honorarios:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtRetencionHonorarios" runat="server" Width="70px" onKeypress=" return( validarNumeroPuntoPositivo('event') ); "
                                                onKeyUp=" return( valOnKeyUp_txtRetencionHonorarios(this) ); " onFocus=" return( aceptarFoco(this) ); "
                                                onblur="  return( valBlur(event) ); " Font-Bold="true" Text="0"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            ( % ):
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtRetencionHonorariosPercent" runat="server" Width="40px" onKeypress=" return( validarNumeroPuntoPositivo('event') ); "
                                                onKeyUp="  return( valOnKeyUp_txtRetencionHonorariosPercent(this) ); " onFocus=" return( aceptarFoco(this) ); "
                                                onblur="  return( valBlur(event) ); " Font-Bold="true" Text="0"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            Otros Tributos:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtOtrosTributos" runat="server" Width="70px" onKeypress=" return( validarNumeroPuntoPositivo('event') ); "
                                                onKeyUp=" return( valOnKeyUp_txtMontoNoAfectoIgv_Otributos(this) ); " onFocus=" return( aceptarFoco(this) ); "
                                                onblur="  return( valBlur(event) ); " Font-Bold="true" Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chb_SujetoPercepcion" AutoPostBack="true" runat="server" Font-Bold="true"
                                                CssClass="Texto" Text="Sujeto a Percepci�n" />
                                        </td>
                                        <td style="width: 20px">
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMoneda_SujetoPercepcion" runat="server" Font-Bold="true" CssClass="LabelRojo"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPercepcion_Monto" Enabled="false" runat="server" Width="70px"
                                                onKeypress=" return( validarNumeroPuntoPositivo('event') ); " onKeyUp=" return( valOnKeyUp_txtPercepcion_Monto(this) ); "
                                                onFocus=" return( aceptarFoco(this) ); " onblur="  return( valBlur(event) ); " Font-Bold="true"
                                                Text="0"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chb_SujetoRetenci�n" AutoPostBack="true" runat="server" Font-Bold="true"
                                                CssClass="Texto" Text="Sujeto a Retenci�n" />
                                        </td>
                                        <td style="width: 20px">
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMoneda_SujetoRetencion" runat="server" Font-Bold="true" CssClass="LabelRojo"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtRetencion_Monto" Enabled="false" runat="server" Width="70px"
                                                onKeypress=" return( validarNumeroPuntoPositivo('event') ); " onFocus=" return( aceptarFoco(this) ); "
                                                onblur="  return( valBlur(event) ); " Font-Bold="true" Text="0"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chb_SujetoDetraccion" runat="server" AutoPostBack="true" CssClass="Texto"
                                                Font-Bold="true" Text="Sujeto a Detracci�n" />
                                        </td>
                                        <td style="width: 20px">
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMoneda_SujetoDetraccion" runat="server" CssClass="LabelRojo" Font-Bold="true"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDetraccion_Monto" runat="server" Enabled="false" Font-Bold="true"
                                                onblur="  return( valBlur(event) ); " onFocus=" return( aceptarFoco(this) ); " onKeypress=" return( validarNumeroPuntoPositivo('event') ); "
                                                Text="0" Width="70px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="ckDetraccion" runat="server" CssClass="Texto" Text="Sin Auto-Detracci�n"
                                                onClick="  calcularConceptoDetraccion(); " />
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_ConceptoDetraccion" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:CheckBox ID="chb_ConceptoDetraccion" runat="server" onClick=" onClick_ConceptoDetraccion(this); "
                                                                OnCheckedChanged="chb_ConceptoDetraccion_CheckedChanged" AutoPostBack="true" />

                                                            <asp:HiddenField ID="hddIdMoneda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                            <asp:HiddenField ID="hddIdConcepto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                            <asp:HiddenField ID="hddPorcentDetraccion" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"PorcentDetraccion","{0:F3}")%>' />

                                                            <asp:HiddenField ID="hddMontoMinimo" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"MontoMinimoDetraccion","{0:F3}")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Nombre" HeaderText="Concepto" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="PorcentDetraccion" DataFormatString="{0:F3}" HeaderText="Porcentaje (%)"
                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-Font-Bold="true" />
                                        <asp:TemplateField HeaderText="Monto M�nimo Detracci�n" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>

                                                            <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MontoMinimoDetraccion","{0:F3}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Monto" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonedaMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>

                                                            <asp:TextBox ID="txtMonto" onblur=" return(  valBlur(event) ); " onFocus=" return(  aceptarFoco(this)  ); "
                                                                onKeyUp=" return( calcularConceptoDetraccion() ); " Enabled="false" onKeypress=" return( validarNumeroPuntoPositivo('event') ); "
                                                                runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'
                                                                Width="80px"> </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold">
                                            Afecto I.G.V.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMontoAfectoIgv" runat="server" CssClass="TextBox_ReadOnly" onFocus=" return( lostFocus_Totales() ); "
                                                onKeypress=" return( false ); " Text="0" Width="70px"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            NO Afecto I.G.V.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMontoNoAfectoIgv" runat="server" CssClass="TextBox_ReadOnly"
                                                onFocus=" return( lostFocus_Totales() ); " onKeypress=" return( false ); " Text="0"
                                                Width="70px"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            Valor de Venta:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubTotal" runat="server" CssClass="TextBox_ReadOnly" onFocus=" return( lostFocus_Totales() ); "
                                                onKeypress=" return( false ); " Text="0" Width="70px"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            I.G.V.(
                                            <asp:Label ID="lblPorcentIgv" runat="server" Text="0"></asp:Label>
                                            %):
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtIgv" runat="server" CssClass="TextBox_ReadOnly" onFocus=" return( lostFocus_Totales() ); "
                                                onKeypress=" return( false ); " Text="0" Width="70px"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            Total:
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTotal" runat="server" CssClass="TextBox_ReadOnly" onFocus=" return( lostFocus_Totales() ); "
                                                onKeypress=" return( false ); " Text="0" Width="70px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold">
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            Detracci�n:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDetraccion" runat="server" CssClass="TextBox_ReadOnly" onFocus=" return( lostFocus_Totales() ); "
                                                onKeypress=" return( false ); " Text="0" Width="70px"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            Percepci�n:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPercepcion" runat="server" CssClass="TextBox_ReadOnly" onFocus=" return( lostFocus_Totales() ); "
                                                onKeypress=" return( false ); " Text="0" Width="70px"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            Total a Pagar
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaTotalAPagar" runat="server" CssClass="LabelRojo" Font-Bold="true"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTotalAPagar" runat="server" CssClass="TextBox_ReadOnly" onFocus=" return( lostFocus_Totales() ); "
                                                onKeypress=" return( false ); " Text="0" Width="70px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                CENTRO DE COSTOS
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_CentroCosto" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_CentroCosto_Combos" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto" style="text-align: right; font-weight: bold">
                                                Centro de Costo:
                                            </td>
                                            <td style="width: 400px">
                                                <asp:DropDownList ID="cboUnidadNegocio" runat="server" Width="100%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAdd_CentroCosto" runat="server" Text="Agregar" Width="70px" ToolTip="Agregar Centro de Costo." />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAgregarNuevoCentroC" runat="server" Text="Agregar Nuevo C.Costo"  ToolTip="Agregar Centro de Costo." />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto" style="text-align: right; font-weight: bold">
                                                Sub Area Funcional:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboDeptoFuncional" runat="server" Width="100%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto" style="text-align: right; font-weight: bold">
                                                Concepto Gasto:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboSubArea1" runat="server" Width="100%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto" style="text-align: right; font-weight: bold">
                                                Detalle Gasto:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboSubArea2" runat="server" Width="100%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto" style="text-align: right; font-weight: bold">
                                                Sub Detalle Gasto:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboSubArea3" runat="server" Width="100%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_CentroCosto_Grilla" runat="server">
                                    <asp:GridView ID="GV_CentroCosto" runat="server" AutoGenerateColumns="False" Width="100%">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Und. Negocio" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-Height="25px">
                                                <ItemTemplate>
                                                                <asp:Label ID="lblUnidadNegocio" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadNegocio")%>'></asp:Label>
                                                                <asp:HiddenField ID="hddIdCentroCosto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCentroCosto")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="DeptoFuncional" HeaderText="Depto. Funcional" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:BoundField DataField="SubArea1" HeaderText="Sub �rea 1" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:BoundField DataField="SubArea2" HeaderText="Sub �rea 2" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:BoundField DataField="SubArea3" HeaderText="Sub �rea 3" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Monto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-Height="25px">
                                                <ItemTemplate>
                                                                <asp:Label ID="lblMoneda_Monto" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                                <asp:TextBox ID="txtMonto" TabIndex="500" Width="70px" Font-Bold="true" onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); "
                                                                    onFocus=" return( aceptarFoco(this) ); " onblur=" return( valBlur(event) ); " runat="server"
                                                                    onKeyUp="   return( calcularCentroCosto() );" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaCabecera" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                          <table width="100%">
                                            <tr>
                                                <td class="SubTituloCelda" runat="server" id="NewCC"  >
                                                    NUEVO CENTRO DE COSTOS
                                                </td>
                                            </tr>
                                          </table>
                                    <asp:GridView ID="GV_NuevoCentroCosto" runat="server" AutoGenerateColumns="False" Width="100%">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Und. Negocio" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-Height="25px">
                                                <ItemTemplate>
                                                                <asp:Label ID="lblUnidadNegocio2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadNegocio")%>'></asp:Label>
                                                                <asp:HiddenField ID="hddIdCentroCosto2" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCentroCosto")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="DeptoFuncional" HeaderText="Depto. Funcional" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:BoundField DataField="SubArea1" HeaderText="Sub �rea 1" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:BoundField DataField="SubArea2" HeaderText="Sub �rea 2" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:BoundField DataField="SubArea3" HeaderText="Sub �rea 3" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Monto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-Height="25px">
                                                <ItemTemplate>
                                                                <asp:Label ID="lblMoneda_Monto2" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                                <asp:TextBox ID="txtMonto" TabIndex="500" Width="70px" Font-Bold="true" onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); "
                                                                    onFocus=" return( aceptarFoco(this) ); " onblur=" return( valBlur(event) ); " runat="server"
                                                                    onKeyUp="   return( calcularCentroCosto2() );" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaCabecera" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                    <table width="100%">
                                    <tr>
                                    <td align="center">
                                    <asp:Button id="btnGuardarCentroC" runat="server" Text="Guardar Nuevo Centro de Costo"/>
                                    </td>
                                    </tr></table>
                                    
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right">
                <table>
                    <tr>
                        <td class="Texto">
                            Total Centro Costo:
                        </td>
                        <td>
                            <asp:TextBox ID="txtTotalCentroCostro" runat="server" onKeyPress="return ( false );"
                                onFocus="return ( Readonly(this) );" CssClass="TextBox_ReadOnly"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                OBSERVACIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" Width="100%" Font-Bold="true"
                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                    runat="server" Height="100px" Style="margin-bottom: 0px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
              <asp:HiddenField ID="hddIdPersona2" runat="server" />
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="hddIndex" runat="server" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de b�squeda realiza un filtrado de acuerdo al Beneficiario seleccionado.
                    rdo al Beneficiario seleccionado.
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="pnlRazpnApe" runat="server" DefaultButton="btnBuscarPersonaGrilla" >
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this,event));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarPersonaGrilla" >
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                    </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>                                                                    
                                                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarPersonaGrilla" >
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                    </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnBuscarPersonaGrilla" runat="server" Text="Buscar" CssClass="btnBuscar"
                                                                             OnClientClick="this.disabled=true;this.value='Procesando'" UseSubmitBehavior="false" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddl_Rol" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox>
                                                                <asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    
      <div id="capaPersona2" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton3" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona2'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="PnlBuscarBeneficiario" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona2" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:TextBox ID="tbApellido2" onKeyPress="return(validarCajaBusqueda2(this,event));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbDni2" onKeyPress="return(validarCajaBusquedaNumero2(event));" onFocus="javascript:validarbusqueda2();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbRuc2" runat="server" onKeyPress="return(validarCajaBusquedaNumero2(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:ImageButton ID="btBuscarPersonaGrilla2" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />--%>
                                                                            <asp:Button ID="btBuscarPersonaGrillas2" runat="server" Text="Buscar" CssClass="btnBuscar"
                                                                        OnClientClick="this.disabled;this.value='Procesando...'" UseSubmitBehavior="false"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="DropDownList1" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar2" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior2" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion2('0'));" />
                                                            <asp:Button ID="btSiguiente2" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion2('1'));" />
                                                            <asp:TextBox ID="tbPageIndex2" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox>
                                                                <asp:Button ID="btIr2" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion2('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO2" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 1200px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="cbotipodocumento1" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisi�n</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                    <asp:HiddenField ID="hddIdEmpresa_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />

                                                    <asp:HiddenField ID="hddIdAlmacen_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                    <asp:HiddenField ID="hddIdPersona_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Descripci�n" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Importe" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMoneda" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblImporte" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Documento Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                            DataTextField="DescripcionCorto" DataValueField="Id" runat="server"    >
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <%--   <asp:DropDownList ID="ddl_IdDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getIdDocumentoRef")%>'
                                            DataTextField="IdDocRelacionado" DataValueField="Id" runat="server" >
                                        </asp:DropDownList> --%>
                                 <asp:TemplateField  Visible="false" HeaderText="Id Documento. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                               <asp:Label ID="lblIdDocumentoRef" ForeColor="Transparent" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"strIdDocumentoRef")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaCxP_Saldos" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaCxP_Saldos')   );" />
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    CUENTAS POR PAGAR
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_Documento_Cab_CXP" runat="server" AutoGenerateColumns="False"
                        Width="100%">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda_Monto" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                <asp:Label ID="lblMonto" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Saldo" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda_Saldo" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                <asp:TextBox ID="txtSaldo" onblur=" return( valBlur(event) ); " runat="server" Width="80px"
                                                    Font-Bold="true" onKeypress=" return( validarNumeroPuntoPositivo('event') ); " onFocus=" return( aceptarFoco(this)  ); "
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F3}")%>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Sustento" ItemStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                               <asp:CheckBox ID="chkSustentando" runat="server"  Checked="true" Enabled="false"  />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="SubTituloCelda">
                    DOCUMENTOS DE REFERENCIA
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_DocumentoRef_CXP" runat="server" AutoGenerateColumns="False"
                        Width="100%">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
                            <asp:BoundField DataField="NomTipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdMovCuentaCXP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMovCtaPP")%>' />

                                             <asp:HiddenField ID="hddIdDocumentoRef" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda_Monto" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                <asp:Label ID="lblMonto" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Saldo" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda_Saldo" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                <asp:Label ID="lblSaldo" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Saldo" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda_SaldoNew" ForeColor="Red" Font-Bold="true" runat="server"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                <asp:TextBox ID="txtSaldo_New" onblur=" return( valBlur(event) ); " runat="server" Width="80px"
                                                    Font-Bold="true" onKeypress=" return( validarNumeroPuntoPositivo('event') ); " onFocus=" return( aceptarFoco(this)  ); "
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"SaldoNew","{0:F3}")%>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Sustento" ItemStyle-HorizontalAlign="Center" Visible="false">
                                <ItemTemplate>
                                               <asp:CheckBox ID="chksustento" runat="server" />
                                            <asp:HiddenField  ID="hddSustento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Sustento")%>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            
                <tr>
                <td class="SubTituloCelda">
                    DOCUMENTOS DE MOV. BANCARIOS RELACIONADOS
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_MovBanco" runat="server" AutoGenerateColumns="False"
                        Width="100%">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
                            <asp:BoundField DataField="banco" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Banco" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Nro. Cuenta" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblnrocuenta" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"nrocuenta")%>'></asp:Label>
                                                <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda_Monto" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"moneda")%>'></asp:Label>
                                                <asp:Label ID="lblMonto" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"dcan_monto","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                          <asp:BoundField DataField="dcan_NumeroCheque" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Cheque" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
             <tr>
                <td class="SubTituloCelda">
                    DOCUMENTOS RELACIONADOS - DOCUMENTOS PROVISIONADOS
                </td>
            </tr>
                <tr>
                <td>
                    <asp:GridView ID="GV_ReciboEgreso" runat="server" AutoGenerateColumns="False"
                        Width="100%">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
                         
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Documento" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblTipoDocEgreso" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>'></asp:Label>:
                                                <asp:Label ID="lblNroDocEgreso" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                <asp:HiddenField ID="hddIdDocumentoEgreso" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:BoundField DataField="FechaEmision" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Fecha Emisi�n" ItemStyle-Font-Bold="true"  ItemStyle-Height="25px" DataFormatString="{0:dd/MM/yyyy}"
                                ItemStyle-HorizontalAlign="Center" />
                            
                             <asp:BoundField DataField="DescripcionPersona" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Beneficiario" ItemStyle-Font-Bold="true"  ItemStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" />  
                                  
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMonedaEgreso" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                <asp:Label ID="lblMontoEgreso" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Total","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         
                         
                             
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
                <tr>
                <td>
                    <asp:GridView ID="GV_CancelacionBancos" runat="server" AutoGenerateColumns="False"
                        Width="100%">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
                         
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Documento" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblTipoDocCanceBanco" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>'></asp:Label>:
                                                <asp:Label ID="lblNroDocCanceBanco" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                <asp:HiddenField ID="hddIdDocumentoCanceBanco" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:BoundField DataField="FechaEmision" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Fecha Emisi�n" ItemStyle-Font-Bold="true"  ItemStyle-Height="25px" DataFormatString="{0:dd/MM/yyyy}"
                                ItemStyle-HorizontalAlign="Center" />
                            
                             <asp:BoundField DataField="DescripcionPersona" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Beneficiario" ItemStyle-Font-Bold="true"  ItemStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" />  
                                  
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMonedaCanceBanco" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                <asp:Label ID="lblMontoCanceBanco" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Total","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         
                         
                             
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
                <td style="text-align: center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnGuardar_CxP" runat="server" Text="Guardar" ToolTip="Guardar" Width="80px"
                                    OnClientClick=" return(  valSaveDocumento() ); " />
                            </td>
                            <td>
                                <asp:Button ID="btnCerrar_CXP" runat="server" Text="Cerrar" Width="80px" ToolTip="Cerrar"
                                    OnClientClick="return(  offCapa('capaCxP_Saldos')   );" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDocumentoCab_BuscarDocumento" style="border: 3px solid blue; padding: 8px;
        width: 850px; height: auto; position: absolute; background-color: white; z-index: 2;
        display: none; top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentoCab_BuscarDocumento')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_DocumentoCab_BuscarDocumento" runat="server" AutoGenerateColumns="False"
                        Width="100%" PageSize="20">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderText=""
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Proveedor" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Button ID="btnAnterior_Documento" runat="server" Width="50px" Text="<" ToolTip="P�gina Anterior"
                        OnClientClick="return(valNavegacionDocumento('0'));" Style="cursor: hand;" />
                    <asp:Button ID="btnPosterior_Documento" runat="server" Width="50px" Text=">" ToolTip="P�gina Posterior"
                        OnClientClick="return(valNavegacionDocumento('1'));" Style="cursor: hand;" />
                    <asp:TextBox ID="txtPageIndex_Documento" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                        runat="server"></asp:TextBox><asp:Button ID="btnIr_Documento" runat="server" Width="50px"
                            Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionDocumento('2'));"
                            Style="cursor: hand;" />
                    <asp:TextBox ID="txtPageIndexGO_Documento" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
     <div id="capaReporteDocExternos" style="border: 3px solid blue; padding: 8px; width: 900px;
        height:auto ; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;"     >
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton4" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaReporteDocExternos')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTipoDocumento" runat="server" Enabled="false">
                                <asp:ListItem Selected="True" Value="0" Text="DOC.EXTERNO"  ></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                              <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="ddlTiendaReporte" runat="server" >
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>

                
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdTipoBusquedaExt" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisi�n</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td></td>
               
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="PnlFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtInicioDocExterno" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtInicioDocExterno">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtInicioDocExterno">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFinDocExterno" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender6" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFinDocExterno">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender6" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFinDocExterno">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnGenerarReportefecha" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                             <td><asp:Button id="btnExportar" runat="server" Text="Exportar Excel"/></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="PnlDocumento" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtserieDocexterno" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtcodigoDocexterno" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnGenerarReportecodigo"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                             <td><asp:Button id="btnExportr2" runat="server" Text="Exportar Excel"/></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel3" runat="server"  Width="100%" >
                        <asp:GridView ID="GV_ReporteDocExterno" runat="server" AutoGenerateColumns="false"
                            Width="100%">
                            <Columns>
                              
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo Documento" ItemStyle-ForeColor="Red" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumentoDocExt" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Venc"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    
                                       <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblmonedaDocexterno" ForeColor="Red"  runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                    <asp:Label ID="lblmontoDocexterno" ForeColor="Red" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Total")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Beneficiario" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                     <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        //***************************************************** BUSQUEDA PERSONA
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function MostrarCapaReporteDocExterno() {
            onCapa('capaReporteDocExternos');
            var txtSerie = document.getElementById('<%=txtserieDocexterno.ClientID %>');
            if (txtSerie != null) {
                txtSerie.select();
                txtSerie.focus();
            }
            return false;
        }
        function valNavegacion2(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex2.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbApellido2.ClientID%>').select();
                document.getElementById('<%=tbApellido2.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO2.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO2.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO2.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO2.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO2.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusqueda2(obj, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrillas2.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function validarbusqueda2() {
            var tb = document.getElementById('<%=tbApellido2.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona2.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero2(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrillas2.ClientID %>');
                boton.focus();
                return true;
            }
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }

        function validarCajaBusquedaNumero(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btnBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);            
            if (caracter == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btnBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function mostrarCapaPersona2() {
            onCapa('capaPersona2');
            document.getElementById('<%=tbApellido2.ClientID%>').select();
            document.getElementById('<%=tbApellido2.ClientID%>').focus();
            return false;
        }

        function valOnClick_btnBuscar_DocumentoRef() {
         var hddIdPersona2 = document.getElementById('<%=hddIdPersona2.ClientID%>');
         if (isNaN(parseInt(hddIdPersona2.value)) || hddIdPersona2.value.length <= 0 || parseInt(hddIdPersona2.value) <= 0) {
                alert('Para referenciar un Documento, tiene que elegir un beneficiario.');
                return false;
              }
              if ((parseInt(hddIdPersona2.value) != NaN) || hddIdPersona2.value.length > 0 || parseInt(hddIdPersona2.value) != 0) {
                 onCapa('capaDocumentosReferencia');
                 return false;
             }
            
        }

        function valOnClick_btnAceptarBuscarDocRefxCodigo() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('Ingrese un Nro. de Serie para la b�squeda.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('Ingrese un Nro. de Documento para la b�squeda.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }
        function lostFocus_Totales() {
            document.getElementById('<%=btnBuscar_DocumentoRef.ClientID%>').focus();
            return false;
        }
        function calcularTotales() {
            var TotalRetencionHonorarios = parseFloat(document.getElementById('<%=txtRetencionHonorarios.ClientID %>').value);
            if (isNaN(TotalRetencionHonorarios)) { TotalRetencionHonorarios = 0; }

            var montoIsc = parseFloat(document.getElementById('<%=txtIsc.ClientID%>').value);
            if (isNaN(montoIsc)) { montoIsc = 0; }

            var montoOtrosTributos = parseFloat(document.getElementById('<%=txtOtrosTributos.ClientID%>').value);
            if (isNaN(montoOtrosTributos)) { montoOtrosTributos = 0; }

            var montoAfectoIgv = parseFloat(document.getElementById('<%=txtMontoAfectoIgv.ClientID%>').value);
            if (isNaN(montoAfectoIgv)) { montoAfectoIgv = 0; }

            var montoNoAfectoIgv = parseFloat(document.getElementById('<%=txtMontoNoAfectoIgv.ClientID%>').value);
            if (isNaN(montoNoAfectoIgv)) { montoNoAfectoIgv = 0; }

            var porcentIgv = parseFloat(document.getElementById('<%=lblPorcentIgv.ClientID%>').innerHTML);
            if (isNaN(porcentIgv)) { porcentIgv = 0; }

            var percepcion = parseFloat(document.getElementById('<%=txtPercepcion.ClientID%>').value);
            if (isNaN(percepcion)) { percepcion = 0; }

            var Detraccion = 0;
            if (document.getElementById('<%=ckDetraccion.ClientID %>').status == true) {
                Detraccion = parseFloat(document.getElementById('<%=txtDetraccion.ClientID%>').value);
                if (isNaN(Detraccion)) { Detraccion = 0; }
            }


            var subTotal = 0;
            var total = 0;
            var totalPagar = 0;
            var igv = 0;

            //            total = montoNoAfectoIgv + montoAfectoIgv + igv;
            total = montoAfectoIgv + montoNoAfectoIgv;


            igv = montoAfectoIgv - (montoAfectoIgv / ((porcentIgv / 100) + 1));
            if (isNaN(igv)) { igv = 0; }

            subTotal = montoNoAfectoIgv + montoAfectoIgv - igv;
            if (isNaN(subTotal)) { subTotal = 0; }


            if (isNaN(total)) { total = 0; }
            totalPagar = (total + percepcion - TotalRetencionHonorarios) - Detraccion;

            if (isNaN(totalPagar)) { totalPagar = 0; }

            //******************* MOSTRAMOS VALORES
            document.getElementById('<%=txtIgv.ClientID%>').value = redondear(igv, 2);
            document.getElementById('<%=txtTotal.ClientID%>').value = redondear(total, 2);
            document.getElementById('<%=txtSubTotal.ClientID%>').value = redondear(subTotal, 2);
            document.getElementById('<%=txtTotalAPagar.ClientID%>').value = redondear(totalPagar, 2);

            return false;
        }
        function valOnKeyUp_txtPercepcion_Monto(caja) {

            var percepcion = parseFloat(caja.value);
            if (isNaN(percepcion)) { percepcion = 0; }

            document.getElementById('<%=txtPercepcion.ClientID%>').value = redondear(percepcion, 2);

            calcularTotales();

            return false;
        }


        function valOnKeyUp_txtMontoAfectoIgv_Monto(caja) {
            var total = 0;
            var montoAfectoIgv = parseFloat(caja.value);
            if (isNaN(montoAfectoIgv)) { montoAfectoIgv = 0; }

            var montoIsc = parseFloat(document.getElementById('<%=txtIsc.ClientID%>').value);
            if (isNaN(montoIsc)) { montoIsc = 0; }

            total = montoAfectoIgv + montoIsc;
            document.getElementById('<%=txtMontoAfectoIgv.ClientID%>').value = redondear(total, 2);

            calcularTotales();

            return false;
        }

        function valOnKeyUp_txtMontoAfectoIgv_Isc(caja) {
            var total = 0;
            var montoIsc = parseFloat(caja.value);
            if (isNaN(montoIsc)) { montoIsc = 0; }

            var montoAfectoIgv = parseFloat(document.getElementById('<%=txtMontoAfectoIgv_Monto.ClientID%>').value);
            if (isNaN(montoAfectoIgv)) { montoAfectoIgv = 0; }

            total = montoAfectoIgv + montoIsc;
            document.getElementById('<%=txtMontoAfectoIgv.ClientID%>').value = redondear(total, 2);

            calcularTotales();

            return false;
        }

        function valOnKeyUp_txtMontoNoAfectoIgv_Monto(caja) {
            document.getElementById('<%=txtRetencionHonorariosPercent.ClientID %>').value = 0;
            document.getElementById('<%=txtRetencionHonorarios.ClientID %>').value = 0;

            var total = 0;
            var montoNoAfectoIgv = parseFloat(caja.value);
            if (isNaN(montoNoAfectoIgv)) { montoNoAfectoIgv = 0; }

            var montoOtributos = parseFloat(document.getElementById('<%=txtOtrosTributos.ClientID%>').value);
            if (isNaN(montoOtributos)) { montoOtributos = 0; }

            total = montoNoAfectoIgv + montoOtributos;
            document.getElementById('<%=txtMontoNoAfectoIgv.ClientID%>').value = redondear(total, 2);

            calcularTotales();

            return false;
        }

        function valOnKeyUp_txtMontoNoAfectoIgv_Otributos(caja) {
            var total = 0;
            var montoOtributos = parseFloat(caja.value);
            if (isNaN(montoOtributos)) { montoOtributos = 0; }

            var montoNoAfectoIgv = parseFloat(document.getElementById('<%=txtMontoNoAfectoIgv_Monto.ClientID%>').value);
            if (isNaN(montoNoAfectoIgv)) { montoNoAfectoIgv = 0; }

            total = montoOtributos + montoNoAfectoIgv;
            document.getElementById('<%=txtMontoNoAfectoIgv.ClientID%>').value = redondear(total, 2);

            calcularTotales();
            return false;
        }

        function calcularConceptoDetraccion() {

            var grilla = document.getElementById('<%=GV_ConceptoDetraccion.ClientID%>');

            var detraccionTotal = 0;
            var monto = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];

                    var chbSelect = rowElem.cells[0].children[0];
                    var hddPorcent = rowElem.cells[0].children[3];
                    var porcent = parseFloat(hddPorcent.value);
                    if (isNaN(porcent)) { porcent = 0; }
                    var hddMontoMinimo = rowElem.cells[0].children[4];
                    var montoMinimo = parseFloat(hddMontoMinimo.value);
                    if (isNaN(montoMinimo)) { montoMinimo = 0; }
                    var txtMonto = rowElem.cells[4].children[1];
                    monto = parseFloat(txtMonto.value);
                    if (isNaN(monto)) { monto = 0; }

                    if (chbSelect.status && monto > montoMinimo) {

                        detraccionTotal = detraccionTotal + monto * (porcent / 100);
                        break;

                    }

                }
            }
            var txtMontoAfectoIgv_Monto = document.getElementById('<%=txtMontoAfectoIgv_Monto.ClientID%>');
            txtMontoAfectoIgv_Monto.value = redondear(monto, 2);
            document.getElementById('<%=txtDetraccion_Monto.ClientID%>').value = redondear(detraccionTotal, 2);

            if (document.getElementById('<%=ckDetraccion.ClientID %>').status == true) {
                document.getElementById('<%=txtDetraccion.ClientID%>').value = redondear(detraccionTotal, 2);
            } else {
                document.getElementById('<%=txtDetraccion.ClientID%>').value = 0;
            }

            valOnKeyUp_txtMontoAfectoIgv_Monto(txtMontoAfectoIgv_Monto);

            return false;
        }



        function valSaveDocumento() {

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }

            var txtSerie = document.getElementById('<%=txtSerie.ClientID%>');
            if (txtSerie.value.length <= 0) {
                alert('DEBE INGRESAR UN NRO. SERIE. NO SE PERMITE LA OPERACI�N.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }

            var txtNroDocumento = document.getElementById('<%=txtNroDocumento.ClientID%>');
            if (txtNroDocumento.value.length <= 0) {
                alert('DEBE INGRESAR UN NRO. DOCUMENTO. NO SE PERMITE LA OPERACI�N.');
                txtNroDocumento.select();
                txtNroDocumento.focus();
                return false;
            }

            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }

            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            if (isNaN(parseInt(cboMoneda.value)) || cboMoneda.value.length <= 0) {
                alert('Debe seleccionar una Moneda de Emisi�n.');
                return false;
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0 || parseInt(hddIdPersona.value) <= 0) {
                alert('DEBE SELECCIONAR UN PROVEEDOR. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
            var mensaje = '';
            switch (parseInt(hddFrmModo.value)) {
                case 1: //******** NUEVO
                    mensaje = 'Desea continuar con la Operaci�n ?';
                    break;
                case 2: //******** EDITAR
                    mensaje = 'Desea continuar con la Operaci�n ?';
                    break;
            }
            var TotalCentroCosto = 0;
            var cont = 0;
            var monto = 0;
            var uniNeg = '';
            var DptoFun = '';
            var s1 = '';
            var s2 = '';
            var s3 = '';
            var uniNegx = '';
            var DptoFunx = '';
            var s1x = '';
            var s2x = '';
            var s3x = '';
            var totalaPagar = document.getElementById('<%=txtTotalAPagar.ClientID %>');
            var valtotalaPagar = parseFloat(totalaPagar.value);

            if (isNaN(valtotalaPagar)) { valtotalaPagar = 0; }

            if (valtotalaPagar < 0) {
                alert('EL TOTAL A PAGAR NO PUEDE SER NEGATIVO.\nNO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var GV_CentroCosto = document.getElementById('<%= GV_CentroCosto.ClientID %>');
            if (GV_CentroCosto == null) {
                alert('DEBE INGRESAR EL CENTRO DE COSTO.\nNO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var IdCentroCosto = '';
            if (GV_CentroCosto != null) {
                for (var i = 1; i < GV_CentroCosto.rows.length; i++) {
                    var rowElem = GV_CentroCosto.rows[i];
                    cont = 0;
                    IdCentroCosto = rowElem.cells[1].children[1].value;

                    for (var k = 1; k < GV_CentroCosto.rows.length; k++) {
                        var rowElemx = GV_CentroCosto.rows[k];

                        if (rowElemx.cells[1].children[1].value == IdCentroCosto) {
                            cont = cont + 1;
                            if (cont > 1) {
                                alert('LA CUADRICULA DE CENTRO DE COSTO POSEE ELEMENTOS REPETIDOS.\nNO SE PERMITE LA OPERACI�N.');
                                return false;
                            }
                        }
                    }

                    monto = parseFloat(rowElem.cells[6].children[1].value);
                    if (isNaN(monto)) { monto = 0; }
                    TotalCentroCosto = TotalCentroCosto + monto;

                }



                var subTotal = parseFloat(document.getElementById('<%=txtSubTotal.ClientID %>').value);
                if (isNaN(subTotal)) { subTotal = 0; }

                if (TotalCentroCosto != subTotal) {
                    alert('EL VALOR VENTA DEBE SER IGUAL AL CENTRO DE COSTO.\nNO SE PERMITE LA OPERACI�N.');
                    return false;
                }


            }

            return confirm(mensaje);
        }
        //***********************************
        function valOnClickBuscar() {
            var txtSerie = document.getElementById('<%=txtSerie.ClientID %>');
            //            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            //            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');

            //            if (txtNroDocumento.readOnly == true) {  //**** Se habilita la B�squeda
            //                panel_Cab.disabled = false;
            //                btnBuscarDocumentoxCodigo.disabled = false;
            //                txtNroDocumento.readOnly = false;
            //                txtNroDocumento.disabled = false;
            //                txtNroDocumento.style.backgroundColor = 'Yellow';
            txtSerie.select();
            txtSerie.focus();
            return false;
            //            }
            //            return true;
        }
        function valOnClickBuscarDocumentoxCodigo() {
            var IdPersona = parseInt(document.getElementById('<%=hddIdPersona.ClientID %>').value);
            if (isNaN(IdPersona)) { IdPersona = 0; }

            if (IdPersona == 0) {

                var txtSerie = document.getElementById('<%=txtSerie.ClientID %>');
                if (txtSerie.value.length <= 0) {
                    alert('Debe ingresar un valor v�lido.');
                    txtSerie.select();
                    txtSerie.focus();
                    return false;
                }

                var txtNroDocumento = document.getElementById('<%=txtNroDocumento.ClientID %>');
                if (txtNroDocumento.value.length <= 0) {
                    alert('Debe ingresar un valor v�lido.');
                    txtNroDocumento.select();
                    txtNroDocumento.focus();
                    return false;
                }
            }
            return true;
        }


        function valOnClickEditar() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Edici�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite su edici�n.');
                return false;
            }
            return true;
        }
        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para su Anulaci�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya est� ANULADO. No se permite su anulaci�n.');
                return false;
            }
            return confirm('Desea continuar con el Proceso de ANULACI�N del Documento ?');
        }
        function valOnKeyPress_txtNroDocumento(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);            
            if (caracter == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').Click();
            }
        }
        function valOnClick_chb_Select_CB(checkBox) {

            var grilla = document.getElementById('<%=GV_CuentaBancaria.ClientID%>');
            var txtObservaciones = document.getElementById('<%=txtObservaciones.ClientID%>');
            var cad = '';
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[0].id == checkBox.id) {

                        if (checkBox.status) {

                            var banco = rowElem.cells[1].children[1].innerHTML;
                            var nroCuenta = rowElem.cells[2].children[0].innerHTML;
                            cad = cad + 'Banco: ' + banco + '. Cuenta Bancaria:' + nroCuenta;
                            break;
                        }

                    }

                }
            }

            txtObservaciones.value = txtObservaciones.value + ' / ' + cad;

            return true;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function valNavegacionDocumento(tipoMov) {

            var index = parseInt(document.getElementById('<%=txtPageIndex_Documento.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Documento.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Documento.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Documento.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Documento.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Documento.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function calcularCentroCosto() {
            var TotalCentroCosto = 0;
            var GV_CentroCosto = document.getElementById('<%=GV_CentroCosto.ClientID %>');
            if (GV_CentroCosto != null) {
                for (var i = 1; i < GV_CentroCosto.rows.length; i++) {
                    var rowElem = GV_CentroCosto.rows[i];

                    CentroCosto = parseFloat(rowElem.cells[6].children[1].value);
                    if (isNaN(CentroCosto)) { CentroCosto = 0; }

                    TotalCentroCosto = TotalCentroCosto + CentroCosto;
                }
            }

            document.getElementById('<%=txtTotalCentroCostro.ClientID%>').value = redondear(TotalCentroCosto, 2);
            return false;
        }
        function calcularCentroCosto2() {
            var TotalCentroCosto = 0;
            var GV_NuevoCentroCosto = document.getElementById('<%=GV_NuevoCentroCosto.ClientID %>');
            if (GV_NuevoCentroCosto != null) {
                for (var i = 1; i < GV_NuevoCentroCosto.rows.length; i++) {
                    var rowElem = GV_NuevoCentroCosto.rows[i];

                    CentroCosto = parseFloat(rowElem.cells[6].children[1].value);
                    if (isNaN(CentroCosto)) { CentroCosto = 0; }

                    TotalCentroCosto = TotalCentroCosto + CentroCosto;
                }
            }

            document.getElementById('<%=txtTotalCentroCostro.ClientID%>').value = redondear(TotalCentroCosto, 2);
            return false;
        }
        ///////
        function valOnKeyUp_txtRetencionHonorariosPercent(caja) {
            var porcentaje = parseFloat(caja.value);
            var TotalRetencionHonorarios = 0;
            var MontoNoAfectoIgv_Monto = parseFloat(document.getElementById('<%=txtMontoNoAfectoIgv_Monto.ClientID %>').value);

            if (isNaN(porcentaje)) { porcentaje = 0; }

            if (porcentaje > 0 && MontoNoAfectoIgv_Monto > 0) {
                TotalRetencionHonorarios = MontoNoAfectoIgv_Monto * (porcentaje / 100);
            }

            document.getElementById('<%=txtRetencionHonorarios.ClientID %>').value = redondear(TotalRetencionHonorarios, 2);
            calcularTotales();
            return false;
        }
        //
        function valOnKeyUp_txtRetencionHonorarios(caja) {
            var TotalRetencionHonorariosPercent = 0;
            var MontoNoAfectoIgv_Monto = parseFloat(document.getElementById('<%=txtMontoNoAfectoIgv_Monto.ClientID %>').value);
            var TotalRetencionHonorarios = parseFloat(caja.value);
            if (isNaN(TotalRetencionHonorarios)) { TotalRetencionHonorarios = 0; }

            TotalRetencionHonorariosPercent = (TotalRetencionHonorarios / MontoNoAfectoIgv_Monto) * 100;

            document.getElementById('<%=txtRetencionHonorariosPercent.ClientID %>').value = redondear(TotalRetencionHonorariosPercent, 2);

        }
        function onClick_ConceptoDetraccion(ck) {
            var grilla = document.getElementById('<%=GV_ConceptoDetraccion.ClientID%>');

            var detraccionTotal = 0;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];
                    var chbSelect = rowElem.cells[0].children[0];

                    if (chbSelect.id != ck.id) {
                        chbSelect.checked = false;
                    }
                }
            }
            return true;
        }
        
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
