﻿Imports Entidades
Imports Negocio
Imports general.librerias.serializador

Partial Public Class PagoMasivoDetracciones
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Dim cuenta As String
    Dim documentoi As String
    Dim idpersonai As String
    Dim idcuentadetraccioni As String


    Dim DetIdServicio As String
    Dim DetIdOperacion As String
    Dim DetSerie As String
    Dim DetCodigo As String
    Dim DetMontoDetraccion As String
    Dim DetEstadoDet As String
    Dim DetIDDocumentoGenera As String

    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.TxtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.TxtFechaFin.Text = Me.TxtFechaInicio.Text

        Dim Usuario As Integer = CInt(Session("IdUsuario"))
        Dim nroVoucher As String = String.Empty

        Dim flag As String = Request.QueryString("flag")
        Select Case flag
            Case "llenarcombos"

                Dim lista As New List(Of be_TipoOperacion)
                Dim rptaTipope As String = ""
                Try
                    lista = (New blTipoOperacion).listarTipoOperacion(2102866605)
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rptaTipope = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(rptaTipope)
                    Response.End()
                Else
                    Response.Write(rptaTipope)
                    Response.End()
                End If

            Case "llenarcomboBanco"
                Dim IdEstado As String = Request.QueryString("IdEstadoc")
                Dim rpta As String = ""
                Dim lista As New List(Of be_Bancos)
                Try
                    lista = (New bl_Bancos).listarBancos(Convert.ToInt32(IdEstado))
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rpta)
                    Response.End()
                Else
                    Response.Write(rpta)
                    Response.End()
                End If

            Case "llenarcomboservicio"

                Dim lista As New List(Of be_Concepto)
                Dim rptaConcepto As String = ""
                Try
                    lista = (New bl_Concepto).listarConcepto(2102866601)
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rptaConcepto = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(rptaConcepto)
                    Response.End()
                Else
                    Response.Write(rptaConcepto)
                    Response.End()
                End If
            Case "GuardarDetraccion"

                Dim IdServicio As String = Request.QueryString("IdServicio")
                Dim IdOperacion As String = Request.QueryString("IdOperacion")
                Dim Serie As String = Request.QueryString("Serie")
                Dim Codigo As String = Request.QueryString("Codigo")
                Dim MontoDetraccion As String = Request.QueryString("MontoDetraccion")
                Dim EstadoDet As String = Request.QueryString("Estado")
                Dim IDDocumentoGenera As String = Request.QueryString("IDDocumentoGenera")

                DetIdServicio = IdServicio
                DetIdOperacion = IdOperacion
                DetSerie = Serie
                DetCodigo = Codigo
                DetMontoDetraccion = MontoDetraccion
                DetEstadoDet = EstadoDet
                DetIDDocumentoGenera = IDDocumentoGenera

                If (IDDocumentoGenera <> 0) Then
                    Dim objMontoRegimenDet As Entidades.be_MontoRegimenDet = obtenerMontoRegimen()
                    If (objMontoRegimenDet.IdDocumento <> Nothing) Then
                        registrar_MontoRegimenDetalle()
                    Else
                        objScript.mostrarMsjAlerta(Me, "La Cuenta Bancaria ingresada ya se encuentra Registrada. No procede la operación.")
                    End If
                Else

                End If

            Case "ActualizarConstancia"
               
                
                Dim txtconstancia As String = Request.QueryString("Constancia")
                Dim iddocumento As String = Request.QueryString("iddocumento")
                Dim txtlote As String = Request.QueryString("lote")
                Dim txtPeriodo As String = Request.QueryString("periodo")

                Dim lista As New List(Of be_detraccionMasiva)
             
                   
                    Dim rpta As String = ""
                    Try

                        Dim objeto As New bl_Anexo_Documento
                    objeto.ActualizarConstancia(iddocumento, txtconstancia, txtPeriodo)
                    lista = (New blPagoDetraccionesMasivas).listaDetraccionesporLote(txtlote, txtPeriodo)
                     Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    If lista.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try

                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()
                    End If

            Case "Guardar"
                    Dim txtnrocuenta As String = Request.QueryString("CtaDetraccion")
                    Dim iddocumento As String = Request.QueryString("iddocumento")
                    Dim idpersona As String = Request.QueryString("idpersona")
                    Dim idcuentadetraccion As String = Request.QueryString("idcuentadetraccion")

                    cuenta = txtnrocuenta
                    documentoi = iddocumento
                    idpersonai = idpersona
                    idcuentadetraccioni = idcuentadetraccion

                    If (txtnrocuenta <> "") Then
                        Dim objCuentaBancaria As Entidades.CuentaBancaria = obtenerCuentaBancaria()
                        Dim objCuenta As Entidades.CuentaBancaria = New Negocio.CuentaBancaria().VerificarsiExisteCBDetraccion(objCuentaBancaria.IdBanco, objCuentaBancaria.IdPersona, objCuentaBancaria.IdMoneda)

                        If (objCuenta.IdCuentaBancaria = 0) Then
                            registrar_CuentaBancaria()
                        End If

                        If (objCuenta.IdCuentaBancaria > 0) Then
                            registrar_CuentaBancaria()
                        End If

                    Else

                        objScript.mostrarMsjAlerta(Me, "La Cuenta Bancaria ingresada ya se encuentra Registrada. No procede la operación.")
                    End If

            Case "ConsultaDetracciones"

                    Dim txtFechaInicio As String = Request.QueryString("fechaInicio")
                    Dim txtFechaFinal As String = Request.QueryString("FechaFinal")
                    Dim txtPeriodo As String = Request.QueryString("periodo")

                    Dim lista As New List(Of be_detraccionMasiva)
                    Dim rpta As String = ""
                    Try
                        lista = (New blPagoDetraccionesMasivas).listarDetraccionesMasivas(txtFechaInicio, txtFechaFinal, 0)

                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    If lista.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try

                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()
                    End If
            Case "ConsultaDetraccionesporLote"

                    Dim txtlote As String = Request.QueryString("lote")
                    Dim txtPeriodo As String = Request.QueryString("periodo")

                    Dim lista As New List(Of be_detraccionMasiva)
                    Dim rpta As String = ""
                    Try
                        lista = (New blPagoDetraccionesMasivas).listaDetraccionesporLote(txtlote, txtPeriodo)

                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    If lista.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try

                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()
                    End If
            Case "GuardarEstadoDetraccion"

                    Dim iddocumento As String = Request.QueryString("iddocumento")
                    Dim estado As String = Request.QueryString("estado")

                    Dim lista As New List(Of be_detraccionMasiva)
                    Dim rpta As String = ""
                    Try

                        lista = (New blPagoDetraccionesMasivas).listarDetraccionesMasivasEstados(iddocumento, estado)

                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    If lista.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try

                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()
                    End If
            Case "CambiarMontoDetraccion"

                    Dim iddocumento As String = Request.QueryString("iddocumento")
                    Dim MontoN As String = Request.QueryString("MontoN")
                    Dim estado As String = Request.QueryString("estado")

                    Dim MontoEntero As Integer = Convert.ToInt32(MontoN)
                    Dim lista As New List(Of be_detraccionMasiva)
                    Dim rpta As String = ""
                    Try

                        lista = (New blPagoDetraccionesMasivas).listarDetraccionesMasivasMontosEstados(iddocumento, MontoEntero, estado)

                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    If lista.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try

                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()
                    End If

            Case "ExportarFormato"

                    Dim txtFechaInicio As String = Request.QueryString("fechaInicio")
                    Dim txtFechaFinal As String = Request.QueryString("FechaFinal")
                    Dim txtPeriodo As String = Request.QueryString("periodo")

                    Dim lista As New List(Of be_Exportacion)
                    Dim rpta As String = ""
                    Try
                        lista = (New blExportacion).listarDetraccionesAprobadas(txtFechaInicio, txtFechaFinal, txtPeriodo)

                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    If lista.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try

                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()
                    End If
            Case "GenerarLoteDetraccion"

                    Dim txtFechaInicio As String = Request.QueryString("fechaInicio")
                    Dim txtFechaFinal As String = Request.QueryString("FechaFinal")
                    Dim txtPeriodo As String = Request.QueryString("periodo")

                    Dim lista As New List(Of be_Exportacion)
                    Dim GeneraDetraccion As New List(Of be_Exportacion)

                    Dim rpta As String = ""
                    Try
                        GeneraDetraccion = (New blExportacion).GeneraLoteDetracciones(txtFechaInicio, txtFechaFinal, 0)
                        'lista = (New blExportacion).listarDetraccionesAprobadas(txtFechaInicio, txtFechaFinal, 0)

                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    If lista.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try

                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()
                    End If

            Case "GenerarReporteLoteDetraccion"
                    'Dim txtFechaInicio As String = Request.QueryString("fechaInicio")
                    'Dim txtFechaFinal As String = Request.QueryString("FechaFinal")
                    Dim txtlote As String = Request.QueryString("lote")
                    Dim listaReporte As New List(Of be_Exportacion)
                    'Dim lista As New List(Of be_Exportacion)

                    Dim rpta As String = ""
                    Try
                        'CierreLote = (New blExportacion).CierreLoteDetracciones(txtFechaInicio, txtFechaFinal, txtlote)
                        listaReporte = (New blExportacion).listarDetraccionesxLote(txtlote)

                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    If listaReporte.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(listaReporte, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try

                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()
                    End If


            Case "CancelarLoteDetracciones"

                    Dim txtlote As String = Request.QueryString("lote")
                    Dim periodocanc As String = Request.QueryString("periodo")
                    Dim idBancoCanc As String = Request.QueryString("idBancoCanc")
                    Dim nrooperacion As String = Request.QueryString("nrooperacion")
                    Dim montocancelado As String = Request.QueryString("montocancelado")
                    Dim FechaCancelacion As String = Request.QueryString("FechaCancelacion")

                    Dim lista As New List(Of be_Exportacion)

                    Dim obj As New Negocio.bl_pagoProveedor
                    Dim IdUsuario As Integer
                    IdUsuario = Convert.ToInt32(Session.Item("IdUsuario"))
                    Dim objeto22 As New Negocio.bl_pagoProveedor

                    If FechaCancelacion <> "" Then
                        nroVoucher = objeto22.CancelarDetraccionesLotes(txtlote, periodocanc, idBancoCanc, nrooperacion, montocancelado, FechaCancelacion, IdUsuario)

                    Response.Write(nroVoucher)

                    Response.End()

                End If

            Case "CierreLoteDetracciones"

                    Dim txtFechaInicio As String = Request.QueryString("fechaInicio")
                    Dim txtFechaFinal As String = Request.QueryString("FechaFinal")
                    Dim txtlote As String = Request.QueryString("lote")

                    Dim lista As New List(Of be_Exportacion)
                    Dim CierreLote As New List(Of be_Exportacion)

                    Dim rpta As String = ""
                    Try
                        CierreLote = (New blExportacion).CierreLoteDetracciones(txtFechaInicio, txtFechaFinal, txtlote)

                        'lista = (New blExportacion).listarDetraccionesAprobadas(txtFechaInicio, txtFechaFinal, 0)

                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    If lista.Count > 0 Then
                        Try
                            rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                        Catch ex As Exception
                            objScript.mostrarMsjAlerta(Me, ex.Message)
                        End Try

                        Response.Write(rpta)
                        Response.End()
                    Else
                        Response.Write(rpta)
                        Response.End()
                    End If
        End Select


    End Sub

    Private Sub registrar_CuentaBancaria()
        Try
            Dim objCuentaBancaria As Entidades.CuentaBancaria = obtenerCuentaBancaria()

            If ((New Negocio.CuentaBancaria).InsertaCuentaBancariaDetraccion(objCuentaBancaria)) Then
                objScript.mostrarMsjAlerta(Me, "La Cuenta detraccion fue Actualizada con éxito.")
            Else
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    'Private Sub RegistrarConstancia()
    '    Try
    '        '   Dim objCuentaBancaria As Entidades.CuentaBancaria = obtenerCuentaBancaria()

    '        If ((New Negocio.CuentaBancaria).InsertaCuentaBancariaDetraccion(objCuentaBancaria)) Then
    '            objScript.mostrarMsjAlerta(Me, "La Cuenta detraccion fue Actualizada con éxito.")
    '        Else
    '            objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")
    '        End If

    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub





    Private Sub registrar_MontoRegimenDetalle()
        Try
            Dim objMontoRegimenDet As Entidades.be_MontoRegimenDet = obtenerMontoRegimen()
            If ((New Negocio.MontoRegimenDet).InsertMontoRegimenDet(objMontoRegimenDet)) Then
                objScript.mostrarMsjAlerta(Me, "La Cuenta detraccion fue Actualizada con éxito.")
            Else
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



    Private Function obtenerCuentaBancaria() As Entidades.CuentaBancaria
        Dim obj As New Entidades.CuentaBancaria
        With obj
            .IdBanco = 17
            .IdPersona = idpersonai
            .IdMoneda = 1
            .Saldocontable = 0
            .SaldoDisponible = 0
            .Estado = "1"
            .NroCuentaBancaria = cuenta
            .NroCuentaContable = ""
            .NroCuentaInterbanca = ""
            .esCuentaDetraccion = 1
            .SwiftBancaria = ""
        End With
        Return obj
    End Function


    Private Function obtenerMontoRegimen() As Entidades.be_MontoRegimenDet
        Dim obj As New Entidades.be_MontoRegimenDet
        With obj
            .IdDocumento = DetIDDocumentoGenera
            .IdServicio = DetIdServicio
            .IdOperacion = DetIdOperacion
            .ro_Id = 2
            .Monto = 0
            .MontoDetraccion = DetMontoDetraccion
            .Serie = DetSerie
            .Codigo = DetCodigo
            .EstadoDet = 2

        End With
        Return obj
    End Function



End Class