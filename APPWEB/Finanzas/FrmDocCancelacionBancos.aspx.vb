﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmDocCancelacionBancos
    Inherits System.Web.UI.Page

#Region "Atributos"

    Private objScript As New ScriptManagerClass
    Private objCbo As New Combo
    Private ListaDetalleConcepto As List(Of Entidades.DetalleConcepto)
    Private NegFechaActual As Negocio.FechaActual ' = Nothing

    Private Property ListaCancelacion() As List(Of Entidades.DatosCancelacion)
        Get
            Return CType(Session.Item("ListaCancelacion"), List(Of Entidades.DatosCancelacion))
        End Get
        Set(ByVal value As List(Of Entidades.DatosCancelacion))
            Session.Remove("ListaCancelacion")
            If value IsNot Nothing Then
                Session.Add("ListaCancelacion", value)
            End If
        End Set
    End Property

    Private Property IdMedioPagoPrincipal() As Integer
        Get
            Return CInt(hddIdMedioPagoPrincipal.Value)
        End Get
        Set(ByVal value As Integer)
            hddIdMedioPagoPrincipal.Value = CStr(value)
        End Set
    End Property

    Private Property IdMedioPagoInterfaz() As Integer
        Get
            Return CInt(hddIdMedioPagoInterfaz.Value)
        End Get
        Set(ByVal value As Integer)
            hddIdMedioPagoInterfaz.Value = CStr(value)
        End Set
    End Property

    Private Property ListaCheques() As List(Of Entidades.DocumentoCheque)
        Get
            Return CType(Session.Item("ListaCheques"), List(Of Entidades.DocumentoCheque))
        End Get
        Set(ByVal value As List(Of Entidades.DocumentoCheque))
            Session.Remove("ListaCheques")
            If value IsNot Nothing Then
                Session.Add("ListaCheques", value)
            End If
        End Set
    End Property
    Private Property ChequeEgreso() As Entidades.DocumentoCheque
        Get
            Return CType(Session.Item("ChequeEgreso"), Entidades.DocumentoCheque)
        End Get
        Set(ByVal value As Entidades.DocumentoCheque)
            Session.Remove("ChequeEgreso")
            If value IsNot Nothing Then
                Session.Add("ChequeEgreso", value)
            End If
        End Set
    End Property
    Private Property IdPersona() As Integer
        Get
            Return CInt(hddIdPersona.Value)
        End Get
        Set(ByVal value As Integer)
            hddIdPersona.Value = CStr(value)
        End Set
    End Property
    Private Property IdCheque() As Integer
        Get
            Return CInt(ViewState("IdCheque"))
        End Get
        Set(ByVal value As Integer)
            ViewState.Remove("IdCheque")
            ViewState.Add("IdCheque", value)
        End Set
    End Property
    Private Property Modo() As Integer
        Get
            Return CInt(hddFrmModo.Value)
        End Get
        Set(ByVal value As Integer)
            hddFrmModo.Value = CStr(value)
        End Set
    End Property
    Private Property IdDocumento() As Integer
        Get
            Return CInt(hddIdDocumento.Value)
        End Get
        Set(ByVal value As Integer)
            hddIdDocumento.Value = CStr(value)
        End Set
    End Property
    Private Property IdConceptoMovBanco() As Integer
        Get
            Return CInt(ViewState("IdConceptoMovBanco"))
        End Get
        Set(ByVal value As Integer)
            ViewState.Remove("IdConceptoMovBanco")
            ViewState.Add("IdConceptoMovBanco", value)
        End Set
    End Property
    'Cambio 2010-04-20 1812
    'ini
    'p significa parametro, program y en otros casos persistencia (sesiones, viewstate)
    Private Property pIdTipoOperacion() As Integer
        Get
            Return CInt(cboTipoOperacion.SelectedValue)
        End Get
        Set(ByVal value As Integer)
            cboTipoOperacion.SelectedValue = CStr(value)
        End Set
    End Property
    'Private Const _IDTIPOOPERACION As Integer = 32      'Cancelacion Bancos
    'fin

    Private Const _IDCONDICIONPAGO As Integer = 1       'Contado
    Private Const _IDTIPODOCUMENTO As Integer = 51      'Documento Cancelación Bancos, antes era 43, ya se actualizo la BD
    Private Const _FACTOR As Integer = -1               'Egreso
    Private Const _IDTIPOMOVIMIENTO As Integer = 2      'Egreso

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum

#End Region

#Region "Funciones Auxiliares"
    Private Function DescFechaActual() As String
        Return Format(FechaActual, "dd/MM/yyyy")
    End Function
    Private Function FechaActual() As Date
        If NegFechaActual Is Nothing Then
            NegFechaActual = New Negocio.FechaActual
        End If
        Return NegFechaActual.SelectFechaActual
    End Function

    Private Sub GenerarCodigoDocumento()
        Try
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), _IDTIPODOCUMENTO)

            If (IsNumeric(Me.cboSerie.SelectedValue)) Then
                Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
            Else
                Me.txtCodigoDocumento.Text = ""
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub actualizarControles_BANCOS()

        With objCbo
            .LlenarCboBanco(Me.cboBanco, False)
            .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
        End With
    End Sub
#End Region

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    Private Sub validarVariablesUrl(e As EventArgs)
        Dim idTipoOperacion = Request.QueryString("idTipoOperacion")
        If idTipoOperacion IsNot Nothing Then
            Dim idProveedor As Integer = Request.QueryString("idProveedor")
            Me.cboTipoOperacion.SelectedValue = idTipoOperacion
            'SECCION PROVEEDOR
            cargarPersona(idProveedor)
            'SECCION DEUDAS PENDIENTES
            cboMotivoDocCancelacionBancos_SelectedIndexChanged(cboMotivoDocCancelacionBancos, e)
            'If (txtFechaVcto.Text <> "") Then
            '    actualizarMotivoPago(1)
            'Else
            '    objScript.mostrarMsjAlerta(Me, "Debe de elegir una fecha fin para continuar con la consulta.")
            'End If
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            ConfigurarDatos()
            inicializarFrm()
            validarVariablesUrl(e)
        End If
        visualizarMoneda()
    End Sub

    Private Sub visualizarMoneda()
        '************ Mostramos las Monedas
        Me.lblMonedaFaltante.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalAPagar.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaTotalRecibido.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaVuelto.Text = Me.cboMoneda.SelectedItem.ToString
    End Sub


    Private Sub inicializarFrm()

        Try

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                '.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

                .LLenarCboEstadoDocumento(Me.cboEstado)
                .LlenarCboMoneda(Me.cboMoneda)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, _IDTIPODOCUMENTO, False)

                '**************** DATOS DE CANCELACION
                .LlenarCboMedioPagoxIdTipoDocumento(Me.cboMedioPago, _IDTIPODOCUMENTO)
                .LlenarCboBanco(Me.cboBanco, False)
                .LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
                .LlenarCboMoneda(Me.cboMoneda_DatoCancelacion, False)
                '.LlenarCboPostxIdCaja(Me.cboPost_DC, CInt(Me.cboCaja.SelectedValue), False)


                Me.txtFechaEmision.Text = DescFechaActual()
                Me.txtFechaACobrar.Text = Me.txtFechaEmision.Text
                Me.txtFechaVcto.Text = Me.txtFechaEmision.Text
                '*************** MEDIO DE PAGO PRINCIPAL
                Me.IdMedioPagoPrincipal = (New Negocio.MedioPago).SelectIdMedioPagoPrincipal()
                actualizarControlesMedioPago(_IDCONDICIONPAGO, CInt(Me.cboMedioPago.SelectedValue))

                ValidarPermisos()
                verFrm(FrmModo.Nuevo, True, True, True, True, True, True)

                '**********Busqueda Persona
                .LlenarCboRol(cboRol, True)

            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {178, 179, 180, 181, 182})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

    End Sub

    Private Sub limpiarControlesFrm(Optional ByVal limpiarDatosPersona As Boolean = True, Optional ByVal procesoEdicion As Boolean = False)


        If Not procesoEdicion Then
            Me.txtCodigoDocumento.Text = ""
            Me.txtFechaEmision.Text = DescFechaActual()
            Me.IdDocumento = 0

        End If

        If limpiarDatosPersona Then
            Me.txtDescripcionPersona.Text = ""
            Me.txtDNI.Text = ""
            Me.txtRUC.Text = ""
            Me.IdPersona = 0
        End If



        Me.txtFechaACobrar.Text = DescFechaActual()
        Me.txtFechaInicio_BA.Text = Me.txtFechaACobrar.Text
        Me.txtFechaFin_BA.Text = Me.txtFechaACobrar.Text
        Me.txtFaltante.Text = "0"
        Me.txtTotalAPagar.Text = "0"
        Me.txtTotalRecibido.Text = "0"
        Me.txtVuelto.Text = "0"



        Me.GV_Cancelacion.DataSource = Nothing
        Me.GV_Cancelacion.DataBind()
        Me.GV_Deudas.DataSource = Nothing
        Me.GV_Deudas.DataBind()
        Me.GV_Otros.DataSource = Nothing
        Me.GV_Otros.DataBind()
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.lblMessageHelp.Visible = False

        Me.txtObservaciones.Text = ""

        Me.hddCodigoDocumento.Value = txtCodigoDocumento.Text

        'actualizarControlesMedioPago(_IDCONDICIONPAGO, IdMedioPagoPrincipal)
        actualizarControlesMedioPago(_IDCONDICIONPAGO, CInt(cboMedioPago.SelectedValue))

    End Sub
    Private Sub verFrm(ByVal modoFrm As Integer, ByVal generarCodigoDoc As Boolean, ByVal limpiarSessionViewState As Boolean, ByVal initListasSession As Boolean, ByVal limpiarControles As Boolean, ByVal initMotivoReciboI As Boolean, ByVal limpiarDatosPersona As Boolean, Optional ByVal procesoEdicion As Boolean = False)

        If (limpiarControles) Then
            limpiarControlesFrm(limpiarDatosPersona, procesoEdicion)
        End If


        If (limpiarSessionViewState) Then
            ListaCancelacion = Nothing
            'Session.Remove("ListaCancelacion")
            'ViewState.Clear()
        End If


        If (initListasSession) Then
            Me.ListaCancelacion = New List(Of Entidades.DatosCancelacion)
            'setListaCancelacion(Me.ListaCancelacion)
        End If

        If (generarCodigoDoc) Then
            GenerarCodigoDocumento()
        End If

        If (initMotivoReciboI) Then
            Me.cboMotivoDocCancelacionBancos.SelectedValue = "0"
            Me.btnAddConcepto_Otros.Visible = False
        End If

        '************** MODO FRM
        Modo = modoFrm
        ActualizarBotonesControl()

    End Sub
    Private Sub ActualizarBotonesControl()

        Select Case Modo

            Case 0 '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                'Me.cboCaja.Enabled = True

                Me.cboMoneda.Enabled = True

            Case 1 '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cancelacion.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                'Me.cboCaja.Enabled = True

                Me.cboMoneda.Enabled = True


            Case 2 '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cancelacion.Enabled = True
                Me.Panel_Cliente.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                'Me.cboCaja.Enabled = True

                Me.cboMoneda.Enabled = True

            Case 3 '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '******** Me.btnBuscarDocumentoxcodigo.Visible = True

                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()

                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False

                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True
                'Me.cboCaja.Enabled = True

                Me.cboMoneda.Enabled = True

            Case 4 '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                '********** Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                'Me.cboCaja.Enabled = False

                Me.cboMoneda.Enabled = False

            Case 5 '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '********* Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True

                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                'Me.cboCaja.Enabled = False

                Me.cboMoneda.Enabled = False

            Case 6  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                '********* Me.btnBuscarDocumentoxcodigo.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.Panel_Cancelacion.Enabled = False
                Me.Panel_Cliente.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Cab.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                'Me.cboCaja.Enabled = False

                Me.cboMoneda.Enabled = False


        End Select
    End Sub



#Region "Eventos"
    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try


            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            'objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            '
            'objCbo.LlenarCboCajaxIdTiendaxIdUsuario(Me.cboCaja, CInt(Me.cboTienda.SelectedValue), CInt(Session("IdUsuario")), False)

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            If (IsNumeric(Me.cboSerie.SelectedValue)) Then
                Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
            Else
                Me.txtCodigoDocumento.Text = ""
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboMotivoDocCancelacionBancos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMotivoDocCancelacionBancos.SelectedIndexChanged
        If (txtFechaVcto.Text <> "") Then
            actualizarMotivoPago(1)
        Else
            objScript.mostrarMsjAlerta(Me, "Debe de elegir una fecha fin para continuar con la consulta.")
        End If


    End Sub
    Private Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
        Try
            objCbo.LlenarCboCuentaBancaria(Me.cboCuentaBancaria, CInt(Me.cboBanco.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    'Private Sub cboPost_DC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPost_DC.SelectedIndexChanged
    '    Try
    '        actualizarControles_POST()
    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub

#Region "Botones"
    Protected Sub btnVerHistorial_Click(ByVal sender As Object, ByVal e As EventArgs)
        verHistorialAbonos(sender)
    End Sub
    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True, True, True)
    End Sub

    Private Sub btnBuscarDocumentoxcodigo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarDocumentoxCodigo.Click
        Try
            buscarDocumento(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value), Nothing)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        HabilitarEdicionDocumento()
    End Sub
    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        anularDocumentoDocCancelacionBancos(IdDocumento)
    End Sub

    Private Sub btnAddConcepto_Otros_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddConcepto_Otros.Click
        addNewDetalle_Otros()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        'registrarDocCancelacionBancos()
    End Sub

#End Region
#Region "Grillas"
    Private Sub GV_Otros_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Otros.SelectedIndexChanged
        eliminarDetalleRecibo_Otros(Me.GV_Otros.SelectedIndex)
    End Sub
    Private Sub GV_Otros_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Otros.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim gvrow As GridViewRow = CType(e.Row.Cells(1).NamingContainer, GridViewRow)
                Dim combo As DropDownList = CType(gvrow.FindControl("cboConcepto"), DropDownList)

                If (Me.ListaDetalleConcepto(e.Row.RowIndex).IdConcepto <> Nothing) Then
                    combo.SelectedValue = CStr(Me.ListaDetalleConcepto(e.Row.RowIndex).IdConcepto)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region
#End Region

    Private Sub actualizarMotivoPago(ByVal Opcion As Integer)
        Try
            Dim fecha As Date = Nothing
            fecha = CDate(txtFechaVcto.Text)
            '********* Limpiamos la grilla Deudas
            Me.GV_Deudas.DataSource = Nothing
            Me.GV_Deudas.DataBind()
            Me.GV_Otros.DataSource = Nothing
            Me.GV_Otros.DataBind()

            Me.lblMessageHelp.Visible = False
            Me.btnAddConcepto_Otros.Visible = False

            Select Case Opcion

                Case 0 '************ Seleccione una Opción

                Case 1 '************ Pago de deudas
                    Dim lista As List(Of Entidades.Documento_MovCuentaPorPagar) = (New Negocio.MovCuentaPorPagar).SelectDeudaxIdProveedorxIdMonedaxIdTipoDocCancelTwo(IdPersona, CInt(cboMoneda.SelectedValue), _IDTIPODOCUMENTO, fecha)
                    If (lista.Count <= 0) Then
                        Me.cboMotivoDocCancelacionBancos.SelectedValue = "0"
                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "alert('La Persona seleccionada no posee deudas.'); calcularTotalAPagar();", True)
                        Return
                    Else
                        Me.lblMessageHelp.Visible = True
                        Me.GV_Deudas.DataSource = lista
                        Me.GV_Deudas.DataBind()


                        For Each gvRow As GridViewRow In GV_Deudas.Rows
                            Dim chkItem As CheckBox = DirectCast(gvRow.FindControl("chkSustento"), CheckBox)
                            Dim hddSustento As HiddenField = DirectCast(gvRow.FindControl("hddSustento"), HiddenField)

                            If (hddSustento.Value = "0") Then
                                chkItem.Checked = False
                            Else
                                chkItem.Checked = True
                            End If
                        Next
                    End If

                Case 2 '************ Otros                    
                    Me.btnAddConcepto_Otros.Visible = True
                    Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(True, 1)
                    Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
                    Me.GV_Otros.DataBind()

            End Select

            '********** Recalculamos el total a pagar y los datos de cancelación
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularTotalAPagar();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub verHistorialAbonos(ByVal sender As Object)
        Try

            Dim IdMovCtaPP As Integer = Nothing

            For i As Integer = 0 To Me.GV_Deudas.Rows.Count - 1

                If (CType(sender, ImageButton).ClientID = CType(Me.GV_Deudas.Rows(i).FindControl("btnVerHistorialPago"), ImageButton).ClientID) Then

                    IdMovCtaPP = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdMovCtaPP"), HiddenField).Value)
                    Exit For

                End If

            Next

            If (IdMovCtaPP = Nothing) Then
                Throw New Exception("No se halló el Registro. Error del Sistema.")
            Else

                '************ MOSTRAMOS EL HISTORIAL DE ABONOS
                Dim listaAbonos As List(Of Entidades.Documento_MovCuentaPorPagar) = (New Negocio.MovCuentaPorPagar).SelectAbonosxIdMovCtaPP(IdMovCtaPP)
                If (listaAbonos.Count <= 0) Then
                    Throw New Exception("No se hallaron abonos para el documento seleccionado.")
                Else
                    Me.GV_Abonos.DataSource = listaAbonos
                    Me.GV_Abonos.DataBind()
                    objScript.onCapa(Me, "capaAbonos")
                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub addNewDetalle_Otros()
        Try
            '********* Obtengo la Lista
            Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(False, Nothing)
            '*********** Creo un Nuevo Detalle
            Dim listaAux As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(_IDTIPODOCUMENTO)
            '************ Creo el objeto y lo agrego a la Lista
            Dim obj As New Entidades.DetalleConcepto
            With obj
                .Moneda = Me.cboMoneda.SelectedItem.ToString
                .Monto = 0
                .Concepto = ""
                .ListaConcepto = listaAux
                .IdConcepto = Nothing
            End With
            Me.ListaDetalleConcepto.Add(obj)
            '************ Grilla
            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub eliminarDetalleRecibo_Otros(ByVal index As Integer)
        Try
            '****** Elimino el detalle
            Me.ListaDetalleConcepto = obtenerListaDetalleConcepto(False, Nothing)
            Me.ListaDetalleConcepto.RemoveAt(index)
            '******** Grilla
            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      calcularTotalAPagar();       ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub validarFrm()
        If (Me.GV_Cancelacion.Rows.Count <= 0) Then
            Throw New Exception("NO SE HAN INGRESADO DATOS DE CANCELACIÓN. NO SE PERMITE LA OPERACIÓN.")
        End If
        'Me.ListaCancelacion = getListaCancelacion()
        If CDec(Me.txtVuelto.Text) > 0 Then
            Throw New Exception("NO SE DEBE GENERAR VUELTO. NO SE PERMITE LA OPERACIÓN.")
        End If
        'For i As Integer = 0 To Me.ListaCancelacion.Count - 1
        '    If (Me.ListaCancelacion(i).IdMedioPago <> IdMedioPagoPrincipal) And CDec(Me.txtVuelto.Text) > 0 Then
        '        Throw New Exception("SE HAN INGRESADO MEDIOS DE PAGO QUE NO DEBEN GENERAR VUELTO. NO SE PERMITE LA OPERACIÓN.")
        '    End If
        'Next
    End Sub
    Private Sub registrarDocCancelacionBancos()
        Try
            validarFrm()
            '****** cabecera Documento
            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            '******* Detalle CONCEPTO
            Me.ListaDetalleConcepto = obtenerDetalleConcepto_Save()
            '******** obtengo la lista pago caja a insertar
            Dim listaDatosCancelacion As List(Of Entidades.DatosCancelacion) = obtenerListaDatosCancelacion_Save()
            'Dim objMovCaja As Entidades.MovCaja = obtenerMovCaja()
            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
            Dim listaMovBanco As List(Of Entidades.MovBancoView) = obtenerListaMovBanco(listaDatosCancelacion, objDocumento)
            'Dim objSustento As Entidades.Anexo_Documento = New Entidades.Anexo_Documento
            'objSustento.NroOperacion = "SUSTENTAR"
            'For Each gvRow As GridViewRow In gvSustento.Rows
            '    Dim chkItem As CheckBox = DirectCast(gvRow.FindControl("checksustento"), CheckBox)
            '    If (chkItem.Checked = True) Then
            '        objSustento.EstadoRequerimiento = 1
            '    Else
            '        objSustento.EstadoRequerimiento = 0
            '    End If
            'Next
            'Select Case Modo
            '    Case 1 '******* NUEVO
            '        'Dim IdDocumento As Integer = (New Negocio.DocReciboCaja).ReciboCajaIngresoInsert(objDocumento, Me.ListaDetalleConcepto, objMovCaja, listaDatosCancelacion, objObservaciones, listaMovBanco)
            '       ' Dim IdDocumento2 As Integer = (New Negocio.DocCancelacion).Insert(objDocumento, Me.ListaDetalleConcepto, listaDatosCancelacion, objObservaciones, listaMovBanco, Nothing, Nothing)
            '        If (IdDocumento2 > 0) Then

            '            verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False)
            '            IdDocumento = IdDocumento2
            '            objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")
            '            limpiar_datos()
            '        Else
            '            Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            '        End If


            '    Case 2 '******* EDITAR
            '        'If ((New Negocio.DocReciboCaja).Update(objDocumento, Me.ListaDetalleConcepto, objMovCaja, listaDatosCancelacion, objObservaciones, listaMovBanco) > 0) Then
            '        If ((New Negocio.DocCancelacion).Update(objDocumento, Me.ListaDetalleConcepto, listaDatosCancelacion, objObservaciones, listaMovBanco, Nothing, Nothing) > 0) Then

            '            verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False, False)

            '            objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")
            '            limpiar_datos()
            '        Else
            '            Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            '        End If
            'End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Public Sub limpiar_datos()
        ListaCancelacion.Clear()
        ListaDetalleConcepto.Clear()
    End Sub

#Region "Generación de Objetos"

    Private Function obtenerDetalleConcepto_Save() As List(Of Entidades.DetalleConcepto)

        Dim lista As New List(Of Entidades.DetalleConcepto)

        Select Case CInt(Me.cboMotivoDocCancelacionBancos.SelectedValue)
            Case 0 '********** Elegir motivo
                Throw New Exception("NO SE HA AGREGADO DETALLES AL DOCUMENTO. NO SE PERMITE LA OPERACIÓN.")

            Case 1 '************ Pago deudas
                For i As Integer = 0 To Me.GV_Deudas.Rows.Count - 1
                    If (CType(Me.GV_Deudas.Rows(i).FindControl("chbSelectDeuda"), CheckBox).Checked) Then
                        Dim obj As New Entidades.DetalleConcepto
                        With obj
                            .Monto = CDec(CType(Me.GV_Deudas.Rows(i).FindControl("txtAbono"), TextBox).Text)
                            .Concepto = Me.GV_Deudas.Rows(i).Cells(1).Text + " Nro. " + CType(Me.GV_Deudas.Rows(i).FindControl("lblNroDocumentoDeuda"), Label).Text
                            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)

                            '******** Los documentos a los cuales se les va a afectar
                            .IdDocumentoRef = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdDocumentoDeuda"), HiddenField).Value)
                            .IdMovCuentaRef = CInt(CType(Me.GV_Deudas.Rows(i).FindControl("hddIdMovCtaPP"), HiddenField).Value)
                            'If (CType(Me.GV_Deudas.Rows(i).FindControl("chkSustento"), CheckBox).Checked) Then
                            .ResultSustento = 1
                            'Else
                            '    .ResultSustento = 0
                            'End If

                        End With
                        lista.Add(obj)
                    End If
                Next
            Case 2  '******** otros
                lista = obtenerListaDetalleConcepto(False, Nothing)
        End Select
        Return lista
    End Function

    Private Function obtenerListaDetalleConcepto(ByVal generarNewLista As Boolean, ByVal cantidadInitNew As Integer) As List(Of Entidades.DetalleConcepto)

        Dim lista As New List(Of Entidades.DetalleConcepto)

        '********** Obtengo la lista de conceptos segun el tipo de Recibo
        Dim listaAux As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(_IDTIPODOCUMENTO)

        If (generarNewLista) Then  '********** Generamos la Lista de DetalleRecibo // Otros

            '************ Genero la lista
            For i As Integer = 0 To cantidadInitNew - 1

                Dim obj As New Entidades.DetalleConcepto

                obj.Moneda = Me.cboMoneda.SelectedItem.ToString
                obj.Monto = 0
                obj.Concepto = ""
                obj.ListaConcepto = listaAux
                obj.IdConcepto = Nothing
                lista.Add(obj)

            Next
        Else

            '**************** DEVOLVEMOS LA LISTA DEL DETALLE CONCEPTO (ACTUALIZAMOS)

            For i As Integer = 0 To Me.GV_Otros.Rows.Count - 1

                Dim obj As New Entidades.DetalleConcepto

                obj.Moneda = CType(Me.GV_Otros.Rows(i).FindControl("lblMoneda"), Label).Text
                obj.Monto = CDec(CType(Me.GV_Otros.Rows(i).FindControl("txtAbono"), TextBox).Text)
                obj.Concepto = CType(Me.GV_Otros.Rows(i).FindControl("txtDescripcionConcepto"), TextBox).Text
                obj.ListaConcepto = listaAux
                obj.IdConcepto = CInt(CType(Me.GV_Otros.Rows(i).FindControl("cboConcepto"), DropDownList).SelectedValue)
                lista.Add(obj)

            Next

        End If

        Return lista

    End Function
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            .IdUsuario = CInt(Session("IdUsuario"))
            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = Me.cboSerie.SelectedItem.ToString
            .Codigo = Me.txtCodigoDocumento.Text
            .IdTipoDocumento = _IDTIPODOCUMENTO
            .IdEstadoDoc = 1  '************ Siempre es ACTIVO
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            '.IdCaja = CInt(Me.cboCaja.SelectedValue)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdPersona = IdPersona
            .TotalAPagar = CDec(Me.txtTotalAPagar.Text)
            .Total = CDec(Me.txtTotalAPagar.Text)
            .IdTipoOperacion = pIdTipoOperacion
            .IdCondicionPago = _IDCONDICIONPAGO
            .FechaCancelacion = FechaActual()

            Select Case Modo
                Case FrmModo.Nuevo  '******* Nuevo
                    .Id = Nothing
                Case FrmModo.Editar '***** Editar
                    .Id = IdDocumento
            End Select

            .Vuelto = CDec(Me.txtVuelto.Text)
            .IdEstadoCancelacion = 2   '******** CANCELADO
            .TotalLetras = (New Aletras).Letras(CStr(objDocumento.TotalAPagar)) + " /100 " + (New Negocio.Moneda).SelectxId(.IdMoneda)(0).Descripcion

        End With

        Return objDocumento

    End Function

    Private Function obtenerListaMovBanco(ByVal lista As List(Of Entidades.DatosCancelacion), ByVal objDocumento As Entidades.Documento) As List(Of Entidades.MovBancoView)

        Dim listaMovBanco As New List(Of Entidades.MovBancoView)
        For i As Integer = 0 To lista.Count - 1
            'Si el datos cancelacion esta asociado a un cheque egreso, no se crea un mov banco
            'porque este ya se creo a la hora de crear ese cheque.
            If lista(i).IdMedioPagoInterfaz <> 7 Then

                Dim obj As New Entidades.MovBancoView
                With obj
                    .IdBanco = lista(i).IdBanco
                    .IdCuentaBancaria = lista(i).IdCuentaBancaria
                    .Monto = lista(i).Monto
                    .IdMoneda = lista(i).IdMoneda
                    .IdMedioPago = lista(i).IdMedioPago
                    .IdUsuario = objDocumento.IdUsuario
                    .IdPersonaRef = objDocumento.IdPersona
                    '.IdCaja = objDocumento.IdCaja
                    .IdTienda = objDocumento.IdTienda
                    .FechaMov = objDocumento.FechaEmision
                    .NroOperacion = lista(i).NroOperacion
                    .IdConceptoMovBanco = IdConceptoMovBanco
                    .Factor = _FACTOR
                    .EstadoAprobacion = False    'Revisar
                    .EstadoMov = True

                End With

                listaMovBanco.Add(obj)
            End If
        Next

        Return listaMovBanco

    End Function

    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservaciones As Entidades.Observacion = Nothing
        If (Me.txtObservaciones.Text.Trim.Length > 0) Then

            objObservaciones = New Entidades.Observacion
            objObservaciones.Observacion = Me.txtObservaciones.Text.Trim

        End If
        Return objObservaciones
    End Function

    Private Function obtenerListaDatosCancelacion_Save() As List(Of Entidades.DatosCancelacion)

        If (Me.ListaCancelacion.Count = 0) Then

            Dim objDatosCancelacion As New Entidades.DatosCancelacion
            With objDatosCancelacion
                .Monto = CDec(Me.txtTotalAPagar.Text)
                .Factor = _FACTOR
                .IdMedioPago = Me.IdMedioPagoPrincipal
                .IdMedioPagoInterfaz = IdMedioPagoInterfaz
                .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
                .IdTipoMovimiento = _IDTIPOMOVIMIENTO
                '.IdCaja = CInt(cboCaja.SelectedValue)
            End With

            Me.ListaCancelacion.Add(objDatosCancelacion)

        End If

        Return Me.ListaCancelacion

    End Function

#End Region

    Private Sub cargarDocumentoCab(ByVal objDocumento As Entidades.Documento)

        Me.txtCodigoDocumento.Text = objDocumento.Codigo
        Me.txtFechaEmision.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = CStr(objDocumento.IdEstadoDoc)
        Me.IdDocumento = objDocumento.Id
        Me.txtTotalAPagar.Text = CStr(Math.Round(objDocumento.TotalAPagar, 2))

        If (Me.cboMoneda.Items.FindByValue(CStr(objDocumento.IdMoneda)) IsNot Nothing) Then
            Me.cboMoneda.SelectedValue = CStr(objDocumento.IdMoneda)
        End If

        If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
            Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
        End If

    End Sub

    Private Sub HabilitarEdicionDocumento()
        Try
            If (New Negocio.DatosCancelacion).DeshacerMov_Edicion(IdDocumento) Then

                verFrm(FrmModo.Editar, False, True, True, True, True, False, True)
                objScript.mostrarMsjAlerta(Me, "El Proceso resultó con éxito.")

            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub anularDocumentoDocCancelacionBancos(ByVal IdDoc As Integer)
        Try
            If ((New Negocio.DatosCancelacion).Anular(IdDoc)) Then
                verFrm(FrmModo.Documento_Anular_Exito, False, False, False, False, False, False, False)
                objScript.mostrarMsjAlerta(Me, "El Proceso resultó con éxito.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub buscarDocumento(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)
        Try
            Dim objDocumento As Entidades.Documento = Nothing

            If (IdDocumento = Nothing) Then
                objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
            Else
                objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
            End If

            '******** Si paso es xq si existe el documento
            verFrm(FrmModo.Documento_Buscar_Exito, False, True, True, True, True, True)

            cargarDocumentoCab(objDocumento)
            cargarPersona(objDocumento.IdPersona)
            Me.txtNroVoucher.Text = objDocumento.NroVoucherConta
            Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)
            If (objObservaciones IsNot Nothing) Then
                Me.txtObservaciones.Text = objObservaciones.Observacion
            End If

            Me.ListaDetalleConcepto = (New Negocio.DetalleConcepto).SelectxIdDocumento(objDocumento.Id)
            Me.GV_Otros.DataSource = Me.ListaDetalleConcepto
            Me.GV_Otros.DataBind()


            Me.ListaCancelacion = obtenerListaCancelacion_Load(objDocumento.Id)
            'setListaCancelacion(Me.ListaCancelacion)
            Me.GV_Cancelacion.DataSource = Me.ListaCancelacion
            Me.GV_Cancelacion.DataBind()

            '************ Calculamos los datos de cancelación
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularDatosCancelacion();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#Region "**************************** DATOS DE CANCELACION"

    Private Function obtenerListaCancelacion_Load(ByVal IdDocumento As Integer) As List(Of Entidades.DatosCancelacion)
        Dim lista As List(Of Entidades.DatosCancelacion) = (New Negocio.DatosCancelacion).SelectListxIdDocumento(IdDocumento)

        'For i As Integer = 0 To lista.Count - 1

        '    Select Case lista(i).IdMedioPagoInterfaz


        '        Case 1  '*********** EFECTIVO

        '            lista(i).Descripcion = " Medio Pago: [ " + lista(i).MedioPago + " ] Monto: [ " + lista(i).Moneda + " " + CStr(Math.Round(lista(i).Monto, 2)) + " ]"

        '        Case 2  '*********** BANCO  (NRO OPERACION)

        '            lista(i).Descripcion = " Medio Pago: [ " + lista(i).MedioPago + " ] Monto: [ " + lista(i).Moneda + " " + CStr(Math.Round(lista(i).Monto, 2)) + " ] Banco: [ " + lista(i).Banco + " ] Cuenta: [ " + lista(i).CuentaBancaria + " ]"


        '        Case 3  '************** BANCO CHEQUE

        '            Dim fecha As String = ""

        '            If (lista(i).FechaACobrar <> Nothing) Then
        '                fecha = Format(lista(i).FechaACobrar, "dd/MM/yyyy")
        '            Else
        '                fecha = "---"
        '            End If

        '            lista(i).Descripcion = " Medio Pago: [ " + lista(i).MedioPago + " ] Monto: [ " + lista(i).Moneda + " " + CStr(Math.Round(lista(i).Monto, 2)) + " ] Banco: [ " + lista(i).Banco + " ] Fecha a Cobrar: [ " + fecha + " ]"


        '            'Case 4 '************* TARJETA

        '            '    lista(i).Descripcion = " Medio Pago: [ " + lista(i).MedioPago + " ] Monto: [ " + lista(i).Moneda + " " + CStr(Math.Round(lista(i).Monto, 2)) + " ] Banco: [ " + lista(i).Banco + " ] Cuenta: [ " + lista(i).CuentaBancaria + " ]"

        '            'Case 5   '*********** NOTA DE CREDITO

        '            '    lista(i).Descripcion = " Medio Pago: [ " + lista(i).MedioPago + " ] Monto: [ " + lista(i).Moneda + " " + CStr(Math.Round(lista(i).Monto, 2)) + " ] Nro. Documento: [ " + lista(i).NroOperacion + " ] "

        '            'Case 6  '************* COMP RETENCION

        '            '    lista(i).Descripcion = " Medio Pago: [ " + lista(i).MedioPago + " ] Monto: [ " + lista(i).Moneda + " " + CStr(Math.Round(lista(i).Monto, 2)) + " ] Nro. Documento: [ " + lista(i).NroOperacion + " ] "

        '    End Select


        'Next

        Return lista
    End Function

    Protected Sub cboMedioPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMedioPago.SelectedIndexChanged
        Try
            actualizarControlesMedioPago(_IDCONDICIONPAGO, CInt(Me.cboMedioPago.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarControlesMedioPago(ByVal IdCondicionPago As Integer, ByVal IdMedioPago As Integer)

        '************ Limpiar Datos de Cancelación
        Me.txtNro_DC.Text = ""
        Me.txtMonto_DatoCancelacion.Text = "0"
        Me.txtFechaACobrar.Text = ""

        Select Case IdCondicionPago
            Case 1  '**************** CONTADO
                Me.Panel_CP_Contado.Visible = True

                '*************** MEDIO DE PAGO
                Me.lblMonto_DC.Visible = False
                Me.txtMonto_DatoCancelacion.Visible = False
                Me.cboMoneda_DatoCancelacion.Visible = False
                Me.btnAddDatoCancelacion.Visible = False

                'Me.lblPost_DC.Visible = False
                'Me.cboPost_DC.Visible = False
                'Me.lblTarjeta_DC.Visible = False
                'Me.cboTarjeta_DC.Visible = False
                Me.lblBanco_DC.Visible = False
                Me.cboBanco.Visible = False
                Me.lblCuentaBancaria_DC.Visible = False
                Me.cboCuentaBancaria.Visible = False
                Me.lblNro_DC.Visible = False
                Me.txtNro_DC.Visible = False
                Me.lblFechaACobrar_DC.Visible = False
                Me.txtFechaACobrar.Visible = False
                Me.btnSeleccionarCheque.Visible = False

                Me.Panel_DatosCancelacion_Add.Enabled = True
                Me.txtMonto_DatoCancelacion.Enabled = True
                Me.cboMoneda_DatoCancelacion.Enabled = True

                'Me.btnVer_NotaCredito.Visible = False

                Dim objMedioPago As Entidades.MedioPago = (New Negocio.MedioPago).SelectxId(IdMedioPago)(0)

                Me.cboBanco.Enabled = True
                Me.cboCuentaBancaria.Enabled = True

                Select Case objMedioPago.IdMedioPagoInterfaz

                    Case 1  '*************** EFECTIVO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True

                    Case 2  '*************** BANCO
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblCuentaBancaria_DC.Visible = True
                        Me.cboCuentaBancaria.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True

                        actualizarControles_BANCOS()

                    Case 3  '**************** BANCO CHEQUE
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True
                        Me.lblFechaACobrar_DC.Visible = True
                        Me.txtFechaACobrar.Visible = True

                        actualizarControles_BANCOS()

                    Case 4  '*********** TARJETA
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblCuentaBancaria_DC.Visible = True
                        Me.cboCuentaBancaria.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True
                        'Me.lblPost_DC.Visible = True
                        'Me.cboPost_DC.Visible = True
                        'Me.lblTarjeta_DC.Visible = True
                        'Me.cboTarjeta_DC.Visible = True

                        '************ Configuramos los cambios por POST
                        'actualizarControles_POST()
                        Me.cboBanco.Enabled = False
                        Me.cboCuentaBancaria.Enabled = False

                    Case 5  '************* NOTA CREDITO

                        'Me.btnVer_NotaCredito.Visible = True

                    Case 6  '************** COMPROBANTE DE RETENCION

                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True

                    Case 7  '************** CHEQUE EGRESO 
                        Me.lblMonto_DC.Visible = True
                        Me.txtMonto_DatoCancelacion.Visible = True
                        Me.cboMoneda_DatoCancelacion.Visible = True
                        Me.btnAddDatoCancelacion.Visible = True
                        Me.lblBanco_DC.Visible = True
                        Me.cboBanco.Visible = True
                        Me.lblCuentaBancaria_DC.Visible = True
                        Me.cboCuentaBancaria.Visible = True
                        Me.lblNro_DC.Visible = True
                        Me.txtNro_DC.Visible = True
                        Me.lblFechaACobrar_DC.Visible = True
                        Me.txtFechaACobrar.Visible = True

                        actualizarControles_BANCOS()
                        Me.btnSeleccionarCheque.Visible = True
                        Me.Panel_DatosCancelacion_Add.Enabled = False
                        Me.txtMonto_DatoCancelacion.Enabled = False
                        Me.cboMoneda_DatoCancelacion.Enabled = False


                End Select

                IdMedioPagoInterfaz = objMedioPago.IdMedioPagoInterfaz
                IdConceptoMovBanco = objMedioPago.IdConceptoMovBanco
            Case 2  '**************** CREDITO
                Me.Panel_CP_Contado.Visible = False

        End Select

    End Sub

    Protected Sub btnAddDatoCancelacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddDatoCancelacion.Click
        Try
            addDatoCancelacion()
        Catch ex As Exception
            Me.objScript.mostrarMsjAlerta(Me, "Problema en la Agregación de Datos de cancelación")
        End Try
    End Sub

    Private Sub addDatoCancelacion()
        Try

            'Me.ListaCancelacion = getListaCancelacion()

            Dim objDatosCancelacion As New Entidades.DatosCancelacion

            objDatosCancelacion.IdMedioPago = CInt(Me.cboMedioPago.SelectedValue)
            objDatosCancelacion.Factor = _FACTOR
            objDatosCancelacion.IdTipoMovimiento = _IDTIPOMOVIMIENTO
            objDatosCancelacion.IdMedioPagoInterfaz = IdMedioPagoInterfaz

            Select Case IdMedioPagoInterfaz

                Case 1  '*********** EFECTIVO

                    objDatosCancelacion.MonedaSimboloDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objDatosCancelacion.Monto = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objDatosCancelacion.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objDatosCancelacion.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objDatosCancelacion.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objDatosCancelacion.Monto, objDatosCancelacion.IdMoneda, objDatosCancelacion.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objDatosCancelacion.Descripcion = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objDatosCancelacion.Monto, 2)) + " ]"

                Case 2  '*********** BANCO  (NRO OPERACION)

                    objDatosCancelacion.Monto = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objDatosCancelacion.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)
                    objDatosCancelacion.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)

                    objDatosCancelacion.MonedaSimboloDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objDatosCancelacion.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objDatosCancelacion.Monto, objDatosCancelacion.IdMoneda, objDatosCancelacion.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))

                    objDatosCancelacion.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objDatosCancelacion.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)

                    objDatosCancelacion.Descripcion = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objDatosCancelacion.Monto, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Cuenta: [ " + Me.cboCuentaBancaria.SelectedItem.ToString + " ]"
                    objDatosCancelacion.NroOperacion = (Me.txtNro_DC.Text)

                Case 3, 7  '************** BANCO CHEQUE y por ahora CHEQUE EGRESO

                    objDatosCancelacion.Monto = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objDatosCancelacion.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    objDatosCancelacion.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objDatosCancelacion.MonedaSimboloDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objDatosCancelacion.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objDatosCancelacion.Monto, objDatosCancelacion.IdMoneda, objDatosCancelacion.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))

                    objDatosCancelacion.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    'Se agrego la sgte linea
                    objDatosCancelacion.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)
                    Dim fecha As String = ""
                    Try
                        objDatosCancelacion.FechaACobrar = CDate(Me.txtFechaACobrar.Text)
                        fecha = Me.txtFechaACobrar.Text
                    Catch ex As Exception
                        objDatosCancelacion.FechaACobrar = Nothing
                        fecha = "---"
                    End Try
                    objDatosCancelacion.NumeroCheque = Me.txtNro_DC.Text
                    objDatosCancelacion.IdDocumentoRef = ChequeEgreso.Id

                    objDatosCancelacion.Descripcion = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + _
                    " ] Nro. Cheque: [ " + objDatosCancelacion.NumeroCheque.ToString + _
                    " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + _
                    " " + CStr(Math.Round(objDatosCancelacion.Monto, 2)) + _
                    " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + _
                    " ] Cuenta: [ " + Me.cboCuentaBancaria.SelectedItem.ToString + _
                    " ] Fecha a Cobrar: [ " + fecha + " ]"

                Case 4 '************* TARJETA

                    objDatosCancelacion.Monto = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objDatosCancelacion.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    objDatosCancelacion.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objDatosCancelacion.MonedaSimboloDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objDatosCancelacion.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objDatosCancelacion.Monto, objDatosCancelacion.IdMoneda, objDatosCancelacion.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objDatosCancelacion.NroOperacion = CStr(Me.txtNro_DC.Text.Trim)

                    objDatosCancelacion.IdBanco = CInt(Me.cboBanco.SelectedValue)
                    objDatosCancelacion.IdCuentaBancaria = CInt(Me.cboCuentaBancaria.SelectedValue)
                    'objDatosCancelacion.IdPost = CInt(Me.cboPost_DC.SelectedValue)
                    'objDatosCancelacion.IdTarjeta = CInt(Me.cboTarjeta_DC.SelectedValue)
                    objDatosCancelacion.Descripcion = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objDatosCancelacion.Monto, 2)) + " ] Banco: [ " + Me.cboBanco.SelectedItem.ToString + " ] Cuenta: [ " + Me.cboCuentaBancaria.SelectedValue.ToString + " ]"

                Case 5   '*********** NOTA DE CREDITO

                    '*************** NO SE UTILIZA

                Case 6   '************** COMP RETENCION

                    objDatosCancelacion.Monto = CDec(Me.txtMonto_DatoCancelacion.Text)
                    objDatosCancelacion.IdMoneda = CInt(Me.cboMoneda_DatoCancelacion.SelectedValue)

                    objDatosCancelacion.IdMonedaDestino = CInt(Me.cboMoneda.SelectedValue)
                    objDatosCancelacion.MonedaSimboloDestino = CStr(Me.cboMoneda.SelectedItem.ToString)
                    objDatosCancelacion.MontoEquivalenteDestino = (New Negocio.Util).CalcularValorxTipoCambio(objDatosCancelacion.Monto, objDatosCancelacion.IdMoneda, objDatosCancelacion.IdMonedaDestino, "E", CDate(Me.txtFechaEmision.Text))
                    objDatosCancelacion.NroOperacion = CStr(Me.txtNro_DC.Text.Trim)

                    objDatosCancelacion.Descripcion = " Medio Pago: [ " + Me.cboMedioPago.SelectedItem.ToString + " ] Monto: [ " + Me.cboMoneda_DatoCancelacion.SelectedItem.ToString + " " + CStr(Math.Round(objDatosCancelacion.Monto, 2)) + " ] "
                    'Case 7  '*************** CHEQUE EGRESO


            End Select

            Me.ListaCancelacion.Add(objDatosCancelacion)
            'setListaCancelacion(Me.ListaCancelacion)

            Me.GV_Cancelacion.DataSource = Me.ListaCancelacion
            Me.GV_Cancelacion.DataBind()

            Me.txtMonto_DatoCancelacion.Text = "0"
            Me.txtNro_DC.Text = ""
            Me.txtFechaACobrar.Text = ""

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Protected Sub GV_Cancelacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_Cancelacion.SelectedIndexChanged
        quitarDatoCancelacion()
    End Sub
    Private Sub quitarDatoCancelacion()
        Try
            Me.ListaCancelacion.RemoveAt(Me.GV_Cancelacion.SelectedRow.RowIndex)

            Me.GV_Cancelacion.DataSource = Me.ListaCancelacion
            Me.GV_Cancelacion.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularDatosCancelacion(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region

#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = IdPersona
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaInicio_BA.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaFin_BA.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(_IDTIPODOCUMENTO, IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            If (Me.GV_BusquedaAvanzado.Rows.Count <= 0) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", "  onCapa('capaBusquedaAvanzado');  alert('No se hallaron registros.');    ", True)
            Else
                objScript.onCapa(Me, "capaBusquedaAvanzado")
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            '************  cargarDocumentoNotaCredito(0, 0, )
            buscarDocumento(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region


#Region "****************Buscar Personas Mantenimiento"
    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            Me.GV_Deudas.DataSource = Nothing
            Me.GV_Deudas.DataBind()

            Me.GV_Cancelacion.DataSource = Nothing
            Me.GV_Cancelacion.DataBind()

            Me.ListaCancelacion = New List(Of Entidades.DatosCancelacion)
            'setListaCancelacion(Me.ListaCancelacion)

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   offCapa('capaPersona');    calcularDatosCancelacion();    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
        If (objPersonaView IsNot Nothing) Then

            Me.txtDescripcionPersona.Text = objPersonaView.Descripcion
            Me.txtDNI.Text = objPersonaView.Dni
            Me.txtRUC.Text = objPersonaView.Ruc
            Me.IdPersona = objPersonaView.IdPersona
            objScript.offCapa(Me, "capaPersona")

        End If
    End Sub
    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)

        listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", CInt(cboRol.SelectedValue))  '******************** CLIENTE

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
#Region "Busqueda de Cheques"

    Protected Sub btnSeleccionarCheque_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSeleccionarCheque.Click
        objScript.onCapa(Me, "capaCheques")
        ListaCheques = (New Negocio.DocumentoCheque).SelectxIdBeneficiarioxEstadoCancelacion_Out_DatosCancelacion(IdPersona, False)
        ListaCheques = filtrarListaCheques(ListaCheques, ListaCancelacion)
        GV_Cheques.DataSource = ListaCheques
        GV_Cheques.DataBind()

    End Sub
    Public Function filtrarListaCheques(ByVal LCheques As List(Of Entidades.DocumentoCheque), _
                                   ByVal LDatosCancel As List(Of Entidades.DatosCancelacion)) _
                                   As List(Of Entidades.DocumentoCheque)

        If LDatosCancel IsNot Nothing And LDatosCancel.Count > 0 Then
            Dim j As Integer
            Dim obj As Entidades.DocumentoCheque

            For j = 0 To LDatosCancel.Count - 1
                For Each obj In LCheques
                    If obj.Id = LDatosCancel(j).IdDocumentoRef Then
                        LCheques.Remove(obj)
                        Exit For
                    End If
                Next
                If LCheques.Count = 0 Then
                    Exit For
                End If
            Next
        End If
        Return LCheques
    End Function
    Protected Sub lbtnSeleccionarCheque_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ind As Integer 'indice de la lista correspondiente al registro a modificar

            'Se obtiene el Id del Registro a editar 
            'ini
            Dim lbtn As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lbtn.NamingContainer, GridViewRow)
            IdCheque = CInt(CType(fila.Cells(0).FindControl("hddId"), HiddenField).Value.Trim())
            'fin

            'se obtiene el indice de la lista, para asi obtener la entidad a editar 
            ind = ListaChequesGetIndiceEntidadxId(IdCheque)
            ChequeEgreso = ListaCheques(ind).getClone
            'se carga los controles
            'ini
            With ChequeEgreso

                Me.txtNro_DC.Text = .ChequeNumero
                Me.txtMonto_DatoCancelacion.Text = .DescMonto
                Me.cboMoneda_DatoCancelacion.SelectedValue = CStr(.IdMoneda)
                Me.cboBanco.SelectedValue = CStr(.SerieCheque.IdBanco)
                actualizarControles_BANCOS()
                Me.cboCuentaBancaria.SelectedValue = CStr(.SerieCheque.IdCuentaBancaria)
                Me.txtFechaACobrar.Text = .DescFechaACobrar

                'Me.txtMontoCheque.Text = .DescMonto
            End With
            'fin

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Function ListaChequesGetIndiceEntidadxId(ByVal id As Integer) As Integer
        For i As Integer = 0 To ListaCheques.Count - 1
            If ListaCheques(i).Id = id Then
                Return i
            End If
        Next
        Return -1
    End Function
#End Region
End Class