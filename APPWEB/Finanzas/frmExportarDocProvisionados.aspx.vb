﻿Imports Entidades
Imports Negocio
Imports general.librerias.serializador
Public Class frmExportarDocProvisionados
    Inherits System.Web.UI.Page
    Private objScript As ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim flag As String = Request.QueryString("flag")
        Dim txtFechaInicio As String = Request.QueryString("fechaInicio")
        Dim txtFechaFinal As String = Request.QueryString("FechaFinal")
        Dim ddlEmpresa As String = Request.QueryString("empresa")
        Dim txtPeriodo As String = Request.QueryString("periodo")
        Select Case flag
            Case "L"
                Dim lista As New List(Of be_provision)
                Dim rpta As String = ""
                Try
                    lista = (New bl_SolucontPase).importarDocProvisionados(txtFechaInicio, txtFechaFinal, ddlEmpresa, txtPeriodo, "038")
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(rpta)
                    Response.End()
                Else
                    Response.Write(rpta)
                    Response.End()
                End If
            Case "I"
                Dim lista2 As New List(Of be_provision)
                Dim rpta2 As String = ""
                Try
                    lista2 = (New bl_SolucontPase).listaDocProvisionadosCorrectamente(txtFechaInicio, txtFechaFinal, txtPeriodo)
                Catch
                    lista2 = Nothing
                    Response.Write(rpta2)
                    Response.End()
                End Try
                If lista2.Count > 0 Then
                    rpta2 = (New objeto).SerializarLista(lista2, "|", "▼", False, "", False)
                    Response.Write(rpta2)
                    Response.End()
                Else
                    Response.Write(rpta2)
                    Response.End()
                End If
        End Select

        Me.txtPeriodo.Text = Date.Today.Year.ToString()
    End Sub


End Class