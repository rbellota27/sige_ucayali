﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Linq
Imports System.Configuration



Partial Public Class FrmMantMovBanco
    Inherits System.Web.UI.Page

    Private objCbo As New Combo
    Private objScript As New ScriptManagerClass
    Private objNegMovBancoView As New Negocio.MovBancoView
    Private ListMovBancoView As New List(Of Entidades.MovBancoView)
    Private ListCuentaBancaria As New List(Of Entidades.CuentaBancaria)
    Private p As New Entidades.MovBancoView
    Private modo As New modo_menu
    Private Util As New Util
    Private fechaMovInicial, fechaMovFinal As New Date
    Private permisoEstadoAprob As New Boolean
    Private ListaTCB As New List(Of Entidades.TipoConceptoBanco)
    Private ListaDocRef As New List(Of Entidades.Anexo_MovBanco)
    Private Const Const_IdEmpresa As Integer = 1 'Sanicenter
    Private Const Const_IdArea As Integer = 21 'Finanzas
    Private ListaExcelImportacion As List(Of Entidades.MovBancoView)
#Region "Cadena Conx"
    Private con As New SqlConnection("Data Source=192.168.2.220;Initial Catalog=SIGE_SANISERVER;User ID=sa;pwd=Sani1234")
#End Region


    Private Property IdMovBanco() As Integer
        Get
            Return CInt(ViewState("IdMovBanco"))
        End Get
        Set(ByVal value As Integer)
            ViewState.Remove("IdMovBanco")
            ViewState.Add("IdMovBanco", value)
        End Set
    End Property
    Private Function getListaExcelImportacion() As List(Of Entidades.MovBancoView)
        Return CType(Session.Item("ListaExcelImportacion"), List(Of Entidades.MovBancoView))
    End Function
    Private Sub setListaExcelImportacion(ByVal lista As List(Of Entidades.MovBancoView))
        Session.Remove("ListaExcelImportacion")
        Session.Add("ListaExcelImportacion", lista)
    End Sub

    Public Property ListMovBanco_Grilla() As List(Of Entidades.MovBancoView)
        Get
            Return CType(Session("ListMovBanco_Grilla"), List(Of Entidades.MovBancoView))
        End Get
        Set(ByVal value As List(Of Entidades.MovBancoView))
            Session.Remove("ListMovBanco_Grilla")
            Session.Add("ListMovBanco_Grilla", value)
        End Set
    End Property

    Enum modo_menu
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Insertar = 3
        Actualizar = 4
    End Enum

    Private Sub ConfigurarDatos()
        'If Session("ConfigurarDatos") IsNot Nothing Then
        '    hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        'End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ConfigurarDatos()
            ValidarPermisos()
            modo = modo_menu.Inicio
            inicializarFRM()
            HabilitarControles(modo)
            ocultaLBL(False)
        Else
            ' actualizar_atributos()
        End If
    End Sub
    Private Sub ValidarPermisos()
        permisoEstadoAprob = False

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {109})

        If listaPermisos(0) > 0 Then
            permisoEstadoAprob = True
        Else
            permisoEstadoAprob = False
        End If
        ViewState("permisoEstadoAprob") = permisoEstadoAprob

    End Sub
    Private Sub inicializarFRM()
        Dim objScript As New ScriptManagerClass
        Try

            cargar_controles()
            cargar_recargar_datos()
            'cargar_dgv()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub ObtenerDatosparaRegistro()
        Try
            'Dim i As Integer

            ListaDocRef = CType(Session.Item("ListaDocRef"), List(Of Entidades.Anexo_MovBanco))
            ListaTCB = CType(Session.Item("ListaTCB"), List(Of Entidades.TipoConceptoBanco))
            permisoEstadoAprob = CType(ViewState("permisoEstadoAprob"), Boolean)
            ListMovBancoView = CType(Session.Item("ListMovBancoView"), List(Of Entidades.MovBancoView))
            ListMovBanco_Grilla = CType(Session.Item("ListMovBanco_Grilla"), List(Of Entidades.MovBancoView))
            modo = CType(Session.Item("modo"), modo_menu)
            p.IdPersonaRef = CType(Session.Item("IdPersonaRef"), Integer)

            p.IdMovBanco = CType(Session.Item("IdRegistroFila"), Integer)
            p.IdBanco = CInt(Me.cboBanco.SelectedValue)
            p.IdBanco2 = CInt(Me.cboBanco2.SelectedValue)
            p.IdCuentaBancaria = CInt(IIf(CStr(Me.cboCuentaBancaria.SelectedValue) <> "", Me.cboCuentaBancaria.SelectedValue, 0))
            p.IdCuentaBancaria2 = CInt(IIf(CStr(Me.cboCuentaBancaria2.SelectedValue) <> "", Me.cboCuentaBancaria2.SelectedValue, 0))

            p.NroOperacion = Util.decodificarTexto(txtNroOperacion.Text)
            p.Observacion = Util.decodificarTexto(txtObservacion.Text)
            If txtMonto.Text <> "" Then
                p.Monto = CDec(Util.decodificarTexto(txtMonto.Text))
            Else
                p.Monto = 0D
            End If

            p.EstadoAprobacion = chkEstadoAprobacion.Checked
            Select Case CInt(rbtlEstadoMov.SelectedValue)
                Case 2
                    p.EstadoMov = Nothing
                Case 1
                    p.EstadoMov = True
                Case 0
                    p.EstadoMov = False
            End Select

            p.IdTipoConceptoBanco = CInt(Me.cboTipoConceptoBanco.SelectedValue)
            p.IdConceptoMovBanco = CInt(IIf(CStr(Me.cboConceptoMovBanco.SelectedValue) <> "", Me.cboConceptoMovBanco.SelectedValue, 0))

            'Revisar ini

            If p.IdTipoConceptoBanco = 8 Then
                p.Factor = -1
            ElseIf p.IdTipoConceptoBanco = 9 Then
                p.Factor = 1
            Else
                p.Factor = 0
            End If
              
            'Select Case CInt(cboTipoMovimiento.SelectedValue)
            '    Case 3
            '        p.Factor = Nothing
            '    Case 2
            '        p.Factor = -1
            '    Case 1
            '        p.Factor = 1
            '    Case Else
            'End Select
            'Revisar fin


            p.IdUsuario = CType(Session.Item("IdUsuario"), Integer)
            'p.IdSupervisor = CType(Session.Item("IdSupervisor"), Integer)

            p.IdTipoTarjeta = CInt(Me.cboTipoTarjeta.SelectedValue)
            p.IdTarjeta = CInt(IIf(CStr(Me.cboTarjeta.SelectedValue) <> "", Me.cboTarjeta.SelectedValue, 0))

            p.IdTienda = CInt(Me.cboTienda.SelectedValue)

            p.IdCaja = CInt(IIf(CStr(Me.cboCaja.SelectedValue) <> "", Me.cboCaja.SelectedValue, 0))

            p.IdPost = CInt(Me.cboPost.SelectedValue)

            p.IdPersonaRef = CType(Session.Item("IdPersonaRef"), Integer)

            If txtFechaMov.Text <> "" Then
                p.FechaMov = CDate(txtFechaMov.Text)
            Else
                p.FechaMov = Nothing
            End If

            p.FechaRegistro = Now.Date()
            'Cambiar ini
            p.FechaAprobacion = Nothing
            'Cambiar fin

            If txtFechaMovInicial.Text <> "" Then
                fechaMovInicial = CDate(txtFechaMovInicial.Text)
            Else
                fechaMovInicial = Nothing
            End If

            If txtFechaMovFinal.Text <> "" Then
                fechaMovFinal = CDate(txtFechaMovFinal.Text)
            Else
                fechaMovFinal = Nothing
            End If

            If (txtReferencia1Mov.Text <> "") Then
                p.Referencia1x = txtReferencia1Mov.Text
            Else
                p.Referencia1x = Nothing
            End If
            If (txtReferencia2Mov.Text <> "") Then
                p.Referencia2x = txtReferencia2Mov.Text
            Else
                p.Referencia2x = Nothing
            End If


        Catch ex As Exception
        End Try
    End Sub
    Protected Sub cargar_recargar_datos()
        'ListMovBancoView = objNegMovBancoView.SelectAll()
        Session.Remove("ListMovBancoView")
        Session.Add("ListMovBancoView", ListMovBancoView)

        Session.Add("ListMovBanco_Grilla", ListMovBanco_Grilla)
        Session.Remove("ListMovBanco_Grilla")

        Session.Add("ListaExcelImportacion", ListaExcelImportacion)
        Session.Remove("ListaExcelImportacion")

        Session.Remove("ListaTCB")
        Session.Add("ListaTCB", ListaTCB)
    End Sub
    Sub cargar_recargar_modo(ByVal x As modo_menu)
        modo = x
        Session.Remove("modo")
        Session.Add("modo", modo)
    End Sub
    Sub cargar_recargar_Id(ByVal x As Integer)  'almacena el Id del Registro a editar
        p.IdMovBanco = x
        Session.Remove("IdRegistroFila")
        Session.Add("IdRegistroFila", p.IdMovBanco)
    End Sub
    Sub cargar_recargar_IdPersonaRef(ByVal x As Integer)  'almacena el Id del Registro a editar
        p.IdPersonaRef = x
        Session.Remove("IdPersonaRef")
        Session.Add("IdPersonaRef", p.IdPersonaRef)
    End Sub
    Sub cargar_recargar_permisoEstadoAprob(ByVal x As Boolean)
        permisoEstadoAprob = x
        Session.Remove("permisoEstadoAprob")
        Session.Add("permisoEstadoAprob", permisoEstadoAprob)
    End Sub
    Sub cargar_recargar_ListaDocRef(ByVal x As List(Of Entidades.Anexo_MovBanco))
        ListaDocRef = x
        Session.Remove("ListaDocRef")
        Session.Add("ListaDocRef", ListaDocRef)

    End Sub
    Private Sub cargar_controles()
        objCbo.LlenarCboBanco(cboBanco, True)
        objCbo.LlenarCboCuentaBancaria(cboCuentaBancaria, CInt(cboBanco.SelectedValue), True)

        objCbo.LlenarCboBanco(cboBanco2, True)
        objCbo.LlenarCboCuentaBancaria(cboCuentaBancaria2, CInt(cboBanco2.SelectedValue), True)

        objCbo.LlenarCboTipoConceptoBanco(cboTipoConceptoBanco, True)
        objCbo.LlenarCboConceptoMovBancoxIdTipoConceptoBanco(cboConceptoMovBanco, CInt(cboTipoConceptoBanco.SelectedValue), True)

        'objCbo.LlenarCboTipoMovimiento(cboTipoMovimiento, True)

        objCbo.LlenarCboTipoTarjeta(cboTipoTarjeta, True)
        objCbo.LlenarCboTarjetaxIdTipoTarjeta(cboTarjeta, CInt(cboTipoTarjeta.SelectedValue), True)

        objCbo.LLenarCboTienda(cboTienda, True) 'Verificar si esta bien, o se cambia la funcion por la ottra que incluye el ID Empresa
        objCbo.LlenarCboCajaxIdTienda(cboCaja, CInt(cboTienda.SelectedValue), True)

        objCbo.LlenarCboPostxIdCuentaBancaria(cboPost, CInt(cboCuentaBancaria.SelectedValue), True)

        'Me.txtFechaMov.Text = (New Negocio.FechaActual).SelectFechaActual.ToString
        Me.txtFechaMov.Text = Now.Date.ToString
        Me.txtFechaMovInicial.Text = Me.txtFechaMov.Text
        Me.txtFechaMovFinal.Text = Me.txtFechaMov.Text

        txtFechaInicio_DocRef.Text = Me.txtFechaMov.Text
        txtFechaFin_DocRef.Text = Me.txtFechaMov.Text

        ListaTCB = (New Negocio.TipoConceptoBanco).SelectAllActivo()
        Panel_Importar.Visible = False
        'Analizar ¿IdPropietario = IdUsuario? y mas.
        objCbo.LlenarCboTipoDocumento(cboTipoDocumento, Const_IdEmpresa, Const_IdArea)
        'objCbo.LlenarCboTipoDocumentoIndep(cboTipoDocumento)

        'ListCuentaBancaria=(new Negocio.CuentaBancaria).

    End Sub

    'Protected Sub cargar_dgv()
    '    actualizar_atributos()
    '    If modo = modo_menu.Inicio Then
    '        ListMovBanco_Grilla = objNegMovBancoView.FiltrarListaxRangoFechas(p, ListMovBancoView, fechaMovInicial, fechaMovFinal)
    '    Else
    '        ListMovBanco_Grilla = objNegMovBancoView.FiltrarLista(p, ListMovBancoView)
    '    End If
    '    dgvMovBanco.DataSource = ListMovBanco_Grilla
    '    dgvMovBanco.DataBind()

    '    GV_EdicionDeImportacion.DataSource = Nothing
    '    GV_EdicionDeImportacion.DataBind()
    '    GV_DataDeExcel.DataSource = Nothing
    '    GV_DataDeExcel.DataBind()
    'End Sub

    Public Sub limpiarCtrl()

        Me.lblSaldoContable.Text = ""
        Me.lblSaldoDisponible.Text = ""

        Me.cboBanco.SelectedIndex = 0
        Me.cboCuentaBancaria.SelectedIndex = 0

        'Me.cboTipoMovimiento.SelectedIndex = 0

        Me.cboTipoConceptoBanco.SelectedIndex = 0
        Me.cboConceptoMovBanco.SelectedIndex = 0

        Me.cboTipoTarjeta.SelectedIndex = 0
        Me.cboTarjeta.SelectedIndex = 0

        Me.cboTienda.SelectedIndex = 0
        Me.cboCaja.SelectedIndex = 0

        Me.cboPost.SelectedIndex = 0

        Me.txtNroOperacion.Text = ""
        Me.txtObservacion.Text = ""
        Me.txtMonto.Text = "0.0"
        'Me.txtFechaMov.Text = (New Negocio.FechaActual).SelectFechaActual.ToString
        Me.txtFechaMov.Text = Now.Date.ToString
        Me.txtFechaMovInicial.Text = Me.txtFechaMov.Text
        Me.txtFechaMovFinal.Text = Me.txtFechaMov.Text

        Me.rbtlEstadoMov.SelectedValue = "1" 'Activos
        Me.chkEstadoAprobacion.Checked = False
        Me.GV_DataDeExcel.DataSource = Nothing
        Me.GV_DataDeExcel.DataBind()
        GV_EdicionDeImportacion.DataSource = Nothing
        GV_EdicionDeImportacion.DataBind()


        Me.dgvMovBanco.DataSource = Nothing
        Me.dgvMovBanco.DataBind()

        Me.txtReferencia1Mov.Text = ""
        Me.txtReferencia2Mov.Text = ""
        'Me.txtNombrePersonaRef.Text = ""
        'txtDNI.Text = ""
        'txtRUC.Text = ""

    End Sub
    Private Sub ocultaLBL(ByVal t As Boolean)
        Me.lblSalContDesc.Visible = t
        Me.lblSalDisponibleDescp.Visible = t
    End Sub

    'Private Sub cargar_entidad_en_controles(ByVal x As Entidades.MovBancoView)
    '    cboBanco.SelectedValue = CStr(x.IdBanco)
    '    cboTarjeta.SelectedValue = CStr(x.IdTarjeta)
    '    txtNroOperacion.Text = x.NroOperacion
    '    rbtlEstadoMov.SelectedValue = x.EstadoMov
    'End Sub
    Public Sub HabilitarControles(ByVal x As modo_menu)
        Select Case x
            Case modo_menu.Inicio

                Me.btnNuevo.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscar.Visible = True
                Me.PanelMovBancarios.Visible = True
                Me.txtReferencia1Mov.Text = ""
                Me.txtReferencia2Mov.Text = ""

                Me.txtReferencia1Mov.Visible = False
                Me.txtReferencia2Mov.Visible = False

                Me.lblReferencia1Mov.Visible = False
                Me.lblReferencia2Mov.Visible = False
                Me.cboBanco.Enabled = True
                Me.cboCuentaBancaria.Enabled = True
                'Me.cboTarjeta.Enabled = True
                'Me.txtNroOperacion.Visible = True
                'Me.txtmonto.Visible = True
                Me.rbtlEstadoMov.Items(0).Enabled = True

                'Me.lblBanco.Visible = True
                'Me.lblTarjeta.Visible = True
                'Me.lblNroOperacion.Visible = True
                'Me.lblEstadoMov.Visible = True

                Me.lblMonto.Visible = False
                Me.lblMoneda.Visible = False
                Me.txtMonto.Visible = False

                Me.lblFechaMov.Visible = False
                Me.txtFechaMov.Visible = False
                Me.lblFechaMovInicial.Visible = True
                Me.txtFechaMovInicial.Visible = True
                Me.lblFechaMovFinal.Visible = True
                Me.txtFechaMovFinal.Visible = True

                'txtFechaMovInicial.Text = (New Negocio.FechaActual).SelectFechaActual.ToString
                txtFechaMovInicial.Text = Now.Date.ToString
                txtFechaMovFinal.Text = txtFechaMovInicial.Text
                Me.Panel_Importar.Visible = False
                Me.dgvMovBanco.Enabled = True
                Me.dgvMovBanco.DataSource = Nothing
                Me.dgvMovBanco.DataBind()

                Me.GV_DataDeExcel.DataSource = Nothing
                Me.GV_DataDeExcel.DataBind()
                'pnlMovBancoAdd_Cabecera.Visible = False
                'pnlMovBancoAdd.Visible = False
                Panel_DatosAdicionales.Visible = False
                'pnlPersonaRef_Cabecera.Visible = False
                'pnlPersonaRef.Visible = False
                Panel_PersonaRef.Visible = False
                Panel_DocumentoRef.Visible = False
                If permisoEstadoAprob Then
                    Me.chkEstadoAprobacion.Enabled = False
                End If

                cboTipoConceptoBanco.Enabled = True
            Case modo_menu.Nuevo

                Me.lblReferencia1Mov.Visible = True
                Me.lblReferencia2Mov.Visible = True
                Me.txtReferencia1Mov.Visible = True
                Me.txtReferencia2Mov.Visible = True
                Me.Panel_Importar.Visible = False
                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False
                Me.PanelMovBancarios.Visible = True
                Me.cboBanco.Enabled = True
                Me.cboCuentaBancaria.Enabled = True
                'Me.cboTarjeta.Enabled = True
                Me.btnVerParaEditar.Visible = False
                'Me.txtNroOperacion.Visible = True
                'Me.txtmonto.Visible = True
                Me.rbtlEstadoMov.Items(0).Enabled = False
                Me.GV_DataDeExcel.DataSource = Nothing
                Me.GV_DataDeExcel.DataBind()
                'Me.lblTarjeta.Visible = True
                'Me.lblBanco.Visible = True
                'Me.lblNroOperacion.Visible = True
                'Me.lblEstadoMov.Visible = True

                Me.lblMonto.Visible = True
                Me.lblMoneda.Visible = True
                Me.txtMonto.Visible = True

                Me.lblFechaMov.Visible = True
                Me.txtFechaMov.Visible = True
                Me.lblFechaMovInicial.Visible = False
                Me.txtFechaMovInicial.Visible = False
                Me.lblFechaMovFinal.Visible = False
                Me.txtFechaMovFinal.Visible = False

                txtFechaMovInicial.Text = ""
                txtFechaMovFinal.Text = ""
                Me.dgvMovBanco.DataSource = Nothing
                Me.dgvMovBanco.DataBind()

                Me.dgvMovBanco.Enabled = False

                'pnlMovBancoAdd_Cabecera.Visible = True
                'pnlMovBancoAdd.Visible = True
                Panel_DatosAdicionales.Visible = True

                'pnlPersonaRef_Cabecera.Visible = True
                'pnlPersonaRef.Visible = True
                Panel_PersonaRef.Visible = True
                Panel_DocumentoRef.Visible = False
                If permisoEstadoAprob Then
                    Me.chkEstadoAprobacion.Enabled = True
                End If

                cboTipoConceptoBanco.Enabled = True

            Case modo_menu.Editar

                Me.btnNuevo.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnBuscar.Visible = False
                Me.txtReferencia1Mov.Visible = True
                Me.txtReferencia2Mov.Visible = True
                Me.lblReferencia1Mov.Visible = True
                Me.lblReferencia2Mov.Visible = True
                Me.cboBanco.Enabled = False
                'Me.cboTarjeta.Enabled = False
                Me.cboCuentaBancaria.Enabled = False
                'Me.txtNroOperacion.Visible = True
                'Me.txtmonto.Visible = True
                Me.rbtlEstadoMov.Items(0).Enabled = False

                'Me.lblBanco.Visible = True
                'Me.lblTarjeta.Visible = True
                'Me.lblNroOperacion.Visible = True
                'Me.lblEstadoMov.Visible = True

                Me.lblMonto.Visible = True
                Me.lblMoneda.Visible = True
                Me.txtMonto.Visible = True

                Me.lblFechaMov.Visible = True
                Me.txtFechaMov.Visible = True
                Me.lblFechaMovInicial.Visible = False
                Me.txtFechaMovInicial.Visible = False
                Me.lblFechaMovFinal.Visible = False
                Me.txtFechaMovFinal.Visible = False

                txtFechaMovInicial.Text = ""
                txtFechaMovFinal.Text = ""

                Me.dgvMovBanco.Enabled = False
                Me.GV_DataDeExcel.DataSource = Nothing
                Me.GV_DataDeExcel.DataBind()
                'pnlMovBancoAdd_Cabecera.Visible = True
                'pnlMovBancoAdd.Visible = True
                Panel_DatosAdicionales.Visible = True

                'pnlPersonaRef_Cabecera.Visible = True
                'pnlPersonaRef.Visible = True
                Panel_PersonaRef.Visible = True
                Panel_DocumentoRef.Visible = True
                If permisoEstadoAprob Then
                    Me.chkEstadoAprobacion.Enabled = True
                End If

                cboTipoConceptoBanco.Enabled = True

            Case modo_menu.Actualizar
            Case modo_menu.Insertar

        End Select
    End Sub
    Protected Function ListaGetIndiceEntidadxId(ByVal lista As List(Of Entidades.MovBancoView), ByVal id As Integer) As Integer
        For i As Integer = 0 To lista.Count - 1
            If lista(i).IdMovBanco = id Then
                Return i
            End If
        Next
        Return -1
    End Function


#Region "Eventos"
#Region "Botones"

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        If CDate(txtFechaMovInicial.Text) <= CDate(txtFechaMovFinal.Text) Then
            'cargar_dgv()
            BuscarMovBancariosxFechas()
        Else
            objScript.mostrarMsjAlerta(Me, "La fecha inicial es mayor que la fecha final")
        End If
    End Sub
    Private Sub BuscarMovBancariosxFechas()
        Dim ListaMovBanco As New List(Of Entidades.MovBancoView)
        Dim FechaInicial As Date = Nothing
        Dim FechaFinal As Date = Nothing
        Dim NroOperacion As String = Nothing
        Dim IdBanco As Integer = Nothing
        Dim IdCuentaBancaria As Integer = Nothing

        FechaInicial = CDate(txtFechaMovInicial.Text)
        FechaFinal = CDate(txtFechaMovFinal.Text)

        If (txtNroOperacion.Text = "") Then
            NroOperacion = Nothing
        Else
            NroOperacion = txtNroOperacion.Text
        End If


        If (cboBanco.SelectedValue <> "0") Then
            IdBanco = CInt(cboBanco.SelectedValue)
        Else
            IdBanco = Nothing
        End If

        If (cboCuentaBancaria.SelectedValue <> "0") Then
            IdCuentaBancaria = CInt(cboCuentaBancaria.SelectedValue)
        Else
            IdCuentaBancaria = Nothing
        End If


        ListaMovBanco = New Negocio.MovBancoView().SelectMovBancosxFechas(FechaInicial, FechaFinal, NroOperacion, IdBanco, IdCuentaBancaria)
        dgvMovBanco.DataSource = ListaMovBanco
        dgvMovBanco.DataBind()

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        cargar_recargar_modo(modo_menu.Nuevo)
        HabilitarControles(modo)
        'limpiarCtrl()  'se deben dejar los datos escritos
        'cargar_dgv()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        cargar_recargar_modo(modo_menu.Inicio)
        cargar_recargar_Id(0)   'Limpia el ID
        HabilitarControles(modo_menu.Inicio)
        limpiarCtrl()
        'cargar_dgv()
        ocultaLBL(False)
    End Sub
    Private Sub preparar_persistencia_entidad()
        'Los combos manejan valor 0, pero los sp manejan nulos, 
        'asi que hay que enviarles los valores correctos
        If p.IdUsuario = 0 Then
            p.IdUsuario = Nothing
        End If
        If p.IdSupervisor = 0 Then
            p.IdSupervisor = Nothing
        End If
        If p.IdTarjeta = 0 Then
            p.IdTarjeta = Nothing
        End If

        If p.IdPersonaRef = 0 Then
            p.IdPersonaRef = Nothing
        End If
        If p.IdCaja = 0 Then
            p.IdCaja = Nothing
        End If
        If p.IdTienda = 0 Then
            p.IdTienda = Nothing
        End If
        If p.IdPost = 0 Then
            p.IdPost = Nothing
        End If

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click

        Try

            'opcional ini
            cargar_recargar_datos() 'Los datos disponibles se actualizan antes de realizar la validacion
            'opcional fin
            preparar_persistencia_entidad()
            ObtenerDatosparaRegistro()

            Select Case modo
                Case modo_menu.Nuevo
                    'If objNegMovBancoView.ValidarEntidad(p, ListMovBancoView) Then
                    'p.FechaRegistro = Now--Este se hace en la bd

                    'If objNegMovBancoView.InsertT(p) Then
                    If objNegMovBancoView.InsertAll(p, ListaDocRef) Then
                        objScript.mostrarMsjAlerta(Me, "Se Guardó Correctamente")
                        Me.dgvMovBanco.DataSource = Nothing
                        Me.dgvMovBanco.DataBind()
                    End If
                    'Else
                    '    objScript.mostrarMsjAlerta(Me, "Ya existe un registro con ese Banco, Cuenta Bancaria y Tipo de Tarjeta")
                    'End If
                Case modo_menu.Editar
                    If objNegMovBancoView.UpdateAll(p, ListaDocRef) Then
                        objScript.mostrarMsjAlerta(Me, "Se Actualizó Correctamente")
                        Me.dgvMovBanco.DataSource = Nothing
                        Me.dgvMovBanco.DataBind()
                    End If
            End Select
            ''cargar_dgv()    
            'Muestra el registro ingresado

            cargar_recargar_Id(0)   'Limpia el ID
            cargar_recargar_datos()
            cargar_recargar_modo(modo_menu.Inicio)
            HabilitarControles(modo)

            limpiarCtrl()   'Limpia los controles
            ocultaLBL(False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
#Region "Carga de Combos"

    Protected Sub cboBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboBanco.SelectedIndexChanged
        objCbo.LlenarCboCuentaBancaria(cboCuentaBancaria, CInt(cboBanco.SelectedValue), True)
    End Sub
    Protected Sub cboBanco2_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboBanco2.SelectedIndexChanged
        objCbo.LlenarCboCuentaBancaria(cboCuentaBancaria2, CInt(cboBanco2.SelectedValue), True)
    End Sub

    Protected Sub cboTipoConceptoBanco_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTipoConceptoBanco.SelectedIndexChanged
        objCbo.LlenarCboConceptoMovBancoxIdTipoConceptoBanco(cboConceptoMovBanco, CInt(cboTipoConceptoBanco.SelectedValue), True)
    End Sub

    Protected Sub cboTipoTarjeta_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTipoTarjeta.SelectedIndexChanged
        objCbo.LlenarCboTarjetaxIdTipoTarjeta(cboTarjeta, CInt(cboTipoTarjeta.SelectedValue), True)
    End Sub

    Protected Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTienda.SelectedIndexChanged
        objCbo.LlenarCboCajaxIdTienda(cboCaja, CInt(cboTienda.SelectedValue), True)
    End Sub

    Protected Sub cboCuentaBancaria_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboCuentaBancaria.SelectedIndexChanged
        If modo = modo_menu.Nuevo Or modo = modo_menu.Editar Then
            Dim i As New Integer
            i = cboCuentaBancaria.SelectedItem.Text.IndexOf(" "c)
            If i > -1 Or CInt(cboCuentaBancaria.SelectedValue) <> 0 Then
                Me.lblMoneda.Text = cboCuentaBancaria.SelectedItem.Text.Substring(0, i)
            Else
                Me.lblMoneda.Text = ""
            End If
            objCbo.LlenarCboPostxIdCuentaBancaria(cboPost, CInt(cboCuentaBancaria.SelectedValue), True)

        End If


    End Sub
#End Region
#End Region
#Region "Buscar Persona Referencia"

    Private Sub cargarDatosPersonaGrilla(ByVal grilla As GridView, ByVal texto As String, ByVal opcion As Integer, ByVal tipoMov As Integer)
        Try
            '********** opcion 0: descripcion
            '********** opcion 1: ruc
            '********** opcion 2: dni
            Dim index As Integer = 0
            Select Case tipoMov
                Case 0 '**************** INICIO
                    index = 0
                Case 1 '**************** Anterior
                    index = (CInt(txtPageIndex_Persona.Text) - 1) - 1
                Case 2 '**************** Posterior
                    index = (CInt(txtPageIndex_Persona.Text) - 1) + 1
                Case 3 '***************** IR
                    index = (CInt(txtPageIndexGO_Persona.Text) - 1)
            End Select

            Dim listaPV As List(Of Entidades.PersonaView) = (New Negocio.PersonaView).SelectActivoxParam_Paginado(texto, opcion, grilla.PageSize, index)

            If listaPV.Count <= 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            Else
                grilla.DataSource = listaPV
                grilla.DataBind()
                txtPageIndex_Persona.Text = CStr(index + 1)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarPersona_Grilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarPersona_Grilla.Click
        ViewState.Add("TextoBuscarPersona", txtBuscarPersona_Grilla.Text)
        ViewState.Add("OpcionBuscarPersona", Me.cmbFiltro_BuscarPersona.SelectedValue)
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, txtBuscarPersona_Grilla.Text, CInt(Me.cmbFiltro_BuscarPersona.SelectedValue), 0)
        'txtPageIndex_Persona.Text = "1"
        'txtPageIndexGo_Persona.Text = "1"
    End Sub
    Private Sub btnAnterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 1)
    End Sub
    Private Sub btnPosterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 2)
    End Sub
    Private Sub btnIr_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 3)
    End Sub

    Private Sub DGV_BuscarPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_BuscarPersona.SelectedIndexChanged
        p.IdPersonaRef = CInt(DGV_BuscarPersona.SelectedRow.Cells(1).Text)
        cargar_recargar_IdPersonaRef(CInt(p.IdPersonaRef))
        addPersona(CInt(p.IdPersonaRef))
    End Sub

    Private Sub addPersona(ByVal IdPersona As Integer)
        Try
            Dim objCliente As Entidades.PersonaView = (New Negocio.Cliente).SelectxId(IdPersona)
            Me.txtNombrePersonaRef.Text = objCliente.getNombreParaMostrar()
            Me.txtDNI.Text = objCliente.Dni
            Me.txtRUC.Text = objCliente.Ruc

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "addPersona_Venta();", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub LimpiarBuscarCliente()
        Me.txtNombrePersonaRef.Text = ""
        Me.txtDNI.Text = ""
        Me.txtRUC.Text = ""
    End Sub

#End Region
#Region "Documento Referencia"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click
        mostrarDocumentosRef_Find()
    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try
            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0
            IdPersona = CType(Session("IdPersonaRef"), Integer)
            'If (Me.hddIdPersona.Value.Trim.Length > 0 And IsNumeric(Me.hddIdPersona.Value)) Then
            '    If (CInt(Me.hddIdPersona.Value) > 0) Then
            '        IdPersona = CInt(Me.hddIdPersona.Value)
            '    End If
            'End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            'Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdPersona, CInt(Me.cboTipoDocumento.SelectedValue), serie, codigo, fechaInicio, fechafin, True)
            Dim lista As List(Of Entidades.DocumentoView) = _
            (New Negocio.Documento).Documento_Buscar(Const_IdEmpresa, 0, IdPersona, _
            CInt(Me.cboTipoDocumento.SelectedValue), serie, codigo, fechaInicio, fechafin, True)
            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Me.GV_DocumentosReferencia_Find.DataSource = Nothing
                Me.GV_DocumentosReferencia_Find.DataBind()
                'Throw New Exception("No se hallaron registros.")
                'objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub
    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If


    End Sub
    'Agrega un documento referencia
    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        'cargarDocumentoReferencia()

        Dim x As New Entidades.Anexo_MovBanco
        x.IdDocumentoRef = CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value)
        x.NroDocumento = CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblNroDocumento_Find"), Label).Text
        x.NomTipoDocumento = CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("lblNomTipoDocumento"), Label).Text
        x.IdMovBanco = p.IdMovBanco
        x.IdBanco = p.IdBanco
        x.IdCuentaBancaria = p.IdCuentaBancaria

        If ValidarDocumentoRef(x, ListaDocRef) Then
            ListaDocRef.Add(x)
            cargar_recargar_ListaDocRef(ListaDocRef)
            dgvDocRef.DataSource = ListaDocRef
            dgvDocRef.DataBind()
        Else
            objScript.mostrarMsjAlerta(Me, "Este Documento ya fue anexado.")
        End If
    End Sub

    Protected Function ValidarDocumentoRef(ByVal x As Entidades.Anexo_MovBanco, ByVal L As List(Of Entidades.Anexo_MovBanco)) As Boolean
        'Busca si ya se encuentra ese DocumentoRef en la lista
        For i As Integer = 0 To L.Count - 1
            If x.IdDocumentoRef = L(i).IdDocumentoRef Then
                Return False
            End If
        Next
        Return True
    End Function

    Protected Sub lbtnQuitarDocRef_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lbtnQuitarDocRef As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
        Dim fila As GridViewRow = CType(lbtnQuitarDocRef.NamingContainer, GridViewRow)
        Dim IdDocRef As Integer
        IdDocRef = CInt(CType(fila.FindControl("hddIdDocRef"), HiddenField).Value)

        For i As Integer = 0 To ListaDocRef.Count - 1
            If ListaDocRef(i).IdDocumentoRef = IdDocRef Then
                ListaDocRef.RemoveAt(i)
            End If
        Next
        cargar_recargar_ListaDocRef(ListaDocRef)
        dgvDocRef.DataSource = ListaDocRef
        dgvDocRef.DataBind()

    End Sub

#End Region
    'Protected Sub dgvMovBanco_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvMovBanco.RowDataBound
    '    'LinkButton Editar esta en un UpdatePanel y para que este no atrape su postback, se registra en el ScriptManager de la página Maestra
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow And e.Row.RowState = DataControlRowState.Normal Or _
    '        e.Row.RowState = DataControlRowState.Alternate Or e.Row.RowState = DataControlRowState.Selected Then
    '            Dim sm As ScriptManager
    '            sm = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
    '            sm.RegisterPostBackControl(DirectCast(e.Row.FindControl("lbtnEditar"), LinkButton))
    '        End If
    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub


#Region "Módulo Importacion de Excel hacia SQL"

    ''Importar...
    Protected Sub btnImportar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnImportar.Click
        Panel_Importar.Visible = True
        Me.PanelMovBancarios.Visible = False
        Me.btnImport.Visible = True
        GV_DataDeExcel.DataSource = Nothing
        GV_DataDeExcel.DataBind()

        GV_EdicionDeImportacion.DataSource = Nothing
        GV_EdicionDeImportacion.DataBind()
    End Sub

    Protected Sub btnImport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnImport.Click
        If (fileuploadExcel.HasFile = True And cboBanco2.SelectedValue <> "" And cboCuentaBancaria2.SelectedValue <> "") Then
            Try
                LlenarGrillaLinqServer()
                If (GV_DataDeExcel.Rows.Count > 0) Then
                    ''El primer método obtiene todos los valores de la grilla ya importada
                    obtenerListaExcelImportado()
                    RelacionarDatosImportados()

                    btnVerParaEditar.Visible = True
                    btnImport.Visible = False
                Else
                    objScript.mostrarMsjAlerta(Me, "No hay datos en el Excel para Importar.No procede la operación.")
                End If
            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message)
            End Try
        Else
            objScript.mostrarMsjAlerta(Me, "Seleccione un archivo excel para continuar con la Importación.")
        End If
    End Sub
    Private Function obtenerListaExcelImportado() As List(Of Entidades.MovBancoView)
        Dim lista As New List(Of Entidades.MovBancoView)
        For i As Integer = 0 To Me.GV_DataDeExcel.Rows.Count - 1

            Dim obj As New Entidades.MovBancoView
            obj.IdBanco2 = CInt(cboBanco2.SelectedValue)
            obj.IdCuentaBancaria2 = CInt(cboCuentaBancaria2.SelectedValue)

            With obj
                '.IdBanco2 = CInt(CType(Me.GV_DataDeExcel.Rows(i).FindControl("lblNroDocumento"), Label).Text)
                .Descripción_operación = CStr(CType(Me.GV_DataDeExcel.Rows(i).FindControl("lblDescripcionOP"), Label).Text)
                .Fecha = CDate(CType(Me.GV_DataDeExcel.Rows(i).FindControl("lblFechaOP"), Label).Text)
                '.FechaMov = CDate(CType(Me.GV_DataDeExcel.Rows(i).FindControl("lblFechaValutaOP"), Label).Text)
                .Monto = CDec(CType(Me.GV_DataDeExcel.Rows(i).FindControl("lblMontoOP"), Label).Text)
                '.Sucursal_agencia = CStr(CType(Me.GV_DataDeExcel.Rows(i).FindControl("lblSucursalOP"), Label).Text)
                .Operación_Número = CStr(CType(Me.GV_DataDeExcel.Rows(i).FindControl("lblOperacionNumero"), Label).Text)
                '.Usuario = CStr(CType(Me.GV_DataDeExcel.Rows(i).FindControl("lblUsuario"), Label).Text)
                .Referencia1 = CStr(CType(Me.GV_DataDeExcel.Rows(i).FindControl("lblReferenciaOP"), Label).Text)
                .Referencia2 = CStr(CType(Me.GV_DataDeExcel.Rows(i).FindControl("lblReferenciaOP2"), Label).Text)
            End With

            lista.Add(obj)
        Next

        ListaExcelImportacion = lista
        setListaExcelImportacion(Me.ListaExcelImportacion)
        Return lista
    End Function
    Private Sub RelacionarDatosImportados()

        Dim Identificador As Integer = New Negocio.MovBancoView().SelectUltimoIdentificadorExport()
        Dim IdentificadorResult As Integer = 0
        IdentificadorResult = Identificador + 1
        ListaExcelImportacion = getListaExcelImportacion()

        Dim lista As New List(Of Entidades.MovBancoView)
        For i As Integer = 0 To Me.ListaExcelImportacion.Count - 1
            Dim objMovBancoView As New Entidades.MovBancoView
            With objMovBancoView
                .NroOperacion = Me.ListaExcelImportacion(i).Operación_Número
                .FechaRegistro = Me.ListaExcelImportacion(i).Fecha
                .FechaMov = Me.ListaExcelImportacion(i).Fecha
                .Observacion = Me.ListaExcelImportacion(i).Descripción_operación.Replace("*", "")
                .IdBanco2 = Me.ListaExcelImportacion(i).IdBanco2
                .IdCuentaBancaria2 = Me.ListaExcelImportacion(i).IdCuentaBancaria2
                .Referencia1 = Me.ListaExcelImportacion(i).Referencia1.ToUpper
                .Referencia2 = Me.ListaExcelImportacion(i).Referencia2.ToUpper
                .IdConceptoMovBanco = 1700000001 '1700000005
                If (Me.ListaExcelImportacion(i).Monto < 0) Then
                    .Monto = ListaExcelImportacion(i).Monto * -1
                    .Factor = -1
                Else
                    .Monto = ListaExcelImportacion(i).Monto
                    .Factor = 1
                End If
                .EstadoMov = True
                .IdUsuario = CType(Session.Item("IdUsuario"), Integer)
                .EstadoAprobacion = False
                .IdentificadorExport = IdentificadorResult
            End With
            lista.Add(objMovBancoView)
        Next
        ListaExcelImportacion = lista
        setListaExcelImportacion(Me.ListaExcelImportacion)

        Try
            Dim resultado As Boolean = New Negocio.MovBancoView().InsertMovBancoExcel(lista)
            cboBanco2.DataSource = Nothing
            cboBanco2.DataBind()
            cboCuentaBancaria2.DataSource = Nothing
            cboCuentaBancaria2.DataBind()

            objCbo.LlenarCboBanco(cboBanco2, True)
            objCbo.LlenarCboCuentaBancaria(cboCuentaBancaria2, CInt(cboBanco2.SelectedValue), True)
            If (resultado = False) Then
                objScript.mostrarMsjAlerta(Me, "Registro masivo guardado correctamente. ")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    ''Boton que Importa los datos del Excel a la grilla Nº 1
   
    'Private Sub LlenarGrilladelExcel()
    '    Try
    '        Dim connString As String = ""
    '        Dim strFileType As String = Path.GetExtension(fileuploadExcel.FileName).ToLower()
    '        Dim path__1 As String = fileuploadExcel.PostedFile.FileName
    '        'Connection String to Excel Workbook
    '        If strFileType.Trim() = ".xls" Then
    '            connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & path__1 & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
    '        ElseIf strFileType.Trim() = ".xlsx" Then
    '            connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & path__1 & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
    '        End If
    '        ''Query : va a jalar según la estructura del banco seleccionado (Leer la tabla estructura para poder mandar parametros correctos.)
    '        ''Segun el banco se arma el query del Excel. (Módulo de configuracion de estructura.)
    '        Dim valoresatraer As String = ""
    '        valoresatraer = " [Fecha],[Fecha_valuta],[Descripción_operación],[Monto], [Saldo], [Sucursal_agencia], [Operación_Número], [Operación_Hora], [Usuario],[UTC] ,[Referencia2] "

    '        Dim query As String = "SELECT " + valoresatraer + " FROM [Sheet1$]"
    '        Dim conn As New OleDbConnection(connString)
    '        If conn.State = ConnectionState.Closed Then
    '            conn.Open()
    '        End If
    '        Dim cmd As New OleDbCommand(query, conn)
    '        Dim da As New OleDbDataAdapter(cmd)
    '        Dim ds As New DataSet()
    '        da.Fill(ds)
    '        GV_DataDeExcel.DataSource = ds.Tables(0)
    '        GV_DataDeExcel.DataBind()
    '        da.Dispose()
    '        conn.Close()
    '        conn.Dispose()
    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub

    Private Sub LlenarGrillaLinqServer()
        Try
            Dim objMovBancoV As Entidades.MovBancoView = New Entidades.MovBancoView
            objMovBancoV.IdBanco2 = CInt(cboBanco2.SelectedValue)
            objMovBancoV.IdCuentaBancaria2 = CInt(cboCuentaBancaria2.SelectedValue)
            Dim objRecibeMov As Entidades.MovBancoView = New Negocio.MovBancoView().SelectxIdBancoxIdCuentaBancaria(objMovBancoV.IdBanco2, objMovBancoV.IdCuentaBancaria2)

            Dim NombreArchivo As String = ""
            NombreArchivo = fileuploadExcel.FileName
            Dim obj As Entidades.ImportacionExcel = New Entidades.ImportacionExcel
            obj.IdEstructuraBanco = objRecibeMov.idestructurabanco
            obj.NombreHoja = objRecibeMov.nom_Hoja

            Dim ListaMovBanco As List(Of Entidades.MovBancoView) = New Negocio.MovBancoView().SelectExcelttoGridView(NombreArchivo, obj.NombreHoja, obj.IdEstructuraBanco)

            GV_DataDeExcel.DataSource = ListaMovBanco
            GV_DataDeExcel.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Se encontró Datos invalidos al momento de Importar.Verifique su excel.No procede la operación.")
        End Try

    End Sub
   
    Protected Sub GV_EdicionDeImportacion_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GV_EdicionDeImportacion.RowCancelingEdit
        e.Cancel = True
        GV_EdicionDeImportacion.EditIndex = -1
        Me.ListaExcelImportacion = getListaExcelImportacion()
        GV_EdicionDeImportacion.DataSource = ListaExcelImportacion
        GV_EdicionDeImportacion.DataBind()
    End Sub
    Protected Sub GV_EdicionDeImportacion_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GV_EdicionDeImportacion.RowUpdating

        Dim IdTienda As String = TryCast(GV_EdicionDeImportacion.Rows(e.RowIndex).FindControl("cboTienda"), DropDownList).SelectedValue
        Dim Referencia1 As String = CStr(CType(GV_EdicionDeImportacion.Rows(e.RowIndex).FindControl("txtReferencia1"), TextBox).Text)
        Dim Referencia2 As String = CStr(CType(GV_EdicionDeImportacion.Rows(e.RowIndex).FindControl("txtReferencia2"), TextBox).Text)
        Dim IdMovBanco As Integer = CInt(CType(GV_EdicionDeImportacion.Rows(e.RowIndex).FindControl("hddIdMovBanco"), HiddenField).Value)
        Dim IdConcepto As String = TryCast(GV_EdicionDeImportacion.Rows(e.RowIndex).FindControl("cboConcepto"), DropDownList).SelectedValue

        Dim query As String = "update MovBanco set IdTienda = @IdTienda , Referencia1=@Referencia1,Referencia2=@Referencia2,IdConceptoMovBanco=@IdConceptoMovBanco  where IdMovBanco = @IdMovBanco"
        Try
            Using cmd As New SqlCommand(query, con)
                cmd.CommandTimeout = 0
                cmd.Parameters.AddWithValue("@IdTienda", IdTienda)
                cmd.Parameters.AddWithValue("@IdMovBanco", IdMovBanco)
                cmd.Parameters.AddWithValue("@Referencia1", Referencia1.ToUpper)
                cmd.Parameters.AddWithValue("@Referencia2", Referencia2.ToUpper)
                cmd.Parameters.AddWithValue("@IdConceptoMovBanco", IdConcepto)
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
                GV_EdicionDeImportacion.EditIndex = -1
                e.Cancel = True
                Llenar2Grid()
            End Using
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Tiene que actualizar todos los campos mostrados. Ejm: Tienda, Referencia,Concepto y demás.(Los campos Tienda y Concepto son obligatorios). No procede la actualización.")
        End Try
    End Sub
    Protected Sub GV_EdicionDeImportacion_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GV_EdicionDeImportacion.RowEditing

        GV_EdicionDeImportacion.EditIndex = e.NewEditIndex
        Me.ListaExcelImportacion = getListaExcelImportacion()
        GV_EdicionDeImportacion.DataSource = ListaExcelImportacion
        GV_EdicionDeImportacion.DataBind()
    End Sub
    Protected Sub btnVerParaEditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnVerParaEditar.Click
        Llenar2Grid()
    End Sub
    Private Sub Llenar2Grid()
        ListaExcelImportacion = getListaExcelImportacion()
        Dim ValidarIdentificador As Integer = 0
        For i As Integer = 0 To Me.ListaExcelImportacion.Count - 1
            ValidarIdentificador = Me.ListaExcelImportacion(i).IdentificadorExport
            Exit For
        Next
        Try
            Dim lista As List(Of Entidades.MovBancoView) = New Negocio.MovBancoView().SelectInsercionExcel(ValidarIdentificador)
            GV_EdicionDeImportacion.DataSource = lista
            GV_EdicionDeImportacion.DataBind()

            ListaExcelImportacion = lista
            setListaExcelImportacion(Me.ListaExcelImportacion)
            GV_DataDeExcel.DataSource = Nothing
            GV_DataDeExcel.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Error en consulta del registro Excel. Verifique que el Excel tenga datos correctos.")
        End Try
    End Sub
    Protected Sub GV_EdicionDeImportacion_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_EdicionDeImportacion.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow AndAlso GV_EdicionDeImportacion.EditIndex = e.Row.RowIndex Then
            con.Open()
            Dim ddl As DropDownList = DirectCast(e.Row.FindControl("cboTienda"), DropDownList)
            Dim ddl2 As DropDownList = DirectCast(e.Row.FindControl("cboConcepto"), DropDownList)

            Dim cmd As New SqlCommand("select tie_Nombre,IdTienda from Tienda ", con)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            da.Fill(ds)

            ddl.DataSource = ds
            ddl.DataTextField = "tie_Nombre"
            ddl.DataValueField = "IdTienda"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Seleccione--", "0"))

            Dim cmd2 As New SqlCommand("select IdConceptoMovBanco,cban_DescripcionBreve  from ConceptoMovBanco ", con)
            Dim da2 As New SqlDataAdapter(cmd2)
            Dim ds2 As New DataSet()
            da2.Fill(ds2)

            ddl2.DataSource = ds2
            ddl2.DataTextField = "cban_DescripcionBreve"
            ddl2.DataValueField = "IdConceptoMovBanco"
            ddl2.DataBind()
            ddl2.Items.Insert(0, New ListItem("--Seleccione--", "0"))
            con.Close()
        End If
    End Sub
#End Region

    Protected Sub GV_EdicionDeImportacion_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GV_EdicionDeImportacion.RowDeleting
        Try
            'getting key value, row id
            Dim IdMovBanco As Integer = Convert.ToInt32(GV_EdicionDeImportacion.DataKeys(e.RowIndex).Value.ToString())
            'getting row field subjectname
            'Dim IdMovBanco As HiddenField = CType(GV_EdicionDeImportacion.Rows(e.RowIndex).FindControl("hddIdMovBanco"), HiddenField)
            Dim Query As String = "DELETE FROM MovBanco WHERE IdMovBanco=@IdMovBanco"
            Using cmd As New SqlCommand(Query, con)
                con.Open()
                cmd.Parameters.AddWithValue("@IdMovBanco", IdMovBanco)
                cmd.ExecuteNonQuery()
                con.Close()
                Llenar2Grid()
                objScript.mostrarMsjAlerta(Me, "Registro eliminado correctamente.")
            End Using
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas al eliminar el registro. Comuniquese con el área de Sistemas.")
        End Try
    End Sub
    Protected Sub dgvMovBanco_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvMovBanco.RowCommand
        If (e.CommandName = "Edit") Then

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim row As GridViewRow = dgvMovBanco.Rows(index)
            Dim ind As Integer = 0
            Dim IdMovBanco As Integer = 0

            IdMovBanco = CInt(CType(Me.dgvMovBanco.Rows(index).FindControl("hddIdMovBanco"), HiddenField).Value)
            hhdIdMovBancoGene.Value = CStr(IdMovBanco)
            If (IdMovBanco <> 0) Then
                EdicionMovBanco(IdMovBanco)
            End If

            If Me.dgvMovBanco IsNot Nothing Then
                Me.lblSaldoDisponible.Text = CStr(CType(Me.dgvMovBanco.Rows(index).FindControl("hdfSaldoDisponible"), HiddenField).Value.Trim)
                Me.lblSaldoContable.Text = CStr(CType(Me.dgvMovBanco.Rows(index).FindControl("hdfSalContable"), HiddenField).Value.Trim)
            End If

        End If
    End Sub
    Private Sub EdicionMovBanco(ByVal IdMovBanco As Integer)
        Try
            ocultaLBL(True)
            cargar_recargar_modo(modo_menu.Editar)
            'ind = ListaGetIndiceEntidadxId(ListMovBanco_Grilla, IdMovBanco)
            cargar_recargar_Id(IdMovBanco)
            Dim objMovBanco As Entidades.MovBancoView = New Negocio.MovBancoView().SelectxId(IdMovBanco)
            If (objMovBanco IsNot Nothing) Then
                With objMovBanco
                    If (.IdBanco <> 0) Then
                        Me.cboBanco.SelectedValue = CStr(.IdBanco)
                        objCbo.LlenarCboCuentaBancaria(cboCuentaBancaria, .IdBanco, True)
                    End If
                    If (.Referencia1x IsNot Nothing) Then
                        Me.txtReferencia1Mov.Text = .Referencia1x
                    End If
                    If (.Referencia2x IsNot Nothing) Then
                        Me.txtReferencia2Mov.Text = .Referencia2x
                    End If

                    If (.IdCuentaBancaria <> 0) Then
                        Me.cboCuentaBancaria.SelectedValue = CStr(.IdCuentaBancaria)
                        objCbo.LlenarCboPostxIdBancoxIdCuentaBancaria(cboPost, .IdBanco, .IdCuentaBancaria, True)
                    End If

                    If (.IdPost <> 0) Then
                        Me.cboPost.SelectedValue = CStr(.IdPost)
                    End If

                    If (.IdTipoTarjeta <> 0) Then
                        Me.cboTipoTarjeta.SelectedValue = CStr(.IdTipoTarjeta)
                        objCbo.LlenarCboTarjetaxIdTipoTarjeta(cboTarjeta, .IdTipoTarjeta, True)
                    End If

                    If (.IdTarjeta <> 0) Then
                        Me.cboTarjeta.SelectedValue = CStr(.IdTarjeta)
                    End If

                    If (.IdTienda <> 0) Then
                        Me.cboTienda.SelectedValue = CStr(.IdTienda)
                        objCbo.LlenarCboCajaxIdTienda(cboCaja, .IdTienda, True)
                        Me.cboCaja.SelectedValue = CStr(.IdCaja)
                    End If

                    If (.IdTipoConceptoBanco <> 0) Then
                        Me.cboTipoConceptoBanco.SelectedValue = CStr(.IdTipoConceptoBanco)
                        objCbo.LlenarCboConceptoMovBancoxIdTipoConceptoBanco(cboConceptoMovBanco, .IdTipoConceptoBanco, True)
                        Me.cboConceptoMovBanco.SelectedValue = CStr(.IdConceptoMovBanco)
                    End If
                    If (.EstadoAprobacion <> Nothing) Then
                        Me.chkEstadoAprobacion.Checked = .EstadoAprobacion
                    End If


                    If .EstadoMov Then
                        Me.rbtlEstadoMov.SelectedValue = "1"
                    Else
                        Me.rbtlEstadoMov.SelectedValue = "0"
                    End If


                    Me.txtNroOperacion.Text = .NroOperacion
                    Me.txtObservacion.Text = .Observacion
                    Me.txtMonto.Text = .DescMonto
                    Me.txtFechaMov.Text = CStr(IIf(.DescFechaMov = Nothing, Now.Date.ToString, .DescFechaMov))

                End With

                'fin

                If p.IdMovBanco > 0 Then
                    Try
                        ListaDocRef = (New Negocio.Anexo_MovBanco).SelectxIdMovBanco(p.IdMovBanco)
                        cargar_recargar_ListaDocRef(ListaDocRef)
                        dgvDocRef.DataSource = ListaDocRef
                        dgvDocRef.DataBind()
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, "")
                    End Try
                End If

                HabilitarControles(modo)
            Else
                objScript.mostrarMsjAlerta(Me, "Error al momento de cargar la información.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    'Protected Sub lbtnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try

    '        ocultaLBL(True)
    '        Dim ind As Integer 'indice de la lista correspondiente al registro a modificar

    '        cargar_recargar_modo(modo_menu.Editar)
    '        'Se obtiene el Id del Registro a editar 
    '        'ini
    '        Dim lbtnEditar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
    '        Dim fila As GridViewRow = CType(lbtnEditar.NamingContainer, GridViewRow)
    '        IdMovBanco = CInt(CType(fila.Cells(0).FindControl("hddIdMovBanco"), HiddenField).Value.Trim())
    '        'fin


    '        If Me.dgvMovBanco IsNot Nothing Then
    '            Me.lblSaldoDisponible.Text = CStr(CType(fila.Cells(12).FindControl("hdfSaldoDisponible"), HiddenField).Value.Trim)
    '            Me.lblSaldoContable.Text = CStr(CType(fila.Cells(13).FindControl("hdfSalContable"), HiddenField).Value.Trim)
    '        End If

    '        'se obtiene el indice de la lista, para asi obtener la entidad a editar 
    '        ind = ListaGetIndiceEntidadxId(ListMovBanco_Grilla, IdMovBanco)
    '        cargar_recargar_Id(IdMovBanco)

    '        'se carga los controles
    '        'ini
    '        With ListMovBanco_Grilla(ind)

    '            Me.cboBanco.SelectedValue = CStr(.IdBanco)
    '            objCbo.LlenarCboCuentaBancaria(cboCuentaBancaria, .IdBanco, True)
    '            Me.cboCuentaBancaria.SelectedValue = CStr(.IdCuentaBancaria)
    '            objCbo.LlenarCboPostxIdBancoxIdCuentaBancaria(cboPost, .IdBanco, .IdCuentaBancaria, True)
    '            Me.cboPost.SelectedValue = CStr(.IdPost)

    '            Me.cboTipoTarjeta.SelectedValue = CStr(.IdTipoTarjeta)
    '            objCbo.LlenarCboTarjetaxIdTipoTarjeta(cboTarjeta, .IdTipoTarjeta, True)
    '            Me.cboTarjeta.SelectedValue = CStr(.IdTarjeta)

    '            Me.cboTienda.SelectedValue = CStr(.IdTienda)
    '            objCbo.LlenarCboCajaxIdTienda(cboCaja, .IdTienda, True)
    '            Me.cboCaja.SelectedValue = CStr(.IdCaja)

    '            Me.cboTipoConceptoBanco.SelectedValue = CStr(.IdTipoConceptoBanco)
    '            objCbo.LlenarCboConceptoMovBancoxIdTipoConceptoBanco(cboConceptoMovBanco, .IdTipoConceptoBanco, True)
    '            Me.cboConceptoMovBanco.SelectedValue = CStr(.IdConceptoMovBanco)

    '            Me.chkEstadoAprobacion.Checked = .EstadoAprobacion

    '            If .EstadoMov Then
    '                Me.rbtlEstadoMov.SelectedValue = "1"
    '            Else
    '                Me.rbtlEstadoMov.SelectedValue = "0"
    '            End If

    '            Me.txtNroOperacion.Text = .NroOperacion
    '            Me.txtObservacion.Text = .Observacion
    '            Me.txtMonto.Text = .DescMonto
    '            Me.txtFechaMov.Text = CStr(IIf(.DescFechaMov = "", Now.Date.ToString, .DescFechaMov))

    '        End With
    '        'fin

    '        If p.IdMovBanco > 0 Then
    '            ListaDocRef = (New Negocio.Anexo_MovBanco).SelectxIdMovBanco(p.IdMovBanco)
    '        End If

    '        cargar_recargar_ListaDocRef(ListaDocRef)
    '        dgvDocRef.DataSource = ListaDocRef
    '        dgvDocRef.DataBind()

    '        HabilitarControles(modo)

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub
    Protected Sub dgvMovBanco_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgvMovBanco.RowEditing
        dgvMovBanco.EditIndex = e.NewEditIndex
        dgvMovBanco.EditIndex = -1
        'SelectMovBancoxId(CInt(hhdIdMovBancoGene.Value))
    End Sub
    Private Sub SelectMovBancoxId(ByVal IdMovBanco As Integer)
        Try
            Dim ListaMovBanco As Entidades.MovBancoView = New Negocio.MovBancoView().SelectMovBancosxId(IdMovBanco)
            dgvMovBanco.DataSource = ListaMovBanco
            dgvMovBanco.DataBind()
            hhdIdMovBancoGene.Value = "0"
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class