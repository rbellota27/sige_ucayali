<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" EnableEventValidation = "false"
    CodeBehind="FrmMantMovBanco.aspx.vb" Inherits="APPWEB.FrmMantMovBanco" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
|<asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscar">
        <table width="100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                    onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" onmouseout="this.src='../../Imagenes/Buscar_B.JPG';" />
                                <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='../../Imagenes/Nuevo_A.JPG';"
                                    onmouseout="this.src='../../Imagenes/Nuevo_b.JPG';" />
                                <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                    onmouseover="this.src='../../Imagenes/Guardar_A.JPG';" onmouseout="this.src='../../Imagenes/Guardar_B.JPG';"
                                    OnClientClick="return(validarSave());" CausesValidation="true" />
                                <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                    onmouseover="this.src='../../Imagenes/Cancelar_A.JPG';" onmouseout="this.src='../../Imagenes/Cancelar_B.JPG';"
                                    OnClientClick="return(confirm('Desea cancelar el proceso?'));" />
                                    <asp:Button id="btnImportar" runat="server" text="Importaci�n Masiva"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Panel ID="Panel_Importar" runat="server"  Visible="false">
        <table width="100%">
          
         <tr>
        <td class="TituloCelda" align="center">
                IMPORTAR DOCUMENTOS
        </td>
        </tr>
        <tr>
        <td>
        <table>
          <tr>
                <td class="Label">
                    <asp:Label ID="lblBanco2" runat="server" Text="Banco:"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="cboBanco2" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td class="Label">
                    <asp:Label ID="lblCuentaBancaria2" runat="server" Text="Cuenta Bancaria:"></asp:Label>
                </td>
                <td>
                    <asp:UpdatePanel ID="upCuentaBancaria2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:DropDownList ID="cboCuentaBancaria2" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="cboBanco2" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                </tr>
        <tr>  
        <td class="Label" ><asp:Label ID="lblTexto" runat="server" Text="Seleccione el Archivo Excel: " ></asp:Label> </td>
        <td><asp:FileUpload ID="fileuploadExcel" runat="server" />  </td>
     
        </tr>
      <tr>
      <td></td>
      <td>
      <asp:Button ID="btnImport" runat="server" Text="Generar Importaci�n" />
      </td>
      
      <td>  <asp:Button  ID="btnVerParaEditar" runat="server" Text="Editar Datos" Visible="false"/></td>
        
      </tr>


                      </table> 
                        </td>
        </tr>
        </table>
        <table width="100%">
        
        <tr>
        <td>
         <asp:GridView ID="GV_DataDeExcel" runat="server" AutoGenerateColumns="False" 
                BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" 
                CellPadding="3" Width="98%" >
            <RowStyle ForeColor="#000066" />
            <Columns>
                <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Observaciones">
                    <ItemTemplate>
                        <asp:Label ID="lblDescripcionOP" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Descripci�n_operaci�n")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Mov." 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblFechaOP" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Fecha","{0:d}")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Monto" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblMontoOP" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
               <%--   <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Saldo" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblSaldo" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F3}")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>--%>
               <%-- <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Sucursal - agencia" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblSucursalOP" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Sucursal_agencia")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Nro. Operaci�n" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblOperacionNumero" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Operaci�n_N�mero")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                 <%-- <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Operaci�n - Hora" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblOperHoraOP" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Operaci�n_Hora")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>--%>
                
                 <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Referencia 1" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblReferenciaOP" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Referencia1")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                
                <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Referencia 2" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblReferenciaOP2" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Referencia2")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                
                
              <%--  <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Usuario" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblUsuario" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Usuario")%>'></asp:Label>
                    </ItemTemplate>
                     <EditItemTemplate>
                    <asp:TextBox runat="server" ID="txtUsuario" Text='<%# Eval("Usuario")%>' />
                    </EditItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>--%>
              <asp:TemplateField>
         
            </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        </asp:GridView>
        </td>
        </tr>
        <tr>
        <td></td>
        </tr>
        <tr>
          <asp:GridView ID="GV_EdicionDeImportacion" runat="server" AutoGenerateColumns="False" 
                BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" 
                CellPadding="3" Width="98%"  DataKeyNames ="IdMovBanco"
              >
            <RowStyle ForeColor="#000066" 
            
            />
            <Columns>
            <asp:CommandField ButtonType="Button" ShowEditButton="true" ShowCancelButton="true" />
                <asp:TemplateField >
                <ItemTemplate>
                 
                <asp:Button ID="deleteButton" runat="server" CommandName="Delete" Text="Eliminar"
                OnClientClick="return confirm('�Esta seguro que desea eliminar este Mov. Bancario?');" />
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Observaciones">
                    <ItemTemplate>
                        <asp:Label ID="lblDescripcionOP" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Observacion")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Mov." 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblFechaOP" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"FechaMov","{0:d}")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <%--<asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Valuta" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblFechaValutaOP" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Fecha_valuta","{0:d}")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Monto" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblMontoOP" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Tienda" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblSucursalOP" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                    <asp:Label ID="lblSucursalOP" runat="server" Text='<%# Eval("Sucursal_agencia")%>' Visible = "false"></asp:Label>
                    <asp:DropDownList ID = "cboTienda" runat = "server">
                    </asp:DropDownList>
                       <asp:HiddenField ID="hddIdMovBanco" runat="server" 
                                Value='<%#DataBinder.Eval(Container.DataItem,"IdMovBanco")%>'/>
                    </EditItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Nro. Operaci�n" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                    <table>
                                <asp:Label ID="lblOperacionNumero" runat="server" 
                                Text='<%#DataBinder.Eval(Container.DataItem,"NroOperacion")%>'></asp:Label> 
                   </table>
                    </ItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                
                                
                <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Concepto" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblConcepto" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"ConceptoMovBanco")%>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                    <asp:Label ID="lblConcepto" runat="server" Text='<%# Eval("ConceptoMovBanco")%>' Visible = "false"></asp:Label>
                    <asp:DropDownList ID = "cboConcepto" runat = "server">
                    </asp:DropDownList>
                    </EditItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                
                
                <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Referencia 1" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblReferencia1" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Referencia1")%>'></asp:Label>
                    </ItemTemplate>
                    
                     <EditItemTemplate>
                    <asp:Label ID="lblReferencia1" runat="server" Text='<%# Eval("Referencia1")%>' Visible = "false"></asp:Label>
                    <asp:TextBox ID = "txtReferencia1" runat = "server" Text='<%# Eval("Referencia1")%>'></asp:TextBox>
                    </EditItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                
                 <asp:TemplateField HeaderStyle-Height="25px" 
                    HeaderStyle-HorizontalAlign="Center" HeaderText="Asunto" 
                    ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblReferencia2" runat="server" 
                            Text='<%#DataBinder.Eval(Container.DataItem,"Referencia2")%>'></asp:Label>
                    </ItemTemplate>
                    
                     <EditItemTemplate>
                    <asp:Label ID="lblReferencia2" runat="server" Text='<%# Eval("Referencia2")%>' Visible = "false"></asp:Label>
                    <asp:TextBox ID = "txtReferencia2" runat = "server" Text='<%# Eval("Referencia2")%>'></asp:TextBox>
                    </EditItemTemplate>
                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        </asp:GridView>
        </tr>
        
       
        </table>
        <table>
<%--           <asp:GridView ID="grvExcelData" runat="server" BackColor="White"  Width="98%"
                BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" >
                    <RowStyle ForeColor="#000066" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#006699" Font-Bold="true" ForeColor="White" />
                </asp:GridView>--%>
        </table>
        <table>
       
        
        </table>
        </asp:Panel>
        <asp:Panel ID="PanelMovBancarios" runat="server"  Visible="false">
       
        
        <br />
          <table style="width: 100%;">
         <tr>
                            <td class="SubTituloCelda">
                               MOVIMIENTOS BANCARIOS
                            </td>
                        </tr>
                        
                        </table>
        <table style="width: 100%;">
        
        <tr>
                     <td class="Label">
                    <asp:Label ID="lblFechaMovInicial" runat="server" Text="Fecha Mov. Inicial:"> </asp:Label>
                </td>
                    <td>
                    <asp:TextBox ID="txtFechaMovInicial" runat="server" onblur="return(valFecha(this));"> </asp:TextBox>
                    <cc1:MaskedEditExtender ID="txtFechaMovInicial_MaskedEditExtender" runat="server"
                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                        CultureTimePlaceholder="" Enabled="True" TargetControlID="txtFechaMovInicial"
                        ClearMaskOnLostFocus="false" Mask="99/99/9999">
                    </cc1:MaskedEditExtender>
                    <cc1:CalendarExtender ID="txtFechaMovInicial_CalendarExtender" runat="server" TargetControlID="txtFechaMovInicial"
                        Enabled="True" Format="dd/MM/yyyy">
                    </cc1:CalendarExtender>
                        </td>
                  
                                  <td class="Label">
                    <asp:Label ID="lblFechaMovFinal" runat="server" Text="Fecha Mov. Final:"></asp:Label></td><td>
                    <asp:TextBox ID="txtFechaMovFinal" runat="server" onblur="return(valFecha(this));"></asp:TextBox><cc1:MaskedEditExtender ID="txtFechaMovFinal_MaskedEditExtender" runat="server" CultureAMPMPlaceholder=""
                        CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                        CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                        Enabled="True" TargetControlID="txtFechaMovFinal" ClearMaskOnLostFocus="false"
                        Mask="99/99/9999">
                    </cc1:MaskedEditExtender>
                    <cc1:CalendarExtender ID="txtFechaMovFinal_CalendarExtender" runat="server" TargetControlID="txtFechaMovFinal"
                        Enabled="True" Format="dd/MM/yyyy">
                    </cc1:CalendarExtender>
                </td>
        </tr>

            <tr>
           
                <td class="Label">
                    <asp:Label ID="lblBanco" runat="server" Text="Banco:"></asp:Label></td><td>
                    <asp:DropDownList ID="cboBanco" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td class="Label">
                    <asp:Label ID="lblCuentaBancaria" runat="server" Text="Cuenta Bancaria:"></asp:Label></td><td>
                  <%--  <asp:UpdatePanel ID="upCuentaBancaria" runat="server" UpdateMode="Conditional">--%>
                        <%--<ContentTemplate>--%>
                            <asp:DropDownList ID="cboCuentaBancaria" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                     <%--   </ContentTemplate>--%>
                      <%--  <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="cboBanco" EventName="SelectedIndexChanged" />
                        </Triggers>--%>
              <%--      </asp:UpdatePanel>--%>
                </td>
                <td class="Label">
                    <asp:Label ID="lblEstadoMov" runat="server" Text="Estado:"></asp:Label></td><td>
                    <asp:RadioButtonList ID="rbtlEstadoMov" runat="server" RepeatDirection="Horizontal "
                        CssClass="Label">
                        <asp:ListItem Text="Todos" Value="2"></asp:ListItem><asp:ListItem Text="Activo" Selected="True" Value="1"></asp:ListItem><asp:ListItem Text="Inactivo" Value="0"></asp:ListItem></asp:RadioButtonList></td></tr><tr>
                <td class="Label">
                    <asp:Label ID="lblTipoConcepto" runat="server" Text="Tipo Concepto:" Style="margin-left: 0px"></asp:Label></td><td>
                    <asp:DropDownList ID="cboTipoConceptoBanco" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td class="Label">
                    <asp:Label ID="lblConceptoMovBanco" runat="server" Text="Concepto Mov. Banco:"></asp:Label></td><td>
               <%--     <asp:UpdatePanel ID="upConceptoMovBanco" runat="server" UpdateMode="Conditional">--%>
                       <%-- <ContentTemplate>--%>
                            <asp:DropDownList ID="cboConceptoMovBanco" runat="server">
                            </asp:DropDownList>
                      <%--  </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="cboTipoConceptoBanco" EventName="SelectedIndexChanged" />
                        </Triggers>--%>
   <%--                 </asp:UpdatePanel>--%>
                </td>
            </tr>
            <tr>
                <td class="Label">
                    <asp:Label ID="lblNroOperacion" runat="server" Text="Nro. Operaci�n:"></asp:Label></td><td>
                    <asp:TextBox ID="txtNroOperacion" runat="server"></asp:TextBox>
                    </td>
                  <td class="Label">
                    <asp:Label ID="lblMonto" runat="server" Text="Monto:"></asp:Label>
                    <asp:Label ID="lblMoneda" runat="server" Text=""></asp:Label>
        
                </td>
                <td>
                    <asp:TextBox ID="txtMonto" runat="server" onblur="return(valBlur(event));"></asp:TextBox></td></tr><tr>
                <td class="Label">
                    <asp:Label ID="lblFechaMov" runat="server" Text="Fecha Movimiento:"></asp:Label></td><td>
                    <asp:UpdatePanel ID="upFechaMov" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFechaMov" runat="server" onblur="return(valFecha(this));"></asp:TextBox><cc1:MaskedEditExtender ID="txtFechaMov_MaskedEditExtender" runat="server" CultureAMPMPlaceholder=""
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                Enabled="True" TargetControlID="txtFechaMov" ClearMaskOnLostFocus="false" Mask="99/99/9999">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFechaMov_CalendarExtender" runat="server" TargetControlID="txtFechaMov"
                                Enabled="True" Format="dd/MM/yyyy">
                            </cc1:CalendarExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="Label">
                    <asp:Label ID="lblSalDisponibleDescp" runat="server" Text="Saldo Disponible:"></asp:Label></td><td>
                    <asp:Label ID="lblSaldoDisponible" runat="server" Font-Bold="true" CssClass="LabelRojo"  Text=" "></asp:Label></td><td class="Label">
                    <asp:Label ID="lblSalContDesc" runat="server" Text="Saldo Contable:"></asp:Label></td><td>
                    <asp:Label ID="lblSaldoContable" runat="server" Font-Bold="true" CssClass="LabelRojo" Text=" "></asp:Label></td></tr>
                
                    
        </table>
        <table width="59%">
        <tr>
                 <td class="Label"> 
                    <asp:Label ID="lblReferencia1Mov" runat="server" Text="Asunto: "></asp:Label>
                    </td>
                    <td>
                    <asp:TextBox ID="txtReferencia1Mov" runat="server" Text="" TextMode="MultiLine"  Width="385px" Height="35px" ></asp:TextBox>
                    </td>
                    </tr>
                    <tr>
                    <td class="Label">
                     <asp:Label ID="lblReferencia2Mov" runat="server" Text="Descripci�n: "></asp:Label>
                    </td>
                      <td>
                    <asp:TextBox ID="txtReferencia2Mov" runat="server" Text="" TextMode="MultiLine"  Width="385px" Height="35px" ></asp:TextBox>
                    </td>
        </tr>
        </table>
        
         </asp:Panel>
        <asp:Panel ID="Panel_DatosAdicionales" runat="server">
            <table width="100%">
                <tr>
                    <td align="left" class="TituloCeldaLeft">
                        <asp:Panel ID="pnlMovBancoAdd_Cabecera" runat="server">
                            <asp:Image ID="ImgMovBancoAdd" runat="server" />&nbsp;Datos Adicionales
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" TargetControlID="pnlMovBancoAdd"
                                ImageControlID="ImgMovBancoAdd" ExpandedImage="../Imagenes/Menos_B.JPG" CollapsedImage="../Imagenes/Mas_B.JPG"
                                Collapsed="true" CollapseControlID="ImgMovBancoAdd" ExpandControlID="ImgMovBancoAdd">
                            </cc1:CollapsiblePanelExtender>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlMovBancoAdd" runat="server">
                            <table width="50%">
                                <tr>
                                    <td class="Label">
                                        <asp:Label ID="lblTienda" runat="server" Text="Tienda:"></asp:Label></td><td>
                                        <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Label">
                                        <asp:Label ID="lblCaja" runat="server" Text="Caja:"></asp:Label></td><td>
                                    <%--    <asp:UpdatePanel ID="upCaja" runat="server" UpdateMode="Conditional">--%>
                                          <%--  <ContentTemplate>--%>
                                                <asp:DropDownList ID="cboCaja" runat="server">
                                                </asp:DropDownList>
                                          <%--  </ContentTemplate>--%>
                                          <%--  <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="cboTienda" EventName="SelectedIndexChanged" />
                                            </Triggers>--%>
                                      <%--  </asp:UpdatePanel>--%>
                                    </td>
                                    <td class="Label">
                                        <asp:Label ID="lblPost" runat="server" Text="Post:"></asp:Label></td><td>
                                    <%--    <asp:UpdatePanel ID="upPost" runat="server" UpdateMode="Conditional">--%>
                                           <%-- <ContentTemplate>--%>
                                                <asp:DropDownList ID="cboPost" runat="server">
                                                </asp:DropDownList>
                                        <%--    </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="cboCuentaBancaria" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="cboBanco" EventName="SelectedIndexChanged" />
                                            </Triggers>--%>
                                      <%--  </asp:UpdatePanel>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Label">
                                        <asp:Label ID="lblTipoTarjeta" runat="server" Text="Tipo Tarjeta:"></asp:Label></td><td>
                                        <asp:DropDownList ID="cboTipoTarjeta" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Label">
                                        <asp:Label ID="lblTarjeta" runat="server" Text="Tarjeta:"></asp:Label></td><td>
                                       <%-- <asp:UpdatePanel ID="upTarjeta" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>--%>
                                                <asp:DropDownList ID="cboTarjeta" runat="server">
                                                </asp:DropDownList>
<%--                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="cboTipoTarjeta" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkEstadoAprobacion" runat="server" Text="Aprobado:" TextAlign="Left"
                                            class="Label" Enabled="False" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="Panel_PersonaRef" runat="server">
            <table width="100%">
                <tr>
                    <td align="left" class="TituloCeldaLeft">
                        <asp:Panel ID="pnlPersonaRef_Cabecera" runat="server">
                            <asp:Image ID="ImgPersonaRef" runat="server" />&nbsp;Persona Referencia
                            <cc1:CollapsiblePanelExtender ID="cpePersonaRef" runat="server" TargetControlID="pnlPersonaRef"
                                ImageControlID="ImgPersonaRef" ExpandedImage="../Imagenes/Menos_B.JPG" CollapsedImage="../Imagenes/Mas_B.JPG"
                                Collapsed="true" CollapseControlID="ImgPersonaRef" ExpandControlID="ImgPersonaRef">
                            </cc1:CollapsiblePanelExtender>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlPersonaRef" runat="server">
                            <asp:UpdatePanel ID="upPersonaRef" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td class="Label">
                                                <asp:Label ID="lblNombrePersonaRef" runat="server" Text="Persona Referencia:"></asp:Label></td><td colspan="3">
                                                <asp:TextBox ID="txtNombrePersonaRef" runat="server" Width="300px"></asp:TextBox></td><td>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="btnBuscarCliente" runat="server" ImageUrl="../Imagenes/Buscar_b.JPG"
                                                    OnClientClick="return(mostrarCapaPersona());" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                    onmouseover="this.src='../Imagenes/Buscar_A.JPG';" TabIndex="13" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Label">
                                                <asp:Label ID="lblDNI" runat="server" Text="DNI:"></asp:Label></td><td>
                                                <asp:TextBox ID="txtDNI" runat="server"></asp:TextBox></td><td class="Label">
                                                <asp:Label ID="lblRUC" runat="server" Text="RUC:"></asp:Label></td><td>
                                                <asp:TextBox ID="txtRUC" runat="server"></asp:TextBox></td></tr></table></ContentTemplate><Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="DGV_BuscarPersona" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="Panel_DocumentoRef" runat="server">
            <table width="100%">
                <tr>
                    <td align="left" class="TituloCeldaLeft">
                        <asp:Panel ID="pnlDocumentoRef_Cabecera" runat="server">
                            <asp:Image ID="ImgDocumentoRef" runat="server" />&nbsp;Documento Referencia
                            <cc1:CollapsiblePanelExtender ID="cpeDocumentoRef" runat="server" TargetControlID="pnlDocumentoRef"
                                ImageControlID="ImgDocumentoRef" ExpandedImage="../Imagenes/Menos_B.JPG" CollapsedImage="../Imagenes/Mas_B.JPG"
                                Collapsed="true" CollapseControlID="ImgDocumentoRef" ExpandControlID="ImgDocumentoRef">
                            </cc1:CollapsiblePanelExtender>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlDocumentoRef" runat="server">
                            <asp:UpdatePanel ID="upDocumentoRef" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="btnBuscarDocumentoRef" runat="server" ImageUrl="../Imagenes/Buscar_b.JPG"
                                                    OnClientClick="return(onCapa('capaDocumentosReferencia'));" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                    onmouseover="this.src='../Imagenes/Buscar_A.JPG';" TabIndex="14" ToolTip="Buscar Documento Referencia" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="PnlGrillaDocRef" runat="server" Height="250px" ScrollBars="Vertical">
                                                    <asp:GridView ID="dgvDocRef" runat="server" AutoGenerateColumns="False" Style="text-align: left">
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbtnQuitarDocRef" runat="server" OnClick="lbtnQuitarDocRef_Click">Quitar</asp:LinkButton></ItemTemplate></asp:TemplateField><%--<asp:BoundField HeaderText="Id" DataField="IdDocumentoRef"></asp:BoundField>
                                                            <asp:BoundField HeaderText="Nro. Documento" DataField="NroDocumento"></asp:BoundField>
                                                            --%>
                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Nro. Documento"
                                                                ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:HiddenField ID="hddIdDocRef" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumentoRef")%>' />
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="NroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label></td></tr></table></ItemTemplate><HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Tipo de Documento" DataField="NomTipoDocumento"></asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="GV_DocumentosReferencia_Find" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table width="100%">
            <tr>
                <td align="left" class="TituloCeldaLeft">
                    <asp:Panel ID="pnlObservacion_Cabecera" runat="server">
                        <asp:Image ID="ImgObservacion" runat="server" />&nbsp;Observaciones
                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="pnlObservacion"
                            ImageControlID="ImgObservacion" ExpandedImage="../Imagenes/Menos_B.JPG" CollapsedImage="../Imagenes/Mas_B.JPG"
                            Collapsed="true" CollapseControlID="ImgObservacion" ExpandControlID="ImgObservacion">
                        </cc1:CollapsiblePanelExtender>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlObservacion" runat="server">
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine" Style="width: 100%;
                                        height: 58px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" MaxLength="400"></asp:TextBox></td></tr></table></asp:Panel></td></tr></table><table width="100%">
            <tr>
                <td style="text-align: center;">
   
                            <div style="overflow: auto; width:100%; height: 400px">
                                <asp:GridView ID="dgvMovBanco" runat="server" AutoGenerateColumns="False" Width="100%"
                                    CellPadding="2" HorizontalAlign="Justify" EnableViewState="True" >
                                    <Columns>
                                     <%--   <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnEditar" runat="server" OnClick="lbtnEditar_Click">Editar</asp:LinkButton>
                                                <asp:HiddenField ID="hddIdMovBanco" Value='<%# DataBinder.Eval(Container.DataItem,"IdMovBanco") %>'
                                                    runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                              <asp:Button ID="btnEditar" runat="server" 
                                              CommandName="Edit" 
                                              CommandArgument="<%# CType(Container,GridViewRow).RowIndex %>"
                                              Text="Editar" />
                                                 <asp:HiddenField ID="hddIdMovBanco" Value='<%# DataBinder.Eval(Container.DataItem,"IdMovBanco") %>'
                                                    runat="server" />
                                               </ItemTemplate> 
                                            </asp:TemplateField>
                                        <%--                    <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminar_Click">Eliminar</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                                        <asp:BoundField DataField="DescFechaRegistro" HeaderText="F. Registro" NullDisplayText="" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="DescFechaMov" HeaderText="F. Mov." NullDisplayText="" DataFormatString="{0:dd/MM/yyyy}"/>
                                        <asp:BoundField DataField="TipoConceptoBancoResumen" HeaderText="T. Concepto" NullDisplayText="" />
                                        <asp:BoundField DataField="ConceptoMovBanco" HeaderText="Concepto Mov." NullDisplayText="" />
                                        <asp:BoundField DataField="DescMonto" HeaderText="Monto Mov." NullDisplayText="" />
                                        <asp:BoundField DataField="DescFechaAprobacion" HeaderText="F. Aprobaci�n" NullDisplayText="" DataFormatString="{0:dd/MM/yyyy}"/>
                                        <asp:BoundField DataField="Tienda" HeaderText="Tienda" NullDisplayText="" />
                                        <asp:BoundField DataField="Usuario" HeaderText="Usuario" NullDisplayText="" />
                                        <asp:BoundField DataField="NroOperacion" HeaderText="Nro. Operaci�n" NullDisplayText="" />
                                        <asp:BoundField DataField="Referencia1x" HeaderText="Asunto" NullDisplayText="" />
                                        <asp:BoundField DataField="Referencia2x" HeaderText="Descripci�n" NullDisplayText="" />
                                        <asp:CheckBoxField DataField="EstadoMov" HeaderText="Activo" />
                                        <asp:TemplateField  >
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdfSaldoDisponible" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"SaldoDisponible") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField  >
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdfSalContable" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"SaldoContable") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       </Columns>
                                       <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </div>
                       
                </td>
            </tr>
        </table>
     
    </asp:Panel>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_Persona" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="left">
                            <asp:GridView ID="DGV_BuscarPersona" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                PageSize="20" Width="100%">
                                <Columns>
                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                    <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                                    <asp:BoundField DataField="getNombreParaMostrar" HeaderText="Descripci�n" NullDisplayText="---" />
                                    <asp:BoundField DataField="Dni" HeaderText="D.N.I." NullDisplayText="---" />
                                    <asp:BoundField DataField="Ruc" HeaderText="R.U.C." NullDisplayText="---" />
                                    <asp:BoundField DataField="Direccion" HeaderText="Direcci�n" NullDisplayText="---" />
                                    <%--<asp:BoundField DataField="PorcentPercepcion" HeaderText="Percep. (%)" NullDisplayText="0"
                                        DataFormatString="{0:F2}" />
                                    <asp:BoundField DataField="PorcentRetencion" HeaderText="Retenc. (%)" NullDisplayText="0"
                                        DataFormatString="{0:F2}" />--%></Columns><HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr class="Label">
                        <td align="left">
                            <table>
                                <tr>
                                <td>Texto:</td>
                                    <td>
                                        <asp:DropDownList ID="cmbFiltro_BuscarPersona" runat="server">
                                            <asp:ListItem Selected="True" Value="0">Descripci�n</asp:ListItem><asp:ListItem Value="1">R.U.C.</asp:ListItem><asp:ListItem Value="2">D.N.I.</asp:ListItem></asp:DropDownList></td>
                                    <td>
                                        Filtro:
                                    </td>
                                    <td>
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarPersona_Grilla">                                        
                                        <asp:TextBox ID="txtBuscarPersona_Grilla" runat="server" onKeypress="return(  valKeypressBuscarPersona(this)  );"
                                            Width="250px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox><asp:ImageButton ID="btnBuscarPersona_Grilla" runat="server" CausesValidation="false"
                                            ImageUrl="../Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                            </asp:Panel>
                                    </td>
                                   
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="../Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('capaPersona'));" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnAnterior_Persona" runat="server" Font-Bold="true" Width="50px"
                                Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionPersona('0'));" />
                            <asp:Button ID="btnPosterior_Persona" runat="server" Font-Bold="true" Width="50px"
                                Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionPersona('1'));" />
                            <asp:TextBox ID="txtPageIndex_Persona" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                runat="server"></asp:TextBox><asp:Button ID="btnIr_Persona" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionPersona('2'));" />
                            <asp:TextBox ID="txtPageIndexGO_Persona" Width="50px" CssClass="TextBox_ReadOnly"
                                runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox></td></tr></table></ContentTemplate></asp:UpdatePanel></div><div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisi�n</asp:ListItem><asp:ListItem Value="1">Por Nro. Documento</asp:ListItem></asp:RadioButtonList></td></tr><tr>
                <td>
                    <label class="Label">
                        Tipo de Documento:                        Tipo de Documento:                Tipo de Documento:</label>
                    <asp:DropDownList ID="cboTipoDocumento" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox><cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox><cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server" Visible="false">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox></td><td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox></td><td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="False"
                            Width="100%">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Select"
                                            Text="Seleccionar"></asp:LinkButton></ItemTemplate><HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Tipo" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblNomTipoDocumento" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>'></asp:Label></td></tr></table></ItemTemplate><HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Nro. Documento"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblNroDocumento_Find" runat="server" Font-Bold="true" ForeColor="Red"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label></td><td>
                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Fecha Emisi�n" ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Fecha Vcto." ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DescripcionPersona" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Cliente" ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RUC" HeaderStyle-HorizontalAlign="Center" HeaderText="R.U.C."
                                    ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DNI" HeaderStyle-HorizontalAlign="Center" HeaderText="D.N.I."
                                    ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NomEstadoDocumento" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Estado" ItemStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        
          <asp:HiddenField id="hhdIdMovBancoGene" Value="0" runat="server"/>
    </div>
      
            
        <script language="javascript" type="text/javascript">
        function validarSave() {
            var Banco = document.getElementById('<%=cboBanco.ClientID%>');
            var idBanco = Banco.value;
            if (idBanco == 0) {
                alert('Seleccione un Banco.');

                return false;
            }
            var CuentaBancaria = document.getElementById('<%=cboCuentaBancaria.ClientID%>');
            var idCuentaBancaria = CuentaBancaria.value;
            if (idCuentaBancaria == 0) {
                alert('Seleccione una Cuenta Bancaria.');

                return false;
            }
            //            var Tarjeta = document.getElementById('<%=cboTarjeta.ClientID%>');
            //            var idTarjeta = Tarjeta.value;
            //            if (idTarjeta == 0) {
            //                alert('Seleccione un Tipo de Tarjeta.');

            //                return false;
            //            }
            var ConceptoMovBanco = document.getElementById('<%=cboConceptoMovBanco.ClientID%>');
            var idConceptoMovBanco = ConceptoMovBanco.value;
            if (idConceptoMovBanco == 0) {
                alert('Seleccione un Concepto Mov. Banco.');

                return false;
            }

            var NroOperacion = document.getElementById('<%=txtNroOperacion.ClientID%>');
            if (NroOperacion.value.length == 0) {
                alert('Debe Ingresar una Nro. Operaci�n.');
                NroOperacion.select();
                NroOperacion.focus();
                return false;
            }
            var Monto = document.getElementById('<%=txtMonto.ClientID%>');
            if (Monto.value.length == 0) {
                alert('Debe Ingresar un Monto.');
                Monto.select();
                Monto.focus();
                return false;
            }

            return (confirm('Desea Continuar Con la Operacion'));
        }

        function mostrarCapaPersona() {

            onCapa('capaPersona');

            return false;
        }

        function valKeypressBuscarPersona(obj) {
            if (event.keyCode == 13) {
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscarPersona_Grilla.ClientID%>').focus();
            }
            return true;
        }
        function addPersona_Venta() {
            offCapa('capaPersona');

            return false;
        }
        function valNavegacionPersona(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Persona.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valOnClick_btnAceptarBuscarDocRefxCodigo() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('Ingrese un Nro. de Serie para la b�squeda.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('Ingrese un Nro. de Documento para la b�squeda.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }

    </script>

    <script language="javascript" type="text/javascript">
    
    </script>

</asp:Content>
