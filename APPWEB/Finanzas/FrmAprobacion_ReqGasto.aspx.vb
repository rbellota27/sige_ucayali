﻿

Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports
Imports Entidades.RequerimientoGasto
Imports System.Net
Imports System.Net.Mail

Partial Public Class FrmAprobacion_ReqGasto
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    '<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            inicializarFrm()
        End If
    End Sub

    Private Sub inicializarFrm()
        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboAreaxIdusuario(Me.cboArea, CInt(Session("IdUsuario")), Nothing, False)

            End With

            Me.txtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaFin.Text = Me.txtFechaInicio.Text
            Me.valortotal.Text = ""
            Me.lbltotalx.Visible = False
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        visualizarFrm()
    End Sub

    Private Sub visualizarFrm()
        Me.valortotal.Text = ""
        Dim calculando As Decimal = 0
        Try
            Dim obj As Entidades.RequerimientoGasto = New Entidades.RequerimientoGasto

            Dim fechaInicio, fechaFin As Date

            If (IsDate(Me.txtFechaInicio.Text) And Me.txtFechaInicio.Text.Trim.Length > 0) Then
                fechaInicio = CDate(Me.txtFechaInicio.Text)
            End If

            If (IsDate(Me.txtFechaFin.Text) And Me.txtFechaFin.Text.Trim.Length > 0) Then
                fechaFin = CDate(Me.txtFechaFin.Text)
            End If

            Me.GV_DocumentoReqGasto.DataSource = (New Negocio.RequerimientoGasto).DocumentoRequerimientoGasto_SelectAprobacion(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboArea.SelectedValue), CInt(Me.cboOpcion_Aprobacion.SelectedValue), fechaInicio, fechaFin, CDec(Me.txtMonto.Text))
            Me.GV_DocumentoReqGasto.DataBind()



            ''Verificando que requerimientos de gasto han sido aprobados si es asi se le habilita la opción de impresión.
            For Each gvRow As GridViewRow In GV_DocumentoReqGasto.Rows

                Dim btn As Button = DirectCast(gvRow.FindControl("btnimprimir"), Button)
                Dim btn_aprob As Button = DirectCast(gvRow.FindControl("btnAprobar"), Button)
                Dim chkItem As CheckBox = DirectCast(gvRow.FindControl("chb_Aprobado"), CheckBox)
                Dim btnDesaprobar As Button = DirectCast(gvRow.FindControl("btnDesaprobar"), Button)
                'Dim est_reque As Label = DirectCast(gvRow.FindControl("est_reque"), Label)
                Dim estadorequerimiento As String = (CStr(CType(gvRow.FindControl("hddestadoreque"), HiddenField).Value)) ''Validando que campos necesitan tener habilitados los botones.
                Dim Sumatorioa As Decimal = (CDec(CType(gvRow.FindControl("lblMonto"), Label).Text))

                Dim total As Decimal = 0
                If (Sumatorioa > 0) Then
                    total += Sumatorioa
                End If

                calculando += total

                If (chkItem.Checked) Then
                    btn.Enabled = True
                    btn_aprob.Enabled = False
                Else
                    btn_aprob.Enabled = True
                    btn.Enabled = False
                    btnDesaprobar.Enabled = True
                End If
                ''Activando y desactivando botones según sea el estado del requerimiento.
                If (estadorequerimiento.ToString() = "2") Then
                    btnDesaprobar.Enabled = False
                    btn_aprob.Enabled = False
                    btn.Enabled = True
                ElseIf (estadorequerimiento.ToString() = "0") Then
                    btn_aprob.Enabled = True
                    btnDesaprobar.Enabled = True
                End If
            Next

            valortotal.Text += CStr(calculando)
            Me.lbltotalx.Visible = True

            If (Me.GV_DocumentoReqGasto.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        valOnChange_cboEmpresa()
    End Sub

    Private Sub valOnChange_cboEmpresa()
        Try
            Dim objCbo As New Combo
            With objCbo
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub visualizarCapaAprobacion(ByVal index As Integer, ByVal codigo As Integer)
        Try

            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {115})

            If listaPermisos(0) > 0 Then  '********* AUTORIZAR (jefe Inmediato)
            Else
                Throw New Exception("NO POSEE LOS PERMISOS PARA APROBAR")
            End If

            Dim IdDocumento As Integer = CInt(CType(Me.GV_DocumentoReqGasto.Rows(index).FindControl("hddIdDocumento"), HiddenField).Value)
            Dim IdPersona As Integer = CInt(CType(Me.GV_DocumentoReqGasto.Rows(index).FindControl("hddIdPersona"), HiddenField).Value)
            Dim NumDocumento As String = CStr(CType(Me.GV_DocumentoReqGasto.Rows(index).FindControl("hddNumDocu"), HiddenField).Value)
            Dim beneficiario As String = CStr(CType(Me.GV_DocumentoReqGasto.Rows(index).FindControl("hddbeneficiario"), HiddenField).Value)
            Dim estadorequerimiento As String = (CStr(CType(Me.GV_DocumentoReqGasto.Rows(index).FindControl("hddestadoreque"), HiddenField).Value))


            Me.hddIdDocumento.Value = CStr(IdDocumento)
            Me.hddIdPersona.Value = CStr(IdPersona)
            Me.hddNumDocu.Value = CStr(NumDocumento)
            Me.hddbeneficiario.Value = CStr(beneficiario)
            Me.hddestadoreque.Value = CStr(estadorequerimiento)

            Dim objAnexoDocumento As Entidades.Anexo_Documento = (New Negocio.Anexo_Documento).Anexo_DocumentoSelectxIdDocumento(IdDocumento)
            Dim cont As Integer = (New Negocio.Util).ValidarExistenciaxTablax1Campo("MovCuentaPorPagar", "IdDocumento", CStr(IdDocumento))

            ''Validando el codigo recibido por el boton aprobar 
            ''o desaprobar y asignandole valor según sea el caso.
            If (objAnexoDocumento IsNot Nothing) Then

                With objAnexoDocumento

                    If (codigo = 1) Then
                        .EstadoRequerimiento = 1
                        .Aprobar = True
                        Me.rdbAutorizado.SelectedValue = "1"

                    ElseIf (codigo = 2) Then
                        .EstadoRequerimiento = 2
                        .Aprobar = False
                        Me.rdbAutorizado.SelectedValue = "0"
                    End If

                    If (cont >= 0 And codigo = 1) Then
                        Me.chbEnviarCxP.Checked = True
                        Me.txtFechaAprobacion.Text = CStr(.FechaAprobacion)
                        Me.txtFechaAprobacion.Text = CStr((New Negocio.FechaActual).SelectFechaActual)
                    ElseIf (cont >= 0 And codigo = 2) Then
                        Me.chbEnviarCxP.Checked = False
                        Me.txtFechaAprobacion.Text = CStr(.FechaAprobacion)
                        Me.txtFechaAprobacion.Text = CStr((New Negocio.FechaActual).SelectFechaActual)
                    End If

                End With
            Else

            End If


            'objAnexoDocumento.

            'If()


            'objScript.onCapa(Me, "capaAprobarReqGasto")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    'Private Sub rdbAutorizado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbAutorizado.SelectedIndexChanged
    '    valOnChange_rdbAutorizado()
    'End Sub
    'Private Sub valOnChange_rdbAutorizado()

    '    Try

    '        Select Case CInt(Me.rdbAutorizado.SelectedValue)
    '            Case 0  '*********** NO APROBAR

    '                Me.txtFechaAprobacion.Text = ""
    '                Me.chbEnviarCxP.Checked = False
    '                Me.chbEnviarCxP.Enabled = False

    '            Case 1

    '                Me.txtFechaAprobacion.Text = CStr((New Negocio.FechaActual).SelectFechaActual)
    '                Me.chbEnviarCxP.Enabled = True

    '        End Select

    '        objScript.onCapa(Me, "capaAprobarReqGasto")

    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try

    'End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click
        registrarAprobacion()
    End Sub
    Private Sub registrarAprobacion()

        Try

            Dim objAnexoDocumento As Entidades.Anexo_Documento = obtenerAnexo_Documento()
            Dim objMovCuenta_CXP As Entidades.MovCuentaPorPagar = obtenerMovCuentaxPagar()
            Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)

            If ((New Negocio.RequerimientoGasto).aprobarDocumento(IdDocumento, objAnexoDocumento, objMovCuenta_CXP)) Then

                Me.GV_DocumentoReqGasto.DataSource = Nothing
                Me.GV_DocumentoReqGasto.DataBind()
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")


                ''IMPLEMENTACION DEL ENVIO DE CORREO AUTOMATICO .


                'Dim valor As Integer = objAnexoDocumento.IdUsuarioSupervisor

                'Dim objSuper As Entidades.Usuario = (New Negocio.Usuario).SelectTodosUsuario(CInt(valor))

                'Dim CorreoUsu As String = String.Empty
                'Dim nombres As String = String.Empty
                'Dim NumeroDocu As String = CStr(hddNumDocu.Value)
                ' ''
                'Dim benefici As String = CStr(hddbeneficiario.Value)
                'Dim Supervis As String = CStr(objSuper.Nombres)

                ' ''
                'Dim objUsu As Entidades.Usuario = (New Negocio.Usuario).SelectTodosUsuario(CInt(Me.hddIdPersona.Value))
                'CorreoUsu = objUsu.CorreoUsu
                'nombres = objUsu.Nombres
                ' ''

                'Dim Desde As String = "sistemas@sanicenter.com.pe"
                'Dim Motivo As String = "Estado de Solicitud de Requerimiento de Gasto."
                'Dim Para As String = CorreoUsu
                'Dim MensajeApro As String = "Estimado Usuario: " + nombres + "<br/>" + "El Nro Documento: " + NumeroDocu + "," + "con fecha de Respuesta: " + CStr(objAnexoDocumento.FechaAprobacion) + ", que tiene como beneficiario al Sr(a)." + benefici + "<br/>" + "Ha sido aprobado por: " + Supervis + "<br/>" + "Estado del documento: " + "Aprobado." + "<br/>" + "Tipo de Proceso: Requerimiento"
                'Dim MensajeDesapro As String = "Estimado Usuario: " + nombres + "<br/>" + "El Nro Documento: " + NumeroDocu + "," + "con fecha de Respuesta: " + CStr(objAnexoDocumento.FechaAprobacion) + ", que tiene como beneficiario al Sr(a)." + benefici + "<br/>" + "Ha sido desaprobado por: " + Supervis + "<br/>" + "Estado del documento: " + "Desaprobado." + "<br/>" + "Tipo de Proceso: Requerimiento"
                'Dim TipProceso As String = String.Empty


                'If (objAnexoDocumento.EstadoRequerimiento = 1) Then   ''Mando el correo si lo he aprobado
                '    SendEmail(Desde, "Gocrospoma@sanicenter.com.pe", Motivo, MensajeApro, True)
                'ElseIf (objAnexoDocumento.EstadoRequerimiento = 2) Then   ''Mando el correo si lo he desaprobado
                '    SendEmail(Desde, "Gocrospoma@sanicenter.com.pe", Motivo, MensajeDesapro, True)
                'Else

                'End If

            Else

                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    ''Método para enviar correo.
    'Public Function SendEmail(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String, ByVal IsBodyHTML As Boolean) As Boolean
    '    Dim arrToArray As String()
    '    Dim splitter As Char = CChar(";")

    '    'Loop through the recepient list and separate them
    '    'based on semi colon
    '    arrToArray = strTo.Split(splitter)

    '    Dim mm As MailMessage = New MailMessage()
    '    mm.From = New MailAddress(strFrom)
    '    mm.Subject = strSubject
    '    mm.Body = strBody
    '    mm.IsBodyHtml = True
    '    'Reply To Address (Optional)
    '    mm.ReplyTo = New MailAddress("finanzas@sanicenter.com.pe")


    '    'Add the recepient email Addresses
    '    For Each s As String In arrToArray
    '        mm.To.Add(New MailAddress(s))
    '    Next

    '    Dim smtp As SmtpClient = New SmtpClient()
    '    Try
    '        'Your SMTP Server
    '        smtp.Host = "smtp.gmail.com"

    '        'SSL Settings depending on your Server
    '        smtp.EnableSsl = True


    '        'Creadentials for the Server
    '        Dim NetworkCred As NetworkCredential = New System.Net.NetworkCredential()

    '        'Your Email
    '        NetworkCred.UserName = "sistemas@sanicenter.com.pe"

    '        'Your Password
    '        NetworkCred.Password = "Sopor15.*"

    '        smtp.UseDefaultCredentials = True
    '        smtp.Credentials = NetworkCred

    '        'Port No of the Server 
    '        smtp.Port = 587
    '        smtp.Send(mm)
    '        Return True
    '    Catch
    '        Return False
    '    Finally
    '        smtp = Nothing
    '        mm = Nothing
    '    End Try
    'End Function
   

    Private Function obtenerMovCuentaxPagar() As Entidades.MovCuentaPorPagar

        Dim objMovCuentaCxP As Entidades.MovCuentaPorPagar = Nothing

        If (Me.chbEnviarCxP.Checked = True) Then

            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(CInt(Me.hddIdDocumento.Value))

            objMovCuentaCxP = New Entidades.MovCuentaPorPagar

            With objMovCuentaCxP

                .Monto = CDec(objDocumento.TotalAPagar)
                .Saldo = CDec(objDocumento.TotalAPagar)
                .Factor = 1
                .IdProveedor = CInt(hddIdPersona.Value)
                .IdCuentaProv = Nothing
                .IdDocumento = CInt(Me.hddIdDocumento.Value)
                .IdMovCuentaTipo = 1

            End With

        End If
        Return objMovCuentaCxP

    End Function
    Private Function obtenerAnexo_Documento() As Entidades.Anexo_Documento
        ''Asignandole valor a el estado del Requerimiento si aprobo o no.
        Dim objAnexo_Documento As New Entidades.Anexo_Documento

        With objAnexo_Documento
            .IdDocumento = CInt(Me.hddIdDocumento.Value)

            Dim objAnexDoc As Entidades.Anexo_Documento = New Negocio.Anexo_Documento().Anexo_DocumentoSelectxIdDocumento(CInt(Me.hddIdDocumento.Value))
            If (objAnexDoc.Banco.Length > 0 And objAnexDoc.NroCuentaBancaria.Length > 0) Then
                .NroCuentaBancaria = objAnexDoc.NroCuentaBancaria
                .Banco = objAnexDoc.Banco
                .IdCuentaBancaria = objAnexDoc.IdCuentaBancaria
                .cb_CuentaInterbancaria = objAnexDoc.cb_CuentaInterbancaria
                .cb_SwiftBancario = objAnexDoc.cb_SwiftBancario
            End If

            Select Case CInt(Me.rdbAutorizado.SelectedValue)
                Case 0
                    .EstadoRequerimiento = 2
                    .Aprobar = False
                    .IdArea = CInt(Me.cboArea.SelectedValue)
                    .IdUsuarioSupervisor = CInt(Session("IdUsuario"))
                Case 1
                    .EstadoRequerimiento = 1
                    .Aprobar = True
                    .IdArea = CInt(Me.cboArea.SelectedValue)
                    .IdUsuarioSupervisor = CInt(Session("IdUsuario"))
            End Select
            Try
                .FechaAprobacion = CDate(Me.txtFechaAprobacion.Text)
            Catch ex As Exception
                .FechaAprobacion = Nothing
            End Try
        End With
        Return objAnexo_Documento

    End Function
    ''eventos agregados para hacer dinamico los botones de la grilla. Aprobar / Desaprobar según la elección que elija
    ''se pueda hacer diferentes funcionalidades.
    Protected Sub GV_DocumentoReqGasto_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GV_DocumentoReqGasto.RowCommand


        If (e.CommandName = "Insert") Then
            Dim index As Integer = CInt(e.CommandArgument)
            visualizarCapaAprobacion(index, 1)
            registrarAprobacion()
            visualizarFrm()
        ElseIf (e.CommandName = "InserDes") Then
            Dim index As Integer = CInt(e.CommandArgument)
            visualizarCapaAprobacion(index, 2)
            registrarAprobacion()
            visualizarFrm()
        End If
    End Sub

    ''Cambiando de valor las celdas mostradas con un determinado valor.
    Protected Sub GV_DocumentoReqGasto_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_DocumentoReqGasto.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim valor As String = e.Row.Cells(8).Text
            'If valor = "0" Then
            '    e.Row.Cells(8).Text = "Pendiente"
            If valor = "1" Then
                e.Row.Cells(8).Text = "Aprobado"
            ElseIf valor = "2" Then
                e.Row.Cells(8).Text = "Desaprobado"
            Else
                e.Row.Cells(8).Text = "Pendiente"
            End If
        End If

    End Sub
End Class