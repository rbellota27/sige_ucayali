﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmEmisionCheque

    '''<summary>
    '''Control Panel1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel1 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control UpdateProgressSerieCheque.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpdateProgressSerieCheque As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''Control upSerieCheque.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upSerieCheque As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control dgvSerieCheque.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dgvSerieCheque As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btnNuevo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnNuevo As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control btnBuscar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscar As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control btnGuardar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnGuardar As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control btnCancelar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCancelar As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control btnImprimir.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnImprimir As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control lblTituloPnlDatos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTituloPnlDatos As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control btnAbrirCapaProgramacionPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAbrirCapaProgramacionPago As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control lblFechaACobrarInicial.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblFechaACobrarInicial As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control txtFechaACobrarInicial.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaACobrarInicial As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtFechaACobrarInicial_MaskedEditExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaACobrarInicial_MaskedEditExtender As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control txtFechaACobrarInicial_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaACobrarInicial_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control lblFechaACobrarFinal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblFechaACobrarFinal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control txtFechaACobrarFinal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaACobrarFinal As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtFechaACobrarFinal_MaskedEditExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaACobrarFinal_MaskedEditExtender As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control txtFechaACobrarFinal_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaACobrarFinal_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control lblBanco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblBanco As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control cboBanco.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboBanco As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control lblCuentaBancaria.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCuentaBancaria As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control upCuentaBancaria.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upCuentaBancaria As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control cboCuentaBancaria.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboCuentaBancaria As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control lblEstadoDoc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEstadoDoc As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control cboEstadoDoc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboEstadoDoc As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control lblcboSerieNumeracion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblcboSerieNumeracion As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control upcboSerieNumeracion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upcboSerieNumeracion As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control cboSerieNumeracion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboSerieNumeracion As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control lblNumeroCheque.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblNumeroCheque As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control upNumeroCheque.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upNumeroCheque As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control txtNumeroCheque.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNumeroCheque As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control lblEstadoEntrega.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEstadoEntrega As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control cboEstadoEntregado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboEstadoEntregado As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control lblMonto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMonto As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control upMonedaSymbol.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upMonedaSymbol As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control lblMoneda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMoneda As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control txtMonto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtMonto As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control lblNumeroVoucher.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblNumeroVoucher As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control txtNumeroVoucher.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNumeroVoucher As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control lblEstadoCancelacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEstadoCancelacion As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control cboEstadoCancelacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboEstadoCancelacion As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control lblFechaACobrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblFechaACobrar As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control upFechaACobrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upFechaACobrar As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control txtFechaACobrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaACobrar As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtFechaACobrar_MaskedEditExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaACobrar_MaskedEditExtender As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control txtFechaACobrar_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaACobrar_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control lblFechaEmision.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblFechaEmision As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control upFechaEmision.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upFechaEmision As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control txtFechaEmision.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaEmision As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtFechaEmision_MaskedEditExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaEmision_MaskedEditExtender As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control txtFechaEmision_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaEmision_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control lblMensajeInformativo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMensajeInformativo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control Panel_PersonaBenef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_PersonaBenef As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control upPersonaBenef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upPersonaBenef As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control lblTextBeneficiario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTextBeneficiario As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lblBeneficiario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblBeneficiario As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control btnBuscarCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarCliente As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control lblTextDNI.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTextDNI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lblDNI.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblDNI As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lblTextRUC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTextRUC As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lblRUC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblRUC As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control Panel_DocumentoRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_DocumentoRef As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control btnBuscarDocumento_Ref.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarDocumento_Ref As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control GV_DocumentoRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_DocumentoRef As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control Panel_Observacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Observacion As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control pnlObservacion_Cabecera.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlObservacion_Cabecera As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control ImgObservacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImgObservacion As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''Control CollapsiblePanelExtender1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CollapsiblePanelExtender1 As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''Control pnlObservacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlObservacion As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtObservacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtObservacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control upGrilla_Progress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upGrilla_Progress As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''Control upGrilla.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents upGrilla As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control dgvDatos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dgvDatos As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control hddIdDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdDocumento As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdProgramacionPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdProgramacionPago As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control btnClose_ProgramacionPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnClose_ProgramacionPago As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control cboEmpresa_PP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboEmpresa_PP As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboTienda_PP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboTienda_PP As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboMoneda_PP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboMoneda_PP As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control MaskedEdit_FechaIni_PP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEdit_FechaIni_PP As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control Calendar_FechaIni_PP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Calendar_FechaIni_PP As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control txt_FechaIni_PP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt_FechaIni_PP As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEdit_FechaFin_PP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEdit_FechaFin_PP As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control Calendar_FechaFin_PP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Calendar_FechaFin_PP As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control txt_FechaFin_PP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txt_FechaFin_PP As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscar_PP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscar_PP As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control Panel_ProgramacionPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_ProgramacionPago As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control GV_ProgramacionPago.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_ProgramacionPago As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btnCerrar_Capa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCerrar_Capa As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control pnlBusquedaPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlBusquedaPersona As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control rdbTipoPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rdbTipoPersona As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Control cboRol.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboRol As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control tbRazonApe.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbRazonApe As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbbuscarDni.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbbuscarDni As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control tbbuscarRuc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbbuscarRuc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btBuscarPersonaGrilla.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btBuscarPersonaGrilla As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control gvBuscar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvBuscar As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btAnterior.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btAnterior As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btSiguiente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btSiguiente As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tbPageIndex.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbPageIndex As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btIr.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btIr As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tbPageIndexGO.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbPageIndexGO As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control ImageButton6.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton6 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control cbotipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cbotipoDocumento As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control rdbBuscarDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rdbBuscarDocRef As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Control Panel_BuscarDocRefxFecha.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_BuscarDocRefxFecha As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtFechaInicio_DocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaInicio_DocRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender4 As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender3 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control txtFechaFin_DocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaFin_DocRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender5.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender5 As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender4 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control btnAceptarBuscarDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAceptarBuscarDocRef As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control Panel_BuscarDocRefxCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_BuscarDocRefxCodigo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtSerie_BuscarDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtSerie_BuscarDocRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtCodigo_BuscarDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCodigo_BuscarDocRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnAceptarBuscarDocRefxCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAceptarBuscarDocRefxCodigo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control Panel_DocRef_Find.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_DocRef_Find As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control GV_DocumentosReferencia_Find.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_DocumentosReferencia_Find As Global.System.Web.UI.WebControls.GridView
End Class
