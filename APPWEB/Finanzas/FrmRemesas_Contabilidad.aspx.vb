﻿Imports Entidades
Imports Negocio
Imports general.librerias.serializador
Partial Public Class FrmRemesas_Contabilidad
    Inherits System.Web.UI.Page


    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property

    Private objScript As New ScriptManagerClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.txtFechaInicial.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaFinal.Text = Me.txtFechaInicial.Text
        Me.txtFechaAsiento.Text = Me.txtFechaInicial.Text

        Dim Usuario As Integer = CInt(Session("IdUsuario"))
        Dim nroVoucher As String = String.Empty

        Dim flag As String = Request.QueryString("flag")
        Select Case flag
            Case "llenarcomboBanco"
                Dim IdEstado As String = Request.QueryString("IdEstadoc")
                Dim rpta As String = ""
                Dim lista As New List(Of be_Bancos)
                Try
                    lista = (New bl_Bancos).listarBancos(Convert.ToInt32(IdEstado))
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rpta)
                    Response.End()
                Else
                    Response.Write(rpta)
                    Response.End()
                End If

                'Case "llenarMoneda"

                '    Dim lista As New List(Of be)
                '    Dim rptaTipope As String = ""
                '    Try
                '        lista = (New blTipoOperacion).listarTipoOperacion(2102866605)
                '    Catch ex As Exception
                '        objScript.mostrarMsjAlerta(Me, ex.Message)
                '    End Try

                '    If lista.Count > 0 Then
                '        Try
                '            rptaTipope = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                '        Catch ex As Exception
                '            objScript.mostrarMsjAlerta(Me, ex.Message)
                '        End Try

                '        Response.Write(rptaTipope)
                '        Response.End()
                '    Else
                '        Response.Write(rptaTipope)
                '        Response.End()
                '    End If

            Case "BuscarMovimientosBancos"


                Dim Empresa As String = Request.QueryString("Empresa")
                Dim Moneda As String = Request.QueryString("Moneda")
                Dim banco As String = Request.QueryString("banco")
                Dim fechaInicio As String = Request.QueryString("fechaInicio")
                Dim FechaFinal As String = Request.QueryString("FechaFinal")
                Dim FechaAsiento As String = Request.QueryString("FechaAsiento")

                Dim lista As New List(Of be_RemesasContabilidad)
                Dim rptaTipope As String = ""
                Try
                    lista = (New bl_RemesasContabilidad).listarRemesasContabilidad(Empresa, Convert.ToInt32(Moneda), Convert.ToInt32(banco), fechaInicio, FechaFinal, FechaAsiento)
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rptaTipope = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(rptaTipope)
                    Response.End()
                Else
                    Response.Write(rptaTipope)
                    Response.End()
                End If

            Case "UpdateDocumentoEgreso"

                Dim IdMovBanco As String = Request.QueryString("IdMovBanco")
                Dim FechaAsiento As String = Request.QueryString("FechaAsiento")

                Dim lista As New List(Of be_RemesasContabilidad)
                Dim rptaTipope As String = ""

                Try

                    lista = (New bl_RemesasContabilidad).UpdateDocumentoEgreso(IdMovBanco, FechaAsiento)

                    'UpdateDocumentoEgreso SP_Marcar_Remesas_Contabilidad

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rptaTipope = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(rptaTipope)
                    Response.End()
                Else

                    Response.Write(rptaTipope)
                    Response.End()

                End If


            Case "GenerarAsientoContable"
                Dim periodo As String = Request.QueryString("periodo")
                Dim Moneda As String = Request.QueryString("Moneda")
                Dim banco As String = Request.QueryString("banco")
                Dim FechaAsiento As String = Request.QueryString("FechaAsiento")

                Dim lista As New List(Of be_RemesasContabilidad)
                Dim rptaTipope As String = ""

                Dim obj As New Negocio.bl_RemesasContabilidad
                Dim IdUsuario As Integer
                IdUsuario = Convert.ToInt32(Session.Item("IdUsuario"))
                Dim objetoVoucher As New Negocio.bl_RemesasContabilidad

                If FechaAsiento <> "" Then

                    nroVoucher = objetoVoucher.CrearAsientoRemesas(periodo, banco, Moneda, FechaAsiento, IdUsuario)

                    Response.Write(nroVoucher)

                    Response.End()

                End If
        End Select
    End Sub

End Class