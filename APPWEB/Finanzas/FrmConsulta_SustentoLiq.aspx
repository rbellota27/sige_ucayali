<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmConsulta_SustentoLiq.aspx.vb" Inherits="APPWEB.FrmConsulta_SustentoLiq" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td class="TituloCelda">
                CONSULTA
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight: bold">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight: bold">
                            Caja:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboCaja" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Rendici�n de Cuentas:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboOpcion_Sustento" runat="server" Width="100%">
                                <asp:ListItem Value="0" Selected="True">Por sustentar</asp:ListItem>
                                <asp:ListItem Value="1">Sustentados</asp:ListItem>
                                <asp:ListItem Value="2">Todos</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Liquidaci�n:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboOpcion_Liquidacion" runat="server" Width="100%">
                                <asp:ListItem Value="0" Selected="True">Por Liquidar</asp:ListItem>
                                <asp:ListItem Value="1">Liquidados</asp:ListItem>
                                <asp:ListItem Value="2">Todos</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Fecha:
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 25px">
                                    </td>
                                    <td class="Texto" style="font-weight: bold; text-align: right">
                                        Inicio:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFecha_Inicio" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                            Width="100px"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="txtFecha_Inicio_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFecha_Inicio">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="txtFecha_Inicio_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFecha_Inicio">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td class="Texto" style="font-weight: bold; text-align: right">
                                        Fin:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFecha_Fin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                            Width="100px"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="txtFecha_Fin_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFecha_Fin">
                                        </cc1:MaskedEditExtender>
                                        <cc1:CalendarExtender ID="txtFecha_Fin_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" TargetControlID="txtFecha_Fin">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnAceptar_ViewRpt" runat="server" Text="Aceptar" Width="70px"
                                        ToolTip="Mostrar Documentos de Recibo de Egreso" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnMostrarRpt" runat="server" Width="100px" Text="Reporte Mov." ToolTip="Mostrar Reporte de Movimientos de Caja por Rango de Fechas" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <fieldset class="FieldSetPanel_OnlyBorder">
                                <legend class="Texto" style="text-align: left; font-weight: bold">Persona</legend>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Descripci�n:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDescripcionPersona" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                                Width="300px" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            D.N.I.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDni" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" Width="120px"
                                                runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnBuscar_Persona" runat="server" Text="Buscar" Width="80px" 
                                            OnClientClick=" return( valOnClick_btnBuscar_Persona() ); " />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnLimpiar_Persona" runat="server" Text="Limpiar" Width="80px" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="70px" ToolTip="Guardar cambios"
                    OnClientClick="  return( valOnClick_btnGuardar()  );  " Visible="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GV_DocumentoReqGasto" runat="server" AutoGenerateColumns="False"
                    Width="100%">
                    <HeaderStyle CssClass="GrillaHeader" />
                    <Columns>
                        <asp:BoundField DataField="TipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Emisi�n" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="DiasRetrazo" DataFormatString="{0:F0}" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center" HeaderText="D�as Retrazo" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="DescripcionPersona" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Descripci�n" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Monto x Rendir" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMoneda_MontoxRendir" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMontoxSustentar" runat="server" Width="70px" Font-Bold="true"
                                                Text='<%#DataBinder.Eval(Container.DataItem,"MontoxSustentar","{0:F3}")%>' onblur=" return( valBlur(event) ); "
                                                onFocus=" return( aceptarFoco(this) ); " onKeyPress=" return( validarNumeroPuntoPositivo('event') ); "
                                                TabIndex="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="Liquidado" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chb_Liquidado" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Liquidado")%>' />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderText="" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="btnSustentar" runat="server" OnClientClick=" return( valOnClick_btnSustentar() ); "
                                                OnClick="Click_btnSustentar">Sustentar</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                RESUMEN
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td style="text-align: right">
                            <asp:Label ID="lblTotalFisico" runat="server" Text="-" CssClass="Texto" Font-Bold="true"></asp:Label>
                        </td>
                        <td>
                            <asp:GridView ID="GV_CajaResumen_Fisico" runat="server" AutoGenerateColumns="False"
                                Width="500px">
                                <HeaderStyle CssClass="GrillaHeader" />
                                <Columns>
                                    <asp:BoundField DataField="moneda_descripcion" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"moneda")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"monto","{0:F3}")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight: bold; text-align: right">
                            Total por Sustentar:
                        </td>
                        <td>
                            <asp:GridView ID="GV_TotalxSustentar" runat="server" AutoGenerateColumns="False"
                                Width="500px">
                                <HeaderStyle CssClass="GrillaHeader" />
                                <Columns>
                                    <asp:BoundField DataField="moneda_descripcion" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"moneda")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblMonto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"monto","{0:F3}")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                REPORTE DE MOVIMIENTOS DE CAJA
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GV_RptMovCaja" runat="server" AutoGenerateColumns="False" Width="100%">
                    <HeaderStyle CssClass="GrillaHeader" />
                    <Columns>
                        <asp:BoundField DataField="IdTabla" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Font-Bold="true" HeaderText="Nro." ItemStyle-Font-Bold="true" ItemStyle-Height="25px"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="TipoDoc_NroDoc" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Font-Bold="true" HeaderText="Documento" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="DescripcionPersona" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Font-Bold="true" HeaderText="Descripci�n" ItemStyle-Height="25px"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="FechaEmision" HeaderStyle-Height="25px" DataFormatString="{0:dd/MM/yyyy}"
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" HeaderText="Fecha"
                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Entrada_Salida" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Font-Bold="true" HeaderText="Mov." ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="TotalAPagar_MonedaBase" HeaderStyle-Height="25px" DataFormatString="{0:F3}"
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" HeaderText="Monto"
                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="Saldo" HeaderStyle-Height="25px" DataFormatString="{0:F3}"
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" HeaderText="Saldo"
                            ItemStyle-Height="25px" HeaderStyle-ForeColor="Blue" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-BackColor="Yellow" />
                        <asp:BoundField DataField="MontoxSustentar" HeaderStyle-Height="25px" DataFormatString="{0:F3}"
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" HeaderText="Monto x Sustentar"
                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow"
                            HeaderStyle-ForeColor="Blue" />
                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Font-Bold="true" HeaderText="Liquidado" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-BackColor="Yellow" HeaderStyle-ForeColor="Blue">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chb_Liquidado" runat="server" Enabled="true" onClick=" return( false ); "
                                                Checked='<%#DataBinder.Eval(Container.DataItem,"Liquidado")%>' />
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdPersona" runat="server" Value="" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdMedioPagoPrincipal" runat="server" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        R.U.C.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Rol:
                                                                    </td>
                                                                    <td colspan="2">
                                                                        <asp:DropDownList ID="cboRol" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">

        function valOnClick_btnGuardar() {
            return confirm('Desea continuar con la Operaci�n ?');
        }

        function valOnClick_btnSustentar() {
            return true;
        }
        function visualizarDocumento_ReqGasto(IdDocumentoReqGasto) {
            window.open('FrmDocCancelacionCaja.aspx?IdDocumento_Sustentar=' + IdDocumentoReqGasto + '&Modo=7', null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }

        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj) {
            var key = event.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function valOnClick_btnBuscar_Persona() {
            mostrarCapaPersona();
            return false;
        }
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
