﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmAprobacionPagoRequerimientos.aspx.vb" Inherits="APPWEB.FrmAprobacionPagoRequerimientos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td class="TituloCelda" style="width:870px">
                seleccion de requerimientos a cancelar :</td>
        </tr>
        <tr>
            <td style="width: 870px">
                <table>
                    <tr>
                        <td class="Texto" style="font-weight:bold; width: 57px;">
                            Tienda</td>
                        <td>
                            :</td>
                        <td class="Texto" style="font-weight:bold" >
                            </td>
                        <td>
                            <select id="cboTienda" name="D1" style="width: 192px">
                            <option></option>
                        </select></td>
                        <td class="Texto" style="font-weight:bold">
                            Área:</td>
                        <td>
                            <select id="cboArea" name="D1" style="width: 216px">
                            <option></option>
                        </select></td>
                        <td class="Texto" style="font-weight:bold; width: 85px;" >
                            Aprobación:</td>
                        <td class="style2" style="width: 3px">
                         
                         <select ID="cboAprobacion" style="width:229px; margin-left: 0px;" name="D2">
                          <option value="1">--TODOS---</option>
                                 <option value="2">Por Aprobar</option>
                                 <option value="3">Aprobado</option>
                                 <option value="4">FACTURAS PAGADOS</option>
                         </select>
                        </td>
                        <td style="width: 111px">
                            <input id="btnBuscar" style="width: 130px" type="button" value="Buscar" onclick="return btnBuscar_onclick(this)"/></td>

                    </tr>
                    <tr>
                        
                        <td colspan="8">
                        <table>
                        <tr>
                        <td style="color: #0000FF; background-color: #99CCFF;">FechaInicio :</td>
                         <td style="background-color: #99CCFF">
                            <asp:TextBox ID="TxtFechaInicio" runat="server" style="margin-left: 0px" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="txtfecha_CalendarExtender" runat="server" 
                                           TargetControlID="TxtFechaInicio"  Format="dd/MM/yyyy"  />
                            </td>
                          <td style="background-color: #99CCFF">&nbsp;</td>
                             <td style="color: #0000FF; text-align: right; background-color: #99CCFF;">Fecha Fin :</td>
                         <td style="background-color: #99CCFF"> <asp:TextBox ID="TxtFechaFin" runat="server" ></asp:TextBox>                                         
                                           <cc1:calendarextender ID="Calendarextender1" runat="server" 
                                           TargetControlID="TxtFechaFin"  Format="dd/MM/yyyy"  /></td>
                          <td style="width: 108px; background-color: #99CCFF;">&nbsp;</td>
                             <td style="width: 10px; background-color: #99CCFF"></td>
                         <td></td>
                          <td class="style2">
                              &nbsp;</td>
                        </tr>
                           <tr>
                        <td style="color: #0000FF; background-color: #FFFFCC;">Total Soles :</td>
                         <td style="background-color: #FFFFCC"> 
                            <input id="txttotalsoles" style="width: 129px" type="text" /></td>
                          <td style="background-color: #FFFFCC"></td>
                             <td style="text-align: right; background-color: #FFFFCC;"><span style="color: #0000FF">Total Dolares </span>:</td>
                         <td style="background-color: #FFFFCC"> 
                            <input id="txttotaldolar" style="width: 125px" type="text" /></td>
                          <td style="width: 108px; background-color: #FFFFCC;">
                              <input id="btnSumar" 
                                type="button" value="Ver Totales" style="width: 113px" 
                                  onclick="return btnSumar_onclick()" /></td>
                             <td style="width: 10px; background-color: #FFFFCC">&nbsp;</td>
                         <td>
                         &nbsp;</td>
                          <td class="style2">
                          </td>
                        </tr>

                        </table>
                        </td>
                        <td style="width: 111px">
                       
                       
                            <input id="btnExportar" style="width: 135px" type="button" value="Exportar a Excel"  
                                onclick="return btnExportar_onclick(this)"/></td>
                     
                    </tr>
                </table>
            </td>
            
        </tr>
        <tr>
            <td style="width: 350px"><input id="chkCambioFecha" type="checkbox" onchange="return MarcaroDesmarcarFecha()" />Cambio de Fecha a Documentos </td>
        </tr>
        
        <tr>
            <td style="width: 350px"><input id="ChkGeneraRetencion" type="checkbox" onchange="return MarcaroDesmarcarGenerandoRetencion()" /><span 
                    style="color: #006699">Generar Retención a Documentos Programados</span>.</td>
        </tr>

        <tr>
            <td style="width: 870px">
             
                              <div id="CapaCambioFechaPago" 
                                style="width:450px;border:1px solid blue;background-color:White;z-index:99999; height: 50px; display: none;">

                            <table class="style1" style="background-color: #99FF66">
                                <tr>
                                    <td style="width: 180px">
                                        <strong>Fecha Reprogramacion :</strong></td>
                                    <td style="width: 128px">
                                        <asp:TextBox ID="TxtFechaReprogramacion" runat="server" 
                                            style="margin-left: 0px"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtTxtFechaReprogramacion_CalendarExtender" 
                                            runat="server" Format="dd/MM/yyyy" TargetControlID="TxtFechaReprogramacion" />
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="color: #006600; width: 180px">
                                        &nbsp;</td>
                                    <td style="width: 128px">
                                      
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="width: 180px">
                                        &nbsp;</td>
                                    <td style="width: 128px">
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </div></td>
                            </tr>

                              <tr>
            <td style="width: 870px">
             
                              <div id="CapaGeneraRetencion" 
                                style="width:450px;border:1px solid Skyblue;background-color:White;z-index:99999; height: 50px; display: none;">

                            <table class="style1" style="background-color: #99FF66">
                                <tr>
                                    <td style="width: 180px">
                                        <strong>Generando Retencion al documento a Marcar ...</strong></td>
                                    <td style="width: 128px">
                                  <%--      <asp:TextBox ID="TextBox1" runat="server" 
                                            style="margin-left: 0px"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender2" 
                                            runat="server" Format="dd/MM/yyyy" TargetControlID="TxtFechaReprogramacion" />--%>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="color: #006600; width: 180px">
                                        &nbsp;</td>
                                    <td style="width: 128px">
                                      
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="width: 180px">
                                        &nbsp;</td>
                                    <td style="width: 128px">
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </div></td>
                            </tr>
                            <tr>
                            
                                 
                                
                                <td style="height: 20px; width: 870px;">
                                 <div id="CapaGrillaDetalle">
                                 </div>
                                    </td>
                            </tr>
                            <tr>
                                <td style="width: 870px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width: 870px; height: 23px;">
                                    </td>
                            </tr>
                            <tr>
                                <td style="width: 870px; height: 23px;">
                                    </td>
                            </tr>
                            <tr>
                                <td style="width: 870px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width: 870px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                              <td style="width: 870px">
                                    <asp:HiddenField ID="hddIdPersona" runat="server" Value="" />
                                    <asp:HiddenField ID="hddIdDocumentoExt" runat="server" />
                                  <asp:HiddenField ID="hddNumDocu" runat="server" Value=""/>
                                  <asp:HiddenField ID="hddbeneficiario" runat="server" Value=""/>
                                  <asp:HiddenField ID="hddsupervisor" runat="server" Value=""/>
                                    <asp:HiddenField ID="hddestadoreque" runat="server" Value=""/>
                                    <asp:HiddenField ID="hddIdDocumento" runat="server" Value="" />
                                </td>
                               
                                
                            </tr>
                        </table>
                          <div  id="capaAprobarReqGasto"  
                            style="border: 3px solid blue; padding: 8px; width:680px; height:auto; position:absolute; background-color:white; z-index:2; display :none; top: 160px; left: 150px;">
                                                    <table style="width: 100%;">
                                                    <tr><td align="right">
                                                           <asp:ImageButton ID="btnCerrar_capaAprobarReqGasto" runat="server"  ToolTip="Cerrar"
                                                                    ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaAprobarReqGasto')   );" />   
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                          <asp:Panel ID="Panel_Aprobacion" runat="server">
                                <table width="100%" >
                            <tr>
                            <td class="SubTituloCelda">DATOS DE APROBACIÓN</td>
                            </tr>
                            <tr>
                            <td>
                            <table>
                            <tr>
                            <td>
                                <asp:RadioButtonList ID="rdbAutorizado" runat="server" 
                                    RepeatDirection="Horizontal" TextAlign="Right" CssClass="Texto" 
                                    Font-Bold="true" AutoPostBack="True" >
                                <asp:ListItem  Value="1" >Sí</asp:ListItem>
                                <asp:ListItem  Value="0" Selected="True" >No</asp:ListItem>            
                                </asp:RadioButtonList>
                            </td>
                            <td style="width:30px" ></td>
                            <td class="Texto" style="font-weight:bold">Fecha Aprobación:</td>
                            <td>
                            <asp:TextBox ID="txtFechaAprobacion" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" Width="180px" runat="server"></asp:TextBox>
                            </td>
                            <td  style="width:30px"  ></td>
                            <td>
                                <asp:CheckBox ID="chbEnviarCxP" runat="server" Text="Enviar a Cuenta por Pagar" CssClass="Texto" Font-Bold="true" Enabled="false" /></td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                            </table>
                                </asp:Panel>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                    <td style="text-align: center">
                                                    <table>
                                                    <tr>
                                                    <td>
                                                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="70px" ToolTip="Guardar" OnClientClick="return(  valOnClick_btnGuardar()  );" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnCerrar" runat="server" Text="Cerrar" Width="70px" ToolTip="Cerrar" OnClientClick=" return( offCapa('capaAprobarReqGasto') ); " />
                                                    </td>
                                                    </tr>
                                                    </table>
                                                    </td>
                                                    </tr>
                                     </table>                                  
                            </div>  
                                 
        <script language="javascript"  type="text/javascript" >
            window.onload = varios();


            function varios() {
                ConsultaComboTienda();
                ConsultaComboAreas();
            }

            function MarcaroDesmarcar() {

                  reprogramando();

              }

              function MarcaroDesmarcarFecha() {

                  reprogramandoFecha();

              }

              function MarcaroDesmarcarGenerandoRetencion() {
                  GenerandoRetencion();
              }

              function GenerandoRetencion(objeto, flag) {
                  var chkMarcarD = document.getElementById("ChkGeneraRetencion");

                  if (chkMarcarD.checked == false) {
                      CapaGeneraRetencion.style.display = 'none';

                  }
                  if (chkMarcarD.checked == true) {
                      CapaGeneraRetencion.style.display = 'block';
                  }

              }


              function reprogramandoFecha(objeto, flag) {
                  var chkMarcarD = document.getElementById("chkCambioFecha");

                  if (chkMarcarD.checked == false) {
                      CapaCambioFechaPago.style.display = 'none';

                  }
                  if (chkMarcarD.checked == true) {
                      CapaCambioFechaPago.style.display = 'block';
                  }

              }

             function reprogramando(objeto, flag) {
                 var chkMarcarD = document.getElementById("chkMarcar");
                 
                 if (chkMarcarD.checked == false) {
                    CapaCambioFechaPago.style.display = 'none';
        
                }
                if (chkMarcarD.checked == true) {               
                    CapaCambioFechaPago.style.display = 'block';
                }

             }

            function ConsultaComboTienda() {
                var opcion = "llenarComboTienda";
                var IdEmpresa = 1;
                var datacombo = new FormData();
                datacombo.append('flag', 'llenarComboTienda');
                datacombo.append('IdEmpresa', IdEmpresa);
                var xhrrep = new XMLHttpRequest();
                xhrrep.open("POST", "FrmAprobacionPagoRequerimientos.aspx?flag=" + opcion + "&IdEmpresa=" + IdEmpresa, true);
                xhrrep.onreadystatechange = function () {
                    if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                        mostrarListasCombos(xhrrep.responseText);
                    }
                }
                xhrrep.send(datacombo);
                return false;
            }

            
            function ConsultaComboAreas() {
                var opcion = "llenarComboArea";
                var IdEmpresa = 1;
                var datacomboAreas = new FormData();
                datacomboAreas.append('flag', 'llenarComboArea');
                datacomboAreas.append('IdEmpresa', IdEmpresa);
                var xhrrepArea = new XMLHttpRequest();
                xhrrepArea.open("POST", "FrmAprobacionPagoRequerimientos.aspx?flag=" + opcion + "&IdEmpresa=" + IdEmpresa, true);
                xhrrepArea.onreadystatechange = function () {
                    if (xhrrepArea.readyState == 4 && xhrrepArea.status == 200) {
                       mostrarListasComboArea(xhrrepArea.responseText);
                    }
                }
                xhrrepArea.send(datacomboAreas);
                return false;
            }
            

            function ConsultarCombos() {

            }

            function btnSumar_onclick()
            {
                ConsultarRegistrosSumar();
            }

            function btnBuscar_onclick(objeto) {
                ConsultarRegistros(objeto);
            }

            function btnExportar_onclick(objeto) {
                ConsultarRegistrosaExportar(objeto);
            }

            function btnCambiarFecha_onclick() {
//                ReprogramarPago();
            }

            function ReprogramarPago(objeto, flag) {
            var chkMarcarD = document.getElementById("chkCambioFecha");

            if (chkMarcarD.checked == true) { var opcion = "ReprogramarPago"; }
            if (chkMarcarD.checked == false) { var opcion = "ReprogramarPago"; }               

                var fechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
                var FechaFinal = document.getElementById('<%= TxtFechaFin.ClientID%>').value;
                var idtienda = document.getElementById("cboTienda").value;
                var idAprobado = document.getElementById("cboAprobacion").value;
                var idArea = document.getElementById("cboArea").value;

                var parts = FechaFinal.split('/');
                var mydate = parts[2];
                var periodo = mydate;

                var datarep = new FormData();
                datarep.append('flag', 'ReprogramarPago');
                datarep.append('fechaInicio', fechaInicio);
                datarep.append('FechaFinal', FechaFinal);
                datarep.append('idtienda', idtienda);
                datarep.append('idAprobado', idAprobado);
                datarep.append('idArea', idArea);


                var xhr = new XMLHttpRequest();
                xhr.open("POST", "FrmAprobacionPagoRequerimientos.aspx?flag=" + opcion + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&idtienda=" + idtienda + "&idAprobado=" + idAprobado + "&idArea=" + idArea, true);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {

                        //fun_lista(xhr.responseText);

                    }
                }
                xhr.send(null);
                return false;

            }

            function ConsultarRegistrosaExportar(objeto) {
                var opcion = "ConsultaDocumentosaExportar";
                var fechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
                var FechaFinal = document.getElementById('<%= TxtFechaFin.ClientID%>').value;
                var idtienda = document.getElementById("cboTienda").value;
                var idAprobado = document.getElementById("cboAprobacion").value;
                var idArea = document.getElementById("cboArea").value;

                var parts = FechaFinal.split('/');
                var mydate = parts[2];
                var periodo = mydate;

                var datarep = new FormData();
                datarep.append('flag', 'ConsultaDocumentosaExportar');
                datarep.append('fechaInicio', fechaInicio);
                datarep.append('FechaFinal', FechaFinal);
                datarep.append('idtienda', idtienda);
                datarep.append('idAprobado', idAprobado);
                datarep.append('idArea', idArea);

                var xhr = new XMLHttpRequest();
                xhr.open("POST", "FrmAprobacionPagoRequerimientos.aspx?flag=" + opcion + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&idtienda=" + idtienda + "&idAprobado=" + idAprobado + "&idArea=" + idArea, true);
                xhr.onloadstart = function () { objeto.disabled = true; objeto.value = "Exportando..."; }
                xhr.onloadend = function () { objeto.disabled = false; objeto.value = "Exportar a Excel"; }

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        fun_listaaExportar(xhr.responseText);
                    }
                }
                xhr.send(null);
                return false;
            }

            function ConsultarRegistros(objeto) {
                var opcion = "ConsultaDocumentos";          
                var fechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
                var FechaFinal = document.getElementById('<%= TxtFechaFin.ClientID%>').value;
                var idtienda = document.getElementById("cboTienda").value;
                var idAprobado = document.getElementById("cboAprobacion").value;
                var idArea = document.getElementById("cboArea").value;

                var parts = FechaFinal.split('/');
                var mydate = parts[2];
                var periodo = mydate;

                var datarep = new FormData();
                datarep.append('flag', 'ConsultaDocumentos');
                datarep.append('fechaInicio', fechaInicio);
                datarep.append('FechaFinal', FechaFinal);
                datarep.append('idtienda', idtienda);
                datarep.append('idAprobado', idAprobado);
                datarep.append('idArea', idArea);

                var xhr = new XMLHttpRequest();
                xhr.open("POST", "FrmAprobacionPagoRequerimientos.aspx?flag=" + opcion + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&idtienda=" + idtienda + "&idAprobado=" + idAprobado + "&idArea=" + idArea, true);
                xhr.onloadstart = function () { objeto.disabled = true; objeto.value = "Procesando..."; }
                xhr.onloadend = function () { objeto.disabled = false; objeto.value = "Consultar"; }
               
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        fun_lista(xhr.responseText);
                    }
                }
                xhr.send(null);
                return false;
            }

            function ConsultarRegistrosSumar(objeto, flag) {
                var opcion = "ConsultarRegistrosSumar";

                var fechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
                var FechaFinal = document.getElementById('<%= TxtFechaFin.ClientID%>').value;
                var idtienda = document.getElementById("cboTienda").value;
                var idAprobado = document.getElementById("cboAprobacion").value;
                var idArea = document.getElementById("cboArea").value;

                var parts = FechaFinal.split('/');
                var mydate = parts[2];
                var periodo = mydate;

                var datarep = new FormData();
                datarep.append('flag', 'ConsultarRegistrosSumar');
                datarep.append('fechaInicio', fechaInicio);
                datarep.append('FechaFinal', FechaFinal);
                datarep.append('idtienda', idtienda);
                datarep.append('idAprobado', idAprobado);
                datarep.append('idArea', idArea);


                var xhr = new XMLHttpRequest();
                xhr.open("POST", "FrmAprobacionPagoRequerimientos.aspx?flag=" + opcion + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&idtienda=" + idtienda + "&idAprobado=" + idAprobado + "&idArea=" + idArea, true);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                       fun_listasuma(xhr.responseText);
                         
                    }
                }
                xhr.send(null);
                return false;
            }

            function fun_lista(lista) {
                filas = lista.split("▼");
                crearTabla();
                crearMatriz();
                mostrarMatriz();
                configurarFiltros();
            }

            function fun_listaaExportar(listaExp) {

                filasexp = listaExp.split("▼");
                
//               crearTablaaExportar();
               exportandoexcel();

           }

           function exportandoexcel() {
               crearTablaaExportar();
               var blob = new Blob([excel], { type: 'application/vnd.ms-excel' });
               if (navigator.appVersion.toString().indexOf('.NET') > 0) {
                   window.navigator.msSaveBlob(blob, "Reporte.xls");
               }
               else {
                   this.download = "Reporte.xls";
                   this.href = window.URL.createObjectURL(blob);
               }
           }

            function fun_listasuma(lista) {
                filas = lista.split("▼");


                document.getElementById("txttotalsoles").value = filas[0];
                document.getElementById("txttotaldolar").value = filas[1];

//                crearTabla();
//                crearMatriz();
//                mostrarMatriz();
//                configurarFiltros();
            }


            function crearTabla() {
                var nRegistros = filas.length;
                var cabeceras = ["Aprobar", "FechaAprobacion", "Area", "Moneda", "NroProgramacion", "DescripcionPersona", "FechaEmision", "FechaPago", "NroDocumento", "TipoDocumento", "TotalAPagar","M.Detraccion", "M.Retencion","Pago","Banco"];
                var nCabeceras = cabeceras.length;
                var contenido = "<table><thead>";
                contenido += "<tr class='GrillaHeader'>";
                for (var j = 0; j < nCabeceras; j++) {
                    contenido += "<th>";
                    contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
                    contenido += "</th>";
                }
                contenido += "</tr></thead><tbody id='tbDocumentos' class='GrillaRow'></tbody>";
                contenido += "</table>";
                var div = document.getElementById("CapaGrillaDetalle");
                div.innerHTML = contenido;
            }



            function crearTablaaExportar() {
                crearMatrizaExportar();
                excel = "<html><head><meta charset='UTF-8'></meta></head>"              
                var nRegistros = matrizExp.length;
              if (nRegistros > 0){
                    var cabeceras = ["Area", "Moneda", "NroProgramacion", "DescripcionPersona", "FechaEmision", "FechaPago", "NroDocumento", "TipoDocumento", "TotalAPagar", "M.Detraccion", "M.Retencion", "Pago", "Banco","NroOperacion"];
                    var nCampos = cabeceras.length;
                     excel += "<table><thead><tr>";
                     for (var j = 0; j < nCampos; j++) {
                         excel += "<th style='background-color:blue;color:white;'>";
                         excel += cabeceras[j];
                         excel += "</th>";
                     }                  
                 excel += "</tr></thead><tbody>";
                 for (var i = 0; i < nRegistros; i++) {
                     excel += "<tr>";
                     for (var j = 0; j < nCampos; j++) {
                         excel += "<td style='background-color:white;color:blue;'>";
                         excel += matrizExp[i][j];
                         excel += "</td>";
                     }
                     excel += "</tr>";
                 }
                 excel+= "</tbody></table></body></html>"; 
            
            }
 }

 function verificar(iddocumento, idrequerimiento) {
     var chkMarc = document.getElementById("ChkGeneraRetencion");

     if (chkMarc.checked == false) {

         var chkMarcarD = document.getElementById("chkCambioFecha");

         if (chkMarcarD.checked == true) {
             var opcion = "ReprogramarPago";

         }
         if (chkMarcarD.checked == false) {
             var opcion = "UpdateRequerimiento";
         }

         var IdEstado = null;
         var FechaReprogramada = document.getElementById('<%= TxtFechaReprogramacion.ClientID%>').value;
         var fechaInicio = document.getElementById('<%= TxtFechaInicio.ClientID%>').value;
         var FechaFinal = document.getElementById('<%= TxtFechaFin.ClientID%>').value;
         var idtienda = document.getElementById("cboTienda").value;
         var idAprobado = document.getElementById("cboAprobacion").value;
         var idArea = document.getElementById("cboArea").value;

         var iddocumento = iddocumento
         var idrequerimiento = idrequerimiento
         var parts = FechaFinal.split('/');
         var mydate = parts[2];
         var periodo = mydate;

         var datarep = new FormData();
         datarep.append('flag', opcion);
         datarep.append('fechaInicio', fechaInicio);
         datarep.append('FechaFinal', FechaFinal);
         datarep.append('idtienda', idtienda);
         datarep.append('idAprobado', idAprobado);
         datarep.append('idArea', idArea);
         datarep.append('iddocumento', iddocumento);
         datarep.append('idrequerimiento', idrequerimiento);
         datarep.append('FechaReprogramada', FechaReprogramada);
         var xhrrep = new XMLHttpRequest();
         // xhrrep.open("POST", "FrmAprobacionPagoRequerimientos.aspx?flag=" + opcion + "&FechaInicio=" + FechaInicio + "&FechaFin=" + FechaFin + "&iddocumento=" + iddocumento, true);
         xhrrep.open("POST", "FrmAprobacionPagoRequerimientos.aspx?flag=" + opcion + "&fechaInicio=" + fechaInicio + "&FechaFinal=" + FechaFinal + "&idtienda=" + idtienda + "&idAprobado=" + idAprobado + "&idArea=" + idArea + "&iddocumento=" + iddocumento + "&idrequerimiento=" + idrequerimiento + "&FechaReprogramada=" + FechaReprogramada, true);

         xhrrep.onreadystatechange = function () {
             if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                 // fun_lista(xhrrep.responseText);

             }
         }
         xhrrep.send(datarep);
         return false;

     }
     if (chkMarc.checked == true) {

         var opcion = "UpdateRequerimientoRetencion";


         var datarept = new FormData();
         datarept.append('flag', opcion);
//         datarep.append('fechaInicio', fechaInicio);
//         datarep.append('FechaFinal', FechaFinal);
//         datarep.append('idtienda', idtienda);
//         datarep.append('idAprobado', idAprobado);
//         datarep.append('idArea', idArea);
         datarept.append('iddocumento', iddocumento);
         datarept.append('idrequerimiento', idrequerimiento);
//         datarep.append('FechaReprogramada', FechaReprogramada);
         var xhrrep = new XMLHttpRequest();
         // xhrrep.open("POST", "FrmAprobacionPagoRequerimientos.aspx?flag=" + opcion + "&FechaInicio=" + FechaInicio + "&FechaFin=" + FechaFin + "&iddocumento=" + iddocumento, true);
         xhrrep.open("POST", "FrmAprobacionPagoRequerimientos.aspx?flag=" + opcion + "&iddocumento=" + iddocumento + "&idrequerimiento=" + idrequerimiento , true);

         xhrrep.onreadystatechange = function () {
             if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                 // fun_lista(xhrrep.responseText);

             }
         }
         xhrrep.send(datarept);
         return false;














     }

  }


            function crearMatrizaExportar() {
                matrizExp = [];
                var nRegistros = filasexp.length;
                var nCampos;
                var campos;
                var c = 0;
                var exito;
                var textos = document.getElementsByClassName("texto");
                var nTextos = textos.length;
                var texto;
                var x;
                for (var i = 0; i < nRegistros; i++) {
                    exito = true;
                    campos = filasexp[i].split("|");
                    nCampos = campos.length;
                    for (var j = 0; j < nTextos; j++) {
                        texto = textos[j];
                        x = j;

                        if (texto.value.length > 0) {
                            exito = campos[x].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        }

                        if (!exito) break;

                    }
                    if (exito) {
                        matrizExp[c] = [];
                        for (var j = 0; j < nCampos; j++) {
                            if (isNaN(campos[j])) matrizExp[c][j] = campos[j];

                            else matrizExp[c][j] = campos[j];
                        }
                        c++;
                    }
                }
            }

            function crearMatriz() {
                matriz = [];
                var nRegistros = filas.length;
                var nCampos;
                var campos;
                var c = 0;
                var exito;
                var textos = document.getElementsByClassName("texto");
                var nTextos = textos.length;
                var texto;
                var x;
                for (var i = 0; i < nRegistros; i++) {
                    exito = true;
                    campos = filas[i].split("|");
                    nCampos = campos.length;
                    for (var j = 0; j < nTextos; j++) {
                        texto = textos[j];
                        x = j;

                        if (texto.value.length > 0) {
                            exito = campos[x].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        }


                        if (!exito) break;
                    }
                    if (exito) {
                        matriz[c] = [];
                        for (var j = 0; j < nCampos; j++) {
                            if (isNaN(campos[j])) matriz[c][j] = campos[j];

                            else matriz[c][j] = campos[j];
                        }
                        c++;
                    }
                }
            }


            function mostrarMatriz() {
                var nRegistros = matriz.length;
                var contenido = "";
                if (nRegistros > 0) {
                    var nCampos = matriz[0].length;
                    for (var i = 0; i < nRegistros; i++) {

                        if (matriz[i][20] == 0) {
                            contenido += "<tr bgcolor='#CEF6D8'>";
                        }
//                        if (matriz[i][0] != 0) {
//                            contenido += "<tr bgcolor='#00FF99'>";
                        //                        }

                        if (matriz[i][20] == 1) {
                            contenido += "<tr bgcolor='#F5A9A9'>";
                        }

                        if (matriz[i][20] == 2) {
                            contenido += "<tr bgcolor='#FAAC58'>";
                        }
                        // ncampos -5
                        for (var j = 0; j < nCampos-6; j++) {
                            var cadenaid
                            var cadenareq
                            cadenaid = '' + matriz[i][17] + '' ;
                            cadenareq = '' + matriz[i][16] + '';
                            contenido += "<td>";
                            if (j == 0) {
                                if (matriz[i][0] == 0) {
                                    contenido += "<input type='checkbox' value='checkbox'  onchange='return verificar(" + '"' + cadenaid + '"' + ',' + '"' + cadenareq + '"' + ");' />"
                                }
                                if (matriz[i][0] == 1) {
                                    contenido += "<input type='checkbox' value='checkbox' checked = true onchange='return verificar(" + '"' + cadenaid + '"' + ',' + '"' + cadenareq + '"' + ");'/>"
                                }

                            }

                            if (j > 0) {
                                contenido += matriz[i][j];
                            }
                            contenido += "</td>";

                        }
                        contenido += "</tr>";
                    }
                }
                var spnMensaje = document.getElementById("nroFilas");
                var tabla = document.getElementById("tbDocumentos");
                tabla.innerHTML = contenido;
            }

            function configurarFiltros() {
                var textos = document.getElementsByClassName("texto");
                var nTextos = textos.length;
                var texto;
                for (var j = 0; j < nTextos; j++) {
                    texto = textos[j];
                    texto.onkeyup = function () {
                        crearMatriz();
                        mostrarMatriz();
                    }
                }
            }

            function mostrarListasCombos(rptacombo) {
                if (rptacombo != "") {
                    var listas2 = rptacombo;
                    data1 = crearArray(listas2, "▼");
                    listarcombos();
                }
                return false;
            }

            function mostrarListasComboArea(rptacombo) {
                if (rptacombo != "") {
                    var listaArea = rptacombo;
                    dataArea = crearArray(listaArea, "▼");
                    listarcomboArea();
                }
                return false;
            }

            function listarcomboArea() {
                var Area = [];
                var nRegistros = dataArea.length;
                var campos2;
                var TipoServ;
                for (var i = 0; i < nRegistros; i++) {
                    campos2 = dataArea[i].split("|");
                    {
                        TipoServ = campos2[0];
                        TipoServ += "|";
                        TipoServ += campos2[1];
                        Area.push(Area);
                    }
                }
                crearCombo(dataArea, "cboArea", "|");
            }

            function listarcombos() {
                var TipoServicio = [];
                var nRegistros = data1.length;
                var campos2;
                var TipoServ;
                for (var i = 0; i < nRegistros; i++) {
                    campos2 = data1[i].split("|");
                    {
                        TipoServ = campos2[0];
                        TipoServ += "|";
                        TipoServ += campos2[1];
                        TipoServicio.push(TipoServicio);
                    }
                }
                crearCombo(data1, "cboTienda", "|");
            }

            function crearCombo(lista, nombreCombo, separador, nombreItem, valorItem) {
                var contenido = "";
                if (nombreItem != null && valorItem != null) {
                    contenido += "<option value='";
                    contenido += valorItem;
                    contenido += "'>";
                    contenido += nombreItem;
                    contenido += "</option>";
                }
                var nRegistros = lista.length;
                if (nRegistros > 0) {
                    var campos;
                    for (var i = 0; i < nRegistros; i++) {
                        campos = lista[i].split(separador);
                        contenido += "<option value='";
                        contenido += campos[0];
                        contenido += "'>";
                        contenido += campos[1];
                        contenido += "</option>";
                    }
                }
                var cbo = document.getElementById(nombreCombo);
                if (cbo != null) cbo.innerHTML = contenido;

            }

            function crearArray(lista, separador) {
                var data = lista.split(separador);
                if (data.length === 1 && data[0] === "") data = [];
                return data;
            }
               
        
        </script>       
</asp:Content>
