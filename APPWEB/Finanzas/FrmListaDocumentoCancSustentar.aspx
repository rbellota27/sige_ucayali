<%@ Page Title="" Language="vb" AutoEventWireup="false"
    MasterPageFile="~/Principal.Master" CodeBehind="FrmListaDocumentoCancSustentar.aspx.vb" Inherits="APPWEB.FrmListaDocumentoCancSustentar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
      
        <tr>
            <td class="TituloCelda">
               LISTADO DE DOCUMENTOS  POR SUSTENTAR
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server" >
                    <table >
                        <tr>
                            <td align="right" class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                           
                           
                           <td></td>
                           <td></td>
                           <td>
                           <asp:Button ID="btnBuscarBanco" runat="server" Text="Buscar Reporte" />
                           </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Inicio:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                                </cc1:CalendarExtender>
                           </td>
                          <td align="right" class="Texto">
                                Fecha de Fin:
                            </td>
                            <td>
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaFin" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaFin" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                                </cc1:CalendarExtender>
                            </td>
                        </tr>

              
        
                        
                         </table>
                 </asp:Panel>   
                         </td>
      
                 
    </tr>
            
       <tr>
            <td class="TituloCelda">
                BENEFICIARIO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Persona" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Descripci�n:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionPersona" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                    Width="400px" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersona" OnClientClick="return( mostrarCapaPersona()  );"
                                    runat="server" Text="Buscar Ben." ToolTip="Buscar beneficiario" />
                            </td>
                            <td>
                                 <asp:ImageButton ID="btnLimpiarMaestroObra" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                    onmouseout="this.src='/Imagenes/Limpiar_B.JPG';" onmouseover="this.src='/Imagenes/Limpiar_A.JPG';"
                                                    OnClientClick="return ( LimpiarMaestroObra() );" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDni" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRuc" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto" colspan="4">
                                &nbsp;</td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>        
    </table>
    <table width="100%">
         <tr>
                        <td class="SubTituloCelda">
                          LISTADO 
                        </td>
                    </tr>
         <tr>
                        <td>
                            <asp:Panel ID="Panel_DocumentoRef" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_Deudas" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                              <Columns>
                            <asp:TemplateField ShowHeader="False" HeaderText="Opcines"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" >
                            <ItemTemplate>
                                          <asp:Button id="btnSustentar" Text="Sustentar" runat="server" OnClientClick="return confirm('�Esta seguro de sustentar este documento?');"
                                          CommandArgument='<%# Container.DataItemIndex%>'  CommandName="Insert"/>
                           
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            
                        </asp:TemplateField>  
                        
                           <asp:TemplateField ShowHeader="False" HeaderText="Documento"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" >
                            <ItemTemplate>
                                           <asp:Label ID="lbldocumento" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomTipoDocumento")%>' Font-Bold="true" style="color:Red " ></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            
                        </asp:TemplateField> 
                        
                        <asp:TemplateField HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" HeaderText="Nro. Documento" 
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                             <asp:Label ID="lblserie" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>' style="color:Red "  Font-Bold="true"></asp:Label>
                                             
                                        </td>
                                         <td>
                                            <asp:HiddenField ID="hddIdDocumentoRef" runat="server" 
                                                Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>

                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                     </asp:TemplateField>
                       <asp:TemplateField HeaderStyle-Height="25px" 
                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Tienda" 
                                        ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbltienda" runat="server" Font-Bold="true" 
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>
                                                </td>
                                               
                                            </tr>
                                        </table>
                                </ItemTemplate>

                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                   </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" HeaderText="Fecha de Emisi�n">
                           <ItemTemplate>
                              <asp:Label ID="lblfechaemision" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FechaEmision","{0:dd/MM/yyyy}")%>' ></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField> 
                        
                          <asp:TemplateField ShowHeader="False" HeaderText="Fecha de Vencimiento">
                           <ItemTemplate>
                              <asp:Label ID="lblfechavencimiento" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FechaVenc","{0:dd/MM/yyyy}")%>' ></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField> 
                     
                     

                  <asp:TemplateField ShowHeader="False" HeaderText="Beneficiario">
                            <ItemTemplate>
                                           <asp:Label ID="lbldescpersona" runat="server" Font-Bold="true" 
                                            Text='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>     
                          
 
                       <asp:TemplateField ShowHeader="False" HeaderText="Monto" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                           <ItemTemplate>
                           <asp:Label ID="lblmoneda" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>' ></asp:Label>
                              <asp:Label ID="lblmonto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MontoTotal")%>' ></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField> 
                        
                         <asp:TemplateField ShowHeader="False" HeaderText="Saldo" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                           <ItemTemplate>
                           <asp:Label ID="lblmoneda2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>' ></asp:Label>
                              <asp:Label ID="lblsaldo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Saldo")%>' ></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>    
                         
                        <%--    <asp:TemplateField ShowHeader="False" HeaderText="Doc Referencia" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                           <ItemTemplate>
                              <asp:Label ID="lblestado" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomEstadoDocumento")%>' ></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>  --%>

                                        </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    
                                </table>
                                <table >
                              
                                </table>
                             
                            </asp:Panel>
                        </td>
                    </tr>
                    
    </table>
          
           <table>

        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
       
    </table>
 
  
   
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>

            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de b�squeda realiza un filtrado de acuerdo al destinatario seleccionado.
                </td>
            </tr>
        </table>
    </div>
     <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
                                <asp:HiddenField ID="hddFrmModo" runat="server" />
                                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                                <asp:HiddenField ID="hddIdPersona" runat="server" Value="0"  />
                                 <asp:HiddenField ID="hddIdDocumento" runat="server" />
    <script language="javascript" type="text/javascript">
        //***********************************

        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }
 
                
    </script>

    <script language="javascript" type="text/javascript">

//        function SaveGuardar() {
//            mensaje = 'Esta seguro que desea sustentar este documento ?';
//            return confirm(mensaje);
//        }
        
        function LimpiarMaestroObra() {
            document.getElementById('<%=txtDescripcionPersona.ClientID %>').value = '';
            document.getElementById('<%=txtDni.ClientID %>').value = '';
            document.getElementById('<%=txtRuc.ClientID %>').value = '';
            document.getElementById('<%=hddIdPersona.ClientID %>').value = '0';
            return false;
        }
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
