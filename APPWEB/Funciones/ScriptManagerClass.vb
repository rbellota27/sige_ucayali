﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Public Class ScriptManagerClass
    Public Sub mostrarMsjAlerta(ByVal page As Page, ByVal texto As String)        
        Dim s As String = texto.Replace("'", "").Replace(vbCrLf, " ").Replace(vbCr, " ")
        ScriptManager.RegisterStartupScript(page, page.GetType, "onLoad", "mostrarMsjAlerta('" & s & "');", True)
    End Sub
    Public Sub offCapa(ByVal page As Page, ByVal nomCapa As String)
        ScriptManager.RegisterStartupScript(page, page.GetType, "onLoad", "offCapa('" & nomCapa & "');", True)
    End Sub
    Public Sub onCapa(ByVal page As Page, ByVal nomCapa As String)
        ScriptManager.RegisterStartupScript(page, page.GetType, "onLoad", "onCapa('" & nomCapa & "');", True)
    End Sub
    'Public Sub calcularTotales(ByVal page As Page)
    '    ScriptManager.RegisterStartupScript(page, page.GetType, "onLoad", "calcularImporte('-1');", True)
    'End Sub
    'Public Sub createCustomAlert(ByVal page As Page, ByVal texto As String)
    '    ScriptManager.RegisterStartupScript(page, page.GetType, "onLoad", "createCustomAlert('" & texto & "');", True)
    'End Sub
End Class
