﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Linq
Imports System.Web.UI.WebControls

Public Class Combo
    Public Sub SelectEscalaTienda(ByVal cbo As DropDownList)
        Dim obj As New Negocio.EmpresaTiendaUsuario
        Dim lista As List(Of Entidades.Tienda) = obj.SelectEscalaTienda

        cbo.DataTextField = "NombreEscala"
        cbo.DataValueField = "IdEscala"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboAtencion(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Usuario
        Dim lista As List(Of Entidades.Usuario) = obj.SelectCboAtencion

        cbo.DataTextField = "Nombres"
        cbo.DataValueField = "IdPersona"
        cbo.DataSource = lista
        cbo.DataBind()
        cbo.Items.Insert(0, New ListItem("------", "0"))

    End Sub

    Public Sub LlenarCboTiendaxObjetivo(ByVal cbo As DropDownList)
        Dim obj As New Negocio.EmpresaTiendaUsuario
        Dim lista As List(Of Entidades.Tienda) = obj.SelectTiendaObjetivo

        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LlenarCbo_TipoAlmacen(ByVal cbo As DropDownList)

        Dim obj As New Negocio.TipoAlmacen
        Dim lista As List(Of Entidades.TipoAlmacen) = obj.SelectxIdTipoAlmacen
        Dim objTipoAlmacen As New Entidades.TipoAlmacen

        cbo.DataTextField = "Tipoalm_Nombre"
        cbo.DataValueField = "IdTipoAlmacen"
        cbo.DataSource = lista
        cbo.DataBind()

    End Sub

    Public Sub LlenarCboTotalPag_DetalleDocumento(ByVal cbo As DropDownList, ByVal IdDocumento As Integer, ByVal PageSize As Integer, ByVal addElement As Boolean)

        Dim FinPage As Integer = (New Negocio.DetalleDocumento).fn_TotalPag_DetalleDocumento(IdDocumento, PageSize)
        Dim IncioPage As Integer = 1

        cbo.Items.Clear()
        For x As Integer = IncioPage To FinPage
            cbo.Items.Add(New ListItem(CStr(x), CStr(x)))
        Next

        If addElement Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If

    End Sub

    Public Sub LlenarCboTotalPag_LineaxSubLinea(ByVal cbo As DropDownList, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal PageSize As Integer, ByVal addElement As Boolean)

        Dim FinPage As Integer = (New Negocio.SubLinea).fn_TotalPag_LineaxSubLinea(IdLinea, IdSubLinea, PageSize)
        Dim IncioPage As Integer = 1

        cbo.Items.Clear()
        For x As Integer = IncioPage To FinPage
            cbo.Items.Add(New ListItem(CStr(x), CStr(x)))
        Next

        If addElement Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If

    End Sub


    Public Sub LlenarCboCajaxIdTiendaxIdUsuarioNew(ByVal cbo As DropDownList, ByVal IdTienda As Integer, ByVal IdUsuario As Integer, ByVal addElement As Boolean)

        Dim lista As List(Of Entidades.Caja) = (New Negocio.CajaUsuario).SelectxIdPersona(IdUsuario)

        If IdTienda > 0 And Not IsNothing(lista) Then
            cbo.DataSource = lista.FindAll(Function(xCaja As Entidades.Caja) xCaja.IdTienda = IdTienda)
        Else
            cbo.DataSource = lista
        End If

        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdCaja"
        cbo.DataBind()

        If addElement Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If

    End Sub

    Public Sub LlenarCbo_CajaxId(ByVal cbo As DropDownList, ByVal IdCaja As Integer, Optional ByVal addElement As Boolean = False)

        Dim Lista As New List(Of Entidades.Caja)
        Lista.Add((New Negocio.Caja).SelectCajaxId(IdCaja))

        With cbo

            .DataSource = Lista
            .DataValueField = "IdCaja"
            .DataTextField = "Nombre"
            .DataBind()

            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If

        End With

    End Sub

    Public Sub LlenarCbo_ConceptoMovBanco_MedioPago(ByVal cbo As DropDownList, ByVal IdMedioPago As Integer, ByVal IdTipoConceptoBanco As Integer, Optional ByVal Abv As Boolean = False, Optional ByVal addElement As Boolean = False)

        With cbo
            .DataSource = (New Negocio.ConceptoMovBanco).ConceptoMovBanco_MedioPago(IdMedioPago, IdTipoConceptoBanco)

            .DataValueField = "Id"

            If Abv Then
                .DataTextField = "DescripcionBreve"
            Else
                .DataTextField = "Nombre"
            End If

            .DataBind()

            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If

        End With

    End Sub

    Public Sub LlenarCbo_Banco_CuentaBancaria(ByVal cbo As DropDownList, ByVal IdPersona As Integer, Optional ByVal Abv As Boolean = False, Optional ByVal addElement As Boolean = False)

        With cbo
            .DataSource = (New Negocio.Banco).Banco_CuentaBancaria(IdPersona)
            .DataValueField = "Id"

            If Abv Then
                .DataTextField = "Abrev"
            Else
                .DataTextField = "Nombre"
            End If

            .DataBind()

            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If

        End With

    End Sub

    Public Sub LlenarCbo_MedioPago_MovBanco(ByVal cbo As DropDownList, ByVal IdTipoConceptoBanco As Integer, Optional ByVal addElement As Boolean = False)

        With cbo
            .DataSource = (New Negocio.MedioPago).MedioPago_MovBanco(IdTipoConceptoBanco)
            .DataValueField = "Id"
            .DataTextField = "Nombre"
            .DataBind()

            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If

        End With

    End Sub


    Public Sub LlenarCboTarjetaxTipoTarjetaCaja(ByVal cbo As DropDownList, ByVal IdTipoTarjeta As Integer, ByVal IdCaja As Integer, Optional ByVal addElement As Boolean = False)

        With cbo
            .DataSource = (New Negocio.TarjetaView).SelectCboxTipoTarjetaCaja(IdTipoTarjeta, IdCaja)
            .DataTextField = "Nombre"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboTipoTarjetaxIdCaja(ByVal cbo As DropDownList, ByVal IdCaja As Integer, Optional ByVal addElement As Boolean = False)

        With cbo
            .DataSource = (New Negocio.TipoTarjeta).SelectCboxIdCaja(IdCaja)
            .DataTextField = "Nombre"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub


    Public Sub llenarCboCampania(ByVal cbo As DropDownList, _
                                                  ByVal IdEmpresa As Integer, _
                                                  ByVal IdTienda As Integer, _
                                                  Optional ByVal addElemento As Boolean = False)

        cbo.DataSource = (New Negocio.Campania).Campania_SelectxParams(0, IdEmpresa, IdTienda, "", Nothing, Nothing, True)


        cbo.DataTextField = "cam_Descripcion"
        cbo.DataValueField = "IdCampania"
        cbo.DataBind()

        If (addElemento) Then
            cbo.Items.Insert(0, (New ListItem("-----", "0")))
        End If

    End Sub

    Public Sub llenarCboRelacionDocumentoxIdTipoDocumento(ByVal cbo As DropDownList, _
                                                  ByVal IdDocumento As Integer, _
                                                  ByVal IdTipoDocumento As Integer, _
                                                  Optional ByVal addElemento As Boolean = False)

        cbo.DataSource = (New Negocio.RelacionDocumento).SelectxIdDocumentoxIdTipoDocumento(IdDocumento, IdTipoDocumento)

        cbo.DataTextField = "Codigo"
        cbo.DataValueField = "Id"
        cbo.DataBind()

        If (addElemento) Then
            cbo.Items.Insert(0, (New ListItem("-----", "0")))
        End If

    End Sub

    ''' <summary>
    '''  Opcion: 1 --> descripcion corta. 2 --> descripcion larga.
    ''' </summary>    
    Public Sub llenarCboTipoDocumentoxIdTipoDocumentos(ByVal cbo As DropDownList, _
                         ByVal IdTipoDocumento As String, Optional ByVal opcion As Integer = 1, Optional ByVal addElemento As Boolean = False)


        cbo.DataSource = (New Negocio.TipoDocumento).SelectTipoDocumentoxIdTipoDocumento(IdTipoDocumento)

        If opcion = 1 Then
            cbo.DataTextField = "DescripcionCorto"
        End If
        If opcion = 2 Then
            cbo.DataTextField = "Descripcion"
        End If

        cbo.DataValueField = "Id"
        cbo.DataBind()

        If (addElemento) Then
            cbo.Items.Insert(0, (New ListItem("-----", "0")))
        End If

    End Sub

    Public Sub llenarCboTelefonoxPersonaDato(ByVal cbo As DropDownList, ByVal Lista As List(Of Entidades.Telefono), Optional ByVal addElement As Boolean = False)

        cbo.DataSource = Lista

        cbo.DataValueField = "IdTipoTelefono"
        cbo.DataTextField = "Numero"
        cbo.DataBind()

        If (addElement) Or cbo.Items.Count = 0 Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If

    End Sub

    Public Sub llenarCboTipoAgentexIdPersona(ByVal cbo As DropDownList, ByVal IdPersona As Integer, Optional ByVal addElement As Boolean = False)

        cbo.DataSource = (New Negocio.PersonaTipoAgente).PersonaTipoAgente_Select(IdPersona)

        cbo.DataValueField = "IdAgente"
        cbo.DataTextField = "Descripcion"
        cbo.DataBind()

        If (addElement) Or cbo.Items.Count = 0 Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If

    End Sub

    Public Sub llenarCboContactoxIdPersona(ByVal cbo As DropDownList, ByVal IdPersona As Integer, Optional ByVal addIdUsuario As Integer = 0, Optional ByVal addElement As Boolean = False)
        cbo.DataSource = (New Negocio.OrdenCompra).listarContactoxIdPersona(IdPersona, addIdUsuario)

        cbo.DataValueField = "IdPersona"
        cbo.DataTextField = "Nombres"
        cbo.DataBind()

        If (addElement) Or cbo.Items.Count = 0 Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If

    End Sub

    Public Sub llenarCboCorreoxIdPersona(ByVal cbo As DropDownList, ByVal IdPersona As Integer, Optional ByVal addElement As Boolean = False)
        cbo.DataSource = (New Negocio.CorreoView).SelectxIdPersona(IdPersona)

        cbo.DataValueField = "Id"
        cbo.DataTextField = "Nombre"
        cbo.DataBind()

        If (addElement) Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If

    End Sub

    Public Sub llenarCboTipoComision(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        cbo.DataSource = (New Negocio.TipoComision).SelectAllActivo()

        cbo.DataValueField = "IdTipoComision"
        cbo.DataTextField = "Nombre"
        cbo.DataBind()

        If (addElement) Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If

    End Sub

    Public Sub llenarCboComsionCab(ByVal cbo As DropDownList, Optional ByVal IdTipoComision As Integer = 0, Optional ByVal addelement As Boolean = False)

        cbo.DataSource = (New Negocio.Comision_Cab).Comision_Cab_cbo(IdTipoComision)

        cbo.DataValueField = "IdComisionCab"
        cbo.DataTextField = "Descripcion"
        cbo.DataBind()

        If (addelement) Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If

    End Sub

    Public Sub llenarCboValorReferencia_Transporte(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)

        cbo.DataSource = (New Negocio.ValorReferencia_Transporte).SelectxParams_DT(Nothing, True)

        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "IdValorReferenciaT"
        cbo.DataBind()

        If (addElemento) Then
            cbo.Items.Insert(0, (New ListItem("-----", "0")))
        End If

    End Sub

    Public Sub llenarCboUnidadMedidaxIdMagnitud(ByVal cbo As DropDownList, ByVal IdMagnitud As Integer, Optional ByVal addElemento As Boolean = False)

        cbo.DataSource = (New Negocio.MagnitudUnidad).SelectCboUM(IdMagnitud)

        cbo.DataTextField = "DescripcionCorto"
        cbo.DataValueField = "Id"
        cbo.DataBind()

        If (addElemento) Then
            cbo.Items.Insert(0, (New ListItem("-----", "0")))
        End If

    End Sub

    Public Sub LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(ByVal cbo As DropDownList, ByVal IdTipoDocumento As Integer, ByVal IdCondicionPago As Integer, Optional ByVal addElemento As Boolean = False)

        cbo.DataSource = (New Negocio.MedioPago).SelectxIdTipoDocumentoxIdCondicionPago(IdTipoDocumento, IdCondicionPago)

        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()

        If (addElemento) Then
            cbo.Items.Insert(0, (New ListItem("-Todos-", "0")))
        End If

    End Sub

    Public Sub LlenarCboUnidadNegocio(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Centro_Costo).Listar_Cod_UniNeg_Centro_Costo(1)
            .DataValueField = "Cod_UniNeg"
            .DataTextField = "Nombre_CC"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "00"))
            End If
        End With
    End Sub

    Public Sub LlenarCboDeptoFuncional(ByVal cbo As DropDownList, ByVal codUnidadNegocio As String, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Centro_Costo).Listar_Cod_DepFunc_Centro_costo(codUnidadNegocio, 1)
            .DataValueField = "Cod_DepFunc"
            .DataTextField = "Nombre_CC"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "00"))
            End If
        End With
    End Sub

    Public Sub LlenarCboSubArea1(ByVal cbo As DropDownList, ByVal codUnidadNegocio As String, ByVal codDeptoFuncional As String, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Centro_Costo).Listar_Cod_SubCodigo2_Centro_costo(codUnidadNegocio, codDeptoFuncional, 1)
            .DataValueField = "Cod_SubCodigo2"
            .DataTextField = "Nombre_CC"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "00"))
            End If
        End With
    End Sub

    Public Sub LlenarCboSubArea2(ByVal cbo As DropDownList, ByVal codUnidadNegocio As String, ByVal codDeptoFuncional As String, ByVal codSubArea1 As String, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Centro_Costo).Listar_Cod_SubCodigo3_Centro_costo(codUnidadNegocio, codDeptoFuncional, codSubArea1, 1)
            .DataValueField = "Cod_SubCodigo3"
            .DataTextField = "Nombre_CC"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "00"))
            End If
        End With
    End Sub

    Public Sub LlenarCboSubArea3(ByVal cbo As DropDownList, ByVal codUnidadNegocio As String, ByVal codDeptoFuncional As String, ByVal codSubArea1 As String, ByVal codSubArea2 As String, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Centro_Costo).Listar_Cod_SubCodigo4_Centro_costo(codUnidadNegocio, codDeptoFuncional, codSubArea1, codSubArea2, 1)
            .DataValueField = "Cod_SubCodigo4"
            .DataTextField = "Nombre_CC"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "00"))
            End If
        End With
    End Sub

    Public Sub LlenarCboBanco(ByVal cbo As DropDownList, ByVal IdEmpresa As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            Dim lista As New List(Of Entidades.Banco)
            lista = (New Negocio.CuentaBancaria).SelectCboBanco(IdEmpresa)
            .DataSource = lista
            .DataValueField = "Id"
            .DataTextField = "Nombre"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub
    Public Sub LlenarCboTipoImpresion(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.ConfigImpresion).llenaCbo
            .DataValueField = "IdImpresion"
            .DataTextField = "Tp_Nombre"
            .DataBind()

            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If

        End With
    End Sub


    Public Sub LlenarCboTipoDocumentoExterno(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.TipoDocumento).SelectTipoDocumentoExternoActivo
            .DataValueField = "Id"
            .DataTextField = "Descripcion"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboAlmacenCentroDistribucionxIdEmpresa(ByVal cbo As DropDownList, ByVal IdEmpresa As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Almacen).AlmacenSelectActivoCentroDistribucionxIdEmpresa(IdEmpresa)
            .DataValueField = "IdAlmacen"
            .DataTextField = "Nombre"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboSemanaxIdYear(ByVal cbo As DropDownList, ByVal IdYear As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectSemanaxYear(IdYear)
            .DataValueField = "IdSemana"
            .DataTextField = "IdSemana"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(ByVal cbo As DropDownList, ByVal IdTipoAlmacen As Integer, ByVal IdEmpresa As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Almacen).SelectAllActivoxIdTipoAlmacenxIdEmpresa(IdTipoAlmacen, IdEmpresa)
            .DataValueField = "IdAlmacen"
            .DataTextField = "Nombre"
            .DataBind()
            'If addElement Then
            .Items.Insert(0, New ListItem("------", "0"))
            'End If
        End With
    End Sub


    'Dim lista As List(Of Entidades.SubLinea)
    '    If idLinea = 0 Then
    '        lista = New List(Of Entidades.SubLinea)
    'Dim objSubLinea As New Entidades.SubLinea(0, "-----")
    '        lista.Add(objSubLinea)
    '    Else
    '        lista = (New Negocio.SubLinea).SelectActivoxLineaxTipoExistencia(idLinea, idtipoexistencia)
    '        If addElemento Then
    'Dim objSubLinea As New Entidades.SubLinea(0, "-----")
    '            lista.Insert(0, objSubLinea)
    '        End If
    '    End If
    '    cbo.DataTextField = "Nombre"
    '    cbo.DataValueField = "Id"
    '    cbo.DataSource = lista
    '    cbo.DataBind()






    Public Sub LlenarCboTipoTablaxIdSubLinea(ByVal cbo As DropDownList, ByVal idsublinea As Integer, Optional ByVal addElemento As Boolean = False)
        Dim lista As List(Of Entidades.SubLinea_TipoTabla)
        If idsublinea = 0 Then
            lista = New List(Of Entidades.SubLinea_TipoTabla)
            Dim obj As New Entidades.SubLinea_TipoTabla(0, "Seleccione Tipo Tabla", True)
            lista.Add(obj)
        Else
            lista = (New Negocio.SubLinea_TipoTabla).SelectAllTipoTablaxIdSubLinea(idsublinea)
            If addElemento Then
                Dim obj As New Entidades.SubLinea_TipoTabla(0, "Seleccione Tipo Tabla", True)
                lista.Insert(0, obj)
            End If
        End If
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdTipoTabla"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub

    Public Sub LLenarCboAlmacenxIdTipoAlmacenxIdTienda(ByVal cbo As DropDownList, ByVal IdTipoAlmacen As Integer, ByVal IdTienda As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Almacen).SelectCboxIdTipoAlmacenxIdTienda(IdTipoAlmacen, IdTienda)
            .DataValueField = "IdAlmacen"
            .DataTextField = "Nombre"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    ''' <summary>
    ''' Lista Docuemnto Referencia Documento
    ''' </summary>   
    ''' <param name="Opcion">1 descripcinCorta ,2 descripcinLarga</param>
    Public Sub LLenarCboTipoDocumentoRefxIdTipoDocumento(ByVal cbo As DropDownList, ByVal IdTipoDocumento As Integer, ByVal Opcion As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.TipoDocumentoRef).SelectxIdtipoDocRef(IdTipoDocumento)
            .DataValueField = "Id"
            Select Case Opcion
                Case 1
                    .DataTextField = "DescripcionCorto"
                Case 2
                    .DataTextField = "Descripcion"
            End Select

            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboMotivoTrasladoxIdTipoOperacion(ByVal cbo As DropDownList, ByVal IdTipoOperacion As Integer, ByVal addElement As Boolean)

        Dim lista As List(Of Entidades.MotivoTraslado) = (New Negocio.MotivoT_TipoOperacion).SelectCboxIdTipoOperacion(IdTipoOperacion)

        If (addElement) Then
            lista.Insert(0, (New Entidades.MotivoTraslado(0, "---")))
        End If

        cbo.DataValueField = "Id"
        cbo.DataTextField = "Nombre"

        cbo.DataSource = lista
        cbo.DataBind()

    End Sub


    Public Sub LlenarCboSerieChequexIdCuentaBancaria(ByVal cbo As DropDownList, _
    ByVal IdCuentaBancaria As Integer, _
    Optional ByVal addElement As Boolean = False)

        With cbo
            .Items.Clear()
            .DataSource = (New Negocio.SerieChequeView).SelectCboxIdCuentaBancaria(IdCuentaBancaria)
            .DataTextField = "SerieYNumeracion"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboSerieChequexIdBancoxIdCuentaBancaria(ByVal cbo As DropDownList, _
    ByVal IdBanco As Integer, ByVal IdCuentaBancaria As Integer, _
    Optional ByVal addElement As Boolean = False)

        With cbo
            .DataSource = (New Negocio.SerieChequeView).SelectCboxIdBancoxIdCuentaBancaria(IdBanco, IdCuentaBancaria)
            .DataTextField = "SerieYNumeracion"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LLenarCboAreaxIdtiendaxIdUsuarioxSoloCentroCosto(ByVal cbo As DropDownList, ByVal idtienda As Integer, ByVal idusuario As Integer, Optional ByVal addElement As Boolean = False)

        cbo.DataSource = (New Negocio.Area).Area_Select_TiendaArea_UsuarioArea(idtienda, idusuario)
        cbo.DataValueField = "IdCompuesto"
        cbo.DataTextField = "DescripcionCorta"
        cbo.DataBind()
        If addElement Then
            cbo.Items.Insert(0, New ListItem("------", "0,0,0"))
        End If
    End Sub

    Public Sub LlenarCboTipoAlmacen(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)

        With cbo
            .DataSource = (New Negocio.TipoAlmacen).SelectAllActivo()
            .DataTextField = "Tipoalm_Nombre"
            .DataValueField = "IdTipoAlmacen"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LLenarCboALmacenPrincipalxIdTienda(ByVal cbo As DropDownList, ByVal idTienda As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Almacen).SelectAlmacenPrincipalxIdTienda(idTienda)
            .DataTextField = "Nombre"
            .DataValueField = "IdAlmacen"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboYear(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)

        With cbo
            .DataSource = (New Negocio.ProgramacionPedido).GetYear()
            .DataTextField = "idyear"
            .DataValueField = "idyear"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub
    Public Sub LlenarCboSemanaOfYear(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)

        With cbo
            .DataSource = (New Negocio.ProgramacionPedido).GetSemanaDYear
            .DataTextField = "idsemana"
            .DataValueField = "idsemana"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboPostxIdCaja(ByVal cbo As DropDownList, ByVal IdCaja As Integer, Optional ByVal addElement As Boolean = False)

        With cbo
            .DataSource = (New Negocio.PostView).SelectCboxIdCaja(IdCaja)
            .DataTextField = "Descripcion"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub


    Public Sub LlenarCboTarjetaxIdPost(ByVal cbo As DropDownList, ByVal IdPost As Integer, Optional ByVal addElement As Boolean = False)

        With cbo
            .DataSource = (New Negocio.TarjetaView).SelectCboxIdPost(IdPost)
            .DataTextField = "Nombre"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub


    Public Sub LlenarCboPostxIdBancoxIdCuentaBancaria(ByVal cbo As DropDownList, _
        ByVal IdBanco As Integer, ByVal IdCuentaBancaria As Integer, _
        Optional ByVal addElement As Boolean = False)

        With cbo
            .DataSource = (New Negocio.PostView).SelectxIdBancoxIdCuentaBancaria(IdBanco, IdCuentaBancaria)
            .DataTextField = "Nombre"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboPostxIdCuentaBancaria(ByVal cbo As DropDownList, ByVal Id As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.PostView).SelectxIdCuentaBancaria(Id)
            .DataTextField = "Nombre"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboPostxIdBanco(ByVal cbo As DropDownList, ByVal Id As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.PostView).SelectxIdBanco(Id)
            .DataTextField = "Nombre"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboTarjetaxIdTipoTarjeta(ByVal cbo As DropDownList, ByVal Id As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.TarjetaView).SelectxIdTipoTarjeta(Id)
            .DataTextField = "Nombre"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboConceptoMovBancoxIdTipoConceptoBanco(ByVal cbo As DropDownList, ByVal Id As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.ConceptoMovBancoView).SelectxIdTipoConceptoBanco(Id)
            .DataTextField = "Nombre"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub
    Public Sub LlenarCboTipoMovimiento(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.TipoMovimiento).SelectAllActivo()
            .DataTextField = "Nombre"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub
    Public Sub LlenarCboTipoConceptoBanco(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.TipoConceptoBanco).SelectCbo()
            .DataTextField = "Descripcion"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub
    Public Sub LlenarCboFactor(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        With cbo
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
            .Items.Insert(1, New ListItem("1", "1"))
            .Items.Insert(2, New ListItem("-1", "-1"))
        End With
    End Sub

    Public Sub LlenarCboCod_SubCodigo4(ByVal cbo As DropDownList, ByVal Cod_UniNeg As String, _
                                    ByVal Cod_DepFunc As String, ByVal Cod_SubCodigo2 As String, _
                                    ByVal Cod_SubCodigo3 As String, _
                                    Optional ByVal addElement As Boolean = False, _
                                    Optional ByVal activo As Integer = 1)
        With cbo
            Dim Lista As List(Of Entidades.Centro_costo) = (New Negocio.Centro_Costo).Listar_Cod_SubCodigo4_Centro_costo(Cod_UniNeg, Cod_DepFunc, Cod_SubCodigo2, Cod_SubCodigo3, activo)
            If Lista.Count > 0 Then
                .DataSource = Lista
                .DataTextField = "Nombre_CC"
                .DataValueField = "Cod_SubCodigo4"
                .DataBind()
            Else
                .Items.Clear()
            End If
            If addElement Then
                .Items.Insert(0, New ListItem("------", "00"))
            End If
        End With
    End Sub

    Public Sub LlenarCboCod_SubCodigo3(ByVal cbo As DropDownList, ByVal Cod_UniNeg As String, _
                                    ByVal Cod_DepFunc As String, ByVal Cod_SubCodigo2 As String, _
                                    Optional ByVal addElement As Boolean = False, _
                                    Optional ByVal activo As Integer = 1)
        With cbo
            Dim Lista As List(Of Entidades.Centro_costo) = (New Negocio.Centro_Costo).Listar_Cod_SubCodigo3_Centro_costo(Cod_UniNeg, Cod_DepFunc, Cod_SubCodigo2, activo)
            If Lista.Count > 0 Then
                .DataSource = Lista
                .DataTextField = "Nombre_CC"
                .DataValueField = "Cod_SubCodigo3"
                .DataBind()
            Else
                .Items.Clear()
            End If
            If addElement Then
                .Items.Insert(0, New ListItem("------", "00"))
            End If
        End With
    End Sub

    Public Sub LlenarCboCod_SubCodigo2(ByVal cbo As DropDownList, ByVal Cod_UniNeg As String, _
                            ByVal Cod_DepFunc As String, Optional ByVal addElement As Boolean = False, _
                            Optional ByVal activo As Integer = 1)
        With cbo
            Dim Lista As List(Of Entidades.Centro_costo) = (New Negocio.Centro_Costo).Listar_Cod_SubCodigo2_Centro_costo(Cod_UniNeg, Cod_DepFunc, activo)
            If Lista.Count > 0 Then
                .DataSource = Lista
                .DataTextField = "Nombre_CC"
                .DataValueField = "Cod_SubCodigo2"
                .DataBind()
            Else
                .Items.Clear()
            End If
            If addElement Then
                .Items.Insert(0, New ListItem("------", "00"))
            End If
        End With
    End Sub

    Public Sub LlenarCboCod_DepFunc(ByVal cbo As DropDownList, ByVal cod_unineg As String, _
                                    Optional ByVal addElement As Boolean = False, _
                                    Optional ByVal activo As Integer = 1)
        With cbo
            Dim Lista As List(Of Entidades.Centro_costo) = (New Negocio.Centro_Costo).Listar_Cod_DepFunc_Centro_costo(cod_unineg, activo)
            If Lista.Count > 0 Then
                .DataSource = Lista
                .DataTextField = "Nombre_CC"
                .DataValueField = "Cod_DepFunc"
                .DataBind()
            Else
                .Items.Clear()
            End If
            If addElement Then
                .Items.Insert(0, New ListItem("------", "00"))
            End If
        End With
    End Sub

    Public Sub LlenarCboCod_UniNeg(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False, _
                        Optional ByVal activo As Integer = 1)
        With cbo
            Dim Lista As List(Of Entidades.Centro_costo) = (New Negocio.Centro_Costo).Listar_Cod_UniNeg_Centro_Costo(activo)
            If Lista.Count > 0 Then
                .DataSource = Lista
                .DataTextField = "Nombre_CC"
                .DataValueField = "Cod_UniNeg"
                .DataBind()
            Else
                .Items.Clear()
            End If
            If addElement Then
                .Items.Insert(0, New ListItem("------", "00"))
            End If
        End With
    End Sub

    Public Sub LlenarCboTipoTarjeta(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.TipoTarjeta).SelectAllActivo
            .DataTextField = "Nombre"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboPost(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.PostView).SelectAllActivo
            .DataTextField = "Nombre"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboMedioPagoInterfaz(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.MedioPago).SelectCboMpInterfaz
            .DataTextField = "mpi_Descripcion"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboLineaxArea(ByVal cbo As DropDownList, ByVal idarea As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Linea_Area).ListarLineaxArea(idarea)
            .DataTextField = "Descripcion"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub
    Public Sub LlenarCboLineaxTipoexistenciaxArea(ByVal cbo As DropDownList, ByVal idarea As Integer, ByVal idtipoexistencia As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Linea_Area).ListarLineaxTipoexistenciaxArea(idarea, idtipoexistencia)
            .DataTextField = "Descripcion"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub
    Public Sub LlenarCboTipoexistenciaxArea(ByVal cbo As DropDownList, ByVal idarea As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Linea_Area).ListarTipoexistenciaxArea(idarea)
            .DataTextField = "Nombre"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub


    Public Sub LlenarCboAreaxIdusuario(ByVal cbo As DropDownList, ByVal IdUsuario As Integer, Optional ByVal addIdArea As Integer = Nothing, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.Usuario_Area).ListarAreaxIdusuario(IdUsuario, addIdArea)
            .DataTextField = "ar_NombreCorto"
            .DataValueField = "IdArea"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboAreaxIdusuarioAprov(ByVal cbo As DropDownList, ByVal IdUsuario As Integer, Optional ByVal addIdArea As Integer = Nothing, Optional ByVal addElement As Boolean = True)
        With cbo
            .DataSource = (New Negocio.Usuario_Area).ListarAreaxIdusuario(IdUsuario, addIdArea)
            .DataTextField = "ar_NombreCorto"
            .DataValueField = "IdArea"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub


    Public Sub LlenarCboTipoGasto(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        cbo.DataSource = (New Negocio.TipoGasto).SelectCbo
        cbo.DataValueField = "IdTipoGasto"
        cbo.DataTextField = "TG_Nombre"
        cbo.DataBind()

        If addElement Then
            cbo.Items.Insert(0, New ListItem("-----", "0"))
        End If
    End Sub

    Public Sub LlenarCboConceptoxIdTipoDocumento(ByVal cbo As DropDownList, ByVal IdTipoDocumento As Integer, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(IdTipoDocumento)
        If addElement Then
            Dim objConcepto As New Entidades.Concepto(0, "-----")
            lista.Insert(0, objConcepto)
        End If

        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()

    End Sub

    Public Sub LlenarCboArea(ByVal cbo As DropDownList, ByVal addElement As Boolean)

        Dim lista As List(Of Entidades.Area) = (New Negocio.Area).SelectCbo
        If addElement Then
            Dim objArea As New Entidades.Area(0, "-----", True)
            lista.Insert(0, objArea)
        End If

        cbo.DataSource = lista
        cbo.DataTextField = "DescripcionCorta"
        cbo.DataValueField = "Id"
        cbo.DataBind()

    End Sub
    Public Sub LlenarCboArea_ParametroGeneral(ByVal cbo As DropDownList, ByVal addElement As Boolean)

        Dim lista As List(Of Entidades.Area) = (New Negocio.ParametroGeneral).SelectCboArea
        If addElement Then
            Dim objArea As New Entidades.Area(0, "-----", True)
            lista.Insert(0, objArea)
        End If

        cbo.DataSource = lista
        cbo.DataTextField = "DescripcionCorta"
        cbo.DataValueField = "Id"
        cbo.DataBind()

    End Sub

    Public Sub LlenarCboArea_Permiso(ByVal cbo As DropDownList, ByVal addElement As Boolean)

        Dim lista As List(Of Entidades.Area) = (New Negocio.Permiso).SelectCboArea
        If addElement Then
            Dim objArea As New Entidades.Area(0, "-----", True)
            lista.Insert(0, objArea)
        End If

        cbo.DataSource = lista
        cbo.DataTextField = "DescripcionCorta"
        cbo.DataValueField = "Id"
        cbo.DataBind()

    End Sub



    Public Sub LlenarCboCajaxIdTiendaxIdUsuario(ByVal cbo As DropDownList, ByVal IdTienda As Integer, ByVal IdUsuario As Integer, ByVal addElement As Boolean)

        Dim lista As List(Of Entidades.Caja) = (New Negocio.CajaUsuario).SelectCboxIdTiendaxIdUsuario(IdUsuario, IdTienda)
        If addElement Then
            Dim objCaja As New Entidades.Caja(0, "-----")
            lista.Insert(0, objCaja)
            'modificado
            cbo.DataSource = lista
            cbo.DataTextField = "Nombre"
            cbo.DataValueField = "IdCaja"
            cbo.DataBind()
        ElseIf lista.Count = 0 Then
            Dim objCaja As New Entidades.Caja(0, "-----")
            lista.Insert(0, objCaja)
            'modificado
            cbo.DataSource = lista
            cbo.DataTextField = "Nombre"
            cbo.DataValueField = "IdCaja"
            cbo.DataBind()
        Else
            cbo.DataSource = lista
            cbo.DataTextField = "Nombre"
            cbo.DataValueField = "IdCaja"
            cbo.DataBind()
        End If

        

    End Sub



    Public Sub LlenarCboEmpresaxIdUsuario(ByVal cbo As DropDownList, ByVal IdUsuario As Integer, ByVal addElement As Boolean)

        Dim lista As List(Of Entidades.Propietario) = (New Negocio.EmpresaTiendaUsuario).SelectEmpresaxIdUsuario(IdUsuario)
        If addElement Then
            Dim objPropietario As New Entidades.Propietario(0, "-----")
            lista.Insert(0, objPropietario)
        End If

        cbo.DataSource = lista
        cbo.DataTextField = "NombreComercial"
        cbo.DataValueField = "Id"
        cbo.DataBind()

    End Sub



    Public Sub LlenarCboTiendaxIdUsuario(ByVal cbo As DropDownList, ByVal IdUsuario As Integer, ByVal addElemento As Boolean)

        Dim lista As List(Of Entidades.Tienda) = (New Negocio.EmpresaTiendaUsuario).SelectTiendaxIdUsuario(IdUsuario)
        If addElemento Then
            lista.Insert(0, (New Entidades.Tienda(0, "-----")))
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboTiendaxIdEmpresaxIdUsuario(ByVal cbo As DropDownList, ByVal IdEmpresa As Integer, ByVal IdUsuario As Integer, ByVal addElemento As Boolean)

        Dim lista As List(Of Entidades.Tienda) = (New Negocio.EmpresaTiendaUsuario).SelectTiendaxIdEmpresaxIdUsuario(IdUsuario, IdEmpresa)
        If addElemento Then
            lista.Insert(0, (New Entidades.Tienda(0, "-----")))
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub


    Public Sub LlenarCboMonedaBase(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.Moneda
        Dim lista As List(Of Entidades.Moneda) = obj.SelectCboMonedaBase
        If addElemento Then
            Dim objMoneda As New Entidades.Moneda(0, "---", "---")
            lista.Insert(0, objMoneda)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Simbolo"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboOficinaxIdBanco(ByVal cbo As DropDownList, ByVal IdBanco As Integer, Optional ByVal addElement As Boolean = False)

        Dim Lista As New List(Of Entidades.Oficina)
        Lista = (New Negocio.Oficina).SelectCboxIdBanco(IdBanco)

        If (addElement) Then
            Lista.Insert(0, New Entidades.Oficina(0, "-----", "1"))
        End If

        cbo.DataSource = Lista
        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub llenarCboDocumentoI(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim Lista As List(Of Entidades.TipoDocumentoI) = (New Negocio.TipoDocumentoI).SelectAllActivo()
        cbo.DataValueField = "Id"
        cbo.DataTextField = "Abv"
        If addElemento Then
            Dim obj As New Entidades.TipoDocumentoI(0, "-----")
            Lista.Insert(0, obj)
        End If
        cbo.DataSource = Lista
        cbo.DataBind()
    End Sub


    Public Sub LlenarCboConceptoxTipoUso(ByVal cbo As DropDownList, ByVal TipoUso As Integer, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.Concepto
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        Dim lista As List(Of Entidades.Concepto) = obj.SelectCboxTipoUso(TipoUso)
        If addElemento Then
            Dim objConcepto As New Entidades.Concepto(0, "-----")
            lista.Insert(0, objConcepto)
        End If
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboCajaxIdTiendaxIdUsuario_Caja(ByVal cbo As DropDownList, ByVal IdTienda As Integer, ByVal IdUsuario As Integer, Optional ByVal addRayas As Boolean = False)
        Dim obj As New Negocio.CajaUsuario
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdCaja"
        Dim lista As List(Of Entidades.Caja) = obj.SelectCboxIdTiendaxIdUsuario_Caja(IdTienda, IdUsuario)
        If addRayas Then
            Dim objCajaEntidad As New Entidades.Caja(0, "-----")
            lista.Insert(0, objCajaEntidad)
        End If
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub

    Public Sub llenarCboTiendaxIdEmpresaxIdUsuario_Caja(ByVal cbo As DropDownList, ByVal idEmpresa As Integer, ByVal IdUsuario As Integer, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.Tienda
        Dim lista As List(Of Entidades.Tienda) = obj.SelectCboxIdEmpresaxIdUsuario_Caja(idEmpresa, IdUsuario)
        If addElemento Then
            Dim objTienda As New Entidades.Tienda(0, "-----")
            lista.Insert(0, objTienda)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Public Sub llenarCboLineaxTipoExistencia(ByVal cb As DropDownList, ByVal idtipoexistencia As Integer, Optional ByVal addValor As Boolean = False)
        Dim Lista As List(Of Entidades.Linea)
        If idtipoexistencia = 0 Then
            Lista = New List(Of Entidades.Linea)
            Lista.Insert(0, (New Entidades.Linea(0, "-----")))
        Else
            Lista = (New Negocio.Linea).SelectCboLineaxIdtipoexistencia(idtipoexistencia)
            If addValor Then
                Lista.Insert(0, (New Entidades.Linea(0, "-----")))
            End If
        End If

        cb.DataSource = Lista
        cb.DataValueField = "Id"
        cb.DataTextField = "Descripcion"
        cb.DataBind()
    End Sub
    Public Sub llenarCboLineaMarca(ByVal cb As DropDownList, ByVal idlinea As Integer, ByVal esMante As Integer, _
                                   Optional ByVal addvalor As Boolean = False)
        Dim Lista As List(Of Entidades.Marca) = (New Negocio.OrdenTrabajo).listarLineaMarca(idlinea, esMante)
        If addvalor Then
            Lista.Insert(0, (New Entidades.Marca(0, "-----")))
        End If
        cb.DataSource = Lista
        cb.DataValueField = "Id"
        cb.DataTextField = "Descripcion"
        cb.DataBind()
    End Sub

    Public Sub llenarCboModeloLineaMarca(ByVal cb As DropDownList, ByVal idlineaMarca As Integer, Optional ByVal addvalor As Boolean = False)
        Dim Lista As List(Of Entidades.ModeloLineaMarca) = (New Negocio.OrdenTrabajo).listarModeloLineaMarca(idlineaMarca)
        If addvalor Then
            Lista.Insert(0, (New Entidades.ModeloLineaMarca(0, "-----")))
        End If
        cb.DataSource = Lista
        cb.DataValueField = "idmodelo"
        cb.DataTextField = "mod_Nombre"
        cb.DataBind()
    End Sub

    Public Sub LlenarCboRol(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)

        Dim lista As List(Of Entidades.Rol) = (New Negocio.Rol).SelectCbo
        If addElement Then
            Dim objRol As New Entidades.Rol(0, "-----", "1")
            lista.Insert(0, objRol)
        End If

        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()

    End Sub
    Public Sub LlenarCboRolProveed(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)

        Dim lista As List(Of Entidades.Rol) = (New Negocio.Rol).SelectCboProved
        If addElement Then
            Dim objRol As New Entidades.Rol(0, "-----", "1")
            lista.Insert(0, objRol)
        End If

        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()

    End Sub
    Public Sub LlenarCboRolCliente(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = True)

        Dim lista As List(Of Entidades.Rol) = (New Negocio.Rol).SelectCboCliente
        If addElement Then
            Dim objRol As New Entidades.Rol(0, "-----", "1")
            lista.Insert(0, objRol)
        End If


        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()






    End Sub


    Public Sub llenarCboTipoOperacionxIdMotivoT(ByVal cbo As DropDownList, ByVal IdMotivoT As Integer, Optional ByVal addElemento As Boolean = False)

        Dim Lista As List(Of Entidades.TipoOperacion) = (New Negocio.TipoOperacion).SelectCboxIdMotivoTraslado(IdMotivoT)
        If addElemento Then
            Lista.Insert(0, (New Entidades.TipoOperacion(0, "-----")))
        End If
        cbo.DataSource = Lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()

    End Sub

    Public Sub llenarCboTipoOperacionxIdTpoDocumento(ByVal cbo As DropDownList, ByVal IdTipoDocumento As Integer, Optional ByVal addElemento As Boolean = False, _
                                                     Optional ByVal idUsuario As Integer = 0, Optional ByVal str As String = "")
        Dim Lista As List(Of Entidades.TipoOperacion) = (New Negocio.TipoOperacion).SelectCboxIdTipoDocumento(IdTipoDocumento)
        If Lista.Count > 0 Then
            If addElemento Then
                'listaPermisoTipoOperacion.Insert(0, (New Entidades.be_validacion(0, "-----")))
            End If
            cbo.DataSource = Lista
            cbo.DataTextField = "Nombre"
            cbo.DataValueField = "Id"
            cbo.DataBind()
            cbo.Items.Insert(0, New ListItem("-----", 0))
        Else

            If addElemento Then
                Lista.Insert(0, (New Entidades.TipoOperacion(0, "-----")))
            End If
            cbo.DataSource = Lista
            cbo.DataTextField = "Nombre"
            cbo.DataValueField = "Id"
            cbo.DataBind()
        End If
    End Sub

    Public Sub llenarCboCondicionesComerciales(ByVal cbo As DropDownList, ByVal idDocumento As Integer, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.CondicionPago
        Dim Lista As List(Of Entidades.CondicionComercial) = obj.SelectAllCondicionesComerciales(idDocumento)
        If addElemento Then
            Dim objCondicionComercial As New Entidades.CondicionComercial(0, "-----")
            Lista.Insert(0, objCondicionComercial)
        End If
        cbo.DataSource = Lista
        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Public Sub llenarCboAlmacen(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.Almacen
        Dim lista As List(Of Entidades.Almacen) = obj.SelectCbo()
        If addElemento Then
            Dim objAlmacen As New Entidades.Almacen(0, "-----")
            lista.Insert(0, objAlmacen)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdAlmacen"
        cbo.DataBind()
    End Sub
    Public Sub llenarCboAlmacenxTipoAlmacenPrincipal(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.Almacen
        Dim lista As List(Of Entidades.Almacen) = obj.SelectAlmacenTAPrincipal()
        If addElemento Then
            Dim objAlmacen As New Entidades.Almacen(0, "-----")
            lista.Insert(0, objAlmacen)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdAlmacen"
        cbo.DataBind()
    End Sub
    Public Sub llenarCboTipoOperacion(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.TipoOperacion
        Dim lista As List(Of Entidades.TipoOperacion) = obj.SelectAllActivo()

        If addElemento Then
            Dim objTipoOperacion As New Entidades.TipoOperacion(0, "-----")
            lista.Insert(0, objTipoOperacion)
        End If

        cbo.DataValueField = "Id"
        cbo.DataTextField = "Nombre"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboLinea(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim nLinea As New Negocio.Linea
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo()
        If addElemento Then
            Dim objLinea As New Entidades.Linea(0, "-----")
            lista.Insert(0, objLinea)
        End If
        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub


    Public Sub LlenarCboSubLineaxIdLinea(ByVal cbo As DropDownList, ByVal idLinea As Integer, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = obj.SelectCboxIdLinea(idLinea)
        If addElemento Then
            Dim objSubLinea As New Entidades.SubLinea(0, "-----")
            lista.Insert(0, objSubLinea)
        End If

        If lista Is Nothing Then lista = New List(Of Entidades.SubLinea)
        If lista.Count <= 0 Then
            Dim objSubLinea As New Entidades.SubLinea(0, "-----")
            lista.Insert(0, objSubLinea)
        End If

        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboPerfil(ByVal cbo As DropDownList, Optional ByVal addRayas As Boolean = False)
        Dim obj As New Negocio.Perfil
        cbo.DataTextField = "NomPerfil"
        cbo.DataValueField = "IdPerfil"
        Dim lista As List(Of Entidades.Perfil) = obj.SelectCbo
        If addRayas Then
            Dim objPerfil As New Entidades.Perfil(0, "-Seleccione-")
            lista.Insert(0, objPerfil)
        End If
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboCajaxIdTienda(ByVal cbo As DropDownList, ByVal IdTienda As Integer, Optional ByVal addRayas As Boolean = False)
        Dim obj As New Negocio.Caja
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdCaja"
        Dim lista As List(Of Entidades.Caja) = obj.SelectCboxIdTienda(IdTienda)
        If addRayas Then
            Dim objCajaEntidad As New Entidades.Caja(0, "-----")
            lista.Insert(0, objCajaEntidad)
        End If
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LLenarCboUsuario(ByVal Cbo As DropDownList, ByVal AddElement As Boolean)

        Dim Lista As List(Of Entidades.Natural) = (New Negocio.Usuario).SelectCbo
        Cbo.DataSource = Lista
        Cbo.DataTextField = "Nombres"
        Cbo.DataValueField = "IdPersona"
        Cbo.DataBind()

        If (AddElement) Then
            Cbo.Items.Insert(0, New ListItem("------", "0"))
        End If

    End Sub
    Public Sub llenarCboAlmacenxIdTienda(ByVal cbo As DropDownList, ByVal IdTienda As Integer, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.Almacen
        Dim lista As List(Of Entidades.Almacen) = obj.SelectCboxIdTienda(IdTienda)
        If addElemento Then
            Dim objAlmacen As New Entidades.Almacen(0, "-----")
            lista.Insert(0, objAlmacen)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdAlmacen"
        cbo.DataBind()
    End Sub
    Public Sub llenarCboTiendaxIdEmpresa(ByVal cbo As DropDownList, ByVal idEmpresa As Integer, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.Tienda
        Dim lista As List(Of Entidades.Tienda) = obj.SelectCboxEmpresa(idEmpresa)
        If addElemento Then
            Dim objTienda As New Entidades.Tienda(0, "-Todos-")
            lista.Insert(0, objTienda)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub llenarCboAlmacenxIdEmpresa(ByVal cbo As DropDownList, ByVal IdEmpresa As Integer, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.Almacen
        Dim lista As List(Of Entidades.Almacen) = obj.SelectCboxIdEmpresa(IdEmpresa)
        If addElemento Then
            Dim objAlmacen As New Entidades.Almacen(0, "-----")
            lista.Insert(0, objAlmacen)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdAlmacen"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboTipoDocumento(ByVal cbo As DropDownList, ByVal IdPropietario As Integer, ByVal IdArea As Integer, _
                                      Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.TipoDocumento
        Dim Lista As List(Of Entidades.TipoDocumento) = obj.SelectCboxIdEmpresaxIdArea(IdPropietario, IdArea)
        If addElemento Then
            Dim objTipoDoc As New Entidades.TipoDocumento(0, "-----")
            Lista.Insert(0, objTipoDoc)
        End If
        cbo.DataSource = Lista
        cbo.DataTextField = "DescripcionCorto"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    ''AGREGADO 10/07/14
    Public Sub LlenarCboTipoDocumentoProgPago(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.TipoDocumento
        Dim Lista As List(Of Entidades.TipoDocumento) = obj.SelectCboTipoDocumentoProgPago
        If addElemento Then
            Dim objTipoDoc As New Entidades.TipoDocumento(0, "-----")
            Lista.Insert(0, objTipoDoc)
        End If
        cbo.DataSource = Lista
        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboTipoDocumentoIndep(ByVal cbo As DropDownList, Optional ByVal AddLinea As Boolean = False)
        Dim obj As New Negocio.TipoDocumento
        Dim lista As List(Of Entidades.TipoDocumento) = obj.SelecttipodocumentoInd
        If AddLinea = True Then
            Dim objLinea As New Entidades.TipoDocumento(0, "--------")
            lista.Insert(0, objLinea)
        End If

        cbo.DataSource = lista
        cbo.DataTextField = "DescripcionCorto"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub


    Public Sub LlenarCboMagnitud(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Magnitud
        cbo.DataSource = obj.SelectCbo
        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboMotivoBaja(ByVal cbo As DropDownList)
        Dim obj As New Negocio.MotivoBaja
        Dim lista As List(Of Entidades.MotivoBaja) = obj.SelectAllActivo
        Dim objMotivoBaja As New Entidades.MotivoBaja
        objMotivoBaja.Id = 0
        objMotivoBaja.Descripcion = "-----"
        lista.Insert(0, objMotivoBaja)
        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboCuentaBancaria(ByVal cbo As DropDownList, ByVal IdBanco As Integer, ByVal IdPropietario As Integer, Optional ByVal addElement As Boolean = False)

        With cbo
            Dim lista As New List(Of Entidades.CuentaBancaria)
            lista = (New Negocio.CuentaBancaria).SelectCboxIdBancoxIdPersona(IdBanco, IdPropietario)
            If lista.Count = 0 Then Exit Sub
            .DataSource = lista
            .DataTextField = "NroCuentaBancaria"
            .DataValueField = "IdCuentaBancaria"
            .DataBind()

            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If

        End With

    End Sub
    Public Sub LlenarCboCuentaBancaria(ByVal cbo As DropDownList, ByVal IdBanco As Integer, Optional ByVal addElement As Boolean = False)
        Dim obj As New Negocio.CuentaBancaria
        Dim Lista As New List(Of Entidades.CuentaBancaria)

        Lista = obj.SelectCboxIdBanco(IdBanco)

        If (addElement) Then
            Dim CuentaBancaria As New Entidades.CuentaBancaria
            CuentaBancaria.IdCuentaBancaria = CInt(0)
            CuentaBancaria.NroCuentaBancaria = "------------"
            Lista.Insert(0, CuentaBancaria)
        End If


        cbo.DataSource = Lista
        cbo.DataTextField = "NroCuentaBancaria"
        cbo.DataValueField = "IdCuentaBancaria"
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboMedioPago(ByVal cbo As DropDownList, Optional ByVal addRayas As Boolean = False)
        Dim obj As New Negocio.MedioPago
        Dim lista As List(Of Entidades.MedioPago) = obj.SelectCbo
        If addRayas Then
            Dim objMedioPago As New Entidades.MedioPago(0, "-----")
            lista.Insert(0, objMedioPago)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboMedioPagoxIdTipoDocumento(ByVal cbo As DropDownList, ByVal IdTipoDocumento As Integer, Optional ByVal addRayas As Boolean = False)

        Dim lista As List(Of Entidades.MedioPago) = (New Negocio.TipoDocumento_MedioPago).SelectCboxIdTipoDocumento(IdTipoDocumento)
        If addRayas Then
            Dim objMedioPago As New Entidades.MedioPago(0, "-----")
            lista.Insert(0, objMedioPago)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboMedioPagoCredito(ByVal cbo As DropDownList, Optional ByVal addRayas As Boolean = False, Optional ByVal addCredito As Boolean = False)
        Dim obj As New Negocio.MedioPago
        Dim lista As List(Of Entidades.MedioPago) = obj.SelectCbo
        If addRayas Then
            Dim objMedioPago As New Entidades.MedioPago(0, "-----")
            lista.Insert(0, objMedioPago)
        End If
        If addCredito Then
            Dim objmedio As New Entidades.MedioPago(-1, "Credito")
            lista.Add(objmedio)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboBanco(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        Dim obj As New Negocio.Banco
        Dim Lista As New List(Of Entidades.Banco)


        Lista = obj.SelectCbo

        If (addElement) Then
            Dim Banco As New Entidades.Banco
            Banco.Id = CInt(0)
            Banco.Nombre = "----------------"
            Lista.Insert(0, Banco)
        End If

        cbo.DataSource = Lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboMoneda(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.Moneda
        Dim lista As List(Of Entidades.Moneda) = obj.SelectCbo
        If addElemento Then
            Dim objMoneda As New Entidades.Moneda(0, "---", "---")
            lista.Insert(0, objMoneda)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Simbolo"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboConcepto(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.Concepto
        Dim lista As List(Of Entidades.Concepto) = obj.SelectCbo
        If addElemento Then
            Dim objConcepto As New Entidades.Concepto(0, "-----")
            lista.Insert(0, objConcepto)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboPropietario(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        Dim obj As New Negocio.Propietario
        Dim lista As List(Of Entidades.Propietario) = obj.SelectCbo
        If addElement Then
            Dim objPropietario As New Entidades.Propietario(0, "-----")
            lista.Insert(0, objPropietario)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NombreComercial"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboCondicionPago(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)

        Dim obj As New Negocio.CondicionPago
        cbo.DataSource = obj.SelectAllActivo
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()

        If addElement Then
            cbo.Items.Insert(0, New ListItem("-Todos-", "0"))
        End If

    End Sub
    Public Sub LlenarCboMonedaNoBase(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Moneda
        cbo.DataTextField = "Simbolo"
        cbo.DataValueField = "Id"
        cbo.DataSource = obj.SelectCboNoBase
        cbo.DataBind()
    End Sub
    Public Sub llenarCboMotivoTraslado(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        Dim obj As New Negocio.MotivoTraslado
        Dim lista As List(Of Entidades.MotivoTraslado) = obj.SelectAllActivo
        If addElement Then
            Dim objMOtivoTraslado As New Entidades.MotivoTraslado(0, "-----")
            lista.Insert(0, objMOtivoTraslado)
        End If
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LLenarCboSeriexIdUsuarioValidacion(ByVal cbo As DropDownList, ByVal IdUsuario As Integer, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDocumento As Integer)
        Dim obj As New Negocio.Serie
        Dim ObjSerie As New List(Of Entidades.Serie)
        Dim ListaSerie As New List(Of Entidades.Serie)
        ''Validando si el usuario se encuentra en la tabla asignacion
        ObjSerie = obj.LLenarCboSeriexIdUsuarioValidacion(IdUsuario, IdTienda)
        If (ObjSerie Is Nothing) Then
            cbo.DataSource = obj.SelectCboxIdsEmpTienTDocNotinAsignacion(IdEmpresa, IdTienda, IdTipoDocumento)
            cbo.DataTextField = "Numero"
            cbo.DataValueField = "IdSerie"
            cbo.DataBind()
        Else
            ListaSerie.AddRange(ObjSerie)
            cbo.DataSource = ListaSerie
            cbo.DataTextField = "Numero"
            cbo.DataValueField = "IdSerie"
            cbo.DataBind()
        End If

    End Sub
    Public Sub LLenarCboSeriexIdsEmpTienTipoDoc(ByVal cbo As DropDownList, ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDocumento As Integer)
        Dim obj As New Negocio.Serie
        cbo.DataSource = obj.SelectCboxIdsEmpTienTDoc(IdEmpresa, IdTienda, IdTipoDocumento)
        cbo.DataTextField = "Numero"
        cbo.DataValueField = "IdSerie"
        cbo.DataBind()
    End Sub

    Public Sub LLenarCboEstadoDocumento(ByVal cbo As DropDownList)
        Dim Lista As New List(Of Entidades.EstadoDocumento)
        ' Dim EstDoc As New Entidades.EstadoDocumento
        Dim obj As New Negocio.EstadoDocumento
        Lista = obj.SelectCbo
        ' EstDoc.Id = 0
        ' EstDoc.Descripcion = "-----------"
        '  Lista.Insert(0, EstDoc)
        cbo.DataSource = Lista
        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Public Sub LLenarCboEstadoCancelacion(ByVal cbo As DropDownList, ByVal addElemento As Boolean)

        Dim Lista As List(Of Entidades.EstadoCancelacion) = (New Negocio.EstadoCancelacion).SelectCbo

        If (addElemento) Then
            Lista.Insert(0, New Entidades.EstadoCancelacion(0, "----"))
        End If

        cbo.DataSource = Lista
        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Public Sub LLenarCboEstadoEntrega(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.EstadoEntrega
        Dim Lista As List(Of Entidades.EstadoEntrega) = obj.SelectCbo
        If addElemento Then
            Dim objEstadoEntrega As New Entidades.EstadoEntrega(0, "-----")
            Lista.Insert(0, objEstadoEntrega)
        End If
        cbo.DataSource = Lista
        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LLenarCboTienda(ByVal cbo As DropDownList, Optional ByVal busqueda As Boolean = False)
        Dim obj As New Negocio.Tienda
        Dim lista As List(Of Entidades.Tienda) = obj.SelectCbo
        If busqueda Then
            Dim objTienda As New Entidades.Tienda
            objTienda.Id = 0
            objTienda.Nombre = "-----"
            lista.Insert(0, objTienda)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LLenarCboDepartamento(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Ubigeo
        Dim lista As List(Of Entidades.Ubigeo) = obj.SelectAllDepartamentos
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "CodDpto"
        cbo.DataBind()
    End Sub
    Public Sub LLenarCboProvincia(ByVal cbo As DropDownList, ByVal CodDpto As String)
        Dim obj As New Negocio.Ubigeo
        Dim lista As List(Of Entidades.Ubigeo) = obj.SelectAllProvinciasxCodDpto(CodDpto)
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "CodProv"
        cbo.DataBind()
    End Sub
    Public Sub LLenarCboDistrito(ByVal cbo As DropDownList, ByVal CodDpto As String, ByVal CodProv As String)
        Dim obj As New Negocio.Ubigeo
        Dim lista As List(Of Entidades.Ubigeo) = obj.SelectAllDistritosxCodDptoxCodProv(CodDpto, CodProv)
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "CodDist"
        cbo.DataBind()
    End Sub
    Public Sub LLenarCboSubLinea(ByVal cbo As DropDownList, Optional ByVal busqueda As Boolean = False)
        Dim obj As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = obj.SelectCbo
        If busqueda Then
            Dim objSubLinea As New Entidades.SubLinea
            objSubLinea.Id = 0
            objSubLinea.Nombre = "----------"
            lista.Insert(0, objSubLinea)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarZonaUbigeo(ByVal cbo As DropDownList, Optional ByVal busqueda As Boolean = False)
        Dim obj As New Negocio.ZonaUbigeo
        Dim lista As List(Of Entidades.ZonaUbigeo) = obj.SelectCbo
        If busqueda Then
            Dim obj2 As New Entidades.ZonaUbigeo
            obj2.IdZona = "0"
            obj2.Nombre = "----------"
            lista.Insert(0, obj2)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdZona"
        cbo.DataBind()
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="cbo"></param>
    ''' <param name="ver_descripcion">1:completa 2:Abv </param>
    ''' <param name="addElement"></param>
    ''' <remarks></remarks>
    Public Sub LlenarTipoZona(ByVal cbo As DropDownList, ByVal ver_descripcion As Integer, Optional ByVal addElement As Boolean = False)
        Dim lista As List(Of Entidades.ZonaTipo) = (New Negocio.ZonaTipo).SelectCbo

        If addElement Then
            lista.Insert(0, New Entidades.ZonaTipo(0))
        End If
        cbo.DataSource = lista
        If ver_descripcion = 1 Then
            cbo.DataTextField = "Nombre"
        Else
            cbo.DataTextField = "Abv"
        End If
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarTipoDireccion(ByVal cbo As DropDownList)
        Dim obj As New Negocio.TipoDireccion
        cbo.DataSource = obj.SelectCbo
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarTipoVia(ByVal cbo As DropDownList)
        Dim obj As New Negocio.ViaTipo
        cbo.DataSource = obj.SelectCbo
        cbo.DataTextField = "Abv"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarGiro(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Giro
        cbo.DataSource = obj.SelectCbo
        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarTipoTelefono(ByVal cbo As DropDownList)
        Dim obj As New Negocio.TipoTelefono
        cbo.DataSource = obj.SelectCbo
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarTipoCorreo(ByVal cbo As DropDownList)
        Dim obj As New Negocio.TipoCorreo
        cbo.DataSource = obj.SelectCbo
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboNacionalidad(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        Dim lista As List(Of Entidades.Nacionalidad) = (New Negocio.Nacionalidad).SelectCbo
        If addElement Then
            Dim objNacionalidad As New Entidades.Nacionalidad
            objNacionalidad.Id = 0
            objNacionalidad.Descripcion = "-----"
            lista.Insert(0, objNacionalidad)
        End If
        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboCargo(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Cargo
        Dim lista As List(Of Entidades.Cargo) = obj.SelectCbo
        Dim objCargo As New Entidades.Cargo
        objCargo.Id = 0
        objCargo.Nombre = "-----"
        lista.Insert(0, objCargo)
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboTipoPV(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = True)

        Dim obj As New Negocio.TipoPrecioV
        Dim lista As List(Of Entidades.TipoPrecioV) = obj.SelectCbo

        If (addElemento) Then

            Dim objTipoPV As New Entidades.TipoPrecioV
            objTipoPV.IdTipoPv = 0
            objTipoPV.Nombre = "-Todos-"
            lista.Insert(0, objTipoPV)

        End If

        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdTipoPv"
        cbo.DataSource = lista
        cbo.DataBind()

    End Sub
    Public Sub LlenarCboTipoPV1(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)

        Dim lista As List(Of Entidades.TipoPrecioV) = (New Negocio.TipoPrecioV).SelectCbo1

        If (addElement) Then
            lista.Insert(0, New Entidades.TipoPrecioV(0, "-----"))
        End If



        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdTipoPv"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboTipoAgente(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)

        cbo.DataSource = (New Negocio.TipoAgente).SelectCbo
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdAgente"
        cbo.DataBind()

        If addElement Then
            cbo.Items.Insert(0, New ListItem("------", "0"))
        End If

    End Sub
    Public Sub LlenarCboCondTrabajo(ByVal cbo As DropDownList)
        Dim obj As New Negocio.CondTrabajo
        Dim lista As List(Of Entidades.CondTrabajo) = obj.SelectCbo
        Dim objCondTrabajo As New Entidades.CondTrabajo
        objCondTrabajo.Id = 0
        objCondTrabajo.Nombre = "-----"
        lista.Insert(0, objCondTrabajo)
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboEstadoCivil(ByVal cbo As DropDownList)
        Dim obj As New Negocio.EstadoCivil
        Dim lista As List(Of Entidades.EstadoCivil) = obj.SelectCbo
        Dim objEstadoCivil As New Entidades.EstadoCivil
        objEstadoCivil.Id = 0
        objEstadoCivil.Nombre = "-----"
        lista.Insert(0, objEstadoCivil)
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboProfesion(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Profesion
        Dim lista As List(Of Entidades.Profesion) = obj.SelectCbo
        Dim objProfesion As New Entidades.Profesion
        objProfesion.Id = 0
        objProfesion.Nombre = "-----"
        lista.Insert(0, objProfesion)
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboOcupacion(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Ocupacion
        Dim lista As List(Of Entidades.Ocupacion) = obj.SelectCbo
        Dim objOcupacion As New Entidades.Ocupacion
        objOcupacion.Id = 0
        objOcupacion.Nombre = "-----"
        lista.Insert(0, objOcupacion)
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboProductos(ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim ngcProducto As New Negocio.Producto
        Dim lista As List(Of Entidades.Producto) = ngcProducto.SelectProductoxLineaSubLinea(idlinea, idsublinea)
        If addElemento Then
            Dim objProducto As New Entidades.Producto(0, "-----")
            lista.Insert(0, objProducto)
        End If
        cbo.DataTextField = "Descripcion"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub

    Public Sub llenarCboTipoExistencia(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim Lista As List(Of Entidades.TipoExistencia) = (New Negocio.TipoExistencia).SelectAllActivo()
        If addElemento Then
            Dim obj As New Entidades.TipoExistencia(0, "-----")
            Lista.Insert(0, obj)
        End If
        cbo.DataSource = Lista
        cbo.DataValueField = "Id"
        cbo.DataTextField = "Nombre"
        cbo.DataBind()

        If cbo.Items.FindByValue("1") IsNot Nothing Then
            cbo.SelectedValue = "1"
        End If

    End Sub

    Public Sub LlenarCboTipoExistenciaxIdVehiculo(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Vehiculo
        Dim lista As List(Of Entidades.Vehiculo) = obj.SelectCboTipoExistenciaxIdVehiculo
        Dim objVehiculo As New Entidades.Vehiculo
        'objVehiculo.IdTipoExistencia = 0
        'objVehiculo.nombreTipoExistencia = "-----"
        'lista.Insert(0, objVehiculo)
        If lista.Count = 1 Then
            cbo.Enabled = False
        Else
            cbo.Enabled = True
        End If
        cbo.DataTextField = "nombreTipoExistencia"
        cbo.DataValueField = "IdTipoExistencia"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub
    Public Sub llenarCboAreaxEmpresa(ByVal cb As DropDownList, ByVal IdEmpresa As Integer, Optional ByVal addValor As Boolean = False)
        Dim Lista As List(Of Entidades.Area) = (New Negocio.Area).SelectCboxIdEmpresa(IdEmpresa)
        If addValor Then
            Lista.Insert(0, (New Entidades.Area(0, "-----")))
        End If
        cb.DataSource = Lista
        cb.DataValueField = "Id"
        cb.DataTextField = "DescripcionCorta"
        cb.DataBind()
    End Sub
    Public Sub LlenarComboTipoDocumento(ByVal cb As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.TipoDocumento
        Dim lista As List(Of Entidades.TipoDocumento) = obj.SelectCbo
        If addElemento Then
            Dim objTipoDocumento As New Entidades.TipoDocumento(0, "---")
            lista.Insert(0, objTipoDocumento)
        End If
        cb.DataSource = lista
        cb.DataValueField = "Id"
        cb.DataTextField = "Descripcion"
        cb.DataBind()
    End Sub
    Public Sub LlenarCboPerfilUsuario(ByVal cbo As DropDownList, ByVal idusuario As Integer, Optional ByVal addElemento As Boolean = False)
        Dim negOpcion As New Negocio.opcion
        Dim lista As List(Of Entidades.Perfil) = negOpcion.SelectPerfilxUsuario(idusuario)
        If addElemento Then
            Dim objOpcion As New Entidades.Perfil(0, "-----")
            lista.Insert(0, objOpcion)

        End If
        cbo.DataTextField = "nomPerfil"
        cbo.DataValueField = "IdPerfil"
        cbo.DataSource = lista
        cbo.DataBind()

    End Sub

    Public Sub LlenarCboProcedencia(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Procedencia) = (New Negocio.Procedencia).SelectAllActivo_Cbo
        If addElement Then
            Dim objProcedencia As New Entidades.Procedencia("0", "-----", True)
            lista.Insert(0, objProcedencia)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdProcedencia"
        cbo.DataBind()

    End Sub
    Public Sub LlenarCboPaisxIdProcedencia(ByVal IdProcedencia As String, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Pais) = (New Negocio.Pais).SelectAllActivoxIdProcedencia_Cbo(IdProcedencia)
        If addElement Then
            Dim objPais As New Entidades.Pais("0", "0", "-----", True)
            lista.Insert(0, objPais)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdPais"
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboMarca(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Marca) = (New Negocio.Marca).SelectAllActivo()
        If addElement Then
            Dim objMarca As New Entidades.Marca(0, "-----", True)
            lista.Insert(0, objMarca)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Descripcion"
        'cbo.DataTextField = "Abv"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboFormato(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Material) = (New Negocio.Material).SelectAllActivo
        If addElement Then
            Dim objFormato As New Entidades.Material(0, "-----", True)
            lista.Insert(0, objFormato)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Descripcion"
        'cbo.DataTextField = "Abv"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboProcedenciaxIdPais(ByVal IdPais As String, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Procedencia) = (New Negocio.Procedencia).SelectAllActivoxIdPais(IdPais)
        If addElement Then
            Dim objProcedencia As New Entidades.Procedencia("0", "-----", True)
            lista.Insert(0, objProcedencia)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdProcedencia"
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboPais(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Pais) = (New Negocio.Pais).SelectAllActivo
        If addElement Then
            Dim objPais As New Entidades.Pais(CStr(0), "-----", True)
            lista.Insert(0, objPais)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdPais"
        cbo.DataBind()
    End Sub


    Public Sub LlenarCboPaisTodos(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Pais) = (New Negocio.Pais).SelectAllActivoTodos
        If addElement Then
            Dim objPais As New Entidades.Pais(CStr(0), "-----", True)
            lista.Insert(0, objPais)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdPais"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboFabricante(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Fabricante) = (New Negocio.Fabricante).SelectAllActivo()
        If addElement Then
            Dim objFabricante As New Entidades.Fabricante(0, "-----", True)
            lista.Insert(0, objFabricante)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NombreCorto"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboModelo(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Modelo) = (New Negocio.Modelo).SelectAllActivo()
        If addElement Then
            Dim objModelo As New Entidades.Modelo("0", "-----", True)
            lista.Insert(0, objModelo)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "m_Nombre"
        cbo.DataValueField = "IdModelo"
        cbo.DataBind()
    End Sub

    Public Sub LlenarCboProveedor(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.PersonaView) = (New Negocio.Proveedor).SelectCbo
        If addElement Then
            Dim objModelo As New Entidades.PersonaView(0, "-----", "-----", "-----")
            lista.Insert(0, objModelo)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "RazonSocial"
        cbo.DataValueField = "IdPersona"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboProveedorImportados(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.PersonaView) = (New Negocio.Proveedor).SelectCboImportado
        If addElement Then
            Dim objModelo As New Entidades.PersonaView(0, "-----", "-----", "-----")
            lista.Insert(0, objModelo)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "RazonSocial"
        cbo.DataValueField = "IdPersona"
        cbo.DataBind()
    End Sub

    Public Sub LlenarMarcaxLinea(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.MarcaLinea) = (New Negocio.MarcaLinea).SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia)
        If addElement Then
            Dim objMarcaLinea As New Entidades.MarcaLinea(0, "-----", True)
            lista.Insert(0, objMarcaLinea)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NomMarca"
        cbo.DataValueField = "IdMarca"
        cbo.DataBind()
    End Sub
    Public Sub LlenarProveedorxLinea(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.ProveedorLinea) = (New Negocio.ProveedorLinea).SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia)
        If addElement Then
            Dim objProveedorLinea As New Entidades.ProveedorLinea(0, "-----", True)
            lista.Insert(0, objProveedorLinea)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NomProveedor"
        cbo.DataValueField = "IdProveedor"
        cbo.DataBind()
    End Sub


    Public Sub LlenarFormatoxLinea(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.FormatoLinea) = (New Negocio.FormatoLinea).SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia)
        If addElement Then
            Dim objFormatoLinea As New Entidades.FormatoLinea(0, "-----", True)
            lista.Insert(0, objFormatoLinea)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NomFormato"
        cbo.DataValueField = "IdFormato"
        cbo.DataBind()
    End Sub

    Public Sub LlenarFabricantexLinea(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal idpais As String, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.FabricanteLinea) = (New Negocio.FabricanteLinea).SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia, idpais)
        If addElement Then
            Dim objFabricanteLinea As New Entidades.FabricanteLinea(0, "-----", True)
            lista.Insert(0, objFabricanteLinea)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NomFabricante"
        cbo.DataValueField = "IdFabricante"
        cbo.DataBind()
    End Sub
    Public Sub LlenarPaisxLinea(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.PaisLinea) = (New Negocio.PaisLinea).SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia)
        If addElement Then
            Dim objPaisLinea As New Entidades.PaisLinea("0", "-----", True)
            lista.Insert(0, objPaisLinea)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NomPais"
        cbo.DataValueField = "IdPais"
        cbo.DataBind()
    End Sub

    Public Sub LlenarModeloxLinea(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.ModeloLinea) = (New Negocio.ModeloLinea).SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia)
        If addElement Then
            Dim objModeloLinea As New Entidades.ModeloLinea("0", "-----", True)
            lista.Insert(0, objModeloLinea)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NomModelo"
        cbo.DataValueField = "IdModelo"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboEstilo(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Estilo) = (New Negocio.Estilo).SelectAllActivo
        If addElement Then
            Dim objEstilo As New Entidades.Estilo(0, "-----", True)
            lista.Insert(0, objEstilo)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Descripcion"
        'cbo.DataTextField = "Abv"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboTransito(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Transito) = (New Negocio.Transito).SelectAllActivo
        If addElement Then
            Dim objTransito As New Entidades.Transito(0, "-----", True)
            lista.Insert(0, objTransito)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        'cbo.DataTextField = "Abv"
        cbo.DataValueField = "IdTransito"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCboCalidad(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Calidad) = (New Negocio.Calidad).SelectAllActivo
        If addElement Then
            Dim objCalidad As New Entidades.Calidad(0, "-----", True)
            lista.Insert(0, objCalidad)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        'cbo.DataTextField = "Abv"
        cbo.DataValueField = "IdCalidad"
        cbo.DataBind()
    End Sub
    Public Sub LlenarEstiloxLinea(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.EstiloLinea) = (New Negocio.EstiloLinea).SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia)
        If addElement Then
            Dim objEstiloLinea As New Entidades.EstiloLinea(0, "-----", True)
            lista.Insert(0, objEstiloLinea)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NomEstilo"
        cbo.DataValueField = "IdEstilo"
        cbo.DataBind()

    End Sub
    Public Sub LlenarCboTipoDocumentosCaja(ByVal cbo As DropDownList, Optional ByVal AddLinea As Boolean = False)
        Dim obj As New Negocio.TipoDocumento
        Dim lista As List(Of Entidades.TipoDocumento) = obj.SelecttipodocumentosCaja
        If AddLinea = True Then
            Dim objLinea As New Entidades.TipoDocumento(0, "--------")
            lista.Insert(0, objLinea)
        End If

        cbo.DataSource = lista
        cbo.DataTextField = "DescripcionCorto"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarTransitoxLinea(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.TransitoLinea) = (New Negocio.TransitoLinea).SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia)
        If addElement Then
            Dim objTransitoLinea As New Entidades.TransitoLinea(0, "-----", True)
            lista.Insert(0, objTransitoLinea)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NomTransito"
        cbo.DataValueField = "IdTransito"
        cbo.DataBind()
    End Sub
    Public Sub LlenarCalidadxLinea(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.CalidadLinea) = (New Negocio.CalidadLinea).SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia)
        If addElement Then
            Dim objCalidadLinea As New Entidades.CalidadLinea(0, "-----", True)
            lista.Insert(0, objCalidadLinea)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NomCalidad"
        cbo.DataValueField = "IdCalidad"
        cbo.DataBind()
    End Sub
    Public Sub llenarCboRegimen(ByVal cbo As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.Regimen
        Dim lista As List(Of Entidades.Regimen) = obj.SelectCbo()

        If addElemento Then
            Dim objRegimen As New Entidades.Regimen(0, "-----")
            lista.Insert(0, objRegimen)
        End If

        cbo.DataValueField = "Id"
        cbo.DataTextField = "Nombre"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub

    'CargarDatosCancepto
    Public Sub LlenarCboMotivoGastoxIdConcepto(ByVal cbo As DropDownList, ByVal IdConcepto As Integer, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.MotivoGasto).SelectxConceptoxMotivoGasto(IdConcepto)
            .DataTextField = "Nombre_MG"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboTipoPrecioImportacion(ByVal cbo As DropDownList, Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = (New Negocio.TipoPrecioImportacion).SelectAllActivo
            .DataTextField = "NomCorto"
            .DataValueField = "Id"
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub
    Public Sub LlenarCboColor(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Color) = (New Negocio.Color).SelectAllActivo
        If addElement Then
            Dim objColor As New Entidades.Color(0, "-----", True)
            lista.Insert(0, objColor)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        'cbo.DataTextField = "Abv"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Public Sub LlenarComboSubLinea(ByVal cb As DropDownList, Optional ByVal addElemento As Boolean = False)
        Dim obj As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = obj.SelectCbo
        If addElemento Then
            Dim objSubLinea As New Entidades.SubLinea(0, "---")
            lista.Insert(0, objSubLinea)
        End If
        cb.DataSource = lista
        cb.DataValueField = "Id"
        cb.DataTextField = "Nombre"
        cb.DataBind()
    End Sub

    Public Sub LlenarColorxLinea(ByVal idlinea As Integer, ByVal idtipoexistencia As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.Color_Linea) = (New Negocio.Color_Linea).SelectAllActivoxIdLineaIdTipoExistencia(idlinea, idtipoexistencia)
        If addElement Then
            Dim objColorLinea As New Entidades.Color_Linea(0, "-----", True)
            lista.Insert(0, objColorLinea)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NombreCorto"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    'Public Sub LlenarSub_SubLineaxLinea(ByVal idsubsublinea As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
    '    Dim lista As List(Of Entidades.Sub_SubLinea) = (New Negocio.Sub_SubLinea).SelectIdSubLinea(idsubsublinea)
    '    If addElement Then
    '        Dim objSubSubLinea As New Entidades.Sub_SubLinea(0, "-----", True)
    '        lista.Insert(0, objSubSubLinea)
    '    End If
    '    cbo.DataSource = lista
    '    cbo.DataTextField = "Sub_SubL_Abv"
    '    cbo.DataValueField = "IdSub_SubLinea"
    '    cbo.DataBind()
    'End Sub

    'Public Sub LlenarCboTipoTabla(ByVal estado As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
    '    Dim lista As List(Of Entidades.TipoTabla) = (New Negocio.TipoTabla).SelectAllTipoTablaxEstado(estado)
    '    If addElement Then
    '        Dim objtipotabla As New Entidades.TipoTabla(0, "Seleccione Tabla", True)
    '        lista.Insert(0, objtipotabla)
    '    End If
    '    cbo.DataSource = lista
    '    cbo.DataTextField = "Nombre"
    '    cbo.DataValueField = "IdTipoTabla"
    '    cbo.DataBind()
    'End Sub


    Public Sub LlenarCboTipoTabla(ByVal estado As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.TipoTabla) = (New Negocio.TipoTabla).SelectAllTipoTablaxEstado(estado)
        If addElement Then
            Dim objtipotabla As New Entidades.TipoTabla(0, "Seleccione Tabla", True)
            lista.Insert(0, objtipotabla)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdTipoTabla"
        cbo.DataBind()
    End Sub


    'LLENAR combo EL TIPO TABLA  de configuracion tipo tabla por valor
    Public Sub LlenarCboTipoTabla2(ByVal Estado As Integer, ByVal Linea As Integer, ByVal Sublinea As Integer, ByVal cbo As DropDownList, ByVal addElement As Boolean)

        Dim lista As List(Of Entidades.TipoTabla) = (New Negocio.TipoTabla).SelectAllTipoTablaxLineaSub(Estado, Linea, Sublinea)
        If addElement Then
            Dim objtipotabla As New Entidades.TipoTabla(0, "Seleccione Tabla", True)
            lista.Insert(0, objtipotabla)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "IdTipoTabla"
        cbo.DataBind()
    End Sub


    Public Sub LlenarCboSubLineaxIdLineaxIdTipoExistencia(ByVal cbo As DropDownList, ByVal idLinea As Integer, ByVal idtipoexistencia As Integer, Optional ByVal addElemento As Boolean = False)
        Dim lista As List(Of Entidades.SubLinea)
        If idLinea = 0 Then
            lista = New List(Of Entidades.SubLinea)
            Dim objSubLinea As New Entidades.SubLinea(0, "-----")
            lista.Add(objSubLinea)
        Else
            lista = (New Negocio.SubLinea).SelectActivoxLineaxTipoExistencia(idLinea, idtipoexistencia)
            If addElemento Then
                Dim objSubLinea As New Entidades.SubLinea(0, "-----")
                lista.Insert(0, objSubLinea)
            End If
        End If
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataSource = lista
        cbo.DataBind()
    End Sub

End Class

