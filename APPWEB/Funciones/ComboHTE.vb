﻿Imports Entidades
Imports System.Web.UI.WebControls

Public Class ComboHTE

    Public Sub LlenarCboEmpresaxIdUsuario(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), ByVal addElement As Boolean)
        'Public Sub LlenarCboEmpresaxIdUsuario(ByVal cbo As DropDownList, ByVal lista As List(Of Propietario), ByVal IdUsuario As Integer, ByVal addElement As Boolean)
        'Dim lista As List(Of Entidades.Propietario) = (New Negocio.EmpresaTiendaUsuario).SelectEmpresaxIdUsuario(IdUsuario)
        If addElement Then
            Dim obj As New beCampoEntero()
            obj.campo1 = 0
            obj.campo2 = "-----"
            lista.Insert(0, obj)
        End If
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'NombreComercial
            .DataValueField = "campo1" 'Id
            .DataBind()
        End With

    End Sub

    Public Sub LlenarCboTiendaxIdEmpresaxIdUsuario(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), ByVal addElemento As Boolean)
        'Public Sub LlenarCboTiendaxIdEmpresaxIdUsuario(ByVal cbo As DropDownList, ByVal lista As List(Of Tienda), ByVal IdEmpresa As Integer, ByVal IdUsuario As Integer, ByVal addElemento As Boolean)
        'Dim lista As List(Of Entidades.Tienda) = (New Negocio.EmpresaTiendaUsuario).SelectTiendaxIdEmpresaxIdUsuario(IdUsuario, IdEmpresa)
        If addElemento Then
            Dim obj As New beCampoEntero()
            obj.campo1 = 0
            obj.campo2 = "-----"
            lista.Insert(0, obj)
        End If
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'Nombre
            .DataValueField = "campo1" 'Id
            .DataBind()
        End With
    End Sub

    Public Sub LLenarCboSeriexIdsEmpTienTipoDoc(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero))
        'Public Sub LLenarCboSeriexIdsEmpTienTipoDoc(ByVal cbo As DropDownList, ByVal lista As List(Of Serie), ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdTipoDocumento As Integer)
        'Dim obj As New Negocio.Serie
        'cbo.DataSource = obj.SelectCboxIdsEmpTienTDoc(IdEmpresa, IdTienda, IdTipoDocumento)
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'Numero
            .DataValueField = "campo1" 'IdSerie
            .DataBind()
        End With
    End Sub

    Public Sub LLenarCboTipoDocumentoRefxIdTipoDocumento(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero2), ByVal Opcion As Integer, Optional ByVal addElement As Boolean = False)
        'Public Sub LLenarCboTipoDocumentoRefxIdTipoDocumento(ByVal cbo As DropDownList, ByVal lista As List(Of TipoDocumento), ByVal IdTipoDocumento As Integer, ByVal Opcion As Integer, Optional ByVal addElement As Boolean = False)
        '(New Negocio.TipoDocumentoRef).SelectxIdtipoDocRef(IdTipoDocumento)
        With cbo
            .DataSource = lista
            .DataValueField = "campo1" 'Id
            Select Case Opcion
                Case 1
                    .DataTextField = "campo2" 'DescripcionCorto
                Case 2
                    .DataTextField = "campo3" 'Descripcion
            End Select
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboMoneda(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), Optional ByVal addElemento As Boolean = False)
        'Dim obj As New Negocio.Moneda
        'Dim lista As List(Of Entidades.Moneda) = obj.SelectCbo
        If addElemento Then
            Dim obj As New beCampoEntero
            obj.campo1 = 0
            obj.campo2 = "---"
            lista.Insert(0, obj)
        End If
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'Simbolo
            .DataValueField = "campo1" 'Id
            .DataBind()
        End With
    End Sub

    Public Sub LLenarCboEstadoDocumento(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero))
        'Dim Lista As New List(Of Entidades.EstadoDocumento)
        '' Dim EstDoc As New Entidades.EstadoDocumento
        'Dim obj As New Negocio.EstadoDocumento
        'Lista = obj.SelectCbo
        '' EstDoc.Id = 0
        '' EstDoc.Descripcion = "-----------"
        ''  Lista.Insert(0, EstDoc)
        Dim obj As New beCampoEntero()
        obj.campo1 = 0
        obj.campo2 = "-----------"
        lista.Insert(0, obj)
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'Descripcion
            .DataValueField = "campo1" 'Id
            .DataBind()
        End With
    End Sub

    Public Sub llenarCboTipoOperacionxIdTpoDocumento(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), ByVal IdTipoDocumento As Integer, Optional ByVal addElemento As Boolean = False)
        'Dim Lista As List(Of Entidades.TipoOperacion) = (New Negocio.TipoOperacion).SelectCboxIdTipoDocumento(IdTipoDocumento)
        If addElemento Then
            Dim obj As New beCampoEntero()
            obj.campo1 = 0
            obj.campo2 = "-----"
            lista.Insert(0, obj)
        End If
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'Nombre
            .DataValueField = "campo1" 'Id
            .DataBind()
        End With
    End Sub

    Public Sub LlenarCboMotivoTrasladoxIdTipoOperacion(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), ByVal addElement As Boolean)
        'Dim lista As List(Of Entidades.MotivoTraslado) = (New Negocio.MotivoT_TipoOperacion).SelectCboxIdTipoOperacion(IdTipoOperacion)
        If (addElement) Then
            Dim obj As New beCampoEntero()
            obj.campo1 = 0
            obj.campo2 = "---"
            lista.Insert(0, obj)
        End If
        With cbo
            .DataValueField = "campo1" 'Id
            .DataTextField = "campo2" 'Nombre
            .DataSource = lista
            .DataBind()
        End With
    End Sub

    Public Sub LlenarCboCondicionPago(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), Optional ByVal addElement As Boolean = False)
        'Dim obj As New Negocio.CondicionPago
        'cbo.DataSource = obj.SelectAllActivo
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'Nombre
            .DataValueField = "campo1" 'Id
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("-Todos-", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboMedioPagoxIdTipoDocumentoxIdCondicionPago(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), Optional ByVal addElemento As Boolean = False)
        '(New Negocio.MedioPago).SelectxIdTipoDocumentoxIdCondicionPago(IdTipoDocumento, IdCondicionPago)
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'Nombre
            .DataValueField = "campo1" 'Id
            .DataBind()
            If (addElemento) Then
                cbo.Items.Insert(0, (New ListItem("-Todos-", "0")))
            End If
        End With
    End Sub

    Public Sub LlenarCboTipoPV(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), Optional ByVal addElemento As Boolean = True)
        'Dim obj As New Negocio.TipoPrecioV
        'Dim lista As List(Of Entidades.TipoPrecioV) = obj.SelectCbo
        If (addElemento) Then
            Dim obj As New Entidades.beCampoEntero
            obj.campo1 = 0
            obj.campo2 = "-Todos-"
            lista.Insert(0, obj)
        End If
        With cbo
            .DataTextField = "campo2" 'Nombre
            .DataValueField = "campo1" 'IdTipoPv
            .DataSource = lista
            .DataBind()
        End With
    End Sub

    Public Sub LlenarCboRolCliente(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero2), Optional ByVal addElement As Boolean = True)
        'Dim lista As List(Of Entidades.Rol) = (New Negocio.Rol).SelectCboCliente
        If addElement Then
            Dim obj As New Entidades.beCampoEntero2
            obj.campo1 = 0
            obj.campo2 = "-----"
            obj.campo3 = "1"
            lista.Insert(0, obj)
        End If
        With cbo
            .DataTextField = "campo2" 'Nombre
            .DataValueField = "campo1" 'Id
            .DataSource = lista
            .DataBind()
        End With
    End Sub

    'Public Sub LlenarCboTipoPV(ByVal cbo As DropDownList, ByVal lista As List(Of TipoPrecioV), Optional ByVal addElemento As Boolean = True)
    '    'Dim obj As New Negocio.TipoPrecioV
    '    'Dim lista As List(Of Entidades.TipoPrecioV) = obj.SelectCbo
    '    If (addElemento) Then
    '        Dim objTipoPV As New Entidades.TipoPrecioV
    '        objTipoPV.IdTipoPv = 0
    '        objTipoPV.Nombre = "-Todos-"
    '        lista.Insert(0, objTipoPV)
    '    End If
    '    With cbo
    '        .DataTextField = "Nombre"
    '        .DataValueField = "IdTipoPv"
    '        .DataSource = lista
    '        .DataBind()
    '    End With
    'End Sub

    Public Sub LlenarCboTipoAgente(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), Optional ByVal addElement As Boolean = False)
        'cbo.DataSource = (New Negocio.TipoAgente).SelectCbo
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'Nombre
            .DataValueField = "campo1" 'IdAgente
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LLenarCboDepartamento(ByVal cbo As DropDownList, ByVal lista As List(Of beCboDepartamento))
        'Dim obj As New Negocio.Ubigeo
        'Dim lista As List(Of Entidades.Ubigeo) = obj.SelectAllDepartamentos
        With cbo
            .DataSource = lista
            .DataTextField = "Nombre"
            .DataValueField = "CodDpto"
            '.DataTextField = "campo2" 'Nombre
            '.DataValueField = "campo1" 'CodDpto
            .DataBind()
        End With
    End Sub

    Public Sub LLenarCboProvincia(ByVal cbo As DropDownList, ByVal lista As List(Of beCboProvincia))
        'Dim obj As New Negocio.Ubigeo
        'Dim lista As List(Of Entidades.Ubigeo) = obj.SelectAllProvinciasxCodDpto(CodDpto)
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "CodProv"
        'cbo.DataTextField = "campo2" 'Nombre
        'cbo.DataValueField = "campo1" 'CodProv
        cbo.DataBind()
    End Sub

    Public Sub LLenarCboDistrito(ByVal cbo As DropDownList, ByVal lista As List(Of beCboDistrito))
        'Dim obj As New Negocio.Ubigeo
        'Dim lista As List(Of Entidades.Ubigeo) = obj.SelectAllDistritosxCodDptoxCodProv(CodDpto, CodProv)
        cbo.DataSource = lista
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "CodDist"
        'cbo.DataTextField = "campo2" 'Nombre
        'cbo.DataValueField = "campo1" 'CodDist
        cbo.DataBind()
    End Sub

    Public Sub llenarCboAlmacenxIdTienda(ByVal cbo As DropDownList, ByVal lista As List(Of Entidades.beCampoEntero), Optional ByVal addElemento As Boolean = False)
        'Dim obj As New Negocio.Almacen
        'Dim lista As List(Of Entidades.Almacen) = obj.SelectCboxIdTienda(IdTienda)
        If addElemento Then
            Dim obj As New Entidades.beCampoEntero
            obj.campo1 = 0
            obj.campo2 = "-----"
            lista.Insert(0, obj)
        End If
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'Nombre
            .DataValueField = "campo1" 'IdAlmacen
            .DataBind()
        End With
    End Sub

    Public Sub llenarCboTipoExistencia(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), Optional ByVal addElemento As Boolean = False)
        'Dim Lista As List(Of Entidades.TipoExistencia) = (New Negocio.TipoExistencia).SelectAllActivo()
        If addElemento Then
            Dim obj As New Entidades.beCampoEntero
            obj.campo1 = 0
            obj.campo2 = "-----"
            lista.Insert(0, obj)
        End If
        With cbo
            .DataSource = lista
            .DataValueField = "campo1" 'Id
            .DataTextField = "campo2" 'Nombre
            .DataBind()
            If .Items.FindByValue("1") IsNot Nothing Then
                .SelectedValue = "1"
            End If
        End With
    End Sub

    Public Sub llenarCboLineaxTipoExistencia(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), Optional ByVal addValor As Boolean = False)
        If addValor Then
            Dim obj As New Entidades.beCampoEntero
            obj.campo1 = 0
            obj.campo2 = "-----"
            lista.Insert(0, obj)
        End If
        With cbo
            .DataSource = lista
            .DataValueField = "campo1" 'Id
            .DataTextField = "campo2" 'Descripcion
            .DataBind()
        End With
    End Sub

    Public Sub LlenarCboSubLineaxIdLineaxIdTipoExistencia(ByVal cbo As DropDownList)
        Dim lista As New List(Of beCampoEntero)
        Dim obj As New Entidades.beCampoEntero
        obj.campo1 = 0
        obj.campo2 = "-----"
        lista.Insert(0, obj)
        With cbo
            .DataTextField = "campo2" 'Nombre
            .DataValueField = "campo1" 'Id
            .DataSource = lista
            .DataBind()
        End With
    End Sub

    Public Sub LlenarCboTipoTablaxIdSubLinea(ByVal cbo As DropDownList)
        Dim lista As New List(Of beCampoEntero)
        Dim obj As New Entidades.beCampoEntero
        obj.campo1 = 0
        obj.campo2 = "Seleccione Tipo Tabla"
        lista.Insert(0, obj)
        With cbo
            .DataTextField = "campo2" 'Nombre
            .DataValueField = "campo1" 'IdTipoTabla
            .DataSource = lista
            .DataBind()
        End With
    End Sub

    Public Sub LLenarCboUsuario(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), ByVal AddElement As Boolean)
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'Nombres
            .DataValueField = "campo1" 'IdPersona
            .DataBind()
            If (AddElement) Then
                cbo.Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub LlenarCboTipoAlmacen(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), Optional ByVal addElement As Boolean = False)
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'Tipoalm_Nombre
            .DataValueField = "campo1" 'IdTipoAlmacen
            .DataBind()
            If addElement Then
                .Items.Insert(0, New ListItem("------", "0"))
            End If
        End With
    End Sub

    Public Sub llenarCboUnidadMedidaxIdMagnitud(ByVal cbo As DropDownList, ByVal lista As List(Of beCampoEntero), Optional ByVal addElemento As Boolean = False)
        With cbo
            .DataSource = lista
            .DataTextField = "campo2" 'DescripcionCorto
            .DataValueField = "campo1" 'Id
            .DataBind()
            If (addElemento) Then
                .Items.Insert(0, (New ListItem("-----", "0")))
            End If
        End With
    End Sub

End Class
