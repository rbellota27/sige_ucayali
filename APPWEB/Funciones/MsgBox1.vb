'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Option Explicit On
Option Strict On
Imports System.ComponentModel
Imports System.Web.UI
Imports System.Web.UI.WebControls
Public Class MsgBox1
    'Heredo de WebControl
    Inherits System.Web.UI.WebControls.WebControl
    'Implemento IPostBackEventHandler para poder enviar mensajes desde el c�digo JavaScript
    Implements IPostBackEventHandler
    'Ac� se guarda el mensaje a mostrar
    Private _Message As String
    'Ac� se guarda la clave para poder identificar a que mensaje se responde.
    Private _Key As String
    'Ac� guardamos si se desea disparar el evento en el Si
    Private _PostBackOnYes As Boolean
    'Y aqu� si se desea disparar el evento en el No
    Private _PostBackOnNo As Boolean
    'Definimos los eventos que puede disparar este control
    Public Event YesChoosed(ByVal sender As Object, ByVal Key As String)
    Public Event NoChoosed(ByVal sender As Object, ByVal Key As String)
    'Este m�todo se utiliza para mostrar un mensaje de confirmaci�n.
    Public Sub ShowConfirmation(ByVal Message As String, ByVal Key As String, _
    ByVal PostBackOnYes As Boolean, ByVal PostBackOnNo As Boolean)
        'El �a�a es para identificar que es un mensaje de confirmaci�n (ver Render)
        _Message = "�a�a" & Message
        _Key = Key
        _PostBackOnYes = PostBackOnYes
        _PostBackOnNo = PostBackOnNo
    End Sub
    'Este m�todo se usa para mostrar un mensaje simple
    Public Sub ShowMessage(ByVal Message As String)
        _Message = Message
    End Sub
    'Preparamos el control para enviar la funci�n javascript
    Protected Overrides Sub OnPreRender(ByVal e As EventArgs)
        If Not MyBase.Page.IsClientScriptBlockRegistered("MsgBox") Then
            'Registro todo el script del control
            Page.RegisterClientScriptBlock("MsgBox", FuncionJava1())
        End If
    End Sub
    Private Function FuncionJava1() As String
        'Ac� esta la clave de todo este ejemplo...
        'Si no se dispara el evento en el Si, se vac�a la variable.
        Dim miPostBackOnYes As String = "MsgBoxTextoMensaje="""";"
        'Si no se dispara el evento en el No, se vacia la variable.
        Dim miPostBackOnNo As String = "MsgBoxTextoMensaje="""";"
        'Dependiendo lo que se elija, hacemos referencia o no, a la funci�n que ejecuta el postback.
        'Adem�s recordamos quien esta disparando el evento. (_Key)
        If _PostBackOnYes Then
            'El .Net Framework guarda en miPostBackOnYes la llamada a la funci�n
            'javascript que dispara el evento
            miPostBackOnYes = Page.GetPostBackEventReference(Me, "Yes" & _Key)
        End If
        If _PostBackOnNo Then
            'El .Net Framework guarda en miPostBackOnNo la llamada a la funci�n
            'javascript que dispara el evento
            miPostBackOnNo = Page.GetPostBackEventReference(Me, "No_" & _Key)
        End If
        'Como se ve, se programa el evento onfocus del objeto window y se dispara la funci�n.
        'La funci�n verifica si MsgBoxTextoMensaje esta vac�a, si es as�, no hace nada, de
        'lo contrario muestra el cartel o la confirmaci�n, y luego vac�a la variable.
        'En el caso de confirmaci�n se dispara la funci�n java guardada en
        'miPostBackOnYes � en miPostBackOnNo
        Return "<script language=""javascript""> " & _
        "var MsgBoxTipoMensaje; " & _
        "var MsgBoxTextoMensaje; " & _
        "window.attachEvent(""onfocus"", MsgBoxMostrarMensaje); " & _
        "function MsgBoxMostrarMensaje() { " & _
        "if (MsgBoxTextoMensaje) { " & _
        "if (MsgBoxTextoMensaje != """") { " & _
        "if (MsgBoxTipoMensaje==2) {" & _
        " alert(MsgBoxTextoMensaje); " & _
        "} else {" & _
        "if (confirm(MsgBoxTextoMensaje)) { " & _
        miPostBackOnYes & _
        "} else { " & _
        miPostBackOnNo & _
        "}} MsgBoxTextoMensaje=""""; " & _
        " }}} </script>"
    End Function
    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        'Dibujamos el control
        If ModoDisenio(Me) Then
            'Si estamos en tiempo dise�o. Muestro el nombre del control.
            writer.Write(Me.ID)
        Else
            'Estamos en tiempo de ejecuci�n. Envi� el script con el contenido
            'de las variables de JavaScript que corresponda
            If _Message <> String.Empty Then
                'Antes de hacer nada reemplazo los Enter por espacios,
                'las comillas dobles por comillas simples.
                'Sino lo hacemos, no sale el cartel en el navegador.
                Dim miSB As System.Text.StringBuilder = New System.Text.StringBuilder(_Message)
                miSB.Replace(Microsoft.VisualBasic.vbCr, " "c)
                miSB.Replace(Microsoft.VisualBasic.vbLf, " "c)
                miSB.Replace("""", "'"c)
                'Verifico el tipo de mensaje
                If miSB.ToString.StartsWith("�a�a") Then
                    'Es un mensaje de confirmaci�n. (confirm)
                    'Env�o solo el valor de las variables de JavaScript, la funci�n
                    ' se ejecutar� cuando tome el foco la pagina. (Ver OnPreRender)"
                    Me.Page.Response.Write("<script>MsgBoxTipoMensaje=1;MsgBoxTextoMensaje = """ + miSB.ToString.Substring(4) + """;</script>")
                Else
                    'Es un mensaje com�n. (alert)
                    'Env�o solo el valor de las variables de JavaScript, la funci�n
                    ' se ejecutar� cuando tome el foco la pagina. (Ver OnPreRender)
                    Me.Page.Response.Write("<script>MsgBoxTipoMensaje=2;MsgBoxTextoMensaje = """ + miSB.ToString + """;</script>")
                End If
                miSB = Nothing
            End If
        End If
    End Sub
    'Funci�n privada para verificar si el control esta en modo dise�o.
    Private Shared Function ModoDisenio(ByVal QueControl As _
    System.Web.UI.WebControls.WebControl) As Boolean
        Dim DesignMode As Boolean = False
        Try
            DesignMode = QueControl.Site.DesignMode
        Catch : End Try
        Return DesignMode
    End Function
    'Este m�todo se ejecuta cuando el usuario clic en aceptar o cancelar.
    Public Sub RaisePostBackEvent(ByVal eventArgument As String) _
    Implements IPostBackEventHandler.RaisePostBackEvent
        Select Case eventArgument.Substring(0, 3)
            Case "Yes"
                'Si eligi� Aceptar la primera parte del argumento es Yes (Ver FuncionJava1)
                'Disparo el evento en la p�gina. Le env�o en "Key" quien hizo la llamada
                RaiseEvent YesChoosed(Me, eventArgument.Substring(3))
            Case "No_"
                'Si eligi� Aceptar la primera parte del argumento es No_ (Ver FuncionJava1)
                'Disparo el evento en la p�gina. Le env�o en "Key" quien hizo la llamada
                RaiseEvent NoChoosed(Me, eventArgument.Substring(3))
        End Select
    End Sub

End Class
