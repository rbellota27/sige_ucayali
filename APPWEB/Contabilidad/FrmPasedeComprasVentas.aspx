<%@ Page Title="Pase de Ventas/Compras SIGE" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmPasedeComprasVentas.aspx.vb" Inherits="APPWEB.FrmPasedeComprasVentas" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1" width="100%">


           <tr>
            <td class="TituloCelda">
               REGISTRO DE PASE DE VENTAS/COMPRAS
            </td>
        </tr>
        <tr>
        <td>
        <asp:Panel ID="Panel_Cab" runat="server">
                   <table width="100%">
                   <tr>
                   <td>
                   <table width="53%">
                   <tr>
                   <td></td>
                       <td class="Texto">      
                           <asp:Label ID="lblAnio" runat="server" Text="A�o: " ></asp:Label>         </td>       
                        <td>  
                            <asp:DropDownList ID="ddlAnio" runat="server"  Width="60PX" >
                            <asp:ListItem  Text="2013" Value="0"></asp:ListItem>
                            <asp:ListItem Selected="True" Text="2014" Value="1"></asp:ListItem>
                            <asp:ListItem  Text="2015" Value="2"></asp:ListItem>
                            <asp:ListItem  Text="2016" Value="3"></asp:ListItem>
                            <asp:ListItem  Text="2017" Value="4"></asp:ListItem>
                            <asp:ListItem  Text="2018" Value="5"></asp:ListItem>
                            </asp:DropDownList> 
                                    </td>     </tr>
                     <tr>    
                     <td></td>
                            <td align="right" class="Texto">
                                Fecha Inicio:
                            </td>
                            <td >
                                <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="150px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                                </cc1:CalendarExtender>
                           </td>
                           <td></td>
                          <td align="right" class="Texto">
                                Fecha de Fin:
                            </td>
                            <td>
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="150px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaFin" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaFin" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                                </cc1:CalendarExtender>
                            </td>
 
                        </tr>
                      
                   </table>
                   </td>
                 
                    </tr>
                    <tr>
                    <td>
                   <table width="40%">
                  
                   <tr>


                   <td>
                                <asp:Button id="btnLimpiar" runat="server" Text="Limpiar" Width="125px" />
                                </td>
                                
                                <td>
                                <asp:Button id="btnGuardar" runat="server" Text="Generar Pase" Width="125px"/>
                                </td>
                               
                               <%--  <td>
                                <asp:Button id="btnBuscar" runat="server" Text="Buscar" Width="125px"/>
                                </td>--%>
                                 <td>
                                <asp:Button id="btnVerPases" runat="server" Text="Ver Pases Realizados" Width="140px"/>
                                </td>
                   </tr>
                   </table>
                    </td>
                    </tr>
                   </table>
                   <br />

                  <table width="100%">
                   <tr>
                                 
                                <td class="TituloCelda">
                                    DOCUMENTOS que no pasar�n DEL SOLUCON AL SIGE
                                </td>
                            </tr>
                            </table >
                            <table width="100%">
                                <tr>
                                
                                <td>  
                                <asp:GridView ID="GV_PaseVentasCompras" runat="server" AutoGenerateColumns="False" 
                                        Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" 
                                        BorderWidth="1px" CellPadding="3"  >
                                       
                                <Columns>
                                  <%--  <asp:ButtonField Text = "Seleccionar" CommandName = "Select"  />--%>
                                   <asp:TemplateField HeaderText="Nro Voucher Conta." >
                                   <ItemTemplate>
                                    <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblNroVoucherConta"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"NroVoucherConta")%>'></asp:Label></td> 
                                             </tr>
                                             </table>                                        
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ruc" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                         <asp:Label ID="lblRuc"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Ruc")%>'></asp:Label>
                                   </ItemTemplate>
                                   
                                                                       
                                        <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                   
                                                                       
                                   </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Raz�n Social" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" >
                                   <ItemTemplate>
                                            <table>
                                            <tr>
                                            <td>   <asp:Label ID="lblDescripcionPersona"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"DescripcionPersona")%>'></asp:Label> </td>
                                          
                                            </table>
                                   </ItemTemplate>                      
                                         <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                         <ItemStyle  />
                                   </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Serie" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                            <table>
                                            <tr>
                                            <td> <asp:Label ID="lblSerie"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Serie")%>'></asp:Label></td>
                                            </tr>
                                            </table>
                                   </ItemTemplate>                      
                                         <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                         <ItemStyle HorizontalAlign="Center" />
                                   </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="NroDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                              <table>
                                            <tr>
                                            <td>
                                             <asp:Label ID="lblNroDocumento"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label></td>
                                            </tr>
                                            </table>
                                   </ItemTemplate>
                                                                    
                                   <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                   <ItemStyle HorizontalAlign="Center" />
                                                                    
                                   </asp:TemplateField>
                                   
                               <asp:TemplateField HeaderText="Fecha Emisi�n" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                              <table>
                                            <tr>
                                           <td>
                                         <asp:Label ID="lblFechaEmision"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"FechaEmision","{0:d}")%>'></asp:Label>
                                            </td></tr>
                                            </table>
                                   </ItemTemplate>
                                                                    
                                   <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                   <ItemStyle HorizontalAlign="Center" />
                                                                    
                                   </asp:TemplateField>
                                   
                                
                                     <asp:TemplateField HeaderText="Centro Costo" >
                                   <ItemTemplate>
                                         <asp:Label ID="lblCentroCosto"  runat="server"  ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"CentroCosto")%>'></asp:Label>
                                        
                                   </ItemTemplate>
                               
                                  </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Total">
                                   <ItemTemplate>
                                    <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblTotal"  runat="server"  ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Total","{0:F}")%>'></asp:Label>
                                           </td>
                                           </tr>
                                   </table>
                                   </ItemTemplate>
                               
                                  </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Base Soles">
                                   <ItemTemplate>
                                    <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblBaseSoles"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"BaseSoles","{0:F}")%>'></asp:Label> </td>
                                        
                                            </tr>
                                           </table>
                                     </ItemTemplate>
                                   </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Concepto">
                                   <ItemTemplate>
                                      <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblConcepto"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Concepto")%>'></asp:Label> </td>
                                         
                                                </tr>
                                                </table>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                   
<%--                                    <asp:TemplateField HeaderText="Tipo de Documento Ref." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                   <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblTipoDocumento"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"TipoDocumento")%>'></asp:Label> </td>
                                   </tr>
                                   </table>
                                   </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                   </asp:TemplateField>--%>
                                 <%--     <asp:TemplateField HeaderText="Doc. Referencia" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                      <table>
                                            <tr>
                                            <td>
                                         <asp:Label ID="lblNumeroDocumento"  runat="server" Font-Bold="true" ForeColor="Red"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"NumeroDocumento")%>'></asp:Label> </td>
                                     </tr>
                                     </table>
                                   </ItemTemplate>
                                          <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                          <ItemStyle HorizontalAlign="Center" />
                                     
                                   </asp:TemplateField>--%>                               
                                 <%-- <asp:TemplateField HeaderText="Fecha Emisi�n Doc." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                         <asp:Label ID="lblFechaEmision"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"mcp_Fecha","{0:d}")%>'></asp:Label>
                                   </ItemTemplate>                          
                                       <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                       <ItemStyle HorizontalAlign="Center" />
                                   </asp:TemplateField>
                                   
                                      <asp:TemplateField HeaderText="Fecha de Vencimiento Doc." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                         <asp:Label ID="lblFechaVenci"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaVenc","{0:d}")%>'></asp:Label>
                                   </ItemTemplate>
                                          <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                          <ItemStyle HorizontalAlign="Center" />
                                   </asp:TemplateField>--%>
                                 <%--    <asp:TemplateField HeaderText="Banco Proveedor">
                                   <ItemTemplate>
                                         <asp:Label ID="lbljulio"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Julio","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                   
                                   </asp:TemplateField>
                                   
                                        <asp:TemplateField HeaderText="Cuenta Proveedor">
                                   <ItemTemplate>
                                         <asp:Label ID="lblagosto"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Agosto","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                     
                                   </asp:TemplateField>--%>
                                 <%-- <asp:TemplateField HeaderText="Estado" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                   <ItemTemplate>
                                         <asp:Label ID="lblestado"  runat="server" ForeColor="#0B3861"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"edoc_Nombre")%>'></asp:Label>
                                   </ItemTemplate>
                                     
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                     
                                   </asp:TemplateField>--%>
                                </Columns>
                                <RowStyle CssClass="GrillaRow" ForeColor="#000066" />
                                <FooterStyle CssClass="GrillaFooter" BackColor="White" ForeColor="#000066" />
                                <PagerStyle CssClass="GrillaPager"
                                        HorizontalAlign="Left" BackColor="White" ForeColor="#000066" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" BackColor="#669999" Font-Bold="True" 
                                        ForeColor="White" />
                                <HeaderStyle CssClass="GrillaHeader" BackColor="#006699" Font-Bold="True" 
                                        ForeColor="White" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            </asp:GridView></td>
                                </tr>
                                     </table>
                                                       
             </asp:Panel>
        </td>
          
      
        </tr>
              
</table>


 <script language="javascript" type="text/javascript">

 
        
 </script>

</asp:Content>
