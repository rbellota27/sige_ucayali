﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmEmpresaTienda_SubLinea.aspx.vb" Inherits="APPWEB.FrmEmpresaTienda_SubLinea" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td>
            
                  <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" Width="80px" runat="server" Text="Nuevo" ToolTip="Generar un Doc. Ajuste de Inventario desde un Doc. Toma de Inventario." />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" OnClientClick="return( valOnClick_btnGuardar()  );" Width="80px" runat="server" Text="Guardar" ToolTip="Guardar" />
                        </td>
                        <td>
                        
                            <asp:Button ID="btnCancelar"  Width="80px" runat="server" Text="Cancelar" ToolTip="Cancelar" />
                        
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td style="width:100%" align="right">
                        <table>
                <tr>
                    <td class="Texto">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
                        </td>
                    </tr>
                </table>
            
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                EMPRESA - TIENDA - SUB LÍNEA</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight:bold;text-align:right" >
                            Empresa:</td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true" Width="300px">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight:bold;text-align:right"  >
                            Tienda:</td>
                        <td>
                            <asp:DropDownList ID="cboTienda" runat="server" Width="300px" >
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="80px" ToolTip="Buscar" />
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="font-weight:bold;text-align:right">
                            Línea:</td>
                        <td>
                            <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true" Width="100%" >
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight:bold;text-align:right" >
                            Sub Línea:</td>
                        <td>
                            <asp:DropDownList ID="cboSubLinea" runat="server" Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_NroCuenta" runat="server">
                <table>
                    <tr>
                        <td class="Texto"  style="font-weight:bold;text-align:right" >
                        Cuenta Debe Compra:
                         </td>
                        <td>
                            <asp:TextBox ID="txtCuentaDebeCompra" runat="server" Width="130px" Font-Bold="true"></asp:TextBox>
                           </td>
                        <td class="Texto"  style="font-weight:bold;text-align:right" >
                        Cuenta Haber Compra:
                         </td>
                        <td>
                            <asp:TextBox ID="txtCuentaHaberCompra" runat="server" Width="130px" Font-Bold="true"></asp:TextBox>
                           </td>
                           <td class="Texto"  style="font-weight:bold;text-align:right" >
                        Cuenta Costo Compra:
                         </td>
                        <td>
                            <asp:TextBox ID="txtCuentaCostoCompra" runat="server" Width="130px" Font-Bold="true"></asp:TextBox>
                           </td>
                          </tr>  
                       <tr>
                        <td class="Texto"  style="font-weight:bold;text-align:right" >
                        Cuenta Debe Venta:
                         </td>
                        <td>
                            <asp:TextBox ID="txtCuentaDebeVenta" runat="server" Width="130px" Font-Bold="true"></asp:TextBox>
                           </td>
                        <td class="Texto"  style="font-weight:bold;text-align:right" >
                        Cuenta Haber Venta:
                         </td>
                        <td>
                            <asp:TextBox ID="txtCuentaHaberVenta" runat="server" Width="130px" Font-Bold="true"></asp:TextBox>
                           </td>
                           <td class="Texto"  style="font-weight:bold;text-align:right" >
                        Cuenta Costo Venta:
                         </td>
                        <td>
                            <asp:TextBox ID="txtCuentaCostoVenta" runat="server" Width="130px" Font-Bold="true"></asp:TextBox>
                           </td>
                          </tr>  
                </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Grilla" runat="server">
         
         <asp:GridView ID="GV_EmpresaTienda_SubLinea" runat="server" AutoGenerateColumns="False" Width="100%" >
                <Columns>                  
                   <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                        <ItemTemplate>
                            <asp:LinkButton ID="btnEditar" runat="server" OnClick ="btnEditar_Click">Editar</asp:LinkButton>
                            <asp:HiddenField ID ="hddIdLinea" Value='<%# DataBinder.Eval(Container.DataItem,"IdLinea") %>'  runat ="server"  />
                            <asp:HiddenField ID ="hddIdSubLinea" Value='<%# DataBinder.Eval(Container.DataItem,"IdSubLinea") %>'  runat ="server"  />
                            <asp:HiddenField ID ="hddIdEmpresa" Value='<%# DataBinder.Eval(Container.DataItem,"IdEmpresa") %>'  runat ="server"  />
                            <asp:HiddenField ID ="hddIdTienda" Value='<%# DataBinder.Eval(Container.DataItem,"IdTienda") %>'  runat ="server"  />                            
                        </ItemTemplate>
                    </asp:TemplateField>
                  <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                        <ItemTemplate>
                            <asp:LinkButton ID="btnEliminar" ForeColor="Red"  OnClientClick="   return(  valOnClick_btnEliminar() );  " runat="server" OnClick ="btnEliminar_Click" >Eliminar</asp:LinkButton>
                        </ItemTemplate>
                  </asp:TemplateField>
                                       
                  <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                  </asp:BoundField>
                  
                  <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                  </asp:BoundField>
                  
                  <asp:BoundField DataField="Linea" HeaderText="Línea" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                  </asp:BoundField>
                  
                  <asp:BoundField DataField="SubLinea" HeaderText="Sub Línea" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                  </asp:BoundField>
                  
                  
                </Columns>
                
                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                <EditRowStyle CssClass="GrillaEditRow" />
                <FooterStyle CssClass="GrillaFooter" />
                <HeaderStyle CssClass="GrillaHeader" />
                <PagerStyle CssClass="GrillaPager" />
                <RowStyle CssClass="GrillaRow" />
                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>       
                
                </asp:Panel>
         
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
            <asp:HiddenField ID="hddFrmModo" runat="server" Value="" />
            </td>
        </tr>
    </table>
    
    <script language="javascript" type="text/javascript">

        function valOnClick_btnEliminar() {
            return confirm('Desea continuar con la Operación ?');
        }

        function valOnClick_btnGuardar() {

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0 || parseInt(cboEmpresa.value) <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }

            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0 || parseInt(cboTienda.value) <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }

            var cboLinea = document.getElementById('<%=cboLinea.ClientID%>');
            if (isNaN(parseInt(cboLinea.value)) || cboLinea.value.length <= 0 || parseInt(cboLinea.value) <= 0) {
                alert('Debe seleccionar una Línea');
                return false;
            }

            var cboSubLinea = document.getElementById('<%=cboSubLinea.ClientID%>');
            /*if (isNaN(parseInt(cboSubLinea.value)) || cboSubLinea.value.length <= 0 || parseInt(cboSubLinea.value) <= 0) {
                alert('Debe seleccionar una Sub Línea');
                return false;
            }*/
            
            return confirm('Desea continuar con la Operación ?');
        }
    
    </script>
    
</asp:Content>
