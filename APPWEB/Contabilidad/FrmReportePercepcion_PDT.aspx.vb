﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmReportePercepcion_PDT
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then

            inicializarFrm()

        End If
    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboPropietario(Me.cboEmpresa)
                .LLenarCboTienda(Me.cboTienda, True)
                .LlenarCboMonedaBase(Me.cboMoneda, False)
                .LlenarCboTipoDocumento(Me.cboTipoDocumento, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.hddIdArea.Value), True)

                Me.txtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
                Me.txtFechaFin.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub



    Private Sub generarArchivo()
        Try

            Dim address As String = "C:\SIGE\Percepcion_PDT\"
            Dim texto As String = (New Negocio.Contabilidad).ReportePercepcion_PDT(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue), CInt(Me.cboMoneda.SelectedValue), CDate(Me.txtFechaInicio.Text), CDate(Me.txtFechaFin.Text))
            Dim FileName As String = "0697" + (New Negocio.Persona).getRUC(CInt(Me.cboEmpresa.SelectedValue)) + CStr(CDate(Me.txtFechaFin.Text).Year) + (New Util).completeText(CStr(CDate(Me.txtFechaFin.Text).Month), 2, "0", "L") + ".txt"

            address = address + FileName

            Dim sw As New System.IO.StreamWriter(address)
            sw.Write(texto)
            sw.Close()

            objScript.mostrarMsjAlerta(Me, "El Archivo " + FileName + " fue generado con éxito. Ruta del Archivo: " + address)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

    End Sub

    Protected Sub btnGenerarReportePDT_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGenerarReportePDT.Click
        generarArchivo()
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        cargarCboTipoDocumento(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.hddIdArea.Value))
    End Sub
    Private Sub cargarCboTipoDocumento(ByVal IdEmpresa As Integer, ByVal IdArea As Integer)
        Try

            Dim objCbo As New Combo

            objCbo.LlenarCboTipoDocumento(Me.cboTipoDocumento, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.hddIdArea.Value), True)


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
End Class