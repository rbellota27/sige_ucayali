<%@ Page Title="" Language="vb" AutoEventWireup="false"       MasterPageFile="~/Principal.Master"           CodeBehind="FrmReportePercepcion_PDT.aspx.vb" Inherits="APPWEB.FrmReportePercepcion_PDT" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
    <tr class="TituloCelda">
    <td>
    Reporte Contabilidad - Percepción
    </td>
    </tr>
    <tr>
    <td>
    
        <asp:UpdatePanel ID="UpdatePanel_Cab" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
    <table width="100%">
    <tr>
            <td>                                        
                <table>
                    <tr>
                        <td class="Texto">
                            Empresa:</td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true"    >
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Tienda:</td>
                        <td>
                            <asp:DropDownList ID="cboTienda" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Tipo Documento:</td>
                        <td>
                            <asp:DropDownList ID="cboTipoDocumento" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Moneda:</td>
                        <td>
                            <asp:DropDownList ID="cboMoneda" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>                                  
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto">
                            Fecha Inicio:</td>
                        <td>
                        
                            <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" 
                                Font-Bold="True" onblur="return(valFecha(this));" Width="90px"></asp:TextBox>                            
                           <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender" runat="server" 
                                 CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                                 CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                 CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"                                  
                                 TargetControlID="txtFechaInicio"
                               ClearMaskOnLostFocus="false"    
                                  Mask="99/99/9999">
                             </cc1:MaskedEditExtender>
                               <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" 
                                            TargetControlID="txtFechaInicio" Enabled="True" Format="dd/MM/yyyy" >
                                        </cc1:CalendarExtender> 
                        </td>
                        <td class="Texto">
                            Fecha Fin:</td>
                        <td>
                        
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" 
                                Font-Bold="True" onblur="return(valFecha(this));" Width="90px"></asp:TextBox>
                         <cc1:MaskedEditExtender ID="txtFechaFin_MaskedEditExtender" runat="server" 
                                 CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                                 CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                 CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"                                  
                                 TargetControlID="txtFechaFin"
                               ClearMaskOnLostFocus="false"    
                                  Mask="99/99/9999">
                             </cc1:MaskedEditExtender>
                               <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" 
                                            TargetControlID="txtFechaFin" Enabled="True" Format="dd/MM/yyyy" >
                                        </cc1:CalendarExtender>   
                        
                        </td>
                        <td>
                            <asp:Button ID="btnAceptar" OnClientClick="return(  valOnClickMostrarReporte()    );" runat="server" Width="120px" Text="Mostrar Reporte" ToolTip="Mostrar Reporte" />
                        </td>
                        <td>
                        <asp:Button  ID="btnGenerarReportePDT" OnClientClick="return(valOnClickGenerarReporte());" runat="server" Text="Generar Reporte Percepción - PDT" ToolTip="Generar Reporte Percepción - PDT" Width="220px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
    
        </ContentTemplate>
        </asp:UpdatePanel>
    
    
    
    </td>
    </tr>
        
        <tr>
            <td>
            <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 900px"></iframe>
            </td>                
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdArea" runat="server" Value="2" />  <!--  CAJA -->
            </td>
        </tr>
        </table>
        
        <script language="javascript" type="text/javascript">

            function valOnClickGenerarReporte() {
                var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID %>');
                if (isNaN(parseInt(cboEmpresa.value)) || parseInt(cboEmpresa.value) <= 0 || cboEmpresa.value.length <= 0) {
                    alert('Debe seleccionar una Empresa.');
                    return false;
                }
                var cboTienda = document.getElementById('<%=cboTienda.ClientID %>');
                if (isNaN(parseInt(cboTienda.value)) ||  cboTienda.value.length <= 0) {
                    alert('Debe seleccionar una Tienda.');
                    return false;
                }
                var cboMoneda = document.getElementById('<%=cboMoneda.ClientID %>');
                if (isNaN(parseInt(cboMoneda.value)) || parseInt(cboMoneda.value) <= 0 || cboMoneda.value.length <= 0) {
                    alert('Debe seleccionar una Moneda.');
                    return false;
                }
                var cboTipoDocumento = document.getElementById('<%=cboTipoDocumento.ClientID %>');
                if (isNaN(parseInt(cboTipoDocumento.value)) || cboTipoDocumento.value.length <= 0) {
                    alert('Debe seleccionar un Tipo de Documento.');
                    return false;
                }
                
                var txtFechaInicio = document.getElementById('<%=txtFechaInicio.ClientID %>');
                var txtFechaFin = document.getElementById('<%=txtFechaFin.ClientID %>');

                var cad = '';
                cad = cad + 'Empresa   : ' + getCampoxValorCombo(cboEmpresa, cboEmpresa.value) + '\n';
                cad = cad + 'Tienda      : ' + getCampoxValorCombo(cboTienda, cboTienda.value)+'\n';
                cad = cad + 'Tipo Doc.  : ' + getCampoxValorCombo(cboTipoDocumento, cboTipoDocumento.value)+'\n';
                cad = cad + 'Moneda    : ' + getCampoxValorCombo(cboMoneda, cboMoneda.value)+'\n';
                cad = cad + 'F. Inicio    : ' + txtFechaInicio.value+'\n';
                cad = cad + 'F. Fin        : ' + txtFechaFin.value+'\n';
                cad=cad + 'Desea continuar con la Generación del Archivo ?.';
                return confirm(cad);

            }


            function valOnClickMostrarReporte() {

                var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID %>');
                if (isNaN(parseInt(cboEmpresa.value)) || parseInt(cboEmpresa.value) <= 0 || cboEmpresa.value.length <= 0) {
                    alert('Debe seleccionar una Empresa.');
                    return false;
                }
                var cboTienda = document.getElementById('<%=cboTienda.ClientID %>');
                if (isNaN(parseInt(cboTienda.value))  || cboTienda.value.length <= 0) {
                    alert('Debe seleccionar una Tienda.');
                    return false;
                }
                var cboMoneda = document.getElementById('<%=cboMoneda.ClientID %>');
                if (isNaN(parseInt(cboMoneda.value)) || parseInt(cboMoneda.value) <= 0 || cboMoneda.value.length <= 0) {
                    alert('Debe seleccionar una Moneda.');
                    return false;
                }
                var cboTipoDocumento = document.getElementById('<%=cboTipoDocumento.ClientID %>');
                if (isNaN(parseInt(cboTipoDocumento.value)) || cboTipoDocumento.value.length <= 0) {
                    alert('Debe seleccionar un Tipo de Documento.');
                    return false;
                }
              
                
                
                var txtFechaInicio = document.getElementById('<%=txtFechaInicio.ClientID %>');
                var txtFechaFin = document.getElementById('<%=txtFechaFin.ClientID %>');

                frame1.location.href = '../Reportes/Visor.aspx?iReporte=1&IdEmpresa=' + cboEmpresa.value + '&IdTienda=' + cboTienda.value + '&IdTipoDocumento=' + cboTipoDocumento.value + '&IdMonedaDestino=' + cboMoneda.value + '&FechaI=' + txtFechaInicio.value + '&FechaF=' + txtFechaFin.value + '&TipoDocumento=' + getCampoxValorCombo(cboTipoDocumento, cboTipoDocumento.value);
                
                
                return false;
            }
        
        </script>        
</asp:Content>

