﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.Configuration

Partial Public Class FrmPasedeComprasVentas
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
#Region "Cadena Conx"
    'Private conx As New SqlClient.SqlConnection("Data Source=192.168.2.243;Initial Catalog=NTODatos;User ID=sa;pwd=")
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            LimpiarControles()
        End If
    End Sub
    Private Sub LimpiarControles()
        ddlAnio.SelectedIndex = 1
        txtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        txtFechaFin.Text = txtFechaInicio.Text
        GV_PaseVentasCompras.DataSource = Nothing
        GV_PaseVentasCompras.DataBind()
    End Sub
    Protected Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLimpiar.Click
        LimpiarControles()
    End Sub
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click
        Dim FechaInicioPase As String = ""
        Dim FechaFinPase As String = ""
        Dim Anio As String = ""

        FechaInicioPase = (txtFechaInicio.Text)
        FechaFinPase = (txtFechaFin.Text)
        Anio = ddlAnio.SelectedItem.Text
        If (CDate(FechaFinPase) = Date.Today) Then
            objScript.mostrarMsjAlerta(Me, "No se puede tener como fecha fin del pase, el día de hoy.")
        Else
            Dim Verificando As Integer = New Negocio.Documento().VerificandoHistorialPases(FechaInicioPase, FechaFinPase, Anio)
            If (Verificando = 0) Then
                Try
                    EjecutarPaseEnNTODatos(FechaInicioPase, FechaFinPase, Anio)
                    Dim Resultado As Boolean = New Negocio.Documento().GenerarPaseVentasCompras(FechaInicioPase, FechaFinPase, Anio, CInt(Session("IdUsuario")))
                    Dim ListaNoPasados As List(Of Entidades.Documento) = New Negocio.Documento().VerificandoSinPasar(FechaInicioPase, FechaFinPase, Anio)

                    If (Resultado = True And ListaNoPasados Is Nothing) Then
                        objScript.mostrarMsjAlerta(Me, "El pase se generó correctamente.")
                    ElseIf (Resultado = True And ListaNoPasados IsNot Nothing) Then
                        objScript.mostrarMsjAlerta(Me, "El pase se generó correctamente, pero algunos documentos no pasarón correctamente. Verifique líneas abajo y vuelva a generar.")
                        GV_PaseVentasCompras.DataSource = ListaNoPasados
                        GV_PaseVentasCompras.DataBind()
                    Else
                        objScript.mostrarMsjAlerta(Me, "Hubo problemas al generar el pase. Vuelva a intentarlo.")
                    End If
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
            Else
                objScript.mostrarMsjAlerta(Me, "La Fecha Ingresada ya se encuentra con un registro de pase. No procede la operación.")
            End If
        End If

    End Sub
    Private Sub EjecutarPaseEnNTODatos(ByVal FechaInicioPase As String, ByVal FechaFinPase As String, ByVal Anio As String)



        Dim con As New SqlConnection("Data Source=192.168.2.243;Initial Catalog=NTODatos;User ID=sa;pwd=")
        Dim cmd As New SqlCommand()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "spCn_RptRegistroComprasPaseSige2"
        cmd.Parameters.Add("@desde", SqlDbType.VarChar).Value = FechaInicioPase
        cmd.Parameters.Add("@hasta", SqlDbType.VarChar).Value = FechaFinPase
        cmd.Parameters.Add("@Pan_cAnio", SqlDbType.VarChar).Value = Anio
        cmd.CommandTimeout = 0
        cmd.Connection = con
        Try
            con.Open()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            con.Close()
            con.Dispose()

            cmd.Dispose()

        End Try
    End Sub

    Protected Sub btnVerPases_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnVerPases.Click
        Dim url As String = "FrmPasesRealizados.aspx"
        Dim s As String = "window.open('" & url + "', 'popup_window', 'width=980,height=640,left=100,resizable=no,scrollbars=yes');"
        ClientScript.RegisterStartupScript(Me.GetType(), "script", s, True)
    End Sub

    'Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click

    'End Sub
End Class