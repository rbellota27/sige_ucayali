﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmExportContabilidad
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then


            inicializarFrm()

        End If
    End Sub
    Private Sub inicializarFrm()
        Try

            Dim objCombo As New Combo
            objCombo.LlenarCboPropietario(Me.cboEmpresa, False)

            Me.txtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaFin.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptar.Click

        ExportarContabilidad_Ventas()

    End Sub
    Private Sub ExportarContabilidad_Ventas()

        Try

            If ((New Negocio.Contabilidad).ExportarContabilidad_Ventas(CInt(Me.cboEmpresa.SelectedValue), CDate(Me.txtFechaInicio.Text), CDate(Me.txtFechaFin.Text))) Then
                objScript.mostrarMsjAlerta(Me, "El Proceso de Exportación se realizó con Éxito.")
            Else
                Throw New Exception("Problemas en la Exportación.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


End Class