﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports System.Web.Security
Partial Public Class frmInicio
    Inherits System.Web.UI.Page
    Public Shared IdUsuarioGeneral As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        txtLogin.Focus()

    End Sub
    Private Sub LimpiarControles()
        Me.txtLogin.Text = ""
        Me.txtClave.Text = ""
    End Sub
    Private Sub Validar()
        Dim Login, Clave As String
        Dim IdPersona As Integer = Nothing
        Dim Intentos As Integer = Nothing
        Dim IdPerfil As Integer = Nothing
        Dim IdTienda As String = String.Empty
        Login = Me.txtLogin.Text.Trim
        Clave = Me.txtClave.Text.Trim

        IdPersona = (New Negocio.Usuario).ValidarIdentidad(Login, Clave)
        IdPerfil = (New Negocio.Usuario).ValidarIdentidadPerfil(Login, Clave)

        If IdPersona <> 0 Then
            IdTienda = (New Negocio.Util).ValidarTiendaxUsuario(IdPersona)
            FormsAuthentication.RedirectFromLoginPage(Login, True)
            Dim IdCaja As Integer = (New Negocio.Caja).SelectIdCajaxIdPersona(IdPersona)
            IdUsuarioGeneral = IdPersona
            Session.Add("IdPersona", IdPersona)            
            Session.Add("IdUsuario", IdPersona)
            Session.Add("IdCaja", IdCaja)
            Session.Add("IdPerfil", IdPerfil)
            Session.Add("idTiendaUsuario", IdTienda)
            Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {17})
            Session.Add("ConfigurarDatos", CDec(listaParametros(0)))

            Response.Redirect("~/FrmPrincipal_Inicio.aspx")

        Else

            Dim Util As New Util
            If Me.getIntentos = 2 Then
                Util.Mensaje("Han terminado el Número de Intentos Permitidos", Me)
                Util.Salir(Me)
            Else
                Util.Mensaje("Usuario y/o Contraseña incorrectos.", Me)
                Intentos = Me.getIntentos
                Intentos += 1
                Me.setIntentos(Intentos)
            End If
        End If
    End Sub
    Private Function getIntentos() As Integer
        Return CInt(ViewState.Item("Intentos"))
    End Function
    Private Sub setIntentos(ByVal Intentos As Integer)
        ViewState.Add("Intentos", Intentos)
    End Sub
    Protected Sub btnIngresar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnIngresar.Click
        Me.Validar()
    End Sub
End Class