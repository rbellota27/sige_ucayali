﻿Public Class frmCosteoImportacionProyectado
    Inherits System.Web.UI.Page
    Dim cbo As New Combo
    Dim objScript As New ScriptManagerClass
    Private listaDetOC As New List(Of Entidades.ImportacionDetalle)
    Private listaDetGasto As New List(Of Entidades.DocImportacion)
    Private ListaProveedor As List(Of Entidades.PersonaView)

    Private lista_DetalleGasto As List(Of Entidades.DocImportacion)
    Private obj_DetalleGasta As Entidades.DocImportacion
    Private Const IdTipoDocumento_OrdenCompra As Integer = 16
    Private Const IdTipoDocumento_GuiaRecepcion As Integer = 25


    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptmanager As ScriptManager = scriptmanager.GetCurrent(Me.Page)
        scriptmanager.RegisterPostBackControl(Me.btnExportarExcel)
        If Not Page.IsPostBack Then

            ConfigurarDatos()
            ValidarPermisos()

            cbo.LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
            cbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            cbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            cbo.LlenarCboMoneda(Me.cboMoneda, True)
            If cboMoneda.Items.FindByValue("2") IsNot Nothing Then
                cboMoneda.SelectedValue = "2"
            End If
            cboMoneda.Enabled = False

            'cbo.LlenarCboProveedor(Me.cboProveedorCapaOC, True)
            cbo.LlenarCboProveedorImportados(Me.cboProveedorCapaOC, True)
            GenerarCodigoDocumento()
            cbo.LlenarCboTipoGasto(Me.cboTipoGastoCapaG, True)

            setListaDetalleOC(Me.listaDetOC)
            Me.dgvDetalleOC.DataSource = Me.listaDetOC
            Me.dgvDetalleOC.DataBind()

            setListaDetalleGasto(Me.listaDetGasto)
            Me.dgvDetalleGasto.DataSource = Me.listaDetOC
            Me.dgvDetalleGasto.DataBind()
            ActivarBotones(True, True, False, False, False, False, False)
            HabilitarPaneles(False, False, False)
            LimpiarControles()

            cbo.LlenarCboProveedorImportados(Me.cboProveedorBusImp, True)
        End If
    End Sub
    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / FECHA VCTO / NRO DIAS VIGENCIA / REGISTRAR CLIENTE / EDITAR CLIENTE
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {125, 126, 127, 128, 129, 183})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGrabar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGrabar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* BUSCAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscar.Enabled = True

        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscar.Enabled = False

        End If

        If listaPermisos(4) > 0 Then  '********* IMPRIMIR
            Me.btnImprimir.Enabled = True
            Me.btnImprimir.Enabled = True

        Else
            Me.btnImprimir.Enabled = False
            Me.btnImprimir.Enabled = False

        End If
        If listaPermisos(5) > 0 Then  '********* ACTUALIZAR PRECIO
            Me.chkUpdatePrecioI.Enabled = False
        Else
            Me.chkUpdatePrecioI.Enabled = False
        End If
    End Sub

    Private Sub GenerarCodigoDocumento()
        Try
            If (IsNumeric(Me.cboSerie.SelectedValue)) Then
                Me.txtNroImportacion.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
            Else
                Me.txtNroImportacion.Text = ""
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        cbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
        cbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
        GenerarCodigoDocumento()
    End Sub
    Protected Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTienda.SelectedIndexChanged
        cbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
        GenerarCodigoDocumento()
    End Sub
    Protected Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboSerie.SelectedIndexChanged
        GenerarCodigoDocumento()
    End Sub
    Public Sub ActivarBotones(ByVal btnNuevo As Boolean, ByVal btnBuscar As Boolean, ByVal btnGrabar As Boolean, _
                              ByVal btnCancelar As Boolean, ByVal btnEditar As Boolean, ByVal btnImprimir As Boolean, ByVal btnAnulado As Boolean)
        Me.btnNuevo.Visible = btnNuevo
        Me.btnBuscar.Visible = btnBuscar
        Me.btnGrabar.Visible = btnGrabar
        Me.btnCancelar.Visible = btnCancelar
        Me.btnEditar.Visible = btnEditar
        'Me.btnImprimir.Visible = btnImprimir
        Me.btnAnular.Visible = btnAnulado
    End Sub
    Public Sub HabilitarPaneles(ByVal FlagPanelCab As Boolean, ByVal PanelBusqueda As Boolean, ByVal PnlCabEnabled As Boolean)
        Me.PanelCab.Visible = FlagPanelCab
        Me.PanelBuscar.Visible = PanelBusqueda
        Me.PanelCab.Enabled = PnlCabEnabled
    End Sub
    Public Sub LimpiarControles()
        Me.txtRazonSocial.Text = ""
        Me.txtRuc.Text = ""
        Me.txtDireccion.Text = ""
        Me.txtNroOC.Text = ""
        Me.dgvDetalleOC.DataBind()
        Me.txttotalocOC.Text = ""
        Me.txttotalgastoOC.Text = ""
        Me.hddTotalPesoOC.Value = "0"
        Me.hddFechaEmisionOC.Value = ""
        Me.txtTotalContenedores.Text = ""
        Me.txtFchSalidad.Text = ""
        Me.txtFchLlegada.Text = ""
        Me.txtFchIngMilla.Text = ""
        Me.dgvDetalleGasto.DataBind()
        Me.txttotalpesoG.Text = ""
        Me.txttotalmonedaG.Text = ""
        Me.txtTotalGastoDetalleG.Text = ""
        Me.txtPesoTotal.Text = ""
        Me.txtTotalOC.Text = ""
        Me.txtTotalGastos.Text = ""
        Me.txtTotalImportacion.Text = ""
        Me.hddIdDocumento.Value = "0"
        Me.hddindex.Value = "0"
        Me.hddRelacDoc2.Value = "0"
        Me.dgvCapaOC.DataBind()
        Me.dgvGastoCapa.DataBind()
        'Me.TxtNroImportacionBuscar.Text = ""
        cboWorFor.SelectedValue = "0"
        hddIdProveedor.Value = ""
        Me.dgvListImport.DataBind()
        cboMoneda.SelectedIndex = 2
        chkUpdatePrecioI.Checked = False
        txtCodigoBusImp.Text = ""
        txtSerieBusImp.Text = ""
        cboProveedorBusImp.SelectedValue = "0"
        TbTipoCambioOC.Text = ""
        Session("ExistNroImp") = "False"
    End Sub
#Region "Busqueda Capa OC"
    Private Sub btnBuscarOCcapa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarOCcapa.Click
        Dim NroSerie, NroCorrelativo As Integer
        If Me.txtNroSeriecapaOC.Text = "" Then : NroSerie = 0 : Else : NroSerie = CInt(txtNroSeriecapaOC.Text) : End If
        If Me.txtNroCorrelativocapaOC.Text = "" Then : NroCorrelativo = 0 : Else : NroCorrelativo = CInt(txtNroCorrelativocapaOC.Text) : End If

        BuscarOC(dgvCapaOC, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(NroSerie), CInt(NroCorrelativo), CInt(Me.cboProveedorCapaOC.SelectedValue), 0)
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaBuscarOC();", True)
    End Sub
    Private Sub BuscarOC(ByVal dgv As GridView, ByVal idempresa As Integer, ByVal idtienda As Integer, _
                                ByVal NroSerie As Integer, ByVal NroCorrelativo As Integer, ByVal idproveedor As Integer, _
                                ByVal tipoMov As Integer)
        If Me.txtNroSeriecapaOC.Text = "" Then : NroSerie = 0 : Else : NroSerie = CInt(txtNroSeriecapaOC.Text) : End If
        If Me.txtNroCorrelativocapaOC.Text = "" Then : NroCorrelativo = 0 : Else : NroCorrelativo = CInt(txtNroCorrelativocapaOC.Text) : End If
        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(txtPageIndex_OC.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(txtPageIndex_OC.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(txtPageIndexGO_OC.Text) - 1)
        End Select
        Dim ngcImportacion As New Negocio.DocImportacion
        Dim lista As List(Of Entidades.DocImportacion) = ngcImportacion.OrdenCompraSelectNroOCxIdProveedor(CInt(idempresa), CInt(idtienda), CInt(NroSerie), CInt(NroCorrelativo), CInt(idproveedor), index, dgv.PageSize)
        If lista.Count > 0 Then
            dgv.DataSource = lista
            dgv.DataBind()
            txtPageIndex_OC.Text = CStr(index + 1)
        Else
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, "No se encontraron registros")
        End If
    End Sub
    Private Sub btnAnterior_OC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_OC.Click
        Try
            Dim CAPAOCSERIE, CAPAOCCORRE As Integer
            If txtNroSeriecapaOC.Text = "" Then
                CAPAOCSERIE = 0
            Else
                CAPAOCSERIE = CInt(txtNroSeriecapaOC.Text)
            End If
            If txtNroCorrelativocapaOC.Text = "" Then
                CAPAOCCORRE = 0
            Else
                CAPAOCCORRE = CInt(txtNroCorrelativocapaOC.Text)
            End If
            BuscarOC(dgvCapaOC, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(CAPAOCSERIE), CInt(CAPAOCCORRE), CInt(Me.cboProveedorCapaOC.SelectedValue), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_OC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_OC.Click
        Try
            Dim CAPAOCSERIE, CAPAOCCORRE As Integer
            If txtNroSeriecapaOC.Text = "" Then
                CAPAOCSERIE = 0
            Else
                CAPAOCSERIE = CInt(txtNroSeriecapaOC.Text)
            End If
            If txtNroCorrelativocapaOC.Text = "" Then
                CAPAOCCORRE = 0
            Else
                CAPAOCCORRE = CInt(txtNroCorrelativocapaOC.Text)
            End If
            BuscarOC(dgvCapaOC, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(CAPAOCSERIE), CInt(CAPAOCCORRE), CInt(Me.cboProveedorCapaOC.SelectedValue), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnPosterior_OC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_OC.Click
        Try
            Dim CAPAOCSERIE, CAPAOCCORRE As Integer
            If txtNroSeriecapaOC.Text = "" Then
                CAPAOCSERIE = 0
            Else
                CAPAOCSERIE = CInt(txtNroSeriecapaOC.Text)
            End If
            If txtNroCorrelativocapaOC.Text = "" Then
                CAPAOCCORRE = 0
            Else
                CAPAOCCORRE = CInt(txtNroCorrelativocapaOC.Text)
            End If
            BuscarOC(dgvCapaOC, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CAPAOCSERIE, CAPAOCCORRE, CInt(Me.cboProveedorCapaOC.SelectedValue), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function getListaDetalleOC() As List(Of Entidades.ImportacionDetalle)
        Return CType(Session.Item("listaDetalleOC"), List(Of Entidades.ImportacionDetalle))
    End Function
    Private Sub setListaDetalleOC(ByVal lista As List(Of Entidades.ImportacionDetalle))
        Session.Remove("listaDetalleOC")
        Session.Add("listaDetalleOC", lista)
    End Sub
    Private Sub addGrillaDetalleOC()
        Try
            '*********  agrego el detalle OC
            Me.listaDetOC = Me.getListaDetalleOC
            For i As Integer = 0 To dgvCapaOC.Rows.Count - 1


                Dim chbAdd As CheckBox = CType(Me.dgvCapaOC.Rows(i).Cells(0).FindControl("chkSelectOC"), CheckBox)
                Dim hddIdTipoOperacion As HiddenField = CType(Me.dgvCapaOC.Rows(i).FindControl("hddIdTipoOperacion"), HiddenField)
                Dim hddworthfob As HiddenField = CType(Me.dgvCapaOC.Rows(i).FindControl("hddworthfob"), HiddenField)
                If chbAdd.Checked Then

                    ' AQUI CAMBIE EL ORDEN ESTABA ARRIBA

                    If CStr(Session("ExistNroImp")) = "False" Then
                        If RTrim(CStr(HttpUtility.HtmlDecode(dgvCapaOC.Rows(i).Cells(8).Text))) <> "." Then
                            objScript.mostrarMsjAlerta(Me, "La O/C ya tiene un nro importación.")
                            Exit Sub
                            Exit For
                        End If
                    End If



                    Dim ngcDetalleOC As New Negocio.ImportacionDetalle
                    Me.txtRazonSocial.Text = HttpUtility.HtmlDecode(dgvCapaOC.Rows(i).Cells(6).Text).Trim
                    Me.hddFechaEmisionOC.Value = HttpUtility.HtmlDecode(dgvCapaOC.Rows(i).Cells(5).Text)
                    ViewState.Add("FechaEmision", HttpUtility.HtmlDecode(dgvCapaOC.Rows(i).Cells(5).Text))
                    hddRelacDoc2.Value = CStr(dgvCapaOC.Rows(i).Cells(1).Text)
                    hddIdTipoOperacionv2.Value = hddIdTipoOperacion.Value
                    Me.cboWorFor.SelectedValue = hddworthfob.Value
                    TbTipoCambioOC.Text = CStr(Math.Round(CDec(tbTipoCambio.Text), 3))

                    Dim ListaDetalleOrdenC As List(Of Entidades.ImportacionDetalle) = ngcDetalleOC.OrdenCompraSelectIdOC(CInt(dgvCapaOC.Rows(i).Cells(1).Text), CDec(tbTipoCambio.Text))

                    For x As Integer = 0 To ListaDetalleOrdenC.Count - 1
                        Dim obj As New Entidades.ImportacionDetalle
                        Me.txtRuc.Text = ListaDetalleOrdenC.Item(x).Ruc
                        Me.txtDireccion.Text = ListaDetalleOrdenC.Item(x).Direccion
                        hddIdProveedor.Value = CStr(ListaDetalleOrdenC.Item(x).IdProveedor)
                        Me.txtNroOC.Text = ListaDetalleOrdenC.Item(x).NroOrdenCompra()

                        obj.IdProducto = ListaDetalleOrdenC.Item(x).IdProducto
                        obj.CodigoProducto = ListaDetalleOrdenC.Item(x).CodigoProducto
                        obj.NomProducto = ListaDetalleOrdenC.Item(x).NomProducto
                        obj.IdUM = ListaDetalleOrdenC.Item(x).IdUM
                        obj.NomUM = ListaDetalleOrdenC.Item(x).NomUM
                        obj.Cantidad = ListaDetalleOrdenC.Item(x).Cantidad
                        obj.Peso = ListaDetalleOrdenC.Item(x).Peso
                        obj.AbvMagnitud = ListaDetalleOrdenC.Item(x).AbvMagnitud
                        obj.Costo = ListaDetalleOrdenC.Item(x).Costo
                        obj.PrecioUnitario = ListaDetalleOrdenC.Item(x).PrecioUnitario
                        obj.IdDetalleAfecto = ListaDetalleOrdenC.Item(x).IdDetalleDocumento
                        Me.listaDetOC.Add(obj)
                    Next
                End If
            Next
            dgvDetalleOC.DataSource = Me.listaDetOC
            dgvDetalleOC.DataBind()
            Me.setListaDetalleOC(Me.listaDetOC)

            calcularTotalPesoCosto()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnAñadirOCcapa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAñadirOCcapa.Click
        setListaDetalleOC(Me.listaDetOC)
        addGrillaDetalleOC()
    End Sub

    Protected Sub lkbQuitar_Click(ByVal sender As Object, ByVal e As EventArgs)
        quitarDetalleDocumento(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        calcularTotalPesoCosto()
    End Sub


    Private Sub quitarDetalleDocumento(ByVal Index As Integer)
        ' ERROR REVISAR FUNCION AL ELIMINAR PROD DE LA ORDEN COMPRA
        Try
            Me.listaDetOC = Me.getListaDetalleOC
            Me.listaDetOC.RemoveAt(Index)
            Me.setListaDetalleOC(Me.listaDetOC)
            dgvDetalleOC.DataSource = Me.listaDetOC
            dgvDetalleOC.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Sub calcularTotalPesoCosto()
        hddTotalPesoOC.Value = "0"
        txttotalocOC.Text = "0"
        For x As Integer = 0 To Me.dgvDetalleOC.Rows.Count - 1
            'peso
            Dim sumapeso As Double
            Dim txtpesoOC As TextBox = CType(Me.dgvDetalleOC.Rows(x).Cells(5).FindControl("txtdgvPesoOC"), TextBox)
            sumapeso += CDec(txtpesoOC.Text)
            hddTotalPesoOC.Value = CStr(sumapeso)
            'prorrateo(costo o OC)
            Dim suma As Double
            Dim txtcostoOC As TextBox = CType(Me.dgvDetalleOC.Rows(x).Cells(7).FindControl("txtdgvCostoOC"), TextBox)
            suma += CDec(txtcostoOC.Text)
            txttotalocOC.Text = CStr(suma)
        Next
    End Sub
#End Region
#Region "Busqueda Capa Gasto"
    Private Sub btnBuscarGastoCapaG_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarGastoCapaG.Click
        BuscarGasto(Me.dgvGastoCapa, CInt(Me.cboTipoGastoCapaG.SelectedValue), CStr(Me.txtDescripcionCapaG.Text), 0)
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaBuscarGasto();", True)
    End Sub
    Private Sub BuscarGasto(ByVal dgv As GridView, ByVal idtipogasto As Integer, ByVal nomconcepto As String, _
                            ByVal tipoMov As Integer)
        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(txtPageIndex_Gasto.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(txtPageIndex_Gasto.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(txtPageIndexGO_Gasto.Text) - 1)
        End Select
        Dim ngcConceptoTipoDoc As New Negocio.Concepto_TipoDocumento
        Dim lista As List(Of Entidades.Concepto_TipoDocumento) = ngcConceptoTipoDoc.SelectConceptoTipoDocxIdTipoGastoxNomConcepto(CInt(idtipogasto), CStr(nomconcepto), index, dgv.PageSize)
        If lista.Count <= 0 Then
            dgv.DataBind()
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, "No se encontraron registros")
        Else
            dgv.DataSource = lista
            dgv.DataBind()
            txtPageIndex_Gasto.Text = CStr(index + 1)
        End If
    End Sub
    Private Sub btnAnterior_Gasto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Gasto.Click
        Try
            BuscarGasto(Me.dgvGastoCapa, CInt(Me.cboTipoGastoCapaG.SelectedValue), CStr(Me.txtDescripcionCapaG.Text), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Gasto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Gasto.Click
        Try
            BuscarGasto(Me.dgvGastoCapa, CInt(Me.cboTipoGastoCapaG.SelectedValue), CStr(Me.txtDescripcionCapaG.Text), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnPosterior_Gasto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Gasto.Click
        Try
            BuscarGasto(Me.dgvGastoCapa, CInt(Me.cboTipoGastoCapaG.SelectedValue), CStr(Me.txtDescripcionCapaG.Text), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub SelectAllTipoDocumento(ByVal cbo As DropDownList, ByVal addElement As Boolean)
        Dim lista As List(Of Entidades.DocImportacion) = (New Negocio.DocImportacion).SelectAllTipoDocumento()
        If addElement Then
            Dim objImpTipoDoc As New Entidades.DocImportacion(0, "-----")
            lista.Insert(0, objImpTipoDoc)
        End If
        cbo.DataSource = lista
        cbo.DataTextField = "NomTipoDoc"
        cbo.DataValueField = "IdTipoDocumento"
        cbo.DataBind()
    End Sub
    Private Function getListaDetalleGasto() As List(Of Entidades.DocImportacion)
        Return CType(Session.Item("listaDetalleGasto"), List(Of Entidades.DocImportacion))
    End Function
    Private Sub setListaDetalleGasto(ByVal lista As List(Of Entidades.DocImportacion))
        Session.Remove("listaDetalleGasto")
        Session.Add("listaDetalleGasto", lista)
    End Sub

    Private Sub addGrillaDetalleGasto()
        Try
            '*********  agrego el detalle Gasto
            Me.listaDetGasto = Me.getListaDetalleGasto

            For i As Integer = 0 To dgvGastoCapa.Rows.Count - 1
                Dim chbAdd As CheckBox = CType(Me.dgvGastoCapa.Rows(i).Cells(0).FindControl("chkSelectGasto"), CheckBox)
                If chbAdd.Checked Then

                    If getListaDetalleGastoGrv(listaDetGasto, CInt(dgvGastoCapa.Rows(i).Cells(1).Text)) Then
                        Dim ngcDetalleGasto As New Negocio.DocImportacion
                        Dim ListaDetalleGasto As List(Of Entidades.DocImportacion) = ngcDetalleGasto.DetalleGastoSelectxIdConcepto(CInt(dgvGastoCapa.Rows(i).Cells(1).Text))
                        For x As Integer = 0 To ListaDetalleGasto.Count - 1
                            Dim obj As New Entidades.DocImportacion
                            obj.IdConcepto = ListaDetalleGasto.Item(x).IdConcepto
                            obj.NomConcepto = ListaDetalleGasto.Item(x).NomConcepto
                            obj.IdMoneda = ListaDetalleGasto.Item(x).IdMoneda
                            obj.MonSimbolo = ListaDetalleGasto.Item(x).MonSimbolo
                            obj.NomCalculo = ListaDetalleGasto.Item(x).NomCalculo
                            obj.SubTotalGasto = ListaDetalleGasto.Item(x).SubTotalGasto
                            Me.listaDetGasto.Add(obj)
                        Next
                    End If

                End If
            Next
            dgvDetalleGasto.DataSource = Me.listaDetGasto
            dgvDetalleGasto.DataBind()
            CalcularTotalGasto()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub quitarGastoGrllaDetalle()
        Try
            Me.listaDetGasto = Me.getListaDetalleGasto
            Me.listaDetGasto.RemoveAt(dgvDetalleGasto.SelectedIndex)
            Me.setListaDetalleGasto(Me.listaDetGasto)
            dgvDetalleGasto.DataSource = Me.listaDetGasto
            dgvDetalleGasto.DataBind()
            For x2 As Integer = 0 To Me.dgvDetalleGasto.Rows.Count - 1
                Dim cboTipDoc As DropDownList = CType(Me.dgvDetalleGasto.Rows(x2).Cells(4).FindControl("cboTipDocDetalleGasto"), DropDownList)
                SelectAllTipoDocumento(cboTipDoc, True)
            Next
        Catch ex As Exception
        Finally
        End Try
    End Sub

    Sub CalcularTotalGasto()
        txtTotalGastoDetalleG.Text = "0"
        For x2 As Integer = 0 To Me.dgvDetalleGasto.Rows.Count - 1
            Dim suma As Double
            Dim txtSubTotalDG As TextBox = CType(Me.dgvDetalleGasto.Rows(x2).Cells(7).FindControl("txtSubTotalDetalleGasto"), TextBox)
            suma += CDec(txtSubTotalDG.Text)
            txtTotalGastoDetalleG.Text = CStr(suma)
        Next
    End Sub






#End Region

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        hddModo.Value = "1"
        cbo.LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
        cbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
        cbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
        GenerarCodigoDocumento()

        cbo.LlenarCboMoneda(Me.cboMoneda, True)
        setListaDetalleGasto(listaDetGasto)
        cbo.LlenarCboTipoPrecioImportacion(Me.cboWorFor, True)
        HabilitarPaneles(True, False, True)
        ActivarBotones(False, False, True, True, False, False, False)
        LimpiarControles()
        'Me.cboMoneda.Enabled = True
        hddIndicador.Value = CStr(0)
    End Sub
    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        setListaDetalleGasto(listaDetGasto)
        ActivarBotones(False, False, False, True, False, False, False)
        HabilitarPaneles(False, True, False)
        LimpiarControles()
        hddIndicador.Value = CStr(1)
    End Sub
    Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGrabar.Click
        Try
            Grabar()
            ActivarBotones(True, True, False, False, False, True, False)
            HabilitarPaneles(False, True, False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Sub Grabar()
        Try
            Dim ngcImportacion As New Negocio.DocImportacion
            Dim objImport As New Entidades.DocImportacion
            With objImport
                .IdDocumento = CInt(Me.hddIdDocumento.Value)
                .doc_Codigo = Me.txtNroImportacion.Text
                .doc_Serie = Me.cboSerie.SelectedItem.Text
                .Serie = CInt(Me.cboSerie.SelectedValue)
                .IdTienda = CInt(Me.cboTienda.SelectedValue)
                .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                '.FechaEmision = CDate(Me.hddFechaEmisionOC.Value)
                .FechaEmision = CDate(IIf(CStr(ViewState("FechaEmision")) = "", Nothing, CStr(ViewState("FechaEmision"))))
                .doc_FechaSalida = CDate(IIf(txtFchSalidad.Text = "", Nothing, txtFchSalidad.Text))
                .doc_FechaLLegada = CDate(IIf(Me.txtFchLlegada.Text = "", Nothing, Me.txtFchLlegada.Text))
                .doc_FechaIngMilla = CDate(IIf(Me.txtFchIngMilla.Text = "", Nothing, Me.txtFchIngMilla.Text))
                .ImpTotal_Imp = CDec(hddTotalImportacion.Value)
                .ImpTotalOC = CDec(Me.txttotalocOC.Text)
                .ImpPesoTotal = CDec(Me.hddTotalPeso.Value)
                .IdUsuario = CInt(Session("IdUsuario"))
                .IdMoneda = CInt(cboMoneda.SelectedValue)
                .IdTipoDocumento = CInt(hddIdTipoDocumento.Value)
                .IdTipoOperacion = CInt(hddIdTipoOperacionv2.Value)
                .IdProveedor = CInt(hddIdProveedor.Value)
            End With

            Dim ldetalleimportacion As New List(Of Entidades.ImportacionDetalle)
            For i As Integer = 0 To Me.dgvDetalleOC.Rows.Count - 1
                Dim obj As New Entidades.ImportacionDetalle
                Dim hddidum As HiddenField = CType(Me.dgvDetalleOC.Rows(i).Cells(3).FindControl("hddIdUM"), HiddenField)
                Dim txtdgvCantOC As TextBox = CType(Me.dgvDetalleOC.Rows(i).Cells(4).FindControl("txtdgvCantOC"), TextBox)
                Dim txtdgvPUnitOC As TextBox = CType(Me.dgvDetalleOC.Rows(i).Cells(6).FindControl("txtdgvPUnitOC"), TextBox)
                Dim txtdgvCostoOC As TextBox = CType(Me.dgvDetalleOC.Rows(i).Cells(7).FindControl("txtdgvCostoOC"), TextBox)
                Dim txtdgvPesoOC As TextBox = CType(Me.dgvDetalleOC.Rows(i).Cells(5).FindControl("txtdgvPesoOC"), TextBox)
                ' TEXTBOXT C.Uni.Imp
                'Dim hddCUniImporte As HiddenField = CType(Me.dgvDetalleOC.Rows(i).Cells(7).FindControl("hddCUniImporte"), HiddenField)

                Dim txtArancel As TextBox = CType(Me.dgvDetalleOC.Rows(i).FindControl("txtArancel"), TextBox)
                Dim hddCUniImporte As TextBox = CType(Me.dgvDetalleOC.Rows(i).FindControl("txtdgvCUniImp"), TextBox)


                Dim hddIdDetalleDocumento As HiddenField = CType(Me.dgvDetalleOC.Rows(i).Cells(3).FindControl("hddIdDetalleDocumento"), HiddenField)
                Dim hddIdDetalleAfecto As HiddenField = CType(Me.dgvDetalleOC.Rows(i).Cells(3).FindControl("hddIdDetalleAfecto"), HiddenField)
                With obj
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
                    .IdDetalleDocumento = CInt(hddIdDetalleDocumento.Value)
                    .IdProducto = CInt(CType(dgvDetalleOC.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                    .IdUM = CInt(hddidum.Value)
                    .Cantidad = CDec(txtdgvCantOC.Text)
                    .PrecioUnitario = CDec(txtdgvPUnitOC.Text)
                    .Costo = CDec(txtdgvCostoOC.Text)
                    .Peso = CDec(txtdgvPesoOC.Text)
                    .dc_Importe = CDec(hddCUniImporte.Text)
                    .Arancel = CDec(txtArancel.Text)
                    .IdDetalleAfecto = CInt(hddIdDetalleAfecto.Value)
                End With
                ldetalleimportacion.Add(obj)
            Next

            Dim objAnexoImportacion As New Entidades.Anexo_Documento
            With objAnexoImportacion
                .IdDocumento = CInt(Me.hddIdDocumento.Value)
                .doc_Importacion = True
                .IdTPImp = CInt(Me.cboWorFor.SelectedValue)
                .NroContenedores = CStr(txtTotalContenedores.Text)
                .MontoxSustentar = CDec(TbTipoCambioOC.Text) 'Aqui esta el tipo de cambio del dia.
            End With

            Dim ldetalleconcepto As New List(Of Entidades.DetalleConcepto)
            For x As Integer = 0 To Me.dgvDetalleGasto.Rows.Count - 1
                Dim obj As New Entidades.DetalleConcepto
                Dim hdd_idproveedor As HiddenField = CType(Me.dgvDetalleGasto.Rows(x).Cells(3).FindControl("hdd_idproveedor"), HiddenField)
                Dim hdd_idmoneda As HiddenField = CType(Me.dgvDetalleGasto.Rows(x).Cells(6).FindControl("hdd_idmoneda"), HiddenField)
                Dim cboTipDocDetalleGasto As DropDownList = CType(Me.dgvDetalleGasto.Rows(x).Cells(4).FindControl("cboTipDocDetalleGasto"), DropDownList)
                Dim txtNroDocDetalleGasto As TextBox = CType(Me.dgvDetalleGasto.Rows(x).Cells(3).FindControl("txtNroDocDetalleGasto"), TextBox)
                Dim txtSubTotalDetalleGasto As TextBox = CType(Me.dgvDetalleGasto.Rows(x).Cells(8).FindControl("txtSubTotalDetalleGasto"), TextBox)
                Dim hddIdDetalleConcepto As HiddenField = CType(Me.dgvDetalleGasto.Rows(x).Cells(3).FindControl("hddIdDetalleConcepto"), HiddenField)
                With obj
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
                    .IdDetalleConcepto = CInt(hddIdDetalleConcepto.Value)
                    .IdTipoDocumentoRef = CInt(cboTipDocDetalleGasto.SelectedValue)
                    .NroDocumento = txtNroDocDetalleGasto.Text
                    .IdMoneda = CInt(hdd_idmoneda.Value)
                    .Monto = CDec(txtSubTotalDetalleGasto.Text)
                    .IdConcepto = CInt(dgvDetalleGasto.Rows(x).Cells(1).Text)
                    .IdProveedor = CInt(hdd_idproveedor.Value)
                End With
                ldetalleconcepto.Add(obj)
            Next

            Dim objRelacDoc As New Entidades.RelacionDocumento
            With objRelacDoc
                .IdDocumento1 = CInt(Me.hddIdDocumento.Value)
                .IdDocumento2 = CInt(Me.hddRelacDoc2.Value)
            End With

            Select Case hddModo.Value
                Case "1"
                    If ngcImportacion.InsertaConfiguracionImportacion(objImport, CBool(Me.chkUpdatePrecioI.Checked), ldetalleimportacion, objAnexoImportacion, ldetalleconcepto, objRelacDoc) Then

                        Dim Contador As Integer = ngcImportacion.SelectCostosSinPasarxIdDocumento(CInt(Me.hddRelacDoc2.Value))
                        If (Contador = 0) Then
                            objScript.mostrarMsjAlerta(Me, "La importación se registró con éxito.")
                        Else
                            objScript.mostrarMsjAlerta(Me, "La importación se registró con éxito, pero hubieron algunos productos que no actualizaron su costo. Favor de editar y volver a calcular los costos.")
                        End If

                    Else
                        objScript.mostrarMsjAlerta(Me, "Problemas en la Operación.")
                    End If
                Case "2"
                    If ngcImportacion.ActualizaConfiguracionImportacion(CInt(Me.hddIdDocumento.Value), CBool(Me.chkUpdatePrecioI.Checked), objImport, ldetalleimportacion, objAnexoImportacion, ldetalleconcepto, objRelacDoc) Then

                        Dim Contador As Integer = ngcImportacion.SelectCostosSinPasarxIdDocumento(CInt(Me.hddRelacDoc2.Value))
                        If (Contador = 0) Then
                            objScript.mostrarMsjAlerta(Me, "La importación se actualizó con éxito.")
                        Else
                            objScript.mostrarMsjAlerta(Me, "La importación se actualizó con éxito, pero hubieron algunos productos que no actualizaron su costo. Favor de editar y volver a calcular los costos.")
                        End If
                    Else
                        objScript.mostrarMsjAlerta(Me, "Problemas en la Operación.")
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelar.Click
        setListaDetalleGasto(listaDetGasto)
        ActivarBotones(True, True, False, False, False, False, False)
        HabilitarPaneles(False, False, False)
        LimpiarControles()
    End Sub
    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditar.Click
        Try
            Dim ListaGuiaRecepcion As List(Of Entidades.Documento) = (New Negocio.RelacionDocumento).SelectxIdDocumentoxIdTipoDocumento(CInt(Me.hddRelacDoc2.Value), IdTipoDocumento_GuiaRecepcion)

            If Not ListaGuiaRecepcion Is Nothing Then : If ListaGuiaRecepcion.Count > 0 Then
                    Dim mensaje As String = ""
                    For i As Integer = 0 To ListaGuiaRecepcion.Count - 1

                        mensaje = ListaGuiaRecepcion(i).NomTipoDocumento + " " + ListaGuiaRecepcion(i).Codigo + ".\n "

                    Next
                    Throw New Exception("LA ORDEN DE COMPRA RELACIONADA A ESTE COSTEO DE IMPORTACION.\n ESTA RELACIONADA A LOS SIGUIENTES DOCUMENTOS:\n " + mensaje)
                    Return

                End If : End If

            hddModo.Value = "2"
            cbo.LlenarCboTipoPrecioImportacion(Me.cboWorFor, True)
            ActivarBotones(False, False, True, True, False, True, True)
            HabilitarPaneles(True, False, True)
            EditarImportacion(CInt(hddIdDocumento.Value))
            Me.cboMoneda.Enabled = False
            hddIndicador.Value = CStr(1)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub EditarImportacion(ByVal IdDocumento As Integer)
        Try
            ' CABECERA IMPORTACION
            Dim ngcImportacion As New Negocio.DocImportacion
            Dim objImportacion As New Entidades.DocImportacion
            'objImportacion = ngcImportacion.ImportacionSelectId(NroImportacion)
            objImportacion = ngcImportacion.ImportacionSelectId(IdDocumento)

            Me.hddIdDocumento.Value = CStr(objImportacion.IdDocumento)
            cbo.LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
            Me.cboEmpresa.SelectedValue = CStr(objImportacion.IdEmpresa)

            cbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(objImportacion.IdEmpresa), CInt(Session("IdUsuario")), False)
            Me.cboTienda.SelectedValue = CStr(objImportacion.IdTienda)

            cbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(objImportacion.IdEmpresa), CInt(objImportacion.IdTienda), CInt(Me.hddIdTipoDocumento.Value))
            Me.cboSerie.SelectedValue = CStr(objImportacion.Serie)
            txtNroImportacion.Text = CStr(objImportacion.doc_Codigo)

            Me.hddIdProveedor.Value = CStr(objImportacion.IdProveedor)
            Me.txtRazonSocial.Text = CStr(objImportacion.RazonSocial)
            Me.txtRuc.Text = CStr(objImportacion.Ruc)
            Me.txtDireccion.Text = CStr(objImportacion.Direccion)
            Me.hddIdTipoDocumento.Value = CStr(objImportacion.IdTipoDocumento)
            '.doc_FechaLLegada = CDate(IIf(Me.txtFchLlegada.Text = "", Nothing, Me.txtFchLlegada.Text))
            Me.hddFechaEmisionOC.Value = CStr(CDate(IIf(objImportacion.FechaEmision = Nothing, Nothing, objImportacion.FechaEmision)))
            ViewState.Add("FechaEmision", CStr(CDate(IIf(objImportacion.FechaEmision = Nothing, Nothing, objImportacion.FechaEmision))))

            Me.txtFchLlegada.Text = CStr(IIf(objImportacion.doc_FechaLLegada = Nothing, Nothing, objImportacion.doc_FechaLLegada))
            Me.txtFchSalidad.Text = CStr(IIf(objImportacion.doc_FechaSalida = Nothing, Nothing, objImportacion.doc_FechaSalida))
            Me.txtFchIngMilla.Text = CStr(IIf(objImportacion.doc_FechaIngMilla = Nothing, Nothing, objImportacion.doc_FechaIngMilla))

            Me.cboMoneda.SelectedValue = CStr(objImportacion.IdMoneda)
            'Me.cboWorFor.SelectedValue = CStr(objImportacion.PrecImportacion)
            cbo.LlenarCboTipoPrecioImportacion(Me.cboWorFor, True)
            Me.cboWorFor.SelectedValue = CStr(objImportacion.IdTPImp)
            Me.txtTotalContenedores.Text = CStr(objImportacion.NroContenedores)
            Me.hddIdTipoOperacionv2.Value = CStr(objImportacion.IdTipoOperacion)

            Me.hddRelacDoc2.Value = CStr(objImportacion.IdDocumento2)
            txtNroOC.Text = CStr(objImportacion.NroDoc)
            Me.TbTipoCambioOC.Text = CStr(Math.Round(objImportacion.MontoxSustentar, 3))



            ' DETALLE IMPORTACION
            Dim ngcDetalleImportacion As New Negocio.ImportacionDetalle
            Dim ListaDetalleImp As List(Of Entidades.ImportacionDetalle) = ngcDetalleImportacion.DetalleImportacionSelectId(CInt(objImportacion.IdDocumento))
            For x As Integer = 0 To ListaDetalleImp.Count - 1
                Dim obj As New Entidades.ImportacionDetalle
                obj.IdDetalleDocumento = ListaDetalleImp.Item(x).IdDetalleDocumento
                obj.IdProducto = ListaDetalleImp.Item(x).IdProducto
                obj.CodigoProducto = ListaDetalleImp.Item(x).CodigoProducto
                obj.NomProducto = ListaDetalleImp.Item(x).NomProducto
                obj.IdUM = ListaDetalleImp.Item(x).IdUM
                obj.NomUM = ListaDetalleImp.Item(x).NomUM
                obj.Cantidad = ListaDetalleImp.Item(x).Cantidad
                obj.Peso = ListaDetalleImp.Item(x).Peso
                obj.AbvMagnitud = ListaDetalleImp.Item(x).AbvMagnitud
                obj.PrecioUnitario = ListaDetalleImp.Item(x).PrecioUnitario
                obj.Costo = ListaDetalleImp.Item(x).Costo
                obj.IdDetalleAfecto = ListaDetalleImp.Item(x).IdDetalleAfecto
                obj.Arancel = ListaDetalleImp(x).Arancel
                obj.dc_Importe = ListaDetalleImp.Item(x).dc_Importe
                Me.listaDetOC.Add(obj)
            Next
            dgvDetalleOC.DataSource = Me.listaDetOC
            dgvDetalleOC.DataBind()
            Me.setListaDetalleOC(Me.listaDetOC)
            calcularTotalPesoCosto()

            'DETALLE GASTO
            Dim ngcDetalleGastoImp As New Negocio.DocImportacion
            lista_DetalleGasto = ngcDetalleGastoImp.DetalleGastoImportacionSelectxId(CInt(objImportacion.IdDocumento))

            If lista_DetalleGasto.Count > 0 Then
                For x As Integer = 0 To lista_DetalleGasto.Count - 1
                    If lista_DetalleGasto(0).objTipoDocumento Is Nothing Then
                        lista_DetalleGasto(0).objTipoDocumento = getTipoDocumento()
                    End If
                    If x > 0 Then lista_DetalleGasto(x).objTipoDocumento = lista_DetalleGasto(0).objTipoDocumento
                Next
            End If

            dgvDetalleGasto.DataSource = Me.lista_DetalleGasto
            dgvDetalleGasto.DataBind()

            CalcularTotalGasto()

            Dim ListaDetalleGastoImpA As List(Of Entidades.DocImportacion) = ngcDetalleGastoImp.DetalleGastoImportacionSelectxId(CInt(objImportacion.IdDocumento))
            For xA As Integer = 0 To ListaDetalleGastoImpA.Count - 1
                Dim objDGIA As New Entidades.DocImportacion
                objDGIA.IdTipoDocumento = ListaDetalleGastoImpA.Item(xA).IdTipoDocumento

                For x2 As Integer = 0 To Me.dgvDetalleGasto.Rows.Count - 1
                    Dim cboTipDoc As DropDownList = CType(Me.dgvDetalleGasto.Rows(x2).Cells(4).FindControl("cboTipDocDetalleGasto"), DropDownList)
                    SelectAllTipoDocumento(cboTipDoc, True)
                    cboTipDoc.SelectedValue = CStr(ListaDetalleGastoImpA.Item(x2).IdTipoDocumento)
                Next
            Next
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "calcularTotalPesoOC();calcularPpeso();calculartotalgastotxt();calcularResumen();totalimportacion();", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        Finally
        End Try
    End Sub
    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Dim ngcAnularImp As New Negocio.DocImportacion
        Dim objAnularImp As New Entidades.DocImportacion
        objAnularImp.IdDocumento = CInt(Me.hddIdDocumento.Value)
        If ngcAnularImp.AnularImportacion(objAnularImp) Then
            ActivarBotones(True, True, False, False, False, True, False)
            HabilitarPaneles(False, False, False)
            LimpiarControles()
        Else
            objScript.mostrarMsjAlerta(Me, "Problemas en la Operación.")
        End If
    End Sub

#Region "Grilla Detalle de Gasto"

    Private Function getLista_DetalleGasto() As List(Of Entidades.DocImportacion)
        lista_DetalleGasto = New List(Of Entidades.DocImportacion)
        For Each fila As GridViewRow In Me.dgvDetalleGasto.Rows
            obj_DetalleGasta = New Entidades.DocImportacion
            With obj_DetalleGasta
                .IdConcepto = CInt(fila.Cells(1).Text)
                .NomConcepto = HttpUtility.HtmlDecode(fila.Cells(2).Text)
                .NroDocumento = CStr(CType(fila.Cells(3).FindControl("txtNroDocDetalleGasto"), TextBox).Text)
                .IdDetalleConcepto = CInt(CType(fila.Cells(3).FindControl("hddIdDetalleConcepto"), HiddenField).Value)
                Dim cboTipoDocumento As DropDownList = CType(fila.Cells(4).FindControl("cboTipDocDetalleGasto"), DropDownList)
                If fila.RowIndex = 0 Then
                    Dim lista As New List(Of Entidades.DocImportacion)
                    For x As Integer = 0 To cboTipoDocumento.Items.Count - 1
                        Dim obj As New Entidades.DocImportacion
                        With obj
                            .IdTipoDocumento = CInt(cboTipoDocumento.Items(x).Value)
                            .NomTipoDoc = cboTipoDocumento.Items(x).Text
                        End With
                        lista.Add(obj)
                    Next
                    .objTipoDocumento = lista
                End If
                If lista_DetalleGasto.Count > 0 Then .objTipoDocumento = lista_DetalleGasto(0).objTipoDocumento
                .IdTipoDocumento = CInt(cboTipoDocumento.SelectedValue)
                .IdProveedor = CInt(CType(fila.Cells(5).FindControl("hdd_idproveedor"), HiddenField).Value)
                .RazonSocial = HttpUtility.HtmlDecode(CType(fila.Cells(5).FindControl("lblnomproveedor"), Label).Text)
                .IdMoneda = CInt(CType(fila.Cells(6).FindControl("hdd_idmoneda"), HiddenField).Value)
                .MonSimbolo = HttpUtility.HtmlDecode(CType(fila.Cells(6).FindControl("lblMonSimboloDetalleGasto"), Label).Text)
                .NomCalculo = HttpUtility.HtmlDecode(fila.Cells(7).Text)
                .SubTotalGasto = CDec(CType(fila.Cells(8).FindControl("txtSubTotalDetalleGasto"), TextBox).Text)
            End With
            lista_DetalleGasto.Add(obj_DetalleGasta)
        Next
        Return lista_DetalleGasto
    End Function

    Private Sub LLenar_DetalleGasto()
        lista_DetalleGasto = getLista_DetalleGasto()

        For i As Integer = 0 To dgvGastoCapa.Rows.Count - 1
            Dim chbAdd As CheckBox = CType(Me.dgvGastoCapa.Rows(i).Cells(0).FindControl("chkSelectGasto"), CheckBox)
            If chbAdd.Checked Then
                If getListaDetalleGastoGrv(lista_DetalleGasto, CInt(dgvGastoCapa.Rows(i).Cells(1).Text)) Then
                    obj_DetalleGasta = New Entidades.DocImportacion
                    With obj_DetalleGasta

                        Dim ListaDetalleGasto As List(Of Entidades.DocImportacion) = (New Negocio.DocImportacion).DetalleGastoSelectxIdConcepto(CInt(dgvGastoCapa.Rows(i).Cells(1).Text))

                        For x As Integer = 0 To ListaDetalleGasto.Count - 1
                            Dim obj As New Entidades.DocImportacion
                            .IdConcepto = ListaDetalleGasto.Item(x).IdConcepto
                            .NomConcepto = ListaDetalleGasto.Item(x).NomConcepto
                            .IdMoneda = ListaDetalleGasto.Item(x).IdMoneda
                            .MonSimbolo = ListaDetalleGasto.Item(x).MonSimbolo
                            .NomCalculo = ListaDetalleGasto.Item(x).NomCalculo
                            .SubTotalGasto = ListaDetalleGasto.Item(x).SubTotalGasto
                        Next

                        .IdTipoDocumento = 0
                        If lista_DetalleGasto.Count = 0 Then .objTipoDocumento = getTipoDocumento()
                        If lista_DetalleGasto.Count > 0 Then .objTipoDocumento = lista_DetalleGasto(0).objTipoDocumento
                    End With
                    lista_DetalleGasto.Add(obj_DetalleGasta)
                End If
            End If
        Next
        dgvDetalleGasto.DataSource = lista_DetalleGasto
        dgvDetalleGasto.DataBind()
        CalcularTotalGasto()
    End Sub

    Private Sub dgvDetalleGasto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDetalleGasto.SelectedIndexChanged
        lista_DetalleGasto = getLista_DetalleGasto()
        lista_DetalleGasto.RemoveAt(dgvDetalleGasto.SelectedIndex)
        dgvDetalleGasto.DataSource = lista_DetalleGasto
        dgvDetalleGasto.DataBind()
        CalcularTotalGasto()
    End Sub

    Private Sub dgvDetalleGasto_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvDetalleGasto.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cbo As DropDownList = CType(e.Row.Cells(4).FindControl("cboTipDocDetalleGasto"), DropDownList)
            If lista_DetalleGasto(e.Row.RowIndex).IdTipoDocumento <> 0 Then
                cbo.SelectedValue = CStr(lista_DetalleGasto(e.Row.RowIndex).IdTipoDocumento)
            End If
        End If
    End Sub

    Private Sub btnAñadirGastoCapaG_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAñadirGastoCapaG.Click
        Try
            Me.LLenar_DetalleGasto()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
        'addGrillaDetalleGasto()
    End Sub

    Private Function getTipoDocumento() As List(Of Entidades.DocImportacion)
        Dim lista As List(Of Entidades.DocImportacion) = (New Negocio.DocImportacion).SelectAllTipoDocumento()
        Dim objImpTipoDoc As New Entidades.DocImportacion(0, "-----")
        lista.Insert(0, objImpTipoDoc)
        Return lista
    End Function

    Private Function getListaDetalleGastoGrv(ByVal lista As List(Of Entidades.DocImportacion), ByVal idconcepto As Integer) As Boolean
        For x As Integer = 0 To lista.Count - 1
            If lista(x).IdConcepto = idconcepto Then
                Return False
            End If
        Next
        Return True
    End Function

#End Region

    Private Sub btnOkBusImp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOkBusImp.Click
        hddIndicador.Value = CStr(1)
        Dim BusSerie, BusCodigo As Integer
        If Me.txtSerieBusImp.Text = "" Then : BusSerie = 0 : ElseIf Me.txtSerieBusImp.Text <> "" Then : BusSerie = CInt(txtSerieBusImp.Text) : End If
        If Me.txtCodigoBusImp.Text = "" Then : BusCodigo = 0 : ElseIf Me.txtCodigoBusImp.Text <> "" Then : BusCodigo = CInt(txtCodigoBusImp.Text) : End If
        ListarImportacion(CInt(BusSerie), CInt(BusCodigo), CInt(Me.cboProveedorBusImp.SelectedValue), Me.dgvListImport)
    End Sub
    Sub ListarImportacion(ByVal Serie As Integer, ByVal Codigo As Integer, ByVal IdProveedor As Integer, ByVal grilla As GridView)
        Dim ngcImportacion As New Negocio.DocImportacion
        ' Dim lista As List(Of Entidades.DocImportacion) = ngcImportacion.ImportacionSelectAllxSeriexCodigoxProveedor(CInt(Serie), CInt(Codigo), CInt(IdProveedor))
        Dim lista As List(Of Entidades.DocImportacion) = ngcImportacion.ImportacionProyectadoSelectAllxSeriexCodigoxProveedor(CInt(Serie), CInt(Codigo), CInt(IdProveedor))

        If lista.Count > 0 Then
            grilla.DataSource = lista
            grilla.DataBind()
            Session("ExistNroImp") = "True"
        Else
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, "No se encontraron registros")
            grilla.DataBind()
        End If
    End Sub
    Protected Sub dgvListImport_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvListImport.SelectedIndexChanged
        Dim hddIdDoc As HiddenField = CType(Me.dgvListImport.SelectedRow.Cells(1).FindControl("hddIdDocBusImp"), HiddenField)
        EditarImportacion(CInt(hddIdDoc.Value))
        Me.HabilitarPaneles(True, False, False)
        Me.ActivarBotones(False, False, False, True, True, True, False)
        chkUpdatePrecioI.Checked = False
    End Sub


#Region "Busqueda de Proveedores"

    Private Sub gvProveedor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvProveedor.SelectedIndexChanged
        Try
            Dim hdd_idproveedor As HiddenField = CType(Me.dgvDetalleGasto.Rows(CInt(Me.hddindex.Value)).Cells(5).FindControl("hdd_idproveedor"), HiddenField)
            Dim lblnomproveedor As Label = CType(Me.dgvDetalleGasto.Rows(CInt(Me.hddindex.Value)).Cells(5).FindControl("lblnomproveedor"), Label)
            hdd_idproveedor.Value = gvProveedor.SelectedRow.Cells(1).Text
            lblnomproveedor.Text = HttpUtility.HtmlDecode(gvProveedor.SelectedRow.Cells(2).Text)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btBuscarProveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btBuscarProveedor.Click
        ViewState.Add("NomPersona", Me.txtBuscarProveedor.Text.Trim)
        ViewState.Add("Ruc", Me.txtbuscarRucProveedor.Text.Trim)
        ViewState.Add("Condicion", CInt(Me.rbcondicion.SelectedValue))
        ViewState.Add("TipoPersona", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", 1, 2)))

        OnClick_BuscarProveedor()
    End Sub

    Private Sub OnClick_BuscarProveedor()
        BuscarProveedor(0)
    End Sub

    Private Sub BuscarProveedor(ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(tbPageIndex_Proveedor.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(tbPageIndex_Proveedor.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(tbPageIndex_Proveedor.Text) - 1)
        End Select

        ListaProveedor = (New Negocio.OrdenCompra).listarPersonaJuridicaxRolxEmpresa(CStr(ViewState("NomPersona")), CStr(ViewState("Ruc")), CInt(ViewState("TipoPersona")), 2, CInt(ViewState("Condicion")), 1, index, gvProveedor.PageSize)

        If ListaProveedor.Count > 0 Then
            gvProveedor.DataSource = ListaProveedor
            gvProveedor.DataBind()
            tbPageIndex_Proveedor.Text = CStr(index + 1)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " onCapa('verBusquedaProveedor'); ", True)
        Else
            gvProveedor.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " onCapa('verBusquedaProveedor'); alert('No se hallaron registros');", True)
        End If

    End Sub

    Private Sub btAnterior_Proveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior_Proveedor.Click
        Try
            BuscarProveedor(1)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Proveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente_Proveedor.Click
        Try
            BuscarProveedor(2)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Proveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr_Proveedor.Click
        Try
            BuscarProveedor(3)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#End Region




    Private Sub btnExportarExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click
        UpdatePanel1.Update()
        OnClick_ExportarExcel()
    End Sub
    Private Sub OnClick_ExportarExcel()

        calcularTotalPesoCosto()
        Dim totalpesooc As Decimal = 0 : If IsNumeric(hddTotalPesoOC.Value) Then totalpesooc = CDec(hddTotalPesoOC.Value)
        Dim totalcostooc As Decimal = 0 : If IsNumeric(txttotalocOC.Text) Then totalcostooc = CDec(txttotalocOC.Text)
        Dim TotalGastoxPeso As Decimal = getTotalGastoxPeso()
        Dim TotalGastoxMoneda As Decimal = getTotalGastoxMoneda()

        For i As Integer = -1 To Me.dgvDetalleOC.Rows.Count - 1

            Dim trow As New TableRow()
            For p As Integer = 0 To 15

                Dim tcell As New TableCell()
                tcell.ControlStyle.BorderStyle = BorderStyle.NotSet
                tcell.ControlStyle.BorderWidth = 1

                If i = -1 Then
                    tcell.ControlStyle.BackColor = Drawing.Color.LightSteelBlue
                    tcell.ControlStyle.Font.Bold = True
                    tcell.ControlStyle.Height = 25
                    Select Case p
                        Case 0 : tcell.Text = "Cod."
                        Case 1 : tcell.Text = "Descripcion"
                        Case 2 : tcell.Text = "UM"
                        Case 3 : tcell.Text = "Cant"
                        Case 4 : tcell.Text = "Peso/Volumen"
                        Case 5 : tcell.Text = "P.Unit."
                        Case 6 : tcell.Text = "Costo"
                        Case 7 : tcell.Text = "P.Peso"
                        Case 8 : tcell.Text = "P.Monto"
                        Case 9 : tcell.Text = "T.Gasto"
                        Case 10 : tcell.Text = "G.Uni"
                        Case 11 : tcell.Text = "C.Imp"
                        Case 12 : tcell.Text = "Arancel"
                        Case 13 : tcell.Text = "C.Uni.Imp"
                        Case 14 : tcell.Text = "SubTotal.Imp"
                        Case 15 : tcell.Text = "C.Uni.Imp"
                    End Select
                Else

                    Dim PesoxProducto As Decimal = 0
                    If IsNumeric(CType(Me.dgvDetalleOC.Rows(i).FindControl("txtdgvPesoOC"), TextBox).Text) Then PesoxProducto = CDec(CType(Me.dgvDetalleOC.Rows(i).FindControl("txtdgvPesoOC"), TextBox).Text)
                    Dim Peso As Decimal = (PesoxProducto * TotalGastoxPeso) / totalpesooc

                    Dim CostoxProducto As Decimal = 0
                    If IsNumeric(CType(Me.dgvDetalleOC.Rows(i).FindControl("txtdgvCostoOC"), TextBox).Text) Then CostoxProducto = CDec(CType(Me.dgvDetalleOC.Rows(i).FindControl("txtdgvCostoOC"), TextBox).Text)
                    Dim Monto As Decimal = (CostoxProducto * TotalGastoxMoneda) / totalcostooc

                    Dim Cantidad As Decimal = 0
                    If IsNumeric(CType(Me.dgvDetalleOC.Rows(i).FindControl("txtdgvCantOC"), TextBox).Text) Then Cantidad = CDec(CType(Me.dgvDetalleOC.Rows(i).FindControl("txtdgvCantOC"), TextBox).Text)

                    Dim GastoUnitario As Decimal = 0
                    GastoUnitario = ((Peso + Monto) / Cantidad) + (CostoxProducto / Cantidad)

                    Dim GastoUnitarioImporte As Decimal = 0
                    GastoUnitarioImporte = (((Peso + Monto) / Cantidad) + (CostoxProducto / Cantidad)) * Cantidad

                    Select Case p

                        Case 0 : tcell.Text = HttpUtility.HtmlDecode(CType(Me.dgvDetalleOC.Rows(i).FindControl("lblCodigoProducto"), Label).Text).Trim 'Cod.
                        Case 1 : tcell.Text = HttpUtility.HtmlDecode(Me.dgvDetalleOC.Rows(i).Cells(2).Text).Trim 'Descripcion
                        Case 2 : tcell.Text = HttpUtility.HtmlDecode(CType(Me.dgvDetalleOC.Rows(i).FindControl("lblUM"), Label).Text).Trim 'UM
                        Case 3 : tcell.Text = HttpUtility.HtmlDecode(CStr(Cantidad)).Trim 'Cant
                        Case 4 : tcell.Text = HttpUtility.HtmlDecode(CType(Me.dgvDetalleOC.Rows(i).FindControl("txtdgvPesoOC"), TextBox).Text).Trim 'Peso/Volumen
                        Case 5 : tcell.Text = HttpUtility.HtmlDecode(CType(Me.dgvDetalleOC.Rows(i).FindControl("txtdgvPUnitOC"), TextBox).Text).Trim 'P.Unit.
                        Case 6 : tcell.Text = HttpUtility.HtmlDecode(CType(Me.dgvDetalleOC.Rows(i).FindControl("txtdgvCostoOC"), TextBox).Text).Trim 'Costo
                        Case 7 : tcell.Text = HttpUtility.HtmlDecode(CStr(Math.Round(Peso, 2))).Trim 'P.Peso
                        Case 8 : tcell.Text = HttpUtility.HtmlDecode(CStr(Math.Round(Monto, 2))).Trim 'P.Monto
                        Case 9 : tcell.Text = HttpUtility.HtmlDecode(CStr(Math.Round(Peso + Monto, 2))).Trim 'T.Gasto
                        Case 10 : tcell.Text = HttpUtility.HtmlDecode(CStr(Math.Round((Peso + Monto) / Cantidad, 2))).Trim 'G.Uni
                        Case 11 : tcell.Text = HttpUtility.HtmlDecode(CStr(Math.Round(CostoxProducto / Cantidad, 2))).Trim 'C.Imp.
                        Case 12 : tcell.Text = HttpUtility.HtmlDecode(CType(Me.dgvDetalleOC.Rows(i).FindControl("txtArancel"), TextBox).Text).Trim 'Arancel
                        Case 13 : tcell.Text = HttpUtility.HtmlDecode(CStr(Math.Round(GastoUnitario, 2))).Trim 'C.Uni.Imp.
                        Case 14 : tcell.Text = HttpUtility.HtmlDecode(CStr(Math.Round(GastoUnitarioImporte, 2))).Trim 'SubTotal.Imp
                        Case 15 : tcell.Text = HttpUtility.HtmlDecode(CType(Me.dgvDetalleOC.Rows(i).FindControl("txtdgvCUniImp"), TextBox).Text).Trim 'C.Uni.Imp

                    End Select
                End If

                trow.Cells.Add(tcell)

            Next

            tablaPrincipal.Rows.Add(trow)
        Next




        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + "Importacion")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"

        Response.ContentEncoding = System.Text.Encoding.UTF8

        Dim strWriter As System.IO.StringWriter
        strWriter = New System.IO.StringWriter
        Dim htmlTextWriter As System.Web.UI.HtmlTextWriter
        htmlTextWriter = New System.Web.UI.HtmlTextWriter(strWriter)



        tablaPrincipal.RenderControl(htmlTextWriter)

        EnableViewState = False

        Response.Write(strWriter.ToString)

        Response.End()

    End Sub

    Function getTotalGastoxPeso() As Decimal

        Dim TotalGastoxPeso As Decimal = 0
        For Each row As GridViewRow In Me.dgvDetalleGasto.Rows

            If row.Cells(7).Text = "Peso" Then
                If IsNumeric(CType(row.FindControl("txtSubTotalDetalleGasto"), TextBox).Text) Then
                    TotalGastoxPeso = TotalGastoxPeso + CDec(CType(row.FindControl("txtSubTotalDetalleGasto"), TextBox).Text)
                End If
            End If

        Next

        Return TotalGastoxPeso

    End Function

    Function getTotalGastoxMoneda() As Decimal

        Dim TotalGastoxMoneda As Decimal = 0
        For Each row As GridViewRow In Me.dgvDetalleGasto.Rows

            If row.Cells(7).Text = "Moneda" Then
                If IsNumeric(CType(row.FindControl("txtSubTotalDetalleGasto"), TextBox).Text) Then
                    TotalGastoxMoneda = TotalGastoxMoneda + CDec(CType(row.FindControl("txtSubTotalDetalleGasto"), TextBox).Text)
                End If
            End If

        Next

        Return TotalGastoxMoneda

    End Function

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
    End Sub
End Class