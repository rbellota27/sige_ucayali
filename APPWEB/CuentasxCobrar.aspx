﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="CuentasxCobrar.aspx.vb" Inherits="APPWEB.CuentasxCobrar" 
    title="C.x Cobrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table border="0" cellspacing="0" style="width: 1000px; height: 49px">
        <tr>
            
            <td style="width: 100px; height: 44px">
                <asp:Menu ID="Menu1" runat="server" BackColor="Control" DynamicHorizontalOffset="2"
                    Font-Bold="False" Font-Italic="False" Font-Names="Verdana" Font-Size="8pt" ForeColor="DarkRed"
                    Orientation="Horizontal" StaticSubMenuIndent="10px" Style="vertical-align: top">
                    <StaticSelectedStyle BackColor="#5D7B9D" />
                    <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                    <DynamicHoverStyle BackColor="Peru" ForeColor="White" />
                    <DynamicMenuStyle BackColor="#F7F6F3" />
                    <DynamicSelectedStyle BackColor="SteelBlue" />
                    <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                    <StaticHoverStyle BackColor="#7C6F57" ForeColor="White" />
                    <Items>
                        <asp:MenuItem Text="Areas" Value="Areas">
                            <asp:MenuItem NavigateUrl="~/Ventas.aspx" Text="Ventas" Value="Ventas"></asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/Almacen.aspx" Text="Almac&#233;n" Value="Almac&#233;n">
                            </asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/CuentasxCobrar.aspx" Text="Cuentas Por Cobrar" Value="Cuentas Por Cobrar">
                            </asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/CuentasxPagar.aspx" Text="Cuentas Por Pagar" Value="Cuentas Por Pagar">
                            </asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/Compras.aspx" Text="Compras" Value="Compras"></asp:MenuItem>
                            <asp:MenuItem Text="Administraci&#243;n del Sistema" Value="Administraci&#243;n del Sistema" NavigateUrl="~/frmAdministracionSistema.aspx">
                            </asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/frmInformacionGerencial.aspx" Text="Informaci&#243;n Gerencial"
                                Value="Informaci&#243;n Gerencial"></asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/Principal.aspx" Text="Principal" Value="Principal"></asp:MenuItem>
                        </asp:MenuItem>
                        <asp:MenuItem Text="Mantenimientos" Value="Mantenimientos">
                            <asp:MenuItem Text="Cuenta del Cliente" Value="Cuenta del Cliente"></asp:MenuItem>
                        </asp:MenuItem>
                        <asp:MenuItem Text="Recibos de Ingreso" Value="Recibos de Ingreso"></asp:MenuItem>
                        <asp:MenuItem Text="Estado de Cuenta del Cliente" Value="Estado de Cuenta del Cliente">
                        </asp:MenuItem>
                        <asp:MenuItem Text="Deudas Totalizadas" Value="Deudas Totalizadas"></asp:MenuItem>
                    </Items>
                </asp:Menu>
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
</asp:Content>
