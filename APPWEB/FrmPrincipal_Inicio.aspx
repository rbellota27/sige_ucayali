﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmPrincipal_Inicio.aspx.vb" Culture="Auto" UICulture="Auto"
    Inherits="APPWEB.FrmPrincipal_Inicio" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>    
     <!DOCTYPE html> 

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <title>.::Sistema Integral de Gestión Empresarial::. - SIGE</title>

    <link href="Estilos/stlGeneral.css" rel="stylesheet" type="text/css" />
    <link href="Estilos/Menu.css" rel="stylesheet" type="text/css" />
    <link href="Estilos/Controles.css" rel="stylesheet" type="text/css" />
    <link href="css/StyleMenuTelerik.css" rel="stylesheet" type="text/css" />

</head>
<body style="background: white; margin-top: 1px; margin-left: 3px; margin-right: 2px;height:100%;max-width:100%;width:100%" onload="mueveReloj()">
    <form id="form1" runat="server">
    <telerik:RadScriptManager Runat="server" EnablePageMethods="True" EnableTheming="True"
            EnableScriptGlobalization="True">
            <Scripts>
                <asp:ScriptReference Assembly="Telerik.Web.UI"
                    Name="Telerik.Web.UI.Common.Core.js">
                </asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI"
                    Name="Telerik.Web.UI.Common.jQuery.js">
                </asp:ScriptReference>
                <asp:ScriptReference Assembly="Telerik.Web.UI"
                    Name="Telerik.Web.UI.Common.jQueryInclude.js">
                </asp:ScriptReference>
            </Scripts>
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager2" runat="server">
    </telerik:RadAjaxManager>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table width="100%" cellpadding="1" cellspacing="0">
                    <tr>

                        <td style="height: 70px; width: 125px;" align="left" valign="baseline">
                            <asp:Image ID="Image4" runat="server" Width="125px" Height="70px" ImageUrl="~/Imagenes/PrincipalIzquierda.png"
                                ImageAlign="AbsMiddle" />
                        </td>
                        <td style="height: 70px; width: 100%;" align="center" valign="baseline">
                            <asp:Image ID="Image1" runat="server" Height="70px" Width="100%" ImageUrl="~/Imagenes/Banner.png"
                                ImageAlign="AbsMiddle" />
                        </td>
                        <td style="height: 70px; width: 125px;" align="right" valign="baseline">
                            <asp:Image ID="Image6" Width="125px" Height="70px" runat="server" ImageUrl="~/Imagenes/PrincipalDerecha.jpg"
                                ImageAlign="AbsMiddle" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table style="background: #799CC8;" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 100%;" valign="top">
                            <div class="demo-container no-bg">       
                                <telerik:RadMenu ID="RadMenu1" CssClass="mainMenu" runat="server"
                                    ShowToggleHandle="True" ResolvedRenderMode="Classic" BackColor="#0066CC">
                                </telerik:RadMenu>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>            
                <table style="background: White;width:100%" cellpadding="1" cellspacing="1">
                    <tr>
                        <td width="50px">
                         <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button ID="xbtnProducto" runat="server" Text="Ctrl+Shift+P"></asp:Button>
                            </ContentTemplate>
                        </asp:UpdatePanel>                             
                        </td>

                          <td width="50px">
                         <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button ID="xbtnProductoSIGE" runat="server" Text="SIGE"></asp:Button>
                            </ContentTemplate>
                        </asp:UpdatePanel>                             
                        </td>
                        <td align="left" class="Texto">
                           Usuario:
                        </td>
                        <td >                            
                            <asp:Label ID="lblUsuario" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                        </td>
                        <td align="left" class="Texto">
                            Tienda:
                        </td>
                        <td>
                            <asp:Label ID="Lb_Tienda" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                        </td>
                        <td class="Texto" align="left">
                            Fecha Ingreso:
                        </td>
                        <td>
                            <asp:Label ID="reloj" runat="server" CssClass="LabelRojo" Text=""></asp:Label>
                        </td>
                        <td class="Texto">
                            T.C. SUNAT ($):
                        </td>
                        <td>
                            <asp:Label ID="lblTC_Sunat" CssClass="LabelRojo" runat="server" Text="0"></asp:Label>
                        </td>
                        <td class="Texto">
                            T.C. INTERNO ($):
                        </td>
                        <td>
                            <asp:Label ID="lblTC_Indusferr" CssClass="LabelRojo" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrarSesion" runat="server" ImageUrl="~/Imagenes/CerrarSesion_B.JPG"
                                ImageAlign="AbsMiddle" onmouseout="this.src='Imagenes/CerrarSesion_B.JPG';"
                                onmouseover="this.src='Imagenes/CerrarSesion_A.JPG';" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <iframe name="frame_Content" id="frame_Content" style="width:100%;max-width:100%;border-bottom:0px;border-left:0px;background-color:White"
     onload='resizeIframe(this);'>    </iframe>
    </form>
</body>
<script type="text/javascript">
    function resizeIframe(obj) {
        obj.style.width = 100 + '%';
        obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    }
    </script>
    <script src="JS/JS_Fecha.js" type="text/javascript"></script>
    <script src="JS/JS_Impresion.js" type="text/javascript"></script>
    <script src="JS/Util.js" type="text/javascript"></script>
    <script type="text/javascript">

        function startTime() {
            var today = new Date();
            var dia = today.getDate();
            var mes = today.getMonth() + 1;
            var anio = today.getFullYear();
            //Concateno 0 si fuese necesario
            if (dia.toString().length == 1) {dia = "0" + dia; }
            if (mes.toString().length == 1) { mes = "0" + mes; }

            h = today.getHours();
            m = today.getMinutes();
            s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('reloj').innerHTML = dia + "/" + mes + "/" + anio + " " + h + ":" + m + ":" + s; t = setTimeout('startTime()', 500);
        }
        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        window.onload = function () { startTime(); } 

        //******* Detectando Tecla ESC
        function detectar_tecla(elEvento) {
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);

            //            if (window.event) keyCode = window.event.keyCode;
            //            else if (e) keyCode = e.which;
            with (elEvento) {
                if ((caracter == 80 && ctrlKey && shiftKey)) {  //**  Ctrl + Shift + 1
                    document.getElementById('<%=xbtnProducto.ClientID %>').click();
                    return false;
                    //window.open('../Finanzas/FrmPage_Load.aspx?Tipo=3', null, 'resizable=yes,width=800,height=700,scrollbars=1', null);
                }
            }
        }
        //document.onkeydown = detectar_tecla('event');

        function window_onkeydown(evento) {
            detectar_tecla(evento);
        }
    </script>
</html>
