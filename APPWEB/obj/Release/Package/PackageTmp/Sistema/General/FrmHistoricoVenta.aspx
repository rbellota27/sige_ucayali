﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmHistoricoVenta.aspx.vb" Inherits="APPWEB.FrmHistoricoVenta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td class="TituloCelda">
                HISTÓRICO VENTAS</td>
        </tr>
        <tr align="center">
            <td >
                <table >
                    <tr>
                        <td class="Texto" style="font-weight:bold;text-align:right" >
                            Empresa:</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true" >
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight:bold;text-align:right" >
                            Tienda:</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="cboTienda" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight:bold;text-align:right"  >
                            Línea:</td>
                        <td style="text-align: left">
                          <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true" >
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight:bold;text-align:right"  >
                            Sub Línea:</td>
                        <td style="text-align: left">
                             <asp:DropDownList ID="cboSubLinea" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td  colspan="8" >
                        <table>
                        <tr>
                        <td class="Texto" style="font-weight:bold;text-align:right" >Año:</td>
                        <td>
                        <asp:DropDownList ID="cboAnio" runat="server">
                        </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight:bold;text-align:right" >Mes:</td>
                        <td>
                           <asp:DropDownList ID="cboMes" runat="server">
                            <asp:ListItem Value="1">Enero</asp:ListItem>
                            <asp:ListItem Value="2">Febrero</asp:ListItem>
                            <asp:ListItem Value="3">Marzo</asp:ListItem>
                            <asp:ListItem Value="4">Abril</asp:ListItem>
                            <asp:ListItem Value="5">Mayo</asp:ListItem>
                            <asp:ListItem Value="6">Junio</asp:ListItem>
                            <asp:ListItem Value="7">Julio</asp:ListItem>
                            <asp:ListItem Value="8">Agosto</asp:ListItem>
                            <asp:ListItem Value="9">Setiembre</asp:ListItem>
                            <asp:ListItem Value="10">Octubre</asp:ListItem>
                            <asp:ListItem Value="11">Noviembre</asp:ListItem>
                            <asp:ListItem Value="12">Diciembre</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        </tr>
                        </table>
                            
                        
                         
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
            <table>
            <tr>
            <td>
                <asp:Button ID="btnGuardar" Width="120px" runat="server" Text="Guardar Historial" OnClientClick="return( valOnClick_btnGuardar()  );" /></td>            
            </tr>
            </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">

        function valOnClick_btnGuardar() {

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('DEBE SELECCIONAR UNA EMPRESA. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('DEBE SELECCIONAR UNA TIENDA. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
        
            return confirm('Desea continuar con la Generación del < Histórico - Venta > según los parámetros seleccionados ?');
        }
    
    </script>
</asp:Content>
