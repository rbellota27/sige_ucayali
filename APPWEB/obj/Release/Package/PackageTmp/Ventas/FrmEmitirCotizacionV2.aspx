<%@ Page Title="Emisi�n Cotizaci�n" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmEmitirCotizacionV2.aspx.vb" Inherits="APPWEB.FrmEmitirCotizacionV2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" ToolTip="Nuevo" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                                Text="Editar" ToolTip="Editar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                                Text="Anular" ToolTip="Anular" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" OnClientClick="return( valSaveDocumento()  );"
                                Text="Guardar" ToolTip="Guardar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  valOnClick_btnImprimir(1)  );"
                                Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimirDetallado" runat="server" OnClientClick="return(  valOnClick_btnImprimir(2)  );"
                                Text="Imprimir Estandar" ToolTip="Imprimir Estandar" Width="130px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimirResumido" runat="server" OnClientClick="return(  valOnClick_btnImprimir(3)  );"
                                Text="Imprimir Resumido" ToolTip="Imprimir Resumido" Width="130px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscarDocumentoRef" runat="server" Text="Documento Ref." OnClientClick="  return(  onCapa('capaDocumentosReferencia')  );  "
                                ToolTip="Buscar Documento Referencia" Width="115px" />
                        </td>
                        <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCeldaLeft">
                <asp:Image ID="imgcot" runat="server" Width="15px" />&nbsp;COTIZACI�N
                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="Panel_Cab"
                    ImageControlID="imgcot" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                    Collapsed="true" CollapseControlID="imgcot" ExpandControlID="imgcot">
                </cc1:CollapsiblePanelExtender>
                                
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa::
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" Width="100%" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( valOnKeyPressCodigoDoc(this)  );"
                                    ReadOnly="true" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    ToolTip="Buscar Documento por [ Serie - N�mero ]." OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" ToolTip="B�squeda Avanzada" OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );"
                                    runat="server" Enabled="false" CssClass="Texto">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Moneda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboMoneda" Width="100%" runat="server" CssClass="LabelRojo"
                                    Font-Bold="true" AutoPostBack="false" onChange=" return( valOnChange_cboMoneda()  ); "
                                    onKeyup=" return( valOnChange_cboMoneda()  ); " onKeypress=" return( valOnChange_cboMoneda()  ); "
                                    onClick=" return( valOnChange_cboMoneda()  ); ">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Tipo Operaci�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" runat="server" Width="100%" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Motivo Traslado:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboMotivoTraslado" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Condici�n:
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="cboCondicionPago" Width="100%" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Medio de Pago:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboMedioPago" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Vcto.:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtFechaVcto" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision0_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaVcto_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaVcto">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Fecha a Entregar:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtFechaAEntregar" runat="server" CssClass="TextBox_Fecha" onblur="return(     valFecha_Blank(this)  );"
                                    Width="100px"></asp:TextBox>
                                <cc1:CalendarExtender ID="txtFechaAEntregar_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaAEntregar">
                                </cc1:CalendarExtender>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaAEntregar">
                                </cc1:MaskedEditExtender>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Almac�n Ref.:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboAlmacenReferencia" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Vendedor:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboUsuarioComision" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                DOCUMENTOS DE REFERENCIA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_DocRef" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="NomTipoDocumento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red"
                                            HeaderText="Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                        <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdAlmacen0" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdPersona0" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
          
            
                <asp:Panel ID="pn_MaestroObra" runat="server" Width="100%">
                    <table width="100%">
                       <tr>
                            <td class="TituloCeldaLeft">
                                <asp:Image ID="ImageMaestroObra" runat="server" />&nbsp;MAESTRO DE OBRA
                   
                              <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_Panel_MaestroObra" runat="server"
                                    TargetControlID="Panel_MaestroObra" ImageControlID="ImageMaestroObra" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                    CollapsedImage="~/Imagenes/Mas_B.JPG" Collapsed="true" CollapseControlID="ImageMaestroObra"
                                    ExpandControlID="ImageMaestroObra">                                
                                </cc1:CollapsiblePanelExtender>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_MaestroObra" runat="server" Width="100%">
                                    <table>
                                        <tr>
                                            <td class="Texto" align="right">
                                                Maestro:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMaestro" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                    Width="450px"></asp:TextBox>
                                            </td>
                                           <td>
                                           
                                               <asp:TextBox ID="txtCodigoMaestro" runat="server" CssClass="TextBoxReadOnly" 
                                                   ReadOnly="True" Width="16px" ForeColor="#DEE1E5"></asp:TextBox>
                                           
                                           </td>
                                           <td>
                                           
                                          
                                                   
                                                   
                                                   <asp:TextBox ID="txtIdMaestroObra_BuscarxId" runat="server" 
                                                       onFocus="return( aceptarFoco(this)  );" 
                                                       onKeyPress="return(  valKeyPressBuscarMaestroId()  );" TabIndex="16" 
                                                       Width="74px"></asp:TextBox>
                                           
                                           </td>
                                           
                                           <td>
                                                <asp:ImageButton ID="btnBuscarMaestroxId" runat="server" 
                                                    ImageUrl="~/Imagenes/Busqueda_b.JPG" 
                                                    OnClientClick="return( valBuscarMaestroObra());" 
                                                    onmouseout="this.src='/Imagenes/Busqueda_b.JPG';" 
                                                    onmouseover="this.src='/Imagenes/Busqueda_A.JPG';" Style="width: 27px" 
                                                    TabIndex="17" />
                                            </td>
                                            <td>
                                            
                                           
                                                    <asp:ImageButton ID="btnBuscarMaestroObra" runat="server" 
                                                    ImageUrl="~/Imagenes/Buscar_b.JPG" OnClientClick="return(buscarMaestroObra());" 
                                                    onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                                    onmouseover="this.src='/Imagenes/Buscar_A.JPG';" TabIndex="13" Width="63px"/>                                                    
                                                                                                
                                            </td>
                                            
             
                                              <td>
                                                  <asp:ImageButton ID="btnNuevoCliente0" runat="server" 
                                                      ImageUrl="~/Imagenes/Nuevo_b.JPG" OnClientClick="return(onCapaMaestroObra());" 
                                                      onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" 
                                                      onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" TabIndex="14" />
                                            </td>
                                              <td>
                                                  <asp:ImageButton ID="btnEditar_Cliente0" runat="server" 
                                                      ImageUrl="~/Imagenes/Editar_B.JPG" 
                                                      OnClientClick=" return(    valEditarMaestroObra()   ); " 
                                                      onmouseout="this.src='/Imagenes/Editar_B.JPG';" 
                                                      onmouseover="this.src='/Imagenes/Editar_A.JPG';" Visible="true" />
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="Texto" align="right">
                                                D.N.I.:
                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtDni_Maestro" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                Width="150px"></asp:TextBox>
                                                        </td>
                                                        <td class="Texto" align="right">
                                                            R.U.C.:
                                                            
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtRuc_Maestro" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                Width="150px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                CLIENTE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cliente" runat="server">
                    <table cellspacing="0" style="width: 100%">
                        <tr>
                            <td style="text-align: right;">
                                &nbsp;
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarCliente" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                OnClientClick="return( buscarCliente()  );" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                                onmouseover="this.src='/Imagenes/Buscar_A.JPG';" TabIndex="13" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnNuevoCliente" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG"
                                                OnClientClick="return(onCapaCliente());" onmouseout="this.src='/Imagenes/Nuevo_b.JPG';"
                                                onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" TabIndex="14" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnEditar_Cliente" runat="server" ImageUrl="~/Imagenes/Editar_B.JPG"
                                                OnClientClick=" return(    valEditarCliente()   ); " onmouseout="this.src='/Imagenes/Editar_B.JPG';"
                                                onmouseover="this.src='/Imagenes/Editar_A.JPG';" Visible="true" />
                                        </td>
                                        <td class="Texto">
                                            Tipo Persona:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoPersona_PanelCliente" runat="server" Enabled="False"
                                                Font-Bold="True" Width="116px">
                                                <asp:ListItem Value="0">---------</asp:ListItem>
                                                <asp:ListItem Value="1">Natural</asp:ListItem>
                                                <asp:ListItem Value="2">Jur�dica</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        <asp:Label ID="Label12026" runat="server" CssClass="Label" Text="Tipo Cliente"></asp:Label>
                                        </td>
                                         <td>
                                <asp:DropDownList ID="cborolcliente" runat="server" Enabled="False" 
                                    Font-Bold="True" onClick="  return(); " 
                                    onKeypress=" return(); " 
                                    Width="207px">
                                </asp:DropDownList>
                                         
                                           </td>
                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Ap. Paterno o R. Social:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtApPaterno_RazonSocial" runat="server" CssClass="TextBoxReadOnly"
                                    ReadOnly="True" Width="270px"></asp:TextBox>
                                <asp:Label ID="Label39_12" runat="server" CssClass="Label" Text="Ap. Materno:"></asp:Label>
                                <asp:TextBox ID="txtApMaterno" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                    Width="100px"></asp:TextBox>
                                <asp:Label ID="Label40" runat="server" CssClass="Label" Text="Nombre:"></asp:Label>
                                <asp:TextBox ID="txtNombres" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                    Width="130px"></asp:TextBox>
                                <asp:TextBox ID="txtCodigoCliente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                    Width="50px"></asp:TextBox>
                                <asp:TextBox ID="txtIdCliente_BuscarxId" runat="server" onFocus="return( aceptarFoco(this)  );"
                                    onKeyPress="return(  valKeyPressBuscarPersonaxId()  );" TabIndex="16" Width="50px"></asp:TextBox>
                                <asp:ImageButton ID="btnBuscarClientexIdCliente" runat="server" ImageUrl="~/Imagenes/Busqueda_b.JPG"
                                    OnClientClick="return(valBuscarClientexId());" onmouseout="this.src='/Imagenes/Busqueda_b.JPG';"
                                    onmouseover="this.src='/Imagenes/Busqueda_A.JPG';" Style="width: 27px" TabIndex="17" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label3" runat="server" CssClass="Label" Text="Dni"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDni" runat="server" CssClass="TextBoxReadOnly" MaxLength="8"
                                    ReadOnly="True" Style="margin-left: 0px; margin-bottom: 0px" Width="70px"></asp:TextBox>
                                <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Ruc"></asp:Label>
                                <asp:TextBox ID="txtRuc" runat="server" CssClass="TextBoxReadOnly" MaxLength="11"
                                    ReadOnly="True" Width="90px"></asp:TextBox>
                                <asp:Label ID="Label12021" runat="server" CssClass="Label" Text="Precio de Venta"></asp:Label>
                                <asp:DropDownList ID="cboTipoPrecioV" runat="server" onClick="  return(   valOnClick_cboTipoPrecioV()    ); "
                                    Enabled="false" Font-Bold="True" Width="123px" onKeypress=" return(       valOnClick_cboTipoPrecioV()        ); ">
                                 </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; height: 25px;">
                                <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Direcci�n"></asp:Label>
                            </td>
                            <td style="height: 25px">
                                <asp:TextBox ID="txtDireccionCliente" runat="server" CssClass="TextBoxReadOnly" MaxLength="50"
                                    ReadOnly="True" Width="588px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label12022" runat="server" CssClass="Label" Text="Tipo Agente"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoAgente" runat="server" Enabled="False" Font-Bold="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label12025" runat="server" CssClass="Label" Text="Tasa del Agente %"></asp:Label>
                                <asp:TextBox ID="txtTasaAgente" runat="server" CssClass="TextBox_ReadOnly" MaxLength="11"
                                    ReadOnly="True" Width="65px"></asp:TextBox>
                            </td>
                        </tr>
                       
                    </table>
                    <table width="100%">
                        <tr>
                            <td align="center" style="width: 50%;">                                
                                <asp:GridView ID="GV_LineaCredito" runat="server" AutoGenerateColumns="False" Width="269px"
                                    Style="margin-left: 0px; margin-top: 0px;">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Otorgado" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdCuenta" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdMoneda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblMonedaOtorgado" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblOtorgado" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"CargoMaximo","{0:F2}")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Disponible" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblMonedaDisponible" runat="server" Font-Bold="true" ForeColor="Red"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblDisponible" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"Disponible","{0:F2}")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                            <td align="center" style="width: 50%;">
                                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" Width="96px"
                                    Style="margin-left: 0px; margin-top: 0px;">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Ventas-UltimoMes"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdCuentas" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdMonedas" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblMonedaOtorgados" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblVenta_Mes" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Venta_mes","{0:F2}")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Ventas-UltimoA�o"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdCuenta1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdMoneda1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblMonedaOtorgado1" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblVenta_A�os" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Venta_a�o","{0:F2}")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                                <asp:Label ID="lbl_detalles" runat="server" Text="Detalle de Ultimas Ventas" Style="color: #FFFFFF;
                                    font-family: Arial; font-weight: bold; font-size: 15px; background-color: #5D7B9D;"></asp:Label><br />
                                <asp:ImageButton ID="btnVentasMes" runat="server" ImageUrl="~/Imagenes/Ok_b.bmp"
                                    onmouseout="this.src='/Imagenes/Ok_b.bmp';" onmouseover="this.src='/Imagenes/Ok_b.bmp';"
                                    Width="17px" ToolTip="Ventas-Ultimo mes" Style="height: 18px" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                LISTADO DE PRODUCTOS
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Detalle" runat="server" Width="100%" CssClass="PanelCodBarrasBackground">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <table class="TextBox_CodigoBarras" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            &nbsp;<asp:ImageButton ID="btnAgregar" runat="server" ImageUrl="~/Imagenes/BuscarProducto_A.JPG"
                                                OnClientClick="return(valOnClick_btnAgregar());" onmouseout="this.src='/Imagenes/BuscarProducto_A.JPG';"
                                                onmouseover="this.src='/Imagenes/BuscarProducto_A.JPG';" />
                                        </td>
                                        <td runat="server" id="tdSecCodBarras">
                                            <asp:Label ID="lblCodBarras" runat="server" Text="Codigo Barras"></asp:Label>
                                            <asp:TextBox ID="txtCodBarrasPrincipal" runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                onFocus="val_CodBarras('1'); onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                onKeypress="return( valKeyPressDescripcionProdCodBarras(this) );" AutoCompleteType="None"></asp:TextBox>
                                            <asp:Button ID="btnAceptarCodBarras" runat="server" OnClientClick="return(valOnClick_btnAgregarCodBarras());"
                                                Text="Aceptar" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiarDetalleDocumento" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='/Imagenes/Limpiar_B.JPG';" onmouseover="this.src='/Imagenes/Limpiar_A.JPG';" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_GV_Detalle" runat="server" Width="990px" ScrollBars="Horizontal">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" 
                                                    Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                                        </td>
                                                                        <td class="Texto">
                                                                            <asp:LinkButton ID="lkbQuitar" Font-Bold="true" runat="server" OnClick="lkbQuitar_Click">Quitar</asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Producto" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblDescripcion" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomProducto")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddPrecioComercial" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"pvComercial","{0:F2}")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddcodBarraProductoDetDoc" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Prod_CodigoBarras")%>' />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtCantidad" TabIndex="100" onblur=" return(      valPorcentMaximoDctoxArticulo_OnBlur(this)    ); "
                                                                                onKeyPress="return(    validarNumeroPuntoPositivo('event')       );" onFocus="return(   aceptarFoco(this)   );"
                                                                                onKeyup="return(   calcularTotales_GV_Detalle(0)           );" runat="server"
                                                                                Width="60px" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F4}")%>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="cboUM" DataTextField="DescripcionCorto" DataValueField="Id"
                                                                                runat="server" OnSelectedIndexChanged="cboUnidadMedida_GV_Detalle" OnTextChanged="cboUnidadMedida_GV_Detalle" AutoPostBack="true">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="btnAddGlosaDetalle" runat="server" ToolTip="Agregar Glosa al detalle"
                                                                                ImageUrl="~/Imagenes/Glosa_B.jpg" OnClientClick=" return(  onCapaGlosa(this)      );  "
                                                                                onmouseout="this.src='/Imagenes/Glosa_B.jpg';" onmouseover="this.src='/Imagenes/Glosa_A.jpg';" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="btnMostrarComponenteKit" runat="server" ImageUrl="~/Imagenes/search_add.ico"
                                                                                Width="20px" ToolTip="Visualizar Componentes del Kit" OnClick="btnMostrarComponenteKit_Click" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddEquivalencia_UnidadMedida" runat="server" Value="0" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddUnidadMedida_Secundario" runat="server" Value="" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddEquivalencia_Secundario" runat="server" Value="0" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddPeso" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Peso","{0:F4}")%>' />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                       <asp:BoundField DataField="PrecioLista" DataFormatString="{0:F2}" HeaderText="P. Lista"
                                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                         <%-- <asp:TemplateField HeaderText="P. Lista" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>  
                                                                        <asp:Label ID="lblPrecioListaDetalle" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioLista","{0:F2}")%>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="P. Venta" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtPrecioVenta" TabIndex="101" CssClass="TextoRight" onblur=" return(      valPorcentMaximoDctoxArticulo_OnBlur(this)    ); "
                                                                                onKeyPress="return(    validarNumeroPuntoPositivo('event')       );" onFocus="return(   aceptarFoco(this)   );"
                                                                                onKeyup="return(   calcularTotales_GV_Detalle(1,this)           );" runat="server"
                                                                                Width="60px" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioSD","{0:F2}")%>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Desc." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtDescuento_Valor" TabIndex="102" CssClass="TextoRight" onblur=" return(      valPorcentMaximoDctoxArticulo_OnBlur(this)    ); "
                                                                                onKeyPress="return(    validarNumeroPuntoPositivo('event')       );" onFocus="return(   aceptarFoco(this)   );"
                                                                                onKeyup="return(   calcularTotales_GV_Detalle(2)           );" runat="server"
                                                                                Width="50px" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Descuento","{0:F4}")%>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtDctoValor_MaximoxPVenta" onKeypress="return( false );" onFocus="return(  valOnFocus_txtImporte()    );"
                                                                                CssClass="TextBoxReadOnly_Right" runat="server" Width="40px" Font-Bold="true"
                                                                                Text='<%#DataBinder.Eval(Container.DataItem,"DctoValor_MaximoxPVenta","{0:F4}")%>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Desc (%)." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtDescuento_Porcent" TabIndex="103" CssClass="TextoRight" onblur=" return(      valPorcentMaximoDctoxArticulo_OnBlur(this)   ); "
                                                                                onKeyPress="return(    validarNumeroPuntoPositivo('event')       );" onFocus="return(   aceptarFoco(this)   );"
                                                                                onKeyup="return(   calcularTotales_GV_Detalle(3));" runat="server" Width="50px"
                                                                                Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"PorcentDcto","{0:F2}")%>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtDctoPorcent_MaximoxPVenta" onKeypress="return( false );" onFocus="return(  valOnFocus_txtImporte()    );"
                                                                                CssClass="TextBoxReadOnly_Right" runat="server" Width="40px" Font-Bold="true"
                                                                                Text='<%#DataBinder.Eval(Container.DataItem,"DctoPorcent_MaximoxPVenta","{0:F2}")%>'></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="btnDsctoVolVtas" OnClick="btnConsultar_DsctoVolVtas_OnClick"
                                                                                runat="server" ToolTip="Consultar Dscto. Vol. Vtas. " ImageUrl="~/Caja/iconos/edit_text.gif" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddCantMin" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"CantMin","{0:F2}")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddDsctoOriginal" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"DsctoOriginal","{0:F2}")%>' />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="P.V. Final" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtPrecioVentaFinal" TabIndex="104" CssClass="TextBoxReadOnly_Right"
                                                                                onKeypress=" return(  false  ); " onFocus="return(  valOnFocus_txtImporte()    );"
                                                                                runat="server" Width="60px" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCD","{0:F2}")%>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Importe" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="txtImporte" onKeypress=" return(  false  ); " onFocus="return(  valOnFocus_txtImporte()    );"
                                                                                CssClass="TextBoxReadOnly_Right" runat="server" Width="60px" Font-Bold="true"
                                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Importe","{0:F2}")%>'></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="TasaPercepcion" DataFormatString="{0:F2}" HeaderText="Percep. (%)"
                                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="btnPermisoDcto_GV_Detalle" runat="server" ToolTip="Permiso por Descuento"
                                                                                ImageUrl="~/Imagenes/Ok_b.bmp" OnClientClick="return(    valOnClick_btnPermisoDcto_GV_Detalle(this)    );" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddPorcentDctoMaximo" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"PorcentDctoMaximo","{0:F4}")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddIdSupervisor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUsuarioSupervisor")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddGlosa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"DetalleGlosa")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddVolumenVentaMin" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"VolumenVentaMin")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="btnConsultarVolumenVentaMin" OnClick="btnConsultarVolumenVentaMin_Detalle_OnClick"
                                                                                runat="server" ToolTip="Modificar Tipo de Precio de Venta" ImageUrl="~/Imagenes/Ok_b.bmp"
                                                                                OnClientClick="return(    valOnClick_btnConsultarVolumenVentaMin(this)    );" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddPrecioBaseDcto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"PrecioBaseDcto")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="btnEquivalencia_PR" runat="server" ToolTip="Calcular Equivalencia"
                                                                                ImageUrl="~/Imagenes/Ok_b.bmp" OnClientClick="return(    valOnClick_btnEquivalencia_PR(this)    );"
                                                                                OnClick="btnEquivalencia_PR_Click" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:ImageButton ID="btnViewCampania" runat="server" ImageUrl="~/Imagenes/regalo.jpg"
                                                                                Width="20px" ToolTip="Producto en Campa�a" OnClick="valOnClick_btnViewCampania_GV_Detalle" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddHelp" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddIdProductoAux" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProductoAux")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddIdCampania" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCampania")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddCantTrans" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"CantidadDetalleAfecto")%>' />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GV_ComponenteKit" runat="server" AutoGenerateColumns="false" Width="100%">
                                                    <Columns>
                                                        <asp:BoundField DataField="CodigoProd_Comp" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                                            ItemStyle-Font-Bold="true" />
                                                        <asp:BoundField DataField="Componente" HeaderText="Componente" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                                            ItemStyle-Font-Bold="true" />
                                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblCantidad_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad_Comp","{0:F3}")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblUnidadMedida_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida_Comp")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Precio" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblMonedaPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaPrecio_Comp")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Precio_Comp","{0:F3}")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table cellpadding="0" cellspacing="2">
                                    <tr>
                                        <td class="Texto" style="width: 110px">
                                            Peso Total:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPesoTotal" runat="server" Width="90px" onKeyDown="return( false );"
                                                onFocus=" return ( Readonly(this) );" CssClass="TextBox_ReadOnly"></asp:TextBox>
                                            <asp:DropDownList ID="cboPesoTotal" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="text-align: right" class="Texto">
                                            Descuento:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDescuento" runat="server" CssClass="TextBox_ReadOnly" onFocus="return( lostFocus() );"
                                                onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                        </td>
                                        <td style="width: 90px; text-align: right;" class="Texto">
                                            Importe Total:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtImporteTotal" runat="server" CssClass="TextBox_ReadOnly" onFocus="return( lostFocus() );"
                                                onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                        </td>
                                        <td style="width: 85px;" class="Texto">
                                            Conceptos:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTotalConcepto_PanelDetalle" runat="server" CssClass="TextBox_ReadOnly"
                                                onFocus="return( lostFocus() );" onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            Descuento maximo:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDescuentoMax" runat="server" CssClass="TextBox_ReadOnly" onFocus="return( lostFocus() );"
                                                onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            Valor de Venta:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubTotal" runat="server" CssClass="TextBox_ReadOnly" onFocus="return( lostFocus() );"
                                                onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                        </td>
                                        <td style="width: 90px;" class="Texto">
                                            I.G.V.:
                                            <asp:Label ID="lblPorcentIgv" runat="server" Text="0"></asp:Label>
                                            %
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtIGV" runat="server" CssClass="TextBox_ReadOnly" onFocus="return( lostFocus() );"
                                                onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                        </td>
                                        <td style="width: 85px; text-align: right;" class="Texto">
                                            Total:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaTotal" runat="server" CssClass="Label" Text="-"></asp:Label>
                                            <asp:TextBox ID="txtTotal" runat="server" CssClass="TextBox_ReadOnly" onFocus="return( lostFocus() );"
                                                onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="width: 109px; text-align: right;">
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="Texto">
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="Texto" style="width: 90px;">
                                            Percepci�n:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPercepcion" runat="server" CssClass="TextBox_ReadOnly" onFocus="return( lostFocus() );"
                                                onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                        </td>
                                        <td class="Texto" style="width: 85px; text-align: right;">
                                            Total a Pagar:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaTotalPagar" runat="server" CssClass="Label" Text="-"></asp:Label>
                                            <asp:TextBox ID="txtTotalAPagar" runat="server" CssClass="TextBox_ReadOnly" onFocus="return( lostFocus() );"
                                                onKeypress="return( false );" Width="90px">0</asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="TituloCelda" runat="server" id="TrTituloPuntoPartida">
            <td>
                PUNTO DE PARTIDA
            </td>
        </tr>
        <tr runat="server" id="TrDetallePuntoPartida">
            <td>
                <asp:Panel ID="Panel_PuntoPartida" runat="server" Width="100%">
                    <table border="0" cellspacing="0">
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Depto.:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="cboDepto_Partida" runat="server" Width="150px" DataTextField="Nombre"
                                    DataValueField="CodDpto" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label10" runat="server" CssClass="Label" Text="Provincia"></asp:Label>
                                <asp:DropDownList ID="cboProvincia_Partida" runat="server" DataTextField="Nombre"
                                    DataValueField="CodProv" AutoPostBack="True" Width="250px">
                                </asp:DropDownList>
                                <asp:Label ID="Label11" runat="server" CssClass="Label" Text="Distrito"></asp:Label>
                                <asp:DropDownList ID="cboDistrito_Partida" runat="server" Width="250px" DataTextField="Nombre"
                                    DataValueField="CodDist">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="Label12" runat="server" CssClass="Label" Text="Direcci�n"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtDireccion_Partida" runat="server" Width="608px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr runat="server" id="TrTituloPuntoLlegada">
            <td class="TituloCelda">
                PUNTO DE LLEGADA
            </td>
        </tr>
        <tr runat="server" id="TrDetallePuntoLlegada">
            <td>
                <asp:Panel ID="Panel_PuntoLlegada" runat="server" Width="100%">
                    <table border="0" cellspacing="0">
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="Label65" runat="server" CssClass="Label" Text="Depto.:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="cboDepartamento" runat="server" Width="150px" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label66" runat="server" CssClass="Label" Text="Provincia"></asp:Label>
                                <asp:DropDownList ID="cboProvincia" runat="server" AutoPostBack="True" Width="250px">
                                </asp:DropDownList>
                                <asp:Label ID="Label67" runat="server" CssClass="Label" Text="Distrito"></asp:Label>
                                <asp:DropDownList ID="cboDistrito" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="Label68" runat="server" CssClass="Label" Text="Direcci�n"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtDireccion_Llegada" runat="server" Width="608px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr runat="server" id="TrTituloConcepto">
            <td class="TituloCelda">
                CONCEPTOS
            </td>
        </tr>
        <tr runat="server" id="TrDetalleConcepto">
            <td>
                <asp:Panel ID="Panel_Concepto" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Button ID="btnAddDetalleCobro" Width="110px" runat="server" Text="Ingresar Nuevo" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Concepto" runat="server" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" SelectText="Quitar" HeaderStyle-Height="25px"
                                            HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Concepto" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cboConcepto" AutoPostBack="true" OnSelectedIndexChanged="cboConcepto_SelectedIndexChanged"
                                                                runat="server" DataTextField="Nombre" DataValueField="Id" DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaConcepto")%>'>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtDescripcionConcepto" Width="350px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Concepto")%>'
                                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Monto" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblMonedaMontoConcepto" Font-Bold="true" runat="server" ForeColor="Red"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtMonto" onKeypress=" return(    valOnKeyPress_txtMonto(this)    ); "
                                                                onFocus="  return(   aceptarFoco(this)   );  " onblur="return(   valBlur(event)     );"
                                                                onKeyup="  return( calcularTotalConcepto()   ); " Width="100px" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Total Ingresado:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaTotalGasto" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTotalConcepto" CssClass="TextBox_ReadOnly" onFocus="return( lostFocusConcepto() );"
                                                onKeypress="return( false );" Width="90px" runat="server" Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCeldaLeft">
                <asp:Image ID="imgobss" runat="server" Width="15px" />&nbsp;OBSERVACIONES
                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="Panel_Obs"
                    ImageControlID="imgobss" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                    Collapsed="true" CollapseControlID="imgobss" ExpandControlID="imgobss">
                </cc1:CollapsiblePanelExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" Width="100%" runat="server"
                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                    Height="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="14" />
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddIdTipoPVDefault" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddBusquedaMaestro" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdMaestroObra" runat="server" />
                <asp:HiddenField ID="hddIndex_GV_Detalle" runat="server" />
                <asp:HiddenField ID="hddIndex_Campania_Producto" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdMedioPago_Default" runat="server" Value="0" />
                <asp:HiddenField ID="hddTipoImpresion" runat="server" Value="0" />
                <asp:HiddenField ID="hddTipoDiseno" runat="server" Value="0" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hddCodBarras" runat="server" Value="0" />
                <asp:HiddenField ID="hddTipoOperacion" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Existencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                L&iacute;nea:
                            </td>
                            <td>
                                <table cellpadding="0" cellspacing="1">
                                    <tr>
                                        <td>
                                            <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                                DataTextField="Descripcion" DataValueField="Id">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Sub L&iacute;nea:
                                        </td>
                                        <td>
                                            <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" AutoPostBack="true" runat="server"
                                                DataTextField="Nombre" DataValueField="Id">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Descripci&oacute;n:
                            </td>
                            <td>
                                <table cellpadding="0" cellspacing="1">
                                    <tr>
                                        <td>
                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                onKeypress="return( valKeyPressDescripcionProd(this) );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            C�d.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onKeypress="return( valKeyPressDescripcionProd(this) );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" TabIndex="205"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='/Imagenes/Buscar_b.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                onmouseout="this.src='/Imagenes/Agregar_B.JPG';" onmouseover="this.src='/Imagenes/Agregar_A.JPG';"
                                                OnClientClick="return(valAddProductos());" TabIndex="208" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='/Imagenes/Limpiar_B.JPG';" onmouseover="this.src='/Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnVerCampania_Consulta" runat="server" Text="Campa�as" Width="80px"
                                                ToolTip="Ver Campa�as Vigentes" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                &nbsp;
                            </td>
                            <td>
                                <table cellpadding="0" cellspacing="1">
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chb_FiltroProductoCampania" runat="server" Checked="false" CssClass="Texto"
                                                Font-Bold="true" Text="Filtrar Productos en Campa�a." />
                                        </td>
                                        <td class="Label">
                                            &nbsp;Cod. Barras:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCodBarrasCapa" runat="server" onKeypress="return( valKeyPressDescripcionProd(this) );"
                                                Width="90" onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="val_CodBarrasCapa('1'); onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                        </td>
                                        <td class="Label">
                                            Cod. Proveedor:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCodProveedor" runat="server" onKeypress="return( valKeyPressDescripcionProd(this) );"
                                                Width="90" onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" 
                                    AutoGenerateColumns="False" Width="650px">
                                    <Columns>
                                        <asp:CommandField HeaderStyle-Height="25px" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="75px" 
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" SelectText="Quitar" 
                                            ShowSelectButton="True" />
                                        <asp:TemplateField HeaderStyle-Height="25px" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Atributo" 
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomTipoTabla" runat="server" 
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                <asp:HiddenField ID="hddIdTipoTabla" runat="server" 
                                                    Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Valor" 
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboTTV" runat="server" 
                                                    DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' 
                                                    DataTextField="Nombre" DataValueField="IdTipoTablaValor" Width="200px">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="GrillaRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%"
                        PageSize="20">
                        <Columns>
                            <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCantidad_AddProd" TabIndex="210" Width="65px" onKeyup="return(  valStockDisponible_AddProd() );"
                                        onblur="return( valBlurClear('0.00',event) );" onfocus="return(read_Lista(this));"
                                        onKeypress="return( valKeyPressCantidadAddProd()  );" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>'
                                        runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo" />
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" NullDisplayText="---" />
                            <asp:TemplateField HeaderText="Origen">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNoVisible" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NoVisible")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="U.M.">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="cmbUnidadMedida_AddProd" runat="server" OnSelectedIndexChanged="cmbUnidadMedidaVenta_Catalogo_SelectedIndexChanged"
                                                    AutoPostBack="true" TabIndex="211" DataTextField="DescripcionCorto" DataValueField="Id">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NomTipoPV" HeaderText="Tipo P.V." NullDisplayText="---" />
                            <asp:BoundField DataField="SimbMoneda" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"
                                NullDisplayText="---" />
                            <asp:BoundField DataField="PrecioSD" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"
                                HeaderText="P. Venta" NullDisplayText="0" DataFormatString="{0:F2}" />
                            <asp:BoundField DataField="StockDisponibleN" HeaderText="Stock Disponible" NullDisplayText="0"
                                DataFormatString="{0:F4}" />
                            <asp:BoundField DataField="Percepcion" HeaderText="Percepci�n (%)" NullDisplayText="0"
                                DataFormatString="{0:F2}" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="btnConsultarStockAlmacenes_BuscarProd" runat="server" OnClick="mostrarCapaStockPrecioxProducto"
                                                    ImageUrl="~/Imagenes/Ok_b.bmp" ToolTip="Consultar Stock y Precios por Tienda / Almac�n." />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="btnConsultarTipoPrecio" runat="server" OnClick="mostrarCapaConsultarTipoPrecio"
                                                    ImageUrl="~/Imagenes/Ok_b.bmp" ToolTip="Consultar Tipo Precios de Venta." />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddPrecioComercial" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"pvComercial","{0:F2}")%>' />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="btnMostrarComponenteKit_Find" runat="server" ImageUrl="~/Imagenes/search_add.ico"
                                                    Width="20px" ToolTip="Visualizar Componentes del Kit" OnClick="btnMostrarComponenteKit_Find_Click" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="btnViewCampania" runat="server" ImageUrl="~/Imagenes/regalo.jpg"
                                                    Width="20px" ToolTip="Producto en Campa�a" OnClick="valOnClick_btnViewCampania" />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddCodBarraProdCapa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Prod_CodigoBarras")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ComponenteKit_Find" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="CodigoProd_Comp" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                ItemStyle-Font-Bold="true" />
                            <asp:BoundField DataField="Componente" HeaderText="Componente" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                ItemStyle-Font-Bold="true" />
                            <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCantidad_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad_Comp","{0:F3}")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUnidadMedida_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida_Comp")%>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Precio" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMonedaPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaPrecio_Comp")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Precio_Comp","{0:F3}")%>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaVentaMes" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 237px; left: 30px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="ImageButton9" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                    OnClientClick="return(  offCapa('capaVentaMes')   );" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <h3>
                        Ultimas Ventas Realizadas</h3>
                    <asp:GridView ID="GV_VentaMes" runat="server" AutoGenerateColumns="False" Width="900px"
                        Style="margin-left: 0px; margin-top: 0px;">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Documento" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdCuentaventames" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdMonedaventames" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaOtorgadoventames" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbltdoc_NombreCorto" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"tdoc_NombreCorto","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Numero" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdCuentaventamesultima" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdMonedaventamesultima" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaOtorgadoventamesultima" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNumeroultima" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Numero","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="FechaEmision" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdCuentaventamesultima1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdMonedaventamesultima1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaOtorgadoventamesultima1" runat="server" Font-Bold="true"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbltdoc_FechaEmisionultima" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"doc_FechaEmision","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Producto" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblprod_Nombre" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"prod_Nombre","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="UnidadMedida"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdCuenta2ventasmes3ultima" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdMoneda2ventasmes3ultima" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaOtorgado2ventasmes3ultima" runat="server" Font-Bold="true"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbldc_UMedidaultima" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"dc_UMedida","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdCuentasventasmes" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdMonedasventasmes" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaOtorgadosventasmes" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbldc_Cantidad" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"dc_Cantidad","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Precio" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdCuenta1ventasmes2" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdMoneda1ventasmes2" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaOtorgado1ventasmes2" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbldc_PrecioCD" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"dc_PrecioCD","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Total" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdCuenta2ventasmes3" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdMoneda2ventasmes3" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaOtorgado2ventasmes3" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbltotal" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"total","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="TotalaPagar"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdCuenta2ventasmes3totalapagar" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdMoneda2ventasmes3totalapagar" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaOtorgado2ventasmes3totalapagar" runat="server" Font-Bold="true"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbltotalapagar" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"totalapagar","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                
                
                
                
                
                
                
                </td>
            </tr>
        </table>
    </div>
    <div id="capaVentaA�o" style="border: 3px solid blue; padding: 8px; width: 817px;
        height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="ImageButton10" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                    OnClientClick="return(  offCapa('capaVentaA�o')   );" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <h3>
                        Ventas realizadas en el ultimo a�o</h3>
                    <asp:GridView ID="GV_VentaA�o" runat="server" AutoGenerateColumns="False" Width="431px"
                        Style="margin-left: 0px; margin-top: 0px;">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Documento" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdCuentaventaa�o" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdMoneda7ventaa�o" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaOtorgado7ventaa�o" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbltdoc_NombreCortoventaa�o" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"tdoc_NombreCorto","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Producto" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMonedaDisponible7ventaa�o" runat="server" Font-Bold="true" ForeColor="Red"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblprod_Nombreventaa�o" runat="server" Font-Bold="true" ForeColor="Red"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"prod_Nombre","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdCuentasventaa�os30" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdMonedasventaa�os30" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaOtorgadosventaa�os30" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbldc_Cantidadventasa�os30" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"dc_Cantidad","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Precio" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdCuentaventas�os31" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdMonedaventas�os31" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaOtorgadoventas�os31" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbldc_PrecioCDventasa�os31" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"dc_PrecioCD","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="Total" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdCuentaventasa�os32" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdMonedaventasa�os32" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaOtorgadoventasa�os32" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbltotala�oventasa�os32" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"total","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaConsultarStockPrecioxProducto" style="border: 3px solid blue; padding: 8px;
        width: 800px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa2('capaConsultarStockPrecioxProducto')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProductoConsultarStockPrecioxProd" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIdProductoStockPrecio" runat="server" />
                                <asp:HiddenField ID="hddDescripcionFlete" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Tipo Almac&eacute;n:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTipoAlmacen" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ConsultarStockPrecioxProd" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Tienda" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdTienda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTienda")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTienda" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Almac�n" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAlmacen" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Almacen")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Precio Lista" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMoneda" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SimbMoneda")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPrecioLista" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioLista","{0:F3}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Disponible" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblStock" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StockDisponibleN","{0:F3}")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUM" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cadenaUM")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCantidad" onKeyUp="return ( onKeyUp_PrecioStockCantidad('1') );"
                                        onKeyPress="return(    validarNumeroPuntoPositivo('event')       );" onFocus="return(   aceptarFoco(this)   );"
                                        onBlur=" return ( onBlur_PrecioStock(this) );" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F4}")%>'
                                        runat="server" Width="60px" Font-Bold="true"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Flete 1" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblFlete1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Flete1","{0:F2}")%>'
                                        ForeColor="Red" Font-Bold="true"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Flete 2" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtFlete2" Text='<%#DataBinder.Eval(Container.DataItem,"Flete2","{0:F2}")%>'
                                        runat="server" onKeyPress="return(    validarNumeroPuntoPositivo('event')       );"
                                        onBlur=" return ( onBlur_PrecioStock(this) );" onKeyUp="return ( onKeyUp_PrecioStockCantidad('2') );"
                                        onFocus="return(   aceptarFoco(this)   );" Width="60px" Font-Bold="true"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock-Tr�nsito">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblStockEnTransito" runat="server" ForeColor="Red" Font-Bold="true"
                                                    Text='<%# DataBinder.Eval(Container.DataItem,"StockEnTransito","{0:F3}") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUMStockEnTransito" ForeColor="Red" Font-Bold="true" runat="server"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"cadenaUM")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <table>
                        <tr>
                            <td class="Texto">
                                Total Stock:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStockTotal" runat="server" Width="70px" onFocus="return(onFocus_ReadOnly(this));"
                                    CssClass="TextBoxReadOnly"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Total Stock Transito:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStockTransitoTotal" runat="server" Width="70px" onFocus="return(onFocus_ReadOnly(this));"
                                    CssClass="TextBoxReadOnly"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td class="Texto">
                                Cantidad:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStockPrecioCant" runat="server" Width="70px" onFocus="return(onFocus_ReadOnly(this));"
                                    CssClass="TextBoxReadOnly_Right"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Total Flete:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStockPrecioTotal" runat="server" Width="70px" onFocus="return(onFocus_ReadOnly(this));"
                                    CssClass="TextBoxReadOnly_Right"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnAddProducto_ConsultarStockPrecio" runat="server" Text="Aceptar"
                        Width="90px" OnClientClick="return(  onClick_AddProducto_ConsultarStockPrecio()  );" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaConsultarTipoPrecio" style="border: 3px solid blue; padding: 8px; width: 500px;
        height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton4" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa2('capaConsultarTipoPrecio')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold" align="right">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProducto_ConsultarTipoPrecio" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold" align="right">
                                Tienda:
                            </td>
                            <td>
                                <asp:Label ID="lblTienda_ConsultarTipoPrecio" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ConsultarTipoPrecio" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="NomTipoPV" HeaderText="Tipo Precio" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="cadenaUM" HeaderText="U. M." HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Precio Lista" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMoneda" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SimbMoneda")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPrecioLista" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioLista","{0:F2}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnAceptar_ConsultarTipoPrecio" runat="server" Text="Aceptar" Width="90px"
                        OnClientClick="return(  offCapa2('capaConsultarTipoPrecio')   );" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaAddPermiso_Detalle" style="border: 3px solid blue; padding: 8px; width: 500px;
        height: auto; position: absolute; top: 356px; left: 169px; background-color: white;
        z-index: 3; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton3" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaAddPermiso_Detalle')   );" />
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    Descuento Adicional
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProducto_AddDctoAdicional" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Usuario:
                            </td>
                            <td>
                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                    ID="txtLogin_AddDctoAdicional" Width="150px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Contrase�a:
                            </td>
                            <td>
                                <asp:TextBox ID="txtClave_AddDctoAdicional" Width="150px" runat="server" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Confirm. Contrase�a:
                            </td>
                            <td>
                                <asp:TextBox ID="txtClaveConfirmar_AddDctoAdicional" Width="150px" runat="server"
                                    TextMode="Password"> </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                % Descuento Base::
                            </td>
                            <td>
                                <asp:TextBox ID="txtDescuentoBase" Width="50px" CssClass="TextBoxReadOnly_Right"
                                    Font-Bold="true" onKeyPress="return(    validarNumeroPuntoPositivo('event')       );"
                                    onFocus="return(   aceptarFoco(this) );" onblur="return(   valBlur(event)     );"
                                    runat="server">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Descuento Adicional:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDescuentoAdicional" Width="50px" Font-Bold="true" onKeyPress="return(    validarNumeroPuntoPositivo('event')       );"
                                    onFocus="return(   aceptarFoco(this) );" onblur="return(   valBlur(event)     );"
                                    runat="server">0</asp:TextBox>
                            </td>
                              <td>
                              <asp:CheckBox ID="Chkmasivo" runat="server" /> Descuento Masivo
                    <%--         <label>
                            <input type="checkbox" name="checkbox" value="checkbox" />
                            </label>
                             Descuento Masivo--%>
                             </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnAceptar_AddDctoAdicional" OnClientClick="return(  valOnClick_btnAceptar_AddDctoAdicional()   );"
                                    runat="server" Text="Aceptar" Width="80px" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancelar_AddDctoAdicional" runat="server" Text="Cerrar" Width="80px"
                                    OnClientClick="return(    offCapa('capaAddPermiso_Detalle')       );" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaCambiarTipoPrecioPV" style="border: 3px solid blue; padding: 8px; width: 500px;
        height: auto; position: absolute; top: 356px; left: 169px; background-color: white;
        z-index: 3; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton5" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaCambiarTipoPrecioPV')   );" />
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    Cambiar Tipo de Precio de Venta
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProducto_CambiarTipoPV_GV_Detalle" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIndexCambiarTipoPV_GV_Detalle" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold">
                                            Tipo Precio Venta:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoPrecioPV_CambiarDetalle" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCambiarTipoPrecioVenta_Detalle" runat="server" Text="Cambiar"
                                                Width="85px" OnClientClick=" return(  valOnClick_btnCambiarTipoPrecioVenta_Detalle()  ); " />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnConsultarTipoPrecio_CapaCambiarTipoPV" runat="server" OnClick="mostrarCapaConsultarTipoPrecio_CapaCambiarTipoPV"
                                                ImageUrl="~/Imagenes/Ayuda_A.JPG" ToolTip="Consultar Precios de Lista" />
                                        </td>
                                    </tr>
                                </table>
                    </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_VolumenVentaMin" runat="server" AutoGenerateColumns="false"
                                    Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="TipoPV" HeaderText="Tipo Precio Venta" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Volumen M�nimo Venta" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblVolumenVentaMin" Font-Bold="true" ForeColor="Red" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"vven_CantidadMin_UMPr","{0:F2}")%>'></asp:Label>
                                                        </td>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblUM_VolumenVentaMin" Font-Bold="true" ForeColor="Red" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"um_NombreCorto")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Requisito Cliente" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblTipoPrecioVenta_Requisito" Font-Bold="true" ForeColor="Red" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"TipoPrecioV_Ref")%>'></asp:Label>
                                                        </td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                            Width="450px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    
    
     <div id="IngresoMaestroObra" style="border: 3px solid blue; padding: 4px; width: 850px;
        height: auto; position: absolute; top: 302px; left: 35px; background-color: white;
        z-index: 3; display: none;">
        <table width="100%" cellpadding="0">
            <tr>
                <td class="Texto">
                    Tipo Persona:
                </td>
                <td>
                 
                  
                    <asp:DropDownList onchange="return(activarPanelMaestroObra());" ID="cboTipoPersona_MaestroObra"
                    
                  
                        runat="server">
                        <asp:ListItem Value="N" Selected="True">Natural</asp:ListItem>
                        <asp:ListItem Value="J">Jur�dica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Tipo Agente:
                </td>
                <td>
                    <asp:DropDownList ID="cmbTipoAgente_MaestroObra" runat="server" >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Raz&oacute;n Social:
                </td>
                <td>
                    <asp:TextBox ID="txtRazonSocial_MaestroObra" Width="653px" runat="server"  MaxLength="100"
                        Enabled="false" onKeypress="return( valKeyPressSaveMaestroObra(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Ap. Paterno:
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtApPaterno_MaestroObra" runat="server" Width="282px" MaxLength="25"
                                    onKeypress="return( valKeyPressSaveMaestroObra(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp;Ap. Materno:
                            </td>
                            <td>
                                <asp:TextBox ID="txtApMaterno_MaestroObra" runat="server" Width="282px" MaxLength="25"
                                    onKeypress="return( valKeyPressSaveMaestroObra(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Nombres:
                </td>
                <td>
                    <asp:TextBox ID="txtNombres_MaestroObra" runat="server" Width="282px" MaxLength="25"
                        onKeypress="return( valKeyPressSaveMaestroObra(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    D.N.I.:
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDNI_MaestroObra" runat="server" MaxLength="8" onKeypress="return( onKeyPressEsNumero('event')  );"
                                    onFocus="return( aceptarFoco(this)  );"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp;R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC_MaestroObra" onKeypress="return( onKeyPressEsNumero('event')  );" onFocus="return( aceptarFoco(this)  );"
                                    runat="server" MaxLength="11"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Direcci&oacute;n:
                </td>
                <td>
                    <asp:TextBox ID="txtDireccion_MaestroObra" runat="server" MaxLength="120" Width="653px"
                        onKeypress="return( valKeyPressSaveMaestroObra(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="return ( onFocusTextTransform(this,configurarDatos) );aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Depto.:
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:DropDownList ID="cboMODepartamentoN" runat="server" Width="150px" DataTextField="Nombre"
                                    DataValueField="CodDpto" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Provincia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboMOProvinciaN" runat="server" Width="250px" DataTextField="Nombre"
                                    DataValueField="CodProv" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Distrito:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboMODistritoN" runat="server" DataTextField="Nombre" DataValueField="CodDist"
                                    Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Tel&oacute;fono:
                </td>
                <td>
                    <asp:TextBox ID="txtTelefono_MaestroObra" runat="server" MaxLength="20" Width="147px"
                        onFocus="return( aceptarFoco(this)  );"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    E-Mail:
                </td>
                <td>
                    <asp:TextBox ID="txtMail_MaestroObra" runat="server" Width="282px" MaxLength="50" onKeypress="return( valKeyPressSaveMaestroObra(this)  );"
                        onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td class="Texto" style="width: 120px;">
                    Fecha Nacimiento:
                </td>
                <td>
                    <asp:TextBox ID="txtFechaNac" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                        Width="100px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender_txtFechaNac" runat="server" Enabled="True"
                        Format="dd/MM/yyyy" TargetControlID="txtFechaNac">
                    </cc1:CalendarExtender>
                </td>
            
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:ImageButton ID="btnGuardarMaestroObra" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                        OnClientClick="return(validarSaveMaestroObra());" onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                        onmouseout="this.src='/Imagenes/Guardar_B.JPG';" CausesValidation="true" />
                    <asp:ImageButton ID="ImageButton12" runat="server" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('IngresoMaestroObra'));" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaEditarMaestroObra" style="border: 3px solid blue; padding: 10px; width: 850px;
        height: auto; position: absolute; top: 302px; left: 35px; background-color: white;
        z-index: 3; display: none;">
        <table width="100%" cellpadding="0">
            <tr>
                <td class="Texto">
                   
                </td>
                <td>
                                          
                    <asp:DropDownList ID="cboTipoAgente_EditarMO" visible="false" runat="server" onClick="return(  valOnClickTipoAgente_EditarMO()  );">
                    </asp:DropDownList>
                    
                </td>
            </tr>
            <tr>
                <td class="Texto">
                   
                </td>
                <td>
                    <asp:TextBox ID="txtRazonSocial_EditarMO" Width="653px" Visible="false" runat="server" MaxLength="100"
                        onKeypress="return(  valOnKeyPress_EditarMO('2',this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Ap. Paterno:
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtApPaterno_EditarMO" runat="server" onKeypress="return(  valOnKeyPress_EditarMO('1',this)  );"
                                    Width="282px" MaxLength="25" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp;Ap. Materno:
                            </td>
                            <td>
                                <asp:TextBox ID="txtApMaterno_EditarMO" runat="server" onKeypress="return(  valOnKeyPress_EditarMO('1',this)  );"
                                    Width="282px" MaxLength="25" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Nombres:
                </td>
                <td>
                    <asp:TextBox ID="txtNombres_EditarMO" runat="server" Width="282px" onKeypress="return(  valOnKeyPress_EditarMO('1',this)  );"
                        MaxLength="25" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    D.N.I.:
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDNI_EditarMO" runat="server" MaxLength="8" onFocus="return( aceptarFoco(this)  );"
                                    onKeypress="return(  valOnKeyPressDNI_EditarMO()  );"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp;R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC_EditarMO" onFocus="return( aceptarFoco(this)  );" runat="server"
                                    MaxLength="11" onKeypress="return( valOnKeyPressRUC_EditarMO() );"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Direcci&oacute;n:
                </td>
                <td>
                    <asp:TextBox ID="txtDireccion_EditarMO" runat="server" MaxLength="120" Width="653px"
                        onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Depto.:
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:DropDownList ID="cboMODepartamentoE" runat="server" Width="150px" DataTextField="Nombre"
                                    DataValueField="CodDpto" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Provincia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboMOProvinciaE" runat="server" Width="250px" DataTextField="Nombre"
                                    DataValueField="CodProv" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Distrito:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboMODistritoE" runat="server" Width="250px" DataTextField="Nombre" DataValueField="CodDist">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        
                        
                    </table>
                    
                    
                </td>
            </tr>
            
            <tr>
                <td class="Texto">
                    Tel&oacute;fono:
                </td>
                <td>
                    <asp:TextBox ID="txtTelefono_EditarMO" runat="server" MaxLength="20" Width="147px"
                        onFocus="return( aceptarFoco(this)  );"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    E-Mail:
                </td>
                <td>
                    <asp:TextBox ID="txtemail_EditarMO" runat="server" Width="282px" MaxLength="50" onKeypress="return(valKeyPressSaveMaestroObra(this)  );"
                        onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:ImageButton ID="btnGuardar_EditarMO" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                        OnClientClick="return(  valOnClickGuardar_EditarMO()    );" onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                        onmouseout="this.src='/Imagenes/Guardar_B.JPG';" CausesValidation="true" />
                    <asp:ImageButton ID="btnCerrar_CapaEditarMO" runat="server" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaEditarMaestroObra'));" />
                </td>
            </tr>
        </table>
    </div>
    
    <div id="capaRegistrarCliente" style="border: 3px solid blue; padding: 4px; width: 850px;
        height: auto; position: absolute; top: 302px; left: 35px; background-color: white;
        z-index: 3; display: none;">
        <table width="100%" cellpadding="0">
            <tr>
                <td class="Texto">
                    Tipo Persona:
                </td>
                <td>
                    <asp:DropDownList onchange="return(activarPanel());" ID="cboTipoPersona_RegistrarCliente"
                        runat="server">
                        <asp:ListItem Value="N" Selected="True">Natural</asp:ListItem>
                        <asp:ListItem Value="J">Jur�dica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Tipo Agente:
                </td>
                <td>
                
                    <asp:DropDownList ID="cmbTipoAgente_RegistrarCliente" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Raz&oacute;n Social:
                </td>
                <td>
                    <asp:TextBox ID="txtRazonSocial_Cliente" Width="653px" runat="server" MaxLength="100"
                        Enabled="false" onKeypress="return( valKeyPressSaveCliente(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Ap. Paterno:
                </td>
                <td>
                
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtApPaterno_Cliente" runat="server" Width="282px" MaxLength="25"
                                    onKeypress="return( valKeyPressSaveCliente(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp;Ap. Materno:
                            </td>
                            <td>
                                <asp:TextBox ID="txtApMaterno_Cliente" runat="server" Width="282px" MaxLength="25"
                                    onKeypress="return( valKeyPressSaveCliente(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Nombres:
                </td>
                <td>
                    <asp:TextBox ID="txtNombres_Cliente" runat="server" Width="282px" MaxLength="25"
                        onKeypress="return( valKeyPressSaveCliente(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    D.N.I.:
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDNI_Cliente" runat="server" MaxLength="8" onKeypress="return( onKeyPressEsNumero('event')  );"
                                    onFocus="return( aceptarFoco(this)  );"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp;R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC_Cliente" onKeypress="return( onKeyPressEsNumero('event')  );" onFocus="return( aceptarFoco(this)  );"
                                    runat="server" MaxLength="11"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Direcci&oacute;n:
                </td>
                <td>
                    <asp:TextBox ID="txtDireccion_Cliente" runat="server" MaxLength="120" Width="653px"
                        onKeypress="return( valKeyPressSaveCliente(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="return ( onFocusTextTransform(this,configurarDatos) );aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Depto.:
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:DropDownList ID="cboCliDepartamentoN" runat="server" Width="150px" DataTextField="Nombre"
                                    DataValueField="CodDpto" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Provincia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboCliProvinciaN" runat="server" Width="250px" DataTextField="Nombre"
                                    DataValueField="CodProv" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Distrito:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboCliDistritoN" runat="server" DataTextField="Nombre" DataValueField="CodDist"
                                    Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Tel&oacute;fono:
                </td>
                <td>
                    <asp:TextBox ID="txtTelefono_Cliente" runat="server" MaxLength="20" Width="147px"
                        onFocus="return( aceptarFoco(this)  );"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    E-Mail:
                </td>
                <td>
                    <asp:TextBox ID="txtMail_Cliente" runat="server" Width="282px" MaxLength="50" onKeypress="return( valKeyPressSaveCliente(this)  );"
                        onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
           
            <tr>
                <td align="center" colspan="2">
                    <asp:ImageButton ID="btnGuardarCliente" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                        OnClientClick="return(validarSaveCliente());" onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                        onmouseout="this.src='/Imagenes/Guardar_B.JPG';" CausesValidation="true" />
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaRegistrarCliente'));" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaEditarPersona" style="border: 3px solid blue; padding: 10px; width: 850px;
        height: auto; position: absolute; top: 302px; left: 35px; background-color: white;
        z-index: 3; display: none;">
        <table width="100%" cellpadding="0">
            <tr>
                <td class="Texto">
                    Tipo Agente:
                </td>
                <td>
                    <asp:DropDownList ID="cboTipoAgente_EditarCliente" runat="server" onClick="return(  valOnClickTipoAgente_EditarCliente()  );">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Raz�n Social:
                </td>
                <td>
                    <asp:TextBox ID="txtRazonSocial_EditarCliente" Width="653px" runat="server" MaxLength="100"
                        onKeypress="return(  valOnKeyPress_EditarCliente('2',this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Ap. Paterno:
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtApPaterno_EditarCliente" runat="server" onKeypress="return(  valOnKeyPress_EditarCliente('1',this)  );"
                                    Width="282px" MaxLength="25" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp;Ap. Materno:
                            </td>
                            <td>
                                <asp:TextBox ID="txtApMaterno_EditarCliente" runat="server" onKeypress="return(  valOnKeyPress_EditarCliente('1',this)  );"
                                    Width="282px" MaxLength="25" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Nombres:
                </td>
                <td>
                    <asp:TextBox ID="txtNombres_EditarCliente" runat="server" Width="282px" onKeypress="return(  valOnKeyPress_EditarCliente('1',this)  );"
                        MaxLength="25" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    D.N.I.:
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDNI_EditarCliente" runat="server" MaxLength="8" onFocus="return( aceptarFoco(this)  );"
                                    onKeypress="return(  valOnKeyPressDNI_EditarCliente()  );"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp;R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC_EditarCliente" onFocus="return( aceptarFoco(this)  );" runat="server"
                                    MaxLength="11" onKeypress="return( valOnKeyPressRUC_EditarCliente() );"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Direcci&oacute;n:
                </td>
                <td>
                    <asp:TextBox ID="txtDireccion_EditarCliente" runat="server" MaxLength="120" Width="653px"
                        onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Depto.:
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:DropDownList ID="cboCliDepartamentoE" runat="server" Width="150px" DataTextField="Nombre"
                                    DataValueField="CodDpto" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Provincia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboCliProvinciaE" runat="server" Width="250px" DataTextField="Nombre"
                                    DataValueField="CodProv" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Distrito:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboCliDistritoE" runat="server" Width="250px" DataTextField="Nombre"
                                    DataValueField="CodDist">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                            <td class="Texto">
                                Tel&oacute;fono:
                            </td>
                            <td>
                                <asp:TextBox ID="txtfonoE" runat="server" MaxLength="20" Width="147px"
                                                onFocus="return( aceptarFoco(this)  );"></asp:TextBox>
                            </td>
                        </tr>
                           <tr>
                                <td class="Texto">
                                    E-Mail:
                                </td>
                            <td>
                                    <asp:TextBox ID="txtmailE" runat="server" Width="282px" MaxLength="50" onKeypress="return(valKeyPressSaveMaestroObra(this)  );"
                                        onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                </td>
                        </tr>
            
            <tr>
                <td colspan="2" align="center">
                    <asp:ImageButton ID="btnGuardar_EditarCliente" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                        OnClientClick="return(  valOnClickGuardar_EditarCliente()    );" onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                        onmouseout="this.src='/Imagenes/Guardar_B.JPG';" CausesValidation="true" />
                    <asp:ImageButton ID="btnCerrar_CapaEditarCliente" runat="server" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaEditarPersona'));" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaGlosa" style="border: 3px solid blue; padding: 8px; width: auto; height: auto;
        position: absolute; top: 300px; left: 250px; background-color: white; z-index: 2;
        display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton7" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaGlosa'));" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table>
                        <tr>
                            <td class="Texto">
                                Glosa:
                            </td>
                            <td>
                                <asp:TextBox ID="txtGlosa_CapaGlosa" runat="server" Width="350px" TextMode="MultiLine"
                                    Height="50px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIndexGlosa_Gv_Detalle" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:ImageButton ID="btnAddGlosa_CapaGlosa" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                        onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" OnClientClick="return(     valOnClickAddGlosa()       );"
                        onmouseover="this.src='/Imagenes/Aceptar_B.JPG';" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de b�squeda realiza un filtrado de acuerdo al cliente seleccionado.
                </td>
            </tr>
        </table>
    </div>
    <div id="capaEquivalenciaProducto" style="border: 3px solid blue; padding: 8px; width: 450px;
        height: auto; position: absolute; top: 237px; left: 250px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaEquivalenciaProducto')   );" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Ingrese Cantidad:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCantidad_Ingreso" onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); "
                                                onFocus=" return(  aceptarFoco(this)  ); " Width="100px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboUnidadMedida_Ingreso" runat="server" DataTextField="NombreCortoUM"
                                                 DataValueField="IdUnidadMedida">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCalcular" OnClientClick=" return(  valOnClick_btnCalcular()   ); "
                                                runat="server" Text="Calcular" ToolTip="Calcular equivalencias" Width="80px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            Resultado:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCantidad_Salida" Font-Bold="true" CssClass="TextBox_ReadOnlyLeft"
                                                Text="" ReadOnly="true" Width="100px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboUnidadMedida_Salida" runat="server" DataTextField="NombreCortoUM"
                                              DataValueField="IdUnidadMedida">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GV_CalcularEquivalencia" runat="server" AutoGenerateColumns="False"
                                    Width="400px">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chb_UnidadMedida" runat="server" Checked="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unidad Medida" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblUnidadMedida" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdUnidadMedida" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblUnidadMedidaAbv" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NombreCortoUM")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GV_ResuldoEQ" runat="server" AutoGenerateColumns="False" Width="400px">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="UnidadMedida" HeaderText=""
                                            ItemStyle-Height="25px" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="UnidadMedidaAbv" HeaderText="U.M."
                                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="Cantidad" HeaderText="Cantidad"
                                            DataFormatString="{0:F4}" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:RadioButtonList ID="rbl_UtilizarRedondeo" runat="server" RepeatDirection="Horizontal"
                                    CssClass="Label">
                                    <asp:ListItem Selected="True" Value="0">No Utilizar Redondeo</asp:ListItem>
                                    <asp:ListItem Value="1">Redondeo [ arriba ]</asp:ListItem>
                                    <asp:ListItem Value="2">Redondeo [ abajo ]</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnAceptar_EquivalenciaPR" runat="server" Text="Aceptar" Width="80px"
                                    ToolTip="Aceptar Equivalencia" OnClientClick="  return(  offCapa('capaEquivalenciaProducto') ); "/>
                                    <%-- OnClientClick=" return(  valOnClick_btnAceptar_EquivalenciaPR()  );  "--%>
                                    
                            </td>
                            
                            <td>
                                <asp:Button ID="btnCerrar_EquivalenciaPR" runat="server" Text="Cerra" Width="80px"
                                    ToolTip="Cerrar" OnClientClick="  return(  offCapa('capaEquivalenciaProducto') ); " />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaCampania_Producto" style="border: 3px solid blue; padding: 8px; width: 800px;
        height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton14" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa2('capaCampania_Producto')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblCodigoProducto_capaCampania_Producto" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                            <td style="width: 15px">
                            </td>
                            <td>
                                <asp:Label ID="lblProducto_capaCampania_Producto" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_Campania_Producto" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="Campa�a" DataField="Campania" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Inicio" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFechaInicio" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FechaInicio","{0:dd/MM/yyyy}")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdCampania" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCampania")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdUnidadMedida" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fin" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFechaFin" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FechaFin","{0:dd/MM/yyyy}")%>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="U.M." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblUnidadMedida" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida")%>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Precio" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMoneda" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPrecioCampania" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCampania","{0:F3}")%>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cantidad M�n." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCantidadMin" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CantidadMin","{0:F3}")%>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnCerrar_capaCampania_Producto" runat="server" Text="Cerrar" Width="70px"
                        OnClientClick="return(  offCapa2('capaCampania_Producto')   );" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaConsultar_Campania" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_capaConsultar_Campania" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImageButton15" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(  offCapa2('capaConsultar_Campania')   );" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_Campania_Cab" runat="server" AutoGenerateColumns="false" Width="100%">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Campa�a" DataField="cam_Descripcion" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="Inicio" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblFechaInicio" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cam_FechaInicio","{0:dd/MM/yyyy}")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hddIdCampania" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCampania")%>' />
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fin" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblFechaFin" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cam_FechaFin","{0:dd/MM/yyyy}")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Empresa" DataField="per_NComercial" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Tienda" DataField="tie_Nombre" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_Campania_Det" runat="server" AutoGenerateColumns="false" Width="100%"
                                AllowPaging="true" PageSize="20">
                                <Columns>
                                    <asp:BoundField HeaderText="C�digo" DataField="CodigoProducto" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Producto" DataField="Producto" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="U.M." DataField="UnidadMedida" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Cantidad M�n." DataFormatString="{0:F3}" DataField="CantidadMin"
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="Precio" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMoneda" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda_Campania")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblPrecio" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Precio","{0:F3}")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnCerrar_capaConsultar_Campania" runat="server" Text="Cerrar" Width="70px"
                                OnClientClick="return(  offCapa2('capaConsultar_Campania')   );" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton8" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoDocumento" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisi�n</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Vcto."
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Importe" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblImporteMoneda" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblImporte" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                            DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        function valKeyPressCodigoSL() {
            if (event.keyCode == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            } val_CodBarrasCapa
            return true;
        }
        function valKeyPressDescripcionProd(obj) {
            if (event.keyCode == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }



        function valKeyPressDescripcionProdCodBarras(obj) {
            ejecucion = '0';
            var codBarras = document.getElementById('<%=txtCodBarrasPrincipal.ClientID%>');

            if (event.keyCode == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                if (codBarras.value.length <= 0) {
                    alert('no se se ingreso Codigo de Barras');
                    return false;
                } else {
                    document.getElementById('<%=btnAceptarCodBarras.ClientID %>').click();
                    return false;
                }
            }
        }

        function valKeyPressDescripcionProdCodBarrasCapa(obj) {
            ejecucion = '0';

            var codBarras = document.getElementById('<%=txtCodBarrasPrincipal.ClientID%>');

            if (event.keyCode == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                if (codBarras.value.length <= 0) {
                    alert('no se se ingreso Codigo de Barras');
                    return false;
                } else {
                    document.getElementById('<%=btnAceptarCodBarras.ClientID %>').click();
                    return false;
                }
            }
        }
        function valOnClick_btnAgregarCodBarras() {
            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            var txtIdCliente = document.getElementById('<%=txtCodigoCliente.ClientID%>');
            if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            var codBarrasPrincipal = document.getElementById('<%=txtCodBarrasPrincipal.ClientID%>');

            if (codBarrasPrincipal.value.length <= 0) {
                alert('no ingreso en codigo de barras');
                return false;
            } else {
                document.getElementById('<%=btnAceptarCodBarras.ClientID%>').focus();
                return true;
            }
        }



        function valKeyPressDescripcionProdCodBarrasCapa(obj) {
            var codBarras = document.getElementById('<%=txtCodBarrasCapa.ClientID%>');

            if (event.keyCode == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                if (codBarras.value.length <= 0) {
                    alert('no se se ingreso Codigo de Barras');
                    return false;
                } else {
                    document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
                    return true;
                }
            }
        }


        function CursorCodBarras() {
            document.getElementById('<%=txtCodBarrasPrincipal.ClientID%>').focus();
            document.getElementById('<%=txtCodBarrasPrincipal.ClientID%>').select();
            return false;
        }

        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valAddProductos() {

            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            var txtIdCliente = document.getElementById('<%=txtCodigoCliente.ClientID%>');
            if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }

            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('No se seleccionaron productos.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                var txtCantidad = rowElem.cells[0].children[0];
                if (parseFloat(txtCantidad.value) > 0) {
                    // if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
                    //                        alert('La cantidad ingresada excede a la cantidad disponible.');
                    //                        txtCantidad.select();
                    //                        txtCantidad.focus();
                    //     return false;
                    //  } else if (parseFloat(txtCantidad.value) > 0) {
                    cont = cont + 1;
                    //  }
                }
            }

            if (cont == 0) {
                alert('No se seleccionaron productos.');
                return false;
            }

            /*
            //************* Validamos que no agregue de otro almacen
            
            if (grillaDetalle != null) {

                if (grillaDetalle.rows.length > 1) {

                    //************ valido el almacen
                    
            var hddIdAlmacen = 
                    

                    if (parseFloat(cboAlmacen.value) != parseFloat(hddIdAlmacen.value)) {
                        
            return false;
            }
            }
            }
            */
            return true;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO

            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }
        function valStockDisponible_AddProd() {
            /*var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
            if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].id == event.srcElement.id) {
            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
            alert('La cantidad ingresada excede a la cantidad disponible.');
            rowElem.cells[0].children[0].select();
            rowElem.cells[0].children[0].focus();
            return false;
            }
            return false;
            }

                }
            }*/
            return false;
        }
        function valKeyPressCantidadAddProd() {
            if (event.keyCode == 13) {
                document.getElementById('<%=btnAddProductos_AddProd.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }
        function valOnClick_btnAgregar() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        function valOnClick_capames() {
            onCapa('capaVentaMes');
            /*document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            */
            return false;
        }
        function valOnClick_capaa�o() {
            onCapa('capaVentaA�o');
            /*  document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            */
            return false;
        }
        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj) {
            var key = event.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        
        //************ MANTENIMIENTO PERSONA (nuevo)
        function onCapaCliente() {
            LimpiarControlesCliente();
            onCapa('capaRegistrarCliente');
            var opcion = document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID %>').value;
            if (opcion == 'N') {
                document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').disabled = false;
                document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').disabled = true;
                document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').focus();
            } else if (opcion == 'J') {
                document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').disabled = true;
                document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').disabled = false;
                document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').focus();
            }
            return false;
        }


        function mostrarCapamaestroObra() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }
        
        function onCapaMaestroObra() {
            LimpiarControlesMaestroObra();
            onCapa('IngresoMaestroObra');
            var opcion = document.getElementById('<%=cboTipoPersona_MaestroObra.ClientID %>').value;
   //         var opcion = document.getElementById('<%=cmbTipoAgente_MaestroObra.ClientID %>').value;
            if (opcion == 'N') {
                document.getElementById('<%= txtApPaterno_MaestroObra.ClientID%>').disabled = false;
                document.getElementById('<%= txtRazonSocial_MaestroObra.ClientID%>').disabled = true;
                document.getElementById('<%= txtApPaterno_MaestroObra.ClientID%>').focus();
            } else if (opcion == 'J') {
            document.getElementById('<%= txtApPaterno_MaestroObra.ClientID%>').disabled = true;
            document.getElementById('<%= txtRazonSocial_MaestroObra.ClientID%>').disabled = false;
            document.getElementById('<%= txtRazonSocial_MaestroObra.ClientID%>').focus();
            }
            return false;
        }

        void function LimpiarControlesMaestroObra() {
            document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtApMaterno_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtNombres_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtDNI_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtRUC_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtDireccion_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtTelefono_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtMail_Cliente.ClientID%>').value = "";
            document.getElementById('<%= cmbTipoAgente_MaestroObra.ClientID%>').value = '0';
        }
        
        void function LimpiarControlesCliente() {
            document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtApMaterno_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtNombres_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtDNI_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtRUC_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtDireccion_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtTelefono_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtMail_Cliente.ClientID%>').value = "";
            document.getElementById('<%= cmbTipoAgente_RegistrarCliente.ClientID%>').value = '0';
        }
        function validarSaveCliente() {
            var cboTipoPersona = document.getElementById('<%=cboTipoPersona_RegistrarCliente.ClientID %>').value;
            var txtDNI_Cliente = document.getElementById('<%=txtDNI_Cliente.ClientID %>');
            var txtRUC_Cliente = document.getElementById('<%=txtRUC_Cliente.ClientID %>');
            var cboCliDistritoN = document.getElementById('<%=cboCliDistritoN.ClientID %>');

            switch (cboTipoPersona) {

                case 'N':
                    if (txtDNI_Cliente.value.length > 0) {
                        if (txtDNI_Cliente.value.length < 8) {
                            alert("REVISE EL NUMERO DE D.N.I. DE LA PERSONA.");
                            return false;
                        }
                    }
                    if (txtRUC_Cliente.value.length > 0) {
                        if (txtRUC_Cliente.value.length < 11) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA.");
                            return false;
                        }
                        var valido = false;
                        if (txtRUC_Cliente.value.substring(0, 1) == '1') { valido = true; }
                        if (valido == false) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA.");
                            return false;
                        }
                    }

                    break;

                case 'J':
                    if (txtRUC_Cliente.value.length > 0) {
                        if (txtRUC_Cliente.value.length < 11) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA.");
                            return false;
                        }
                        if (txtRUC_Cliente.value.substring(0, 1) != '2') {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA.");
                            return false;
                        }
                    }

                    break;
            }
            
            if (cboCliDistritoN.value == '00' || cboCliDistritoN.value.length <= 0) {
                alert("SELECCIONE EL UBIGEO DE LA PERSONA.");
                return false;
            }


            return (confirm('Desea continuar con la operaci�n?'));
        }

        function validarSaveMaestroObra() {
        
            var cboTipoPersona = document.getElementById('<%=cboTipoPersona_MaestroObra.ClientID %>').value;
            var txtDNI_MaestroObra = document.getElementById('<%=txtDNI_MaestroObra.ClientID %>');
            var txtRUC_MaestroObra = document.getElementById('<%=txtRUC_MaestroObra.ClientID %>');
            var cboMODistritoN = document.getElementById('<%=cboMODistritoN.ClientID %>');

            switch (cboTipoPersona) {

                case 'N':
                    if (txtDNI_MaestroObra.value.length > 0) {
                        if (txtDNI_MaestroObra.value.length < 8) {
                            alert("REVISE EL NUMERO DE D.N.I. DE LA PERSONA.");
                            return false;
                        }
                    }
                    if (txtRUC_MaestroObra.value.length > 0) {
                        if (txtRUC_MaestroObra.value.length < 11) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA.");
                            return false;
                        }
                        var valido = false;
                        if (txtRUC_MaestroObra.value.substring(0, 1) == '1') { valido = true; }
                        if (valido == false) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA.");
                            return false;
                        }
                    }

                    break;

                case 'J':
                    if (txtRUC_MaestroObra.value.length > 0) {
                        if (txtRUC_MaestroObra.value.length < 11) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA.");
                            return false;
                        }
                        if (txtRUC_MaestroObra.value.substring(0, 1) != '2') {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA.");
                            return false;
                        }
                    }

                    break;
            }
            if (cboMODistritoN.value == '00' || cboMODistritoN.value.length <= 0) {
                alert("SELECCIONE EL UBIGEO DE LA PERSONA.");
                return false;
            }


            return (confirm('Desea continuar con la operaci�n?'));
        }
        
        function valKeyPressSaveCliente(obj) {
            if (event.keyCode == 13) {
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnGuardarCliente.ClientID%>').focus();
            }
            return true;
        }

        function valKeyPressSaveMaestroObra(obj) {
            if (event.keyCode == 13) {
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnGuardarMaestroObra.ClientID%>').focus();
            }
            return true;
        }
        function activarPanel_V1() {
            var combo = document.getElementById('<%=cboTipoPersona_RegistrarCliente.ClientID %>');
            var flag = false;
            switch (combo.value) {
                case 'N':
                    flag = false;
                    break;
                case 'J':
                    flag = true;
                    break;
            }
            //controles de Per. Natural

            document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtApMaterno_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtNombres_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtDNI_Cliente.ClientID%>').disabled = flag;

            //controles de Per. Juridica
            document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').disabled = !(flag);
            switch (combo.value) {
                case 'N':
                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').select();
                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').focus();
                    break;
                case 'J':
                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').select();
                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').focus();
                    break;
            }
            return true;
        }
        function activarPanel() {
            LimpiarControlesCliente();
            var combo = document.getElementById('<%=cboTipoPersona_RegistrarCliente.ClientID %>');
            var flag = false;
            switch (combo.value) {
                case 'N':
                    flag = false;
                    break;
                case 'J':
                    flag = true;
                    break;
            }
            //controles de Per. Natural

            document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtApMaterno_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtNombres_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtDNI_Cliente.ClientID%>').disabled = flag;

            //controles de Per. Juridica
            document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').disabled = !(flag);
            switch (combo.value) {
                case 'N':
                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').select();
                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').focus();
                    break;
                case 'J':
                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').select();
                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').focus();
                    break;
            }
            return true;
        }


        function activarPanelMaestroObra() {
            LimpiarControlesMaestroObra();
            var combo = document.getElementById('<%=cboTipoPersona_MaestroObra.ClientID %>');
            var flag = false;
            switch (combo.value) {
                case 'N':
                    flag = false;
                    break;
                case 'J':
                    flag = true;
                    break;
            }
            //controles de Per. Natural

            document.getElementById('<%= txtApPaterno_MaestroObra.ClientID%>').disabled = flag;
            document.getElementById('<%= txtApMaterno_MaestroObra.ClientID%>').disabled = flag;
            document.getElementById('<%= txtNombres_MaestroObra.ClientID%>').disabled = flag;
            document.getElementById('<%= txtDNI_MaestroObra.ClientID%>').disabled = flag;

            //controles de Per. Juridica
            document.getElementById('<%= txtRazonSocial_MaestroObra.ClientID%>').disabled = !(flag);
            switch (combo.value) {
                case 'N':
                    document.getElementById('<%= txtApPaterno_MaestroObra.ClientID%>').select();
                    document.getElementById('<%= txtApPaterno_MaestroObra.ClientID%>').focus();
                    break;
                case 'J':
                    document.getElementById('<%= txtRazonSocial_MaestroObra.ClientID%>').select();
                    document.getElementById('<%= txtRazonSocial_MaestroObra.ClientID%>').focus();
                    break;
            }
            return true;
        }



        function valOnClickTipoAgente_MaestroObra() {
            var cboTipoPersona = document.getElementById('<%=cboTipoPersona_MaestroObra.ClientID%>');
            var cmbTipoAgente_RegistrarCliente = document.getElementById('<%=cmbTipoAgente_MaestroObra.ClientID%>');
            if (cboTipoPersona.value == 'N') {
                alert('Opci�n no disponible para la Persona tipo [Natural].');
                cmbTipoAgente_RegistrarCliente.value = '0';
                return false;
            }
            return true;
        }    
        
        function valOnClickTipoAgente_RegistrarCliente() {
            var cboTipoPersona = document.getElementById('<%=cboTipoPersona_RegistrarCliente.ClientID%>');
            var cmbTipoAgente_RegistrarCliente = document.getElementById('<%=cmbTipoAgente_RegistrarCliente.ClientID%>');
            if (cboTipoPersona.value == 'N') {
                alert('Opci�n no disponible para la Persona tipo [Natural].');
                cmbTipoAgente_RegistrarCliente.value = '0';
                return false;
            }
            return true;
        }
        //********** FIN MANT (NUEVO)
        //******************************* MANT PERSONA (EDITAR)
        function valEditarCliente() {
            var txtIdCliente = document.getElementById('<%=txtCodigoCliente.ClientID%>');
            if (isNaN(txtIdCliente.value) || txtIdCliente.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            } else {

                var IdTipoPersona = parseInt(document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID%>').value);
                var IdTipoAgente = parseInt(document.getElementById('<%=cboTipoAgente.ClientID%>').value);
                var txtRazonSocial = document.getElementById('<%=txtApPaterno_RazonSocial.ClientID%>');
                var txtApPaterno = document.getElementById('<%=txtApPaterno_RazonSocial.ClientID%>');
                var txtApMaterno = document.getElementById('<%=txtApMaterno.ClientID%>');
                var txtNombres = document.getElementById('<%=txtNombres.ClientID%>');
                var txtDNI = document.getElementById('<%=txtDni.ClientID%>');
                var txtRUC = document.getElementById('<%=txtRuc.ClientID%>');
                var txtDireccion = document.getElementById('<%=txtDireccionCliente.ClientID%>');

                //********* Limpiamos las cajas
                document.getElementById('<%=txtApPaterno_EditarCliente.ClientID%>').value = '';
                document.getElementById('<%=txtRazonSocial_EditarCliente.ClientID%>').value = '';
                document.getElementById('<%=cboTipoAgente_EditarCliente.ClientID%>').value = '0';
                document.getElementById('<%=txtApMaterno_EditarCliente.ClientID%>').value = '';
                document.getElementById('<%=txtNombres_EditarCliente.ClientID%>').value = '';
                document.getElementById('<%=txtDNI_EditarCliente.ClientID%>').value = '';
                document.getElementById('<%=txtRUC_EditarCliente.ClientID%>').value = '';
                document.getElementById('<%=txtDireccion_EditarCliente.ClientID%>').value = '';

                //********** Mostramos los valores para su Edici�n
                switch (IdTipoPersona) {
                    case 0: //**** No se tiene un Tipo de Persona
                        alert('No se ha seleccionado ning�n tipo de Persona.');
                        return false;
                        break;
                    case 1: //**** NATURAL
                        document.getElementById('<%=txtApPaterno_EditarCliente.ClientID%>').value = txtApPaterno.value;
                        break;
                    case 2: //**** JURIDICA
                        document.getElementById('<%=txtRazonSocial_EditarCliente.ClientID%>').value = txtRazonSocial.value;
                        break;
                }
                document.getElementById('<%=cboTipoAgente_EditarCliente.ClientID%>').value = IdTipoAgente;
                document.getElementById('<%=txtApMaterno_EditarCliente.ClientID%>').value = txtApMaterno.value;
                document.getElementById('<%=txtNombres_EditarCliente.ClientID%>').value = txtNombres.value;
                document.getElementById('<%=txtDNI_EditarCliente.ClientID%>').value = txtDNI.value;
                document.getElementById('<%=txtRUC_EditarCliente.ClientID%>').value = txtRUC.value;
                document.getElementById('<%=txtDireccion_EditarCliente.ClientID%>').value = txtDireccion.value;
                onCapa('capaEditarPersona');
            }
            return false;
        }


        function valEditarMaestroObra() {
        
            var txtIdMaestroObra = document.getElementById('<%=txtCodigoMaestro.ClientID%>');

            if (isNaN(txtIdMaestroObra.value) || txtIdMaestroObra.value.length <= 0) {
                alert('Debe seleccionar un Maestro Obra.');
                return false;
            } 
            else {

               var IdTipoPersona =1;
              
              var IdTipoAgente = parseInt(document.getElementById('<%=cboTipoAgente.ClientID%>').value);
               var txtRazonSocial = document.getElementById('<%=txtApPaterno_RazonSocial.ClientID%>');
               var txtApPaterno = document.getElementById('<%=txtApPaterno_RazonSocial.ClientID%>');
                
                var txtApMaterno = document.getElementById('<%=txtApMaterno.ClientID%>');
                var txtNombres = document.getElementById('<%=txtMaestro.ClientID%>');
                var txtDNI = document.getElementById('<%=txtDni.ClientID%>');
                var txtRUC = document.getElementById('<%=txtRuc.ClientID%>');
                
 
                //********* Limpiamos las cajas
//                document.getElementById('<%=txtApPaterno_EditarMO.ClientID%>').value = '';
//                document.getElementById('<%=txtRazonSocial_EditarMO.ClientID%>').value = '';
////                document.getElementById('<%=cboTipoAgente_EditarMO.ClientID%>').value = '0';
//                document.getElementById('<%=txtApMaterno_EditarMO.ClientID%>').value = '';
//                document.getElementById('<%=txtNombres_EditarMO.ClientID%>').value = '';
//                document.getElementById('<%=txtDNI_EditarMO.ClientID%>').value = '';
//                document.getElementById('<%=txtRUC_EditarMO.ClientID%>').value = '';
                
               //document.getElementById('<%=txtDireccion_EditarMO.ClientID%>').value = '';

                //********** Mostramos los valores para su Edici�n
                switch (IdTipoPersona) {
                    case 0: //**** No se tiene un Tipo de Persona
                        alert('No se ha seleccionado ning�n tipo de Persona.');
                        return false;
                        break;
                    case 1: //**** NATURAL
                   //     document.getElementById('<%=txtApPaterno_EditarMO.ClientID%>').value = txtApPaterno.value;
                        break;
                    case 2: //**** JURIDICA
                        document.getElementById('<%=txtRazonSocial_EditarMO.ClientID%>').value = txtRazonSocial.value;
                        break;
                }
//             document.getElementById('<%=cboTipoAgente_EditarMO.ClientID%>').value = IdTipoAgente;
//                document.getElementById('<%=txtApMaterno_EditarMO.ClientID%>').value = txtApMaterno.value;
//                document.getElementById('<%=txtNombres_EditarMO.ClientID%>').value = appat.value;
//                document.getElementById('<%=txtDNI_EditarMO.ClientID%>').value = txtDNI.value;
//                document.getElementById('<%=txtRUC_EditarMO.ClientID%>').value = txtRUC.value;
               // document.getElementById('<%=txtDireccion_EditarMO.ClientID%>').value = txtDireccion.value;
                onCapa('capaEditarMaestroObra');
            }
            return false;
        }
        
        
        
        
        function valOnClickTipoAgente_EditarCliente() {
            var tipoPersona = document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID %>').value;
            if (tipoPersona == '0') {
                alert('Debe seleccionar un Tipo de Persona [Natural - Jur�dica].');
                return false;
            }
            if (tipoPersona == '1') {  //*** No es Jur�dica
                alert('Opci�n no disponible para el Tipo de Persona [Natural].');
                return false;
            }
            return true;
        }


        function valOnClickTipoAgente_EditarMO() {
            var tipoPersona = document.getElementById('<%=cboTipoPersona_MaestroObra.ClientID %>').value;
            if (tipoPersona == '0') {
                alert('Debe seleccionar un Tipo de Persona [Natural - Jur�dica].');
                return false;
            }
            if (tipoPersona == '1') {  //*** No es Jur�dica
                alert('Opci�n no disponible para el Tipo de Persona [Natural].');
                return false;
            }
            return true;
        }

        function valOnKeyPress_EditarMO(opcion, obj) {

            if (event.keyCode == 13) {
                if (obj != null) {
                    onBlurTextTransform(obj, configurarDatos);
                }
                var boton = document.getElementById('<%=btnGuardar_EditarMO.ClientID %>');
                boton.click();
            }

            var tipoPersona = 1;
            if (tipoPersona == '0') {
                alert('Debe seleccionar un Tipo de Persona [Natural - Jur�dica].');
                return false;
            }
            if (opcion == '0') {  //**** Accesible con ambos tipos de persona
                return true;
            }
            if (tipoPersona == opcion) {  //**** Campo correcto para el tipo de persona
                return true;
            } else {
                alert('Este campo no es accesible para el tipo de Persona [' + getCampoxValorCombo(document.getElementById('<%=cboTipoPersona_MaestroObra.ClientID %>'), tipoPersona) + '].');
                return false;
            }
            return false;
        }
        
        function valOnKeyPress_EditarCliente(opcion, obj) {

            if (event.keyCode == 13) {
                if (obj != null) {
                    onBlurTextTransform(obj, configurarDatos);
                }
                var boton = document.getElementById('<%=btnGuardar_EditarCliente.ClientID %>');
                boton.click();
            }

            var tipoPersona = document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID %>').value;
            if (tipoPersona == '0') {
                alert('Debe seleccionar un Tipo de Persona [Natural - Jur�dica].');
                return false;
            }
            if (opcion == '0') {  //**** Accesible con ambos tipos de persona
                return true;
            }
            if (tipoPersona == opcion) {  //**** Campo correcto para el tipo de persona
                return true;
            } else {
                alert('Este campo no es accesible para el tipo de Persona [' + getCampoxValorCombo(document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID %>'), tipoPersona) + '].');
                return false;
            }
            return false;
        }
        function valOnKeyPressDNI_EditarCliente() {
            var flag = valOnKeyPress_EditarCliente('1');
            if (flag) { //*** Validamos el DNI
                return onKeyPressEsNumero('event');
            }
            return false;
        }
        function valOnKeyPressRUC_EditarCliente() {
            var flag = valOnKeyPress_EditarCliente('0');
            if (flag) { //*** Validamos el DNI
                return onKeyPressEsNumero('event');
            }
            return false;
        }
        //        function valKeyPressEditarCliente(obj) {
        //            if (event.keyCode == 13) {
        //                onBlurTextTransform(obj, configurarDatos);
        //                document.getElementById('<%=btnGuardar_EditarCliente.ClientID%>').focus();
        //            }
        //            return true;
        //        }
        function valOnClickGuardar_EditarMO() {
            var tipoPersona = "1";
            var cboMODistritoE = document.getElementById('<%=cboMODistritoE.ClientID %>').value;
            switch (parseInt(tipoPersona)) {
                case 0:  //**** ----
                    alert('No se ha seleccionado un Tipo de Persona.');
                    return false;
                    break;
                case 1:  //**** NATURAL
                    var txtApPaterno = document.getElementById('<%=txtApPaterno_EditarMO.ClientID%>');
                    if (CajaEnBlanco(txtApPaterno)) {
                        alert('El campo [Ap. Paterno] es un campo requerido.');
                        txtApPaterno.select();
                        txtApPaterno.focus();
                        return false;
                    }
                    var txtNombres = document.getElementById('<%=txtNombres_EditarMO.ClientID%>');
                    if (CajaEnBlanco(txtNombres)) {
                        alert('El Campo [Nombre] es un campo requerido.');
                        txtNombres.select();
                        txtNombres.focus();
                        return false;
                    }
                    var txtDNI_MO = document.getElementById('<%=txtDNI_EditarMO.ClientID%>');
                    if (txtDNI_MO.value.length > 0) {
                        if (txtDNI_MO.value.length < 8) {
                            alert("REVISE EL NUMERO DE D.N.I. DE LA PERSONA NATURAL.");
                            return false;
                        }
                    }
                    
                    break;
                case 2:  //**** JURIDICA
                    var txtRazonSocial = document.getElementById('<%=txtRazonSocial_EditarMO.ClientID%>');
                    if (CajaEnBlanco(txtRazonSocial)) {
                        alert('El Campo [Raz�n Social] es un campo requerido.');
                        txtRazonSocial.select();
                        txtRazonSocial.focus();
                        return false;
                    }
                    var txtRUC = document.getElementById('<%=txtRUC_EditarMO.ClientID%>');
                    if (CajaEnBlanco(txtRUC)) {
                        alert('El Campo [R.U.C.] es un campo requerido.');
                        txtRUC.select();
                        txtRUC.focus();
                        return false;
                    }
                    if (txtRUC.value.length < 11) {
                        alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA JURIDICA.");
                        return false;
                    }
                    if (txtRUC.value.substring(0, 1) != '2') {
                        alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA JURIDICA.");
                        return false;
                    }
                    break;
            }
            var txtDireccion = document.getElementById('<%=txtDireccion_EditarMO.ClientID%>');
            if (CajaEnBlanco(txtDireccion)) {
                // alert('El Campo [Direccion] es un campo requerido.');
                txtDireccion.select();
                txtDireccion.focus();
                return false;
            }
            if (cboMODistritoE.value == '00' )
             {
                alert("SELECCIONE EL UBIGEO DE LA PERSONA.");
                return false;
            }
//        if (cboMODistritoE.value == '00' || cboMODistritoE.value.length <= 0) {
//            if ( cboMODistritoE.value.Length <= 0) {
//                alert("SELECCIONE EL UBIGEO DE LA PERSONA.");
//                return false;
//            }
            return confirm('Desea guardar los cambios?');
        }
        
        
        
        
        
        
        function valOnClickGuardar_EditarCliente() {
            var tipoPersona = document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID %>').value;
            var cboCliDistritoE = document.getElementById('<%=cboCliDistritoE.ClientID %>').value;
            switch (parseInt(tipoPersona)) {
                case 0:  //**** ----
                    alert('No se ha seleccionado un Tipo de Persona.');
                    return false;
                    break;
                case 1:  //**** NATURAL
                    var txtApPaterno = document.getElementById('<%=txtApPaterno_EditarCliente.ClientID%>');
                    if (CajaEnBlanco(txtApPaterno)) {
                        alert('El campo [Ap. Paterno] es un campo requerido.');
                        txtApPaterno.select();
                        txtApPaterno.focus();
                        return false;
                    }
                    var txtNombres = document.getElementById('<%=txtNombres_EditarCliente.ClientID%>');
                    if (CajaEnBlanco(txtNombres)) {
                        alert('El Campo [Nombre] es un campo requerido.');
                        txtNombres.select();
                        txtNombres.focus();
                        return false;
                    }
                    var txtDNI_Cliente = document.getElementById('<%=txtDNI_EditarCliente.ClientID%>');
                    if (txtDNI_Cliente.value.length > 0) {
                        if (txtDNI_Cliente.value.length < 8) {
                            alert("REVISE EL NUMERO DE D.N.I. DE LA PERSONA NATURAL.");
                            return false;
                        }
                    }
                    var txtRUC_Cliente = document.getElementById('<%=txtRUC_EditarCliente.ClientID%>');
                    if (txtRUC_Cliente.value.length > 0) {
                        if (txtRUC_Cliente.value.length < 11) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA NATURAL.");
                            return false;
                        }
                        var valido = false;
                        if (txtRUC_Cliente.value.substring(0, 1) == '1') { valido = true; }
                        if (valido == false) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA NATURAL.");
                            return false;
                        }
                    }
                    break;
                case 2:  //**** JURIDICA
                    var txtRazonSocial = document.getElementById('<%=txtRazonSocial_EditarCliente.ClientID%>');
                    if (CajaEnBlanco(txtRazonSocial)) {
                        alert('El Campo [Raz�n Social] es un campo requerido.');
                        txtRazonSocial.select();
                        txtRazonSocial.focus();
                        return false;
                    }
                    var txtRUC = document.getElementById('<%=txtRUC_EditarCliente.ClientID%>');
                    if (CajaEnBlanco(txtRUC)) {
                        alert('El Campo [R.U.C.] es un campo requerido.');
                        txtRUC.select();
                        txtRUC.focus();
                        return false;
                    }
                    if (txtRUC.value.length < 11) {
                        alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA JURIDICA.");
                        return false;
                    }
                    if (txtRUC.value.substring(0, 1) != '2') {
                        alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA JURIDICA.");
                        return false;
                    }
                    break;
            }
            var txtDireccion = document.getElementById('<%=txtDireccion_EditarCliente.ClientID%>');
            if (CajaEnBlanco(txtDireccion)) {
                // alert('El Campo [Direccion] es un campo requerido.');
                txtDireccion.select();
                txtDireccion.focus();
                return false;
            }
            if (cboCliDistritoE.value == '00') {
                alert("SELECCIONE EL UBIGEO DE LA PERSONA.");
                return false;
            }
            return confirm('Desea guardar los cambios?');
        }
        //***************** FIN MANT (EDITAR)
        //***************************************************** BUSQUEDA PERSONA
        function valBuscarClientexId() {
            var caja = document.getElementById('<%=txtIdCliente_BuscarxId.ClientID%>');
            if (CajaEnBlanco(caja)) {
                alert('Ingrese un C�digo de Cliente.');
                caja.select();
                caja.focus();
                return false;
            }
            return true;
        }

        function valBuscarMaestroObra() {
            var caja = document.getElementById('<%=txtIdMaestroObra_BuscarxId.ClientID%>');
            if (CajaEnBlanco(caja)) {
                alert('Ingrese un C�digo de MaestroObra.');
                caja.select();
                caja.focus();
                return false;
            }
            return true;
        }
        
        
        function valKeyPressBuscarPersonaxId() {
            if (event.keyCode == 13) {
                document.getElementById('<%=btnBuscarClientexIdCliente.ClientID%>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }
            return true;
        }

        function valKeyPressBuscarMaestroId() {
            if (event.keyCode == 13) {
                document.getElementById('<%=btnBuscarMaestroxId.ClientID%>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }
            return true;
        }

        //********************* PROCESO DE CALCULO DE IMPORTES
        function calcularTotales_GV_Detalle(opcion, obj) {
            /*
            Opcion:
            0: Cantidad   1: P Venta     2: Dcto Valor     3: Dcto Porcent
            4: PV Final
            */
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var cantidad = 0;
            var peso = 0;
            var pVenta = 0;
            var dctoValor = 0;
            var dctoPorcent = 0;
            var pvFinal = 0;
            var pLista = 0;
            var pComercial = 0;
            var importe = 0;
            var importeTotal = 0;
            var dctoTotal = 0;
            var percepcionTotal = 0;
            var TasaPercepcion = 0;
            var porcentDctoMaximo = 0;
            var precioBaseDcto = '';
            var dctoValor_MaxxPVenta = 0;
            var dctoPorcent_MaxxPVenta = 0;
            var descripcionProd = '';
            var codigoProd = '';
            //** se agrego el 28 marzo 2011
            var IdProducto = 0;
            var IdProductoAux = 0;
            var IdCampania = 0;
            var CantCampania = 0;
            var volVentaMinimo = 0;            

            //***descuento maximo
            var dctomax = 0;
            var totaldctomax = 0;

            //********** cajas de texto
            var txtDctoValor;
            var txtDctoPorcent;
            var txtPVFinal;
            var txtImporte;
            var txtDctoValor_MaxxPVenta;
            var txtDctoPorcent_MaxxPVenta;
            var percepcion = 0;
            var PesoTotal = 0;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    cantidad = parseFloat(rowElem.cells[3].children[0].cells[0].children[0].value);
                    if (isNaN(cantidad)) { cantidad = 0; }

                    peso = parseFloat(rowElem.cells[3].children[0].cells[7].children[0].value);
                    if (isNaN(peso)) { peso = 0; }

                    pLista = parseFloat(rowElem.cells[4].innerHTML);
                    if (isNaN(pLista)) { pLista = 0; }
                    pVenta = parseFloat(rowElem.cells[5].children[0].cells[0].children[0].value);
                    if (isNaN(pVenta)) { pVenta = 0; }
                    txtDctoValor = rowElem.cells[6].children[0].cells[0].children[0];
                    dctoValor = parseFloat(txtDctoValor.value);
                    if (isNaN(dctoValor)) { dctoValor = 0; }
                    txtDctoPorcent = rowElem.cells[7].children[0].cells[0].children[0];
                    dctoPorcent = parseFloat(txtDctoPorcent.value);
                    if (isNaN(dctoPorcent)) { dctoPorcent = 0; }
                    txtPVFinal = rowElem.cells[8].children[0].cells[0].children[0];
                    pvFinal = parseFloat(txtPVFinal.value);
                    if (isNaN(pvFinal)) { pvFinal = 0; }

                    porcentDctoMaximo = parseFloat(rowElem.cells[11].children[0].cells[1].children[0].value);
                    txtDctoValor_MaxxPVenta = rowElem.cells[6].children[0].cells[1].children[0];
                    dctoValor_MaxxPVenta = parseFloat(txtDctoValor_MaxxPVenta.value);
                    if (isNaN(dctoValor_MaxxPVenta)) { dctoValor_MaxxPVenta = 0; }

                    txtDctoPorcent_MaxxPVenta = rowElem.cells[7].children[0].cells[1].children[0];
                    dctoPorcent_MaxxPVenta =
                    parseFloat(txtDctoPorcent_MaxxPVenta.value);
                    if (isNaN(dctoPorcent_MaxxPVenta)) { dctoPorcent_MaxxPVenta = 0; }

                    
                    //************* OBTENEMOS EL PRECIO BASE DCTO
                    precioBaseDcto = rowElem.cells[11].children[0].cells[7].children[0].value;
                    pComercial = parseFloat(rowElem.cells[2].children[0].cells[1].children[0].value);

                    txtImporte = rowElem.cells[9].children[0].cells[0].children[0];

                    //** se agrego el 28 marzo 2011
                    IdProducto = rowElem.cells[11].children[0].cells[6].children[0].value;

                    IdProductoAux = rowElem.cells[11].children[0].cells[11].children[0].value;
                    if (isNaN(parseInt(IdProductoAux))) { IdProductoAux = 0; }

                    IdCampania = rowElem.cells[11].children[0].cells[12].children[0].value;
                    if (isNaN(parseInt(IdCampania))) { IdCampania = 0; }

                    CantCampania = parseFloat(rowElem.cells[11].children[0].cells[13].children[0].value);
                    if (isNaN(CantCampania)) { CantCampania = 0; }

                    volVentaMinimo = parseFloat(rowElem.cells[11].children[0].cells[4].children[0].value);

                    if (parseInt(IdCampania) > 0 && parseInt(IdProductoAux) > 0 && parseFloat(CantCampania) > 0) { // es un producto en campania
                        var cantidadTransferencia = Math.floor(parseFloat(getVolumenMinVenta(IdCampania, IdProductoAux)) / (CantCampania / volVentaMinimo)); if (isNaN(cantidadTransferencia)) { cantidadTransferencia = 0; }
                        rowElem.cells[3].children[0].cells[0].children[0].value = cantidadTransferencia;
                    }


                    switch (opcion) {
                        case 0:   //************ CANTIDAD
                            
                            break;
                        case 1:  //************ P VENTA                        
                            if (obj != null) {
                                if (obj.id == rowElem.cells[5].children[0].cells[0].children[0].id) {
                                    pvFinal = pVenta;
                                    dctoPorcent = 0;
                                    dctoValor = 0;
                                    txtPVFinal.value = redondear(pvFinal, 2);
                                    txtDctoPorcent.value = redondear(dctoPorcent, 2);
                                    txtDctoValor.value = redondear(dctoValor, 4);
                                }
                            }
                            break;
                        case 2:  //*********** DCTO VALOR
                            pvFinal = pVenta - dctoValor;
                            if (pVenta <= 0) {
                                dctoPorcent = 0;
                            } else {
                                dctoPorcent = ((dctoValor) / (pVenta)) * 100;
                            }
                            txtPVFinal.value = redondear(pvFinal, 2);
                            txtDctoPorcent.value = redondear(dctoPorcent, 2);
                            break;
                        case 3:   //*********** DCTO PORCENT
                            pvFinal = pVenta * (1 - dctoPorcent / 100);

                            if (pvFinal >= pVenta) {
                                dctoValor = 0;
                            } else {
                                dctoValor = pVenta - pvFinal;
                            }
                            txtPVFinal.value = redondear(pvFinal, 2);
                            txtDctoValor.value = redondear(dctoValor, 4);
                            break;
                        case 4:   //*********** PV FINAL
                            if (pvFinal >= pVenta) {
                                dctoPorcent = 0;
                                dctoValor = 0;
                            } else {
                                dctoValor = (pVenta - pvFinal);
                                if (pVenta <= 0) {
                                    dctoPorcent = 0;
                                } else {
                                    dctoPorcent = ((dctoValor) / (pVenta)) * 100;
                                }
                            }
                            txtDctoPorcent.value = redondear(dctoPorcent, 2);
                            txtDctoValor.value = redondear(dctoValor, 4);
                            break;
                    }

                    //*********** MANEJO DE DESCUENTOS
                    /*
                    EL PORCENTAJE DE DCTO ESTA EN FUNCION AL TIPO DE PRECIO DE LISTA O AL TIPO DE PRECIO COMERCIAL
                    PL: SOBRE PRECIO DE LISTA
                    PC: SOBRE PRECIO COMERCIAL
                    */

                    var dctoPrecioBase = 0;
                    var dif_PrecioVenta_PrecioBase = 0;
                    var precioBase = 0;

                    if (precioBaseDcto == 'PL') {
                        precioBase = pLista;
                    } else if (precioBaseDcto == 'PC') {
                        precioBase = pComercial;
                    } else {
                        //alert('NO SE HA DESIGNADO UN PRECIO BASE EN LA APLICACI�N DE DESCUENTOS (CONFIGURACI�N DESCUENTOS). PROBLEMA EN LA OPERACI�N.');
                        precioBase = pLista;
                        //return false;
                    }

                    dctoPrecioBase = precioBase * (porcentDctoMaximo / 100);
                    dif_PrecioVenta_PrecioBase = pVenta - precioBase;
                    dctoValor_MaxxPVenta = dctoPrecioBase + dif_PrecioVenta_PrecioBase;
                    if (dctoValor_MaxxPVenta <= 0) { dctoValor_MaxxPVenta = 0; }
                    if (pVenta > 0) {
                        dctoPorcent_MaxxPVenta = (dctoValor_MaxxPVenta / pVenta) * 100;
                    } else {
                        dctoPorcent_MaxxPVenta = 0;
                    }
                    if (dctoPorcent_MaxxPVenta <= 0) { dctoPorcent_MaxxPVenta = 0; }

                    txtDctoValor_MaxxPVenta.value = redondear(dctoValor_MaxxPVenta, 4);
                    txtDctoPorcent_MaxxPVenta.value = redondear(dctoPorcent_MaxxPVenta, 2);
                    //********************* end Manejo de Descuentos

                    var preImporte = cantidad * redondear(pvFinal, 2);
                    importe = preImporte.toPrecision(10);
                    //***************************** IMPRIMIMOS DATOS                    
                    txtImporte.value = redondear(importe, 2);
                    importeTotal = importeTotal + redondear(importe, 2);

                    //****************  DESCUENTO MAXIMO
                    dctomax = cantidad * parseFloat(txtDctoValor_MaxxPVenta.value);
                    totaldctomax = totaldctomax + redondear(dctomax, 2);

                    //*************   PERCEPCION
                    TasaPercepcion = parseFloat(rowElem.cells[10].innerHTML);

                    if (TasaPercepcion > 0) {
                        percepcion = (TasaPercepcion / 100);
                    }

                    //****************  DESCUENTO
                    dctoTotal = dctoTotal + (dctoValor * cantidad);

                    var prePeso = cantidad * peso;
                    if (isNaN(prePeso)) { prePeso = 0; }
                    PesoTotal = PesoTotal + redondear(prePeso, 2);


                } //********* FIN FOR                                                
            } //************ FIN IF                        

            //***************** SE IMPRIME LOS VALORES
            var porcentIgv = parseFloat(document.getElementById('<%=lblPorcentIgv.ClientID%>').innerHTML);
            var totalConcepto;
            if (document.getElementById('<%=txtTotalConcepto.ClientID%>') == null) {
                totalConcepto = 0;
            } else {
                totalConcepto = parseFloat(document.getElementById('<%=txtTotalConcepto.ClientID%>').value);
            }

            var subTotal = 0;
            var total = 0;
            var igv = 0;
            var totalPagar = 0;

            total = importeTotal + totalConcepto;
            subTotal = total / (1 + porcentIgv / 100);
            igv = total - subTotal;
            percepcionTotal = total * percepcion;
            totalPagar = total + percepcionTotal;


            document.getElementById('<%=txtImporteTotal.ClientID%>').value = Format(importeTotal, 2);
            document.getElementById('<%=txtSubTotal.ClientID%>').value = Format(subTotal, 2);
            document.getElementById('<%=txtIGV.ClientID%>').value = Format(igv, 2);
            document.getElementById('<%=txtTotal.ClientID%>').value = Format(total, 2);
            document.getElementById('<%=txtPercepcion.ClientID%>').value = redondear(percepcionTotal, 2);
            document.getElementById('<%=txtDescuento.ClientID%>').value = redondear(dctoTotal, 2);
            document.getElementById('<%=txtTotalConcepto_PanelDetalle.ClientID%>').value = redondear(totalConcepto, 2);
            document.getElementById('<%=txtTotalAPagar.ClientID%>').value = Format(totalPagar, 2);
            document.getElementById('<%=txtDescuentoMax.ClientID%>').value = redondear(totaldctomax, 2);
            document.getElementById('<%=txtPesoTotal.ClientID%>').value = redondear(PesoTotal, 2);

            return false;
        }
        //
        function getVolumenMinVenta(v_IdCampania, v_IdProducto) {
            var cantidad = 0;
            var _IdCampania = 0;
            var _IdProducto = 0;
            var grilla1 = document.getElementById('<%=GV_Detalle.ClientID%>');
            for (var j = 1; j < grilla1.rows.length; j++) {
                var rowElemx = grilla1.rows[j];

                _IdProducto = rowElemx.cells[11].children[0].cells[6].children[0].value;
                if (isNaN(parseInt(_IdProducto))) { _IdProducto = 0; }

                _IdCampania = rowElemx.cells[11].children[0].cells[12].children[0].value;
                if (isNaN(parseInt(_IdCampania))) { _IdCampania = 0; }

                if (parseInt(_IdProducto) == parseInt(v_IdProducto) && parseInt(_IdCampania) == parseInt(v_IdCampania)) {
                    cantidad = parseFloat(rowElemx.cells[3].children[0].cells[0].children[0].value);
                    if (isNaN(cantidad)) { cantidad = 0; }

                    return cantidad;
                    break;

                }
            }
        }
        //
        function valPorcentMaximoDctoxArticulo_OnBlur(caja) {

            if (caja != null) {
                if (CajaEnBlanco(caja) || isNaN(caja.value)) {
                    caja.select();
                    caja.focus();
                    alert('Debe ingresar un valor v�lido.');
                    return false;
                }
            }

            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var pvFinal = 0;
            var pLista = 0;
            var pComercial = 0; //************ PRECIO DE LA TABLA PRODUCTO TIPO PV          
            var porcentDctoMaximo = 0;
            var descripcionProd = '';
            var codigoProd = '';
            var pVenta = 0;
            var cantidad = 0;
            var volVentaMinimo = 0;
            var DsctoOriginal = 0;
            var CantxDsctoMin = 0;
            //********** cajas de texto
            var txtPVFinal;
            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            var moneda = getCampoxValorCombo(cboMoneda, cboMoneda.value);
            var CantMin = 0;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];

                    pComercial = parseFloat(rowElem.cells[2].children[0].cells[1].children[0].value);
                    if (isNaN(pComercial)) { pComercial = 0; }
                    pVenta = parseFloat(rowElem.cells[5].children[0].cells[0].children[0].value);
                    pLista = parseFloat(rowElem.cells[4].innerHTML);
                    txtPVFinal = rowElem.cells[8].children[0].cells[0].children[0];
                    pvFinal = parseFloat(txtPVFinal.value);
                    if (isNaN(pvFinal)) { pvFinal = 0; }

               
                    porcentDctoMaximo =redondear( parseFloat(rowElem.cells[11].children[0].cells[1].children[0].value),2);

                    codigoProd = rowElem.cells[1].innerHTML;
                    descripcionProd = rowElem.cells[2].children[0].cells[0].children[0].innerHTML;

                    if (pVenta < pLista) {
                        alert('El precio de venta del producto  < ' + codigoProd + '  -  ' + descripcionProd + ' > no puede ser menor al precio de lista.\nNO SE PERMITE LA OPERACI�N.');
                        return false;
                    }


                    //****** CALCULAMOS EL PORCENTAJE DE DCTO SOBRE EL PRECIO DE LISTA

                    //*********** MANEJO DE DESCUENTOS
                    /*
                    EL PORCENTAJE DE DCTO ESTA EN FUNCION AL TIPO DE PRECIO DE LISTA O AL TIPO DE PRECIO COMERCIAL
                    PL: SOBRE PRECIO DE LISTA
                    PC: SOBRE PRECIO COMERCIAL
                    */

                    var dctoPrecioBase_User = 0;
                    var dctoPrecioBase = 0;
                    var dif_PrecioVenta_PrecioBase = 0;
                    var precioBase = 0;
                    var precioBaseDcto = rowElem.cells[11].children[0].cells[7].children[0].value;
                    var dctoValor_MaxxPVenta = 0;
                    var txtDctoValor = rowElem.cells[6].children[0].cells[0].children[0];
                    var dctoValor = parseFloat(txtDctoValor.value);
                    var txtPorcentDctoValor_User = rowElem.cells[7].children[0].cells[0].children[0];
                    var PorcentDctoValor_User = parseFloat(txtPorcentDctoValor_User.value);
                    if (isNaN(PorcentDctoValor_User)) { PorcentDctoValor_User = 0; }

                    var IdProductoAux = rowElem.cells[11].children[0].cells[11].children[0].value;
                    if (isNaN(parseInt(IdProductoAux))) { IdProductoAux = 0; }
                    var IdCampania = rowElem.cells[11].children[0].cells[12].children[0].value;
                    if (isNaN(parseInt(IdCampania))) { IdCampania = 0; }
                    var CantCampania = parseFloat(rowElem.cells[11].children[0].cells[13].children[0].value);
                    if (isNaN(CantCampania)) { CantCampania = 0; }

                    if (precioBaseDcto == 'PL') {
                        precioBase = pLista;
                    } else if (precioBaseDcto == 'PC') {
                        precioBase = pComercial;
                    } else {
                        //alert('NO SE HA DESIGNADO UN PRECIO BASE EN LA APLICACI�N DE DESCUENTOS (CONFIGURACI�N DESCUENTOS). PROBLEMA EN LA OPERACI�N.');
                        precioBase = pLista;
                        //return false;
                    }

                    dctoPrecioBase = precioBase * (porcentDctoMaximo / 100);
                    if (PorcentDctoValor_User > 0) {
                        dctoPrecioBase_User = precioBase * (PorcentDctoValor_User / 100);
                    }

                    dif_PrecioVenta_PrecioBase = pVenta - precioBase;
                    dctoValor_MaxxPVenta = dctoPrecioBase + dif_PrecioVenta_PrecioBase;
                    //if (dctoValor_MaxxPVenta <= 0) { dctoValor_MaxxPVenta = 0; }


                    if (dctoValor_MaxxPVenta < 0) {
                        alert('EL PRECIO M�NIMO DE VENTA PARA EL PRODUCTO < ' + codigoProd + '  -  ' + descripcionProd + ' >  ES DE < ' + moneda + ' ' + redondear((precioBase - dctoPrecioBase), 2) + ' >. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }
                    if (dctoValor > redondear(dctoValor_MaxxPVenta, 4)) {
                        alert('EL DESCUENTO M�XIMO PERMITIDO PARA EL PRODUCTO < ' + codigoProd + '  -  ' + descripcionProd + ' >  ES DE < ' + moneda + ' ' + redondear(dctoValor_MaxxPVenta, 2) + ' >. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }
                    if ((dctoPrecioBase_User > dctoValor_MaxxPVenta)) {
                        alert('EL DESCUENTO M�XIMO PERMITIDO PARA EL PRODUCTO < ' + codigoProd + '  -  ' + descripcionProd + ' >  ES DE < ' + porcentDctoMaximo + ' >. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }


                    //****************** VALIDAMOS EL VOLUMEN MINIMO DE VENTA
                    cantidad = parseFloat(rowElem.cells[3].children[0].cells[0].children[0].value);
                    if (isNaN(cantidad)) { cantidad = 0; }
                    volVentaMinimo = parseFloat(rowElem.cells[11].children[0].cells[4].children[0].value);
                    if (isNaN(volVentaMinimo)) { volVentaMinimo = 0; }
                    if (cantidad < volVentaMinimo) {

                        if (parseInt(IdCampania) > 0 && parseInt(IdProductoAux) > 0 && parseFloat(CantCampania) > 0) { // es un producto en campania
                            alert('LA CANTIDAD INGRESADA PARA EL PRODUCTO [ ' + descripcionProd.toUpperCase() + ' ] ES MENOR A [' + volVentaMinimo + '] PARA LA APLICACI�N DE LA CAMPA�A.');
                        } else {
                            alert('LA CANTIDAD INGRESADA PARA EL PRODUCTO [ ' + descripcionProd.toUpperCase() + ' ] ES MENOR AL VOLUMEN DE VENTA M�NIMO [' + volVentaMinimo + '] PARA LA APLICACI�N DEL PRECIO DE LISTA SELECCIONADO.');
                        }

                        return false;
                    }

                    CantMin = parseFloat(rowElem.cells[7].children[0].cells[3].children[0].value);
                    if (isNaN(CantMin)) { CantMin = 0; }

                    DsctoOriginal = parseFloat(rowElem.cells[7].children[0].cells[4].children[0].value);
                    if (isNaN(DsctoOriginal)) { DsctoOriginal = 0; }                    

                    if (cantidad < CantMin) {

//                        if (caja != null) {
//                            if (caja.id == rowElem.cells[3].children[0].cells[0].children[0].id) {
                                rowElem.cells[11].children[0].cells[1].children[0].value = DsctoOriginal;
                                rowElem.cells[7].children[0].cells[1].children[0].value = DsctoOriginal;
                                rowElem.cells[6].children[0].cells[1].children[0].value = redondear((pLista * (DsctoOriginal / 100)), 4);
                                rowElem.cells[7].children[0].cells[3].children[0].value = '0';
//                            }
//                        }

                                alert('EL PRODUCTO [ ' + descripcionProd.toUpperCase() + ' ].\n POSEE UN DESCUENTO POR VOLUMEN DE VENTA.\n LA CANTIDAD DE VENTA DEBE SER MAYOR A [' + CantMin + ']. NO SE PERMITE LA OPERACI�N.');                        
                    }

                } //********* FIN FOR
            } //************ FIN IF

            return true;
        }
        function valOnFocus_txtImporte() {
            var boton = document.getElementById('<%=btnAgregar.ClientID%>');
            boton.focus();
            return false;
        }
        function valOnClick_btnPermisoDcto_GV_Detalle(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var descripcionProd = '';
            var DsctoBase = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];
                    if (boton.id == rowElem.cells[11].children[0].cells[0].children[0].id) {
                        descripcionProd = rowElem.cells[2].children[0].cells[0].children[0].innerHTML;
                        DsctoBase = parseFloat(rowElem.cells[7].children[0].cells[1].children[0].value);
                        if (isNaN(DsctoBase)) { DsctoBase = 0; }
                        onCapa('capaAddPermiso_Detalle');
                        document.getElementById('<%=lblProducto_AddDctoAdicional.ClientID%>').innerHTML = descripcionProd;
                        document.getElementById('<%=txtDescuentoBase.ClientID%>').value = DsctoBase;
                        document.getElementById('<%=hddIndex_GV_Detalle.ClientID%>').value = (i - 1);
                        document.getElementById('<%=txtLogin_AddDctoAdicional.ClientID%>').value = '';
                        document.getElementById('<%=txtClave_AddDctoAdicional.ClientID%>').value = '';
                        document.getElementById('<%=txtClaveConfirmar_AddDctoAdicional.ClientID%>').value = '';
                        document.getElementById('<%=txtLogin_AddDctoAdicional.ClientID%>').select();
                        document.getElementById('<%=txtLogin_AddDctoAdicional.ClientID%>').focus();
                        return false;
                    }
                }
            }
            alert('Problemas en la ubicaci�n de la Fila.');
            return false;
        }
        function valOnClick_btnAceptar_AddDctoAdicional() {
            var txtUsuario = document.getElementById('<%=txtLogin_AddDctoAdicional.ClientID%>');
            if (CajaEnBlanco(txtUsuario)) {
                alert('Campo requerido');
                txtUsuario.select();
                txtUsuario.focus();
                return false;
            }
            var txtClave1 = document.getElementById('<%=txtClave_AddDctoAdicional.ClientID%>');
            if (CajaEnBlanco(txtClave1)) {
                alert('Campo requerido');
                txtClave1.select();
                txtClave1.focus();
                return false;
            }
            var txtClave2 = document.getElementById('<%=txtClaveConfirmar_AddDctoAdicional.ClientID%>');
            if (CajaEnBlanco(txtClave2)) {
                alert('Campo requerido');
                txtClave2.select();
                txtClave2.focus();
                return false;
            }
            if (txtClave1.value != txtClave2.value) {
                alert('Confirme su contrase�a.');
                txtClave2.select();
                txtClave2.focus();
                return false;
            }
            return true;
        }
        function lostFocus() {
            document.getElementById('<%=btnAgregar.ClientID%>').focus();
            return false;
        }
        function valOnClickAddGlosa() {
            var txtGlosa = document.getElementById('<%= txtGlosa_CapaGlosa.ClientID%>');
            if (txtGlosa.value.length <= 0) {
                alert('Ingrese una descripci�n.');
                txtGlosa.select();
                txtGlosa.focus();
                return false;
            }
            return true;
        }
        function onCapaGlosa(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var index = -1;
            var glosa = '';
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[3].children[0].cells[2].children[0].id == boton.id) {
                        index = (i - 1);
                        glosa = rowElem.cells[11].children[0].cells[3].children[0].value;
                        break;
                    }
                }
            }
            if (index < 0) {
                alert('No se hall� el �ndice del Producto seleccionado.');
                return false;
            }
            onCapa('capaGlosa');
            document.getElementById('<%=hddIndexGlosa_Gv_Detalle.ClientID%>').value = index;
            document.getElementById('<%= txtGlosa_CapaGlosa.ClientID%>').value = glosa;
            document.getElementById('<%= txtGlosa_CapaGlosa.ClientID%>').select();
            document.getElementById('<%= txtGlosa_CapaGlosa.ClientID%>').focus();
            return false;
        }
        function lostFocusConcepto() {
            document.getElementById('<%= btnAddDetalleCobro.ClientID%>').focus();
            return false;
        }
        function calcularTotalConcepto() {
            var grilla = document.getElementById('<%=GV_Concepto.ClientID%>');
            var txtMonto;
            var monto = 0;
            var montoTotal = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    txtMonto = rowElem.cells[2].children[0].cells[1].children[0];
                    monto = parseFloat(txtMonto.value);
                    if (isNaN(monto)) { monto = 0; }
                    montoTotal = montoTotal + monto;
                }
            }
            document.getElementById('<%=txtTotalConcepto.ClientID%>').value = redondear(montoTotal, 2);
            calcularTotales_GV_Detalle(0);
            return false;
        }
        function valOnKeyPress_txtMonto(caja) {
            var grilla = document.getElementById('<%=GV_Concepto.ClientID%>');
            var txtMonto;
            var cboConcepto;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    txtMonto = rowElem.cells[2].children[0].cells[1].children[0];
                    cboConcepto = rowElem.cells[1].children[0].cells[0].children[0];
                    if (txtMonto.id == caja.id) {
                        if (cboConcepto.value.length <= 0 || parseInt(cboConcepto.value) <= 0) {
                            alert('Debe seleccionar un Concepto.');
                            txtMonto.value = 0;
                            return false;
                        } else {
                            break;
                        }

                    }
                }
            }
            return (validarNumeroPuntoPositivo('event'));
        }
        function valOnClick_cboTipoPrecioV() {
            var txtIdCliente = document.getElementById('<%=txtCodigoCliente.ClientID%>');
            if (isNaN(txtIdCliente.value) || txtIdCliente.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                mostrarCapaPersona();
                return false;
            }
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla == null) {
                alert('DEBE INGRESAR PRODUCTOS.');
                return false;
            }
            return true;
        }

        function valOnClick_btnConsultarVolumenVentaMin(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];
                    if (boton.id == rowElem.cells[11].children[0].cells[5].children[0].id) {
                        document.getElementById('<%=hddIndexCambiarTipoPV_GV_Detalle.ClientID%>').value = (i - 1);
                        return true;
                    }
                }
            }
            alert('Problemas en la ubicaci�n de la Fila.');
            return false;
        }
        function valOnClick_btnCambiarTipoPrecioVenta_Detalle() {
            var cboTipoPV = document.getElementById('<%=cboTipoPrecioPV_CambiarDetalle.ClientID%>');
            if (isNaN(parseInt(cboTipoPV.value)) || parseInt(cboTipoPV.value) <= 0 || cboTipoPV.value.length <= 0) {
                alert('Debe seleccionar un Tipo de Precio de Venta.');
                return false;
            }
            return true;
        }
        function valSaveDocumento() {

            //************ VALIDAMOS DESCUENTOS / VOLUMEN VENTA MINIMO
            if (!(valPorcentMaximoDctoxArticulo_OnBlur(null))) {
                return false;
            }

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie de Documento.');
                return false;
            }
            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            if (isNaN(parseInt(cboMoneda.value)) || cboMoneda.value.length <= 0) {
                alert('Debe seleccionar una Moneda.');
                return false;
            }
            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }

            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }


            var txtTotalAPagar = document.getElementById('<%=txtTotalAPagar.ClientID%>');
            if (isNaN(parseFloat(txtTotalAPagar.value)) || txtTotalAPagar.value.length < 0 || parseFloat(txtTotalAPagar.value) < 0) {
                alert('El Total a Pagar debe ser un valor mayor a cero.');
                return false;
            }
            
            

            var monedaEmision = getCampoxValorCombo(cboMoneda, cboMoneda.value);

            //************* Validando cantidades y precios
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                var cantidad = 0;
                var precioFinal = 0;
                var txtCantidad;
                var txtPrecioFinal;
                var porcentDctoMaximo = 0;
                var PrecioLista = 0;
                
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    cantidad = parseFloat(rowElem.cells[3].children[0].cells[0].children[0].value);
                    txtCantidad = rowElem.cells[3].children[0].cells[0].children[0];
                    PrecioLista = parseFloat(rowElem.cells[4].innerText); 
                    if (isNaN(cantidad)) { cantidad = 0; }
                    if (cantidad <= 0) {
                        alert('Ingrese una cantidad mayor a cero.');
                        txtCantidad.select();
                        txtCantidad.focus();
                        return false;
                    }
                    if (isNaN(PrecioLista)) {
                        PrecioLista = 0;
                    }
                    if (PrecioLista <= 0) { 
                        alert('Agrege un producto que contenga un Precio de Lista mayor a cero. No procede la operaci�n.');
                        return false;
                    }
                    
                    txtPrecioFinal = rowElem.cells[9].children[0].cells[0].children[0];
                    precioFinal = parseFloat(txtPrecioFinal.value);
                    if (isNaN(precioFinal)) { precioFinal = 0; }

                    porcentDctoMaximo = parseFloat(rowElem.cells[11].children[0].cells[1].children[0].value);

                    if (precioFinal <= 0 && porcentDctoMaximo < 100) {
                        alert('El Precio Final de Venta debe ser un valor mayor a cero.');
                        txtPrecioFinal.select();
                        txtPrecioFinal.focus();
                        return false;
                    }
                }
            }

            var grilla = document.getElementById('<%=GV_Concepto.ClientID%>');
            if (grilla != null) {
                var txtMonto;
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    txtMonto = rowElem.cells[2].children[0].cells[1].children[0];
                    monto = parseFloat(txtMonto.value);
                    if (isNaN(monto)) { monto = 0; }
                    if (monto <= 0) {
                        alert('El valor debe ser mayor a cero.');
                        txtMonto.select();
                        txtMonto.focus();
                        return false;
                    }
                }
            }

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
            var mensaje = '';
            switch (parseInt(hddFrmModo.value)) {
                case 1: //******** NUEVO
                    mensaje = 'Usted est� registrando un Documento [ Cotizaci�n ] por un valor de ' + monedaEmision + ' ' + txtTotalAPagar.value + '. Desea continuar con la Operaci�n ?';
                    break;
                case 2: //******** EDITAR
                    mensaje = 'Usted est� editando un Documento [ Cotizaci�n ] por un valor de ' + monedaEmision + ' ' + txtTotalAPagar.value + '. Desea continuar con la Operaci�n ?';
                    break;
            }
            if (confirm(mensaje)) {
                document.getElementById('<%=btnGuardar.ClientID %>').style.visibility = 'hidden';
                return true;
            }
            else {
                document.getElementById('<%=btnGuardar.ClientID %>').style.visibility = 'visible';
                return false;
            }
        }

        function valOnChange_cboMoneda() {
            var grillaDetalle = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grillaDetalle != null) {
                alert('Debe quitar el detalle del Documento. No se permite la Operaci�n.');
                document.getElementById('<%=btnAgregar.ClientID%>').select();
                document.getElementById('<%=btnAgregar.ClientID%>').focus();
                return false;
            }
            var grillaConcepto = document.getElementById('<%=GV_Concepto.ClientID%>');
            if (grillaConcepto != null) {
                alert('Debe quitar el detalle por Conceptos Adicionales. No se permite la Operaci�n.');
                document.getElementById('<%=btnAgregar.ClientID%>').select();
                document.getElementById('<%=btnAgregar.ClientID%>').focus();
                return false;
            }
            return true;
        }


        function ExpandCollapse() {
            var collPanel = $find("CollapsiblePanelExtender1");
            if (collPanel.get_Collapsed())
                collPanel.set_Collapsed(false);
            else
                collPanel.set_Collapsed(true);
        }

        function valOnClickBuscar() {

            var collPanel = $find("ctl00_ContentPlaceHolder1_CollapsiblePanelExtender3");
            if (collPanel.get_Collapsed()) { collPanel.set_Collapsed(false); }

            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            } else {
                if (txtCodigoDocumento != null) {
                    txtCodigoDocumento.select();
                    txtCodigoDocumento.focus();
                }
            }
            return false;
        }
        function valOnKeyPressCodigoDoc(txtCodigoDocumento) {
            var key = event.keyCode;
            if (key == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }

        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor v�lido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }
        function valOnClickEditar() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Edici�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite su edici�n.');
                return false;
            }

            return true;
        }

        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para su Anulaci�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya est� ANULADO. No se permite su anulaci�n.');
                return false;
            }
            return confirm('Desea continuar con el Proceso de ANULACI�N del Documento ?');
        }

        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');

        }
        function buscarMaestroObra() {
            document.getElementById('<%=hddBusquedaMaestro.ClientID %>').value = '1';
            mostrarCapaPersona();
            return true;
        }

        function buscarCliente() {
            document.getElementById('<%=hddBusquedaMaestro.ClientID %>').value = '0';
            mostrarCapaPersona();
            return false;
        }
        function valOnClick_btnmostrardetalle() {
            onCapa('capaBuscarProducto_AddProd');
            return false;
        }
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        //**********************************    CALCULO DE EQUIVALENCIA PRODUCTO
        function valOnClick_btnEquivalencia_PR(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];
                    if (boton.id == rowElem.cells[11].children[0].cells[8].children[0].id) {
                        //onCapa('capaEquivalenciaProducto');                                                
                        document.getElementById('<%=hddIndex_GV_Detalle.ClientID%>').value = (i - 1);
                        //document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').select();
                        //document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').focus();
                        return true;
                    }
                }
            }
            alert('Problemas en la ubicaci�n de la Fila.');
            return false;
        }
        function mostrarCapaEquivalencia_PR() {
            onCapa('capaEquivalenciaProducto');
            //document.getElementById('<%=hddIndex_GV_Detalle.ClientID%>').value = (i - 1);
            document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').select();
            document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').focus();
            return false;
        }
        function valOnClick_btnCalcular() {
        
        
            var cboUnidadMedida_Ingreso = document.getElementById('<%=cboUnidadMedida_Ingreso.ClientID %>');
            if (isNaN(parseFloat(cboUnidadMedida_Ingreso.value)) || cboUnidadMedida_Ingreso.value.length <= 0) {
                alert('DEBE SELECCIONAR UNA UNIDAD DE MEDIDA DE INGRESO.');
                return false;
            }
            var cboUnidadMedida_Salida = document.getElementById('<%=cboUnidadMedida_Salida.ClientID %>');
            if (isNaN(parseFloat(cboUnidadMedida_Salida.value)) || cboUnidadMedida_Salida.value.length <= 0) {
                alert('DEBE SELECCIONAR UNA UNIDAD DE MEDIDA DE SALIDA.');
                return false;
            }
            var txtCantidad = document.getElementById('<%=txtCantidad_Ingreso.ClientID %>');
            if (isNaN(parseFloat(txtCantidad.value)) || txtCantidad.value.length <= 0) {
                alert('INGRESE UN VALOR V�LIDO.');
                txtCantidad.select();
                txtCantidad.focus();
                return false;
            }
            return true;
        }
        function valOnClick_btnAceptar_EquivalenciaPR() {
            var grilla = document.getElementById('<%=GV_ResuldoEQ.ClientID %>');
            if (grilla == null) {
                alert('NO SE HA REALIZADO EL C�LCULO RESPECTIVO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            return true;
        }
        //**********************************    FIN  CALCULO DE EQUIVALENCIA PRODUCTO

        function valOnClick_btnImprimir(tipoimpresion) {


            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>');
            var hddTD = document.getElementById('<%=hddTipoDiseno.ClientID %>');

            if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                alert('NO SE TIENE UN DOCUMENTO PARA IMPRESI�N. NO SE PERMITE LA OPERACI�N.');
            } else {
                window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=14&IdDocumento=' + hddIdDocumento.value + '&tipoimpresion=' + tipoimpresion, 'Ventas', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            }

            return false;
        }
        function valOnClick_btnAceptarBuscarDocRefxCodigo() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('Ingrese un Nro. de Serie para la b�squeda.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('Ingrese un Nro. de Documento para la b�squeda.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }
        
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        var ejecucion = document.getElementById('<%=hddCodBarras.ClientID %>').value;
        /////////////////////////////////////////

        function val_CodBarras(valor) {
            ejecucion = valor;
            var codBarras = document.getElementById('<%=txtCodBarrasPrincipal.ClientID %>');
            if (codBarras != null) {
                if (codBarras.value.length > 0) {
                    if (ejecucion == '1') {
                        document.getElementById('<%=btnAceptarCodBarras.ClientID %>').click();
                        return false;
                    }
                }
            }
            setTimeout('val_CodBarras(ejecucion);', 1000);

        }

        function val_CodBarrasCapa(valor) {
            ejecucion = valor;
            var codBarras = document.getElementById('<%=txtCodBarrasCapa.ClientID %>');
            if (codBarras != null) {
                if (codBarras.value.length > 0) {
                    if (ejecucion == '1') {
                        document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID %>').click();
                        return false;
                    }
                }
            }
            setTimeout('val_CodBarras(ejecucion);', 0.000);

        }

        ////////////////////////////////////////

        function read_Lista(obj) {
            var cantidad = 0.00;
            var grillaListaProducto = document.getElementById('<%=DGV_AddProd.ClientID %>');
            var grillaProducto = document.getElementById('<%=GV_Detalle.ClientID %>');

            for (var i = 1; i < grillaListaProducto.rows.length; i++) {
                var rowListaProducto = grillaListaProducto.rows[i];

                if (rowListaProducto.cells[0].children[0].id == obj.id) {

                    //
                    if (grillaProducto != null) {
                        for (var k = 1; k < grillaProducto.rows.length; k++) {
                            var rowProducto = grillaProducto.rows[k];
                            if (rowProducto.cells[11].children[0].cells[6].children[0].value == rowListaProducto.cells[10].children[0].cells[2].children[0].value) {
                                var cantidad = parseFloat(rowProducto.cells[3].children[0].cells[0].children[0].value);
                                if (isNaN(cantidad)) {
                                    obj.value = "0.00";
                                    obj.select();
                                    return false;
                                }
                            }
                        }
                    }
                    //

                }
            }

            if (parseFloat(cantidad) == 0) {
                obj.value = parseFloat(cantidad);
                obj.select();
            } else {
                obj.value = parseFloat(cantidad);
                obj.select();
            }
            return false;
        }
        //
        function onKeyUp_PrecioStockCantidad(opcion) {

            var IdTiendaDocumento = parseInt(document.getElementById('<%=cboTienda.ClientID %>').value);
            var GV_ConsultarStockPrecioxProd = document.getElementById('<%=GV_ConsultarStockPrecioxProd.ClientID %>');
            var cantidad = 0;
            var flete1 = 0;
            var flete2 = 0;
            var idtienda = 0;
            var precioPL = 0;
            var precioPLDocumento = 0;
            var totalFlete = 0;
            var totalCantidad = 0;
            var descripcionFlete = '';

            if (GV_ConsultarStockPrecioxProd != null) {

                for (var k = 1; k < GV_ConsultarStockPrecioxProd.rows.length; k++) {
                    var rowElemk = GV_ConsultarStockPrecioxProd.rows[k];

                    idtienda = parseInt(rowElemk.cells[0].children[0].cells[0].children[0].value);
                    if (idtienda == IdTiendaDocumento) {
                        precioPLDocumento = parseFloat(rowElemk.cells[2].children[0].cells[1].children[0].innerText);
                        if (isNaN(precioPLDocumento)) { precioPLDocumento = 0; }
                    }
                }
                for (var i = 1; i < GV_ConsultarStockPrecioxProd.rows.length; i++) {
                    var rowElem = GV_ConsultarStockPrecioxProd.rows[i];

                    var txtCantidad = rowElem.cells[4].children[0];
                    cantidad = parseFloat(txtCantidad.value);
                    if (isNaN(cantidad)) { cantidad = 0; }

                    var txtflete1 = rowElem.cells[5].children[0];

                    var txtflete2 = rowElem.cells[6].children[0];
                    flete2 = parseFloat(txtflete2.value);
                    if (isNaN(flete2)) { flete2 = 0; }

                    precioPL = parseFloat(rowElem.cells[2].children[0].cells[1].children[0].innerText);
                    if (isNaN(precioPL)) { precioPL = 0; }

                    var stock = parseFloat(rowElem.cells[3].children[0].cells[0].children[0].innerText);
                    if (isNaN(stock)) { stock = 0; }

                    var almacen = rowElem.cells[1].children[0].cells[1].children[0].innerText;

                    switch (opcion) {
                        case '1': // cantidad

                            break;
                        case '2': // flete 2

                            break;
                    }

                    if (cantidad > 0 && precioPL > 0 && stock > 0) {

                        flete1 = cantidad * Math.abs(precioPLDocumento - precioPL);
                        txtflete1.innerText = redondear(flete1, 2);

                        descripcionFlete = descripcionFlete + almacen + ': ' + rowElem.cells[2].children[0].cells[0].children[0].innerText + ' ' + txtflete1.innerText + ' ';

                        totalFlete = totalFlete + (flete1 + flete2);
                        totalCantidad = totalCantidad + cantidad;

                    }

                }
            } //end if
            // Imprimo los datos del algoritmo cristian panta farfan

            document.getElementById('<%=txtStockPrecioTotal.ClientID %>').readOnly = true;
            document.getElementById('<%=txtStockPrecioTotal.ClientID %>').value = redondear(totalFlete, 2);
            document.getElementById('<%=txtStockPrecioCant.ClientID %>').readOnly = true;
            document.getElementById('<%=txtStockPrecioCant.ClientID %>').value = redondear(totalCantidad, 4);
            document.getElementById('<%=hddDescripcionFlete.ClientID %>').value = descripcionFlete;
            return false;
        }
        //
        function CalcularConsultarStockPrecioxProd() {


            var GV_ConsultarStockPrecioxProd = document.getElementById('<%=GV_ConsultarStockPrecioxProd.ClientID %>');
            var StockTotal = 0;
            var StockTransitoTotal = 0;

            if (GV_ConsultarStockPrecioxProd != null) {
                for (var i = 1; i < GV_ConsultarStockPrecioxProd.rows.length; i++) {
                    var rowElem = GV_ConsultarStockPrecioxProd.rows[i];

                    var Stock = parseFloat(rowElem.cells[3].children[0].cells[0].children[0].innerText);
                    if (isNaN(Stock)) { Stock = 0; }

                    StockTotal = StockTotal + Stock;

                    var StockTransito = parseFloat(rowElem.cells[7].children[0].cells[0].children[0].innerText);
                    if (isNaN(StockTransito)) { StockTransito = 0; }

                    StockTransitoTotal = StockTransitoTotal + StockTransito;

                } //end for
            } //end if
            document.getElementById('<%=txtStockTotal.ClientID %>').value = redondear(StockTotal, 4)
            document.getElementById('<%=txtStockTransitoTotal.ClientID %>').value = redondear(StockTransitoTotal, 4)
            return false;
        }
        //
        function onBlur_PrecioStock(caja) {
            if (caja != null) {
                if (isNaN(parseFloat(caja.value))) {
                    caja.select();
                    caja.focus();
                    alert('Debe ingresar un valor v�lido.');
                    return false;
                }
            }

            var GV_ConsultarStockPrecioxProd = document.getElementById('<%=GV_ConsultarStockPrecioxProd.ClientID %>');
            if (GV_ConsultarStockPrecioxProd != null) {
                for (var i = 1; i < GV_ConsultarStockPrecioxProd.rows.length; i++) {
                    var rowElem = GV_ConsultarStockPrecioxProd.rows[i];

                    var Cantidad = parseFloat(rowElem.cells[4].children[0].value);
                    if (isNaN(Cantidad)) { Cantidad = 0; }

                    var flete2 = parseFloat(rowElem.cells[6].children[0].value);
                    if (isNaN(flete2)) { flete2 = 0; }

                    var stock = parseFloat(rowElem.cells[3].children[0].cells[0].children[0].innerText);
                    if (isNaN(stock)) { stock = 0; }

                    var tienda = rowElem.cells[0].children[0].cells[1].children[0].innerText;
                    var almacen = rowElem.cells[1].children[0].cells[1].children[0].innerText;

                    if (Cantidad > 0 && Cantidad > stock) {
                        alert('El almacen [' + almacen + '] de la tienda [' + tienda + '] no tiene stock suficiente.\nNo se permite la operaci�n.');
                        return false;
                    }

                    if (flete2 > 0 && Cantidad <= 0) {
                        alert('No ha ingresado una cantidad para el almacen [' + almacen + '] de la tienda [' + tienda + '].\nNo se permite la operaci�n.');
                        return false;
                    }

                }
            }
            return true;
        }


        function onClick_AddProducto_ConsultarStockPrecio() {
            if (!(onBlur_PrecioStock(null))) {
                return false;
            }
            var totalFlete = 0;
            var totalCantidad = 0;

            totalFlete = parseFloat(document.getElementById('<%=txtStockPrecioTotal.ClientID %>').value);
            if (isNaN(totalFlete)) { totalFlete = 0; }

            totalCantidad = parseFloat(document.getElementById('<%=txtStockPrecioCant.ClientID %>').value);
            if (isNaN(totalCantidad)) { totalCantidad = 0; }

            if (totalFlete <= 0 || totalCantidad <= 0) {
                document.getElementById('<%=hddDescripcionFlete.ClientID %>').value = '';
                offCapa2('capaConsultarStockPrecioxProducto');
                return false;
            }
            return true;
        }

        function onFocus_ReadOnly(caja) {
            caja.readOnly = true;
            return false;
        }
        
    </script>

</asp:Content>
