<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmComisionCab.aspx.vb" Inherits="APPWEB.FrmComisionCab" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" ToolTip="Generar un Doc. Ajuste de Inventario desde un Doc. Toma de Inventario."
                                Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" OnClientClick="return( valOnClick_btnGuardar()  );"
                                Text="Guardar" ToolTip="Guardar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" ToolTip="Cancelar" Width="80px" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                COMISIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Registro" runat="server">
                    <table class="style1">
                        <tr>
                            <td class="SubTituloCelda">
                                DATOS GENERALES
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            Tipo Comisi&oacute;n:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddl_tipoComision" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            Descripci�n:
                                        </td>
                                        <td>
                                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                ID="txtDescripcion" MaxLength="70" runat="server" Font-Bold="true" Width="500px"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            Abv.:
                                        </td>
                                        <td>
                                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                ID="txtAbv" MaxLength="10" runat="server" Font-Bold="true" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            Considerar:
                                        </td>
                                        <td colspan="3">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="chb_Factura" runat="server" Checked="true" CssClass="Texto" Font-Bold="true"
                                                            Text="Factura de Venta" />
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chb_Boleta" runat="server" Checked="true" CssClass="Texto" Font-Bold="true"
                                                            Text="Boleta de Venta" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            Estado:
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rdbEstado" runat="server" CssClass="Texto" Font-Bold="true"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                       <%-- <tr>
                            <td class="SubTituloCelda">
                                COMISIONISTAS
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <asp:Button ID="btnBuscarComisionista" OnClientClick=" return(  valOnClick_btnBuscarComisionista() ); "
                                    Width="150px" runat="server" Text="Buscar Comisionista" 
                                    ToolTip="Buscar a los Comisionistas asignados a este tipo de Comisi�n." 
                                    Visible="False" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GV_Comisionista" runat="server" AutoGenerateColumns="False" 
                                    Width="100%" Visible="False">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" ItemStyle-Height="25px"
                                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:CommandField>
                                        <asp:TemplateField HeaderText="Descripci�n" ItemStyle-Height="25px" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblDescripcion" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Descripcion")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdPersona" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="D.N.I." ItemStyle-Height="25px" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblDni" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Dni")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Perfil" ItemStyle-Height="25px" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblPerfil" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Perfil")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Empresa" ItemStyle-Height="25px" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblEmpresa" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Empresa")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tienda" ItemStyle-Height="25px" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblTienda" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Base Comisi�n">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="gv_ddl_basecomision" runat="server" SelectedValue='<%# DataBinder.Eval(Container.DataItem,"baseComision") %>'>
                                                    <asp:ListItem Selected="True" Value="PV">Venta Final</asp:ListItem>
                                                    <asp:ListItem Value="PL">Precio Lista</asp:ListItem>
                                                    <asp:ListItem Value="SP">Sobre Precio</asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pocentaje">
                                            <ItemTemplate>
                                                <asp:TextBox ID="gv_txt_valcomision" runat="server" Style="text-align: right" onFocus="return ( aceptarFoco(this) );"
                                                    Text='<%# DataBinder.Eval(Container.DataItem,"valPorcentaje","{0:F3}") %>' Width="65px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Estado" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chbEstado" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>' />
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td class="SubTituloCelda">
                                TIPOS DE PRECIOS DE VENTA
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            Tipo de Precio de Venta asignado a la Comisi�n:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoPv" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAgregarTipoPv" runat="server" Width="80px" Text="Agregar" ToolTip="Agregar el Tipo de Precio de Venta" />
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_TipoPrecioVenta" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="btnQuitar_TipoPrecioV" OnClick="valOnClick_btnQuitar_TipoPrecioV"
                                                                runat="server">Quitar</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Descripci�n" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblDescripcion" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TipoPrecioV")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdTipoPV" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoPV")%>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Estado" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chbEstado" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>' />
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td class="SubTituloCelda">
                                RESTRICCI�N - CLIENTES
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold; text-align: center">
                                Seleccione a los clientes sobre los cuales no se comisionar�
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnBuscarRestriccionCliente" runat="server" Text="Buscar Persona"
                                    OnClientClick=" return( valOnClick_btnBuscarRestriccionCliente()  ); " ToolTip="Buscar a los Comisionistas asignados a este tipo de Comisi�n."
                                    Width="150px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Restriccion_Cliente" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" SelectText="Quitar"
                                            ShowSelectButton="True" />
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Descripci�n" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblDescripcion" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Descripcion")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdPersona" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="D.N.I." ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblDni" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Dni")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Perfil" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblPerfil" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Perfil")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Empresa" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblEmpresa" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Empresa")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Tienda" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblTienda" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Estado" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chbEstado" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>' />
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Busqueda" runat="server">
                    <table width="100%">
                        <tr>
                            <td class="SubTituloCelda">
                                COMISIONES REGISTRADAS
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_BuscarComision" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" SelectText="Editar"
                                            ShowSelectButton="True">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:CommandField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Tipo Comsi�n" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="gv_hdd_idTipoComision" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoComision") %>' />
                                                <asp:Label ID="Label1" runat="server" Font-Bold="true" Text='<%# DataBinder.Eval(Container.DataItem,"TipoComision") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Descripci�n" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblDescripcion" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Descripcion")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdComisionCab" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdComisionCab")%>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Abv." ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblAbv" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Abv")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Estado" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chbEstado" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>'
                                                    Enabled="false" />
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdComision" runat="server" Value="" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddOpcionPersona" runat="server" Value="" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 170px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_capaPersona" runat="server">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(offCapa('capaPersona'));" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButtonList ID="rdbTipoBusqueda_Persona" runat="server" CssClass="Texto"
                                Font-Bold="true" RepeatDirection="Horizontal" AutoPostBack="true">
                                <asp:ListItem Value="0">General</asp:ListItem>
                                <asp:ListItem Value="1" Selected="True">Empresa/Tienda</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlBusquedaPersona" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td colspan="5">
                                                                                <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                                    AutoPostBack="false">
                                                                                    <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                                    <asp:ListItem Value="J">Juridica</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="Texto">
                                                                                Raz�n Social / Nombres:
                                                                            </td>
                                                                            <td colspan="4">
                                                                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscar_Persona_General">                                                                                
                                                                                <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                                                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                                    Width="450px"></asp:TextBox>
                                                                                    </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="Texto">
                                                                                D.N.I.:
                                                                            </td>
                                                                            <td>
                                                                            <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscar_Persona_General">
                                                                                <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                                    MaxLength="8" runat="server"></asp:TextBox>
                                                                                    </asp:Panel>
                                                                            </td>
                                                                            <td class="Texto">
                                                                                R.U.C.:
                                                                            </td>
                                                                            <td>
                                                                            <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscar_Persona_General">
                                                                                <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                                    MaxLength="11"></asp:TextBox>
                                                                                    </asp:Panel>
                                                                            </td>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Button ID="btnBuscar_Persona_General" runat="server" Text="Buscar" Width="80px"
                                                                                                ToolTip="Buscar" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Button ID="btnAgregar_Persona_General" runat="server" Text="Agregar" Width="80px"
                                                                                                ToolTip="Agregar" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="Texto">
                                                                                Rol:
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:DropDownList ID="cboRol" runat="server">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                        PageSize="20" Width="100%">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                                                ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:CheckBox ID="chb_Select" runat="server" />
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="IdPersona" HeaderText="C�d." NullDisplayText="0" HeaderStyle-Height="25px"
                                                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                                            <asp:BoundField DataField="Nombre" HeaderText="Nombre / Raz�n Social" NullDisplayText="---"
                                                                                HeaderStyle-Height="25px" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                                                ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                                                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---" HeaderStyle-Height="25px"
                                                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---" HeaderStyle-Height="25px"
                                                                                ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                        ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                                    <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                        ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                                    <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                        runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                            Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                                    <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                        onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_BusquedaPersona_Usuario" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold; text-align: right">
                                                        Empresa:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboEmpresa_BuscarPersona" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="Texto" style="font-weight: bold; text-align: right">
                                                        Tienda:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboTienda_BuscarPersona" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="Texto" style="font-weight: bold; text-align: right">
                                                        Perfil:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboPerfil_BuscarPersona" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarPersona_Usuario" runat="server" Text="Buscar" Width="80px"
                                                            ToolTip="Buscar" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAdd_Persona_Usuario" runat="server" Text="Agregar" Width="80px"
                                                            ToolTip="Agregar los comisionistas seleccionados." />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_BuscarPersona_Usuario" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                PageSize="30" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chb_Select" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="IdPersona" HeaderText="C�digo" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Dni" HeaderText="D.N.I." HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                                    <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Perfil" HeaderText="Perfil" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    </asp:BoundField>
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAdd_Persona_Usuario" />
                <asp:PostBackTrigger ControlID="btnAgregar_Persona_General" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <script language="javascript" type="text/javascript">

        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btnBuscar_Persona_General.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj) {
            var key = event.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btnBuscar_Persona_General.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function mostrarCapaPersona() {
            onCapa('capaPersona');
            /*document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();*/
            return false;
        }
        function valOnClick_btnBuscarComisionista() {
            mostrarCapaPersona();
            document.getElementById('<%=hddOpcionPersona.ClientID%>').value = '0';
            return false;
        }
        function valOnClick_btnBuscarRestriccionCliente() {
            mostrarCapaPersona();
            document.getElementById('<%=hddOpcionPersona.ClientID%>').value = '1';
            return false;
        }
        function valOnClick_btnGuardar() {
            var txtDescripcion = document.getElementById('<%=txtDescripcion.ClientID%>');
            if (CajaEnBlanco(txtDescripcion)) {
                alert('DEBE INGRESAR UNA DESCRIPCI�N. NO SE PERMITE LA OPERACI�N.');
                txtDescripcion.select();
                txtDescripcion.focus();
                return false;
            }       

            return confirm('Desea continuar con la Operaci�n ?');
        }
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
