﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmPruebaDocMerc.aspx.vb" Inherits="APPWEB.FrmPruebaDocMerc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width ="100%">
        <tr>
            <td class ="Label" >
            Tipo  Documento
            </td>
            <td>
            <asp:DropDownList ID ="cboTipoDocumento" runat ="server" >
            <asp:ListItem Value ="9" Text="Factura"></asp:ListItem>
            <asp:ListItem Value ="10" Text="Boleta"></asp:ListItem>
            <asp:ListItem Value ="5" Text="Recibo Ingreso"> </asp:ListItem>
            <asp:ListItem Value ="6" Text="Recibo Egreso"> </asp:ListItem>
            <asp:ListItem Value ="8" Text="Orden Compra"> </asp:ListItem>
            <asp:ListItem Value ="7" Text="Nota Credito"> </asp:ListItem>
            <asp:ListItem Value ="11" Text="Nota Debito"> </asp:ListItem>
            <asp:ListItem Value ="12" Text="Pedido Cliente"> </asp:ListItem>
            <asp:ListItem Value ="13" Text="Cotizacion"> </asp:ListItem>
            <asp:ListItem Value ="14" Text="Guia Remision Remitente"> </asp:ListItem>
            <asp:ListItem Value ="15" Text="Guia Recepcion"> </asp:ListItem>
            
            
            </asp:DropDownList>
            </td>
            <td class="Label">
                Documento
            </td>
            <td>
                <asp:TextBox ID="txtDocumento" runat="server" OnKeyPress="return (AcceptNum(event));"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td>
                <asp:Button ID="btnAceptar" runat="server" Text="aceptar" OnClientClick=" return ( MostrarReporte()  );" />
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
        <td colspan ="10">
            <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
            </iframe>
        </td>
        </tr>
        <tr>
            <td>
            
            </td>
        </tr>
    </table>
   
   <script language  ="javascript"  type="text/javascript">

    
    function MostrarReporte(){
        var Doc = document.getElementById('<%= txtDocumento.ClientID%>').value;
        var tdoc = document.getElementById('<%= cboTipoDocumento.ClientID%>').value;

        frame1.location.href = 'VisorDocMercantiles.aspx?iReporte=' + tdoc + '&IdDocumento=' + Doc; 
        return false;
     
    }

    function AcceptNum(evt) {

        var nav4 = window.Event ? true : false;

        var key = nav4 ? evt.which : evt.keyCode;

        return (key <= 13 || (key >= 48 && key <= 57) || key == 44);

    }

   
   </script>
   </asp:Content>
