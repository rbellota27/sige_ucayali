﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmCosteoImportacionProyectado.aspx.vb" Inherits="APPWEB.frmCosteoImportacionProyectado" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <table align="center" width="100%">
        <tr>
            <td>
                <table align="left">
                    <tr>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <td>
                                    <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" Width="81px" OnClientClick="LimpiarBuscarDoc();LimpiarBuscarGasto();" />
                                </td>
                                <td>
                                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="81px" />
                                </td>
                                <td>
                                    <asp:Button ID="btnGrabar" runat="server" Text="Grabar" Width="81px" OnClientClick="return(valSaveImportacion());" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="81px" />
                                </td>
                                <td>
                                    <asp:Button ID="btnEditar" runat="server" Text="Editar" Width="81px" />
                                </td>
                                <td>
                                    <asp:Button ID="btnAnular" runat="server" Text="Anular" Width="81px" OnClientClick="return(confirm('Esta seguro de anular la importacion.'));" />
                                </td>
                                <td>
                                    <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" Width="81px" Visible="true"
                                        OnClientClick="return ( onClick_Imprimir() );" />
                                </td>
                                <td>
                                    <asp:Button ID="btnExportarExcel" runat="server" Text="Exportar" ToolTip="Exportar el cuadro a excel" />
                                </td>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Costo de Importaci&oacute;n Proyectado
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanelDETALLEDOC" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="PanelCab" runat="server">
                            <table align="center" width="100%">
                                <tr>
                                    <td>
                                        <table align="left">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Empresa:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Tienda:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label6" runat="server" CssClass="Label" Text="Serie:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNroImportacion" CssClass="TextBox_ReadOnly" runat="server" Width="130px"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TituloCelda">
                                        Datos Proveedor
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table align="center">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label3" runat="server" CssClass="Label" Text="Rason Social:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRazonSocial" CssClass="TextBoxReadOnly" runat="server" Width="400px"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Ruc:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRuc" CssClass="TextBoxReadOnly" runat="server" Width="98px" ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td rowspan="2">
                                                    <asp:Button ID="btnBusOrdenCompra" runat="server" Text="Buscar O/C" Width="100px"
                                                        OnClientClick="return(mostrarCapaBuscarOC());" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Dirección:"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="txtDireccion" CssClass="TextBoxReadOnly" runat="server" Width="540px"
                                                        ReadOnly="True" TextMode="MultiLine"></asp:TextBox>
                                                    <asp:HiddenField ID="hddIdProveedor" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TituloCelda">
                                        Detalle de Orden de Compra
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table align="center" width="100%">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td class="Texto">
                                                                Nro O/C:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNroOC" CssClass="TextBoxReadOnly" runat="server" Width="150px"
                                                                    ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td class="Texto">
                                                                Tipo Cambio:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="TbTipoCambioOC" Font-Bold="true" Enabled="false" onKeyPress="return(validarNumeroPuntoPositivo('event'));"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="dgvDetalleOC" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                                <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                                                <asp:LinkButton CssClass="Texto" ID="lkbQuitar" Font-Bold="true" runat="server" OnClick="lkbQuitar_Click">Quitar</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Código">
                                                                <ItemTemplate>
                                                                                <asp:Label ID="lblCodigoProducto" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CodigoProducto") %>'></asp:Label>

                                                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>' />

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Descripcion" DataField="NomProducto" />
                                                            <asp:TemplateField HeaderText="UM">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUM" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NomUM") %>'></asp:Label>
                                                                    <asp:HiddenField ID="hddIdUM" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdUM") %>' />
                                                                    <asp:HiddenField ID="hddIdDetalleDocumento" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdDetalleDocumento") %>' />
                                                                    <asp:HiddenField ID="hddIdDetalleAfecto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdDetalleAfecto") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cant">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtdgvCantOC" runat="server" Width="50px" onblur="return(valBlur(event));"
                                                                        onKeypress="return(validarNumeroPunto(event));" onfocus="return(aceptarFoco(this));"
                                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Peso/Volumen">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtdgvPesoOC" runat="server" Width="50px" onKeyPress="return(validarNumeroPunto(event));"
                                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Peso","{0:F2}")%>'></asp:TextBox>
                                                                    <asp:Label ID="lblAbvMagnitud" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AbvMagnitud") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="P.Unit.">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtdgvPUnitOC" runat="server" Width="40px" onKeyPress="return(validarNumeroPunto(event));"
                                                                        Text='<%#DataBinder.Eval(Container.DataItem,"PrecioUnitario","{0:F2}")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Costo">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtdgvCostoOC" runat="server" Width="60px" onKeyUp="return(calcularTotalOC());"
                                                                        onKeyPress="return(validarNumeroPunto(event));" Text='<%#DataBinder.Eval(Container.DataItem,"Costo","{0:F2}")%>'></asp:TextBox>
                                                                    <asp:HiddenField ID="hddCUniImporte" runat="server" Value="0" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="P.Peso" DataField="" DataFormatString="{0:F2}" />
                                                            <asp:BoundField HeaderText="P.Monto" DataField="" DataFormatString="{0:F2}" />
                                                            <asp:BoundField HeaderText="T.Gasto" DataField="" DataFormatString="{0:F2}" />
                                                            <asp:BoundField HeaderText="G.Uni" DataField="" DataFormatString="{0:F2}" />
                                                            <asp:BoundField HeaderText="C.Imp" DataField="" DataFormatString="{0:F2}" />
                                                            <asp:TemplateField HeaderText="ADVALOREM">
                                                                <ItemTemplate>
                                                                                <asp:TextBox ID="txtArancel" runat="server" Width="55px" onFocus="return ( aceptarFoco(this) );"
                                                                                    onKeyPress=" return ( validarNumeroPuntoPositivo('event') );" Text='<%#DataBinder.Eval(Container.DataItem,"Arancel","{0:F3}")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="C.Uni.Imp" DataField="" DataFormatString="{0:F2}" />
                                                            <asp:BoundField HeaderText="SubTotal.Imp" DataField="" DataFormatString="{0:F2}" />
                                                            <asp:TemplateField HeaderText="C.Uni.Imp">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtdgvCUniImp" runat="server" Width="60px" onKeyPress="return(validarNumeroPunto(event));"
                                                                        Text='<%#DataBinder.Eval(Container.DataItem,"dc_Importe","{0:F2}")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:HiddenField ID="hddTotalPesoOC" runat="server" />
                                                                <asp:HiddenField ID="hddFechaEmisionOC" runat="server" />
                                                            </td>
                                                            <td align="center">
                                                                <asp:Label ID="Label22" runat="server" CssClass="Label" Text="Total O/C:"></asp:Label>
                                                                <asp:TextBox ID="txttotalocOC" CssClass="TextBoxReadOnly" runat="server" Width="80px"
                                                                    ReadOnly="True" onKeypress="return( false );"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label24" runat="server" CssClass="Label" Text="Total Gastos:"></asp:Label>
                                                                <asp:TextBox ID="txttotalgastoOC" CssClass="TextBoxReadOnly" runat="server" Width="80px"
                                                                    ReadOnly="True" onKeypress="return( false );"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <asp:CheckBox ID="chkUpdatePrecioI" runat="server"  CssClass="LabelRojo" 
                                                                    Font-Bold="true" Visible="false"
                                                                    Text="Actualizar precio en la orden de compra" Enabled="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TituloCelda">
                                        Datos Adicionales de Importación
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table align="center">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label27" runat="server" CssClass="Label" Text="Precio Importacion:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cboWorFor" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label8" runat="server" CssClass="Label" Text="Moneda:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cboMoneda" runat="server" Enabled="False">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label9" runat="server" CssClass="Label" Text="Total Contenedores/Camiones:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTotalContenedores" CssClass="" runat="server" Width="150px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label10" runat="server" CssClass="Label" Text="Fecha Salida:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFchSalidad" runat="server" Width="84px"></asp:TextBox>
                                                    <cc1:CalendarExtender runat="server" ID="CalendFchSalidad_CalendarExtender" TargetControlID="txtFchSalidad"
                                                        Format="d">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label11" runat="server" CssClass="Label" Text="Fecha Aduanas:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFchLlegada" runat="server" Width="84px"></asp:TextBox>
                                                    <cc1:CalendarExtender runat="server" ID="txtFchLlegada_CalendarExtender" TargetControlID="txtFchLlegada"
                                                        Format="d">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label12" runat="server" CssClass="Label" Text="Fecha Ing. Almacen:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFchIngMilla" runat="server" Width="84px"></asp:TextBox>
                                                    <cc1:CalendarExtender runat="server" ID="txtFchIngMilla_CalendarExtender" TargetControlID="txtFchIngMilla"
                                                        Format="d">
                                                    </cc1:CalendarExtender>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TituloCelda">
                                        Detalle de Gastos
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table align="center">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="dgvDetalleGasto" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <Columns>
                                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                            <asp:BoundField HeaderText="Cod." DataField="IdConcepto" />
                                                            <asp:BoundField HeaderText="Descripcion" DataField="NomConcepto" />
                                                            <asp:TemplateField HeaderText="Nro Doc">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtNroDocDetalleGasto" runat="server" Width="80px" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'
                                                                        onfocus="return(aceptarFoco(this));"></asp:TextBox>
                                                                    <asp:HiddenField ID="hddIdDetalleConcepto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDetalleConcepto")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Tipo Documento">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="cboTipDocDetalleGasto" runat="server" DataTextField="NomTipoDoc"
                                                                        DataValueField="IdTipoDocumento" DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoDocumento") %>'>
                                                                    </asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Proveedor">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnBuscarProveedor" runat="server" ImageUrl="~/Imagenes/Busqueda.JPG"
                                                                        AlternateText="Buscar Proveedor." OnClientClick="return (mostrarCapaProveedor(this));" />
                                                                    <asp:HiddenField ID="hdd_idproveedor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProveedor")%>' />
                                                                    <asp:Label ID="lblnomproveedor" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RazonSocial")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Moneda">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hdd_idmoneda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdMoneda") %>' />
                                                                    <asp:Label ID="lblMonSimboloDetalleGasto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonSimbolo")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Calculo" DataField="NomCalculo" />
                                                            <asp:TemplateField HeaderText="Sub Total">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtSubTotalDetalleGasto" runat="server" Width="80px" onKeyUp="return(calcularTotalGasto());"
                                                                        onKeyPress="return(validarNumeroPunto(event));" Text='<%#DataBinder.Eval(Container.DataItem,"SubTotalGasto","{0:F2}")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    </asp:GridView>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnBuscarGasto" runat="server" Text="Buscar Gasto" OnClientClick="return(mostrarCapaBuscarGasto());"
                                                        Width="100px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label25" runat="server" CssClass="Label" Text="Gastos x Peso:"></asp:Label>
                                                                <asp:TextBox ID="txttotalpesoG" Width="80px" CssClass="TextBoxReadOnly" runat="server"
                                                                    ReadOnly="True" onKeypress="return( false );"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label26" runat="server" CssClass="Label" Text="Gastos x Moneda:"></asp:Label>
                                                                <asp:TextBox ID="txttotalmonedaG" Width="80px" CssClass="TextBoxReadOnly" runat="server"
                                                                    ReadOnly="True" onKeypress="return( false );"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label21" runat="server" CssClass="Label" Text="Total Gastos:"></asp:Label>
                                                                <asp:TextBox ID="txtTotalGastoDetalleG" Width="80px" CssClass="TextBoxReadOnly" runat="server"
                                                                    ReadOnly="True" onKeypress="return( false );"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TituloCelda">
                                        Resumen
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table align="center">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label14" runat="server" CssClass="Label" Text="Peso Total:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPesoTotal" CssClass="TextBoxReadOnly" runat="server" Width="100px"
                                                        ReadOnly="True" onKeypress="return( false );"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label15" runat="server" CssClass="Label" Text="Total O/C:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTotalOC" CssClass="TextBoxReadOnly" runat="server" Width="100px"
                                                        ReadOnly="True" onKeypress="return( false );"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Total Gastos:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTotalGastos" CssClass="TextBoxReadOnly" runat="server" Width="100px"
                                                        ReadOnly="True" onKeypress="return( false );"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label17" runat="server" CssClass="Label" Text="Total Importación:"></asp:Label>
                                                </td>
                                                <td style="margin-left: 120px">
                                                    <asp:TextBox ID="txtTotalImportacion" CssClass="TextBoxReadOnly" runat="server" Width="100px"
                                                        ReadOnly="True" onKeypress="return( false );"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <input id="btnCalcularCostos" type="button" runat="server" value="Calcular Costos"
                                                                onclick="calcularTotalPesoOC(1);calcularPpeso();calculartotalgastotxt();calcularResumen();totalimportacion();" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <table>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="2101100003" />
                                    <asp:HiddenField ID="hddModo" runat="server" Visible="False" />
                                    <asp:HiddenField ID="hddIdDocumento" runat="server" />
                                    <asp:HiddenField ID="hddindex" runat="server" Value="0" />
                                    <asp:HiddenField ID="hddRelacDoc2" runat="server" Visible="False" />
                                    <asp:HiddenField ID="hddIdTipoOperacionv2" runat="server" Visible="False" />
                                    <asp:HiddenField ID="hddTotalImportacion" runat="server" Value="0" />
                                    <asp:HiddenField ID="hddTotalPeso" runat="server" Value="0" />
                                    <asp:HiddenField ID="hddIndicador" runat="server" Value="0" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelBuscar" runat="server">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" runat="server" CssClass="Label" Text="Serie:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSerieBusImp" runat="server" Width="50px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="Label29" runat="server" CssClass="Label" Text="Número:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCodigoBusImp" runat="server" Width="90px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="Label30" runat="server" CssClass="Label" Text="Proveedor:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="cboProveedorBusImp" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnOkBusImp" runat="server" Text="OK" />
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td>
                                <asp:GridView ID="dgvListImport" runat="server" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" />
                                        <asp:TemplateField HeaderText="Nro Importacion">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hddIdDocBusImp" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdDocumento") %>' />
                                                <asp:Label ID="Label31" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"IdImportacion") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Empresa" DataField="NomEmpresa" />
                                        <asp:BoundField HeaderText="Tienda" DataField="NomTienda" />
                                        <asp:BoundField HeaderText="Proveedor" DataField="RazonSocial" />
                                        <asp:BoundField HeaderText="Ruc" DataField="Ruc" />
                                        <asp:BoundField HeaderText="Fecha Emision" DataField="FechaEmision" DataFormatString="{0:d}" />
                                        <asp:BoundField HeaderText="Fecha Emision O/C" DataField="FechaEmisionOC" DataFormatString="{0:d}" />
                                        <asp:BoundField HeaderText="Nro O/C" DataField="NroDoc" />
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:Table ID="tablaPrincipal" runat="server">
                </asp:Table>
            </td>
        </tr>
    </table>
    <div id="capaBuscarOC" style="border: 3px solid blue; padding: 4px; width: 900px;
        height: auto; position: absolute; top: 203px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanelBuscarP" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrarCapaOC" runat="server" OnClientClick="return(offCapa('capaBuscarOC'));"
                                ImageUrl="~/Imagenes/Cerrar.gif" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="Texto">
                                        Tipo Cambio:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbTipoCambio" Font-Bold="true" onKeyPress="return(validarNumeroPuntoPositivo('event'));"
                                            runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="Texto">
                                        Proveedor:
                                    </td>
                                    <td colspan="4">
                                        <asp:DropDownList ID="cboProveedorCapaOC" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Texto">
                                        Serie:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtNroSeriecapaOC" Width="50px" onfocus="return(aceptarFoco(this));"
                                            onKeyPress="return(onKeyPressEsNumero('event'));" runat="server" MaxLength="4"></asp:TextBox>
                                    </td>
                                    <td class="Texto">
                                        N&uacute;mero:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtNroCorrelativocapaOC" Width="90px" onfocus="return(aceptarFoco(this));"
                                            onKeyPress="return(onKeyPressEsNumero('event'));" runat="server" MaxLength="12"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnBuscarOCcapa" runat="server" Text="Buscar" Width="85px" />
                                        <asp:Button ID="btnAñadirOCcapa" runat="server" Text="Añadir" OnClientClick="return( valAddOC());"
                                            Width="85px" />
                                        <input id="btnLimpiarOCcapa" type="button" value="Limpiar" onclick="return(LimpiarBuscarDoc());" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="dgvCapaOC" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                Width="100%" PageSize="15">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelectOC" runat="server" onClick="return(valSelectOC('0',this));" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="IdDocumento" HeaderText="Id" NullDisplayText="0" />
                                    <asp:BoundField DataField="NroOrdenCompra" HeaderText="Nro O/C" NullDisplayText="" />
                                    <asp:BoundField DataField="NomTipoDoc" HeaderText="Tipo Doc." NullDisplayText="" />
                                    <asp:BoundField DataField="MonSimbolo" HeaderText="Moneda." NullDisplayText="" />
                                    <asp:BoundField DataField="FechaEmision" HeaderText="Fecha" NullDisplayText="" DataFormatString="{0:d}" />
                                    <asp:BoundField DataField="RazonSocial" HeaderText="Proveedor" NullDisplayText="" />
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hddIdTipoOperacion" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoOperacion") %>' />
                                            <asp:HiddenField ID="hddworthfob" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTPImp") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="IdImportacion" HeaderText="Nro Importación" />
                                    <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                                DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnAnterior_OC" runat="server" Font-Bold="true" Width="40px" Text="<"
                                ToolTip="Página Anterior" OnClientClick="return(valNavegacionOC('0'));" />
                            <asp:Button ID="btnPosterior_OC" runat="server" Font-Bold="true" Width="40px" Text=">"
                                ToolTip="Página Posterior" OnClientClick="return(valNavegacionOC('1'));" />
                            <asp:TextBox ID="txtPageIndex_OC" Width="40px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                runat="server"></asp:TextBox>
                            <asp:Button ID="btnIr_OC" runat="server" Font-Bold="true" Width="40px" Text="Ir"
                                ToolTip="Ir a la Página" OnClientClick="return(valNavegacionOC('2'));" />
                            <asp:TextBox ID="txtPageIndexGO_OC" Width="40px" CssClass="TextBox_ReadOnly" runat="server"
                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAñadirOCcapa" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div id="capaBuscarGasto" style="border: 3px solid blue; padding: 8px; width: 550px;
        height: auto; position: absolute; top: 203px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <input id="btnCerrarCapaGasto" type="button" value="Cerrar" onclick="return(offCapa('capaBuscarGasto'));"
                                style="width: 80px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label23" runat="server" CssClass="Label" Text="Tipo Gasto:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboTipoGastoCapaG" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDescripcionCapaG" Width="150px" onfocus="return(aceptarFoco(this));"
                                            runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnBuscarGastoCapaG" runat="server" Text="Buscar" OnClientClick="return(valBuscarGasto());" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnAñadirGastoCapaG" runat="server" Text="Añadir" OnClientClick="return(valAddGasto());" />
                                    </td>
                                    <td>
                                        <input id="btnLimpiarCastoCapaG" type="button" value="Limpiar" onclick="return(LimpiarBuscarGasto());" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="dgvGastoCapa" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                Width="100%" PageSize="15">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelectGasto" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="IdConcepto" HeaderText="Id" NullDisplayText="0" />
                                    <asp:BoundField DataField="NomConcepto" HeaderText="Descripción" NullDisplayText="" />
                                    <asp:BoundField DataField="SimboloMoneda" HeaderText="Moneda" NullDisplayText="" />
                                    <asp:BoundField DataField="CtDoc_Valor" HeaderText="Valor" NullDisplayText="" DataFormatString="{0:F2}" />
                                    <asp:BoundField DataField="con_TipoCalculo" HeaderText="Calculo" NullDisplayText="" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnAnterior_Gasto" runat="server" Font-Bold="true" Width="40px" Text="<"
                                ToolTip="Página Anterior" OnClientClick="return(valNavegacionGasto('0'));" />
                            <asp:Button ID="btnPosterior_Gasto" runat="server" Font-Bold="true" Width="40px"
                                Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionGasto('1'));" />
                            <asp:TextBox ID="txtPageIndex_Gasto" Width="40px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                runat="server"></asp:TextBox>
                            <asp:Button ID="btnIr_Gasto" runat="server" Font-Bold="true" Width="40px" Text="Ir"
                                ToolTip="Ir a la Página" OnClientClick="return(valNavegacionGasto('2'));" />
                            <asp:TextBox ID="txtPageIndexGO_Gasto" Width="40px" CssClass="TextBox_ReadOnly" runat="server"
                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAñadirGastoCapaG" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div id="verBusquedaProveedor" style="border: 3px solid blue; padding: 2px; width: 900px;
        height: auto; position: absolute; top: 200px; left: 25px; background-color: white;
        z-index: 2; display: none;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="imgCerraCapaProveedor" runat="server" ImageUrl="~/Imagenes/Cerrar.gif"
                        OnClientClick="return(offCapa('verBusquedaProveedor'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                    AutoPostBack="false">
                                    <asp:ListItem Value="N">Natural</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Buscar Proveedor:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbcondicion" runat="server" RepeatDirection="Horizontal"
                                    CssClass="Texto" Style="cursor: hand;" onClick="return ( onChange_txtBuscarProveedorFocus() );">
                                    <asp:ListItem Value="1" Text="Nacional" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Extranjero"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Razon Social / Nombres:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtBuscarProveedor" onkeyPress="return ( onKeyPressProveedor(this,event) );"
                                    runat="server" Width="400px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtbuscarRucProveedor" runat="server" onkeyPress="return ( onKeyPressProveedor(this,event) );"
                                    MaxLength="11"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btBuscarProveedor" runat="server" Text="Buscar" Style="cursor: hand;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="gvProveedor" runat="server" AutoGenerateColumns="False" HeaderStyle-Height="25px"
                        Width="100%" RowStyle-Height="25px">
                        <Columns>
                            <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="True">
                            </asp:CommandField>
                            <asp:BoundField DataField="idpersona" HeaderText="Cód." NullDisplayText="0"></asp:BoundField>
                            <asp:BoundField DataField="RazonSocial" HeaderText="Razón Social" NullDisplayText="---" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---" />
                            <asp:BoundField DataField="Direccion" HeaderText="Dirección" NullDisplayText="---">
                            </asp:BoundField>
                            <asp:BoundField DataField="Telefeono" HeaderText="Teléfono" NullDisplayText="---" />
                            <asp:BoundField DataField="Correo" HeaderText="Correo" NullDisplayText="---">
                                <ItemStyle />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddidagente" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoAgente") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Button ID="btAnterior_Proveedor" runat="server" Width="50px" Text="<" ToolTip="Página Anterior"
                        OnClientClick="return(valNavegacionProveedor('0'));" Style="cursor: hand;" />
                    <asp:Button ID="btSiguiente_Proveedor" runat="server" Width="50px" Text=">" ToolTip="Página Posterior"
                        OnClientClick="return(valNavegacionProveedor('1'));" Style="cursor: hand;" />
                    <asp:TextBox ID="tbPageIndex_Proveedor" Width="50px" CssClass="TextBoxReadOnly_Right"
                        runat="server"></asp:TextBox><asp:Button ID="btIr_Proveedor" runat="server" Width="50px"
                            Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionProveedor('2'));"
                            Style="cursor: hand;" />
                    <asp:TextBox ID="tbPageIndexGO_Proveedor" Width="50px" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript" language="javascript">
        function mostrarCapaBuscarOC() {
            onCapa('capaBuscarOC');
            document.getElementById('<%=txtNroSeriecapaOC.ClientID%>').select();
            document.getElementById('<%=txtNroSeriecapaOC.ClientID %>').focus();
            return false;

        }
        function valAddOC() {
            var tbTipoCambio = document.getElementById('<%=tbTipoCambio.ClientID %>').value;
            if (isNaN(tbTipoCambio) || tbTipoCambio.length == 0) {
                alert('Debe Ingresar un Tipo de Cambio.');
                return false;
            }
            var grilla = document.getElementById('<%=dgvCapaOC.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('Seleccione Orden de compra.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                if (rowElem.cells[0].children[0].getElementsByTagName('INPUT')[0].checked == true) {
                    cont = cont + 1;
                }
            }
            if (cont == 0) {
                alert('Seleccione Orden de compra.');
                return false;
            }
            return true;
        }
        function valNavegacionOC(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_OC.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=txtNroSeriecapaOC.ClientID%>').select();
                document.getElementById('<%=txtNroSeriecapaOC.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_OC.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGO_OC.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_OC.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_OC.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_OC.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function LimpiarBuscarDoc() {
            var cajaSerie = document.getElementById('<%= txtNroSeriecapaOC.ClientID%>');
            var cajaCodigo = document.getElementById('<%= txtNroCorrelativocapaOC.ClientID%>');
            var cboProveedor = document.getElementById('<%=cboProveedorCapaOC.ClientID %>').value = 0;
            var grilla = document.getElementById('<%=dgvCapaOC.ClientID %>');
            LimpiarCaja(cajaSerie);
            LimpiarCaja(cajaCodigo);
            return true;
        }

        function valBlur(event) {
            var event_element = e.target ? e.target : e.srcElement;
            var id = event_element.id;
            var caja = document.getElementById(id);
            if (CajaEnBlanco(caja)) {
                caja.value = 0;
            }
            if (!esDecimal(caja.value)) {
                caja.select();
                caja.focus();
                alert('Valor no válido.');
            }
        }

        function mostrarCapaBuscarGasto() {
            onCapa('capaBuscarGasto');
            document.getElementById('<%=cboTipoGastoCapaG.ClientID %>').focus();
            return false;

        }
        function valAddGasto() {
            var grilla = document.getElementById('<%=dgvGastoCapa.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('Seleccione Gasto.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                if (rowElem.cells[0].getElementsByTagName('INPUT')[0].checked == true) {
                    cont = cont + 1;
                }
            }
            if (cont == 0) {
                alert('Seleccione Gasto.');
                return false;
            }
            return true;
        }
        function valNavegacionGasto(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Gasto.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=cboTipoGastoCapaG.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Gasto.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGO_Gasto.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Gasto.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Gasto.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Gasto.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function valBuscarGasto() {
            var cboTipoGasto = document.getElementById('<%=cboTipoGastoCapaG.ClientID %>');
            if (parseFloat(cboTipoGasto.value) == 0) {
                alert('Seleccione Tipo de Gasto.');
                return false;
            }
            return true;
        }

        function LimpiarBuscarGasto() {
            var cboTipoGasto = document.getElementById('<%=cboTipoGastoCapaG.ClientID %>').value = 0;
            var cajaDescripcion = document.getElementById('<%= txtDescripcionCapaG.ClientID%>');
            var grilla = document.getElementById('<%=dgvGastoCapa.ClientID %>');
            LimpiarCaja(cajaDescripcion);
            return true;
        }

        function calcularTotalGasto() {

            var totalgasto = 0;
            var grilla = document.getElementById('<%=dgvDetalleGasto.ClientID %>');

            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];

                var subtotal = redondear(parseFloat(rowElem.cells[8].children[0].value), 2);
                if (isNaN(subtotal)) {
                    subtotal = 0;
                }
                totalgasto = totalgasto + subtotal;

            } //FIM DEL FOR

            //mostrar resultados
            document.getElementById('<%=txtTotalGastoDetalleG.ClientID %>').value = redondear(totalgasto, 2);
            //************************** imprimo en la sección Detalle de gastos
            return true;
        }
        function calcularTotalOC() {

            var totaloc = 0;
            var grilla = document.getElementById('<%=dgvDetalleOC.ClientID %>');

            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];

                var costo = redondear(parseFloat(rowElem.cells[7].children[0].value), 2);
                if (isNaN(costo)) {
                    costo = 0;
                }
                totaloc = totaloc + costo;

            } //FIM DEL FOR

            //mostrar resultados
            document.getElementById('<%=txttotalocOC.ClientID %>').value = redondear(totaloc, 2);
            //************************** imprimo en la sección Detalle de gastos
            return true;
        }

        function calcularTotalPesoOC(validar) {
            var grillaOC = document.getElementById('<%= dgvDetalleOC.ClientID%>');
            var grillag = document.getElementById('<%= dgvDetalleGasto.ClientID%>');
            //            var Check = document.getElementById('<%= chkUpdatePrecioI.ClientID%>');

            if (grillaOC == null || grillag == null) {
                alert('No existe información para calcular el costo.');
                return false;
            }

            for (var x = 1; x < grillag.rows.length; x++) {
                var rowElem = grillag.rows[x];
                var valsubtotal = 0;
                var valsubtotal = redondear(parseFloat(rowElem.cells[8].children[0].value), 2);
                if (isNaN(valsubtotal) || (valsubtotal == 0)) {
                    alert('El Subtotal de los conceptos debe ser mayor a 0.');
                    return false;
                }
                var validmoneda = rowElem.cells[6].children[0].value;
                if (validmoneda != 2) {
                    alert('La moneda de los conceptos deben de estar en Dolares, verificar información.');
                    return false;
                }

            }

            var totalpesooc = 0;
            var grilla = document.getElementById('<%=dgvDetalleOC.ClientID %>');

            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];

                var peso = redondear(parseFloat(rowElem.cells[5].children[0].value), 2);
                if (isNaN(peso) || (peso == 0)) {
                    //alert('El peso debe ser mayor a 0.');
                    peso = 0;
                }

                if (validar != null) {
                    if (validar == 1) {
                        if (isNaN(peso)) { peso = 0; }
                        if (peso <= 0) {
                            alert('INGRESE EL PESO DEL PRODUCTO < ' + rowElem.cells[2].innerText + ' >. NO SE PERMITE LA OPERACIÓN.');
                            return false;
                        }
                    }
                }
                totalpesooc = totalpesooc + peso;

            } //FIM DEL FOR

            //mostrar resultados
            xtotalpesooc = redondear(totalpesooc, 2);
            if (isNaN(xtotalpesooc)) {
                xtotalpesooc = 0;
            }
            document.getElementById('<%=hddTotalPesoOC.ClientID %>').value = xtotalpesooc;
            //************************** imprimo en la sección Detalle de gastos
            // document.getElementById('<%=chkUpdatePrecioI.ClientID %>').checked = true;
           //modificadomedina
            return true;

        }
        function calcularPpeso() {
            var hddindicador = document.getElementById('<%= hddIndicador.ClientID%>');
            var grillaOC = document.getElementById('<%= dgvDetalleOC.ClientID%>');
            var grillag = document.getElementById('<%= dgvDetalleGasto.ClientID%>');
            if (grillaOC == null || grillag == null) {
                return false;
            }

            var grillagastos = document.getElementById('<%=dgvDetalleGasto.ClientID %>');
            // prorrateo peso
            var totalp = 0;
            var valp = 0;
            for (var x = 1; x < grillagastos.rows.length; x++) {
                var rowElem = grillagastos.rows[x];
                var p = rowElem.cells[7].innerHTML;
                if (p == 'Peso') {
                    valp = parseFloat(rowElem.cells[8].children[0].value);
                }
                else {
                    valp = 0;
                }
                totalp = totalp + valp;
            }
            // prorrateo monto
            var totalm = 0;
            var valm = 0;
            for (var xm = 1; xm < grillagastos.rows.length; xm++) {
                var rowElem = grillagastos.rows[xm];
                var m = rowElem.cells[7].innerHTML;
                if (m == 'Moneda') {
                    valm = parseFloat(rowElem.cells[8].children[0].value);
                }
                else {
                    valm = 0;
                }
                totalm = totalm + valm;
            }
            //*****************************************************************************************************//
            var grilla = document.getElementById('<%=dgvDetalleOC.ClientID %>');
            var totalpesooc = parseFloat(document.getElementById('<%=hddTotalPesoOC.ClientID %>').value);
            var totalcostooc = parseFloat(document.getElementById('<%=txttotalocOC.ClientID %>').value);

            if (!esDecimal(totalpesooc) || isNaN(parseFloat(totalpesooc))) {
                totalpesooc = 0;
            }
            if (!esDecimal(totalcostooc) || isNaN(parseFloat(totalcostooc))) {
                totalcostooc = 0;
            }

            //var acumtotal = 0;
            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];

                var pesoxprod = parseFloat(rowElem.cells[5].children[0].value);
                if (!esDecimal(pesoxprod) || isNaN(parseFloat(pesoxprod))) {
                    pesoxprod = 0;
                }
                //costo total OC
                var costoxprod = parseFloat(rowElem.cells[7].children[0].value);
                if (!esDecimal(costoxprod) || isNaN(parseFloat(costoxprod))) {
                    costoxprod = 0;
                }

                //p. peso
                xpeso = redondear(((pesoxprod * totalp) / totalpesooc), 2);
                if (isNaN(xpeso)) {
                    xpeso = 0;
                }
                rowElem.cells[8].innerHTML = xpeso;

                // p. monto
                xmonto = redondear((costoxprod * totalm / totalcostooc), 2);
                if (isNaN(xmonto)) {
                    xmonto = 0;
                }
                rowElem.cells[9].innerHTML = xmonto;
                // t. gasto
                xtgasto = redondear((pesoxprod * totalp / totalpesooc) + (costoxprod * totalm / totalcostooc), 2);
                if (isNaN(xtgasto)) {
                    xtgasto = 0;
                }
                rowElem.cells[10].innerHTML = xtgasto;
                // g. unitario
                var cantxprod = parseFloat(rowElem.cells[4].children[0].value);
                xgunitario = redondear(((pesoxprod * totalp / totalpesooc) + (costoxprod * totalm / totalcostooc)) / (cantxprod), 2);
                if (isNaN(xgunitario)) {
                    xgunitario = 0;
                }
                rowElem.cells[11].innerHTML = xgunitario;
                // c. importe
                xcimporte = redondear(costoxprod / cantxprod, 2);
                if (isNaN(xcimporte)) {
                    xcimporte = 0;
                }
                rowElem.cells[12].innerHTML = xcimporte;

                var arancel = 0;
                arancel = parseFloat(rowElem.cells[13].children[0].value);
                if (isNaN(arancel)) { arancel = 0; }


                // c.uni.imp
                xcuniimp = redondear((((pesoxprod * totalp / totalpesooc) + (costoxprod * totalm / totalcostooc)) / (cantxprod)) + (costoxprod / cantxprod), 2);
                if (isNaN(xcuniimp)) {
                    xcuniimp = 0;
                }
                rowElem.cells[14].innerHTML = redondear((xcuniimp + arancel), 2);

                //SE AGREGO ESTA COLUMNA NUEVA C.UNI.IMP EDITABLE
                //hddIndicador
                if (parseFloat(hddindicador.value) == 0) {
                    rowElem.cells[16].children[0].value = redondear((xcuniimp + arancel), 2);
                }

                xcimp2 = redondear((((pesoxprod * totalp / totalpesooc) + (costoxprod * totalm / totalcostooc)) / (cantxprod)) + (costoxprod / cantxprod), 2);
                if (isNaN(xcimp2)) {
                    xcimp2 = 0;
                }
                rowElem.cells[7].children[1].value = xcimp2;

                // subtotal.imp
                xsubtimp = redondear(((((pesoxprod * totalp / totalpesooc) + (costoxprod * totalm / totalcostooc)) / (cantxprod)) + (costoxprod / cantxprod)) * cantxprod, 2);
                if (isNaN(xsubtimp)) {
                    xsubtimp = 0;
                }
                rowElem.cells[15].innerHTML = xsubtimp;
                //*********************************************

                document.getElementById('<%=txttotalpesoG.ClientID %>').value = redondear(totalp, 2);
                document.getElementById('<%=txttotalmonedaG.ClientID %>').value = redondear(totalm, 2);

                document.getElementById('<%=hddTotalPeso.ClientID %>').value = redondear(totalp, 2);

                //acumtotal = acumtotal + parseFloat(rowElem.cells[12].innerHTML)

            }
            return true;
        }
        function calculartotalgastotxt() {
            var grillaOC = document.getElementById('<%= dgvDetalleOC.ClientID%>');
            var grillag = document.getElementById('<%= dgvDetalleGasto.ClientID%>');
            if (grillaOC == null || grillag == null) {
                return false;
            }

            var totalgastos = 0;
            var grilla = document.getElementById('<%=dgvDetalleOC.ClientID %>');
            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                var tgastos = redondear(parseFloat(rowElem.cells[10].innerHTML), 2);
                if (isNaN(tgastos)) {
                    tgastos = 0;
                }
                totalgastos = totalgastos + tgastos;

                xtotalgasto = redondear(totalgastos, 2);
                if (isNaN(xtotalgasto)) {
                    xtotalgasto = 0;
                }
                document.getElementById('<%=txttotalgastoOC.ClientID %>').value = xtotalgasto;
            }
            return true;
        }
        function calcularResumen() {
            var totalpeso = document.getElementById('<%=hddTotalPesoOC.ClientID %>').value;
            var totaloc = document.getElementById('<%=txttotalocOC.ClientID %>').value;
            var totalgasto = document.getElementById('<%=txttotalgastoOC.ClientID %>').value;

            document.getElementById('<%=txtPesoTotal.ClientID %>').value = redondear(totalpeso, 2);
            document.getElementById('<%=txtTotalOC.ClientID %>').value = redondear(totaloc, 2);
            document.getElementById('<%=txtTotalGastos.ClientID %>').value = redondear(totalgasto, 2);
            return true;
        }
        function totalimportacion() {
            var totaloc1 = document.getElementById('<%=txttotalocOC.ClientID %>').value;
            var totalgasto1 = document.getElementById('<%=txttotalgastoOC.ClientID %>').value;
            if (isNaN(parseFloat(totaloc1))) {
                totaloc1 = 0;
            }
            if (isNaN(parseFloat(totalgasto1))) {
                totalgasto1 = 0;
            }

            document.getElementById('<%=txtTotalImportacion.ClientID %>').value = redondear(parseFloat(totaloc1) + parseFloat(totalgasto1), 2);

            document.getElementById('<%=hddTotalImportacion.ClientID %>').value = redondear(parseFloat(totaloc1) + parseFloat(totalgasto1), 2);

            return true;
        }


        function mostrarCapaProveedor(boton) {
            var grilla = document.getElementById('<%=dgvDetalleGasto.ClientID %>');
            var index = document.getElementById('<%=hddindex.ClientID %>');

            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (rowElem.cells[5].children[0].id == boton.id) {
                    index.value = i - 1;
                }
            }

            onCapa('verBusquedaProveedor');

            return false;
        }

        function valSaveImportacion() {
            var cajaIdProveedor = document.getElementById('<%= hddIdProveedor.ClientID%>');
            if (CajaEnBlanco(cajaIdProveedor) == true) {
                alert('Seleccionar un proveedor.');
                return false;
            }
            var cajaNroImportacion = document.getElementById('<%= txtNroImportacion.ClientID%>');
            if (CajaEnBlanco(cajaNroImportacion) == true) {
                alert('Nro importación Campo requerido.');
                cajaNroImportacion.focus();
                return false;
            }
            var cajaNroOC = document.getElementById('<%= txtNroOC.ClientID%>');
            if (CajaEnBlanco(cajaNroOC) == true) {
                alert('Nro orden compra Campo requerido.');
                cajaNroOC.focus();
                return false;
            }
            var cboidmoneda = document.getElementById('<%= cboMoneda.ClientID%>');
            if (cboidmoneda.value == 0) {
                alert('Debe seleccionar moneda');
                return false;
            }
            //    var cajaNroCont = document.getElementById('<%= txtTotalContenedores.ClientID%>');
            //    if (CajaEnBlanco(cajaNroCont) == true) {
            //        alert('Ingrese nro contenedores.');
            //        cajaNroCont.focus();
            //        return false;
            //    }
            //    var cajaFchSalida = document.getElementById('<%= txtFchSalidad.ClientID%>');
            //    if (CajaEnBlanco(cajaFchSalida) == true) {
            //        alert('Ingrese fecha salida.');
            //        cajaFchSalida.focus();
            //        return false;
            //    }
            //    var cajaFchLlegada = document.getElementById('<%= txtFchLlegada.ClientID%>');
            //    if (CajaEnBlanco(cajaFchLlegada) == true) {
            //        alert('Ingrese fecha llegada.');
            //        cajaFchLlegada.focus();
            //        return false;
            //    }
            //    var cajaFchMilla = document.getElementById('<%= txtFchIngMilla.ClientID%>');
            //    if (CajaEnBlanco(cajaFchMilla) == true) {
            //        alert('Ingrese fecha de ingreso milla.');
            //        cajaFchMilla.focus();
            //        return false;
            //    }
            var cboworfor = document.getElementById('<%= cboWorFor.ClientID%>');
            if (cboworfor.value == 0) {
                alert('Debe seleccionar precio importación');
                return false;
            }
            //    var cajatotalimp = document.getElementById('<%= txtTotalImportacion.ClientID%>');
            //    if (CajaEnBlanco(cajatotalimp) == true) {
            //        alert('Presionar el botón Calcular Costo.');
            //        return false;
            //    }

            var grilla = document.getElementById('<%=dgvDetalleGasto.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var nrodoc = rowElem.cells[3].children[0].value;
                    //            if (isNaN(nrodoc) || nrodoc == '') {
                    //                alert('Ingrese un Nro Documento, detalle gasto.');
                    //                rowElem.cells[3].children[0].select();
                    //                rowElem.cells[3].children[0].focus();
                    //                return false;
                    //            }
                    //            var cbotipdoc = rowElem.cells[4].children[0].value;
                    //            if (isNaN(cbotipdoc) || cbotipdoc == 0) {
                    //                alert('Seleccione tipo documento, detalle gasto.');                
                    //                rowElem.cells[4].children[0].focus();
                    //                return false;
                    //            }
                    //            var proveedor = rowElem.cells[5].children[1].value;
                    //            if (isNaN(proveedor) || proveedor == 0) {
                    //                alert('buscar un proveedor, detalle gasto.');
                    //                rowElem.cells[5].children[0].focus();
                    //                return false;
                    //            }
                    var validmoneda = rowElem.cells[6].children[0].value;
                    if (validmoneda != 2) {
                        alert('La moneda de los conceptos deben de estar en Dolares, verificar información.');
                        return false;
                    }

                }
            }
            //        var grilladoc = document.getElementById('<%=dgvDetalleOC.ClientID %>');
            //        for (var ii = 1; ii < grilladoc.rows.length; ii++) {
            //            var rowElem = grilladoc.rows[ii];
            //            var peso = redondear(parseFloat(rowElem.cells[5].children[0].value),2);
            //            if (isNaN(parseFloat(peso)) || peso == 0) {
            //                alert('El peso debe ser mayor a 0, detalle de orden de compra.');
            //                rowElem.cells[5].children[0].select();
            //                rowElem.cells[5].children[0].focus();
            //                return false;
            //            }            
            //            var calcular = redondear(parseFloat(rowElem.cells[7].innerHTML), 2);
            //            if (isNaN(parseFloat(calcular)) || calcular == 0) {
            //                alert('volver a presionar el botón Calcular Costo.');                
            //                return false;
            //            }
            //        }
            var txtFchSalidad = document.getElementById('<%=txtFchSalidad.ClientID %>');
            var txtFchLlegada = document.getElementById('<%=txtFchLlegada.ClientID %>');
            if (compararFechas(txtFchSalidad, txtFchLlegada)) {
                alert('LA FECHA DE LLEGADA NO PUEDE SER MENOR A LA FECHA DE SALIDA.');
                return false;
            }


            var dgvDetalleOC = document.getElementById('<%=dgvDetalleOC.ClientID %>');

            for (var i = 1; i < dgvDetalleOC.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = dgvDetalleOC.rows[i];
                var peso = parseFloat(rowElem.cells[5].children[0].value);
                if (isNaN(peso)) { peso = 0; }

                if (peso <= 0) {
                    alert('INGRESE EL PESO DEL PRODUCTO < ' + rowElem.cells[2].innerText + ' >. NO SE PERMITE LA OPERACIÓN.');
                    return false;
                }

            } //FIM DEL FOR

            return confirm('Esta seguro de grabar.');
        }

        function valSelectOC(opcion, obj) {
            var grilla = document.getElementById('<%=dgvCapaOC.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    switch (opcion) {
                        case '0':
                            if (rowElem.cells[0].children[0].id == obj.id) {
                                rowElem.cells[0].children[0].getElementsByTagName('input')[0].checked = true;
                            } else {
                                rowElem.cells[0].children[0].getElementsByTagName('input')[0].checked = false;
                            }
                            break;
                    }
                }
            }
            return true;
        }
        ///////
        function valNavegacionProveedor(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex_Proveedor.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=txtBuscarProveedor.ClientID%>').select();
                document.getElementById('<%=txtBuscarProveedor.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        ////////////////////////////////
        function onKeyPressProveedor(obj, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btBuscarProveedor.CLientID %>').click();
            }
            return true;
        }
        ////  
        function onChange_txtBuscarProveedorFocus() {
            var txtBuscarProveedor = document.getElementById('<%=txtBuscarProveedor.ClientID %>');
            txtBuscarProveedor.focus();
            txtBuscarProveedor.select();
        }

        //
        function compararFechas(fecha, fecha2) {
            var xMonth = fecha.value.substring(3, 5);
            var xDay = fecha.value.substring(0, 2);
            var xYear = fecha.value.substring(6, 10);
            var yMonth = fecha2.value.substring(3, 5);
            var yDay = fecha2.value.substring(0, 2);
            var yYear = fecha2.value.substring(6, 10);

            if (xYear > yYear) {
                return (true)
            }
            else {
                if (xYear == yYear) {
                    if (xMonth > yMonth) {
                        return (true)
                    }
                    else {
                        if (xMonth == yMonth) {
                            if (xDay > yDay)
                                return (true);
                            else
                                return (false);
                        }
                        else
                            return (false);
                    }
                }
                else
                    return (false);
            }
        }
        //        
        /////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////

        function onClick_Imprimir() {

            var IdDocumento = parseInt(document.getElementById('<%=hddIdDocumento.ClientID %>').value);
            if (isNaN(IdDocumento)) { IdDocumento = 0; }
            if (IdDocumento > 0) {
                window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=2101100003&IdDocumento=' + IdDocumento, 'Costeo_Importacion_proyectado', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            }
            return false;
        }
        
    </script>
</asp:Content>
