﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmInicio.aspx.vb" Inherits="APPWEB.frmInicio" ViewStateMode="Disabled" %>

<%@ Register Assembly="MsgBox" Namespace="MsgBox" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>.::SANICENTER::. - SIGE 04.10.2013</title>
    <link href="/Estilos/Controles.css" rel="stylesheet" type="text/css" />
    <link href="/Estilos/stlGeneral.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 643px; height: 673px; margin-left:260px; margin-top: 50px">
        <div id="root" style="position: absolute; width: 750px; height: 622px;">
            <div id="e0" style="position: absolute; left: 33px; top: 127px; width: 537px; height: 374px;">
                <%--<cc1:MsgBox ID="MsgBox1" runat="server" />--%>
                <img src="Imagenes/InicioPrincipal.jpg" alt="" width="537" height="374" />
            </div>
            <div id="e1" style="position: absolute; left: 570px; top: 41px; width: 134px; height: 460px;">
                <table border="0" cellpadding="0" cellspacing="0" width="134">
                    <tbody>
                        <tr>
                            <td bgcolor="#7a95af" valign="top" height="460" nowrap="nowrap">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="e2" style="position: absolute; left: 578px; top: 50px; width: 119px; height: 82px;">
                <img src="Imagenes/InicioDerecha1.jpg" alt="" width="119" height="82" />
            </div>
            <div id="e3" style="position: absolute; left: 578px; top: 141px; width: 119px; height: 82px;">
                <img src="Imagenes/InicioDerecha2.jpg" alt="" width="119" height="82" />
            </div>
            <div id="e4" style="position: absolute; left: 578px; top: 232px; width: 119px; height: 82px;">
                <img src="Imagenes/InicioDerecha3.jpg" alt="" width="119" height="82" />
            </div>
            <div id="e5" style="position: absolute; left: 578px; top: 323px; width: 119px; height: 168px;">
                <img src="Imagenes/InicioDerecha4.jpg" alt="" width="119" height="168" />
            </div>
            <div id="e6" style="position: absolute; left: 187px; top: 183px; width: 310px; height: 121px;">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td>
                                <table style="border: 0px none rgb(0, 0, 0); table-layout: fixed;" border="0" cellpadding="0"
                                    cellspacing="0" width="310">
                                    <col width="310" />
                                    <tbody>
                                        <tr>
                                            <td style="border: 0px none rgb(0, 0, 0);" valign="top" width="310" height="121">
                                                <table border="0" cellpadding="0" cellspacing="0" width="310">
                                                    <col width="250" />
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" width="250" height="89">
                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <table style="border: 1px none rgb(220, 220, 220); table-layout: fixed;" border="1"
                                                                                    cellpadding="0" cellspacing="0" width="250">
                                                                                    <col width="107" />
                                                                                    <col width="141" />
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="border: 0px none rgb(220, 220, 220);" valign="top" width="107" height="28">
                                                                                                <asp:Label ID="Label1" runat="server" Text="Usuario:" CssClass="Label"></asp:Label>
                                                                                            </td>
                                                                                            <td style="border: 0px none rgb(220, 220, 220);" valign="top" width="141" height="28">
                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="141"">
                                                                                                    <col width="118" />
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td valign="top" width="118" height="26">
                                                                                                                <asp:TextBox ID="txtLogin" runat="server" ForeColor="#7A95B0" MaxLength="20"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td width="141" height="2">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="border: 0px none rgb(220, 220, 220);" valign="top" width="107" height="28">
                                                                                                <asp:Label ID="Label2" runat="server" Text="Contraseña:" CssClass="Label"></asp:Label>
                                                                                            </td>
                                                                                            <td style="border: 0px none rgb(220, 220, 220);" valign="top" width="141" height="28">
                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="141">
                                                                                                    <col width="118" />
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td width="141" height="2">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td valign="top" width="118" height="26">
                                                                                                                <asp:TextBox ID="txtClave" runat="server" ForeColor="#7A95B0" MaxLength="15" TextMode="Password"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="border: 0px none rgb(220, 220, 220);" valign="top" width="107" height="31">
                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="107">
                                                                                                    <col width="1" />
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td height="26">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td style="border: 0px none rgb(220, 220, 220);" valign="top" width="141" height="31">
                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="141">
                                                                                                    <col width="20" />
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td height="29">
                                                                                                                <asp:ImageButton ID="btnIngresar" runat="server" ImageUrl="~/Imagenes/Ingresar_A.jpg"
                                                                                                                    onmouseout="this.src='../Imagenes/Ingresar_A.JPG';" onmouseover="this.src='../Imagenes/Ingresar_B.JPG';" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="e7" style="position: absolute; left: 191px; top: 0pt; width: 264px; height: 41px;">
                <img src="Imagenes/InicioSuperiorCentro.jpg" alt="" width="264" height="41" />
            </div>
            <div id="e8" style="position: absolute; left: 455px; top: 0pt; width: 249px; height: 41px;">
                <img src="Imagenes/InicioSuperiorDerecho.jpg" alt="" width="249" height="41" />
            </div>
            <div id="e9" style="position: absolute; left: 33px; top: 41px; width: 537px; height: 86px;">
                <img src="Imagenes/InicioSuperior2.jpg" alt="" width="537" height="86" />
            </div>
            <div id="e10" style="position: absolute; left: 314px; top: 568px; width: 115px; height: 34px;">
                <img src="Imagenes/LogoDigrafic_01-115x34.jpg" alt="" width="115" height="34" />
            </div>
            <div id="e11" style="position: absolute; left: 337px; top: 543px; width: 69px; height: 17px;">
                <span><b><font color="#7a95b0" face="Tahoma"><span style="font-size: 10px; line-height: 13px;">
                    Powered By<br />
                </span></font></b></span>
            </div>
            <div id="e12" style="position: absolute; left: 48px; top: 50px; width: 341px; height: 66px;">
                <img src="Imagenes/InicioLogoEmpresa.jpg" alt="" width="341" height="66" />
            </div>
            <div id="e13" style="position: absolute; left: 33px; top: 0pt; width: 158px; height: 41px;">
                <img src="Imagenes/InicioSuperiorIzquierdo.jpg" alt="" width="158" height="41" />
            </div>
        </div>
    </div>
    </form>
</body>
 <script language="javascript" type="text/javascript">
     history.forward();
  </script>
</html>
