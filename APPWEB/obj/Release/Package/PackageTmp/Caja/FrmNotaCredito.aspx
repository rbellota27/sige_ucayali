<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmNotaCredito.aspx.vb" Inherits="APPWEB.FrmNotaCredito" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../css/buttons.css" rel="stylesheet" type="text/css" />
    <table class="style1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" OnClientClick="return(   valOnClickNuevo()  );"
                                Text="Nuevo" ToolTip="Nuevo" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                                Text="Editar" ToolTip="Editar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                                Text="Anular" ToolTip="Anular" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" OnClientClick="return( valSaveDocumento(this));"
                                Text="Guardar" ToolTip="Guardar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  valOnClick_btnImprimir()  );"
                                Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimirNCElectronica" runat="server" OnClientClick="return(  valOnClick_btnImprimirNCElectronica()  );"
                                Text="Imprimir NC Electr�nica" ToolTip="Imprimir NC Electr�nica" Width="150px" />
                        </td>
                        <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                NOTA DE CR�DITO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                                                <tr>
                            <td align="right" class="LabelRojo" style="font-weight: bold">
                                Tipo Documento:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoDocumento" Width="100%" runat="server" CssClass="LabelRojo"
                                    Font-Bold="true" AutoPostBack="true">
                                    <asp:ListItem Text="Nota de Cr�dito Electr�nica" Value = "1101353003"></asp:ListItem>
                                    <asp:ListItem Text="Nota de Cr�dito" Value = "4"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                               <td>
                               &nbsp;
                            </td>
                            <td>
                   
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" Width="100%" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Panel ID="Panel6" runat="server" DefaultButton="btnBuscarDocumentoxCodigo">                                
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( valOnKeyPressCodigoDoc(this,event)  );"
                                    ReadOnly="true" Width="100px"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    ToolTip="Buscar Documento por [ Serie - N�mero ]." OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="B�squeda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Moneda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboMoneda" Width="100%" runat="server" CssClass="LabelRojo"
                                    Font-Bold="true" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Tipo Operaci�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" runat="server" Width="100%" AutoPostBack="true" onClick="return(validaciones());">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Motivo:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboMotivo" runat="server" AutoPostBack="true" Width="100%" onClick="return(validaciones());">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Vcto.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaVcto" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                    TargetControlID="txtFechaVcto">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto" style="font-weight: bold">
                                Aplicaci�n:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstadoCancelacion" Enabled="false" CssClass="Texto" Font-Bold="true"
                                    runat="server" ToolTip="Estado de Uso de la Nota de Cr�dito." Width="100%">
                                    <asp:ListItem Value="1">Por Aplicar</asp:ListItem>
                                    <asp:ListItem Value="2">Aplicado</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DATOS DEL BENEFICIARIO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cliente" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Descripci�n:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcionPersona" runat="server" CssClass="TextBox_ReadOnlyLeft"
                                    Enabled="true" onFocus="return(   valOnFocus_Cliente()  );" onKeypress="return(false);"
                                    Width="400px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPersona" runat="server" OnClientClick="return( valOnClickBuscarCliente()  );" CssClass="btnBuscar"
                                    Text="Buscar" ToolTip="Buscar Cliente" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDNI" runat="server" CssClass="TextBox_ReadOnlyLeft" Enabled="true"
                                    onFocus="return(   valOnFocus_Cliente()  );" onKeypress="return(false);"></asp:TextBox>
                            </td>
                            <td align="right" class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC" runat="server" CssClass="TextBox_ReadOnlyLeft" Enabled="true"
                                    onFocus="return(   valOnFocus_Cliente()  );" onKeypress="return(false);"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                DOCUMENTO DE REFERENCIA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_DocReferencia" runat="server">
                    <asp:Panel ID="Panel_DocRef" runat="server">
                        <table>
                            <tr>
                                <td colspan="6">
                                    <asp:Button ID="btnBuscarDocRef" OnClientClick="return(  valOnClickBuscarDocRef()  );"
                                        runat="server" Text="Agregar Doc. Referencia" ToolTip="Agregar Documento de Referencia"
                                        Width="160px" Style="margin-bottom: 0px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="Texto" align="right">
                                    Tipo Documento:
                                </td>
                                <td style="width: 150px">
                                    <asp:Label ID="lblTipoDocumentoRef" Font-Bold="true" runat="server" Text="" CssClass="LabelRojo"></asp:Label>
                                </td>
                                <td class="Texto" align="right">
                                    Nro. Documento:
                                </td>
                                <td style="width: 150px">
                                    <asp:Label ID="lblNroDocumentoDocRef" Font-Bold="true" runat="server" Text="" CssClass="LabelRojo"></asp:Label>
                                </td>
                                <td class="Texto" align="right">
                                    Fecha Emisi�n:
                                </td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblFechaEmisionDocRef" Font-Bold="true" runat="server" Text="" CssClass="LabelRojo"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="Texto">
                                    Total:
                                </td>
                                <td style="width: 150px">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMonedaTotalDocRef" Font-Bold="true" CssClass="LabelRojo" runat="server"
                                                    Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTotalDocRef" Font-Bold="true" CssClass="LabelRojo" runat="server"
                                                    Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="right" class="Texto">
                                    Percepci�n:
                                </td>
                                <td style="width: 150px">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMonedaPercepcionDocRef" Font-Bold="true" CssClass="LabelRojo" runat="server"
                                                    Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPercepcionDocRef" Font-Bold="true" CssClass="LabelRojo" runat="server"
                                                    Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="right" class="Texto">
                                    Total General:
                                </td>
                                <td style="width: 100px">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMonedaTotalGeneralDocRef" Font-Bold="true" CssClass="LabelRojo"
                                                    runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTotalGeneralDocRef" Font-Bold="true" CssClass="LabelRojo" runat="server"
                                                    Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="right" class="Texto">
                                    Descuento Global:
                                </td>
                                <td style="width: 100px">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblDescuentoGlobalDocRef" Font-Bold="true" CssClass="LabelRojo" runat="server"
                                                    Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPorcentDescuentoGlobalDocRef" Font-Bold="true" CssClass="LabelRojo"
                                                    runat="server" Text="(%)"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                DETALLE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Detalle" runat="server" ScrollBars="Horizontal" Width="100%">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Opci�n:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboNotaCreditoModo" runat="server" AutoPostBack="true" onClick="   return( valOnClickCboModoNotaCredito()  );   ">
                                                <asp:ListItem Value="0" Selected="True">Listar Art�culos contenidos en el Documento de Referencia</asp:ListItem>
                                                <asp:ListItem Value="1" Enabled="false">Ingresar refrendo y monto del documento</asp:ListItem>
                                                <asp:ListItem Value="2">Listar Gu�as de Recepci�n por el Documento de Referencia</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboGuiaRecepcion" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_ListaProducto" runat="server" Width="100%">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnCargarArticuloAll_DocRef" Width="370px" runat="server" Text="[ Cargar Todos los Art�culos del Documento de Referencia ]"
                                                    OnClientClick="return(   valOnClick_btnCargarArticuloAll_DocRef()    );" />
                                            </td>
                                        </tr>
                                        <tr>
                                          <td>
                                     <asp:Label ID="lblguardadata" runat="server" Text="" style="color:whitesmoke;font-family:Comic Sans MS;text-align:left;font-size:small" Visible="false"></asp:Label>
                                     </td>
                                     </tr>
                                         <tr>
                                    <td> 
                                    <label id="idlab" runat="server" style="color:Red;font-family:Comic Sans MS;text-align:left;font-size:small">Documentos Relacionados :</label>
                                    </td>
                                    </tr>
                     <tr>
                    <td>
                    <asp:GridView ID="gvDocRef" runat="server" AutoGenerateColumns="False" 
                            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" 
                            CellPadding="3"  >
                        <RowStyle ForeColor="#000066" />
                        <Columns>
                            <asp:TemplateField HeaderText="Orden de Despacho">
                              
                                <ItemTemplate>
                                         <asp:Label ID="lblTipoDocumentoRef" ForeColor="Red" Font-Bold="true"  runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"strTipoDocumentoRef")%>'></asp:Label>
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Documento de Venta">
                               
                                <ItemTemplate>
                                   
                                        <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Numero")%>'></asp:Label>
                                            
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView> </td>
                        </tr>   
                                        
                                        
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="false" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btnQuitarProducto" OnClick="btnQuitarDetalle_Click" ToolTip="Quitar Producto del detalle."
                                                                    ImageUrl="~/Imagenes/Eliminar1.gif" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px" HeaderText="C�d">
                                                            <ItemTemplate>
                                                                            <asp:HiddenField ID="hddIdDetalleDocumento_Detalle" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDetalleDocumento")%>' />
                                                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />

                                                                            <asp:HiddenField ID="hddIdDetalleAfecto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDetalleAfecto")%>' />

                                                                            <asp:Label ID="lblCodigoProducto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CodigoProducto")%>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                 
                                                        <asp:BoundField HeaderText="Cantidad Ref." ItemStyle-Font-Bold="true" DataFormatString="{0:F4}"
                                                            HeaderStyle-HorizontalAlign="Center" DataField="CantxAtender" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px" HeaderText="Cantidad">
                                                            <ItemTemplate>
                                                                            <asp:TextBox ID="txtCantidadDetalle" TabIndex="100" onFocus="return( aceptarFoco(this)    );"
                                                                       onKeyup="return(   calcularMontos('1')   );"
                                                                                onblur="return(   valBlur(event)   );"
                                                                                onKeypress="return(  validarNumeroPuntoPositivo('event')   );" runat="server" Width="70px"
                                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F4}")%>'></asp:TextBox>

                                                                            <asp:Label ID="lblUM_Detalle" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"UMedida")%>'></asp:Label>

                                                                            <asp:HiddenField ID="hddIdUnidadMedidaDetalle" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Descripci�n" HeaderStyle-HorizontalAlign="Center" DataField="NomProducto"
                                                            ItemStyle-Width="300px" />
                                                        <asp:BoundField HeaderText="P. Unitario" ItemStyle-Font-Bold="true" DataFormatString="{0:F2}"
                                                            HeaderStyle-HorizontalAlign="Center" DataField="PrecioSD" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px" HeaderText="P. Unitario">
                                                            <ItemTemplate>

                                                                            <asp:TextBox ID="txtPUnit_Detalle" TabIndex="200" onFocus="return( aceptarFoco(this)    );"
                                                                                onKeypress="return(  validarNumeroPuntoPositivo('event')   );" onKeyup="return(   calcularMontos('2')   );"
                                                                                onblur="return(   valBlur(event)   );" runat="server" Width="70px" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCD","{0:F2}")%>'></asp:TextBox>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px" HeaderText="Valor de Venta">
                                                            <ItemTemplate>

                                                                            <asp:TextBox ID="txtImporte_Detalle" CssClass="TextBoxReadOnly" onFocus="return(  valOnFocusImporte()  );"
                                                                                onKeypress="return(  false  );" runat="server" Width="70px" Text='<%#DataBinder.Eval(Container.DataItem,"Importe","{0:F2}")%>'></asp:TextBox>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                      <asp:BoundField HeaderText="Prod.Compuesto" ItemStyle-Font-Bold="true" runat="server"
                                                                      HeaderStyle-HorizontalAlign="Center" DataField="ProdNombreKit" ItemStyle-HorizontalAlign="Center" />  
                                                                                            
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px"  HeaderText="CantxAtender">
                                                            <ItemTemplate>
                                                               
                                                                          <asp:Label ID="cantxatender" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"CaxAtender")%>'></asp:Label>
                                                                     
                                                                            <asp:HiddenField ID="hddcantxatender" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"CaxAtender")%>' />
                                                                       
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                                <asp:GridView ID="GV_Concepto" runat="server" AutoGenerateColumns="false" Width="56%"
                                                    Height="32px">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btnQuitarConcepto" OnClick="btnQuitarDetalleConcepto_Click"
                                                                    ToolTip="Quitar Concepto al Detalle" ImageUrl="~/Imagenes/Eliminar1.gif" runat="server" />
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" Width="50px" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Descripci�n del Concepto" HeaderStyle-HorizontalAlign="Center"
                                                            DataField="Descripcion">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Height="25px" HeaderText="Monto">
                                                            <ItemTemplate>
                                                                            <asp:TextBox ID="txtConcepto" TabIndex="200" onFocus="return( aceptarFoco(this)    );"
                                                                                onKeypress="return(  validarNumeroPuntoPositivo('event')   );" onKeyup="return(   calcularMontos('2')   );"
                                                                                onblur="return(   valBlur(event)   );" runat="server" Width="70px" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F2}")%>'></asp:TextBox>

                                                                            <asp:HiddenField ID="hddIdConcepto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdConcepto") %>' />

                                                                            <asp:HiddenField ID="hddIdDocumentoRef" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumentoRef") %>' />
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                     
                                                    </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_MontoDetalle" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Refrendo:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRefrendo" Width="500px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="Texto" style="font-weight: bold">
                                                Monto:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMonedaMontoNotaCredito" runat="server" CssClass="LabelRojo" Font-Bold="true"
                                                    Text="-"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMontoValorNotaCredito" onKeyup="return(   calcularMontos('0')   );"
                                                    onblur="return(   valBlur(event)   );" onKeypress="return(  validarNumeroPuntoPositivo('event')   );"
                                                    onFocus="return(  aceptarFoco(this)    );" CssClass="LabelRojo" Width="70px"
                                                    Font-Bold="true" Text="0" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Sub Total:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaSubTotal" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                                        </td>
                                        <td style="width: 130px">
                                            <asp:TextBox ID="txtSubTotal" onFocus="return(   valOnFocus_Totales()   );" onKeypress="return(  false );"
                                                Width="90px" CssClass="TextBox_ReadOnly" Text="0" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            I.G.V.
                                        </td>
                                        <td>
                                            <asp:Label ID="lblIGV_Tasa" runat="server" CssClass="Label" Text="-"></asp:Label>
                                        </td>
                                        <td class="Texto">
                                            (%):
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaIGV" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                                        </td>
                                        <td style="width: 130px">
                                            <asp:TextBox ID="txtIGV" onFocus="return(   valOnFocus_Totales()   );" Width="90px"
                                                CssClass="TextBox_ReadOnly" Text="0" onKeypress="return(  false );" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            Total:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaTotal" runat="server" CssClass="LabelRojo" Text="-"></asp:Label>
                                        </td>
                                        <td style="width: 130px">
                                            <asp:TextBox ID="txtTotal" onFocus="return(   valOnFocus_Totales()   );" onKeypress="return(  false );"
                                                Width="90px" CssClass="TextBox_ReadOnly" Text="0" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                OBSERVACIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtObservaciones" Width="100%" Height="150px" runat="server" MaxLength="500"
                                    onBlur="return (onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                    TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddIdDocumentoRef" runat="server" />
                <asp:HiddenField ID="hddIdMonedaDocRef" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
                
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarPersonaGrilla">                                    
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this,event));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />--%>
                                                                        <asp:Button ID="btnBuscarPersonaGrilla" runat="server" Text="Buscar" CssClass="btnBuscar"
                                                                        OnClientClick="this.disabled=true;this.value='Procesando'"  UseSubmitBehavior="false"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 260px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0">Por Fecha de Emisi�n</asp:ListItem>
                        <asp:ListItem Value="1" Selected="True">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Tienda:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTienda_DocRef" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled = true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:Panel ID="Panel4" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">                                                
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                            <asp:Panel ID="Panel5" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td>                                                
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Numero")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />

                                                    <asp:HiddenField ID="hddIdMonedaDocRef_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Total" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaTotal_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblTotal_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Total","{0:F2}")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Percepci�n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaPercepcion_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblPercepcion_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Percepcion","{0:F2}")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total General" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaTotalGeneral_Find" runat="server" Font-Bold="true" ForeColor="Red"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblTotalGeneral_Find" runat="server" Font-Bold="true" ForeColor="Red"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dscto Global" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblDescuentoGlobal" runat="server" Font-Bold="true" ForeColor="Red"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"anex_DescuentoGlobal","{0:F2}")%>'></asp:Label>

                                                    <asp:Label ID="lblPorcentDescuentoGlobal" runat="server" Font-Bold="true" ForeColor="Red"
                                                        Text="(%)"></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NomPropietario" HeaderText="Empresa" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomTienda" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomCondicionPago" HeaderText="Cond. Pago" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                            DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:CheckBox ID="chb_ConsiderarDocRef_NC" runat="server" Checked="false" Text="Considerar documentos con < Nota de Cr�dito > asociados."
                        TextAlign="Right" CssClass="Texto" Font-Bold="true" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_BA" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaInicio_BA" runat="server"
                                                    ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                    CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_BA">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaInicio_BA" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaInicio_BA">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_BA" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaFin_BA" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_BA">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaFin_BA" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaFin_BA">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%" AllowPaging="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de b�squeda realiza un filtrado de acuerdo al cliente seleccionado.
                </td>
            </tr>
        </table>
    </div>
    <div id="capaMensajes" style="width:80px;border:1px solid blue;display:none;top:80px;left:150px">
    
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        //************************** BUSQUEDA PERSONA
        //***************************************************** BUSQUEDA PERSONA
//         function valOnClickGrabarExistGD() {
//            var lblguardadata = document.getElementById('<%=lblguardadata.ClientID%>').innerHTML;
//            if(lblguardadata == 1 ){
//                   alert('Verifique el Documento de Recepci�n, ya que se ha generado un Documento de Despacho.');
//                   return false ;
//            }
//            else {                   
//            return true;
//        }
//        }
        
        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btnBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj,e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btnBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function selectPersona() {
            var grilla = document.getElementById('<%=gvBuscar.ClientID%>');
            if (grilla != null) {
                var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
                var txtDNI = document.getElementById('<%=txtDNI.ClientID%>');
                var txtRUC = document.getElementById('<%=txtRUC.ClientID%>');
                var txtDescripcionPersona = document.getElementById('<%=txtDescripcionPersona.ClientID%>');

                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[0].id == event.srcElement.id) {
                        hddIdPersona.value = rowElem.cells[1].innerHTML;
                        txtDescripcionPersona.value = rowElem.cells[2].innerHTML;
                        txtDNI.value = rowElem.cells[3].innerHTML;
                        txtRUC.value = rowElem.cells[4].innerHTML;
                        offCapa('capaPersona');
                        return false;
                    }
                }
            }
            return false;
        }
        function valOnFocus_Cliente() {
            document.getElementById('<%=btnBuscarPersona.ClientID%>').focus();
            return false;
        }
        //*********************************************************************************** FIN BUSCAR CLIENTE


        function valOnFocus_Totales() {
            document.getElementById('<%=btnBuscarDocRef.ClientID%>').focus();
            return false;
        }

        function mostrarCapaBuscarDocRef() {
            onCapa('capaDocumentosReferencia');
            return false;
        }
        function valOnFocusImporte() {
            document.getElementById('<%=btnBuscarDocRef.ClientID%>').focus();
            return false;
        }
        function valOnClickBuscarCliente() {
            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID %>');
            var cboMotivo = document.getElementById('<%=cboMotivo.ClientID %>');

            var valueCboTipoOperacion = cboTipoOperacion.value;
            var valuecboMotivo = cboMotivo.value;

            var hddIdDocumentoRef = document.getElementById('<%=hddIdDocumentoRef.ClientID%>');
            if (valueCboTipoOperacion == 0 || valuecboMotivo == 0) {
                alert('Deber� seleccionar Tipo de operaci�n / Motivo');
                cboTipoOperacion.focus();
                return false;
            }

            if ((isNaN(parseInt(hddIdDocumentoRef.value)) && hddIdDocumentoRef.value.length <= 0) || parseInt(hddIdDocumentoRef.value) <= 0) {
                mostrarCapaPersona();
            } else {
                alert('Se ha seleccionado un Documento de Referencia, no se permite seleccionar otro cliente. Caso contrario pulse el bot�n [ Nuevo ].');
            }
            return false;
        }
        function valOnClickBuscarDocRef() {
            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID %>');
            var valueCboTipoOperacion = cboTipoOperacion.value;
            var cboMotivo = document.getElementById('<%=cboMotivo.ClientID %>');
            var valuecboMotivo = cboMotivo.value;

            if (valuecboMotivo == 0 || valueCboTipoOperacion == 0) {
                alert('Deber� seleccionar Tipo de operaci�n / Motivo');
                cboTipoOperacion.focus();
                return false;
            } else {
                var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
                if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                    alert('Debe seleccionar un Cliente.');
                    mostrarCapaPersona();
                    return false;
                }
                return true;
            }
        }


        function valOnClickCboModoNotaCredito() {
        
            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID %>');
            var valueCboTipoOperacion = cboTipoOperacion.value;
            var cboMotivo = document.getElementById('<%=cboMotivo.ClientID %>');
            var valuecboMotivo = cboMotivo.value;

            if (cboMotivo == 0 || cboTipoOperacion == 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                cboTipoOperacion.focus();
                return false;
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            var hddIdDocumentoRef = document.getElementById('<%=hddIdDocumentoRef.ClientID%>');
            if (isNaN(parseInt(hddIdDocumentoRef.value)) || hddIdDocumentoRef.value.length <= 0) {
                alert('Debe seleccionar un Documento de Referencia.');
                return false;
            }
            return true;
        }


        function calcularMontos(opcion) {

            //******* opcion
            //** 0: Todo
            //** 1: Cantidad
            //** 2: Articulos de Recepcion

            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var grilla1 = document.getElementById('<%=GV_Concepto.ClientID%>');
            var txtMontoNotaCredito = document.getElementById('<%=txtMontoValorNotaCredito.ClientID%>');
            var total = 0;
            var cantidad = 0;
            var precioUnit = 0;
            var monto = 0;
            var montototal = 0;
            var importe = 0;

            var cboModo = document.getElementById('<%=cboNotaCreditoModo.ClientID%>');

            switch (parseInt(cboModo.value)) {
                case 0:  //*********** Lista Productos
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {
                            var rowElem = grilla.rows[i];
                            cantidad = parseFloat(rowElem.cells[3].children[0].value);
                            if (isNaN(cantidad)) { cantidad = 0; }
                            precioUnit = parseFloat(rowElem.cells[6].children[0].value);
                            if (isNaN(precioUnit)) { precioUnit = 0; }

                            var PreImporte = cantidad * precioUnit;
                            importe = PreImporte.toPrecision(10);                         
                            
                            rowElem.cells[7].children[0].value = redondear(importe, 2);
                            total = total + redondear(importe, 2);
                        }
                    }


                    break;

                case 1:  //************ Monto Total
                    total = parseFloat(txtMontoNotaCredito.value);
                    if (isNaN(total)) { total = 0; }
                    break;

                case 2:  //*********** Lista Productos
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {
                            var rowElem = grilla.rows[i];
                            cantidad = parseFloat(rowElem.cells[3].children[0].value);
                            if (isNaN(cantidad)) { cantidad = 0; }
                            precioUnit = parseFloat(rowElem.cells[6].children[0].value);
                            if (isNaN(precioUnit)) { precioUnit = 0; }
                            importe = cantidad * precioUnit;
                            rowElem.cells[7].children[0].value = redondear(importe, 2);
                            total = total + redondear(importe, 2);
                        }
                    }


                    break;
            }


            var ConceptoTotal = get_ConceptoSinDescuentoGlobal();
            var PorcentDescuentoGlobal = parseFloat(document.getElementById('<%=lblDescuentoGlobalDocRef.ClientID%>').innerHTML);

            total = total + ConceptoTotal;

            var DescuentoGlobal = redondear((total - (total * (1 - PorcentDescuentoGlobal / 100.00))) * -1, 2);

            set_ConceptoDescuentoGlobal(DescuentoGlobal);

            total = total + DescuentoGlobal;
            

            var tasaIGV = parseFloat(document.getElementById('<%=lblIGV_Tasa.ClientID%>').innerHTML);
            var subTotal = (total / (1 + tasaIGV / 100));
            document.getElementById('<%=txtSubTotal.ClientID%>').value = Format(subTotal, 2);  //**** SUB TOTAL
            document.getElementById('<%=txtIGV.ClientID%>').value = Format(subTotal * (tasaIGV / 100), 2);  //**** IGV            
            document.getElementById('<%=txtTotal.ClientID%>').value = Format(total, 2);  //**** TOTAL                                   

            return false;
        }

        function get_ConceptoSinDescuentoGlobal() {
            
            var TotalConcepto = 0;
            var IdConcepto = 0;
            var Monto = 0;

            var GV_Concepto = document.getElementById('<%=GV_Concepto.ClientID%>');

            if (GV_Concepto != null) {

                for (var i = 1; i < GV_Concepto.rows.length; i++) {
                    var rowElem = GV_Concepto.rows[i];

                    IdConcepto = parseInt(rowElem.cells[2].children[1].value);
                    Monto = parseFloat(rowElem.cells[2].children[0].value);

                    if (IdConcepto != 10 && Monto > 0) {
                        TotalConcepto = TotalConcepto + Monto;                        
                    }
                    
                }

            }

            return TotalConcepto;

        }

        function set_ConceptoDescuentoGlobal(valor) {

            var TotalConcepto = 0;
            var IdConcepto = 0;
            var txtMontoConcepto;

            var GV_Concepto = document.getElementById('<%=GV_Concepto.ClientID%>');

            if (GV_Concepto != null) {

                for (var i = 1; i < GV_Concepto.rows.length; i++) {
                    var rowElem = GV_Concepto.rows[i];

                    IdConcepto = parseInt(rowElem.cells[2].children[1].value);
                    txtMontoConcepto = rowElem.cells[2].children[0];

                    if (IdConcepto == 10) {
                        txtMontoConcepto.value = valor;
                        break;
                    }

                }

            }

        }

        function valSaveDocumento(btn) {

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie de Documento.');
                return false;
            }
            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            if (isNaN(parseInt(cboMoneda.value)) || cboMoneda.value.length <= 0) {
                alert('Debe seleccionar una Moneda.');
                return false;
            }
            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }
            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }

            var hddIdDocumentoRef = document.getElementById('<%=hddIdDocumentoRef.ClientID%>');
            if (isNaN(parseInt(hddIdDocumentoRef.value)) || hddIdDocumentoRef.value.length <= 0) {
                alert('Debe seleccionar un Documento de Referencia.');
                return false;
            }

//           var txtTotalAPagar = document.getElementById('<%=txtTotal.ClientID%>');



//            if (isNaN(parseFloat(txtTotalAPagar.value)) || txtTotalAPagar.value.length <= 0 || parseFloat(txtTotalAPagar.value) <= 0) {
//                alert('El Total debe ser un valor mayor a cero.');
//                return false;
//            }

            var moneda = getCampoxValorCombo(cboMoneda, cboMoneda.value);

            var lblTotalDocRef = document.getElementById('<%=lblTotalDocRef.ClientID%>');
            if (parseFloat(lblTotalDocRef.innerHTML) < parseFloat(txtTotalAPagar.value)) {
                alert('El Monto del Documento [ ' + moneda + ' ' + txtTotalAPagar.value + ' ] supera el Valor del Documento de Referencia [ ' + moneda + ' ' + lblTotalDocRef.innerHTML + ' ].');
                return false;
            }

            //************* Validando cantidades y precios
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {

                var cantidadNew = 0;
                var precioUnitNew = 0;
                var cantidadOld = 0;
                var precioUnitOld = 0;

                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    cantidadNew = parseFloat(rowElem.cells[3].children[0].value);
                    precioUnitNew = parseFloat(rowElem.cells[6].children[0].value);

                    cantidadOld = parseFloat(rowElem.cells[2].innerHTML);
                    precioUnitOld = parseFloat(rowElem.cells[5].innerHTML);

                    if (cantidadNew > cantidadOld) {
                        alert('La Cantidad Ingresada [ ' + cantidadNew + ' ] supera la Cantidad M�xima Permitida [ ' + cantidadOld + ' ].');
                        rowElem.cells[3].children[0].cells[0].children[0].select();
                        rowElem.cells[3].children[0].cells[0].children[0].focus();
                        return false;
                    }
                    if (precioUnitNew > precioUnitOld) {
                        alert('El Precio Unitario [ ' + precioUnitNew + ' ] supera el Precio Unitario M�ximo Permitido [ ' + precioUnitOld + ' ].');
                        rowElem.cells[6].children[0].cells[0].children[0].select();
                        rowElem.cells[6].children[0].cells[0].children[0].focus();
                        return false;
                    }
                }
            }
            var mensaje = 'Desea continuar con la Operaci�n ?';
            var txtFechaVcto = document.getElementById('<%=txtFechaVcto.ClientID%>');
            var txtFechaEmision = document.getElementById('<%=txtFechaEmision.ClientID%>');
            if (txtFechaEmision.value == txtFechaVcto.value) {
                mensaje = 'La Fecha de Emisi�n es igual a la Fecha de Vencimiento. La Fecha de Vencimiento representa la Fecha de Caducidad de este documento para su canje. ' + mensaje;
            }

            var respuesta = confirm(mensaje);
            if (respuesta) {
                return true;
            } else {
                return false;
            }
        }

        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }

            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor v�lido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }

        function valOnKeyPressCodigoDoc(txtCodigoDocumento,e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }
        function valOnClickNuevo() {

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            if (parseInt(hddFrmModo.value) == 0 || parseInt(hddFrmModo.value) == 4 || parseInt(hddFrmModo.value) == 5 || parseInt(hddFrmModo.value) == 6) {
                return true;
            }

            var grillaDetalle = document.getElementById('<%=GV_Detalle.ClientID %>');
            if (grillaDetalle == null) {
                return true;
            }

            return (confirm('Est� seguro que desea generar un NUEVO Documento ?'));
        }

        function valOnClickEditar() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Edici�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite su edici�n.');
                return false;
            }
            var cboEstadoCancelacion = document.getElementById('<%=cboEstadoCancelacion.ClientID%>');
            if (parseInt(cboEstadoCancelacion.value) == 2) {
                alert('El Documento ha sido Aplicado. No se permite su edici�n.');
                return false;
            }
            return true;
        }

        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para su Anulaci�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya est� ANULADO. No se permite su anulaci�n.');
                return false;
            }
            return confirm('Desea continuar con el Proceso de ANULACI�N del Documento ?');
        }
        function valOnClick_btnCargarArticuloAll_DocRef() {
            var hddIdDocumentoRef = document.getElementById('<%=hddIdDocumentoRef.ClientID%>');
            if (isNaN(parseInt(hddIdDocumentoRef.value)) || hddIdDocumentoRef.value.length <= 0) {
                alert('Debe seleccionar un Documento de Referencia.');
                return false;
            }
            return true;
        }
        function valOnClick_btnGenerarIngresoAlmacen() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para la Generaci�n de una Gu�a de Recepci�n de Almac�n.');
                return false;
            }
            var cboTipoNotaCredito = document.getElementById('<%=cboNotaCreditoModo.ClientID%>');
            if (parseInt(cboTipoNotaCredito.value) != 0) {
                alert('La Generaci�n de una [ Gu�a de Recepci�n ] solo es posible por la Operaci�n de Nota de Cr�dito [ Devoluci�n de Art�culos ].');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite la Operaci�n.');
                return false;
            }
            window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=25&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);


            return false;
        }
        function valOnClick_btnImprimir() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Impresi�n.');
                return false;
            }
            //window.open('../../Caja/Reportes/Visor1.aspx?iReporte=4&IdDocumento=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=4&IdDocumento=' + hddIdDocumento.value, 'Credito', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }
        function valOnClick_btnImprimirNCElectronica() {
            var comboSerie = document.getElementById('<%= cboSerie.ClientID %>');
            var txtCodigo = document.getElementById('<%= txtCodigoDocumento.ClientID %>');
            var comboTipoDocumento = document.getElementById('<%= cboTipoDocumento.ClientID %>');

            var idSerie = comboSerie.value;
            var idTipoDocumento = comboTipoDocumento.value;
            var codigo = txtCodigo.value;

            window.open('../../caja/reportes/frmImprimirFacElectronica.aspx?idSerie=' + idSerie + '&codigo=' + codigo + '&idtipoDocumento=' + idTipoDocumento + '', 'popup', 'width=800,height=500');
            return false;
        }
        function valOnClick_btnAceptarBuscarDocRefxCodigo() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('Ingrese un Nro. de Serie para la b�squeda.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('Ingrese un Nro. de Documento para la b�squeda.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }

        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');

        }

        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }

        function validaciones() {
            var cboGuiaRecepcion = document.getElementById('<%=cboGuiaRecepcion.ClientID %>');
            var cboNotaCreditoModo = document.getElementById('<%= cboNotaCreditoModo.ClientID %>');
            var grilla = document.getElementById('<%= gvDocRef.ClientID %>');

            if (cboGuiaRecepcion != null && grilla != null) {
                cboNotaCreditoModo.disabled = false;
                alert('No es posible cambiar Tipo de Operacion / Motivo, ya existe una guia de recepci�n seleccionada.');
                return false;
            }
            return true;
        }

    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
