<%@ Page Title="Modulo - Facturacion" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"  Culture="Auto" UICulture="Auto" 
    CodeBehind="FrmCaja_Facturacion.aspx.vb" Inherits="APPWEB.FrmCaja_Facturacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel3" runat="server"><ContentTemplate>
    <table width="100%">
        <tr style="background-color: Yellow">
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 350px">
                            <asp:LinkButton CssClass="Label" Font-Bold="true" ID="lkb_CapaConfigFrm" OnClientClick="return(onCapa('capaConfiguracionFrm'));"
                                runat="server">Config. Formulario</asp:LinkButton>
                        </td>
                        <td>
                            <asp:Label ID="lblCaja" CssClass="LabelRojo" Font-Bold="true" runat="server" Text="Caja"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" ToolTip="Nuevo" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                                Text="Editar" ToolTip="Editar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                                Text="Anular" ToolTip="Anular" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" OnClientClick="return( valSaveDocumento()  );"
                                Text="Guardar" ToolTip="Guardar" Width="80px" OnClick="btnGuardar_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnRecalcular" runat="server" Text="Recalcular" Width="90px" OnClientClick="return(     valOnClick_btnRecalcular()       );" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  valOnClick_btnImprimir(1)  );"
                                Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" Visible="False" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimirDetallado" runat="server" OnClientClick="return(  valOnClick_btnImprimir(2)  );"
                                Text="Imprimir Estandar" ToolTip="Imprimir Documento" Width="125px" Visible="False" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimirGrupo" runat="server" OnClientClick="return(  valOnClick_btnImprimir(3)  );"
                                Text="Imprimir Resumido" ToolTip="Imprimir Documento" Width="125px" Visible="False" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimirFacElectronica" runat="server" OnClientClick="return( val_facele() );"
                                Text="Imprimir Factura Electr�nica" ToolTip="Imprimir Documento" Width="125px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEmitirGuiaRemision" runat="server" OnClientClick="return(  valOnClick_btnEmitirGuiaRemision()  );"
                                Text="Despacho" ToolTip="Emitir Despacho de Mercader�a" Width="80px" Visible="False"
                                Enabled="false" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscarDocumentoRef" runat="server" Text="Cotizaci�n" Font-Bold="true"
                                OnClientClick="  return(  BuscarDocumentoRef() );  " ToolTip="Canjear Cotizaci�n"
                                Width="100px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEmitirPercepcion" runat="server" OnClientClick="return(  onCLick_EmmitirPercepcion()  );"
                                Text="Percepci�n" ToolTip="Emitir Comprobante de Percepci�n Interna" Width="100px" />
                        </td> 
                        <td>
                            <asp:Button ID="btncheckadd" runat="server" Text="Agregar Check" OnClientClick ="return(validacion());" />
                        </td>  
                        <td>
                            <asp:Button ID="btncheckdel" runat="server" Text="Quitar Check" OnClientClick ="return(validacion());" />
                        </td>  
                         <td>
                            <asp:Button ID="btnchangepl" runat="server" Text="Cambiar Punto de Llegada" Visible="false" />
                        </td> 
                         
                           <td>
                            <asp:Button ID="btnaparecer" runat="server" Text="Guardar Punto de Llegada" Visible="false"
                                OnClientClick ="return(validacion2());" />
                        </td> 
                        <td>
                        <asp:Button id="btnCambvende" runat="server" text="Cambiar Vendedor"  Visible="false"   />
                        </td>      
                           <td>
                        <asp:Button id="btnGuardarVende" runat="server" text="Grabar Nuevo Vendedor"  Visible="false"  OnClientClick ="return(validacion3());" />
                        </td>
                                                   <td>
                        <asp:Button id="btnGEnerarFacElectronica" runat="server" text="xxx"  Visible="true" 
                        OnClientClick="this.disabled;this.value='Procesando...'" UseSubmitBehavior="false"/>
                        </td>                                                        
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCeldaLeft">
                <asp:Image ID="imgCabecera" runat="server" Width="15px" />&nbsp;FACTURACI�N - CAJA
                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="Panel_Cab"
                    ImageControlID="imgCabecera" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                    Collapsed="false" CollapseControlID="imgCabecera" ExpandControlID="imgCabecera" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="LabelRojo" style="font-weight: bold">
                                Tipo Documento:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoDocumento" Width="100%" runat="server" CssClass="LabelRojo"
                                    Font-Bold="true" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                               <td>
                                <asp:Label ID="idcoti" runat="server" text =""></asp:Label>
                            </td>
                            <td>
                   
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True" onClick="  return(   valOnClick_cboEmpresa()     ); ">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True" Width="100%">
                                </asp:DropDownList>
                            </td>
                                                       
                            <td>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarDocumentoxCodigo">
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeyPress="return( valOnKeyPressCodigoDoc(this,event)  );"
                                    ReadOnly="true" Width="100px"></asp:TextBox>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" ToolTip="Buscar Documento por [ Serie - N�mero ]." />
                            </td>                            
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado());" ToolTip="B�squeda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Moneda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboMoneda" runat="server" AutoPostBack="true" CssClass="LabelRojo"
                                    Font-Bold="true" onClick=" return( valOnChange_cboMoneda()  ); " onKeypress=" return( valOnChange_cboMoneda()  ); "
                                    onKeyup=" return( valOnChange_cboMoneda()  ); " Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Tipo Operaci�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" Width="100%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td colspan="3" class="TextoRojo">
                                <asp:CheckBox ID="ckProductoComprometidos" runat="server" Text="Productos Comprometidos"
                                    Font-Bold="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Caja:
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="cboCaja" ForeColor="Red" Font-Bold="true" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Fecha a Entregar:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtFechaAEntregar" runat="server" CssClass="TextBox_Fecha" onblur="return(     valFecha_Blank(this)  );"
                                    Width="100px"></asp:TextBox>
                                <cc1:CalendarExtender ID="txtFechaAEntregar_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaAEntregar">
                                </cc1:CalendarExtender>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaAEntregar">
                                </cc1:MaskedEditExtender>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Almac�n Ref.:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboAlmacenReferencia" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Vendedor:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboUsuarioComision" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td >
                               <asp:Label ID="lblnuevovendedor" runat="server"  Text="Nuevo Vendedor:" style="color:Blue"></asp:Label> 
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboNuevoUsuario" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                             <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="imgDocumentoRef" runat="server" Width="15px" />&nbsp;COTIZACI�N DE
                            REFERENCIA
                            <cc1:CollapsiblePanelExtender ID="cpeDocumentoRef" runat="server" TargetControlID="Panel_DocumentoRef"
                                ImageControlID="imgDocumentoRef" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                Collapsed="true" CollapseControlID="imgDocumentoRef" ExpandControlID="imgDocumentoRef">
                            </cc1:CollapsiblePanelExtender>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_DocumentoRef" runat="server" Width="100%">
                                <table>
                                    <tr>
                                        <td align="right" class="Texto">
                                            Documento:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblTipoDocumentoRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="-"></asp:Label>
                                                    </td>
                                                    <td style="width: 15px">
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblNroDocumentoRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="-"></asp:Label>
                                                    </td>
                                                    <td style="width: 15px">
                                                    </td>
                                                    <td align="right" class="Texto">
                                                        Tipo Operacion:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTipoOperacionRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="-"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" class="Texto">
                                            Fecha Emisi�n:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblFechaEmisionRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td align="right" class="Texto">
                                            Fecha Vcto.:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblFechaVctoRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td align="right" class="Texto">
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="Texto">
                                            Cliente:
                                        </td>
                                        <td colspan="3" style="width: 400px">
                                            <asp:Label ID="lblDescripcionClienteRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td align="right" class="Texto">
                                            R.U.C.:
                                        </td>
                                        <td style="width: 150px">
                                            <asp:Label ID="lblRucRef" CssClass="LabelRojo" Font-Bold="true" runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td align="right" class="Texto">
                                            D.N.I.:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDniRef" CssClass="LabelRojo" Font-Bold="true" runat="server" Text="-"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="Texto">
                                            Condici�n Pago:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCondicionPagoRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td align="right" class="Texto">
                                            Medio Pago:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMedioPagoRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td align="right" class="Texto">
                                            Cr�dito (d�as):
                                        </td>
                                        <td>
                                            <asp:Label ID="lblNroDiasVigenciaRef" runat="server" CssClass="LabelRojo" Font-Bold="true"
                                                Text="-"></asp:Label>
                                        </td>
                                        <td align="right" class="Texto">
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="Texto">
                                            Total:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMonedaTotalRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="-"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTotalRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="0"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" class="Texto">
                                            Percepci�n:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMonedaPercepcionRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="-"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblPercepcionRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="0"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" class="Texto">
                                            Total a Pagar:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMonedaTotalAPagarRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="-"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTotalAPagarRef" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                            Text="0"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" class="Texto">
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="Texto">
                                            Observaciones:
                                        </td>
                                        <td colspan="7">
                                            <asp:TextBox ID="txtObservaciones_DocRef" Enabled="false" Text="" TextMode="MultiLine"
                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                runat="server" Width="100%" Height="70px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pn_MaestroObra" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td class="TituloCeldaLeft">
                                <asp:Image ID="ImageMaestroObra" runat="server" />&nbsp;MAESTRO DE OBRA
                                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_Panel_MaestroObra" runat="server"
                                    TargetControlID="Panel_MaestroObra" ImageControlID="ImageMaestroObra" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                    CollapsedImage="~/Imagenes/Mas_B.JPG" Collapsed="true" CollapseControlID="ImageMaestroObra"
                                    ExpandControlID="ImageMaestroObra">
                                </cc1:CollapsiblePanelExtender>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_MaestroObra" runat="server" Width="100%">
                                    <table>
                                        <tr>
                                            <td class="Texto" align="right">
                                                Maestro:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMaestro" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                    Width="450px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnBuscarMaestroObra" runat="server" CssClass="btnBuscar" Text="Buscar" Width="80px" OnClientClick="return(  buscarMaestroObra()  );" />
                                                <asp:ImageButton ID="btnLimpiarMaestroObra" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                    onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';"
                                                    OnClientClick="return ( LimpiarMaestroObra() );" />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto" align="right">
                                                D.N.I.:
                                            </td>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtDni_Maestro" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                Width="150px"></asp:TextBox>
                                                        </td>
                                                        <td class="Texto" align="right">
                                                            R.U.C.:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtRuc_Maestro" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                Width="150px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                CLIENTE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cliente" runat="server">
                    <table cellspacing="0" style="width: 100%">
                        <tr>
                            <td style="text-align: right;">
                                &nbsp;
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarCliente" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                OnClientClick="return(  buscarCliente() );" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                onmouseover="this.src='../Imagenes/Buscar_A.JPG';" TabIndex="13" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnNuevoCliente" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG"
                                                OnClientClick="return(onCapaCliente());" onmouseout="this.src='../Imagenes/Nuevo_b.JPG';"
                                                onmouseover="this.src='../Imagenes/Nuevo_A.JPG';" TabIndex="14" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnEditar_Cliente" runat="server" ImageUrl="~/Imagenes/Editar_B.JPG"
                                                OnClientClick=" return(    valEditarCliente()   ); " onmouseout="this.src='../Imagenes/Editar_B.JPG';"
                                                onmouseover="this.src='../Imagenes/Editar_A.JPG';" Visible="true" />
                                        </td>
                                        <td class="Texto">
                                            Tipo Persona:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoPersona_PanelCliente" runat="server" Enabled="False"
                                                Font-Bold="True" Width="116px">
                                                <asp:ListItem Value="0">---------</asp:ListItem>
                                                <asp:ListItem Value="1">Natural</asp:ListItem>
                                                <asp:ListItem Value="2">Jur�dica</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Ap. Paterno o R. Social:"></asp:Label>
                            </td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="370px">
                                            <asp:TextBox ID="txtApPaterno_RazonSocial" runat="server" CssClass="TextBoxReadOnly"
                                            ReadOnly="True" Width="370px"></asp:TextBox>
                                        </td>
                                        <td width="80px">
                                            <asp:Label ID="Label39_12" runat="server" CssClass="Label" Text="Ap. Materno:"></asp:Label>
                                        </td>
                                        <td width="200px">
                                            <asp:TextBox ID="txtApMaterno" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                            Width="200px"></asp:TextBox>
                                        </td>
                                        <td width="60px">
                                            <asp:Label ID="Label40" runat="server" CssClass="Label" Text="Nombre:"></asp:Label>
                                        </td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtNombres" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                            Width="150px"></asp:TextBox>
                                        </td>
                                        <td width="100px">
                                            <asp:TextBox ID="txtCodigoCliente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                            Width="100px"></asp:TextBox>
                                        </td>
                                        <td width="100px">
                                            <asp:Panel ID="Panel9" runat="server" DefaultButton="btnBuscarClientexIdCliente">                                
                                            <asp:TextBox ID="txtIdCliente_BuscarxId" runat="server" onFocus="return( aceptarFoco(this)  );"
                                            onKeyPress="return(  valKeyPressBuscarPersonaxId(event)  );" TabIndex="16" Width="100px"></asp:TextBox>
                                            </asp:Panel>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarClientexIdCliente" runat="server" ImageUrl="~/Imagenes/Busqueda_b.JPG"
                                            OnClientClick="return(valBuscarClientexId());" onmouseout="this.src='../Imagenes/Busqueda_b.JPG';"
                                            onmouseover="this.src='../Imagenes/Busqueda_A.JPG';" Style="width: 27px" TabIndex="17" />    
                                        </td>
                                    </tr>
                                </table>                                                                
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label3" runat="server" CssClass="Label" Text="Dni"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDni" runat="server" CssClass="TextBoxReadOnly" MaxLength="8"
                                    ReadOnly="True" Style="margin-left: 0px; margin-bottom: 0px" Width="70px"></asp:TextBox>
                                <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Ruc"></asp:Label>
                                <asp:TextBox ID="txtRuc" runat="server" CssClass="TextBoxReadOnly" MaxLength="11"
                                    ReadOnly="True" Width="90px"></asp:TextBox>
                                <asp:Label ID="Label12021" runat="server" CssClass="Label" Text="Precio de Venta"></asp:Label>
                                <asp:DropDownList ID="cboTipoPrecioV" runat="server" onClick="  return(   valOnClick_cboTipoPrecioV()    ); "
                                    AutoPostBack="true" Enabled="false" Font-Bold="True" Width="123px" onKeypress=" return(       valOnClick_cboTipoPrecioV()        ); ">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; height: 25px;">
                                <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Direcci�n"></asp:Label>
                            </td>
                            <td style="height: 25px">
                                <asp:TextBox ID="txtDireccionCliente" runat="server" CssClass="TextBoxReadOnly" MaxLength="50"
                                    ReadOnly="True" Width="661px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label12022" runat="server" CssClass="Label" Text="Tipo Agente"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoAgente" runat="server" Enabled="False" Font-Bold="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label12025" runat="server" CssClass="Label" Text="Tasa del Agente %"></asp:Label>
                                <asp:TextBox ID="txtTasaAgente" runat="server" CssClass="TextBox_ReadOnly" MaxLength="11"
                                    ReadOnly="True" Width="65px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="GV_LineaCredito" runat="server" AutoGenerateColumns="false" Width="500px">
                    <Columns>
                        <asp:TemplateField HeaderText="Otorgado" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                            <asp:HiddenField ID="hddIdCuenta" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCuentaPersona")%>' />

                                            <asp:HiddenField ID="hddIdMoneda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                            <asp:Label ID="lblMonedaOtorgado" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>

                                            <asp:Label ID="lblOtorgado" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CargoMaximo","{0:F2}")%>'></asp:Label>

                                            <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
  
                            </ItemTemplate>
                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Disponible" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                            <asp:Label ID="lblMonedaDisponible" ForeColor="Red" Font-Bold="true" runat="server"
                                                Text='<%#DataBinder.Eval(Container.DataItem,"MonedaSimbolo")%>'></asp:Label>

                                            <asp:Label ID="lblDisponible" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Saldo","{0:F2}")%>'></asp:Label>

                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GrillaHeader" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                <asp:GridView ID="GV_ComponenteKit" runat="server" AutoGenerateColumns="false" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="CodigoProd_Comp" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                            ItemStyle-Font-Bold="true" />
                        <asp:BoundField DataField="Componente" HeaderText="Componente" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                            ItemStyle-Font-Bold="true" />
                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                            <asp:Label ID="lblCantidad_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad_Comp","{0:F3}")%>'></asp:Label>

                                            <asp:Label ID="lblUnidadMedida_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida_Comp")%>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Precio" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                            <asp:Label ID="lblMonedaPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaPrecio_Comp")%>'></asp:Label>

                                            <asp:Label ID="lblPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Precio_Comp","{0:F3}")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="GrillaHeader" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
                LISTADO DE PRODUCTOS
            </td>
        </tr>
        <tr>
            <td>
                <div id="capaDetalle" onmouseover="return capaDetalle_onmouseover()" onmouseout="return capaDetalle_onmouseout()">
                    <asp:Panel ID="Panel_Detalle" runat="server" Width="100%" CssClass="PanelCodBarrasBackground">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table class="TextBox_CodigoBarras">
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="btnAgregar" runat="server" ImageUrl="~/Imagenes/BuscarProducto_A.JPG"
                                                    OnClientClick="return(valOnClick_btnAgregar());" onmouseout="this.src='../Imagenes/BuscarProducto_A.JPG';"
                                                    onmouseover="this.src='../Imagenes/BuscarProducto_A.JPG';" CssClass="ComboRojo" />
                                            </td>
                                            <td runat="server" id="tdSecCodBarras">
                                                <asp:Label ID="lblCodBarras" runat="server" Text="Codigo Barras"></asp:Label>
                                                <asp:TextBox ID="txtCodBarrasPrincipal" runat="server" onKeypress="return( valKeyPressDescripcionProdCodBarras(this) );"
                                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus=" val_CodBarras('1');onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                    AutoCompleteType="None"></asp:TextBox>
                                                <asp:Button ID="btnAceptarCodBarras" runat="server" Text="Aceptar" OnClientClick="return(valOnClick_btnAgregarCodBarras());" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="btnLimpiarDetalleDocumento" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                    onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="Panel_GV_Detalle" runat="server" Width="100%" ScrollBars="Horizontal">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="false" Width="100%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                                <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                                                <asp:LinkButton CssClass="Texto" ID="lkbQuitar" Font-Bold="true" runat="server" OnClick="lkbQuitar_Click">Quitar</asp:LinkButton>
   
                                                                </ItemTemplate>
                                                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Left" Width="50px"/>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center" />
                                                            <asp:TemplateField HeaderText="Producto" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                                <asp:Label ID="lblDescripcion" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomProducto")%>'></asp:Label>

                                                                                <asp:HiddenField ID="hddPrecioComercial" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"pvComercial","{0:F2}")%>' />

                                                                                <asp:HiddenField ID="hddcodBarraProductoDetDoc" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Prod_CodigoBarras")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                                <asp:TextBox ID="txtCantidad" TabIndex="100" onblur=" return(      valPorcentMaximoDctoxArticulo_OnBlur(this)    ); "
                                                                                    onKeyPress="return(    validarNumeroPuntoPositivo('event')       );" onFocus="return(   aceptarFoco(this)   );"
                                                                                    onKeyup="return(   calcularTotales_GV_Detalle(0)           );" runat="server"
                                                                                    Width="60px" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F4}")%>'></asp:TextBox>

                                                                                <asp:DropDownList ID="cboUM" DataTextField="DescripcionCorto" DataValueField="Id"
                                                                                    runat="server" OnSelectedIndexChanged="cboUnidadMedida_GV_Detalle" AutoPostBack="true">
                                                                                </asp:DropDownList>

                                                                                <asp:ImageButton ID="btnLongitud" runat="server" ImageUrl="~/Imagenes/Longitud_B.JPG"
                                                                                    OnClientClick="return(      valOnClick_btnLongitud(this)      );" onmouseout="this.src='../Imagenes/Longitud_B.JPG';"
                                                                                    onmouseover="this.src='../Imagenes/Longitud_A.JPG';" />

                                                                                <asp:ImageButton ID="btnArea" runat="server" ImageUrl="~/Imagenes/Area_b.JPG" OnClientClick="return(  valOnClick_btnArea(this)   );"
                                                                                    onmouseout="this.src='../Imagenes/Area_b.JPG';" onmouseover="this.src='../Imagenes/Area_A.JPG';" />

                                                                                <asp:ImageButton ID="btnAddGlosaDetalle" runat="server" ToolTip="Agregar Glosa al detalle"
                                                                                    ImageUrl="~/Imagenes/Glosa_B.jpg" OnClientClick=" return(  onCapaGlosa(this)      );  "
                                                                                    onmouseout="this.src='../Imagenes/Glosa_B.jpg';" onmouseover="this.src='../Imagenes/Glosa_A.jpg';" />

                                                                                <asp:ImageButton ID="btnMostrarComponenteKit" runat="server" ImageUrl="~/Imagenes/search_add.ico"
                                                                                    Width="20px" ToolTip="Visualizar Componentes del Kit" OnClick="btnMostrarComponenteKit_Click" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" Width="180px"/>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="PrecioLista" DataFormatString="{0:F2}" HeaderText="P. Lista"
                                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                            <asp:TemplateField HeaderText="P. Venta" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                                <asp:TextBox ID="txtPrecioVenta" TabIndex="101" CssClass="TextoRight" onblur=" return(      valPorcentMaximoDctoxArticulo_OnBlur(this)    ); "
                                                                                    onKeyPress="return(    validarNumeroPuntoPositivo('event')       );" onFocus="return(   aceptarFoco(this)   );"
                                                                                    onKeyup="return(   calcularTotales_GV_Detalle(1,this)           );" runat="server"
                                                                                    Width="60px" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioSD","{0:F2}")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Desc." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                                <asp:TextBox ID="txtDescuento_Valor" TabIndex="102"  onblur=" return(      valPorcentMaximoDctoxArticulo_OnBlur(this)    ); "
                                                                                    onKeyPress="return( false );" onFocus="return(   aceptarFoco(this)   );" CssClass="TextBoxReadOnly_Right"
                                                                                    onKeyup="return(   calcularTotales_GV_Detalle(2)           );" runat="server"
                                                                                    Width="50px" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Descuento","{0:F4}")%>'></asp:TextBox>
                                                                                <asp:TextBox ID="txtDctoValor_MaximoxPVenta" onKeypress="return( false );" onFocus="return(  valOnFocus_txtImporte()    );"
                                                                                     runat="server" Width="40px" Font-Bold="true" CssClass="TextBoxReadOnly_Right"
                                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"DctoValor_MaximoxPVenta","{0:F4}")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" Width="140px" />
                                                            </asp:TemplateField>
                                                            
                                                             <%--CssClass="TextBoxReadOnly_Right"--%>
                                                            <asp:TemplateField HeaderText="Desc (%)." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                                <asp:TextBox ID="txtDescuento_Porcent" TabIndex="103"  onblur=" return(      valPorcentMaximoDctoxArticulo_OnBlur(this)   ); "
                                                                                    onKeyPress="return( false );" onFocus="return(   aceptarFoco(this)   );"
                                                                                    onKeyup="return(   calcularTotales_GV_Detalle(3));" runat="server" Width="50px" CssClass="TextBoxReadOnly_Right"
                                                                                    Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"PorcentDcto","{0:F2}")%>' Enabled="True"></asp:TextBox>

                                                                                <asp:TextBox ID="txtDctoPorcent_MaximoxPVenta" onKeypress="return( false );" onFocus="return(  valOnFocus_txtImporte()    );"
                                                                                     runat="server" Width="40px" Font-Bold="true" CssClass="TextBoxReadOnly_Right"
                                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"DctoPorcent_MaximoxPVenta","{0:F2}")%>'></asp:TextBox>

                                                                </ItemTemplate>
                                                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" Width="140px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="P.V. Final" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                                <asp:TextBox ID="txtPrecioVentaFinal" TabIndex="104" onKeypress=" return(  false  ); "
                                                                                    onFocus="return(  valOnFocus_txtImporte()    );" CssClass="TextBoxReadOnly_Right"
                                                                                    runat="server" Width="60px" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCD","{0:F2}")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Importe" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                                <asp:TextBox ID="txtImporte" onKeypress=" return(  false  ); " onFocus="return(  valOnFocus_txtImporte()    );"
                                                                                    CssClass="TextBoxReadOnly_Right" runat="server" Width="60px" Font-Bold="true"
                                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Importe","{0:F2}")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="TasaPercepcion" DataFormatString="{0:F2}" HeaderText="Percep. (%)"
                                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                                <asp:ImageButton ID="btnPermisoDcto_GV_Detalle" runat="server" ToolTip="Permiso por Descuento"
                                                                                    ImageUrl="~/Imagenes/Ok_b.bmp" OnClientClick="return(    valOnClick_btnPermisoDcto_GV_Detalle(this)    );" />

                                                                                <asp:HiddenField ID="hddPorcentDctoMaximo" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"PorcentDctoMaximo","{0:F4}")%>' />

                                                                                <asp:HiddenField ID="hddIdSupervisor" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUsuarioSupervisor")%>' />

                                                                                <asp:HiddenField ID="hddGlosa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"DetalleGlosa")%>' />

                                                                                <asp:HiddenField ID="hddVolumenVentaMin" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"VolumenVentaMin")%>' />

                                                                                <asp:ImageButton ID="btnConsultarVolumenVentaMin"
                                                                                    runat="server" ToolTip="Modificar Tipo de Precio de Venta" ImageUrl="~/Imagenes/Ok_b.bmp"
                                                                                    OnClientClick="return(    valOnClick_btnConsultarVolumenVentaMin(this)    );" />
                                                                                     <%--OnClick="btnConsultarVolumenVentaMin_Detalle_OnClick"--%>

                                                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />

                                                                                <asp:HiddenField ID="hddPrecioBaseDcto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"PrecioBaseDcto")%>' />

                                                                                <asp:ImageButton ID="btnEquivalencia_PR" runat="server" ToolTip="Calcular Equivalencia"
                                                                                    ImageUrl="~/Imagenes/Ok_b.bmp" OnClientClick="return(    valOnClick_btnEquivalencia_PR(this)    );" />
                                                                                     <%--OnClick="btnEquivalencia_PR_Click" --%>

                                                                                <asp:ImageButton ID="btnViewCampania" runat="server" ImageUrl="~/Imagenes/regalo.jpg"
                                                                                    Width="20px" ToolTip="Producto en Campa�a"  OnClick="valOnClick_btnViewCampania_GV_Detalle"  />
                                                                                  
                                                                                <asp:HiddenField ID="hddHelp" runat="server" />

                                                                                <asp:HiddenField ID="hddIdProductoAux" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProductoAux")%>' />

                                                                                <asp:HiddenField ID="hddIdCampania" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCampania")%>' />

                                                                                <asp:HiddenField ID="hddCantTrans" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"CantidadDetalleAfecto")%>' />
                                                                </ItemTemplate>
                                                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" Width="85px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                        <FooterStyle CssClass="GrillaFooter" />
                                                        <PagerStyle CssClass="GrillaPager" />
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                <table align="right">
                    <tr>
                        <td class="Texto">
                            Descuento maximo:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDescuentoMax" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( false );"
                                onFocus="return ( onFocus_Readonly(this) );" Width="90px" Font-Size="10pt"></asp:TextBox>
                        </td>
                        <td style="text-align: right" class="Texto">
                            Descuento:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDescuento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( false );"
                                onFocus="return ( onFocus_Readonly(this) );" Width="90px">0</asp:TextBox>
                        </td>
                        <td style="width: 90px; text-align: right;" class="Texto">
                            Importe Total:
                        </td>
                        <td>
                            <asp:TextBox ID="txtImporteTotal" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( false );"
                                onFocus="return ( onFocus_Readonly(this) );" Width="90px">0</asp:TextBox>
                        </td>
                        <td style="width: 85px;" class="Texto">
                            Conceptos:
                        </td>
                        <td>
                            <asp:TextBox ID="txtTotalConcepto_PanelDetalle" runat="server" CssClass="TextBox_ReadOnly"
                                onKeypress="return( false );" onFocus="return ( onFocus_Readonly(this) );" Width="90px">0</asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Descuento Global:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDescuentoGlobal" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( false );"
                                onFocus="return ( onFocus_Readonly(this) );" Width="90px">0</asp:TextBox>
                            <asp:ImageButton ID="btnOpen_DescuentoGlobal" runat="server" ToolTip="Ingresar Descuento Global"
                                ImageUrl="~/Caja/iconos/edit_text.gif" />
                        </td>
                        <td class="Texto">
                            Valor de Venta:
                        </td>
                        <td>
                            <asp:TextBox ID="txtSubTotal" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( false );"
                                onFocus="return ( onFocus_Readonly(this) );" Width="90px">0</asp:TextBox>
                        </td>
                        <td style="width: 90px;" class="Texto">
                            I.G.V.:
                            <asp:Label ID="lblPorcentIgv" runat="server" Text="0"></asp:Label>
                            %
                        </td>
                        <td>
                            <asp:TextBox ID="txtIGV" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( false );"
                                onFocus="return ( onFocus_Readonly(this) );" Width="90px">0</asp:TextBox>
                        </td>
                        <td style="width: 85px; text-align: right;" class="Texto">
                            Total:
                        </td>
                        <td>
                            <asp:Label ID="lblMonedaTotal" runat="server" CssClass="Label" Text="-"></asp:Label>
                            <asp:TextBox ID="txtTotal" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( false );"
                                onFocus="return ( onFocus_Readonly(this) );" Width="90px">0</asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="Texto">
                            No Afecto I.G.V:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNoAfectoIgv" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( false );"
                                onFocus="return ( onFocus_Readonly(this) );" Width="90px">0</asp:TextBox>
                        </td>
                        <td class="Texto" style="width: 90px;">
                            Percepci�n:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPercepcion" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( false );"
                                onFocus="return ( onFocus_Readonly(this) );" Width="90px">0</asp:TextBox>
                        </td>
                        <td class="Texto" style="width: 85px; text-align: right;">
                            Total a Pagar:
                        </td>
                        <td>
                            <asp:Label ID="lblMonedaTotalPagar" runat="server" CssClass="Label" Text="-"></asp:Label>
                            <asp:TextBox ID="txtTotalAPagar" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( false );"
                                onFocus="return ( onFocus_Readonly(this) );" Width="90px">0</asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_ValorRef_TRANSPORTE" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td style="background-color: Yellow; font-weight: bold; text-align: center" class="Texto">
                                VALOR REFERENCIAL - SERVICIO DE TRANSPORTE
                            </td>
                        </tr>
                        <tr style="background-color: Yellow">
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold">
                                            Valor Referencial:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboValorReferencial" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboMoneda_ValorRef" runat="server" Font-Bold="true" ForeColor="Red">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtValorReferencial" onKeyUp=" return( valOnKeyUp_txtValorReferencial()  ); "
                                                onKeyPress=" return( validarNumeroPuntoPositivo('event') ); " onFocus=" return( aceptarFoco(this) ); "
                                                onBlur=" return( valBlur(event) ); " Text="0" Width="70px" Font-Bold="true" runat="server"></asp:TextBox>
                                        </td>
                                        <td style="font-weight: bold" class="Texto">
                                            Cantidad:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCantidad_Transporte" CssClass="TextBox_ReadOnly" Text="0" onKeyPress=" return( false ); "
                                                Width="70px" Font-Bold="true" runat="server"></asp:TextBox>
                                        </td>
                                        <td style="font-weight: bold" class="Texto">
                                            Valor Ref. Total:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtValorReferencial_Total" CssClass="TextBox_ReadOnly" onKeyPress=" return( false ); "
                                                Text="0" Width="90px" Font-Bold="true" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="TituloCelda" runat="server" id="trTituloPuntoPartida">
            <td>
                PUNTO DE PARTIDA
            </td>
        </tr>
        <tr runat="server" id="trDetallePuntoPartida">
            <td>
                <asp:Panel ID="Panel_PuntoPartida" runat="server" Width="100%">
                    <table border="0" cellspacing="0">
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Depto.:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="cboDepto_Partida" runat="server" Width="150px" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label10" runat="server" CssClass="Label" Text="Provincia"></asp:Label>
                                <asp:DropDownList ID="cboProvincia_Partida" runat="server" AutoPostBack="True" Width="250px">
                                </asp:DropDownList>
                                <asp:Label ID="Label11" runat="server" CssClass="Label" Text="Distrito"></asp:Label>
                                <asp:DropDownList ID="cboDistrito_Partida" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="Label12" runat="server" CssClass="Label" Text="Direcci�n"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtDireccion_Partida" runat="server" Width="608px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr runat="server" id="trTituloPuntoLlegada">
            <td class="TituloCelda">
                PUNTO DE LLEGADA
            </td>
        </tr>
        <tr runat="server" id="trDetallePuntoLlegada">
            <td>
                <asp:Panel ID="Panel_PuntoLlegada" runat="server" Width="100%">
                    <table border="0" cellspacing="0">
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="Label65" runat="server" CssClass="Label" Text="Depto.:"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="cboDepartamento" runat="server" Width="150px" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Label ID="Label66" runat="server" CssClass="Label" Text="Provincia"></asp:Label>
                                <asp:DropDownList ID="cboProvincia" runat="server" AutoPostBack="True" Width="250px">
                                </asp:DropDownList>
                                <asp:Label ID="Label67" runat="server" CssClass="Label" Text="Distrito"></asp:Label>
                                <asp:DropDownList ID="cboDistrito" runat="server" Width="250px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="Label68" runat="server" CssClass="Label" Text="Direcci�n"></asp:Label>
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtDireccion_Llegada" runat="server" Width="608px" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr runat="server" id="trtituloConceptos">
            <td class="TituloCelda">
                CONCEPTOS
            </td>
        </tr>
        <tr runat="server" id="trDetalleConcepto">
            <td>
                <asp:Panel ID="Panel_Concepto" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Button ID="btnAddDetalleCobro" Width="110px" runat="server" Text="Ingresar Nuevo" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Concepto" runat="server" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" SelectText="Quitar" HeaderStyle-Height="25px"
                                            HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Concepto" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:DropDownList ID="cboConcepto" AutoPostBack="true" OnSelectedIndexChanged="cboConcepto_SelectedIndexChanged"
                                                                runat="server" DataTextField="Nombre" DataValueField="Id" DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaConcepto")%>'>
                                                            </asp:DropDownList>

                                                            <asp:TextBox ID="txtDescripcionConcepto" Width="300px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Concepto")%>'
                                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"></asp:TextBox>

                                                            <asp:ImageButton ID="btnAdelanto" runat="server" ToolTip="Ingresar Monto de Adelanto"
                                                                ImageUrl="~/Imagenes/Ok_b.bmp" OnClick="btnAdelanto_Click" OnClientClick=" return( valOnClick_btnAdelanto() ); " />

                                                            <asp:HiddenField ID="hddConceptoAdelanto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"ConceptoAdelanto_Integer")%>' />

                                                            <asp:HiddenField ID="hddIdDocumentoRef" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumentoRef")%>' />

                                                            <asp:HiddenField ID="hddNoAfectoIgv" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"NoAfectoIgv_Integer")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Monto" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonedaMontoConcepto" Font-Bold="true" runat="server" ForeColor="Red"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>

                                                            <asp:TextBox ID="txtMonto" onKeypress=" return(    valOnKeyPress_txtMonto(this)    ); "
                                                                onFocus="  return(   aceptarFoco(this)   );  " onblur="return(   valBlur(event)     );"
                                                                onKeyup="  return( calcularTotalConcepto() ); " Width="100px" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F2}")%>'></asp:TextBox>

                                                            <asp:HiddenField ID="hddPorcentDetraccion" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"PorcentDetraccion","{0:F3}")%>' />

                                                            <asp:HiddenField ID="hddMontoMinimoDetraccion" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"MontoMinDetraccion","{0:F3}")%>' />

                                                            <asp:Label ID="lblNoAfectoIgv" Font-Bold="true" runat="server" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NoAfectoIgv_String")%>'></asp:Label>

                                                            <asp:HiddenField ID="hddPorcentDsctoGlobal" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"PorcentDsctoGlobal","{0:F3}")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Total Ingresado:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMonedaTotalGasto" ForeColor="Red" CssClass="LabelRojo" Font-Bold="true"
                                                runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTotalConcepto" CssClass="TextBox_ReadOnly" onFocus="return( lostFocusConcepto() );"
                                                onKeypress="return( false );" Width="90px" runat="server" Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCeldaLeft">
                <asp:Image ID="ImgGo" runat="server" Width="15px" />&nbsp;R�GIMEN DE OPERACI�N //
                RETENCI�N - DETRACCI�N
                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" TargetControlID="Panel_RegimenOperacion"
                    ImageControlID="ImgGo" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                    Collapsed="true" CollapseControlID="ImgGo" ExpandControlID="ImgGo">
                </cc1:CollapsiblePanelExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_RegimenOperacion" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold; text-align: right">
                                Retenci�n:
                            </td>
                            <td>
                                <asp:Label ID="lblMoneda_Retencion" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                    Text="-"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtRetencion" Width="80px" Font-Bold="true" onFocus="return( aceptarFoco(this)  );"
                                    onBlur="return( valBlur(event)  );" onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); "
                                    runat="server"></asp:TextBox>
                            </td>
                            <td class="Texto" style="font-weight: bold; text-align: right">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold; text-align: right">
                                Detracci�n:
                            </td>
                            <td>
                                <asp:Label ID="lblMoneda_Detraccion" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                    Text="-"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDetraccion" Width="80px" Font-Bold="true" onFocus="return( aceptarFoco(this)  );"
                                    onBlur="return( valBlur(event)  );" onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); "
                                    runat="server"></asp:TextBox>
                            </td>
                            <td class="Texto" style="font-weight: bold; text-align: right">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DATOS DE CANCELACI�N
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_DatosCancelacion" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCondicionPago_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                Text="Condici�n de Pago:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboCondicionPago" runat="server" AutoPostBack="True" onClick="  return(  valOnClick_cboCondicionPago(this)   );  ">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_CP_Contado" runat="server" Width="100%">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblMedioPago_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                Text="Medio de Pago:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboMedioPago" CssClass="LabelRojo" Font-Bold="true" runat="server"
                                                                AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="lblMonto_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Monto:"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="cboMoneda_DatoCancelacion" runat="server" Width="100px">
                                                            </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                    <asp:Panel ID="Panel16" runat="server" DefaultButton="btnAddDatoCancelacion">
                                                            <asp:TextBox ID="txtMonto_DatoCancelacion" onFocus=" return(  aceptarFoco(this)   ); "
                                                                runat="server" onKeypress="  return(  valOnKeyPress_txtMonto_DatoCancelacion(event)  ); "
                                                                Width="100px" Text="0"></asp:TextBox>
                                                                </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>                                                                                                                        
                                                        </td>
                                                        <td colspan="4">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Button ID="btnAddDatoCancelacion" runat="server" Text="Agregar" Width="70px"
                                                                            OnClientClick=" return(   valOnClick_btnAddDatoCancelacion()  ); " />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnVer_NotaCredito" runat="server" OnClientClick=" return(   valOnClick_btnVer_NotaCredito()  ); "
                                                                            Text="Buscar N/C" Width="90px" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblPost_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Tipo Tarjeta:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboPost_DC" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            <asp:Label ID="lblTarjeta_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Tarjeta:"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="cboTarjeta_DC" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblBanco_DC" CssClass="Texto" Font-Bold="true" runat="server" Text="Banco:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboBanco" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblCuentaBancaria_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                Text="Cuenta:"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="cboCuentaBancaria" runat="server" Width="100%">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td colspan="4">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Button ID="btnEnviarSolicitudAprobac�on" runat="server" OnClientClick=" return(  valOnClick_btnEnviarSolicitudAprobac�on()  ); "
                                                                            Text="Solicitar Aprob." ToolTip="Solicitar Aprobaci�n" Width="110px" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnConsultarAprobacion" runat="server" Text="Consultar Aprob." ToolTip="Consultar Aprobaci�n"
                                                                            Width="110px" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblNro_DC" runat="server" CssClass="Texto" Font-Bold="true" Text="N�mero:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNro_DC" runat="server" onFocus=" return(  aceptarFoco(this)   ); "
                                                                onKeypress=" return(    valOnKeyPress_txtNro_DC()    ); " Width="90%"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align: right">
                                                            <asp:Label ID="lblFechaACobrar_DC" CssClass="Texto" Font-Bold="true" runat="server"
                                                                Text="Fecha a Cobrar:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFechaACobrar" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                                                Width="100px"></asp:TextBox>
                                                            <cc1:MaskedEditExtender ID="MaskedEditExtender6" runat="server" ClearMaskOnLostFocus="false"
                                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaACobrar">
                                                            </cc1:MaskedEditExtender>
                                                            <cc1:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                TargetControlID="txtFechaACobrar">
                                                            </cc1:CalendarExtender>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GV_Cancelacion" runat="server" AutoGenerateColumns="false" Width="100%">
                                                    <Columns>
                                                        <asp:CommandField ButtonType="Link" SelectText="Quitar" ItemStyle-HorizontalAlign="Center"
                                                            ShowSelectButton="true" ItemStyle-Width="75px" />
                                                        <asp:TemplateField HeaderText="Monto" ItemStyle-Width="120px" HeaderStyle-Height="25px"
                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblMonedaMonto" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMonedaDestino")%>'></asp:Label>

                                                                            <asp:Label ID="lblMonto" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MontoEquivalenteDestino","{0:F2}")%>'></asp:Label>

                                                                            <asp:HiddenField ID="hddIdMedioPago" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMedioPago")%>' />

                                                                            <asp:HiddenField ID="hddIdMonedaOrigen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="descripcionPagoCaja" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderText="Descripci�n" />
                                                    </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Total Recibido:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaTotalRecibido" Text="-" runat="server" CssClass="LabelRojo"
                                                                            Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtTotalRecibido" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="TextBox_ReadOnly" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Faltante:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaFaltante" Text="-" runat="server" CssClass="LabelRojo" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFaltante" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="Faltante" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="Texto" align="right">
                                                            Vuelto:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMonedaVuelto" Text="-" runat="server" CssClass="LabelRojo" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtVuelto" onKeypress="return(  false  );" onFocus="return(  valFocusCancelacion() );"
                                                                            Width="90px" Text="0" CssClass="Vuelto" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_CP_Credito" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Medio de Pago:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboMedioPago_Credito" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Texto" style="font-weight: bold">
                                                N� Dias:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtnrodias" runat="server" MaxLength="3" onKeyPress=" return(   validarNumeroPuntoPositivo('event')   );   "
                                                    Width="40px" onFocus="return ( aceptarFoco(this)  );" onKeyUp="  return(   validarNrodias()   );  "></asp:TextBox>
                                            </td>
                                            <td class="Texto" style="font-weight: bold">
                                                Fecha de Vcto.:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaVcto" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="txtFechaEmision0_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaVcto">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="txtFechaVcto_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaVcto">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="Image_Vehiculo" runat="server" />&nbsp;VEH�CULO
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_Vehiculo" runat="server"
                                TargetControlID="Panel_Vehiculo" ImageControlID="Image_Vehiculo" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" Collapsed="true" CollapseControlID="Image_Vehiculo"
                                ExpandControlID="Image_Vehiculo">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Vehiculo" runat="server" Width="100%">
                                <table>
                                    <tr>
                                        <td align="right" class="Texto">
                                            Modelo / Marca:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtModelo_Vehiculo" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="200px"></asp:TextBox>
                                                    </td>
                                                    <td align="right" class="Texto">
                                                        Nro. Placa:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPlaca_Vehiculo" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="120px"></asp:TextBox>
                                                    </td>
                                                    <td align="right" class="Texto">
                                                        Nro. Certificado MTC:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCertificado_Vehiculo" runat="server" CssClass="TextBoxReadOnly"
                                                            ReadOnly="True" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarVehiculo" runat="server" Text="Buscar" Width="80px" OnClientClick=" return(   valOnClick_btnBuscarVehiculo()      ); " />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%--Fin vehiculo--%>
        <tr>
            <td class="TituloCeldaLeft">
                <asp:Image ID="imgobs" runat="server" Width="15px" />&nbsp;OBSERVACIONES
                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="Panel_Obs"
                    ImageControlID="imgobs" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                    Collapsed="true" CollapseControlID="imgobs" ExpandControlID="imgobs">
                </cc1:CollapsiblePanelExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" Width="100%" runat="server"
                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                    Height="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddIdDocumentoRef" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumentoRef" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdTipoOperacionRef" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdTipoPVDefault" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddIdMedioPagoInterfaz" runat="server" />
                <asp:HiddenField ID="hddIdMedioPagoPrincipal" runat="server" />
                <asp:HiddenField ID="hddIdMaestroObra" runat="server" />
                <asp:HiddenField ID="hddBusquedaMaestro" runat="server" Value="0" />
                <asp:HiddenField ID="hddPermiso_OnlyPanel_Cancelacion" runat="server" Value="0" />
                <asp:HiddenField ID="hddPermiso_AddMedioPago" runat="server" Value="0" />
                <asp:HiddenField ID="hddIndexGlosa_Gv_Detalle" runat="server" />
                <asp:HiddenField ID="hddMontoMaxAfectoPercepcion" runat="server" Value="0" />
                <asp:HiddenField ID="hddSujetoRetencion" runat="server" Value="0" />
                <asp:HiddenField ID="hddIndex_Campania_Producto" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdUnidadMedida_RetazoLongitud" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdUnidadMedida_RetazoArea" runat="server" Value="0" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hddCodBarras" runat="server" Value="0" />
                <asp:HiddenField ID="hddValidarBoleta" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdVehiculo" runat="server" />
                <asp:HiddenField ID="hdddoc_CompPercepcion" runat="server" />
                <asp:HiddenField ID="hddMontoAfectoRetencion" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
                <table>
                    <tr>
                        <td>
                            <asp:Image ID="btnOpen_Pregunta" runat="server" Width="1px" Height="1px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <cc1:ModalPopupExtender ID="ModalPopup_DescuentoGlobal" runat="server" TargetControlID="btnOpen_DescuentoGlobal"
                    PopupControlID="Panel_DescuentoGlobal" BackgroundCssClass="modalBackground" Enabled="true"
                    RepositionMode="None" CancelControlID="btnClose_DescuentoGlobal" Y="400">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="Panel_DescuentoGlobal" runat="server" CssClass="modalPopup" Style="display: none;">
                    <table>
                        <tr>
                            <td align="right">
                                <asp:Image ID="btnClose_DescuentoGlobal" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Descuento (%):
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMonto_DescuentoGlobal" CssClass="TextoRight" runat="server" Width="70px"
                                                Font-Bold="true" onKeyPress=" return(  valOnKeyPress_txtMonto_DescuentoGlobal(event)  ); "
                                                onKeyup=" return(  valBlur(event)  ); " onFocus="return(   aceptarFoco(this)  );"
                                                Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnAceptar_DescuentoGlobal" runat="server" Text="Aceptar" Width="80px"
                                    OnClientClick="return(  valOnClick_btnAceptar_DescuentoGlobal() );" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <cc1:ModalPopupExtender ID="ModalPopup_Pregunta" runat="server" TargetControlID="btnOpen_Pregunta"
                    PopupControlID="Panel_Pregunta" BackgroundCssClass="modalBackground2" Enabled="true"
                    RepositionMode="None" CancelControlID="btnClose_Pregunta" Y="80">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="Panel_Pregunta" runat="server" CssClass="modalPopup" Style="display: none;"
                    Width="350px">
                    <table width="100%">
                        <tr>
                            <td class="SubTituloCelda">
                                <asp:Label ID="lblTitulo" runat="server" Text="Certificaci�n Club Maestro de Obra"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblPregunta" runat="server" Font-Bold="true" CssClass="Texto" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:RadioButtonList ID="rbPregunta" runat="server" CssClass="Texto" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1" Text="SI"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="NO"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            <asp:Image ID="imgIcono" Height="20px" Width="20px" runat="server" ImageUrl="~/Caja/iconos/nav_help.gif" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnAceptar_MaestroObra" runat="server" Text="Aceptar" Width="80px"
                                                OnClientClick=" return ( Aceptar_MaestroObra() );" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnClose_Pregunta" runat="server" Text="Cancelar" Width="80px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>

    <div id="capaConfiguracionFrm" style="border: 3px solid blue; padding: 8px; width: auto;
        height: auto; position: absolute; top: 120px; left: 38px; background-color: white;
        z-index: 2; display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton9" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaConfiguracionFrm'));" />
                </td>
            </tr>
            <tr align="left">
                <td>
                    <table>
                        <tr>
                            <td class="Label" align="right">
                                Mover Stock F�sico:
                            </td>
                            <td style="width: 75px">
                                <asp:CheckBox ID="chb_MoverAlmacen" Enabled="true" Checked="false" runat="server"
                                    onClick=" return( valOnClick_chb_MoverAlmacen() ); " />
                            </td>
                            <td class="Label" align="right">
                                Comprometer Stock:
                            </td>
                            <td>
                                <asp:CheckBox ID="chb_ComprometerStock" Enabled="true" Checked="true" runat="server"
                                    onClick=" return(   valOnClick_chb_ComprometerStock()  ); " />
                            </td>
                        </tr>
                        <tr>
                            <td class="Label" align="right">
                                Separar Afectos / No Afectos:
                            </td>
                            <td style="width: 75px">
                                <asp:CheckBox ID="chb_SepararAfecto_NoAfecto" Checked="false" runat="server" />
                            </td>
                            <td class="Label" align="right">
                                Saltar Correlativo:
                            </td>
                            <td>
                                <asp:CheckBox ID="chb_SaltarCorrelativo" Checked="false" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Label" align="right">
                                Separar x Montos:
                            </td>
                            <td style="width: 75px">
                                <asp:CheckBox ID="chb_SepararxMontos" Checked="false" runat="server" />
                            </td>
                            <td class="Label" align="right">
                                Comprometer Percepci�n:
                            </td>
                            <td>
                                <asp:CheckBox ID="chb_CompPercepcion" Enabled="true" onClick="return(   valOnClickChb_CompPercepcion(this)    );"
                                    runat="server" Checked="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Label" align="right">
                                Usar Alias (Boleteo):
                            </td>
                            <td style="width: 75px">
                                <asp:CheckBox ID="chb_UseAlias" Checked="false" runat="server" />
                            </td>
                            <td class="Label" align="right">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional"><ContentTemplate><!--BUSCAR PRODUCTO-->
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Tipo Existencia:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cmbTipoExistencia" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                                DataTextField="Descripcion" DataValueField="Id">
                                            </asp:DropDownList>
                                            <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                            <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" AutoPostBack="true" runat="server"
                                                DataTextField="Nombre" DataValueField="Id">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Panel ID="Panel5" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">                                            
                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                onKeypress="return( valKeyPressDescripcionProd(this,event) );"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td class="Texto">
                                            C�d.:
                                        </td>
                                        <td>
                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">    
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                onKeypress="return( valKeyPressDescripcionProd(this,event) );" TabIndex="205"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';"
                                                />--%>
                                            <asp:Button ID="btnBuscarGrilla_AddProducto" runat="server" Text="Buscar" TabIndex="207" CssClass="btnBuscar"
                                            OnClientClick="this.disabled;this.value='Procesando...'" UseSubmitBehavior="false"/>
                                            
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../Imagenes/Agregar_A.JPG';"
                                                OnClientClick="return(valAddProductos());" TabIndex="208" />--%>
                                            <asp:Button ID="btnAddProductos_AddProducto" runat="server" Text="A�adir" TabIndex="208"  CssClass="btnA�adir"
                                            OnClientClick="return(valAddProductos());"  />

                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />--%>
                                            <asp:Button ID="btnLimpiar_AddProducto" runat="server" Text="Limpiar" TabIndex="209" CssClass="btnLimpiar"
                                            OnClientClick="return(limpiarBuscarProducto());"     />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnVerCampania_Consulta" runat="server" Text="Campa�as" Width="80px" CssClass="btnCampa�a"
                                                ToolTip="Ver Campa�as Vigentes" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chb_FiltroProductoCampania" runat="server" Checked="false" CssClass="Texto"
                                                Font-Bold="true" Text="Filtrar Productos en Campa�a." />
                                        </td>
                                        <td class="Label">
                                            Cod. Barras:
                                        </td>
                                        <td>
                                            <asp:Panel ID="Panel7" runat="server" DefaultButton="btnAddProductos_AddProducto">                                            
                                            <asp:TextBox ID="txtCodBarrasCapa" runat="server" onKeypress="return( valKeyPressDescripcionProdCodBarrasCapa(this,event) );"
                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                <asp:Panel ID="Panel10" runat="server" DefaultButton="btnAddProductos_AddProducto">
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%"
                        PageSize="20">
                        <Columns>
                            <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px">
                                <ItemTemplate>                                    
                                    <asp:TextBox ID="txtCantidad_AddProd" TabIndex="210" Width="65px" onKeyup="return(  valStockDisponible_AddProd() );"
                                        onblur="return( valBlurClear('0.00',event) );" onfocus="return(read_Lista(this));"
                                        onKeypress="return( valKeyPressCantidadAddProd(event)  );" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>'
                                        runat="server"></asp:TextBox>                                        
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo" />
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" NullDisplayText="---" />
                            <asp:TemplateField HeaderText="U.M.">
                                <ItemTemplate>
                                    <asp:DropDownList ID="cmbUnidadMedida_AddProd" runat="server" OnSelectedIndexChanged="cmbUnidadMedidaVenta_Catalogo_SelectedIndexChanged"
                                        AutoPostBack="true" TabIndex="211" DataTextField="DescripcionCorto" DataValueField="Id">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NomTipoPV" HeaderText="Tipo P.V." NullDisplayText="---" />
                            <asp:BoundField DataField="SimbMoneda" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"
                                NullDisplayText="---" />
                            <asp:BoundField DataField="PrecioSD" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"
                                HeaderText="P. Venta" NullDisplayText="0" DataFormatString="{0:F2}" />
                            <asp:BoundField DataField="StockDisponibleN" HeaderText="Stock Disponible" NullDisplayText="0"
                                DataFormatString="{0:F4}" />
                            <asp:BoundField DataField="Percepcion" HeaderText="Percepci�n (%)" NullDisplayText="0"
                                DataFormatString="{0:F2}" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                                <asp:ImageButton ID="btnConsultarStockAlmacenes_BuscarProd" runat="server" OnClick="mostrarCapaStockPrecioxProducto"
                                                    ImageUrl="~/Imagenes/Ok_b.bmp" ToolTip="Consultar Stock y Precios por Tienda / Almac�n."
                                                    OnClientClick = "this.src='../Imagenes/loader.gif'" />
                                                <asp:ImageButton ID="btnConsultarTipoPrecio" runat="server" OnClick="mostrarCapaConsultarTipoPrecio"
                                                    ImageUrl="~/Imagenes/Ok_b.bmp" ToolTip="Consultar Tipo Precios de Venta."
                                                    OnClientClick = "this.src='../Imagenes/loader.gif'" />

                                                <asp:HiddenField ID="hddIdProducto_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />

                                                <asp:ImageButton ID="btnMostrarComponenteKit_Find" runat="server" ImageUrl="~/Imagenes/search_add.ico"
                                                    Width="20px" ToolTip="Visualizar Componentes del Kit" OnClick="btnMostrarComponenteKit_Find_Click" />

                                                <asp:ImageButton ID="btnViewCampania" runat="server" ImageUrl="~/Imagenes/regalo.jpg"
                                                    Width="20px" ToolTip="Producto en Campa�a" OnClick="valOnClick_btnViewCampania" />

                                                <asp:HiddenField ID="hddCodBarraProdCapa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Prod_CodigoBarras")%>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ComponenteKit_Find" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="CodigoProd_Comp" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                ItemStyle-Font-Bold="true" />
                            <asp:BoundField DataField="Componente" HeaderText="Componente" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                ItemStyle-Font-Bold="true" />
                            <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCantidad_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad_Comp","{0:F3}")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUnidadMedida_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida_Comp")%>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Precio" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMonedaPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaPrecio_Comp")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Precio_Comp","{0:F3}")%>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:Panel ID="Panel15" runat="server" DefaultButton="btnIr_Productos">
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero(event));"></asp:TextBox>
                        </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </ContentTemplate></asp:UpdatePanel>

    <div id="capaConsultarStockPrecioxProducto" style="border: 3px solid blue; padding: 8px;
        width: 700px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa2('capaConsultarStockPrecioxProducto')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProductoConsultarStockPrecioxProd" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ConsultarStockPrecioxProd" runat="server" AutoGenerateColumns="False"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Tienda" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:HiddenField ID="hddIdTienda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTienda")%>' />

                                                <asp:Label ID="lblTienda" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Almac�n" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                <asp:Label ID="lblAlmacen" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Almacen")%>'></asp:Label>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Precio Lista" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SimbMoneda")%>'></asp:Label>

                                                <asp:Label ID="lblPrecioLista" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioLista","{0:F3}")%>'></asp:Label>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Disponible" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblStock" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StockDisponibleN","{0:F3}")%>'></asp:Label>

                                                <asp:Label ID="lblUM" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cadenaUM")%>'></asp:Label>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock-Tr�nsito">
                                <ItemTemplate>
                                                <asp:Label ID="lblStockEnTransito" runat="server" ForeColor="Red" Font-Bold="true"
                                                    Text='<%# DataBinder.Eval(Container.DataItem,"StockEnTransito","{0:F3}") %>'></asp:Label>

                                                <asp:Label ID="lblUMStockEnTransito" ForeColor="Red" Font-Bold="true" runat="server"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"cadenaUM")%>'></asp:Label>

                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <table>
                        <tr>
                            <td class="Texto">
                                Total Stock:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStockTotal" runat="server" Width="70px" onFocus="return(onFocus_ReadOnly(this));"
                                    CssClass="TextBoxReadOnly"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Total Stock Transito:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStockTransitoTotal" runat="server" Width="70px" onFocus="return(onFocus_ReadOnly(this));"
                                    CssClass="TextBoxReadOnly"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnAddProducto_ConsultarStockPrecio" runat="server" Text="Aceptar"
                        Width="90px" OnClientClick="return(  offCapa2('capaConsultarStockPrecioxProducto')   );" />
                </td>
            </tr>
        </table>
    </div>

    <div id="capaConsultarTipoPrecio" style="border: 3px solid blue; padding: 8px; width: 500px;
        height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton4" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa2('capaConsultarTipoPrecio')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold" align="right">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProducto_ConsultarTipoPrecio" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold" align="right">
                                Tienda:
                            </td>
                            <td>
                                <asp:Label ID="lblTienda_ConsultarTipoPrecio" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ConsultarTipoPrecio" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="NomTipoPV" HeaderText="Tipo Precio" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="cadenaUM" HeaderText="U. M." HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Precio Lista" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SimbMoneda")%>'></asp:Label>

                                                <asp:Label ID="lblPrecioLista" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioLista","{0:F2}")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnAceptar_ConsultarTipoPrecio" runat="server" Text="Aceptar" Width="90px"
                        OnClientClick="return(  offCapa2('capaConsultarTipoPrecio')   );" />
                </td>
            </tr>
        </table>
    </div>

    <div id="capaAddPermiso_Detalle" style="border: 3px solid blue; padding: 8px; width: 500px;
        height: auto; position: absolute; top: 356px; left: 169px; background-color: white;
        z-index: 3; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton3" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaAddPermiso_Detalle')   );" />
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    Descuento Adicional
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProducto_AddDctoAdicional" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIndex_GV_Detalle" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Usuario:
                            </td>
                            <td>
                                <asp:TextBox ID="txtLogin_AddDctoAdicional" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="return ( onFocusTextTransform(this,configurarDatos) );" Width="150px"
                                    runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Contrase�a:
                            </td>
                            <td>
                                <asp:TextBox ID="txtClave_AddDctoAdicional" Width="150px" runat="server" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Contrase�a:
                            </td>
                            <td>
                                <asp:Panel ID="Panel11" runat="server" DefaultButton="btnAceptar_AddDctoAdicional">                                
                                <asp:TextBox ID="txtClaveConfirmar_AddDctoAdicional" Width="150px" runat="server"
                                    TextMode="Password"> </asp:TextBox>
                                    </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnAceptar_AddDctoAdicional" OnClientClick="return(  valOnClick_btnAceptar_AddDctoAdicional()   );"
                                    runat="server" Text="Aceptar" Width="80px" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancelar_AddDctoAdicional" runat="server" Text="Cerrar" Width="80px"
                                    OnClientClick="return(    offCapa('capaAddPermiso_Detalle')       );" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div id="capaCambiarTipoPrecioPV" style="border: 3px solid blue; padding: 8px; width: 500px;
        height: auto; position: absolute; top: 356px; left: 169px; background-color: white;
        z-index: 3; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton5" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaCambiarTipoPrecioPV')   );" />
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    Descuento Adicional
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProducto_CambiarTipoPV_GV_Detalle" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIndexCambiarTipoPV_GV_Detalle" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold">
                                            Tipo Precio Venta:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoPrecioPV_CambiarDetalle" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCambiarTipoPrecioVenta_Detalle" runat="server" Text="Cambiar"
                                                Width="85px" OnClientClick=" return(  valOnClick_btnCambiarTipoPrecioVenta_Detalle()  ); " />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnConsultarTipoPrecio_CapaCambiarTipoPV" runat="server" OnClick="mostrarCapaConsultarTipoPrecio_CapaCambiarTipoPV"
                                                ImageUrl="~/Imagenes/Ayuda_A.JPG" ToolTip="Consultar Precios de Lista" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_VolumenVentaMin" runat="server" AutoGenerateColumns="false"
                                    Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="TipoPV" HeaderText="Tipo Precio Venta" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Volumen M�nimo." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblVolumenVentaMin" Font-Bold="true" ForeColor="Red" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"vven_CantidadMin_UMPr","{0:F2}")%>'></asp:Label>
                                                            <asp:Label ID="lblUM_VolumenVentaMin" Font-Bold="true" ForeColor="Red" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"um_NombreCorto")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Requisito Cliente" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblTipoPrecioVenta_Requisito" Font-Bold="true" ForeColor="Red" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"TipoPrecioV_Ref")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate><!--BUSCAR PERSONAS-->
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarPersonaGrilla">                                                                        
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this,event));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscarPersonaGrilla">                                                                        
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel4" runat="server" DefaultButton="btnBuscarPersonaGrilla">                                                                        
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';"
                                                                            OnClientClick="this.disabled=true" />--%>
                                                                        <asp:Button ID="btnBuscarPersonaGrilla" runat="server" Text="Buscar" CssClass="btnBuscar"
                                                                        OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Rol:
                                                                    </td>
                                                                    <td colspan="2">
                                                                        <asp:DropDownList ID="cmbRol" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero(event));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </ContentTemplate></asp:UpdatePanel>

    <div id="capaRegistrarCliente" style="border: 3px solid blue; padding: 4px; width: 750px;
        height: auto; position: absolute; top: 302px; left: 35px; background-color: white;
        z-index: 3; display: none;">
        <table width="100%" cellpadding="0">
            <tr>
                <td class="Texto">
                    Tipo Persona:
                </td>
                <td>
                    <asp:DropDownList onchange="return(activarPanel());" ID="cboTipoPersona_RegistrarCliente"
                        runat="server">
                        <asp:ListItem Value="N" Selected="True">Natural</asp:ListItem>
                        <asp:ListItem Value="J">Jur�dica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Tipo Agente:
                </td>
                <td>
                    <asp:DropDownList ID="cmbTipoAgente_RegistrarCliente" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Raz&oacute;n Social:
                </td>
                <td>
                    <asp:TextBox ID="txtRazonSocial_Cliente" Width="653px" runat="server" MaxLength="100"
                        Enabled="false" onKeypress="return( valKeyPressSaveCliente(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Ap. Paterno:
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtApPaterno_Cliente" runat="server" Width="282px" MaxLength="25"
                                    onKeypress="return( valKeyPressSaveCliente(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp;Ap. Materno:
                            </td>
                            <td>
                                <asp:TextBox ID="txtApMaterno_Cliente" runat="server" Width="282px" MaxLength="25"
                                    onKeypress="return( valKeyPressSaveCliente(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Nombres:
                </td>
                <td>
                    <asp:TextBox ID="txtNombres_Cliente" runat="server" Width="282px" MaxLength="25"
                        onKeypress="return( valKeyPressSaveCliente(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    D.N.I.:
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDNI_Cliente" runat="server" MaxLength="8" onKeypress="return( onKeyPressEsNumero(event)  );"
                                    onFocus="return( aceptarFoco(this)  );"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp;R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC_Cliente" onKeypress="return( onKeyPressEsNumero(event)  );" onFocus="return( aceptarFoco(this)  );"
                                    runat="server" MaxLength="11"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Direcci&oacute;n:
                </td>
                <td>
                    <asp:TextBox ID="txtDireccion_Cliente" runat="server" MaxLength="120" Width="653px"
                        onKeypress="return( valKeyPressSaveCliente(this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="return ( onFocusTextTransform(this,configurarDatos) );aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Tel&oacute;fono:
                </td>
                <td>
                    <asp:TextBox ID="txtTelefono_Cliente" runat="server" MaxLength="20" Width="147px"
                        onFocus="return( aceptarFoco(this)  );"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    E-Mail:
                </td>
                <td>
                    <asp:TextBox ID="txtMail_Cliente" runat="server" Width="282px" MaxLength="50" onKeypress="return( valKeyPressSaveCliente(this)  );"
                        onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:ImageButton ID="btnGuardarCliente" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                        OnClientClick="return(validarSaveCliente());" onmouseover="this.src='../Imagenes/Guardar_A.JPG';"
                        onmouseout="this.src='../Imagenes/Guardar_B.JPG';" CausesValidation="true" />
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaRegistrarCliente'));" />
                </td>
            </tr>
        </table>
    </div>

    <div id="capaEditarPersona" style="border: 3px solid blue; padding: 10px; width: 750px;
        height: auto; position: absolute; top: 302px; left: 35px; background-color: white;
        z-index: 3; display: none;">
        <table width="100%" cellpadding="0">
            <tr>
                <td class="Texto">
                    Tipo Agente:
                </td>
                <td>
                    <asp:DropDownList ID="cboTipoAgente_EditarCliente" runat="server" onClick="return(  valOnClickTipoAgente_EditarCliente()  );">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Raz�n Social:
                </td>
                <td>
                    <asp:TextBox ID="txtRazonSocial_EditarCliente" Width="653px" runat="server" MaxLength="100"
                        onKeypress="return(  valOnKeyPress_EditarCliente('2',this)  );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Ap. Paterno:
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtApPaterno_EditarCliente" runat="server" onKeypress="return(  valOnKeyPress_EditarCliente('1',this)  );"
                                    Width="282px" MaxLength="25" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp;Ap. Materno:
                            </td>
                            <td>
                                <asp:TextBox ID="txtApMaterno_EditarCliente" runat="server" onKeypress="return(  valOnKeyPress_EditarCliente('1',this)  );"
                                    Width="282px" MaxLength="25" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Nombres:
                </td>
                <td>
                    <asp:TextBox ID="txtNombres_EditarCliente" runat="server" Width="282px" onKeypress="return(  valOnKeyPress_EditarCliente('1',this)  );"
                        MaxLength="25" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    D.N.I.:
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDNI_EditarCliente" runat="server" MaxLength="8" onFocus="return( aceptarFoco(this)  );"
                                    onKeypress="return(  valOnKeyPressDNI_EditarCliente()  );"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                &nbsp;R.U.C.:
                            </td>
                            <td>
                                <asp:TextBox ID="txtRUC_EditarCliente" onFocus="return( aceptarFoco(this)  );" runat="server"
                                    MaxLength="11" onKeypress="return( valOnKeyPressRUC_EditarCliente() );"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Texto">
                    Direcci&oacute;n:
                </td>
                <td>
                    <asp:TextBox ID="txtDireccion_EditarCliente" runat="server" MaxLength="120" Width="653px"
                        onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:ImageButton ID="btnGuardar_EditarCliente" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                        OnClientClick="return(  valOnClickGuardar_EditarCliente()    );" onmouseover="this.src='../Imagenes/Guardar_A.JPG';"
                        onmouseout="this.src='../Imagenes/Guardar_B.JPG';" CausesValidation="true" />
                    <asp:ImageButton ID="btnCerrar_CapaEditarCliente" runat="server" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaEditarPersona'));" />
                </td>
            </tr>
        </table>
    </div>

    <div id="capaGlosa" style="border: 3px solid blue; padding: 8px; width: auto; height: auto;
        position: absolute; top: 300px; left: 250px; background-color: white; z-index: 2;
        display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton7" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaGlosa'));" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table>
                        <tr>
                            <td class="Texto">
                                Glosa:
                            </td>
                            <td>
                                <asp:TextBox ID="txtGlosa_CapaGlosa" runat="server" Width="350px" TextMode="MultiLine"
                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                    Height="50px"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:ImageButton ID="btnAddGlosa_CapaGlosa" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                        onmouseout="this.src='../Imagenes/Aceptar_B.JPG';" OnClientClick="return(     valOnClickAddGlosa()       );"
                        onmouseover="this.src='../Imagenes/Aceptar_B.JPG';" />
                </td>
            </tr>
        </table>
    </div>

    <div id="capaDocumento_NotaCredito" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; background-color: white; z-index: 2;
        display: none; top: 900px; left: 25px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton8" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumento_NotaCredito')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_NotaCredito" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_NotaCredito" runat="server" AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" HeaderStyle-Height="25px" />
                                <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaMontoRecibir_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:TextBox ID="txtMontoRecibir_Find" Width="70px" Font-Bold="true" onKeypress="return(  validarNumeroPunto(event)  );"
                                                        onblur="return(  valBlur(event)   );" onFocus="return(   aceptarFoco(this)   );" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Numero")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />

                                                    <asp:HiddenField ID="hddIdMonedaDocRef_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                    <asp:HiddenField ID="hddIdEstadoCancelacion_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEstadoCancelacion")%>' />

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Vcto."
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Contador" DataFormatString="{0:F0}" HeaderText="Vigencia (D�as)"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaMonto_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblMonto_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Saldo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblMonedaSaldo_Find" ForeColor="Red" runat="server" Font-Bold="true"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblSaldo_Find" runat="server" ForeColor="Red" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"ImporteTotal","{0:F2}")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NomPropietario" HeaderText="Empresa" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomTienda" HeaderText="Tienda" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>

    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>                    
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%" AllowPaging="True" PagerStyle-ForeColor="White" PagerSettings-Position="TopAndBottom">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de b�squeda realiza un filtrado de acuerdo al cliente seleccionado.
                </td>
            </tr>
        </table>
    </div>

    <asp:UpdatePanel ID="UpdatePanel4" runat="server"><ContentTemplate><!--BUSCAR COTIZACIONES-->
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 2px; width: 950px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 15px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoDocumentoRef" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisi�n</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:Panel ID="Panel13" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero(event)  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                            <asp:Panel ID="Panel14" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero(event)  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%" AllowPaging="True" PagerStyle-ForeColor="White" PagerSettings-Position="TopAndBottom">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Vcto."
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Importe" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblImporteMoneda" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                    <asp:Label ID="lblImporte" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>
    
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                            DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="chb_ValidarCanjeUnico_Cotizacion" CssClass="Texto" runat="server"
                        Checked="true" Text="Una Cotizaci�n s�lo debe estar asociado a un Documento de Venta." />
                </td>
            </tr>
        </table>
    </div>
    </ContentTemplate></asp:UpdatePanel>

    <div id="capaImpresionDocumento" style="border: 3px solid blue; padding: 8px; width: 840px;
        height: auto; position: absolute; top: 225px; left: 38px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_Print" runat="server">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImageButton10" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(offCapa('capaImpresionDocumento'));" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_ImpresionDoc_Cab" runat="server" AutoGenerateColumns="false"
                                Width="700px">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="true" HeaderStyle-Width="80px" SelectText="Ver Detalle"
                                        ButtonType="Link" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Nro. Documento" DataField="NroDocumento" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                                    <asp:BoundField HeaderText="Moneda" ItemStyle-ForeColor="Red" DataField="NomMoneda"
                                        HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Bold="true" />
                                    <asp:BoundField HeaderText="Total" DataFormatString="{0:F2}" DataField="Total" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                                    <asp:BoundField HeaderText="Percepci�n" DataFormatString="{0:F2}" DataField="Percepcion"
                                        HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Bold="true" />
                                    <asp:BoundField HeaderText="Total A Pagar" ItemStyle-ForeColor="Red" DataFormatString="{0:F2}"
                                        DataField="TotalAPagar" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                                    <asp:TemplateField HeaderText="Personalizado" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                                        <asp:ImageButton ID="btnImprimir" runat="server" ImageUrl="~/Imagenes/Print.ico"
                                                            ToolTip="Imprimir Documento" OnClientClick="  return(  valOnClick_btnImprimir_Capa(this,1)  );  " />
                                                        <asp:HiddenField ID="hddIdDocumentoPrint" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                    <%--                                                    <td>
                                                        <asp:ImageButton ID="btnDespachar" OnClientClick="return(  valOnClick_btnDespachar(this)   );"
                                                            runat="server" ImageUrl="" ToolTip="Despachar Documento" />
                                                    </td>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Estandar" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                                        <asp:ImageButton ID="btnImprimirDet" runat="server" ImageUrl="~/Imagenes/Print.ico"
                                                            ToolTip="Imprimir Detallado" OnClientClick="  return(  valOnClick_btnImprimir_Capa(this,2)  );  " />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Resumido" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                                        <asp:ImageButton ID="btnImprimirAgrup" runat="server" ImageUrl="~/Imagenes/Print.ico"
                                                            ToolTip="Imprimir Resumido" OnClientClick="  return(  valOnClick_btnImprimir_Capa(this,3)  );  " />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left">
                            <table>
                                <tr>
                                    <td class="Label">
                                        Percepci�n Total:
                                    </td>
                                    <td>
                                        <asp:Label CssClass="LabelRojo" ID="lblMonedaPercepcion_DocSAVED" runat="server"
                                            Text="-" Font-Bold="true"></asp:Label>
                                        <asp:TextBox ID="txtPercepcion_DocSAVED" CssClass="TextBox_ReadOnly" Width="80px"
                                            runat="server" Enabled="false" Text="0"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="Label">
                                        Total a Pagar:
                                    </td>
                                    <td>
                                        <asp:Label CssClass="LabelRojo" ID="lblMonedaTotalAPagar_DocSAVED" runat="server"
                                            Text="-" Font-Bold="true"></asp:Label>
                                        <asp:TextBox ID="txtTotalAPagar_DocSAVED" CssClass="TextBox_ReadOnly" Width="100px"
                                            runat="server" Enabled="false" Text="0"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_ImpresionDoc_Det" runat="server" Width="800px" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField DataField="IdProducto" HeaderText="C�digo" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="NomProducto" HeaderText="Producto" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" />
                                    <asp:BoundField DataField="UMedida" HeaderText="U.M." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-Height="20px" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" DataFormatString="{0:F2}"
                                        HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="PrecioCD" HeaderText="P.V. Final" DataFormatString="{0:F2}"
                                        HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Importe" HeaderText="Importe" DataFormatString="{0:F2}"
                                        HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px"
                                        ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAceptar_Print" Width="80px" runat="server" Text="Aceptar" OnClientClick=" return( offCapa('capaImpresionDocumento')  ); " />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="capaVistaPreviaDocumento" style="border: 3px solid blue; padding: 8px; width: auto;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 200px; left: 104px;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaVistaPrevia" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaVistaPreviaDocumento'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_VistaPreviaDoc" runat="server" AutoGenerateColumns="false" Width="100%">
                        <Columns>
                            <asp:BoundField NullDisplayText="---" ItemStyle-Height="25px" HeaderText="Moneda"
                                HeaderStyle-Width="70px" DataField="NomMoneda" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                            <asp:BoundField HeaderText="Total" DataFormatString="{0:F2}" HeaderStyle-Width="110px"
                                DataField="Total" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Font-Bold="true" />
                            <asp:BoundField HeaderText="Percepci�n" DataFormatString="{0:F2}" HeaderStyle-Width="110px"
                                DataField="Percepcion" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Font-Bold="true" />
                            <asp:BoundField HeaderText="Total A Pagar" DataFormatString="{0:F2}" HeaderStyle-Width="110px"
                                DataField="TotalAPagar" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Font-Bold="true" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Label">
                                Total:
                            </td>
                            <td>
                                <asp:Label CssClass="LabelRojo" ID="lblMoneda_Total_VistaPrevia" runat="server" Text="-"></asp:Label>
                                <asp:TextBox ID="txtTotal_VistaPrevia" CssClass="TextBox_ReadOnly" Width="80px" runat="server"
                                    Text="0" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td class="Label">
                                Percepci�n Total:
                            </td>
                            <td>
                                <asp:Label CssClass="LabelRojo" ID="lblMoneda_Percepcion_VistaPrevia" runat="server"
                                    Text="-"></asp:Label>
                                <asp:TextBox ID="txtPercepcion_VistaPrevia" CssClass="TextBox_ReadOnly" Width="80px"
                                    runat="server" Text="0" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                            <td class="Label">
                                Total a Pagar:
                            </td>
                            <td>
                                <asp:Label CssClass="LabelRojo" ID="lblMoneda_TotalAPagar_VistaPrevia" runat="server"
                                    Text="-"></asp:Label>
                                <asp:TextBox ID="txt_TotalAPagar_VistaPrevia" CssClass="TextBox_ReadOnly" Width="80px"
                                    runat="server" Text="0" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnGuardar_VistaPrevia" runat="server" Text="Guardar" Width="80px"
                                    OnClientClick=" return( valSaveDocumento()  );  " />
                            </td>
                            <td>
                                <asp:Button ID="btnAceptar_VistaPrevia" runat="server" Text="Cerrar" Width="80px"
                                    OnClientClick=" return(   offCapa('capaVistaPreviaDocumento')  ); " />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div id="capaAdelanto" style="border: 3px solid blue; padding: 8px; width: 800px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 850px; left: 47px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton11" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaAdelanto')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Motivo del Concepto:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoAdelanto" runat="server" AutoPostBack="true">
                                    <asp:ListItem Value="0" Selected="True">[ Seleccione un Motivo de Adelanto ]</asp:ListItem>
                                    <asp:ListItem Value="1">Ingresar Nuevo Concepto Adelanto</asp:ListItem>
                                    <asp:ListItem Value="2">Aplicar Concepto de Adelanto Existente</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_Adelanto_Nuevo" runat="server">
                        <table>
                            <tr>
                                <td class="Texto">
                                    Monto:
                                </td>
                                <td>
                                    <asp:Label ID="lblMoneda_MontoAdelanto_Nuevo" CssClass="LabelRojo" Font-Bold="true"
                                        runat="server" Text="-"></asp:Label>
                                </td>
                                <td>
                                    <asp:Panel ID="Panel12" runat="server" DefaultButton="btnAceptar_MontoAdelanto_Nuevo">                                    
                                    <asp:TextBox ID="txtMonto_Adelanto_Nuevo" CssClass="TextoRight" runat="server" Width="70px"
                                        Font-Bold="true" onKeyPress=" return(  valOnKeyPress_txtMonto_Adelanto_Nuevo(event)  ); "
                                        onKeyup=" return(  valBlur(event)  ); " onFocus="return(   aceptarFoco(this)  );"
                                        Text="0"></asp:TextBox>
                                        </asp:Panel>
                                </td>
                                <td>
                                    <asp:Button ID="btnAceptar_MontoAdelanto_Nuevo" runat="server" Text="Aceptar" Width="80px"
                                        OnClientClick="return(  valOnClick_btnAceptar_MontoAdelanto_Nuevo() );" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_Adelanto_Aplicacion" runat="server" Width="100%">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_BuscarDocRef_AdelantoApp" runat="server" AutoGenerateColumns="false"
                                        Width="100%">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-Width="50px" HeaderStyle-Height="25px" />
                                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Tipo" />
                                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                                <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                                <asp:HiddenField ID="hddIdDocumentoRef_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                                <asp:Label ID="lblMoneda_MontoDocRef_AdelantoApp" ForeColor="Red" Font-Bold="true"
                                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                                <asp:Label ID="lblMontoDocRef_AdelantoApp" runat="server" ForeColor="Red" Font-Bold="true"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F2}")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" />
                                        </Columns>
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hddIndexGV_Concepto" runat="server" />
                </td>
            </tr>
        </table>
    </div>

    <div id="capaPermiso_AddMedioPago" style="border: 3px solid blue; padding: 8px; width: 350px;
        height: auto; position: absolute; top: 850px; left: 250px; background-color: white;
        z-index: 3; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton12" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaPermiso_AddMedioPago')   );" />
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    Autorizaci�n - Medio de Pago
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Usuario:
                            </td>
                            <td>
                                <asp:TextBox ID="txtUsuario_AddMedioPago" Width="150px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Contrase�a:
                            </td>
                            <td>
                                <asp:TextBox ID="txtClave_AddMedioPago" Width="150px" runat="server" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Contrase�a:
                            </td>
                            <td>
                                <asp:TextBox ID="txtClave2_AddMedioPago" Width="150px" runat="server" TextMode="Password"> </asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnAceptar_AddMedioPago" OnClientClick=" return(  valOnClick_btnAceptar_AddMedioPago() ); "
                                    runat="server" Text="Aceptar" Width="80px" />
                            </td>
                            <td>
                                <asp:Button ID="btnCerrar_AddMedioPago" runat="server" Text="Cerrar" Width="80px"
                                    OnClientClick="return(    offCapa('capaPermiso_AddMedioPago')       );" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div id="capaEquivalenciaProducto" style="border: 3px solid blue; padding: 8px; width: 450px;
        height: auto; position: absolute; top: 237px; left: 250px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton13" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaEquivalenciaProducto')   );" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Ingrese Cantidad:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCantidad_Ingreso" onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); "
                                                onFocus=" return(  aceptarFoco(this)  ); " Width="100px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboUnidadMedida_Ingreso" runat="server" DataTextField="NombreCortoUM"
                                                DataValueField="IdUnidadMedida">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCalcular" OnClientClick=" return(  valOnClick_btnCalcular()   ); "
                                                runat="server" Text="Calcular" ToolTip="Calcular equivalencias" Width="80px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            Resultado:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCantidad_Salida" Font-Bold="true" CssClass="TextBox_ReadOnlyLeft"
                                                Text="" ReadOnly="true" Width="100px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboUnidadMedida_Salida" runat="server" DataTextField="NombreCortoUM"
                                                DataValueField="IdUnidadMedida">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GV_CalcularEquivalencia" runat="server" AutoGenerateColumns="False"
                                    Width="400px">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chb_UnidadMedida" runat="server" Checked="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unidad Medida" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblUnidadMedida" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida")%>'></asp:Label>

                                                            <asp:HiddenField ID="hddIdUnidadMedida" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblUnidadMedidaAbv" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NombreCortoUM")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GV_ResuldoEQ" runat="server" AutoGenerateColumns="False" Width="400px">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="UnidadMedida" HeaderText=""
                                            ItemStyle-Height="25px" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="UnidadMedidaAbv" HeaderText="U.M."
                                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="Cantidad" HeaderText="Cantidad"
                                            DataFormatString="{0:F4}" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:RadioButtonList ID="rbl_UtilizarRedondeo" runat="server" RepeatDirection="Horizontal"
                                    CssClass="Label">
                                    <asp:ListItem Selected="True" Value="0">No Utilizar Redondeo</asp:ListItem>
                                    <asp:ListItem Value="1">Redondeo [ arriba ]</asp:ListItem>
                                    <asp:ListItem Value="2">Redondeo [ abajo ]</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnAceptar_EquivalenciaPR" runat="server" Text="Aceptar" Width="82px"
                                    ToolTip="Aceptar Equivalencia" OnClientClick=" return(  valOnClick_btnAceptar_EquivalenciaPR()  );  " />
                            </td>
                            <td>
                                <asp:Button ID="btnCerrar_EquivalenciaPR" runat="server" Text="Cerrar" Width="82px"
                                    ToolTip="Cerrar" OnClientClick="  return(  offCapa('capaEquivalenciaProducto') ); " />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
    </div>

    <div id="capaRetazo_Area" style="border: 3px solid blue; padding: 8px; width: auto;
        height: auto; position: absolute; top: 600px; left: 291px; background-color: white;
        z-index: 2; display: none;">
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Ancho:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox onKeypress="return(  validarNumeroPuntoPositivo('event')  );" ID="txtRetazoArea_Ancho"
                                                Width="80px" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Largo:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtRetazoArea_Largo" Width="80px" runat="server" onKeypress="return( validarNumeroPuntoPositivo('event') );"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Cantidad:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox onKeypress="return( validarNumeroPuntoPositivo('event') );" ID="txtRetazoArea_Cantidad"
                                                Width="80px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnRetazoArea_Aceptar" runat="server" Text="Aceptar" Width="80px"
                                    OnClientClick="return(    valOnClick_btnRetazoArea_Aceptar()    );" />
                            </td>
                            <td>
                                <asp:Button ID="btnRetazoArea_Cerrar" runat="server" Text="Cerrar" Width="80px" OnClientClick="return(  offCapa('capaRetazo_Area')  );" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div id="capaRetazo_Longitud" style="border: 3px solid blue; padding: 8px; width: auto;
        height: auto; position: absolute; top: 600px; left: 291px; background-color: white;
        z-index: 2; display: none;">
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Largo:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtRetazoLongitud_Largo" Width="80px" runat="server" onKeypress="return( validarNumeroPuntoPositivo('event') );"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            <asp:Label ID="lblUnidadMedida_RetazoLongitud" runat="server" Text="-"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Cantidad:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox onKeypress="return( validarNumeroPuntoPositivo('event') );" ID="txtRetazoLongitud_Cantidad"
                                                Width="80px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnRetazoLongitud_Aceptar" runat="server" Text="Aceptar" Width="80px"
                                    OnClientClick="return(   valOnClick_btnRetazoLongitud_Aceptar()     );" />
                            </td>
                            <td>
                                <asp:Button ID="btnRetazoLongitud_Cerrar" runat="server" Text="Cerrar" Width="80px"
                                    OnClientClick="return(  offCapa('capaRetazo_Longitud')  );" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div id="capaCampania_Producto" style="border: 3px solid blue; padding: 8px; width: 800px;
        height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton14" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa2('capaCampania_Producto')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblCodigoProducto_capaCampania_Producto" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                            <td style="width: 15px">
                            </td>
                            <td>
                                <asp:Label ID="lblProducto_capaCampania_Producto" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_Campania_Producto" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="Campa�a" DataField="Campania" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Inicio" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblFechaInicio" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FechaInicio","{0:dd/MM/yyyy}")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdCampania" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCampania")%>' />

                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />

                                                <asp:HiddenField ID="hddIdUnidadMedida" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fin" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblFechaFin" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FechaFin","{0:dd/MM/yyyy}")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="U.M." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblUnidadMedida" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Precio" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>

                                                <asp:Label ID="lblPrecioCampania" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCampania","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cantidad M�n." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblCantidadMin" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CantidadMin","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnCerrar_capaCampania_Producto" runat="server" Text="Cerrar" Width="70px"
                        OnClientClick="return(  offCapa2('capaCampania_Producto')   );" />
                </td>
            </tr>
        </table>
    </div>

    <div id="capaConsultar_Campania" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_capaConsultar_Campania" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImageButton15" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(  offCapa2('capaConsultar_Campania')   );" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_Campania_Cab" runat="server" AutoGenerateColumns="false" Width="100%">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Campa�a" DataField="cam_Descripcion" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="Inicio" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                                        <asp:Label ID="lblFechaInicio" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cam_FechaInicio","{0:dd/MM/yyyy}")%>'></asp:Label>

                                                        <asp:HiddenField ID="hddIdCampania" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCampania")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fin" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                                        <asp:Label ID="lblFechaFin" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cam_FechaFin","{0:dd/MM/yyyy}")%>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Empresa" DataField="per_NComercial" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Tienda" DataField="tie_Nombre" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GV_Campania_Det" runat="server" AutoGenerateColumns="false" Width="100%"
                                AllowPaging="true" PageSize="20">
                                <Columns>
                                    <asp:BoundField HeaderText="C�digo" DataField="CodigoProducto" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Producto" DataField="Producto" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="U.M." DataField="UnidadMedida" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Cantidad M�n." DataFormatString="{0:F3}" DataField="CantidadMin"
                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="Precio" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                                        <asp:Label ID="lblMoneda" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda_Campania")%>'></asp:Label>

                                                        <asp:Label ID="lblPrecio" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Precio","{0:F3}")%>'></asp:Label>  
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnCerrar_capaConsultar_Campania" runat="server" Text="Cerrar" Width="70px"
                                OnClientClick="return(  offCapa2('capaConsultar_Campania')   );" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="capaCheque_AddDatoCancelacion" style="border: 3px solid blue; padding: 8px;
        width: 850px; height: auto; position: absolute; top: 1150px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaCheque_AddDatoCancelacion" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaCheque_AddDatoCancelacion')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_Cheque_AddDC" runat="server" AutoGenerateColumns="false" Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Banco" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblBanco" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Banco")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdCheque" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdCheque")%>' />

                                                <asp:HiddenField ID="hddIdMedioPago" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMedioPago")%>' />

                                                <asp:HiddenField ID="hddIdMoneda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                <asp:HiddenField ID="hddIdBanco" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdBanco")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Nro." DataField="NumeroCheque" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="Fecha Cobrar" DataField="FechaCobrar" DataFormatString="{0:dd/MM/yyyy}"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Monto" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblMoneda" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Moneda")%>'></asp:Label>

                                                <asp:Label ID="lblMonto" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Monto","{0:F3}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Estado" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"
                                DataField="EstadoMov_Desc" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField HeaderText="Obs." DataField="Observacion" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr align="center">
                <td>
                    <asp:Button ID="btnCerrar_Cheque_AddDC" runat="server" Text="Cerrar" Width="80px"
                        ToolTip="Cerrar" OnClientClick=" return( offCapa('capaCheque_AddDatoCancelacion') ); " />
                </td>
            </tr>
        </table>
    </div>

    <div id="capaVehiculo" style="border: 3px solid blue; padding: 8px; width: 560px;
        height: auto; position: absolute; top: 1057px; left: 291px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton16" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaVehiculo'));" />
                </td>
            </tr>
            <tr class="Label">
                <td align="left">
                    Nro. Placa:
                    <asp:Panel ID="Panel8" runat="server" DefaultButton="btnBuscarVehiculo_Grilla">
                    
                    <asp:TextBox ID="txtNroPlaca_BuscarVehiculo" runat="server" Width="120px"></asp:TextBox>

                    <asp:Button ID="btnBuscarVehiculo_Grilla" runat="server" Text="Buscar" Width="80px" />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_Vehiculo" runat="server" AutoGenerateColumns="false" Width="100%">
                        <Columns>
                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="true" ButtonType="Link"
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Nro. Placa" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroPlaca" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Placa")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdVehiculo" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
  
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Modelo" HeaderText="Modelo" NullDisplayText="---" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="ConstanciaIns" HeaderText="Const. Inscripci�n" NullDisplayText="---"
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };

        function validacion3() {

            if (confirm('�Desea continuar con el cambio de vendedor?') == true) {
                return true
            } else {
                return false
            }
        }

        function validacion2() {

            if (confirm('�Desea continuar con la modificaci�n del Punto de Llegada?') == true) {
                return true
            } else {
                return false
            }
        }

        function validacion() {
            if (confirm('�Desea continuar con la modificaci�n del Check?') == true) {
                return true
            } else {
                return false
            }
        }

        function valKeyPressCodigoSL() {
            if (event.keyCode == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProducto.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(obj, e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key.keyCode == 13) { // Enter      
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscarGrilla_AddProducto.ClientID%>').focus();
            }
            return true;
        }

        function valKeyPressDescripcionProdCodBarras(obj) {
            ejecucion = '0';
            var codBarras = document.getElementById('<%=txtCodBarrasPrincipal.ClientID%>');

            if (event.keyCode == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                if (codBarras.value.length <= 0) {
                    alert('no se se ingreso Codigo de Barras');
                    return false;
                } else {
                    document.getElementById('<%=btnAceptarCodBarras.ClientID %>').click();
                    return false;
                }
            }
        }


        function CursorCodBarras() {
            document.getElementById('<%=txtCodBarrasPrincipal.ClientID%>').focus();
            document.getElementById('<%=txtCodBarrasPrincipal.ClientID%>').select();
            return false;
        }

        function valKeyPressDescripcionProdCodBarrasCapa(obj,e) {
            var codBarras = document.getElementById('<%=txtCodBarrasCapa.ClientID%>');
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                if (codBarras.value.length <= 0) {
                    alert('no se se ingreso Codigo de Barras');
                    return false;
                } else {
                    document.getElementById('<%=btnBuscarGrilla_AddProducto.ClientID%>').focus();
                    return true;
                }
            }
        }


        function valOnClick_btnAgregarCodBarras() {
            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            var txtIdCliente = document.getElementById('<%=txtCodigoCliente.ClientID%>');
            if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            var codBarrasPrincipal = document.getElementById('<%=txtCodBarrasPrincipal.ClientID%>');

            if (codBarrasPrincipal.value.length <= 0) {
                alert('no ingreso en codigo de barras');
                return false;
            } else {
                return true;
            }
        }

        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valAddProductos() {

            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            var txtIdCliente = document.getElementById('<%=txtCodigoCliente.ClientID%>');
            if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }

            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('No se seleccionaron productos.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 por que 0 es la cabecera
                var rowElem = grilla.rows[i];
                var txtCantidad = rowElem.cells[0].children[0];
                if (parseFloat(txtCantidad.value) > 0) {
                    // if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
                    //                        alert('La cantidad ingresada excede a la cantidad disponible.');
                    //                        txtCantidad.select();
                    //                        txtCantidad.focus();
                    //     return false;
                    //  } else if (parseFloat(txtCantidad.value) > 0) {
                    cont = cont + 1;
                    //  }
                }
            }

            if (cont == 0) {
                alert('No se seleccionaron productos.');
                return false;
            }

            /*
            //************* Validamos que no agregue de otro almacen
            
            if (grillaDetalle != null) {

            if (grillaDetalle.rows.length > 1) {

            //************ valido el almacen
                    
            var hddIdAlmacen = 
                    

            if (parseFloat(cboAlmacen.value) != parseFloat(hddIdAlmacen.value)) {
                        
            return false;
            }
            }
            }
            */

            var NoAfectoIgv = 0;
            NoAfectoIgv = ConceptoNoAfectoIgv();
            if (parseInt(NoAfectoIgv) == 1) {
                alert('EL DOCUMENTO POSEE CONCEPTOS NO AFECTOS A I.G.V. \n NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            return true;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO





            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }
        function valStockDisponible_AddProd() {
            /*var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
            if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].id == event.srcElement.id) {
            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
            alert('La cantidad ingresada excede a la cantidad disponible.');
            rowElem.cells[0].children[0].select();
            rowElem.cells[0].children[0].focus();
            return false;
            }
            return false;
            }

            }
            }*/
            return false;
        }
        function valKeyPressCantidadAddProd(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key.keyCode == 13) {
                document.getElementById('<%=btnAddProductos_AddProducto.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo("event");
            }
            return true;
        }
        function valOnClick_btnAgregar() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }

        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btnBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj,e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btnBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }
        //************ MANTENIMIENTO PERSONA (nuevo)
        function onCapaCliente() {
            LimpiarControlesCliente();
            onCapa('capaRegistrarCliente');
            var opcion = document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID %>').value;
            if (opcion == 'N') {
                document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').disabled = false;
                document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').disabled = true;
                document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').focus();
            } else if (opcion == 'J') {
                document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').disabled = true;
                document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').disabled = false;
                document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').focus();
            }
            return false;
        }
        void function LimpiarControlesCliente() {
            document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtApMaterno_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtNombres_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtDNI_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtRUC_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtDireccion_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtTelefono_Cliente.ClientID%>').value = "";
            document.getElementById('<%= txtMail_Cliente.ClientID%>').value = "";
            document.getElementById('<%= cmbTipoAgente_RegistrarCliente.ClientID%>').value = '0';
        }
        function validarSaveCliente() {
            var cboTipoPersona = document.getElementById('<%=cboTipoPersona_RegistrarCliente.ClientID %>').value;
            var txtDNI_Cliente = document.getElementById('<%=txtDNI_Cliente.ClientID %>');
            var txtRUC_Cliente = document.getElementById('<%=txtRUC_Cliente.ClientID %>');

            switch (cboTipoPersona) {

                case 'N':
                    if (txtDNI_Cliente.value.length > 0) {
                        if (txtDNI_Cliente.value.length < 8) {
                            alert("REVISE EL NUMERO DE D.N.I. DE LA PERSONA.");
                            return false;
                        }
                    }
                    if (txtRUC_Cliente.value.length > 0) {
                        if (txtRUC_Cliente.value.length < 11) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA.");
                            return false;
                        }
                        var valido = false;
                        if (txtRUC_Cliente.value.substring(0, 2) == '10' || txtRUC_Cliente.value.substring(0, 2) == '15') { valido = true; }
                        if (valido == false) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA.");
                            return false;
                        }
                    }

                    break;

                case 'J':
                    if (txtRUC_Cliente.value.length > 0) {
                        if (txtRUC_Cliente.value.length < 11) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA.");
                            return false;
                        }
                        if (txtRUC_Cliente.value.substring(0, 2) != '20') {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA.");
                            return false;
                        }
                    }

                    break;
            }

            return (confirm('Desea continuar con la operaci�n?'));
        }
        function valKeyPressSaveCliente(obj) {
            if (event.keyCode == 13) {
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnGuardarCliente.ClientID%>').focus();
            }
            return true;
        }
        function activarPanel_V1() {
            var combo = document.getElementById('<%=cboTipoPersona_RegistrarCliente.ClientID %>');
            var flag = false;
            switch (combo.value) {
                case 'N':
                    flag = false;
                    break;
                case 'J':
                    flag = true;
                    break;
            }
            //controles de Per. Natural

            document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtApMaterno_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtNombres_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtDNI_Cliente.ClientID%>').disabled = flag;

            //controles de Per. Juridica
            document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').disabled = !(flag);
            switch (combo.value) {
                case 'N':
                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').select();
                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').focus();
                    break;
                case 'J':
                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').select();
                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').focus();
                    break;
            }
            return true;
        }
        function activarPanel() {
            LimpiarControlesCliente();
            var combo = document.getElementById('<%=cboTipoPersona_RegistrarCliente.ClientID %>');
            var flag = false;
            switch (combo.value) {
                case 'N':
                    flag = false;
                    break;
                case 'J':
                    flag = true;
                    break;
            }
            //controles de Per. Natural

            document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtApMaterno_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtNombres_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtDNI_Cliente.ClientID%>').disabled = flag;

            //controles de Per. Juridica
            document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').disabled = !(flag);
            switch (combo.value) {
                case 'N':
                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').select();
                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').focus();
                    break;
                case 'J':
                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').select();
                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').focus();
                    break;
            }
            return true;
        }
        function valOnClickTipoAgente_RegistrarCliente() {
            var cboTipoPersona = document.getElementById('<%=cboTipoPersona_RegistrarCliente.ClientID%>');
            var cmbTipoAgente_RegistrarCliente = document.getElementById('<%=cmbTipoAgente_RegistrarCliente.ClientID%>');
            if (cboTipoPersona.value == 'N') {
                alert('Opci�n no disponible para la Persona tipo [Natural].');
                cmbTipoAgente_RegistrarCliente.value = '0';
                return false;
            }
            return true;
        }
        //********** FIN MANT (NUEVO)
        //******************************* MANT PERSONA (EDITAR)
        function valEditarCliente() {
            var txtIdCliente = document.getElementById('<%=txtCodigoCliente.ClientID%>');
            if (isNaN(txtIdCliente.value) || txtIdCliente.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            } else {

                var IdTipoPersona = parseInt(document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID%>').value);
                var IdTipoAgente = parseInt(document.getElementById('<%=cboTipoAgente.ClientID%>').value);
                var txtRazonSocial = document.getElementById('<%=txtApPaterno_RazonSocial.ClientID%>');
                var txtApPaterno = document.getElementById('<%=txtApPaterno_RazonSocial.ClientID%>');
                var txtApMaterno = document.getElementById('<%=txtApMaterno.ClientID%>');
                var txtNombres = document.getElementById('<%=txtNombres.ClientID%>');
                var txtDNI = document.getElementById('<%=txtDni.ClientID%>');
                var txtRUC = document.getElementById('<%=txtRuc.ClientID%>');
                var txtDireccion = document.getElementById('<%=txtDireccionCliente.ClientID%>');

                //********* Limpiamos las cajas
                document.getElementById('<%=txtApPaterno_EditarCliente.ClientID%>').value = '';
                document.getElementById('<%=txtRazonSocial_EditarCliente.ClientID%>').value = '';
                document.getElementById('<%=cboTipoAgente_EditarCliente.ClientID%>').value = '0';
                document.getElementById('<%=txtApMaterno_EditarCliente.ClientID%>').value = '';
                document.getElementById('<%=txtNombres_EditarCliente.ClientID%>').value = '';
                document.getElementById('<%=txtDNI_EditarCliente.ClientID%>').value = '';
                document.getElementById('<%=txtRUC_EditarCliente.ClientID%>').value = '';
                document.getElementById('<%=txtDireccion_EditarCliente.ClientID%>').value = '';

                //********** Mostramos los valores para su Edici�n
                switch (IdTipoPersona) {
                    case 0: //**** No se tiene un Tipo de Persona
                        alert('No se ha seleccionado ning�n tipo de Persona.');
                        return false;
                        break;
                    case 1: //**** NATURAL
                        document.getElementById('<%=txtApPaterno_EditarCliente.ClientID%>').value = txtApPaterno.value;
                        break;
                    case 2: //**** JURIDICA
                        document.getElementById('<%=txtRazonSocial_EditarCliente.ClientID%>').value = txtRazonSocial.value;
                        var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
                        if (grilla != null) {
                            document.getElementById('<%=cboTipoAgente_EditarCliente.ClientID%>').disabled = true;
                        }
                        break;
                }
                document.getElementById('<%=cboTipoAgente_EditarCliente.ClientID%>').value = IdTipoAgente;
                document.getElementById('<%=txtApMaterno_EditarCliente.ClientID%>').value = txtApMaterno.value;
                document.getElementById('<%=txtNombres_EditarCliente.ClientID%>').value = txtNombres.value;
                document.getElementById('<%=txtDNI_EditarCliente.ClientID%>').value = txtDNI.value;
                document.getElementById('<%=txtRUC_EditarCliente.ClientID%>').value = txtRUC.value;
                document.getElementById('<%=txtDireccion_EditarCliente.ClientID%>').value = txtDireccion.value;
                onCapa('capaEditarPersona');
            }
            return false;
        }
        function valOnClickTipoAgente_EditarCliente() {
            var tipoPersona = document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID %>').value;
            if (tipoPersona == '0') {
                alert('Debe seleccionar un Tipo de Persona [Natural - Jur�dica].');
                return false;
            }
            if (tipoPersona == '1') {  //*** No es Jur�dica
                alert('Opci�n no disponible para el Tipo de Persona [Natural].');
                return false;
            }
            return true;
        }
        function valOnKeyPress_EditarCliente(opcion, obj) {

            if (event.keyCode == 13) {
                if (obj != null) {
                    onBlurTextTransform(obj, configurarDatos);
                }
                var boton = document.getElementById('<%=btnGuardar_EditarCliente.ClientID %>');
                boton.click();
            }

            var tipoPersona = document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID %>').value;
            if (tipoPersona == '0') {
                alert('Debe seleccionar un Tipo de Persona [Natural - Jur�dica].');
                return false;
            }
            if (opcion == '0') {  //**** Accesible con ambos tipos de persona
                return true;
            }
            if (tipoPersona == opcion) {  //**** Campo correcto para el tipo de persona
                return true;
            } else {
                alert('Este campo no es accesible para el tipo de Persona [' + getCampoxValorCombo(document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID %>'), tipoPersona) + '].');
                return false;
            }
            return false;
        }
        function valOnKeyPressDNI_EditarCliente() {
            var flag = valOnKeyPress_EditarCliente('1');
            if (flag) { //*** Validamos el DNI
                return onKeyPressEsNumero('event');
            }
            return false;
        }
        function valOnKeyPressRUC_EditarCliente() {
            var flag = valOnKeyPress_EditarCliente('0');
            if (flag) { //*** Validamos el DNI
                return onKeyPressEsNumero('event');
            }
            return false;
        }
        //        function valKeyPressEditarCliente(obj) {
        //            if (event.keyCode == 13) {
        //                onBlurTextTransform(obj, configurarDatos);
        //                document.getElementById('<%=btnGuardar_EditarCliente.ClientID%>').focus();
        //            }
        //            return true;
        //        }

        function valOnClickGuardar_EditarCliente() {
            var tipoPersona = document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID %>').value;
            switch (parseInt(tipoPersona)) {
                case 0:  //**** ----
                    alert('No se ha seleccionado un Tipo de Persona.');
                    return false;
                    break;
                case 1:  //**** NATURAL
                    var txtApPaterno = document.getElementById('<%=txtApPaterno_EditarCliente.ClientID%>');
                    if (CajaEnBlanco(txtApPaterno)) {
                        alert('El campo [Ap. Paterno] es un campo requerido.');
                        txtApPaterno.select();
                        txtApPaterno.focus();
                        return false;
                    }
                    var txtNombres = document.getElementById('<%=txtNombres_EditarCliente.ClientID%>');
                    if (CajaEnBlanco(txtNombres)) {
                        alert('El Campo [Nombre] es un campo requerido.');
                        txtNombres.select();
                        txtNombres.focus();
                        return false;
                    }
                    var txtDNI_Cliente = document.getElementById('<%=txtDNI_EditarCliente.ClientID%>');
                    if (txtDNI_Cliente.value.length > 0) {
                        if (txtDNI_Cliente.value.length < 8) {
                            alert("REVISE EL NUMERO DE D.N.I. DE LA PERSONA NATURAL.");
                            return false;
                        }
                    }
                    var txtRUC_Cliente = document.getElementById('<%=txtRUC_EditarCliente.ClientID%>');
                    if (txtRUC_Cliente.value.length > 0) {
                        if (txtRUC_Cliente.value.length < 11) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA NATURAL.");
                            return false;
                        }
                        var valido = false;
                        if (txtRUC_Cliente.value.substring(0, 2) == '10' || txtRUC_Cliente.value.substring(0, 2) == '15') { valido = true; }
                        if (valido == false) {
                            alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA NATURAL.");
                            return false;
                        }
                    }
                    break;
                case 2:  //**** JURIDICA
                    var txtRazonSocial = document.getElementById('<%=txtRazonSocial_EditarCliente.ClientID%>');
                    if (CajaEnBlanco(txtRazonSocial)) {
                        alert('El Campo [Raz�n Social] es un campo requerido.');
                        txtRazonSocial.select();
                        txtRazonSocial.focus();
                        return false;
                    }
                    var txtRUC = document.getElementById('<%=txtRUC_EditarCliente.ClientID%>');
                    if (CajaEnBlanco(txtRUC)) {
                        alert('El Campo [R.U.C.] es un campo requerido.');
                        txtRUC.select();
                        txtRUC.focus();
                        return false;
                    }
                    if (txtRUC.value.length < 11) {
                        alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA JURIDICA.");
                        return false;
                    }
                    if (txtRUC.value.substring(0, 2) != '20') {
                        alert("REVISE EL NUMERO DE R.U.C. DE LA PERSONA JURIDICA.");
                        return false;
                    }
                    break;
            }
            var txtDireccion = document.getElementById('<%=txtDireccion_EditarCliente.ClientID%>');
            if (CajaEnBlanco(txtDireccion)) {
                // alert('El Campo [Direccion] es un campo requerido.');
                txtDireccion.select();
                txtDireccion.focus();
                return false;
            }
            return confirm('Desea guardar los cambios?');
        }
        //***************** FIN MANT (EDITAR)

        //***************************************************** BUSQUEDA PERSONA
        function valBuscarClientexId() {
            var caja = document.getElementById('<%=txtIdCliente_BuscarxId.ClientID%>');
            if (CajaEnBlanco(caja)) {
                alert('Ingrese un C�digo de Cliente.');
                caja.select();
                caja.focus();
                return false;
            }
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                alert('Debe quitar el detalle del Documento. No se permite la Operaci�n.');
                return false;
            }
            return true;
        }
        function valKeyPressBuscarPersonaxId(e) {
            if (window.event) keyCode = window.event.keyCode;
            else if (e) keyCode = e.which;

            if (keyCode == 13) {
                document.getElementById('<%=btnBuscarClientexIdCliente.ClientID%>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }
            return true;
        }

        //********************* PROCESO DE CALCULO DE IMPORTES
        function calcularTotales_GV_Detalle(opcion, obj) {
            /*
            Opcion:
            0: Cantidad   1: P Venta     2: Dcto Valor     3: Dcto Porcent
            4: PV Final
            */

            var cantidadTotal = 0;

            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var CompPercepcion = parseInt(document.getElementById('<%=hdddoc_CompPercepcion.ClientID%>').value);
            if (isNaN(CompPercepcion)) { CompPercepcion = 0; }
            var cantidad = 0;
            var pVenta = 0;
            var dctoValor = 0;
            var dctoPorcent = 0;
            var pvFinal = 0;
            var pLista = 0;
            var pComercial = 0;
            var importe = 0;
            var importeTotal = 0;
            var dctoTotal = 0;
            var percepcionTotal = 0;
            var TasaPercepcion = 0;
            var porcentDctoMaximo = 0;
            var precioBaseDcto = '';
            var dctoValor_MaxxPVenta = 0;
            var dctoPorcent_MaxxPVenta = 0;
            var descripcionProd = '';
            var codigoProd = '';
            //** se agrego el 28 marzo 2011            
            var IdProducto = 0;
            var IdProductoAux = 0;
            var IdCampania = 0;
            var CantCampania = 0;
            var volVentaMinimo = 0;

            //***descuento maximo
            var dctomax = 0;
            var totaldctomax = 0;

            //********** cajas de texto
            var txtDctoValor;
            var txtDctoPorcent;
            var txtPVFinal;
            var txtImporte;
            var txtDctoValor_MaxxPVenta;
            var txtDctoPorcent_MaxxPVenta;
            var percepcion = 0;
            var cantElem = 0;
            var cantElemValido = 0;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    cantidad = parseFloat(rowElem.cells[3].children[0].value);
                    if (isNaN(cantidad)) { cantidad = 0; }
                    pLista = parseFloat(rowElem.cells[4].innerHTML);
                    if (isNaN(pLista)) { pLista = 0; }
                    pVenta = parseFloat(rowElem.cells[5].children[0].value);
                    if (isNaN(pVenta)) { pVenta = 0; }
                    txtDctoValor = rowElem.cells[6].children[0];
                    dctoValor = parseFloat(txtDctoValor.value);
                    if (isNaN(dctoValor)) { dctoValor = 0; }
                    txtDctoPorcent = rowElem.cells[7].children[0];
                    dctoPorcent = parseFloat(txtDctoPorcent.value);
                    if (isNaN(dctoPorcent)) { dctoPorcent = 0; }
                    txtPVFinal = rowElem.cells[8].children[0];
                    pvFinal = parseFloat(txtPVFinal.value);
                    if (isNaN(pvFinal)) { pvFinal = 0; }

                    porcentDctoMaximo = parseFloat(rowElem.cells[11].children[1].value);
                    txtDctoValor_MaxxPVenta = rowElem.cells[6].children[1];
                    dctoValor_MaxxPVenta = parseFloat(txtDctoValor_MaxxPVenta.value);
                    if (isNaN(dctoValor_MaxxPVenta)) { dctoValor_MaxxPVenta = 0; }

                    txtDctoPorcent_MaxxPVenta = rowElem.cells[7].children[1];
                    dctoPorcent_MaxxPVenta = parseFloat(txtDctoPorcent_MaxxPVenta.value);
                    if (isNaN(dctoPorcent_MaxxPVenta)) { dctoPorcent_MaxxPVenta = 0; }

                    //************* OBTENEMOS EL PRECIO BASE DCTO
                    precioBaseDcto = rowElem.cells[11].children[7].value;
                    pComercial = parseFloat(rowElem.cells[2].children[1].value);

                    txtImporte = rowElem.cells[9].children[0];

                    //** se agrego el 28 marzo 2011

                    IdProducto = rowElem.cells[11].children[6].value;

                    IdProductoAux = rowElem.cells[11].children[11].value;
                    if (isNaN(parseInt(IdProductoAux))) { IdProductoAux = 0; }

                    IdCampania = rowElem.cells[11].children[12].value;
                    if (isNaN(parseInt(IdCampania))) { IdCampania = 0; }

                    var CanCampaniaValidacion = rowElem.cells[11].children[13];
                    if (typeof CanCampaniaValidacion != "undefined") {
                        CantCampania = parseFloat(rowElem.cells[11].children[13].value);
                        if (isNaN(CantCampania)) { CantCampania = 0; }
                    }

                    volVentaMinimo = parseFloat(rowElem.cells[11].children[4].value);

                    if (parseInt(IdCampania) > 0 && parseInt(IdProductoAux) > 0 && parseFloat(CantCampania) > 0) { // es un producto en campania
                        var cantidadTransferencia = Math.floor(parseFloat(getVolumenMinVenta(IdCampania, IdProductoAux)) / (CantCampania / volVentaMinimo)); if (isNaN(cantidadTransferencia)) { cantidadTransferencia = 0; }
                        rowElem.cells[3].children[0].value = cantidadTransferencia;
                    }

                    switch (opcion) {
                        case 0:   //************ CANTIDAD 
                            break;
                        case 1:  //************ P VENTA
                            if (obj != null) {
                                if (obj.id == rowElem.cells[5].children[0].id) {
                                    pvFinal = pVenta;
                                    dctoPorcent = 0;
                                    dctoValor = 0;
                                    txtPVFinal.value = redondear(pvFinal, 2);
                                    txtDctoPorcent.value = redondear(dctoPorcent, 2);
                                    txtDctoValor.value = redondear(dctoValor, 4);
                                }
                            }
                            break;
                        case 2:  //*********** DCTO VALOR
                            pvFinal = pVenta - dctoValor;
                            if (pVenta <= 0) {
                                dctoPorcent = 0;
                            } else {
                                dctoPorcent = ((dctoValor) / (pVenta)) * 100;
                            }
                            txtPVFinal.value = redondear(pvFinal, 2);
                            txtDctoPorcent.value = redondear(dctoPorcent, 2);

                            break;
                        case 3:   //*********** DCTO PORCENT
                            pvFinal = pVenta * (1 - dctoPorcent / 100);

                            if (pvFinal >= pVenta) {
                                dctoValor = 0;
                            } else {
                                dctoValor = pVenta - pvFinal;
                            }
                            txtPVFinal.value = redondear(pvFinal, 2);
                            txtDctoValor.value = redondear(dctoValor, 4);

                            break;
                        case 4:   //*********** PV FINAL
                            if (pvFinal >= pVenta) {
                                dctoPorcent = 0;
                                dctoValor = 0;
                            } else {
                                dctoValor = (pVenta - pvFinal);
                                if (pVenta <= 0) {
                                    dctoPorcent = 0;
                                } else {
                                    dctoPorcent = ((dctoValor) / (pVenta)) * 100;
                                }
                            }
                            txtDctoPorcent.value = redondear(dctoPorcent, 2);
                            txtDctoValor.value = redondear(dctoValor, 4);

                            break;
                    }

                    //*********** MANEJO DE DESCUENTOS
                    /*
                    EL PORCENTAJE DE DCTO ESTA EN FUNCION AL TIPO DE PRECIO DE LISTA O AL TIPO DE PRECIO COMERCIAL
                    PL: SOBRE PRECIO DE LISTA
                    PC: SOBRE PRECIO COMERCIAL
                    */

                    var dctoPrecioBase = 0;
                    var dif_PrecioVenta_PrecioBase = 0;
                    var precioBase = 0;

                    if (precioBaseDcto == 'PL') {
                        precioBase = pLista;
                    } else if (precioBaseDcto == 'PC') {
                        precioBase = pComercial;
                    } else {
                        //alert('NO SE HA DESIGNADO UN PRECIO BASE EN LA APLICACI�N DE DESCUENTOS (CONFIGURACI�N DESCUENTOS). PROBLEMA EN LA OPERACI�N.');
                        precioBase = pLista;
                        //return false;
                    }

                    dctoPrecioBase = precioBase * (porcentDctoMaximo / 100);
                    dif_PrecioVenta_PrecioBase = pVenta - precioBase;
                    dctoValor_MaxxPVenta = dctoPrecioBase + dif_PrecioVenta_PrecioBase;
                    if (dctoValor_MaxxPVenta <= 0) { dctoValor_MaxxPVenta = 0; }
                    if (pVenta > 0) {
                        dctoPorcent_MaxxPVenta = (dctoValor_MaxxPVenta / pVenta) * 100;
                    } else {
                        dctoPorcent_MaxxPVenta = 0;
                    }
                    if (dctoPorcent_MaxxPVenta <= 0) { dctoPorcent_MaxxPVenta = 0; }

                    txtDctoValor_MaxxPVenta.value = redondear(dctoValor_MaxxPVenta, 4);
                    txtDctoPorcent_MaxxPVenta.value = redondear(dctoPorcent_MaxxPVenta, 2);
                    //********************* end Manejo de Descuentos


                    var preImporte = cantidad * redondear(pvFinal, 2);
                    importe = preImporte.toPrecision(10);
                    //***************************** IMPRIMIMOS DATOS                    
                    txtImporte.value = redondear(importe, 2);
                    importeTotal = importeTotal + redondear(importe, 2);
                    //***************************** CASO BOLETA
                    if (importe > 0) {
                        cantElemValido = cantElemValido + cantidad;
                        cantElem = cantElem + 1;
                    }

                    //*************   PERCEPCION
                    TasaPercepcion = parseFloat(rowElem.cells[10].innerHTML);
                    if (TasaPercepcion > 0) {
                        percepcion = (TasaPercepcion / 100);
                    }

                    //****************  DESCUENTO
                    dctoTotal = dctoTotal + (dctoValor * cantidad);

                    //****************  DESCUENTO MAXIMO
                    dctomax = cantidad * parseFloat(txtDctoValor_MaxxPVenta.value);
                    totaldctomax = totaldctomax + redondear(dctomax, 2);

                    //****** CANTIDAD TOTAL ( PARA LA FACTURA POR TRANSPORTE )
                    cantidadTotal = cantidadTotal + cantidad;

                } //********* FIN FOR                                                
            } //************ FIN IF

            //***************** SE IMPRIME LOS VALORES


            var porcentIgv = parseFloat(document.getElementById('<%=lblPorcentIgv.ClientID%>').innerHTML);
            if (porcentIgv == null) {
                porcentIgv = 0;
            }

            var totalConcepto = 0;
            if (document.getElementById('<%=txtTotalConcepto.ClientID%>') == null) {
                totalConcepto = 0;
            } else {
                totalConcepto = parseFloat(document.getElementById('<%=txtTotalConcepto.ClientID%>').value);
            }

            var totalConceptoAbs = 0;
            totalConceptoAbs = calcularTotalConceptoAbs();

            var NoAfectoIgv = ConceptoNoAfectoIgv();

            var subTotal = 0;
            var total = 0;
            var igv = 0;
            var totalPagar = 0;

            if (NoAfectoIgv == 1) {
                total = importeTotal + totalConcepto;
                subTotal = total;
                igv = total - subTotal;
                percepcionTotal = (importeTotal + totalConceptoAbs) * percepcion;
                totalPagar = total + percepcionTotal;

            } else {
                total = importeTotal + totalConcepto;
                subTotal = total / (1 + porcentIgv / 100);
                igv = total - subTotal;
                percepcionTotal = (importeTotal + totalConceptoAbs) * percepcion;
                totalPagar = total + percepcionTotal;

            }

            //********************* ANALISIS DE LA PERCEPCION
            var montoMaxAfectoPercepcion = parseFloat(document.getElementById('<%=hddMontoMaxAfectoPercepcion.ClientID%>').value);
            var cboTipoDocumento = document.getElementById('<%=cboTipoDocumento.ClientID%>');

            if (parseInt(cboTipoDocumento.value) == 3) { //******** BOLETA
                if ((total <= montoMaxAfectoPercepcion && importeTotal <= montoMaxAfectoPercepcion) || (cantElem == 1 && cantElemValido == 1 && percepcionTotal > 0)) {
                    percepcionTotal = 0;
                    totalPagar = total;
                }
            }
            if (CompPercepcion == 1) {  // ************* EL DOCUMENTO HA COMPROMETIDO LA PERCEPCION APARTE
                percepcionTotal = 0;
                totalPagar = total;
            }

            //********************* ANALISIS DE LA RETENCION
            var txtTasaAgente = document.getElementById('<%=txtTasaAgente.ClientID%>');
            var txtRetencion = document.getElementById('<%=txtRetencion.ClientID%>');
            var hddSujetoRetencion = document.getElementById('<%=hddSujetoRetencion.ClientID%>');
            var montoRetencion = 0;

            if (parseInt(hddSujetoRetencion.value) == 1 && parseInt(cboTipoDocumento.value) == 1) {  //********** AGENTE DE RETENCION
                var tasaRetencion = parseFloat(txtTasaAgente.value);
                var MontoAfectoRetencion = parseFloat(document.getElementById('<%=hddMontoAfectoRetencion.ClientID%>').value);
                if (isNaN(MontoAfectoRetencion)) { MontoAfectoRetencion = 0; }
                if (total > MontoAfectoRetencion && importeTotal > MontoAfectoRetencion) {
                    montoRetencion = (importeTotal + totalConceptoAbs) * (tasaRetencion / 100);
                }
            }


            document.getElementById('<%=txtImporteTotal.ClientID%>').value = Format(importeTotal, 2);
            document.getElementById('<%=txtSubTotal.ClientID%>').value = Format(subTotal, 2);
            document.getElementById('<%=txtIGV.ClientID%>').value = Format(igv, 2);
            document.getElementById('<%=txtTotal.ClientID%>').value = Format(total, 2);
            document.getElementById('<%=txtPercepcion.ClientID%>').value = redondear(percepcionTotal, 2);
            document.getElementById('<%=txtDescuento.ClientID%>').value = redondear(dctoTotal, 2);
            document.getElementById('<%=txtTotalConcepto_PanelDetalle.ClientID%>').value = redondear(totalConcepto, 2);
            document.getElementById('<%=txtTotalAPagar.ClientID%>').value = Format(totalPagar, 2);
            document.getElementById('<%=txtRetencion.ClientID%>').value = redondear(montoRetencion, 3);
            document.getElementById('<%=txtDescuentoMax.ClientID%>').value = redondear(totaldctomax, 2);

            //**************** FACTURA DE TRANSPORTE
            if (parseInt(cboTipoDocumento.value) == 59) {

                var txtValorReferencial = document.getElementById('<%=txtValorReferencial.ClientID%>');
                var txtValorReferencial_Total = document.getElementById('<%=txtValorReferencial_Total.ClientID%>');
                var txtCantidad_TR = document.getElementById('<%=txtCantidad_Transporte.ClientID%>');
                var valorReferencial = parseFloat(txtValorReferencial.value);
                if (isNaN(parseFloat(txtValorReferencial.value)) || txtValorReferencial.value.length <= 0) { valorReferencial = 0; }

                var valorReferencial_Total = cantidadTotal * valorReferencial;
                txtCantidad_TR.value = redondear(cantidadTotal, 3);
                txtValorReferencial_Total.value = redondear(valorReferencial_Total, 3);

            }

            //**************** CALCULAMOS LOS DATOS DE CANCELACION
            var cboCondicionPago = document.getElementById('<%=cboCondicionPago.ClientID%>');
            if (parseInt(cboCondicionPago.value) == 1) {  //**** CONTADO
                calcularDatosCancelacion();
            }

            return false;
        }
        //
        function getVolumenMinVenta(v_IdCampania, v_IdProducto) {
            var cantidad = 0;
            var _IdCampania = 0;
            var _IdProducto = 0;
            var grilla1 = document.getElementById('<%=GV_Detalle.ClientID%>');
            for (var j = 1; j < grilla1.rows.length; j++) {
                var rowElemx = grilla1.rows[j];

                _IdProducto = rowElemx.cells[11].children[6].value;
                if (isNaN(parseInt(_IdProducto))) { _IdProducto = 0; }

                _IdCampania = rowElemx.cells[11].children[12].value;
                if (isNaN(parseInt(_IdCampania))) { _IdCampania = 0; }

                if (parseInt(_IdProducto) == parseInt(v_IdProducto) && parseInt(_IdCampania) == parseInt(v_IdCampania)) {
                    cantidad = parseFloat(rowElemx.cells[3].children[0].value);
                    if (isNaN(cantidad)) { cantidad = 0; }

                    return cantidad;
                    break;

                }
            }
        }
        //
        function valPorcentMaximoDctoxArticulo_OnBlur(caja) {
            if (caja != null) {
                if (CajaEnBlanco(caja) || isNaN(caja.value)) {
                    caja.select();
                    caja.focus();
                    alert('Debe ingresar un valor v�lido.');
                    return false;
                }
            }

            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var pvFinal = 0;
            var pLista = 0;
            var pComercial = 0; //************ PRECIO DE LA TABLA PRODUCTO TIPO PV          
            var porcentDctoMaximo = 0;
            var descripcionProd = '';
            var codigoProd = '';
            var pVenta = 0;
            var cantidad = 0;
            var volVentaMinimo = 0;
            //********** cajas de texto
            var txtPVFinal;
            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            var moneda = getCampoxValorCombo(cboMoneda, cboMoneda.value);

            var validarFrm = true;
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
            var hddIdDocumentoRef = document.getElementById('<%=hddIdDocumentoRef.ClientID%>');

            if ((parseInt(hddFrmModo.value) == 2) || (isNaN(parseInt(hddIdDocumentoRef.value)) == false && hddIdDocumentoRef.value.length > 0)) {
                validarFrm = false;
            }

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];

                    pComercial = parseFloat(rowElem.cells[2].children[1].value);
                    if (isNaN(pComercial)) { pComercial = 0; }

                    pVenta = parseFloat(rowElem.cells[5].children[0].value);
                    pLista = parseFloat(rowElem.cells[4].innerHTML);
                    txtPVFinal = rowElem.cells[8].children[0];
                    pvFinal = parseFloat(txtPVFinal.value);
                    if (isNaN(pvFinal)) { pvFinal = 0; }

                    porcentDctoMaximo = parseFloat(rowElem.cells[11].children[1].value);

                    codigoProd = rowElem.cells[1].innerHTML;
                    descripcionProd = rowElem.cells[2].children[0].innerHTML;

                    if (pVenta < pLista) {
                        alert('El precio de venta del producto  < ' + codigoProd + '  -  ' + descripcionProd + ' > no puede ser menor al precio de lista.\nNO SE PERMITE LA OPERACI�N.');
                        return false;
                    }

                    //****** CALCULAMOS EL PORCENTAJE DE DCTO SOBRE EL PRECIO DE LISTA

                    //*********** MANEJO DE DESCUENTOS
                    /*
                    EL PORCENTAJE DE DCTO ESTA EN FUNCION AL TIPO DE PRECIO DE LISTA O AL TIPO DE PRECIO COMERCIAL
                    PL: SOBRE PRECIO DE LISTA
                    PC: SOBRE PRECIO COMERCIAL
                    */

                    var dctoPrecioBase = 0;
                    var dctoPrecioBase_User = 0;
                    var dif_PrecioVenta_PrecioBase = 0;
                    var precioBase = 0;
                    var precioBaseDcto = rowElem.cells[11].children[7].value;
                    var dctoValor_MaxxPVenta = 0;
                    var txtDctoValor = rowElem.cells[6].children[0];
                    var dctoValor = parseFloat(txtDctoValor.value);
                    var txtPorcentDctoValor_User = rowElem.cells[7].children[0];
                    var PorcentDctoValor_User = parseFloat(txtPorcentDctoValor_User.value);

                    var IdProductoAux = rowElem.cells[11].children[11].value;
                    if (isNaN(parseInt(IdProductoAux))) { IdProductoAux = 0; }
                    var IdCampania = rowElem.cells[11].children[12].value;
                    if (isNaN(parseInt(IdCampania))) { IdCampania = 0; }
                    //qwe
//                    var CantCampania = parseFloat(rowElem.cells[11].children[13].value);
//                    if (isNaN(CantCampania)) { CantCampania = 0; }

                    var CantCampaniaValidacion = rowElem.cells[11].children[13];
                    if (typeof CantCampaniaValidacion != "undefined") {
                        CantCampania = parseFloat(rowElem.cells[11].children[13].value);
                        if (isNaN(CantCampania)) { CantCampania = 0; }
                    }

                    if (isNaN(PorcentDctoValor_User)) { PorcentDctoValor_User = 0; }

                    if (precioBaseDcto == 'PL') {
                        precioBase = pLista;
                    } else if (precioBaseDcto == 'PC') {
                        precioBase = pComercial;
                    } else {
                        //alert('NO SE HA DESIGNADO UN PRECIO BASE EN LA APLICACI�N DE DESCUENTOS (CONFIGURACI�N DESCUENTOS). PROBLEMA EN LA OPERACI�N.');
                        precioBase = pLista;
                        //return false;
                    }

                    dctoPrecioBase = precioBase * (porcentDctoMaximo / 100);
                    if (PorcentDctoValor_User > 0) {
                        dctoPrecioBase_User = precioBase * (PorcentDctoValor_User / 100);
                    }

                    dif_PrecioVenta_PrecioBase = pVenta - precioBase;
                    dctoValor_MaxxPVenta = dctoPrecioBase + dif_PrecioVenta_PrecioBase;
                    //if (dctoValor_MaxxPVenta <= 0) { dctoValor_MaxxPVenta = 0; }

                    if (dctoValor_MaxxPVenta < 0 && validarFrm) {
                        alert('EL PRECIO M�NIMO DE VENTA PARA EL PRODUCTO < ' + codigoProd + '  -  ' + descripcionProd + ' >  ES DE < ' + moneda + ' ' + redondear((precioBase - dctoPrecioBase), 2) + ' >. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }

                    if ((dctoValor > redondear(dctoValor_MaxxPVenta, 4)) && validarFrm) {
                        alert('EL DESCUENTO M�XIMO PERMITIDO PARA EL PRODUCTO < ' + codigoProd + '  -  ' + descripcionProd + ' >  ES DE < ' + moneda + ' ' + redondear(dctoValor_MaxxPVenta, 2) + ' >. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }

                    if ((dctoPrecioBase_User > dctoValor_MaxxPVenta) && validarFrm) {
                        alert('EL DESCUENTO M�XIMO PERMITIDO PARA EL PRODUCTO < ' + codigoProd + '  -  ' + descripcionProd + ' >  ES DE < ' + porcentDctoMaximo + ' >. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }

                    //****************** VALIDAMOS EL VOLUMEN MINIMO DE VENTA
                    cantidad = parseFloat(rowElem.cells[3].children[0].value);
                    if (isNaN(cantidad)) { cantidad = 0; }
                    volVentaMinimo = parseFloat(rowElem.cells[11].children[4].value);
                    if (isNaN(volVentaMinimo)) { volVentaMinimo = 0; }
                    if (cantidad < volVentaMinimo && validarFrm) {

                        if (parseInt(IdCampania) > 0 && parseInt(IdProductoAux) > 0 && parseFloat(CantCampania) > 0) { // es un producto en campania
                            alert('LA CANTIDAD INGRESADA PARA EL PRODUCTO [ ' + descripcionProd.toUpperCase() + ' ] ES MENOR A [' + volVentaMinimo + '] PARA LA APLICACI�N DE LA CAMPA�A.');
                        } else {
                            alert('LA CANTIDAD INGRESADA PARA EL PRODUCTO [ ' + descripcionProd.toUpperCase() + ' ] ES MENOR AL VOLUMEN DE VENTA M�NIMO [' + volVentaMinimo + '] PARA LA APLICACI�N DEL PRECIO DE LISTA SELECCIONADO.');
                        }

                        return false;
                    }

                } //********* FIN FOR
            } //************ FIN IF

            return true;
        }
        function valOnFocus_txtImporte() {
            var boton = document.getElementById('<%=btnAgregar.ClientID%>');
            boton.focus();
            return false;
        }
        function valOnClick_btnPermisoDcto_GV_Detalle(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var descripcionProd = '';
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];
                    if (boton.id == rowElem.cells[11].children[0].id) {
                        descripcionProd = rowElem.cells[2].children[0].innerHTML;
                        onCapa('capaAddPermiso_Detalle');
                        document.getElementById('<%=lblProducto_AddDctoAdicional.ClientID%>').innerHTML = descripcionProd;
                        document.getElementById('<%=hddIndex_GV_Detalle.ClientID%>').value = (i - 1);
                        document.getElementById('<%=txtLogin_AddDctoAdicional.ClientID%>').value = '';
                        document.getElementById('<%=txtClave_AddDctoAdicional.ClientID%>').value = '';
                        document.getElementById('<%=txtClaveConfirmar_AddDctoAdicional.ClientID%>').value = '';
                        document.getElementById('<%=txtLogin_AddDctoAdicional.ClientID%>').select();
                        document.getElementById('<%=txtLogin_AddDctoAdicional.ClientID%>').focus();
                        return false;
                    }
                }
            }
            alert('Problemas en la ubicaci�n de la Fila.');
            return false;
        }
        function valOnClick_btnAceptar_AddDctoAdicional() {
            var txtUsuario = document.getElementById('<%=txtLogin_AddDctoAdicional.ClientID%>');
            if (CajaEnBlanco(txtUsuario)) {
                alert('Campo requerido');
                txtUsuario.select();
                txtUsuario.focus();
                return false;
            }
            var txtClave1 = document.getElementById('<%=txtClave_AddDctoAdicional.ClientID%>');
            if (CajaEnBlanco(txtClave1)) {
                alert('Campo requerido');
                txtClave1.select();
                txtClave1.focus();
                return false;
            }
            var txtClave2 = document.getElementById('<%=txtClaveConfirmar_AddDctoAdicional.ClientID%>');
            if (CajaEnBlanco(txtClave2)) {
                alert('Campo requerido');
                txtClave2.select();
                txtClave2.focus();
                return false;
            }
            if (txtClave1.value != txtClave2.value) {
                alert('Confirme su contrase�a.');
                txtClave2.select();
                txtClave2.focus();
                return false;
            }
            return true;
        }
        function lostFocus() {
            document.getElementById('<%=btnAgregar.ClientID%>').focus();
            return false;
        }
        function valOnClickAddGlosa() {
            var txtGlosa = document.getElementById('<%= txtGlosa_CapaGlosa.ClientID%>');
            if (txtGlosa.value.length <= 0) {
                alert('Ingrese una descripci�n.');
                txtGlosa.select();
                txtGlosa.focus();
                return false;
            }
            return true;
        }
        function onCapaGlosa(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var index = -1;
            var glosa = '';
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[3].children[2].id == boton.id) {
                        index = (i - 1);
                        glosa = rowElem.cells[11].children[3].value;
                        break;
                    }
                }
            }
            if (index < 0) {
                alert('No se hall� el �ndice del Producto seleccionado.');
                return false;
            }
            onCapa('capaGlosa');
            document.getElementById('<%=hddIndexGlosa_Gv_Detalle.ClientID%>').value = index;
            document.getElementById('<%= txtGlosa_CapaGlosa.ClientID%>').value = glosa;
            document.getElementById('<%= txtGlosa_CapaGlosa.ClientID%>').select();
            document.getElementById('<%= txtGlosa_CapaGlosa.ClientID%>').focus();
            return false;
        }
        function lostFocusConcepto() {
            document.getElementById('<%= btnAddDetalleCobro.ClientID%>').focus();
            return false;
        }
        function calcularTotalConcepto() {
            var grilla = document.getElementById('<%=GV_Concepto.ClientID%>');
            var txtMonto;
            var monto = 0;
            var montoTotal = 0;
            var detraccionTotal = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    txtMonto = rowElem.cells[2].children[1];
                    monto = parseFloat(txtMonto.value);
                    if (isNaN(monto)) { monto = 0; }
                    montoTotal = montoTotal + monto;


                    //************* ANALISIS DE LA DETRACCION
                    var porcentDetraccion = parseFloat(rowElem.cells[2].children[2].value);
                    var montoMinimoDetraccion = parseFloat(rowElem.cells[2].children[3].value);

                    if (porcentDetraccion > 0 && monto > montoMinimoDetraccion) {
                        detraccionTotal = detraccionTotal + monto * (porcentDetraccion / 100);
                    }
                }
            }
            document.getElementById('<%=txtTotalConcepto.ClientID%>').value = redondear(montoTotal, 2);
            document.getElementById('<%=txtDetraccion.ClientID%>').value = redondear(detraccionTotal, 3);
            calcularTotales_GV_Detalle(0);
            return false;
        }
        function calcularTotalConceptoAbs() {
            var grilla = document.getElementById('<%=GV_Concepto.ClientID%>');
            var txtMonto;
            var monto = 0;
            var montoTotal = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    txtMonto = rowElem.cells[2].children[1];
                    monto = parseFloat(txtMonto.value);
                    if (isNaN(monto)) { monto = 0; }
                    if (monto > 0 || rowElem.cells[1].children[0].value == 10) {
                        montoTotal = montoTotal + monto;
                    }
                }
            }
            return montoTotal;
        }

        function ConceptoNoAfectoIgv() {
            var NoAfectoIgv = 0;
            var grilla = document.getElementById('<%=GV_Concepto.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    //hddNoAfectoIgv = rowElem.cells[1].children[5];
                    //NoAfectoIgv = parseInt(hddNoAfectoIgv.value);
                    //if (NoAfectoIgv == 1) { break; }
                    if (typeof hddNoAfectoIgv != "undefined") {
                        NoAfectoIgv = 1;
                        break;
                    }
                }
            }            
            return NoAfectoIgv;
        }



        function valOnKeyPress_txtMonto(caja) {
            var grilla = document.getElementById('<%=GV_Concepto.ClientID%>');
            var txtMonto;
            var cboConcepto;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    txtMonto = rowElem.cells[2].children[1];
                    cboConcepto = rowElem.cells[1].children[0];
                    if (txtMonto.id == caja.id) {
                        if (cboConcepto.value.length <= 0 || parseInt(cboConcepto.value) <= 0) {
                            alert('Debe seleccionar un Concepto.');
                            txtMonto.value = 0;
                            return false;
                        } else {
                            break;
                        }

                    }
                }
            }
            return (validarNumeroPuntoPositivo('event'));
        }
        function valOnClick_cboTipoPrecioV() {
            var txtIdCliente = document.getElementById('<%=txtCodigoCliente.ClientID%>');
            if (isNaN(txtIdCliente.value) || txtIdCliente.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                mostrarCapaPersona();
                return false;
            }
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla == null) {
                alert('DEBE INGRESAR PRODUCTOS.');
                return false;
            }
            return true;
        }

        function valOnClick_btnConsultarVolumenVentaMin(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];
                    if (boton.id == rowElem.cells[11].children[5].id) {
                        document.getElementById('<%=hddIndexCambiarTipoPV_GV_Detalle.ClientID%>').value = (i - 1);
                        return true;
                    }
                }
            }
            alert('Problemas en la ubicaci�n de la Fila.');
            return false;
        }
        function valOnClick_btnCambiarTipoPrecioVenta_Detalle() {
            var cboTipoPV = document.getElementById('<%=cboTipoPrecioPV_CambiarDetalle.ClientID%>');
            if (isNaN(parseInt(cboTipoPV.value)) || parseInt(cboTipoPV.value) <= 0 || cboTipoPV.value.length <= 0) {
                alert('Debe seleccionar un Tipo de Precio de Venta.');
                return false;
            }
            return true;
        }





        function valSaveDocumento() {

            var valorVenta = parseFloat(UnFormat(document.getElementById('<%=txtSubTotal.ClientID%>').value, ',', ''));
            if (isNaN(valorVenta)) { valorVenta = 0; }

            if (valorVenta < 0) {
                alert('VERIFIQUE LOS CONCEPTOS DEL DOCUMENTO /n RECUERDE, SI ESTA APLICANDO UN ANTICIPO DE PAGO, ESTE NO PUEDE SUPERAR EL IMPORTE TOTAL DE SU DOCUMENTO.');
                return false;
            }

            //************ VALIDAMOS DESCUENTOS / VOLUMEN VENTA MINIMO
            if (!(valPorcentMaximoDctoxArticulo_OnBlur(null))) {
                return false;
            }

            if (!(valSave_DescuentoGlobal())) {
                return false;
            }

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }

            var cboAlmacenReferencia = document.getElementById('<%=cboAlmacenReferencia.ClientID%>');
            if (isNaN(parseInt(cboAlmacenReferencia.value)) || cboAlmacenReferencia.value.length <= 0) {
                alert('Debe seleccionar un Almac�n');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie de Documento.');
                return false;
            }

            var cboCaja = document.getElementById('<%=cboCaja.ClientID%>');
            if (isNaN(parseInt(cboCaja.value)) || cboCaja.value.length <= 0) {
                alert('Debe seleccionar una Caja.');
                return false;
            }

            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            if (isNaN(parseInt(cboMoneda.value)) || cboMoneda.value.length <= 0) {
                alert('Debe seleccionar una Moneda.');
                return false;
            }
            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }

            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }


            var txtDireccionCliente = document.getElementById('<%=txtDireccionCliente.ClientID %>');
            var txtRuc = document.getElementById('<%=txtRuc.ClientID %>');
            var txtDni = document.getElementById('<%=txtDni.ClientID %>');
            var cboTipoDocumento = document.getElementById('<%=cboTipoDocumento.ClientID%>');

            if (isNaN(parseInt(cboTipoDocumento.value)) || cboTipoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Tipo de Documento.');
                return false;
            }
            if (cboTipoDocumento.value == 1 || cboTipoDocumento.value == '1' || cboTipoDocumento.value == 1101353001 || cboTipoDocumento.value == '1101353001') { //FACTURA                
                if (txtDireccionCliente.value.length <= 0) {
                    alert('El cliente debe tener direcci�n para realizar la facturac��n.');
                    return false;
                }
                if (txtRuc.value.length <= 0) {
                    alert('El cliente debe tener R.U.C. para realizar la facturac��n.');
                    return false;
                }
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }

            var txtTotalAPagar = document.getElementById('<%=txtTotalAPagar.ClientID%>');
            var TotalAPagar = parseFloat(UnFormat(txtTotalAPagar.value, ',', ''));
            if (isNaN(TotalAPagar)) {
                alert('El Total a Pagar debe ser un valor mayor a cero.');
                return false;
            }

            var IdTipoPersona = parseInt(document.getElementById('<%=cboTipoPersona_PanelCliente.ClientID%>').value);
            if (cboTipoDocumento.value == 3 || cboTipoDocumento.value == '3' || cboTipoDocumento.value == 1101353002 || cboTipoDocumento.value == '1101353002') { // boleta
                var hddValidarBoleta = parseFloat(document.getElementById('<%=hddValidarBoleta.ClientID %>').value);
                if (isNaN(hddValidarBoleta)) { hddValidarBoleta = 0; }

                if (hddValidarBoleta > 0 && IdTipoPersona == 1) {
                    if (TotalAPagar >= hddValidarBoleta) {
                        if (txtDireccionCliente.value.length <= 0) {
                            alert('El cliente debe tener direcci�n para realizar la boleta.');
                            return false;
                        }
                        if (txtDni.value.length <= 0) {
                            alert('El cliente debe tener D.N.I para realizar la boleta.');
                            return false;
                        }
                    }
                }

                var ImporteTotal = 0;
                if (document.getElementById('<%=txtImporteTotal.ClientID%>') == null) {
                    ImporteTotal = 0;
                } else {
                    ImporteTotal = parseFloat(UnFormat(document.getElementById('<%=txtImporteTotal.ClientID%>').value, ',', ''));
                }

                var montoMaxAfectoPercepcion = parseFloat(document.getElementById('<%=hddMontoMaxAfectoPercepcion.ClientID%>').value);
                if (isNaN(montoMaxAfectoPercepcion)) { montoMaxAfectoPercepcion = 0; }

                if (TotalAPagar <= montoMaxAfectoPercepcion && ImporteTotal <= montoMaxAfectoPercepcion) {
                    document.getElementById('<%=chb_SaltarCorrelativo.ClientID%>').checked = false;
                    document.getElementById('<%=chb_SepararxMontos.ClientID%>').checked = false;
                    document.getElementById('<%=chb_CompPercepcion.ClientID%>').checked = false;
                    document.getElementById('<%=chb_UseAlias.ClientID%>').checked = false;
                    document.getElementById('<%=chb_SepararAfecto_NoAfecto.ClientID%>').checked = false;
                }
            }

            var valPercepcion = parseFloat(UnFormat(document.getElementById('<%=txtPercepcion.ClientID%>').value, ',', ''));

            var monedaEmision = getCampoxValorCombo(cboMoneda, cboMoneda.value);

            //************* Validando cantidades y precios
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                var cantidad = 0;
                var precioFinal = 0;
                var txtCantidad;
                var txtPrecioFinal;
                var porcentDctoMaximo = 0;

                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    cantidad = parseFloat(rowElem.cells[3].children[0].value);
                    txtCantidad = rowElem.cells[3].children[0];

                    if (isNaN(cantidad)) { cantidad = 0; }
                    if (cantidad <= 0) {
                        alert('Ingrese una cantidad mayor a cero.');
                        txtCantidad.select();
                        txtCantidad.focus();
                        return false;
                    }
                    txtPrecioFinal = rowElem.cells[9].children[0];
                    precioFinal = parseFloat(txtPrecioFinal.value);
                    if (isNaN(precioFinal)) { precioFinal = 0; }

                    porcentDctoMaximo = parseFloat(rowElem.cells[11].children[1].value);

                    if (precioFinal <= 0 && porcentDctoMaximo < 100) {
                        alert('El Precio Final de Venta debe ser un valor mayor a cero.');
                        txtPrecioFinal.select();
                        txtPrecioFinal.focus();
                        return false;
                    }
                }
            }


            var cboCondicionPago = document.getElementById('<%=cboCondicionPago.ClientID%>');
            var chb_CompPercepcion = document.getElementById('<%=chb_CompPercepcion.ClientID%>');

            var grilla = document.getElementById('<%=GV_Concepto.ClientID%>');
            if (grilla != null) {
                var txtMonto;
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    txtMonto = rowElem.cells[2].children[1];
                    monto = parseFloat(txtMonto.value);
                    if (isNaN(monto)) { monto = 0; }
                    if (monto == 0) {
                        alert('SE HAN INGRESADO [ CONCEPTOS ] CON VALOR CERO. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }
                     
                    botonAnticipo = rowElem.cells[1].children[2];

                    if (chb_CompPercepcion.checked == false && monto < 0 && valPercepcion > 0 && botonAnticipo != null) {
                        alert('HA APLICADO UN DOCUMENTO DE ANTICIPO, LA PERCEPCI�N DEBE GENERARLA EN UN NUEVO DOCUMENTO. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }                    
                    
//                    if (botonAnticipo != null && monto > 0) {
////                          if (parseInt(cboCondicionPago.value) != 1 || document.getElementById('<%=GV_Cancelacion.ClientID%>') == null) {
////                            alert('EL ANTICIPO DEBE SER CANCELADO. NO SE PERMITE LA OPERACI�N.');
////                            return false;
////                        }

//                    }

                }
            }


            //***************** VALIDAMOS PAGO AL CONTADO

            if (parseInt(cboCondicionPago.value) == 1) {  //******* CONTADO
                //******** VALIDAMOS LOS DATOS DE CANCELACION
                var grilla = document.getElementById('<%=GV_Cancelacion.ClientID%>');
                var hddIdMedioPagoPrincipal = document.getElementById('<%=hddIdMedioPagoPrincipal.ClientID%>');
                var vuelto = parseFloat(document.getElementById('<%=txtVuelto.ClientID%>').value);
                if (isNaN(vuelto)) { vuelto = 0; }
                if (grilla == null) {
                    alert('Debe cancelar el documento. No se permite la Operaci�n.');
                    return false;
                }
                if (grilla != null) {
                    for (var i = 1; i < grilla.rows.length; i++) {
                        var rowElem = grilla.rows[i];
                        var monto = parseFloat(rowElem.cells[1].children[1].innerHTML);
                        if (isNaN(monto)) { monto = 0; }
                        if (monto <= 0 && TotalAPagar > 0) {
                            alert('Se ha ingresado un Dato de Cancelaci�n con un valor igual a cero. No se permite la Operaci�n.');
                            return false;
                        }

                        var IdMedioPago = parseInt(rowElem.cells[1].children[2].value);

                        if (IdMedioPago == 2 && valPercepcion > 0 && chb_CompPercepcion.checked == false) {
                            alert('HA INGRESADO UNA NOTA DE CREDITO COMO MEDIO DE PAGO /n LA PERCEPCI�N DEBE GENERARLA EN UN NUEVO DOCUMENTO. NO SE PERMITE LA OPERACI�N.');
                            return false;
                        }

                        if (vuelto > 0 && parseInt(hddIdMedioPagoPrincipal.value) != IdMedioPago) {
                            var montoEfectivo = getMontoMedioPagoPrincipal();
                            if (montoEfectivo > 0) {
                                if (montoEfectivo < vuelto) {
                                    alert('El medio de Pago Principal es menor al vuelto. No se permite la Operaci�n.');
                                    return false;
                                }
                            } else {
                                alert('Se han ingresado Medios de Pago que NO deben generar vuelto. No se permite la Operaci�n.');
                                return false;
                            }
                        } // end if
                    }
                }


                //                else {
                //                
                //                    //alert('Debe ingresar un Dato de Cancelaci�n.');

                //                    var msn;
                //                    var mond;
                //                    var tipdoc= getCampoxValorCombo(cboTipoDocumento, cboTipoDocumento.value);
                //                    mond = getCampoxValorCombo(cboMoneda, cboMoneda.value);
                //                    msn = 'Usted est� registrando una [ ' + tipoDocumento + ' ] por un valor de ' + monedaEmision + ' ' + txtTotalAPagar.value + '. Desea continuar con la Operaci�n ?';
                //                    confirm (msn )

                //                }


                var txtTotalRecibido = document.getElementById('<%=txtTotalRecibido.ClientID%>');
                var TotalRecibido = parseFloat(UnFormat(txtTotalRecibido.value, ',', ''));

                var grillaCanl = document.getElementById('<%=GV_Cancelacion.ClientID%>');

                if (parseInt(parseInt(cboCondicionPago.value)) == 1) {   //**** CONTADO
                    if (grillaCanl != null) {

                        if (TotalRecibido < TotalAPagar) {
                            alert('El [ Total de Cancelaci�n ] es inferior al [ Total a Pagar ] del documento de Venta. No se permite la Operaci�n.');
                            return false;
                        }
                    }

                }
            } else {
                //************** CREDITO
                var grilla = document.getElementById('<%=GV_LineaCredito.ClientID%>');
                if (grilla == null) {
                    alert('El Cliente no posee L�nea de Cr�dito. No se permite la Operaci�n.');
                    return false;
                }
                var cboMedioPago_Credito = document.getElementById('<%=cboMedioPago_Credito.ClientID%>');
                if (isNaN(parseInt(cboMedioPago_Credito.value)) || cboMedioPago_Credito.value.length <= 0) {
                    alert('Debe seleccionar un medio de pago. No se permite la Operaci�n.');
                    return false;
                }

            }

            //***************** VALIDANDO LAS OPCIONES DE RECALCULAR
            var chb_SepararAfecto_NoAfecto = document.getElementById('<%=chb_SepararAfecto_NoAfecto.ClientID%>');
            var chb_SaltarCorrelativo = document.getElementById('<%=chb_SaltarCorrelativo.ClientID%>');
            var chb_SepararxMontos = document.getElementById('<%=chb_SepararxMontos.ClientID%>');

            var chb_SepararAfecto_NoAfecto = document.getElementById('<%=chb_SepararAfecto_NoAfecto.ClientID%>');

            if ((chb_SepararAfecto_NoAfecto.status || chb_SaltarCorrelativo.status || chb_SepararxMontos.status || chb_CompPercepcion.status || chb_SepararAfecto_NoAfecto.status) && (capaVistaPreviaDocumento.style.display == 'none')) {
                alert('Se tienen opciones de configuraci�n del Formulario activados, los cuales requieren que se pulse el bot�n [ Recalcular ].');
                return false;
            }


            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
            var mensaje = '';
            var tipoDocumento = getCampoxValorCombo(cboTipoDocumento, cboTipoDocumento.value);
            switch (parseInt(hddFrmModo.value)) {
                case 1: //******** NUEVO
                    mensaje = 'Usted est� registrando un Documento [ ' + tipoDocumento + ' ] por un valor de ' + monedaEmision + ' ' + txtTotalAPagar.value + '. Desea continuar con la Operaci�n ?';
                    break;
                case 2: //******** EDITAR
                    mensaje = 'Usted est� editando un Documento [ ' + tipoDocumento + ' ] por un valor de ' + monedaEmision + ' ' + txtTotalAPagar.value + '. Desea continuar con la Operaci�n ?';
                    break;
            }
            if (confirm(mensaje)) {
                document.getElementById('<%=btnGuardar.ClientID %>').style.visibility = 'hidden';
                return true;
            }
            else {
                document.getElementById('<%=btnGuardar.ClientID %>').style.visibility = 'visible';
                return false;
            }
        }
        // ****************

        function getMontoMedioPagoPrincipal() {
            var montoEfectivo = 0;
            var GV_Cancelacion = document.getElementById('<%=GV_Cancelacion.ClientID%>');
            var hddIdMedioPagoPrincipal = document.getElementById('<%=hddIdMedioPagoPrincipal.ClientID%>');
            var vuelto = parseFloat(document.getElementById('<%=txtVuelto.ClientID%>').value);
            if (isNaN(vuelto)) { vuelto = 0; }
            if (GV_Cancelacion != null) {
                for (var i = 1; i < GV_Cancelacion.rows.length; i++) {
                    var xrowElem = GV_Cancelacion.rows[i];

                    var IdMedioPago = parseInt(xrowElem.cells[1].children[2].value);

                    if (parseInt(hddIdMedioPagoPrincipal.value) == IdMedioPago) {

                        var xmonto = parseFloat(xrowElem.cells[1].children[1].innerHTML);
                        if (isNaN(xmonto)) { monto = 0; }

                        montoEfectivo = montoEfectivo + xmonto;
                    }
                }
                return montoEfectivo;
            }
        }


        //*******************
        function valOnChange_cboMoneda() {
            var grillaDetalle = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grillaDetalle != null) {
                alert('Debe quitar el detalle del Documento. No se permite la Operaci�n.');
                document.getElementById('<%=btnAgregar.ClientID%>').select();
                document.getElementById('<%=btnAgregar.ClientID%>').focus();
                return false;
            }
            var grillaConcepto = document.getElementById('<%=GV_Concepto.ClientID%>');
            if (grillaConcepto != null) {
                alert('Debe quitar el detalle por Conceptos Adicionales. No se permite la Operaci�n.');
                document.getElementById('<%=btnAgregar.ClientID%>').select();
                document.getElementById('<%=btnAgregar.ClientID%>').focus();
                return false;
            }
            return true;
        }



        //***********************************
        function valOnClickBuscar() {

//            var collPanel = $find("ctl00_ContentPlaceHolder1_CollapsiblePanelExtender1");
//            if (collPanel.get_Collapsed()) { collPanel.set_Collapsed(false); }

            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            } else {
                if (txtCodigoDocumento != null) {
                    txtCodigoDocumento.select();
                    txtCodigoDocumento.focus();
                }
            }
            return false;
        }












        function valOnKeyPressCodigoDoc(txtCodigoDocumento, e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;            
            if (key == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }

        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor v�lido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }

        function valOnClickEditar() {
//            var collPanel = $find("ctl00_ContentPlaceHolder1_CollapsiblePanelExtender1");
//            if (collPanel.get_Collapsed()) { collPanel.set_Collapsed(false); }

            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Edici�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite su edici�n.');
                return false;
            }
            var lblCaja = document.getElementById('<%=lblCaja.ClientID%>');
            if (lblCaja.innerHTML != 'Caja Aperturada') {
                alert('La caja no esta apertutada.\nNo se permite la operaci�n.');
                return false;
            }

            return true;
        }


        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para su Anulaci�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya est� ANULADO. No se permite su anulaci�n.');
                return false;
            }
            return confirm('Desea continuar con el Proceso de ANULACI�N del Documento ?');
        }

        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');

        }



        //qwe
        function calcularDatosCancelacion() {
            var totalAPagar = parseFloat(UnFormat(document.getElementById('<%=txtTotalAPagar.ClientID%>').value, ',', ''));
            var totalRecibido = 0;
            var PretotalRecibido = 0;
            var grilla = document.getElementById('<%=GV_Cancelacion.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    PretotalRecibido = PretotalRecibido + parseFloat(rowElem.cells[1].children[1].innerHTML);
                }
            }
            totalRecibido = PretotalRecibido.toPrecision(10);

            var faltante = 0;
            var vuelto = 0;
            if ((totalAPagar - totalRecibido) > 0) {
                faltante = Math.abs((totalAPagar - totalRecibido));
            } else {
                vuelto = Math.abs((totalAPagar - totalRecibido));
            }

            //********** Imprimimos valores
            var txtTotalRecibido = document.getElementById('<%=txtTotalRecibido.ClientID%>');
            if (txtTotalRecibido != null) {
                document.getElementById('<%=txtTotalRecibido.ClientID%>').value = Format(totalRecibido, 2);
            }

            var txtFaltante = document.getElementById('<%=txtFaltante.ClientID%>');
            if (txtFaltante != null) {
                document.getElementById('<%=txtFaltante.ClientID%>').value = Format(faltante, 2);

                var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
                if (parseInt(cboEstado.value) == 2) {
                    document.getElementById('<%=txtFaltante.ClientID%>').value = '0.00';
                }

                var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
                if (parseInt(hddFrmModo.value) == 4) {
                    var Percepcion = parseFloat(document.getElementById('<%=txtPercepcion.ClientID%>').value);
                    if (isNaN(Percepcion)) { Percepcion = 0; }
                    if (Percepcion > 0 && faltante > 0) {
                        var nuevoFaltante = 0;
                        nuevoFaltante = redondear(faltante, 2) - Percepcion;
                        document.getElementById('<%=txtFaltante.ClientID%>').value = Format(nuevoFaltante, 2);
                    }
                }
            }

            var txtVuelto = document.getElementById('<%=txtVuelto.ClientID%>');
            if (txtVuelto != null) {
                document.getElementById('<%=txtVuelto.ClientID%>').value = Format(vuelto, 2);
            }

            return false;
        }





        function valOnKeyPress_txtMonto_DatoCancelacion(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) { // Enter
                document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }


        function valOnClick_btnAddDatoCancelacion() {
            var totalAPagar = parseFloat(UnFormat(document.getElementById('<%=txtTotalAPagar.ClientID%>').value, ',', ''));
            var cboMoneda = document.getElementById('<%=cboMoneda_DatoCancelacion.ClientID%>');
            if (cboMoneda != null) {
                if (isNaN(parseInt(cboMoneda.value)) || parseInt(cboMoneda.value) <= 0 || cboMoneda.value.length <= 0) {
                    alert('Debe seleccionar una [ Moneda ].');
                    return false;
                }
            }
            var txtMonto = document.getElementById('<%=txtMonto_DatoCancelacion.ClientID%>');
            if (txtMonto != null) {

                if (isNaN(parseFloat(txtMonto.value)) || txtMonto.value.length <= 0) {
                    alert('Debe ingresar un valor v�lido.');
                    txtMonto.select();
                    txtMonto.focus();
                    return false;
                }
                if (totalAPagar > 0 && parseFloat(txtMonto.value) <= 0) {
                    alert('Debe ingresar un valor v�lido.');
                    return false;
                }

                if (parseFloat(txtMonto.value) < 0) {
                    alert('Debe ingresar un valor v�lido.');
                    return false;
                }
            }
            var cboPost = document.getElementById('<%=cboPost_DC.ClientID%>');
            if (cboPost != null) {
                if (isNaN(parseInt(cboPost.value)) || parseInt(cboPost.value) <= 0 || cboPost.value.length <= 0) {
                    alert('Debe seleccionar un [ Post ].');
                    return false;
                }
            }
            var cboTarjeta = document.getElementById('<%=cboTarjeta_DC.ClientID%>');
            if (cboTarjeta != null) {
                if (isNaN(parseInt(cboTarjeta.value)) || parseInt(cboTarjeta.value) <= 0 || cboTarjeta.value.length <= 0) {
                    alert('Debe seleccionar un [ Tipo de Tarjeta ].');
                    return false;
                }
            }
            var cboBanco = document.getElementById('<%=cboBanco.ClientID%>');
            if (cboBanco != null) {
                if (isNaN(parseInt(cboBanco.value)) || parseInt(cboBanco.value) <= 0 || cboBanco.value.length <= 0) {
                    alert('Debe seleccionar un [ Banco ].');
                    return false;
                }
            }
            var cboCuentaBancaria = document.getElementById('<%=cboCuentaBancaria.ClientID%>');
            if (cboCuentaBancaria != null) {
                if (isNaN(parseInt(cboCuentaBancaria.value)) || parseInt(cboCuentaBancaria.value) <= 0 || cboCuentaBancaria.value.length <= 0) {
                    alert('Debe seleccionar una [ Cuenta Bancaria ].');
                    return false;
                }
            }
            var txtNro = document.getElementById('<%=txtNro_DC.ClientID%>');
            if (txtNro != null) {
                if (txtNro.value.length <= 0) {
                    alert('Debe ingresar un valor v�lido.');
                    txtNro.select();
                    txtNro.focus();
                    return false;
                }
            }
            return true;
        }
        function valOnClick_btnVer_NotaCredito() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            return true;
        }
        function valFocusCancelacion() {
            if (document.getElementById('<%=btnAddDatoCancelacion.ClientID%>') != null) {
                document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            }
            return false;
        }

        function valOnClick_btnAceptarBuscarDocRefxCodigo() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('Ingrese un Nro. de Serie para la b�squeda.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('Ingrese un Nro. de Documento para la b�squeda.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }






        function onCapaVistaPrevia() {

            onCapa('capaVistaPreviaDocumento');

            var grilla = document.getElementById('<%=GV_VistaPreviaDoc.ClientID%>');
            var total = 0;
            var totalAPagar = 0;
            var percepcion = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    total = total + parseFloat(rowElem.cells[1].innerHTML);
                    percepcion = percepcion + parseFloat(rowElem.cells[2].innerHTML);
                    totalAPagar = totalAPagar + parseFloat(rowElem.cells[3].innerHTML);
                }
            }

            document.getElementById('<%=txtTotal_VistaPrevia.ClientID%>').value = redondear(total, 2);
            document.getElementById('<%=txtPercepcion_VistaPrevia.ClientID%>').value = redondear(percepcion, 2);
            document.getElementById('<%=txt_TotalAPagar_VistaPrevia.ClientID%>').value = redondear(totalAPagar, 2);

            //***************** Pintamos los valores en el formulario
            document.getElementById('<%=txtTotalAPagar.ClientID%>').value = redondear(totalAPagar, 2);
            document.getElementById('<%=txtPercepcion.ClientID%>').value = redondear(percepcion, 2);

            //*********************** Hallamos el vuelto
            calcularDatosCancelacion();

        }
        function valOnClick_btnRecalcular() {

            //************ VALIDAMOS DESCUENTOS / VOLUMEN VENTA MINIMO
            if (!(valPorcentMaximoDctoxArticulo_OnBlur(null))) {
                return false;
            }

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie de Documento.');
                return false;
            }

            var cboCaja = document.getElementById('<%=cboCaja.ClientID%>');
            if (isNaN(parseInt(cboCaja.value)) || cboCaja.value.length <= 0) {
                alert('Debe seleccionar una Caja.');
                return false;
            }

            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            if (isNaN(parseInt(cboMoneda.value)) || cboMoneda.value.length <= 0) {
                alert('Debe seleccionar una Moneda.');
                return false;
            }
            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }

            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var cboTipoDocumento = document.getElementById('<%=cboTipoDocumento.ClientID%>');
            if (isNaN(parseInt(cboTipoDocumento.value)) || cboTipoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Tipo de Documento.');
                return false;
            }

            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }

            var txtTotalAPagar = document.getElementById('<%=txtTotalAPagar.ClientID%>');
            var TotalAPagar = parseFloat(UnFormat(txtTotalAPagar.value, ',', ''));

            if (isNaN(TotalAPagar)) {
                alert('El Total a Pagar debe ser un valor mayor a cero.');
                return false;
            }


            var ImporteTotal = 0;
            if (document.getElementById('<%=txtImporteTotal.ClientID%>') == null) {
                ImporteTotal = 0;
            } else {
                ImporteTotal = parseFloat(UnFormat(document.getElementById('<%=txtImporteTotal.ClientID%>').value, ',', ''));
            }

            if (parseInt(cboTipoDocumento.value) == 3) { //****** BOLETA
                var montoMaxAfectoPercepcion = parseFloat(document.getElementById('<%=hddMontoMaxAfectoPercepcion.ClientID%>').value);
                if (isNaN(montoMaxAfectoPercepcion)) { montoMaxAfectoPercepcion = 0; }


                if (TotalAPagar <= montoMaxAfectoPercepcion && ImporteTotal <= montoMaxAfectoPercepcion) {
                    alert('No puede recalcular la boleta es menor a ' + montoMaxAfectoPercepcion + '.');
                    return false;
                }
            }

            var monedaEmision = getCampoxValorCombo(cboMoneda, cboMoneda.value);

            //************* Validando cantidades y precios
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                var cantidad = 0;
                var precioFinal = 0;
                var txtCantidad;
                var txtPrecioFinal;
                var porcentDctoMaximo = 0;

                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    cantidad = parseFloat(rowElem.cells[3].children[0].value);
                    txtCantidad = parseFloat(rowElem.cells[3].children[0]);
                    if (isNaN(cantidad)) { cantidad = 0; }
                    if (cantidad <= 0) {
                        alert('Ingrese una cantidad mayor a cero.');
                        txtCantidad.select();
                        txtCantidad.focus();
                        return false;
                    }
                    txtPrecioFinal = rowElem.cells[9].children[0];
                    precioFinal = parseFloat(txtPrecioFinal.value);
                    if (isNaN(precioFinal)) { precioFinal = 0; }

                    porcentDctoMaximo = parseFloat(rowElem.cells[11].children[1].value);

                    if (precioFinal <= 0 && porcentDctoMaximo < 100) {
                        alert('El Precio Final de Venta debe ser un valor mayor a cero.');
                        txtPrecioFinal.select();
                        txtPrecioFinal.focus();
                        return false;
                    }
                }
            }

            var grilla = document.getElementById('<%=GV_Concepto.ClientID%>');
            if (grilla != null) {
                var txtMonto;
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    txtMonto = rowElem.cells[2].children[1];
                    monto = parseFloat(txtMonto.value);
                    if (isNaN(monto)) { monto = 0; }
                    if (monto == 0) {
                        alert('El valor debe ser mayor a cero.');
                        txtMonto.select();
                        txtMonto.focus();
                        return false;
                    }
                }
            }
            return true;
        }





        function valOnKeyPress_txtNro_DC() {
            if (event.keyCode == 13) { //**************** Enter
                document.getElementById('<%=btnAddDatoCancelacion.ClientID%>').focus();
            }
            return true;
        }
        function valOnClick_cboEmpresa() {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                alert('Se ha agregado detalle al documento de venta. No se permite la Operaci�n.');
                document.getElementById('<%=btnAgregar.ClientID%>').focus();
                return false;
            }
            return true;
        }
        function valOnClickChb_CompPercepcion(chb_CompPercepcion) {
            if (chb_CompPercepcion.status) {
                alert('Se ha activado la Opci�n [ Comprometer Percepci�n ], el Sistema no tomar� en cuenta la Percepci�n, para ello deber� < Generar un Comprobante de Percepci�n >.');
            }
            return true;
        }
        function valOnClick_cboCondicionPago(cboCondicionPago) {
            var grilla = document.getElementById('<%=GV_Cancelacion.ClientID %>');
            if (grilla != null) {
                alert('Se han ingresado datos de Cancelaci�n. Debe quitar los datos de Cancelaci�n.');
                document.getElementById('<%=btnAgregar.ClientID %>').focus();
                return false;
            }
            return true;
        }
        function buscarMaestroObra() {
            document.getElementById('<%=hddBusquedaMaestro.ClientID %>').value = '1';
            return true;
        }
        function buscarCliente() {
            document.getElementById('<%=hddBusquedaMaestro.ClientID %>').value = '0';
            mostrarCapaPersona();
            return false;
        }

        function valOnClick_btnAceptar_MontoAdelanto_Nuevo() {
            var txtMonto = document.getElementById('<%=txtMonto_Adelanto_Nuevo.ClientID %>');
            if (isNaN(parseFloat(txtMonto.value)) || txtMonto.value.length <= 0 || parseFloat(txtMonto.value) <= 0) {
                alert('Ingrese un valor v�lido.');
                txtMonto.select();
                txtMonto.focus();
                return false;
            }
            return true;
        }

        function valOnClick_btnAceptar_DescuentoGlobal() {
            var txtMonto = document.getElementById('<%=txtMonto_DescuentoGlobal.ClientID %>');
            if (isNaN(parseFloat(txtMonto.value)) || txtMonto.value.length <= 0 || parseFloat(txtMonto.value) <= 0) {
                alert('Ingrese un valor v�lido.');
                txtMonto.select();
                txtMonto.focus();
                return false;
            }
            return true;
        }

        function valOnClick_btnAdelanto() {
            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID %>');
            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
                alert('Debe seleccionar un Cliente.');
                return false;
            }
            return true;
        }

        function valOnKeyPress_txtMonto_Adelanto_Nuevo(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key.keyCode == 13) { // Enter
                document.getElementById('<%=btnAceptar_MontoAdelanto_Nuevo.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
        } 
        function valOnKeyPress_txtMonto_DescuentoGlobal(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key.keyCode == 13) { // Enter
                document.getElementById('<%=btnAceptar_DescuentoGlobal.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
        }
        function valOnClick_chb_MoverAlmacen() {
            document.getElementById('<%=chb_ComprometerStock.ClientID%>').status = false;
            return true;
        }
        function valOnClick_chb_ComprometerStock() {
            document.getElementById('<%=chb_MoverAlmacen.ClientID%>').status = false;
            return true;
        }
        function valOnClick_btnEmitirGuiaRemision_Capa() {
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');

            switch (hddFrmModo.value) {
                case '5':  //**************** SAVE EXITO
                    var grilla = document.getElementById('<%=GV_ImpresionDoc_Cab.ClientID%>');
                    if (grilla != null) {
                        if (grilla.rows.length == 2) {//************ UN SOLO DOCUMENTO
                            var hddIdDocumento = grilla.rows[1].cells[6].children[1];
                            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                                alert('NO SE HA PODIDO DETERMINAR EL DOCUMENTO. PROBLEMAS EN LA OPERACI�N.');
                                return false;
                            } else {
                                window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=6&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                return false;
                            }
                        } else {

                            for (var i = 1; i < grilla.rows.length; i++) {
                                var rowElem = grilla.rows[i];
                                var chb_Select = rowElem.cells[6].children[0];
                                var hddIdDocumento = rowElem.cells[6].children[1];
                                if (chb_Select.status) {
                                    if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                                        alert('NO SE HA PODIDO DETERMINAR EL DOCUMENTO. PROBLEMAS EN LA OPERACI�N.');
                                        return false;
                                    } else {
                                        window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=6&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                        return false;
                                    }
                                    return false;
                                }
                            }
                            alert('DEBE SELECCIONAR UN DOCUMENTO PARA DESPACHO. NO SE PERMITE LA OPERACI�N.');
                            return false;
                        }
                    } else {
                        alert('NO SE TIENE UN DOCUMENTO PARA IMPRESI�N. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }
                    break;
                default:  //***************** EDITAR
                    var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
                    if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                        alert('NO SE TIENE UN DOCUMENTO PARA IMPRESI�N. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    } else {
                        window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=6&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                        return false;
                    }
                    break;
            }
            return false;
        }


        function valOnClick_btnEmitirGuiaRemision() {
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
            switch (hddFrmModo.value) {
                case '5':  //**************** SAVE EXITO
                    var grilla = document.getElementById('<%=GV_ImpresionDoc_Cab.ClientID%>');
                    if (grilla != null) {
                        if (grilla.rows.length == 2) {//************ UN SOLO DOCUMENTO
                            var hddIdDocumento = grilla.rows[1].cells[6].children[1];
                            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                                alert('NO SE HA PODIDO DETERMINAR EL DOCUMENTO. PROBLEMAS EN LA OPERACI�N.');
                                return false;
                            } else {
                                window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=6&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                return false;
                            }
                        } else {

                            onCapa('capaImpresionDocumento');
                            return false;

                        }
                    } else {
                        alert('NO SE TIENE UN DOCUMENTO PARA DESPACHO. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }
                    break;
                default:  //***************** EDITAR
                    var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
                    if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                        alert('NO SE TIENE UN DOCUMENTO PARA DESPACHO. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    } else {
                        window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=6&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                        return false;
                    }
                    break;
            }
            return false;
        }
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        function valOnClick_btnAceptar_AddMedioPago() {
            var txtUsuario = document.getElementById('<%=txtUsuario_AddMedioPago.ClientID %>');
            if (txtUsuario.value.length <= 0) {
                alert('CAMPO REQUERIDO. NO SE PERMITE LA OPERACI�N.');
                txtUsuario.select();
                txtUsuario.focus();
                return false;
            }
            var txtClave1 = document.getElementById('<%=txtClave_AddMedioPago.ClientID %>');
            if (txtClave1.value.length <= 0) {
                alert('CAMPO REQUERIDO. NO SE PERMITE LA OPERACI�N.');
                txtClave1.select();
                txtClave1.focus();
                return false;
            }
            var txtClave2 = document.getElementById('<%=txtClave2_AddMedioPago.ClientID %>');
            if (txtClave2.value.length <= 0) {
                alert('CAMPO REQUERIDO. NO SE PERMITE LA OPERACI�N.');
                txtClave2.select();
                txtClave2.focus();
                return false;
            }
            return true;
        }
        function valOnClick_btnGenerarCompPercepcion_Capa() {
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');

            switch (hddFrmModo.value) {
                case '5':  //**************** SAVE EXITO
                    var grilla = document.getElementById('<%=GV_ImpresionDoc_Cab.ClientID%>');
                    if (grilla != null) {
                        if (grilla.rows.length == 2) {//************ UN SOLO DOCUMENTO
                            var hddIdDocumento = grilla.rows[1].cells[6].children[0].cells[1].children[0];
                            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                                alert('NO SE HA PODIDO DETERMINAR EL DOCUMENTO. PROBLEMAS EN LA OPERACI�N.');
                                return false;
                            } else {
                                window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=30&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                return false;
                            }
                        } else {

                            for (var i = 1; i < grilla.rows.length; i++) {
                                var rowElem = grilla.rows[i];
                                var chb_Select = rowElem.cells[6].children[0].cells[0].children[0];
                                var hddIdDocumento = rowElem.cells[6].children[0].cells[1].children[0];
                                if (chb_Select.status) {
                                    if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                                        alert('NO SE HA PODIDO DETERMINAR EL DOCUMENTO. PROBLEMAS EN LA OPERACI�N.');
                                        return false;
                                    } else {
                                        window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=30&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                        return false;
                                    }
                                    return false;
                                }
                            }
                            alert('DEBE SELECCIONAR UN DOCUMENTO. NO SE PERMITE LA OPERACI�N.');
                            return false;
                        }
                    } else {
                        alert('NO SE TIENE UN DOCUMENTO. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }
                    break;
                default:  //***************** EDITAR
                    var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
                    if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                        alert('NO SE TIENE UN DOCUMENTO. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    } else {
                        window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=30&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                        return false;
                    }
                    break;
            }
            return false;
        }
        function verCapaPermiso_AddMedioPago() {
            onCapa('capaPermiso_AddMedioPago');
            document.getElementById('<%=txtUsuario_AddMedioPago.ClientID%>').select();
            document.getElementById('<%=txtUsuario_AddMedioPago.ClientID%>').focus();
            return false;
        }

        function valOnClick_btnGenerarComprobantePercepcion() {
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
            switch (hddFrmModo.value) {
                case '5':  //**************** SAVE EXITO
                    var grilla = document.getElementById('<%=GV_ImpresionDoc_Cab.ClientID%>');
                    if (grilla != null) {
                        if (grilla.rows.length == 2) {//************ UN SOLO DOCUMENTO
                            var hddIdDocumento = grilla.rows[1].cells[6].children[1];
                            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                                alert('NO SE HA PODIDO DETERMINAR EL DOCUMENTO. PROBLEMAS EN LA OPERACI�N.');
                                return false;
                            } else {
                                window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=30&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                return false;
                            }
                        } else {

                            onCapa('capaImpresionDocumento');
                            return false;

                        }
                    } else {
                        alert('NO SE TIENE UN DOCUMENTO. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }
                    break;
                default:  //***************** EDITAR
                    var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
                    if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                        alert('NO SE TIENE UN DOCUMENTO. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    } else {
                        window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=30&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                        return false;
                    }
                    break;
            }
            return false;
        }


        //**********************************    CALCULO DE EQUIVALENCIA PRODUCTO
        function valOnClick_btnEquivalencia_PR(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];
                    if (boton.id == rowElem.cells[11].children[8].id) {
                        //onCapa('capaEquivalenciaProducto');                                                
                        document.getElementById('<%=hddIndex_GV_Detalle.ClientID%>').value = (i - 1);
                        //document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').select();
                        //document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').focus();
                        return true;
                    }
                }
            }
            alert('Problemas en la ubicaci�n de la Fila.');
            return false;
        }
        function mostrarCapaEquivalencia_PR() {
            onCapa('capaEquivalenciaProducto');
            //document.getElementById('<%=hddIndex_GV_Detalle.ClientID%>').value = (i - 1);
            document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').select();
            document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').focus();
            return false;
        }
        function valOnClick_btnCalcular() {
            var cboUnidadMedida_Ingreso = document.getElementById('<%=cboUnidadMedida_Ingreso.ClientID %>');
            if (isNaN(parseFloat(cboUnidadMedida_Ingreso.value)) || cboUnidadMedida_Ingreso.value.length <= 0) {
                alert('DEBE SELECCIONAR UNA UNIDAD DE MEDIDA DE INGRESO.');
                return false;
            }
            var cboUnidadMedida_Salida = document.getElementById('<%=cboUnidadMedida_Salida.ClientID %>');
            if (isNaN(parseFloat(cboUnidadMedida_Salida.value)) || cboUnidadMedida_Salida.value.length <= 0) {
                alert('DEBE SELECCIONAR UNA UNIDAD DE MEDIDA DE SALIDA.');
                return false;
            }
            var txtCantidad = document.getElementById('<%=txtCantidad_Ingreso.ClientID %>');
            if (isNaN(parseFloat(txtCantidad.value)) || txtCantidad.value.length <= 0) {
                alert('INGRESE UN VALOR V�LIDO.');
                txtCantidad.select();
                txtCantidad.focus();
                return false;
            }
            return true;
        }
        function valOnClick_btnAceptar_EquivalenciaPR() {
            var grilla = document.getElementById('<%=GV_ResuldoEQ.ClientID %>');
            if (grilla == null) {
                alert('NO SE HA REALIZADO EL C�LCULO RESPECTIVO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            return true;
        }
        //**********************************    FIN  CALCULO DE EQUIVALENCIA PRODUCTO

        function mostrarCapaAnticipo() {
            onCapa('capaAdelanto');
            var cboAnticipo = document.getElementById('<%=cboTipoAdelanto.ClientID %>');
            //cboAnticipo.select();
            cboAnticipo.focus();
            return false;
        }

        function mostrarCapaAnticipo2() {
            onCapa('capaAdelanto');
            var cboAnticipo = document.getElementById('<%=cboTipoAdelanto.ClientID %>');
            return false;
        }



        function valOnClick_btnRetazoArea_Aceptar() {

            var txtArea = document.getElementById('<%= txtRetazoArea_Ancho.ClientID%>');
            var txtLargo = document.getElementById('<%= txtRetazoArea_Largo.ClientID%>');
            var txtCantidad = document.getElementById('<%= txtRetazoArea_Cantidad.ClientID%>');

            if (isNaN(parseFloat(txtArea.value)) || txtArea.value.length <= 0) {
                alert('Ingrese un valor v�lido.');
                txtArea.select();
                txtArea.focus();
                return false;
            }
            if (isNaN(parseFloat(txtLargo.value)) || txtLargo.value.length <= 0) {
                alert('Ingrese un valor v�lido.');
                txtLargo.select();
                txtLargo.focus();
                return false;
            }
            if (isNaN(parseFloat(txtCantidad.value)) || txtCantidad.value.length <= 0) {
                alert('Ingrese un valor v�lido.');
                txtCantidad.select();
                txtCantidad.focus();
                return false;
            }

            return true;
        }

        function valOnClick_btnRetazoLongitud_Aceptar() {

            var txtLargo = document.getElementById('<%= txtRetazoLongitud_Largo.ClientID%>');
            var txtCantidad = document.getElementById('<%= txtRetazoLongitud_Cantidad.ClientID%>');

            if (isNaN(parseFloat(txtLargo.value)) || txtLargo.value.length <= 0) {
                alert('Ingrese un valor v�lido.');
                txtLargo.select();
                txtLargo.focus();
                return false;
            }

            if (isNaN(parseFloat(txtCantidad.value)) || txtCantidad.value.length <= 0) {
                alert('Ingrese un valor v�lido.');
                txtCantidad.select();
                txtCantidad.focus();
                return false;
            }

            return true;
        }

        function valOnClick_btnImprimir(tipoimpresion) {

            var grilla = document.getElementById('<%=GV_ImpresionDoc_Cab.ClientID %>');
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>');
            var cboTipoDocumento = document.getElementById('<%=cboTipoDocumento.ClientID %>');
            var IdDocumento = 0;
            if (grilla != null) {
                if (grilla.rows.length == 2) { //************* SE HA GENERADO SOLO UN DOCUMENTO
                    IdDocumento = parseInt(grilla.rows[1].cells[6].children[1].value);
                } else {
                    onCapa('capaImpresionDocumento');
                    return false;
                }
            } else {
                if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                    alert('NO SE TIENE UN DOCUMENTO PARA IMPRESI�N. NO SE PERMITE LA OPERACI�N.');
                    return false;
                } else {
                    IdDocumento = parseInt(hddIdDocumento.value);
                }
            }
            if (parseInt(cboTipoDocumento.value) == 1) {
                //window.open('../../DocumentosCliente/FrmVisor_DocumentosCliente.aspx?iReporte=1&IdDocumento=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=1&IdDocumento=' + IdDocumento + '&tipoimpresion=' + tipoimpresion, 'Factura', 'resizable=yes,width=1000,height=800,scrollbars=1', null);

            } else if (parseInt(cboTipoDocumento.value) == 3) {
                //window.open('../../DocumentosCliente/FrmVisor_DocumentosCliente.aspx?iReporte=2&IdDocumento=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=3&IdDocumento=' + IdDocumento + '&tipoimpresion=' + tipoimpresion, 'Boleta', 'resizable=yes,width=1000,height=800,scrollbars=1', null);

            } else if (parseInt(cboTipoDocumento.value) == 4) {
                //window.open('../../DocumentosCliente/FrmVisor_DocumentosCliente.aspx?iReporte=2&IdDocumento=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=4&IdDocumento=' + IdDocumento + '&tipoimpresion=' + tipoimpresion, 'Factura Electr�nica', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            }
            return false;
        }

        function valOnClick_btnImprimir_Capa(boton, tipoImp) {
            var IdDocumento = 0;
            var grilla = document.getElementById('<%=GV_ImpresionDoc_Cab.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];
                    if (boton.id == rowElem.cells[6].children[0].id || boton.id == rowElem.cells[7].children[0].id || boton.id == rowElem.cells[8].children[0].id) {
                        IdDocumento = parseInt(rowElem.cells[6].children[1].value);
                        break;
                    }
                }

                var cboTipoDocumento = document.getElementById('<%=cboTipoDocumento.ClientID %>');

                if (parseInt(cboTipoDocumento.value) == 1) {
                    //window.open('../../DocumentosCliente/FrmVisor_DocumentosCliente.aspx?iReporte=1&IdDocumento=' + IdDocumento, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                    window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=1&IdDocumento=' + IdDocumento + '&tipoimpresion=' + tipoImp, 'Factura', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                } else if (parseInt(cboTipoDocumento.value) == 3) {
                    //window.open('../../DocumentosCliente/FrmVisor_DocumentosCliente.aspx?iReporte=2&IdDocumento=' + IdDocumento, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                    window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=3&IdDocumento=' + IdDocumento + '&tipoimpresion=' + tipoImp, 'Boleta', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                }

            }
            return false;
        }

        function activarPanel_V1() {
            var combo = document.getElementById('<%=cboTipoPersona_RegistrarCliente.ClientID %>');
            var flag = false;
            switch (combo.value) {
                case 'N':
                    flag = false;
                    break;
                case 'J':
                    flag = true;
                    break;
            }
            //controles de Per. Natural

            document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtApMaterno_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtNombres_Cliente.ClientID%>').disabled = flag;
            document.getElementById('<%= txtDNI_Cliente.ClientID%>').disabled = flag;

            //controles de Per. Juridica
            document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').disabled = !(flag);
            switch (combo.value) {
                case 'N':
                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').select();
                    document.getElementById('<%= txtApPaterno_Cliente.ClientID%>').focus();
                    break;
                case 'J':
                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').select();
                    document.getElementById('<%= txtRazonSocial_Cliente.ClientID%>').focus();
                    break;
            }
            return true;
        }
        function valOnClick_btnEnviarSolicitudAprobac�on() {
            return confirm('Desea continuar con el Registro de la Solicitud de Aprobaci�n ?');
        }
        function valOnKeyUp_txtValorReferencial() {

            var cboTipoDocumento = document.getElementById('<%=cboTipoDocumento.ClientID%>');
            //**************** FACTURA DE TRANSPORTE
            if (parseInt(cboTipoDocumento.value) == 59) {

                var txtValorReferencial = document.getElementById('<%=txtValorReferencial.ClientID%>');
                var txtValorReferencial_Total = document.getElementById('<%=txtValorReferencial_Total.ClientID%>');
                var txtCantidad_TR = document.getElementById('<%=txtCantidad_Transporte.ClientID%>');

                var valorReferencial = parseFloat(txtValorReferencial.value);
                var cantidad = parseFloat(txtCantidad_TR.value);
                if (isNaN(parseFloat(txtValorReferencial.value)) || txtValorReferencial.value.length <= 0) { valorReferencial = 0; }
                if (isNaN(parseFloat(txtCantidad_TR.value)) || txtCantidad_TR.value.length <= 0) { cantidad = 0; }

                var valorReferencial_Total = cantidad * valorReferencial;
                txtValorReferencial_Total.value = redondear(valorReferencial_Total, 3);

            }

            return false;
        }
        function valOnClick_btnLongitud(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID %>');

            var hddIdUnidadMedida_RetazoLongitud = document.getElementById('<%=hddIdUnidadMedida_RetazoLongitud.ClientID %>');
            var IdUnidadMedida_RetazoLongitud = parseInt(hddIdUnidadMedida_RetazoLongitud.value);

            if (grilla != null) {
                var flag = false;
                var indexGrilla = -1;
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[3].children[2].id == boton.id) {

                        //***************
                        var cboUM = rowElem.cells[3].children[1];

                        for (var x = 0; x < cboUM.length; x++) {
                            if (parseInt(cboUM[x].value) == IdUnidadMedida_RetazoLongitud) {  //***** LONGITUD (METROS)
                                flag = true;
                                indexGrilla = (i - 1);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            if (flag == false || indexGrilla < 0) {
                alert('EL PRODUCTO SELECCIONADO NO CUENTA CON LA UNIDAD DE MEDIDA RETAZO - LONGITUD. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            onCapa('capaRetazo_Longitud');
            document.getElementById('<%= txtRetazoLongitud_Largo.ClientID%>').value = '';
            document.getElementById('<%= txtRetazoLongitud_Cantidad.ClientID%>').value = '';
            document.getElementById('<%= txtRetazoLongitud_Largo.ClientID%>').select();
            document.getElementById('<%= txtRetazoLongitud_Largo.ClientID%>').focus();
            document.getElementById('<%= hddIndex_GV_Detalle.ClientID%>').value = (indexGrilla);
            return false;
        }
        //funcion vehiculo
        function valOnClick_btnBuscarVehiculo() {
            onCapa('capaVehiculo');
            document.getElementById('<%=txtNroPlaca_BuscarVehiculo.ClientID%>').select();
            document.getElementById('<%=txtNroPlaca_BuscarVehiculo.ClientID%>').focus();
            return false;
        }
        function valOnClick_btnArea(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID %>');

            var hddIdUnidadMedida_RetazoArea = document.getElementById('<%=hddIdUnidadMedida_RetazoArea.ClientID %>');
            var IdUnidadMedida_RetazoArea = parseInt(hddIdUnidadMedida_RetazoArea.value);

            if (grilla != null) {
                var flag = false;
                var indexGrilla = -1;
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[3].children[3].id == boton.id) {

                        //***************
                        var cboUM = rowElem.cells[3].children[1];

                        for (var x = 0; x < cboUM.length; x++) {
                            if (parseInt(cboUM[x].value) == IdUnidadMedida_RetazoArea) {
                                flag = true;
                                indexGrilla = (i - 1);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            if (flag == false || indexGrilla < 0) {
                alert('EL PRODUCTO SELECCIONADO NO CUENTA CON LA UNIDAD DE MEDIDA RETAZO - LONGITUD. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            onCapa('capaRetazo_Area');
            document.getElementById('<%= txtRetazoArea_Ancho.ClientID%>').value = '';
            document.getElementById('<%= txtRetazoArea_Largo.ClientID%>').value = '';
            document.getElementById('<%= txtRetazoArea_Cantidad.ClientID%>').value = '';
            document.getElementById('<%= txtRetazoArea_Ancho.ClientID%>').select();
            document.getElementById('<%= txtRetazoArea_Ancho.ClientID%>').focus();
            document.getElementById('<%= hddIndex_GV_Detalle.ClientID%>').value = (indexGrilla);

            return false;
        }
        //
        function LimpiarMaestroObra() {
            document.getElementById('<%=txtMaestro.ClientID %>').value = '';
            document.getElementById('<%=txtDni_Maestro.ClientID %>').value = '';
            document.getElementById('<%=txtMaestro.ClientID %>').value = '';
            document.getElementById('<%=hddIdMaestroObra.ClientID %>').value = '';
            return false;
        }
        

    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        var ejecucion = document.getElementById('<%=hddCodBarras.ClientID %>').value;
        /////////////////////////////////////////

        function val_CodBarras(valor) {
            ejecucion = valor;
            var codBarras = document.getElementById('<%=txtCodBarrasPrincipal.ClientID %>');
            if (codBarras != null) {
                if (codBarras.value.length > 0) {
                    if (ejecucion == '1') {
                        document.getElementById('<%=btnAceptarCodBarras.ClientID %>').click();
                        return false;
                    }
                }
            }
            setTimeout('val_CodBarras(ejecucion);', 0.00000);

        }


        function read_Lista(obj) {
            var cantidad = 0.00;
            var grillaListaProducto = document.getElementById('<%=DGV_AddProd.ClientID %>');
            var grillaProducto = document.getElementById('<%=GV_Detalle.ClientID %>');

            for (var i = 1; i < grillaListaProducto.rows.length; i++) {
                var rowListaProducto = grillaListaProducto.rows[i];

                if (rowListaProducto.cells[0].children[0].id == obj.id) {

                    //
                    if (grillaProducto != null) {
                        for (var k = 1; k < grillaProducto.rows.length; k++) {
                            var rowProducto = grillaProducto.rows[k];
                            if (rowProducto.cells[11].children[6].value == rowListaProducto.cells[9].children[2].value) {
                                var cantidad = parseFloat(rowProducto.cells[3].children[0].value);
                                if (isNaN(cantidad)) {
                                    obj.value = "0.00";
                                    obj.select();
                                    return false;
                                }
                            }
                        }
                    }
                    //

                }
            }

            if (parseFloat(cantidad) == 0) {
                obj.value = parseFloat(cantidad);
                obj.select();
            } else {
                obj.value = parseFloat(cantidad);
                obj.select();
            }
            return false;
        }

        function onFocus_Readonly(caja) {
            caja.readOnly = true;
            return false;
        }

        function capaDetalle_onmouseover() {
            var onlyCancelacion = parseInt(document.getElementById('<%=hddPermiso_OnlyPanel_Cancelacion.ClientID %>').value);
            if (onlyCancelacion == 0) {
                enableFalse();
            }
        }

        function capaDetalle_onmouseout() {
            enableTrue();
        }

        function enableFalse() {
            var controls = document.getElementById("<%=Panel_Detalle.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < controls.length; i++) {
                if (controls[i].id != 'ctl00_ContentPlaceHolder1_btnLimpiarDetalleDocumento') {
                    controls[i].disabled = true;
                }
            }
        }
        function enableTrue() {
            var controls = document.getElementById("<%=Panel_Detalle.ClientID%>").getElementsByTagName("input");
            for (var i = 0; i < controls.length; i++) {
                controls[i].disabled = false;
            }
        }

        function CalcularConsultarStockPrecioxProd() {


            var GV_ConsultarStockPrecioxProd = document.getElementById('<%=GV_ConsultarStockPrecioxProd.ClientID %>');
            var StockTotal = 0;
            var StockTransitoTotal = 0;

            if (GV_ConsultarStockPrecioxProd != null) {
                for (var i = 1; i < GV_ConsultarStockPrecioxProd.rows.length; i++) {
                    var rowElem = GV_ConsultarStockPrecioxProd.rows[i];

                    var Stock = parseFloat(rowElem.cells[3].children[0].innerHTML);
                    if (isNaN(Stock)) { Stock = 0; }

                    StockTotal = StockTotal + Stock;

                    var StockTransito = parseFloat(rowElem.cells[4].children[0].innerHTML);
                    if (isNaN(StockTransito)) { StockTransito = 0; }

                    StockTransitoTotal = StockTransitoTotal + StockTransito;

                } //end for
            } //end if
            document.getElementById('<%=txtStockTotal.ClientID %>').value = redondear(StockTotal, 4)
            document.getElementById('<%=txtStockTransitoTotal.ClientID %>').value = redondear(StockTransitoTotal, 4)
            return false;
        }
        //
        function onCLick_EmmitirPercepcion() {

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');

            switch (hddFrmModo.value) {
                case '5':  //**************** SAVE EXITO
                    var grilla = document.getElementById('<%=GV_ImpresionDoc_Cab.ClientID%>');
                    if (grilla != null) {
                        if (grilla.rows.length == 2) {//************ UN SOLO DOCUMENTO
                            var hddIdDocumento = grilla.rows[1].cells[6].children[1];
                            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                                alert('NO SE HA PODIDO DETERMINAR EL DOCUMENTO. PROBLEMAS EN LA OPERACI�N.');
                                return false;
                            } else {
                                window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=30&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                return false;
                            }
                        } else {

                            for (var i = 1; i < grilla.rows.length; i++) {
                                var rowElem = grilla.rows[i];
                                var chb_Select = rowElem.cells[6].children[0];
                                var hddIdDocumento = rowElem.cells[6].children[1];
                                if (chb_Select.status) {
                                    if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                                        alert('NO SE HA PODIDO DETERMINAR EL DOCUMENTO. PROBLEMAS EN LA OPERACI�N.');
                                        return false;
                                    } else {
                                        window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=30&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                                        return false;
                                    }
                                    return false;
                                }
                            }
                            alert('DEBE SELECCIONAR UN DOCUMENTO PARA DESPACHO. NO SE PERMITE LA OPERACI�N.');
                            return false;
                        }
                    } else {
                        alert('NO SE TIENE UN DOCUMENTO PARA IMPRESI�N. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }
                    break;
                default:  //***************** EDITAR
                    var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
                    if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0 || parseInt(hddIdDocumento.value) <= 0) {
                        alert('NO SE TIENE UN DOCUMENTO PARA IMPRESI�N. NO SE PERMITE LA OPERACI�N.');
                        return false;
                    } else {
                        window.open('FrmDocumento_Load.aspx?IdTipoDocumentoLoad=30&IdDocumentoRef=' + hddIdDocumento.value, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
                        return false;
                    }
                    break;
            }
            return false;

        }

        //

        function validarNrodias() {

//            var collPanel = $find("ctl00_ContentPlaceHolder1_CollapsiblePanelExtender1");
//            if (collPanel.get_Collapsed()) { collPanel.set_Collapsed(false); }

            var txtFechaEmision = document.getElementById('<%=txtFechaEmision.ClientID %>');
            var day = parseInt(document.getElementById('<%=txtnrodias.ClientID %>').value);
            if (isNaN(day)) { day = 0; }


            var fecha = new Date(converToDate(txtFechaEmision.value));
            fecha.setDate(fecha.getDate() + day);

            document.getElementById('<%=txtFechaVcto.ClientID %>').value = (fecha.getDate() < 10 ? "0" + fecha.getDate().toString() : fecha.getDate().toString()) + "/" + (fecha.getMonth() + 1 < 10 ? "0" + (fecha.getMonth() + 1).toString() : (fecha.getMonth() + 1).toString()) + "/" + fecha.getFullYear().toString();


            return true;
        }

        function BuscarDocumentoRef() {

            var lblCaja = document.getElementById('<%=lblCaja.ClientID%>');            
            if (lblCaja.innerHTML != 'Caja Aperturada') {
                alert('La caja no esta apertutada.\nNo se permite la operaci�n.');
                return false;
            }
            onCapa('capaDocumentosReferencia')
            return false;
        }

        function valSave_DescuentoGlobal() {

            var PorcentDescuentoGlobal = parseFloat(document.getElementById('<%=txtDescuentoGlobal.ClientID%>').value);
            if (isNaN(PorcentDescuentoGlobal)) { PorcentDescuentoGlobal = 0; }

            if (PorcentDescuentoGlobal > 0) {

                var val_DescuentoGlobal = get_TotalConcepto_DescuentoGlobal();

                var ImporteTotal = parseFloat(UnFormat(document.getElementById('<%=txtImporteTotal.ClientID%>').value, ',', ''));
                var ConceptoTotal = get_TotalConcepto_SinDescuentoGlobal();
                var TotalDocumento = ImporteTotal + ConceptoTotal;

                var DescuentoGlobal = (TotalDocumento - (TotalDocumento * (1.0 - PorcentDescuentoGlobal / 100.0))) * -1;

                if (redondear(val_DescuentoGlobal, 2) != redondear(DescuentoGlobal, 2)) {
                    alert('RECALCULE EL DESCUENTO GLOBAL AL DOCUMENTO. NO SE PERMITE LA OPERACION.');
                    return false;
                }

            }
            return true;


        }

        function get_TotalConcepto_SinDescuentoGlobal() {

            var GV_Concepto = document.getElementById('<%=GV_Concepto.ClientID%>');
            var TotalConcepto = 0;

            if (GV_Concepto != null) {
                var txtMontoConcepto;
                var montoConcepto = 0;
                for (var i = 1; i < GV_Concepto.rows.length; i++) {
                    var rowElem = GV_Concepto.rows[i];
                    txtMontoConcepto = rowElem.cells[2].children[1];
                    montoConcepto = parseFloat(txtMontoConcepto.value);
                    if (isNaN(montoConcepto)) { montoConcepto = 0; }
                    if (montoConcepto > 0 && rowElem.cells[1].children[0].value != 10) {
                        TotalConcepto = TotalConcepto + montoConcepto;
                    }
                }
            }
            return TotalConcepto;
        }


        function get_TotalConcepto_DescuentoGlobal() {

            var GV_Concepto = document.getElementById('<%=GV_Concepto.ClientID%>');
            var TotalConcepto = 0;

            if (GV_Concepto != null) {
                var txtMontoConcepto;
                var montoConcepto = 0;
                for (var i = 1; i < GV_Concepto.rows.length; i++) {
                    var rowElem = GV_Concepto.rows[i];
                    txtMontoConcepto = rowElem.cells[2].children[1];
                    montoConcepto = parseFloat(txtMontoConcepto.value);
                    if (isNaN(montoConcepto)) { montoConcepto = 0; }
                    if (montoConcepto != 0 && rowElem.cells[1].children[0].value == 10) {
                        TotalConcepto = TotalConcepto + montoConcepto;
                    }
                }
            }
            return TotalConcepto;
        }


        function Aceptar_MaestroObra() {
            var rbPregunta = document.getElementById('<%=rbPregunta.ClientID %>');
            var ctrlPregunta = rbPregunta.getElementsByTagName('input');
            var IdPregunta = -1;
            for (var i = 0; i < ctrlPregunta.length; i++) {
                if (ctrlPregunta[i].checked == true) {
                    IdPregunta = ctrlPregunta[i].value;                    
                }
            }

            if (IdPregunta == -1) {
                alert('Debe seleccionar un opci�n. No se permite la operaci�n.');
                return false;
            }
            var ventana = confirm('Desea continuar con la operaci�n.');

            if (ventana) {
                return true;
            } else {
                document.getElementById('<%=btnClose_Pregunta.ClientID %>').click();
                return false;
            }

        }

        function val_facele() {
            var comboSerie = document.getElementById('<%= cboSerie.ClientID %>');
            var txtCodigo = document.getElementById('<%= txtCodigoDocumento.ClientID %>');
            var comboTipoDocumento = document.getElementById('<%= cboTipoDocumento.ClientID %>');

            var idSerie = comboSerie.value;
            var idTipoDocumento = comboTipoDocumento.value;
            var codigo = txtCodigo.value;

            window.open('../../caja/reportes/frmImprimirFacElectronica.aspx?idSerie=' + idSerie + '&codigo=' + codigo + '&idtipoDocumento=' + idTipoDocumento + '', 'popup', 'width=800,height=500');
            return false;
        }


        
    </script>

</asp:Content>
