﻿function crearcomboTienda(cadenaData) {
    //.split("*")[0]
    var divTienda = document.getElementById("divTienda");
    var divEmpresa = document.getElementById("divEmpresa");
    var divAlmacen = document.getElementById("divAlmacen");
    cadenaData = cadenaData.split("*")
    for (var h = 0; h < cadenaData.length; h++) {
        var filas = cadenaData[h].split(";");
        var nroFilas = filas.length;
        var contenido = "";
        var columnas = filas[0].split("|");
        var nroColumnas = columnas.length;
        var item;
        if (h == 0) { contenido += '<select id="cboTienda">'; }
        if (h == 1) { contenido += '<select id="cboEmpresa">'; }
        if (h == 2) { contenido += '<select id="cboAlmacen">'; }

        for (var i = 0; i < nroFilas; i++) {
            item = filas[i].split("|");
            for (var j = 0; j < nroColumnas; j++) {
                if (i == 0) {
                    contenido += '<option selected="selected" value=';
                } else {
                    contenido += '<option value=';
                }

                contenido += '"' + item[j] + '"';
                j++;
                contenido += '>'
                contenido += item[j];
                contenido += "</option>";
            }
        }
        contenido += "</select>";
        if (h == 0) { divTienda.innerHTML = contenido; }
        if (h == 1) { divEmpresa.innerHTML = contenido; }
        if (h == 2) { divAlmacen.innerHTML = contenido; }
    }

}
//    var xhr = new XMLHttpRequest();
//    var idUsuario = <%=session("idUsuario") %>;
//    xhr.open("get", "frmReponerStockGeneral.aspx?idUsuario=" + idUsuario, true);
//    xhr.onreadystatechange = function () {
//            if (xhr.readyState == 4 && xhr.status == 200) {
//                fun_lista(xhr.responseText);
//            }
//        }
//        xhr.send();
//        return false;

function esperarMientrasBusca() {
    var espera = document.getElementById("divEspera");
    espera.style.display = "block";
}

function enviarDatosCoti(nombreGrupo) {
    var grilla = document.getElementById("tabReposicion");

    var cb = [];
    var n = 0, cuales = "";
    var cadena1 = "";
    var cadena2 = "";
    cb = document.getElementsByName(nombreGrupo);
    //alert('El total de checkbox de name = ' + nombreGrupo + ' es: ' + cb.length);
    for (var i = 0; i < cb.length; i++) {
        var e = parseInt(i);

        if (cb[i].checked == true) {
            //                if (n < 25) 
            //                    cadena1 += cb[i].value + '|' + grilla.rows[e + 1].cells[1].children[0].value + '▼';
            //                }
            cadena2 += cb[i].value + '|' + grilla.rows[e + 2].cells[1].children[0].value + '▼';

            //alert('Valor del checkbox ' + (e + 1) + ': ' + cb[i].value + ' y su cantidad es: ' + grilla.rows[e + 1].cells[1].children[0].value);
            cuales += cb[i].value + ' ';
            n++;
        }
    } // fin loop
    if (n == 0) {
        var mensaje = ' no hubo selección';
        alert(mensaje);
        return false;
    }
    //        alert(cadena1.length);
    //        alert(cadena2.length);
    var data = new FormData();
    data.append('flag', 'C');
    data.append('datos2', cadena2.substr(0, cadena2.length - 1));

    var xhr = new XMLHttpRequest();
    //var nroCaracteres = cadena.length;
    //cadena = cadena.substr(0, parseInt(nroCaracteres) - 1).toString();
    //xhr.open('GET', "frmReponerStockGeneral.aspx?flag=C&datos2=" + cadena2.substr(0, cadena2.length - 1), true);
    xhr.open('POST', "frmReponerStockGeneral.aspx", true);
    //var parametros = "flag=C&datos2 =" + cadena2.substr(0, cadena2.length - 1);
    //xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            alert(xhr.responseText);
        }
    }
    xhr.send(data);
    return false;
    //alert(cadena);
    //        if (n == 0) {
    //            var mensaje = ' no hubo selección';
    //        } else {
    //            var xhr = new XMLHttpRequest();
    //            xhr.open("get", "frmReponerStockGeneral.aspx?flag=C&datos=" + cadena, true);
    //            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    //            xhr.onreadystatechange = function () {
    //                if (xhr.readyState == 4 && xhr.status == 200) {
    //                    alert(xhr.responseText);
    //                }
    //            }
    //            xhr.send()
    //            return false;
    //        }
    //        } else {
    //            var mensaje = 'Se marcaron el/los checkbox de valor: ' + cuales
    //        }
    //alert('El total de checkbox marcados es: ' + n);
    //alert(mensaje);
    //document.getElementById('boxes').reset();
    //return false;
}
function enviarDatos(objeto) {
    var espera = document.getElementById("divEspera");
    var txtFechaInicio = document.getElementById("<%=txtFecInicio.ClientID %>").value;
    var txtFechaFin = document.getElementById("<%=txtFecFin.ClientID %>").value;
    var tienda = document.getElementById("cboTienda");
    var idTienda = tienda.options[tienda.selectedIndex].value;

    var empresa = document.getElementById("cboEmpresa");
    var idEmpresa = empresa.options[empresa.selectedIndex].value;

    var div = document.getElementById("datos");

    var xhr = new XMLHttpRequest();
    xhr.open("get", "frmReponerStockGeneral.aspx?flag=G&idTienda=" + idTienda + "&idEmpresa=" + idEmpresa + "&txtInicio=" + txtFechaInicio + "&txtFin=" + txtFechaFin, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onloadstart = function () { espera.style.display = "block"; }
    xhr.onloadend = function () { espera.style.display = "none"; }
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            fun_lista(xhr.responseText);
        }
    }
    xhr.send();
    return false;
}

function fun_lista(lista) {
    var filas = lista.split(",");
    crearTabla(filas)
}


function crearTabla(filas) {
    var nroFilas = filas.length;
    var columnas = filas[1].split("|");
    var nroColumnas = columnas.length;
    var item;

    var nroColumnas;
    var contenido = '<table id="tabReposicion" style="border:1px solid blue"><thead class="GrillaHeader">';
    contenido += "<tr>";
    for (var h = 0; h < nroColumnas; h++) {
        if (h == 4) {
            contenido += '<th colspan="15" style="text-align:center">Semana de Ventas</th>';
        } else {
            if (h < 15) {
                contenido += '<th></th>';
            }
        }
    }
    contenido += "</tr>";
    for (var i = 0; i < nroFilas; i++) {
        contenido += "<tr>";
        item = filas[i].split("|");
        for (var j = 0; j < nroColumnas; j++) {
            if (i == 0) {
                if (j == 0) {
                    contenido += '<th><input id="CheckSelect" type="checkbox" /></th>';
                    contenido += '<th>Cant. Reponer</th>';
                    //Llena la columna con el código del producto
                    if (j != 0 && j != 1) {
                        contenido += "<td>";
                        contenido += item[j];
                        contenido += "</td>";
                    }
                } else {
                    if (j != 0 && j != 1) {
                        contenido += "<td>";
                        contenido += item[j];
                        contenido += "</td>";
                    }
                }
            } else {
                if (j == 0) {
                    //Inserta Checkbox y textbox
                    //valido que la cantidad a reponer sea de tipo numerico para que se marque por defecto el checkbox
                    if (isNaN(parseFloat(item[24]))) {
                        contenido += '<td><input id="CheckSelect" onclick="contador(nroFilasGlobal)" type="checkbox" name="check_reponer" value=' + '' + item[0] + '|' + item[1] + '' + ' /></td>';
                    } else {
                        contenido += '<td><input id="CheckSelect" onclick="contador(nroFilasGlobal)" type="checkbox" name="check_reponer" value=' + '' + item[0] + '|' + item[1] + '' + ' checked="checked" /></td>';
                    }
                    contenido += '<td><input id="txtCantidadReponer" type="text" value=' + '' + item[24] + '' + ' style="width:40px" /></td>';
                    //Llena la columna con el código del producto
                    if (j != 0 && j != 1) {
                        if (j >= 4 && j <= 18) {
                            contenido += '<td style="width:auto>';
                        } else { contenido += '<td style="width:auto">'; }
                        contenido += item[j];
                        contenido += "</td>";
                    }
                } else {
                    if (j != 0 && j != 1) {
                        contenido += '<td style="width:auto">';
                        contenido += item[j];
                        contenido += "</td>";
                    }
                }
            }
        }
        if (i == 0) {
            contenido += "</tr>";
            contenido += "</thead>";
            contenido += "<tbody class='GrillaRow'>";
        } else {
            contenido += "</tr>";
        }
    }
    contenido += "</table>";
    var divDatos = document.getElementById("datos");
    divDatos.innerHTML = contenido;
    nroFilasGlobal = nroFilas;
    contador(nroFilasGlobal);
}
function contador(nroFilas) {
    var contador = document.getElementById("lblContador");
    var contadorReponer = document.getElementById("lblReponer");

    var cb = [];
    cb = document.getElementsByName("check_reponer");
    var n = 0;
    for (var i = 0; i < cb.length; i++) {
        var e = parseInt(i);
        if (cb[i].checked == true) {
            n++;
        }
    }

    contador.innerHTML = nroFilas - 1;
    contadorReponer.innerHTML = n;
    return false;
}
function ExportToExcel(id) {
    var tabla = document.getElementById('tabReposicion');
    var nroFila = tabla.rows.length;
    var columna;
    //        for (var i = 0; i < nroFila; i++) {
    //            columna = tabla.rows[i].cells[0].value;
    //        }
    var html = tabla.outerHTML;
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
}