﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="SeguimientoPicking.aspx.vb" Inherits="APPWEB.SeguimientoPicking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  <div>
    
        <table class="style1">
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table class="style1">
                        <tr>
                            <td class="style2">
                                <input id="btnMostrar" type="button" value="Mostrar Ordenes " onclick="return btnMostrarOrdenes_onclick()" /></td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="CapaSeguimiento"></div></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>


<script type="text/javascript" language="javascript">
    
     function btnMostrarOrdenes_onclick(objeto, flag) 
     {
        var opcion = "ConsultaOrdenes";
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "SeguimientoPicking.aspx?flag=" + opcion, true);
         
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200)
                 {
                    fun_lista(xhr.responseText);
                 }
        }
        xhr.send(null);
        return false;
    }



    function fun_lista(lista) {

        filas = lista.split("▼");
        crearTabla();
        crearMatriz();
        mostrarMatriz();
//      configurarFiltros();
   
    }



    function crearMatriz() {
        matriz = [];
        var nRegistros = filas.length;
        var nCampos;
        var campos;
        var c = 0;
        var exito;
        var textos = document.getElementsByClassName("texto");
        var nTextos = textos.length;
        var texto;
        var x;
        for (var i = 0; i < nRegistros; i++) {
            exito = true;
            campos = filas[i].split("|");
            nCampos = campos.length;
            for (var j = 0; j < nTextos; j++) {
                texto = textos[j];
                x = j;

                if (x < 10) {
                    if (texto.value.length > 0) {
                        if (campos[x] != campos[0]) {
                            exito = campos[x - 1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        }

                    }
                }
                if (x == 10 && x < 13) {
                    if (texto.value.length > 0) {
                        if (campos[x] != campos[0]) {
                            exito = campos[x - 1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        }
                    }
                }

                if (!exito) break;
            }
            if (exito) {
                matriz[c] = [];
                for (var j = 0; j < nCampos; j++) {
                    if (isNaN(campos[j])) matriz[c][j] = campos[j];

                    else matriz[c][j] = campos[j];
                }
                c++;
            }
        }
    }


 
    function crearTabla() {

            var nRegistros = filas.length;
            var cabeceras = ["Marcar", "iddocumento", "NroOPV", "doc_FechaEmision", "tie_Nombre", "FechaHoraRegistro", "CadTipoDocumentoRef","TipoOperacion"];
            var nCabeceras = cabeceras.length;
            
            var contenido = "<table><thead>";
            contenido += "<tr class='GrillaHeader'>";
            for (var j = 0; j < nCabeceras; j++) {
                contenido += "<th>";
                contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
                contenido += "</th>";
            }    
            contenido += "</tr></thead><tbody id='tbDocumentos' class='GrillaRow'></tbody>";
            
            contenido += "</table>";
            var div = document.getElementById("CapaSeguimiento");
            div.innerHTML = contenido;
            
   }




   function mostrarMatriz() {

       var cabeceras = ["Marcar", "iddocumento", "NroOPV", "doc_FechaEmision", "tie_Nombre", "FechaHoraRegistro", "CadTipoDocumentoRef", "TipoOperacion"];
       var nCabeceras = cabeceras.length;


      var nRegistros = matriz.length;
      var contenido = "";
       if (nRegistros > 0) {
           var nCampos = matriz[0].length;
           for (var i = 0; i < nRegistros; i++) {
           
                 contenido += "<tr>";
                 var contenido = "<table><thead>";
                 contenido += "<tr class='GrillaHeader'>";
                 for (var k = 0; k < nCabeceras; k++) {
                     contenido += "<th>";
                     contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[k] + "'/>";
                     contenido += "</th>";
                 }
                 contenido += "</tr></thead></table>";
                 contenido += "</tr>";

                   contenido += "<tr>";
                   for (var j = 0; j < nCampos ; j++) {
                     
                                  contenido += "<td>";
                                  contenido += matriz[i][j];
                                  contenido += "</td>";
                            }

                   contenido += "</tr>";
          }
      }

       var spnMensaje = document.getElementById("nroFilas");
       var tabla = document.getElementById("tbDocumentos");
       tabla.innerHTML = contenido;
   }



   

</script>
 
</asp:Content>
