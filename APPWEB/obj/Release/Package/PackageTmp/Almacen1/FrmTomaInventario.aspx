<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmTomaInventario.aspx.vb" Inherits="APPWEB.FrmTomaInventario" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
    <tr>
    <td class="TituloCelda">TOMA DE INVENTARIO</td>
    </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo"  Width="80px" OnClientClick="return( valGenerarDoc()  );"  runat="server" Text="Nuevo" ToolTip="Generar Documento de Toma de Inventario" />
                            
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" Width="80px" OnClientClick="return( valBuscar() );" runat="server" Text="Buscar" ToolTip="Buscar Documento" />
                        </td>
                        <td>
                        <asp:Button ID="btnEditar" Width="80px" runat="server" Text="Editar" ToolTip="Editar" OnClientClick="return(  valEditar() );" />
                        </td>
                        <td>
                        <asp:Button ID="btnAnular" Width="80px" runat="server" Text="Anular" ToolTip="Anular" OnClientClick="return(  valAnular() );" />                        
                        
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" Width="80px" runat="server" Text="Guardar" ToolTip="Guardar" OnClientClick="return(  valSave()  );" />
                        </td>
                        <td>
                            <asp:Button ID="btnProcesarAjuste" OnClientClick="return(   valProcesoAjusteInv()      );" Width="130px" runat="server" Text="Proceso de Ajuste"  ToolTip="Procesar Ajuste" />
                            
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" Width="80px" OnClientClick="return( imprimir()  );" runat="server" Text="Imprimir" ToolTip="Imprimir" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancelar" Width="80px" OnClientClick="return(confirm('Desea cancelar la Operaci�n ?'));" runat="server" Text="Cancelar" ToolTip="Cancelar" />
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        <td>
            <asp:Panel ID="Panel_Cab" runat="server">
        <table>
        
        <tr>
        <td class="Label" >Empresa:</td>
        <td><asp:DropDownList ID="cboEmpresa" runat="server" 
    AutoPostBack="True">
                    </asp:DropDownList>
                </td>
        <td class="Label" >Tienda:</td>
        <td><asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                    </asp:DropDownList></td>
        <td class="Label">Serie:</td>
        <td>
        
        <asp:DropDownList ID="cboSerie" runat="server">
                    </asp:DropDownList>
        <asp:TextBox ID="txtCodigoDocumento"  onKeypress="return(  onKeyPressEsNumero('event')  );" Width="100px" CssClass="TextBox_ReadOnly"  runat="server"></asp:TextBox>
        <asp:Button ID="btnBuscarDocumento" Width="150px" OnClientClick="return(valBuscarDocumento());"   runat="server" Text="Buscar Documento" ToolTip="Buscar Documento" />
        </td>
        </tr>
        <tr>
        <td class="Label">Tipo Doc.:</td>
        <td><asp:DropDownList ID="cboTipoDocumento" runat="server">
                <asp:ListItem Value="28">Toma de Inventario</asp:ListItem>
                    </asp:DropDownList></td>
        <td class="Label">Estado</td>
        <td> <asp:DropDownList ID="cboEstado" runat="server"  Enabled="false" >
                    </asp:DropDownList> </td>
        <td class="Label" >Fecha Emisi�n:</td>
        <td><asp:TextBox ID="txtFechaEmision" Enabled="false" runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(  valFecha(this) );" Width="100px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaEmision">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                            </cc1:CalendarExtender></td>
        </tr>
        
        </table>    
            
            </asp:Panel>
        
        
        
        </td>
        </tr>
        <tr>
        <td>
        
            <asp:Panel ID="Panel_Emp_FechaTomaInv" runat="server">
            <table>
        <tr>
        <td class="Label"  >  Empleado  </td>
        <td>  <asp:TextBox ID="txtNomEmpleado" CssClass="TextBox_ReadOnlyLeft" Width="372px" Enabled="true" onKeypress="return(false);" runat="server"></asp:TextBox>  </td>
        <td>  <asp:Button ID="BtnBuscarEmpleado" Width="130px" runat="server" 
                                OnClientClick="return(mostrarCapaEmpleado());" Text="Buscar Empleado" 
                                ToolTip="Buscar Encargado para la Toma de Inventario" />  </td>
        <td>    </td>
        </tr>
        <tr>
        <td class="Label"  >  Almac�n:   </td>
        <td>  <asp:Label ID="lblNomAlmacen" runat="server" CssClass="LabelRojo" 
                        Font-Bold="true" Text=""></asp:Label>   </td>
        <td class="Label"  >  Fecha de Toma de Inventario:   </td>
        <td>   <asp:TextBox ID="txtFechaTomaInv" runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(  valFecha(this) );" Width="100px"></asp:TextBox>
                            <asp:Button ID="btnRecalcularSaldosSistema" Width="200px" OnClientClick="return( valRecalcularSaldosxFecha() );" runat="server" Text="Recalcular Saldos - Sstema" ToolTip="Recalcula los Saldos mostrados por el Sistema hasta la < Fecha ingresada de Toma de Inventario>" />
                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaTomaInv">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaTomaInv">
                            </cc1:CalendarExtender>  </td>
        </tr>
        <tr>
        <td class="Label"  >  Saldos de:</td>
        <td>  
            <asp:Label ID="lblNombreEmpresaSaldos" CssClass="LabelRojo" Font-Bold="true" runat="server" Text=""></asp:Label>
            </td>
        <td class="Label"  >  &nbsp;</td>
        <td>   &nbsp;</td>
        </tr>
        </table>
            </asp:Panel>
        
        
        
        
        
        </td>
        </tr>
        <tr>
            <td>
            
            <table id="TFrmPrincipal" width="100%">            
            <tr class="SubTituloCelda">
            <td >DETALLE</td>
            </tr>
            <tr>
            <td>
                
                
                <asp:Panel ID="Panel_Detalle" runat="server">
                
                <table>
                    <tr align="left">
                        <td  class="Texto" align="left">
                            L�nea:
                            <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true" 
                                DataTextField="Descripcion" DataValueField="Id">
                            </asp:DropDownList>
                            &nbsp;&nbsp;&nbsp; Sub L�nea:
                            <asp:DropDownList ID="cboSubLinea" runat="server" DataTextField="Nombre" 
                                DataValueField="Id" >
                            </asp:DropDownList>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnAceptar_Cab" Width="130px" runat="server" Text="Mostrar Detalle" 
                                ToolTip="Mostrar Detalle" />
                            &nbsp;&nbsp;&nbsp;&nbsp;                            
                        </td>
                    </tr>
                    <tr>
                    <td>
                    
                          <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%">
                            <Columns>
                            
                            <asp:TemplateField HeaderText="C�digo">
                            <ItemTemplate>
                            
                            <table>
                            <tr>
                            <td>
                                <asp:Label ID="lblIdProducto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>'    ></asp:Label></td>
                            <td>
                                <asp:HiddenField ID="hddIdDetalleDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDetalleDocumento")%>' />
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIdDocumento" runat="server"  Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIdUnidadMedida" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />
                            </td>
                            </tr>
                            </table>
                            
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                            
                            <asp:BoundField DataField="NomProducto" HeaderText="Descripci�n" />
                            <asp:BoundField DataField="UMedida" HeaderStyle-Width="80px"  ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"  HeaderText="U. M." />
                            <asp:BoundField DataField="CantxAtender" HeaderStyle-Width="100px"  ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"  HeaderText="Cant. Sistema" DataFormatString="{0:F2}" />
                            
                            <asp:TemplateField  HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:TextBox ID="txtCantidad" 
                                Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>'  onkeyup="return( calcularCantidades('0') );"
                                onFocus="return(  aceptarFoco(this)  );" Width="90px" runat="server" Font-Bold="true" onKeypress="return( validarNumeroPuntoPositivo('event') );" onblur="return( valBlur(event) );"  ></asp:TextBox>
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                              <asp:TemplateField HeaderText="Faltante"   ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:TextBox ID="txtFaltante"  Enabled="false" Width="90px" runat="server" Font-Bold="true" Text="0" ></asp:TextBox>
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                            <asp:TemplateField HeaderText="Sobrante"  ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:TextBox ID="txtSobrante" Enabled="false"  Width="90px" runat="server" Font-Bold="true" Text="0" ></asp:TextBox>
                            </ItemTemplate>
                            </asp:TemplateField>                                                                                    
                            
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />                            
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>      
                    
                    </td>
                    </tr>
                </table>
                </asp:Panel>
                
                </td>
            </tr>
            <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td>
                        
                            
                        
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
            <td>
            <asp:CheckBox CssClass="LabelRojo" ID="chbMostrarCantSistema" Text="Mostrar Cantidades del Sistema en la Impresi�n" runat="server" Checked="false" />
            </td>
            </tr>
            <tr>
            <td>
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                
                </td>
            </tr>
            <tr>
            <td>
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
                </td>
            </tr>
            <tr>
            <td>
            
            <asp:HiddenField ID="hddIdAlmacen" runat="server" />     
            </td>
            </tr>
            <tr>
            <td>
            
             <asp:HiddenField ID="hddIdEmpleado" runat="server" />
            
            </td>
            </tr>
            <tr>
            <td>
            
            
            
                <asp:HiddenField ID="hddIdEmpresaTomaInv"  runat="server" />
            
               
            
            </td>
            </tr>
            <tr>
            <td>
                <asp:HiddenField ID="hddPoseeDocAjusteInv" runat="server" />
            </td>
            </tr>
            <tr>
            <td></td>
            </tr>
            <tr>
            <td></td>
            </tr>
            </table>
            
            </td>
        </tr>        
        <tr>
            <td>
                           
                
                
                
                
                             
           
          
            
                
                
                
                
                
            </td>
        </tr>
    </table>
    
       <div id="capaGenerarDocumento"
                    
                    style="border: 3px solid blue; padding: 8px; width:auto; height:auto; position:absolute;background-color:white; z-index:2; display :none; top: 213px; left: 128px;">
               
                <table>
                
                <tr>
                <td>
                
                  
                   <asp:UpdatePanel ID="UpdatePanel_GenerarDoc" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                  
                <table>
                
                <tr>
                <td align="right" class="Label">Empresa:</td>
                <td align="left">
                    <asp:DropDownList ID="cboEmpresa_GD" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="right" class="Label">Almac�n:</td>
                <td align="left"><asp:DropDownList ID="cboAlmacen_GD" runat="server">
                    </asp:DropDownList></td>
                </tr>
                <tr>
                <td align="right" class="Label">L�nea:</td>
                <td align="left"><asp:DropDownList ID="cboLinea_GD" runat="server" AutoPostBack="true" >
                    </asp:DropDownList></td>
                <td align="right" class="Label">Sub L�nea:</td>
                <td align="left"><asp:DropDownList ID="cboSubLinea_GD" runat="server">
                    </asp:DropDownList></td>
                </tr>                
                </table>                                   
                    
                
                
                    </ContentTemplate>
                    </asp:UpdatePanel>
                
                
                
                
                </td>
                </tr>
                
                
                <tr>
                <td align="center">
                
                <table>
                    <tr>
                    <td>
                        <asp:Button ID="btnAceptar_GD" runat="server" Text="Aceptar" ToolTip="Generar Documento de Toma Inventarios" OnClientClick="return( valGenerarDoc_Aceptar()  );" /></td>
                    <td>
                        <asp:Button ID="btnCancelar_GD" OnClientClick="return( offCapa('capaGenerarDocumento') );" runat="server" Text="Cancelar" ToolTip="Cancelar" /></td>
                    </tr>
                    </table>
                
                </td>
                </tr>
                
                
                </table>    
                
                
                
                </div>
       <div  id="capaPersona" 
        
                    style="border: 3px solid blue; padding: 8px; width:900px; height:auto; position:absolute; top:184px; left:38px; background-color:white; z-index:2; display :none; ">
                        <asp:UpdatePanel ID="UpdatePanel_Persona" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                                       <asp:ImageButton ID="btnCerrar_Capa" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaPersona'));" />   
                                </td>
                                </tr>
                                    <tr class="Label">
                                        <td align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Filtro:</td>
                                                    <td>
                                                        <asp:DropDownList ID="cmbFiltro_BuscarEmpleado" runat="server">
                                                        <asp:ListItem Selected="True" Value="0">Descripci�n</asp:ListItem>                                                        
                                                        <asp:ListItem Value="1">D.N.I.</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        Texto:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtBuscarEmpleado_Grilla" runat="server" Width="250px"></asp:TextBox>
                                                        <asp:ImageButton ID="btnBuscarEmpleado_Grilla" runat="server" 
                                                            CausesValidation="false" ImageUrl="~/Imagenes/Buscar_b.JPG"                                                             
                                                            onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                         <asp:GridView ID="GV_Empleado" runat="server" AllowPaging="true" 
                                                                  AutoGenerateColumns="false" PageSize="20" Width="100%">
                                                                  <Columns>
                                                                  
                                                                  <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                  <ItemTemplate >
                                                                  
                                                                  <asp:ImageButton ID="btnSelectEmpleado"  OnClientClick="return(  valSelectEmpleado()  );"  ToolTip="Seleccionar Empleado" ImageUrl="~/Imagenes/Ok_b.bmp"  runat="server"  />
                                                                  
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Empleado"  >
                                                                  <ItemTemplate>
                                                                  <table>
                                                                  <tr>
                                                                  <td> 
                                                                      <asp:Label ID="lblNomEmpleado" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'   ></asp:Label>  </td>
                                                                  <td>  
                                                                      <asp:HiddenField ID="hddIdEmpleado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' /> </td>
                                                                  </tr>
                                                                  </table>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:BoundField DataField="Dni" HeaderText="D.N.I."  />
                                                                  <asp:BoundField DataField="NomCargo" HeaderText="Cargo"  />
                                                                  
                                                                  
                                                                  </Columns>
                                                                  <HeaderStyle CssClass="GrillaHeader" />
                                                                  <FooterStyle CssClass="GrillaFooter" />
                                                                  <PagerStyle CssClass="GrillaPager" />
                                                                  <RowStyle CssClass="GrillaRow" />
                                                                  <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                  <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                  <EditRowStyle CssClass="GrillaEditRow" />
                                                              </asp:GridView></td>
                                    </tr> 
                                    <tr>
                                    
                                    <td>                                                                                                               
                                    </td>
                                    </tr>                                     
                 
                 </table>      
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>                           
    
    <script language="javascript" type="text/javascript">

        function valGenerarDoc() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID %>');
            if (isNaN(cboSerie.value) || cboSerie.value.length == 0) {
                alert('Debe registrar un NRO de SERIE para el documento.');
                return false;
            }

            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (CajaEnBlanco(txtCodigoDocumento)) {
                alert('No se ha generado un C�DIGO para el Documento.');
                return false;
            }

            var hddIdEmpleado = document.getElementById('<%=hddIdEmpleado.ClientID %>');
            if (isNaN(hddIdEmpleado.value) || hddIdEmpleado.value.length == 0) {
                if( ! confirm('No se ha seleccionado un empleado responsable de la Toma de Inventarios. Desea continuar de todos modos ?')  ){
                    return false;
                }
            }
            
            onCapa('capaGenerarDocumento');
            return false;
        }


        function calcularCantidades(recorrerAll) {

            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla == null) {                
                return false;
            }

            var IdEvento = '';
            if(event != null){
                IdEvento=event.srcElement.id;                
            }
            
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                if (rowElem.cells[4].children[0].id ==IdEvento  || recorrerAll=='1' ) {

                    var cantidad = parseFloat(      rowElem.cells[4].children[0].value       );
                    if (isNaN(cantidad)) { cantidad = 0; }

                    var cantidadSistema = parseFloat(rowElem.cells[3].innerHTML);
                    if (isNaN(cantidadSistema)) { cantidadSistema = 0; }

                    var diferencia = cantidadSistema - cantidad;
                    var faltante = 0
                    var sobrante = 0
                    if (diferencia > 0) {
                        sobrante = diferencia;
                        faltante = 0;
                    } else if (diferencia < 0) {
                        sobrante = 0;
                        faltante = diferencia*(-1) ;
                    }
                    
                    //********* Imprimimos
                    rowElem.cells[5].children[0].value = redondear(sobrante, 2);  //******** Sobrante
                    rowElem.cells[6].children[0].value = redondear(faltante, 2);  //******** Faltante
                    
                    if( recorrerAll=='0' ){
                        return false;
                    }
                }
            }
            
            return false;
        }

        function valSave() {

            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>');
            if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length == 0) {
                alert('No se ha cargado ning�n Documento < Toma de Inventario >.');
                return false;
            }

            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla == null) {
                alert('No se cargaron productos.');
                return false;
            }
            
            return (  confirm('Desea continuar con la operaci�n ?') );
        }
        function valBuscarDocumento() {

            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (CajaEnBlanco(txtCodigoDocumento)) {
                alert('Ingrese un C�DIGO de Documento.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }

            var IdSerie = parseInt(document.getElementById('<%=cboSerie.ClientID %>').value);
            if( isNaN(IdSerie)  ||  IdSerie.length==0  ){
                alert('Seleccione una SERIE.');
                return false;
            }
            
            return true;
        }
        function valBuscar() {
            var IdSerie = parseInt(document.getElementById('<%=cboSerie.ClientID %>').value);
            if (isNaN(IdSerie) || IdSerie.length == 0) {
                alert('Seleccione una SERIE.');
                return false;
            }
            return true;
        }
        function mostrarCapaEmpleado() {
            onCapa('capaPersona');
            document.getElementById('<%=txtBuscarEmpleado_Grilla.ClientID%>').select();
            document.getElementById('<%=txtBuscarEmpleado_Grilla.ClientID%>').focus();
            return false;
        }

        function valSelectEmpleado() {
            var grilla = document.getElementById('<%=GV_Empleado.ClientID%>');
            if (grilla != null) {

                var hddIdEmpleado = document.getElementById('<%=hddIdEmpleado.ClientID%>');
                var txtNomEmpleado = document.getElementById('<%=txtNomEmpleado.ClientID%>');
                
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[0].id == event.srcElement.id ) {

                        txtNomEmpleado.value = rowElem.cells[1].children[0].cells[0].children[0].innerHTML;
                        hddIdEmpleado.value = rowElem.cells[1].children[0].cells[1].children[0].value;

                        offCapa('capaPersona');
                        return false;
                        
                    }
                }
            
            
                
            }
        
        
            return false;
        }
        function imprimir() {

            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length == 0) {
                alert('No se ha seleccionado ning�n Documento para Impresi�n.');
                return false;
            }

            var chbMostrarDatosSIGE = document.getElementById('<%=chbMostrarCantSistema.ClientID%>');
            var Opcion = '0';
            if (chbMostrarDatosSIGE.status ) {Opcion = '1'; }

            window.open('../../Almacen1/Reportes/Visor.aspx?iReporte=4&IdDocumento=' + hddIdDocumento.value+'&Opcion=' + Opcion, 'Inventario', 'resizable=yes,width=1000,height=700,scrollbars=1', null);
            //window.open('../../Reportes/visor.aspx?iReporte=2&IdDoc=' + hdd.value, 'Factura', 'resizable=yes,width=1000,height=700', null);
            return false;
        }

        function valRecalcularSaldosxFecha() {
            var txtFechaTomaInv = document.getElementById('<%=txtFechaTomaInv.ClientID %>');
            if (CajaEnBlanco(txtFechaTomaInv)) {
                alert('Ingrese una Fecha de Toma de Inventario.');
                txtFechaTomaInv.select();
                txtFechaTomaInv.focus();
                return false;
           }
            return (confirm('Desea continuar con la Operaci�n de Recalcular los Saldos del Sistema para la fecha de Toma de Inventario Ingresada ?'));
        }
        function valEditar() {

            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length == 0) {
                alert('No se ha seleccionado ning�n Documento al cual hacer referencia.');
                return false;
            }
            var IdEstado = parseInt(document.getElementById('<%=cboEstado.ClientID %>').value);
            if (IdEstado==2) {
                alert('El Documento ha sido anulado.');
                return false;
            }
            return true;
        }
        function valAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length == 0) {
                alert('No se ha seleccionado ning�n Documento al cual hacer referencia.');
                return false;
            }
            var IdEstado = parseInt(document.getElementById('<%=cboEstado.ClientID %>').value);
            if (IdEstado == 2) {
                alert('El Documento ya ha sido anulado.');
                return false;
            }
            var PoseeDocAjusteInv =parseInt( document.getElementById('<%=hddPoseeDocAjusteInv.ClientID%>').value);
            if (  PoseeDocAjusteInv>0 ) {
                alert('El Documento Toma de Inventario no puede ser anulado porque posee un documento Ajuste de Inventario. Anule dicho documento relacionado y luego podr� anular este Documento Toma de Inventario.');
                return false;
            }
            return (   confirm('Desea continuar con el proceso de anulaci�n del Doc. Toma de Inventario ?')        );
        }
        function valProcesoAjusteInv() {

            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length == 0) {
                alert('No se ha seleccionado ning�n Documento al cual hacer referencia.');
                return false;
            }
            var IdEstado = parseInt(document.getElementById('<%=cboEstado.ClientID %>').value);
            if (IdEstado == 2) {
                alert('El Documento est� anulado. No procede el Proceso de Ajuste de Inventario.');
                return false;
            }

            window.open('FrmProcesoAjusteInventario.aspx?IdDocumentoTI=' + hddIdDocumento.value, 'Proceso_Ajuste_Inventario', 'resizable=yes,width=1000,height=700,scrollbars=1', null);            
                    
            
            return false;
        }
        function valGenerarDoc_Aceptar() {
            var txtFechaTomaInv = document.getElementById('<%=txtFechaTomaInv.ClientID%>');

            return (confirm('La Fecha de Toma de Inventario es : '+ txtFechaTomaInv.value  + '. Desea continuar con la operaci�n ?'));
        }
    </script>
    
</asp:Content>


