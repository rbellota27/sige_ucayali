﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" EnableEventValidation="true"
MasterPageFile="~/Principal.Master" CodeBehind="WebForm2.aspx.vb" Inherits="APPWEB.WebForm2" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

            <div id="contenedor" style="width:100%;height:100%">
            <div style ="width:100%">
              <div style ="width:100%"> 
                <table width="100%">
                    <tr>
                             <td class="TituloCeldaLeft">
                                <span>ACCIONES GENERALES</span>
                             </td>
                    </tr>
                </table>
              </div>
              <div align="center">
                <div style="width:25%;height:5%;float:left;margin-right:1px">
                    <div style="width:100%">
                        <span class="TituloCelda">CREAR PROGRAMACIÓN</span>
                    </div>
                    <div style="width:100%">
                        <asp:Button ID="btnBuscarProgramacion" runat="server" Text="Crear Programación" CssClass="btnCrearProgramacion" />
                    </div>
                </div>
                <div style="width:25%;height:5%;float:left;margin-right:1px">
                    <div style="width:100%">
                        <span class="TituloCelda">BUSCAR PROGRAMACIÓN</span>
                    </div>
                    <div style="width:100%">
                        <div style="width:49%;float:left">
                            <span class="Texto" style="width:15%">Desde:</span>
                            <asp:TextBox ID="txtCalendarioInicio" runat="server" Width="40%"></asp:TextBox>
                            
                            <cc1:CalendarExtender ID="calEx_txtFecInicio_BuscarProgramacion" runat="server" TargetControlID="txtCalendarioInicio"
                            Format="dd/MM/yyyy">
                            </cc1:CalendarExtender>
                        </div>
                        <div style="width:50%;float:left">
                            <span class="Texto" style="width:15%">Hasta:</span>
                            <asp:TextBox ID="txtCalendarioFin" runat="server" Width="40%"></asp:TextBox>
                            <cc1:CalendarExtender ID="calEx_txtFecfin_BuscarProgramacion" runat="server" TargetControlID="txtCalendarioFin"
                            Format="dd/MM/yyyy">
                            </cc1:CalendarExtender>
                        </div>
                    </div>
                    <div style="width:100%" align="center">
                        <asp:TextBox ID="txtDescTransporte" runat="server" Width="150px"></asp:TextBox>
                        <asp:TextBox ID="txtCodigoTransporte" runat="server" Width="50px"></asp:TextBox>
                        <asp:CheckBox ID="chkTransporte" runat="server" Checked="true" />
                    </div>
                    <div style="width:100%" align="center">
                        <asp:Button ID="btnBuscarFiltros" runat="server" Text="Buscar" CssClass="btnBuscar" />
                    </div>

                </div>
                <div style="width:25%;height:5%;float:left">
                    <div>
                        <span class="TituloCelda">EXPORTAR PROGRAMACIÓN</span>
                    </div>
                    <div style="width:100%" align="center">
                        <div style="width:49%;float:left">
                            <span class="Texto">Desde:</span>
                            <asp:TextBox ID="txt3" runat="server" Width="45%"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txt3" Format="dd/MM/yyyy">
                            </cc1:CalendarExtender>
                        </div>
                        <div style="width:50%;float:left">
                            <span class="Texto">Hasta:</span>
                            <asp:TextBox ID="txt4" runat="server" Width="45%" ></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txt4" Format="dd/MM/yyyy">
                            </cc1:CalendarExtender>
                        </div>
                    </div>
                    <div style="width:100%" align="center">
                        <asp:Button ID="btnExcel" runat="server" CssClass="btnExportarExcel" Text="Excel" OnClick="btnExportar_Click" CommandName="excel"
                            Height="35px" />
                        <asp:Button ID="btnPdf" runat="server" CssClass="btnExportarPdf" text="Pdf" OnClick="btnExportar_Click" CommandName="pdf"
                            Height="35px" />
                        <asp:Button ID="btnWord" runat="server" CssClass="btnWord" text="Word" OnClick="btnExportar_Click" CommandName="word"
                        Height="35px" />
                            
                    </div>
                </div>
                <div style="width:24%;height:5%;float:left">
                    <div>
                        <span class="TituloCelda">BUSCAR GUIA</span>
                    </div>
                    <div style="width:100%" align="center">
                        <div style="float:left">
                            <span class="Texto">Nro Guia:</span>
                            <asp:TextBox ID="txtNroSerie" runat="server" Text="" Width="60px"></asp:TextBox>
                            <asp:TextBox Width="100px" ID="txtBuscarDocumento" runat="server" Text=""></asp:TextBox>
                            <asp:Button ID="btnBuscarDocumentoGeneral" runat="server" Text="Buscar" CssClass="btnBuscar" />
                        </div>
                    </div>
                    <div style="width:100%" align="center">
                        <asp:Button ID="btnDescpachoClienteFinal" runat="server" Text=" Exportar Despachos Cliente Final" OnClick="btnClientefinal_Click" CommandName="pdf" />
                    </div>
                    <div style="width:100%">
                        <asp:Button ID="btnDespachoSanicenterSubcontratado" runat="server" Text=" Exportar Despachos Cliente Final" OnClick="btnSanicenter_sucursal_Click" CommandName="pdf" />
                    </div>
                </div>
              </div>
            </div>
            <div style="width:100%;float:left" class="TituloCelda">
                <asp:Image ID="Image1" runat="server" Width="15px" ImageUrl="~/Imagenes/Menos_B.JPG" />&nbsp; PROGRAMACIÓN - [DESPACHOS POR ATENDER]
            </div>                
            <div style="float:left;width:100%;padding-top:5px">
            <asp:Panel ID="pnlCronogramaOnline" runat="server">
                <asp:GridView ID="gvVehiculos" runat="server" AutoGenerateColumns="False" Width="100%" Font-Size="8pt" 
        DataKeyNames="idCronograma" ShowHeaderWhenEmpty="True">
        <HeaderStyle CssClass="GrillaHeader" HorizontalAlign="Center" />
            <Columns>
                <asp:TemplateField HeaderText="Estado">
                    <ItemTemplate>
                        <asp:Image ID="imgEstadoDespacho" runat="server" ImageUrl="~/Imagenes/Estado_Amarillo.png" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>                      
                    <ItemTemplate>
                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"idEstadoDespacho")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Turno">
                    <HeaderTemplate>
                        Turno:
                        <asp:DropDownList ID="ddlTurnoFiltro"  runat="server" AutoPostBack="true" 
                        AppendDataBoundItems="true" OnSelectedIndexChanged="evento_cambiarTurno" >
                        <asp:ListItem Text = "TODO" Value = "TODO"></asp:ListItem>
                        <asp:ListItem Text = "MAÑANA" Value = "MAÑANA"></asp:ListItem>
                        <asp:ListItem Text = "TARDE" Value = "TARDE"></asp:ListItem>
                        <asp:ListItem Text = "NOCHE" Value = "NOCHE"></asp:ListItem>
                        </asp:DropDownList>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("turno") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="fecInicio" HeaderText="Fec. Ini Despacho" 
                    ItemStyle-HorizontalAlign="Center">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="fecFin" HeaderText="Fec. Fin Despacho" 
                    ItemStyle-HorizontalAlign="Center">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="nomAgencia" HeaderText="Agencia / Transportista" 
                    ItemStyle-HorizontalAlign="Center">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Placa Veh.">
                    <ItemTemplate>
                        <asp:Label ID="lblplaca" runat="server" Text='<%# Bind("vehPlaca") %>'></asp:Label> -
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("nroVuelta") %>' ForeColor="Red"></asp:Label>
                    </ItemTemplate>
                    <HeaderTemplate>
                        Placa:
                        <asp:DropDownList ID="ddlPlacaVehiculo"  runat="server" AutoPostBack="true" 
                        AppendDataBoundItems="true" OnSelectedIndexChanged="evento_filtrarplaca" >
                        <asp:ListItem Text = "TODO" Value = "TODO"></asp:ListItem>
                        </asp:DropDownList>
                    </HeaderTemplate>
                    <HeaderStyle Width="150px" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>                      
                <asp:BoundField DataField="vehModelo" HeaderText="Modelo" 
                    ItemStyle-HorizontalAlign="Center" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="capacidadMaxima" HeaderText="Cap.Máxima" 
                    ItemStyle-HorizontalAlign="Center" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="capacidadAlcanzada" HeaderText="Cap.Actual" 
                    ItemStyle-HorizontalAlign="Center" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="cantDocAsociado" HeaderText="Cant.Doc" 
                    ItemStyle-HorizontalAlign="Center" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="conductor" HeaderText="Conductor" 
                    ItemStyle-HorizontalAlign="Center" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="ayudante" HeaderText="Ayudante" 
                    ItemStyle-HorizontalAlign="Center" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="control" HeaderText="Control" 
                    ItemStyle-HorizontalAlign="Center" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:CommandField ButtonType="Image" ShowSelectButton="True" 
                    SelectImageUrl="~/Imagenes/eyeEditar.png" SelectText="Ver" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:CommandField>
            </Columns>
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
        </asp:GridView>      
            </asp:Panel>
            </div>
            <div style="width:100%;float:left;padding-top:7px" class="TituloCelda">
                <asp:Image ID="Image4" runat="server" Width="15px" ImageUrl="~/Imagenes/Menos_B.JPG" />&nbsp; PROGRAMACIÓN - [DESPACHOS ATENDIDOS]
                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender5" runat="server" TargetControlID="Panel1" SuppressPostBack="true"
                            ImageControlID="Image4" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG" CollapsedSize="0"
                            Collapsed="false" CollapseControlID="Image4" ExpandControlID="Image4">
                        </cc1:CollapsiblePanelExtender>
            </div> 
            <div style="float:left;width:100%;padding-top:5px">
                    <asp:Panel ID="Panel1" runat="server">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="100%"
                            DataKeyNames="turno,vehModelo,capacidadMaxima,capacidadAlcanzada,IdConductor,IdAyudante,IdControl,idDocumentoRelacion,idEstadoDespacho,idCronograma,IdVehiculo,fecInicio,nomAgencia,idAgencia" ShowHeaderWhenEmpty="True">
                    <HeaderStyle CssClass="GrillaHeader" />
                    <Columns>
                      <asp:TemplateField HeaderText="Estado">
                          <ItemTemplate>
                              <asp:Image ID="imgEstadoDespacho" runat="server" ImageUrl="~/Imagenes/Estado_Plomo.png" />
                          </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField>                      
                          <ItemTemplate>
                              <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"idEstadoDespacho")%>' />
                          </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="idCronogramaDespacho" HeaderText="idCronogramaDespacho" Visible="false" />
                      <asp:BoundField DataField="idDocumentoRelacion" HeaderText="idDocumentoRelacion" Visible="false" />
                      <asp:BoundField DataField="IdConductor" HeaderText="IdConductor" Visible="false" />
                      <asp:BoundField DataField="IdAyudante" HeaderText="IdAyudante" Visible="false" />
                      <asp:BoundField DataField="IdControl" HeaderText="IdControl" Visible="false" />
                      <asp:BoundField DataField="turno" HeaderText="Turno" />
<%--                      <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lbl"></asp:Label>
                        </ItemTemplate>                        
                      </asp:TemplateField>--%>
                      <asp:BoundField DataField="fecInicio" HeaderText="Fec. Ini Despacho" ItemStyle-HorizontalAlign="Center" />
                      <asp:BoundField DataField="fecFin" HeaderText="Fec. Fin Despacho" ItemStyle-HorizontalAlign="Center" />
                      <asp:BoundField DataField="nomAgencia" HeaderText="Agencia / Transportista" ItemStyle-HorizontalAlign="Center" />
                      <asp:TemplateField HeaderText="Placa Veh.">
                          <ItemTemplate>
                              <asp:Label ID="lblplaca" runat="server" Text='<%# Bind("vehPlaca") %>'></asp:Label> -                               
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("nroVuelta") %>' ForeColor="Red"></asp:Label>
                          </ItemTemplate>
                          <HeaderTemplate>
                                Placa:
                              <asp:DropDownList ID="ddlPlacaVehiculo2"  runat="server" AutoPostBack="true" 
                              AppendDataBoundItems="true"  >
                                <asp:ListItem Text = "TODO" Value = "TODO"></asp:ListItem>
                              </asp:DropDownList>
                          </HeaderTemplate>
                          <HeaderStyle Width="150px" />
                          <ItemStyle HorizontalAlign="Center" />
                      </asp:TemplateField>                      
                      <asp:BoundField DataField="vehModelo" HeaderText="Modelo" ItemStyle-HorizontalAlign="Center" />
                      <asp:BoundField DataField="capacidadMaxima" HeaderText="Cap.Máxima" ItemStyle-HorizontalAlign="Right" />
                      <asp:BoundField DataField="capacidadAlcanzada" HeaderText="Cap.Actual" ItemStyle-HorizontalAlign="Right" />
                      <asp:BoundField DataField="cantDocAsociado" HeaderText="Cant.Doc" ItemStyle-HorizontalAlign="Center" />
                      <asp:BoundField DataField="conductor" HeaderText="Conductor" ItemStyle-HorizontalAlign="Center" />
                      <asp:BoundField DataField="ayudante" HeaderText="Ayudante" ItemStyle-HorizontalAlign="Center" />
                      <asp:BoundField DataField="control" HeaderText="Control" ItemStyle-HorizontalAlign="Center" />
                      <asp:CommandField ButtonType="Image" ShowSelectButton="True" 
                          SelectImageUrl="~/Imagenes/eyeEditar.png" SelectText="Ver" >
                      <ItemStyle HorizontalAlign="Center" />
                      </asp:CommandField>
                  </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
              </asp:GridView>      
                    </asp:Panel>
                    </div>      
        </div>
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>        
        <div id="capaDespachos" style="border: 3px solid blue; padding: 8px; 
            width: 900px; height: 1300px; position: absolute; overflow:scroll;min-height: 225px;top:20px; left: 100px;margin-bottom: 10px;margin-right: 10px; background-color: white;
            z-index: 2; display: none;">
            <%-- onmousedown="comienzoMovimiento(event, this.id);" onmouseover="this.style.cursor='move'"--%>
                <div style="float:right;width:100%;" >
                <table align="right">
                    <tr>
                        <td>
                        <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                            OnClientClick="return(  offCapa('capaDespachos')   );" />
                        </td>
                    </tr>
                </table>
            </div>
                <div style="width:100%;padding-top:5px">
                <div align="center" style="padding-top:5px;padding-bottom:5px">
                    <asp:Label ID="lblEstado" runat="server" Text="NUEVO - [CRONOGRAMA DE DESPACHO]" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="hhdlblEstado" runat="server" Value="0" />
                    <asp:HiddenField ID="hhdidCronogramaDespacho" Value="0" runat="server" />
                </div>
                <div align="center" class="TituloCeldaLeft" style="padding-top:5px">
                <span style="font-weight:bold">FECHA - [Inicio de Despacho]</span>            
                </div>
                <div style="width:100%;padding-bottom:5px" >
                <div style="width:100%">
                    <div>
                        <table>
                            <tr>
                                <td class="Texto">
                                    Turno:
                                </td>
                                <td class="Texto">
                                    <asp:DropDownList ID="ddlTurno" runat="server">
                                    <asp:ListItem Value="Mañana" Text="Mañana"></asp:ListItem>
                                    <asp:ListItem Value="Tarde" Text="Tarde"></asp:ListItem>
                                    <asp:ListItem Value="Noche" Text="Noche"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="Texto">
                                    Fecha:
                                </td>
                                <td class="Texto">
                                    <asp:TextBox ID="txtFecha" runat="server"></asp:TextBox>               
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                     CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFecha" />
                                    <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtFecha" Format="dd/MM/yyyy" />
                                </td>
                                <td class="Texto">
                                    Hora:
                                </td>
                                <td>
                                    <asp:TextBox ID="txthora" runat="server" Width="50px"></asp:TextBox>
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txthora" Mask="99:99" 
                                    MaskType="Number" InputDirection="LeftToRight" ErrorTooltipEnabled="true"
                                    MessageValidatorTip="true" AcceptNegative="Left" ClearMaskOnLostFocus="false" />
                                <span style="font-style:italic;font-size:smaller;width:200px">ejem. 14:06</span>
                                </td>
                                <td class="Texto">
                                    Nro° Vuelta
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNroVuelta" runat="server" Width="40px" Text="1"></asp:TextBox>
                                </td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td>
                                    <asp:Button ID="btnAnular" runat="server" Text="Anular" CssClass="btnCancelar" />
                                    <asp:Button ID="btnGuardarDespacho" runat="server" Text="Guardar" OnClientClick="return btnGuardar()" CssClass="btnGuardar" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
                <div>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                        <asp:Image ID="Image2" runat="server" Width="15px" />&nbsp; DATOS GENERALES - [VEHÍCULO]
                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="pnlDatosVehiculo" SuppressPostBack="true"
                            ImageControlID="Image2" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG" CollapsedSize="0"
                            Collapsed="false" CollapseControlID="Image2" ExpandControlID="Image2">
                        </cc1:CollapsiblePanelExtender>
                         </td>
                    </tr>
                </table>
            </div>
                <div style="width:100%">
                <asp:Panel ID="pnlDatosVehiculo" runat="server">
                <table width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="lblTituloPlaca" runat="server" Text="Vehiculo/Placa" CssClass="Texto"></asp:Label>
                        </td>
                        <td style="width:160px">
                            <asp:TextBox ID="txtPlaca" runat="server" Width="80px"></asp:TextBox>
                            <asp:Button ID="btnBuscarVehiculo" Text="Buscar" Width="80px" runat="server" CssClass="btnBuscar" OnClientClick="return(  offCapa('valOnClick_btnBuscarVehiculo')   );" />
                        </td>
                        <td>
                            <asp:Label ID="lblTituloModelo" runat="server" Text="Modelo" CssClass="Texto"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtmodelo" runat="server" Width="180px" ReadOnly="true"></asp:TextBox>
                        </td>       
                        <td class="Texto">
                            Conductor
                        </td>
                        <td>
                            <div>
                                <asp:DropDownList ID="ddlConductor" runat="server" Width="200px">
                                </asp:DropDownList>                                            
                            </div>
                        </td>                    
                    </tr>
                    <tr>
                        <td class="Texto" style="width:5%">
                            Ayudante
                        </td>
                        <td>
                            <div>
                                <asp:DropDownList ID="ddlAyudante" runat="server" Width="200px">
                                </asp:DropDownList> 
                            </div>
                        </td>    
                        <td class="Texto">
                            Control
                        </td>
                        <td style="width:5%">
                            <div>
                                <asp:Button ID="btnControladores" runat="server" Text="Cargar Controladores" Width="100%" />
                            </div>
                        </td>
                        <td class="Texto">
                            Capacidad (kg)
                        </td>
                        <td rowspan="2">
                            <asp:TextBox ID="txtCapacidadTotal" runat="server" Width="100px"  onblur="revalidarCapacidad();" onkeyup="calcularCapacidadEditable();" ></asp:TextBox>
                            <asp:Label ID="Label1" runat="server" Text="Disponible" Font-Italic="true"></asp:Label><br />
                            <asp:TextBox ID="txtCapacidadUsada" runat="server" Width="100px" Enabled="false"></asp:TextBox>
                            <asp:Label ID="Label2" runat="server" Text="Usado" Font-Italic="true"></asp:Label><br />
                            <asp:TextBox ID="txtCapacidadSinUsar" runat="server" Enabled="false" Width="100px" Text="0"></asp:TextBox>
                            <asp:Label ID="Label3" runat="server" Text="Sin Usar" Font-Italic="true"></asp:Label>
                        </td>             
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gv_Controladores" runat="server" AutoGenerateColumns="False" Width="100%" Font-Size="8pt">                            
                                <Columns>
                                    <asp:BoundField DataField="nom_sector" HeaderText="ZONA" />
                                    <asp:BoundField DataField="controlador" HeaderText="CONTROLADOR(ES)" />
                                    <%--<asp:BoundField DataField="sl_Nombre" HeaderText="LINEA / SUBLINEA" />--%>
                                </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <RowStyle CssClass="GrillaRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                </asp:Panel>
            </div>
                <div style="padding-top:5px;padding-bottom:5px" align="center">
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                        <asp:Image ID="imgcot" runat="server" Width="15px" />&nbsp; AGREGAR DOCUMENTOS - [Guias de Remisión]
                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="pnlCliente" SuppressPostBack="true"
                            ImageControlID="imgcot" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedSize="1000"
                            Collapsed="false" CollapseControlID="imgcot" ExpandControlID="imgcot">
                        </cc1:CollapsiblePanelExtender>
                                
                         </td>
                    </tr>
                </table>
            </div>
                <div id="divCliente" style="height:200px;width:100%">
                <asp:Panel ID="pnlCliente" runat="server">
                    <div style="float:left;width:100%">      
                        <table align="left">
                        <tr>
                        <td>
                            <asp:CheckBox ID="chkActivo" runat="server" Checked="false" /></td>
                        <td class="Texto">Fecha</td>
                        <td>
                            <asp:TextBox ID="txtFechaFiltro" runat="server"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                     CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFiltro" />
                            <cc1:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txtFechaFiltro" Format="dd/MM/yyyy" />
                        </td>
                        <td class="Texto">Tiendas</td>
                        <td>
                            <asp:DropDownList ID="ddlTiendas" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Text="----" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Serie
                        </td>
                        <td>
                            <asp:TextBox ID="txtSerie" runat="server" Width="50px" MaxLength="3"></asp:TextBox>
                        </td>
                        <td class="Texto">
                            Nro. Documento
                        </td>
                        <td>
                            <asp:TextBox ID="txtNrodocumento" runat="server" Width="100px" ></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscarDocumento" runat="server" Text="Buscar" CssClass="btnBuscar"
                            OnClientClick="this.disabled=true;this.value='Procesando'" UseSubmitBehavior="false" />
                        </td>
                    </tr>             
                </table>
                    </div>
                    <div style="float:left;width:100%;height:250px;overflow:scroll">
                    <asp:GridView ID="gvGuias" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="IdTienda,IdSerie,IdDocumento">
                        <HeaderStyle CssClass="GrillaHeader" HorizontalAlign="Center" />
                         <Columns>
                             <asp:TemplateField ShowHeader="False">
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbSeleccionar" runat="server" CommandName="Seleccionar" ForeColor="Black" Font-Size="Small">Seleccionar</asp:LinkButton>
                                </HeaderTemplate>
                                 <ItemTemplate>
                                     <asp:CheckBox ID="ckbSeleccionar" runat="server" AutoPostBack="true" OnCheckedChanged="SeleccionarGuias" />
                                 </ItemTemplate>
                                 <HeaderStyle VerticalAlign="Middle" />
                                 <ItemStyle Font-Italic="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                             </asp:TemplateField>                             
                             <asp:BoundField DataField="IdDocumento" HeaderText="IdDocumento" Visible="false" />
                             <asp:BoundField DataField="IdSerie" HeaderText="IdSerie" Visible="false" />
                             <asp:BoundField DataField="IdTienda" HeaderText="IdTienda" Visible="false" />
                             <asp:BoundField DataField="numero" HeaderText="Nro.Doc" />
                             <asp:BoundField DataField="peso" HeaderText="Peso"  DataFormatString="{0:F2}" >
                                <ItemStyle HorizontalAlign="Right" />
                             </asp:BoundField>
                             <asp:BoundField DataField="cliente" HeaderText="Cliente" >
                               <ItemStyle HorizontalAlign="Center" />
                             </asp:BoundField>
                             <asp:BoundField DataField="distrito" HeaderText="Distrito" >
                               <ItemStyle HorizontalAlign="Center" />
                             </asp:BoundField>
                             <asp:BoundField DataField="tie_Nombre" HeaderText="Tienda" >
                               <ItemStyle HorizontalAlign="Center" />
                             </asp:BoundField>
                             <asp:BoundField DataField="mt_Nombre" HeaderText="Mot. Traslado" >
                               <ItemStyle HorizontalAlign="Center" />
                             </asp:BoundField>                                
                             <asp:TemplateField HeaderText="Detalle Doc.">
                               <ItemTemplate>                               
                                 <asp:ImageButton ID="imgVerGuiasReferencias" runat="server" ImageUrl="~/Imagenes/Lapiz_A.JPG" ToolTip="Ver Detalle Documento" />
                               </ItemTemplate>
                                 <ItemStyle HorizontalAlign="Center" />
                              </asp:TemplateField>
                                  <asp:CommandField />
                            </Columns>                                            
                            <FooterStyle CssClass="GrillaFooter" />                                            
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                       </asp:GridView>
                    </div>
                    <div style="float:left;width:100%;border-top:10px" align="center" class="TituloCelda">
                        <span style="font-weight:bold;">GUIAS ASIGNADAS</span>
                    </div>
                    <div style="float:left;width:100%;padding-top:3px;height:300px;overflow:scroll">
                    <asp:GridView ID="gvGuiasSeleccionadas" runat="server" AutoGenerateColumns="False" DataKeyNames="IdTienda,IdSerie,IdDocumento"
                            Width="100%" ShowHeaderWhenEmpty="true" >
                    <HeaderStyle CssClass="GrillaHeader"  HorizontalAlign="Center"/>
                    <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbQuitar" runat="server" Font-Italic="true" CommandName="Quitar" CommandArgument='<%# Container.DataItemIndex %>'><%# Container.DataItemIndex + 1 %>Quitar</asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="numero" HeaderText="Nro.Doc" />
                    <asp:TemplateField HeaderText="Peso">
                        <ItemTemplate>
                            <asp:TextBox ID="txtTemplatePeso" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"peso") %>' onkeyUp="return(valorPesoKeyUp());"></asp:TextBox>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>                    
                    <asp:BoundField DataField="cliente" HeaderText="Cliente" >                        
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="distrito" HeaderText="Distrito" >
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="tie_Nombre" HeaderText="Tienda" >
                    <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>                                    
                    <asp:TemplateField HeaderText="Entregado">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" ToolTip="Activar Fecha de Despacho" onclick="marcarCheck(this)" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                        <HeaderTemplate>
                            <asp:CheckBox ID="CheckBox2" runat="server" ToolTip="Marcar Todos" onclick="marcarTodos(this);" />
                        </HeaderTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fec. Despacho">
                        <ItemTemplate>
                            <asp:TextBox ID="txtFecDespachoFinal" runat="server" disabled="true"
                            Text='<%#DataBinder.Eval(Container.DataItem,"fecfinDespacho")%>'></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtFecDespachoFinal" Format="dd/MM/yyyy HH:mm" />
                            <cc1:MaskedEditExtender ID="txtFecDespachoFinal_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999 99:99" TargetControlID="txtFecDespachoFinal" AcceptAMPM="true" >
                                </cc1:MaskedEditExtender>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"/>
                    </asp:TemplateField>
                    <asp:BoundField DataField="IdDocumento" HeaderText="IdDocumento" Visible="false" />
                    <asp:BoundField DataField="IdSerie" HeaderText="IdSerie" Visible="false" />
                    <asp:BoundField DataField="IdTienda" HeaderText="IdTienda" Visible="false" />
                    </Columns>
                    <RowStyle CssClass="GrillaRow" />
                    </asp:GridView>
                    </div>
                </asp:Panel>
            </div>
            </div>
        </div>
        </ContentTemplate>
        </asp:UpdatePanel>
            <div id="capaVehiculo" style="border: 3px solid blue; padding: 8px; width: 560px;
            height: auto; position: absolute; top: 100px; left: 87px; background-color: white;
            z-index: 2; display: none;">
            <table style="width: 100%;">
                <tr>
                    <td align="right">
                        <asp:ImageButton ID="ImageButton5" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                            OnClientClick="return(offCapa('capaVehiculo'));" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlCheckBox" runat="server">
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="Texto" RepeatDirection="Horizontal" AutoPostBack="true">
                            <asp:ListItem Text = "Vehiculos Sanicenter." Value = "1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text = "Agencia - Transportista" Value = "2"></asp:ListItem>                            
                        </asp:RadioButtonList>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GV_Transportista" runat="server" AutoGenerateColumns="False" EmptyDataText="No se encontrarón registros" EmptyDataRowStyle-ForeColor="Red" ShowHeader="true" 
                                    Width="100%" AllowPaging="True" DataKeyNames="IdPersona">
                                    <RowStyle CssClass="GrillaRow" />
                                    <HeaderStyle CssClass="GrillaHeader"  VerticalAlign="Middle" />
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False"
                                                    CommandName="Select" Text="Seleccionar"></asp:LinkButton>
                                                <asp:HiddenField ID="hddindice" runat="server" Value='<%# Container.DataItemIndex %>' />
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>                                                                                
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTransportista" runat="server" Font-Bold="true" 
                                                    ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"Transportista")%>'></asp:Label>
                                                <asp:HiddenField ID="HiddenField2" runat="server" 
                                                    Value='<%#DataBinder.Eval(Container.DataItem,"idPersona")%>' />
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <asp:TextBox ID="TxtFiltroTransportista" onkeypress="return (validarEnter(event));" runat="server" Width="100px">
                                                 </asp:TextBox>
                                                 <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="ontextChanged" />
                                            </HeaderTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Doc_identidad" HeaderText="R.U.C/D.N.I" />
                                        <asp:BoundField DataField="tel_numero" HeaderText="Telefono" />
                                        <asp:BoundField DataField="distrito" HeaderText="Distrito"  />
                                    </Columns>
                                </asp:GridView>
                      <asp:GridView ID="GV_Vehiculo" runat="server" AutoGenerateColumns="false" Width="100%" DataKeyNames="Modelo,ConstanciaIns,veh_Capacidad,Placa,IdProducto" >
                        <Columns>
                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="true" ButtonType="Link"
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Nro. Placa" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblNroPlaca" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Placa")%>'></asp:Label>                                                
                                    <asp:HiddenField ID="hddIdVehiculo" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Modelo" HeaderText="Modelo" NullDisplayText="---" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="ConstanciaIns" HeaderText="Const. Inscripción" NullDisplayText="---"
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="veh_Capacidad" HeaderText="capacidad" NullDisplayText="---"
                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="RadioButtonList1" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            </div>
            <div id="capaAnular" style="border: 3px solid blue; padding: 8px; width: 360px;
            height: auto; position: absolute; top: 100px; left: 450px; background-color: white;
            z-index: 2; display: none;">
                <table width="100%">
                    <tr>
                        <td align="right" colspan="2">
                            <img alt="" src="../../Imagenes/Cerrar.gif" onclick="offCapa('capaAnular')" />
                            <%--<asp:ImageButton ID="ImageButton1" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                            OnClientClick="return(offCapa('capaAnular'));" />--%>
                        </td>
                    </tr>
                    <tr align="left">
                         <td>Tipo Anulación</td>
                         <td>
                            <asp:DropDownList ID="ddlTipoAnulacion" runat="server" Width="200px" 
                                 DataSourceID="SqlDataSource1" DataTextField="desc_anulacion" 
                                 DataValueField="id_tbl_anulacion">
                            </asp:DropDownList>
                            <asp:Button ID="btnCrearTipoAnulacion" runat="server" Text="..." />
                             <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                 ConnectionString="<%$ ConnectionStrings:Conexion %>" 
                                 SelectCommand="select id_tbl_anulacion,desc_anulacion from TBL_TIPO_ANULACION where estado=1"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>Comentario:</td>
                        <td>
                            <asp:TextBox ID="txtComentarioAnulacion" runat="server" TextMode="MultiLine" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:button ID="btnTipoanulacion" runat="server" Text="Anular" />
                        </td>
                    </tr>
                </table>
            </div>
    <script type="text/javascript" language="javascript">
        function SelectGuias() {
            var grillaOrigen = document.getElementById('<%=gvGuias.ClientID %>');
            var grillaDestino = document.getElementById('<%=gvGuiasSeleccionadas.ClientID %>');
            var seleccion = false;
            for (var i = 1; i < grillaOrigen.rows.length; i++) {
                if (grillaOrigen.rows[i].cells[0].getElementsByTagName('INPUT')[0].checked) {
                    //var nroDoc = grillaOrigen.rows[i].cells[1].innerHTML;
                    var tbod = grillaDestino.rows[0].parentNode;
                    var newRow = grillaOrigen.rows[i].cloneNode(true);
                    tbod.appendChild(newRow);
                    grillaOrigen.rows[i].style.display = "none";
                    break;
                    //                    //crea nueva fila
                    //                    var nuevaFila = grillaDestino.insertRow();

                    //                    //crea nueva celda
                    //                    var newCell = nuevaFila.insertCell();

                    //                    //crea nuevo elemento dentro de la celda
                    //                    var newCheckBox = document.createElement('input'); newCheckBox.type = 'checkbox';
                    //                    newCell.appendChild(newCheckBox);

                    //                    //nueva celda
                    //                    newCell = nuevaFila.insertCell();
                    //                    var newValue = document.createTextNode(nroDoc);
                    //                    newCell.appendChild(newValue);

                    //                    //grillaDestino.rows[0].cells[1].children[0].innerHTML = grillaOrigen.rows[i].cells[1].children[0].innerHTML;
                    // break;
                }
            }
            return true;
        }

        function validarEnter(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            if (caracter == 13) {
                return false;
            }                
        }

        function carga() {
            posicion = 0;
            
            // IE
            if (navigator.userAgent.indexOf("MSIE") >= 0) navegador = 0;
            // Otros
            else navegador = 1;
        }

        function evitaEventos(event) {
            // Funcion que evita que se ejecuten eventos adicionales
            if (navegador == 0) {
                window.event.cancelBubble = true;
                window.event.returnValue = false;
            }
            if (navegador == 1) event.preventDefault();
        }

        function comienzoMovimiento(event, id) {
            elMovimiento = document.getElementById(id);

            // Obtengo la posicion del cursor
            if (navegador == 0) {
                cursorComienzoX = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
                cursorComienzoY = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;

                document.attachEvent("onmousemove", enMovimiento);
                document.attachEvent("onmouseup", finMovimiento);
            }
            if (navegador == 1) {
                cursorComienzoX = event.clientX + window.scrollX;
                cursorComienzoY = event.clientY + window.scrollY;

                document.addEventListener("mousemove", enMovimiento, true);
                document.addEventListener("mouseup", finMovimiento, true);
            }

            elComienzoX = parseInt(elMovimiento.style.left);
            elComienzoY = parseInt(elMovimiento.style.top);
            // Actualizo el posicion del elemento
            elMovimiento.style.zIndex = ++posicion;

            evitaEventos(event);
        }

        function enMovimiento(event) {
            var xActual, yActual;
            if (navegador == 0) {
                xActual = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
                yActual = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
            }
            if (navegador == 1) {
                xActual = event.clientX + window.scrollX;
                yActual = event.clientY + window.scrollY;
            }

            elMovimiento.style.left = (elComienzoX + xActual - cursorComienzoX) + "px";
            elMovimiento.style.top = (elComienzoY + yActual - cursorComienzoY) + "px";

            evitaEventos(event);
        }

        function finMovimiento(event) {
            if (navegador == 0) {
                document.detachEvent("onmousemove", enMovimiento);
                document.detachEvent("onmouseup", finMovimiento);
            }
            if (navegador == 1) {
                document.removeEventListener("mousemove", enMovimiento, true);
                document.removeEventListener("mouseup", finMovimiento, true);
            }
        }

        window.onload = carga;
//        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
//        function EndRequestHandler(sender, args) {
//            if (args.get_error() != undefined) {
//                args.set_errorHandled(true);
//            }
        //        }
        function calcularCapacidadEditable() {
            var disponible = parseFloat(document.getElementById("<%=txtCapacidadTotal.clientID %>").value);
            var usado = parseFloat(document.getElementById("<%=txtCapacidadUsada.clientID %>").value);
            var sinUsar = parseFloat(document.getElementById("<%=txtCapacidadSinUsar.clientID %>").value);
            document.getElementById("<%=txtCapacidadSinUsar.clientID %>").value = (disponible - usado).toFixed(2);
        }

        function revalidarCapacidad() {
            var disponible = document.getElementById("<%=txtCapacidadTotal.clientID %>");
            var usado = document.getElementById("<%=txtCapacidadUsada.clientID %>");
            var sinUsar = document.getElementById("<%=txtCapacidadSinUsar.clientID %>");
            var buttonGuardar = document.getElementById("<%=btnGuardarDespacho.clientID %>");
            if (parseFloat(disponible.value) < parseFloat(usado.value)) {
                buttonGuardar.disabled = true;
                alert("Peso Disponible no puede ser menor que el peso usado, para continuar debe ingresar un peso disponible mayor al peso usado y/o quitar guias a esta programación");
                disponible.focus();
            } else {buttonGuardar.disabled = false; }
        }

        function valorPesoKeyUp() {
            var disponible = document.getElementById("<%=txtCapacidadTotal.clientID %>");
            var usado = document.getElementById("<%=txtCapacidadUsada.clientID %>");
            var sinUsar = document.getElementById("<%=txtCapacidadSinUsar.clientID %>");
            var grillaSeleccion = document.getElementById("<%=gvGuiasSeleccionadas.clientID %>");
           var resultado = 0
           for (var i = 1; i < grillaSeleccion.rows.length; i++) {
               var pesoAdicional = parseFloat(grillaSeleccion.rows[i].cells[2].children[0].value);
               if (isNaN(pesoAdicional)) {pesoAdicional = 0;}
               var resultado = pesoAdicional + parseFloat(resultado);
               if (isNaN(resultado)) { resultado = 0; }
               if (parseFloat(resultado) > parseFloat(disponible.value)) {
                   alert("El peso ingresado excedió la capacidad total del vehiculo, vuelva a intentarlo.");
                   grillaSeleccion.rows[i].cells[2].children[0].value = 0;
                   document.getElementById("<%=txtCapacidadUsada.clientID %>").value = parseFloat(disponible.value) - parseFloat(sinUsar.value);
                   grillaSeleccion.rows[i].cells[2].children[0].select();
                   grillaSeleccion.rows[i].cells[2].children[0].focus();
                   return false
               } else {
                   document.getElementById("<%=txtCapacidadUsada.clientID %>").value = parseFloat(resultado).toFixed(2);
                   document.getElementById("<%=txtCapacidadSinUsar.clientID %>").value = parseFloat(parseFloat(disponible.value) - parseFloat(resultado)).toFixed(2);
               }
           }
           return false;
       }
       function marcarTodos(objeto) {
           var dt = new Date();
           var fecha = dt.format("dd/MM/yyyy");
           var grilla = document.getElementById("<%=gvGuiasSeleccionadas.clientID %>");
           var casilla = true;
            for (var i = 1; i < grilla.rows.length; i++) {
                grilla.rows[i].cells[6].getElementsByTagName('INPUT')[0].checked = objeto.checked;
                casilla = objeto.checked;
                grilla.rows[i].cells[7].children[0].value = fecha + " " + dt.getHours() + ":" + dt.getMinutes();
                grilla.rows[i].cells[7].children[0].disabled = !casilla;
            }
        }

        function marcarCheck(objeto) {
            var dt = new Date();
            var fecha = dt.format("dd/MM/yyyy");            

            var grilla = document.getElementById("<%=gvGuiasSeleccionadas.clientID %>");
            var casilla = true;
            for (var i = 1; i < grilla.rows.length; i++) {
                if (grilla.rows[i].cells[6].getElementsByTagName('INPUT')[0].checked == objeto.checked) {
                    casilla = objeto.checked;
                    grilla.rows[i].cells[7].children[0].value = fecha + " " + dt.getHours() + ":" + dt.getMinutes();
                    grilla.rows[i].cells[7].children[0].disabled = !casilla;
                }
            }
        }

        function mostrarCapaPersona() {
            onCapa('capaDespachos');
            return true;
        }
        function buscarCliente() {
            mostrarCapaPersona();
            return true;
        }
        function Filtros(fromTextBox, toTextBox) {
            var length = fromTextBox.value.length;
            var maxLength = fromTextBox.getAttribute("maxLength");
            if (length == maxLength) {                                
                    document.getElementById(toTextBox).focus();
            }
        }

        function valOnClick_btnBuscarVehiculo() {
            onCapa('capaVehiculo');
            return false;
        }

        function verGuias(objeto) {
            onCapa('capaDespachos');
            return false;
        }

        function btnGuardar() {
            var vartxtPlaca = document.getElementById("<%=txtPlaca.clientID %>");
            var txtmodelo = document.getElementById("<%=txtmodelo.clientID %>");
            var ddlTurno = document.getElementById("<%=ddlTurno.clientID %>");
            var txtFecha = document.getElementById("<%=txtFecha.clientID %>");
            var txthora = document.getElementById("<%=txthora.clientID %>");

            var varConductor = document.getElementById("<%=ddlConductor.clientID %>");
            var varAyudante = document.getElementById("<%=ddlAyudante.clientID %>");
            var rb = document.getElementById("<%=RadioButtonList1.clientID %>");
            var grilla = document.getElementById("<%=gvGuiasSeleccionadas.clientID %>");
            var radio = rb.getElementsByTagName("input");
            var filas = 0
            if (grilla != null) {
                filas = grilla.rows.length;
            }
            if (filas == 0) {
                alert("No se permite la operación, debe asignar como mínimo una guia a esta programación.");
                return false;
            }
            if (radio[0].checked) {
                if (vartxtPlaca.value.length == 0) {
                    alert("Debe ingresar Nro de placa válido");
                    return false;
                }
                if (varConductor.value.length == 0 && varAyudante.value.length == 0) {
                    alert("Datos incompletos en DATOS GENERALES - [VEHÍCULO] ");
                    return false;
                }

            } else {
            return confirm(".::Programación::. \n" + txtmodelo.value + "\n Turno: " + ddlTurno.options[ddlTurno.selectedIndex].text + "\n Fecha: " + txtFecha.value + " " + txthora.value);                
            }
        }

        function Abrir_ventana(pagina) {
        var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width=1200, height=700, top=0, left=0";
        window.open(pagina,"Guia Remisión - [Detalle]",opciones);
        }

        function ValidarPeso() {
            var capacidadTotal = document.getElementById("<%=txtCapacidadTotal.clientID %>");
            var capacidadUsada = document.getElementById("<%=txtCapacidadUsada.clientID %>");        
            var grilla = document.getElementById('<%=gvGuias.ClientID%>');        
            if (capacidadUsada.value = capacidadTotal.value) {
                alert("Limite de carga completo para estevehiculo \n No es posible asignar mas peso.")
                return false;
            }
            if (capacidadUsada.value > capacidadTotal.value) {
                alert("Carga excede la capacidad total de este vehiculo\n No es posible asignar mas peso.")
                return false;
            }
            return true;
        }

    </script>
</asp:Content>
