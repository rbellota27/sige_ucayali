﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmZonaUbigeo.aspx.vb" Inherits="APPWEB.FrmZonaUbigeo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">

        function validarNumeroPunto(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter== 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }

        function valBlur(event) {
            var id = event.srcElement.id;
            var caja = document.getElementById(id);
            if (!esDecimal(caja.value)) {
                caja.select();
                caja.focus();
                alert('Valor no válido.');
            }
        }

        function validar(e) { // 1
            tecla = (document.all) ? e.keyCode : e.which; // 2
            if (tecla == 8) return true; // 3
            patron = /[A-Za-z\s]/; // 4
            te = String.fromCharCode(tecla); // 5
            return patron.test(te); // 6
        }

        function validarSave() {
            var caja = document.getElementById('<%=TxtNombre.ClientID %>');
            if (caja.value.length == 0) {
                alert('Debe ingresar nombre de Zona');
                var cajaCod = document.getElementById('<%=TxtNombre.ClientID %>');
                cajaCod.select();
                cajaCod.focus();
                return false;
            }
            caja = document.getElementById('<%=TxtDescripcion.ClientID %>');
            if (caja.value.length == 0) {
                alert('Debe ingresar una Descripcion.');
                caja.select();
                caja.focus();
                return false;
            }
            caja = document.getElementById('<%=TxtValor.ClientID %>');
            if (caja.value.length == 0) {
                alert('Debe ingresar una Descripcion.');
                caja.select();
                caja.focus();
                return false;
            }
            var rdbEstado = document.getElementById('<%=rdbEstado.ClientID %>');
            var hddModo = document.getElementById('<%=hddModo.ClientID %>');

            return (confirm('Desea Continuar con la operación'));
        }
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%;">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='/Imagenes/Nuevo_A.JPG';"
                            onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" />
                        <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                            onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                            OnClientClick="return(validarSave());" CausesValidation="true" Style="height: 26px" />
                        <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                            onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';"
                            OnClientClick="return(confirm('Desea cancelar el proceso?'));" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 23px">
                        <asp:Panel ID="Panel_Perfil" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="TituloCelda" style="width: 880px">
                                        ZONA UBIGEO
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 75px">
                                                    <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Nombre:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="TxtNombre" runat="server" onkeypress="return validar(event)"></asp:TextBox>
                                                    <asp:TextBox ID="TxtCodigo" runat="server" BackColor="Silver" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 75px">
                                                    <asp:Label ID="Label2" runat="server" Text="Descripción:" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="TxtDescripcion" runat="server" onkeypress="return validar(event)"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 75px">
                                                    <asp:Label ID="Label3" runat="server" Text="Valor:" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cboMoneda" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="TxtValor" runat="server" onKeypress="return(validarNumeroPunto(event));"
                                                        onblur="return(valBlur(event));"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 75px">
                                                    <asp:Label ID="lblEstado" runat="server" Text="Estado:" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rdbEstado" runat="server" CssClass="Label" RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="1">Activo</asp:ListItem>
                                                        <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="TituloCelda">
                        Filtro
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_Busqueda" runat="server">
                            <table>
                                <tr>
                                    <td style="width: 983px">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label25" runat="server" Text="Estado:" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rdbEstadoBusqueda" runat="server" CssClass="Label" RepeatDirection="Horizontal"
                                                        AutoPostBack="true">
                                                        <asp:ListItem Value="">Todos </asp:ListItem>
                                                        <asp:ListItem Value="1" Selected="True">Activo</asp:ListItem>
                                                        <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 983px">
                                        <asp:GridView ID="DGVZonaUbigeo" runat="server" AllowPaging="True" PageSize="30"
                                            AutoGenerateColumns="False" Width="100%">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminar_Click">Quitar</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IdZona" HeaderText="Id" NullDisplayText="---" />
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" NullDisplayText="---" />
                                                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" NullDisplayText="---" />
                                                <asp:BoundField DataField="Moneda" HeaderText="SimboloMoneda" NullDisplayText="---">
                                                    <ControlStyle Width="0px" />
                                                    <HeaderStyle Width="0px" />
                                                    <ItemStyle Width="0px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="MontoMin" HeaderText="MontoMinimo" />
                                                <asp:BoundField DataField="strEstado" HeaderText="Estado" />
                                            </Columns>
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <RowStyle CssClass="GrillaRow" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="hddModo" runat="server" />
                        <asp:HiddenField ID="hddLogin" runat="server" Value="" />
                        <asp:HiddenField ID="hddClave" runat="server" Value="" />
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
