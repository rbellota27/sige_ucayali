<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmVentasxVendedor.aspx.vb" Inherits="APPWEB.FrmVentasxVendedor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td align="center" class="TituloCelda">
                Ventas por Vendedor
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel_EmpresaTienda" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td align="center">
                                    <table cellspacing="1">
                                        <tr>
                                            <td class="Label">
                                                Empresa:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="false">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Label">
                                                Tienda:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="false" Enabled="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellspacing="1">
                                        <tr>
                                            <td class="Label">
                                                Linea:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true" Enabled="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Label">
                                                Sublinea:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboSublinea" runat="server" AutoPostBack="false" Enabled="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Label">
                            Fecha Inicio:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" TargetControlID="txtFechaInicio">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="Label">
                            Fecha Fin:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaFin" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" TargetControlID="txtFechaFin">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td valign="top" align="left">
                            <fieldset title="Tipo Reporte" class="FieldSetPanelReport">
                                <legend>Tipo Reporte: </legend>
                                <asp:RadioButtonList ID="rbtTipoReporte" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="1" Text="Detallado"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Resumido"></asp:ListItem>
                                </asp:RadioButtonList>
                            </fieldset>
                        </td>
                        <td valign="top" align="left">
                            <fieldset title="Agrupado Por:" class="FieldSetPanelReport">
                                <legend>Agrupado Por: </legend>
                                <asp:RadioButtonList ID="rbtlTpReporte" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Vendedor" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Fecha Emision" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel_Cliente" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td class="TituloCeldaLeft">
                                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="PnlCliente"
                                        CollapsedSize="0" ExpandedSize="80" Collapsed="true" ExpandControlID="ImgCliente"
                                        CollapseControlID="ImgCliente" TextLabelID="lblCliente" ImageControlID="ImgCliente"
                                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                        CollapsedText="Buscar Vendedor" ExpandedText="Buscar Vendedor" ExpandDirection="Vertical"
                                        SuppressPostBack="True" />
                                    <asp:Image ID="ImgCliente" runat="server" Width="16px" />
                                    <asp:Label ID="lblCliente" runat="server" Text="Buscar Vendedor" CssClass="LabelBlanco"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="PnlCliente" runat="server">
                                        <table cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="btnBuscarCliente" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                        onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                                        OnClientClick="return(mostrarCapaPersona());" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label12024" runat="server" CssClass="Label" Text="Tipo de Persona"></asp:Label>
                                                    <asp:DropDownList ID="cboTipoPersona" runat="server" Width="116px" Font-Bold="True"
                                                        Enabled="False">
                                                        <asp:ListItem>---------</asp:ListItem>
                                                        <asp:ListItem>Natural</asp:ListItem>
                                                        <asp:ListItem>Juridica</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Limpiar_b.JPG"
                                                        onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';" onmouseout="this.src='../../Imagenes/Limpiar_b.JPG';"
                                                        OnClientClick="return(LimpiarControlesCliente());" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 139px; text-align: right;">
                                                    <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Nombre"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRazonSocial" CssClass="TextBoxReadOnly" runat="server" Width="521px"
                                                        MaxLength="50" ReadOnly="True"></asp:TextBox>
                                                    <asp:TextBox ID="txtCodigoCliente" CssClass="TextBoxReadOnly" runat="server" Width="99px"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; width: 139px">
                                                    <asp:Label ID="Label3" runat="server" Text="Dni" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDni" runat="server" Width="70px" CssClass="TextBoxReadOnly" Style="margin-left: 0px;
                                                        margin-bottom: 0px" MaxLength="8" ReadOnly="True"></asp:TextBox>
                                                    <asp:Label ID="Label4" runat="server" Text="Ruc" CssClass="Label"></asp:Label>
                                                    <asp:TextBox ID="txtRuc" runat="server" Width="90px" MaxLength="11" CssClass="TextBoxReadOnly"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="gvBuscar" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:ImageButton ID="btnAceptar" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                            OnClientClick="return(mostrarReporte());" onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';"
                            onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                </iframe>
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btBuscarPersonaGrilla">                                                                        
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                                                             
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddl_Rol" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="Página Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="Página Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">

        function mostrarReporte() {
            var fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
            var fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
            var IdEmpresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
            var IdTienda = document.getElementById('<%= cboTienda.ClientID%>').value;
            var idcliente = document.getElementById('<%= txtCodigoCliente.ClientID%>').value;
            var linea = document.getElementById('<%=cbolinea.ClientID %>').value;
            var sublinea = document.getElementById('<%=cbosublinea.ClientID %>').value;

            var NomEmpresa = '';
            var NomTienda = '';
            if (idcliente == '') {
                idcliente = 0;
            } else {
                idcliente = idcliente;
            }

            var cbo = document.getElementById('<%= cmbEmpresa.ClientID%>');
            for (var i = 0; i < cbo.length; i++) {
                if (IdEmpresa == cbo[i].value) {
                    NomEmpresa = cbo[i].text;
                    break;
                }
            }
            cbo = document.getElementById('<%= cboTienda.ClientID%>');
            for (var i = 0; i < cbo.length; i++) {
                if (IdTienda == cbo[i].value) {
                    NomTienda = cbo[i].text;
                    break;
                }
            }



            var rdbList = document.getElementById('<%=rbtTipoReporte.ClientID %>');
            var tipo = 0;

            var radioTipoReporte = rdbList.getElementsByTagName("INPUT");
            if (radioTipoReporte[0].checked == true) {
                tipo = radioTipoReporte[0].value;

            } else if (radioTipoReporte[1].checked == true) {
                tipo = radioTipoReporte[1].value;
            }


            var rblist = document.getElementById('<%=rbtlTpReporte.ClientID%>');
            var radioTipoReporte = rblist.getElementsByTagName("INPUT");
            var reporte = 0;
            if (radioTipoReporte[0].checked == true) {
                reporte = 1;
            } else if (radioTipoReporte[0].checked == true) {
                reporte = 2;
            } else {
                reporte = 1;
            }

            frame1.location.href = '../../compras/Reportes/visorCompras.aspx?iReporte=6&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + IdEmpresa + '&IdTienda=' + IdTienda + '&NomEmpresa=' + NomEmpresa + '&NomTienda=' + NomTienda + '&idcliente=' + idcliente + '&tipo=' + tipo + '&reporte=' + reporte + '&linea=' + linea + '&sublinea=' + sublinea;
            return false;
        }




        function onCapaCliente(s) {
            LimpiarControlesCliente();
            onCapa(s);
            document.getElementById('<%= txtRazonSocial.ClientID%>').focus();
            return false;
        }
        void function LimpiarControlesCliente() {
            document.getElementById('<%= txtRazonSocial.ClientID%>').value = "";
            document.getElementById('<%= txtDNI.ClientID%>').value = "";
            document.getElementById('<%= txtRUC.ClientID%>').value = "";
            document.getElementById('<%= txtCodigoCliente.ClientID %>').value = "";
            return false;
        }



        function validarNumeroPunto(event) {
            var key = event.keyCode;
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }



        //Buscar SubLinea
        function ValidarEnteroSinEnter() {
            if (onKeyPressEsNumero('event') == false) {
                alert('Caracter no válido. Solo se permiten números Enteros');
                return false;
            }
            return true;
        }


        //End Buscar SubLinea
        //Buscar Cliente

        function BuscarCliente() {

            return true;
        }


        function ValidarEnter() {
            return (!esEnter(event));
        }
        function ValidaEnter() {
            if (esEnter(event) == true) {
                alert('Tecla No permitida');
                caja.focus();
                return false;
            }
            return true;
        }



        function mostrarCapaPersona() {
            onCapa('capaPersona');
            return false;
        }
        function addPersona_Venta() {
            offCapa('capaPersona');

            return false;
        }


        function aceptarFoco(caja) {
            caja.select();
            caja.focus();
            return true;
        }


        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj) {
            var key = event.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

    </script>

</asp:Content>
