﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmMantTipoDocumentoAE.aspx.vb" Inherits="APPWEB.FrmMantTipoDocumentoAE" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <script type="text/javascript" >
      Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
      Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
          if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
              offsetX = Math.floor(offsetX);
              offsetY = Math.floor(offsetY);
          }
          this._origOnFormActiveElement(element, offsetX, offsetY);
      };
      function validarSoloNumeroEntero() {
          var key = window.event.keyCode; //codigo de tecla.
          
          if (!onKeyPressEsNumero('event')) {
               return false;
          }


      } 


      function validarSoloNumerodecimal(Valor) {
          var key = event.keyCode;
          var coma;
          var punto;
          coma = false;
          punto = false;

          if (!esDecimal(Valor.value)) {
                    return false;
                }
                if (key == 44) {
                    if (coma == false) {
                        coma = true;
                    } else {
                        alert('Ya ha escrito una coma');
                        return false;
                    }
                }
                if (key == 46) {
                    if (punto == false)
                    { punto = true; }
                    else {
                        alert('Ya ha escrito un punto');
                        return false;
                    }

                }

          }

          function valSaveTipoDocumentoAE() {

              var cboarea = document.getElementById('<%=CboArea.ClientID%>');
              if (isNaN(parseInt(cboarea.value)) || parseInt(cboarea.value) <= 0 || cboarea.value.length <= 0) {
                  alert('Seleccione un Área.');
                  return false
              }

              var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
              if (isNaN(parseInt(cboEmpresa.value)) || parseInt(cboEmpresa.value) <= 0 || cboEmpresa.value.length <= 0) {
                  alert('Seleccione una Empresa.');
                  return false
              }

              var cboTipoDocumento = document.getElementById('<%=CboTipoDocumento.ClientID%>');
              if (isNaN(parseInt(cboTipoDocumento.value)) || parseInt(cboTipoDocumento.value) <= 0 || cboTipoDocumento.value.length <= 0) {
                  alert('Seleccione un Tipo de Documento.');
                  return false
              }


              var txtcantidadfilas = document.getElementById('<%=txtCantidadFilas.ClientID %>');
              if (isNaN(parseInt(txtcantidadfilas.value)) || txtcantidadfilas.value.length <= 0) {
                  txtcantidadfilas.value = '0';
              }
              var txtmaxNoAfectado = document.getElementById('<%=txtMaxNoAfectado.ClientID %>');
              if (isNaN(parseInt(txtmaxNoAfectado.value)) || txtmaxNoAfectado.value.length <= 0) {
                  txtmaxNoAfectado.value = '0';
              }
              var txtmontoMaxAfectado = document.getElementById('<%=txtMontoMaxAfectado.ClientID %>');
              if (isNaN(parseInt(txtmontoMaxAfectado.value)) || txtmontoMaxAfectado.value.length <= 0) {
                  txtmontoMaxAfectado.value = '0';
              }              

              return (confirm('Desea continuar con la Operación ?'));
          }

  
  </script>
     
     <table style="width: 100%" >
     <tr>
     <td style="text-align: left">
     <asp:Label ID="lblMensaje" CssClass="LabelRojo" runat="server" Font-Bold ="true" ></asp:Label>
     </td>
    </tr>
    <tr>
                   <td class="TituloCelda" style="height: 21px">Tipo Documento AE</td>
    </tr>
    
   </table>
   <table>
   <tr>
   <td >
   
                                    <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" 
                                         />
   
   </td>
       <td>
                                    <asp:ImageButton ID="btnGuardar" runat="server" 
                                        ImageUrl="~/Imagenes/Guardar_B.JPG" OnClientClick="return(valSaveTipoDocumentoAE());" 
               style="height: 26px" />                                    
                                    </td>
       <td>
                                    <asp:ImageButton ID="btnCancelar" runat="server" 
                                        ImageUrl="~/Imagenes/Cancelar_B.JPG"  OnClientClick="return( confirm('Desea cancelar el proceso?')   );"
                                        />                                    
                                </td>
   </tr>
   </table>
    <asp:Panel ID="PanelTipoDocumentoAE" runat="server">
    <table width="100%">
    <tr>
    <td><table style="width: 100%">
        
      
       <tr>
         <td style="width: 176px; text-align: right">
                  <asp:Label ID="Label6" runat="server" CssClass="Label" Text="Empresa:" 
                  Width="105px"></asp:Label>
        </td>
       <td style="width: 176px">
                  <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True" 
                      style="height: 22px">
                  </asp:DropDownList>
       </td>
         <td style="width: 55px; text-align: right">
                  <asp:Label ID="Label7" runat="server" CssClass="Label" Text="Área:" 
                  Width="55px"></asp:Label>
        </td>
        <td style="width: 149px">
                  <asp:DropDownList ID="CboArea" runat="server"   >
                  </asp:DropDownList>
       </td>
         <td style="width: 96px; text-align: right">
                  <asp:Label ID="Label8" runat="server" CssClass="Label" Text="Tipo Documento:" 
                  Width="100px" Height="16px"></asp:Label>
        </td>
        <td>
                  <asp:DropDownList ID="CboTipoDocumento" runat="server">
                  </asp:DropDownList>
       </td>
       </tr>
       </table></td>    
    </tr>
    <tr>
    <td>
    <table>
        <tr>
        <td style="text-align: right">
                  <asp:Label ID="lblCantFilas" runat="server" CssClass="Label" Text="Cantidad Filas:" 
                  Width="105px"></asp:Label>
        </td>
        <td style="width: 175px">
        
            <asp:TextBox ID="txtCantidadFilas" onKeypress="return(validarSoloNumeroEntero());"  runat="server"></asp:TextBox>
        
        </td>
        <td style="width: 55px; text-align: right">
                  <asp:Label ID="Label3" runat="server" CssClass="Label" Text="Moneda:" 
                  Width="55px"></asp:Label>
        </td>
        <td>
        
                  <asp:DropDownList ID="cboMoneda" runat="server">
                  </asp:DropDownList>
        
        </td>
         </tr>
        <tr>
          <td style="text-align: right">
                  <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Monto Máx. Prod. Afectos:" 
                  Width="165px"></asp:Label>
        </td>
        <td style="width: 175px">
        
            <asp:TextBox ID="txtMontoMaxAfectado" onKeypress="return(validarNumeroPuntoPositivo('event'));" runat="server"></asp:TextBox>
        
        </td>
        </tr>
        
         <tr>
          <td style="text-align: right">
                  <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Monto Máx. Prod. No Afectos:" 
                  Width="165px"></asp:Label>
        </td>
        <td style="width: 175px">
        
            <asp:TextBox ID="txtMaxNoAfectado" onKeypress="return(validarNumeroPuntoPositivo('event'));" runat="server"></asp:TextBox>
        
        </td>
        </tr>
        
      
       </table>
    
    </td>
    </tr>
    <tr class="LabelRojo" style="font-weight:bold">
    <td>
        *** El proceso de actualización es según la Empresa / Tipo Documento (no se 
        considera el Área).
    </td>
    </tr>
        <tr class="LabelRojo" style="font-weight:bold">
            <td>
                *** En caso del Tipo de Documento [ Factura ], los Montos hacen referencia al 
                Monto Máximo permitido para el pago en [ Efectivo ].</td>
        </tr>
        <tr class="LabelRojo" style="font-weight:bold">
            <td>
                *** En caso del Tipo de Documento [ Boleta ], los Montos hacen referencia al 
                Monto Máximo permitido para el manejo de la Percepción.</td>
        </tr>
    </table>
        </asp:Panel>  
        <asp:Panel ID="Panel_Busqueda" runat="server">
        
        <table width="100%">
        <tr>
        <td>
        <table>
        <tr>
        <td class="Label">Empresa:</td>
        <td><asp:DropDownList ID="cboBusquedaEmpresa" runat="server">
                  </asp:DropDownList></td>
        <td class="Label">Área:</td>
        <td><asp:DropDownList ID="CboBusquedaArea" runat="server">
                  </asp:DropDownList></td>
       <td>
       <asp:ImageButton ID="btnFiltrar" runat="server" 
                ImageUrl="~/Imagenes/Buscar_B.JPG" 
                onmouseout="this.src='/Imagenes/Buscar_B.JPG';" 
                onmouseover="this.src='/Imagenes/Buscar_A.JPG';" Visible="False" />
       </td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
        <td>                
        
        <table>
        <tr>
        <td> <asp:Label ID="lbl_MensajeBusq" runat="server" CssClass="LabelRojo" 
                Text="No se hallaron registros."></asp:Label></td>
        <td><asp:HiddenField ID="HDD_IdEmpresa" runat="server" /></td>
        <td><asp:HiddenField ID="HDD_IdArea" runat="server" /></td>
        <td><asp:HiddenField ID="HDD_IdTipoDocumento" runat="server" /></td>
        </tr>
        </table>
        
        </td>
        </tr>        
        <tr>
        <td>
        
            <asp:Panel ID="Panel_GV_Busqueda" runat="server" ScrollBars="Horizontal" Width="100%">
        <asp:GridView ID="DGV_TipoDocumentoAE" runat="server" AutoGenerateColumns="False" AllowPaging="true" PageSize="40" Width="100%" >
                        <Columns>
                            <asp:TemplateField HeaderText="Eliminar" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblEliminar" OnClick ="Click_Eliminar" OnClientClick="return(confirm('Desea Eliminar el registro?'));" runat="server" >Eliminar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Editar" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbEditar" OnClick ="Click_Editar" runat="server" >Editar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Empresa" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:HiddenField ID="HDD_TipoDocumentoAExIdEmpresa" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"IdEmpresa") %>' />
                                    <asp:Label ID="lbl_NomEmpresa" runat="server" 
                                        Text='<%# DataBinder.Eval(Container.DataItem,"NomEmpresa") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Área" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:HiddenField ID="HDD_TipoDocumentoAExIdArea" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"IdArea") %>' />
                                    <asp:Label ID="lblNomArea" runat="server" 
                                        Text='<%# DataBinder.Eval(Container.DataItem,"NomArea") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tipo de Documento" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:HiddenField ID="HDD_TipoDocumentoAExIdTipoDocumento" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoDocumento") %>' />
                                    <asp:Label ID="lblNomTipoDocumento" runat="server" 
                                        Text='<%# DataBinder.Eval(Container.DataItem,"NomTipoDocumento") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="CantidadFilas" HeaderText="Filas" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="MontoMaximoAfecto" DataFormatString="{0:F2}" HeaderText="Monto Prod. Afectos" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="MontoMaximoNoAfecto" DataFormatString="{0:F2}" HeaderText="Monto Prod. No Afectos" HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Moneda" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdd_IdMoneda" runat="server" 
                                        Value='<%# DataBinder.Eval(Container.DataItem,"IdMoneda") %>' />
                                    <asp:Label ID="lblNomMoneda" runat="server" 
                                        Text='<%# DataBinder.Eval(Container.DataItem,"NomMoneda") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        
                        <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            <RowStyle CssClass="GrillaRow" />
                        
                        
                    </asp:GridView>    
            </asp:Panel>
        
        
        
        </td>
        </tr>
        </table>
        
        </asp:Panel>   
</asp:Content>
