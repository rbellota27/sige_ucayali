﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantTipoTarjeta.aspx.vb" Inherits="APPWEB.FrmMantTipoTarjeta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton ="btnBuscar">

    <table width="100%">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                onmouseover="this.src='/Imagenes/Buscar_A.JPG';" 
                                onmouseout="this.src='/Imagenes/Buscar_B.JPG';" />
                            <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='/Imagenes/Nuevo_A.JPG';"
                                onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" />
                            <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                OnClientClick="return(validarSave());" CausesValidation="true" />
                            <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';"
                                OnClientClick="return(confirm('Desea cancelar el proceso?'));" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Tipo Tarjeta
            </td>
        </tr>
        <tr>
            <td>
                <table>                    
                    <tr>
                        <td class="Label">
                            <asp:Label ID="lblnombre" runat="server" Text="Nombre:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="txtNombre" runat="server" Width="300px"></asp:TextBox>
                        </td>                        
                        <td class="Label">
                            <asp:Label ID="lblEstado" runat="server" Text="Estado:"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rbtlEstado" runat="server" RepeatDirection="Horizontal "
                                CssClass="Label">
                                <asp:ListItem Text="Todos"  Value="2"></asp:ListItem>
                                <asp:ListItem Text="Activo" Selected="True" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>                                
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
              <asp:UpdateProgress ID="upGrilla_Progress" runat="server" DisplayAfter="1000" AssociatedUpdatePanelID="upGrilla">
                        <ProgressTemplate>
                            <label class="Label">
                                Cargando...</label></ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upGrilla" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                <asp:GridView ID="dgvTipoTarjeta" runat="server" AutoGenerateColumns="False" Width="500px" HorizontalAlign="Justify" >
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEditar" runat="server" OnClick="lbtnEditar_Click">Editar</asp:LinkButton>                                
                                <asp:HiddenField ID="hddId" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>'
                                    runat="server" />
                                <asp:HiddenField ID="hddIdEstado" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
<%--                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminar_Click">Eliminar</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" NullDisplayText="" />
                        <asp:BoundField DataField="DescEstado" HeaderText="Estado" NullDisplayText="0" />
                    </Columns>
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <HeaderStyle CssClass="GrillaHeader" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
                </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    </asp:Panel>
    <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
    
    
    <script language="javascript" type="text/javascript">
        function validarSave() {            
            
            var nombre = document.getElementById('<%=txtNombre.ClientID%>');
            if (nombre.value.length == 0) {
                alert('Debe Ingresar un Nombre.');
                nombre.select();
                nombre.focus();
                return false;
            }

            return (confirm('Desea Continuar Con la Operacion'));
       }
    </script>
    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
