<%@ Page Title="" Language="vb" AutoEventWireup="false"      MasterPageFile="~/Principal.Master"     CodeBehind="FrmMantSerie.aspx.vb" Inherits="APPWEB.FrmMantSerie" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="APPWEB" namespace="APPWEB" tagprefix="cc2" %>
<%@ Register assembly="MsgBox" namespace="MsgBox" tagprefix="cc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" language="javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
    function validarLong() {
        return false;
    }
    function calcularLong() {
        var txtNumero = document.getElementById('<%=txtNumero.ClientID %>');
        var txtLongitud = document.getElementById('<%=txtLongitud.ClientID %>');
        txtLongitud.value = txtNumero.value.length;            
    }
    function validarCajaNumero() {
        if (onKeyPressEsNumero('event') == false) {
            alert('Caracter no v�lido. Solo se permiten n�meros.');
            return false;
        }
        return true;
    }
    function validarSave() {
        var txtNumero = document.getElementById('<%=txtNumero.ClientID %>');
        if (CajaEnBlanco(txtNumero)) {
            alert('Ingrese un n�mero');
            txtNumero.focus();
            return false;
        }
        var txtInicio = document.getElementById('<%=txtInicio.ClientID %>');
        if (CajaEnBlanco(txtInicio)) {
            alert('Ingrese un n�mero inicial');
            txtInicio.focus();
            return false;
        }
        var txtCantidad = document.getElementById('<%=txtCantidad.ClientID %>');
        if (CajaEnBlanco(txtCantidad)) {
            alert('Ingrese una cantidad.');
            txtCantidad.focus();
            return false;
        }
        var txtLongitud = document.getElementById('<%=txtLongitud.ClientID %>');
        if (CajaEnBlanco(txtLongitud)) {
            alert('Ingrese una longitud.');
            txtLongitud.focus();
            return false;
        }
        return (confirm('Desea continuar con la operaci�n?'));
    }
    
</script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">     <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" 
                            onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" 
                            onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" />
                        <asp:ImageButton ID="btnGuardar" runat="server" CausesValidation="true" 
                            ImageUrl="~/Imagenes/Guardar_B.JPG" OnClientClick="return(validarSave());" 
                            onmouseout="this.src='/Imagenes/Guardar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Guardar_A.JPG';"
                             />
                        <asp:ImageButton ID="btnCancelar" runat="server" 
                            ImageUrl="~/Imagenes/Cancelar_B.JPG" 
                            onmouseout="this.src='/Imagenes/Cancelar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" />
                        <cc1:ConfirmButtonExtender ID="btnCancelar_ConfirmButtonExtender" 
                            runat="server" ConfirmText="Desea cancelar el proceso?" 
                            TargetControlID="btnCancelar">
                        </cc1:ConfirmButtonExtender>
                        </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_Serie" runat="server">
                     <table width="100%">
                            <tr>
                    <td class="TituloCelda">
                        SERIE</td>
                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" CssClass="Label" runat="server" Text="Empresa:"></asp:Label>
                                        <asp:DropDownList ID="cmbEmpresa" runat="server" 
                                            DataTextField="NombreComercial" DataValueField="Id" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Tienda:"></asp:Label>
                                        <asp:DropDownList ID="cmbTienda" runat="server" DataTextField="Nombre" DataValueField="Id">
                                        </asp:DropDownList>
                                        <asp:Label ID="Label14" CssClass="Label" runat="server" Text="Tipo Documento:"></asp:Label>
                                        <asp:DropDownList ID="cmbTipoDoc" DataTextField="Descripcion" DataValueField="Id" runat="server">
                                        </asp:DropDownList>
                                        </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label3" CssClass="Label" runat="server" Text="N�mero:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNumero" onKeypress="return(validarCajaNumero());" onkeyup="return(calcularLong());" 
                                                        runat="server" MaxLength="4"></asp:TextBox>
                                                    <asp:Label ID="Label6" CssClass="Label" runat="server" Text="Inicio:"></asp:Label>
                                                    <asp:TextBox ID="txtInicio" onKeypress="return(validarCajaNumero());" runat="server"></asp:TextBox>
                                                    <asp:Label ID="Label7" CssClass="Label" runat="server" Text="Cantidad:"></asp:Label>
                                                    <asp:TextBox ID="txtCantidad" onKeypress="return(validarCajaNumero());" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Longitud:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLongitud" onKeypress="return(validarLong());" CssClass="TextBoxReadOnly" Width="75px" runat="server" EnableViewState="true"></asp:TextBox>
                                                    <asp:Label ID="Label8" runat="server" CssClass="Label" Text="Fecha de Registro:"></asp:Label>
                                                    <asp:TextBox ID="txtFechaRegistro" CssClass="TextBoxReadOnly" ReadOnly="true" 
                                                        runat="server" Width="140px"></asp:TextBox>                                                    
                                                    <asp:Label ID="Label15" runat="server" Text="Id:" CssClass="Label"></asp:Label>
                                                    <asp:TextBox ID="txtID" CssClass="TextBoxReadOnly" ReadOnly="true" Width="75px" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Estado:"></asp:Label>
                                                </td>
                                            <td>
                                                <asp:RadioButtonList ID="rdbEstado" runat="server" 
                                                    RepeatDirection="Horizontal" CssClass="Label" >
                                                    <asp:ListItem Value="1" Selected="True">Activo</asp:ListItem>
                                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                </asp:RadioButtonList>
                                                </td>
                                                </tr></table></td></tr></table>   
                        </asp:Panel>
                    </td>
                </tr>                
                                                
<tr>
<td>
<asp:Panel ID="Panel_Filtro" runat="server">
    <table width="100%">
        <tr><td class="TituloCelda">Filtro</td>
        </tr>
        <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" Text="Empresa:" CssClass="Label"></asp:Label>
                        <asp:DropDownList ID="cmbEmpresa_B" runat="server" DataTextField="NombreComercial" DataValueField="Id" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:Label ID="Label10" runat="server" Text="Tienda:" CssClass="Label"></asp:Label>
                        <asp:DropDownList ID="cmbTienda_B" runat="server" DataTextField="Nombre" 
                            DataValueField="Id">
                        </asp:DropDownList>
                        <asp:Label ID="Label12" runat="server" Text="Tipo Documento:" CssClass="Label"></asp:Label>                        
                        <asp:DropDownList ID="cmbTipoDoc_B" runat="server" DataTextField="Descripcion" 
                            DataValueField="Id">
                        </asp:DropDownList>                                                
                        <asp:ImageButton ID="btnBuscar" runat="server" 
                            ImageUrl="~/Imagenes/Buscar_B.JPG"                             
                            onmouseout="this.src='/Imagenes/Buscar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" Visible="True" />
                    </td></tr><tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label13" runat="server" Text="Estado:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:RadioButtonList CssClass="Label" ID="rdbEstado_B" runat="server" 
                                        RepeatDirection="Horizontal" AutoPostBack="True">
                                    <asp:ListItem Value="">Todos</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </td></tr><tr>
                    <td>
                        <asp:GridView ID="DGVSerie_B" Width="100%" runat="server" 
                            AutoGenerateColumns="False" AllowPaging="true" PageSize="10" >
                        <Columns>                                               
                            <asp:CommandField SelectText="Editar" ShowSelectButton="True" />
                            <asp:BoundField HeaderText="Id" DataField="IdSerie" NullDisplayText="---" />
                            <asp:BoundField HeaderText="Empresa" DataField="NomEmpresa" NullDisplayText="---" />
                            <asp:BoundField HeaderText="Tienda" DataField="NomTienda" NullDisplayText="---" />
                            <asp:BoundField HeaderText="Tipo Doc." DataField="NomTipoDoc" NullDisplayText="---" />                                                        
                            <asp:BoundField HeaderText="N�mero" DataField="Numero" NullDisplayText="---" />
                            <asp:BoundField HeaderText="Inicio" DataField="Inicio" NullDisplayText="---" />
                            <asp:BoundField HeaderText="Cantidad" DataField="Cantidad" NullDisplayText="---" />                            
                            <asp:BoundField HeaderText="Longitud" DataField="Longitud" NullDisplayText="---" />
                            <asp:BoundField HeaderText="Estado" DataField="DescEstado" NullDisplayText="---" />
                            <asp:BoundField HeaderText="Fec. Alta" DataField="DescFechaAlta" NullDisplayText="---" />                            
                        </Columns>
                        
                        <RowStyle CssClass="GrillaRow" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        </asp:GridView>
                    </td></tr>
    </table>
    </asp:Panel>
</td>    
</tr>
<tr>
<td>
    <asp:HiddenField ID="hddModo" runat="server" Value="I" />
</td>
</tr></table>
</ContentTemplate>    </asp:UpdatePanel>
</asp:Content>
