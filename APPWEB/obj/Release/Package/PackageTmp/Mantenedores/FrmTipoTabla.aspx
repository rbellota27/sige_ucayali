<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmTipoTabla.aspx.vb" Inherits="APPWEB.FrmTipoTabla" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table align="center" width="100%">
        <tr>
            <td align="center" class="TituloCelda">
                Configuraci�n de Tablas
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upnel" runat="server">
    <ContentTemplate>
    <table align="center" width="100%">
        <tr>
            <td>
                <cc1:TabContainer ID="Tabs" runat="server" ActiveTabIndex="0" Width="100%" style="width:100%;visibility:visible;" OnDemand="true" >
                    <cc1:TabPanel runat="server" HeaderText="TipoTabla" ID="TabTipoTabla">
                        <HeaderTemplate>
                            TIPO TABLA
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnNuevoTT" runat="server" Text="Nuevo" Width="100px" />
                                                            <asp:Button ID="btnGrabarTT" runat="server" Text="Grabar" Width="100px" OnClientClick="return(valSaveTipoTabla());" />
                                                            <asp:Button ID="btnEditarTT" runat="server" Text="Editar" Width="100px" />
                                                            <asp:Button ID="btnCancelarTT" runat="server" Text="Cancelar" Width="100px" OnClientClick="return(confirm('Est� seguro de cancelar la operaci�n ?.'));" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="PanelNuevoTT" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label1" runat="server" Text="Nombre:" ForeColor="#4277AD"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                                ID="txtNombreTT" runat="server" Width="500px" MaxLength="150"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label2" runat="server" Text="Abv:" ForeColor="#4277AD"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                                ID="txtAbvTT" runat="server" Width="500px" MaxLength="150"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label19" runat="server" Text="Longitud:" ForeColor="#4277AD"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:RadioButtonList ID="rbLongitudTT" runat="server" ForeColor="#4277AD" RepeatDirection="Horizontal">
                                                                                <asp:ListItem Value="1">1 Caracter</asp:ListItem>
                                                                                <asp:ListItem Value="2">2 Caracteres</asp:ListItem>
                                                                                <asp:ListItem Value="3">3 Caracteres</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:RadioButtonList ID="rbEstadoTT" runat="server" ForeColor="#4277AD" RepeatDirection="Horizontal"
                                                                                AutoPostBack="True">
                                                                                <asp:ListItem Value="1">Activo</asp:ListItem>
                                                                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="hddModoTT" runat="server" />
                                                            <asp:HiddenField ID="hddIdTipoTabla" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="dgvTipoTabla" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="up_busqueda_tt" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel ID="PanelBuscarTT" runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label20" runat="server" Text="Buscar por Descripcion:" ForeColor="#4277AD"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txt_buscar_tt" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Button ID="btn_buscar_tt" runat="server" Text="Buscar" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="Panel1" runat="server">
                                                                    <asp:GridView ID="dgvTipoTabla" runat="server" PageSize="10" Width="100%" AutoGenerateColumns="False">
                                                                        <Columns>
                                                                            <asp:CommandField ShowSelectButton="True" />
                                                                            <asp:BoundField DataField="IdTipoTabla" HeaderText="Nro" />
                                                                            <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                                                            <asp:BoundField DataField="Abv" HeaderText="Abv" />
                                                                            <asp:BoundField DataField="Longitud" HeaderText="Longitud" />
                                                                            <asp:TemplateField HeaderText="Estado">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblstrEstado" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"strEstado") %>'></asp:Label>
                                                                                    <asp:HiddenField ID="hddEstado" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btn_anterior_tt" runat="server" Width="50px" Text="<" ToolTip="P�gina Anterior"
                                                                    OnClientClick="return(valNavegacion_tt('0'));" Style="cursor: hand;" />
                                                                <asp:Button ID="btn_siguiente_tt" runat="server" Width="50px" Text=">" ToolTip="P�gina Posterior"
                                                                    OnClientClick="return(valNavegacion_tt('1'));" Style="cursor: hand;" />
                                                                <asp:TextBox ID="txt_PageIndex_tt" runat="server" Width="50px" CssClass="TextBoxReadOnly_Right"></asp:TextBox>
                                                                <asp:Button ID="btn_Ir_tt" runat="server" Width="50px" Text="Ir" ToolTip="Ir a la P�gina"
                                                                    OnClientClick="return(valNavegacion_tt('2'));" Style="cursor: hand;" />
                                                                <asp:TextBox ID="txt_PageIndexGO_tt" Width="50px" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButtonList ID="rbEstadoBusquedaTT" runat="server" ForeColor="#4277AD" RepeatDirection="Horizontal"
                                                                    AutoPostBack="True">
                                                                    <asp:ListItem Value="2">Todos</asp:ListItem>
                                                                    <asp:ListItem Value="1">Activo</asp:ListItem>
                                                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnCancelarTT" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnGrabarTT" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnNuevoTT" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="Tabla" ID="TabTabla">
                        <HeaderTemplate>
                            TABLA
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnNuevoTTV" runat="server" Text="Nuevo" Width="100px" OnClientClick="return(valCodigoTipoTablaValor());" />
                                                            <asp:Button ID="btnGrabarTTV" runat="server" Text="Grabar" Width="100px" OnClientClick="return(valSaveTipoTablaValor());" />
                                                            <asp:Button ID="btnEditarTTV" runat="server" Text="Editar" Width="100px" />
                                                            <asp:Button ID="btnCancelarTTV" runat="server" Text="Cancelar" Width="100px" OnClientClick="return(confirm('Est� seguro de cancelar la operaci�n ?.'));" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label6" runat="server" Text="Nombre de la Tabla:" ForeColor="#4277AD"></asp:Label>
                                                            <asp:DropDownList ID="cboTablaTTV" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TituloCelda">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="PanelNuevoTTV" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label3" runat="server" Text="C�digo:" ForeColor="#4277AD"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtCodigoTTV" runat="server" Width="80px"></asp:TextBox>
                                                                            <asp:Label ID="lblMsjCodigoTTV" runat="server" ForeColor="Red"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label4" runat="server" Text="Descripci�n:" ForeColor="#4277AD"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                                ID="txtDescripcionTTV" runat="server" Width="400px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label5" runat="server" Text="Abv:" ForeColor="#4277AD"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="return ( onFocusTextTransform(this,configurarDatos) );"
                                                                                ID="txtAbvTTV" runat="server" Width="400px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:RadioButtonList ID="rbEstadoTTV" runat="server" ForeColor="#4277AD" RepeatDirection="Horizontal"
                                                                                AutoPostBack="True">
                                                                                <asp:ListItem Value="1">Activo</asp:ListItem>
                                                                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="hddModoTTV" runat="server" />
                                                            <asp:HiddenField ID="hddIdTipoTablaValorTTV" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="dgvTablaTTV" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="up_busqueda_t" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel ID="PanelBuscarTTV" runat="server">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label21" runat="server" Text="Buscar por Descripcion:" ForeColor="#4277AD"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Panel ID="Panel9" runat="server" DefaultButton="btn_buscar_t">                                                                            
                                                                            <asp:TextBox ID="txt_buscar_t" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Button ID="btn_buscar_t" runat="server" Text="Buscar" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="Panel2" runat="server">
                                                                    <asp:GridView ID="dgvTablaTTV" runat="server" AutoGenerateColumns="False" Width="100%">
                                                                        <Columns>
                                                                            <asp:CommandField ShowSelectButton="True" />
                                                                            <asp:TemplateField HeaderText="C�digo">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCodigo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Codigo") %>'></asp:Label>
                                                                                    <asp:HiddenField ID="hddIdTipoTablaValor" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTablaValor") %>' />
                                                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="Nombre" HeaderText="Descripci�n" />
                                                                            <asp:BoundField DataField="Abv" HeaderText="Abv" />
                                                                            <asp:TemplateField HeaderText="Estado">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblstrEstado" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"strEstado") %>'></asp:Label>
                                                                                    <asp:HiddenField ID="hddEstado" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btn_anterior_t" runat="server" Width="50px" Text="<" ToolTip="P�gina Anterior"
                                                                    OnClientClick="return(valNavegacion_t('0'));" Style="cursor: hand;" />
                                                                <asp:Button ID="btn_siguiente_t" runat="server" Width="50px" Text=">" ToolTip="P�gina Posterior"
                                                                    OnClientClick="return(valNavegacion_t('1'));" Style="cursor: hand;" />
                                                                <asp:TextBox ID="txt_PageIndex_t" runat="server" Width="50px" CssClass="TextBoxReadOnly_Right"></asp:TextBox>
                                                                <asp:Button ID="btn_ir_t" runat="server" Width="50px" Text="Ir" ToolTip="Ir a la P�gina"
                                                                    OnClientClick="return(valNavegacion_t('2'));" Style="cursor: hand;" />
                                                                <asp:TextBox ID="txt_PageIndexGo_t" Width="50px" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButtonList ID="rbEstadoBusquedaTTV" runat="server" ForeColor="#4277AD"
                                                                    RepeatDirection="Horizontal" AutoPostBack="True">
                                                                    <asp:ListItem Value="2">Todos</asp:ListItem>
                                                                    <asp:ListItem Value="1">Activo</asp:ListItem>
                                                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="cboTablaTTV" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="btnCancelarTTV" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnGrabarTTV" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnNuevoTTV" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="CONFIGURACI�N TIPO TABLA" ID="TabConfiguracion">
                        <HeaderTemplate>
                            CONFIGURACI�N TIPO TABLA
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdateProgress ID="UpdateProgress3" runat="server" AssociatedUpdatePanelID="updatePanel3">
                                <ProgressTemplate>
                                    <div id="Div2" style="border: 0px solid; padding: 10px; position: absolute; top: 250px;
                                        left: 447px; z-index: 4; display: block; visibility: visible;">
                                        <asp:Image ID="Image3" runat="server" ImageUrl="~/Imagenes/ajax.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <table>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="updatePanel3" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnNuevoConfig" runat="server" Text="Nuevo" Width="100px" />
                                                            <asp:Button ID="btnGrabarConfig" runat="server" Text="Grabar" Width="100px" OnClientClick="return(valSaveConfiguracion());" />
                                                            <asp:Button ID="btnEditarConfig" runat="server" Text="Editar" Width="100px" />
                                                            <asp:Button ID="btnCancelarConfig" runat="server" Text="Cancelar" Width="100px" OnClientClick="return(confirm('Est� seguro de cancelar la operaci�n ?.'));" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label7" runat="server" Text="Tipo Existencia:" ForeColor="#4277AD"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboTipoExistenciaConfig" runat="server" AutoPostBack="true"
                                                                            Width="450px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:Label ID="Label8" runat="server" Text="Linea:" ForeColor="#4277AD"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboLineaConfig" runat="server" AutoPostBack="true" Width="450px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Label9" runat="server" Text="SubLinea:" ForeColor="#4277AD"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboSubLineaConfig" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="PanelNuevoConfig" runat="server" ScrollBars="Both">
                                                                <table>
                                                                    <tr>
                                                                        <td class="TituloCelda">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="Label10" runat="server" Text="Tipo Tabla:" ForeColor="#4277AD"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:DropDownList ID="cboTipoTablaConfig" runat="server" AutoPostBack="true">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Button ID="btnAgregarConfig" runat="server" Text="Agregar" Width="90px" OnClientClick="return(valConfiguracionGrilla());" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table border="1" style="border-collapse: collapse;">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <table>
                                                                                                                    <tr>
                                                                                                                        <td align="center">
                                                                                                                            <asp:Label ID="Label12" runat="server" Text="Tipo Uso" ForeColor="#4277AD"></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:RadioButtonList ID="rbTipoUsoConfig" runat="server" ForeColor="#4277AD" CssClass=""
                                                                                                                                RepeatDirection="Vertical" AutoPostBack="True">
                                                                                                                                <asp:ListItem Value="OR">Origen</asp:ListItem>
                                                                                                                                <asp:ListItem Value="CO">C�digo</asp:ListItem>
                                                                                                                                <asp:ListItem Value="AT">Atributos</asp:ListItem>
                                                                                                                                <asp:ListItem Value="SU">Sin Uso</asp:ListItem>
                                                                                                                            </asp:RadioButtonList>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <table>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:Label ID="Label11" runat="server" Text="Nro Orden" ForeColor="#4277AD"></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:TextBox ID="txtNroOrdenConfig" runat="server" Width="70px" onkeypress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                            <td valign="middle">
                                                                                                                <asp:CheckBox ID="chkParteNombreConfig" runat="server" Text="Parte Nombre" ForeColor="#4277AD" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="3">
                                                                                                                <asp:Panel ID="PanelEstadoConfig" runat="server">
                                                                                                                    <table>
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:Label ID="Label13" runat="server" Text="Estado:" ForeColor="#4277AD"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <asp:RadioButtonList ID="rbEstadoConfig" runat="server" ForeColor="#4277AD" CssClass=""
                                                                                                                                    RepeatDirection="Horizontal" AutoPostBack="True">
                                                                                                                                    <asp:ListItem Value="1">Activo</asp:ListItem>
                                                                                                                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                                                                                                </asp:RadioButtonList>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </asp:Panel>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <asp:Panel ID="Panel3" runat="server" Width="950px" Height="300px" ScrollBars="both">
                                                                                            <asp:GridView ID="dgvSubLineaTipoTabla" runat="server" AutoGenerateColumns="False">
                                                                                                <RowStyle CssClass="GrillaRow" />
                                                                                                <Columns>
                                                                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                                                                    <asp:TemplateField HeaderText="Id">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:HiddenField ID="hddIdSubLinea" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdSubLinea") %>' />
                                                                                                            <asp:Label ID="lblIdTipoTabla" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>'></asp:Label>
                                                                                                            <asp:HiddenField ID="hddIdLinea" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdLinea") %>' />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="Nombre" HeaderText="Tipo Tabla" />
                                                                                                    <asp:TemplateField HeaderText="Tipo Uso">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:HiddenField ID="hddTipoUso" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"TipoUso") %>' />
                                                                                                            <asp:Label ID="lblNomTipoUso" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NomTipoUso") %>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="NroOrden" HeaderText="Nro Orden" />
                                                                                                    <asp:TemplateField HeaderText="Parte Nombre">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="chkParteNombre" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"ParteNombre") %>' />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Activo">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="chkEstado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                                                                                            <asp:HiddenField ID="hddNoDele" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Indicador") %>' />
                                                                                                            <asp:HiddenField ID="hddLongitud" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Longitud") %>' />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <RowStyle CssClass="GrillaRow" />
                                                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                                                <PagerStyle CssClass="GrillaPager" />
                                                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                            </asp:GridView>
                                                                                        </asp:Panel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="dgvListConfig" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="updatePanel6" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel ID="PanelBuscarConfig" runat="server">
                                                    <table>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:RadioButtonList ID="rbEstadoBusquedaConfig" runat="server" ForeColor="#4277AD"
                                                                    CssClass="" RepeatDirection="Horizontal" AutoPostBack="True">
                                                                    <asp:ListItem Value="2">Todos</asp:ListItem>
                                                                    <asp:ListItem Value="1">Activo</asp:ListItem>
                                                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="Panel4" runat="server" Width="950px" ScrollBars="both">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:GridView ID="dgvListConfig" runat="server" AutoGenerateColumns="False" PageSize="10">
                                                                                    <RowStyle CssClass="GrillaRow" />
                                                                                    <Columns>
                                                                                        <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
                                                                                        <asp:TemplateField HeaderText="Linea">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="hddIdLinea" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdLinea") %>' />
                                                                                                <asp:Label ID="lblNomLinea" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NomLinea") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Sub Linea">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="hddIdSubLinea" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdSubLinea") %>' />
                                                                                                <asp:Label ID="lblNomSubLinea" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NomSubLinea") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="Nombre" HeaderText="Configuraci�n Tipo Tabla" />
                                                                                        <asp:TemplateField HeaderText="Estado">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="hddEstado" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                                                                                <asp:Label ID="lblNomEstado" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"strEstado") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <RowStyle CssClass="GrillaRow" />
                                                                                    <FooterStyle CssClass="GrillaFooter" />
                                                                                    <PagerStyle CssClass="GrillaPager" />
                                                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Button ID="btn_anterior_slConfig" runat="server" Width="50px" Text="<" ToolTip="P�gina Anterior"
                                                                                    OnClientClick="return(valNavegacion_slConfig('0'));" Style="cursor: hand;" />
                                                                                <asp:Button ID="btn_siguiente_slConfig" runat="server" Width="50px" Text=">" ToolTip="P�gina Posterior"
                                                                                    OnClientClick="return(valNavegacion_slConfig('1'));" Style="cursor: hand;" />
                                                                                <asp:TextBox ID="txt_pageIndex_slConfig" runat="server" Width="50px" CssClass="TextBoxReadOnly_Right"></asp:TextBox>
                                                                                <asp:Button ID="btn_ir_slConfig" runat="server" Width="50px" Text="Ir" ToolTip="Ir a la P�gina"
                                                                                    OnClientClick="return(valNavegacion_slConfig('2'));" Style="cursor: hand;" />
                                                                                <asp:TextBox ID="txt_PageIndexGo_slConfig" Width="50px" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnNuevoConfig" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnGrabarConfig" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnCancelarConfig" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="cboTipoExistenciaConfig" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="cboLineaConfig" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="cboSubLineaConfig" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HiddenField ID="hddModoConfig" runat="server" Value="0" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="CONFIGURACI�N TABLA POR VALOR" ID="TabConfiguracionxValor">
                        <HeaderTemplate>
                            CONFIGURACI�N TABLA POR VALOR
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:UpdateProgress ID="UpdateProgress7" runat="server">
                                            <ProgressTemplate>
                                                <div id="Div1" style="border: 0px solid; padding: 10px; position: absolute; top: 200px;
                                                    left: 447px; z-index: 4; display: block; visibility: visible;">
                                                    <asp:Image ID="Image7" runat="server" ImageUrl="~/Imagenes/ajax.gif" />
                                                </div>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnNuevoConfigTablaxValor" runat="server" Text="Nuevo" Width="100px" />
                                                            <asp:Button ID="btnGrabarConfigTablaxValor" runat="server" Text="Grabar" Width="100px"
                                                                OnClientClick="return(valSaveConfigTablaxValor());" />
                                                            <asp:Button ID="btnEditarConfigTablaxValor" runat="server" Text="Editar" Width="100px" />
                                                            <asp:Button ID="btnCancelarConfigTablaxValor" runat="server" Text="Cancelar" Width="100px"
                                                                OnClientClick="return(confirm('Est� seguro de cancelar la operaci�n ?.'));" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="right">
                                                                        <span style="color: #4277AD;">Tipo Existencia:</span>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <asp:DropDownList ID="cboTipoExistenciaConfigxValor" runat="server" AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        <span style="color: #4277AD;">Linea:</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboLineaConfigxValor" runat="server" AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td align="right">
                                                                        <span style="color: #4277AD;">SubLinea:</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboSubLineaConfigxValor" runat="server" AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="PanelNuevoConfigTablaxValor" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <span style="color: #4277AD;">Tipo Tabla:</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="cboTipoTablaConfigxValor" runat="server" AutoPostBack="True">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnAgregarConfiggxValor" runat="server" Text="Agregar" Width="80px"
                                                                                            OnClientClick="return(valConfiguracionxValorGrilla());" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <table width="950px">
                                                                                <tr>
                                                                                    <td style="width: 50%;" valign="top">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td colspan="2">
                                                                                                                <asp:Label ID="lblMsjRegistroTipoTabla" runat="server" Font-Bold="True" ForeColor="Red"
                                                                                                                    Font-Size="8pt"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtBuscarCttv" runat="server" onKeypress="return(valKeyPressBusquedaTipoTablaValor());"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Button ID="btnBuscarCttv" runat="server" Text="Buscar" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Panel ID="Panel5" runat="server" Height="400px" ScrollBars="Vertical">
                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:GridView ID="dgvTipoTablaConfigxValor" runat="server" AutoGenerateColumns="False">
                                                                                                                        <RowStyle CssClass="GrillaRow" />
                                                                                                                        <Columns>
                                                                                                                            <asp:TemplateField>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:CheckBox ID="ChkSeleccionar" runat="server" />
                                                                                                                                </ItemTemplate>
                                                                                                                                <HeaderTemplate>
                                                                                                                                    <asp:CheckBox ID="chkcttvMasivo" runat="server" OnClick="selectAll(this)" />
                                                                                                                                </HeaderTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:BoundField DataField="Codigo" HeaderText="C�digo" />
                                                                                                                            <asp:TemplateField HeaderText="Nombre">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                                                                                                    <asp:HiddenField ID="hddIdTipoTablaValor" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTablaValor") %>' />
                                                                                                                                    <asp:Label ID="lblNomTipoTablaValor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:BoundField DataField="TipoUso" HeaderText="Tipo Uso" />
                                                                                                                            <asp:TemplateField HeaderText="Estado">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:HiddenField ID="hddEstado" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                                                                                                                    <asp:Label ID="lblNomEstado" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"strEstado") %>'></asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                        </Columns>
                                                                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                                                    </asp:GridView>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:Button ID="btn_anterior_SetValor" runat="server" Width="50px" Text="<" ToolTip="P�gina Anterior"
                                                                                                                        OnClientClick="return(valNavegacion_SetValor('0'));" Style="cursor: hand;" />
                                                                                                                    <asp:Button ID="btn_siguiente_SetValor" runat="server" Width="50px" Text=">" ToolTip="P�gina Posterior"
                                                                                                                        OnClientClick="return(valNavegacion_SetValor('1'));" Style="cursor: hand;" />
                                                                                                                    <asp:TextBox ID="txt_PageIndex_SetValor" runat="server" Width="50px" CssClass="TextBoxReadOnly_Right"></asp:TextBox>
                                                                                                                    <asp:Button ID="btn_Ir_SetValor" runat="server" Width="50px" Text="Ir" ToolTip="Ir a la P�gina"
                                                                                                                        OnClientClick="return(valNavegacion_SetValor('2'));" Style="cursor: hand;" />
                                                                                                                    <asp:TextBox ID="txt_PageIndexGo_SetValor" Width="50px" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </asp:Panel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td style="width: 50%;" valign="top">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <span style="color: #4277AD;">Descripcion:</span>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtBuscarTValor" runat="server" onKeypress="return(valKeyPressBusquedaTipoTablaValor());"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Button ID="btnBuscarTValor" runat="server" Text="Buscar" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Panel ID="Panel6" runat="server" Height="400px" ScrollBars="Vertical">
                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:GridView ID="dgvListConfigxValor" runat="server" AutoGenerateColumns="False"
                                                                                                                        GridLines="Horizontal">
                                                                                                                        <RowStyle CssClass="GrillaRow" />
                                                                                                                        <Columns>
                                                                                                                            <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                                                                                                                                HeaderStyle-HorizontalAlign="Center">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <table>
                                                                                                                                        <tr>
                                                                                                                                            <td class="Texto">
                                                                                                                                                <asp:LinkButton ID="lkbQuitar" Font-Bold="true" runat="server" OnClientClick=" return ( confirm('Desea continuar con la operaci�n?') ); "
                                                                                                                                                    OnClick="lkbQuitar_Click">Quitar</asp:LinkButton>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </ItemTemplate>
                                                                                                                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:HiddenField ID="hddIdSubLinea" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdSubLinea") %>' />
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTabla") %>' />
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:BoundField DataField="TipoUso" HeaderText="Tipo Uso" />
                                                                                                                            <asp:BoundField DataField="Codigo" HeaderText="C�digo" />
                                                                                                                            <asp:TemplateField HeaderText="Descripci�n">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:HiddenField ID="hddIdTipoTablaValor" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoTablaValor") %>' />
                                                                                                                                    <asp:Label ID="lblNomTipoTablaValor" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Nombre") %>'></asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                            <asp:TemplateField HeaderText="Estado">
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:HiddenField ID="hddEstado" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                                                                                                                    <asp:Label ID="lblNomEstado" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"strEstado") %>'></asp:Label>
                                                                                                                                    <asp:HiddenField ID="hddNoDele" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Indicador") %>' />
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateField>
                                                                                                                        </Columns>
                                                                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                                                                    </asp:GridView>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:Button ID="btnAnteriorTValor" runat="server" Width="50px" Text="<" ToolTip="P�gina Anterior"
                                                                                                                        OnClientClick="return(valNavegacion_TValor('0'));" Style="cursor: hand;" />
                                                                                                                    <asp:Button ID="btnSiguienteTValor" runat="server" Width="50px" Text=">" ToolTip="P�gina Posterior"
                                                                                                                        OnClientClick="return(valNavegacion_TValor('1'));" Style="cursor: hand;" />
                                                                                                                    <asp:TextBox ID="txtPageIndex_TValor" runat="server" Width="50px" CssClass="TextBoxReadOnly_Right"></asp:TextBox>
                                                                                                                    <asp:Button ID="btnIrTValor" runat="server" Width="50px" Text="Ir" ToolTip="Ir a la P�gina"
                                                                                                                        OnClientClick="return(valNavegacion_TValor('2'));" Style="cursor: hand;" />
                                                                                                                    <asp:TextBox ID="txtPageIndexGo_TValor" Width="50px" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </asp:Panel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="dgvBusquedaConfigxValor" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel ID="PanelBuscarConfigTablaxValor" runat="server">
                                                    <asp:Panel ID="Panel7" runat="server">
                                                        <table>
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:RadioButtonList ID="rbEstadoBusquedaConfigxValor" runat="server" ForeColor="#4277AD"
                                                                        RepeatDirection="Horizontal" AutoPostBack="True">
                                                                        <asp:ListItem Value="2">Todos</asp:ListItem>
                                                                        <asp:ListItem Value="1">Activo</asp:ListItem>
                                                                        <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Panel ID="Panel8" runat="server" ScrollBars="Vertical" Width="950px">
                                                                        <asp:GridView ID="dgvBusquedaConfigxValor" runat="server" AutoGenerateColumns="False">
                                                                            <RowStyle CssClass="GrillaRow" />
                                                                            <Columns>
                                                                                <asp:CommandField ShowSelectButton="True" />
                                                                                <asp:TemplateField HeaderText="Linea">
                                                                                    <ItemTemplate>
                                                                                        <asp:HiddenField ID="hddIdLinea" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdLinea") %>' />
                                                                                        <asp:Label ID="lblNomLinea" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NomLinea") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Sub Linea">
                                                                                    <ItemTemplate>
                                                                                        <asp:HiddenField ID="hddIdSubLinea" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdSubLinea") %>' />
                                                                                        <asp:Label ID="lblNomSubLinea" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NomSubLinea") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="Nombre" HeaderText="Configuraci�n Tipo Tabla por Valor" />
                                                                                <asp:TemplateField HeaderText="Estado">
                                                                                    <ItemTemplate>
                                                                                        <asp:HiddenField ID="hddEstado" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                                                                                        <asp:Label ID="lblNomEstado" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"strEstado") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <FooterStyle CssClass="GrillaFooter" />
                                                                            <PagerStyle CssClass="GrillaPager" />
                                                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                            <HeaderStyle CssClass="GrillaHeader" />
                                                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="btn_anterior_ConfigValor" runat="server" Width="50px" Text="<" ToolTip="P�gina Anterior"
                                                                        OnClientClick="return(valNavegacion_ConfigValor('0'));" Style="cursor: hand;" />
                                                                    <asp:Button ID="btn_siguiente_ConfigValor" runat="server" Width="50px" Text=">" ToolTip="P�gina Posterior"
                                                                        OnClientClick="return(valNavegacion_ConfigValor('1'));" Style="cursor: hand;" />
                                                                    <asp:TextBox ID="txt_PageIndex_ConfigValor" runat="server" Width="50px" CssClass="TextBoxReadOnly_Right"></asp:TextBox>
                                                                    <asp:Button ID="btn_Ir_ConfigValor" runat="server" Width="50px" Text="Ir" ToolTip="Ir a la P�gina"
                                                                        OnClientClick="return(valNavegacion_ConfigValor('2'));" Style="cursor: hand;" />
                                                                    <asp:TextBox ID="txt_PageIndexGo_ConfigValor" Width="50px" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="cboTipoExistenciaConfigxValor" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="cboLineaConfigxValor" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="cboSubLineaConfigxValor" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="btnNuevoConfigTablaxValor" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnGrabarConfigTablaxValor" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnCancelarConfigTablaxValor" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HiddenField ID="hddModoConfigValor" runat="server" Value="0" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </td>
        </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />

    <script language="javascript" type="text/javascript">
        function valSaveTipoTabla() {
            var txtNombre = document.getElementById('<%=txtNombreTT.ClientID %>');
            if (txtNombre.value.length <= 0) {
                alert('Ingrese nombre.');
                txtNombre.select();
                txtNombre.focus();
                return false;
            }
            var txtAbv = document.getElementById('<%=txtAbvTT.ClientID %>');
            if (txtAbv.value.length <= 0) {
                alert('Ingrese Abv.');
                txtAbv.select();
                txtAbv.focus();
                return false;
            }


            return (confirm('Desea grabar la informaci�n. ?'));
        }

        function valCodigoTipoTablaValor() {
            var cbotablattv = document.getElementById('<%=cboTablaTTV.ClientID %>');
            if (parseFloat(cbotablattv.value) == 0) {
                alert('Seleccione Tipo Tabla para generar el c�digo.');
                cbotablattv.focus();
                return false;
            }
            return true;
        }

        function valSaveTipoTablaValor() {
            var codigo = document.getElementById('<%=txtCodigoTTV.ClientID %>');
            if (codigo.value.length <= 0) {
                alert('Ingrese c�digo.');
                codigo.select();
                codigo.focus();
                return false;
            }
            var descripcion = document.getElementById('<%=txtDescripcionTTV.ClientID %>');
            if (descripcion.value.length <= 0) {
                alert('Ingrese descripci�n.');
                descripcion.select();
                descripcion.focus();
                return false;
            }
            var abv = document.getElementById('<%=txtAbvTTV.ClientID %>');
            if (abv.value.length <= 0) {
                alert('Ingrese Abv.');
                abv.select();
                abv.focus();
                return false;
            }
            var cbotablattv = document.getElementById('<%=cboTablaTTV.ClientID %>');
            if (parseFloat(cbotablattv.value) == 0) {
                alert('Seleccione Tipo Tabla.');
                cbotablattv.focus();
                return false;
            }

            return (confirm('Desea grabar la informaci�n. ?'));
        }

        function valSaveConfiguracion() {
            var cbotipoexit = document.getElementById('<%=cboTipoExistenciaConfig.ClientID %>');
            if (parseFloat(cbotipoexit.value) == 0) {
                alert('Seleccione tipo existencia.');
                cbotipoexit.focus();
                return false;
            }
            var cbolinea = document.getElementById('<%=cboLineaConfig.ClientID %>');
            if (parseFloat(cbolinea.value) == 0) {
                alert('Seleccione linea.');
                cbolinea.focus();
                return false;
            }

            var grilla = document.getElementById('<%=dgvSubLineaTipoTabla.ClientID %>');
            var hddmodoconfig = document.getElementById('<%=hddModoConfig.ClientID %>');
            if (parseFloat(hddmodoconfig.value) == 1) {
                if (grilla == null) {
                    alert('No �xiste informaci�n.');
                    return false;
                }
            }

            if (validarExistenciadeLineaSublinea() == false) {
                return false;
            }


            var cbosublinea = document.getElementById('<%=cboSubLineaConfig.ClientID %>');
            if (parseFloat(cbosublinea.value) == 0) {
                return confirm('Desea generar la configuraci�n para todas las sub lineas de la linea ' + '[ ' + cbolinea.options[cbolinea.selectedIndex].text + ' ].');
            }
            else {
                return (confirm('Desea grabar la informaci�n. ?'));
            }
            return true;
        }

        function valConfiguracionGrilla() {
            var cbotipoexit = document.getElementById('<%=cboTipoExistenciaConfig.ClientID %>');
            if (parseFloat(cbotipoexit.value) == 0) {
                alert('Seleccione tipo existencia.');
                cbotipoexit.focus();
                return false;
            }
            var cbolinea = document.getElementById('<%=cboLineaConfig.ClientID %>');
            if (parseFloat(cbolinea.value) == 0) {
                alert('Seleccione linea.');
                cbolinea.focus();
                return false;
            }
            var cbotipotablaconfig = document.getElementById('<%=cboTipoTablaConfig.ClientID %>');
            if (parseFloat(cbotipotablaconfig.value) == -3) {
                alert('Seleccione tipo tabla.')
                cbotipotablaconfig.focus();
                return false;
            }

            var orden = document.getElementById('<%=txtNroOrdenConfig.ClientID %>');
            var grilla = document.getElementById('<%=dgvSubLineaTipoTabla.ClientID %>');

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];


                    if (rowElem.cells[1].children[1].innerHTML == cbotipotablaconfig.value && rowElem.cells[1].children[1].innerHTML == cbotipotablaconfig.value) {
                        alert('El tipo tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                    if (rowElem.cells[4].innerHTML == orden.value && rowElem.cells[4].innerHTML == orden.value) {
                        alert('El nro orden ya ha sido ingresado.');
                        orden.focus();
                        return false;
                    }
                }
            }

            if ((orden.value) == '') {
                alert('Ingrese nro de orden.')
                orden.focus();
                return false;
            }
            //        if (validarExistenciadeLineaSublinea() == false) {
            //            return false;
            //        }        
            return true;
        }

        ///////////

        function validarExistenciadeLineaSublinea() {
            var cbotipotablaconfig = document.getElementById('<%=cboTipoTablaConfig.ClientID %>');
            var grilla = document.getElementById('<%=dgvSubLineaTipoTabla.ClientID %>');
            var existeLinea = 0;
            var existesublinea = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].innerHTML == -2) {
                        existeLinea = 1;
                    }
                    if (rowElem.cells[1].children[1].innerHTML == -1) {
                        existesublinea = 1;
                    }
                } //next

            } else {
                if (cbotipotablaconfig.value == -2) {
                    return true;
                }
            }

            if (existeLinea == 0 && cbotipotablaconfig.value != -2) {
                alert('No ha seleccionado una linea del tipo tabla para la configuraci�n');
                return false;
            }
            if (existesublinea == 0 && cbotipotablaconfig.value != -1 && existeLinea == 1) {
                alert('No ha seleccionado una sub linea del tipo tabla para la configuraci�n');
                return false;
            }
        }

        function valSaveConfigTablaxValor() {
            var cbotipoexit = document.getElementById('<%=cboTipoExistenciaConfigxValor.ClientID %>');
            if (parseFloat(cbotipoexit.value) == 0) {
                alert('Seleccione tipo existencia.');
                cbotipoexit.focus();
                return false;
            }
            var cbolinea = document.getElementById('<%=cboLineaConfigxValor.ClientID %>');
            if (parseFloat(cbolinea.value) == 0) {
                alert('Seleccione linea.');
                cbolinea.focus();
                return false;
            }
            var cbosublinea = document.getElementById('<%=cboSubLineaConfigxValor.ClientID %>');
            if (parseFloat(cbosublinea.value) == 0) {
                alert('Seleccione sub linea.');
                cbosublinea.focus();
                return false;
            }
            var grilla = document.getElementById('<%=dgvListConfigxValor.ClientID %>');
            var hddmodoconfig = document.getElementById('<%=hddModoConfigValor.ClientID %>');
            if (parseFloat(hddmodoconfig.value) == 1) {
                if (grilla == null) {
                    alert('No �xiste informaci�n, agregar registros para cada tipo tabla.');
                    return false;
                }
            }
            return (confirm('Desea grabar la informaci�n. ?'));
        }


        function valConfiguracionxValorGrilla() {
            var cbotipoexit = document.getElementById('<%=cboTipoExistenciaConfigxValor.ClientID %>');
            if (parseFloat(cbotipoexit.value) == 0) {
                alert('Seleccione tipo existencia.');
                cbotipoexit.focus();
                return false;
            }
            var cbolinea = document.getElementById('<%=cboLineaConfigxValor.ClientID %>');
            if (parseFloat(cbolinea.value) == 0) {
                alert('Seleccione linea.');
                cbolinea.focus();
                return false;
            }
            var cbosublinea = document.getElementById('<%=cboSubLineaConfigxValor.ClientID %>');
            if (parseFloat(cbosublinea.value) == 0) {
                alert('Seleccione sub linea.');
                cbosublinea.focus();
                return false;
            }
            var cbotipotablaconfig = document.getElementById('<%=cboTipoTablaConfigxValor.ClientID %>');
            if (parseFloat(cbotipotablaconfig.value) == 0) {
                alert('Seleccione tipo tabla.')
                cbotipotablaconfig.focus();
                return false;
            }
            var grilla = document.getElementById('<%=dgvTipoTablaConfigxValor.ClientID %>');
            var grillalista = document.getElementById('<%=dgvListConfigxValor.ClientID %>');
            var cont = 0;
            if (grilla == null) {
                alert('No �xiste informaci�n del Tipo Tabla Seleccionado.');
                return false;
            }
            else {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];


                    if (grillalista != null) {
                        for (var ii = 1; ii < grillalista.rows.length; ii++) {//comienzo en 1 xq 0 es la cabecera
                            var rowElem2 = grillalista.rows[ii];
                            if (rowElem.cells[0].getElementsByTagName('INPUT')[0].checked == true) {
                                if (rowElem.cells[2].children[1].value == rowElem2.cells[5].children[0].value && cbosublinea.value == rowElem2.cells[1].children[0].value) {
                                    alert('La Sub Linea ' + '[ ' + cbosublinea.options[cbosublinea.selectedIndex].text + ' ]' + ' del tipo tabla ' + '[ ' + cbotipotablaconfig.options[cbotipotablaconfig.selectedIndex].text + ' ]' + ' y el registro ' + '[ ' + rowElem.cells[2].children[2].innerHTML + ' ]' + ' ya ha sido ingresado.');
                                    return false;
                                }
                            }
                        }

                    }


                    if (rowElem.cells[0].getElementsByTagName('INPUT')[0].checked == true) {
                        cont = cont + 1;
                    }
                }
                if (cont == 0) {
                    alert('Seleccionar los registros que desea agregar.');
                    return false;
                }
            }


            return true;
        }
        //


        function valNavegacion_TValor(tipoMov) {

            var index = parseInt(document.getElementById('<%=txtPageIndex_TValor.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGo_TValor.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
            }
            return true;
        }
        //

        function valNavegacion_tt(tipoMov) {

            var index = parseInt(document.getElementById('<%=txt_PageIndex_tt.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txt_buscar_tt.ClientID%>').select();
                document.getElementById('<%=txt_buscar_tt.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txt_PageIndexGO_tt.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txt_PageIndexGO_tt.ClientID%>').select();
                        document.getElementById('<%=txt_PageIndexGO_tt.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txt_PageIndexGO_tt.ClientID%>').select();
                        document.getElementById('<%=txt_PageIndexGO_tt.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //
        function valNavegacion_t(tipoMov) {

            var index = parseInt(document.getElementById('<%=txt_PageIndex_t.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txt_buscar_t.ClientID%>').select();
                document.getElementById('<%=txt_buscar_t.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txt_PageIndexGo_t.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txt_PageIndexGO_t.ClientID%>').select();
                        document.getElementById('<%=txt_PageIndexGO_t.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txt_PageIndexGO_t.ClientID%>').select();
                        document.getElementById('<%=txt_PageIndexGO_t.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //
        //
        function valNavegacion_ConfigValor(tipoMov) {

            var index = parseInt(document.getElementById('<%=txt_PageIndex_ConfigValor.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txt_PageIndexGo_ConfigValor.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txt_PageIndexGO_ConfigValor.ClientID%>').select();
                        document.getElementById('<%=txt_PageIndexGO_ConfigValor.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txt_PageIndexGO_ConfigValor.ClientID%>').select();
                        document.getElementById('<%=txt_PageIndexGO_ConfigValor.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //

        function valNavegacion_SetValor(tipoMov) {

            var index = parseInt(document.getElementById('<%=txt_PageIndex_SetValor.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txt_PageIndexGo_SetValor.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txt_PageIndexGO_SetValor.ClientID%>').select();
                        document.getElementById('<%=txt_PageIndexGO_SetValor.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txt_PageIndexGO_SetValor.ClientID%>').select();
                        document.getElementById('<%=txt_PageIndexGO_SetValor.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //

        function valNavegacion_slConfig(tipoMov) {

            var index = parseInt(document.getElementById('<%=txt_PageIndex_slConfig.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txt_PageIndexGo_slConfig.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txt_PageIndexGO_slConfig.ClientID%>').select();
                        document.getElementById('<%=txt_PageIndexGO_slConfig.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txt_PageIndexGO_slConfig.ClientID%>').select();
                        document.getElementById('<%=txt_PageIndexGO_slConfig.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;

        function selectAll(invoker) {
            var grillaTipoTablaConfigxValor = document.getElementById('<%=dgvTipoTablaConfigxValor.ClientID %>');

            if (grillaTipoTablaConfigxValor != null) {
                for (var i = 1; i < grillaTipoTablaConfigxValor.rows.length; i++) {
                    var rowElem = grillaTipoTablaConfigxValor.rows[i];
                    rowElem.cells[0].getElementsByTagName('INPUT')[0].checked = invoker.checked;
                }
            }
            return false;
        }

        function valKeyPressBusquedaTipoTablaValor() {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) { // Enter
                document.getElementById('<%=btnBuscarCttv.ClientID%>').click();
            }
            return true;
        }
        //
    </script>

</asp:Content>
