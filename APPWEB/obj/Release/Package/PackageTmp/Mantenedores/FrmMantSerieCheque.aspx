<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantSerieCheque.aspx.vb" Inherits="APPWEB.FrmMantSerieCheque" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscar">
        <table width="100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                    onmouseover="this.src='/Imagenes/Buscar_A.JPG';" onmouseout="this.src='/Imagenes/Buscar_B.JPG';" />
                                <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='/Imagenes/Nuevo_A.JPG';"
                                    onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" />
                                <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                    onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                    OnClientClick="return(validarSave());" CausesValidation="true" />
                                <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                    onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';"
                                    OnClientClick="return(confirm('Desea cancelar el proceso?'));" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="TituloCelda">
                    Registro de Series de Cheques
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Label">
                                <asp:Label ID="lblBanco" runat="server" Text="Banco:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="cboBanco" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="Label">
                                <asp:Label ID="lblCuentaBancaria" runat="server" Text="Cuenta Bancaria:"></asp:Label>
                            </td>
                            <td>
                                <asp:UpdatePanel ID="upCuentaBancaria" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="cboCuentaBancaria" runat="server">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="cboBanco" 
                                            EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                            <td class="Label">
                                <asp:Label ID="lblEstado" runat="server" Text="Estado:"></asp:Label>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbtlEstado" runat="server" RepeatDirection="Horizontal "
                                    CssClass="Label">
                                    <asp:ListItem Text="Todos" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Activo" Selected="True" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Inactivo" Value="0"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="Label">
                                <asp:Label ID="lblSerie" runat="server" Text="Serie:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSerie" runat="server"></asp:TextBox>
                            </td>
                            <td class="Label">
                                <asp:Label ID="lblNroInicio" runat="server" Text="Nro. Inicio:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNroInicio" runat="server"></asp:TextBox>
                            </td>
                            <td class="Label">
                                <asp:Label ID="lblNroFin" runat="server" Text="Nro. Fin:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNroFin" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Label">
                                <asp:Label ID="lblLongitud" runat="server" Text="Longitud:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtLongitud" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Label">
                                <asp:Label ID="lbldescripcion" runat="server" Text="Descripción:"></asp:Label>
                            </td>
                            <td colspan ="3">
                                <asp:TextBox onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus ="return ( onFocusTextTransform(this,configurarDatos) );" ID="txtDescripcion" runat="server" Width ="100%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdateProgress ID="upGrilla_Progress" runat="server" DisplayAfter="1000" AssociatedUpdatePanelID="upGrilla">
                        <ProgressTemplate>
                            <label class="Label">
                                Cargando...</label></ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="upGrilla" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                    <asp:GridView ID="dgvSerieCheque" runat="server" AutoGenerateColumns="False" CellPadding="2"
                        HorizontalAlign="Justify" EnableViewState="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEditar" runat="server" OnClick="lbtnEditar_Click">Editar</asp:LinkButton>
                                    <asp:HiddenField ID="hddId" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>'
                                        runat="server" />
                                    <asp:HiddenField ID="hddIdBanco" Value='<%# DataBinder.Eval(Container.DataItem,"IdBanco") %>'
                                        runat="server" />
                                    <asp:HiddenField ID="hddIdCuentaBancaria" Value='<%# DataBinder.Eval(Container.DataItem,"IdCuentaBancaria") %>'
                                        runat="server" />
                                    <asp:HiddenField ID="hddIdEstado" Value='<%# DataBinder.Eval(Container.DataItem,"Estado") %>'
                                        runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminar_Click">Eliminar</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                            <asp:BoundField DataField="Banco" HeaderText="Banco" />
                            <asp:BoundField DataField="DescCuentaBancaria" HeaderText="Cuenta Bancaria" />
                            <asp:BoundField DataField="Serie" HeaderText="Serie" NullDisplayText="" />
                            <asp:BoundField DataField="DescNroInicio" HeaderText="Nro. Inicio" />
                            <asp:BoundField DataField="DescNroFin" HeaderText="Nro. Fin" />
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripción" NullDisplayText="" />                            
                            <asp:BoundField DataField="DescEstado" HeaderText="Estado" NullDisplayText="0" />
                        </Columns>
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <HeaderStyle CssClass="GrillaHeader" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                    </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />                            
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    
<asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />


    <script language="javascript" type="text/javascript">
        function validarSave() {
            var Banco = document.getElementById('<%=cboBanco.ClientID%>');
            var idBanco = Banco.value;
            if (idBanco == 0) {
                alert('Seleccione un Banco.');

                return false;
            }
            var CuentaBancaria = document.getElementById('<%=cboCuentaBancaria.ClientID%>');
            var idCuentaBancaria = CuentaBancaria.value;
            if (idCuentaBancaria == 0) {
                alert('Seleccione una Cuenta Bancaria.');

                return false;
            }

            var descripcion = document.getElementById('<%=txtdescripcion.ClientID%>');
            if (descripcion.value.length == 0) {
                alert('Debe Ingresar una Descripción.');
                descripcion.select();
                descripcion.focus();
                return false;
            }

            var Serie = document.getElementById('<%=txtSerie.ClientID%>');
            if (Serie.value.length == 0) {
                alert('Debe Ingresar una Serie.');
                Serie.select();
                Serie.focus();
                return false;
            }

            return (confirm('Desea Continuar Con la Operacion'));
        }
    </script>
    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
