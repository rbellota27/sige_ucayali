<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantVolumenVenta.aspx.vb" Inherits="APPWEB.FrmMantVolumenVenta"
    Title="Volumen M�nimo de Venta" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">
        function valSave() {
            return confirm('Todos los registros seleccionados tendr�n el valor ingresado. Los cambios ser�n grabados en el Sistema. Desea continuar ?');
        }

        function valSave2() {
            var gvProductoVolVenta = document.getElementById('<%=gvProductoVolVenta.ClientID %>');
            if (gvProductoVolVenta == null) {
                alert('La cuadricula de productos esta vacia');
                return false;
            }
            return confirm('Todos los registros seleccionados tendr�n el valor ingresado. Los cambios ser�n grabados en el Sistema. Desea continuar ?');
        }

        function onClick_AceptarVolVenta() {
            var cantidad = 0;

            var tbCantidad = parseFloat(document.getElementById('<%=tbCantidad.ClientID %>').value);
            if (isNaN(tbCantidad)) { tbCantidad = 0; }

            var gvProductoVolVenta = document.getElementById('<%=gvProductoVolVenta.ClientID %>');
            if (gvProductoVolVenta != null) {

                for (var i = 1; i < gvProductoVolVenta.rows.length; i++) {
                    var rowElem = gvProductoVolVenta.rows[i];
                    var txtcantidad = rowElem.cells[3].children[0].cells[0].children[0];
                    txtcantidad.value = redondear(tbCantidad, 2);
                }

            }
            return false;
        }

        function validarNumeroPunto(event) {
            var key = event.keyCode;
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        //
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        
    </script>

    <asp:Panel ID="Panel_PorSubLinea" runat="server">
        <table style="width: 100%">
            <tr>
                <td class="TituloCelda">
                    Volumen M�nimo de Venta
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_PorSubLinea_Cab" runat="server">
                        <fieldset title="Busqueda de Productos" class="FieldSetPanel">
                            <legend>Productos</legend>
                            <table>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="Label_fsp">
                                                    <label>
                                                        Tienda:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cmbTienda" runat="server" DataTextField="Nombre" DataValueField="IdTienda"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Label_fsp">
                                                    <label>
                                                        Tipo de Precio de Venta:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cmbTipoPrecioVenta" runat="server" DataTextField="Nombre" DataValueField="IdTipoPv"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Label_fsp">
                                                    <label>
                                                        Tipo Existencia:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cmbTipoExistencia" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Label_fsp">
                                                    <label>
                                                        L�nea:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cmbLinea" runat="server" AutoPostBack="true" DataTextField="Descripcion"
                                                        DataValueField="Id">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Label_fsp">
                                                    <label>
                                                        Sub-L�nea:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cmbSubLinea" runat="server" DataTextField="Nombre" DataValueField="Id"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                                            CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                                            CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                                            CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                            CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                                            SuppressPostBack="true">
                                        </cc1:CollapsiblePanelExtender>
                                        <asp:Image ID="Image21_11" runat="server" Height="16px" ForeColor="#4277AD" />
                                        <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada"></asp:Label><asp:Panel
                                            ID="Panel_BusqAvanzadoProd" runat="server">
                                            <table width="100">
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td style="font-weight: bold; color: #4277AD">
                                                                    Atributo:
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );"
                                                                        Style="cursor: hand;" />
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" Style="cursor: hand;" />
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="80px" Style="cursor: hand;" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                                            Width="650px">
                                                            <RowStyle CssClass="GrillaRow" />
                                                            <Columns>
                                                                <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                                    HeaderStyle-Height="25px" ItemStyle-Height="25px" />
                                                                <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label><asp:HiddenField
                                                                            ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                                            DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                            <HeaderStyle CssClass="GrillaCabecera" />
                                                            <EditRowStyle CssClass="GrillaEditRow" />
                                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel_GV_Busqueda" runat="server" Width="100%" HorizontalAlign="Center">
        <asp:UpdateProgress ID="upGrilla_Progress" runat="server" DisplayAfter="1000" AssociatedUpdatePanelID="upGrilla">
            <ProgressTemplate>
                <asp:Image ID="ImgAjax" runat="server" ImageUrl="~/Imagenes/ajax.gif" />
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upGrilla" runat="server">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <fieldset title="Registrar Volumen M�nimo de Venta por Productos" class="FieldSetPanel">
                                <legend>Replicaci�n del Volumen M�nimo de Venta</legend>
                                <table>
                                    <tr>
                                        <td>
                                            Cantidad Minima de Venta:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbCantidad" runat="server" onKeyPress="return(    validarNumeroPuntoPositivo('event')       );"
                                                onFocus="return(   aceptarFoco(this)   );"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btn_AceptarVolVenta" runat="server" Text="Aceptar" ToolTip="Cambiar Cantidad Minima de Venta"
                                                OnClientClick="return ( onClick_AceptarVolVenta() );" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btn_Guardar" runat="server" Text="Guardar" OnClientClick="return ( valSave2() );" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvProductoVolVenta" runat="server" AutoGenerateColumns="False"
                                Width="100%">
                                <RowStyle CssClass="GrillaRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="C�digo">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblCodigoProducto" runat="server" Font-Bold="true" Text='<%# DataBinder.Eval(Container.DataItem,"prod_Codigo") %>'
                                                            ForeColor="#4277AD"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Descripcion" DataField="prod_Nombre" />
                                    <asp:BoundField HeaderText="U.M." DataField="um_NombreLargo" />
                                    <asp:TemplateField HeaderText="Cantidad M�nima de Venta">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtcantidad" runat="server" Font-Bold="true" onKeyPress="return(    validarNumeroPuntoPositivo('event')       );"
                                                            onFocus="return(   aceptarFoco(this)   );" Text='<%# DataBinder.Eval(Container.DataItem,"vven_CantidadMin_UMPr","{0:F2}") %>'></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <HeaderStyle CssClass="GrillaCabecera" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
