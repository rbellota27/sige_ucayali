﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO
Partial Public Class FrmImpresionFormatos
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Private listaProductos As New List(Of Entidades.Catalogo)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            CargadePaneles()
            inicializarFrm()
        End If
    End Sub
    Private Sub limpiarCajasdeTexto()
        Me.lblDescripcion.Visible = False
        Me.txtDescripcionProd.Visible = False
        Me.lblcodigoProdBus.Visible = True
        Me.txtcodigoprod.Visible = True
        Me.txtcodigoprod.Text = ""
        Me.txtDescripcionProd.Text = ""
        Me.rbtSeleccion.Visible = True

    End Sub
    Private Sub LimpiarDespuesdeAgregarproductos()
        Me.txtDescripcionProd.Text = ""
        Me.txtcodigoprod.Text = ""
        Me.rbtSeleccion.Visible = True
    End Sub
    Private Function getlistaProductos() As List(Of Entidades.Catalogo)
        Return CType(Session.Item("listaProductos"), List(Of Entidades.Catalogo))
    End Function
    Private Sub setlistaProductos(ByVal lista As List(Of Entidades.Catalogo))
        Session.Remove("listaProductos")
        Session.Add("listaProductos", lista)
    End Sub
    Private Sub CargadePaneles()
        Me.Panel_GV_Detalle.Visible = False
    End Sub
    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        inicializarFrm()
        limpiarCajasdeTexto()
    End Sub
    Protected Sub btnBuscarProducto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarProducto.Click
        Try
            If (ddlTienda.SelectedValue <> "0" And ddlTipoPrecioV.SelectedValue <> "0") Then
                Me.Panel_GV_Detalle.Visible = True
                limpiarCajasdeTexto()
                rbtSeleccion.SelectedValue = "0"
            Else
                objScript.mostrarMsjAlerta(Me, "Seleccione una tienda y un tipo Precio de Venta. No procede la operación.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub inicializarFrm()
        Try
            Session.Remove("listaProductos")
            Dim objCbo As New Combo
            With objCbo
                .LLenarCboTienda(Me.ddlTienda, True)
                .LlenarCboTipoPV(Me.ddlTipoPrecioV, False)
            End With
            btnBuscarProducto.Visible = True
            Panel_GV_Detalle.Visible = False
            Me.listaProductos = New List(Of Entidades.Catalogo)
            setlistaProductos(Me.listaProductos)

            GV_ProductoAgregado.DataSource = Nothing
            GV_ProductoAgregado.DataBind()
            GV_Detalle.DataSource = Nothing
            GV_Detalle.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnBuscarSeleccion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarSeleccion.Click
        Try
            If (rbtSeleccion.SelectedValue = "0") Then
                If (txtcodigoprod.Text = "" Or ddlTipoPrecioV.SelectedValue = "0") Then
                    objScript.offCapa(Me, "Ingrese un código correcto o elija Tipo Precio de Venta para continuar con la búsqueda.")
                Else
                    BuscarProducto()
                End If
            Else
                If (txtDescripcionProd.Text = "" Or ddlTipoPrecioV.SelectedValue = "0") Then
                    objScript.offCapa(Me, "Ingrese una descripción correcta o elija Tipo Precio de Venta para continuar con la búsqueda.")
                Else
                    BuscarProducto()
                End If


            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub BuscarProducto()
        Dim ProductoNombre As String = ""
        Dim codigoProducto As String = ""
        Dim IdTienda As Integer = 0
        Dim IdTipoPV As Integer = 0
        ProductoNombre = txtDescripcionProd.Text
        codigoProducto = txtcodigoprod.Text
        IdTienda = CInt(ddlTienda.SelectedValue)
        IdTipoPV = CInt(ddlTipoPrecioV.SelectedValue)
        Dim ListaProducto As New List(Of Entidades.Catalogo)
        ListaProducto = (New Negocio.Catalogo).SelectProductoxCodigoxDescripcion(IdTienda, IdTipoPV, codigoProducto, ProductoNombre)
        GV_Detalle.DataSource = ListaProducto
        GV_Detalle.DataBind()
    End Sub
    Protected Sub rbtSeleccion_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rbtSeleccion.SelectedIndexChanged
        If (rbtSeleccion.SelectedValue = "0") Then
            lblcodigoProdBus.Visible = True
            txtcodigoprod.Visible = True
            txtDescripcionProd.Text = ""
            lblDescripcion.Visible = False
            txtDescripcionProd.Visible = False
        Else
            lblDescripcion.Visible = True
            txtDescripcionProd.Visible = True
            lblcodigoProdBus.Visible = False
            txtcodigoprod.Visible = False
            txtcodigoprod.Text = ""
        End If
    End Sub
    Protected Sub GV_Detalle_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GV_Detalle.RowCommand
        Me.listaProductos = getlistaProductos()
        If (Me.listaProductos.Count <= 9) Then
            If (e.CommandName = "Select") Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = GV_Detalle.Rows(index)
                Dim ind As Integer = 0
                Dim IdProducto As Integer = 0
                Dim IdTienda As Integer = 0
                Dim IdTipoPV As Integer = 0

                IdProducto = CInt(CType(Me.GV_Detalle.Rows(index).FindControl("hddIdProducto"), HiddenField).Value)
                IdTienda = CInt(CType(Me.GV_Detalle.Rows(index).FindControl("hddIdTienda"), HiddenField).Value)
                IdTipoPV = CInt(CType(Me.GV_Detalle.Rows(index).FindControl("hddIdTipoPV"), HiddenField).Value)
                BuscarProductoYAgregarSession(IdProducto, IdTienda, IdTipoPV)

            End If
        Else
            objScript.mostrarMsjAlerta(Me, "Solo se permiten 10 items como máximo. No procede la operación.")
        End If
    End Sub
    Private Sub BuscarProductoYAgregarSession(ByVal IdProducto As Integer, ByVal IdTienda As Integer, ByVal IdTipoPV As Integer)
        Try
            Dim ObjProducto As Entidades.Catalogo = New Negocio.Catalogo().SelectProductoxIdProducto(IdProducto, IdTienda, IdTipoPV)


            Dim ObProducto2 As Entidades.Catalogo = New Negocio.Catalogo().SelectxIdProductoDetalladoDescripcion(IdProducto)

            If (ObjProducto.IdProducto = ObProducto2.IdProducto) Then
                ObjProducto.Pais = ObProducto2.Pais
                ObjProducto.Atributo1 = ObProducto2.Atributo1
                ObjProducto.Formato = ObProducto2.Formato
                ObjProducto.NomLinea = ObProducto2.NomLinea
                ObjProducto.NomSubLinea = ObProducto2.NomSubLinea
            End If

            Me.listaProductos = getlistaProductos()
            Me.listaProductos.Add(ObjProducto)
            setlistaProductos(Me.listaProductos)

            GV_ProductoAgregado.DataSource = listaProductos
            GV_ProductoAgregado.DataBind()
            Panel_GV_Detalle.Visible = True

            btnBuscarProducto.Visible = False
            GV_Detalle.DataSource = Nothing
            GV_Detalle.DataBind()
            LimpiarDespuesdeAgregarproductos()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub QuitarProducto_Click(ByVal sender As Object, ByVal e As EventArgs)
        Quitar_Producto(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub
    Private Sub Quitar_Producto(ByVal index As Integer)
        Me.listaProductos = getlistaProductos()
        Me.listaProductos.RemoveAt(index)
        Me.GV_ProductoAgregado.DataSource = listaProductos
        Me.GV_ProductoAgregado.DataBind()
    End Sub
    Protected Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        Try
            If (Me.GV_ProductoAgregado.Rows.Count > 0) Then
                Me.listaProductos = getlistaProductos()

                For i As Integer = 0 To Me.listaProductos.Count - 1
                    If (CBool(CType(Me.GV_ProductoAgregado.Rows(i).FindControl("chkprecio"), CheckBox).Checked) = True) Then
                        Me.listaProductos(i).PrecioLista = Me.listaProductos(i).PrecioLista
                    Else
                        Me.listaProductos(i).PrecioLista = 0
                    End If
                Next

                For i As Integer = 0 To Me.listaProductos.Count - 1
                    If (CBool(CType(Me.GV_ProductoAgregado.Rows(i).FindControl("chkpais"), CheckBox).Checked) = True) Then
                        Me.listaProductos(i).Pais = Me.listaProductos(i).Pais
                    Else
                        Me.listaProductos(i).Pais = ""
                    End If
                Next

                For i As Integer = 0 To Me.listaProductos.Count - 1
                    If (CInt(CType(Me.GV_ProductoAgregado.Rows(i).FindControl("hhdIdkit"), HiddenField).Value) = 1) Then
                        Me.listaProductos(i).Atributo1 = Me.listaProductos(i).Descripcion
                        'Me.listaProductos(i).Formato = Me.listaProductos(i).Formato
                    End If

                Next


                Session.Add("listaProductos", listaProductos)
                ' Response.Redirect("PlantillaImpresionMark.aspx")
                'Server.Transfer("PlantillaImpresionMark.aspx")
                'btnImprimir.Attributes.Add("onclick", "win=window.open('./PlantillaImpresionMark.aspx" + "','','width=810,height=540,resizable=no,scrollbars=yes'); win.focus();")

                Dim url As String = "PlantillaImpresionMark.aspx"
                Dim s As String = "window.open('" & url + "', 'popup_window', 'width=810,height=540,left=100,resizable=no,scrollbars=yes');"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "PopUp", s, True)

                Me.GV_ProductoAgregado.DataSource = Nothing
                Me.GV_ProductoAgregado.DataBind()
                btnBuscarProducto.Visible = True
                Panel_GV_Detalle.Visible = False

            Else
                objScript.mostrarMsjAlerta(Me, "Agrege por lo menos 1 producto para continuar con la impresión. No procede la operación.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

End Class