﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO
Partial Public Class PlantillaImpresionMark
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim lista As New List(Of Entidades.Catalogo)
        lista = CType((Session("listaProductos")), Global.System.Collections.Generic.List(Of Global.Entidades.Catalogo))
        GenerarPDF(lista, "../Plantilla/FormatoImpresion2.pdf")
    End Sub
    Private Sub GenerarPDF(ByVal Lista As List(Of Entidades.Catalogo), ByVal NombreReporte As String)

        Dim FileNameTemplate As String = Server.MapPath(NombreReporte)
        Response.Clear()
        Response.ContentType = "application/pdf"
        Dim reader As PdfReader = New PdfReader(FileNameTemplate)
        Dim stamp As PdfStamper = New PdfStamper(reader, Response.OutputStream)

        '
        Dim CodigoProducto1 As String = ""
        Dim CodigoProducto2 As String = ""
        Dim CodigoProducto3 As String = ""
        Dim CodigoProducto4 As String = ""
        Dim CodigoProducto5 As String = ""
        Dim CodigoProducto6 As String = ""
        Dim CodigoProducto7 As String = ""
        Dim CodigoProducto8 As String = ""
        Dim CodigoProducto9 As String = ""
        Dim CodigoProducto10 As String = ""


        Dim NombreProductoTodo1 As String = ""
        Dim NombreProductoTodo2 As String = ""
        Dim NombreProductoTodo3 As String = ""
        Dim NombreProductoTodo4 As String = ""
        Dim NombreProductoTodo5 As String = ""
        Dim NombreProductoTodo6 As String = ""
        Dim NombreProductoTodo7 As String = ""
        Dim NombreProductoTodo8 As String = ""
        Dim NombreProductoTodo9 As String = ""
        Dim NombreProductoTodo10 As String = ""

        Dim NombreProducto1 As String = ""
        Dim NombreProducto2 As String = ""
        Dim NombreProducto3 As String = ""
        Dim NombreProducto4 As String = ""
        Dim NombreProducto5 As String = ""
        Dim NombreProducto6 As String = ""
        Dim NombreProducto7 As String = ""
        Dim NombreProducto8 As String = ""
        Dim NombreProducto9 As String = ""
        Dim NombreProducto10 As String = ""

        Dim PrecioProducto1 As String = ""
        Dim PrecioProducto2 As String = ""
        Dim PrecioProducto3 As String = ""
        Dim PrecioProducto4 As String = ""
        Dim PrecioProducto5 As String = ""
        Dim PrecioProducto6 As String = ""
        Dim PrecioProducto7 As String = ""
        Dim PrecioProducto8 As String = ""
        Dim PrecioProducto9 As String = ""
        Dim PrecioProducto10 As String = ""

        Dim PaisProducto1 As String = ""
        Dim PaisProducto2 As String = ""
        Dim PaisProducto3 As String = ""
        Dim PaisProducto4 As String = ""
        Dim PaisProducto5 As String = ""
        Dim PaisProducto6 As String = ""
        Dim PaisProducto7 As String = ""
        Dim PaisProducto8 As String = ""
        Dim PaisProducto9 As String = ""
        Dim PaisProducto10 As String = ""

        Dim CantidadxCaja1 As String = ""
        Dim CantidadxCaja2 As String = ""
        Dim CantidadxCaja3 As String = ""
        Dim CantidadxCaja4 As String = ""
        Dim CantidadxCaja5 As String = ""
        Dim CantidadxCaja6 As String = ""
        Dim CantidadxCaja7 As String = ""
        Dim CantidadxCaja8 As String = ""
        Dim CantidadxCaja9 As String = ""
        Dim CantidadxCaja10 As String = ""

        Dim MonedaProducto1 As String = ""
        Dim MonedaProducto2 As String = ""
        Dim MonedaProducto3 As String = ""
        Dim MonedaProducto4 As String = ""
        Dim MonedaProducto5 As String = ""
        Dim MonedaProducto6 As String = ""
        Dim MonedaProducto7 As String = ""
        Dim MonedaProducto8 As String = ""
        Dim MonedaProducto9 As String = ""
        Dim MonedaProducto10 As String = ""

        Dim UMProducto1 As String = ""
        Dim UMProducto2 As String = ""
        Dim UMProducto3 As String = ""
        Dim UMProducto4 As String = ""
        Dim UMProducto5 As String = ""
        Dim UMProducto6 As String = ""
        Dim UMProducto7 As String = ""
        Dim UMProducto8 As String = ""
        Dim UMProducto9 As String = ""
        Dim UMProducto10 As String = ""

        Dim Formato1 As String = ""
        Dim Formato2 As String = ""
        Dim Formato3 As String = ""
        Dim Formato4 As String = ""
        Dim Formato5 As String = ""
        Dim Formato6 As String = ""
        Dim Formato7 As String = ""
        Dim Formato8 As String = ""
        Dim Formato9 As String = ""
        Dim Formato10 As String = ""

        ''Primer Producto'***********************'
        For i As Integer = 0 To Lista.Count - 1
            CodigoProducto1 = Lista(i).CodigoProducto
            NombreProducto1 = Lista(i).NomLinea + " " + Lista(i).NomSubLinea
            NombreProductoTodo1 = Lista(i).Atributo1
            Formato1 = Lista(i).Formato
            If (NombreProducto1.Length > 35) Then
                NombreProducto1 = NombreProducto1.Substring(0, 35)
            End If

            If (Lista(i).PrecioLista = 0) Then
                PrecioProducto1 = ""
                UMProducto1 = ""
                MonedaProducto1 = ""
            Else
                PrecioProducto1 = CStr(Math.Round(Lista(i).PrecioLista, 2))
                UMProducto1 = Lista(i).cadenaUM
                MonedaProducto1 = "S/."
            End If
            If (Lista(i).Pais <> "") Then
                PaisProducto1 = Lista(i).Pais
            End If

            If (Lista(i).cadenaUM <> "M2") Then
                CantidadxCaja1 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + "-" + Lista(i).cadenaUM
            Else
                CantidadxCaja1 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + Lista(i).cadenaUM + " X CAJA"
            End If


            Exit For
        Next


        ''Segundo Producto'***********************'
        If (Lista.Count > 1) Then
            For i As Integer = 1 To Lista.Count - 1
                CodigoProducto2 = Lista(i).CodigoProducto
                NombreProducto2 = Lista(i).NomLinea + " " + Lista(i).NomSubLinea
                NombreProductoTodo2 = Lista(i).Atributo1
                Formato2 = Lista(i).Formato
                If (NombreProducto2.Length > 35) Then
                    NombreProducto2 = NombreProducto2.Substring(0, 35)
                End If

                If (Lista(i).PrecioLista = 0) Then
                    PrecioProducto2 = ""
                    MonedaProducto2 = ""
                    UMProducto2 = ""
                Else
                    PrecioProducto2 = CStr(Math.Round(Lista(i).PrecioLista, 2))
                    MonedaProducto2 = "S/."
                    UMProducto2 = Lista(i).cadenaUM
                End If

                If (Lista(i).Pais <> "") Then
                    PaisProducto2 = Lista(i).Pais
                End If


                If (Lista(i).cadenaUM <> "M2") Then
                    CantidadxCaja2 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + " - " + Lista(i).cadenaUM
                Else
                    CantidadxCaja2 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + Lista(i).cadenaUM + " X CAJA"
                End If

                Exit For
            Next

        End If

        ''Tercer Producto'***********************'
        If (Lista.Count > 2) Then
            For i As Integer = 2 To Lista.Count - 1
                CodigoProducto3 = Lista(i).CodigoProducto
                NombreProducto3 = Lista(i).NomLinea + " " + Lista(i).NomSubLinea
                Formato3 = Lista(i).Formato
                If (NombreProducto3.Length > 35) Then
                    NombreProducto3 = NombreProducto3.Substring(0, 35)
                End If
                NombreProductoTodo3 = Lista(i).Atributo1
                If (Lista(i).Pais <> "") Then
                    PaisProducto3 = Lista(i).Pais
                End If

                If (Lista(i).PrecioLista = 0) Then
                    PrecioProducto3 = ""
                    UMProducto3 = ""
                    MonedaProducto3 = ""
                Else
                    PrecioProducto3 = CStr(Math.Round(Lista(i).PrecioLista, 2))
                    UMProducto3 = Lista(i).cadenaUM
                    MonedaProducto3 = "S/."
                End If


                If (Lista(i).cadenaUM <> "M2") Then
                    CantidadxCaja3 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + " - " + Lista(i).cadenaUM
                Else
                    CantidadxCaja3 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + Lista(i).cadenaUM + " X CAJA"
                End If

                Exit For
            Next
        End If

        ''Cuarto Producto'***********************'
        If (Lista.Count > 3) Then
            For i As Integer = 3 To Lista.Count - 1
                CodigoProducto4 = Lista(i).CodigoProducto
                NombreProducto4 = Lista(i).NomLinea + " " + Lista(i).NomSubLinea
                Formato4 = Lista(i).Formato
                NombreProductoTodo4 = Lista(i).Atributo1
                If (NombreProducto4.Length > 35) Then
                    NombreProducto4 = NombreProducto4.Substring(0, 35)
                End If
                If (Lista(i).Pais <> "") Then
                    PaisProducto4 = Lista(i).Pais
                End If

                If (Lista(i).cadenaUM <> "M2") Then
                    CantidadxCaja4 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + " - " + Lista(i).cadenaUM
                Else
                    CantidadxCaja4 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + Lista(i).cadenaUM + " X CAJA"
                End If

                If (Lista(i).PrecioLista = 0) Then
                    PrecioProducto4 = ""
                    MonedaProducto4 = ""
                    UMProducto4 = ""
                Else
                    PrecioProducto4 = CStr(Math.Round(Lista(i).PrecioLista, 2))
                    MonedaProducto4 = "S/."
                    UMProducto4 = Lista(i).cadenaUM
                End If
                Exit For
            Next
        End If

        If (Lista.Count > 4) Then
            ''Quinto Producto'***********************'
            For i As Integer = 4 To Lista.Count - 1
                CodigoProducto5 = Lista(i).CodigoProducto
                NombreProducto5 = Lista(i).NomLinea + " " + Lista(i).NomSubLinea
                NombreProductoTodo5 = Lista(i).Atributo1
                Formato5 = Lista(i).Formato
                If (NombreProducto5.Length > 35) Then
                    NombreProducto5 = NombreProducto5.Substring(0, 35)
                End If

                If (Lista(i).PrecioLista = 0) Then
                    PrecioProducto5 = ""
                    UMProducto5 = ""
                    MonedaProducto5 = ""
                Else
                    PrecioProducto5 = CStr(Math.Round(Lista(i).PrecioLista, 2))
                    UMProducto5 = Lista(i).cadenaUM
                    MonedaProducto5 = "S/."
                End If
                If (Lista(i).Pais <> "") Then
                    PaisProducto5 = Lista(i).Pais
                End If

                If (Lista(i).cadenaUM <> "M2") Then
                    CantidadxCaja5 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + "-" + Lista(i).cadenaUM
                Else
                    CantidadxCaja5 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + Lista(i).cadenaUM + " X CAJA"
                End If


                Exit For
            Next
        End If


        ''Sexto Producto'***********************'
        If (Lista.Count > 5) Then
            For i As Integer = 5 To Lista.Count - 1
                CodigoProducto6 = Lista(i).CodigoProducto
                NombreProducto6 = Lista(i).NomLinea + " " + Lista(i).NomSubLinea
                NombreProductoTodo6 = Lista(i).Atributo1
                Formato6 = Lista(i).Formato
                If (NombreProducto6.Length > 35) Then
                    NombreProducto6 = NombreProducto6.Substring(0, 35)
                End If

                If (Lista(i).PrecioLista = 0) Then
                    PrecioProducto6 = ""
                    MonedaProducto6 = ""
                    UMProducto6 = ""
                Else
                    PrecioProducto6 = CStr(Math.Round(Lista(i).PrecioLista, 2))
                    MonedaProducto6 = "S/."
                    UMProducto6 = Lista(i).cadenaUM
                End If

                If (Lista(i).Pais <> "") Then
                    PaisProducto6 = Lista(i).Pais
                End If
                If (Lista(i).cadenaUM <> "M2") Then
                    CantidadxCaja6 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + " - " + Lista(i).cadenaUM
                Else
                    CantidadxCaja6 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + Lista(i).cadenaUM + " X CAJA"
                End If

                Exit For
            Next

        End If

        ''Septimo Producto'***********************'
        If (Lista.Count > 6) Then
            For i As Integer = 6 To Lista.Count - 1
                CodigoProducto7 = Lista(i).CodigoProducto
                NombreProducto7 = Lista(i).NomLinea + " " + Lista(i).NomSubLinea
                Formato7 = Lista(i).Formato
                If (NombreProducto7.Length > 35) Then
                    NombreProducto7 = NombreProducto7.Substring(0, 35)
                End If
                NombreProductoTodo7 = Lista(i).Atributo1
                If (Lista(i).Pais <> "") Then
                    PaisProducto7 = Lista(i).Pais
                End If

                If (Lista(i).PrecioLista = 0) Then
                    PrecioProducto7 = ""
                    UMProducto7 = ""
                    MonedaProducto7 = ""
                Else
                    PrecioProducto7 = CStr(Math.Round(Lista(i).PrecioLista, 2))
                    UMProducto7 = Lista(i).cadenaUM
                    MonedaProducto7 = "S/."
                End If


                If (Lista(i).cadenaUM <> "M2") Then
                    CantidadxCaja7 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + " - " + Lista(i).cadenaUM
                Else
                    CantidadxCaja7 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + Lista(i).cadenaUM + " X CAJA"
                End If

                Exit For
            Next
        End If

        ''Octavo Producto'***********************'
        If (Lista.Count > 7) Then
            For i As Integer = 7 To Lista.Count - 1
                CodigoProducto8 = Lista(i).CodigoProducto
                NombreProducto8 = Lista(i).NomLinea + " " + Lista(i).NomSubLinea
                Formato8 = Lista(i).Formato
                NombreProductoTodo8 = Lista(i).Atributo1
                If (NombreProducto8.Length > 35) Then
                    NombreProducto8 = NombreProducto8.Substring(0, 35)
                End If
                If (Lista(i).Pais <> "") Then
                    PaisProducto8 = Lista(i).Pais
                End If

                If (Lista(i).cadenaUM <> "M2") Then
                    CantidadxCaja8 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + " - " + Lista(i).cadenaUM
                Else
                    CantidadxCaja8 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + Lista(i).cadenaUM + " X CAJA"
                End If

                If (Lista(i).PrecioLista = 0) Then
                    PrecioProducto8 = ""
                    MonedaProducto8 = ""
                    UMProducto8 = ""
                Else
                    PrecioProducto8 = CStr(Math.Round(Lista(i).PrecioLista, 2))
                    MonedaProducto8 = "S/."
                    UMProducto8 = Lista(i).cadenaUM
                End If
                Exit For
            Next
        End If


        ''Noveno Producto'***********************'
        If (Lista.Count > 8) Then
            For i As Integer = 8 To Lista.Count - 1
                CodigoProducto9 = Lista(i).CodigoProducto
                NombreProducto9 = Lista(i).NomLinea + " " + Lista(i).NomSubLinea
                Formato9 = Lista(i).Formato
                NombreProductoTodo9 = Lista(i).Atributo1
                If (NombreProducto9.Length > 35) Then
                    NombreProducto9 = NombreProducto9.Substring(0, 35)
                End If
                If (Lista(i).Pais <> "") Then
                    PaisProducto9 = Lista(i).Pais
                End If

                If (Lista(i).cadenaUM <> "M2") Then
                    CantidadxCaja9 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + " - " + Lista(i).cadenaUM
                Else
                    CantidadxCaja9 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + Lista(i).cadenaUM + " X CAJA"
                End If

                If (Lista(i).PrecioLista = 0) Then
                    PrecioProducto9 = ""
                    MonedaProducto9 = ""
                    UMProducto9 = ""
                Else
                    PrecioProducto9 = CStr(Math.Round(Lista(i).PrecioLista, 2))
                    MonedaProducto9 = "S/."
                    UMProducto9 = Lista(i).cadenaUM
                End If
                Exit For
            Next
        End If

        ''Decimo Producto'***********************'
        If (Lista.Count > 9) Then
            For i As Integer = 9 To Lista.Count - 1
                CodigoProducto10 = Lista(i).CodigoProducto
                NombreProducto10 = Lista(i).NomLinea + " " + Lista(i).NomSubLinea
                Formato10 = Lista(i).Formato
                NombreProductoTodo10 = Lista(i).Atributo1
                If (NombreProducto10.Length > 35) Then
                    NombreProducto10 = NombreProducto10.Substring(0, 35)
                End If
                If (Lista(i).Pais <> "") Then
                    PaisProducto10 = Lista(i).Pais
                End If

                If (Lista(i).cadenaUM <> "M2") Then
                    CantidadxCaja10 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + " - " + Lista(i).cadenaUM
                Else
                    CantidadxCaja10 = CStr(Math.Round(CDec(Lista(i).Equivalencia), 2)) + Lista(i).cadenaUM + " X CAJA"
                End If

                If (Lista(i).PrecioLista = 0) Then
                    PrecioProducto10 = ""
                    MonedaProducto10 = ""
                    UMProducto10 = ""
                Else
                    PrecioProducto10 = CStr(Math.Round(Lista(i).PrecioLista, 2))
                    MonedaProducto10 = "S/."
                    UMProducto10 = Lista(i).cadenaUM
                End If
                Exit For
            Next
        End If

        ''MANDANDO A LA PLANTILLA PARA IMPRIMIR

        stamp.AcroFields.SetField("tfCodigoProducto1", CodigoProducto1)
        stamp.AcroFields.SetField("tfCodigoProducto2", CodigoProducto2)
        stamp.AcroFields.SetField("tfCodigoProducto3", CodigoProducto3)
        stamp.AcroFields.SetField("tfCodigoProducto4", CodigoProducto4)
        stamp.AcroFields.SetField("tfCodigoProducto5", CodigoProducto5)
        stamp.AcroFields.SetField("tfCodigoProducto6", CodigoProducto6)
        stamp.AcroFields.SetField("tfCodigoProducto7", CodigoProducto7)
        stamp.AcroFields.SetField("tfCodigoProducto8", CodigoProducto8)
        stamp.AcroFields.SetField("tfCodigoProducto9", CodigoProducto9)
        stamp.AcroFields.SetField("tfCodigoProducto10", CodigoProducto10)

        stamp.AcroFields.SetField("tfNombreProducto1", NombreProducto1)
        stamp.AcroFields.SetField("tfNombreProducto2", NombreProducto2)
        stamp.AcroFields.SetField("tfNombreProducto3", NombreProducto3)
        stamp.AcroFields.SetField("tfNombreProducto4", NombreProducto4)
        stamp.AcroFields.SetField("tfNombreProducto5", NombreProducto5)
        stamp.AcroFields.SetField("tfNombreProducto6", NombreProducto6)
        stamp.AcroFields.SetField("tfNombreProducto7", NombreProducto7)
        stamp.AcroFields.SetField("tfNombreProducto8", NombreProducto8)
        stamp.AcroFields.SetField("tfNombreProducto9", NombreProducto9)
        stamp.AcroFields.SetField("tfNombreProducto10", NombreProducto10)



        stamp.AcroFields.SetField("tfPrecioProducto1", PrecioProducto1)
        stamp.AcroFields.SetField("tfPrecioProducto2", PrecioProducto2)
        stamp.AcroFields.SetField("tfPrecioProducto3", PrecioProducto3)
        stamp.AcroFields.SetField("tfPrecioProducto4", PrecioProducto4)
        stamp.AcroFields.SetField("tfPrecioProducto5", PrecioProducto5)
        stamp.AcroFields.SetField("tfPrecioProducto6", PrecioProducto6)
        stamp.AcroFields.SetField("tfPrecioProducto7", PrecioProducto7)
        stamp.AcroFields.SetField("tfPrecioProducto8", PrecioProducto8)
        stamp.AcroFields.SetField("tfPrecioProducto9", PrecioProducto9)
        stamp.AcroFields.SetField("tfPrecioProducto10", PrecioProducto10)



        stamp.AcroFields.SetField("tfPaisProducto1", PaisProducto1)
        stamp.AcroFields.SetField("tfPaisProducto2", PaisProducto2)
        stamp.AcroFields.SetField("tfPaisProducto3", PaisProducto3)
        stamp.AcroFields.SetField("tfPaisProducto4", PaisProducto4)
        stamp.AcroFields.SetField("tfPaisProducto5", PaisProducto5)
        stamp.AcroFields.SetField("tfPaisProducto6", PaisProducto6)
        stamp.AcroFields.SetField("tfPaisProducto7", PaisProducto7)
        stamp.AcroFields.SetField("tfPaisProducto8", PaisProducto8)
        stamp.AcroFields.SetField("tfPaisProducto9", PaisProducto9)
        stamp.AcroFields.SetField("tfPaisProducto10", PaisProducto10)

        stamp.AcroFields.SetField("tfCantidadProducto1", CantidadxCaja1)
        stamp.AcroFields.SetField("tfCantidadProducto2", CantidadxCaja2)
        stamp.AcroFields.SetField("tfCantidadProducto3", CantidadxCaja3)
        stamp.AcroFields.SetField("tfCantidadProducto4", CantidadxCaja4)
        stamp.AcroFields.SetField("tfCantidadProducto5", CantidadxCaja5)
        stamp.AcroFields.SetField("tfCantidadProducto6", CantidadxCaja6)
        stamp.AcroFields.SetField("tfCantidadProducto7", CantidadxCaja7)
        stamp.AcroFields.SetField("tfCantidadProducto8", CantidadxCaja8)
        stamp.AcroFields.SetField("tfCantidadProducto9", CantidadxCaja9)
        stamp.AcroFields.SetField("tfCantidadProducto10", CantidadxCaja10)


        stamp.AcroFields.SetField("tfUMProducto1", UMProducto1)
        stamp.AcroFields.SetField("tfUMProducto2", UMProducto2)
        stamp.AcroFields.SetField("tfUMProducto3", UMProducto3)
        stamp.AcroFields.SetField("tfUMProducto4", UMProducto4)
        stamp.AcroFields.SetField("tfUMProducto5", UMProducto5)
        stamp.AcroFields.SetField("tfUMProducto6", UMProducto6)
        stamp.AcroFields.SetField("tfUMProducto7", UMProducto7)
        stamp.AcroFields.SetField("tfUMProducto8", UMProducto8)
        stamp.AcroFields.SetField("tfUMProducto9", UMProducto9)
        stamp.AcroFields.SetField("tfUMProducto10", UMProducto10)

        stamp.AcroFields.SetField("tfMonedaProducto1", MonedaProducto1)
        stamp.AcroFields.SetField("tfMonedaProducto2", MonedaProducto2)
        stamp.AcroFields.SetField("tfMonedaProducto3", MonedaProducto3)
        stamp.AcroFields.SetField("tfMonedaProducto4", MonedaProducto4)
        stamp.AcroFields.SetField("tfMonedaProducto5", MonedaProducto5)
        stamp.AcroFields.SetField("tfMonedaProducto6", MonedaProducto6)
        stamp.AcroFields.SetField("tfMonedaProducto7", MonedaProducto7)
        stamp.AcroFields.SetField("tfMonedaProducto8", MonedaProducto8)
        stamp.AcroFields.SetField("tfMonedaProducto9", MonedaProducto9)
        stamp.AcroFields.SetField("tfMonedaProducto10", MonedaProducto10)

        stamp.AcroFields.SetField("tfPaisProducto1", PaisProducto1)
        stamp.AcroFields.SetField("tfPaisProducto2", PaisProducto2)
        stamp.AcroFields.SetField("tfPaisProducto3", PaisProducto3)
        stamp.AcroFields.SetField("tfPaisProducto4", PaisProducto4)
        stamp.AcroFields.SetField("tfPaisProducto5", PaisProducto5)
        stamp.AcroFields.SetField("tfPaisProducto6", PaisProducto6)
        stamp.AcroFields.SetField("tfPaisProducto7", PaisProducto7)
        stamp.AcroFields.SetField("tfPaisProducto8", PaisProducto8)
        stamp.AcroFields.SetField("tfPaisProducto9", PaisProducto9)
        stamp.AcroFields.SetField("tfPaisProducto10", PaisProducto10)

        stamp.AcroFields.SetField("tfNombreProductoAtributo1", NombreProductoTodo1)
        stamp.AcroFields.SetField("tfNombreProductoAtributo2", NombreProductoTodo2)
        stamp.AcroFields.SetField("tfNombreProductoAtributo3", NombreProductoTodo3)
        stamp.AcroFields.SetField("tfNombreProductoAtributo4", NombreProductoTodo4)
        stamp.AcroFields.SetField("tfNombreProductoAtributo5", NombreProductoTodo5)
        stamp.AcroFields.SetField("tfNombreProductoAtributo6", NombreProductoTodo6)
        stamp.AcroFields.SetField("tfNombreProductoAtributo7", NombreProductoTodo7)
        stamp.AcroFields.SetField("tfNombreProductoAtributo8", NombreProductoTodo8)
        stamp.AcroFields.SetField("tfNombreProductoAtributo9", NombreProductoTodo9)
        stamp.AcroFields.SetField("tfNombreProductoAtributo10", NombreProductoTodo10)

        stamp.AcroFields.SetField("tfFormato1", Formato1)
        stamp.AcroFields.SetField("tfFormato2", Formato2)
        stamp.AcroFields.SetField("tfFormato3", Formato3)
        stamp.AcroFields.SetField("tfFormato4", Formato4)
        stamp.AcroFields.SetField("tfFormato5", Formato5)
        stamp.AcroFields.SetField("tfFormato6", Formato6)
        stamp.AcroFields.SetField("tfFormato7", Formato7)
        stamp.AcroFields.SetField("tfFormato8", Formato8)
        stamp.AcroFields.SetField("tfFormato9", Formato9)
        stamp.AcroFields.SetField("tfFormato10", Formato10)

        stamp.FormFlattening = True
        stamp.Close()
        reader.Close()
        Response.End()

        Session.Remove("listaProductos")

    End Sub
End Class