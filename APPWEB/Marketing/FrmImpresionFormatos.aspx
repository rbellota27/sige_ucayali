<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmImpresionFormatos.aspx.vb" Inherits="APPWEB.FrmImpresionFormatos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table class="style1 ">
        <tr>
       <td class="TituloCelda">
                IMPRESI�N  DE PLANTILLA DE PRODUCTOS
            </td>
            </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nueva Impresi�n" ToolTip="Nuevo" Width="240px" />
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><asp:ImageButton id="btnImprimir" runat="server" ImageUrl="~/Imagenes/Imprimir.png"    OnClientClick="return(  ValonclickImprimir()  );"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td>
        <table>
        <tr>
                 <td> <asp:Label ID="lblTienda" runat="server" align="right" class="Texto" Text="Tienda:"></asp:Label></td>
                 <td> <asp:DropDownList ID="ddlTienda" runat="server" Width="150px"></asp:DropDownList></td>
            
                 </tr>
        <tr>
                 <td>
                 <asp:Label ID="lblTipoPrecio" runat="server" Text="Tipo Precio V.:" align="right" class="Texto"></asp:Label>  
                 </td>
                 <td>
                 <asp:DropDownList ID="ddlTipoPrecioV" runat="server" Width="120px"></asp:DropDownList>
                 </td>
                 </tr>
      
                
        </table>
                </td>
        </tr>
           <tr>
                        <td class="SubTituloCelda">
                          PRODUCTOS AGREGADOS
                        </td>
                    </tr>
        <tr>
        <td>
         <asp:Panel ID="PnlProductoagregado" runat="server" Width="100%" 
                                    ScrollBars="Horizontal" style="margin-bottom: 0px">
        <asp:GridView ID="GV_ProductoAgregado" runat="server" AutoGenerateColumns ="false" Width="100%">
         <HeaderStyle CssClass="GrillaHeader" />
         <Columns>      
                        
                    <asp:TemplateField HeaderText="Opciones">
                                        <ItemTemplate>
                                            <table>
                                                <tr><td>
                                                        <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                    </td>

                                                    <td>
                                                        <asp:LinkButton ID="QuitarProducto" runat="server" OnClick="QuitarProducto_Click" >Quitar</asp:LinkButton>
                                                    </td>
  
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                           <asp:TemplateField ShowHeader="False" HeaderText="Codigo"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center"   ItemStyle-Height="25px">
                            <ItemTemplate>
                                           <asp:Label ID="lblcodigoprod" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CodigoProducto")%>'  style="color:Red " ></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"  Height="25px"></ItemStyle>
                            
                        </asp:TemplateField> 
                        
                          <asp:TemplateField ShowHeader="False" HeaderText="Nombre"   HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center"  ItemStyle-Height="25px" >
                            <ItemTemplate >
                                           <asp:Label ID="lblnombreprod" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Descripcion")%>'  ></asp:Label>
                            </ItemTemplate>
                          
                        </asp:TemplateField> 
                        
                           <asp:TemplateField ShowHeader="False" HeaderText="Precio"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center"  ItemStyle-Height="25px">
                            <ItemTemplate>
                                           <asp:Label ID="lblnprecioprod" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioLista","{0:F3}")%>'  style="color:Red "  ></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField> 
                        
                           <asp:TemplateField ShowHeader="False" HeaderText="Tienda"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center"  ItemStyle-Height="25px">
                            <ItemTemplate>
                                           <asp:Label ID="lblTiendaprod" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'  style="color:Red "  ></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField> 
                        
                         <asp:TemplateField ShowHeader="False" HeaderText="UM"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center"  ItemStyle-Height="25px">
                            <ItemTemplate>
                                           <asp:Label ID="lblUNprod" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cadenaUM")%>' ></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            
                        </asp:TemplateField> 
                        
                          <asp:TemplateField ShowHeader="False" HeaderText="Equivalencia por Caja"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center"  ItemStyle-Height="25px">
                            <ItemTemplate>
                                           <asp:Label ID="lblEquivalencia" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Equivalencia")%>' ></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            
                        </asp:TemplateField> 
                         <asp:TemplateField ShowHeader="False" HeaderText="Pa�s"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center"  ItemStyle-Height="25px">
                            <ItemTemplate>
                                           <asp:Label ID="lblpais" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Pais")%>' ></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            
                        </asp:TemplateField> 
                        
                        
                         <asp:TemplateField ShowHeader="False" HeaderText="Pais"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center"  ItemStyle-Height="25px">
                            <ItemTemplate>
                                           <asp:CheckBox id="chkpais" runat="server" Enabled="true" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            
                        </asp:TemplateField> 
                         
                         <asp:TemplateField ShowHeader="False" HeaderText="Precio"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" 
                            HeaderStyle-HorizontalAlign="Center"  ItemStyle-Height="25px">
                            <ItemTemplate>
                                           <asp:CheckBox id="chkprecio" runat="server" Enabled="true" />
                                           <asp:HiddenField  ID="hhdIdkit" runat="server"  Value='<%#DataBinder.Eval(Container.DataItem,"IsKit")%>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            
                        </asp:TemplateField> 
         
         </Columns>
           <RowStyle CssClass="GrillaRow" />
        <FooterStyle CssClass="GrillaFooter" />
        <SelectedRowStyle CssClass="GrillaSelectedRow" />
        <HeaderStyle CssClass="GrillaCabecera" />
        <EditRowStyle CssClass="GrillaEditRow" />
        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
         </asp:GridView>
      </asp:Panel>
         </td>
        </tr>
        <tr>
                <td class="TituloCelda">
                    B�SQUEDA DE PRODUCTOs
                </td>
        </tr>
          <tr>
                <td >
                <asp:ImageButton id="btnBuscarProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_A.jpg"/>
                </td>
                </tr>
         <tr>
               <td>
                      <asp:Panel ID="Panel_GV_Detalle" runat="server" Width="100%" 
                                    ScrollBars="Horizontal" style="margin-bottom: 0px">
                             <table width="100%" cellpadding="0" cellspacing="0">
                                 <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                             <asp:RadioButtonList RepeatDirection="Horizontal" runat="server" ID="rbtSeleccion" Visible="false" AutoPostBack="true" CssClass="Texto">
                                                            <asp:ListItem Value="0" Text="Por C�digo" Selected="True" ></asp:ListItem>
                                                            <asp:ListItem Value="1" Text="Por Descripci�n"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                            </td>
                                    
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                           <td>
                                                                <asp:Label ID="lblcodigoProdBus" runat="server" Text="Ingrese el c�digo:" align="right" class="Texto" Width="120px"></asp:Label>
                                                                <asp:Label ID="lblDescripcion" runat="server" Text="Ingrese el descripci�n:" align="right" class="Texto"></asp:Label>
                                                         </td>
                                                            <td>
                                                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarSeleccion">
                                                                <asp:TextBox ID="txtcodigoprod" runat="server" Text="" Width="130px">
                                                                 </asp:TextBox>
                                                                 </asp:Panel>
                                  
                                                                <asp:TextBox ID="txtDescripcionProd" runat="server" Text="" Width="320px">
                                                                </asp:TextBox>
                                                             </td>
                                                              <td> <asp:Button  ID="btnBuscarSeleccion" runat="server" Text="Buscar"/></td>
                                  
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                       </tr>
                                        <tr>
                                            <td>
                                             <asp:Panel ID="Panel_MovCuenta_CxP" runat="server" Height="300px" ScrollBars="Vertical">
                                                <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" 
                                                    Width="96%">
                                                    <Columns>
                                             <asp:TemplateField>
                                            <ItemTemplate>
                                              <asp:Button ID="btnSeleccionar" runat="server" 
                                              CommandName="Select" 
                                              CommandArgument="<%# CType(Container,GridViewRow).RowIndex %>"
                                              Text="Seleccionar" />
                                                 <asp:HiddenField ID="hddIdProducto" Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>'
                                                    runat="server" />
                                                    <asp:HiddenField ID="hddIdTipoPV" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoPV") %>'
                                                    runat="server" />
                                                     <asp:HiddenField ID="hddIdTienda" Value='<%# DataBinder.Eval(Container.DataItem,"IdTienda") %>'
                                                    runat="server" />
                                               </ItemTemplate> 
                                            </asp:TemplateField>
                                                        <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Producto" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblDescripcion" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Descripcion")%>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                      
                                                        <asp:BoundField DataField="PrecioLista" DataFormatString="{0:F2}" HeaderText="Precio"
                                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                 
 
                                                       
                                                     
                                                    </Columns>
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                       
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
    
       
        <tr>
        <td>
        <table>
       
               
                 
                 </table>
                 </td>
        </tr>
        </table>
        
        
   <script language="javascript" type="text/javascript">
       Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
       Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
           if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
               offsetX = Math.floor(offsetX);
               offsetY = Math.floor(offsetY);
           }
           this._origOnFormActiveElement(element, offsetX, offsetY);
       };
       function ValonclickImprimir() {
           return (confirm('Esta seguro que desea continuar con la impresi�n?'));
       }

       function pop() {
           window.open('PlantillaImpresionMark.aspx', 'popup_window', 'width=810,height=540,left=100,resizable=no,scrollbars=yes');
       }  
        </script>
</asp:Content>

