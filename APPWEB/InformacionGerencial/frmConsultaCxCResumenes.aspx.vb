﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmConsultaCxCResumenes
    Inherits System.Web.UI.Page

    Private objCombo As New Combo
    Private objscript As New ScriptManagerClass
    Private objnegocio As New Negocio.CXCResumenView
    Private objNegUsu As New Negocio.UsuarioView
    Dim LCxCDocumento As List(Of Entidades.CxCResumenView)
    Dim LCxCResumen As List(Of Entidades.CxCResumenView)
    Dim LCliente As List(Of Entidades.CxCResumenView)

    Private Property listaCuentasPorCobrar() As List(Of Entidades.CxCResumenView)
        Get
            Return CType(Session("listaCuentasPorCobrar"), List(Of Entidades.CxCResumenView))
        End Get
        Set(ByVal value As List(Of Entidades.CxCResumenView))
            Session.Remove("listaCuentasPorCobrar")
            Session.Add("listaCuentasPorCobrar", value)
        End Set
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Call inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try
            Me.listaCuentasPorCobrar = New List(Of Entidades.CxCResumenView)
            Me.ocultacontrolesMoneda(False)
            Me.OcultarControles(False)
            ImprimirDoc.Visible = False
            objCombo.LlenarCboPropietario(Me.ddlEmpresa, True)
            'objCombo.LLenarCboTienda(Me.cboTienda, True)

            If objNegUsu.FunVerTodasTiendas(CInt(Session("IdUsuario")), 10) > 0 Then
                objCombo.LlenarCboTiendaxIdUsuario(Me.cboTienda, CInt(Session("IdUsuario")), True)
            Else
                objCombo.LlenarCboTiendaxIdUsuario(Me.cboTienda, CInt(Session("IdUsuario")), False)
            End If

            objCombo.LlenarCboPropietario(Me.ddlidEmpresaCliente, True)

            'objCombo.LLenarCboTienda(Me.ddlIdTiendaCliente, True)

            If objNegUsu.FunVerTodasTiendas(CInt(Session("IdUsuario")), 10) > 0 Then
                objCombo.LlenarCboTiendaxIdUsuario(Me.ddlIdTiendaCliente, CInt(Session("IdUsuario")), True)
            Else
                objCombo.LlenarCboTiendaxIdUsuario(Me.ddlIdTiendaCliente, CInt(Session("IdUsuario")), False)
            End If

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub OcultarControles(ByVal flag As Boolean)
        Me.gvwCxCxMoneda.Visible = flag
        Me.txtMontoDolares.Visible = flag
        Me.txtMontoSoles.Visible = flag
        Me.gvwCxCxDocumento.Visible = flag
        Me.Label3.Visible = flag
        Me.Label4.Visible = flag
        Me.gvwCxCxDocumento.DataSource = Nothing
        Me.gvwCxCxDocumento.DataBind()
        Me.gvwCxCxDocumento.Visible = flag
    End Sub
    Private Sub ocultaControlesDocumento(ByVal flag As Boolean)
        Me.gvwCxCxDocumento.Visible = flag
        Me.ImprimirDoc.Visible = flag
    End Sub
    Private Sub ocultacontrolesMoneda(ByVal flag As Boolean)
        Me.txtMontoDolares.Visible = flag
        Me.txtMontoSoles.Visible = flag
        Me.Label3.Visible = flag
        Me.Label4.Visible = flag
    End Sub

    Private Function totalizarMontos(ByVal gvw As GridView, ByVal filainicial As Integer, ByVal filafinal As Integer, ByVal columna As Byte) As Decimal
        Dim montoTotal As Decimal = 0
        For x As Int32 = filainicial To filafinal
            montoTotal += Convert.ToDecimal(gvw.Rows(x).Cells(columna).Text)
        Next
        Return montoTotal
    End Function
    Private Sub CargaGrillaCxCResumenMoneda()
        '***** Carga los datos de la Grilla
        LCxCResumen = (New Negocio.CXCResumenView).SelectGrillaCxCResumen(CInt(Me.ddlEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue))
        If LCxCResumen Is Nothing Then LCxCResumen = New List(Of Entidades.CxCResumenView)


        If LCxCResumen.Count = 0 Then
            Me.lbl_Mensaje.Text = "No hay datos para mostrar"
            Me.lbl_Mensaje.Visible = True
            Me.OcultarControles(False)
            Me.ocultaControlesDocumento(False)
            Me.ocultacontrolesMoneda(False)
        Else
            Me.lbl_Mensaje.Visible = False
            Me.lbl_Mensaje.Text = ""
            Me.OcultarControles(True)
            Me.ocultacontrolesMoneda(True)
        End If


        gvwCxCxMoneda.DataSource = LCxCResumen
        gvwCxCxMoneda.DataBind()

        Me.txtMontoDolares.Text = ""
        Me.txtMontoSoles.Text = ""

        If LCxCResumen.Count > 0 Then
            Dim nroS As Decimal
            nroS = totalizarMontos(gvwCxCxMoneda, 0, gvwCxCxMoneda.Rows.Count - 1, 4)
            Me.txtMontoSoles.Text = CStr(Math.Round(nroS, 3))

            'If Me.gvwCxCxMoneda.Rows.Count > 1 Then
            If IsNumeric(gvwCxCxMoneda.Rows(0).Cells(2).Text) Then
                If CDec(gvwCxCxMoneda.Rows(0).Cells(2).Text) > 0 Then
                    Dim nro As Decimal
                    nro = Convert.ToDecimal(Me.txtMontoSoles.Text) / Convert.ToDecimal(gvwCxCxMoneda.Rows(0).Cells(2).Text)
                    'Me.txtMontoDolares.Text = CStr(Convert.ToDecimal(Me.txtMontoSoles.Text) / Convert.ToDecimal(gvwCxCxMoneda.Rows(1).Cells(2).Text))
                    Me.txtMontoDolares.Text = CStr(Math.Round(nro, 3))
                End If
            End If
            'End If

        End If

    End Sub
    Protected Sub ddlEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlEmpresa.SelectedIndexChanged

        'objCombo.llenarCboTiendaxIdEmpresa(Me.cboTienda, CInt(Me.ddlEmpresa.SelectedValue), True)

        If objNegUsu.FunVerTodasTiendas(CInt(Session("IdUsuario")), 10) > 0 Then
            objCombo.LlenarCboTiendaxIdUsuario(Me.cboTienda, CInt(Session("IdUsuario")), True)
        Else
            objCombo.LlenarCboTiendaxIdUsuario(Me.cboTienda, CInt(Session("IdUsuario")), False)
        End If


    End Sub
    Public Function SelectGrillaCliente(ByVal idEmpresa As Integer, ByVal idtienda As Integer, ByVal idpersona As Integer) As List(Of Entidades.CxCResumenView)
        '***** Carga los datos de la Grilla cliente
        Dim CxCCliente As New Negocio.CXCResumenView
        Dim LCxCCliente As New List(Of Entidades.CxCResumenView)
        LCxCCliente = CxCCliente.SelectGrillaCxCResumenCliente(idEmpresa, idtienda, idpersona)
        Return LCxCCliente
    End Function

    Protected Sub cmd_BuscaCliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_BuscaCliente.Click

    End Sub

    Private Sub GvwCxCxClientes_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvwCxCxClientes.PageIndexChanging
        Me.LCliente = Me.listaCuentasPorCobrar

        Me.GvwCxCxClientes.PageIndex = e.NewPageIndex
        Me.GvwCxCxClientes.DataSource = Me.LCliente
        Me.GvwCxCxClientes.DataBind()
        listaCuentasPorCobrar = Me.LCliente

    End Sub

    Protected Sub GvwCxCxClientes_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GvwCxCxClientes.SelectedIndexChanged
        'un cliente y su deuda
        Session("idpersonaG") = CInt(Me.GvwCxCxClientes.SelectedRow.Cells(1).Text)
        Dim idpersona As Integer = CInt(Me.GvwCxCxClientes.SelectedRow.Cells(1).Text)
        ImprimirDoc.Visible = True

        Dim CxCResumentocumento As New Negocio.CXCResumenView

        'LCxCDocumento = Me.SelectGrillaDocumento(CInt(Me.ddlEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.GvwCxCxClientes.SelectedRow.Cells(2).Text), CInt(Me.GvwCxCxClientes.SelectedRow.Cells(1).Text))
        LCxCDocumento = CxCResumentocumento.SelectGrillaCxCResumenDocumento(CInt(Me.ddlidEmpresaCliente.SelectedValue), CInt(Me.ddlIdTiendaCliente.SelectedValue), 0, CInt(Session("idpersonaG").ToString))
        '
        If LCxCDocumento.Count = 0 Then
            Me.lblMensajeDocumento.Text = "No hay datos para mostrar"
            Me.lblMensajeDocumento.Visible = True
            Me.ocultaControlesDocumento(False)

        Else
            Me.lblMensajeDocumento.Visible = False
            Me.lblMensajeDocumento.Text = ""
            Me.ocultaControlesDocumento(True)

        End If

        gvwCxCxDocumento.DataSource = LCxCDocumento
        gvwCxCxDocumento.DataBind()
    End Sub
    Protected Sub cmd_Imprimir_Cliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Imprimir_Cliente.Click
        If Me.GvwCxCxClientes.Rows.Count = 1 Then
            'If CInt(DGV_BuscarPersona.SelectedRow.Cells(1).Text.Trim) <> 0 Then
            '    Session("idpersonaAll") = CInt(DGV_BuscarPersona.SelectedRow.Cells(1).Text.Trim)
            'Else
            '    Session("idpersonaAll") = 0
            'End If
            Session("idpersonaAll") = Session("idpersonaAll") 'CInt(DGV_BuscarPersona.SelectedRow.Cells(1).Text.Trim)
        Else

            Session("idpersonaAll") = 0
        End If

    End Sub

    Protected Sub ImprimirDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImprimirDoc.Click
        'Response.Redirect("./reportes/VisorRG.aspx")
    End Sub

    Private Sub CargaDatosGrillaClientes(ByVal grilla As GridView, Optional ByVal idpersona As Integer = 0)
        If idpersona >= 0 Then
            'ViewState.Add("IdPersona", idpersona)

            LCliente = Me.SelectGrillaCliente(CInt(ddlidEmpresaCliente.SelectedValue), CInt(ddlIdTiendaCliente.SelectedValue), idpersona)
            Me.ocultaControlesDocumento(False)
            If LCliente.Count = 0 Then
                Me.lblMensajeCliente.Text = "No hay datos para mostrar"
                Me.lblMensajeCliente.Visible = True
            Else
                Me.lblMensajeCliente.Visible = False
                Me.lblMensajeCliente.Text = ""
            End If
            Me.listaCuentasPorCobrar = LCliente
            grilla.DataSource = LCliente
            grilla.DataBind()
        End If
    End Sub
    Protected Sub btnAceptaCliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptaCliente.Click
        Me.CargaDatosGrillaClientes(Me.GvwCxCxClientes)
        If Me.GvwCxCxClientes.Rows.Count = 1 Then
            Session("idpersonaAll") = Session("idpersonaAll") 'CInt(DGV_BuscarPersona.SelectedRow.Cells(1).Text.Trim)
        Else

            Session("idpersonaAll") = 0
        End If
    End Sub

    Protected Sub ddlidEmpresaCliente_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlidEmpresaCliente.SelectedIndexChanged
        Me.objCombo.llenarCboTiendaxIdEmpresa(ddlIdTiendaCliente, CInt(Me.ddlidEmpresaCliente.SelectedValue), True)
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        CargaGrillaCxCResumenMoneda()
    End Sub

    Private Sub DGV_BuscarPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_BuscarPersona.SelectedIndexChanged
        Me.GvwCxCxClientes.DataSource = Nothing
        GvwCxCxClientes.DataBind()

        Session("idpersonaAll") = CInt(DGV_BuscarPersona.SelectedRow.Cells(1).Text.Trim)

        ViewState.Add("IdPersona", CInt(DGV_BuscarPersona.SelectedRow.Cells(1).Text.Trim))
        Dim persona As Integer = CInt(ViewState("IdPersona"))
        Me.CargaDatosGrillaClientes(Me.GvwCxCxClientes, CInt(ViewState("IdPersona")))
        CierrarCapa()
    End Sub
    Private Sub LLenarCliente(ByVal cliente As Entidades.PersonaView)
        Dim NegPersonaTipoAgente As New Negocio.PersonaTipoAgente
        Dim TipoAgente As New Entidades.TipoAgente
        TipoAgente = NegPersonaTipoAgente.SelectIdAgenteTasaxIdPersona(cliente.IdPersona)
    End Sub
    Private Sub addPersona(ByVal IdPersona As Integer)
        Try
            Dim objCliente As Entidades.PersonaView = (New Negocio.Cliente).SelectxId(IdPersona)
            Me.LLenarCliente(objCliente)
            '********* Para 
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "addPersona_Venta();", True)
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "El Cliente no existe.")
        End Try
    End Sub
    Private Sub CierrarCapa()
        Try
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "CierraPersona();", True)
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "El Cliente no existe.")
        End Try

    End Sub
    Private Sub LevantaCapaDetalleAbono(ByVal iddocumento As Integer)
        Try
            'nroDias

            Dim objDetAbono As List(Of Entidades.CxCResumenView) = (New Negocio.CXCResumenView).GetDetalleAbonos(iddocumento)

            For Each _Row As Entidades.CxCResumenView In objDetAbono
                If (_Row.nroDias <= 0) Then
                    _Row.nroDias = 0
                End If
            Next

            Me.gvwDetalleAbonos.DataSource = objDetAbono
            Me.gvwDetalleAbonos.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapaDetalleAbonos();", True)

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "No Tiene Detalle.")
        End Try
    End Sub
    Private Sub BuscarClientexDni()

        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            Me.LLenarCliente(Cliente)
        Catch ex As Exception
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub BuscarClientexRuc()

        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            Me.LLenarCliente(Cliente)
        Catch ex As Exception
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub BuscarClientexId(ByVal IdCliente As Integer)
        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            Cliente = obj.SelectxId(IdCliente)
            Me.LLenarCliente(Cliente)
        Catch ex As Exception
            'MsgBox1.ShowMessage(ex.Message)
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosPersonaGrilla(ByVal grilla As GridView, ByVal texto As String, ByVal opcion As Integer, ByVal tipoMov As Integer)
        Try
            '********** opcion 0: descripcion
            '********** opcion 1: ruc
            '********** opcion 2: dni
            Dim index As Integer = 0
            Select Case tipoMov
                Case 0 '**************** INICIO
                    index = 0
                Case 1 '**************** Anterior
                    index = (CInt(txtPageIndex_Persona.Text) - 1) - 1
                Case 2 '**************** Posterior
                    index = (CInt(txtPageIndex_Persona.Text) - 1) + 1
                Case 3 '***************** IR
                    index = (CInt(txtPageIndexGO_Persona.Text) - 1)
            End Select

            Dim listaPV As List(Of Entidades.PersonaView) = (New Negocio.PersonaView).SelectActivoxParam_Paginado(texto, opcion, grilla.PageSize, index)

            If listaPV.Count <= 0 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onload ", " alert('No se hallaron registros.'); onCapa('capaPersona');  ", True)
            Else
                grilla.DataSource = listaPV
                grilla.DataBind()
                txtPageIndex_Persona.Text = CStr(index + 1)
                ScriptManager.RegisterStartupScript(Me, Me.GetType, " onload ", "  onCapa('capaPersona');  ", True)
            End If

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "Problemas en la carga de datos Persona.")
        End Try
    End Sub


    Private Sub DGV_BuscarPersona_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_BuscarPersona.PageIndexChanging
        'DGV_BuscarPersona.PageIndex = e.NewPageIndex
        'cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, txtBuscarPersona_Grilla.Text, CInt(Me.cmbFiltro_BuscarPersona.SelectedValue))
    End Sub
    Private Sub btnAnterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 1)
    End Sub
    Private Sub btnPosterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 2)
    End Sub
    Private Sub btnIr_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 3)
    End Sub
    Private Sub gvwCxCxDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvwCxCxDocumento.SelectedIndexChanged
        Try
            ViewState.Add("iddocumento", Me.gvwCxCxDocumento.SelectedRow.Cells(1).Text)
            Dim doc As Integer = CInt(ViewState("iddocumento"))
            LevantaCapaDetalleAbono(doc)
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarPersona_Grillas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarPersona_Grillas.Click
        ViewState.Add("TextoBuscarPersona", txtBuscarPersona_Grilla.Text)
        ViewState.Add("OpcionBuscarPersona", Me.cmbFiltro_BuscarPersona.SelectedValue)
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, txtBuscarPersona_Grilla.Text, CInt(Me.cmbFiltro_BuscarPersona.SelectedValue), 0)
    End Sub
End Class