﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class VisorGrillaaspx
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim iReporte As String = Request.QueryString("iReporte")

            Me.lblTitulo.Width = 50
            lblTitulo.ForeColor = Drawing.Color.Black
            lblTitulo.Font.Size = 20
            lblTitulo.Height = 40
            lblTitulo.BackColor = Drawing.Color.SkyBlue

            If iReporte Is Nothing Then
                iReporte = ""
            End If

            ViewState.Add("iReporte", iReporte)
            Select Case iReporte


                Case "1"
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdLinea", Request.QueryString("IdLinea"))
                    ViewState.Add("IdSublinea", Request.QueryString("IdSublinea"))
                    ViewState.Add("IdProducto", Request.QueryString("IdProducto"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))

            End Select
        End If

        mostrarReporte()
    End Sub
    Private Sub mostrarReporte()
        Select Case ViewState.Item("iReporte").ToString

            Case "1"
                reporteVentasxTiendaExportExcel()

        End Select
    End Sub
    Private Sub reporteVentasxTiendaExportExcel()
        Try
            Dim vidempresa, vIdLinea, vIdSublinea, vidproducto As Integer
            vidempresa = CInt(ViewState.Item("IdEmpresa"))
            vIdLinea = CInt(ViewState.Item("IdLinea"))
            vIdSublinea = CInt(ViewState.Item("IdSublinea"))
            vidproducto = CInt(ViewState.Item("IdProducto"))
            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente




            ds = objReporte.VentasXTiendaExcel(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSublinea")), CInt(ViewState("IdProducto")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")))

            If ds IsNot Nothing Then
                Me.lblTitulo.Text = "VENTAS DE ARTICULOS POR TIENDA"
                Me.dgwVentasxTienda.DataSource = ds
                dgwVentasxTienda.DataBind()

                For Each row As GridViewRow In Me.dgwVentasxTienda.Rows
                    'total Sublinea
                    If Right(row.Cells(0).Text, 5) = "33333" Then
                        row.BackColor = Drawing.Color.SkyBlue
                        row.Cells(0).Text = ""
                    End If

                    'total linea
                    If Right(row.Cells(0).Text, 5) = "44111" Then
                        row.BackColor = Drawing.Color.SkyBlue
                        row.Cells(0).Text = ""
                    End If
                    'total general
                    If Right(row.Cells(0).Text, 5) = "44888" Then
                        row.BackColor = Drawing.Color.SkyBlue
                        row.Cells(0).Text = ""
                    End If
                    If Right(row.Cells(0).Text, 5) = "44999" Then
                        row.Cells(1).BackColor = Drawing.Color.SkyBlue
                        row.Cells(0).Text = ""
                    End If
                    'total linea vacia
                    If Right(row.Cells(0).Text, 5) = "44444" Then
                        row.BackColor = Drawing.Color.SkyBlue
                        row.Cells(0).Text = ""
                    End If

                Next


                Response.Clear()
                Response.Buffer = True
                Response.AddHeader("content-disposition", "attachment;filename=VentasProductosxTienda.xls")
                Response.Charset = ""
                Response.ContentType = "application/vnd.ms-excel"
            End If
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub

End Class