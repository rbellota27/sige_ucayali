﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmUtilidadxProducto.aspx.vb" Inherits="APPWEB.FrmUtilidadxProducto" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table style="width: 100%" class="style1">
        <tr>
            <td colspan="9" align="center " class="TituloCelda">
                Utilidad por Producto 
            </td>
        </tr>
        <tr>
            <td>
               <asp:UpdatePanel  ID ="udpDetalle" runat ="server" >
               <ContentTemplate >
                  <table style="width: 100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="Label">
                                                    Empresa:<asp:DropDownList ID="cmbEmpresa" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Label">
                                                    Tienda:<asp:DropDownList ID="cmbTienda" runat="server" Enabled="false">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Label">
                                                    Linea:
                                                    <asp:DropDownList ID="cboLineapri" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Label">
                                                    Sublinea:
                                                    <asp:DropDownList ID="cbosublineapri" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    
                </table>
               </ContentTemplate>

               </asp:UpdatePanel>
             
                
            </td>
        </tr>
        <tr>
            <td>

                <table>
                    <tr>
                        <td>
                        </td>
                        <td class="Label_fsp">
                            Fecha Inicio:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="txtFechaInicio">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="Label_fsp">
                            Fecha Fin:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                TargetControlID="txtFechaFin">
                            </cc1:CalendarExtender>
                        </td>

                        </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                            onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';"
                            OnClientClick="return(mostrarReporteDetallado());" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddidproducto" runat="server" />
                <asp:HiddenField ID="hddFechaActual" runat="server" />
                <br />
            </td>
        </tr>
    </table>


    <script language="javascript" type="text/javascript">

        function mostrarReporteDetallado() {

            var IdEmpresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
            if (document.getElementById('<%= cmbEmpresa.ClientID%>' == null)) {
                IdEmpresa = 0
            }

            var IdTienda = document.getElementById('<%= cmbTienda.ClientID%>').value;
            if (document.getElementById('<%= cmbTienda.ClientID%>') == null) {
                IdTienda = 0
            }

            var Idlinea = document.getElementById('<%= cbolineapri.ClientID%>').value;
            if (document.getElementById('<%= cbolineapri.ClientID%>') == null) {
                Idlinea = 0
            }
            var IdSublinea = document.getElementById('<%= cbosublineapri.ClientID%>').value;
            if (document.getElementById('<%= cbosublineapri.ClientID%>') == null) {
                IdSublinea = 0
            }

            var cont = 0;
            var fechaInicio;
            var fechaFin;
            if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
                fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
                fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
            } else {
                fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
                fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
            }
            frame1.location.href = 'VisorRG.aspx?iReporte=9&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&Idempresa=' + IdEmpresa + '&IdTienda=' + IdTienda + '&Idlinea=' + Idlinea + '&IdSublinea=' + IdSublinea;
            return false;
        }
    </script>

</asp:Content>
