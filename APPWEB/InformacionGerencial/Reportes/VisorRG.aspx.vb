﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports ADODB
Imports Microsoft.Office.Interop
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Data
Imports System.Reflection
Imports Excel = Microsoft.Office.Interop.Excel

Partial Public Class VisorRG
    Inherits System.Web.UI.Page
    Dim reporte As New ReportDocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim iReporte As String = Request.QueryString("iReporte")

            If iReporte Is Nothing Then
                iReporte = ""
            End If

            ViewState.Add("iReporte", iReporte)
            Select Case iReporte

                Case "1" 'cxcCliente
                    ViewState.Add("idPersonaCli", CInt(Session("idpersonaAll")))
                    ViewState.Add("idempresa", Request.QueryString("idempresa"))
                    ViewState.Add("idtienda", Request.QueryString("idtienda"))
                Case "2" 'cxcDocumento

                    ViewState.Add("idPersonaDoc", CInt(Session("idpersonaG")))
                    ViewState.Add("idempresa", Request.QueryString("idempresa"))
                    ViewState.Add("idtienda", Request.QueryString("idtienda"))

                Case "3"
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdAlmacenOrigen", Request.QueryString("IdAlmacenOrigen"))
                    ViewState.Add("idAlmacenDestino", Request.QueryString("idAlmacenDestino"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("mercaTransito", Request.QueryString("mercaTransito"))
                    ViewState.Add("IdTipoOperacion", Request.QueryString("IdTipoOperacion"))

                Case "4"
                    ViewState.Add("idpersona", Request.QueryString("idpersona"))
                    ViewState.Add("idempresa", Request.QueryString("idempresa"))
                    ViewState.Add("idtienda", Request.QueryString("idtienda"))
                    ViewState.Add("idopcion", Request.QueryString("idopcion"))
                    ViewState.Add("FechaIni", Request.QueryString("FechaIni"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                Case "5"
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdLinea", Request.QueryString("IdLinea"))
                    ViewState.Add("IdSublinea", Request.QueryString("IdSublinea"))
                    ViewState.Add("IdProducto", Request.QueryString("IdProducto"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                Case "6"
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                Case "7"
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("nprimeros", Request.QueryString("nprimeros"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("idlinea", Request.QueryString("idlinea"))
                    ViewState.Add("idsublinea", Request.QueryString("idsublinea"))
                    ViewState.Add("IdVendedor", Request.QueryString("IdVendedor"))

                Case "8"
                    ViewState.Add("idpersona", Request.QueryString("idpersona"))
                    ViewState.Add("idempresa", Request.QueryString("idempresa"))
                    ViewState.Add("idtienda", Request.QueryString("idtienda"))
                    ViewState.Add("idopcion", Request.QueryString("idopcion"))
                Case "9"
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("Idlinea", Request.QueryString("Idlinea"))
                    ViewState.Add("IdSublinea", Request.QueryString("IdSublinea"))

            End Select
        End If

        mostrarReporte()


    End Sub
    Private Sub mostrarReporte()
        Select Case ViewState.Item("iReporte").ToString
            Case "1"
                ReporteResumenCxCClientes()
            Case "2"
                ReporteResumenCxCDocumento()
            Case "3"
                ReporteDetalleGuiasRemision()
            Case "4"
                ReporteDetalleCxC()
            Case "5"
                'reporteVentasXTienda()
                reporteVentasXTiendaExcel()
                'reporteVentasXTiendaExcelOL()

            Case "6"
                reporteIngresoxTienda()
            Case "7"
                reporteRankingProductos()
            Case "8"
                ReporteDetalleCxCDocCliente()
            Case "9"
                ReportexUtilidad()
            Case Else
                ReporteResumenCxCClientes()
        End Select
    End Sub
    Private Sub ReportexUtilidad()
        Try
            Dim ds As New DataSet
            Dim objReporte As New Negocio.CXCResumenView

            ds = objReporte.getUtilidadxProducto(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")), CInt(ViewState("Idlinea")), CInt(ViewState("IdSublinea")))
            reporte = New CR_UtilidadxProducto
            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            Me.CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub

    Private Sub ReporteResumenCxCClientes()
        Try
            Dim vidpersona As Integer
            vidpersona = CInt(ViewState.Item("idPersonaCli"))
            vidpersona = CInt(ViewState.Item("idempresa"))
            vidpersona = CInt(ViewState.Item("idtienda"))


            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente

            Dim objDataTable As DataTable = objReporte.DetalleCxC(CInt(ViewState.Item("idPersonaCli")), 1, CInt(ViewState.Item("idempresa")), CInt(ViewState.Item("idtienda")), CStr(ViewState.Item("FechaIni")), CStr(ViewState.Item("FechaFin"))).Tables("DT_DetalleCxC")


            For Each fila As DataRow In objDataTable.Rows
                If CInt(IIf(IsDBNull(fila("DiasVencidos")), 0, fila("DiasVencidos"))) <= 0 Then
                    fila("DiasVencidos") = 0

                End If
            Next
            reporte = New CR_DetalleCxC
            reporte.SetDataSource(objDataTable)
            Me.CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteResumenCxCDocumento()
        Try
            Dim vidpersona As Integer
            vidpersona = CInt(ViewState.Item("idPersonaDoc"))

            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente

            Dim objDataTable As DataTable = objReporte.DetalleCxC(CInt(ViewState.Item("idPersonaDoc")), 1, CInt(ViewState.Item("idEmpresa")), CInt(ViewState.Item("idtienda")), CStr(ViewState.Item("FechaIni")), CStr(ViewState.Item("FechaFin"))).Tables("DT_DetalleCxC")


            For Each fila As DataRow In objDataTable.Rows
                If CInt(IIf(IsDBNull(fila("DiasVencidos")), 0, fila("DiasVencidos"))) <= 0 Then
                    fila("DiasVencidos") = 0

                End If
            Next
            reporte = New CR_DetalleCxC
            reporte.SetDataSource(objDataTable)
            Me.CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub ReporteDetalleGuiasRemision()
        Try
            Dim vidempresa, vIdAlmacenOrigen, vidAlmacenDestino, vmercaTransito, vIdTipoOperacion As Integer
            vidempresa = CInt(ViewState.Item("IdEmpresa"))
            vIdAlmacenOrigen = CInt(ViewState.Item("IdAlmacenOrigen"))
            vidAlmacenDestino = CInt(ViewState.Item("idAlmacenDestino"))
            vmercaTransito = CInt(ViewState.Item("mercaTransito"))
            vIdTipoOperacion = CInt(ViewState.Item("IdTipoOperacion"))
            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente
            ds = objReporte.DetalleGuiasRemision(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdAlmacenOrigen")), CInt(ViewState("idAlmacenDestino")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")), vmercaTransito, CInt(ViewState("IdTipoOperacion")))
            reporte = New CR_DetallesGuiaRemision
            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            Me.CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReporteDetalleCxCDocCliente()

        Try
            Dim vidpersona As Integer
            vidpersona = CInt(ViewState.Item("idpersona"))
            vidpersona = CInt(ViewState.Item("idempresa"))
            vidpersona = CInt(ViewState.Item("idtienda"))
            vidpersona = CInt(ViewState.Item("idopcion"))



            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente

            Dim objDataTable As DataTable = objReporte.DetalleCxCDocCliente(CInt(ViewState.Item("idpersona")), CInt(ViewState.Item("idopcion")), CInt(ViewState.Item("idempresa")), CInt(ViewState.Item("idtienda"))).Tables("Dt_cxcDocxCliente")

            reporte = New CR_DetalleCxCdocCliente
            reporte.SetDataSource(objDataTable)
            Me.CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub ReporteDetalleCxC()


        Try
            Dim vidpersona As Integer
            vidpersona = CInt(ViewState.Item("idpersona"))
            vidpersona = CInt(ViewState.Item("idempresa"))
            vidpersona = CInt(ViewState.Item("idtienda"))
            vidpersona = CInt(ViewState.Item("idopcion"))
            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente

            Dim objDataTable As DataTable = objReporte.DetalleCxC(CInt(ViewState.Item("idpersona")), CInt(ViewState.Item("idopcion")), CInt(ViewState.Item("idempresa")), CInt(ViewState.Item("idtienda")), CStr(ViewState.Item("FechaIni")), CStr(ViewState.Item("FechaFin"))).Tables("DT_DetalleCxC")


            For Each fila As DataRow In objDataTable.Rows
                If CInt(IIf(IsDBNull(fila("DiasVencidos")), 0, fila("DiasVencidos"))) <= 0 Then
                    fila("DiasVencidos") = 0

                End If
            Next

            ''For i As Integer = 0 To objDataTable.Rows.Count - 1

            ''    '*********** si nose  vence

            ''    If CInt(objDataTable.Rows(i).Item("DiasVencidos").ToString) <= 0 Then
            ''        With objDataTable.Rows(i)
            ''            .Item("DiasVencidos") = 0
            ''        End With
            ''    End If
            ''Next
            'ds = objReporte.DetalleCxC(CInt(ViewState.Item("idpersona")))
            reporte = New CR_DetalleCxC
            reporte.SetDataSource(objDataTable)
            Me.CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub reporteIngresoxTienda()
        'Try
        '    Dim vidempresa As Integer
        '    vidempresa = CInt(ViewState.Item("IdEmpresa"))

        '    Dim ds As New DataSet
        '    Dim objReporte As New Negocio.Cliente
        '    ds = objReporte.ReporteIngresosxTienda(CInt(ViewState("IdEmpresa")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")))
        '    Dim reporte As New CR_IngresoxTienda
        '    reporte.SetDataSource(ds)
        '    reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
        '    reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
        '    Me.CrystalReportViewer1.ReportSource = reporte
        '    CrystalReportViewer1.DataBind()
        'Catch ex As Exception
        '    Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        'End Try
    End Sub
    Private Sub reporteVentasXTienda()
        Try
            Dim vidempresa, vIdLinea, vIdSublinea, vidproducto As Integer
            vidempresa = CInt(ViewState.Item("IdEmpresa"))
            vIdLinea = CInt(ViewState.Item("IdLinea"))
            vIdSublinea = CInt(ViewState.Item("IdSublinea"))
            vidproducto = CInt(ViewState.Item("IdProducto"))

            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente
            ds = objReporte.VentasXTienda(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSublinea")), CInt(ViewState("IdProducto")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")))
            reporte = New ReportDocument
            reporte = Me.tipoRptxTienda(vidempresa)
            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            Me.CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub reporteVentasXTiendaExcel()
        Try
            Dim vidempresa, vIdLinea, vIdSublinea, vidproducto As Integer
            vidempresa = CInt(ViewState.Item("IdEmpresa"))
            vIdLinea = CInt(ViewState.Item("IdLinea"))
            vIdSublinea = CInt(ViewState.Item("IdSublinea"))
            vidproducto = CInt(ViewState.Item("IdProducto"))

            Dim objReporte As New Negocio.Cliente
            Dim ds As New DataSet
            Dim objutil As New Util
            Dim recorset As New ADODB.Recordset            
            'Dim ruta As String = "../../PlantillasXLT/VtasProductoTiendaRpt"
            'Dim ruta As String = "\PlantillasXLT\VtasProductoTiendaRpt"
            Dim ruta As String = ""
            ruta = Server.MapPath(Request.ApplicationPath) & "PlantillasXLT\VtasProductoTiendaRpt"
            'ruta = Server.MapPath(Request.PhysicalPath) & "PlantillasXLT\VtasProductoTiendaRpt"
            'ruta = Server.MapPath(Request.PhysicalApplicationPath) & "PlantillasXLT\VtasProductoTiendaRpt"
            ruta = Server.UrlDecode(ruta)
            Dim titulo As String = "Ventas por Tienda"

            ds = objReporte.VentasXTiendaExcel(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSublinea")), CInt(ViewState("IdProducto")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")))

            recorset = objutil.ConvertToRecordset(ds.Tables(0))

            objutil.getOExcel(recorset, titulo, ruta)

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub reporteVentasXTiendaExcelOL()
        Try
            Dim vidempresa, vIdLinea, vIdSublinea, vidproducto As Integer
            vidempresa = CInt(ViewState.Item("IdEmpresa"))
            vIdLinea = CInt(ViewState.Item("IdLinea"))
            vIdSublinea = CInt(ViewState.Item("IdSublinea"))
            vidproducto = CInt(ViewState.Item("IdProducto"))

            Dim objReporte As New Negocio.Cliente
            Dim ds As New DataSet
            ds = objReporte.VentasXTiendaExcel(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSublinea")), CInt(ViewState("IdProducto")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")))




            Dim xlExcel As New Excel.Application()
            Dim xlworkBooks As Excel.Workbooks = xlExcel.Workbooks
            Dim xlWorkBook As Excel.Workbook = xlworkBooks.Add
            Dim xlWorkSheet As Excel.Worksheet = CType(xlExcel.ActiveSheet, Excel.Worksheet)

            xlExcel = CType(CreateObject("excel.application"), Excel.ApplicationClass)
            xlExcel.Visible = True

            xlWorkBook = New Excel.Application().Workbooks.Add(Missing.Value)
            xlWorkBook.Application.Visible = True
            xlWorkSheet = CType(xlWorkBook.ActiveSheet, Excel.Worksheet)

            '   Gets the dataset containing the data
            'Dim dsData As DataSet = getData()


            Dim i As Integer = 2

            '   Outputting the fieldnames in pink bold color
            xlWorkSheet.Cells(1, 1) = ds.Tables(0).Columns(0).ColumnName
            xlWorkSheet.Cells(1, 2) = ds.Tables(0).Columns(1).ColumnName
            xlWorkSheet.Cells(1, 3) = ds.Tables(0).Columns(2).ColumnName
            xlWorkSheet.Cells(1, 4) = ds.Tables(0).Columns(3).ColumnName
            xlWorkSheet.Cells(1, 5) = ds.Tables(0).Columns(4).ColumnName

            xlWorkSheet.Range("$A1:$E1").Font.ColorIndex = Excel.Constants.xlColor2
            xlWorkSheet.Range("$A1:$E1").Font.Bold = True

            '   Outputting the data
            For Each dr As DataRow In ds.Tables(0).Rows
                xlWorkSheet.Cells(i, 1) = dr(0)
                xlWorkSheet.Cells(i, 2) = dr(1)
                xlWorkSheet.Cells(i, 3) = dr(2)
                xlWorkSheet.Cells(i, 4) = dr(3)
                xlWorkSheet.Cells(i, 5) = dr(4)
                xlWorkSheet.Cells(i, 6) = dr(5)

                '   Building the formula for calculating the sum
                'xlWorkSheet.Cells(i, 5).Formula = "=SUM($C{0}:$D{0})".Replace("{0}", i.ToString())

                '   Going to the next row
                i = i + 1
            Next

            '   Auto fit the columns
            xlWorkSheet.Columns.AutoFit()

            'xlWorkBook. 

            'xlWorkBook.Close()
            'oExcelApp.Workbooks.Close()
            'oExcelApp.Quit()
            'System.Runtime.InteropServices.Marshal.ReleaseComObject(oExcelApp)
            'xlWorkBook.Close(False)

            'xlWorkBook = Nothing
            'xlWorkSheet = Nothing
            xlWorkBook.SaveAs("C:\Sige\VentasProductos.xlsx")

            NAR(xlWorkSheet)
            xlWorkBook.Saved = False
            xlWorkBook.Close(False)
            NAR(xlWorkBook)
            NAR(xlworkBooks)
            xlExcel.Quit()
            NAR(xlExcel)


            'oExcelApp = null
            GC.Collect()
            GC.WaitForPendingFinalizers()


            '   Generating the graph
            'Dim chart As Excel.Chart
            'chart = xlWorkBook.Charts.Add()

            'With chart
            '    .ChartType = Excel.XlChartType.xlColumnClustered
            '    .SetSourceData(xlWorkSheet.Range("A1:E11"), 2)

            '    .HasTitle = True
            '    .ChartTitle.Characters.Text = "Students' marks"

            '    .Axes(1, Excel.XlAxisGroup.xlPrimary).HasTitle = True
            '    .Axes(1, Excel.XlAxisGroup.xlPrimary).AxisTitle.Characters.Text = "Students"
            '    .Axes(2, Excel.XlAxisGroup.xlPrimary).HasTitle = True
            '    .Axes(2, Excel.XlAxisGroup.xlPrimary).AxisTitle.Characters.Text = "Marks"
            'End With

            'Limpiar todos los objetos

        Catch ex As Exception
            'Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
            Throw ex
        End Try

    End Sub

    Private Sub NAR(ByVal o As Object)
        Try
            'System.Runtime.InteropServices.Marshal.ReleaseComObject(o)
            Do Until _
          System.Runtime.InteropServices.Marshal.ReleaseComObject(o) <= 0
            Loop
        Catch
        Finally
            o = Nothing
        End Try
    End Sub

    Private Sub reporteVentasxTiendaExportExcel()
        Try
            Dim vidempresa, vIdLinea, vIdSublinea, vidproducto As Integer
            vidempresa = CInt(ViewState.Item("IdEmpresa"))
            vIdLinea = CInt(ViewState.Item("IdLinea"))
            vIdSublinea = CInt(ViewState.Item("IdSublinea"))
            vidproducto = CInt(ViewState.Item("IdProducto"))
            Dim ds As New DataSet
            Dim objReporte As New Negocio.Cliente
            ds = objReporte.VentasXTiendaExcel(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSublinea")), CInt(ViewState("IdProducto")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")))


            'Response.Redirect("")

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub

    Private Function tipoRptxTienda(ByVal idempresa As Integer) As ReportDocument
        If (idempresa <> 0) Then
            reporte = New CR_VentasxTiendaM
        Else
            reporte = New CR_VentasxTiendaNN
        End If
        Return reporte
    End Function
    Private Sub reporteRankingProductos()
        Try
            Dim vidempresa, vnprimeros, vidtienda As Integer
            vidempresa = CInt(ViewState.Item("IdEmpresa"))
            vidtienda = CInt(ViewState.Item("IdTienda"))
            vnprimeros = CInt(ViewState.Item("nprimeros"))

            Dim ds As New DataSet
            Dim objReporte As New Negocio.CXCResumenView
            ds = objReporte.getRankingProductos(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CInt(ViewState("nprimeros")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")), CInt(ViewState("idlinea")), CInt(ViewState("idsublinea")), CInt(ViewState("IdVendedor")))
            'Dim reporte As New ReportDocument
            'reporte = Me.tipoReporteRankingProductos(vidempresa, vidtienda)
            reporte = New CR_RankingProductos
            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))

            Me.CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Function tipoReporteRankingProductos(ByVal idempresa As Integer, ByVal idtienda As Integer) As ReportDocument
        If (idempresa <> 0) Then
            If (idtienda <> 0) Then
                reporte = New CR_RankingProductos
            Else
                reporte = New CR_RankingProductosE
            End If
        Else
            If (idtienda <> 0) Then
                reporte = New CR_RankingProductosT
            Else
                reporte = New CR_RankingProductosN
            End If

        End If
        Return reporte

    End Function

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reporte.Close()
        reporte.Dispose()
    End Sub
End Class