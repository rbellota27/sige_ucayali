﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmRptDetalleGuiasRemision1
    Inherits System.Web.UI.Page

    Private objscript As New ScriptManagerClass
    Private objCombo As New Combo
    Private Sub FrmRptRankingClientes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrmrpt()
        End If
    End Sub
    Private Sub inicializarFrmrpt()
        Try
            objCombo.LlenarCboPropietario(Me.cmbEmpresa, False)
            objCombo.llenarCboAlmacen(Me.cmbIdAlmacenOrigen, False)
            objCombo.llenarCboAlmacen(Me.cmbidAlmacenDestino, True)
            objCombo.llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True)
            Dim objFechaActual As New Negocio.FechaActual
            txtFechaInicio.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            txtFechaFin.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            Me.hddFechaActual.Value = CStr(Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy"))

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
  
    Private Sub cmbEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbEmpresa.SelectedIndexChanged
        objCombo.llenarCboAlmacenxIdEmpresa(Me.cmbIdAlmacenOrigen, CInt(Me.cmbEmpresa.SelectedValue), False)
     
    End Sub
    Private Sub cboTipoOperacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoOperacion.SelectedIndexChanged
        
    End Sub

    Protected Sub hddIdTipoDocumento_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles hddIdTipoDocumento.ValueChanged

    End Sub

    Protected Sub hddRbtTipoReporte_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles hddRbtTipoReporte.ValueChanged

    End Sub

    Protected Sub cmbIdAlmacenOrigen_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbIdAlmacenOrigen.SelectedIndexChanged

    End Sub

    Protected Sub chbMercaTransito_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chbMercaTransito.CheckedChanged

    End Sub
End Class