<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmDetalleCxC.aspx.vb" Inherits="APPWEB.FrmDetalleCxC" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="MsgBox" Namespace="MsgBox" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td class="TituloCelda">
                Resumen Cuentas Por Cobrar Por Cliente
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td class="Texto">
                                        Empresa:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboIdEmpresa" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Texto">
                                        Tienda:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboIdTienda" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="Texto">
                                        Fecha Incio:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFechaIni" runat="server" CssClass="TextBox_Fecha" Width="80px"
                                            onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender_FechaIni" runat="server" TargetControlID="txtFechaIni"
                                            Format="dd/MM/yyyy">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td class="Texto">
                                        Fecha Fin:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" Width="80px"
                                            onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender_FechaFin" runat="server" TargetControlID="txtFechaFin"
                                            Format="dd/MM/yyyy">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbkConsiderarFecha" Text="Considerar Rango" CssClass="Texto" runat="server"
                                            Checked="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="TituloCeldaLeft">
                                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="PnlCliente"
                                            CollapsedSize="0" ExpandedSize="80" Collapsed="true" ExpandControlID="ImgCliente"
                                            CollapseControlID="ImgCliente" TextLabelID="lblCliente" ImageControlID="ImgCliente"
                                            CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                            CollapsedText="Buscar Cliente" ExpandedText="Buscar Cliente" ExpandDirection="Vertical"
                                            SuppressPostBack="True" />
                                        <asp:Image ID="ImgCliente" runat="server" Width="16px" />
                                        <asp:Label ID="lblCliente" runat="server" Text="Buscar Cliente" CssClass="LabelBlanco"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="PnlCliente" runat="server">
                                            <table cellspacing="0" style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Limpiar_b.JPG"
                                                            onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';" onmouseout="this.src='../../Imagenes/Limpiar_b.JPG';"
                                                            OnClientClick="return(LimpiarControlesCliente());" />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnBuscarCliente" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                            onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                                            OnClientClick="return(mostrarCapaPersona());" />
                                                        <asp:Label ID="Label12024" runat="server" CssClass="Label" Text="Tipo de Persona"></asp:Label>
                                                        <asp:DropDownList ID="cboTipoPersona" runat="server" Width="116px" Font-Bold="True"
                                                            Enabled="False">
                                                            <asp:ListItem>---------</asp:ListItem>
                                                            <asp:ListItem>Natural</asp:ListItem>
                                                            <asp:ListItem>Juridica</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 139px; text-align: right;">
                                                        <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Nombre o Raz�n Social"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtRazonSocial" CssClass="TextBoxReadOnly" runat="server" Width="521px"
                                                            MaxLength="50" ReadOnly="True"></asp:TextBox>
                                                        <asp:TextBox ID="txtCodigoCliente" CssClass="TextBoxReadOnly" runat="server" Width="99px"
                                                            ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right; width: 139px">
                                                        <asp:Label ID="Label3" runat="server" Text="Dni" CssClass="Label"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDni" runat="server" Width="70px" CssClass="TextBoxReadOnly" Style="margin-left: 0px;
                                                            margin-bottom: 0px" MaxLength="8" ReadOnly="True"></asp:TextBox>
                                                        <asp:Label ID="Label4" runat="server" Text="Ruc" CssClass="Label"></asp:Label>
                                                        <asp:TextBox ID="txtRuc" runat="server" Width="90px" MaxLength="11" CssClass="TextBoxReadOnly"
                                                            ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="LabelLeft">
                                        <asp:RadioButtonList ID="rbtlOpcion" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1" Text="Pendientes" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Todos"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="Cancelados"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                            onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';"
                                            OnClientClick="return(MostrarReporte());" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                            </iframe>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hddFechaActual" runat="server" />
                            <asp:HiddenField ID="hddRbtTipoReporte" runat="server" />
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                        onmouseout="this.src='../../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../../Imagenes/Cerrar_A.JPG';"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr class="Label">
                <td align="left">
                    <table>
                        <tr>
                            <td>
                                Filtro:
                            </td>
                            <td>
                                <asp:DropDownList ID="cmbFiltro_BuscarPersona" runat="server">
                                    <asp:ListItem Selected="True" Value="0">Descripci�n</asp:ListItem>
                                    <asp:ListItem Value="1">R.U.C.</asp:ListItem>
                                    <asp:ListItem Value="2">D.N.I.</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                Texto:
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarPersona_Grilla">                                
                                <asp:TextBox ID="txtBuscarPersona_Grilla" runat="server" Width="250px"></asp:TextBox>
                                <asp:ImageButton ID="btnBuscarPersona_Grilla" runat="server" CausesValidation="false"
                                    ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                    onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />
                                    </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:GridView ID="DGV_BuscarPersona" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                        PageSize="20" Width="100%">
                        <Columns>
                            <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                            <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                            <asp:BoundField DataField="getNombreParaMostrar" HeaderText="Descripci�n" NullDisplayText="---" />
                            <asp:BoundField DataField="Dni" HeaderText="D.N.I." NullDisplayText="---" />
                            <asp:BoundField DataField="Ruc" HeaderText="R.U.C." NullDisplayText="---" />
                            <asp:BoundField DataField="Direccion" HeaderText="Direcci�n" NullDisplayText="---" />
                            <asp:BoundField DataField="PorcentPercepcion" HeaderText="Percep. (%)" NullDisplayText="0"
                                DataFormatString="{0:F2}" />
                            <asp:BoundField DataField="PorcentRetencion" HeaderText="Retenc. (%)" NullDisplayText="0"
                                DataFormatString="{0:F2}" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnAnterior_Persona" runat="server" Font-Bold="true" Width="50px"
                        Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionPersona('0'));" />
                    <asp:Button ID="btnPosterior_Persona" runat="server" Font-Bold="true" Width="50px"
                        Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionPersona('1'));" />
                    <asp:TextBox ID="txtPageIndex_Persona" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                        runat="server"></asp:TextBox>
                    <asp:Button ID="btnIr_Persona" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                        ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionPersona('2'));" />
                    <asp:TextBox ID="txtPageIndexGO_Persona" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript" language="javascript">


        function MostrarReporte() {
            var idpersona = document.getElementById('<%= txtCodigoCliente.ClientID%>').value;
            if (idpersona == "") {
                idpersona = 0;
            } else {
                idpersona = idpersona
            }

            var idempresa = document.getElementById('<%= cboIdEmpresa.ClientID%>').value;
            if (idempresa == "") {
                idempresa = 0;
            } else {
                idempresa = idempresa
            }

            var idtienda = document.getElementById('<%= cboIdTienda.ClientID%>').value;
            if (idtienda == "") {
                idtienda = 0;
            } else {
                idtienda = idtienda
            }


            var idopcion = document.getElementById('<%= rbtlOpcion.ClientID%>');
            var idopc = 1;
            var ctrl_IdOpcion = idopcion.getElementsByTagName('input')

            for (var i = 0; i < ctrl_IdOpcion.length; i++) {
                if (ctrl_IdOpcion[i].checked == true) {
                    idopc = parseInt(ctrl_IdOpcion[i].value);
                }
            }

            var cbkConsiderarFecha = document.getElementById('<%=cbkConsiderarFecha.ClientID %>');
            var FechaIni = '';
            var FechaFin = '';
            if (cbkConsiderarFecha.checked == true) {
                FechaIni = document.getElementById('<%=txtFechaIni.ClientID %>').value;
                FechaFin = document.getElementById('<%=txtFechaFin.ClientID %>').value;
            }


            frame1.location.href = 'visorRG.aspx?iReporte=4&idpersona=' + idpersona + '&idopcion=' + idopc + '&idempresa=' + idempresa + '&idtienda=' + idtienda + '&FechaIni=' + FechaIni + '&FechaFin=' + FechaFin;
            return false;

        }

        //para levantar las capas
        function onCapaCliente(s) {
            LimpiarControlesCliente();
            onCapa(s);
            document.getElementById('<%= txtRazonSocial.ClientID%>').focus();
            return false;
        }
        void function LimpiarControlesCliente() {
            document.getElementById('<%= txtRazonSocial.ClientID%>').value = "";
            document.getElementById('<%= txtDNI.ClientID%>').value = "";
            document.getElementById('<%= txtRUC.ClientID%>').value = "";
            document.getElementById('<%= txtCodigoCliente.ClientID %>').value = "";
            return false;
        }



        function validarNumeroPunto(event) {
            var key = event.keyCode;
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }



        //Buscar SubLinea
        function ValidarEnteroSinEnter() {
            if (onKeyPressEsNumero('event') == false) {
                alert('Caracter no v�lido. Solo se permiten n�meros Enteros');
                return false;
            }
            return true;
        }


        //End Buscar SubLinea
        //Buscar Cliente

        function BuscarCliente() {

            return true;
        }


        function ValidarEnter() {
            return (!esEnter(event));
        }
        function ValidaEnter() {
            if (esEnter(event) == true) {
                alert('Tecla No permitida');
                caja.focus();
                return false;
            }
            return true;
        }
        //End Buscar Cliente


        function mostrarCapaPersona() {
            //******************* validamos que exista detalle

            onCapa('capaPersona');
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
            //} else {
            //    alert('Debe ingresar primero el detalle del documento.');
            //}
            return false;
        }
        function addPersona_Venta() {
            offCapa('capaPersona');

            return false;
        }


        function aceptarFoco(caja) {
            caja.select();
            caja.focus();
            return true;
        }




        function valNavegacionPersona(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Persona.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


    
    
                         
    </script>

</asp:Content>
