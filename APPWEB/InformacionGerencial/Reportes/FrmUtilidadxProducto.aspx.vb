﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmUtilidadxProducto
    Inherits System.Web.UI.Page

    Private objscript As New ScriptManagerClass
    Private objCombo As New Combo
    Private objNegUsu As New Negocio.UsuarioView
    Private listaDetalle As List(Of Entidades.DetalleDocumentoView)
    Private listaDetalleRelacionado As List(Of Entidades.DetalleDocumento)
    Private listaDetalleDocRel_AddProd As List(Of Entidades.DetalleDocumento)
    Private listaProductoView As List(Of Entidades.ProductoView)
    Private listaDetalleProductov As List(Of Entidades.ProductoView)
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try
            btnMostrarReporte.Visible = True
            objCombo.LlenarCboPropietario(Me.cmbEmpresa, True)

            objCombo.LLenarCboTienda(Me.cmbTienda, True)

            'este cod es para llenar por permiso
            'If objNegUsu.FunVerTodasTiendas(CInt(Session("IdUsuario")), 8) > 0 Then
            '    objCombo.LlenarCboTiendaxIdUsuario(Me.cmbTienda, CInt(Session("IdUsuario")), True)
            'Else
            '    objCombo.LlenarCboTiendaxIdUsuario(Me.cmbTienda, CInt(Session("IdUsuario")), False)
            'End If

            objCombo.LlenarCboLinea(Me.cboLineapri, True)
            objCombo.LLenarCboSubLinea(Me.cbosublineapri, True)


            Dim objFechaActual As New Negocio.FechaActual
            txtFechaInicio.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            txtFechaFin.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            Me.hddFechaActual.Value = CStr(Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy"))
            Me.cmbTienda.Enabled = True

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub cboLineapri_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLineapri.SelectedIndexChanged
        objCombo.LlenarCboSubLineaxIdLinea(Me.cbosublineapri, CInt(Me.cboLineapri.SelectedValue), True)

    End Sub


End Class