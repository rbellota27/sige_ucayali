﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmRptRankingProductos.aspx.vb" Inherits="APPWEB.FrmRptRankingProductos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td class="TituloCelda">
                Ranking de Productos
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="center">
                                                <table>
                                                    <tr>
                                                        <td class="Texto">
                                                            Empresa:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cmbEmpresa" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Texto">
                                                            Tienda:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cmbTienda" runat="server" Enabled="true" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="Texto">
                                                            Tipo Existencia:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Texto">
                                                            Linea:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Texto">
                                                            SubLinea:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboSublinea" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="Texto">
                                                            NPrimeros:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox onKeypress="return(onKeyPressEsNumero('event'));" ID="txtNPrimeros" runat="server"
                                                                Width="80"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Fechas" runat="server">
                                <table>
                                    <tr>
                                        <td class="Label">
                                            Fecha Inicio:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                                TargetControlID="txtFechaInicio">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td class="Label">
                                            Fecha Fin:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                                TargetControlID="txtFechaFin">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel_Cliente" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td class="TituloCeldaLeft">
                                                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="PnlCliente"
                                                    CollapsedSize="0" ExpandedSize="80" Collapsed="true" ExpandControlID="ImgCliente"
                                                    CollapseControlID="ImgCliente" TextLabelID="lblCliente" ImageControlID="ImgCliente"
                                                    CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                    CollapsedText="Buscar Vendedor" ExpandedText="Buscar Vendedor" ExpandDirection="Vertical"
                                                    SuppressPostBack="True" />
                                                <asp:Image ID="ImgCliente" runat="server" Width="16px" />
                                                <asp:Label ID="lblCliente" runat="server" Text="Buscar Vendedor" CssClass="LabelBlanco"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Panel ID="PnlCliente" runat="server">
                                                    <table cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="btnBuscarCliente" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                    onmouseover="this.src='/Imagenes/Buscar_A.JPG';" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                                                    OnClientClick="return(mostrarCapaPersona());" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label12024" runat="server" CssClass="Label" Text="Tipo de Persona"></asp:Label>
                                                                <asp:DropDownList ID="cboTipoPersona" runat="server" Width="116px" Font-Bold="True"
                                                                    Enabled="False">
                                                                    <asp:ListItem>---------</asp:ListItem>
                                                                    <asp:ListItem>Natural</asp:ListItem>
                                                                    <asp:ListItem>Juridica</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Limpiar_b.JPG"
                                                                    onmouseover="this.src='/Imagenes/Limpiar_A.JPG';" onmouseout="this.src='/Imagenes/Limpiar_b.JPG';"
                                                                    OnClientClick="return(LimpiarControlesCliente());" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 139px; text-align: right;">
                                                                <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Nombre"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRazonSocial" CssClass="TextBoxReadOnly" runat="server" Width="521px"
                                                                    MaxLength="50" ReadOnly="True"></asp:TextBox>
                                                                <asp:TextBox ID="txtCodigoCliente" CssClass="TextBoxReadOnly" runat="server" Width="99px"
                                                                    ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right; width: 139px">
                                                                <asp:Label ID="Label3" runat="server" Text="Dni" CssClass="Label"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtDni" runat="server" Width="70px" CssClass="TextBoxReadOnly" Style="margin-left: 0px;
                                                                    margin-bottom: 0px" MaxLength="8" ReadOnly="True"></asp:TextBox>
                                                                <asp:Label ID="Label4" runat="server" Text="Ruc" CssClass="Label"></asp:Label>
                                                                <asp:TextBox ID="txtRuc" runat="server" Width="90px" MaxLength="11" CssClass="TextBoxReadOnly"
                                                                    ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="gvBuscar" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                        onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" onmouseover="this.src='/Imagenes/Aceptar_A.JPG';"
                                        OnClientClick="return(MostrarReporte());" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddFechaActual" runat="server" />
                <asp:HiddenField ID="hddRbtTipoReporte" runat="server" />
                <br />
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this));" runat="server"
                                                                            Width="450px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                                                            MaxLength="11"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddl_Rol" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="Página Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="Página Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <%--</ContentTemplate>
</asp:UpdatePanel>--%>

    <script language="javascript" type="text/javascript">

        function MostrarReporte() {
            var idempresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
            var idtienda = document.getElementById('<%=cmbtienda.ClientID%>').value;
            var idlinea = document.getElementById('<%=cboLinea.ClientID%>').value;
            var idsublinea = document.getElementById('<%=cboSublinea.ClientID%>').value;
            var nprimeros = document.getElementById('<%=txtNPrimeros.ClientID%>').value;
            if (nprimeros == "") {
                nprimeros = 0
            }
            else {
                nprimeros = nprimeros
            }

            if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
                fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
                fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
            } else {
                fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
                fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
            }

            var IdVendedor = parseInt(document.getElementById('<%= txtCodigoCliente.ClientID%>').value);
            if (isNaN(IdVendedor)) { IdVendedor = 0; }
            
            frame1.location.href = 'visorRG.aspx?iReporte=7&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + idempresa + '&IdTienda=' + idtienda + '&IdVendedor=' + IdVendedor + '&nprimeros=' + nprimeros + '&idlinea=' + idlinea + '&idsublinea=' + idsublinea;
            return false;

        }

        function mostrarCapaPersona() {
            onCapa('capaPersona');
            return false;
        }

        function addPersona_Venta() {
            offCapa('capaPersona');

            return false;
        }

        function onCapaCliente(s) {
            LimpiarControlesCliente();
            onCapa(s);
            document.getElementById('<%= txtRazonSocial.ClientID%>').focus();
            return false;
        }
        void function LimpiarControlesCliente() {
            document.getElementById('<%= txtRazonSocial.ClientID%>').value = "";
            document.getElementById('<%= txtDNI.ClientID%>').value = "";
            document.getElementById('<%= txtRUC.ClientID%>').value = "";
            document.getElementById('<%= txtCodigoCliente.ClientID %>').value = "";
            return false;
        }

        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj) {
            var key = event.keyCode;
            if (key == 13) {
                
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
    
    </script>

</asp:Content>
