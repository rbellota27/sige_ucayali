﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"     MasterPageFile="~/Principal.Master"    CodeBehind="FrmRptIngresosxTienda.aspx.vb" Inherits="APPWEB.FrmRptIngresosxTienda" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--<ContentTemplate >
--%>
<table style="width: 100%">
         <tr > <td  align ="center " class ="TituloCelda"> Ingresos Por Tiendas </td></tr>
            <td>
                <table>
                    <tr>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                            <table> 
                               
                                <tr>
                                    <td >
                                            <table > 
                                            <tr >
                                            <td class="Label">
                                                Empresa:<asp:DropDownList ID="cmbEmpresa" runat="server" >
                                                </asp:DropDownList>
                                            </td>

                                            </td> 
                                            </tr>
                            </table> 
                            </td>
                            </tr>
                            <%--taable--%>
                                                          
                            </table>    
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    
                    </td>
                    </tr>
                    <tr>
                       <td >
                           <asp:Panel ID="Panel_Fechas" runat="server">
                           
                           <table>
                                    <tr>
                                     <td>
                                     <table >
                                            <tr >
                                                    <td class="Label">
                                                    Fecha Inicio:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                        <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" 
                                                         Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                   <td class="Label">
                                                    Fecha Fin:</td>
                                                    <td>
                                                    <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" 
                                                      Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                                                    </cc1:CalendarExtender>
                                                    </td>
                                     </tr>
                                     
                                     </table>
                                     </td>

                                      
                                    </tr>
                                   

                           </table>
                           
                           </asp:Panel>
                       </td>
                    </tr>

                    <tr>
                        <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                        <asp:ImageButton ID="btnMostrarReporte" runat="server" 
                        ImageUrl="~/Imagenes/Aceptar_B.JPG" onmouseout="this.src='/Imagenes/Aceptar_B.JPG';"                                                
                        onmouseover="this.src='/Imagenes/Aceptar_A.JPG';"
                        OnClientClick="return(MostrarReporte());"
                         />
                               
                             </ContentTemplate>
                        </asp:UpdatePanel>
                    
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td>
            <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px"></iframe>
            </td>
        </tr>
        <tr>
            <td>
                                               
                <asp:HiddenField ID="hddFechaActual" runat="server" />
                <asp:HiddenField ID="hddRbtTipoReporte" runat="server" />
                <br />
            </td>
        </tr>
</table>
<%--</ContentTemplate>
</asp:UpdatePanel>--%>
<script language="javascript" type="text/javascript">

      function MostrarReporte() {
          var idempresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
                    
          if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
              fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
              fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
          } else {
              fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
              fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
          }
          frame1.location.href = 'visorRG.aspx?iReporte=6&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + idempresa; 
          return false;
        
      }
   
    
  </script>


</asp:Content>
