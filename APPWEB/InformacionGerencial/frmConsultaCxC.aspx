﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"       MasterPageFile="~/Principal.Master"        CodeBehind="frmConsultaCxC.aspx.vb" Inherits="APPWEB.frmConsultaDeudaCliente" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="MsgBox" namespace="MsgBox" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
            <table style="width:94%;">
                <tr>
                    <td align="center" class="TituloCelda">
                        Consulta de Cuentas por Cobrar</td>
                </tr>
                <tr>
                    <td>
                        <asp:ImageButton ID="cmd_Buscar" runat="server"
                            ImageUrl="~/Imagenes/Buscar_b.JPG"
                            onmouseout="this.src='/Imagenes/Buscar_B.JPG';"
                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';"
                    ToolTip="Buscar" />
                        <asp:ImageButton ID="cmd_Imprimir" runat="server" cToolTip="Imprimir"
                            ImageUrl="~/Imagenes/Imprimir_B.JPG"
                            onmouseout="this.src='/Imagenes/Imprimir_B.JPG';"
                            onmouseover="this.src='/Imagenes/Imprimir_A.JPG';" />
                        <asp:ImageButton ID="cmd_Cancelar" runat="server" 
                            ImageUrl="~/Imagenes/Cancelar_B.JPG"
                            onmouseout="this.src='/Imagenes/Cancelar_B.JPG';"
                            onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" Visible="False" />
                        <asp:ImageButton ID="cmd_VerTodos" runat="server" 
                            ImageUrl="~/Imagenes/VerTodos_B.jpg"
                            onmouseout="this.src='/Imagenes/VerTodos_B.JPG';"
                            onmouseover="this.src='/Imagenes/VerTodos_A.JPG';"
                            ToolTip="Muestra la lista de Clientes" 
                            Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnl_Busqueda" runat="server" Visible="False" Width="997px">
                                        <table>
                                            <tr>
                                                <td style="width: 246px">
                                                    <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Buscar por:"></asp:Label>
                                                    <asp:DropDownList ID="ddl_BuscaPor" runat="server" AutoPostBack="True" 
                                                        CssClass="Label" Width="155px">
                                                        <asp:ListItem Value="0">Seleccione de la Lista</asp:ListItem>
                                                        <asp:ListItem Value="1">Todos los Clientes</asp:ListItem>
                                                        <asp:ListItem Value="2">Nombre/Razon Social</asp:ListItem>
                                                        <asp:ListItem Value="3">RUC/DNI</asp:ListItem>
                                                        <asp:ListItem Value="4">Codigo</asp:ListItem>
                                                        <asp:ListItem Value="5">Estado</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 147px">
                                                    <asp:RadioButtonList ID="rbl_BuscaNombre" runat="server" CssClass="GroupBox" 
                                                        RepeatDirection="Horizontal" TextAlign="Left" Visible="False">
                                                        <asp:ListItem Selected="True" Value="0">R.Social</asp:ListItem>
                                                        <asp:ListItem Value="1">Nombre</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td style="width: 120px">
                                                    <asp:RadioButtonList ID="rbl_BuscaDNI" runat="server" CssClass="GroupBox" 
                                                        RepeatDirection="Horizontal" TextAlign="Left" Visible="False">
                                                        <asp:ListItem Selected="True" Value="0">RUC</asp:ListItem>
                                                        <asp:ListItem Value="1">DNI</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:TextBox ID="txt_Buscar" runat="server" BorderColor="#4B7CFE" 
                                                        BorderStyle="Solid" BorderWidth="1px" Visible="False" Width="118px"></asp:TextBox>
                                                </td>
                                                <td style="width: 73px">
                                                    <asp:DropDownList ID="ddl_Estado" runat="server" AutoPostBack="True" 
                                                        CssClass="Label" Visible="False">
                                                        <asp:ListItem Text="Activo" Value="1" />
                                                        <asp:ListItem Text="Inactivo" Value="0" />
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="cmd_BuscaCliente" runat="server" 
                                                        ImageUrl="~/Imagenes/Buscar_b.JPG" 
                                                        onmouseout="this.src='/Imagenes/Buscar_B.JPG';" 
                                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" 
                                                        ToolTip="Efectúa la Búsqueda del Cliente" Visible="False" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="cmd_Limpiar" runat="server" 
                                                        ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                        onmouseout="this.src='/Imagenes/Limpiar_B.JPG';" 
                                                        onmouseover="this.src='/Imagenes/Limpiar_A.JPG';" 
                                                        ToolTip="Inicializa los Criterios de búsqueda" Visible="False" />
                                                    <asp:ImageButton ID="cmd_OcultaBusqueda" runat="server" 
                                                        ImageUrl="~/Imagenes/Ocultar_B.JPG" 
                                                        onmouseout="this.src='/Imagenes/Ocultar_B.JPG';" 
                                                        onmouseover="this.src='/Imagenes/Ocultar_A.JPG';" 
                                                        ToolTip="Oculta el Panel de Búsqueda" />
                                                </td>
                                            </tr>
                                        </table>
                         </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="width: 16px">
                        <asp:Panel ID="pnl_ListaClientes" runat="server" Width="995px" Height="290px" Wrap="False">
                            <asp:Label ID="lbl_Mensaje" runat="server" CssClass="LabelRojo" Visible="False" 
                                Width="223px"></asp:Label>
                            <asp:GridView ID="gvCuentasxCobrar" runat="server" AutoGenerateColumns="False" 
                                CellPadding="4" ForeColor="#333333"
                                GridLines= "Horizontal" AllowPaging="True" Width="991px" Height="16px" 
                                AllowSorting="True" BorderColor="Black" BorderStyle="Solid" 
                                BorderWidth="1px" >
                                <EditRowStyle Wrap="false" />
                                <EmptyDataRowStyle Wrap="false" />
                                <RowStyle CssClass="GrillaRow" Wrap="false" />
                                <HeaderStyle  CssClass="GrillaHeader" HorizontalAlign ="Center" Wrap="false" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" Wrap="false" />
                                <SelectedRowStyle CssClass="Grilla_Seleccion" Wrap="false" />
                                <FooterStyle Font-Bold="True" CssClass="GrillaFooter" Wrap="false" />
                                <PagerStyle HorizontalAlign="Center" CssClass="GrillaHeader" Wrap="false" />
                                <PagerSettings Mode ="NumericFirstLast" />
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="width: 49px">
                    <div style="OVERFLOW: auto; WIDTH: 1013px; HEIGHT: 357px; top: 643px; left: 146px;">
                            <table>
                                <tr>
                                    <td class="TituloCelda">
                                        Detalle de Deuda</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvDetalleDeuda" runat="server" AllowPaging="True" 
                                            AllowSorting="True" AutoGenerateColumns="False" BorderColor="Black" 
                                            BorderStyle="Solid" BorderWidth="1px" CellPadding="4" ForeColor="#333333" 
                                            Height="16px" Width="991px">
                                            <EditRowStyle Wrap="false" />
                                            <EmptyDataRowStyle Wrap="false" />
                                            <RowStyle CssClass="GrillaRow" Wrap="false" />
                                            <HeaderStyle CssClass="GrillaHeader" HorizontalAlign="Center" Wrap="false" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" Wrap="false" />
                                            <SelectedRowStyle CssClass="Grilla_Seleccion" Wrap="false" />
                                            <FooterStyle CssClass="GrillaFooter" Font-Bold="True" Wrap="false" />
                                            <PagerStyle CssClass="GrillaHeader" HorizontalAlign="Center" Wrap="false" />
                                            <PagerSettings Mode="NumericFirstLast" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" 
                                            GridLines="None" Height="161px" Width="964px">
                                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <EditRowStyle BackColor="#999999" />
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 12px">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                            &nbsp;</td>
                                </tr>
                            </table>
                     </div>
                    </td>
                </tr>
                <tr>
                    <td>
                                            <cc1:MsgBox ID="MsgBoxClientes" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="font-family: Tahoma; font-size: x-small; font-weight: bold; color: #2B54A3; text-align: center;"
                        valign="middle" align="center">
                        <asp:Label ID="Label38" runat="server" Text="Powered by" Width="63px"></asp:Label>
                        <asp:ImageButton ID="cmd_Digrafic" runat="server"
                            AlternateText="Digrafic S.R.L." ImageUrl="~/Imagenes/LogoDigrafic_01.jpg"
                            PostBackUrl="http://www.digrafic.com.pe" Width="83px" 
                            ImageAlign="Middle" />
                    </td>
                </tr>
            </table>
</asp:Content>
