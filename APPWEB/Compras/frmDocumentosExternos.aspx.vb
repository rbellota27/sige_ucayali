﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmDocumentosExternos
    Inherits System.Web.UI.Page

#Region "Variables"
    Private drop As Combo
    Private fecha As Negocio.FechaActual
    Private objscript As New ScriptManagerClass
    Private list_DocumentoRef As List(Of Entidades.DocumentoView)
    Private list_Detalle As List(Of Entidades.DetalleDocumento)
    Private list_DetalleGuia As List(Of Entidades.DocumentoView)

    Private Util As Negocio.Util
    Private Enum operativo
        Ninguno = 0
        GuardarNuevo = 1
        Actualizar = 2
        Buscar = 3
    End Enum


#End Region

    Private Sub Mantenimiento(ByVal tipo As Integer)
        list_Detalle = getListaDetalle()
        Dim objDocumento As New Entidades.Documento
        With objDocumento
            .Id = CInt(hdd_iddocumento.Value)
            .Codigo = tbnumeroExt.Text.Trim
            .Serie = tbserieExt.Text.Trim
            .FechaEmision = CDate(tbfechaemisionExt.Text.Trim)
            .FechaVenc = CDate(IIf(tbfechavenc.Text.Trim = "", Nothing, tbfechavenc.Text.Trim))
            .FechaCancelacion = CDate(IIf(tbfechacanc.Text.Trim = "", Nothing, tbfechacanc.Text.Trim))
            .TotalAPagar = CDec(tbtotal.Text)
            .IdMoneda = CInt(dlmoneda.SelectedValue)
            .IdTienda = CInt(dltienda.SelectedValue)
            .IdEmpresa = CInt(dlpropietario.SelectedValue)
            Dim totalALetras As String = String.Empty
            Select Case .IdMoneda
                Case 1
                    totalALetras = "/100 SOLES"
                Case 2
                    totalALetras = "/100 DÓLARES AMERICANOS"
            End Select
            .TotalLetras = (New Aletras).Letras(Math.Round(.TotalAPagar, 2).ToString) + totalALetras
            .IdPersona = CInt(hdd_idpersona.Value)
            .IdUsuario = CInt(hdd_idusuario.Value)
            .IdTipoDocumento = CInt(dlTipoDocumento.SelectedValue)
            .IdCondicionPago = CInt(dlcondicionpago.SelectedValue)
            .IdDocRelacionado = CInt(hdd_iddocRelacionado.Value)
        End With
        Dim objObservacion As New Entidades.Observacion
        With objObservacion
            .Id = CInt(hdd_idobservacion.Value)
            .Observacion = tbobservacion.Text.Trim
        End With

        Dim mensaje As String = String.Empty
        Dim update As Boolean = False
        Select Case tipo
            Case operativo.GuardarNuevo
                hdd_iddocumento.Value = CStr((New Negocio.DocumentoExterno).Documento_Externo_Insert(objDocumento, list_Detalle, objObservacion))
                mensaje = "El documento ha sido guardado"
            Case operativo.Actualizar
                update = (New Negocio.DocumentoExterno).Documento_Externo_Update(objDocumento, list_Detalle, objObservacion)
                mensaje = "El documento ha sido actualizado"
        End Select
        objscript.mostrarMsjAlerta(Me, mensaje)
        MostrarBotones("Load")
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                hdd_idusuario.Value = CStr(Session("IdUsuario"))
                hdd_operativo.Value = CStr(operativo.Ninguno)
                CargarDatosOnLoad()
                MostrarBotones("Load")
            End If
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub CargarDatosOnLoad()
        drop = New Combo
        drop.LLenarCboTipoDocumentoRefxIdTipoDocumento(dlTipoDocumento, 46, 2, True) ' 46 registro de documento externos
        drop.LlenarCboRol(dlrolpersona, True)
        drop.LlenarCboMoneda(dlmoneda, True)
        drop.LlenarCboCondicionPago(dlcondicionpago)
        dlcondicionpago.Items.Insert(0, New ListItem("------", "0"))
        drop.LLenarCboEstadoDocumento(dlestado)
        drop.LLenarCboTienda(dltienda, True)
        drop.LlenarCboPropietario(dlpropietario, False)
    End Sub

#Region "procedimientos"

    Private Sub LimpiarRelacion_documento()
        hdd_iddocRelacionado.Value = "0"
        hdd_idguias.Value = "0"

        tbserie.Text = String.Empty
        tbnumero.Text = String.Empty
        tbfechaemision.Text = String.Empty
        tbtotal.Text = String.Empty
        tbtotaldocumento.Text = String.Empty

        tbbuscarserieref.Text = String.Empty
        tbbuscarcodigoref.Text = String.Empty
        tbfechafin.Text = String.Empty
        tbfechaini.Text = String.Empty
        dltienda.SelectedIndex = 0

        rbtipo.SelectedValue = "2"
        panelfechas.Visible = False
        panelserienumero.Visible = True

        gvdetalleguia.DataBind()
        gvproductos.DataBind()
        gvdocumentoref.DataBind()

    End Sub
    Private Sub LimpiarForm()
        hdd_idpersona.Value = "0"
        hdd_iddocumento.Value = "0"
        hdd_idobservacion.Value = "0"
        hdd_operativo.Value = CStr(operativo.Ninguno)

        tbPersona.Text = String.Empty
        tbdni.Text = String.Empty
        tbruc.Text = String.Empty
        tbobservacion.Text = String.Empty

        tbrazonape.Text = String.Empty
        tbserieExt.Text = String.Empty
        tbnumeroExt.Text = String.Empty
        tbfechacanc.Text = String.Empty
        tbfechavenc.Text = String.Empty

        dlTipoDocumento.SelectedIndex = 0
        dldocumento.Items.Clear()
        dltipodocumentoref.Items.Clear()
        dlmoneda.SelectedIndex = 0

        rbtipo.SelectedValue = "2"
        panelfechas.Visible = False

        LimpiarRelacion_documento()

        fecha = New Negocio.FechaActual
        Me.tbfechaemisionExt.Text = Format(fecha.SelectFechaActual, "dd/MM/yyyy")
        tbfechaini.Text = tbfechaemisionExt.Text
        tbfechafin.Text = tbfechaemisionExt.Text
        tbfechainibusqueda.Text = tbfechaemisionExt.Text
        tbfechafinbusqueda.Text = tbfechaemisionExt.Text
    End Sub
    Private Sub MostrarBotones(ByVal modo As String)
        Select Case modo
            Case "Load"
                btnuevo.Visible = True
                btbuscar.Visible = True
                bteditar.Visible = False
                btcancelar.Visible = False
                btguardar.Visible = False
                btanular.Visible = False
                btimprimir.Visible = False
                btBuscarDocumento.Visible = False
                lbAvanzado.Visible = False
                pnlprincipal.Enabled = False

            Case "Nuevo"
                btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = False
                btcancelar.Visible = True
                btguardar.Visible = True
                btanular.Visible = False
                btimprimir.Visible = False
                btBuscarDocumento.Visible = False
                lbAvanzado.Visible = False
                pnlprincipal.Enabled = True

            Case "Buscar"
                btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = False
                btcancelar.Visible = True
                btguardar.Visible = False
                btanular.Visible = False
                btimprimir.Visible = False
                btBuscarDocumento.Visible = True
                lbAvanzado.Visible = True
                pnlprincipal.Enabled = True

            Case "DocumentoEncontrado"
                btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = True
                btcancelar.Visible = True
                btguardar.Visible = False
                btanular.Visible = False
                btimprimir.Visible = True
                btBuscarDocumento.Visible = False
                lbAvanzado.Visible = False
                pnlprincipal.Enabled = False

            Case "Editar"
                btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = False
                btcancelar.Visible = True
                btguardar.Visible = True
                btanular.Visible = True
                btimprimir.Visible = False
                btBuscarDocumento.Visible = False
                lbAvanzado.Visible = False
                pnlprincipal.Enabled = True

        End Select
    End Sub

    Private Function getDataSourceDetalle() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item("Detalle"), List(Of Entidades.DetalleDocumento))
    End Function

    Private Sub setDataSourceDetalle(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove("Detalle")
        Session.Add("Detalle", lista)
    End Sub

#End Region

#Region "Busquedas"

#Region "Busqueda de documento"
    Private Sub Documentoref_Select()
        Dim rango As String = "CONVERT(datetime,'" + tbfechaini.Text + "',103) and CONVERT(datetime,'" + tbfechafin.Text + "',103)"
        Dim codigo As Integer = CInt(IIf(tbbuscarcodigoref.Text = "", 0, tbbuscarcodigoref.Text))
        Dim serie As Integer = CInt(IIf(tbbuscarserieref.Text = "", 0, tbbuscarserieref.Text))
        list_DocumentoRef = (New Negocio.DocumentoExterno).DocumentoExterno_BuscarReferencia(CInt(rbtipo.SelectedValue), _
                                                                                           rango, codigo, serie, CInt(hdd_idpersona.Value), CInt(dltipodocumentoref.SelectedValue))
        If list_DocumentoRef.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('capadocumentoref'); alert('No se hallaron registros');", True) : Exit Sub
        Else
            gvdocumentoref.DataSource = list_DocumentoRef
            gvdocumentoref.DataBind()
        End If
        objscript.onCapa(Me, "capadocumentoref")
    End Sub
    Private Sub btdocumentoref_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btdocumentoref.Click
        Try
            Documentoref_Select()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub gvdocumentoref_Select(ByVal index As Integer)
        hdd_iddocRelacionado.Value = CType(gvdocumentoref.Rows(index).Cells(1).FindControl("hddIdDocumento"), HiddenField).Value
        dlmoneda.SelectedValue = CType(gvdocumentoref.Rows(index).Cells(1).FindControl("hddidmoneda"), HiddenField).Value
        dldocumento.SelectedValue = CType(gvdocumentoref.Rows(index).Cells(1).FindControl("hddidtipodocumento"), HiddenField).Value
        hdd_idguias.Value = CType(gvdocumentoref.Rows(index).Cells(1).FindControl("hddidguias"), HiddenField).Value
        dlcondicionpago.SelectedValue = CType(gvdocumentoref.Rows(index).Cells(1).FindControl("hddidcondicionpago"), HiddenField).Value
        dltienda.SelectedValue = CType(gvdocumentoref.Rows(index).Cells(1).FindControl("hddidtienda"), HiddenField).Value
        dlpropietario.SelectedValue = CType(gvdocumentoref.Rows(index).Cells(1).FindControl("hddidempresa"), HiddenField).Value

        tbserie.Text = gvdocumentoref.Rows(index).Cells(3).Text
        tbnumero.Text = gvdocumentoref.Rows(index).Cells(4).Text
        tbtotaldocumento.Text = FormatNumber(CDec(gvdocumentoref.Rows(index).Cells(7).Text), 2)
        tbfechaemision.Text = gvdocumentoref.Rows(index).Cells(8).Text
        lblSimboloMoneda.Text = dlmoneda.SelectedItem.Text

        list_Detalle = (New Negocio.DetalleDocumento).SelectxIdDocumento(CInt(Me.hdd_iddocRelacionado.Value))
        If list_Detalle.Count > 0 Then
            gvproductos.DataSource = list_Detalle
            gvproductos.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "CalcularMonto(); ", True)
        End If

    End Sub
    Private Sub gvdocumentoref_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvdocumentoref.SelectedIndexChanged
        Try
            gvdocumentoref_Select(gvdocumentoref.SelectedIndex)
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub imgBuscarDetalles_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim row As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)

        lblproducto.Text = "Consulta del Producto : " + HttpUtility.HtmlDecode(row.Cells(2).Text) + "."
        list_DetalleGuia = (New Negocio.DocumentoExterno).DocumentoExterno_ConsultarGuia(hdd_idguias.Value, CInt(row.Cells(1).Text))

        If list_DetalleGuia.Count = 0 Then
            objscript.mostrarMsjAlerta(Me, "El producto [ " + HttpUtility.HtmlDecode(row.Cells(2).Text) + " ] \n no tiene guía de recepción.") : Exit Sub
        End If
        gvdetalleguia.DataSource = list_DetalleGuia
        gvdetalleguia.DataBind()
        objscript.onCapa(Me, "capaGuia")
    End Sub
    Private Sub rbtipo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtipo.SelectedIndexChanged
        rbtipo_Select()
    End Sub
    Private Sub rbtipo_Select()
        panelserienumero.Visible = False
        panelfechas.Visible = False
        Select Case rbtipo.SelectedValue
            Case "1"
                panelfechas.Visible = True
            Case "2"
                panelserienumero.Visible = True
        End Select
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('capadocumentoref');", True)
    End Sub

#End Region

#Region "Busqueda de Personas"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                      ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                      ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objscript.onCapa(Me, "capapersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capapersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btPersona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btPersona.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbrazonape.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("rol", dlrolpersona.SelectedValue)
            ViewState.Add("estado", 1) '******************* ACTIVO

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#End Region

#Region "eventos de controles"
    Protected Sub dlTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlTipoDocumento.SelectedIndexChanged
        Try
            LimpiarRelacion_documento()
            drop = New Combo
            drop.LLenarCboTipoDocumentoRefxIdTipoDocumento(dltipodocumentoref, CInt(dlTipoDocumento.SelectedValue), 2, False)
            dldocumento.Items.Clear()
            drop.LLenarCboTipoDocumentoRefxIdTipoDocumento(dldocumento, CInt(dlTipoDocumento.SelectedValue), 2, False)
            dldocumento.Items.Insert(0, New ListItem("------", "0"))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvproductos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvproductos.SelectedIndexChanged
        Try
            list_Detalle = getListaDetalle()
            list_Detalle.RemoveAt(gvproductos.SelectedIndex)
            gvproductos.DataSource = list_Detalle
            gvproductos.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "CalcularMonto(); ", True)
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function getListaDetalle() As List(Of Entidades.DetalleDocumento)
        list_Detalle = New List(Of Entidades.DetalleDocumento)

        For Each row As GridViewRow In gvproductos.Rows
            Dim obj As New Entidades.DetalleDocumento
            With obj
                .IdProducto = CInt(row.Cells(1).Text)
                .NomProducto = HttpUtility.HtmlDecode(row.Cells(2).Text)
                .UMedida = HttpUtility.HtmlDecode(row.Cells(3).Text)
                .Cantidad = CDec(CType(row.Cells(4).FindControl("gtbcantidad"), TextBox).Text)
                .IdUnidadMedida = CInt(CType(row.Cells(4).FindControl("hddidmedida"), HiddenField).Value)
                .CantxAtender = CDec(CType(row.Cells(5).FindControl("gtbcantidadxatender"), TextBox).Text)
                .PrecioCD = CDec(CType(row.Cells(7).FindControl("gtbprecioCD"), TextBox).Text)
                .Importe = CDec(CType(row.Cells(7).FindControl("gtbimporte"), TextBox).Text)
            End With
            list_Detalle.Add(obj)
        Next
        Return list_Detalle
    End Function

#End Region

#Region "Botones"
    Private Sub btnuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnuevo.Click
        LimpiarForm()
        hdd_operativo.Value = CStr(operativo.GuardarNuevo)
        MostrarBotones("Nuevo")
    End Sub
    Private Sub btguardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btguardar.Click
        Try
            Me.Mantenimiento(CInt(hdd_operativo.Value))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btcancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btcancelar.Click
        LimpiarForm()
        MostrarBotones("Load")
    End Sub
    Private Sub btanular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btanular.Click
        Try
            If hdd_iddocumento.Value <> "0" Then
                Dim objOrdenCompra As New Negocio.OrdenCompra
                objOrdenCompra.AnularOrdenCompra(CInt(hdd_iddocumento.Value))
                dlestado.SelectedValue = "2"
                MostrarBotones("Load")
            End If
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub bteditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bteditar.Click
        list_Detalle = getDataSourceDetalle()
        gvproductos.DataSource = list_Detalle
        gvproductos.DataBind()
        hdd_operativo.Value = CStr(operativo.Actualizar)
        MostrarBotones("Editar")
    End Sub
    Private Sub btbuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btbuscar.Click
        LimpiarForm()
        hdd_operativo.Value = CStr(operativo.Buscar)
        MostrarBotones("Buscar")
    End Sub
    Private Sub btBuscarDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarDocumento.Click
        Try
            btBuscarDocumento_Select()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btBuscarDocumento_Select()
        Dim obj As Entidades.DocumentoView = (New Negocio.DocumentoExterno).DocumentoExterno_SerieNumero(CInt(tbserieExt.Text), _
                                                                                                                                        CInt(tbnumeroExt.Text), CInt(dlTipoDocumento.SelectedValue), _
                                                                                                                                                CInt(hdd_idpersona.Value))
        With obj
            hdd_iddocumento.Value = CStr(.Id)
            tbnumeroExt.Text = .Codigo
            tbserieExt.Text = .Serie
            tbfechaemisionExt.Text = .FechaEmision
            tbfechavenc.Text = CStr(IIf(.FechaVenc = Nothing, "", .FechaVenc))
            tbtotal.Text = FormatNumber(.TotalAPagar, 2)
            hdd_idpersona.Value = CStr(.IdPersona)
            dlestado.SelectedValue = CStr(.IdEstadoDoc)
            dlmoneda.SelectedValue = CStr(.IdMoneda)
            dlTipoDocumento.SelectedValue = CStr(.IdTipoDocumento)
            tbfechacanc.Text = CStr(IIf(.FechaCancelacion = Nothing, "", .FechaCancelacion))
            dlcondicionpago.SelectedValue = CStr(.IdCondicionPago)
            tbruc.Text = .RUC
            tbdni.Text = .DNI
            tbPersona.Text = .RazonSocial
        End With

        list_Detalle = (New Negocio.DetalleDocumento).SelectxIdDocumento(CInt(Me.hdd_iddocumento.Value))
        If list_Detalle.Count > 0 Then
            gvproductos.DataSource = list_Detalle
            gvproductos.DataBind()
        End If
        setDataSourceDetalle(list_Detalle)

        verDocumentoRelacionado()

        verObservaciones()

        MostrarBotones("DocumentoEncontrado")

    End Sub


    Private Sub verDocumentoRelacionado()
        Dim DocRelacionado As Entidades.DocumentoView = (New Negocio.DocumentoExterno).DocumentoExterno_RelacionDoc(CInt(Me.hdd_iddocumento.Value))
        With DocRelacionado
            hdd_iddocRelacionado.Value = CStr(.IdDocRelacionado)
            tbnumero.Text = .Codigo
            tbserie.Text = .Serie
            tbfechaemision.Text = FormatDateTime(CDate(.FechaEmision), DateFormat.ShortDate)
            dldocumento.Text = CStr(.IdTipoDocumento)
            tbtotaldocumento.Text = FormatNumber(.TotalAPagar, 2)
            dltienda.SelectedValue = CStr(.IdTienda)
            hdd_idguias.Value = .NroDocumento
        End With
    End Sub

#End Region

    Private Sub imgbusqueda_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgbusqueda.Click
        Try
            imgbusqueda_Select()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub imgbusqueda_Select()
        Dim rango As String = "CONVERT(datetime,'" + tbfechainibusqueda.Text + "',103) and CONVERT(datetime,'" + tbfechafinbusqueda.Text + "',103)"
        Dim list_busqueda As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoExterno).DocumentoExterno_Busqueda(rango, CInt(hdd_idpersona.Value), CInt(dlTipoDocumento.SelectedValue))

        If list_busqueda.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('capabusqueda'); alert('No se hallaron registros');", True) : Exit Sub
        Else
            gvBusqueda.DataSource = list_busqueda
            gvBusqueda.DataBind()
        End If

        objscript.onCapa(Me, "capabusqueda")
    End Sub

    Private Sub gvBusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBusqueda.SelectedIndexChanged
        Try
            hdd_idpersona.Value = CType(gvBusqueda.SelectedRow.Cells(1).FindControl("hddidpersona"), HiddenField).Value
            dlTipoDocumento.SelectedValue = CType(gvBusqueda.SelectedRow.Cells(1).FindControl("hddidtipodocumento"), HiddenField).Value
            tbserieExt.Text = gvBusqueda.SelectedRow.Cells(3).Text
            tbnumeroExt.Text = gvBusqueda.SelectedRow.Cells(4).Text

            btBuscarDocumento_Select()

            list_Detalle = (New Negocio.DetalleDocumento).SelectxIdDocumento(CInt(Me.hdd_iddocumento.Value))
            If list_Detalle.Count > 0 Then
                gvproductos.DataSource = list_Detalle
                gvproductos.DataBind()
            End If
            setDataSourceDetalle(list_Detalle)

            verObservaciones()

            MostrarBotones("DocumentoEncontrado")

            verDocumentoRelacionado()

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub verObservaciones()
        Dim objObs As Entidades.Observacion = (New Negocio.OrdenCompra).listarObservacionDocumento(CInt(hdd_iddocumento.Value))
        tbobservacion.Text = objObs.Observacion
        hdd_idobservacion.Value = CStr(objObs.Id)
    End Sub
End Class