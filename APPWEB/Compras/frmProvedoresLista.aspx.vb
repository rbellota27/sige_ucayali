'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Partial Public Class Proveedores
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub ddl_Proveedores_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Proveedores.SelectedIndexChanged
        '***** Muestra el text para efectuar el filtro
        Select Case ddl_Proveedores.Text
            Case "Todos los Proveedores"
                '***** Muestra todos los proveedores
                txt_Filtro.Visible = False
                txt_Filtro.Text = ""
                cmd_AplicaFiltro.Visible = False
                ddl_Estados.Visible = False
            Case "R.U.C."
                '***** Filtra por Ruc
                ddl_Estados.Visible = False
                txt_Filtro.MaxLength = 11
                txt_Filtro.Width = 85
                txt_Filtro.Text = ""
                txt_Filtro.Visible = True
                cmd_AplicaFiltro.Visible = True
            Case "Nombre"
                '***** Filtra por Nombre/Razon Social
                ddl_Estados.Visible = False
                txt_Filtro.MaxLength = 35
                txt_Filtro.Width = 245
                txt_Filtro.Text = ""
                txt_Filtro.Visible = True
                cmd_AplicaFiltro.Visible = True
            Case "Estado"
                '***** Filtra por Estado
                txt_Filtro.Visible = False
                ddl_Estados.Visible = True
                cmd_AplicaFiltro.Visible = True
        End Select
    End Sub

End Class