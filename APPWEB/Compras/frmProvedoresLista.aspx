<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmProvedoresLista.aspx.vb" Inherits="APPWEB.Proveedores" 
    title="SIGE v1.0 - Maestro de Proveedores" Debug="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel1" runat="server" Height="150px" Width="865px">
        <asp:Panel ID="Panel2" runat="server" Height="63px" Width="770px">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Menu ID="Menu1" runat="server" BackColor="Control" DynamicHorizontalOffset="2"
                        Font-Bold="False" Font-Italic="False" Font-Names="Verdana" Font-Size="8pt" ForeColor="DarkRed"
                        Orientation="Horizontal" StaticSubMenuIndent="10px" Style="vertical-align: top">
                        <StaticSelectedStyle BackColor="#5D7B9D" />
                        <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                        <DynamicHoverStyle BackColor="Peru" ForeColor="White" />
                        <DynamicMenuStyle BackColor="#F7F6F3" />
                        <DynamicSelectedStyle BackColor="SteelBlue" />
                        <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                        <StaticHoverStyle BackColor="#7C6F57" ForeColor="White" />
                        <Items>
                            <asp:MenuItem Text="Areas" Value="Areas">
                                <asp:MenuItem NavigateUrl="~/Ventas.aspx" Text="Ventas" Value="Ventas"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/Caja/frmCaja.aspx" Text="Caja" Value="Caja"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/Almacen.aspx" Text="Almac&#233;n" Value="Almac&#233;n">
                                </asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/Compras.aspx" Text="Compras" Value="Compras"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/frmAdministracionSistema.aspx" Text="Administraci&#243;n de Sistema"
                                    Value="Administraci&#243;n de Sistema"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/frmInformacionGerencial.aspx" Text="Informaci&#243;n Gerencial"
                                    Value="Informaci&#243;n Gerencial"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/Principal.aspx" Text="Principal" Value="Principal"></asp:MenuItem>
                            </asp:MenuItem>
                            <asp:MenuItem Text="Mantenimientos" Value="Mantenimientos">
                                <asp:MenuItem NavigateUrl="~/Compras/frmProvedoresLista.aspx" Text="Proveedores"
                                    Value="Clientes"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/Mantenedores/frmProductosLista.aspx" Text="Productos"
                                    Value="Prodcutos"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/Mantenedores/FrmMantTablasGenerales.aspx" Text="Tablas Generales"
                                    Value="Tablas Generales"></asp:MenuItem>
                            </asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/Compras/frmOrdenCompraLista.aspx" Text="Orden de Compra"
                                Value="Orden de Compra"></asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/Compras/FrmPedidoLista.aspx" Text="Pedidos al Proveedor"
                                Value="Consultas"></asp:MenuItem>
                            <asp:MenuItem Text="Compras Por Proveedor" Value="Compras Por Proveedor"></asp:MenuItem>
                            <asp:MenuItem Text="Consulta de Stocks" Value="Consulta de Stocks"></asp:MenuItem>
                        </Items>
                    </asp:Menu>
                    <table cellpadding="0" cellspacing="0" style="width: 161px; height: 1px">
                        <tr>
                            <td style="width: 22px; height: 4px">
                                <asp:ImageButton ID="cmd_EditarOcD" runat="server" ImageUrl="~/Imagenes/Editar_B.JPG"
                                    onmouseout="this.src='/Imagenes/Editar_B.JPG';" onmouseover="this.src='/Imagenes/Editar_A.JPG';"
                                    ToolTip="Editar" /></td>
                            <td style="width: 22px; height: 4px">
                                <asp:ImageButton ID="cmd_NuevoOcD" runat="server" ImageUrl="~/Imagenes/Nuevo_B.JPG"
                                    onmouseout="this.src='/Imagenes/Nuevo_B.JPG';" onmouseover="this.src='/Imagenes/Nuevo_A.JPG';"
                                    ToolTip="Nuevo" /></td>
                            <td style="width: 22px; height: 4px">
                                <asp:ImageButton ID="cmd_BuscarOcD" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                    onmouseout="this.src='/Imagenes/Buscar_B.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';"
                                    ToolTip="Buscar" /></td>
                            <td style="width: 22px; height: 4px">
                                <asp:ImageButton ID="cmd_EliminarOcD" runat="server" ImageUrl="~/Imagenes/Anular_B.JPG"
                                    onmouseout="this.src='/Imagenes/Anular_B.JPG';" onmouseover="this.src='/Imagenes/Anular_A.JPG';"
                                    ToolTip="Eliminar" /></td>
                            <td style="width: 22px; height: 4px">
                                <asp:ImageButton ID="cmd_ImprimirOcD" runat="server" cToolTip="Imprimir" ImageUrl="~/Imagenes/Imprimir_B.JPG"
                                    onmouseout="this.src='/Imagenes/Imprimir_B.JPG';" onmouseover="this.src='/Imagenes/Imprimir_A.JPG';" /></td>
                        </tr>
                    </table>
                    <table style="width: 849px">
                        <tr>
                            <td style="height: 21px; text-align: center">
                                <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Tahoma" Font-Size="Medium"
                                    Text="Consulta de Proveedores"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="height: 26px">
            <asp:Label ID="Label1" runat="server" Font-Names="Arial" Text="Filtro Actual:"></asp:Label>
            <asp:DropDownList ID="ddl_Proveedores" runat="server" AutoPostBack="True" Font-Names="Arial">
                <asp:ListItem>Todos los Proveedores</asp:ListItem>
                <asp:ListItem>R.U.C.</asp:ListItem>
                <asp:ListItem>Nombre</asp:ListItem>
                <asp:ListItem>Estado</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txt_Filtro" runat="server" Visible="False" Width="100px"></asp:TextBox>
                                <asp:DropDownList ID="ddl_Estados" runat="server" Visible="False">
                                    <asp:ListItem>Activo</asp:ListItem>
                                    <asp:ListItem>Inactivo</asp:ListItem>
                                </asp:DropDownList>
            <asp:ImageButton ID="cmd_AplicaFiltro" runat="server" ImageUrl="~/Imagenes/Ok_b.bmp"
                ToolTip="Aplicar Filtro" Visible="False" /></td>
                        </tr>
                        <tr>
                            <td>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource1" EmptyDataText="No hay registros de datos para mostrar."
                ForeColor="#333333" GridLines="None" ShowFooter="True" Width="982px">
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" Wrap="True" ForeColor="#333333" />
                <EmptyDataRowStyle Wrap="True" />
                <Columns>
                    <asp:CommandField ButtonType="Image" SelectImageUrl="~/Imagenes/Ok_b.bmp" ShowSelectButton="True" />
                    <asp:BoundField DataField="RUC" HeaderText="RUC" SortExpression="RUC">
                        <ControlStyle Width="85px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Proveedor" HeaderText="Proveedor" SortExpression="Proveedor">
                        <ControlStyle Width="345px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Direccion" HeaderText="Direccion" SortExpression="Direccion">
                        <ControlStyle Width="345px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Contacto" HeaderText="Contacto" SortExpression="Contacto">
                        <ControlStyle Width="200px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Telefono" HeaderText="Telefono" SortExpression="Telefono">
                        <ControlStyle Width="120px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Estado" HeaderText="Estado" SortExpression="Estado">
                        <ControlStyle Width="40px" />
                    </asp:BoundField>
                </Columns>
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" Wrap="True" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle Wrap="True" BackColor="#999999" />
                <AlternatingRowStyle BackColor="White" Wrap="True" ForeColor="#284775" />
            </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    &nbsp; &nbsp;
                </ContentTemplate>
            </asp:UpdatePanel>
            &nbsp;
        </asp:Panel>
        </asp:Panel>
</asp:Content>
