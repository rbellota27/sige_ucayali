﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmOrdenPedido
    Inherits System.Web.UI.Page

#Region "variables"
    Private objScript As New ScriptManagerClass
    Private drop As Combo
    Private fecha As Negocio.FechaActual
    Private ListaCatalogoProductos As List(Of Entidades.Catalogo)
    Private ListaProductos As List(Of Entidades.DetalleDocumentoView)
    Private objDetalleDocumentoView As Entidades.DetalleDocumentoView
    Private Enum operativo
        Ninguno = 0
        GuardarNuevo = 1
        Actualizar = 2
        Anular = 4
        DocumentoEncontrado = 5
    End Enum
    Private objProgramacionPedido As Entidades.ProgramacionPedido
    Private ListDocumento As List(Of Entidades.DocumentoView)
    Private ListaDetalleDocumento As List(Of Entidades.DetalleDocumento)
#End Region

#Region "Procedimientos"

    Private Sub Mantenimiento(ByVal tipo As Integer)

        If operativo.GuardarNuevo = tipo Then generarNroDoc(tbCodigoDocumento)

        Select Case dlOperacion.SelectedValue
            Case "18"  'PEDIDO ENTRE SUCURSALES
                ListaDetalleDocumento = getListaDetalleSucursal()

            Case "19" 'PEDIDO AL CENTRO DISTRIBUIDOR
                ListaProductos = getListaProductos(gvProducto)
                ListaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
                For i As Integer = 0 To ListaProductos.Count - 1
                    Dim objDetalleDocumento As New Entidades.DetalleDocumento
                    With objDetalleDocumento
                        .IdProducto = ListaProductos(i).IdProducto
                        .IdUnidadMedida = ListaProductos(i).IdUMedida
                        .UMedida = ListaProductos(i).UM
                        .Cantidad = ListaProductos(i).Cantidad
                        .CantxAtender = ListaProductos(i).Cantidad

                    End With
                    ListaDetalleDocumento.Add(objDetalleDocumento)
                Next
        End Select

        Dim objDocumento As New Entidades.Documento
        Dim totalALetras As String = ""
        With objDocumento
            .Codigo = Me.tbCodigoDocumento.Text.Trim
            .Serie = Me.dlSerie.SelectedItem.Text
            .IdSerie = CInt(Me.dlSerie.SelectedValue)
            .FechaEmision = CDate(Me.tbFechaEmision.Text)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdEmpresa = CInt(Me.dlEmpresa.SelectedValue)
            .IdTienda = CInt(Me.dlTienda.SelectedValue)
            'IdAlmacen SE GUARDA EL ALMACEN QUE ESTA HACIENDO EL PEDIDO
            .IdAlmacen = CInt(Me.dlAlmacen.SelectedValue)
            .IdEstadoDoc = CInt(Me.dlEstadoDocumento.SelectedValue)
            .IdTipoOperacion = CInt(dlOperacion.SelectedValue)
            .IdTipoDocumento = 15 ' orden pedido
            'LugarEntrega SE GUARDA EL ALMACEN AL CUAL SE LE ESTA HACIENDO EL PEDIDO
            .LugarEntrega = CInt(dlAlmacen2.SelectedValue)
            .IdTiendaSucursal = CInt(dlTienda2.SelectedValue)
            .IdDocRelacionado = CInt(hdd_iddocumentoref.Value)
        End With

        If dlOperacion.SelectedValue = "19" Then
            objProgramacionPedido = New Entidades.ProgramacionPedido
            With objProgramacionPedido
                .IdYear = CInt(String.Format("{0:yyyy}", CDate(tbFechaEmision.Text)))
                .IdSemana = CInt(dlweek.SelectedValue)
            End With
        End If

        Dim objObs As New Entidades.Observacion
        objObs.Id = CInt(hddidObs.Value)
        objObs.Observacion = txtobservacion.Text.Trim

        Dim update As Boolean = False
        Dim IdxCodido() As String
        Select Case tipo
            Case operativo.GuardarNuevo
                IdxCodido = (New Negocio.DocOrdenPedido).InsertOrdenPedido(objDocumento, ListaDetalleDocumento, objObs, ckcomprometer.Checked, objProgramacionPedido).Split(CChar(","))
                hddIdDocumento.Value = IdxCodido(0)
                tbCodigoDocumento.Text = IdxCodido(1)
            Case operativo.Actualizar
                If CInt(hddIdDocumento.Value) <> 0 Then
                    objDocumento.Id = CInt(hddIdDocumento.Value)
                    update = (New Negocio.DocOrdenPedido).UpdateOrdenPedido(objDocumento, ListaDetalleDocumento, objObs, ckcomprometer.Checked, objProgramacionPedido)
                End If
        End Select

        MostrarBotones("Load")
        btImprimir.Visible = True
        Select Case tipo
            Case operativo.GuardarNuevo
                If hddIdDocumento.Value <> "0" Then objScript.mostrarMsjAlerta(Me, "El Documento ha sido guardado con exito")
            Case operativo.Actualizar
                If CInt(hddIdDocumento.Value) <> 0 Then
                    If update = True Then objScript.mostrarMsjAlerta(Me, "El documento ha sido actualizado")
                End If
        End Select

    End Sub

    Private Sub CargarAlIniciar(ByVal tipo As Integer)
        drop = New Combo

        drop.LlenarCboEmpresaxIdUsuario(dlEmpresa, CInt(Session("IdUsuario")), False)
        drop.LlenarCboTiendaxIdEmpresaxIdUsuario(dlTienda, CInt(dlEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
        drop.llenarCboAlmacenxIdTienda(dlAlmacen, CInt(dlTienda.SelectedValue))

        llenarTienda2(dlTienda2)

        drop.LLenarCboEstadoDocumento(dlEstadoDocumento)

        drop.llenarCboTipoOperacionxIdTpoDocumento(dlOperacion, 15, True) '15 ++ orden de pedido
        dlOperacion.SelectedValue = CStr(tipo)

        cargarSerie(dlSerie)
        generarNroDoc(tbCodigoDocumento)

        drop.LlenarCboLinea(cmbLinea_AddProd, True)

        fecha = New Negocio.FechaActual
        Me.tbFechaEmision.Text = Format(fecha.SelectFechaActual, "dd/MM/yyyy")
        ViewState.Add("fecha", Me.tbFechaEmision.Text)
        hdd_idsemana.Value = CStr((New Negocio.DocOrdenPedido).OrdenPedido_SemanaActual(CDate(Me.tbFechaEmision.Text)))
        For x As Integer = 1 To 12
            dlmonth.Items.Add(New ListItem(String.Format("{0:MMMM}", MonthName(x)), CStr(x)))
        Next
        dlmonth.Items.Insert(0, New ListItem("-Seleccionar-", "0"))
    End Sub

    Private Sub cargarSerie(ByVal cb As DropDownList)
        LimpiarCabeceraDoc()
        drop = New Combo
        drop.LLenarCboSeriexIdsEmpTienTipoDoc(cb, CInt(dlEmpresa.SelectedValue), _
                                              CInt(dlTienda.SelectedValue), 15)
    End Sub

    Private Sub generarNroDoc(ByVal tb As TextBox)
        If dlSerie.Items.Count = 0 Then
            tb.Text = String.Empty
            Exit Sub
        End If
        tb.Text = (New Negocio.Serie).GenerarCodigo(CInt(dlSerie.SelectedValue))
    End Sub

    Private Sub llenarTienda2(ByVal drop2 As DropDownList)
        dlTienda2.DataSource = getListaAlmacen()
        dlTienda2.DataBind()
        dlTienda2.Items.Insert(0, New ListItem("- Seleccionar -", "0"))
        dlAlmacen2.Items.Clear()
    End Sub

    Private Function getListaAlmacen() As List(Of Entidades.Tienda)
        Dim lista As New List(Of Entidades.Tienda)
        For x As Integer = 0 To dlTienda.Items.Count - 1
            Dim obj As New Entidades.Tienda
            obj.Id = CInt(dlTienda.Items(x).Value)
            obj.Nombre = CStr(HttpUtility.HtmlDecode(dlTienda.Items(x).Text))
            lista.Add(obj)
        Next
        Return lista
    End Function

    Private Sub MostrarBotones(ByVal modo As String)
        Select Case modo
            Case "Load"
                btNuevo.Visible = True
                btBuscar.Visible = True
                btEditar.Visible = False
                btCancelar.Visible = False
                btGuardar.Visible = False
                btAnular.Visible = False
                btImprimir.Visible = False
                btnBuscarDocumento.Visible = False
                pnlDocumento.Enabled = True
                panelPrincipal.Enabled = False

            Case "Nuevo"
                btNuevo.Visible = False
                btBuscar.Visible = False
                btEditar.Visible = False
                btCancelar.Visible = True
                btGuardar.Visible = True
                btAnular.Visible = False
                btImprimir.Visible = False
                btnBuscarDocumento.Visible = False
                panelPrincipal.Enabled = True
                pnlDocumento.Enabled = False

            Case "Buscar"
                btNuevo.Visible = False
                btBuscar.Visible = False
                btEditar.Visible = False
                btCancelar.Visible = True
                btGuardar.Visible = False
                btAnular.Visible = False
                btImprimir.Visible = False
                btnBuscarDocumento.Visible = True
                panelPrincipal.Enabled = False
                pnlDocumento.Enabled = True

            Case "DocumentoEncontrado"
                btNuevo.Visible = False
                btBuscar.Visible = False
                btEditar.Visible = True
                btCancelar.Visible = True
                btGuardar.Visible = False
                btAnular.Visible = False
                btImprimir.Visible = True
                btnBuscarDocumento.Visible = False
                panelPrincipal.Enabled = False
                pnlDocumento.Enabled = False

            Case "Editar"
                btNuevo.Visible = False
                btBuscar.Visible = False
                btEditar.Visible = False
                btCancelar.Visible = True
                btGuardar.Visible = True
                btAnular.Visible = True
                btImprimir.Visible = False
                btnBuscarDocumento.Visible = False
                panelPrincipal.Enabled = True
                pnlDocumento.Enabled = False

        End Select
    End Sub

    Private Sub verPanel()
        pnlProgPedido.Visible = False
        pnlDocRef.Visible = False
        If dlOperacion.SelectedValue = "19" Then 'PEDIDO AL CENTRO DISTRIBUIDOR
            pnlProgPedido.Visible = True
            lbltipo.Text = "Solicitar Stock"
        End If
        If dlOperacion.SelectedValue = "18" Then 'PEDIDO ENTRE SUCURSALES
            pnlDocRef.Visible = True
            lbltipo.Text = "Consultar Stock"
        End If
    End Sub

    Private Function getListaDetalleSucursal() As List(Of Entidades.DetalleDocumento)
        Return CType(Session("ListaSucursal"), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleSucursal(ByVal Lista As List(Of Entidades.DetalleDocumento))
        Session.Remove("ListaSucursal")
        Session.Add("ListaSucursal", Lista)
    End Sub


#End Region

#Region "limpiar Form"

    Private Sub LimpiarForm()
        LimpiarCabeceraDoc()
        limpiarDocRef()

        txtobservacion.Text = ""
        gvProducto.DataBind()
        gvSucursal.DataBind()
        hdd_operativo.Value = CStr(operativo.Ninguno)
        hddIdDocumento.Value = "0"
        hddidObs.Value = "0"
        hddIdAlmacen.Value = "0"
        dlEstadoDocumento.SelectedValue = "1"
        limpiarProgPedido()
        dlAlmacen2.Items.Clear()
        dlTienda2.SelectedIndex = 0

        ListaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
        setListaDetalleSucursal(ListaDetalleDocumento)

    End Sub

    Private Sub LimpiarCabeceraDoc()
        dlSerie.Items.Clear()
        tbCodigoDocumento.Text = ""
    End Sub

    Private Sub limpiarProgPedido()
        dlweek.Items.Clear()
        tbfechaini.Text = ""
        tbfechafin.Text = ""
        tbutilday.Text = ""
        dlmonth.SelectedValue = "0"
    End Sub

    Private Sub limpiarDocRef()
        Me.hdd_iddocumentoref.Value = "0"
        Me.tbcodigoref.Text = ""
        Me.tbserieref.Text = ""
        Me.tbdocumento.Text = ""
        Me.tbclienteref.Text = ""
        Me.hdd_estadoentrega.Value = "0"
    End Sub
#End Region

#Region "Eventos Principales"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                CargarAlIniciar(CInt(Request.QueryString("idoperacion")))
                MostrarBotones("Load")
                hdd_operativo.Value = CStr(operativo.Ninguno)
                verPanel()
            End If
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btNuevo.Click
        Try
            LimpiarForm()
            hdd_operativo.Value = CStr(operativo.GuardarNuevo)
            verPanel()

            cargarSerie(dlSerie)
            generarNroDoc(tbCodigoDocumento)

            Select Case dlOperacion.SelectedValue
                Case "18" 'Entre sucursales
                   
                Case "19" 'Entre centro distribuidor
                    Dim cad() As String = (New Negocio.DocOrdenPedido).OrdenPedido_Validar_ProgramacionPedido( _
                                                                CDate(tbFechaEmision.Text), CInt(dlEmpresa.SelectedValue), _
                                                                CInt(dlTienda.SelectedValue), CInt(dlAlmacen.SelectedValue)).Split(CChar(","))
                    If cad(0) <> "" Then
                        Dim mensaje$ = "Se ha realizado un documento de la semana [" + cad(2) + "] \n" + _
                                    "con rango de fechas del [" + cad(3) + "] al [" + cad(4) + "] \n" + _
                                    "con serie numero: [" + cad(1) + "] de codigo numero: [" + cad(0) + "]."
                        objScript.mostrarMsjAlerta(Me, mensaje)
                        Exit Sub
                    Else
                        llenarCboSemana()
                    End If
            End Select
            MostrarBotones("Nuevo")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btGuardar.Click

        Try
            Mantenimiento(CInt(hdd_operativo.Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btEditar.Click
        Try
            Select Case dlOperacion.SelectedValue

                Case "18"  'PEDIDO ENTRE SUCURSALES
                    getListaDetalleSucursal()

                Case "19" 'PEDIDO AL CENTRO DISTRIBUIDOR
                    If dlweek.SelectedValue <> hdd_idsemana.Value Then
                        objScript.mostrarMsjAlerta(Me, "Sólo puede editar documentos de la semana [" + hdd_idsemana.Value + "].")
                        Exit Sub
                    End If
            End Select
            hdd_operativo.Value = CStr(operativo.Actualizar)
            MostrarBotones("Editar")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btCancelar.Click
        Try
            LimpiarForm()
            hdd_operativo.Value = CStr(operativo.Ninguno)
            MostrarBotones("Load")
            fecha = New Negocio.FechaActual
            Me.tbFechaEmision.Text = Format(fecha.SelectFechaActual, "dd/MM/yyyy")
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btBuscar.Click
        Try
            LimpiarForm()
            MostrarBotones("Buscar")
            cargarSerie(dlSerie)
            tbCodigoDocumento.ReadOnly = False
            tbCodigoDocumento.Focus()
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnular.Click
        Try
            If hddIdDocumento.Value <> "0" Then
                Dim objOrdenCompra As New Negocio.OrdenCompra
                objOrdenCompra.AnularOrdenCompra(CInt(hddIdDocumento.Value))
                dlEstadoDocumento.SelectedValue = "2"
                MostrarBotones("Load")
            End If
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#End Region

#Region "Seccion del Documento"

    Private Sub dlEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlEmpresa.SelectedIndexChanged
        Try
            'aqui cargo Tienda - Almacen - Serie Doc - Codigo Doc - Tienda2
            drop.LlenarCboTiendaxIdEmpresaxIdUsuario(dlTienda, CInt(dlEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            drop.llenarCboAlmacenxIdTienda(dlAlmacen, CInt(dlTienda.SelectedValue), True)
            cargarSerie(dlSerie)
            generarNroDoc(tbCodigoDocumento)
            llenarTienda2(dlTienda2)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub dlTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlTienda.SelectedIndexChanged
        Try
            'aqui cargo Serie Doc - Codigo Doc - Tienda2
            If hdd_idtiendavalidar.Value = "0" Then
                cargarSerie(dlSerie)
                generarNroDoc(tbCodigoDocumento)
                drop = New Combo
                drop.llenarCboAlmacenxIdTienda(dlAlmacen, CInt(dlTienda.SelectedValue))
                gvdocref.DataBind()
            Else
                objScript.mostrarMsjAlerta(Me, "Hay un documento de referencia no puede cambiar la tienda seleccionada")
                dlTienda.SelectedValue = hdd_idtiendavalidar.Value
            End If
            
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub dlTienda2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlTienda2.SelectedIndexChanged

        Try
            'aqui cargo el almacen2
            If dlTienda2.SelectedValue = dlTienda.SelectedValue Then
                objScript.mostrarMsjAlerta(Me, "No se puede seleccionar la misma [ Tienda ]")
                dlTienda2.SelectedValue = "0"
                Exit Sub
            End If
            drop = New Combo
            If dlOperacion.SelectedValue = "18" Then 'PEDIDO ENTRE SUCURSALES 
                drop.llenarCboAlmacenxIdTienda(dlAlmacen2, CInt(dlTienda2.SelectedValue), True)
                hddIdAlmacen.Value = dlAlmacen2.SelectedValue
            End If
            If dlOperacion.SelectedValue = "19" Then ' PEDIDO AL CENTRO DISTRIBUIDOR
                drop.LLenarCboALmacenPrincipalxIdTienda(dlAlmacen2, CInt(dlTienda2.SelectedValue), True)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "Seccion de Producto"

    Private Function getListaProductos(ByVal gv As GridView) As List(Of Entidades.DetalleDocumentoView)
        ListaProductos = New List(Of Entidades.DetalleDocumentoView)

        For Each fila As GridViewRow In gv.Rows
            objDetalleDocumentoView = New Entidades.DetalleDocumentoView
            With objDetalleDocumentoView
                .IdProducto = CInt(fila.Cells(1).Text)
                .Descripcion = CStr(HttpUtility.HtmlDecode(fila.Cells(2).Text))

                Dim lista As New List(Of Entidades.UnidadMedida)
                Dim cbo As DropDownList = CType(fila.Cells(3).FindControl("dlum"), DropDownList)
                .IdUMedida = CInt(cbo.SelectedValue)
                .UM = CStr(cbo.SelectedItem.Text)
                For x As Integer = 0 To cbo.Items.Count - 1
                    Dim obj As New Entidades.UnidadMedida
                    obj.DescripcionCorto = CStr(HttpUtility.HtmlDecode(cbo.Items(x).Text))
                    obj.Id = CInt(cbo.Items(x).Value)
                    lista.Add(obj)
                Next
                .ListaUMedida = lista
                .Cantidad = CDec(CType(fila.Cells(4).FindControl("txtCant"), TextBox).Text)
            End With
            ListaProductos.Add(objDetalleDocumentoView)
        Next

        Return ListaProductos
    End Function

    Private Sub buscarProductos(ByVal grilla As GridView, ByVal nomProducto As String, _
                                          ByVal idlinea As Integer, ByVal idsublinea As Integer, _
                                          ByVal codSubLinea As Integer, ByVal idalmacen As Integer, _
                                          ByVal tipomov As Integer, ByVal idalmacen1 As Integer)
        Dim index As Integer = 0
        Select Case tipomov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(Me.txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        ListaCatalogoProductos = (New Negocio.DocOrdenPedido).ListarProductosOrdenPedido(idalmacen, nomProducto, idlinea, idsublinea, codSubLinea, index, grilla.PageSize, idalmacen1)

        If ListaCatalogoProductos.Count <= 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " onCapa('verBusquedaProducto'); alert('No se hallaron Registros'); ", True)
        Else
            gvListaProducto.DataSource = Me.ListaCatalogoProductos
            gvListaProducto.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)
            objScript.onCapa(Me, "verBusquedaProducto")
        End If
    End Sub

    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click

        Try
            ViewState.Add("NomProducto", txtDescripcionProd_AddProd.Text.Trim)
            ViewState.Add("IdLinea", cmbLinea_AddProd.SelectedValue)
            ViewState.Add("IdSubLinea", IIf(cmbSubLinea_AddProd.Items.Count = 0, "0", cmbSubLinea_AddProd.SelectedValue))
            ViewState.Add("CodSubLinea", IIf(txtCodigoSubLinea_AddProd.Text.Trim = "", "0", txtCodigoSubLinea_AddProd.Text))

            buscarProductos(gvProducto, CStr(ViewState("NomProducto")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CInt(ViewState("CodSublinea")), CInt(dlAlmacen2.SelectedValue), 0, CInt(dlAlmacen.SelectedValue))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click

        Try
            buscarProductos(gvProducto, CStr(ViewState("NomProducto")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CInt(ViewState("CodSublinea")), CInt(dlAlmacen2.SelectedValue), 1, CInt(dlAlmacen.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click

        Try
            buscarProductos(gvProducto, CStr(ViewState("NomProducto")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CInt(ViewState("CodSublinea")), CInt(dlAlmacen2.SelectedValue), 2, CInt(dlAlmacen.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click

        Try
            buscarProductos(gvProducto, CStr(ViewState("NomProducto")), CInt(ViewState("IdLinea")), CInt(ViewState("IdSubLinea")), CInt(ViewState("CodSublinea")), CInt(dlAlmacen2.SelectedValue), 3, CInt(dlAlmacen.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            If CInt(cmbLinea_AddProd.SelectedValue) <> 0 Then
                drop = New Combo
                drop.LlenarCboSubLineaxIdLinea(cmbSubLinea_AddProd, CInt(cmbLinea_AddProd.SelectedValue), True)
            Else
                cmbSubLinea_AddProd.Items.Clear()
                txtCodigoSubLinea_AddProd.Text = ""
                txtDescripcionProd_AddProd.Text = ""
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " onCapa('verBusquedaProducto'); ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAddProductos_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddProductos_AddProd.Click
        Try

            Select Case dlOperacion.SelectedValue
                Case "18" 'entre sucursales
                    ListaDetalleDocumento = getListaDetalleSucursal()
                    For Each fila As GridViewRow In Me.gvListaProducto.Rows
                        Dim cant As Decimal = CDec(CType(fila.Cells(0).FindControl("tbcantFiltro"), TextBox).Text)
                        If cant > 0 Then
                            Dim obj As New Entidades.DetalleDocumento
                            With obj
                                .IdProducto = CInt(fila.Cells(1).Text)
                                .StockDisponibleN = CDec(CType(fila.Cells(1).FindControl("ghdd_stock"), HiddenField).Value) 'origen
                                .NomProducto = CStr(HttpUtility.HtmlDecode(fila.Cells(2).Text))
                                Dim cbo As DropDownList = CType(fila.Cells(3).FindControl("dlmedida"), DropDownList)

                                .UMedida = cbo.SelectedItem.Text
                                .IdUnidadMedida = CInt(cbo.SelectedValue)
                                .IdUnidadMedidaPrincipal = CInt(cbo.SelectedValue)
                                .CantxAtender = 0
                                .Cantidad = cant
                                .Stock = CDec(fila.Cells(4).Text) 'destino
                                .cantSolicitada = 0
                            End With
                            ListaDetalleDocumento.Add(obj)
                        End If
                    Next

                    gvSucursal.DataSource = ListaDetalleDocumento
                    setListaDetalleSucursal(ListaDetalleDocumento)
                    gvSucursal.DataBind()

                Case "19" 'centro distribuidor
                    ListaProductos = getListaProductos(gvProducto)
                    'recorro la cuadricula de la busqueda de productos
                    For Each fila As GridViewRow In Me.gvListaProducto.Rows
                        Dim cant As Decimal = CDec(CType(fila.Cells(0).FindControl("tbcantFiltro"), TextBox).Text)
                        If cant > 0 Then
                            objDetalleDocumentoView = New Entidades.DetalleDocumentoView
                            With objDetalleDocumentoView
                                .IdProducto = CInt(fila.Cells(1).Text)
                                .Descripcion = CStr(HttpUtility.HtmlDecode(fila.Cells(2).Text))
                                .Cantidad = cant
                                Dim lista As New List(Of Entidades.UnidadMedida)
                                Dim cbo As DropDownList = CType(fila.Cells(3).FindControl("dlmedida"), DropDownList)
                                .IdUMedida = CInt(cbo.SelectedValue)
                                .UM = CStr(cbo.SelectedItem.Text)
                                For x As Integer = 0 To cbo.Items.Count - 1
                                    Dim obj As New Entidades.UnidadMedida
                                    obj.DescripcionCorto = CStr(HttpUtility.HtmlDecode(cbo.Items(x).Text))
                                    obj.Id = CInt(cbo.Items(x).Value)
                                    lista.Add(obj)
                                Next
                                .ListaUMedida = lista
                            End With
                            ListaProductos.Add(objDetalleDocumentoView)
                        End If
                    Next
                    gvProducto.DataSource = ListaProductos
                    gvProducto.DataBind()
            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub imgCerrarCapaProducto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCerrarCapaProducto.Click

        Try
            Me.gvListaProducto.DataSource = Nothing
            Me.gvListaProducto.DataBind()
            Me.cmbLinea_AddProd.SelectedIndex = 0
            Me.cmbSubLinea_AddProd.Items.Clear()
            Me.txtDescripcionProd_AddProd.Text = ""
            Me.txtCodigoSubLinea_AddProd.Text = ""
            objScript.offCapa(Me, "verBusquedaProducto")
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvProducto_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvProducto.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cboUM As DropDownList = CType(e.Row.Cells(3).FindControl("dlum"), DropDownList)
                If Me.ListaProductos(e.Row.RowIndex).IdProducto <> 0 Then
                    If Not Me.ListaProductos(e.Row.RowIndex).ListaUMedida Is Nothing Then
                        cboUM.SelectedValue = CStr(Me.ListaProductos(e.Row.RowIndex).IdUMedida)
                    Else
                        Dim Lista As List(Of Entidades.ProductoUMView) = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.ListaProductos(e.Row.RowIndex).IdProducto)
                        For i As Integer = 0 To Lista.Count - 1
                            cboUM.Items.Insert(i, New ListItem(CStr(HttpUtility.HtmlDecode(Lista(i).NombreCortoUM)), CStr(Lista(i).IdUnidadMedida)))
                            cboUM.SelectedValue = CStr(Me.ListaProductos(e.Row.RowIndex).IdUMedida)
                        Next
                    End If
                End If

            End If
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvProducto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvProducto.SelectedIndexChanged
        Try
            ListaProductos = getListaProductos(gvProducto)
            ListaProductos.RemoveAt(gvProducto.SelectedRow.RowIndex)
            gvProducto.DataSource = ListaProductos
            gvProducto.DataBind()
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region

#Region "Seccion de Pedido al centro distribuidor"
    Private Sub llenarCboSemana()
        dlweek.Items.Clear()
        'Dim year% = CInt(String.Format("{0:yyyy}", CDate(tbFechaEmision.Text)))
        Dim idSemana() As String = (New Negocio.DocOrdenPedido).OrdenPedido_ProgramacionPedido( _
                                                                CDate(tbFechaEmision.Text), CInt(dlEmpresa.SelectedValue), _
                                                                CInt(dlTienda.SelectedValue), CInt(dlAlmacen.SelectedValue)).Split(CChar(","))
        If idSemana(0) <> "" Then
            For x As Integer = 0 To idSemana.Length - 1
                dlweek.Items.Add(New ListItem(idSemana(x), idSemana(x)))
            Next
            CargarDatosProgramacionPedido()
        End If

    End Sub

    Private Sub dlweek_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlweek.SelectedIndexChanged
        If CInt(dlweek.SelectedValue) <> 0 Then CargarDatosProgramacionPedido()
    End Sub

    Private Sub CargarDatosProgramacionPedido()
        Dim year% = CInt(String.Format("{0:yyyy}", CDate(tbFechaEmision.Text)))
        objProgramacionPedido = (New Negocio.ProgramacionPedido).ListarProgramacionMantenimiento(year, CInt(dlweek.SelectedValue))
        With objProgramacionPedido
            dlmonth.SelectedValue = CStr(.cal_Mes)
            tbfechaini.Text = .cal_FechaIni.Date.ToShortDateString
            tbfechafin.Text = .cal_FechaFin.Date.ToShortDateString
            tbutilday.Text = CStr(.cal_DiasUtil)
        End With
    End Sub

#End Region

#Region "Seccion de pedido entre sucursales"

    Private Sub btEncontrarDocref_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btEncontrarDocref.Click
        Try
            ViewState.Add("serieref", tbbuscarserieref.Text.Trim)
            ViewState.Add("codigoref", tbbuscarcodigoref.Text.Trim)
            buscarDocRef(0)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



#End Region

    Private Sub buscarDocRef(ByVal tipomov As Integer)
        Dim index% = 0
        Select Case tipomov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(Me.tbpageindex_docref.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(tbpageindex_docref.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(tbpageindexgo_docref.Text) - 1)
        End Select

        ListDocumento = (New Negocio.DocOrdenPedido).findDocumentoRef(CStr(ViewState("serieref")), _
                                                        CStr(ViewState("codigoref")), index, gvdocref.PageSize, _
                                                         CInt(dlTienda.SelectedValue), CInt(dlEmpresa.SelectedValue), CInt(dlAlmacen.SelectedValue))
        gvdocref.DataSource = ListDocumento
        gvdocref.DataBind()
        If ListDocumento.Count > 0 Then
            tbpageindex_docref.Text = CStr(index + 1)
        End If
        objScript.onCapa(Me, "divdocref")
    End Sub

    Private Sub btanterior_docref_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btanterior_docref.Click

        Try
            buscarDocRef(1)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btposterior_docref_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btposterior_docref.Click

        Try
            buscarDocRef(2)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btir_docref_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btir_docref.Click
        Try
            buscarDocRef(3)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumento.Click
        Try
            '*****Para obtener la Cabecera
            Dim objDocumento As Entidades.Documento = (New Negocio.DocOrdenPedido).SelectOrdenPedido(CInt(tbCodigoDocumento.Text), CInt(Me.dlSerie.SelectedValue))
            With objDocumento
                Me.hddIdDocumento.Value = CStr(.Id)
                Me.tbCodigoDocumento.Text = .Codigo
                Me.tbFechaEmision.Text = String.Format(CStr(.FechaEmision), "{0:d}")
                Me.dlEstadoDocumento.SelectedValue = CStr(.IdEstadoDoc)
                Me.dlEmpresa.SelectedValue = CStr(.IdEmpresa)
                Me.dlTienda.SelectedValue = CStr(.IdTienda)
                Me.dlAlmacen.SelectedValue = CStr(.IdAlmacen)

                dlOperacion.SelectedValue = CStr(.IdTipoOperacion)
                verPanel()

                Dim objEntPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(.Id)
                With objEntPuntoPartida
                    Me.dlTienda2.SelectedValue = CStr(.IdTienda)
                    Me.dlTienda2_SelectedIndexChanged(sender, e)
                    Me.dlAlmacen2.SelectedValue = CStr(.IdAlmacen)
                    hddIdAlmacen.Value = Me.dlAlmacen2.SelectedValue
                End With

                Select Case .IdTipoOperacion
                    Case 18  'Pedido entre sucursales
                        pnlDocRef.Visible = True
                        Me.hdd_iddocumentoref.Value = CStr(.IdDocRelacionado)
                        Me.tbcodigoref.Text = .NomTipoDocumento 'Codigo ref 
                        Me.tbserieref.Text = .NomTipoOperacion  'Serie ref 
                        Me.tbdocumento.Text = .NomEmpleado  'Tipodocumento
                        Me.tbclienteref.Text = .NomEmpresaTomaInv
                        Me.hdd_estadoentrega.Value = CStr(.IdEstadoEntrega)

                        ListaDetalleDocumento = (New Negocio.DocOrdenPedido).OrdenPedidoxSelectDetalleEntresucursales(.Id, CInt(dlAlmacen2.SelectedValue))
                        gvSucursal.DataSource = ListaDetalleDocumento
                        setListaDetalleSucursal(ListaDetalleDocumento)
                        gvSucursal.DataBind()


                    Case 19  'Pedido al centro distribuidor
                        pnlProgPedido.Visible = True
                        pnlProgPedido.Visible = True
                        objProgramacionPedido = (New Negocio.ProgramacionPedido).ListarDocProgPedidoxIdDocumento(.Id)
                        With objProgramacionPedido
                            dlweek.Items.Clear()
                            Dim year% = CInt(String.Format("{0:yyyy}", CDate(tbFechaEmision.Text)))
                            Dim idSemana() As String = .CadIdsemana.Split(CChar(","))
                            If idSemana(0) <> "" Then
                                For x As Integer = 0 To idSemana.Length - 1
                                    dlweek.Items.Add(New ListItem(idSemana(x), idSemana(x)))
                                Next
                            End If
                            dlweek.Items.Insert(0, New ListItem("-Selecconar-", "0"))
                            dlweek.SelectedValue = CStr(.IdSemana)
                            dlmonth.SelectedValue = CStr(.cal_Mes)
                            tbfechaini.Text = .cal_FechaIni.Date.ToShortDateString
                            tbfechafin.Text = .cal_FechaFin.Date.ToShortDateString
                            tbutilday.Text = CStr(.cal_DiasUtil)
                        End With

                        ListaProductos = (New Negocio.DetalleDocumento).DetalleDocumentoViewSelectxIdDocumento(CInt(Me.hddIdDocumento.Value))
                        gvProducto.DataSource = ListaProductos
                        gvProducto.DataBind()
                End Select

                Dim objObs As Entidades.Observacion = (New Negocio.OrdenCompra).listarObservacionDocumento(.Id)
                txtobservacion.Text = objObs.Observacion
                hddidObs.Value = CStr(objObs.Id)

            End With

            MostrarBotones("DocumentoEncontrado")
            hdd_operativo.Value = CStr(operativo.DocumentoEncontrado)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvdocref_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvdocref.SelectedIndexChanged
        hdd_iddocumentoref.Value = CType(gvdocref.SelectedRow.Cells(1).FindControl("hddiddocumentoref"), HiddenField).Value

        tbserieref.Text = gvdocref.SelectedRow.Cells(2).Text
        tbcodigoref.Text = gvdocref.SelectedRow.Cells(3).Text
        tbdocumento.Text = gvdocref.SelectedRow.Cells(4).Text
        tbclienteref.Text = gvdocref.SelectedRow.Cells(6).Text
        hdd_idtiendavalidar.Value = dlTienda.SelectedValue

        ListaDetalleDocumento = (New Negocio.DocOrdenPedido).OrdenPedidoxSelectxIdDocumento(CInt(hdd_iddocumentoref.Value), _
                                                                                            CInt(dlAlmacen.SelectedValue), CInt(dlAlmacen2.SelectedValue))
        gvSucursal.DataSource = ListaDetalleDocumento
        setListaDetalleSucursal(ListaDetalleDocumento)
        gvSucursal.DataBind()

    End Sub

    Protected Sub dlAlmacen2_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlAlmacen2.SelectedIndexChanged
        Try
            If dlAlmacen.SelectedValue = dlAlmacen2.SelectedValue Then
                objScript.mostrarMsjAlerta(Me, "No puede seleccionar el mismo almacen") : dlAlmacen2.SelectedIndex = 0 : Exit Sub
            End If
            If gvSucursal.Rows.Count > 0 Then
                ListaDetalleDocumento = getListaDetalleSucursal()
                For x As Integer = 0 To ListaDetalleDocumento.Count - 1
                    ListaDetalleDocumento(x).Stock = (New Negocio.DocOrdenPedido).OrdenPedidoConsultarStock(CInt(dlAlmacen2.SelectedValue), ListaDetalleDocumento(x).IdProducto)
                Next
                gvSucursal.DataSource = ListaDetalleDocumento
                setListaDetalleSucursal(ListaDetalleDocumento)
                gvSucursal.DataBind()
            End If
            If gvProducto.Rows.Count > 0 Then
                gvProducto.DataBind()
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvSucursal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSucursal.SelectedIndexChanged
        ListaDetalleDocumento = getListaDetalleSucursal()
        ListaDetalleDocumento.RemoveAt(gvSucursal.SelectedIndex)
        gvSucursal.DataSource = ListaDetalleDocumento
        gvSucursal.DataBind()
    End Sub

    Protected Sub imgConsultar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim row As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
        Dim hdd As HiddenField = CType(row.Cells(1).FindControl("hddiddocumentoref"), HiddenField)
        Dim Lista As List(Of Entidades.Documento) = (New Negocio.DocOrdenPedido).OrdenPedidoConsultarRelacionDocumento(CInt(hdd.Value))
        If Lista.Count = 0 Then
            Dim msj$ = "No existe una orden de pedido relacionado a esta " + row.Cells(4).Text + "."
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " onCapa('divdocref'); alert('" + msj + "');", True)
        Else
            gvRelacionDocumento.DataSource = Lista
            gvRelacionDocumento.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " onCapa('divdocref'); onCapa2('capaRelacionDocumento');", True)
        End If
    End Sub

End Class