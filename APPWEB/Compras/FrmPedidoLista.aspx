<%@ Page Language="vb" AutoEventWireup="false"      MasterPageFile="~/Principal.Master"          CodeBehind="FrmPedidoLista.aspx.vb" Inherits="APPWEB.FrmPedidoLista1" 
    title="SIGE v1.0 - Consulta de Pedidos de Compra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <table style="width: 800px">
            <tr>
                <td style="width: 800px">
                    <asp:Menu ID="Menu1" runat="server" BackColor="Control" DynamicHorizontalOffset="2"
                        Font-Bold="False" Font-Italic="False" Font-Names="Verdana" Font-Size="8pt" ForeColor="DarkRed"
                        Orientation="Horizontal" StaticSubMenuIndent="10px" Style="vertical-align: top">
                        <StaticSelectedStyle BackColor="#5D7B9D" />
                        <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                        <DynamicHoverStyle BackColor="Peru" ForeColor="White" />
                        <DynamicMenuStyle BackColor="#F7F6F3" />
                        <DynamicSelectedStyle BackColor="SteelBlue" />
                        <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                        <StaticHoverStyle BackColor="#7C6F57" ForeColor="White" />
                        <Items>
                            <asp:MenuItem Text="Areas" Value="Areas">
                                <asp:MenuItem NavigateUrl="~/Ventas.aspx" Text="Ventas" Value="Ventas"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/Caja/frmCaja.aspx" Text="Caja" Value="Caja"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/Almacen.aspx" Text="Almac&#233;n" Value="Almac&#233;n">
                                </asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/Compras.aspx" Text="Compras" Value="Compras"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/frmAdministracionSistema.aspx" Text="Administraci&#243;n de Sistema"
                                    Value="Administraci&#243;n de Sistema"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/frmInformacionGerencial.aspx" Text="Informaci&#243;n Gerencial"
                                    Value="Informaci&#243;n Gerencial"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/Principal.aspx" Text="Principal" Value="Principal"></asp:MenuItem>
                            </asp:MenuItem>
                            <asp:MenuItem Text="Mantenimientos" Value="Mantenimientos">
                                <asp:MenuItem NavigateUrl="~/Compras/frmProvedoresLista.aspx" Text="Proveedores"
                                    Value="Clientes"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/Mantenedores/frmProductosLista.aspx" Text="Productos"
                                    Value="Prodcutos"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/Mantenedores/FrmMantTablasGenerales.aspx" Text="Tablas Generales"
                                    Value="Tablas Generales"></asp:MenuItem>
                            </asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/Compras/frmOrdenCompraLista.aspx" Text="Orden de Compra"
                                Value="Orden de Compra"></asp:MenuItem>
                            <asp:MenuItem NavigateUrl="~/Compras/FrmPedidoLista.aspx" Text="Pedidos al Proveedor"
                                Value="Consultas"></asp:MenuItem>
                            <asp:MenuItem Text="Compras Por Proveedor" Value="Compras Por Proveedor"></asp:MenuItem>
                            <asp:MenuItem Text="Consulta de Stocks" Value="Consulta de Stocks"></asp:MenuItem>
                        </Items>
                    </asp:Menu>
                </td>
            </tr>
            <tr>
                <td style="height: 10px; text-align: center; width: 800px;">
                    &nbsp;<asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Names="Tahoma"
                        Text="Consulta de Pedidos de Compra" Width="266px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 800px">
                    <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Imagenes/Imprimir_B.JPG" /></td>
            </tr>
            <tr>
                <td style="width: 800px">
                    <asp:Label ID="Label1" runat="server" Text="Filtrar Por:" Width="75px"></asp:Label>
                    <asp:DropDownList ID="ddl_Filtrar" runat="server" Width="133px">
                    </asp:DropDownList>
                    <asp:TextBox ID="txt_Filtro" runat="server" Visible="False"></asp:TextBox>
                    <asp:TextBox ID="txt_FechaFin" runat="server" Visible="False"></asp:TextBox>
                    <asp:DropDownList ID="ddl_Estado" runat="server" Visible="False">
                    </asp:DropDownList>
                    <asp:ImageButton ID="cmd_AplicaFiltro" runat="server" ImageUrl="~/Imagenes/Ok_b.bmp" Visible="False" /></td>
            </tr>
            <tr>
                <td style="width: 800px; height: 231px">
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        CellPadding="4" ForeColor="#333333"
                        GridLines="None" PageSize="5" Width="797px">
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <EditRowStyle BackColor="#999999" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="width: 800px; text-align: center">
                    <asp:Label ID="Label2" runat="server" Font-Names="Tahoma" ForeColor="Black" Text="Detalle:"
                        Width="56px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 800px">
                    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" 
                        AutoGenerateColumns="False" CellPadding="4" GridLines="None" Width="797px" 
                        ForeColor="#333333" Height="279px">
                        <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#999999" />
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="width: 800px">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 800px">
                    &nbsp;</td>
            </tr>
        </table>
    
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
