﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmOrdenCompraLista.aspx.vb" Inherits="APPWEB.frmOrdenCompraLista"
    Title="SIGE v1.0 - Gestión de Ordenes de Compra" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Consulta de Ordenes de Compra
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto">
                            Filtro:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_OrdenCompra" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="0">Todas las Ordenes de Compra</asp:ListItem>
                                <asp:ListItem Value="1">R.U.C.</asp:ListItem>
                                <asp:ListItem Value="2">Razón Social</asp:ListItem>
                                <asp:ListItem Value="3">Rango de Fechas</asp:ListItem>
                                <asp:ListItem Value="4">Estado</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_FiltroOc" runat="server" Visible="False"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_FechaIni" runat="server" Visible="false" Width="90px"></asp:TextBox>
                            <ajax:CalendarExtender ID="calendarIni" runat="server" Format="d" TargetControlID="txt_FechaIni">
                            </ajax:CalendarExtender>
                        </td>
                        <td>
                            <asp:TextBox ID="txt_FechaFin" runat="server" Visible="False" Width="90px"></asp:TextBox>
                            <ajax:CalendarExtender ID="calendarFin" runat="server" Format="d" TargetControlID="txt_FechaFin">
                            </ajax:CalendarExtender>
                        </td>
                        <td>
                            <asp:ImageButton ID="cmd_AplicaFiltroOc" runat="server" ImageUrl="~/Imagenes/Ok_b.bmp"
                                ToolTip="Aplicar Filtro" OnClientClick="return(validarAceptar());" />
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Estado Entrega:
                        </td>
                        <td>
                            <asp:DropDownList ID="dlestEntrega" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Estado Cancelaci&oacute;n:
                        </td>
                        <td>
                            <asp:DropDownList ID="dlestCancelacion" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="gvOrdenCompra" runat="server" PageSize="20" AutoGenerateColumns="False"
                    HeaderStyle-Height="25px" RowStyle-Height="25px" Width="100%">
                    <RowStyle CssClass="GrillaRow" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <HeaderStyle CssClass="GrillaCabecera" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <Columns>
                        <asp:BoundField DataField="Serie" HeaderText="Serie" />
                        <asp:BoundField DataField="Codigo" HeaderText="Numero" />
                        <asp:BoundField DataField="FechaEmision" HeaderText="Fecha Emision" />
                        <asp:BoundField DataField="FechaAEntregar" HeaderText="Fecha A Entregar" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="NomMoneda" HeaderText="Moneda" />
                        <asp:BoundField DataField="TotalAPagar" HeaderText="Total A Pagar" />
                        <asp:BoundField DataField="RazonSocial" HeaderText="Razon Social" />
                        <asp:BoundField DataField="RUC" HeaderText="R.U.C." />
                        <asp:BoundField DataField="NomPropietario" HeaderText="Contacto" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                    ToolTip="Página Anterior" OnClientClick="return(valNavegacion('0'));" />
                <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                    ToolTip="Página Posterior" OnClientClick="return(valNavegacion('1'));" />
                <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                    runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                        Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion('2'));" />
                <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                    onKeyPress="return(onKeyPressEsNumero());"></asp:TextBox>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        ///++++++
        function validarAceptar() {
            var dl = document.getElementById('<%=ddl_OrdenCompra.ClientID %>');
            var dateini = document.getElementById('<%=txt_FechaIni.ClientID %>');
            var datefin = document.getElementById('<%=txt_FechaFin.ClientID %>');
            if (dl.value == '3') {
                if (validarFecha(dateini, 'de inicio') == false) {
                    return false;
                }
                if (validarFecha(datefin, 'de Fin') == false) {
                    return false;
                }

                if (compararFechas(dateini, datefin)) {
                    alert('la fecha de inicio no puede ser menor a la fecha de fin');
                    return false;
                }

            }
            return confirm('Desea continuar con la operacion ?');
        }
        //+++
        function validarFecha(oTxt, mensaje) {
            var bOk = true;
            if (oTxt.value != "") {
                bOk = bOk && (valAno(oTxt));
                bOk = bOk && (valMes(oTxt));
                bOk = bOk && (valDia(oTxt));
                bOk = bOk && (valSep(oTxt));
                if (!bOk) {
                    alert("Fecha inválida (dd/mm/yyyy).");
                    oTxt.select();
                    oTxt.focus();
                    return false;
                }
            } else {
                alert("Ingrese una Fecha de " + mensaje + " . (dd/mm/yyyy).");
                oTxt.select();
                oTxt.focus();
                return false;
            }
            return true;
        }
        ///+++++
        function compararFechas(fecha, fecha2) {
            var xMonth = fecha.value.substring(3, 5);
            var xDay = fecha.value.substring(0, 2);
            var xYear = fecha.value.substring(6, 10);
            var yMonth = fecha2.value.substring(3, 5);
            var yDay = fecha2.value.substring(0, 2);
            var yYear = fecha2.value.substring(6, 10);

            if (xYear > yYear) {
                return (true)
            }
            else {
                if (xYear == yYear) {
                    if (xMonth > yMonth) {
                        return (true)
                    }
                    else {
                        if (xMonth == yMonth) {
                            if (xDay > yDay)
                                return (true);
                            else
                                return (false);
                        }
                        else
                            return (false);
                    }
                }
                else
                    return (false);
            }
        }
    </script>

</asp:Content>
