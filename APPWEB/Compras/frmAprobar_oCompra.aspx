﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmAprobar_oCompra.aspx.vb" Inherits="APPWEB.frmAprobar_oCompra" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                ORDEN DE COMPRA - APROBACI&Oacute;N
            </td>
        </tr>
    </table>
    <center>
        <table>
            <tr>
                <td class="Texto" style="font-weight: bold">
                    Empresa:
                </td>
                <td>
                    <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
                <td class="Texto" style="font-weight: bold">
                    Tienda:
                </td>
                <td>
                    <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
                <td class="Texto" style="font-weight: bold">
                    Serie:
                </td>
                <td>
                    <asp:DropDownList ID="cboSerie" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="70px" ToolTip="Buscar" UseSubmitBehavior="false"
                        Style="cursor: hand;" CssClass="btnBuscar" OnClientClick="this.disabled=true;this.value='Procesando...'"  />
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td class="Texto" style="font-weight: bold">
                    Área:
                </td>
                <td>
                    <asp:DropDownList ID="cboArea" runat="server">
                    </asp:DropDownList>
                </td>
                <td class="Texto" style="font-weight: bold">
                    Aprobación:
                </td>
                <td>
                    <asp:DropDownList ID="cboOpcion_Aprobacion" runat="server">
                        <asp:ListItem Value="0" Selected="True">Por Aprobar</asp:ListItem>
                        <asp:ListItem Value="1">Aprobados</asp:ListItem>
                        <asp:ListItem Value="2">Todos</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td class="Texto" style="font-weight: bold">
                    Fecha Inicio:
                </td>
                <td>
                    <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                        Width="100px"></asp:TextBox>
                    <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender0" runat="server" ClearMaskOnLostFocus="false"
                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                        CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                    </cc1:MaskedEditExtender>
                    <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Enabled="True"
                        Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                    </cc1:CalendarExtender>
                </td>
                <td class="Texto" style="font-weight: bold">
                    Fecha Fin:
                </td>
                <td>
                    <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                        Width="100px"></asp:TextBox>
                    <cc1:MaskedEditExtender ID="txtFechaFin_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                        CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin">
                    </cc1:MaskedEditExtender>
                    <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Enabled="True"
                        Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                    </cc1:CalendarExtender>
                </td>
            </tr>
        </table>
    </center>
    <br />
    <asp:GridView ID="GV_OC" runat="server" AutoGenerateColumns="False" Width="100%">
        <HeaderStyle CssClass="GrillaHeader" />
        <Columns>
            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                ItemStyle-HorizontalAlign="Center" SelectText="Aprobar" ShowSelectButton="True">
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:CommandField>
            <asp:BoundField DataField="NomTipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                ItemStyle-HorizontalAlign="Center">
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center" Font-Bold="True" ForeColor="Red" Height="25px">
                </ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="True" ForeColor="Red" Text='<%# DataBinder.Eval(Container.DataItem,"NroDocumento") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:BoundField DataField="getFechaEmisionText" DataFormatString="{0:dd/MM/yyyy}"
                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Emisión"
                ItemStyle-HorizontalAlign="Center">
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="getFechaCancelacionText" DataFormatString="{0:dd/MM/yyyy}"
                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Canc."
                ItemStyle-HorizontalAlign="Center">
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
              <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}"
                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Venc."
                ItemStyle-HorizontalAlign="Center">
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            
            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                HeaderText="Monto" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblMoneda_Monto" runat="server" Font-Bold="True" Text='<%# DataBinder.Eval(Container.DataItem,"NomMoneda") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblMonto" runat="server" Font-Bold="True" Text='<%# DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}") %>'></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:BoundField DataField="DescripcionPersona" ItemStyle-Font-Bold="true" HeaderStyle-Height="25px"
                HeaderStyle-HorizontalAlign="Center" HeaderText="Proveedor" ItemStyle-HorizontalAlign="Center">
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center" Font-Bold="True"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                HeaderText="Aprobado" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chb_Aprobado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Aprobar") %>'
                                    Enabled="False" />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:BoundField DataField="getFechaAprobacionText" NullDisplayText="" HeaderStyle-Height="25px"
                HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha / Hora" ItemStyle-HorizontalAlign="Center">
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                HeaderText="" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnMostrarDetalle" runat="server" ImageUrl="~/Imagenes/search_add.ico"
                                    OnClientClick=" return( valOnClick_btnMostrarDetalle(this)  ); " ToolTip="Visualizar Detalle"
                                    Width="20px" />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            
              <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                HeaderText="" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:Button id="btnGenerarAutomatico" runat="server" Text="Generar R/Q" CommandArgument='<%# Container.DataItemIndex%>'  CommandName="RegistrarRQ"/>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            
            
                <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                HeaderText="" ItemStyle-HorizontalAlign="Center" Visible="false" >
                <ItemTemplate>
                    <table>
                        <tr>
                            <td>
                                  <asp:Label ID="lblIdDocRelacionado" runat="server" Font-Bold="True" Text='<%# DataBinder.Eval(Container.DataItem,"IdDocRelacionado") %>'></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            
        </Columns>
        <FooterStyle CssClass="GrillaFooter" />
        <PagerStyle CssClass="GrillaPager" />
        <RowStyle CssClass="GrillaRow" />
        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
        <SelectedRowStyle CssClass="GrillaSelectedRow" />
        <EditRowStyle CssClass="GrillaEditRow" />
    </asp:GridView>
    
    
    <table width="100%" visible="false">
            <tr>
                            <td align="right" class="Texto" colspan="4">
                                <asp:GridView ID="GV_CuentaBancaria" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chb_Select_CB" runat="server" onClick=" return(  valOnClick_chb_Select_CB(this)  ); " />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Banco" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdCuentaBancaria" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdCuentaBancaria") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblBanco" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Banco") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdBanco" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdBanco") %>' />
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Nro. Cuenta" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblNroCuentaBancaria" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NroCuentaBancaria") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Nro. Cuenta Interbancaria" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                       
                                                        <td>
                                                            <asp:Label ID="lblcuentainterbanca" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"cb_CuentaInterbancaria") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                         <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Swift" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblswift" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"cb_SwiftBancario") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="GrillaRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaCabecera" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
    </table>
    <asp:HiddenField ID="hddIdDocumento" runat="server" Value="0" />
    <asp:HiddenField ID="hddIdUsuario" runat="server" Value="0" />
    <%--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++--capaAprobarOC--+--%>
    <div id="capaAprobarOC" style="border: 3px solid blue; padding: 8px; width: 680px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 160px; left: 150px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAprobarReqGasto" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaAprobarOC')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_Aprobacion" runat="server">
                        <table width="100%">
                            <tr>
                                <td class="SubTituloCelda">
                                    DATOS DE APROBACIÓN
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:RadioButtonList ID="rdbAutorizado" runat="server" RepeatDirection="Horizontal"
                                                    TextAlign="Right" CssClass="Texto" Font-Bold="true" AutoPostBack="True">
                                                    <asp:ListItem Value="1">Sí</asp:ListItem>
                                                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td style="width: 30px">
                                            </td>
                                            <td class="Texto" style="font-weight: bold">
                                                Fecha Aprobación:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaAprobacion" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                                    Width="180px" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 30px">
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chbEnviarCxP" runat="server" Text="Enviar a Cuenta por Pagar" CssClass="Texto"
                                                    Font-Bold="true" Enabled="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold" colspan="3">
                                                F. Cancelaci&oacute;n:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtfechaVencimiento" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtfechaVencimiento" runat="server"
                                                    ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                    CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtfechaVencimiento">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender_txtfechaVencimiento" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="txtfechaVencimiento">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="70px" ToolTip="Guardar"
                                    OnClientClick="return(  valOnClick_btnGuardar()  );" />
                            </td>
                            <td>
                                <asp:Button ID="btnCerrar" runat="server" Text="Cerrar" Width="70px" ToolTip="Cerrar"
                                    OnClientClick=" return( offCapa('capaAprobarReqGasto') ); " />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>



        
        
    <div id="capaRQ" style="border: 3px solid blue; padding: 8px; width: 790px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 160px; left: 150px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaRQ')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="PnlRQ" runat="server">
                        <table width="100%">
                            <tr>
                                <td class="SubTituloCelda">
                                    REGISTRO AUTOMÁTICO de REQUERIMIENTOS DE GASTO
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Empresa:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboEmpresaRQ" runat="server" ></asp:DropDownList>
                                            </td>
                                            <td class="Texto" style="font-weight: bold">
                                            Tienda:
                                            </td>
                                            <td class="TextBox_ReadOnly" style="font-weight: bold">
                                            <asp:DropDownList ID="cboTiendaRQ" runat="server"  Enabled="false" ></asp:DropDownList>
                                            </td>
                                            <td class="Texto" style="font-weight: bold">
                                               Serie:
                                            </td>
                                            <td >
                                               <asp:DropDownList ID="cboserieRQ" runat="server" AutoPostBack="true" ></asp:DropDownList>
                                            </td>
                                            <td class="Texto" style="font-weight: bold">
                                            Código:
                                            </td>
                                            <td class="TextBox_ReadOnly" style="font-weight: bold">
                                            <asp:TextBox ID="txtcodigoRQ" runat="server" Enabled="false" Text=" " ></asp:TextBox>
                                            </td>
                                            
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnGuardarRQ" runat="server" Text="Guardar" Width="70px" ToolTip="Guardar"/>
                            </td>
                            <td>
                                <asp:Button ID="btnCerrarRQ" runat="server" Text="Cerrar" Width="70px" ToolTip="Cerrar"
                                    OnClientClick=" return( offCapa('capaRQ') ); " />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>    
        
    <asp:HiddenField ID="hddIdPersona" runat="server" Value="0"/>
       <asp:HiddenField ID="hddIdMoneda" runat="server" Value="0"/> 
      <asp:HiddenField ID="hddTotal" runat="server" Value="0"/>
       <asp:HiddenField ID="hddIdDocumentoOC" runat="server" Value="0"/>
    <script language="javascript" type="text/javascript">

        function valOnClick_btnGuardar() {
            return confirm('Desea continuar con la Operación ?');
        }

        function valOnClick_btnMostrarDetalle(control) {

            var grilla = document.getElementById('<%=GV_OC.ClientID%>');
            var IdDocumento = 0;
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var boton = rowElem.cells[9].children[0].cells[0].children[0];
                    var hddIdDocumento = rowElem.cells[2].children[0].cells[1].children[0];

                    if (control.id == boton.id) {
                        IdDocumento = parseInt(hddIdDocumento.value);
                        break;
                    }


                }
            }

            window.open('frmOrdenCompra1.aspx?IdDocumento=' + IdDocumento, null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }
        
    </script>

</asp:Content>
