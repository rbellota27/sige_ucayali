<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/Principal.Master"   CodeBehind="frmProvedoresDetalle.aspx.vb" Inherits="APPWEB.ProveedoresDetalle" 
    title="SIGE v1.0 - Detalle de Proveedor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    &nbsp;<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Menu ID="Menu1" runat="server" BackColor="Control" DynamicHorizontalOffset="2"
                Font-Bold="False" Font-Italic="False" Font-Names="Verdana" Font-Size="8pt" ForeColor="DarkRed"
                Orientation="Horizontal" StaticSubMenuIndent="10px" Style="vertical-align: top">
                <StaticSelectedStyle BackColor="#5D7B9D" />
                <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                <DynamicHoverStyle BackColor="Peru" ForeColor="White" />
                <DynamicMenuStyle BackColor="#F7F6F3" />
                <DynamicSelectedStyle BackColor="SteelBlue" />
                <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                <StaticHoverStyle BackColor="#7C6F57" ForeColor="White" />
                <Items>
                    <asp:MenuItem Text="Areas" Value="Areas">
                        <asp:MenuItem NavigateUrl="~/Ventas.aspx" Text="Ventas" Value="Ventas"></asp:MenuItem>
                        <asp:MenuItem NavigateUrl="~/Caja/frmCaja.aspx" Text="Caja" Value="Caja"></asp:MenuItem>
                        <asp:MenuItem NavigateUrl="~/Almacen.aspx" Text="Almac&#233;n" Value="Almac&#233;n">
                        </asp:MenuItem>
                        <asp:MenuItem NavigateUrl="~/Compras.aspx" Text="Compras" Value="Compras"></asp:MenuItem>
                        <asp:MenuItem NavigateUrl="~/frmAdministracionSistema.aspx" Text="Administraci&#243;n de Sistema"
                            Value="Administraci&#243;n de Sistema"></asp:MenuItem>
                        <asp:MenuItem NavigateUrl="~/frmInformacionGerencial.aspx" Text="Informaci&#243;n Gerencial"
                            Value="Informaci&#243;n Gerencial"></asp:MenuItem>
                        <asp:MenuItem NavigateUrl="~/Principal.aspx" Text="Principal" Value="Principal"></asp:MenuItem>
                    </asp:MenuItem>
                    <asp:MenuItem Text="Mantenimientos" Value="Mantenimientos">
                        <asp:MenuItem NavigateUrl="~/Compras/frmProvedoresLista.aspx" Text="Proveedores"
                            Value="Clientes"></asp:MenuItem>
                        <asp:MenuItem NavigateUrl="~/Mantenedores/frmProductosLista.aspx" Text="Productos"
                            Value="Prodcutos"></asp:MenuItem>
                        <asp:MenuItem NavigateUrl="~/Mantenedores/FrmMantTablasGenerales.aspx" Text="Tablas Generales"
                            Value="Tablas Generales"></asp:MenuItem>
                    </asp:MenuItem>
                    <asp:MenuItem NavigateUrl="~/Compras/frmOrdenCompraLista.aspx" Text="Orden de Compra"
                        Value="Orden de Compra"></asp:MenuItem>
                    <asp:MenuItem NavigateUrl="~/Compras/FrmPedidoLista.aspx" Text="Pedidos al Proveedor"
                        Value="Consultas"></asp:MenuItem>
                    <asp:MenuItem Text="Compras Por Proveedor" Value="Compras Por Proveedor"></asp:MenuItem>
                    <asp:MenuItem Text="Consulta de Stocks" Value="Consulta de Stocks"></asp:MenuItem>
                </Items>
            </asp:Menu>
            <table cellpadding="0" cellspacing="0" style="width: 97%; ">
                <tr>
                    <td style="width: 42px; ">
                        <asp:ImageButton ID="cmd_EditarOcD" runat="server" 
                            ImageUrl="~/Imagenes/Editar_B.JPG" 
                            onmouseout="this.src='/Imagenes/Editar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Editar_A.JPG';" ToolTip="Editar" />
                    </td>
                    <td style="width: 30px; ">
                        <asp:ImageButton ID="cmd_NuevoOcD" runat="server" 
                            ImageUrl="~/Imagenes/Nuevo_B.JPG" 
                            onmouseout="this.src='/Imagenes/Nuevo_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" ToolTip="Nuevo" />
                    </td>
                    <td style="width: 49px; ">
                        <asp:ImageButton ID="cmd_BuscarOcD" runat="server" 
                            ImageUrl="~/Imagenes/Buscar_b.JPG" 
                            onmouseout="this.src='/Imagenes/Buscar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" ToolTip="Buscar" />
                    </td>
                    <td style="width: 19px; ">
                        <asp:ImageButton ID="cmd_EliminarOcD" runat="server" 
                            ImageUrl="~/Imagenes/Anular_B.JPG" 
                            onmouseout="this.src='/Imagenes/Anular_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Anular_A.JPG';" ToolTip="Eliminar" />
                    </td>
                    <td style="width: 22px; height: 4px">
                        <asp:ImageButton ID="cmd_ImprimirOcD" runat="server" cToolTip="Imprimir" 
                            ImageUrl="~/Imagenes/Imprimir_B.JPG" 
                            onmouseout="this.src='/Imagenes/Imprimir_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Imprimir_A.JPG';" />
                    </td>
                    <td style="text-align: right">
                        <asp:ImageButton ID="cmd_Regresar" runat="server" 
                            ImageUrl="~/Imagenes/Arriba_B.JPG"
                            onmouseout="this.src='/Imagenes/Arriba_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Arriba_A.JPG';"                             
                            PostBackUrl="~/Compras/frmProvedoresLista.aspx" 
                            ToolTip="Regresar a Lista de Ordenes de Compra" />
                    </td>
                </tr>
            </table>
            <table style="width: 675px">
                <tr>
                    <td style="text-align: center">
                        <asp:Label ID="Label9" runat="server" Text="Detalle de Proveedor" Font-Bold="True" Font-Names="Tahoma" Font-Size="14pt"></asp:Label></td>
                </tr>
            </table>
            <!-- Aqui Insertar los campos de captura de datos.... INICIO -->
            
<table border="0" cellspacing="0" cellpadding="0"><tr><td style="width: 850px"><!--$end exclude$--><table style="table-layout:fixed;border:0px none #000000" width=713 height=408 border=0 cellspacing=0 cellpadding=0>
    <colgroup>
        <col width="713">
        <tr>
            <td height="408" style="border:0px none #000000" valign="top" width="713">
                <table border="0" cellpadding="0" cellspacing="0" height="408" width="713">
                    <colgroup>
                        <col width="37">
                        <col width="1">
                        <col width="1">
                        <col width="3">
                        <col width="4">
                        <col width="37">
                        <col width="2">
                        <col width="9">
                        <col width="2">
                        <col width="5">
                        <col width="5">
                        <col width="2">
                        <col width="6">
                        <col width="7">
                        <col width="1">
                        <col width="2">
                        <col width="2">
                        <col width="1">
                        <col width="4">
                        <col width="3">
                        <col width="16">
                        <col width="2">
                        <col width="9">
                        <col width="14">
                        <col width="5">
                        <col width="30">
                        <col width="14">
                        <col width="34">
                        <col width="5">
                        <col width="49">
                        <col width="7">
                        <col width="2">
                        <col width="2">
                        <col width="12">
                        <col width="38">
                        <col width="1">
                        <col width="2">
                        <col width="23">
                        <col width="5">
                        <col width="1">
                        <col width="2">
                        <col width="3">
                        <col width="29">
                        <col width="2">
                        <col width="9">
                        <col width="2">
                        <col width="6">
                        <col width="8">
                        <col width="2">
                        <col width="10">
                        <col width="53">
                        <col width="9">
                        <col width="6">
                        <col width="4">
                        <col width="14">
                        <col width="13">
                        <col width="3">
                        <col width="10">
                        <col width="13">
                        <col width="9">
                        <col width="10">
                        <col width="44">
                        <col width="10">
                        <col width="2">
                        <col width="5">
                        <col width="1">
                        <col width="3">
                        <col width="26">
                        <col width="0">
                        <!-- workaround for IE table layout bug -->
                        <tr>
                            <td colspan="68" height="11" width="713">
                            </td>
                            <td height="11">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="40" height="1" width="405">
                            </td>
                            <td colspan="26" height="26" rowspan="5" valign="top" width="279">
                                <asp:TextBox ID="txt_RazonSocial" runat="server" size="38">Construccion y Administraci�n S.A.</asp:TextBox>
                            </td>
                            <td colspan="2" height="1" width="29">
                            </td>
                            <td height="1">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="11" height="3" width="106">
                            </td>
                            <td colspan="16" height="26" rowspan="5" valign="top" width="118">
                                <asp:TextBox ID="txt_Ruc" runat="server" size="15">20109565017</asp:TextBox>
                            </td>
                            <td colspan="13" height="3" width="181">
                            </td>
                            <td colspan="2" height="3" width="29">
                            </td>
                            <td height="3">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="11" height="2" width="106">
                            </td>
                            <td colspan="2" height="2" width="39">
                            </td>
                            <td colspan="9" height="20" nowrap rowspan="2" valign="top" width="136">
                                <span class="text"><span style="font-size:12px;line-height:15px;">
                                <span style="font-family: Tahoma">Nombre o Raz�n Social:</span><br soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="2" height="2" width="6">
                            </td>
                            <td colspan="2" height="2" width="29">
                            </td>
                            <td height="2">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="3" height="18" width="39">
                            </td>
                            <td colspan="6" height="18" nowrap valign="top" width="57">
                                <span class="text"><span style="font-size:12px;line-height:15px;">
                                <span style="font-family: Tahoma">R.U.C.:</span><br soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="2" height="18" width="10">
                            </td>
                            <td colspan="2" height="18" width="39">
                            </td>
                            <td colspan="2" height="18" width="6">
                            </td>
                            <td colspan="2" height="18" width="29">
                            </td>
                            <td height="18">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="11" height="2" width="106">
                            </td>
                            <td colspan="13" height="2" width="181">
                            </td>
                            <td colspan="2" height="2" width="29">
                            </td>
                            <td height="2">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="11" height="1" width="106">
                            </td>
                            <td colspan="41" height="1" width="489">
                            </td>
                            <td height="1">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="40" height="2" width="405">
                            </td>
                            <td align="center" colspan="9" height="15" nowrap rowspan="3" valign="top" 
                                width="63">
                                <span class="text"><span style="font-size:12px;line-height:15px;">
                                <span style="font-family: Tahoma">N� Interior:</span><br soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="19" height="2" width="245">
                            </td>
                            <td height="2">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="40" height="1" width="405">
                            </td>
                            <td height="1" width="10">
                            </td>
                            <td align="center" colspan="2" height="15" nowrap rowspan="3" valign="top" 
                                width="62">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">Etapa:<br 
                                    soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="9" height="1" width="82">
                            </td>
                            <td align="center" colspan="5" height="15" nowrap rowspan="3" valign="top" 
                                width="62">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">Lt.:<br 
                                    soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="2" height="1" width="29">
                            </td>
                            <td height="1">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="40" height="12" width="405">
                            </td>
                            <td height="12" width="10">
                            </td>
                            <td colspan="2" height="12" width="10">
                            </td>
                            <td align="center" colspan="6" height="15" nowrap rowspan="3" valign="top" 
                                width="62">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">Mz.:<br 
                                    soft=""></br>
                                </span></span>
                            </td>
                            <td height="12" width="10">
                            </td>
                            <td colspan="2" height="12" width="29">
                            </td>
                            <td height="12">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="50" height="2" width="478">
                            </td>
                            <td colspan="2" height="2" width="10">
                            </td>
                            <td height="2" width="10">
                            </td>
                            <td colspan="2" height="2" width="29">
                            </td>
                            <td height="2">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="54" height="1" width="550">
                            </td>
                            <td colspan="8" height="1" width="101">
                            </td>
                            <td height="1">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="68" height="5" width="713">
                            </td>
                            <td height="5">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="25" height="4" width="180">
                            </td>
                            <td colspan="5" height="26" rowspan="3" valign="top" width="132">
                                <asp:TextBox ID="txt_Direccion" runat="server" size="17">Javier Prado Este</asp:TextBox>
                            </td>
                            <td colspan="2" height="4" width="9">
                            </td>
                            <td colspan="5" height="26" rowspan="3" valign="top" width="55">
                                <asp:TextBox ID="txt_NumExt" runat="server" size="6">2701</asp:TextBox>
                            </td>
                            <td colspan="2" height="4" width="28">
                            </td>
                            <td colspan="9" height="26" rowspan="3" valign="top" width="62">
                                <asp:TextBox ID="txt_NumInt" runat="server" size="7"></asp:TextBox>
                            </td>
                            <td colspan="2" height="4" width="12">
                            </td>
                            <td colspan="2" height="26" rowspan="3" valign="top" width="62">
                                <asp:TextBox ID="txt_Etapa" runat="server" size="7"></asp:TextBox>
                            </td>
                            <td colspan="2" height="4" width="10">
                            </td>
                            <td colspan="6" height="26" rowspan="3" valign="top" width="62">
                                <asp:TextBox ID="txt_Manzana" runat="server" size="7"></asp:TextBox>
                            </td>
                            <td height="4" width="10">
                            </td>
                            <td colspan="5" height="26" rowspan="3" valign="top" width="62">
                                <asp:TextBox ID="txt_Lote" runat="server" size="7"></asp:TextBox>
                            </td>
                            <td colspan="2" height="4" width="29">
                            </td>
                            <td height="4">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="11" height="5" width="106">
                            </td>
                            <td colspan="13" height="22" rowspan="2" valign="top" width="69">
                                <asp:DropDownList ID="ddl_TipoCalle" runat="server" AutoPostBack="True" 
                                    Font-Names="Arial" size="1">
                                    <asp:ListItem>Av.</asp:ListItem>
                                    <asp:ListItem>Jr.</asp:ListItem>
                                    <asp:ListItem>Ca.</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td height="5" width="5">
                            </td>
                            <td colspan="2" height="5" width="9">
                            </td>
                            <td colspan="2" height="5" width="28">
                            </td>
                            <td colspan="2" height="5" width="12">
                            </td>
                            <td colspan="2" height="5" width="10">
                            </td>
                            <td height="5" width="10">
                            </td>
                            <td colspan="2" height="5" width="29">
                            </td>
                            <td height="5">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="3" height="17" width="39">
                            </td>
                            <td colspan="7" height="17" nowrap valign="top" width="62">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">Direcci�n:<br 
                                    soft=""></br>
                                </span></span>
                            </td>
                            <td height="17" width="5">
                            </td>
                            <td height="17" width="5">
                            </td>
                            <td colspan="2" height="17" width="9">
                            </td>
                            <td colspan="2" height="17" width="28">
                            </td>
                            <td colspan="2" height="17" width="12">
                            </td>
                            <td colspan="2" height="17" width="10">
                            </td>
                            <td height="17" width="10">
                            </td>
                            <td colspan="2" height="17" width="29">
                            </td>
                            <td height="17">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="68" height="2" width="713">
                            </td>
                            <td height="2">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="5" height="1" width="46">
                            </td>
                            <td align="center" colspan="11" height="15" nowrap rowspan="2" valign="top" 
                                width="78">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">Zona:<br 
                                    soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="52" height="1" width="589">
                            </td>
                            <td height="1">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="5" height="14" width="46">
                            </td>
                            <td colspan="4" height="14" width="10">
                            </td>
                            <td align="center" colspan="11" height="15" nowrap rowspan="2" valign="top" 
                                width="185">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">
                                Zonificaci�n:<br soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="3" height="14" width="16">
                            </td>
                            <td align="center" colspan="10" height="15" nowrap rowspan="2" valign="top" 
                                width="106">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">
                                Departamento:<br soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="3" height="14" width="17">
                            </td>
                            <td align="center" colspan="8" height="15" nowrap rowspan="2" valign="top" 
                                width="106">
                                <span class="text"><span style="font-size:12px;line-height:15px;">
                                <span style="font-family: Tahoma">Provincia:</span><br soft=""></br>
                                </span></span>
                            </td>
                            <td height="14" width="13">
                            </td>
                            <td align="center" colspan="9" height="15" nowrap rowspan="2" valign="top" 
                                width="106">
                                <span class="text"><span style="font-size:12px;line-height:15px;">Distrito:<br 
                                    soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="3" height="14" width="30">
                            </td>
                            <td height="14">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td height="1" width="37">
                            </td>
                            <td colspan="17" height="26" rowspan="4" valign="top" width="90">
                                <asp:TextBox ID="txt_Zona" runat="server" size="11">Monterrico Chico</asp:TextBox>
                            </td>
                            <td colspan="2" height="1" width="7">
                            </td>
                            <td colspan="3" height="1" width="16">
                            </td>
                            <td colspan="3" height="1" width="17">
                            </td>
                            <td height="1" width="13">
                            </td>
                            <td colspan="3" height="1" width="30">
                            </td>
                            <td height="1">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td height="2" width="37">
                            </td>
                            <td colspan="50" height="2" width="586">
                            </td>
                            <td height="2">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td height="22" width="37">
                            </td>
                            <td colspan="2" height="22" width="7">
                            </td>
                            <td colspan="13" height="22" valign="top" width="189">
                                <asp:DropDownList ID="ddl_Zonificacion" runat="server" AutoPostBack="True" 
                                    Font-Names="Arial" size="1">
                                    <asp:ListItem>Urbanizacion</asp:ListItem>
                                    <asp:ListItem>Asentamiento Humano</asp:ListItem>
                                    <asp:ListItem>Pueblo Joven</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td height="22" width="12">
                            </td>
                            <td colspan="9" height="22" valign="top" width="104">
                                <asp:DropDownList ID="ddl_Departamento" runat="server" AutoPostBack="True" 
                                    Font-Names="Arial" size="1">
                                    <asp:ListItem>Lima</asp:ListItem>
                                    <asp:ListItem>Arequipa</asp:ListItem>
                                    <asp:ListItem>Puno</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td colspan="4" height="22" width="19">
                            </td>
                            <td colspan="6" height="22" valign="top" width="88">
                                <asp:DropDownList ID="ddl_Provincia" runat="server" AutoPostBack="True" 
                                    Font-Names="Arial" size="1">
                                    <asp:ListItem>Lima</asp:ListItem>
                                    <asp:ListItem>Huaral</asp:ListItem>
                                    <asp:ListItem>Canta</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td colspan="3" height="22" width="31">
                            </td>
                            <td colspan="11" height="22" valign="top" width="110">
                                <asp:DropDownList ID="ddl_Distrito" runat="server" AutoPostBack="True" 
                                    Font-Names="Arial" size="1">
                                    <asp:ListItem>Lima</asp:ListItem>
                                    <asp:ListItem>San Isidro</asp:ListItem>
                                    <asp:ListItem>San Borja</asp:ListItem>
                                    <asp:ListItem>La Molina</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td height="22" style="width: 2808910px">
                            </td>
                            <td height="22">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td height="1" width="37">
                            </td>
                            <td colspan="50" height="1" width="586">
                            </td>
                            <td height="1">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="68" height="5" width="713">
                            </td>
                            <td height="5">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="2" height="15" width="38">
                            </td>
                            <td colspan="10" height="15" nowrap valign="top" width="70">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">Referencia:<br 
                                    soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="7" height="15" width="23">
                            </td>
                            <td colspan="23" height="26" rowspan="3" valign="top" width="279">
                                <asp:TextBox ID="txt_Referencia" runat="server" size="38">Frente al Jockey Plaza</asp:TextBox>
                            </td>
                            <td colspan="4" height="15" width="42">
                            </td>
                            <td colspan="5" height="15" nowrap valign="top" width="79">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">
                                Nacionalidad:<br soft=""></br>
                                </span></span>
                            </td>
                            <td height="15" width="9">
                            </td>
                            <td colspan="15" height="22" rowspan="2" valign="top" width="147">
                                <asp:DropDownList ID="ddl_Nacionalidad" runat="server" AutoPostBack="True" 
                                    Font-Names="Arial" size="1">
                                    <asp:ListItem>Peruana</asp:ListItem>
                                    <asp:ListItem>Chilena</asp:ListItem>
                                    <asp:ListItem>China</asp:ListItem>
                                    <asp:ListItem>Norteamericana</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td height="15" style="width: 2808910px">
                            </td>
                            <td height="15">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="19" height="7" width="131">
                            </td>
                            <td colspan="10" height="7" width="130">
                            </td>
                            <td height="7" style="width: 2808910px">
                            </td>
                            <td height="7">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="19" height="4" width="131">
                            </td>
                            <td colspan="26" height="4" width="303">
                            </td>
                            <td height="4">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="68" height="3" width="713">
                            </td>
                            <td height="3">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="4" height="15" width="42">
                            </td>
                            <td align="center" colspan="24" height="15" nowrap valign="top" width="216">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">Tel�fonos:<br 
                                    soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="8" height="15" width="116">
                            </td>
                            <td align="center" colspan="27" height="15" nowrap valign="top" width="302">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">Correo 
                                Electr�nico:<br soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="5" height="15" width="37">
                            </td>
                            <td height="15">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="68" height="4" width="713">
                            </td>
                            <td height="4">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="2" height="70" width="38">
                            </td>
                            <td colspan="27" height="70" valign="top" width="225">
                                <asp:ListBox ID="lbx_Telefonos" runat="server" size="4">
                                    <asp:ListItem>(511) 221-2742    [Principal]</asp:ListItem>
                                    <asp:ListItem>(511) 99 783-4491 [Celular 1]</asp:ListItem>
                                </asp:ListBox>
                            </td>
                            <td colspan="6" height="70" width="110">
                            </td>
                            <td colspan="29" height="70" valign="top" width="305">
                                <asp:ListBox ID="lbx_Correos" runat="server" size="4">
                                    <asp:ListItem>jjvaldez@casacontratistas.com  [Principal]</asp:ListItem>
                                    <asp:ListItem>jjvaldez_2001@hotmail.com       [Personal]</asp:ListItem>
                                </asp:ListBox>
                            </td>
                            <td colspan="4" height="70" width="35">
                            </td>
                            <td height="70">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="68" height="10" width="713">
                            </td>
                            <td height="10">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="7" height="29" width="85">
                            </td>
                            <td colspan="19" height="29" valign="top" width="125">
                                <asp:Button ID="cmd_NuevoTelefono" runat="server" Font-Names="Tahoma" 
                                    Text="Nuevo Telefono" ToolTip="Agrega un Tel�fono a la lista" />
                            </td>
                            <td colspan="19" height="29" width="240">
                            </td>
                            <td colspan="14" height="29" valign="top" width="153">
                                <asp:Button ID="cmd_NuevoCorreo" runat="server" Font-Names="Tahoma" 
                                    Text="Nuevo Correo" ToolTip="Agrega un correo electronico a la lista" />
                            </td>
                            <td colspan="9" height="29" width="110">
                            </td>
                            <td height="29">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="68" height="3" width="713">
                            </td>
                            <td height="3">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="14" height="5" width="121">
                            </td>
                            <td colspan="48" height="26" rowspan="3" valign="top" width="545">
                                <asp:TextBox ID="txt_WebPage" runat="server" size="76">www.casacontratristas.com</asp:TextBox>
                            </td>
                            <td colspan="6" height="5" width="47">
                            </td>
                            <td height="5">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="2" height="15" width="38">
                            </td>
                            <td colspan="11" height="15" nowrap valign="top" width="76">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">P�gina Web:<br 
                                    soft=""></br>
                                </span></span>
                            </td>
                            <td height="15" width="7">
                            </td>
                            <td colspan="6" height="15" width="47">
                            </td>
                            <td height="15">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="14" height="6" width="121">
                            </td>
                            <td colspan="6" height="6" width="47">
                            </td>
                            <td height="6">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="68" height="5" width="713">
                            </td>
                            <td height="5">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="2" height="15" width="38">
                            </td>
                            <td colspan="6" height="15" nowrap valign="top" width="56">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">Contacto:<br 
                                    soft=""></br>
                                </span></span>
                            </td>
                            <td colspan="6" height="15" width="27">
                            </td>
                            <td colspan="27" height="26" rowspan="2" valign="top" width="286">
                                <asp:TextBox ID="txt_Contacto" runat="server" size="39">Sr. Juan Jose Valdez</asp:TextBox>
                            </td>
                            <td colspan="3" height="15" width="34">
                            </td>
                            <td colspan="13" height="15" nowrap valign="top" width="139">
                                <span class="text"><span style="font-size:12px;line-height:15px;">
                                <span style="font-family: Tahoma">Fecha de Alta en el Sist.:</span><br soft="">
                                </br>
                                </span></span>
                            </td>
                            <td height="15" width="10">
                            </td>
                            <td colspan="4" height="26" rowspan="2" valign="top" width="76">
                                <asp:TextBox ID="txt_FechaAlta" runat="server" size="9">01-Ene-2009</asp:TextBox>
                            </td>
                            <td colspan="6" height="15" width="47">
                            </td>
                            <td height="15">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="14" height="11" width="121">
                            </td>
                            <td colspan="17" height="11" width="183">
                            </td>
                            <td colspan="6" height="11" width="47">
                            </td>
                            <td height="11">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="68" height="5" width="713">
                            </td>
                            <td height="5">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td height="15" width="37">
                            </td>
                            <td colspan="21" height="15" nowrap valign="top" width="115">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">Cargo del 
                                Contacto:<br soft=""></br>
                                </span></span>
                            </td>
                            <td height="15" width="9">
                            </td>
                            <td colspan="17" height="26" rowspan="3" valign="top" width="244">
                                <asp:TextBox ID="txt_CargoContacto" runat="server" size="33">Gerente de Comercializacion</asp:TextBox>
                            </td>
                            <td colspan="4" height="15" width="36">
                            </td>
                            <td colspan="13" height="15" nowrap valign="top" width="139">
                                <span class="text">
                                <span style="font-size:12px;line-height:15px; font-family: Tahoma;">Fecha de 
                                Baja en el Sist.:<br soft=""></br>
                                </span></span>
                            </td>
                            <td height="15" width="10">
                            </td>
                            <td colspan="4" height="26" rowspan="3" valign="top" width="76">
                                <asp:TextBox ID="txt_FechaBaja" runat="server" size="9"></asp:TextBox>
                            </td>
                            <td colspan="6" height="15" width="47">
                            </td>
                            <td height="15">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="23" height="8" width="161">
                            </td>
                            <td colspan="18" height="8" width="185">
                            </td>
                            <td colspan="6" height="8" width="47">
                            </td>
                            <td height="8">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="17" height="3" width="126">
                            </td>
                            <td colspan="4" height="24" rowspan="4" valign="top" width="24">
                                <asp:CheckBox ID="chk_Activo" runat="server" Font-Names="Tahoma" 
                                    Font-Size="Small" Text="Activo" TextAlign="Left" />
                            </td>
                            <td colspan="2" height="3" width="11">
                            </td>
                            <td colspan="18" height="3" width="185">
                            </td>
                            <td colspan="6" height="3" width="47">
                            </td>
                            <td height="3">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="17" height="1" width="126">
                            </td>
                            <td colspan="47" height="1" width="563">
                            </td>
                            <td height="1">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="6" height="15" width="83">
                            </td>
                            <td colspan="9" height="15" nowrap valign="top" width="39">
                                <span class="text"><span style="font-size:12px;line-height:15px;"></span></span>
                            </td>
                            <td colspan="2" height="15" width="4">
                            </td>
                            <td colspan="47" height="15" width="563">
                            </td>
                            <td height="15">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="17" height="5" width="126">
                            </td>
                            <td colspan="47" height="5" width="563">
                            </td>
                            <td height="5">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <td colspan="68" height="4" width="713">
                            </td>
                            <td height="4">
                            </td>
                            <!-- workaround for IE table layout bug -->
                        </tr>
                        <tr>
                            <!-- workaround for IE table layout bug -->
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td style="width: 875px">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td style="width: 2808910px">
                            </td>
                            <td>
                            </td>
                        </tr>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                        </col>
                    </colgroup>
                </table>
            </td>
        </tr>
        </col>
    </colgroup>
</table>
            
            
            <!-- Aqui Insertar los campos de captura de datos.... FIN -->
        </ContentTemplate>
    </asp:UpdatePanel>
    &nbsp; &nbsp;&nbsp;

</asp:Content>
