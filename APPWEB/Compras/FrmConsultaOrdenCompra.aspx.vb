﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmConsultaOrdenCompra
    Inherits System.Web.UI.Page

    Private Const IdTipoDocumento As Integer = 16
    Private oCombo As Combo
    Private objScript As New ScriptManagerClass
    Private ListaOrdenesCompra As List(Of Entidades.DocumentoView)
    Private ListaProveedor As List(Of Entidades.PersonaView)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then

            OnLoad_frm()
            ConfigurarDatos()
        End If

    End Sub

    Private Sub OnLoad_frm()

        Me.txtFechaIni.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy").Trim
        Me.txtFechaFin.Text = Me.txtFechaIni.Text.Trim

        oCombo = New Combo
        With oCombo

            .LlenarCboPropietario(Me.ddlEmpresa, True)
            .llenarCboTiendaxIdEmpresa(Me.ddlTienda, CInt(Me.ddlEmpresa.SelectedValue), True)
            .LLenarCboEstadoEntrega(Me.ddlEstEntrega, True)
            .LLenarCboEstadoCancelacion(Me.ddlEstCancelacion, True)

        End With



    End Sub

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            Me.hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub

    Private Sub ddlEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged

        oCombo = New Combo
        With oCombo

            .llenarCboTiendaxIdEmpresa(Me.ddlTienda, CInt(Me.ddlEmpresa.SelectedValue), True)

        End With

    End Sub

#Region "****************Buscar Personas Mantenimiento"


#Region "Busqueda de Proveedores"

    Private Sub btBuscarProveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btBuscarProveedor.Click
        ViewState.Add("NomPersona", Me.txtBuscarProveedor.Text.Trim)
        ViewState.Add("Ruc", Me.txtbuscarRucProveedor.Text.Trim)
        ViewState.Add("Condicion", CInt(Me.rbcondicion.SelectedValue))
        ViewState.Add("TipoPersona", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", 1, 2)))

        OnClick_BuscarProveedor()
    End Sub

    Private Sub OnClick_BuscarProveedor()
        BuscarProveedor(0)
    End Sub

    Private Sub BuscarProveedor(ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(tbPageIndex_Proveedor.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(tbPageIndex_Proveedor.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(tbPageIndex_Proveedor.Text) - 1)
        End Select

        ListaProveedor = (New Negocio.OrdenCompra).listarPersonaJuridicaxRolxEmpresa(CStr(ViewState("NomPersona")), CStr(ViewState("Ruc")), CInt(ViewState("TipoPersona")), 2, CInt(ViewState("Condicion")), 1, index, gvProveedor.PageSize)

        If ListaProveedor.Count > 0 Then
            gvProveedor.DataSource = ListaProveedor
            gvProveedor.DataBind()
            tbPageIndex_Proveedor.Text = CStr(index + 1)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaProveedor();", True)
        Else
            gvProveedor.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaProveedor(); alert('No se hallaron registros');", True)
        End If

    End Sub

    Private Sub gvProveedor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvProveedor.SelectedIndexChanged
        Try
            Dim IdPersona As Integer = CInt(Me.gvProveedor.SelectedRow.Cells(1).Text)

            Me.hddIdPersona.Value = CStr(IdPersona)
            Me.txtRazonSocial.Text = HttpUtility.HtmlDecode(Me.gvProveedor.SelectedRow.Cells(2).Text).Trim
            Me.txtRuc.Text = HttpUtility.HtmlDecode(Me.gvProveedor.SelectedRow.Cells(3).Text).Trim

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



    Private Sub btAnterior_Proveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior_Proveedor.Click
        Try
            BuscarProveedor(1)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Proveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente_Proveedor.Click
        Try
            BuscarProveedor(2)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Proveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr_Proveedor.Click
        Try
            BuscarProveedor(3)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region


#End Region

    Private Sub btnMostrarReporte_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMostrarReporte.Click
        onClick_MostrarReporte()
    End Sub

    Private Sub onClick_MostrarReporte()
        ViewState.Add("IdEmpresa", CInt(Me.ddlEmpresa.SelectedValue))
        ViewState.Add("IdTienda", CInt(Me.ddlTienda.SelectedValue))
        ViewState.Add("IdProveedor", CInt(Me.hddIdPersona.Value))
        Dim FechaIni As String = txtFechaIni.Text.Trim
        Dim FechaFin As String = txtFechaFin.Text.Trim
        If Not Me.cbkConsiderarFecha.Checked Then
            FechaIni = ""
            FechaFin = ""
        End If
        ViewState.Add("FechaIni", FechaIni)
        ViewState.Add("FechaFin", FechaFin)
        ViewState.Add("IdEstadoCan", CInt(Me.ddlEstCancelacion.SelectedValue))
        ViewState.Add("IdEstadoEnt", CInt(Me.ddlEstEntrega.SelectedValue))

        Buscar_OrdenCompra(0)
    End Sub

    Private Sub Buscar_OrdenCompra(ByVal tipoMov As Integer)

        Dim index% = 0
        Select Case tipoMov
            Case 0
                index = 0
            Case 1
                index = (CInt(tbPageIndexOC.Text) - 1) - 1
            Case 2
                index = (CInt(tbPageIndexOC.Text) - 1) + 1
            Case 3
                index = (CInt(tbPageIndexOC.Text) - 1)

        End Select

        ListaOrdenesCompra = (New Negocio.OrdenCompra).listarOrdenesCompraxFiltro(CInt(ViewState("IdEmpresa")), CInt(ViewState("IdTienda")), CInt(ViewState("IdProveedor")), CStr(ViewState("FechaIni")), CStr(ViewState("FechaFin")), CInt(ViewState("IdEstadoCan")), CInt(ViewState("IdEstadoEnt")), index, GV_OrdenCompra.PageSize)

        If ListaOrdenesCompra.Count > 0 Then

            GV_OrdenCompra.DataSource = ListaOrdenesCompra
            GV_OrdenCompra.DataBind()
            tbPageIndexOC.Text = CStr(index + 1)

        Else

            GV_OrdenCompra.DataSource = Nothing
            GV_OrdenCompra.DataBind()
            objScript.mostrarMsjAlerta(Me, "No se encontraron registros")
        End If

    End Sub

    Private Sub btnAnteriorOC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnteriorOC.Click
        Buscar_OrdenCompra(1)
    End Sub

    Private Sub btnSiguienteOC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguienteOC.Click
        Buscar_OrdenCompra(2)
    End Sub

    Private Sub btIrOC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIrOC.Click
        Buscar_OrdenCompra(3)
    End Sub







#Region "Documento Referencia"

    Public Sub cargaTreeviewDocumentoRef(ByVal IdDocumento As Integer, ByVal ctrlTreeview As TreeView)
        Dim listaopciones As New List(Of Entidades.Opcion)
        listaopciones = (New Negocio.opcion).SelectOpcionxDocumentoRef(IdDocumento)

        ctrlTreeview.Nodes.Clear()

        CargaNodosTreeView(ctrlTreeview, listaopciones)
    End Sub

    Public Sub CargaNodosTreeView(ByVal ctrlTreeview As TreeView, ByVal listaOpcion As List(Of Entidades.Opcion))

        For Each opcion As Entidades.Opcion In listaOpcion
            Dim id, idmenu As Integer
            id = opcion.Id
            idmenu = opcion.IdMenu
            'si padre es igual al hijo entonces es menu padre.
            If (opcion.Id.Equals(opcion.IdMenu)) Then
                Dim TvwTreeNodo As New TreeNode
                'TvwTreeNodo.ShowCheckBox = True

                If opcion.IsPermitido = "1" Then
                    TvwTreeNodo.Checked = True
                Else
                    TvwTreeNodo.Checked = False
                End If
                TvwTreeNodo.Value = opcion.Id.ToString
                TvwTreeNodo.Text = opcion.Nombre.ToString
                'TvwTreeNodo.NavigateUrl = opcion.Formulario.ToString
                TvwTreeNodo.ToolTip = opcion.Formulario.ToString
                TvwTreeNodo.Expand()
                TvwTreeNodo.SelectAction = TreeNodeSelectAction.None
                ctrlTreeview.Nodes.Add(TvwTreeNodo)
                'hacemos un llamado al metodo recursivo encargado de generar el árbol del menú.
                AddTreeViewItem(TvwTreeNodo, listaOpcion)
            End If
        Next
    End Sub

    Private Sub AddTreeViewItem(ByRef mnuMenuItem As TreeNode, ByVal listaOpciones As List(Of Entidades.Opcion))
        'recorremos cada elemento del datatable para poder determinar cuales son elementos hijos
        'del menuitem dado pasado como parametro ByRef.
        For Each opcion As Entidades.Opcion In listaOpciones
            Dim id, idmenu, mnu As Integer
            id = opcion.Id
            idmenu = opcion.IdMenu
            mnu = CInt(mnuMenuItem.Value)

            If opcion.IdMenu.Equals(CInt(mnuMenuItem.Value)) AndAlso Not opcion.Id.Equals(CInt(opcion.IdMenu)) Then
                Dim TvwNewTreeNode As New TreeNode
                'TvwNewTreeNode.ShowCheckBox = True
                If opcion.IsPermitido = "1" Then
                    TvwNewTreeNode.Checked = True
                Else
                    TvwNewTreeNode.Checked = False
                End If
                TvwNewTreeNode.Value = opcion.Id.ToString
                TvwNewTreeNode.Text = opcion.Nombre.ToString
                'TvwNewTreeNode.NavigateUrl = opcion.Formulario.ToString
                TvwNewTreeNode.ToolTip = opcion.Formulario.ToString
                TvwNewTreeNode.SelectAction = TreeNodeSelectAction.Select
                'Agregamos el Nuevo MenuItem al MenuItem que viene de un nivel superior.
                mnuMenuItem.ChildNodes.Add(TvwNewTreeNode)
                'llamada recursiva para ver si el nuevo menú ítem aun tiene elementos hijos.
                AddTreeViewItem(TvwNewTreeNode, listaOpciones)
            End If
        Next
    End Sub



#End Region


    Private Sub GV_OrdenCompra_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_OrdenCompra.SelectedIndexChanged

        Dim IdDocumento As Integer = CInt(CType(GV_OrdenCompra.SelectedRow.FindControl("hhdIdDocumento"), HiddenField).Value)
        cargaTreeviewDocumentoRef(IdDocumento, TreeViewDocumentoRef)

    End Sub


    Private Sub TreeViewDocumentoRef_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeViewDocumentoRef.SelectedNodeChanged

        Dim lista As New List(Of Entidades.Documento)
        Dim IdDocumento As Integer = CInt(Me.TreeViewDocumentoRef.SelectedNode.Value)
        Dim obj As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
        If Not obj Is Nothing Then
            lista.Add(obj)
        End If

        Me.GV_DocumentoRef.DataSource = lista
        Me.GV_DocumentoRef.DataBind()


    End Sub

End Class