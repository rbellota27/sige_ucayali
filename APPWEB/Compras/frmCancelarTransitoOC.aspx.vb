﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmCancelarTransitoOC
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private Const _IdTipoDocumento As Integer = 16

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            inicializarFrm()
            hddIdUsuario.Value = CStr(Session("IdUsuario"))
        End If
    End Sub

    Private Sub inicializarFrm()
        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), _IdTipoDocumento)

            End With

            Me.txtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaFin.Text = Me.txtFechaInicio.Text

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

        onClick_Aceptar()

    End Sub
    Private Sub onClick_Aceptar()
        Try

            Dim obj As New Entidades.Documento
            With obj
                .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                .IdTienda = CInt(Me.cboTienda.SelectedValue)
                .IdSerie = CInt(Me.cboSerie.SelectedValue)

                Try : .FechaEmision = CDate(Me.txtFechaInicio.Text) : Catch ex As Exception : .FechaEmision = Nothing : End Try
                Try : .FechaVenc = CDate(Me.txtFechaFin.Text) : Catch ex As Exception : .FechaVenc = Nothing : End Try


            End With

            If (New Negocio.ApproveOC).CancelarStockTransito(obj) Then
                objScript.mostrarMsjAlerta(Me, "LA OPERACIÓN FINALIZÓ CON ÉXITO")
            Else
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            With objCbo

                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), _IdTipoDocumento)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

End Class