﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmAprobar_oCompra
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private Const _IdTipoDocumento As Integer = 16

    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    Private Sub javascript()
        btnBuscar.Attributes.Add("onClick", "this.disabled='true';this.value='Procesando...'")
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            inicializarFrm()
            hddIdUsuario.Value = CStr(Session("IdUsuario"))
        End If
    End Sub
    Private Sub inicializarFrm()
        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), _IdTipoDocumento)
                .LlenarCboAreaxIdusuario(Me.cboArea, CInt(Session("IdUsuario")), Nothing, False)
            End With
            Me.hddIdMoneda.Value = CStr(0)
            Me.hddIdPersona.Value = CStr(0)
            Me.hddTotal.Value = CStr(0)
            Me.hddIdDocumentoOC.Value = CStr(0)
            Me.txtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaFin.Text = Me.txtFechaInicio.Text

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ LOAD

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        valOnChange_cboEmpresa()
    End Sub
    Private Sub valOnChange_cboEmpresa()
        Try

            Dim objCbo As New Combo
            With objCbo
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ CBOEMPRESA 

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        visualizarRegOrdenCompra()
    End Sub
    Private Sub visualizarRegOrdenCompra()
        Try

            GV_OC.DataSource = (New Negocio.ApproveOC).OrdenCompra_Approve(CInt(cboEmpresa.SelectedValue), CInt(cboTienda.SelectedValue), CInt(cboArea.SelectedValue), CInt(cboOpcion_Aprobacion.SelectedValue), CInt(Me.cboSerie.SelectedValue), txtFechaInicio.Text.Trim, txtFechaFin.Text.Trim)
            GV_OC.DataBind()

            Dim permisos As Integer = New Negocio.Documento().SelectPermisosRQxIdPersona(CInt(Session("IdUsuario")))

            For Each gvRow As GridViewRow In GV_OC.Rows

                Dim chkItem As CheckBox = DirectCast(gvRow.FindControl("chb_Aprobado"), CheckBox)
                Dim btnGenerarRQ As Button = DirectCast(gvRow.FindControl("btnGenerarAutomatico"), Button)
                If (permisos = 1) Then
                    If chkItem.Checked = True Then
                        btnGenerarRQ.Enabled = True
                    Else
                        btnGenerarRQ.Enabled = False
                    End If
                Else
                    btnGenerarRQ.Enabled = False
                End If



            Next


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ BUSCAR

    Private Sub GV_OC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_OC.SelectedIndexChanged
        visualizarCapaOC(GV_OC.SelectedIndex)
    End Sub
    Private Sub visualizarCapaOC(ByVal index As Integer)
        Try

            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {263})

            If listaPermisos(0) > 0 Then  '********* AUTORIZAR 
            Else
                Throw New Exception("NO POSEE LOS PERMISOS PARA APROBAR")
            End If

            Dim IdDocumento As Integer = CInt(CType(GV_OC.Rows(index).Cells(2).FindControl("hddIdDocumento"), HiddenField).Value)

            hddIdDocumento.Value = CStr(IdDocumento)
            Dim objAnexoDocumento As Entidades.Anexo_Documento = (New Negocio.Anexo_Documento).Anexo_DocumentoSelectxIdDocumento(IdDocumento)
            txtfechaVencimiento.Text = CStr(GV_OC.Rows(index).Cells(4).Text)
            Dim cont As Integer = (New Negocio.Util).ValidarExistenciaxTablax1Campo("MovCuentaPorPagar", "IdDocumento", CStr(IdDocumento))

            If (objAnexoDocumento IsNot Nothing) Then

                With objAnexoDocumento

                    If (.Aprobar) Then
                        Me.rdbAutorizado.SelectedValue = "1"
                        Me.chbEnviarCxP.Enabled = True
                    Else
                        Me.rdbAutorizado.SelectedValue = "0"
                        Me.chbEnviarCxP.Enabled = False
                    End If

                    If (.FechaAprobacion <> Nothing) Then
                        Me.txtFechaAprobacion.Text = CStr(.FechaAprobacion)
                    Else
                        Me.txtFechaAprobacion.Text = ""
                    End If

                    If (cont > 0) Then
                        Me.chbEnviarCxP.Checked = True
                    Else
                        Me.chbEnviarCxP.Checked = False
                    End If

                End With

            Else

            End If

            objScript.onCapa(Me, "capaAprobarOC")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ GV_OC

    Private Sub rdbAutorizado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbAutorizado.SelectedIndexChanged
        OnChange_rdbAutorizado()
    End Sub
    Private Sub OnChange_rdbAutorizado()
        Try
            Select Case CInt(Me.rdbAutorizado.SelectedValue)
                Case 0  '*********** NO APROBAR

                    Me.txtFechaAprobacion.Text = ""
                    Me.chbEnviarCxP.Checked = False
                    Me.chbEnviarCxP.Enabled = False

                Case 1
                    Me.chbEnviarCxP.Checked = True
                    Me.txtFechaAprobacion.Text = CStr((New Negocio.FechaActual).SelectFechaActual)
                    Me.chbEnviarCxP.Enabled = False

            End Select

            objScript.onCapa(Me, "capaAprobarOC")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ rdbAutorizado

    Private objOrdenCompra_Approve As Entidades.Documento

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        onClick_Guardar()
    End Sub
    Private Sub onClick_Guardar()
        Try
            objOrdenCompra_Approve = New Entidades.Documento
            With objOrdenCompra_Approve
                .Id = CInt(hddIdDocumento.Value)
                .IdUsuarioSupervisor = CInt(hddIdUsuario.Value)

                Try
                    .FechaVenc = CDate(txtfechaVencimiento.Text)
                    .FechaAprobacion = CDate(txtFechaAprobacion.Text)
                Catch ex As Exception
                    .FechaAprobacion = Nothing
                    .FechaVenc = Nothing
                End Try

                Select Case CInt(Me.rdbAutorizado.SelectedValue)
                    Case 0
                        .Aprobar = False
                    Case 1
                        .Aprobar = True
                End Select

            End With

            If (New Negocio.ApproveOC).OrdenCompra_MovCuentaPorPagar(objOrdenCompra_Approve) Then
                visualizarRegOrdenCompra()
                objScript.mostrarMsjAlerta(Me, "LA OPERACIÓN FINALIZÓ CON ÉXITO")
            Else
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Dim objCbo As New Combo
        With objCbo
            .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), _IdTipoDocumento)
        End With
    End Sub

    Protected Sub GV_OC_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GV_OC.RowCommand

        Dim validador As Integer = 0


        If (e.CommandName = "RegistrarRQ") Then
            Dim index As Integer = CInt(e.CommandArgument)

            For Each row As GridViewRow In Me.GV_OC.Rows
                validador = CInt(CType(Me.GV_OC.Rows(index).FindControl("lblIdDocRelacionado"), Label).Text)
                Exit For
            Next

            If (validador <> 0) Then
                objScript.mostrarMsjAlerta(Me, "La O/C ya tiene un requerimiento relacionado. no procede la operación.")
            Else
                CapturarYRegistrarRQ(index, 1)
            End If
        End If
    End Sub

    Private Sub CapturarYRegistrarRQ(ByVal index As Integer, ByVal codigo As Integer)
        Try

            Dim IdDocumento As Integer = CInt(CType(Me.GV_OC.Rows(index).FindControl("hddIdDocumento"), HiddenField).Value)
            Dim objOC As Entidades.Documento = New Negocio.Documento().SelectDocumentotoInsertRQ(IdDocumento)
            Me.hddIdMoneda.Value = CStr(objOC.IdMoneda)
            Me.hddIdPersona.Value = CStr(objOC.IdPersona)
            Me.hddTotal.Value = CStr(objOC.TotalAPagar)
            Me.hddIdDocumentoOC.Value = CStr(objOC.Id)

            Dim objCbo As New Combo
            With objCbo
                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresaRQ, CInt(objOC.IdUsuario), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTiendaRQ, CInt(Me.cboEmpresaRQ.SelectedValue), CInt(objOC.IdUsuario), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboserieRQ, CInt(Me.cboEmpresaRQ.SelectedValue), CInt(Me.cboTiendaRQ.SelectedValue), CInt(36))

            End With

            GenerarCodigoDocumento()

            Me.GV_CuentaBancaria.DataSource = (New Negocio.CuentaBancaria).SelectActivoxIdPersona_DT(objOC.IdPersona)
            Me.GV_CuentaBancaria.DataBind()


            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaRQ');  ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtcodigoRQ.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboserieRQ.SelectedValue))
        Else
            Me.txtcodigoRQ.Text = ""
        End If

    End Sub

    Protected Sub cboserieRQ_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboserieRQ.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaRQ');  ", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub btnGuardarRQ_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardarRQ.Click

        Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
        Dim objAnexoDocumento As Entidades.Anexo_Documento = obtenerAnexoDocumento()
        Dim objMovCuentaCxP As Entidades.MovCuentaPorPagar = obtenerMovCuentaxPagar()
        Dim listaDetalleConcepto As List(Of Entidades.DetalleConcepto) = actualizarListaDetalleConcepto()
        'Me.listaDetalleConcepto = getListaDetalleConcepto()
        Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
        Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

        If (objAnexoDocumento.IdCuentaBancaria <> 0) Then
            objDocumento.Id = (New Negocio.RequerimientoGasto).registrarDocumento(objDocumento, objAnexoDocumento, listaDetalleConcepto, objMovCuentaCxP, objObservaciones, listaRelacionDocumento)
            Me.hddIdDocumento.Value = CStr(objDocumento.Id)
            objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaRQ'); alert('No se puede registrar ya que el proveedor o beneficiario no tiene una cuenta bancaria de referencia.');   ", True)
        End If

    End Sub
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento
        With objDocumento

            .Id = Nothing
            .IdEmpresa = CInt(Me.cboEmpresaRQ.SelectedValue)
            .IdTienda = CInt(Me.cboTiendaRQ.SelectedValue)
            .IdSerie = CInt(Me.cboserieRQ.SelectedValue)
            .Serie = CStr(Me.cboserieRQ.SelectedItem.ToString)
            .Codigo = Me.txtcodigoRQ.Text.Trim
            .FechaEmision = CDate(Format((DateTime.Now), "dd/MM/yyyy"))
            .FechaCancelacion = CDate(Format((DateTime.Now), "dd/MM/yyyy"))
            .FechaVenc = CDate(DateTime.Now)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdTipoOperacion = CInt(20000001)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdTipoDocumento = CInt(36)
            .IdMoneda = CInt(hddIdMoneda.Value)
            .TotalAPagar = CDec(hddTotal.Value)
            .Total = CDec(hddTotal.Value)
            .ImporteTotal = CDec(hddTotal.Value)
            .SubTotal = CDec(hddTotal.Value)
            .IdPersona = CInt(hddIdPersona.Value)
            .EstadoRequerimiento = 1
            .TotalLetras = (New Negocio.ALetras).Letras(CStr(Math.Round(.Total, 2))) + "/100 " + (New Negocio.Moneda).SelectxId(.IdMoneda)(0).Descripcion
            .IdRemitente = (CInt(Session("IdUsuario")))

        End With

        Return objDocumento
    End Function

    Private Function obtenerAnexoDocumento() As Entidades.Anexo_Documento

        Dim obj As New Entidades.Anexo_Documento
        Dim Valor As Boolean = False

        With obj

            .IdDocumento = Nothing
            .IdArea = CInt(Me.cboArea.SelectedValue)
            .Aprobar = True
            .FechaAprobacion = CDate(DateTime.Now)
            .EstadoRequerimiento = 1
            .IdUsuarioSupervisor = CInt(Session("IdUsuario"))

            If (GV_CuentaBancaria.Rows.Count > 0) Then
                For Each row As GridViewRow In Me.GV_CuentaBancaria.Rows
                    .Banco = CStr(CType(row.FindControl("lblBanco"), Label).Text)
                    .NroCuentaBancaria = CStr(CType(row.FindControl("lblNroCuentaBancaria"), Label).Text)
                    .IdCuentaBancaria = CInt(CType(row.FindControl("hddIdCuentaBancaria"), HiddenField).Value)
                    .cb_CuentaInterbancaria = CStr(CType(row.FindControl("lblcuentainterbanca"), Label).Text)
                    .cb_SwiftBancario = CStr(CType(row.FindControl("lblswift"), Label).Text)
                    Exit For
                Next
            Else
                .Banco = ""
                .NroCuentaBancaria = ""
                .IdCuentaBancaria = 0
                .cb_CuentaInterbancaria = ""
                .cb_SwiftBancario = ""

            End If

        End With
        Return obj

    End Function

    Private Function obtenerMovCuentaxPagar() As Entidades.MovCuentaPorPagar


        Dim objMovCuentaCxP As Entidades.MovCuentaPorPagar = Nothing
            objMovCuentaCxP = New Entidades.MovCuentaPorPagar

            With objMovCuentaCxP

            .Monto = CDec(hddTotal.Value)
            .Saldo = CDec(hddTotal.Value)
            .Factor = 1
            .IdProveedor = CInt(hddIdPersona.Value)
            .IdDocumento = Nothing
            .IdMovCuentaTipo = 1

        End With

        Return objMovCuentaCxP

    End Function

    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservaciones As Entidades.Observacion = Nothing
        objObservaciones = New Entidades.Observacion

        With objObservaciones
            .IdDocumento = Nothing
            .Observacion = "COMPRA DE MERCADERIA INGRESADA VIA INGRESO DE REQUERIMIENTO AUTOMÁTICO"

        End With
        Return objObservaciones
    End Function

    Private Function actualizarListaDetalleConcepto() As List(Of Entidades.DetalleConcepto)

        Dim listaDetalleConcepto As New List(Of Entidades.DetalleConcepto)
        Dim objLista As New Entidades.DetalleConcepto

        objLista.IdConcepto = CInt(87)
        objLista.IdMotivoGasto = CInt(2)
        objLista.Monto = CDec(hddTotal.Value)
        objLista.Concepto = CStr("COMPRA DE MERCADERIA")
        objLista.IdMoneda = CInt(hddIdMoneda.Value)

        listaDetalleConcepto.Add(objLista)

        Return listaDetalleConcepto

    End Function

    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)

            Dim objRelacionDocumento As New Entidades.RelacionDocumento
            With objRelacionDocumento
            .IdDocumento1 = CInt(hddIdDocumentoOC.Value)
            .IdDocumento2 = Nothing
        End With
            lista.Add(objRelacionDocumento)
        Return lista

    End Function
End Class