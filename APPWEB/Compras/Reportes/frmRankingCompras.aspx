﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmRankingCompras.aspx.vb" Inherits="APPWEB.frmRankingCompras" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  
<table style="width: 100%">
<tr > <td  align ="center " class ="TituloCelda"> Ranking de Proveedores</td></tr>
        <tr>
            <td>
                <table>
                    <tr>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                            <table> 
                                
                                <tr>
                                    <td >
                                            <table > 
                                            <tr >
                                            <td class="Label">
                                                Empresa:<asp:DropDownList ID="cmbEmpresa" runat="server" >
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Label">
                                                Tienda:<asp:DropDownList ID="cmbtienda" runat="server"  Enabled="true">
                                                </asp:DropDownList>
                                                <td class="Label">
                                                    Linea:<asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Label">
                                                    Sublinea:<asp:DropDownList ID="cboSublinea" runat="server">
                                                    </asp:DropDownList>
                                                </td>

                                                </td>
                                                <td class="Label">
                                                NPrimeros:<asp:TextBox onKeypress="return(onKeyPressEsNumero('event'));" ID="txtNPrimeros" runat="server"  Width ="80" ></asp:TextBox>
                                                </td>
                                            </td> 
                                            </tr>
                            </table> 
                            </td>
                            </tr>
                            <%--taable--%>
                                                          
                            </table>    
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    
                    </td>
                    </tr>
                    <tr>
                       <td >
                           <asp:Panel ID="Panel_Fechas" runat="server">
                           
                               <table>
                                   <tr>
                                       <td>
                                           <table>
                                               <tr>
                                                   <td class="Label">
                                                       <asp:Label ID="Label1" runat="server" Text="Persona:"></asp:Label>
                                                       <asp:DropDownList ID="cmbTipoPersona" runat="server" Enabled="true">
                                                           <asp:ListItem Value="1" Text="Juridica"></asp:ListItem>
                                                           <asp:ListItem Value="0" Text="Natural"></asp:ListItem>
                                                           </asp:DropDownList>
                                                   </td>
                                                   <td class="Label">
                                                       Forma Pago:<asp:DropDownList ID="cmdTipoOperacion" runat="server">
                                                       </asp:DropDownList>
                                                   </td>
                                                   <td class="Label">
                                                       Moneda:<asp:DropDownList ID="CmdMoneda" runat="server">
                                                       </asp:DropDownList>
                                                   </td>
                                                   <td class="Label">
                                                       Fecha Inicio:
                                                   </td>
                                                   <td>
                                                       <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                       <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                                           TargetControlID="txtFechaInicio">
                                                       </cc1:CalendarExtender>
                                                   </td>
                                                   <td class="Label">
                                                       Fecha Fin:
                                                   </td>
                                                   <td>
                                                       <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                       <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                                           TargetControlID="txtFechaFin">
                                                       </cc1:CalendarExtender>
                                                   </td>
                                               </tr>
                                           </table>
                                       </td>
                                   </tr>
                               </table>
                           
                           </asp:Panel>
                       </td>
                    </tr>

                    <tr>
                        <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                        <asp:ImageButton ID="btnMostrarReporte" runat="server" 
                        ImageUrl="~/Imagenes/Aceptar_B.JPG" onmouseout="this.src='/Imagenes/Aceptar_B.JPG';"                                                
                        onmouseover="this.src='/Imagenes/Aceptar_A.JPG';"
                        OnClientClick="return(MostrarReporte());"
                         />
                               
                             </ContentTemplate>
                        </asp:UpdatePanel>
                    
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td>
            <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px"></iframe>
            </td>
        </tr>
        <tr>
            <td>
                                               
                <asp:HiddenField ID="hddFechaActual" runat="server" />
                <asp:HiddenField ID="hddRbtTipoReporte" runat="server" />
                <br />
            </td>
        </tr>
</table>

<script language="javascript" type="text/javascript">

      function MostrarReporte() {
          var idempresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
          var idtienda = document.getElementById('<%=cmbtienda.ClientID%>').value;
          var idtipopersona = document.getElementById('<%=cmbTipoPersona.ClientID%>').value;
          var idtipooperacion = document.getElementById('<%=cmdTipoOperacion.ClientID%>').value;
          var idMoneda = document.getElementById('<%=CmdMoneda.ClientID%>').value;
          var idlinea = document.getElementById('<%=cboLinea.ClientID%>').value;
          var idsublinea = document.getElementById('<%=cboSublinea.ClientID%>').value;
          
          
          var nprimeros = document.getElementById('<%=txtNPrimeros.ClientID%>').value;
          if (nprimeros == "") {
              nprimeros = 0
          }
          else {
              nprimeros = nprimeros
          }

          if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
              fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
              fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
          } else {
              fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
              fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
          }
          frame1.location.href = 'visorCompras.aspx?iReporte=4&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + idempresa + '&IdTienda=' + idtienda + '&idtipopersona=' + idtipopersona + '&idtipooperacion=' + idtipooperacion + '&idMoneda=' + idMoneda + '&nprimeros=' + nprimeros + '&idlinea=' + idlinea + '&idsublinea=' + idsublinea;
          return false;
        
      }
   
    
  </script>

</asp:Content>
