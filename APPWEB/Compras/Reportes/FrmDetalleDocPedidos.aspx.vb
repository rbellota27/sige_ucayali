﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmDetalleDocPedidos
    Inherits System.Web.UI.Page
    Private objscript As New ScriptManagerClass
    Private objCombo As New Combo
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim objFechaActual As New Negocio.FechaActual
            txtFechaInicio.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            txtFechaFin.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            objCombo.LlenarCboPropietario(Me.cmbEmpresa, True)
            objCombo.LlenarCboAlmacenCentroDistribucionxIdEmpresa(Me.cboAlmacen, CInt(Me.cmbEmpresa.SelectedValue), True)
            objCombo.LlenarCboYear(Me.cboAno, True)
            objCombo.LlenarCboSemanaOfYear(Me.cboSemana, True)
        End If
    End Sub

    Protected Sub cmbEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbEmpresa.SelectedIndexChanged
        objCombo.LlenarCboAlmacenCentroDistribucionxIdEmpresa(Me.cboAlmacen, CInt(Me.cmbEmpresa.SelectedValue), True)
    End Sub

    Protected Sub cboAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboAlmacen.SelectedIndexChanged

    End Sub
End Class