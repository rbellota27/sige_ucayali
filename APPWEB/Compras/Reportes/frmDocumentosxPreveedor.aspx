<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmDocumentosxPreveedor.aspx.vb" Inherits="APPWEB.frmDocumentosxPreveedor" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td align="center" class="TituloCelda">
                Compras por Proveedores
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel_EmpresaTienda" runat="server">
                    <ContentTemplate>
                        <table style="width: 981px; height: 50px">
                            <tr>
                                <td class="Label">
                                    Empresa:
                                    <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="false">
                                    </asp:DropDownList>
                                  
                                    &nbsp;Tienda:
                                    <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="false" Enabled="true">
                                    </asp:DropDownList>
                                    &nbsp;
                                </td>
                                 <td class="Texto">
                                Tipo Existencia:
                         </td><td>
                                <asp:DropDownList ID="cmbTipoExistencia" runat="server" AutoPostBack ="true">
                                </asp:DropDownList>
                            </td>
                                </tr>
                                  
                           
                        
                                <tr>
                                <td class="Label" >
                                    Linea:
                                    <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true" Enabled="true">
                                    </asp:DropDownList>
                                   
                                   
                                </td>
                               <td class="Label" colspan="2"><center>
                                &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;Sublinea:
                                    <asp:DropDownList ID="cboSublinea" runat="server" Enabled="true">
                                    </asp:DropDownList></center>
                               </td>
                           </tr>
                            <tr>
                                <td class="Label" align="left">
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Label">
                            Fecha Inicio:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" TargetControlID="txtFechaInicio">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="Label">
                            Fecha Fin:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaFin" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" TargetControlID="txtFechaFin">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td valign="top" align="left">
                            <fieldset title="Tipo Reporte" class="FieldSetPanelReport">
                                <legend>Tipo Reporte: </legend>
                                <asp:RadioButtonList ID="rbtTipoReporte" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="1" Text="Detallado"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Resumido"></asp:ListItem>
                                </asp:RadioButtonList>
                            </fieldset>
                        </td>
                        <td valign="top" align="left">
                            <fieldset title="Agrupado Por:" class="FieldSetPanelReport">
                                <legend>Agrupado Por: </legend>
                                <asp:RadioButtonList ID="rbtlTpReporte" runat="server" RepeatDirection="Horizontal"
                                    Width="219px">
                                    <asp:ListItem Text="Proveedor" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Fecha Emision" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel_Cliente" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td class="TituloCeldaLeft">
                                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="PnlCliente"
                                        CollapsedSize="0" ExpandedSize="80" Collapsed="true" ExpandControlID="ImgCliente"
                                        CollapseControlID="ImgCliente" TextLabelID="lblCliente" ImageControlID="ImgCliente"
                                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                        CollapsedText="Buscar Proveedor" ExpandedText="Buscar Proveedor" ExpandDirection="Vertical"
                                        SuppressPostBack="True" />
                                    <asp:Image ID="ImgCliente" runat="server" Width="16px" />
                                    <asp:Label ID="lblCliente" runat="server" Text="Buscar Proveedor" CssClass="LabelBlanco"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="PnlCliente" runat="server">
                                        <table cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="btnBuscarCliente" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                                        OnClientClick="return(mostrarCapaPersona());" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label12024" runat="server" CssClass="Label" Text="Tipo de Persona"></asp:Label>
                                                    <asp:DropDownList ID="cboTipoPersona" runat="server" Width="116px" Font-Bold="True"
                                                        Enabled="False">
                                                        <asp:ListItem>---------</asp:ListItem>
                                                        <asp:ListItem>Natural</asp:ListItem>
                                                        <asp:ListItem>Juridica</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Limpiar_b.JPG"
                                                        onmouseover="this.src='/Imagenes/Limpiar_A.JPG';" onmouseout="this.src='/Imagenes/Limpiar_b.JPG';"
                                                        OnClientClick="return(LimpiarControlesCliente());" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 139px; text-align: right;">
                                                    <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Nombre o Razón Social"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRazonSocial" CssClass="TextBoxReadOnly" runat="server" Width="521px"
                                                        MaxLength="50" ReadOnly="True"></asp:TextBox>
                                                    <asp:TextBox ID="txtCodigoCliente" CssClass="TextBoxReadOnly" runat="server" Width="99px"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; width: 139px">
                                                    <asp:Label ID="Label3" runat="server" Text="Dni" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDni" runat="server" Width="70px" CssClass="TextBoxReadOnly" Style="margin-left: 0px;
                                                        margin-bottom: 0px" MaxLength="8" ReadOnly="True"></asp:TextBox>
                                                    <asp:Label ID="Label4" runat="server" Text="Ruc" CssClass="Label"></asp:Label>
                                                    <asp:TextBox ID="txtRuc" runat="server" Width="90px" MaxLength="11" CssClass="TextBoxReadOnly"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DGV_BuscarPersona" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:ImageButton ID="btnAceptar" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                            OnClientClick="return(mostrarReporte());" onmouseout="this.src='/Imagenes/Aceptar_B.JPG';"
                            onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                </iframe>
            </td>
        </tr>
    </table>
 
   <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_Persona" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('capaPersona'));" />
                        </td>
                    </tr>
                    <tr >
                        <td align="left">
                            <table>
                                <tr>
                                    <td class ="Label" >
                                         Persona
                                    </td>
                                    <td class ="Label" >
                                        <asp:RadioButtonList ID="rblpersona" runat="server"  RepeatDirection ="Horizontal"  >
                                        <asp:ListItem Value ="0" Text ="Juridica"  Selected ="True" ></asp:ListItem>
                                        <asp:ListItem Value ="1" Text ="Natural" ></asp:ListItem>
                                       </asp:RadioButtonList>
                                    </td>
                                    <td>
                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LabelLeft" >                                                                             
                                        RUC:
                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarPersona_Grilla">   
                                        <asp:TextBox ID ="txtBPruc" runat ="server"  onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                    <td class="LabelLeft" >                                            
                                        DNI:
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarPersona_Grilla">  
                                        <asp:TextBox ID ="txtBPDni" runat ="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                        </asp:Panel>
                                   </td>
                                    <td class ="LabelLeft" >
                                        Razon Social/Nombre:
                                        <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscarPersona_Grilla">          
                                        <asp:TextBox ID ="txtBuscarPersona_Grilla" runat ="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                    <td class ="LabelLeft">
                                        <asp:ImageButton ID="btnBuscarPersona_Grilla" runat="server" CausesValidation="false"
                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:GridView ID="DGV_BuscarPersona" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                PageSize="20" Width="100%">
                                <Columns>
                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                    <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Descripción" NullDisplayText="---" />
                                    <asp:BoundField DataField="Dni" HeaderText="D.N.I." NullDisplayText="---" />
                                    <asp:BoundField DataField="Ruc" HeaderText="R.U.C." NullDisplayText="---" />
                                    <asp:BoundField DataField="Direccion" HeaderText="Dirección" NullDisplayText="---" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnAnterior_Persona" runat="server" Font-Bold="true" Width="50px"
                                Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionPersona('0'));" />
                            <asp:Button ID="btnPosterior_Persona" runat="server" Font-Bold="true" Width="50px"
                                Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionPersona('1'));" />
                            <asp:TextBox ID="txtPageIndex_Persona" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                runat="server"></asp:TextBox>
                            <asp:Button ID="btnIr_Persona" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                ToolTip="Ir a la Página" OnClientClick="return(valNavegacionPersona('2'));" />
                            <asp:TextBox ID="txtPageIndexGO_Persona" Width="50px" CssClass="TextBox_ReadOnly"
                                runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
            <script language="javascript" type="text/javascript">

                function mostrarReporte() {
                    var fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
                    var fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
                    var IdEmpresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
                    var IdTienda = document.getElementById('<%= cboTienda.ClientID%>').value;
                    var idcliente = document.getElementById('<%= txtCodigoCliente.ClientID%>').value;
                    var linea = document.getElementById('<%=cbolinea.ClientID %>').value;
                    var sublinea = document.getElementById('<%=cbosublinea.ClientID %>').value;  

                    var NomEmpresa = '';
                    var NomTienda = '';
                    if (idcliente == '') {
                        idcliente = 0;
                    } else {
                        idcliente = idcliente;
                    }

                    var cbo = document.getElementById('<%= cmbEmpresa.ClientID%>');
                    for (var i = 0; i < cbo.length; i++) {
                        if (IdEmpresa == cbo[i].value) {
                            NomEmpresa = cbo[i].text;
                            break;
                        }
                    }
                    cbo = document.getElementById('<%= cboTienda.ClientID%>');
                    for (var i = 0; i < cbo.length; i++) {
                        if (IdTienda == cbo[i].value) {
                            NomTienda = cbo[i].text;
                            break;
                        }
                    }



                    var rdbList = document.getElementById('<%=rbtTipoReporte.ClientID %>');
                    var tipo = 0;


                    if (rdbList.cells[0].children[0].status == true) {
                        tipo = rdbList.cells[0].children[0].value;

                    } else if (rdbList.cells[1].children[0].status == true) {
                        tipo = rdbList.cells[1].children[0].value;
                    }
                    var rblist = document.getElementById('<%=rbtlTpReporte.ClientID%>');

                    var reporte = 0;
                    if (rblist.cells[0].children[0].status == true) {
                        reporte = 1;
                    } else if (rblist.cells[1].children[0].status == true) {
                        reporte = 2;
                    } else {
                        reporte = 1;
                    }

                    frame1.location.href = '/compras/Reportes/visorCompras.aspx?iReporte=5&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + IdEmpresa + '&IdTienda=' + IdTienda + '&NomEmpresa=' + NomEmpresa + '&NomTienda=' + NomTienda + '&idcliente=' + idcliente + '&tipo=' + tipo + '&reporte=' + reporte+'&linea=' + linea +'&sublinea='+sublinea  ;
                    return false;
                }




                function onCapaCliente(s) {
                    LimpiarControlesCliente();
                    onCapa(s);
                    document.getElementById('<%= txtRazonSocial.ClientID%>').focus();
                    return false;
                }
                void function LimpiarControlesCliente() {
                    document.getElementById('<%= txtRazonSocial.ClientID%>').value = "";
                    document.getElementById('<%= txtDNI.ClientID%>').value = "";
                    document.getElementById('<%= txtRUC.ClientID%>').value = "";
                    document.getElementById('<%= txtCodigoCliente.ClientID %>').value = "";
                    return false;
                }



                function validarNumeroPunto(event) {
                    var key = event.keyCode;
                    if (key == 46) {
                        return true;
                    } else {
                        if (!onKeyPressEsNumero('event')) {
                            return false;
                        }
                    }
                }



                //Buscar SubLinea
                function ValidarEnteroSinEnter() {
                    if (onKeyPressEsNumero('event') == false) {
                        alert('Caracter no válido. Solo se permiten números Enteros');
                        return false;
                    }
                    return true;
                }


                //End Buscar SubLinea
                //Buscar Cliente

                function BuscarCliente() {

                    return true;
                }


                function ValidarEnter() {
                    return (!esEnter(event));
                }
                function ValidaEnter() {
                    if (esEnter(event) == true) {
                        alert('Tecla No permitida');
                        caja.focus();
                        return false;
                    }
                    return true;
                }



                function mostrarCapaPersona() {
                    onCapa('capaPersona');
                    document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
                    document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();

                    return false;
                }
                function addPersona_Venta() {
                    offCapa('capaPersona');

                    return false;
                }


                function aceptarFoco(caja) {
                    caja.select();
                    caja.focus();
                    return true;
                }




                function valNavegacionPersona(tipoMov) {
                    var index = parseInt(document.getElementById('<%=txtPageIndex_Persona.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                        alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                        document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
                        document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
                        return false;
                    }
                    switch (tipoMov) {
                        case '0':   //************ anterior
                            if (index <= 1) {
                                alert('No existen páginas con índice menor a uno.');
                                return false;
                            }
                            break;
                        case '1':
                            break;
                        case '2': //************ ir
                            index = parseInt(document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').value);
                            if (isNaN(index) || index == null || index.length == 0) {
                                alert('Ingrese una Página de navegación.');
                                document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                                document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                                return false;
                            }
                            if (index < 1) {
                                alert('No existen páginas con índice menor a uno.');
                                document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                                document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                                return false;
                            }
                            break;
                    }
                    return true;
                }


                function validarCajaBusqueda() {
                    var key = event.keyCode;
                    if (key == 13) {
                        var boton = document.getElementById('<%=btnBuscarPersona_Grilla.ClientID %>');
                        boton.focus();
                        return true;
                    }
                }


              </script>
</asp:Content>
