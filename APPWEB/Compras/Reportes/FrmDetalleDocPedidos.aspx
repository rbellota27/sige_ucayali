<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmDetalleDocPedidos.aspx.vb" Inherits="APPWEB.FrmDetalleDocPedidos" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table style="width: 100%">
        <tr>
            <td align="center" class="TituloCelda">
                Detalles de Pedidos
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel_EmpresaTienda" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Label">
                                                Empresa:
                                                <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Label">
                                                Almacen:
                                                <asp:DropDownList ID="cboAlmacen" runat="server" AutoPostBack="false" Enabled="true">
                                                </asp:DropDownList>
                                            </td>
                                             <td class="Label">
                                                Documento:
                                                <asp:DropDownList ID="cboTipoDocumento" runat="server" AutoPostBack="false" Enabled="true">
                                                <asp:ListItem Text ="-----" Value="0"></asp:ListItem>
                                                <asp:ListItem Text ="OP-Sucursales" Value ="15" ></asp:ListItem>
                                                <asp:ListItem Text ="OP-Centro Distribuicion" Value ="56"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="LabelLeft">
                                                <asp:CheckBox ID="chbFSemana" runat="server" Text="Filtro por Semana" />
                                            </td>
                                            <td class="LabelLeft">
                                                Año
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboAno" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelLeft">
                                                Semana
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboSemana" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Label">
                            Fecha Inicio:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                            <cc1:calendarextender id="txtFechaInicio_CalendarExtender" runat="server" targetcontrolid="txtFechaInicio">
                                    </cc1:calendarextender>
                        </td>
                        <td class="Label">
                            Fecha Fin:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaFin" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                            <cc1:calendarextender id="txtFechaFin_CalendarExtender" runat="server" targetcontrolid="txtFechaFin">
                                    </cc1:calendarextender>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:ImageButton ID="btnAceptar" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                            OnClientClick="return(mostrarReporte());" onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';"
                            onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                </iframe>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        function mostrarReporte() {
            var fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
            var fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
            var IdEmpresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
            var idAlmacen = document.getElementById('<%= cboAlmacen.ClientID%>').value;
            var year = document.getElementById('<%= cboAno.ClientID%>').value;
            var Semana = document.getElementById('<%=cboSemana.ClientID %>').value;
            var tipodocs = document.getElementById('<%=cboTipoDocumento.ClientID %>').value;
            var check = document.getElementById('<%=chbFSemana.ClientID %>');

            var fSemana = 0;
            if (check.status == true) {
                var fSemana = 1;
            }
            else {
                var fSemana = 0;
            }


            frame1.location.href = '../../compras/Reportes/visorCompras.aspx?iReporte=8&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + IdEmpresa + '&idAlmacen=' + idAlmacen + '&year=' + year + '&Semana=' + Semana + '&fSemana=' + fSemana + '&tipodocs=' + tipodocs;
            return false;
        }

    </script>
</asp:Content>
