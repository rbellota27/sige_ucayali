﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmVentasxVendedor
    Inherits System.Web.UI.Page
    Private objscript As New ScriptManagerClass
    Private objCombo As New Combo
    Private objNegUsu As New Negocio.UsuarioView
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim objFechaActual As New Negocio.FechaActual
            txtFechaInicio.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            txtFechaFin.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")

            objCombo.LlenarCboPropietario(Me.cmbEmpresa, True)
            objCombo.LLenarCboTienda(Me.cboTienda, True)
            objCombo.LlenarCboLinea(Me.cboLinea, True)
            objCombo.LLenarCboSubLinea(Me.cboSublinea, True)

            objCombo.LlenarCboRol(ddl_Rol, True)

            If Not IsNothing(Me.ddl_Rol.Items.FindByValue("5")) Then
                Me.ddl_Rol.SelectedValue = "5" ' EMPLEADO
            End If

            'objCombo.LlenarCboTiendaxIdUsuario(Me.cboTienda, CInt(Session("IdUsuario")), True)
            'If objNegUsu.FunVerTodasTiendas(CInt(Session("IdUsuario")), 110) > 0 Then
            '    objCombo.LlenarCboTiendaxIdUsuario(Me.cboTienda, CInt(Session("IdUsuario")), True)
            'Else
            '    objCombo.LlenarCboTiendaxIdUsuario(Me.cboTienda, CInt(Session("IdUsuario")), False)
            'End If

        End If
    End Sub
    
    'Este metodo llena o ingresa 
    Private Sub LLenarCliente(ByVal cliente As Entidades.PersonaView)


        If cliente.RazonSocial <> "" Then
            txtRazonSocial.Text = cliente.RazonSocial
            Me.cboTipoPersona.SelectedIndex = 2 'Juridica
        Else
            txtRazonSocial.Text = cliente.Nombre
            Me.cboTipoPersona.SelectedIndex = 1 'Natural
        End If

        Me.txtDni.Text = cliente.Dni
        Me.txtRuc.Text = cliente.Ruc
        Me.txtCodigoCliente.Text = CStr(cliente.IdPersona)


        Dim NegPersonaTipoAgente As New Negocio.PersonaTipoAgente
        Dim TipoAgente As New Entidades.TipoAgente
        TipoAgente = NegPersonaTipoAgente.SelectIdAgenteTasaxIdPersona(cliente.IdPersona)

    End Sub

   
    'Este metodo limpia estas 3 cajas de texto
    Private Sub LimpiarBuscarCliente()
        txtRazonSocial.Text = ""
        Me.txtDni.Text = ""
        Me.txtRuc.Text = ""
        Me.txtCodigoCliente.Text = ""

    End Sub
    'Este metodo busca el dni del cliente
    Private Sub BuscarClientexDni()
        Me.LimpiarBuscarCliente()
        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            'Cliente = obj.SelectxDni(Me.txtBuscarCliente.Text.Trim)
            Me.LLenarCliente(Cliente)
        Catch ex As Exception
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    'Este metodo busca el ruc del cliente
    Private Sub BuscarClientexRuc()
        Me.LimpiarBuscarCliente()
        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            'Cliente = obj.SelectxRuc(Me.txtBuscarCliente.Text.Trim)
            Me.LLenarCliente(Cliente)
        Catch ex As Exception
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub BuscarClientexId(ByVal IdCliente As Integer)
        Me.LimpiarBuscarCliente()
        Dim obj As New Negocio.Cliente
        Dim Cliente As New Entidades.PersonaView
        Try
            Cliente = obj.SelectxId(IdCliente)
            Me.LLenarCliente(Cliente)
        Catch ex As Exception
            'MsgBox1.ShowMessage(ex.Message)
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
 

    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged
        objCombo.LlenarCboSubLineaxIdLinea(Me.cboSublinea, CInt(Me.cboLinea.SelectedValue), True)
    End Sub

    Protected Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar.Click

    End Sub

    Protected Sub btnBuscarCliente_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarCliente.Click

    End Sub


#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", ddl_ROl.SelectedValue)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            addPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub addPersona(ByVal IdPersona As Integer)
        Try

            Dim objCliente As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)
            Me.LLenarCliente(objCliente)

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region


End Class