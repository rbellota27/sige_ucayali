﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmRptPersona.aspx.vb" Inherits="APPWEB.frmRptPersona" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing ="0">
                <tr>
                    <td colspan = "5" class="TituloCelda"  >
                        <center><h3>Listado de Personas</h3></center>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2" align="center" valign="middle">
                        &nbsp; Filtros:
                    </td>
                    <td class="LabelTab">
                        Nombres / Razon Social:
                    </td>
                    <td>
                        <asp:TextBox ID="tbNombres" Width="350px" runat="server"></asp:TextBox>
                    </td>
                    <td class="LabelTab">
                        Tipo:
                    </td>
                    <td>
                        <asp:DropDownList ID="dltipoPersona" Width="180px" runat="server">
                            <asp:ListItem Value="1">Natural</asp:ListItem>
                            <asp:ListItem Value="2">Juridico</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="LabelTab">
                        DNI / RUC:
                    </td>
                    <td>
                        <asp:TextBox ID="tbDocumento" runat="server" MaxLength="11"></asp:TextBox>
                    </td>
                    <td class="LabelTab">
                        Estado:
                    </td>
                    <td>
                        <asp:DropDownList ID="dlEstado" Width="180px" runat="server">
                            <asp:ListItem Selected="True" Value="2">-  Seleccionar -</asp:ListItem>
                            <asp:ListItem Value="1">Activo</asp:ListItem>
                            <asp:ListItem Value="0">Inactivo</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;Orden:
                    </td>
                    <td align="center" colspan="3">
                        <asp:RadioButtonList ID="rbOrdenar" CssClass="Label" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Value="1">Alfabetico</asp:ListItem>
                            <asp:ListItem Value="2">DNI / RUC</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>
                        
                        <asp:ImageButton ID="btnMostrarReporte" runat="server" 
                            ImageUrl="~/Imagenes/Aceptar_B.JPG" OnClientClick="return(mostrarReporte());" 
                            onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
                        
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <iframe id ="iframePersona" width="100%" scrolling="yes" style="height:1024px" ></iframe>

    <script language="javascript" type="text/javascript">


        function mostrarReporte() {
            var nombres = document.getElementById('<%=tbNombres.ClientID%>').value;
            var tipoPersona = document.getElementById('<%=dltipoPersona.ClientID%>').value;
            var Documento = document.getElementById('<%=tbDocumento.ClientID%>').value;
            var estado = document.getElementById('<%=dlEstado.ClientID%>').value;
            var radio = document.getElementById('<%=rbOrdenar.ClientID%>');
            var control = radio.getElementsByTagName('input');
            var ordenar = 0;
            for (var i = 0; i < control.length; i++) {
                if (control[i].checked == true) {
                    ordenar = control[i].value;
                }
            }          
            iframePersona.location.href = 'visorCompras.aspx?iReporte=2&nombres=' + nombres + '&tipoPersona=' + tipoPersona + '&Documento=' + Documento + '&estado=' + estado + '&ordenar=' + ordenar;
            return false;
        }
        
    </script>
    
    
</asp:Content>
