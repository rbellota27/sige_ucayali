﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.IO
Imports System.Reflection
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data.DataTable
Imports Excel = Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Interop.Excel
Imports System.Runtime.InteropServices.Marshal

Partial Public Class frmComparativoxCompraAnual
    Inherits System.Web.UI.Page

#Region "variables"
    Private objscript As New ScriptManagerClass
    Private drop As Combo
    Private ListaProveedor As List(Of Entidades.PersonaView)
    Dim saveFile As FileUpload
#End Region

#Region "procedimientos"

    Private Sub cargarAlIniciar()
        drop = New Combo
        drop.LlenarCboMoneda(dlmoneda, True)
        drop.LlenarCboEmpresaxIdUsuario(dlpropietario, CInt(Session("IdUsuario")), False)
    End Sub

#End Region

#Region "Eventos principales"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Me.Button1.Attributes.Add("onClick", "this.disabled=true;this.value='Procesando...'")
                cargarAlIniciar()
            End If
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    'Private Sub btnMostrarReporte_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMostrarReporte.Click
    '    Try
    '        ReporteComprarativoCompra()
    '    Catch ex As Exception
    '        objscript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub
#End Region

#Region "Busqueda de Proveedores"


    Private Sub btBuscarProveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btBuscarProveedor.Click
        ViewState.Add("NomPersona", Me.txtBuscarProveedor.Text.Trim)
        ViewState.Add("Ruc", Me.txtbuscarRucProveedor.Text.Trim)
        ViewState.Add("Condicion", CInt(Me.rbcondicion.SelectedValue))
        ViewState.Add("TipoPersona", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", 1, 2)))

        OnClick_BuscarProveedor()
    End Sub

    Private Sub OnClick_BuscarProveedor()
        BuscarProveedor(0)
    End Sub

    Private Sub BuscarProveedor(ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(tbPageIndex_Proveedor.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(tbPageIndex_Proveedor.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(tbPageIndex_Proveedor.Text) - 1)
        End Select

        ListaProveedor = (New Negocio.OrdenCompra).listarPersonaJuridicaxRolxEmpresa(CStr(ViewState("NomPersona")), CStr(ViewState("Ruc")), CInt(ViewState("TipoPersona")), 2, CInt(ViewState("Condicion")), 1, index, gvProveedor.PageSize)

        If ListaProveedor.Count > 0 Then
            gvProveedor.DataSource = ListaProveedor
            gvProveedor.DataBind()
            tbPageIndex_Proveedor.Text = CStr(index + 1)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaProveedor();", True)
        Else
            gvProveedor.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaProveedor(); alert('No se hallaron registros');", True)
        End If

    End Sub

    Private Sub btAnterior_Proveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior_Proveedor.Click
        Try
            BuscarProveedor(1)
        Catch ex As Exception

            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Proveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente_Proveedor.Click
        Try
            BuscarProveedor(2)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Proveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr_Proveedor.Click
        Try
            BuscarProveedor(3)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btLimpiarProveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btLimpiarProveedor.Click
        Try
            tbproveedor.Text = ""
            tbruc.Text = ""
            hdd_Idproveedor.Value = "0"
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvProveedor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvProveedor.SelectedIndexChanged

        Try
            Dim ruc$ = gvProveedor.SelectedRow.Cells(3).Text
            If ruc = "" Or ruc = "---" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "validarRucProveedor();", True)
                UpdatePanelProveedor.UpdateMode = UpdatePanelUpdateMode.Conditional
                Exit Sub
            End If
            hdd_Idproveedor.Value = gvProveedor.SelectedRow.Cells(1).Text
            tbproveedor.Text = HttpUtility.HtmlDecode(gvProveedor.SelectedRow.Cells(2).Text)
            tbruc.Text = HttpUtility.HtmlDecode(gvProveedor.SelectedRow.Cells(3).Text)
            UpdatePanelProveedor.UpdateMode = UpdatePanelUpdateMode.Conditional
            If ruc <> "" Or ruc <> "---" Then objscript.offCapa(Me, "verBusquedaProveedor")
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "excel"
    Dim objUtil As Util
    'Private Sub ReporteComprarativoCompra()
    '    Try
    '        Dim data As System.Data.DataTable = (New Negocio.OrdenCompra).getDataSetComparativo(CInt(tbyear.Text), _
    '                                rbtipo.SelectedValue, CInt(hdd_Idproveedor.Value), _
    '                                CInt(dlmoneda.SelectedValue), CInt(dlpropietario.SelectedValue))

    '        ExportToExcel(data)

    '        'If data IsNot Nothing Then
    '        '    Dim objRS As New ADODB.Recordset
    '        '    objUtil = New Util
    '        '    objRS = objUtil.ConvertToRecordset(ds.Tables(0))
    '        '    getOExcel(objRS, "Comparativo Anual de Compras", Server.MapPath(Request.ApplicationPath) & "\PlantillasXLT\ComparativoCompras")
    '        'End If
    '    Catch ex As Exception
    '        Response.Write("<script>alert('" + ex.Message + "');</script>")
    '    End Try
    'End Sub

    Private Sub ExportToExcel(ByVal objDT As System.Data.DataTable)
        Dim Excel As Excel.ApplicationClass
        Excel = CType(CreateObject("excel.application"), Excel.ApplicationClass)
        Dim strFilename As String
        Dim intCol, intRow As Integer
        Dim strPath As String = "C:\SIGE"

        Try
            With Excel
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()

                Dim intI As Integer = 1
                For intCol = 0 To objDT.Columns.Count - 1
                    .Cells(2, intI) = objDT.Columns(intCol).ColumnName

                    intI += 1
                Next
                intI = 3
                Dim intK As Integer = 1
                For intCol = 0 To objDT.Columns.Count - 1
                    intI = 3
                    For intRow = 0 To objDT.Rows.Count - 1
                        .Cells(intI, intK) = objDT.Rows(intRow).ItemArray(intCol)
                        intI += 1
                    Next
                    intK += 1
                    If intK = 11 Then Exit For
                Next
                If Mid$(strPath, strPath.Length, 1) <> "\" Then
                    strPath = strPath & "\"
                End If
                strFilename = strPath & "Excel.xls"
                .ActiveCell.Worksheet.SaveAs(strFilename)
            End With
            System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            Excel = Nothing
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
        ' The excel is created and opened for insert value. We most close this excel using this system
        Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")
        For Each i As Process In pro
            i.Kill()
        Next
    End Sub


    Public Sub getOExcel(ByVal rcset As ADODB.Recordset, ByVal titulo As String, ByVal ruta As String)
        Dim oExcel As Excel.ApplicationClass
        Dim oBook As Excel.WorkbookClass
        Dim oBooks As Excel.Workbooks
        saveFile = New FileUpload

        Try
            Dim Name As String
            Dim hora As String = FormatDateTime(Now, DateFormat.ShortTime)
            Name = Now.Day.ToString + MonthName(Now.Date.Month) + hora.Substring(0, 2) + hora.Substring(3, 2)
            oExcel = CType(CreateObject("excel.application"), Excel.ApplicationClass)
            'oExcel.Visible = True
            oBooks = oExcel.Workbooks
            oBook = CType(oBooks.Open(ruta), Excel.WorkbookClass)
            'oExcel.Visible = True
            oExcel.DisplayAlerts = False
            oExcel.Run("reporte", rcset, titulo)
            oBook.SaveAs("C:\SIGE\rptAnualCompra" + Name + ".xlt")

            ' oExcel.ActiveWorkbook.SaveAs("C:\SIGE\rptAnualCompra" + Name + ".xlt")
            ' saveFile.SaveAs("C:\SIGE\rptAnualCompra" + Name + ".xlt")
            oExcel.Quit()
            oExcel = Nothing
        Catch xErr As Exception
            Throw xErr
        End Try

    End Sub
#End Region



End Class