<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmConsultarCuentasPorPagar.aspx.vb" Inherits="APPWEB.frmConsultarCuentasPorPagar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td class="TituloCelda">
                Consultar Cuentas Por Pagar
            </td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td class="LabelTab">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="dlpropietario" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="LabelTab">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="dlTienda" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="LabelTab">
                            Moneda:
                        </td>
                        <td>
                            <asp:DropDownList ID="dlmoneda" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="LabelTab">
                            Fecha Vencimiento:
                        </td>
                        <td>
                            <asp:TextBox Width="100px" ID="tbfecha" runat="server" CssClass="TextBox_Fecha" onKeyPress="return(false);"></asp:TextBox>
                            <cc1:CalendarExtender ID="calendar" runat="server" Format="d" TargetControlID="tbfecha">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="LabelTab">
                            Raz&oacute;n Social / Nombres:
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="tbPersona" CssClass="TextBoxReadOnly" runat="server" Width="235px"
                                Font-Bold="true" onKeyDown="return(false);"></asp:TextBox>
                        </td>
                        <td align="left">
                            &nbsp;
                            <asp:Button ID="btBuscarPersona" CssClass="btnBuscar" runat="server" Text="Buscar" OnClientClick="return(validarPersona());" />
                            <asp:Button ID="btLimpiarPersona" runat="server" Text="Limpiar" ToolTip="[Borrar - Persona]" CssClass="btnLimpiar"
                                OnClientClick="return(limpiarPersona());" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelTab">
                            D.N.I.:
                        </td>
                        <td class="LabelLeft">
                            <asp:TextBox ID="tbdni" Width="90px" CssClass="TextBoxReadOnly" Font-Bold="true"
                                runat="server" onKeyDown="return(false);"></asp:TextBox>
                        </td>
                        <td class="LabelTab">
                            R.U.C.:
                        </td>
                        <td align="left">
                            <asp:TextBox Width="95px" ID="tbruc" Font-Bold="true" runat="server" CssClass="TextBoxReadOnly"
                                onKeyDown="return(false);"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="ImageButton1" OnClientClick="return(verReporte());" runat="server"
                    ImageUrl="~/Imagenes/Aceptar_B.JPG" />
                <asp:HiddenField ID="hdd_idpersona" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_idusuario" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                <iframe id="iframeConsulta" scrolling="yes" width="100%" height="1024px"></iframe>
            </td>
        </tr>
    </table>
    <div id="capapersona" style="border: 3px solid blue; padding: 8px; width: 750px;
        height: auto; position: absolute; background-color: white; top: 130px; z-index: 2;
        left: 40px; display: none;">
        <table width="100%">
            <tr>
                <td align="right">
                                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                        OnClientClick="return(offCapa('capaPersona'));" />
                                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                    AutoPostBack="false">
                                    <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                    <asp:ListItem Value="J">Jur&iacute;dica</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td class="Texto">
                                Rol:
                            </td>
                            <td colspan="2" align="left">
                                <asp:DropDownList ID="dlrolpersona" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Raz&oacute;n Social / Nombres:
                            </td>
                            <td colspan="4" align="left">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btPersona">                               
                                <asp:TextBox ID="tbrazonape" onKeyPress="return(validarCajaBusqueda());" runat="server"
                                    Width="450px"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                D.N.I.:
                            </td>
                            <td align="left">
                            <asp:Panel ID="Panel2" runat="server" DefaultButton="btPersona">
                                <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero());" onFocus="javascript:validarbusqueda();"
                                    MaxLength="8" runat="server"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td class="Texto">
                                R.U.C.:
                            </td>
                            <td align="left">
                            <asp:Panel ID="Panel3" runat="server" DefaultButton="btPersona">
                                <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero());"
                                    MaxLength="11"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td align="left">
                                <asp:Button ID="btPersona" runat="server" Text="Buscar" CssClass="btnBuscar" 
                                OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="20" Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="return(SelectPersona(this));">Seleccionar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="idpersona" HeaderText="C�digo" NullDisplayText="0" />
                            <asp:BoundField DataField="Nombre" HeaderText="Raz�n Social / Nombres" NullDisplayText="---">
                                <ItemStyle HorizontalAlign="Left" Width="500px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                        ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                    <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                        ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                    <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                        runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                            Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                    <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                        onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        ///////////////////////////////////////////////////////////////////////////////////
        function validarPersona() {
            onCapa('capapersona');
            return false;
        }
        ////////////////////////////////////////////////////////////////////////
        function SelectPersona(obj) {
            var idpersona = document.getElementById('<%=hdd_idpersona.ClientID %>');
            var persona = document.getElementById('<%=tbPersona.ClientID %>');
            var ruc = document.getElementById('<%=tbruc.CLientID %>');
            var dni = document.getElementById('<%=tbdni.CLientID %>');
            var grilla = document.getElementById('<%=gvBuscar.ClientID %>');

            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (rowElem.cells[0].children[0].id == obj.id) {
                    idpersona.value = rowElem.cells[1].innerText;
                    persona.value = rowElem.cells[2].innerText;
                    ruc.value = rowElem.cells[3].innerText;
                    dni.value = rowElem.cells[4].innerText;
                    offCapa('capapersona');
                    return false;
                }
            }

        }
        //////////////////////////////////////////////////////////////////
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbrazonape.ClientID%>').select();
                document.getElementById('<%=tbrazonape.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   /////////////////////////// anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //////////////////////////// ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        /////////////////////////////////////////////////////////////////////////
        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        //////////////////////////////////////////////////////////////////
        function validarCajaBusquedaNumero() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btPersona.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        /////////////////////////////////////////////////////////////////////////////
        function validarCajaBusqueda() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btPersona.ClientID %>');
                boton.focus();
                return true;
            }
        }
        /////////////////////////////////////////////////////////////////////////////
        function limpiarPersona() {
            document.getElementById('<%=hdd_idpersona.ClientID %>').value = '';
            document.getElementById('<%=tbPersona.ClientID %>').value = '';
            document.getElementById('<%=tbruc.CLientID %>').value = '';
            document.getElementById('<%=tbdni.CLientID %>').value = '';
            return false;
        }
        ///////////////////
        function verReporte() {
            var fecha = document.getElementById('<%=tbfecha.ClientID %>').value;
            if (fecha.length > 0) {
                if (validarFechaVen(document.getElementById('<%=tbfecha.ClientID %>')) == false) {
                    return false;
                }                    
            }                      
            
            var IdPersona = document.getElementById('<%=hdd_idpersona.ClientID %>').value;
            var IdEmpresa = document.getElementById('<%=dlpropietario.ClientID %>').value;
            var IdMoneda = document.getElementById('<%=dlmoneda.ClientID %>').value;
            var idTienda = document.getElementById('<%=dlTienda.ClientID %>').value;
            
                        
            iframeConsulta.location.href = 'visorCompras.aspx?iReporte=7&IdEmpresa=' + IdEmpresa + '&IdPersona=' + IdPersona + '&IdMoneda=' + IdMoneda + '&IdTienda=' + idTienda + '&Fecha=' + fecha;
            return false;
        }
        ////////////////////
        function validarFechaVen(oTxt) {
            var bOk = true;
            if (oTxt.value != "") {
                bOk = bOk && (valAno(oTxt));
                bOk = bOk && (valMes(oTxt));
                bOk = bOk && (valDia(oTxt));
                bOk = bOk && (valSep(oTxt));
                if (!bOk) {
                    alert("Fecha inv�lida (dd/mm/yyyy).");
                    oTxt.select();
                    oTxt.focus();
                    return false;
                }
            } 
            return true;
        }
       
    </script>

</asp:Content>
