﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.Reflection
Imports System.IO
Imports System.Data
Imports Microsoft.Office.Interop
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
'25_03_2010
Partial Public Class visorCompras
    Inherits System.Web.UI.Page


    Private objUtil As Util
    Dim reporte As New ReportDocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim parametrosURl$ = Request.QueryString("iReporte")
            If parametrosURl Is Nothing Then parametrosURl = ""
            ViewState.Add("iReporte", parametrosURl)

            Select Case CInt(ViewState("iReporte"))
                Case 1 'Boton Imprimir frmOrdenCompra1
                    ViewState.Add("IdDoc", Request.QueryString("IdDoc"))
                Case 2 'Consulta de Personas
                    ViewState.Add("nombres", Request.QueryString("nombres"))
                    ViewState.Add("tipoPersona", Request.QueryString("tipoPersona"))
                    ViewState.Add("Documento", Request.QueryString("Documento"))
                    ViewState.Add("estado", Request.QueryString("estado"))
                    ViewState.Add("ordenar", Request.QueryString("ordenar"))
                Case 3 'Boton Imprimir frmOrdenPedido
                    ViewState.Add("IdDoc", Request.QueryString("IdDoc"))

                Case 4
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("idtienda", Request.QueryString("idtienda"))
                    ViewState.Add("idtipopersona", Request.QueryString("idtipopersona"))
                    ViewState.Add("idtipooperacion", Request.QueryString("idtipooperacion"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("idMoneda", Request.QueryString("idMoneda"))
                    ViewState.Add("nprimeros", Request.QueryString("nprimeros"))
                    ViewState.Add("idlinea", Request.QueryString("idlinea"))
                    ViewState.Add("idsublinea", Request.QueryString("idsublinea"))
                Case 5
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("NomEmpresa", Request.QueryString("NomEmpresa"))
                    ViewState.Add("NomTienda", Request.QueryString("NomTienda"))
                    ViewState.Add("idcliente", Request.QueryString("idcliente"))
                    ViewState.Add("tipo", Request.QueryString("tipo"))
                    ViewState.Add("reporte", Request.QueryString("reporte"))
                    ViewState.Add("linea", Request.QueryString("linea"))
                    ViewState.Add("sublinea", Request.QueryString("sublinea"))


                Case 6
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("NomEmpresa", Request.QueryString("NomEmpresa"))
                    ViewState.Add("NomTienda", Request.QueryString("NomTienda"))
                    ViewState.Add("idcliente", Request.QueryString("idcliente"))
                    ViewState.Add("tipo", Request.QueryString("tipo"))
                    ViewState.Add("reporte", Request.QueryString("reporte"))
                    ViewState.Add("linea", Request.QueryString("linea"))
                    ViewState.Add("sublinea", Request.QueryString("sublinea"))
                Case 7
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("IdMoneda", Request.QueryString("IdMoneda"))
                    ViewState.Add("IdPersona", Request.QueryString("IdPersona"))
                    ViewState.Add("IdTienda", Request.QueryString("IdTienda"))
                    ViewState.Add("Fecha", Request.QueryString("Fecha"))

                Case 8
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
                    ViewState.Add("IdEmpresa", Request.QueryString("IdEmpresa"))
                    ViewState.Add("idAlmacen", Request.QueryString("idAlmacen"))
                    ViewState.Add("year", Request.QueryString("year"))
                    ViewState.Add("Semana", Request.QueryString("Semana"))
                    ViewState.Add("fSemana", Request.QueryString("fSemana"))
                    ViewState.Add("tipodocs", Request.QueryString("tipodocs"))
            End Select
        End If
        verReporte()
    End Sub

    Private Sub verReporte()
        Select Case CInt(ViewState("iReporte"))
            Case 1 'Boton Imprimir frmOrdenCompra1
                ReporteDocOrdenCompra()
            Case 2 'Consulta de Personas
                ReportePersonas()
            Case 3 'Boton Imprimir frmOrdenPedido
                ReporteDocOrdenPedido()
            Case 4 'Comparativo anual - compra
                ReporteRankingProveedores()
            Case 5
                reporteDocxProveedor()
            Case 6
                reporteVentasxVendedor()
            Case 7
                reporteConsulta_Cuentas_Por_Pagar_Cargos()
            Case 8
                rptDetallePedidosInternos()
        End Select
    End Sub

    Private Sub rptDetallePedidosInternos()
        Dim ds As New DataSet
        Dim obj As New Negocio.DocOrdenPedido
        Dim doc As Integer
        doc = CInt(ViewState("tipodocs"))

        ds = obj.DetalleOrdenPedido(CInt(ViewState("IdEmpresa")), CInt(ViewState("idAlmacen")), CInt(ViewState("year")), CInt(ViewState("Semana")), _
        CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")), CInt(ViewState("fSemana")), doc)
        reporte = New CR_DetallePedidosInternos
        reporte.SetDataSource(ds)

        CrystalReportViewer1.ReportSource = reporte
        CrystalReportViewer1.DataBind()


    End Sub

    Private Sub reporteConsulta_Cuentas_Por_Pagar_Cargos()
        Dim IdEmpresa As Integer = CInt(ViewState("IdEmpresa"))
        Dim IdMoneda As Integer = CInt(ViewState("IdMoneda"))
        Dim IdPersona As Integer = CInt(ViewState("IdPersona"))
        Dim IdTienda As Integer = CInt(ViewState("IdTienda"))
        Dim Fecha As String = CStr(ViewState("Fecha"))

        Dim ds As DataSet = (New Negocio.DocumentoExterno).getDataSet_Cuentas_Por_Pagar_Cargos(IdEmpresa, IdMoneda, IdPersona, IdTienda, Fecha)
        reporte = New CR_Cuentas_Por_Pagar_Cargos
        reporte.SetDataSource(ds)
        reporte.SetParameterValue("@Fecha", (ViewState("Fecha")))

        CrystalReportViewer1.ReportSource = reporte
        CrystalReportViewer1.DataBind()
    End Sub
    Private Sub reporteDocxProveedor()
        Try
            Dim vidempresa, tipo, repte, linea, sublinea As Integer
            Dim tienda, empresa As String

            vidempresa = CInt(ViewState.Item("IdEmpresa"))
            tienda = CStr(ViewState.Item("NomTienda"))
            empresa = CStr(ViewState.Item("NomTienda"))
            tipo = CInt(ViewState.Item("tipo"))
            repte = CInt(ViewState.Item("reporte"))
            linea = CInt(ViewState.Item("linea"))
            sublinea = CInt(ViewState.Item("sublinea"))

            Dim ds As New DataSet
            Dim objReporte As New Negocio.OrdenCompra
            ds = objReporte.ComprasxProveedor(CInt(ViewState("IdEmpresa")), CInt(ViewState.Item("IdTienda")), CInt(ViewState.Item("idcliente")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")), tipo, linea, sublinea)

            reporte = New ReportDocument

            If (tipo = 1) Then
                If repte = 1 Then
                    reporte = New CR_DetalladoVentasCpras
                Else
                    reporte = New CR_DetalladoVentasCprasFEmision
                End If

            Else
                reporte = New CR_ResumidoVtasCpras
            End If

            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            'reporte.SetParameterValue("@NomTienda", ViewState("NomTienda"))
            'reporte.SetParameterValue("@NomEmpresa", ViewState("NomEmpresa"))
            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub reporteVentasxVendedor()
        Try
            Dim vidempresa, tipo, rpte, linea, sublinea As Integer
            Dim tienda, empresa As String

            vidempresa = CInt(ViewState.Item("IdEmpresa"))
            tienda = CStr(ViewState.Item("NomTienda"))
            empresa = CStr(ViewState.Item("NomTienda"))
            tipo = CInt(ViewState.Item("tipo"))
            rpte = CInt(ViewState.Item("reporte"))
            linea = CInt(ViewState.Item("linea"))
            sublinea = CInt(ViewState.Item("sublinea"))

            Dim ds As New DataSet
            Dim objReporte As New Negocio.OrdenCompra
            ds = objReporte.ventasxVendedor(CInt(ViewState("IdEmpresa")), CInt(ViewState.Item("IdTienda")), CInt(ViewState.Item("idcliente")), CStr(ViewState("FechaInicio")), CStr(ViewState("FechaFin")), tipo, linea, sublinea)

            reporte = New ReportDocument

            If (tipo = 1) Then
                If rpte = 1 Then
                    reporte = New CR_DetalladoVentasxVendedor
                Else
                    reporte = New CR_DetalladoVentasXVendedorFEmision
                End If

            Else
                reporte = New CR_ResumidoVtasCpras
            End If

            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            'reporte.SetParameterValue("@NomTienda", ViewState("NomTienda"))
            'reporte.SetParameterValue("@NomEmpresa", ViewState("NomEmpresa"))
            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()

        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub


    Private Sub ReporteRankingProveedores()
        Try
            Dim vidempresa, vidtienda, vtipopersona, vtipooperacion, vidMoneda, nprimeros As Integer
            If CStr(ViewState.Item("IdEmpresa")) = "" Then
                vidempresa = 0
            Else
                vidempresa = CInt(ViewState.Item("IdEmpresa"))
            End If

            If CStr(ViewState.Item("idtienda")) = "" Then
                vidtienda = 0
            Else
                vidtienda = CInt(ViewState.Item("idtienda"))
            End If

            If CStr(ViewState.Item("idtipopersona")) = "" Then
                vtipopersona = 0
            Else
                vtipopersona = CInt(ViewState.Item("idtipopersona"))
            End If

            If CStr(ViewState.Item("idtipooperacion")) = "" Then
                vtipooperacion = 0
            Else
                vtipooperacion = CInt(ViewState.Item("idtipooperacion"))
            End If

            If CStr(ViewState.Item("idMoneda")) = "" Then
                vidMoneda = 0
            Else
                vidMoneda = CInt(ViewState.Item("idMoneda"))
            End If
            Dim vidlinea, vidsublinea As Integer




            If CStr(ViewState.Item("idlinea")) = "" Then
                vidlinea = 0
            Else
                vidlinea = CInt(ViewState.Item("idlinea"))
            End If
            If CStr(ViewState.Item("idsublinea")) = "" Then
                vidsublinea = 0
            Else
                vidsublinea = CInt(ViewState.Item("idsublinea"))
            End If


            If CStr(ViewState.Item("nprimeros")) = "" Then
                nprimeros = 0
            Else
                nprimeros = CInt(ViewState.Item("nprimeros"))
            End If

            Dim ds As New DataSet
            Dim objReporte As New Negocio.OrdenCompra
            ds = objReporte.RankingProvedores(CInt(vidempresa), CInt(vidtienda), CInt(vtipooperacion), CInt(vidMoneda), CInt(vtipopersona), CStr((ViewState.Item("FechaInicio"))), CStr((ViewState.Item("FechaFin"))), nprimeros, vidlinea, vidsublinea)
            'Dim reporte As ReportDocument
            'reporte = Me.TipoReporteRanking(vidempresa, vidtienda)
            reporte = New CR_RankingProveedores
            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@FechaInicio", (ViewState("FechaInicio")))
            reporte.SetParameterValue("@FechaFin", (ViewState("FechaFin")))
            CrystalReportViewer1.ReportSource = reporte
            CrystalReportViewer1.DataBind()

        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, "Problemas en el reporte.")
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub ReportePersonas()
        Try

            Dim ds As DataSet = (New Negocio.Persona).getDataSetConsultaPersona(CStr(ViewState("nombres")), _
                            CStr(ViewState("Documento")), CInt(ViewState("tipoPersona")), CInt(ViewState("estado")), CInt(ViewState("ordenar")))
            If ds IsNot Nothing Then
                reporte = New CR_Persona
                reporte.SetDataSource(ds)
                CrystalReportViewer1.ReportSource = reporte
            End If
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub ReporteDocOrdenCompra()
        Try
            Dim ds As DataSet = (New Negocio.OrdenCompra).getDataSetOrdenCompra(CInt(ViewState("IdDoc")))
            If ds IsNot Nothing Then
                reporte = New CR_DocOrdenCompra
                reporte.SetDataSource(ds)

                CrystalReportViewer1.ReportSource = reporte
            End If
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub ReporteDocOrdenPedido()
        Try
            Dim ds As DataSet = (New Negocio.DocOrdenPedido).getDataSetOrdenPedido(CInt(ViewState("IdDoc")))
            If ds IsNot Nothing Then
                reporte = New CR_OrdenPedido
                reporte.SetDataSource(ds)
                CrystalReportViewer1.ReportSource = reporte
            End If
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    'Private Sub ReporteComprarativoCompra()
    '    Try
    '        Dim ds As DataSet = (New Negocio.OrdenCompra).getDataSetComparativo(CInt(ViewState("idyear")), _
    '                                CStr(ViewState("idtipo")), CInt(ViewState("idproveedor")), _
    '                                CInt(ViewState("idmoneda")), CInt(ViewState("idpropietario")))
    '        If ds IsNot Nothing Then
    '            Dim objRS As New ADODB.Recordset
    '            objUtil = New Util
    '            objRS = objUtil.ConvertToRecordset(ds.Tables(0))
    '            getOExcel(objRS, "Comparativo Anual de Compras", Server.MapPath(Request.ApplicationPath) & "\PlantillasXLT\ComparativoCompras")
    '        End If
    '    Catch ex As Exception
    '        Response.Write("<script>alert('" + ex.Message + "');</script>")
    '    End Try
    'End Sub


    Public Sub getOExcel(ByVal rcset As ADODB.Recordset, ByVal titulo As String, ByVal ruta As String)
        Dim oExcel As Excel.ApplicationClass
        Dim oBook As Excel.WorkbookClass
        Dim oBooks As Excel.Workbooks

        Try
            oExcel = CType(CreateObject("excel.application"), Excel.ApplicationClass)
            oExcel.Visible = True
            oBooks = oExcel.Workbooks
            oBook = CType(oBooks.Open(ruta), Excel.WorkbookClass)

            oExcel.Visible = True
            oExcel.DisplayAlerts = False
            oExcel.Run("reporte", rcset, titulo)

        Catch xErr As Exception
            Throw xErr
        End Try

    End Sub

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reporte.Close()
        reporte.Dispose()
    End Sub
End Class

