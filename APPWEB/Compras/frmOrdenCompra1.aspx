<%@ Page Title="Orden de Compra" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmOrdenCompra1.aspx.vb" Inherits="APPWEB.frmOrdenCompra1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <asp:Panel ID="PanelButtons" runat="server" >
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnuevo" runat="server" Text="Nuevo" Style="cursor: hand; width: 90px" />
                        </td>
                        <td>
                            <asp:Button ID="btguardar" runat="server" Text="Guardar"
                                Style="cursor: hand; width: 90px" />
                        </td>
                        <td>
                            <asp:Button ID="btcancelar" runat="server" Text="Cancelar"
                                Style="cursor: hand; width: 90px" />
                        </td>
                        <td>
                            <asp:Button ID="btbuscar" runat="server" Text="Buscar" Style="cursor: hand; width: 90px" />
                        </td>
                        <td>
                            <asp:Button ID="bteditar" runat="server" Text="Editar"
                                Style="cursor: hand; width: 90px" />
                        </td>
                        <td>
                            <asp:Button ID="btanular" runat="server" Text="Anular"
                                Style="cursor: hand; width: 90px" />
                        </td>
                        
                        <td>
                            <asp:Button ID="btimprimir" runat="server" Text="Imprimir"
                                Style="cursor: hand; width: 90px" />
                        </td>
                         <td>
                            <asp:Button ID="btimprimir02" runat="server" Text="Imprimir con N.Comercial"
                                Style="cursor: hand; width: 160px" />
                        </td>
                    </tr>
                </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                ORDEN COMPRA
            </td>
        </tr>
         <tr>
            <td>
                <asp:Panel ID="pnlDetalle" runat="server">
                    <table style="width: 100%">
                        <tr>
                            <td align="center">
                                <asp:RadioButtonList ID="rbtipodeoc" runat="server" AutoPostBack="True" CssClass="Texto"
                                    RepeatDirection="Horizontal" Style="cursor: hand;" onClick="return ( onClick_rbtipodeoc() );">
                                    <asp:ListItem Value="True" Text="IMPORTADA">IMPORTADA</asp:ListItem>
                                    <asp:ListItem Value="False" Text="NACIONAL">NACIONAL</asp:ListItem>
                                    <asp:ListItem Value="" Text="ACTIVOS">ACTIVOS</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>                      
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Empresa:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlPropietario" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Tienda:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlTienda" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Serie:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlSerie" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        
                                        <td>
                                        <asp:Panel ID="pnlBuscarDocumento" runat="server" DefaultButton="btnBuscarDocumento"> <!-- -->
                                            <asp:TextBox ID="tbCodigoDocumento" runat="server" Width="90px" CssClass="TextBox_ReadOnly"></asp:TextBox>                                            
                                        </asp:Panel>
                                        </td>
                                        <td>
                                           <asp:ImageButton ID="btnBuscarDocumento" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                                ToolTip="Buscar Documento por [ Serie - N�mero ]." Height="16px" />
                                        </td>                                                                                 
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            Moneda:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlMoneda" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Fecha Emisi&oacute;n:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbFechaEmision" runat="server" Width="90px" CssClass="TextBox_Fecha"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            Tipo Operaci&oacute;n:
                                        </td>
                                        <td colspan="2">
                                            <asp:DropDownList ID="dlTipoOperacion" runat="server" Width="100%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            Area:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlarea" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Fecha Vcto:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtnrodiastbFechaVen" runat="server" MaxLength="4"
                                                Width="40px"></asp:TextBox>
                                            <asp:TextBox ID="tbFechaVen" runat="server" CssClass="TextBox_Fecha" Width="80px"></asp:TextBox>
                                            <cc1:CalendarExtender ID="Fven" runat="server" Format="d" TargetControlID="tbFechaVen">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td class="Texto">
                                            Precio Importaci&oacute;n:
                                        </td>
                                        <td colspan="2">
                                            <asp:DropDownList ID="dltipoPrecio" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            Contacto:
                                        </td>
                                        <td colspan="3">
                                            <asp:DropDownList ID="ddl_contactoPropietario" runat="server" AutoPostBack="true"
                                                Width="100%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Est. Cancelaci&oacute;n:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlEstadoCancelacion" runat="server" Enabled="False">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Est. Entrega:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlEstadoEntrega" runat="server" Enabled="False">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Est. Documento:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dlEstadoDocumento" runat="server" Enabled="False">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--Centro Costo--%>
                        <tr id="tr_centroCosto" runat="server" visible="false">
                            <td>
                                <asp:Panel ID="pnlcentroCosto" runat="server" Visible="false">
                                    <table>
                                        <tr>
                                            <td>
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="TituloCeldaLeft">
                                                CENTRO COSTO
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="Texto">
                                                            Unidad Negocio:<br />
                                                            <asp:DropDownList ID="dlunidadnegocio" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Texto">
                                                            Dpto Funcional:<br />
                                                            <asp:DropDownList ID="dldptofuncional" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Texto">
                                                            Sub-&Aacute;rea 1:<br />
                                                            <asp:DropDownList ID="dlsubarea1" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Texto">
                                                            Sub-&Aacute;rea 2:<br />
                                                            <asp:DropDownList ID="dlsubarea2" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Texto">
                                                            Sub-&Aacute;rea 3:<br />
                                                            <asp:DropDownList ID="dlsubarea3" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCeldaLeft">
                                DATOS DEL PROVEEDOR
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Raz&oacute;n Social:
                                        </td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="tbProveedor" CssClass="TextBoxReadOnly" runat="server" Width="450px"></asp:TextBox>
                                            <asp:Button ID="Button1" runat="server" Text="Buscar"
                                                Style="cursor: hand;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            R.U.C.:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="tbRuc" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            Agente:
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddl_agenteProveedor" runat="server" Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            Direcci&oacute;n:
                                        </td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="tbdireccion" CssClass="TextBoxReadOnly" runat="server" Width="450px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            Tel&eacute;fono:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="tbtelefono" CssClass="TextBoxReadOnly" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            Correo:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="tbCorreo" CssClass="TextBoxReadOnly" runat="server"
                                                Width="350px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:GridView ID="gvCuentaProveedor" runat="server" AutoGenerateColumns="False">
                                                <RowStyle CssClass="GrillaRow" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Moneda">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="gdlmoneda" runat="server" DataTextField="Simbolo" DataValueField="Id"
                                                                Enabled="False" DataSource='<%# DataBinder.Eval(Container.DataItem,"ListaMoneda") %>'
                                                                SelectedValue='<%# DataBinder.Eval(Container.DataItem,"IdMoneda") %>'>
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="cprov_CargoMax" DataFormatString="{0:F3}" HeaderText="L�nea Cr�dito" />
                                                    <asp:BoundField DataField="cprov_Saldo" DataFormatString="{0:F3}" HeaderText="Cr�dito Disponible" />
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <HeaderStyle CssClass="GrillaCabecera" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <table>
                                                <tr>
                                                    <td class="SubTituloCelda" colspan="4">
                                                        DATOS DEL CONTACTO
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Texto">
                                                        Nombre:
                                                    </td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddl_contactoProveedor" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="Texto">
                                                        Cargo:
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="tbcontactocargo" runat="server" CssClass="TextBoxReadOnly"
                                                            Width="250px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="Texto">
                                                        Tel&eacute;fono:
                                                    </td>
                                                    <td colspan="3" align="left">
                                                        <asp:DropDownList ID="ddl_contactoTelefono" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCeldaLeft">
                                LISTADO DE PRODUCTOS
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="PanelCodBarrasBackground">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_A.JPG"/>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiarDetalleDocumento" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"/>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" ID="lblCodbarraFabricante" Text="Codigo Barra Fabricante"></asp:Label>
                                        </td>
                                        <td class="TextBox_CodigoBarras">
                                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnAceptarCodBarraFabr">                                            
                                            <asp:TextBox ID="txtCodBarraFabricante" runat="server"></asp:TextBox>
                                            </asp:Panel>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAceptarCodBarraFabr" runat="server" Text="Aceptar" />
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rbigv" runat="server" CssClass="Texto" RepeatDirection="Horizontal"
                                                Style="cursor: hand;">
                                                <asp:ListItem Value="1" onClick="return(validarIgv());">Incluye IGV</asp:ListItem>
                                                <asp:ListItem Value="0" onClick="return(validarIgv());">No Incluye IGV</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                              <asp:Button ID="btnInafecto" runat="server" Text="Inafecto"  OnClientClick="return  (inafecto());"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="gvProducto" runat="server" AutoGenerateColumns="False" Width="100%"
                                    HeaderStyle-Height="25px" RowStyle-Height="25px">
                                    <RowStyle CssClass="GrillaRow" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>

                                                            <asp:LinkButton CssClass="Texto" ID="lkbQuitar" Font-Bold="true" runat="server" OnClick="lkbQuitar_Click">Quitar</asp:LinkButton>

                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo" />
                                        <asp:BoundField DataField="StockDisponible" DataFormatString="{0:F2}" HeaderText="Stock Disponible" />
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Cantidad">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtcant" runat="server" MaxLength="10" onblur="return(valBlur(event));"
                                                    onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPuntoPositivo('event'));"
                                                    onKeyUp="return(calcularImporte(event,'0'));" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F3}")%>'
                                                    Width="45px"></asp:TextBox>
                                                <asp:HiddenField ID="IdProductoGrid" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>' />
                                                <asp:ImageButton ID="btnMostrarComponenteKit" runat="server" ImageUrl="~/Imagenes/search_add.ico"
                                                    Width="20px" ToolTip="Visualizar Componentes del Kit" OnClick="btnMostrarComponenteKit_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="U.M.">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboUnidadMedida" runat="server" AutoPostBack="True" DataSource='<%# DataBinder.Eval(Container.DataItem,"ListaProdUM") %>'
                                                    SelectedValue='<%# DataBinder.Eval(Container.DataItem,"IdUMedida") %>' DataValueField="IdUnidadMedida"
                                                    DataTextField="NombreCortoUM" OnSelectedIndexChanged="cboUnidadMedida_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdd_CodBarraFab" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"codBarraFabricante") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="�ltimo P. Compra">
                                            <ItemTemplate>
                                                <asp:Label ID="lbSimbP" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NomMoneda") %>'></asp:Label>
                                                <asp:Label ID="lbultimoPrecio" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Detraccion","{0:F3}") %>'></asp:Label>
                                                <asp:HiddenField ID="hdd_idmoneda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdMoneda") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Valor Compra">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtprecioCompra" runat="server" onblur="return(valBlur(event));" onfocus="return(aceptarFoco(this));"
                                                    onKeypress="return(validarNumeroPuntoPositivo('event'));" onKeyUp="return(calcularImporte(event,'4',this));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PrecioSD","{0:F3}")%>' Width="75px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dscto">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtdcto" runat="server" onblur="return(valBlur(event));" onfocus="return(aceptarFoco(this));"
                                                    onKeypress="return(validarNumeroPuntoPositivo('event'));" onKeyUp="return(calcularImporte(event,'2',this));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Descuento","{0:F3}")%>' Width="70px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="% Dscto.">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtpdcto" runat="server" onblur="return(valBlur(event));" onfocus="return(aceptarFoco(this));"
                                                    onKeypress="return(validarNumeroPuntoPositivo('event'));" onKeyUp="return(calcularImporte(event,'1',this));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Pdscto","{0:F2}")%>' Width="50px"></asp:TextBox><asp:HiddenField
                                                        ID="hdd_Descuento1" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"dc_Descuento1") %>' />
                                                <asp:HiddenField ID="hdd_Descuento2" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"dc_Descuento2") %>' />
                                                <asp:HiddenField ID="hdd_Descuento3" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"dc_Descuento3") %>' />
                                                <asp:ImageButton ID="imgPdcto" runat="server" ToolTip="[ Agregar Descuentos ]" ImageUrl="~/Imagenes/Ok_b.bmp"
                                                    OnClientClick="return(capaDescuentos(this));" />
                                            </ItemTemplate>
                                            <ItemStyle Width="90px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P. Final">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtprecio" runat="server" onblur="return(valBlur(event));" onfocus="return(aceptarFoco(this));"
                                                    onKeypress="return(validarNumeroPuntoPositivo('event'));" onKeyUp="return(calcularImporte(event,'3',this));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCD","{0:F3}")%>' Width="75px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Importe">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txttotal" runat="server" CssClass="TextBoxReadOnly_Right" onFocus=" return ( onFocus_Readonly(this) ); "
                                                    onKeyPress=" return ( false ); " onKeyDown=" return ( false ); " Text='<%#DataBinder.Eval(Container.DataItem,"Importe","{0:F3}")%>'
                                                    Width="90px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Perc. (%)">
                                            <ItemTemplate>
                                                <asp:TextBox ID="gtbpercepcion" runat="server" onfocus="return(aceptarFoco(this));"
                                                    MaxLength="10" onKeyDown="return(validarKeyDownPerc(event));" onKeyPress=" return(validarKeyPressPrec(event));"
                                                    onKeyUp="return(calcularImporte(event,'5'));" Text='<%# DataBinder.Eval(Container.DataItem,"Percepcion","{0:F2}") %>'
                                                    Width="70px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Costo Importaci�n">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCosteoImportacion" runat="server" ForeColor="Red" Font-Bold="true"
                                                    Text='<%# DataBinder.Eval(Container.DataItem,"dc_Costo_Imp","{0:F3}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaCabecera" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Descuento:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDescuento" runat="server"
                                                CssClass="TextBox_ReadOnly">0</asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            Sub - Total:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubTotal" runat="server" CssClass="TextBox_ReadOnly"
                                                Width="142px">0</asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="Texto">
                                            <asp:Label ID="lblIgv" runat="server" Text=""></asp:Label><asp:Label ID="Label86"
                                                runat="server" Text="%"></asp:Label>I.G.V.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtIGV" runat="server"
                                                CssClass="TextBox_ReadOnly" Width="142px">0</asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                        </td>
                                        <td>
                                        </td>
                                        <td class="Texto">
                                            Total:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTotal" runat="server"
                                                CssClass="TextBox_ReadOnly" Width="142px">0</asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            Percepci&oacute;n:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPerc" runat="server" CssClass="TextBox_ReadOnly">0</asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            Total a Pagar:
                                            <asp:Label ID="lblSimboloMoneda1" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_TotalAPagar" runat="server"
                                                CssClass="TextBox_ReadOnly" Width="142px">0</asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCeldaLeft">
                                CONDICIONES COMERCIALES
                            </td>
                        </tr>
                        <tr>
                           <td align="center">
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Condici�n Pago:
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="dlcondicionpago" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Fecha Cancelaci&oacute;n:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtnrodiastbfechaCancelacion" runat="server" MaxLength="4" Width="40px"></asp:TextBox>
                                            <asp:TextBox ID="tbfechaCancelacion" runat="server" CssClass="TextBox_Fecha"
                                                Width="80px"></asp:TextBox><cc1:CalendarExtender ID="cfechacan" runat="server" Format="d"
                                                    TargetControlID="tbfechaCancelacion">
                                                </cc1:CalendarExtender>
                                        </td>
                                        <td class="Texto">
                                            Fecha de Entrega:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtnrodiastxtFechaEntrega" runat="server" MaxLength="4" Width="40px"></asp:TextBox>
                                            <asp:TextBox ID="txtFechaEntrega" runat="server" CssClass="TextBox_Fecha" Width="80px"></asp:TextBox><cc1:CalendarExtender
                                                    ID="txtFechaEntrega_CalendarExtender" runat="server" Format="d" TargetControlID="txtFechaEntrega">
                                                </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            Direcci&oacute;n de Entrega:
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlAlmacen" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" colspan="4">
                                            <asp:TextBox ID="txtDireccionAlmacen" runat="server"
                                                CssClass="TextBoxReadOnly" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Label ID="Label65" runat="server" CssClass="Label" Text="Departamento:"></asp:Label>
                                        </td>
                                        <td style="text-align: left" colspan="5">
                                            <asp:DropDownList ID="cboDepartamento" runat="server" Width="150px" AutoPostBack="True"
                                                Enabled="false" CssClass="Combo">
                                            </asp:DropDownList>
                                            <asp:Label ID="Label66" runat="server" CssClass="Label" Text="Provincia"></asp:Label>
                                            <asp:DropDownList ID="cboProvincia" runat="server" AutoPostBack="True" Width="250px"
                                                Enabled="false" CssClass="Combo">
                                            </asp:DropDownList>
                                            <asp:Label ID="Label67" runat="server" CssClass="Label" Text="Distrito"></asp:Label>
                                            <asp:DropDownList ID="cboDistrito" runat="server" Width="250px" Enabled="false" CssClass="Combo">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <caption>
                                        <br />
                                    </caption>
                                           <tr>
                            <td class="Texto">
                                Condici�n Comercial:
                            </td>
                            <td colspan="4">
                                <asp:DropDownList ID="cboCondicionComercial" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                <asp:Button ID="btcondicioncomercial" runat="server" Style="cursor: hand;" 
                                    Text="Agregar Condici�n Comercial" 
                                    ToolTip="[ Agregar Condici�nes Comercial ]" />
                            </td>
                        </tr>
                        <tr>
                        <td> </td>
                                 <td> <table><tr> 
                                                <td class="Texto">Fecha de BL:</td>
                                                  <td align="left" >
                                            <asp:TextBox ID="TxtNumDias" runat="server" MaxLength="4"></asp:TextBox>
                                            <asp:TextBox ID="TxtFechaNueva" runat="server" CssClass="TextBox_Fecha" Width="80px"></asp:TextBox>
                                                <cc1:CalendarExtender
                                                    ID="CalendarExtender1" runat="server" Format="d" TargetControlID="TxtFechaNueva">
                                                </cc1:CalendarExtender>
                                        </td>
                                          
                                            </tr> 
                                            </table>                       
                                 </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="center" colspan="4">
                                <asp:GridView ID="gvCondicion" runat="server" AutoGenerateColumns="False" 
                                    HeaderStyle-Height="25px" RowStyle-Height="25px" Width="100%">
                                    <RowStyle CssClass="GrillaRow" />
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                        <asp:TemplateField HeaderText="Descripci�n">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="gv_hdd_IdCondicionComercial" runat="server" 
                                                    Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                                  <asp:HiddenField ID="hddFlag" runat="server" 
                                                    Value='<%# DataBinder.Eval(Container.DataItem,"FlagCC") %>' />   
                                                <asp:Label ID="gv_lbl_Descripcion" runat="server" 
                                                    Text='<%# DataBinder.Eval(Container.DataItem,"Descripcion") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Ref.">
                                            <ItemTemplate>
                                                            <asp:TextBox ID="txtnrodias" runat="server" MaxLength="4" 
                                                                onFocus="return ( aceptarFoco(this)  );" 
                                                                onKeyPress=" return(   validarNumeroPuntoSigno(event)   );   " 
                                                                onKeyUp=" return(   validarNrodiasCondicionComercial(this)   ); " 
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"NroDiasCondi") %>'
                                                                Width="40px" ></asp:TextBox>

                                                            <asp:TextBox ID="gv_txt_Fecha" runat="server" BackColor="#FFFFCC" 
                                                                Font-Bold="true" ForeColor="#FF0000" onblur="return(valFecha_Blank(this));" 
                                                                onFocus=" return ( val_CondicionComercialFecha(this) ); " 
                                                                onKeyUp=" return ( val_CondicionComercialFecha(this) ); " 
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"dcc_fecha") %>' Width="80px"></asp:TextBox>

                                                            <cc1:CalendarExtender ID="CalendarExtender_CC" runat="server" Format="d" 
                                                                TargetControlID="gv_txt_Fecha">
                                                            </cc1:CalendarExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaCabecera" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                                </table>
                            </td>
                        </tr>
                     
                    </table>
                    </asp:Panel>
                    </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="TituloCelda">
                            OBSERVACIONES
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtobservacion" runat="server" Height="50px" MaxLength="450"
                                TextMode="MultiLine" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    </table>
               
      <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
      </table>
    <table style="width: 100%">
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btn_enviar" runat="server" Text="Enviar" Style="width: 85px; cursor: Hand;"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="TituloCeldaLeft">
                            enviar correo
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="Texto" style="width: 20%; height: 20px; font-weight: bold;">
                                        Usuario:
                                    </td>
                                    <td style="width: 100%;">
                                        <asp:DropDownList ID="ddl_usuario" runat="server" Width="432px">
                                        </asp:DropDownList>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Texto" style="width: 20%; height: 20px; font-weight: bold;">
                                        Contacto Proveedor:
                                    </td>
                                    <td style="width: 100%;">
                                        <asp:DropDownList ID="ddl_contact_supplier_mail" runat="server" Width="432px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Texto" style="width: 20%; height: 20px; font-weight: bold;">
                                        Asunto:
                                    </td>
                                    <td style="width: 100%;">
                                        <asp:TextBox ID="txt_Asunto" runat="server" Width="432px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Texto" style="width: 20%; height: 20px; font-weight: bold;" valign="top">
                                        Mensaje:
                                    </td>
                                    <td style="width: 100%;">
                                        <asp:TextBox ID="txt_mensaje" runat="server" Height="216px" TextMode="MultiLine"
                                            Width="432px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddidObs" runat="server" Value="0" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" Value="0" />
                <asp:HiddenField ID="hddIGV" runat="server" />
                <asp:HiddenField ID="hddOperativo" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_Idproveedor" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_perc" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_IdDocRelacionado" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_MovCuenta" runat="server" Value="0" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_IdTipoDocumento" runat="server" Value="16" />
                <asp:HiddenField ID="hdd_IdUsuario" runat="server" Value="0" />
            </td>
        </tr>
    </table>
     <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="pnlRazpnApe" runat="server" DefaultButton="btnBuscarPersonaGrilla" >
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this,event));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel6" runat="server" DefaultButton="btnBuscarPersonaGrilla" >
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                    </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>                                                                    
                                                                    <asp:Panel ID="Panel7" runat="server" DefaultButton="btnBuscarPersonaGrilla" >
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                    </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnBuscarPersonaGrilla" runat="server" Text="Buscar" CssClass="btnBuscar"
                                                                             OnClientClick="this.disabled=true;this.value='Procesando'" UseSubmitBehavior="false" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddl_Rol" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox>
                                                                <asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="verBusquedaProveedor" style="border: 3px solid blue; padding: 2px; width: 900px;
        height: auto; position: absolute; top: 200px; left: 25px; background-color: white;
        z-index: 2; display: none;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="imgCerraCapaProveedor" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                    AutoPostBack="false">
                                    <asp:ListItem Value="N">Natural</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Buscar Proveedor:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbcondicion" runat="server" RepeatDirection="Horizontal"
                                    CssClass="Texto" Style="cursor: hand;" onClick="return ( onChange_txtBuscarProveedorFocus() );">
                                    <asp:ListItem Value="1" Text="Nacional" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Extranjero"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>                                      
                        <tr>
                            <td class="Texto">
                                Razon Social / Nombres:
                            </td>
                            <td colspan="2">
                                <asp:Panel ID="pnlBuscarProveedor" runat="server" DefaultButton="btBuscarProveedor">                                
                                    <asp:TextBox ID="txtBuscarProveedor" runat="server" Width="400px"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                R.U.C.:
                            </td>
                            <td>
                               <asp:Panel ID="pnlBuscarRucProve" runat="server" DefaultButton="btBuscarProveedor">                                    
                                    <asp:TextBox ID="txtbuscarRucProveedor" runat="server" MaxLength="11"></asp:TextBox>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Button ID="btBuscarProveedor" runat="server" Text="Buscar" Style="cursor: hand;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="gvProveedor" runat="server" AutoGenerateColumns="False" HeaderStyle-Height="25px"
                        Width="100%" RowStyle-Height="25px">
                        <Columns>
                            <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="True">
                            </asp:CommandField>
                            <asp:BoundField DataField="idpersona" HeaderText="C�d." NullDisplayText="0"></asp:BoundField>
                            <asp:BoundField DataField="RazonSocial" HeaderText="Raz�n Social" NullDisplayText="---" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---" />
                            <asp:BoundField DataField="Direccion" HeaderText="Direcci�n" NullDisplayText="---">
                            </asp:BoundField>
                            <asp:BoundField DataField="Telefeono" HeaderText="Tel�fono" NullDisplayText="---" />
                            <asp:BoundField DataField="Correo" HeaderText="Correo" NullDisplayText="---">
                                <ItemStyle />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddidagente" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTipoAgente") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Button ID="btAnterior_Proveedor" runat="server" Width="50px" Text="<" ToolTip="P�gina Anterior"
                         Style="cursor: hand;" />
                    <asp:Button ID="btSiguiente_Proveedor" runat="server" Width="50px" Text=">" ToolTip="P�gina Posterior"
                        Style="cursor: hand;" />
                    <asp:TextBox ID="tbPageIndex_Proveedor" Width="50px" CssClass="TextBoxReadOnly_Right"
                        runat="server"></asp:TextBox><asp:Button ID="btIr_Proveedor" runat="server" Width="50px"
                            Text="Ir" ToolTip="Ir a la P�gina"
                            Style="cursor: hand;" />
                    <asp:TextBox ID="tbPageIndexGO_Proveedor" Width="50px" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="verBusquedaProducto" style="border: 3px solid blue; padding: 2px; width: 900px;
        height: auto; position: absolute; top: 400px; left: 35px; background-color: white;
        z-index: 2; display: none;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="imgCerrarCapaProducto" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.gif" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Existencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                L&iacute;nea:
                            </td>
                            <td>
                                <asp:DropDownList ID="cmbLinea_AddProd" runat="server" DataTextField="Nombre" AutoPostBack="true"
                                    DataValueField="Id">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                SubL&iacute;nea:
                            </td>
                            <td>
                                <asp:DropDownList ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    AutoPostBack="true" DataValueField="Id">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Descripci&oacute;n:
                            </td>
                            <td colspan="3">
                                <asp:Panel ID="PnltxtDescrip" runat="server" DefaultButton="btBuscarGrilla_AddProd">                                
                                <asp:TextBox ID="txtDescripcionProd_AddProd" runat="server" Width="300px"></asp:TextBox>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Button ID="btBuscarGrilla_AddProd" runat="server" Text="Buscar" Style="cursor: hand;" CssClass="btnBuscar"
                                    Width="80px" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                            </td>
                            <td>
                                <asp:Button ID="btAddProductos_AddProd" runat="server" Text="Agregar" CssClass="btnA�adir"
                                    Style="cursor: hand;" Width="80px" />
                            </td>
                            <td>
                                <asp:Button ID="Button2" runat="server" Text="Limpiar" CssClass="btnLimpiar"
                                    Style="cursor: hand;" Width="80px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                C�d.:
                            </td>
                            <td>
                            <asp:Panel ID="pnlCodigoProducto" runat="server" DefaultButton="btBuscarGrilla_AddProd">
                                <asp:TextBox ID="txtCodigoProducto" Width="90px" runat="server" TabIndex="205"></asp:TextBox>
                                </asp:Panel>
                            </td>
                            <td class="Texto">
                                C�d.Proveedor:
                            </td>
                            <td>
                            <asp:Panel ID="pnlTxtCodigoProveedor" runat="server" DefaultButton="btBuscarGrilla_AddProd">                            
                                <asp:TextBox ID="txtCodigoProveedor" Width="90px" runat="server" TabIndex="205"></asp:TextBox>
                                </asp:Panel>
                            </td>
                            <td colspan="2">
                                <asp:CheckBox ID="ckAllProducts" runat="server" Text="Solo Producto - Proveedor"
                                    CssClass="Label" Style="cursor: hand;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label><asp:Panel
                        ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" Style="cursor: hand;" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" Style="cursor: hand;" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <RowStyle CssClass="GrillaRow" />
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label><asp:HiddenField
                                                        ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px"
                                                        onChange="onChange_txtDescripcionProd_AddProd();">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaCabecera" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ComponenteKit_Find" runat="server" AutoGenerateColumns="false">
                        <Columns>
                            <asp:BoundField DataField="CodigoProd_Comp" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                ItemStyle-Font-Bold="true" />
                            <asp:BoundField DataField="Componente" HeaderText="Componente" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                ItemStyle-Font-Bold="true" />
                            <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCantidad_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad_Comp","{0:F3}")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUnidadMedida_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida_Comp")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Precio" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMonedaPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaPrecio_Comp")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Precio_Comp","{0:F3}")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel5" runat="server" DefaultButton="btAddProductos_AddProd">
                    <asp:GridView ID="gvListaProducto" runat="server" AutoGenerateColumns="False" PageSize="20"
                        Style="margin-top: 1px">
                        <RowStyle CssClass="GrillaRow" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:TextBox ID="gtbcantidad"  TabIndex="250"
                                        onKeypress="return( onKeyPress_GV_Producto_Cant(event) );"
                                        onblur="return( valBlurClear('0.00',event) );" onfocus="return(read_Lista(this));"
                                        runat="server" Width="50px"></asp:TextBox>
                                        <asp:HiddenField ID="IdProductoLista" runat="server"
                                        Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>'  />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo" />
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" NullDisplayText="---" />
                            <asp:BoundField DataField="Cod_Proveedor" HeaderText="C�d.Prov." />
                            <asp:BoundField DataField="StockDisponibleN" HeaderText="Stock Disponible" NullDisplayText="---"
                                DataFormatString="{0:F2}" />
                            <asp:TemplateField HeaderText="U. Medida">
                                <ItemTemplate>
                                    <asp:Label ID="lbnomMedida" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NomTipoPV") %>'></asp:Label>
                                    <asp:HiddenField ID="hdd_IdUnidadMedida" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdUnidadMedida") %>' />
                                    <asp:HiddenField ID="hdd_CodBarraFabCapBus" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"codBarraFabricante") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="SimbMoneda" HeaderText="" NullDisplayText="---" ItemStyle-ForeColor="Red"
                                ItemStyle-Font-Bold="true" />
                            <asp:BoundField DataField="PrecioSD" HeaderText="P. Compra" NullDisplayText="0" DataFormatString="{0:F3}"
                                ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true" />
                            <asp:BoundField DataField="NomLinea" HeaderText="L�nea" NullDisplayText="---" />
                            <asp:BoundField DataField="NomSubLinea" HeaderText="SubL�nea" NullDisplayText="---" />
                            <asp:BoundField DataField="NoVisible" HeaderText="Origen" NullDisplayText="---" />
                            <asp:BoundField DataField="Percepcion" HeaderText="Perc. (%)" NullDisplayText="0"
                                DataFormatString="{0:F2}" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btverhistorial" runat="server" ImageUrl="~/Imagenes/Ok_b.bmp"
                                        OnClick="btverhistorial_Click" ToolTip="Ver historial del Producto." />
                                    <asp:ImageButton ID="btnMostrarComponenteKit_Find" runat="server" ImageUrl="~/Imagenes/search_add.ico"
                                        Width="20px" ToolTip="Visualizar Componentes del Kit" OnClick="btnMostrarComponenteKit_Find_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Button ID="btnAnterior_Productos" runat="server" Width="50px" Text="<" ToolTip="P�gina Anterior" Style="cursor: hand;" />
                    <asp:Button ID="btnPosterior_Productos" runat="server" Width="50px" Text=">" ToolTip="P�gina Posterior"
                        Style="cursor: hand;" />
                    <asp:TextBox ID="txtPageIndex_Productos" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                        runat="server"></asp:TextBox><asp:Button ID="btnIr_Productos" runat="server" Width="50px"
                            Text="Ir" ToolTip="Ir a la P�gina"
                            Style="cursor: hand;" />
                    <asp:TextBox ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="verhistorial" style="border: 3px solid blue; padding: 8px; width: 600px;
        height: auto; position: fixed; top: 450px; left: 90px; background-color: white;
        z-index: 6; display: none;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.gif"/>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblPrecio" ForeColor="Red" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="gvPrecio" runat="server" AutoGenerateColumns="False" PageSize="20"
                        EmptyDataText="No se encontraron registros" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="CantidadxAtenderText" HeaderText="Fecha de Registro" />
                            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" NullDisplayText="---"
                                DataFormatString="{0:F2}" />
                            <asp:BoundField DataField="Moneda" HeaderText="Moneda" NullDisplayText="---" />
                            <asp:BoundField DataField="PrecioCD" HeaderText="Precio" NullDisplayText="---" DataFormatString="{0:F3}" />
                            <asp:BoundField DataField="Descuento" HeaderText="Descuento" NullDisplayText="---"
                                DataFormatString="{0:F3}" />
                            <asp:BoundField DataField="dc_Descuento1" HeaderText="Dscto 1" NullDisplayText="---"
                                DataFormatString="{0:F2}" />
                            <asp:BoundField DataField="dc_Descuento2" HeaderText="Dscto 2" NullDisplayText="---"
                                DataFormatString="{0:F2}" />
                            <asp:BoundField DataField="dc_Descuento3" HeaderText="Dscto 3" NullDisplayText="---"
                                DataFormatString="{0:F2}" />
                        </Columns>
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <HeaderStyle CssClass="GrillaCabecera" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDescuentos" style="border: 3px solid blue; padding: 3px; width: 390px;
        height: auto; position: absolute; top: 550px; left: 150px; background-color: white;
        z-index: 2; display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton3" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.gif"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblproductodscto" Text="" runat="server" CssClass="Texto"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td class="Texto">
                                Valor Compra:
                            </td>
                            <td>
                                <asp:TextBox ID="txtProdDscto" runat="server" CssClass="TextBoxReadOnly"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Descuento 1:
                            </td>
                            <td>
                                <asp:Panel ID="Panel2" runat="server" DefaultButton="btAceptarDscto">
                                <asp:TextBox ID="tbdescuento1" runat="server" MaxLength="4"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Descuento 2:
                            </td>
                            <td>
                            <asp:Panel ID="Panel3" runat="server" DefaultButton="btAceptarDscto">
                                <asp:TextBox ID="tbdescuento2" runat="server" MaxLength="4"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Descuento 3:
                            </td>
                            <td>
                            <asp:Panel ID="Panel4" runat="server" DefaultButton="btAceptarDscto">
                                <asp:TextBox ID="tbdescuento3" runat="server" MaxLength="4"></asp:TextBox>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="btAceptarDscto" runat="server" Text="Aceptar"
                                    Style="cursor: hand;" />
                                <asp:HiddenField ID="hddIdProdDscto" runat="server" Value="0" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="verBusquedaKit" style="border: 3px solid blue; padding: 2px; width: 626px;
        height: auto; position: absolute; top: 458px; left: 139px; background-color: white;
        z-index: 2; display: NONE;">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="imgCerraCapaKit" runat="server" ImageUrl="~/Imagenes/Cerrar.gif"/>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td class="Texto">
                                Descripcion:
                            </td>
                            <td align="left">
                                <asp:Label ID="lb_Kit" CssClass="LabelRojo" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Precio:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="tb_PrecioComp" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="GV_ComponenteKit" runat="server" AutoGenerateColumns="False" Width="90%">
                        <Columns>
                            <asp:BoundField DataField="CodigoProd_Comp" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                ItemStyle-Font-Bold="true">
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle Font-Bold="True" Height="25px" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Componente" HeaderText="Componente" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                ItemStyle-Font-Bold="true">
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle Font-Bold="True" Height="25px" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCantidad_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad_Comp","{0:F3}")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUnidadMedida_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida_Comp")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hddIdComponente" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdComponente")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle Font-Bold="True" Height="25px" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Precio" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMonedaPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaPrecio_Comp")%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtprecio_comp" runat="server" onKeyUp="onKeyUp_CalcularComponente('1',event);"
                                                    onFocus="aceptarFoco(this);" Text='<%# DataBinder.Eval(Container.DataItem,"Precio_Comp","{0:F3}") %>'></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle Font-Bold="True" Height="25px" HorizontalAlign="Center" />
                            </asp:TemplateField>
                                <asp:TemplateField HeaderText="Porcentaje" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblPorcentaje" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PorcentajeKit")%>'></asp:Label>
                                            </td>
                                        
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                <ItemStyle Font-Bold="True" Height="25px" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td class="Texto">
                                Valor de los Componentes:
                            </td>
                            <td>
                                <asp:TextBox ID="tb_PrecComponente" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                    Font-Bold="true" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="btn_Ajustar_Precio" runat="server" Text="Ajustar Precios" />
                    <asp:Button ID="btn_Aceptar_Precio_Kit" runat="server" Text="Aceptar" />
                    <asp:HiddenField ID="hdd_IdKit_find" runat="server" Value="0" />
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript" language="javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        //        Ninguno = 1
        //        GuardarNuevo = 2
        //        Actualizar = 3
        function validarNumeroMayor(objeto) {
            var valor = objeto.valor;
            if (parseFloat(valor) < 2) {
                return false;
            }
        }

        function imprimirDocumento02() {
            var idDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>').value;
            //window.open('Reportes/visorCompras.aspx?iReporte=1&IdDoc=' + idDocumento, 'Compras', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=99&IdDocumento=' + idDocumento, 'Compras', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }
        function mostrarCapaProveedor() {
            var rbtipodeoc = document.getElementById('<%=rbtipodeoc.ClientID %>');
            var ctrltipodeoc = rbtipodeoc.getElementsByTagName('input');
            var IdTipoOrden = '0';
            for (var i = 0; i < ctrltipodeoc.length; i++) {
                if (ctrltipodeoc[i].checked == true) {
                    IdTipoOrden = ctrltipodeoc[i].value;
                }
            }

            if (IdTipoOrden == '0') {
                alert('SELECCIONE UN TIPO DE ORDEN DE COMPRA.\nNO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var grilla = document.getElementById('<%=gvProducto.ClientID %>');
            if (grilla != null) {
                alert('SE HAN INGRESADO REGISTROS A LA CUADRICULA DE PRODUCTO.\nNO SE PERMITE LA OPERACI�N..');
                return false;
            }
            onCapa('verBusquedaProveedor');
            var tb = document.getElementById('<%=txtBuscarProveedor.ClientID %>');
            tb.focus();
            tb.select();
            return false;
        }

        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function mostrarCapaListaProducto() {
            var idproveedor = document.getElementById('<%=hdd_Idproveedor.ClientID %>');

            var idarea = document.getElementById('<%=dlarea.ClientID %>');
            if (idarea.value == '0' || idarea == '') {
                alert('DEBE SELECCIONAR UN AREA');
                return false;
            }
            if (idproveedor.value == '0') {
                alert('NO HA INGRESADO A UN PROVEEDOR.');
                return false;
            }

            var ControlIgv = document.getElementById('<%=rbigv.ClientID %>');
            var RadioIgv = ControlIgv.getElementsByTagName('input');

            var cont = 0;
            for (var RunIgv = 0; RunIgv < RadioIgv.length; RunIgv++) {
                if (RadioIgv[RunIgv].checked == true) {
                    cont = cont + 1
                }
            }
            if (cont == 0) {
                alert('SELECCIONE [ INCLUYE IGV ] O  [ NO INCLUYE IGV ].');
                return false;
            }

            return true;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function valNavegacionProveedor(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex_Proveedor.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtBuscarProveedor.ClientID%>').select();
                document.getElementById('<%=txtBuscarProveedor.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO_Proveedor.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function valNavegacionProductos(tipoMov) {

            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        function valAddProductos() {
            var grilla = document.getElementById('<%=gvListaProducto.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert("LA CUADRICULA DE PRODUCTOS NO CONTIENE REGISTROS");
                return false;
            } else {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (parseFloat(rowElem.cells[0].children[0].value) > 0) {
                        cont = cont + 1;
                    }
                }
                if (cont == 0) {
                    alert("INGRESE UNA CANTIDAD MAYOR A CERO");
                    return false;
                }
            }
            return true;
        }
        ///
        function validarBuscarDocumento() {
            var caja = document.getElementById('<%=tbCodigoDocumento.ClientID %>');
            if (CajaEnBlanco(caja)) {
                alert('INGRESE UN C�DIGO DE BUSQUEDA.');
                caja.focus();
                return false;
            }
            var cbo = document.getElementById('<%=dlSerie.ClientID %>');
            if (cbo.value == '') {
                alert("GENERE UNA SERIE PARA REALIZAR UNA BUSQUEDA.");
                return false;
            }
            return true;
        }
        //////////////////////////////////////////////////////////////////////
        function valSaveDocumento() {

            var cbo = document.getElementById('<%=dlSerie.ClientID %>');
            if (cbo.value == '') {
                alert("EL DOCUMENTO NO HA GENERADO UNA SERIE.");
                return false;
            }

            //*************** Validamos los productos
            var grilla = document.getElementById('<%=gvProducto.ClientID %>');
            if (grilla == null) {
                alert('LA CUADRICULA DE PRODUCTOS NO CONTIENE REGISTROS.');
                return false;
            } else {
                //************* Validamos las cantidades
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    var cantidad = redondear(parseFloat(rowElem.cells[4].children[0].value), 3);
                    if (isNaN(cantidad)) {
                        cantidad = 0;
                    }
                    var precio = redondear(parseFloat(rowElem.cells[10].children[0].value), 3);
                    if (isNaN(precio)) {
                        precio = 0;
                    }
                    if (cantidad == 0) {
                        alert('INGRESE UNA CANTIDAD MAYOR A CERO PARA EL PRODUCTO ' + rowElem.cells[3].innerHTML + '.');
                        rowElem.cells[4].children[0].select();
                        rowElem.cells[4].children[0].focus();
                        return false;
                    }

//                    if (rowElem.cells[5].children[0].selectedIndex != 0) {
//                        alert('SELECCIONE LA UNIDAD DE MEDIDA PRINCIPAL PARA EL PRODUCTO ' + rowElem.cells[3].innerHTML + '.');
//                        return false;
//                    }

                    //if (precio == 0) {
                    //alert('INGRESE SU PRECIO DE ' + rowElem.cells[3].innerHTML + '.');
                    //rowElem.cells[10].children[0].select();
                    //rowElem.cells[10].children[0].focus();
                    //return false;
                    //}


                }
            }

            var idarea = document.getElementById('<%=dlarea.ClientID %>');
            if (idarea.value == '0' || idarea == '') {
                alert('DEBE SELECCIONAR UN �REA.');
                return false;
            }

            //****************** Validamos al proveedor
            var idproveedor = document.getElementById('<%=hdd_Idproveedor.ClientID %>');
            if (idproveedor.value == '0') {
                alert('DEBE INGRESAR UN PROVEEDOR PARA EL DOCUMENTO.');
                return false;
            }

            var fechaentrega = document.getElementById('<%=txtFechaEntrega.ClientID %>');
            if (validarFecha(fechaentrega, 'ENTREGA DE PRODUCTOS') == false) {
                return false;
            }

            var fechaemi = document.getElementById('<%=tbFechaEmision.ClientID %>');
            if (compararFechas(fechaemi, fechaentrega)) {
                alert('LA FECHA DE ENTREGA DE PRODUCTOS.\nNO PUEDE SER MENOR A LA FECHA DE EMISI�N DEL DOCUMENTO.');
                return false;
            }

            var IdAlmacen = parseInt(document.getElementById('<%=ddlAlmacen.ClientID %>').value);
            if (isNaN(IdAlmacen)) { IdAlmacen = 0; }

            if (IdAlmacen <= 0) {
                alert('SELECCIONE EL ALMAC�N A RECEPCIONAR LA MERCADERIA. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var tbcodigo = document.getElementById('<%=tbCodigoDocumento.ClientID %>');
            if (tbcodigo.value == '') {
                alert('EL NUMERO DEL DOCUMENTO NO HA SIDO GENERADO.');
                return false;
            }

            var FechaVen = document.getElementById('<%=tbFechaVen.ClientID %>');
            if (validarFecha(FechaVen, 'DE VENCIMIENTO') == false) {
                return false;
            }

            var FechaCan = document.getElementById('<%=tbfechaCancelacion.ClientID %>');
            if (validarFecha(FechaCan, 'DE CANCELAMIENTO') == false) {
                return false;
            }

            if (compararFechas(fechaemi, FechaVen)) {
                alert('LA FECHA DE VENCIMIENTO.\nNO PUEDE SER MENOR A LA FECHA DE EMISI�N DEL DOCUMENTO.');
                return false;
            }

            var idcondicionpago = document.getElementById('<%=dlcondicionpago.ClientID %>');
            var idmoneda = document.getElementById('<%=dlmoneda.ClientID %>');
            var totalapagar = document.getElementById('<%=txttotal.ClientID %>');
            var propietario = document.getElementById('<%=dlpropietario.ClientID %>');

            var grillaCondicion = document.getElementById('<%=gvCondicion.ClientID %>');
            if (grillaCondicion != null) {

                for (var j = 1; j < grillaCondicion.rows.length; j++) {
                    var rowElemt = grillaCondicion.rows[j];
                    var existe = 0;
                    for (var k = 1; k < grillaCondicion.rows.length; k++) {
                        var rowElem = grillaCondicion.rows[k];
                        if (rowElem.cells[1].innerText == rowElemt.cells[1].innerText) {
                            existe = existe + 1;
                        }
                        if (existe > 1) {
                            alert('HA INGRESADO CONDICIONES COMERCIALES IGUALES.');
                            return false;
                        }
                    }

                    if (rowElemt.cells[2].children[1].lenght > 0) {
                        if (compararFechas(fechaemi, rowElemt.cells[2].children[1])) {
                            alert('LA FECHA DE LA CONDICI�N COMERCIAL ' + rowElemt.cells[1].innerText + '.\nNO PUEDE SER MENOR A LA FECHA DE EMISI�N DEL DOCUMENTO.');
                            return false;
                        }
                    }

                }
            }

            // --- editar
            var estadoDoc = document.getElementById('<%=dlEstadoDocumento.ClientId %>');
            var estadoCan = document.getElementById('<%=dlEstadoCancelacion.ClientID %>');
            var estadoEnt = document.getElementById('<%=dlEstadoEntrega.ClientID %>');
            var existeRelacion = document.getElementById('<%=hdd_IdDocRelacionado.ClientID %>');
            var existeCancParcial = document.getElementById('<%=hdd_MovCuenta.ClientID %>');

            if (estadoEnt.value == '2') {
                alert('Se ha recepcionado productos, no se puede modificar el documento');
                return false;
            }
            if (estadoCan.value == '2') {
                alert('El documento esta cancelado');
                return false;
            }
            if (estadoDoc.value != '1') {
                alert('El documento tiene un estado que no permite la edici�n');
                return false;
            }

            if (parseInt(existeCancParcial.value) > 0) {
                alert('El documento ha sido cancelado parcialmente');
                return false;
            }

            var mensaje = 'DESEA CONTINUAR CON LA OPERACI�N?'
            if (confirm(mensaje)) {
                document.getElementById('<%=btguardar.ClientID %>').style.visibility = 'hidden';
                return true;
            }
            else {
                document.getElementById('<%=btguardar.ClientID %>').style.visibility = 'visible';
                return false;
            }
        }
        /////////////////////////////////////////////////////////////////////
        function validarEditar() {

            return true;
        }
        /////////////////////////////////////////////////////////////////////
        function imprimirDocumento() {
            var idDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>').value;
            //window.open('Reportes/visorCompras.aspx?iReporte=1&IdDoc=' + idDocumento, 'Compras', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=16&IdDocumento=' + idDocumento, 'Compras', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }
        /////////////////////////////////////////////////////////////////////
        function validarImportacion(control) {
            var tipodeoc = document.getElementById('<%=rbtipodeoc.ClientID %>');
            if (tipodeoc.value == 'True') {
                alert('La moneda para la importacion es ' + control.options[control.selectedIndex].text + '.');
                return false
            }
            return true;
        }
        ///////////////////////////////////////////////////////////////////// 
        function buscarContacto() {
            var idproveedor = document.getElementById('<%=hdd_Idproveedor.ClientID %>');
            if (idproveedor.value == '0') {
                alert('Ingrese un Proveedor al documento.');
                return false;
            }
            return true;
        }
        /////////////////////////////////////////////////////////////////////
        function validarckImportacion() {
            var tipoPrecio = document.getElementById('<%=dltipoPrecio.ClientID %>');
            var radio = document.getElementById('<%=rbtipodeoc.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[0].checked == false) {
                alert('No ha seleccionado importaci�n');
                tipoPrecio.selectedIndex = 0;
                return false;
            }
            return true;
        }
        /////////////////////////////////////////////////////////////////////
        function validarFecha(oTxt, mensaje) {
            var bOk = true;
            if (oTxt.value != "") {
                bOk = bOk && (valAno(oTxt));
                bOk = bOk && (valMes(oTxt));
                bOk = bOk && (valDia(oTxt));
                bOk = bOk && (valSep(oTxt));
                if (!bOk) {
                    alert("Fecha inv�lida (dd/mm/yyyy).");
                    oTxt.select();
                    oTxt.focus();
                    return false;
                }
            } else {
                alert("Ingrese una Fecha de " + mensaje + " . (dd/mm/yyyy).");
                oTxt.select();
                oTxt.focus();
                return false;
            }
            return true;
        }
        /////////////////////////////////////////////////////////////////////
        function validarBuscar(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            var boton = document.getElementById('<%=btnBuscarDocumento.ClientID %>');            
            if (key == 13) {
                boton.focus();
                return true;
            }
        }
        /////////////////////////////////////////////////////////////////////
        function compararFechas(fecha, fecha2) {
            var xMonth = fecha.value.substring(3, 5);
            var xDay = fecha.value.substring(0, 2);
            var xYear = fecha.value.substring(6, 10);
            var yMonth = fecha2.value.substring(3, 5);
            var yDay = fecha2.value.substring(0, 2);
            var yYear = fecha2.value.substring(6, 10);

            if (xYear > yYear) {
                return (true)
            }
            else {
                if (xYear == yYear) {
                    if (xMonth > yMonth) {
                        return (true)
                    }
                    else {
                        if (xMonth == yMonth) {
                            if (xDay > yDay)
                                return (true);
                            else
                                return (false);
                        }
                        else
                            return (false);
                    }
                }
                else
                    return (false);
            }
        }
        /////////////////////////////////////////////////////////////////////
        function LimpiarGrillaProductos() {
            var grilla = document.getElementById('<%=gvListaProducto.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    rowElem.cells[0].children[0].value = '0.00';
                } //next
            } //end if
            onChange_txtDescripcionProd_AddProd();
            return false;
        } //end function
        /////////////////////////////////////////////////////////////////////
        function validarKeyPressPrec(e) {
            if (window.event) keyCode = window.event.keyCode;
            else if (e) keyCode = e.which;        
            var perc = document.getElementById('<%=hdd_perc.ClientID %>');
            if (perc.value == '1') {

                if (keyCode == 46 || keyCode == 45) {
                    return true;
                } else {
                    if (!onKeyPressEsNumero('event')) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        ///////////////////////////////////////////////////////////////////
        function validarKeyDownPerc(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;            
            var perc = document.getElementById('<%=hdd_perc.ClientID %>');
            if (perc.value != '1') {
                if (key == 45 || key == 8) {  // 3 es buscar
                    return false;
                }
            }
            return true;
        }

        /////////////////////////////////////////////////////////////////////////
        function calcularOtrosValores(tipo) {
            var grilla = document.getElementById('<%=gvProducto.ClientID %>');
            var igv = parseFloat(document.getElementById('<%=hddigv.ClientID %>').value);
            var _IncluyeIgv;

            var ControlIgv = document.getElementById('<%=rbigv.ClientID %>');
            var RadioIgv = ControlIgv.getElementsByTagName('input');
            for (var RunIgv = 0; RunIgv < RadioIgv.length; RunIgv++) {
                if (RadioIgv[RunIgv].checked == true) {
                    _IncluyeIgv = RadioIgv[RunIgv].value;
                    break;
                }
            }

            var montoIGV = 0;
            var importeTotal = 0;
            var subtotal = 0;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    var importe = redondear(parseFloat(rowElem.cells[11].children[0].value), 3);
                    if (isNaN(importeTotal)) {
                        importeTotal = 0;
                    }
                    importeTotal = importeTotal + importe;
                } //FIM DEL FOR
            }


            var perc = redondear(parseFloat(document.getElementById('<%=txtPerc.ClientID %>').value), 3);
            if (isNaN(perc)) {
                perc = 0;
            }
            var dscto = redondear(parseFloat(document.getElementById('<%=txtDescuento.ClientID %>').value), 3);
            if (isNaN(dscto)) {
                dscto = 0;
            }

            subtotal = importeTotal - dscto;
            if (tipo != '2') {
                montoIGV = subtotal * igv;
            }
            total = subtotal + montoIGV + perc;

            document.getElementById('<%=txtTotal.ClientID %>').value = Format(total, 3);
            document.getElementById('<%=txtSubTotal.ClientID %>').value = Format(subtotal, 3);
            if (tipo != '1') {//Descuento
                document.getElementById('<%=txtDescuento.ClientID %>').value = Format(dscto, 3);
            }
            if (tipo != '2') {//IGV
                document.getElementById('<%=txtIGV.ClientID %>').value = Format(montoIGV, 3);
            }
            if (tipo != '3') {//Percepcion
                document.getElementById('<%=txtPerc.ClientID %>').value = Format(perc, 3);
            }
            return true;
        }
        //////////////////////
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        //////////////////////////////
        function capaDescuentos(obj) {
            var grilla = document.getElementById('<%=gvProducto.ClientID %>');
            var producto = document.getElementById('<%=lblproductodscto.ClientID %>');
            var IdProducto = document.getElementById('<%=hddIdProdDscto.ClientID %>');
            var valorcompra = document.getElementById('<%=txtProdDscto.ClientID %>');

            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (obj.id == rowElem.cells[9].children[4].id) {
                    producto.innerText = rowElem.cells[3].innerText;
                    IdProducto.value = rowElem.cells[4].children[1].value;
                    valorcompra.value = rowElem.cells[7].children[0].value;
                    document.getElementById('<%=tbdescuento1.ClientID %>').value = rowElem.cells[9].children[1].value;
                    document.getElementById('<%=tbdescuento2.ClientID %>').value = rowElem.cells[9].children[2].value;
                    document.getElementById('<%=tbdescuento3.ClientID %>').value = rowElem.cells[9].children[3].value;
                    onCapa('capaDescuentos');
                    return false;
                } //end if
            } //next            
        }
        //////////////////////////////////
        function calcularDsctos() {
            var IdProducto = document.getElementById('<%=hddIdProdDscto.ClientID %>');
            var grilla = document.getElementById('<%=gvProducto.ClientID %>');
            var descuento1 = 0;
            var descuento2 = 0;
            var descuento3 = 0;

            var dscto1 = document.getElementById('<%=tbdescuento1.ClientID %>');
            if (dscto1.value.length > 0) {
                descuento1 = parseFloat(dscto1.value);
            }
            var dscto2 = document.getElementById('<%=tbdescuento2.ClientID %>');
            if (dscto2.value.length > 0) {
                descuento2 = parseFloat(dscto2.value);
            }
            var dscto3 = document.getElementById('<%=tbdescuento3.ClientID %>');
            if (dscto3.value.length > 0) {
                descuento3 = parseFloat(dscto3.value);
            }

            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (rowElem.cells[4].children[1].value == IdProducto.value) {
                    rowElem.cells[9].children[0].value = redondear(parseFloat((1 - (1 - descuento1 / 100) * (1 - descuento2 / 100) * (1 - descuento3 / 100)) * 100), 3);
                    rowElem.cells[9].children[1].value = descuento1;
                    rowElem.cells[9].children[2].value = descuento2;
                    rowElem.cells[9].children[3].value = descuento3;
                    dscto1.value = '0';
                    dscto2.value = '0';
                    dscto3.value = '0';
                    calcularImporteOnLoad();
                    offCapa('capaDescuentos');
                    return false;
                } //end if
            } //next 


        }
        ///////////////////////////////////
        function validarIgv(obj) {
            var control = document.getElementById('<%=rbigv.ClientID %>');
            var radio = control.getElementsByTagName('input');
            console.log(radio);

            var rbtipodeoc = document.getElementById('<%=rbtipodeoc.ClientID %>');
            var ctrltipodeoc = rbtipodeoc.getElementsByTagName('input');

            var grilla = document.getElementById('<%=gvProducto.ClientID %>');
            var valor = '';

            for (var i = 0; i < radio.length; i++) {
                if (radio[i].checked == true) {
                    valor = radio[i].value;
                }
            }

            for (var k = 0; k < ctrltipodeoc.length; k++) {
                if (ctrltipodeoc[k].checked == true) {
                    if (ctrltipodeoc[k].value == 'True' && valor == '1') {
                        alert('El TIPO DE COMPRA ES [ IMPORTADA ].\nNO SE PERMITE LA OPERACI�N.');
                        for (var i = 0; i < radio.length; i++) {
                            if (radio[i].value != valor) {
                                radio[i].checked = true;
                                return false;
                            }
                        }
                    }
                }
            }

            if (grilla != null) {
                alert('LA CUADRICULA DE PRODUCTOS CONTIENE REGISTROS.');
                for (var i = 0; i < radio.length; i++) {
                    if (radio[i].value != valor) {
                        radio[i].checked = true;
                        return false;
                    }
                }
            }

        }
        ////////////////////////////////
        function onKeyPressProveedor(obj,e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btBuscarProveedor.CLientID %>').focus();
            }
            return true;
        }
        /////////////////////////////
        function onkeyPressProducto(obj,e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        //
        function onChange_txtBuscarProveedorFocus() {
            var txtBuscarProveedor = document.getElementById('<%=txtBuscarProveedor.ClientID %>');
            txtBuscarProveedor.focus();
            txtBuscarProveedor.select();
        }
        function onChange_txtDescripcionProd_AddProd() {
            var txtDescripcionProd_AddProd = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID %>');
            txtDescripcionProd_AddProd.focus();
            txtDescripcionProd_AddProd.select();
        }
        function onclickCondicionComercial() {
            var cboCondicionComercial = document.getElementById('<%=cboCondicionComercial.ClientID %>');
            var gvCondicion = document.getElementById('<%=gvCondicion.ClientID %>');
            if (gvCondicion != null) {
                for (var i = 1; i < gvCondicion.rows.length; i++) {
                    var rowElem = gvCondicion.rows[i];
                    if (cboCondicionComercial.value == rowElem.cells[1].children[0].value) {
                        alert('EL ELEMENTO � ' + cboCondicionComercial.options[cboCondicionComercial.selectedIndex].text + ' � YA HA SIDO INGRESADO.');
                        return false;
                    }
                }
            }
            return true;
        }
        //
        function setFocus_tbCodigoDocumento() {
            var tbCodigoDocumento = document.getElementById('<%=tbCodigoDocumento.CLientID %>');
            tbCodigoDocumento.focus();
            tbCodigoDocumento.select();
        }
        //
        function onClick_rbtipodeoc() {
            var grilla = document.getElementById('<%=gvProducto.ClientID %>');
            if (grilla != null) {
                alert('LA CUADRICULA DE PRODUCTOS CONTIENE REGISTROS.\nNO SE PERMITE LA OPERACI�N.');
                return false;
            }
        }
        //
        function onclick_btn_enviar() {
            var IdDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>');
            if (IdDocumento.value == '0') {
                alert('NO HA BUSCADO O GENERADO UN DOCUMENTO.\nNO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var ddl_usuario = document.getElementById('<%=ddl_usuario.ClientID %>');
            if (ddl_usuario.options.length == 0) {
                alert('NO TIENE REGISTRADO CUENTAS DE CORREO.');
                return false;
            }
            var ddl_contact_supplier_mail = document.getElementById('<%=ddl_contact_supplier_mail.ClientID %>');
            if (ddl_contact_supplier_mail.options.length == 0) {
                alert('EL PROVEEDOR O EL CONTACTO DEL PROVEEDOR NO TIENE REGISTRADO CUENTAS DE CORREO.');
                return false;
            }
            var txt_Asunto = document.getElementById('<%=txt_Asunto.ClientID %>');
            if (txt_Asunto.value == '' || txt_Asunto.value.length == 0) {
                alert('DEBE INGRESAR UN ASUNTO PARA EL CORREO.');
                return false;
            }
            return true;

        }
        //
        function read_Lista(obj) {
            var cantidad = 0;
            var grillaListaProducto = document.getElementById('<%=gvListaProducto.ClientID %>');
            var grillaProducto = document.getElementById('<%=gvProducto.ClientID %>');

            for (var i = 1; i < grillaListaProducto.rows.length; i++) {
                var rowListaProducto = grillaListaProducto.rows[i];

                if (rowListaProducto.cells[0].children[0].id == obj.id) {

                    //
                    if (grillaProducto != null) {
                        for (var k = 1; k < grillaProducto.rows.length; k++) {
                            var rowProducto = grillaProducto.rows[k];
                            if (rowListaProducto.cells[0].children[1].value == rowProducto.cells[4].children[1].value) {
                                var cantidad = redondear(parseFloat(rowProducto.cells[4].children[0].value), 3);
                                if (isNaN(cantidad)) {
                                    obj.value = "0.00";
                                    obj.select();
                                    return false;
                                }
                            }
                        }
                    }
                    //

                }
            }

            obj.value = redondear(parseFloat(cantidad), 3);
            obj.select();
            return false;
        }
        //
        function calcularImporte(e,opc, obj) {
            // variables a mostrar
            var _SubTotal = 0;
            var _Descuento = 0;
            var _Igv = 0;
            var _Total = 0;
            var _Percepcion = 0;
            var _TasaPercepcion = 0;
            var _TotalAPagar = 0;
            var _IncluyeIgv;
            var _tipoOC;

            // variables de la aplicacion
            var Igv = parseFloat(document.getElementById('<%=hddigv.ClientID %>').value);
            var ControlIgv = document.getElementById('<%=rbigv.ClientID %>');
            var RadioIgv = ControlIgv.getElementsByTagName('input');



            var grilla = document.getElementById('<%=gvProducto.ClientID %>');

            for (var RunIgv = 0; RunIgv < RadioIgv.length; RunIgv++) {
                if (RadioIgv[RunIgv].checked == true) {
                    _IncluyeIgv = RadioIgv[RunIgv].value;
                    break;
                }
            }

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    var cantidad = parseFloat(rowElem.cells[4].children[0].value);
                    if (isNaN(cantidad)) {
                        cantidad = 0;
                    }
                    var precioCompra = parseFloat(rowElem.cells[7].children[0].value);
                    if (isNaN(precioCompra)) {
                        precioCompra = 0;
                    }
                    var dcto = parseFloat(rowElem.cells[8].children[0].value);
                    if (isNaN(dcto)) {
                        dcto = 0;
                    }
                    var Pdcto = parseFloat(rowElem.cells[9].children[0].value);
                    if (isNaN(Pdcto)) {
                        Pdcto = 0;
                    }
                    var precioFinal = parseFloat(rowElem.cells[10].children[0].value);
                    if (isNaN(precioFinal)) {
                        precioFinal = 0;
                    }
                    var importe = precioFinal * cantidad;
                    if (isNaN(importe)) {
                        importe = 0;
                    }

                    var percepcion = parseFloat(rowElem.cells[12].children[0].value);
                    if (isNaN(percepcion)) {
                        percepcion = 0;
                    }
                    //obtiene srcElement.Id
                    var e = e ? e : window.event;
                    var event_element = e.target ? e.target : e.srcElement;
                    //termina
                    if (rowElem.cells[4].children[0].id == event_element.id || rowElem.cells[7].children[0].id == event_element.id || rowElem.cells[8].children[0].id == event_element.id || rowElem.cells[9].children[0].id == event_element.id || rowElem.cells[10].children[0].id == event_element.id || rowElem.cells[12].children.id == event_element.id) {//cantidad                        
                        switch (opc) {
                            case '0': //cantidad
                                break;

                            case '1': //porcentaje de descuento
                                if (Pdcto >= 100) {
                                    dcto = precioCompra;
                                    Pdcto = parseFloat(100);
                                    rowElem.cells[9].children[0].value = redondear(parseFloat(Pdcto), 3);
                                } else {
                                    dcto = (parseFloat(precioCompra) * (parseFloat(Pdcto) / parseFloat(100)));
                                }

                                if (obj != null) {

                                    if (obj.id == rowElem.cells[9].children[0].id) {
                                        //descuentos sucesivos
                                        rowElem.cells[9].children[1].value = '0';
                                        rowElem.cells[9].children[2].value = '0';
                                        rowElem.cells[9].children[3].value = '0';
                                    }
                                }

                                precioFinal = parseFloat(precioCompra) - parseFloat(dcto);
                                rowElem.cells[8].children[0].value = redondear(parseFloat(dcto), 3);
                                rowElem.cells[10].children[0].value = redondear(parseFloat(precioFinal), 3);
                                break;

                            case '2': //descuento
                                if (dcto > precioCompra) {
                                    dcto = precioCompra;
                                    Pdcto = parseFloat(100);
                                    rowElem.cells[8].children[0].value = redondear(parseFloat(dcto), 3);
                                } else {
                                    Pdcto = (parseFloat(dcto) / parseFloat(precioCompra)) * parseFloat(100);
                                }

                                if (obj != null) {

                                    if (obj.id == rowElem.cells[8].children[0].id) {
                                        //descuentos sucesivos
                                        rowElem.cells[9].children[1].value = '0';
                                        rowElem.cells[9].children[2].value = '0';
                                        rowElem.cells[9].children[3].value = '0';
                                    }
                                }

                                precioFinal = parseFloat(precioCompra) - parseFloat(dcto);
                                rowElem.cells[9].children[0].value = redondear(parseFloat(Pdcto), 3);
                                rowElem.cells[10].children[0].value = redondear(parseFloat(precioFinal), 3);
                                break;
                            case '3': //precio final
                                if (precioFinal > precioCompra) {
                                    dcto = 0;
                                    Pdcto = 0;
                                } else {
                                    dcto = parseFloat(precioCompra) - parseFloat(precioFinal);
                                    Pdcto = (parseFloat(dcto) / parseFloat(precioCompra)) * parseFloat(100);
                                }

                                if (obj != null) {

                                    if (obj.id == rowElem.cells[10].children[0].id) {
                                        //descuentos sucesivos
                                        rowElem.cells[9].children[1].value = '0';
                                        rowElem.cells[9].children[2].value = '0';
                                        rowElem.cells[9].children[3].value = '0';
                                    }
                                }

                                rowElem.cells[8].children[0].value = redondear(parseFloat(dcto), 3);
                                rowElem.cells[9].children[0].value = redondear(parseFloat(Pdcto), 3);
                                break;
                            case '4': //precio de compra

                                if (obj != null) {

                                    if (obj.id == rowElem.cells[7].children[0].id) {

                                        dcto = 0;
                                        Pdcto = 0;
                                        precioFinal = precioCompra;
                                        rowElem.cells[8].children[0].value = redondear(parseFloat(dcto), 3);
                                        rowElem.cells[9].children[0].value = redondear(parseFloat(Pdcto), 3);
                                        //descuentos sucesivos
                                        rowElem.cells[9].children[1].value = '0';
                                        rowElem.cells[9].children[2].value = '0';
                                        rowElem.cells[9].children[3].value = '0';

                                    }

                                }

                                rowElem.cells[10].children[0].value = redondear(parseFloat(precioFinal), 3);
                                break;

                            case '5': //percepcion

                                break;
                        }
                        importe = parseFloat(precioFinal) * parseFloat(cantidad);
                        rowElem.cells[11].children[0].value = redondear(parseFloat(importe), 3);

                        if (percepcion > 0 != opc != '12') {
                            if (parseFloat(_TasaPercepcion) <= 0) { _TasaPercepcion = parseFloat(percepcion); }
                            _Percepcion = parseFloat(_Percepcion) + (parseFloat(importe) * (parseFloat(percepcion) / parseFloat(100)));
                        }
                    }

                    _Descuento = parseFloat(_Descuento) + (parseFloat(dcto) * parseFloat(cantidad));
                    _SubTotal = parseFloat(_SubTotal) + parseFloat(importe);

                } //end for
            } // end if

            if (_IncluyeIgv == '1') {

                _Total = _SubTotal;
                _SubTotal = (parseFloat(_Total)) / ((parseFloat(Igv) * parseFloat(100)) / (parseFloat(100)) + parseFloat(1));
                _Igv = parseFloat(_SubTotal) * parseFloat(Igv);
                _TotalAPagar = parseFloat(_Percepcion) + parseFloat(_Total);

                document.getElementById('<%=txtTotal.ClientID %>').value = Format(parseFloat(_Total), 3);
                document.getElementById('<%=txtSubTotal.ClientID %>').value = Format(parseFloat(_SubTotal), 3);
                document.getElementById('<%=txtIGV.ClientID %>').value = Format(parseFloat(_Igv), 3);
                document.getElementById('<%=txtDescuento.ClientID %>').value = Format(parseFloat(_Descuento), 3);
                document.getElementById('<%=txtPerc.ClientID %>').value = Format(parseFloat(_Percepcion), 3);
                document.getElementById('<%=txt_TotalAPagar.ClientID %>').value = Format(parseFloat(_TotalAPagar), 3);


            } else {

                var rbtipodeoc = document.getElementById('<%=rbtipodeoc.ClientID %>');
                var ctrltipodeoc = rbtipodeoc.getElementsByTagName('input');
                if (ctrltipodeoc[0].checked == true) { // Importada
                    _Percepcion = parseFloat(0);
                    _Igv = parseFloat(0);
                    _Total = parseFloat(_SubTotal);
                    switch (opc) {
                        case '10': //Descuento
                            var _DescuentoHelp = 0;
                            var txtDscto = 0;
                            txtDscto = parseFloat(document.getElementById('<%=txtDescuento.ClientID %>').value);
                            if (isNaN(txtDscto)) { txtDscto = 0; }
                            if (_Descuento > 0) {
                                _DescuentoHelp = txtDscto - _Descuento;
                                _Descuento = _DescuentoHelp;
                            }
                            if (isNaN(_Descuento)) { _Descuento = 0; }
                            _Total = parseFloat(_SubTotal) - _Descuento;
                            break;
                    }

                    _TotalAPagar = parseFloat(_Total);

                    //mostrar resultados
                    document.getElementById('<%=txtTotal.ClientID %>').value = Format(parseFloat(_Total), 3);
                    document.getElementById('<%=txtSubTotal.ClientID %>').value = Format(parseFloat(_SubTotal), 3);
                    document.getElementById('<%=txtIGV.ClientID %>').value = Format(parseFloat(_Igv), 3);
                    if (opc != '10') { document.getElementById('<%=txtDescuento.ClientID %>').value = Format(parseFloat(_Descuento), 3); }
                    document.getElementById('<%=txtPerc.ClientID %>').value = Format(parseFloat(_Percepcion), 3);
                    document.getElementById('<%=txt_TotalAPagar.ClientID %>').value = Format(parseFloat(_TotalAPagar), 3);
                    return false;
                }
                _Igv = parseFloat(_SubTotal) * parseFloat(Igv);
                _Total = parseFloat(_SubTotal) + parseFloat(_Igv);
                if (parseFloat(_TasaPercepcion) > 0) { _Percepcion = parseFloat(_Total) * (parseFloat(_TasaPercepcion) / parseFloat(100)); }
                _TotalAPagar = parseFloat(_Percepcion) + parseFloat(_Total);

                document.getElementById('<%=txtTotal.ClientID %>').value = Format(parseFloat(_Total), 3);
                document.getElementById('<%=txtSubTotal.ClientID %>').value = Format(parseFloat(_SubTotal), 3);
                document.getElementById('<%=txtIGV.ClientID %>').value = Format(parseFloat(_Igv), 3);
                document.getElementById('<%=txtDescuento.ClientID %>').value = Format(parseFloat(_Descuento), 3);
                document.getElementById('<%=txtPerc.ClientID %>').value = Format(parseFloat(_Percepcion), 3);
                document.getElementById('<%=txt_TotalAPagar.ClientID %>').value = Format(parseFloat(_TotalAPagar), 3);
            }

            //************************** imprimo en la secci�n Detalle de Cancelaci�n            
            return false;
        }
        //
        function calcularImporteOnLoad() {
            // variables a mostrar
            var _SubTotal = 0;
            var _Descuento = 0;
            var _Igv = 0;
            var _Total = 0;
            var _Percepcion = 0;
            var _TasaPercepcion = 0;
            var _TotalAPagar = 0;
            var _IncluyeIgv;

            // variables de la aplicacion
            var Igv = parseFloat(document.getElementById('<%=hddigv.ClientID %>').value);
            var ControlIgv = document.getElementById('<%=rbigv.ClientID %>');
            var RadioIgv = ControlIgv.getElementsByTagName('input');
            var grilla = document.getElementById('<%=gvProducto.ClientID %>');

            for (var RunIgv = 0; RunIgv < RadioIgv.length; RunIgv++) {
                if (RadioIgv[RunIgv].checked == true) {
                    _IncluyeIgv = RadioIgv[RunIgv].value;
                    break;
                }
            }

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    var cantidad = parseFloat(rowElem.cells[4].children[0].value);
                    if (isNaN(cantidad)) {
                        cantidad = 0;
                    }
                    var precioCompra = parseFloat(rowElem.cells[7].children[0].value);
                    if (isNaN(precioCompra)) {
                        precioCompra = 0;
                    }
                    var dcto = parseFloat(rowElem.cells[8].children[0].value);
                    if (isNaN(dcto)) {
                        dcto = 0;
                    }
                    var Pdcto = parseFloat(rowElem.cells[9].children[0].value);
                    if (isNaN(Pdcto)) {
                        Pdcto = 0;
                    }
                    var precioFinal = parseFloat(rowElem.cells[10].children[0].value);
                    if (isNaN(precioFinal)) {
                        precioFinal = 0;
                    }
                    var importe = precioFinal * cantidad;
                    if (isNaN(importe)) {
                        importe = 0;
                    }

                    var percepcion = parseFloat(rowElem.cells[12].children[0].value);
                    if (isNaN(percepcion)) {
                        percepcion = 0;
                    }

                    if (Pdcto >= 100) {
                        dcto = precioCompra;
                        Pdcto = parseFloat(100);
                        rowElem.cells[9].children[0].value = redondear(parseFloat(Pdcto), 3);
                    } else {
                        dcto = parseFloat(precioCompra) * (parseFloat(Pdcto) / parseFloat(100));
                    }
                    precioFinal = parseFloat(precioCompra) - parseFloat(dcto);
                    rowElem.cells[8].children[0].value = redondear(parseFloat(dcto), 3);
                    rowElem.cells[10].children[0].value = redondear(parseFloat(precioFinal), 3);

                    importe = parseFloat(precioFinal) * parseFloat(cantidad);
                    rowElem.cells[11].children[0].value = redondear(parseFloat(importe), 3);

                    if (percepcion > 0) {
                        if (parseFloat(_TasaPercepcion) <= 0) { _TasaPercepcion = parseFloat(percepcion); }
                        _Percepcion = parseFloat(_Percepcion) + (parseFloat(importe) * (parseFloat(percepcion) / parseFloat(100)));
                    }
                    _Descuento = _Descuento + (dcto * cantidad);
                    _SubTotal = _SubTotal + importe;

                } //end for
            } // end if

            if (_IncluyeIgv == '1') {

                _Total = _SubTotal;
                _SubTotal = (parseFloat(_Total)) / ((parseFloat(Igv) * parseFloat(100)) / (parseFloat(100)) + parseFloat(1));
                _Igv = parseFloat(_SubTotal) * parseFloat(Igv);
                _TotalAPagar = parseFloat(_Percepcion) + parseFloat(_Total);

                document.getElementById('<%=txtTotal.ClientID %>').value = Format(parseFloat(_Total), 3);
                document.getElementById('<%=txtSubTotal.ClientID %>').value = Format(parseFloat(_SubTotal), 3);
                document.getElementById('<%=txtIGV.ClientID %>').value = Format(parseFloat(_Igv), 3);
                document.getElementById('<%=txtDescuento.ClientID %>').value = Format(parseFloat(_Descuento), 3);
                document.getElementById('<%=txtPerc.ClientID %>').value = Format(parseFloat(_Percepcion), 3);
                document.getElementById('<%=txt_TotalAPagar.ClientID %>').value = Format(parseFloat(_TotalAPagar), 3);


            } else {

                var rbtipodeoc = document.getElementById('<%=rbtipodeoc.ClientID %>');
                var ctrltipodeoc = rbtipodeoc.getElementsByTagName('input');
                if (ctrltipodeoc[0].checked == true) { // Importada
                    _Percepcion = parseFloat(0);
                    _Igv = parseFloat(0);
                    _Total = parseFloat(_SubTotal);
                    _TotalAPagar = parseFloat(_Total);

                    //mostrar resultados
                    document.getElementById('<%=txtTotal.ClientID %>').value = Format(parseFloat(_Total), 3);
                    document.getElementById('<%=txtSubTotal.ClientID %>').value = Format(parseFloat(_SubTotal), 3);
                    document.getElementById('<%=txtIGV.ClientID %>').value = Format(parseFloat(_Igv), 3);
                    document.getElementById('<%=txtDescuento.ClientID %>').value = Format(parseFloat(_Descuento), 3);
                    document.getElementById('<%=txtPerc.ClientID %>').value = Format(parseFloat(_Percepcion), 3);
                    document.getElementById('<%=txt_TotalAPagar.ClientID %>').value = Format(parseFloat(_TotalAPagar), 3);
                    return false;
                }
                _Igv = parseFloat(_SubTotal) * parseFloat(Igv);
                _Total = parseFloat(_SubTotal) + parseFloat(_Igv);
                if (parseFloat(_TasaPercepcion) > 0) { _Percepcion = parseFloat(_Total) * (parseFloat(_TasaPercepcion) / parseFloat(100)); }
                _TotalAPagar = parseFloat(_Percepcion) + parseFloat(_Total);

                document.getElementById('<%=txtTotal.ClientID %>').value = Format(parseFloat(_Total), 3);
                document.getElementById('<%=txtSubTotal.ClientID %>').value = Format(parseFloat(_SubTotal), 3);
                document.getElementById('<%=txtIGV.ClientID %>').value = Format(parseFloat(_Igv), 3);
                document.getElementById('<%=txtDescuento.ClientID %>').value = Format(parseFloat(_Descuento), 3);
                document.getElementById('<%=txtPerc.ClientID %>').value = Format(parseFloat(_Percepcion), 3);
                document.getElementById('<%=txt_TotalAPagar.ClientID %>').value = Format(parseFloat(_TotalAPagar), 3);
            }

            //************************** imprimo en la secci�n Detalle de Cancelaci�n            
            return false;
        }
        //

    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////

        function valOnClick_btnAgregarCodBarras() {
            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            var idproveedor = document.getElementById('<%=hdd_Idproveedor.ClientID %>');
            if (idproveedor.value == '0') {
                alert('NO HA INGRESADO A UN PROVEEDOR.');
                return false;
            }
            var codBarraFabricante = document.getElementById('<%=txtCodBarraFabricante.ClientID%>');

            if (codBarraFabricante.value.length <= 0) {
                alert('no ingreso en codigo de barras');
                return false;
            } else {
                return true;
            }
        }

        function valKeyPressDescripcionProdCodBarras(obj,e) {
            ejecucion = '0';
            var codBarras = document.getElementById('<%=txtCodBarraFabricante.ClientID%>');
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;            
            
            if (key == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                if (codBarras.value.length <= 0) {
                    alert('no se se ingreso Codigo de Barras');
                    return false;
                } else {
                    document.getElementById('<%=btnAceptarCodBarraFabr.ClientID %>').click();
                    return false;
                }
            }
        }
        function val_CodBarras(valor) {
            ejecucion = valor;
            var codBarras = document.getElementById('<%=txtCodBarraFabricante.ClientID %>');
            if (codBarras != null) {
                if (codBarras.value.length > 0) {
                    if (ejecucion == '1') {
                        document.getElementById('<%=btnAceptarCodBarraFabr.ClientID %>').click();
                        return false;
                    }
                }
            }
            setTimeout('val_CodBarras(ejecucion);', 1000);

        }

        function onChange_txtCodBarraFabricante() {
            var txtCodBarraFabricante = document.getElementById('<%=txtCodBarraFabricante.ClientID %>');
            txtCodBarraFabricante.focus();
            txtCodBarraFabricante.select();
        }
        //
        function onClick_Ajustar_Precio() {
            var montoTotalComponente = 0;
            var newMontoTotalComponente = document.getElementById('<%=tb_PrecioComp.ClientID %>');
            var pMontoTotalComponente = 0;
            var grilla = document.getElementById('<%=GV_ComponenteKit.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    rowElem = grilla.rows[i];

                    var precioxComponente = parseFloat(rowElem.cells[3].children[0].cells[1].children[0].value);
                    if (isNaN(precioxComponente)) {
                        precioxComponente = 0;
                    }
                    montoTotalComponente = montoTotalComponente + precioxComponente;

                } //end for

                pMontoTotalComponente = (parseFloat(newMontoTotalComponente.value) - montoTotalComponente) * 100 / montoTotalComponente;

                for (var p = 1; p < grilla.rows.length; p++) {
                    var rowElemx = grilla.rows[p];
                    var newprecio = parseFloat(rowElemx.cells[3].children[0].cells[1].children[0].value);
                    if (isNaN(newprecio)) {
                        newprecio = 0;
                    }
                    if (newprecio != 0) {
                        var ajuste = newprecio + (newprecio * pMontoTotalComponente / 100)

                        if (isNaN(ajuste)) {
                            ajuste = 0;
                        }
                        rowElemx.cells[3].children[0].cells[1].children[0].value = Format(parseFloat(ajuste), 3);
                    }
                }

            } //end if

            montoTotalComponente = 0;

            for (var i = 1; i < grilla.rows.length; i++) {
                rowElem = grilla.rows[i];

                var precioxComponente = parseFloat(rowElem.cells[3].children[0].cells[1].children[0].value);
                if (isNaN(precioxComponente)) {
                    precioxComponente = 0;
                }
                montoTotalComponente = montoTotalComponente + precioxComponente;

            } //end for

            document.getElementById('<%=tb_PrecComponente.ClientID %>').value = Format(parseFloat(montoTotalComponente), 3);

            return false;
        }

        //
        function onKeyUp_CalcularComponente(opc,e) {
            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;

            var montoTotalComponente = 0;
            var grilla = document.getElementById('<%=GV_ComponenteKit.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    rowElem = grilla.rows[i];

                    var precioxComponente = parseFloat(rowElem.cells[3].children[1].value);
                    if (isNaN(precioxComponente)) {
                        precioxComponente = 0;
                    }

                    if (rowElem.cells[3].children[1].id = event_element.id) {
                        switch (opc) {
                            case '1':

                                montoTotalComponente = montoTotalComponente + precioxComponente;

                                break;

                        } //end switch                        
                    } // end if                    
                } //end for
            } //end if

            document.getElementById('<%=tb_PrecComponente.ClientID %>').value = redondear(parseFloat(montoTotalComponente), 3);

            return true;
        } //end function

        function onFocus_Readonly(caja) {
            caja.readOnly = true;
            return false;
        }
        //

        function inafecto() {          

            var total = document.getElementById('<%=txt_TotalAPagar.ClientID %>').value;         
            document.getElementById('<%=txtIGV.ClientID %>').value = 0;
            document.getElementById('<%=txtSubTotal.ClientID %>').value = total;
        }

        function validarNrodiastbFechaVen() {
            var txtFechaEmision = document.getElementById('<%=tbFechaEmision.ClientID %>');
            var day = parseInt(document.getElementById('<%=txtnrodiastbFechaVen.ClientID %>').value);            
            if (isNaN(day)) { day = 0; }
            if (day > 1) {
                var fecha = new Date(converToDate(txtFechaEmision.value));
                fecha.setDate(fecha.getDate() + day);

                document.getElementById('<%=tbFechaVen.ClientID %>').value = (fecha.getDate() < 10 ? "0" + fecha.getDate().toString() : fecha.getDate().toString()) + "/" + (fecha.getMonth() + 1 < 10 ? "0" + (fecha.getMonth() + 1).toString() : (fecha.getMonth() + 1).toString()) + "/" + fecha.getFullYear().toString();
                return true;
            } else {
                alert("Valor m�nimo debe ser 2 dias");
                document.getElementById('<%=txtnrodiastbFechaVen.ClientID %>').value = 2;
                document.getElementById('<%=txtnrodiastbFechaVen.ClientID %>').focus();
            }
        }
        function validarNrodiastbfechaCancelacion() {
            var txtFechaEmision = document.getElementById('<%=tbFechaEmision.ClientID %>');
            var day = parseInt(document.getElementById('<%=txtnrodiastbfechaCancelacion.ClientID %>').value);
            if (isNaN(day)) { day = 0; }

            var fecha = new Date(converToDate(txtFechaEmision.value));
            fecha.setDate(fecha.getDate() + day);

            document.getElementById('<%=tbfechaCancelacion.ClientID %>').value = (fecha.getDate() < 10 ? "0" + fecha.getDate().toString() : fecha.getDate().toString()) + "/" + (fecha.getMonth() + 1 < 10 ? "0" + (fecha.getMonth() + 1).toString() : (fecha.getMonth() + 1).toString()) + "/" + fecha.getFullYear().toString();


            return true;
        }
        function validarNrodiastxtFechabl() {
            var txtFechaEmision = document.getElementById('<%=tbFechaEmision.ClientID %>');
            var day = parseInt(document.getElementById('<%=TxtNumDias.ClientID %>').value);
            if (isNaN(day)) { day = 0; }
            var fecha = new Date(converToDate(txtFechaEmision.value));
            fecha.setDate(fecha.getDate() + day);

            document.getElementById('<%=TxtFechaNueva.ClientID %>').value = (fecha.getDate() < 10 ? "0" + fecha.getDate().toString() : fecha.getDate().toString()) + "/" + (fecha.getMonth() + 1 < 10 ? "0" + (fecha.getMonth() + 1).toString() : (fecha.getMonth() + 1).toString()) + "/" + fecha.getFullYear().toString();


            return true;
        }
        function validarNrodiastxtFechaEntrega() {
            var txtFechaEmision = document.getElementById('<%=tbFechaEmision.ClientID %>');
            var day = parseInt(document.getElementById('<%=txtnrodiastxtFechaEntrega.ClientID %>').value);
            if (isNaN(day)) { day = 0; }
            var fecha = new Date(converToDate(txtFechaEmision.value));
            fecha.setDate(fecha.getDate() + day);

            document.getElementById('<%=txtFechaEntrega.ClientID %>').value = (fecha.getDate() < 10 ? "0" + fecha.getDate().toString() : fecha.getDate().toString()) + "/" + (fecha.getMonth() + 1 < 10 ? "0" + (fecha.getMonth() + 1).toString() : (fecha.getMonth() + 1).toString()) + "/" + fecha.getFullYear().toString();


            return true;
        }
        function validarNrodiasCondicionComercial(caja) {
            var txtFechaEmision = document.getElementById('<%=tbFechaEmision.ClientID %>');
            var gvCondicion = document.getElementById('<%=gvCondicion.ClientID  %>');

            if (gvCondicion != null) {
                for (var i = 1; i < gvCondicion.rows.length; i++) {
                    var rowElem = gvCondicion.rows[i];
                    var txtNroDias = rowElem.cells[2].children[0];
                    if (caja.id == txtNroDias.id) {
                        var day = parseInt(txtNroDias.value);
                        if (isNaN(day)) { day = 0; }
                        var fecha = new Date(converToDate(txtFechaEmision.value));
                        fecha.setDate(fecha.getDate() + day);
                        rowElem.cells[2].children[1].value = (fecha.getDate() < 10 ? "0" + fecha.getDate().toString() : fecha.getDate().toString()) + "/" + (fecha.getMonth() + 1 < 10 ? "0" + (fecha.getMonth() + 1).toString() : (fecha.getMonth() + 1).toString()) + "/" + fecha.getFullYear().toString();
//                        document.getElementById('<%=txtnrodiastbfechaCancelacion.ClientID %>').value = day;
//                        document.getElementById('<%=tbfechaCancelacion.ClientID %>').value = (fecha.getDate() < 10 ? "0" + fecha.getDate().toString() : fecha.getDate().toString()) + "/" + (fecha.getMonth() + 1 < 10 ? "0" + (fecha.getMonth() + 1).toString() : (fecha.getMonth() + 1).toString()) + "/" + fecha.getFullYear().toString();
                        
                    }
                }
            }
            return false;
        }

        function val_tbFechaVen() {
            var txtFecha2 = document.getElementById('<%=tbFechaVen.ClientID %>');
            var NroDias = 0;
            if (isDate(txtFecha2)) {
                date1 = document.getElementById('<%=tbFechaEmision.ClientID %>').value.split("/");
                date2 = txtFecha2.value.split("/");

                var fecha1 = new Date();
                fecha1.setDate(parseInt(date1[0]));
                fecha1.setMonth(parseInt(date1[1]) - 1);
                fecha1.setYear(parseInt(date1[2]));

                var fecha2 = new Date();
                fecha2.setDate(parseInt(date2[0]));
                fecha2.setMonth(parseInt(date2[1]) - 1);
                fecha2.setYear(parseInt(date2[2]));

                NroDias = DifDate(fecha1, fecha2);
            }
            document.getElementById('<%=txtnrodiastbFechaVen.ClientID %>').value = NroDias;
        }

        function val_tbfechaCancelacion() {
            var txtFecha2 = document.getElementById('<%=tbfechaCancelacion.ClientID %>');
            var NroDias = 0;
            if (isDate(txtFecha2)) {
                var fecha1 = new Date(converToDate(document.getElementById('<%=tbFechaEmision.ClientID %>').value));
                var fecha2 = new Date(converToDate(txtFecha2.value));
                if (fecha2 != null) {
                    NroDias = DifDate(fecha1, fecha2);
                }
            }
            document.getElementById('<%=txtnrodiastbfechaCancelacion.ClientID %>').value = NroDias;
        }
        function val_txtFechabl() {
            var txtFecha2 = document.getElementById('<%=txtFechaEntrega.ClientID %>');
            var NroDias = 0;
            if (isDate(txtFecha2)) {
                var fecha1 = new Date(converToDate(document.getElementById('<%=TxtFechaNueva.ClientID %>').value));
                var fecha2 = new Date(converToDate(txtFecha2.value));
                if (fecha2 != null) {
                    NroDias = DifDate(fecha1, fecha2);
                }
            }
            document.getElementById('<%=TxtNumDias.ClientID %>').value = NroDias;
        }
        

        function val_txtFechaEntrega() {
            var txtFecha2 = document.getElementById('<%=txtFechaEntrega.ClientID %>');
            var NroDias = 0;
            if (isDate(txtFecha2)) {
                var fecha1 = new Date(converToDate(document.getElementById('<%=tbFechaEmision.ClientID %>').value));
                var fecha2 = new Date(converToDate(txtFecha2.value));
                if (fecha2 != null) {
                    NroDias = DifDate(fecha1, fecha2);
                }
            }
            document.getElementById('<%=txtnrodiastxtFechaEntrega.ClientID %>').value = NroDias;
        }

        function val_CondicionComercialFecha(caja) {
            var gvCondicion = document.getElementById('<%=gvCondicion.ClientID  %>');

            if (gvCondicion != null) {
                for (var i = 1; i < gvCondicion.rows.length; i++) {
                    var rowElem = gvCondicion.rows[i];
                    var txtNroDias = rowElem.cells[2].children[0];
                    var txtFecha2 = rowElem.cells[2].children[1];

                    if (caja.id == txtFecha2.id) {
                        var NroDias = 0;
                        if (isDate(txtFecha2)) {
                            var fecha1 = new Date(converToDate(document.getElementById('<%=tbFechaEmision.ClientID %>').value));
                            var fecha2 = new Date(converToDate(txtFecha2.value));
                            if (fecha2 != null) {
                                NroDias = DifDate(fecha1, fecha2);
                            }
                        }
                        txtNroDias.value = NroDias;
                    }
                }
            }
            return false;
        }


        function DifDate(f1, f2) {
            var one_day = ((60 * 1000) * 60) * 24; // milisegundos
            var resta = f2 - f1;
            return Math.abs(Math.round(resta / one_day));
        }

        function onKeyPress_GV_Producto_Cant(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;            
            if (key == 13) {
                document.getElementById('<%=btAddProductos_AddProd.ClientID %>').click();
                return false;
            } else {
                if (validarNumeroPuntoPositivo('event') == false) {
                    return false;
                }
            }
            return true;
        }
        
    </script>

</asp:Content>
