﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Public Class frmOrdenCompra1
    Inherits System.Web.UI.Page


#Region "variables"
    Private objScript As New ScriptManagerClass
    Private drop As New Combo
    Private ListaProveedor As List(Of Entidades.PersonaView)
    Private ListaContacto As List(Of Entidades.PersonaView)
    Private ListaCatalogoProductos As List(Of Entidades.Catalogo)
    Private ListaProductos As List(Of Entidades.DetalleDocumentoView)
    Private objKit As Entidades.Kit
    Private listaKit As List(Of Entidades.Kit)
    Private _ListaKit As List(Of Entidades.Kit)
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)
    Private objDetalleDocumentoView As Entidades.DetalleDocumentoView
    Private fecha As Negocio.FechaActual
    Private util As Negocio.Util
    Private ListaCondicionComercial As List(Of Entidades.CondicionComercial)
    Private ListaMoneda As List(Of Entidades.Moneda)
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor
    Private Lista_CuentaProveedor As List(Of Entidades.CuentaProveedor)


    Private Enum operativo
        Onload = 0
        Nuevo = 1
        Buscar = 2
        Buscar_Exito = 3
        Editar = 4
    End Enum


#End Region


#Region "****************************** [ PROPIEDADES ]"

    Private Property ListaDetalleDocumentoView() As List(Of Entidades.DetalleDocumentoView)
        Get
            Return CType(Session(CStr(ViewState("DetalleDocumento"))), List(Of Entidades.DetalleDocumentoView))
        End Get
        Set(ByVal value As List(Of Entidades.DetalleDocumentoView))
            Session.Remove(CStr(ViewState("DetalleDocumento")))
            Session.Add(CStr(ViewState("DetalleDocumento")), value)
        End Set
    End Property

    Private Sub set_CondicionComercial(ByVal lista As List(Of Entidades.CondicionComercial))
        Session.Remove(CStr(ViewState("CondicionComercial")))
        Session.Add(CStr(ViewState("CondicionComercial")), lista)
    End Sub
    Private Function get_CondicionComercial() As List(Of Entidades.CondicionComercial)
        Return CType(Session(CStr(ViewState("CondicionComercial"))), List(Of Entidades.CondicionComercial))
    End Function

    Private Function getListaKit() As List(Of Entidades.Kit)
        Return CType(Session.Item(CStr(ViewState("ListaKit"))), List(Of Entidades.Kit))
    End Function
    Sub setListaKit(ByVal Lista As List(Of Entidades.Kit))
        Session.Remove(CStr(ViewState("ListaKit")))
        Session.Add(CStr(ViewState("ListaKit")), Lista)
    End Sub

    Private Sub onLoad_Session()

        Dim Aleatorio As New Random()
        ViewState.Add("DetalleDocumento", "DetalleDocumentoOC" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("CondicionComercial", "CondicionComercialOC" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("ListaKit", "ListaKitOC" + Aleatorio.Next(0, 100000).ToString)

    End Sub

#End Region

#Region "****************** CABECERA ( ORDEN COMPRA )"

    Private Sub dlPropietario_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlPropietario.SelectedIndexChanged
        OnChange_Propietario()
    End Sub
    Private Sub OnChange_Propietario()
        Try

            With drop

                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.dlTienda, CInt(Me.dlPropietario.SelectedValue), CInt(Me.hdd_IdUsuario.Value), False)
                .llenarCboContactoxIdPersona(Me.ddl_contactoPropietario, CInt(Me.dlPropietario.SelectedValue))
                .LLenarCboSeriexIdUsuarioValidacion(Me.dlSerie, CInt(Session("IdUsuario")), CInt(Me.dlPropietario.SelectedValue), CInt(Me.dlTienda.SelectedValue), CInt(Me.hdd_IdTipoDocumento.Value))
                GenerarCodigoDocumento()
                .llenarCboAlmacenxIdEmpresa(Me.ddlAlmacen, CInt(Me.dlPropietario.SelectedValue), True)
                ' Me.getDireccionAlmacen()

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub getDireccionAlmacen()
        If IsNumeric(Me.ddlAlmacen.SelectedValue) Then

            Dim ObjAlmacen As Entidades.Almacen = (New Negocio.Almacen).SelectxId(CInt(Me.ddlAlmacen.SelectedValue))
            If Not ObjAlmacen Is Nothing Then


                Me.txtDireccionAlmacen.Text = ObjAlmacen.Direccion

                Dim ubigeo As String = ObjAlmacen.Ubigeo

                If ubigeo.Trim.Length > 0 Then





                    With drop

                        Me.cboDepartamento.SelectedValue = ubigeo.Substring(0, 2)

                        .LLenarCboProvincia(Me.cboProvincia, (Me.cboDepartamento.SelectedValue))
                        Me.cboProvincia.SelectedValue = ubigeo.Substring(2, 2)

                        .LLenarCboDistrito(Me.cboDistrito, Me.cboDepartamento.SelectedValue, (Me.cboProvincia.SelectedValue))
                        Me.cboDistrito.SelectedValue = ubigeo.Substring(4, 2)

                    End With
                End If
            End If
        End If
    End Sub

    Private Sub dlTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlTienda.SelectedIndexChanged
        OnChange_Tienda()
    End Sub

    Private Sub OnChange_Tienda()
        Try

            With drop

                .llenarCboContactoxIdPersona(Me.ddl_contactoPropietario, CInt(Me.dlPropietario.SelectedValue))
                .LLenarCboSeriexIdUsuarioValidacion(Me.dlSerie, CInt(Session("IdUsuario")), CInt(Me.dlPropietario.SelectedValue), CInt(Me.dlTienda.SelectedValue), CInt(Me.hdd_IdTipoDocumento.Value))
            End With

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub verOrdenCompra(ByVal LimpiarFrm As Boolean, ByVal InicializarFrm As Boolean, ByVal IniSesion As Boolean)

        If LimpiarFrm Then
            Me.Limpiar()
        End If

        If InicializarFrm Then

            GenerarCodigoDocumento()

            Me.tbFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.TxtFechaNueva.Text = Me.tbFechaEmision.Text
            Me.TxtNumDias.Text = "0"
            onChangeTipoOC()
        End If

        If IniSesion Then
            Me.ListaDetalleDocumentoView = New List(Of Entidades.DetalleDocumentoView)
        End If

        MostrarBotones()
    End Sub

#End Region

#Region "********************************************** DROPDOWNLIST "

#End Region

#Region "productos"

    Protected Sub btnBuscarProducto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarProducto.Click
        onClick_BuscarProducto()
    End Sub
    Private Sub onClick_BuscarProducto()
        Try


            With drop

                .LlenarCboTipoexistenciaxArea(Me.cboTipoExistencia, CInt(Me.dlarea.SelectedValue), False)
                If Not Me.cboTipoExistencia.Items.FindByValue("1") Is Nothing Then Me.cboTipoExistencia.SelectedValue = "1"
                .LlenarCboLineaxTipoexistenciaxArea(Me.cmbLinea_AddProd, CInt(Me.dlarea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)

            End With

            Me.GV_ComponenteKit_Find.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('verBusquedaProducto'); LimpiarGrillaProductos();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btBuscarGrilla_AddProd.Click
        onClick_BuscarGrilla_AddProd()
    End Sub
    Private Sub onClick_BuscarGrilla_AddProd()

        ViewState.Add("NomProducto", txtDescripcionProd_AddProd.Text.Trim)
        ViewState.Add("IdLinea", cmbLinea_AddProd.SelectedValue)
        ViewState.Add("IdSubLinea", IIf(cmbSubLinea_AddProd.Items.Count = 0, "0", cmbSubLinea_AddProd.SelectedValue))

        BuscarProducto(0)
    End Sub

    Private Sub BuscarProducto(ByVal TipoMov As Integer)
        cargarDatosProductoGrilla(Me.gvListaProducto, CStr(ViewState("NomProducto")), CInt(ViewState("IdLinea")), _
                       CInt(ViewState("IdSubLinea")), 0, TipoMov)
    End Sub

    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal nomProducto As String, _
                                          ByVal idlinea As Integer, ByVal idsublinea As Integer, _
                                          ByVal codSubLinea As Integer, ByVal tipomov As Integer)
        Dim index As Integer = 0
        Select Case tipomov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(Me.txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim codigoProveedor As String = Me.txtCodigoProveedor.Text()

        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        Dim incluyeigv As Boolean = CBool(rbigv.SelectedValue)
        Dim IdAgentePersona As Integer = 0
        If IsNumeric(ddl_agenteProveedor.SelectedValue) Then
            IdAgentePersona = CInt(ddl_agenteProveedor.SelectedValue)
        End If

        ListaCatalogoProductos = (New Negocio.OrdenCompra).listarProductosProveedorOrdenCompra( _
               CInt(dlPropietario.SelectedValue), CInt(hdd_Idproveedor.Value), IdAgentePersona, nomProducto, _
               idlinea, idsublinea, codSubLinea, CInt(dlMoneda.SelectedValue), _
               index, grilla.PageSize, ckAllProducts.Checked, CInt(dlarea.SelectedValue), tableTipoTabla, codigoProducto, codigoProveedor, incluyeigv)

        If ListaCatalogoProductos.Count = 0 Then
            gvListaProducto = Nothing
            gvListaProducto.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('verBusquedaProducto'); onChange_txtDescripcionProd_AddProd(); alert('No se hallaron registros'); ", True)
        Else
            gvListaProducto.DataSource = Me.ListaCatalogoProductos
            gvListaProducto.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)
            Me.GV_ComponenteKit_Find.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('verBusquedaProducto'); LimpiarGrillaProductos(); ", True)

        End If
    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        BuscarProducto(1)
    End Sub
    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        BuscarProducto(2)
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        BuscarProducto(3)
    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            If CInt(cmbLinea_AddProd.SelectedValue) <> 0 Then

                drop.LlenarCboSubLineaxIdLinea(cmbSubLinea_AddProd, CInt(cmbLinea_AddProd.SelectedValue), True)
            Else
                cmbSubLinea_AddProd.Items.Clear()
                txtCodigoProducto.Text = ""
                txtDescripcionProd_AddProd.Text = ""
                txtCodigoProveedor.Text = ""
            End If
            Me.GV_ComponenteKit_Find.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('verBusquedaProducto'); onChange_txtDescripcionProd_AddProd();", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub cboUnidadMedida_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim Index As Integer = CType(CType(sender, DropDownList).NamingContainer, GridViewRow).RowIndex

            ActualizarDetalleDocumentoView(Index)
            Me.ListaProductos = Me.ListaDetalleDocumentoView

            Me.ListaProductos(Index).Cantidad = (New Negocio.Producto).fx_getValorEquivalenteProducto(Me.ListaProductos(Index).IdProducto, Me.ListaProductos(Index).IdUMPrincipal, Me.ListaProductos(Index).IdUMedida, Me.ListaProductos(Index).Cantidad)

            Me.ListaDetalleDocumentoView = ListaProductos

            Me.gvProducto.DataSource = ListaProductos
            Me.gvProducto.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "calcularImporteOnLoad(); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


#End Region

#Region "Procedimientos"

    Private Sub GenerarCodigoDocumento()
        If (IsNumeric(Me.dlSerie.SelectedValue)) Then
            Me.tbCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.dlSerie.SelectedValue))
        Else
            Me.tbCodigoDocumento.Text = ""
        End If
    End Sub

    Protected Sub btverhistorial_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim boton As ImageButton = CType(sender, ImageButton)
        Dim fila As GridViewRow
        fila = CType(boton.NamingContainer, GridViewRow)
        lblPrecio.Text = "Historial de precios del Producto: " + CStr(fila.Cells(2).Text)
        gvPrecio.DataSource = (New Negocio.OrdenCompra).listarPreciosCompra(CInt(CType(fila.Cells(0).FindControl("IdProductoLista"), HiddenField).Value))
        gvPrecio.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " onCapa2('verhistorial'); onCapa('verBusquedaProducto');", True)
    End Sub
    Private Sub MostrarBotones()

        Select Case CInt(Me.hddOperativo.Value)

            Case operativo.Onload

                pnlDetalle.Enabled = False
                btnuevo.Visible = True
                btbuscar.Visible = True
                bteditar.Visible = False
                btcancelar.Visible = False
                btguardar.Visible = False
                btanular.Visible = False
                btimprimir.Visible = False
                btimprimir02.Visible = False
                btnBuscarDocumento.Visible = False
                habilitarControles(True)


            Case operativo.Nuevo

                pnlDetalle.Enabled = True
                btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = False
                btcancelar.Visible = True
                btguardar.Visible = True
                btanular.Visible = False
                btimprimir.Visible = False
                btimprimir02.Visible = False
                btnBuscarDocumento.Visible = False
                habilitarControles(True)

                GenerarCodigoDocumento()

            Case operativo.Buscar

                pnlDetalle.Enabled = True
                btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = False
                btcancelar.Visible = True
                btguardar.Visible = False
                btanular.Visible = False
                btimprimir.Visible = False
                btimprimir02.Visible = False
                btnBuscarDocumento.Visible = True


            Case operativo.Buscar_Exito

                pnlDetalle.Enabled = False
                btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = True
                btcancelar.Visible = True
                btguardar.Visible = False
                btanular.Visible = False
                btimprimir.Visible = True
                btimprimir02.Visible = True
                btnBuscarDocumento.Visible = False

            Case operativo.Editar

                pnlDetalle.Enabled = True
                btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = False
                btcancelar.Visible = True
                btguardar.Visible = True
                btanular.Visible = True
                btimprimir.Visible = False
                btimprimir02.Visible = False
                btnBuscarDocumento.Visible = False
                habilitarControles(False)


        End Select

    End Sub
    Private Sub ValidarPermisos()

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Me.hdd_IdUsuario.Value), New Integer() {117, 118, 119, 120, 121, 124})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnuevo.Enabled = True
            Me.btguardar.Enabled = True
        Else
            Me.btnuevo.Enabled = False
            Me.btguardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.bteditar.Enabled = True
        Else
            Me.bteditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btanular.Enabled = True
        Else
            Me.btanular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btbuscar.Enabled = True
        Else
            Me.btbuscar.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* IMPORTACION

        Else  ' No puede hacer O/C importacion
            For x As Integer = 0 To rbtipodeoc.Items.Count - 1
                If rbtipodeoc.Items(x).Value = "True" Then
                    rbtipodeoc.Items(x).Enabled = False
                    Exit For
                End If
            Next
        End If

        If listaPermisos(5) > 0 Then
            hdd_perc.Value = "1"
        End If

    End Sub
    Private Sub habilitarControles(ByVal valor As Boolean)
        dlPropietario.Enabled = valor
        dlTienda.Enabled = valor
        dlSerie.Enabled = valor
        tbCodigoDocumento.Enabled = valor
    End Sub
    Private Sub Hide_Button()
        btnuevo.Visible = False
        pnlDetalle.Enabled = True
        btbuscar.Visible = False
        bteditar.Visible = False
        btcancelar.Visible = False
        btguardar.Visible = False
        btanular.Visible = False
        btimprimir.Visible = False
        btimprimir02.Visible = False
        btnBuscarDocumento.Visible = False
        habilitarControles(False)
        btnBuscarProducto.Visible = False
        Button1.Visible = False
        Button2.Visible = False
        btcondicioncomercial.Visible = False

    End Sub
    Private Function valPreciosComponentes(ByVal inLista As List(Of Entidades.Kit), _
                                           ByVal inListaProducto As List(Of Entidades.DetalleDocumentoView)) _
                                           As Boolean
        Dim IdKit As Integer
        Dim AddPrecioComp As Decimal

        For p As Integer = 0 To inListaProducto.Count - 1

            If inListaProducto(p).Kit Then
                IdKit = inListaProducto(p).IdProducto
                _ListaKit = inLista.FindAll(Function(K As Entidades.Kit) K.IdKit = IdKit)
                For i As Integer = 0 To _ListaKit.Count - 1
                    AddPrecioComp += _ListaKit(i).Precio_Comp
                Next
                If AddPrecioComp > inListaProducto(p).PrecioCD + 0.5 Then
                    Throw New Exception("Los precios de los componentes del kit " + inListaProducto(p).Descripcion + " superan su precio final\nNo se permite la operación.")
                    Return False
                End If
                AddPrecioComp = 0
            End If

        Next
        Return True
    End Function

#End Region

#Region "Limpiar formulario"

    Private Sub Limpiar()
        '==== Limpiar Valores Decimales

        txtDescuento.Text = "0"
        txtSubTotal.Text = "0"
        txtIGV.Text = "0"
        txtTotal.Text = "0"
        txtPerc.Text = "0"
        txt_TotalAPagar.Text = "0"


        tbFechaVen.Text = ""


        LimpiarContacto()
        tbProveedor.Text = ""
        tbRuc.Text = ""
        tbdireccion.Text = ""
        tbtelefono.Text = ""
        tbCorreo.Text = ""
        txtobservacion.Text = ""
        txtFechaEntrega.Text = ""



        gvProducto.DataBind()
        gvListaProducto.DataBind()
        gvCuentaProveedor.DataBind()
        gvCondicion.DataBind()

        hdd_Idproveedor.Value = "0"
        hddIdDocumento.Value = "0"
        hdd_IdDocRelacionado.Value = "0"
        hdd_MovCuenta.Value = "0"
        dlarea.SelectedIndex = 0
        dltipoPrecio.SelectedIndex = 0
        dlEstadoDocumento.SelectedValue = "1" 'valido
        dlEstadoCancelacion.SelectedValue = "1" 'Por pagar
        dlEstadoEntrega.SelectedValue = "1" 'Por entregar

        For Each radio As RadioButton In rbtipodeoc.Controls
            radio.Checked = False
        Next


        For i As Integer = 0 To Me.rbigv.Items.Count - 1
            Me.rbigv.Items(i).Selected = False
        Next

        ListaProductos = New List(Of Entidades.DetalleDocumentoView)
        setListaKit(Nothing)
    End Sub


    Private Sub LimpiarContacto()
        ddl_contactoProveedor.Items.Clear()
        ddl_contact_supplier_mail.Items.Clear()
        ddl_contactoTelefono.Items.Clear()
        tbcontactocargo.Text = ""
    End Sub

#End Region

#Region "Eventos principales"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Onload_OrdenCompra()

    End Sub
    Public Sub JavaScript()
        Me.btguardar.Attributes.Add("onClick", "return(valSaveDocumento());")
        Me.btcancelar.Attributes.Add("onClick", "return(confirm('Esta seguro de cancelar la operacion ?'));")
        Me.bteditar.Attributes.Add("onClick", "return(validarEditar());")
        Me.btanular.Attributes.Add("onClick", "return(confirm('Esta seguro de anular el documento ?'));")
        Me.btimprimir.Attributes.Add("onClick", "return(imprimirDocumento());")
        Me.btimprimir02.Attributes.Add("onClick", "return(imprimirDocumento02());")

        Me.tbFechaEmision.Attributes.Add("onblur", "return(valFecha_Blank(this));")
        Me.txtnrodiastbFechaVen.Attributes.Add("onKeyPress", "return(   validarNumeroPuntoSigno(event)   ); ")
        Me.txtnrodiastbFechaVen.Attributes.Add("onFocus", "return ( aceptarFoco(this)  );")
        Me.txtnrodiastbFechaVen.Attributes.Add("onKeyUp", "return(   validarNrodiastbFechaVen()   );")
        Me.tbFechaVen.Attributes.Add("onblur", "return(valFecha_Blank(this));")
        Me.tbFechaVen.Attributes.Add("onFocus", "return ( val_tbFechaVen() );")
        Me.tbFechaVen.Attributes.Add("onKeyUp", "return ( val_tbFechaVen() );")
        Me.tbProveedor.Attributes.Add("onKeyDown", "return(false);")
        Me.Button1.Attributes.Add("onClick", "return (mostrarCapaProveedor());")
        'OnClientClick = "return( mostrarCapaPersona()  );"

        Me.tbRuc.Attributes.Add("onKeyDown", "return(false);")
        Me.tbdireccion.Attributes.Add("onKeyDown", "return(false);")
        Me.tbtelefono.Attributes.Add("onKeyDown", "return(false);")
        Me.tbCorreo.Attributes.Add("onKeyDown", "return(false);")
        Me.tbcontactocargo.Attributes.Add("onKeyDown", "return(false);")
        Me.btnBuscarProducto.Attributes.Add("onClick", "return(mostrarCapaListaProducto());")
        Me.btnBuscarProducto.Attributes.Add("onmouseout", "this.src='../Imagenes/BuscarProducto_A.JPG';")
        Me.btnBuscarProducto.Attributes.Add("onmouseover", "this.src='../Imagenes/BuscarProducto_A.JPG';")
        Me.btnLimpiarDetalleDocumento.Attributes.Add("onmouseout", "this.src='../Imagenes/Limpiar_B.JPG';")
        Me.btnLimpiarDetalleDocumento.Attributes.Add("onmouseover", "this.src='../Imagenes/Limpiar_A.JPG';")
        Me.txtCodBarraFabricante.Attributes.Add("onKeypress", "return( valKeyPressDescripcionProdCodBarras(this,event) );")
        Me.txtCodBarraFabricante.Attributes.Add("onBlur", "return ( onBlurTextTransform(this,configurarDatos) );")
        Me.txtCodBarraFabricante.Attributes.Add("onFocus", "val_CodBarras('1');onFocusTextTransform(this,configurarDatos); aceptarFoco(this);")
        Me.btnAceptarCodBarraFabr.Attributes.Add("onClick", "return(valOnClick_btnAgregarCodBarras());")
        Me.txtDescuento.Attributes.Add("onfocus", "return(aceptarFoco(this));")
        Me.txtDescuento.Attributes.Add("onKeypress", "return(validarNumeroPuntoPositivo('event'));")
        Me.txtDescuento.Attributes.Add("onKeyUp", "return( calcularImporte('10'));")
        Me.txtSubTotal.Attributes.Add("onFocus", "return ( onFocus_Readonly(this) );")

        Me.txtIGV.Attributes.Add("onKeyUp", "return( calcularImporte('11'));")
        Me.txtIGV.Attributes.Add("onfocus", "return(aceptarFoco(this));")
        Me.txtIGV.Attributes.Add("onKeypress", "return(validarNumeroPuntoPositivo('event'));")
        Me.txtTotal.Attributes.Add("onFocus", "return ( onFocus_Readonly(this) );")
        Me.txtPerc.Attributes.Add("onfocus", "return(aceptarFoco(this));")
        Me.txtPerc.Attributes.Add("onKeypress", "return(validarNumeroPuntoPositivo('event'));")
        Me.txtPerc.Attributes.Add("onKeyUp", "return( calcularImporte('12'));")
        Me.txt_TotalAPagar.Attributes.Add("onFocus", "return ( onFocus_Readonly(this) );")
        Me.txtPerc.Attributes.Add("onKeyUp", "return( calcularImporte('12'));")
        Me.txtnrodiastbfechaCancelacion.Attributes.Add("onKeyPress", "return(   validarNumeroPuntoSigno(event)   );")
        Me.txtnrodiastbfechaCancelacion.Attributes.Add("onKeyUp", "return(   validarNrodiastbfechaCancelacion()   );")
        Me.txtnrodiastbfechaCancelacion.Attributes.Add("onFocus", "return ( aceptarFoco(this)  );")
        Me.tbfechaCancelacion.Attributes.Add("onKeyUp", "return ( val_tbfechaCancelacion() );")
        Me.tbfechaCancelacion.Attributes.Add("onFocus", "return ( val_tbfechaCancelacion() );")
        Me.tbfechaCancelacion.Attributes.Add("onblur", "return(valFecha_Blank(this));")
        Me.txtnrodiastxtFechaEntrega.Attributes.Add("onKeyPress", "return(   validarNumeroPuntoSigno(event)   );")
        Me.txtnrodiastxtFechaEntrega.Attributes.Add("onKeyUp", "return(   validarNrodiastxtFechaEntrega()   );")
        Me.txtnrodiastxtFechaEntrega.Attributes.Add("onFocus", "return ( aceptarFoco(this)  );")
        Me.txtFechaEntrega.Attributes.Add("onblur", "return(valFecha_Blank(this));")
        Me.txtFechaEntrega.Attributes.Add("onKeyUp", "return ( val_txtFechaEntrega() );")
        Me.txtFechaEntrega.Attributes.Add("onFocus", "return ( val_txtFechaEntrega() );")
        Me.txtDireccionAlmacen.Attributes.Add("onFocus", "return ( onFocus_Readonly(this) );")
        Me.btcondicioncomercial.Attributes.Add("onClick", "return ( onclickCondicionComercial() );")
        Me.TxtNumDias.Attributes.Add("onKeyPress", "return(   validarNumeroPuntoSigno(event)   );")
        Me.TxtNumDias.Attributes.Add("onKeyUp", "return(   validarNrodiastxtFechabl()   );")
        Me.TxtNumDias.Attributes.Add("onFocus", "return ( aceptarFoco(this)  );")
        Me.TxtFechaNueva.Attributes.Add("onblur", "return(valFecha_Blank(this));")
        Me.TxtFechaNueva.Attributes.Add("onKeyUp", "return ( val_txtFechabl() );")
        Me.TxtFechaNueva.Attributes.Add("onFocus", "return ( val_txtFechabl() );")
        Me.txtobservacion.Attributes.Add("onBlur", "return ( onBlurTextTransform(this,configurarDatos) );")
        Me.txtobservacion.Attributes.Add("onFocus", "return ( onFocusTextTransform(this,configurarDatos) );")
        Me.btn_enviar.Attributes.Add("onClick", "return ( onclick_btn_enviar() );")
        Me.imgCerraCapaProveedor.Attributes.Add("onClick", "return(offCapa('verBusquedaProveedor'));")

        Me.txtBuscarProveedor.Attributes.Add("onkeyPress", "return ( onKeyPressProveedor(this,event) );")
        Me.txtBuscarProveedor.Attributes.Add("onBlur", "return ( onBlurTextTransform(this,configurarDatos) );")
        Me.txtBuscarProveedor.Attributes.Add("onFocus", "onFocusTextTransform(this,configurarDatos); aceptarFoco(this);")
        Me.txtbuscarRucProveedor.Attributes.Add("onkeyPress", "return ( onKeyPressProveedor(this,event) );")

        Me.btAnterior_Proveedor.Attributes.Add("onClick", "return(valNavegacionProveedor('0'));")
        Me.btSiguiente_Proveedor.Attributes.Add("onClick", "return(valNavegacionProveedor('1'));")
        Me.btIr_Proveedor.Attributes.Add("onClick", "return(valNavegacionProveedor('2'));")
        Me.tbPageIndexGO_Proveedor.Attributes.Add("onKeyPress", "return(onKeyPressEsNumero('event'));")

        Me.tbCodigoDocumento.Attributes.Add("onKeyPress", "return(validarBuscar(event));")
        Me.btnBuscarDocumento.Attributes.Add("onClick", "return(validarBuscarDocumento());")
        Me.txtDescripcionProd_AddProd.Attributes.Add("onKeyPress", "return ( onkeyPressProducto(this,event) );")
        Me.txtDescripcionProd_AddProd.Attributes.Add("onFocus", "onFocusTextTransform(this,configurarDatos); aceptarFoco(this);")
        Me.txtDescripcionProd_AddProd.Attributes.Add("onBlur", "return ( onBlurTextTransform(this,configurarDatos) );")
        Me.btAddProductos_AddProd.Attributes.Add("onClick", "return(valAddProductos());")
        Me.txtDescripcionProd_AddProd.Attributes.Add("onClick", "return(LimpiarGrillaProductos());")
        Me.txtCodigoProducto.Attributes.Add("onKeyPress", "return ( onkeyPressProducto(this,event) );")
        Me.txtCodigoProducto.Attributes.Add("onFocus", "onFocusTextTransform(this,configurarDatos); aceptarFoco(this);")
        Me.txtCodigoProducto.Attributes.Add("onBlur", "return ( onBlurTextTransform(this,configurarDatos) );")
        Me.txtCodigoProveedor.Attributes.Add("onKeyPress", "return ( onkeyPressProducto(this,event) );")
        Me.txtCodigoProveedor.Attributes.Add("onFocus", "onFocusTextTransform(this,configurarDatos); aceptarFoco(this);")
        Me.txtCodigoProveedor.Attributes.Add("onBlur", "return ( onBlurTextTransform(this,configurarDatos) );")
        Me.btnAddTipoTabla.Attributes.Add("onClick", "return( valAddTabla() );")
        Me.btnAnterior_Productos.Attributes.Add("onClick", "return(valNavegacionProductos('0'));")
        Me.btnPosterior_Productos.Attributes.Add("onClick", "return(valNavegacionProductos('1'));")
        Me.btnIr_Productos.Attributes.Add("onClick", "return(valNavegacionProductos('2'));")
        Me.txtPageIndexGO_Productos.Attributes.Add("onKeyPress", "return(onKeyPressEsNumero('event'));")
        Me.ImageButton1.Attributes.Add("onClick", "return(offCapa2('verhistorial'));")
        Me.ImageButton3.Attributes.Add("onClick", "return(offCapa('capaDescuentos'));")
        Me.txtProdDscto.Attributes.Add("onKeyDown", "return false;")
        Me.tbdescuento1.Attributes.Add("onfocus", "return(aceptarFoco(this));")
        Me.tbdescuento1.Attributes.Add("onKeypress", "return(validarNumeroPuntoPositivo('event'));")
        Me.tbdescuento2.Attributes.Add("onfocus", "return(aceptarFoco(this));")
        Me.tbdescuento2.Attributes.Add("onKeypress", "return(validarNumeroPuntoPositivo('event'));")
        Me.tbdescuento3.Attributes.Add("onfocus", "return(aceptarFoco(this));")
        Me.tbdescuento3.Attributes.Add("onKeypress", "return(validarNumeroPuntoPositivo('event'));")
        Me.btAceptarDscto.Attributes.Add("onClick", "return(calcularDsctos());")
        Me.imgCerraCapaKit.Attributes.Add("onClick", "return(offCapa('verBusquedaKit'));")
        Me.btn_Ajustar_Precio.Attributes.Add("onClick", "return (onClick_Ajustar_Precio());")

    End Sub
    Private Sub Onload_OrdenCompra()
        If Not IsPostBack Then
            hdd_IdUsuario.Value = CStr(Session("IdUsuario"))
            Call JavaScript()

            'Me.pnlBuscarDocumento.DefaultButton = Me.btnBuscarDocumento.UniqueID
            'Me.pnlBtnBuscarProveedor.DefaultButton = Me.btBuscarProveedor.UniqueID

            onLoad_Session()


            ConfigurarDatos()

            If (hdd_IdUsuario.Value <> "" Or hdd_IdUsuario.Value <> Nothing) Then
                ValidarPermisos()
            Else
                hdd_IdUsuario.Value = CStr(Request.QueryString("IdUsuario"))
            End If

            OnInit_OrdenCompra()

            hddOperativo.Value = CStr(operativo.Onload)
            verOrdenCompra(False, True, False)

            If Request.QueryString("IdDocumento") IsNot Nothing Then
                Me.OnClick_BuscarDocumento(CInt(Request.QueryString("IdDocumento")))
                Hide_Button()
            End If

        End If
    End Sub


    Private Sub OnInit_OrdenCompra()


        With drop
            .LlenarCboEmpresaxIdUsuario(Me.dlPropietario, CInt(Me.hdd_IdUsuario.Value), False)

            ' ***************** AQUI SE CARGA TODA LA CABECERA QUE DEPENDE DE LA TIENDA
            OnChange_Propietario()

            .LlenarCboAreaxIdusuario(Me.dlarea, CInt(Me.hdd_IdUsuario.Value), Nothing, False)
            .llenarCboTipoOperacionxIdTpoDocumento(Me.dlTipoOperacion, CInt(Me.hdd_IdTipoDocumento.Value), False)
            .LlenarCboTipoAgente(Me.ddl_agenteProveedor, True)

            ' ***************** ESTADOS DEL DOCUMENTO
            .LLenarCboEstadoDocumento(Me.dlEstadoDocumento)
            .LLenarCboEstadoCancelacion(Me.dlEstadoCancelacion, True)
            .LLenarCboEstadoEntrega(Me.dlEstadoEntrega, True)

            .LlenarCboMoneda(Me.dlMoneda)
            .LlenarCboCondicionPago(Me.dlcondicionpago)
            .LlenarCboTipoPrecioImportacion(Me.dltipoPrecio, True)


            .llenarCboCondicionesComerciales(Me.cboCondicionComercial, CInt(Me.hdd_IdTipoDocumento.Value))

            .llenarCboCorreoxIdPersona(Me.ddl_usuario, CInt(Me.hdd_IdUsuario.Value))
            .llenarCboContactoxIdPersona(Me.ddl_contactoPropietario, CInt(Me.dlPropietario.SelectedValue), CInt(Me.hdd_IdUsuario.Value))

            '************** PUNTO DE LLEGADA
            .LLenarCboDepartamento(Me.cboDepartamento)
            .LLenarCboProvincia(Me.cboProvincia, (Me.cboDepartamento.SelectedValue))
            .LLenarCboDistrito(Me.cboDistrito, (Me.cboDepartamento.SelectedValue), (Me.cboProvincia.SelectedValue))

            'If dlarea.Items.Count > 0 Then .LlenarCboCod_UniNeg(dlunidadnegocio, True)
        End With

        With New Negocio.Impuesto
            Me.hddIGV.Value = CStr(.SelectTasaIGV)
        End With

        Me.lblIgv.Text = CStr(CInt(CDec(Me.hddIGV.Value) * CDec(100)))

    End Sub

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub



#End Region

#Region "Eventos de Controles"


    Private Sub dlSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlSerie.SelectedIndexChanged
        Try
            If CInt(hddOperativo.Value) = operativo.Buscar Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "setFocus_tbCodigoDocumento();", True)
            End If

            GenerarCodigoDocumento()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub dlMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlMoneda.SelectedIndexChanged
        Try
            Me.lblSimboloMoneda1.Text = Me.dlMoneda.SelectedItem.ToString

            Me.ActualizarDetalleDocumentoView()

            Me.ListaProductos = Me.ListaDetalleDocumentoView

            For i As Integer = 0 To ListaProductos.Count - 1
                Dim precio As Decimal = (New Negocio.OrdenCompra).ListarPrecioXidMoneda(CInt(dlMoneda.SelectedValue), ListaProductos(i).IdProducto)
                util = New Negocio.Util
                ListaProductos(i).Detraccion = precio
                ListaProductos(i).NomMoneda = dlMoneda.SelectedItem.Text
                Dim PrecioSD As Decimal = util.SelectValorxIdMonedaOxIdMonedaD(ListaProductos(i).IdMoneda, CInt(dlMoneda.SelectedValue), ListaProductos(i).PrecioSD, "E")
                ListaProductos(i).PrecioSD = PrecioSD
                ListaProductos(i).IdMoneda = CInt(dlMoneda.SelectedValue)
            Next

            gvProducto.DataSource = ListaProductos
            gvProducto.DataBind()

            Me.ListaDetalleDocumentoView = Me.ListaProductos

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "calcularImporteOnLoad();", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "Busqueda de Proveedores"

    Private Sub btBuscarProveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btBuscarProveedor.Click
        ViewState.Add("NomPersona", Me.txtBuscarProveedor.Text.Trim)
        ViewState.Add("Ruc", Me.txtbuscarRucProveedor.Text.Trim)
        ViewState.Add("Condicion", CInt(Me.rbcondicion.SelectedValue))
        ViewState.Add("TipoPersona", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", 1, 2)))

        OnClick_BuscarProveedor()
    End Sub

    Private Sub OnClick_BuscarProveedor()
        BuscarProveedor(0)
    End Sub

    Private Sub BuscarProveedor(ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(tbPageIndex_Proveedor.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(tbPageIndex_Proveedor.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(tbPageIndex_Proveedor.Text) - 1)
        End Select

        ListaProveedor = (New Negocio.OrdenCompra).listarPersonaJuridicaxRolxEmpresa(CStr(ViewState("NomPersona")), CStr(ViewState("Ruc")), CInt(ViewState("TipoPersona")), 2, CInt(ViewState("Condicion")), 1, index, gvProveedor.PageSize)

        If ListaProveedor.Count > 0 Then
            gvProveedor.DataSource = ListaProveedor
            gvProveedor.DataBind()
            tbPageIndex_Proveedor.Text = CStr(index + 1)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaProveedor();", True)
        Else
            gvProveedor.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "mostrarCapaProveedor(); alert('No se hallaron registros');", True)
        End If

    End Sub

    Private Sub gvProveedor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvProveedor.SelectedIndexChanged
        CargarProveedor(CInt(gvProveedor.SelectedRow.Cells(1).Text.Trim))
    End Sub

    Private Sub CargarProveedor(ByVal IdProveedor As Integer, Optional ByVal IdContacto As Integer = -1)

        Try
            Me.ddl_agenteProveedor.SelectedIndex = 0

            Dim objProveedor As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdProveedor)

            Select Case Me.rbtipodeoc.SelectedValue

                Case "True" ' ************************ ORDEN DE COMPRA IMPORTADA


                Case Else   ' ************************ ORDEN DE COMPRA NACIONAL || ACTIVOS
                    If objProveedor.Ruc = "" Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " onCapa('verBusquedaProveedor'); alert('NO SE HA REGISTRADO RUC DEL PROVEEEDOR.'); ", True)
                        Return
                    End If
            End Select

            With objProveedor
                tbProveedor.Text = HttpUtility.HtmlDecode(.Descripcion)
                tbRuc.Text = HttpUtility.HtmlDecode(.Ruc)
                tbdireccion.Text = HttpUtility.HtmlDecode(.Direccion)
                tbtelefono.Text = HttpUtility.HtmlDecode(.Telefeono)
                tbCorreo.Text = HttpUtility.HtmlDecode(.Correo)
            End With

            Dim listaTipoAgente As List(Of Entidades.PersonaTipoAgente) = (New Negocio.PersonaTipoAgente).PersonaTipoAgente_Select(IdProveedor)
            If Not listaTipoAgente Is Nothing Then

                For i As Integer = 0 To listaTipoAgente.Count - 1
                    If listaTipoAgente(i).SujetoAPercepcion Then
                        If Not Me.ddl_agenteProveedor.Items.FindByValue(CStr(listaTipoAgente(i).IdAgente)) Is Nothing Then
                            Me.ddl_agenteProveedor.SelectedValue = CStr(listaTipoAgente(i).IdAgente)
                            Exit For
                        End If
                    End If
                Next

            End If

            hdd_Idproveedor.Value = CStr(IdProveedor)

            verCuenta_Proveedor()

            LimpiarContacto()

            drop.llenarCboContactoxIdPersona(ddl_contactoProveedor, IdProveedor)

            If Not Me.ddl_contactoProveedor.Items.FindByValue(CStr(IdContacto)) Is Nothing Then
                Me.ddl_contactoProveedor.SelectedValue = CStr(IdContacto)
            End If

            If ddl_contactoProveedor.SelectedValue <> "0" Or ddl_contactoProveedor.SelectedValue <> "" Then
                CargarContactoProveedor(CInt(ddl_contactoProveedor.SelectedValue))
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub verCuenta_Proveedor(Optional ByVal validar As Boolean = False)

        Me.Lista_CuentaProveedor = (New Negocio.OrdenCompra).CuentaProveedor_SelectActivo(CInt(hdd_Idproveedor.Value), (CInt(dlPropietario.SelectedValue)))

        If Not IsNothing(Me.Lista_CuentaProveedor) Then
            Me.ListaMoneda = (New Negocio.Moneda).SelectCbo

            For i As Integer = 0 To Me.Lista_CuentaProveedor.Count - 1
                Me.Lista_CuentaProveedor(i).ListaMoneda = ListaMoneda
            Next

        End If

        Me.gvCuentaProveedor.DataSource = Me.Lista_CuentaProveedor
        Me.gvCuentaProveedor.DataBind()

        If validar Then

            Dim IdMoneda As Integer = CInt(Me.dlMoneda.SelectedValue)
            Dim TotalAPagar As Decimal = 0 : If IsNumeric(Me.txt_TotalAPagar.Text.Trim) Then TotalAPagar = CDec(Me.txt_TotalAPagar.Text.Trim)
            Dim Disponible As Decimal = 0

            If Me.Lista_CuentaProveedor.Count > 0 Then

                For i As Integer = 0 To Me.Lista_CuentaProveedor.Count - 1 ' *** 
                    If Me.Lista_CuentaProveedor(i).IdMoneda = IdMoneda Then
                        Disponible = Me.Lista_CuentaProveedor(i).cprov_Saldo
                    End If
                Next

                If Disponible <= 0 Then
                    Disponible = (New Negocio.Util).CalcularValorxTipoCambio(Me.Lista_CuentaProveedor(0).cprov_Saldo, Me.Lista_CuentaProveedor(0).IdMoneda, IdMoneda, "E", CDate(Me.tbFechaEmision.Text))
                End If


                If TotalAPagar > Disponible Then
                    Throw New Exception("El TOTAL DEL DOCUMENTO " + Me.dlMoneda.SelectedItem.Text + " " + CStr(Math.Round(TotalAPagar, 3)) + " SUPERA LA LÍNEA DE CRÉDITO " + Me.dlMoneda.SelectedItem.Text + " " + CStr(Math.Round(Disponible, 3)) + " CON EL PROVEEDOR.")
                    Return
                End If

            Else
                Throw New Exception("NO SE HA REGISTRADO UN LÍNEA DE CRÉDITO CON EL PROVEEDOR.")
                Return
            End If

        End If

    End Sub

    Private Sub btAnterior_Proveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior_Proveedor.Click
        Try
            BuscarProveedor(1)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Proveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente_Proveedor.Click
        Try
            BuscarProveedor(2)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Proveedor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr_Proveedor.Click
        Try
            BuscarProveedor(3)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region


#Region "Agregar - Quitar Productos"

    Private Sub imgCerrarCapaProducto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCerrarCapaProducto.Click
        Me.gvListaProducto.DataBind()
        Me.cmbLinea_AddProd.SelectedIndex = 0
        Me.cmbSubLinea_AddProd.Items.Clear()
        Me.txtDescripcionProd_AddProd.Text = ""
        Me.txtCodigoProducto.Text = ""
        Me.txtCodigoProveedor.Text = ""
        Me.ckAllProducts.Checked = False

        objScript.offCapa(Me, "verBusquedaProducto")
    End Sub



    Protected Sub lkbQuitar_Click(ByVal sender As Object, ByVal e As EventArgs)
        quitarDetalleDocumento(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub quitarDetalleDocumento(ByVal Index As Integer)
        Try

            Me.ActualizarDetalleDocumentoView()
            ListaProductos = Me.ListaDetalleDocumentoView

            Dim IdProducto As Integer = ListaProductos(Index).IdProducto
            ListaProductos.RemoveAt(Index)

            listaKit = getListaKit()
            If listaKit IsNot Nothing Then
                listaKit.RemoveAll(Function(K As Entidades.Kit) K.IdKit = IdProducto)
                setListaKit(listaKit)
            End If

            gvProducto.DataSource = ListaProductos
            gvProducto.DataBind()

            Me.ListaDetalleDocumentoView = Me.ListaProductos

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "calcularImporteOnLoad();", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "seccion de contactos"


    Private Sub CargarContactoProveedor(ByVal IdPersona As Integer)
        If IdPersona > 0 Then
            Dim objPersonaDato As New Entidades.PersonaDato
            objPersonaDato.IdPersona = IdPersona
            Dim ListaPersonaDato As List(Of Entidades.PersonaDato) = (New Negocio.Persona).PersonaDato(objPersonaDato)

            If ListaPersonaDato.Count > 0 Then
                drop.llenarCboTelefonoxPersonaDato(ddl_contactoTelefono, ListaPersonaDato(0).ListaTelefono)
                tbcontactocargo.Text = HttpUtility.HtmlDecode(ListaPersonaDato(0).Cargo)
            End If
            drop.llenarCboCorreoxIdPersona(ddl_contact_supplier_mail, IdPersona)
        End If
    End Sub

#End Region

#Region "************** BOTONES DE OPERATIVIDAD"
    Private Sub btnuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnuevo.Click

        hddOperativo.Value = CStr(operativo.Nuevo)
        verOrdenCompra(True, True, True)

    End Sub

    Private Sub btbuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btbuscar.Click

        hddOperativo.Value = CStr(operativo.Buscar)

        verOrdenCompra(True, True, True)

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "setFocus_tbCodigoDocumento();", True)

    End Sub







    Private Sub btanular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btanular.Click
        Try
            If hddIdDocumento.Value <> "" Then
                Dim objOrdenCompra As New Negocio.OrdenCompra
                objOrdenCompra.AnularOrdenCompra(CInt(hddIdDocumento.Value))
                dlEstadoDocumento.SelectedValue = "2"

                hddOperativo.Value = CStr(operativo.Onload)
                verOrdenCompra(False, False, False)

                hddOperativo.Value = CStr(operativo.Onload)
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btcancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btcancelar.Click

        hddOperativo.Value = CStr(operativo.Onload)

        verOrdenCompra(True, True, True)

    End Sub

    Private Sub bteditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bteditar.Click
        Try

            Dim PoseeCosteoImportacion As Nullable(Of Integer)
            PoseeCosteoImportacion = (New Negocio.OrdenCompra).Validar_CosteoImportacion(CInt(Me.hddIdDocumento.Value))

            If PoseeCosteoImportacion > 0 Then
                Throw New Exception("EL DOCUMENTO POSEE UN COSTEO DE IMPORTACION.")
            End If

            Me.ListaProductos = Me.ListaDetalleDocumentoView
            gvProducto.DataSource = ListaProductos
            gvProducto.DataBind()

            ListaCondicionComercial = get_CondicionComercial()
            gvCondicion.DataSource = ListaCondicionComercial
            gvCondicion.DataBind()

            rbtipodeoc.Enabled = False

            hddOperativo.Value = CStr(operativo.Editar)
            verOrdenCompra(False, False, False)

            CleanSession()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub CleanSession()
        Session.Remove(ViewState("CondicionComercial").ToString)
    End Sub


#End Region




    Dim valCant As Boolean
    Private Sub btAddProductos_AddProd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAddProductos_AddProd.Click
        Try
            ViewState.Add("idarea", dlarea.SelectedValue)


            ActualizarDetalleDocumentoView()

            Me.ListaProductos = Me.ListaDetalleDocumentoView


            For Each fila As GridViewRow In Me.gvListaProducto.Rows
                Dim cant As Decimal = CDec(CType(fila.Cells(0).FindControl("gtbcantidad"), TextBox).Text)
                If cant > 0 Then

                    valCant = False

                    For Each row As GridViewRow In gvProducto.Rows
                        If CInt(CType(row.Cells(4).FindControl("IdProductoGrid"), HiddenField).Value) = _
                                CInt(CType(fila.Cells(0).FindControl("IdProductoLista"), HiddenField).Value) And
                            CInt(CType(row.Cells(5).FindControl("cboUnidadMedida"), DropDownList).SelectedValue) = _
                                CInt(CType(fila.Cells(5).FindControl("hdd_IdUnidadMedida"), HiddenField).Value) Then

                            ListaProductos(row.RowIndex).Cantidad = cant
                            valCant = True
                            Exit For
                        End If
                    Next


                    If valCant = False Then
                        objDetalleDocumentoView = New Entidades.DetalleDocumentoView
                        With objDetalleDocumentoView
                            .IdProducto = CInt(CType(fila.Cells(0).FindControl("IdProductoLista"), HiddenField).Value)
                            .CodigoProducto = HttpUtility.HtmlDecode(fila.Cells(1).Text)
                            .Cantidad = cant
                            .Descripcion = HttpUtility.HtmlDecode(fila.Cells(2).Text)
                            .StockDisponible = CDec(fila.Cells(4).Text)
                            .IdUMedida = CInt(CType(fila.Cells(5).FindControl("hdd_IdUnidadMedida"), HiddenField).Value)
                            .IdMoneda = CInt(dlMoneda.SelectedValue)
                            .NomMoneda = CStr(fila.Cells(6).Text)
                            .Detraccion = CDec(fila.Cells(7).Text)
                            .PrecioSD = CDec(fila.Cells(7).Text)
                            .PrecioCD = CDec(fila.Cells(7).Text)
                            .Kit = CBool(CType(fila.Cells(12).FindControl("btnMostrarComponenteKit_Find"), ImageButton).Visible)
                            .Percepcion = CDec(fila.Cells(11).Text)
                            .codBarraFabricante = CStr(CType(fila.Cells(5).FindControl("hdd_CodBarraFabCapBus"), HiddenField).Value)

                            .ListaProdUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(CInt(CType(fila.Cells(0).FindControl("IdProductoLista"), HiddenField).Value))

                        End With

                        If objDetalleDocumentoView.Kit Then
                            listaKit = getListaKit()
                            If listaKit Is Nothing Then listaKit = New List(Of Entidades.Kit)
                            listaKit.AddRange((New Negocio.Kit).SelectComponentexIdKit_OC(objDetalleDocumentoView.IdProducto, CInt(dlMoneda.SelectedValue)))
                            setListaKit(listaKit)
                        End If

                        ListaProductos.Add(objDetalleDocumentoView)
                    End If
                End If
            Next


            gvProducto.DataSource = ListaProductos
            gvProducto.DataBind()

            Me.ListaDetalleDocumentoView = Me.ListaProductos

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "calcularImporteOnLoad(); onCapa('verBusquedaProducto');", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub ActualizarDetalleDocumentoView(Optional ByVal Index As Integer = -1)
        Me.ListaProductos = Me.ListaDetalleDocumentoView
        For i As Integer = 0 To Me.ListaProductos.Count - 1
            If Index = i Then Me.ListaProductos(i).IdUMPrincipal = Me.ListaProductos(i).IdUMedida
            Me.ListaProductos(i).Cantidad = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtcant"), TextBox).Text)
            Me.ListaProductos(i).IdUMedida = CInt(CType(Me.gvProducto.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
            Me.ListaProductos(i).NomUMPrincipal = CType(Me.gvProducto.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedItem.Text
            Me.ListaProductos(i).PrecioSD = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtprecioCompra"), TextBox).Text)
            Me.ListaProductos(i).Descuento = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtdcto"), TextBox).Text)
            Me.ListaProductos(i).Pdscto = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtpdcto"), TextBox).Text)
            Me.ListaProductos(i).dc_Descuento1 = CDec(CType(Me.gvProducto.Rows(i).FindControl("hdd_descuento1"), HiddenField).Value)
            Me.ListaProductos(i).dc_Descuento2 = CDec(CType(Me.gvProducto.Rows(i).FindControl("hdd_descuento2"), HiddenField).Value)
            Me.ListaProductos(i).dc_Descuento3 = CDec(CType(Me.gvProducto.Rows(i).FindControl("hdd_descuento3"), HiddenField).Value)
            Me.ListaProductos(i).PrecioCD = CDec(CType(Me.gvProducto.Rows(i).FindControl("txtprecio"), TextBox).Text)
            Me.ListaProductos(i).Importe = Me.ListaProductos(i).Cantidad * Me.ListaProductos(i).PrecioCD
            Me.ListaProductos(i).Percepcion = CDec(CType(Me.gvProducto.Rows(i).FindControl("gtbpercepcion"), TextBox).Text)
        Next
        Me.ListaDetalleDocumentoView = ListaProductos
    End Sub
    Private Sub cmbSubLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        If cmbSubLinea_AddProd.Items.Count > 0 And cmbSubLinea_AddProd.SelectedValue <> "0" Then
            drop.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
            Me.GV_ComponenteKit_Find.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('verBusquedaProducto'); onChange_txtDescripcionProd_AddProd();", True)
        End If
    End Sub
#Region "Filtro de busquedad avanza de productos"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('verBusquedaProducto'); onChange_txtDescripcionProd_AddProd();", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "verBusquedaProducto")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('verBusquedaProducto'); onChange_txtDescripcionProd_AddProd();", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1
            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)
        Next

        Return dt
    End Function

    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function

#End Region
#Region "Condicion Comercial"
    Function getCondicionComercial() As List(Of Entidades.CondicionComercial)
        ListaCondicionComercial = New List(Of Entidades.CondicionComercial)
        For Each row As GridViewRow In Me.gvCondicion.Rows
            Dim obj As New Entidades.CondicionComercial

            obj.Id = CInt(CType(row.FindControl("gv_hdd_IdCondicionComercial"), HiddenField).Value)
            obj.Descripcion = HttpUtility.HtmlDecode(CType(row.FindControl("gv_lbl_Descripcion"), Label).Text)
            obj.dcc_fecha = CStr(CType(row.FindControl("gv_txt_Fecha"), TextBox).Text)
            obj.NroDiasCondi = CInt(CType(row.FindControl("txtnrodias"), TextBox).Text)
            obj.FlagCC = CInt(CType(row.FindControl("hddFlag"), HiddenField).Value)
            obj.dcc_fechabl = TxtFechaNueva.Text
            ListaCondicionComercial.Add(obj)

        Next

        Return ListaCondicionComercial
    End Function
    Private Sub addCondicionComercial()
        ListaCondicionComercial = getCondicionComercial()
        Dim obj As New Entidades.CondicionComercial
        obj.Id = CInt(cboCondicionComercial.SelectedValue)
        obj.Descripcion = HttpUtility.HtmlDecode(cboCondicionComercial.SelectedItem.Text)
        'Dim NroDias As Integer = New Negocio.CondicionComercial().SelectxIdCondicionComercial(obj.Id)
        Dim objCondicionComercial As Entidades.CondicionComercial = New Negocio.CondicionComercial().SelectxIdCondicionComercial(obj.Id)
        obj.NroDiasCondi = objCondicionComercial.NroDiasCondi
        obj.FlagCC = objCondicionComercial.FlagCC
        'obj.dcc_fechabl = TxtFechaNueva.Text
        Dim NroDias As Integer = 0
        Dim calcular As Integer = 0
        Dim validacion As Integer = 0
        NroDias = obj.NroDiasCondi
        For i As Integer = 0 To ListaCondicionComercial.Count - 1
            calcular = ListaCondicionComercial(i).FlagCC
            If (calcular = 1 And obj.FlagCC = 1) Then
                validacion = validacion + 1
            Else
                validacion = 0
            End If
        Next



        If (validacion > 0) Then
            objScript.mostrarMsjAlerta(Me, "No se puede agregar mas de una condición con Nro de días mayores a 0")
        Else

            ListaCondicionComercial.Add(obj)
            gvCondicion.DataSource = ListaCondicionComercial
            gvCondicion.DataBind()

            Dim Today As Date = Nothing
            Dim NewDate As Date = Nothing
            Dim FechaBL As Date = Nothing


            'If (NroDias > 0) Then
            '    For Each gvRow As GridViewRow In gvCondicion.Rows
            '        Dim NroDiasCondi As TextBox = DirectCast(gvRow.FindControl("txtnrodias"), TextBox)
            '        Dim FechaCondi As TextBox = DirectCast(gvRow.FindControl("gv_txt_Fecha"), TextBox)

            '        If (NroDias <> 0) Then
            '            Today = System.DateTime.Now
            '            NewDate = Today.AddDays(NroDias)

            '            FechaCondi.Text = (NewDate.ToString("dd/MM/yyyy"))
            '            'NroDiasCondi.Text = CStr(NroDias)
            '            FechaCondi.Enabled = False
            '            NroDiasCondi.Enabled = False

            '            tbfechaCancelacion.Text = (NewDate.ToString("dd/MM/yyyy"))
            '            txtnrodiastbfechaCancelacion.Text = CStr(NroDias)
            '            Exit For
            '        Else
            '            Today = System.DateTime.Now
            '            NroDiasCondi.Text = CStr(0)
            '            tbfechaCancelacion.Text = (Today.ToString("dd/MM/yyyy"))
            '            FechaCondi.Enabled = True
            '            NroDiasCondi.Enabled = True
            '            Exit For
            '        End If
            '    Next

            Dim contador As Integer = gvCondicion.Rows.Count - 1
            For x As Integer = 0 To gvCondicion.Rows.Count - 1
                If (contador = x) Then
                    Dim NroDiasCondi2 As TextBox = DirectCast(gvCondicion.Rows(x).Cells(1).FindControl("txtnrodias"), TextBox)
                    Dim FechaCondi2 As TextBox = DirectCast(gvCondicion.Rows(x).Cells(2).FindControl("gv_txt_Fecha"), TextBox)
                    If (TxtFechaNueva.Text = "" Or TxtFechaNueva.Text = Nothing) Then
                        FechaBL = Date.Now
                    Else
                        FechaBL = CDate(TxtFechaNueva.Text)
                    End If
                    Today = FechaBL.AddDays(NroDias)
                    NewDate = FechaBL.AddDays(NroDias)
                    NroDiasCondi2.Text = CStr(NroDias)

                    If (NroDias > 0) Then
                        FechaCondi2.Text = (NewDate.ToString("dd/MM/yyyy"))
                        FechaCondi2.Enabled = False
                        NroDiasCondi2.Enabled = False
                        tbfechaCancelacion.Text = (NewDate.ToString("dd/MM/yyyy"))
                        txtnrodiastbfechaCancelacion.Text = CStr(NroDias)

                    Else
                        FechaCondi2.Text = (Today.ToString("dd/MM/yyyy"))
                        FechaCondi2.Enabled = True
                        NroDiasCondi2.Enabled = True
                    End If
                End If

            Next

            TxtFechaNueva.Enabled = False
            TxtNumDias.Enabled = False
            For Each gvRow As GridViewRow In gvCondicion.Rows
                Dim NroDiasCondi As TextBox = DirectCast(gvRow.FindControl("txtnrodias"), TextBox)
                Dim FechaCondi As TextBox = DirectCast(gvRow.FindControl("gv_txt_Fecha"), TextBox)
                If (CInt(NroDiasCondi.Text) > 0) Then
                    FechaCondi.Enabled = False
                    NroDiasCondi.Enabled = False
                    End if
            Next

        End If

    End Sub
    Private Sub btcondicioncomercial_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btcondicioncomercial.Click
        Try
            If (CInt(ddlAlmacen.SelectedValue) = 0) Then
                objScript.mostrarMsjAlerta(Me, "Seleccione un Almacén. No procede la operación.")
            ElseIf (CStr(TxtNumDias.Text) = "" Or TxtNumDias.Text = Nothing) Then
                objScript.mostrarMsjAlerta(Me, "Ingrese una fecha BL para continuar con la adición de la Condición Comercial.")
            Else
                addCondicionComercial()
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub onSelect_gvCondicion(ByVal index As Integer)
        ListaCondicionComercial = getCondicionComercial()
        ListaCondicionComercial.RemoveAt(index)
        gvCondicion.DataSource = ListaCondicionComercial
        gvCondicion.DataBind()
        TxtFechaNueva.Enabled = True
        TxtNumDias.Enabled = True
    End Sub

    Private Sub gvCondicion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCondicion.SelectedIndexChanged
        Try
            onSelect_gvCondicion(gvCondicion.SelectedIndex)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region
#Region "Tipo de Orden de Compra"
    Private Sub onChangeTipoOC()
        Try


            Select Case rbtipodeoc.SelectedValue
                Case "True" ' Importada
                    drop.LlenarCboMonedaNoBase(dlMoneda)
                    Me.rbcondicion.SelectedValue = "0" ' ******************** Extranjero
                    Me.rbigv.SelectedValue = "0"  '      ******************** No Incluye IGV
                    Me.dltipoPrecio.Enabled = True

                Case Else  ' Nacional - Activos
                    Me.rbcondicion.SelectedValue = "1" ' ******************** Nacional
                    Me.drop.LlenarCboMoneda(dlMoneda)
                    Me.dltipoPrecio.Enabled = False
                    Me.dltipoPrecio.SelectedIndex = 0

            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub rbtipodeoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtipodeoc.SelectedIndexChanged
        onChangeTipoOC()
    End Sub
#End Region
#Region "Enviar Correo"

    Private mFrom As String
    Private mTo As String
    Private mSubject As String
    Private mMsg As String

    Private mCC As String

    Private mMailServer As String
    Private mPort As Integer
    Private mHost As String
    Private mCuenta As String
    Private mClave As String

    Private NameFile As String

    Private Sub btn_enviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_enviar.Click
        onClick_btn_Enviar()
    End Sub
    Private Sub onClick_btn_Enviar()
        Dim lista As List(Of Entidades.ConfiguracionCorreo) = _
        (New Negocio.ConfiguracionCorreo).ConfiguracionCorreo_select(CInt(dlPropietario.SelectedValue), CInt(dlTienda.SelectedValue), 1)

        If lista.Count = 0 Then
            objScript.mostrarMsjAlerta(Me, "NO SE HA CONFIGURADO EL CORREO.")
            Exit Sub
        End If


        mFrom = ddl_usuario.SelectedItem.Text.Trim.ToLower
        mTo = ddl_contact_supplier_mail.SelectedItem.Text.Trim.ToLower
        mSubject = txt_Asunto.Text.Trim
        mMsg = txt_mensaje.Text.Trim

        mCC = "" ' COPIA A A UNA PERSONA

        mMailServer = lista(0).servidorCorreo.Trim
        mPort = lista(0).PuertoServidorCorreo
        mHost = lista(0).HostInteligente
        mCuenta = lista(0).CuentaCorreo.Trim
        mClave = lista(0).Clave.Trim

        NameFile = dlSerie.SelectedItem.Text.Trim + "-" + tbCodigoDocumento.Text.Trim

        onClick_GenerarPDF(NameFile)

        Try

            Dim message As New MailMessage(mFrom, mTo, mSubject, mMsg)

            Dim attached As New Attachment(Trim(Server.MapPath("~/Compras/" + NameFile + ".pdf")))
            message.Attachments.Add(attached)


            If mCC <> "" Or mCC <> String.Empty Then
                Dim strCC() As String = Split(mCC, ";")
                Dim strThisCC As String
                For Each strThisCC In strCC
                    message.CC.Add(Trim(strThisCC))
                Next
            End If

            Dim mySmtpClient As New SmtpClient(mMailServer, mPort)
            mySmtpClient.Host = mHost

            Dim SMTPUserInfo As New NetworkCredential(mCuenta, mClave)
            mySmtpClient.UseDefaultCredentials = False

            mySmtpClient.Credentials = SMTPUserInfo
            mySmtpClient.Send(message)
            message.Dispose()

            objScript.mostrarMsjAlerta(Me, "El Mensaj ha sido enviado " & message.To.ToString())

        Catch ex As FormatException
            objScript.mostrarMsjAlerta(Me, "Format Exception: " & ex.Message)
        Catch ex As SmtpException
            objScript.mostrarMsjAlerta(Me, "SMTP Exception:  " & ex.Message)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "General Exception:  " & ex.Message)
        Finally
            If File.Exists(Server.MapPath("~/Compras/" + NameFile + ".pdf")) Then
                File.Delete(Server.MapPath("~/Compras/" + NameFile + ".pdf"))
            End If
        End Try
    End Sub

    Private Sub onClick_GenerarPDF(ByVal nameFile As String)

        Dim doc As New ReportDocument()
        Dim ds As DataSet = (New Negocio.OrdenCompra).getDataSetOrdenCompra(CInt(hddIdDocumento.Value))

        If ds IsNot Nothing Then
            Dim objCrystal As New CR_DocOrdenCompra
            objCrystal.SetDataSource(ds)
            doc = objCrystal
            Dim exportOpts As ExportOptions = doc.ExportOptions
            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat
            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile
            exportOpts.DestinationOptions = New DiskFileDestinationOptions()

            Dim diskOpts As New DiskFileDestinationOptions()
            CType(doc.ExportOptions.DestinationOptions, DiskFileDestinationOptions).DiskFileName = Server.MapPath("~/Compras/" + nameFile + ".pdf")

            doc.Export()
            doc.Dispose()

        End If

    End Sub

#End Region
    Private Sub ddl_contactoPropietario_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_contactoPropietario.SelectedIndexChanged

        drop.llenarCboCorreoxIdPersona(ddl_usuario, CInt(ddl_contactoPropietario.SelectedValue))
    End Sub
    Private Sub ddl_contactoProveedor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_contactoProveedor.SelectedIndexChanged
        CargarContactoProveedor(CInt(ddl_contactoProveedor.SelectedValue))
    End Sub
    Private Sub btnAceptarCodBarraFabr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarCodBarraFabr.Click
        BuscarProducto()
        Me.txtCodBarraFabricante.Text = ""
        txtCodBarraFabricante.Focus()
    End Sub
    Private Sub BuscarProducto()
        Try
            ViewState.Add("codBarraFab", Me.txtCodBarraFabricante.Text)
            cargarDatosProductoGrillaCodBarrasFabricante(gvProducto, CStr(ViewState("codBarraFab")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrillaCodBarrasFabricante(ByVal grilla As GridView, ByVal codBarraFab As String)

        Try
            Dim incluyeigv As Boolean = CBool(rbigv.SelectedValue)
            Dim valProdRep As Boolean = False

            Me.ActualizarDetalleDocumentoView()
            Me.ListaProductos = Me.ListaDetalleDocumentoView

            For Each objdet As Entidades.DetalleDocumentoView In Me.ListaProductos
                If objdet.codBarraFabricante = codBarraFab Then
                    objdet.Cantidad = objdet.Cantidad + 1
                    valProdRep = True
                    Exit For
                End If
            Next

            'For j As Integer = 0 To getListaProductosX.Count - 1
            '    If getListaProductosX(j).codBarraFabricante = codBarraFab Then
            '        getListaProductosX(j).Cantidad = getListaProductosX(j).Cantidad + 1
            '        valProdRep = True
            '        Exit For
            '    End If
            'Next
            Dim IdAgentePersona As Integer = 0
            If IsNumeric(ddl_agenteProveedor.SelectedValue) Then
                IdAgentePersona = CInt(ddl_agenteProveedor.SelectedValue)
            End If

            If valProdRep = False Then
                ListaCatalogoProductos = (New Negocio.OrdenCompra).listarProductosProveedorOrdenCompraCodBarraFab( _
            CInt(dlPropietario.SelectedValue), CInt(hdd_Idproveedor.Value), IdAgentePersona _
            , CInt(dlMoneda.SelectedValue), _
              CInt(dlarea.SelectedValue), incluyeigv, codBarraFab)

                If ListaCatalogoProductos.Count <= 0 Then
                    'ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onChange_txtCodBarraFabricante(); alert('No se hallaron registros'); ", True)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "alert('No se hallaron registros'); ", True)
                Else
                    AddProductoAlDetalleDocumento(grilla, ListaCatalogoProductos)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "calcularImporteOnLoad(); ", True)
                End If
            Else

                grilla.DataSource = ListaProductos
                grilla.DataBind()

                Me.ListaProductos = Me.ListaDetalleDocumentoView

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "calcularImporteOnLoad(); ", True)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub
    Private Sub AddProductoAlDetalleDocumento(ByVal gridvw As GridView, ByVal ProductoCobBar As List(Of Entidades.Catalogo))
        Try

            Me.ActualizarDetalleDocumentoView()
            Me.ListaProductos = Me.ListaDetalleDocumentoView

            Dim DetalleDoc As Entidades.DetalleDocumentoView

            For i As Integer = 0 To ProductoCobBar.Count - 1

                DetalleDoc = New Entidades.DetalleDocumentoView
                With DetalleDoc
                    .IdProducto = ProductoCobBar(i).IdProducto
                    .CodigoProducto = ProductoCobBar(i).CodigoProducto
                    .Cantidad = 1
                    .Descripcion = ProductoCobBar(i).Descripcion
                    .StockDisponible = ProductoCobBar(i).StockDisponible
                    .NomUMPrincipal = ProductoCobBar(i).NomTipoPV
                    .IdUMPrincipal = ProductoCobBar(i).IdUnidadMedida
                    .IdMoneda = CInt(dlMoneda.SelectedValue)
                    .NomMoneda = dlMoneda.Text
                    .Detraccion = ProductoCobBar(i).Detraccion
                    .PrecioSD = ProductoCobBar(i).PrecioSD
                    .PrecioCD = ProductoCobBar(i).PrecioLista
                    .Percepcion = ProductoCobBar(i).Percepcion
                    .codBarraFabricante = ProductoCobBar(i).codBarraFabricante

                End With
                ListaProductos.Add(DetalleDoc)

            Next
            gridvw.DataSource = ListaProductos
            gridvw.DataBind()

            Me.ListaDetalleDocumentoView = Me.ListaProductos

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia.SelectedIndexChanged
        Try
            If CInt(cboTipoExistencia.SelectedValue) <> 0 Then


                drop.LlenarCboLineaxTipoexistenciaxArea(cmbLinea_AddProd, CInt(dlarea.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)
                ' drop.LlenarCboLineaxArea(cmbLinea_AddProd, CInt(dlarea.SelectedValue), True)
            Else
                cmbLinea_AddProd.Items.Clear()

                txtCodigoProducto.Text = ""
                txtDescripcionProd_AddProd.Text = ""
                txtCodigoProveedor.Text = ""

            End If

            Dim msj As String = ""
            If gvProducto.Rows.Count > 0 Then
                If CStr(ViewState("idarea")) <> dlarea.SelectedValue And ViewState("idarea") IsNot Nothing Then

                    For x As Integer = 0 To dlarea.Items.Count - 1
                        With dlarea.Items(x)
                            If .Value = CStr(ViewState("idarea")) Then
                                msj = "La cuadricula de productos contiene registros relacionado al area " + .Text
                                If .Value = "0" Then msj = "La cuadricula de productos contiene registros que no estan relacionados con areas"
                                Exit For
                            End If
                        End With
                    Next
                    objScript.mostrarMsjAlerta(Me, msj)
                    Exit Sub
                End If
            End If
            Me.GV_ComponenteKit_Find.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('verBusquedaProducto'); LimpiarGrillaProductos();", True)

            'ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('verBusquedaProducto'); onChange_txtDescripcionProd_AddProd();", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub gvProducto_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvProducto.RowDataBound
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then


                If (Me.ListaProductos(e.Row.RowIndex).Kit) Then  '********* KIT

                    CType(e.Row.FindControl("btnMostrarComponenteKit"), ImageButton).Visible = True

                Else

                    CType(e.Row.FindControl("btnMostrarComponenteKit"), ImageButton).Visible = False

                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    'KIT CAPA BUSCAR PRODUCTO
    Protected Sub btnMostrarComponenteKit_Find_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim index As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex

            Dim ListaKit As List(Of Entidades.Kit) = (New Negocio.Kit).SelectComponentexIdKit_OC(CInt(CType(Me.gvListaProducto.Rows(index).FindControl("IdProductoLista"), HiddenField).Value), CInt(dlMoneda.SelectedValue))

            Me.GV_ComponenteKit_Find.DataSource = ListaKit

            Me.GV_ComponenteKit_Find.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapa('verBusquedaProducto');", True)
        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    'KIT FORMULARIO PRINCIPAL
    Protected Sub btnMostrarComponenteKit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim index As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex
            Dim _Monto As Decimal
            _ListaKit = New List(Of Entidades.Kit)

            hdd_IdKit_find.Value = CType(Me.gvProducto.Rows(index).FindControl("IdProductoGrid"), HiddenField).Value
            tb_PrecioComp.Text = CType(Me.gvProducto.Rows(index).Cells(10).FindControl("txtprecio"), TextBox).Text
            If Not IsNumeric(tb_PrecioComp.Text) Then tb_PrecioComp.Text = "0.00"
            lb_Kit.Text = HttpUtility.HtmlDecode(gvProducto.Rows(index).Cells(3).Text)

            listaKit = getListaKit()

            Dim totalProducto As Decimal = 0
            totalProducto = CDec(tb_PrecioComp.Text)

            If listaKit IsNot Nothing Then
                _ListaKit = listaKit.FindAll(Function(K As Entidades.Kit) K.IdKit = CInt(hdd_IdKit_find.Value))

                Dim TotalCalculado As Decimal = 0
                For x As Integer = 0 To _ListaKit.Count - 1

                    _ListaKit(x).Precio_Comp = ((_ListaKit(x).PorcentajeKit * totalProducto) / (100))
                    TotalCalculado = ((_ListaKit(x).PorcentajeKit * totalProducto) / (100))
                    _Monto = _Monto + TotalCalculado
                Next

                tb_PrecComponente.Text = CStr(Math.Round(_Monto, 3))
                '_ListaKit.Add()

                Me.GV_ComponenteKit.DataSource = _ListaKit
                Me.GV_ComponenteKit.DataBind()

            Else
                tb_PrecComponente.Text = "0.00"
            End If

            objScript.onCapa(Me, "verBusquedaKit")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

        'For k As Integer = 0 To listaKit.Count - 1
        '    Dim objkit As Entidades.Kit = New Entidades.Kit
        '    objkit.Precio_Comp += ((listaKit(k).PorcentajeKit * listaKit(k).Precio_Comp) / (100))
        'Next
        'listaKit.Add(objKit)

    End Sub
    'ACEPTAR PRECIOS DEL KIT SELECCIONADO
    Private Sub btn_Aceptar_Precio_Kit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Aceptar_Precio_Kit.Click
        Try
            listaKit = getGV_ComponenteKit(getListaKit(), CInt(hdd_IdKit_find.Value))
            setListaKit(listaKit)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Function getGV_ComponenteKit(ByVal inLista As List(Of Entidades.Kit), ByVal IdKit As Integer) As List(Of Entidades.Kit)
        _ListaKit = New List(Of Entidades.Kit)
        Dim IdComponente As Integer
        Dim Index As Integer = 0

        _ListaKit.AddRange(inLista.FindAll(Function(K As Entidades.Kit) K.IdKit = IdKit))
        inLista.RemoveAll(Function(K As Entidades.Kit) K.IdKit = IdKit)

        For Each row As GridViewRow In Me.GV_ComponenteKit.Rows
            IdComponente = CInt(CType(row.FindControl("hddIdComponente"), HiddenField).Value)
            Index = _ListaKit.FindIndex(Function(EK As Entidades.Kit) EK.IdComponente = IdComponente And EK.IdKit = IdKit)
            _ListaKit(Index).Precio_Comp = CDec(CType(row.FindControl("txtprecio_comp"), TextBox).Text)
        Next
        inLista.AddRange(_ListaKit)

        Return inLista
    End Function


    Function valPrecioCompraxComponentes(ByVal IdKit As Integer) As Boolean
        Try


            Dim _Monto As Decimal = CDec(tb_PrecioComp.Text)
            _ListaKit = New List(Of Entidades.Kit)

            listaKit = getListaKit()
            If listaKit IsNot Nothing Then


                If IdKit <> 0 Then

                    Me.ActualizarDetalleDocumentoView()
                    Me.ListaProductos = Me.ListaDetalleDocumentoView

                    Dim IdProduct As Integer


                    For i As Integer = 0 To ListaProductos.Count - 1
                        If ListaProductos(i).Kit Then
                            IdProduct = ListaProductos(i).IdProducto
                            _ListaKit.AddRange(listaKit.FindAll(Function(K As Entidades.Kit) K.IdKit = IdProduct))
                            For x As Integer = 0 To _ListaKit.Count - 1
                                _Monto = _Monto + _ListaKit(x).Precio_Comp
                            Next

                            If _Monto > ListaProductos(i).PrecioCD Then
                                Throw New Exception("La suma de los precios de los componentes no pueden superar el precio de")
                                Return False
                            Else
                                _Monto = 0
                            End If

                        End If
                    Next

                    Me.ListaDetalleDocumentoView = Me.ListaProductos

                Else
                    _ListaKit.AddRange(listaKit.FindAll(Function(K As Entidades.Kit) K.IdKit = IdKit))
                    For x As Integer = 0 To _ListaKit.Count - 1
                        _Monto = _Monto + _ListaKit(x).Precio_Comp
                    Next

                    If _Monto > CDec(tb_PrecioComp.Text) And _Monto > 0 Then
                        Throw New Exception("La suma de los precios de los componentes no pueden superar el precio de")
                        Return False
                    End If

                End If

            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

        Return True
    End Function

    Private Sub gvListaProducto_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListaProducto.RowDataBound
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then


                If (Me.ListaCatalogoProductos(e.Row.RowIndex).Kit) Then  '********* KIT

                    CType(e.Row.FindControl("btnMostrarComponenteKit_Find"), ImageButton).Visible = True

                Else

                    CType(e.Row.FindControl("btnMostrarComponenteKit_Find"), ImageButton).Visible = False

                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



    Private Sub ddlAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAlmacen.SelectedIndexChanged
        getDireccionAlmacen()
    End Sub


#Region "********************************************** REGISTRAR ORDEN COMPRA"

    Private Function Validar_Documento() As Boolean
        Try

        
            If CInt(Me.dlcondicionpago.SelectedValue) = 2 Then ' ********** CREDITO

                Me.verCuenta_Proveedor(True)

            End If

            Return True

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Function

    Private Sub btguardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btguardar.Click

        Me.OnClick_Guardar()

    End Sub
    Private Sub OnClick_Guardar()

        If Me.Validar_Documento() Then
            If (gvCondicion.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "Falta Elegir una condición de pago")

            Else
                Me.RegistrarDocumento()
            End If

        End If

    End Sub
    Private Sub RegistrarDocumento()
        Try

            '******************** LISTAS
            Dim list_DetalleDocumento As List(Of Entidades.DetalleDocumento) = obtenerListaDetalleDocumento()
            Dim list_CondicionComercial As List(Of Entidades.CondicionComercial) = getCondicionComercial()

            '******************** ENTIDADES
            Dim obj_Documento As Entidades.Documento = obtenerDocumento()
            Dim obj_MontoRegimen As Entidades.MontoRegimen = obtenerMontoRegimen()
            Dim obj_Observacion As Entidades.Observacion = obtenerObservaciones()


            Dim obj_OrdenCompra As New Negocio.OrdenCompra

            Select Case CInt(Me.hddOperativo.Value)

                Case operativo.Nuevo

                    Dim idxcodigo() As String
                    idxcodigo = CStr(obj_OrdenCompra.InsertarOrdenCompra(obj_Documento, list_DetalleDocumento, obj_Observacion, obj_MontoRegimen, list_CondicionComercial)).Split(CChar(","))
                    Me.hddIdDocumento.Value = idxcodigo(0)
                    Me.tbCodigoDocumento.Text = idxcodigo(1)

                    objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")

                Case operativo.Editar

                    obj_OrdenCompra.updateDocumento(obj_Documento, list_DetalleDocumento, obj_Observacion, obj_MontoRegimen, list_CondicionComercial)

                    objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")

            End Select

            btimprimir.Visible = True
            btimprimir02.Visible = True
            Me.txt_Asunto.Text = dlSerie.SelectedItem.Text.Trim + " - " + tbCodigoDocumento.Text.Trim

            Me.hddOperativo.Value = CStr(operativo.Onload)
            verOrdenCompra(False, False, False)


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerDocumento() As Entidades.Documento
        Dim objDocumento As New Entidades.Documento

        Dim totalALetras As String = ""
        With objDocumento

            Select Case CInt((Me.hddOperativo.Value))
                Case operativo.Nuevo
                    .Id = Nothing
                Case operativo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            '====== documento
            .IdEmpresa = CInt(Me.dlPropietario.SelectedValue)
            .IdTienda = CInt(Me.dlTienda.SelectedValue)
            .IdSerie = CInt(Me.dlSerie.SelectedValue)
            .IdTipoDocumento = CInt(hdd_IdTipoDocumento.Value)
            .IdTipoOperacion = CInt(dlTipoOperacion.SelectedValue)
            .IdArea = CInt(dlarea.SelectedValue)
            .IdCondicionPago = CInt(dlcondicionpago.SelectedValue)

            '====== Fechas del documento
            .FechaEmision = CDate(Me.tbFechaEmision.Text)
            .FechaAEntregar = CDate(IIf(Me.txtFechaEntrega.Text = "", Nothing, Me.txtFechaEntrega.Text))
            .FechaVenc = CDate(IIf(Me.tbFechaVen.Text = "", Nothing, Me.tbFechaVen.Text))
            .FechaCancelacion = CDate(IIf(Me.tbfechaCancelacion.Text = "", Nothing, Me.tbfechaCancelacion.Text))

            '====== Personas
            .IdPersona = CInt(Me.hdd_Idproveedor.Value)
            .IdUsuario = CInt(hdd_IdUsuario.Value)
            .IdDestinatario = CInt(ddl_contactoProveedor.SelectedValue) ' suplier contact
            .IdRemitente = CInt(ddl_contactoPropietario.SelectedValue) 'owner contact

            '====== Montos del documento
            .Descuento = CDec(txtDescuento.Text.Trim)
            .SubTotal = CDec(txtSubTotal.Text.Trim)
            .IGV = CDec(txtIGV.Text.Trim)
            .Total = CDec(txtTotal.Text.Trim)
            .TotalAPagar = CDec(txt_TotalAPagar.Text.Trim)

            .IdMoneda = CInt(Me.dlMoneda.SelectedValue)
            Select Case .IdMoneda
                Case 1
                    totalALetras = "/100 SOLES"
                Case 2
                    totalALetras = "/100 DÓLARES AMERICANOS"
            End Select
            .TotalLetras = (New Aletras).Letras(Math.Round(.TotalAPagar, 2).ToString) + totalALetras

            '====== Estado del documento            
            .IdEstadoDoc = CInt(Me.dlEstadoDocumento.SelectedValue)
            .IdEstadoEntrega = 1 '**** Por entregar
            .IdEstadoCancelacion = 1 '*** Por Cancelar

            '====== Otros            

            .LugarEntrega = CInt(Me.ddlAlmacen.SelectedValue)
            .doc_Importacion = rbtipodeoc.SelectedValue
            .anex_PrecImportacion = CInt(dltipoPrecio.SelectedValue)
            .EnviarTablaCuenta = False
            .anex_igv = CBool(rbigv.SelectedValue)


            If IsNumeric(txtPerc.Text) Then
                If CDec(txtPerc.Text) > 0 Then
                    .CompPercepcion = True
                Else
                    .CompPercepcion = False
                End If
            End If

            ' If pnlcentroCosto.Visible = True Then .Direccion = getCentroCosto() Else .Direccion = "0000000000"

        End With



        Return objDocumento
    End Function
    Private Function obtenerListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)
        Me.ActualizarDetalleDocumentoView()
        Me.ListaProductos = Me.ListaDetalleDocumentoView

        Dim ListaDetalleDocumento As New List(Of Entidades.DetalleDocumento)


        'Interrupcion
        listaKit = getListaKit()
        If listaKit IsNot Nothing Then
            'If valPreciosComponentes(listaKit, ListaProductos) = False Then Exit Function
            valPreciosComponentes(listaKit, ListaProductos) ' ************ verificar
        End If

        For i As Integer = 0 To ListaProductos.Count - 1
            Dim objDetalleDocumento As New Entidades.DetalleDocumento
            With objDetalleDocumento
                .IdProducto = ListaProductos(i).IdProducto
                .IdUnidadMedida = ListaProductos(i).IdUMedida
                .UMedida = ListaProductos(i).NomUMPrincipal
                .Cantidad = ListaProductos(i).Cantidad
                .CantxAtender = ListaProductos(i).Cantidad
                .CantidadTransito = ListaProductos(i).Cantidad
                .dc_CantidadTransito = ListaProductos(i).Cantidad
                .PrecioSD = ListaProductos(i).PrecioSD
                .Descuento = ListaProductos(i).Descuento
                .TasaPercepcion = ListaProductos(i).Percepcion
                .PrecioCD = ListaProductos(i).PrecioCD
                .Importe = ListaProductos(i).Importe
                .IdDocumento = 0
                .dc_Descuento1 = ListaProductos(i).dc_Descuento1
                .dc_Descuento2 = ListaProductos(i).dc_Descuento2
                .dc_Descuento3 = ListaProductos(i).dc_Descuento3

                'If ListaProductos(i).Descuento > 0 Then '******************** PRECIO SIN DECUENTO

                '    If rbigv.SelectedValue = "1" Then ' ******************** INCLUYE IGV
                '        .dc_PrecioSinIGV = ListaProductos(i).PrecioSD / (CDec(hddIGV.Value) + 1)
                '    Else
                '        .dc_PrecioSinIGV = ListaProductos(i).PrecioSD
                '    End If


                'Else

                '****************************************************** PRECIO CON DESCUENTO

                If rbigv.SelectedValue = "1" Then  '******************** INCLUYE IGV
                    .dc_PrecioSinIGV = ListaProductos(i).PrecioCD / (CDec(hddIGV.Value) + 1)
                    .CostoMovIngreso = ListaProductos(i).PrecioSD / (CDec(hddIGV.Value) + 1)
                Else
                    .dc_PrecioSinIGV = ListaProductos(i).PrecioCD
                    .CostoMovIngreso = ListaProductos(i).PrecioSD
                End If

                'End If


            End With

            If ListaProductos(i).Kit Then
                objDetalleDocumento.CantxAtender = 0
                objDetalleDocumento.CantidadTransito = 0
                objDetalleDocumento.dc_CantidadTransito = 0
            End If

            ListaDetalleDocumento.Add(objDetalleDocumento)

            If ListaProductos(i).Kit Then
                Dim IdKit As Integer = ListaProductos(i).IdProducto
                _ListaKit = listaKit.FindAll(Function(pk As Entidades.Kit) pk.IdKit = IdKit)

                For k As Integer = 0 To _ListaKit.Count - 1


                    Dim objDetalleDocumentox As New Entidades.DetalleDocumento
                    With objDetalleDocumentox
                        .IdProducto = _ListaKit(k).IdComponente
                        .IdUnidadMedida = _ListaKit(k).IdUnidadMedida_Comp
                        .UMedida = _ListaKit(k).UnidadMedida_Comp
                        .Cantidad = ListaProductos(i).Cantidad * _ListaKit(k).Cantidad_Comp
                        .CantxAtender = ListaProductos(i).Cantidad * _ListaKit(k).Cantidad_Comp
                        .CantidadTransito = ListaProductos(i).Cantidad * _ListaKit(k).Cantidad_Comp
                        .dc_CantidadTransito = ListaProductos(i).Cantidad * _ListaKit(k).Cantidad_Comp
                        .PrecioSD = _ListaKit(k).Precio_Comp
                        .Descuento = 0
                        .TasaPercepcion = 0
                        .PrecioCD = _ListaKit(k).Precio_Comp
                        .Importe = _ListaKit(k).Precio_Comp * (ListaProductos(i).Cantidad * _ListaKit(k).Cantidad_Comp)
                        .IdDocumento = 0
                        .Kit = True
                        .IdKit = IdKit


                        If rbigv.SelectedValue = "1" Then  '******************** INCLUYE IGV
                            .dc_PrecioSinIGV = .PrecioCD / (CDec(hddIGV.Value) + 1)
                            .CostoMovIngreso = .PrecioSD / (CDec(hddIGV.Value) + 1)
                        Else
                            .dc_PrecioSinIGV = .PrecioCD
                            .CostoMovIngreso = .PrecioSD
                        End If


                    End With
                    ListaDetalleDocumento.Add(objDetalleDocumentox)
                Next


            End If

        Next

        Me.ListaDetalleDocumentoView = Me.ListaProductos

        Return ListaDetalleDocumento


    End Function
    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtobservacion.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddOperativo.Value))
                Case operativo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case operativo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtobservacion.Text.Trim

        End If

        Return objObservacion

    End Function
    Private Function obtenerMontoRegimen() As Entidades.MontoRegimen
        Dim objMontoRegimen As New Entidades.MontoRegimen

        With objMontoRegimen
            .Monto = CDec(IIf(Me.txtPerc.Text = "", 0, Me.txtPerc.Text))
            .ro_Id = 1 ' ******************************** PERCEPCION
        End With

        Return objMontoRegimen
    End Function


#End Region

#Region "********************************************** BUSCAR ORDEN COMPRA"
    Private Sub btnBuscarDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumento.Click

        Try
            OnClick_BuscarDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub OnClick_BuscarDocumento(Optional ByVal IdDocumento As Integer = 0)

        ' ********************************** ENTIDADES
        Dim obj_Documento As Entidades.Documento = (New Negocio.OrdenCompra).SelectOrdenCompra(CInt(Me.dlSerie.SelectedValue), CInt(tbCodigoDocumento.Text), IdDocumento)
        Dim obj_Observacion As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(obj_Documento.Id)

        ' ********************************** LISTAS
        Dim list_CondicionComercial As List(Of Entidades.CondicionComercial) = (New Negocio.Documento_CondicionC).SelectxIdDocumento(obj_Documento.Id)
        Dim list_DetalleDocumentoView As List(Of Entidades.DetalleDocumentoView) = (New Negocio.OrdenCompra).DetalleDocumentoViewSelectxIdDocumento(obj_Documento.Id)



        drop.LlenarCboAreaxIdusuario(dlarea, CInt(hdd_IdUsuario.Value), obj_Documento.IdArea)


        CargarOrdenCompra_GUI(obj_Documento, list_DetalleDocumentoView, list_CondicionComercial, obj_Observacion)

        Me.txt_Asunto.Text = Me.dlSerie.SelectedItem.Text.Trim + " - " + Me.tbCodigoDocumento.Text.Trim


        hddOperativo.Value = CStr(operativo.Buscar_Exito)
        verOrdenCompra(False, False, False)

    End Sub
    Private Sub CargarOrdenCompra_GUI(ByVal objDocumento As Entidades.Documento, ByVal listDetalleDocumentoView As List(Of Entidades.DetalleDocumentoView), ByVal listCondicionComercial As List(Of Entidades.CondicionComercial), ByVal objObservacion As Entidades.Observacion)
        ' ************************************* ENTIDADES DOCUMENTO

        With objDocumento

            Me.dlPropietario.SelectedValue = CStr(.IdEmpresa)
            Me.dlTienda.SelectedValue = CStr(.IdTienda)
            Me.hddIdDocumento.Value = CStr(.Id)
            Me.tbCodigoDocumento.Text = objDocumento.Codigo

            If .IdEstadoDoc = 3 Then

                Me.hddOperativo.Value = CStr(operativo.Buscar_Exito)
                verOrdenCompra(False, False, False)

                Return
            End If

            If Me.dlTipoOperacion.Items.FindByValue(CStr(.IdTipoOperacion)) IsNot Nothing Then
                Me.dlTipoOperacion.SelectedValue = CStr(.IdTipoOperacion)
            End If

            Me.dlarea.SelectedValue = CStr(.IdArea)
            ViewState.Add("idarea", dlarea.SelectedValue)

            Me.dlcondicionpago.SelectedValue = CStr(.IdCondicionPago)

            '==== Montos del documento
            Me.rbtipodeoc.SelectedValue = .doc_Importacion.ToString
            Me.onChangeTipoOC()

            Me.dlMoneda.SelectedValue = CStr(.IdMoneda)
            Me.txtDescuento.Text = CStr(Math.Round(.Descuento, 3))
            Me.txtSubTotal.Text = CStr(Math.Round(.SubTotal, 3))
            Me.txtIGV.Text = CStr(Math.Round(.IGV, 3))
            Me.txtTotal.Text = CStr(Math.Round(.Total, 3))
            Me.txtPerc.Text = CStr(CDec(Math.Round((New Negocio.MontoRegimen).SelectPercepcionxIdDocumento(.Id), 2)))
            Me.txt_TotalAPagar.Text = CStr(Math.Round(.TotalAPagar, 3))

            '==== Estado del documento
            Me.dlEstadoDocumento.SelectedValue = CStr(.IdEstadoDoc)
            Me.dlEstadoCancelacion.SelectedValue = CStr(.IdEstadoCancelacion)
            Me.dlEstadoEntrega.SelectedValue = CStr(.IdEstadoEntrega)

            '==== Fechas del documento
            Me.tbFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
            Me.tbFechaVen.Text = Format(.FechaVenc, "dd/MM/yyyy")
            Me.tbfechaCancelacion.Text = Format(.FechaCancelacion, "dd/MM/yyyy")
            Me.txtFechaEntrega.Text = Format(.FechaAEntregar, "dd/MM/yyyy")

            '==== Otros
            Me.dltipoPrecio.SelectedValue = CStr(.anex_PrecImportacion)
            Me.hdd_MovCuenta.Value = CStr(CInt(.ValorReferencial))
            Me.hdd_IdDocRelacionado.Value = CStr(.IdDocRelacionado)
            Me.rbigv.SelectedValue = CStr(IIf(.anex_igv = True, "1", "0"))

        End With

        ' ********************************************** CONTACTO DEL PROPIETARIO
        If objDocumento.IdRemitente > 0 Then
            If ddl_contactoPropietario.Items.FindByValue(CStr(objDocumento.IdRemitente)) Is Nothing Then
                Dim lista As List(Of Entidades.PersonaView) = (New Negocio.OrdenCompra).listarContactoxIdPersona(objDocumento.IdEmpresa, objDocumento.IdRemitente)

                If lista IsNot Nothing Then
                    For i As Integer = 0 To lista.Count - 1
                        If ddl_contactoPropietario.Items.FindByValue(CStr(lista(i).IdPersona)) Is Nothing Then
                            ddl_contactoPropietario.Items.Add(New ListItem(lista(i).Nombres, CStr(lista(i).IdPersona)))
                        End If
                    Next
                    ddl_contactoPropietario.SelectedValue = CStr(objDocumento.IdRemitente)
                End If
            Else
                ddl_contactoPropietario.SelectedValue = CStr(objDocumento.IdRemitente)
            End If
        End If


        ' ********************************************** ALMACEN DE DESTINO
        If Not Me.ddlAlmacen.Items.FindByValue(CStr(objDocumento.LugarEntrega)) Is Nothing Then
            Me.ddlAlmacen.SelectedValue = CStr(objDocumento.LugarEntrega)
            getDireccionAlmacen()
        End If


        ' ********************************************** PERSONA
        Me.CargarProveedor(objDocumento.IdPersona, objDocumento.IdDestinatario)


        ' ********************************************** DETALLE DOCUMENTO
        Me.ListaProductos = DetalleDocumentoView_OnLoad(listDetalleDocumentoView)
        Me.ListaDetalleDocumentoView = ListaProductos
        Me.gvProducto.DataSource = ListaProductos
        Me.gvProducto.DataBind()



        ' ********************************************** CONDICIONES COMERCIALES 
        Me.set_CondicionComercial(listCondicionComercial)
        Me.gvCondicion.DataSource = listCondicionComercial
        Me.gvCondicion.DataBind()


        ' ********************************************** OBSERVACIONES 
        If Not objObservacion Is Nothing Then
            Me.txtobservacion.Text = HttpUtility.HtmlDecode(objObservacion.Observacion).Trim
        End If


    End Sub

    Private Function DetalleDocumentoView_OnLoad(ByVal listDetalleDocumentoView As List(Of Entidades.DetalleDocumentoView)) As List(Of Entidades.DetalleDocumentoView)

        Me.listaKit = New List(Of Entidades.Kit)

        For x As Integer = 0 To listDetalleDocumentoView.Count - 1
            listDetalleDocumentoView(x).IdMoneda = CInt(Me.dlMoneda.SelectedValue)
            If listDetalleDocumentoView(x).Kit Then
                listaKit.AddRange((New Negocio.Kit).SelectComponentexIdKitDOC_OC(listDetalleDocumentoView(x).IdProducto, CInt(Me.hddIdDocumento.Value)))
            End If

            listDetalleDocumentoView(x).ListaProdUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listDetalleDocumentoView(x).IdProducto)

        Next

        Me.setListaKit(listaKit)

        Return listDetalleDocumentoView

    End Function

#End Region

    Private Sub btnLimpiarDetalleDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarDetalleDocumento.Click
        Me.ListaDetalleDocumentoView = New List(Of Entidades.DetalleDocumentoView)
        setListaKit(Nothing)
        Me.gvProducto.DataBind()
    End Sub

    'Protected Sub GV_ComponenteKit_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_ComponenteKit.RowDataBound

    'End Sub


End Class