﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmCancelarTransitoOC.aspx.vb" Inherits="APPWEB.frmCancelarTransitoOC" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Cancelación De [ Stock - Transito ]
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight: bold">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" style="font-weight: bold">
                            Serie:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboSerie" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" Width="70px" ToolTip="Aceptar"
                                Style="cursor: hand;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Fecha Inicio:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                Width="100px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender0" runat="server" ClearMaskOnLostFocus="false"
                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Enabled="True"
                                Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="Texto" style="font-weight: bold">
                            Fecha Fin:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha_Blank(this) );"
                                Width="100px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="txtFechaFin_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Enabled="True"
                                Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="LabelRojo" style="font-weight: bold;">
                La cancelación de stock en tránsito, solo incluyen los documento cuya fecha de vencimiento
                es menor a la fecha actual ( considerando el rango de fechas enviado por el usuario )
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdUsuario" runat="server" Value="0" />
            </td>
        </tr>
    </table>
</asp:Content>
