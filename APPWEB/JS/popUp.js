﻿var mpeLoading;
function initializeRequest(sender, args) {
    mpeLoading = document.getElementById('capaBase3');
    mpeLoading.style.display = "block";
    mpeLoading.style.zIndex += 10;
}
function endRequest(sender, args) {
    document.getElementById('capaBase3').style.display = "none";
}
Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(initializeRequest);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();