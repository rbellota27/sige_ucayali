﻿function seleccionarRadioFiltro(objetoRadio) {
    var radio = document.getElementsByName("filtro");
    var tr_filtroDocumento = document.getElementById("trFiltroNroDocumento");
    var tr_filtroFecha = document.getElementById("trFiltroFecha");

    for (var i = 0; i < radio.length; i++) {
        if (radio[i].checked) {
            if (radio[i].value == 0) {
                tr_filtroDocumento.style.display = "block";
                tr_filtroFecha.style.display = "none";
            } else {

                tr_filtroDocumento.style.display = "none";
                tr_filtroFecha.style.display = "block";
            }
        }
    }
}

function SelectProvisiones() {
    alert(document.getElementById("gvProvisiones").id);
    var grillaOrigen = document.getElementById("gvProvisiones");
    
    var grillaDestino = document.getElementById("GV_DocumentosProvisionados");
    var totalSumaAPagar = document.getElementById("td_Total");
    var sumaTotal = 0
    var sumaParcial = 0

    var seleccion = false;
    for (var i = 1; i < grillaOrigen.rows.length; i++) {
        if (grillaOrigen.rows[i].cells[0].getElementsByTagName('INPUT')[0].checked) {
            var tbod = grillaDestino.rows[0].parentNode;
            var newRow = grillaOrigen.rows[i].cloneNode(true);
            tbod.appendChild(newRow);
            grillaOrigen.rows[i].style.display = "none";

            if (isNaN(parseFloat(totalSumaAPagar.innerHTML))) {
                sumaParcial = 0;
            } else {
                sumaParcial = parseFloat(totalSumaAPagar.innerHTML);
            }

            sumaTotal = sumaParcial + parseFloat(grillaOrigen.rows[i].cells[10].innerHTML);
            totalSumaAPagar.innerHTML = sumaTotal;
            break;
        }
    }
    return false;
}