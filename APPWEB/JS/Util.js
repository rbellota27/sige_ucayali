﻿/*
'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

*/
function setElementVisibility(elementToSet, showItSwitch, keepPlacementSwitch) {
    if (showItSwitch) {
        elementToSet.style.display = "inline";
        elementToSet.style.visibility = "visible";
    }
    else {
        if (keepPlacementSwitch) {
            elementToSet.style.display = "inline";
            elementToSet.style.visibility = "hidden";
        }
        else {
            elementToSet.style.display = "none";
        }
    }
}


function noCopyMouse(e) {
    var isRight = (e.button) ? (e.button == 2) : (e.which == 3);

    if (isRight) {
        return false;
    }
    return true;
}

function noCopyKey(e) {
    var forbiddenKeys = new Array('c', 'x', 'v');
    var keyCode = (e.keyCode) ? e.keyCode : e.which;
    var isCtrl;

    if (window.event)
        isCtrl = e.ctrlKey
    else
        isCtrl = (window.Event) ? ((e.modifiers & Event.CTRL_MASK) == Event.CTRL_MASK) : false;

    if (isCtrl) {
        for (i = 0; i < forbiddenKeys.length; i++) {
            if (forbiddenKeys[i] == String.fromCharCode(keyCode).toLowerCase()) {
                return false;
            }
        }
    }
    return true;
}

function onBlurTextTransform(obj, caso) {
    var cad = obj.value;
    switch (caso) {
        case '1':
            cad = cad.toLowerCase();
            break;
        case '2':
            cad = cad.toUpperCase();
            break;
    }
    obj.value = '';
    obj.value = cad;
}

function onFocusTextTransform(obj, caso) {
    var modo = '';
    switch (caso) {
        case '0':
            modo = "none";
            break;
        case '1':
            modo = "lowercase";
            break;
        case '2':
            modo = "uppercase";
            break;
    }
    obj.style.textTransform = modo;    
}

function limpiarCombo(combo, index) {
    //************ SI SE DESEA SOLO QUE SE CONSERVE EL PRIMER ITEM INDEX = 0  (PRIMER ITEM INDEX=0)
    for (var i = (combo.length - 1); i > index; i--) {
        combo[i] = null;
    }
}

function convertToDateString(date_JS) {  //**** Recibe como parámetro una fecha en Java Script
    var dia = date_JS.getDate();
    var mes = date_JS.getMonth() + 1;
    var anno = date_JS.getYear();
    if (dia < 10) { dia = "0" + dia; }
    if (mes < 10) { mes = "0" + mes; }
    var fechaActualText = dia + "/" + mes + "/" + anno;
    return fechaActualText;
}

function converToDate(string) { //**** Recibe como parámetro una cadena con formato inicial dd/MM/yyyy
    var date = new Date()
    var mes = parseInt(string.substring(3, 5), 10) - 1;  //en javascript los meses van de 0 a 11
    date.setMonth(mes);
    date.setDate(string.substring(0, 2));
    date.setYear(string.substring(6, 10));
    return date;
}

function LimpiarCombo(combo) {
    for (var i = (combo.length - 1); i > 0; i--) {
        combo[i] = null;
    }
    return false;
}


function LimpiarCaja(caja) {
    caja.value = '';
    return false;
}


function valBlur(e) {
    var e = e ? e : window.event;
    var event_element = e.target ? e.target : e.srcElement;
    var id = event_element.id;
    var caja = document.getElementById(id);
    if (CajaEnBlanco(caja) || isNaN(caja.value)) {
        caja.select();
        caja.focus();
        alert('Debe ingresar un valor válido.');
    }
}

function valBlurClear(texto, e) {
    var e = e ? e : window.event;
    var event_element = e.target ? e.target : e.srcElement;
    var id = event_element.id;
    var caja = document.getElementById(id);
    if (CajaEnBlanco(caja) || isNaN(caja.value)) {
        caja.value=texto;        
    }
}

function validarNumeroPunto(elEvento) {
    //obtener caracter pulsado en todos los exploradores
    var evento = elEvento || window.event;
    var caracter = evento.charCode || evento.keyCode;
    //alert("El carácter pulsado es: " + caracter);
    if (caracter == 46 || caracter == 45) {
        return true;
    } else {
        if (!onKeyPressEsNumero('event')) {
            return false;
        }
    }
}

function validarNumeroPuntoPositivo(e) {
//    var evt = e ? e : event;
//    var key = window.Event ? evt.which : evt.keyCode;
    //
    if (window.event) keyCode = window.event.keyCode;
    else if (e) keyCode = e.which;
    //alert(keyCode)
    //
    if (keyCode == 46) {
        return true;
    } else {
        if (!onKeyPressEsNumero('event')) {
            return false;
        }
    }
}

function validarNumeroPuntoSigno(e) {
    if (window.event) keyCode = window.event.keyCode;
    else if (e) keyCode = e.which;
    if (keyCode == 46 || keyCode == 45 || keyCode == 43) {
        return true;
    } else {
        if (!onKeyPressEsNumero('event')) {
            return false;
        }
    }
}


function getCampoxValorCombo(combo,value) { 
    var campo='';
    if(combo!=null){
        for (var i = 0; i < combo.length; i++) {
            if (combo[i].value == value) {
                campo = combo[i].innerHTML;
                return campo;
            }
            
        }
    }
    return campo;
}

function aceptarFoco(caja) {
    caja.select();
    caja.focus();
    return true;
}

function mostrarVentana(direccion, nombre, ancho, alto) {
    var winl = (screen.width - ancho) / 2;
    var wint = (screen.height - alto) / 2;
    window.open(direccion, nombre, 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,modal=yes,resizable=yes,copyhistory=yes,width=' + ancho + ', height=' + alto + ' ,left='+winl+',top='+wint);
    return false;
}

function CajaEnBlanco(caja) {
    if (caja.value.length == 0) {
        return true;
    } else {
        return false;
    }
}
//function CajaBusquedaEnBlanco(caja) {
//    if (caja.value.length == 0) {
//    var caja = document.getElementById('<%=txtCodigoSubLinea.ClientID%>')
//    if (CajaEnBlanco(caja)) {
//        alert('El Campo de Búsqueda esta en Blanco');
//        caja.focus();
//        return true;
//    } else {
//        return false;
//    }
//}



//////UnFormat(num,",", ""):::esta funcion quita las comas de miles//////
function UnFormat(num,stringToFind, stringToReplace) {
    var temp = num;
    var index = temp.indexOf(stringToFind);

    while (index != -1) {
        temp = temp.replace(stringToFind, stringToReplace);
        index = temp.indexOf(stringToFind);
    }
    return temp;
}
///////////////////Format :covierte la cadena de numeros en com de miles/////////////////////////////////////
function Format(num, dec) {
    var miles = true;
    var signo = 3, expr;
    var cad = "" + num;
    var ceros = "", pos, pdec, i;
    for (i = 0; i < dec; i++)
        ceros += '0';
    pos = cad.indexOf('.')
    if (pos < 0)
        cad = cad + "." + ceros;
    else {
        pdec = cad.length - pos - 1;
        if (pdec <= dec) {
            for (i = 0; i < (dec - pdec); i++)
                cad += '0';
        }
        else {
            num = num * Math.pow(10, dec);
            num = Math.round(num);
            num = num / Math.pow(10, dec);
            cad = new String(num);
        }
    }
    pos = cad.indexOf('.')
    if (pos < 0) pos = cad.lentgh
    if (cad.substr(0, 1) == '-' || cad.substr(0, 1) == '+')
        signo = 4;
    if (miles && pos > signo)
        do {
        expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
        cad.match(expr)
        cad = cad.replace(expr, RegExp.$1 + ',' + RegExp.$2)
    }
    while (cad.indexOf(',') > signo)
    if (dec < 0) cad = cad.replace(/\./, '')
    // return cad
    num = cad;
    return num;
}
///// fin Format  ////////////////////////////////////////////////////////////////





function redondear(num,nDec) {
    var result =num * Math.pow(10,nDec);
    result=Math.round(result);
    result=result/(Math.pow(10,nDec));    
	return result;    
}
function mostrarMsjAlerta(s) {
    alert(s);
}

function esDecimal(valor) {
    if (isNaN(valor)) {        
        return false;
    } else {        
        return true;
    }
}
function esEnter(e) {//funciòn utilizada para la validación del evento textchanged
    //si la tecla presionada es Enter intercepta el evento
    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;
    if (key == 13) {
        return true;
    }
    return false;
}
function onKeyPressEnter(e) {//funciòn utilizada para la validación del evento textchanged
    //No permite el la tecla enter
    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;
    if (key == 13) {
        return false;
    }
    return true;
}
function onKeyPressValidarEnter_EsNumero() {//valida el enter luego si es numero entero positivo
    if (onKeyPressEnter(event) == true) {
        //si no es enter
        return onKeyPressEsNumero('event');                
    }    
    return false;
}
function onKeyPressEsNumero(e) {
    //Valida si la tecla presionada es un número
    var evt = e ? e : event;
    var key = window.Event ? evt.which : evt.keyCode;

    if (window.event) keyCode = window.event.keyCode;
    else if (e) keyCode = e.which;
    //alert(keyCode)
    if (keyCode) // IE
    {
        key = event.keyCode;
    }    
    if (key < 48 || key > 57) {        
        return false;
    }
    return true;
}

function Readonly(caja) {
    caja.readOnly = true;
    return false;
}

//function NumeroEntero() {
//    //Valida si la tecla presionada es un número y adema acepta el enter
//    var key;
//    var flag = true;
//    
//    if (window.event) // IE
//    {
//        key = event.keyCode;
//    }
//    if (key < 48 || key > 57) {
//        flag = false;
//    }
//    if (key == 13) {
//       flag = true;
//    }
//    return flag;
//}

function offCapa(capa) {
    xcapaBase = document.getElementById('capaBase');
    xcapaBase.style.display = 'none';
    xcapa = document.getElementById(capa);
    xcapa.style.display = 'none';
    return false;
}

function onCapa(capa) {
//    $('#capaBase').css('display', 'block');
//    $('#capaBusquedaAvanzado').css('display', 'block');
    xcapaBase = document.getElementById('capaBase');
    xcapaBase.style.display = 'block';
    xcapa = document.getElementById(capa);
    xcapa.style.display = 'block';
    return false;
}

function offCapa2(capa) {
    xcapaBase = document.getElementById('capaBase2');
    xcapaBase.style.display = 'none';
    xcapa = document.getElementById(capa);
    xcapa.style.display = 'none';
    return false;
}
function onCapa2(capa) {
    xcapaBase = document.getElementById('capaBase2');
    xcapaBase.style.display = 'block';
    xcapa = document.getElementById(capa);
    xcapa.style.display = 'block';
    return false;
}

function offCapaModalMensaje(capa) {
    xcapaBaseModal = document.getElementById('capaBaseModal');
    xcapaBaseModal.style.display = 'none';
    xcapa = document.getElementById(capa);
    xcapa.style.display = 'none';
    return false;
}
function onCapaModalMensaje(capa) {
    xcapaBaseModal = document.getElementById('capaBaseModal');
    xcapaBaseModal.style.display = 'block';
    xcapa = document.getElementById(capa);
    xcapa.style.display = 'block';
    return false;
}