﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class frmConfiguracionParametros
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass 'para mostrar mensaje de error
    Private ListaArea As List(Of Entidades.ParametroGeneral) 'declato mi variable listaArea
    Private Lista_Linea_Area As List(Of Entidades.ParametroGeneral)
    Private Obj_Linea_Area As Entidades.ParametroGeneral
    Private Lista_Area As List(Of Entidades.Area)
    Private Obj_Area As Entidades.Area
    Private objCombo As New Combo
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm() 'llama al procedimiento 
            OnChange_cboArea()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try
            objCombo.LlenarCboArea_ParametroGeneral(Me.cboArea, True) 'llamo clase objCombo y Muestro el combo en blanco
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub Obtener_Datos_Grilla(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_Parametros.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then 'Recorro la grilla y obtengo sus datos

                'Dim Codigo As TextBox = e.Row.Cells(0).Text
                Dim Descripcion As TextBox = CType(e.Row.Cells(1).FindControl("dgv_txtdescripcion"), TextBox) 'para control caja texto descripcion
                Dim cboArea As DropDownList = CType(e.Row.Cells(2).FindControl("dgv_CboArea"), DropDownList) 'para el control combo area
                Dim Valor As TextBox = CType(e.Row.Cells(3).FindControl("dgv_txtvalor"), TextBox) ' para el control caja texto valor
                Dim Estado As CheckBox = CType(e.Row.Cells(4).FindControl("dgv_Chb_Estado"), CheckBox)

                '.IdParametro = CInt(e.Row.Cells(0).Text)
                Descripcion.Text = HttpUtility.HtmlDecode(Lista_Linea_Area(e.Row.RowIndex).par_Descripcion)
                If Lista_Linea_Area(e.Row.RowIndex).IdArea <> 0 Then
                    cboArea.SelectedValue = CStr(Lista_Linea_Area(e.Row.RowIndex).IdArea)
                End If
                Valor.Text = CStr(CDec(Lista_Linea_Area(e.Row.RowIndex).par_valor))
                Estado.Checked = Lista_Linea_Area(e.Row.RowIndex).par_Estado
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        registrar_Parametros(CInt(hddId.Value)) 'Llama al procedimiento  registrar_Parametros con el valor de  1 para actualizar datos

    End Sub
    Private Sub registrar_Parametros(ByVal modo As Integer) 'para insertar o actualizar datos
        Dim eobj As New Entidades.ParametroGeneral
        Dim nobj As New Negocio.ParametroGeneral
        Dim exito As Boolean = False
        Try

            Lista_Linea_Area = getLista_Linea_Area() 'Obtengo los datos de la base de datos y le asigno a la lista Lista_Linea_Area

            If modo = 1 Then 'si es 1 actualiza los datos de lista + combo area
                exito = nobj.ActualizaParametroGeneral(Lista_Linea_Area, CInt(Me.cboArea.SelectedValue))
            Else 'caso contrario inserta uno nuevo
                exito = nobj.InsertaParametroGeneral(Lista_Linea_Area, CInt(Me.cboArea.SelectedValue))
                objScript.mostrarMsjAlerta(Me, "El Parametro fue registrado con éxito")
            End If
            objCombo.LlenarCboArea_ParametroGeneral(Me.cboArea, True) 'vuelvo a llenar el comboCboArea del formulario
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Function cargarLinea_Area(ByVal IdArea As Integer) As List(Of Entidades.ParametroGeneral) 'Cargo los datos de la grilla de acuerdo al area seleccionada
        Lista_Linea_Area = (New Negocio.ParametroGeneral).SelectxIdAreaxIdParametro(IdArea)

        If Lista_Linea_Area.Count > 0 Then
            If Lista_Linea_Area(0).objArea Is Nothing Then
                Dim Lista As List(Of Entidades.Area) = (New Negocio.Area).SelectCbo
                Lista.Insert(0, New Entidades.Area(0, "------"))
                For Each obj As Entidades.Area In Lista
                    obj.Descripcion = HttpUtility.HtmlDecode(obj.Descripcion)
                Next
                Lista_Linea_Area(0).objArea = Lista
            End If
        End If

        If Lista_Linea_Area.Count > 0 Then 'Recorro el area de la lista 
            For x As Integer = 1 To Lista_Linea_Area.Count - 1
                Lista_Linea_Area(x).objArea = Lista_Linea_Area(x - 1).objArea
            Next
        End If
        Return Lista_Linea_Area
    End Function
    Private Function getLista_Linea_Area() As List(Of Entidades.ParametroGeneral) 'Obtengo los datos d la tabla y lo muestro en la grilla
        Lista_Linea_Area = New List(Of Entidades.ParametroGeneral)
        For Each fila As GridViewRow In DGV_Parametros.Rows
            Obj_Linea_Area = New Entidades.ParametroGeneral
            With Obj_Linea_Area

                Dim Descripcion As TextBox = CType(fila.Cells(1).FindControl("dgv_txtdescripcion"), TextBox) 'creo el control descripcion
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("dgv_CboArea"), DropDownList) 'creo el control cboArea
                Dim Valor As TextBox = CType(fila.Cells(3).FindControl("dgv_txtvalor"), TextBox)
                Dim Estado As CheckBox = CType(fila.Cells(4).FindControl("dgv_Chb_Estado"), CheckBox)

                .IdParametro = CInt(fila.Cells(0).Text) 'Paso el valor obtenido en el BoundFiel de la grilla a IdParametro
                .par_Descripcion = HttpUtility.HtmlDecode(Descripcion.Text) 'Paso el valor obtenido de la grilla a par_Descripcion
                .IdArea = CInt(CStr(cboArea.SelectedValue))
                .par_valor = CDec(Valor.Text)
                .par_Estado = CBool(Estado.Checked)

                If fila.RowIndex = 0 Then
                    Lista_Area = New List(Of Entidades.Area)
                    For x As Integer = 0 To cboArea.Items.Count - 1
                        Obj_Area = New Entidades.Area

                        With Obj_Area
                            .Id = CInt(cboArea.Items(x).Value)
                            .DescripcionCorta = cboArea.Items(x).Text

                        End With
                        Lista_Area.Add(Obj_Area)
                    Next
                    .objArea = Lista_Area
                End If
                If Lista_Linea_Area.Count > 0 Then .objArea = Lista_Linea_Area(0).objArea
                .par_Estado = CBool(CType(fila.Cells(4).FindControl("dgv_Chb_Estado"), CheckBox).Checked)
            End With
            Lista_Linea_Area.Add(Obj_Linea_Area) 'agregar los datos de la lista
        Next
        Return Lista_Linea_Area
    End Function
    Private Sub DGV_Parametros_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_Parametros.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then 'recorro la grilla reg x reg cuando agrego uno nuevo

                Dim Descripcion As TextBox = CType(e.Row.Cells(1).FindControl("dgv_txtdescripcion"), TextBox) 'creo el control descripcion
                Dim cboArea As DropDownList = CType(e.Row.Cells(2).FindControl("dgv_CboArea"), DropDownList) 'creo el control cboArea
                Dim Valor As TextBox = CType(e.Row.Cells(3).FindControl("dgv_txtvalor"), TextBox)
                Dim Estado As CheckBox = CType(e.Row.Cells(4).FindControl("dgv_Chb_Estado"), CheckBox)

                Descripcion.Text = HttpUtility.HtmlDecode(Lista_Linea_Area(e.Row.RowIndex).par_Descripcion)
                If Lista_Linea_Area(e.Row.RowIndex).IdArea <> 0 Then
                    cboArea.SelectedValue = CStr(Lista_Linea_Area(e.Row.RowIndex).IdArea)
                End If
                Valor.Text = CStr(CDec(Lista_Linea_Area(e.Row.RowIndex).par_valor))
                Estado.Checked = Lista_Linea_Area(e.Row.RowIndex).par_Estado
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub cboArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboArea.SelectedIndexChanged
        OnChange_cboArea()
    End Sub
    Private Sub OnChange_cboArea()
        Try
            hddId.Value = "1" 'cuando seleccione area asigno 1 para actualizar
            Lista_Linea_Area = cargarLinea_Area(CInt(Me.cboArea.SelectedValue)) 'cargo datos a la lista

            If Lista_Linea_Area.Count > 0 Then 'Si es mayor a cero habilita el boton guardar
                btnGuardar.Enabled = True
            End If

            If Lista_Linea_Area.Count = 0 Then 'Si es mayor a cero habilita el boton guardar
                btnGuardar.Enabled = False
            End If

            DGV_Parametros.DataSource = Lista_Linea_Area
            DGV_Parametros.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    '****** Este proceso no se esta usando para este caso
    Protected Sub btnAddParametro_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddParametro.Click
        Try
            hddId.Value = "0" 'Para crear un nuevo 
            'LLenarGrillaArea()'lleno la grilla
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    '****** Este proceso no se esta usando para este caso
    Private Sub LLenarGrillaArea()
        Lista_Linea_Area = getLista_Linea_Area()
        Obj_Linea_Area = New Entidades.ParametroGeneral
        With Obj_Linea_Area
            .IdArea = 0
            If Lista_Linea_Area.Count = 0 Then .objArea = (New Negocio.Area).SelectCbo() 'cuando lista no tiene nada,lleno contro base datos
            If Lista_Linea_Area.Count > 0 Then .objArea = Lista_Linea_Area(0).objArea 'coge el valor la propieda object del primer registro
            .par_Estado = True
        End With
        Lista_Linea_Area.Add(Obj_Linea_Area)
        DGV_Parametros.DataSource = Lista_Linea_Area
        DGV_Parametros.DataBind()
    End Sub
    '****** Este proceso no se esta usando para este caso
    Protected Sub DGV_Parametros_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_Parametros.SelectedIndexChanged
        Try
            Lista_Linea_Area = getLista_Linea_Area()
            Lista_Linea_Area.RemoveAt(DGV_Parametros.SelectedIndex)
            DGV_Parametros.DataSource = Lista_Linea_Area
            DGV_Parametros.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    '****** Este proceso no se esta usando para este caso
    'Private Sub cargarDGV_Parametros(ByVal IdArea As Integer)
    '    Me.DGV_Parametros.DataSource = (New Negocio.ParametroGeneral).SelectxIdAreaxIdParametro(IdArea)
    '    Me.DGV_Parametros.DataBind()
    'End Sub
End Class