﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmConfigImpresionDocs
    Inherits System.Web.UI.Page
    Dim combo As New Combo
    Dim objTimp As New Negocio.ConfigImpresion
    Dim objDocmerc As New Negocio.DocumentosMercantiles
    Private objScript As New ScriptManagerClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            inicioFrm()
        End If
    End Sub

    Protected Sub btnVistaPrevia_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            habilitaCheck(True)
            Dim lbtnMostrar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lbtnMostrar.NamingContainer, GridViewRow)
            Dim idtipodocumento As Integer = 1
            Dim idtipoImpresion As Integer = 1
            Dim iddocumento As Integer = 0
            idtipodocumento = CInt(CType(fila.Cells(3).FindControl("IdTipoDocs"), HiddenField).Value)
            idtipoImpresion = CInt(CType(fila.Cells(3).FindControl("idTipoImpresion"), HiddenField).Value)

            mostrarReporte(idtipodocumento, idtipoImpresion, objDocmerc.ImpDocsEjm(idtipodocumento))
            lbltitulo.Text = fila.Cells(0).Text + " " + fila.Cells(1).Text
            objScript.onCapa(Me, "CapaVistaPrevia")
            habilitaCheck(False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "No Tiene Detalle.")
        End Try
    End Sub
    Private Sub inicioFrm()
        combo.LlenarCboTipoImpresion(cboTImpresion, True)
        combo.LlenarCboTipoDocumentoIndep(cboDocs, True)
        LlenarGrilla(0, 0)
        habilitaCheck(False)
        habilitaSave(False)

    End Sub
    Private Sub LlenarGrilla(ByVal idtipodocs As Integer, ByVal idtipoImpresion As Integer)

        SListaDocsTipRep = objTimp.SelectAll(idtipodocs, idtipoImpresion)
        dgvDocsImpresion.DataSource = SListaDocsTipRep
        dgvDocsImpresion.DataBind()

    End Sub
#Region "Atributos"
    Public Property SListaDocsTipRep() As List(Of Entidades.DocsImpresionView)
        Get
            Return CType(Session("ListaDocsTipRep"), List(Of Entidades.DocsImpresionView))
        End Get
        Set(ByVal value As List(Of Entidades.DocsImpresionView))
            Session.Remove("ListaDocsTipRep")
            Session.Add("ListaDocsTipRep", value)
        End Set
    End Property
#End Region
#Region "Reportes"

    Private Sub mostrarReporte(ByVal idtipodocumento As Integer, ByVal idtipoimpresion As Integer, ByVal iddocumento As Integer)

        Select Case idtipodocumento
            Case 16
                Select Case idtipoimpresion
                    Case 1
                        RptOrdenCompra(iddocumento)
                    Case 2
                        RptOrdenCompra(iddocumento)
                End Select
            Case 17

                Select Case idtipoimpresion
                    Case 1
                        rptReciboI(iddocumento) ''Pesonalizado)
                    Case 2
                        rptReciboI(iddocumento) ''---Estandar)
                End Select

            Case 18
                Select Case idtipoimpresion
                    Case 1
                        rptReciboE(iddocumento) ''Pesonalizado)
                    Case 2
                        rptReciboE(iddocumento) ''---Estandar)
                End Select
            Case 4
                Select Case idtipoimpresion
                    Case 1
                        RptNotaCredito(iddocumento) ''Pesonalizado)
                    Case 2
                        RptNotaCredito(iddocumento) ''---Estandar)
                End Select

            Case 1

                Select Case idtipoimpresion
                    Case 1
                        rptDocFact(iddocumento, idtipoimpresion) ''Pesonalizado)
                    Case 2
                        rptDocFact(iddocumento, idtipoimpresion) ''---Estandar)
                End Select
            Case 3

                Select Case idtipoimpresion
                    Case 1
                        rptDocBol(iddocumento, idtipoimpresion) ''Pesonalizado)
                    Case 2
                        rptDocBol(iddocumento, idtipoimpresion) ''---Estandar)
                End Select


            Case 5
                Select Case idtipoimpresion
                    Case 1
                        rptDocNotaDebito(iddocumento) ''Pesonalizado)
                        'Case 2
                        'rptDocNotaDebito(iddocumento) ''---Estandar)
                End Select

            Case 15, 56 ''orden pedido Ventas Puntual y orden al centro de distribucion

                Select Case idtipoimpresion
                    Case 1
                        rptDocPedidoCliente(iddocumento) ''Pesonalizado)
                    Case 2
                        rptDocPedidoCliente(iddocumento) ''---Estandar)
                End Select

            Case 14

                Select Case idtipoimpresion
                    Case 1
                        RptCotizacion(iddocumento, idtipoimpresion) ''Pesonalizado)
                    Case 2
                        RptCotizacion(iddocumento, idtipoimpresion) ''---Estandar)
                End Select


            Case 6

                Select Case idtipoimpresion
                    Case 1
                        rptDocGuiasRemitente(iddocumento) ''Pesonalizado)
                        ' Case 2
                        'rptDocGuiasRemitente(iddocumento) ''---Estandar)
                End Select
            Case 25
                Select Case idtipoimpresion
                    Case 1
                        rptDocGuiasRecepcion(iddocumento) ''Pesonalizado)
                    Case 2
                        rptDocGuiasRecepcion(iddocumento) ''---Estandar)
                End Select
            Case 47
                Select Case idtipoimpresion
                    Case 1
                        rptDocControlInterno(iddocumento) ''Pesonalizado)
                    Case 2
                        rptDocControlInterno(iddocumento) ''---Estandar)
                End Select
        End Select
    End Sub

    Private Sub RptKardexValorizado(ByVal IdDocumento As Integer)
        Try

            Dim reporte As New CR_DocReciboI

            reporte.SetDataSource((New Negocio.DocumentosMercantiles).ReporteDocReciboI(IdDocumento))

            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub rptKardexFisico()

    End Sub
    Private Sub rptInventarioValorizado1()

    End Sub
    Private Sub rptReciboI(ByVal IdDocumento As Integer)
        Try
            Dim reporte As New CR_DocReciboI

            reporte.SetDataSource((New Negocio.DocumentosMercantiles).ReporteDocReciboI(IdDocumento))

            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub rptReciboE(ByVal IdDocumento As Integer)
        Try
            Dim reporte As New CR_DocReciboE

            reporte.SetDataSource((New Negocio.DocumentosMercantiles).ReporteDocReciboE(IdDocumento))

            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub RptCotizacion(ByVal IdDocumento As Integer, ByVal idtipoimpresion As Integer)
        Try
            Dim reporte As New CR_DoctCotizacion
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).getReporteDocCotizacion(IdDocumento))

            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub RptNotaCredito(ByVal IdDocumento As Integer)
        Try

            Dim reporte As New CR_DocNotaCredito
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).RptDocNotaCredito(IdDocumento))

            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub RptOrdenCompra(ByVal IdDocumento As Integer)
        Try

            Dim reporte As New CR_DocOrdenCompraM
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).getDataSetOrdenCompra(IdDocumento))

            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub rptDocFact(ByVal IdDocumento As Integer, ByVal tipoimpresion As Integer)
        Try
            Dim reporte As New CR_DocFactBol
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocFactBol(IdDocumento, tipoimpresion))

            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub rptDocBol(ByVal IdDocumento As Integer, ByVal tipoimpresion As Integer)
        Try

            Dim reporte As New CR_DocBol
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocFactBol(IdDocumento, tipoimpresion))

            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub rptDocNotaDebito(ByVal IdDocumento As Integer)
        Try

            Dim reporte As New CR_DocNotaDebito1

            reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocNotaDebito(IdDocumento))

            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub rptDocPedidoCliente(ByVal IdDocumento As Integer)
        Try
            Dim reporte As New CR_DocPedidoCliente
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocPedidoCliente(IdDocumento))
            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub
    Private Sub rptDocGuiasRemitente(ByVal IdDocumento As Integer)
        Try
            Dim reporte As New CR_DocGuiaRemisionRemitente
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocGuias(IdDocumento))
            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub
    Private Sub rptDocControlInterno(ByVal IdDocumento As Integer)
        Try
            Dim reporte As New CR_DocControlInterno
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocControlInterno(IdDocumento))
            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub

    Private Sub rptDocGuiasRecepcion(ByVal IdDocumento As Integer)
        Try
            Dim reporte As New CR_DocGuiaRecepcionn
            reporte.SetDataSource((New Negocio.DocumentosMercantiles).rptDocGuias(IdDocumento))
            crvDocsVistaPrev.ReportSource = reporte
            crvDocsVistaPrev.DataBind()
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try

    End Sub

#End Region

    Private Sub btnMostrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrar.Click
        LlenarGrilla(CInt(Me.cboDocs.SelectedValue), CInt(cboTImpresion.SelectedValue))
        If CInt(Me.cboDocs.SelectedValue) = 0 Or CInt(Me.cboTImpresion.SelectedValue) > 0 Then
            habilitaCheck(False)
            habilitaSave(False)
        Else
            habilitaSave(True)
        End If
    End Sub
    Private Sub habilitaCheck(ByVal v As Boolean)
        For i As Integer = 0 To dgvDocsImpresion.Rows.Count - 1
            CType(dgvDocsImpresion.Rows(i).Cells(2).FindControl("chbConfig"), CheckBox).Enabled = v
        Next
    End Sub

    Private Sub AgregarDocsTI()

        If CInt(cboTImpresion.SelectedValue) > 0 Then
            Dim objImpr As New Entidades.DocsImpresionView
            Dim objLista As New List(Of Entidades.DocsImpresionView)
            objLista = SListaDocsTipRep
            If objLista IsNot Nothing Then
                objImpr.IdTipoDocumento = CInt(cboDocs.SelectedValue)
                objImpr.Docs = cboDocs.SelectedItem.Text
                objImpr.IdImpresion = CInt(cboTImpresion.SelectedValue)
                objImpr.Timp = "0"
                objImpr.Tp_Nombre = cboTImpresion.SelectedItem.Text
                objLista.Add(objImpr)
                dgvDocsImpresion.DataSource = objLista
                dgvDocsImpresion.DataBind()
                habilitaSave(True)
            End If

        End If


    End Sub
    Private Sub GuardarConfigDocs()
        Dim objlista As New List(Of Entidades.DocsImpresionView)
        For i As Integer = 0 To dgvDocsImpresion.Rows.Count - 1

            Dim objIm As New Entidades.DocsImpresionView
            objIm.IdTipoDocumento = CInt(CType(dgvDocsImpresion.Rows(i).Cells(3).FindControl("IdTipoDocs"), HiddenField).Value)
            objIm.IdImpresion = CInt(CType(dgvDocsImpresion.Rows(i).Cells(3).FindControl("idTipoImpresion"), HiddenField).Value)
            objIm.Timp = CStr(IIf(CType(dgvDocsImpresion.Rows(i).Cells(2).FindControl("chbConfig"), CheckBox).Checked, "1", "0"))

            objlista.Add(objIm)
        Next
        objTimp.UpdateInsert(objlista)

    End Sub

    Private Sub btnAgregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        AgregarDocsTI()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        'LlenarGrilla(0, 0)
        'habilitaCheck(False)
        SListaDocsTipRep.Clear()
        dgvDocsImpresion.DataSource = Nothing
        Me.dgvDocsImpresion.DataBind()
        habilitaSave(False)
    End Sub
    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        GuardarConfigDocs()
        LlenarGrilla(CInt(Me.cboDocs.SelectedValue), 0)
        habilitaCheck(False)
    End Sub

    Public Sub habilitaSave(ByVal v As Boolean)
        Me.btnGuardar.Enabled = v
    End Sub

End Class