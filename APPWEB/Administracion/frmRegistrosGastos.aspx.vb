﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmRegistrosGastos
    Inherits System.Web.UI.Page

#Region "variables"
    Private objscript As New ScriptManagerClass
    Private drop As Combo
    Private fecha As Negocio.FechaActual
    Private list_RegistroGasto As List(Of Entidades.RequerimientoGasto)
    Private obj_RegistroGasto As Entidades.RequerimientoGasto
    Private list_Moneda As List(Of Entidades.Moneda)
    Private obj_Mantenimiento As Entidades.RegistroGasto.CabeceraRegistroGasto
    Private list_Centro_Costo As List(Of Entidades.Centro_costo)
    Private obj_Centro_Costo As Entidades.Centro_costo
    Private obj_observacion As Entidades.Observacion
    Private Util As Negocio.Util
    Private Enum operativo
        Ninguno = 0
        GuardarNuevo = 1
        Actualizar = 2
        Buscar = 3
    End Enum
#End Region

#Region "procedimientos y funciones"

    Private Sub BuscarRequerimientoGasto(ByVal idserie As Integer, ByVal codigo As Integer)
        LimpiarForm()
        Dim requerimiento As Entidades.RequerimientoGasto.CabeceraRequerimiento = (New Negocio.RequerimientoGasto).selectRequerimientoGasto(idserie, codigo)
        hdd_operativo.Value = CStr(operativo.GuardarNuevo)
        With requerimiento
            hdd_idrequerimientogasto.Value = CStr(.Id)
            drop = New Combo
            'Proceso de llenar la empresa
            drop.LlenarCboEmpresaxIdUsuario(dlempresa, CInt(hdd_idusuario.Value), False)
            dlempresa.SelectedValue = CStr(.IdEmpresa)
            'Proceso de llenar la tienda
            drop.LlenarCboTiendaxIdEmpresaxIdUsuario(dltienda, CInt(dlempresa.SelectedValue), CInt(hdd_idusuario.Value), False)
            dltienda.SelectedValue = CStr(.IdTienda)
            Tienda_Select()
            'Proceso de llenar el area 
            drop.LLenarCboAreaxIdtiendaxIdUsuarioxSoloCentroCosto(dlarea, CInt(dltienda.SelectedValue), CInt(hdd_idusuario.Value), True)
            'proceso de seleccionar el area
            For x As Integer = 0 To dlarea.Items.Count - 1
                Dim cad() As String = dlarea.Items(x).Value.Split(CChar(","))
                If cad(0) = CStr(.IdArea) Then
                    dlarea.SelectedIndex = x
                    Exit For
                End If
            Next
            'proceso de llenar al beneficiario
            tbcodigoref.Text = .Codigo
            tbserieref.Text = .Serie
            tbfechaaprobacion.Text = CStr(.FechaCancelacion) ''fecha aprobacion
            tbaprobadopor.Text = .NomSupervisor
            hdd_idpersona.Value = CStr(.IdPersona)
            tbbeneficiario.Text = CStr(.NombrePersona)
            tbruc.Text = CStr(.Ruc)
            tbdni.Text = CStr(.Dni)

            dloperacion.SelectedValue = CStr(.IdTipoOperacion)
            dlmediopago.SelectedValue = CStr(.IdMedioPagoCredito)
            dlmoneda.SelectedValue = CStr(.IdMoneda)
            dlmonedaregistro.SelectedValue = CStr(.IdMoneda)
            lblSimboloMoneda1.Text = dlmonedaregistro.SelectedItem.Text
            lblsimbolomoneda2.Text = dlmonedaregistro.SelectedItem.Text

            tbtotal.Text = FormatNumber(.TotalAPagar, 2)
            tbmontoSolicitado.Text = FormatNumber(.TotalAPagar, 2)

            obj_observacion = (New Negocio.Observacion).SelectxIdDocumento(CInt(hdd_idrequerimientogasto.Value))
            hdd_idObservacion.Value = CStr(obj_observacion.Id)
            tbobservaciones.Text = obj_observacion.Observacion

            SetListaRequerimiento(.Id)
        End With
        MostrarBotones("DocumentoEncontrado")
    End Sub

    Private Sub ValidarPermisos(ByVal IdUsuario As Integer)

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(IdUsuario, New Integer() {111, 112, 113, 114, 115, 116})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnuevo.Enabled = True
            Me.btguardar.Enabled = True
        Else
            Me.btnuevo.Enabled = False
            Me.btguardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.bteditar.Enabled = True
        Else
            Me.bteditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btanular.Enabled = True
        Else
            Me.btanular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* Consultar
            Me.btbuscar.Enabled = True
        Else
            Me.btbuscar.Enabled = False
        End If



    End Sub
    Private Sub Mantenimiento(ByVal tipo As Integer)
        list_RegistroGasto = getListaRegistroGasto()
        'list_RegistroGasto = GetDataSourceConcepto()
        list_Centro_Costo = getListaCentro_Costo()

        obj_Mantenimiento = New Entidades.RegistroGasto.CabeceraRegistroGasto
        With obj_Mantenimiento
            .Id = CInt(hdd_iddocumento.Value)
            .IdDocRelacionado = CInt(hdd_idrequerimientogasto.Value)
            .IdEmpresa = CInt(dlempresa.SelectedValue)
            .IdTienda = CInt(dltienda.SelectedValue)
            .IdTipoDocumento = 40 'Registro de gasto
            Dim idarea() As String = dlarea.SelectedValue.Split(CChar(","))
            .IdArea = CInt(idarea(0))
            .IdTipoOperacion = CInt(dloperacion.SelectedValue)
            .FechaEmision = CDate(tbfechaemision.Text)
            .IdMedioPagoCredito = CInt(dlmediopago.SelectedValue)
            .IdUsuario = CInt(hdd_idusuario.Value)
            .IdPersona = CInt(hdd_idpersona.Value)
            .IdMoneda = CInt(dlmonedaregistro.SelectedValue)
            .TotalAPagar = CDec(tbtotal.Text)
            .IdSerie = CInt(dlserie.SelectedValue)
        End With

        obj_observacion = New Entidades.Observacion
        obj_observacion.Id = CInt(hdd_idObservacion.Value)
        obj_observacion.Observacion = CStr(tbobservaciones.Text)

        Dim cad() As String
        Dim mensaje As String = ""
        Select Case tipo

            Case operativo.GuardarNuevo
                cad = (New Negocio.RegistroGasto).InsertRegistroGasto(obj_Mantenimiento, list_RegistroGasto, list_Centro_Costo, obj_observacion).Split(CChar(","))
                hdd_iddocumento.Value = cad(0)
                tbcodigo.Text = cad(1)
                mensaje = "El documento ha sido guardado"
            Case operativo.Actualizar
                cad = (New Negocio.RegistroGasto).UpdateRegistroGasto(obj_Mantenimiento, list_RegistroGasto, list_Centro_Costo, obj_observacion).Split(CChar(","))
                tbcodigo.Text = cad(0)
                mensaje = "El documento ha sido actualizado"
        End Select

        MostrarBotones("Load")
        btimprimir.Visible = True
        hdd_operativo.Value = CStr(operativo.Ninguno)
        objscript.mostrarMsjAlerta(Me, mensaje)
    End Sub
    Private Sub cargarSerie(ByVal cb As DropDownList)
        LimpiarCabeceraDoc()
        drop = New Combo
        drop.LLenarCboSeriexIdsEmpTienTipoDoc(cb, CInt(dlempresa.SelectedValue), CInt(dltienda.SelectedValue), 40)
    End Sub
    Private Sub generarNroDoc(ByVal tb As TextBox)
        If dlserie.Items.Count = 0 Then
            tb.Text = String.Empty
            Exit Sub
        End If
        tb.Text = (New Negocio.Serie).GenerarCodigo(CInt(dlserie.SelectedValue))
    End Sub
    Private Sub CargarAlIniciar()
        drop = New Combo
        drop.llenarCboTipoOperacionxIdTpoDocumento(dloperacion, 40, False)
        drop.LlenarCboMoneda(dlmoneda, False)
        drop.LlenarCboMoneda(dlmonedaregistro, False)
        drop.LLenarCboEstadoDocumento(dlestado)
        drop.LlenarCboCod_UniNeg(dlunidadnegocio, True)
        UnidadNegocioSelect()
        drop.LlenarCboMedioPagoxIdTipoDocumento(dlmediopago, 36, False) '36 requerimiento
        drop.LlenarCboMedioPagoxIdTipoDocumento(dlMedioPagoRegistro, 40, False) '40 registro
        fecha = New Negocio.FechaActual
        tbfechaemision.Text = FormatDateTime(fecha.SelectFechaActual, DateFormat.ShortDate)
    End Sub
    Private Sub MostrarBotones(ByVal modo As String)
        Select Case modo
            Case "Load"
                '' btnuevo.Visible = True
                btbuscar.Visible = True
                bteditar.Visible = False
                btcancelar.Visible = False
                btguardar.Visible = False
                btanular.Visible = False
                btimprimir.Visible = False
                imgBuscarDocumento.Visible = False
                pnlPrincipal.Enabled = False

            Case "Nuevo"
                '' btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = False
                btcancelar.Visible = True
                btguardar.Visible = True
                btanular.Visible = False
                btimprimir.Visible = False
                imgBuscarDocumento.Visible = False
                pnlPrincipal.Enabled = True
                HabilitarControles(True)

            Case "Buscar"
                '' btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = False
                btcancelar.Visible = True
                btguardar.Visible = False
                btanular.Visible = False
                btimprimir.Visible = False
                imgBuscarDocumento.Visible = True
                pnlPrincipal.Enabled = True

            Case "DocumentoEncontrado"
                '' btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = True
                btcancelar.Visible = True
                btguardar.Visible = False
                btanular.Visible = False
                btimprimir.Visible = True
                imgBuscarDocumento.Visible = False
                pnlPrincipal.Enabled = False
                HabilitarControles(False)

            Case "Editar"
                '' btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = False
                btcancelar.Visible = True
                btguardar.Visible = True
                btanular.Visible = True
                btimprimir.Visible = False
                imgBuscarDocumento.Visible = False
                pnlPrincipal.Enabled = True

        End Select
    End Sub
    Private Sub LimpiarForm()
        hdd_idrequerimientogasto.Value = "0"
        hdd_iddocumento.Value = "0"
        hdd_idpersona.Value = "0"
        hdd_idObservacion.Value = "0"
        hdd_operativo.Value = CStr(operativo.Ninguno)

        tbobservaciones.Text = String.Empty
        tbtotal.Text = "0"
        tbfechaaprobacion.Text = String.Empty
        tbbeneficiario.Text = String.Empty
        tbdni.Text = String.Empty
        tbruc.Text = String.Empty

        dlestado.SelectedValue = "1"

        gvdetalle.DataBind()
        gvcentrocosto.DataBind()

        If dlarea.Items.Count > 0 Then dlarea.SelectedIndex = 0

        'list_RegistroGasto = New List(Of Entidades.RequerimientoGasto)
        'SetDataSourceConcepto(list_RegistroGasto)
    End Sub
    Private Sub LimpiarCabeceraDoc()
        dlserie.Items.Clear()
        tbcodigo.Text = String.Empty
    End Sub
    Private Sub HabilitarControles(ByVal valor As Boolean)
        dlempresa.Enabled = valor
        dltienda.Enabled = valor
    End Sub

#End Region

#Region "Cabecera del documento"

    Private Sub dlempresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlempresa.SelectedIndexChanged
        Empresa_Select()
    End Sub
    Private Sub Empresa_Select()
        drop = New Combo
        drop.LlenarCboTiendaxIdEmpresaxIdUsuario(dltienda, CInt(dlempresa.SelectedValue), CInt(hdd_idusuario.Value), False)
        cargarSerie(dlserie)
        generarNroDoc(tbcodigo)
    End Sub

    Private Sub dltienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dltienda.SelectedIndexChanged
        Tienda_Select()
    End Sub
    Private Sub Tienda_Select()
        cargarSerie(dlserie)
        generarNroDoc(tbcodigo)
    End Sub

    Private Sub dlserie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlserie.SelectedIndexChanged
        generarNroDoc(tbcodigo)
    End Sub
#End Region

#Region "Eventos Principales"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            hdd_idusuario.Value = CStr(Session("IdUsuario"))
            CargarAlIniciar()
            hdd_operativo.Value = CStr(operativo.Ninguno)
            MostrarBotones("Load")
            ValidarPermisos(CInt(hdd_idusuario.Value))
            If Request.QueryString("serie") IsNot Nothing And Request.QueryString("codigo") IsNot Nothing Then
                BuscarRequerimientoGasto(CInt(Request.QueryString("serie")), CInt(Request.QueryString("codigo")))
            Else
                CargarAlIniciarEmpresaTienda()
            End If
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "CalcularMonto(); CalcularMontoCentroCosto(); javascript:setText(); ", True)
    End Sub

    Private Sub CargarAlIniciarEmpresaTienda()
        drop = New Combo
        drop.LlenarCboEmpresaxIdUsuario(dlempresa, CInt(hdd_idusuario.Value), False)
        Empresa_Select()
    End Sub

    Private Sub btnuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnuevo.Click
        LimpiarForm()
        hdd_operativo.Value = CStr(operativo.GuardarNuevo)
        MostrarBotones("Nuevo")
    End Sub
    Private Sub btguardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btguardar.Click
        Try
            Me.Mantenimiento(CInt(hdd_operativo.Value))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btcancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btcancelar.Click
        LimpiarForm()
        MostrarBotones("Load")
        fecha = New Negocio.FechaActual
        Me.tbfechaemision.Text = Format(fecha.SelectFechaActual, "dd/MM/yyyy")
        cargarSerie(dlserie)
        generarNroDoc(tbcodigo)

    End Sub
    Private Sub btanular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btanular.Click
        Try
            If hdd_iddocumento.Value <> "0" Then
                Dim objOrdenCompra As New Negocio.OrdenCompra
                objOrdenCompra.AnularOrdenCompra(CInt(hdd_iddocumento.Value))
                dlestado.SelectedValue = "2"
                MostrarBotones("Load")

            End If
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub bteditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bteditar.Click
        list_RegistroGasto = GetDataSourceConcepto()
        gvdetalle.DataSource = list_RegistroGasto
        gvdetalle.DataBind()
        Session.Remove("Concepto")
        MostrarBotones("Editar")
    End Sub
    Private Sub btbuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btbuscar.Click
        LimpiarForm()
        cargarSerie(dlserie)
        hdd_operativo.Value = CStr(operativo.Buscar)
        MostrarBotones("Buscar")
        tbcodigo.Focus()

    End Sub

#End Region

#Region "Busqueda de Personas"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                      ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                      ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objscript.onCapa(Me, "capabeneficiario")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capabeneficiario');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("rol", 0)  '******************** TODOS
            ViewState.Add("estado", 1) '******************* ACTIVO

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "Detalle de Requerimiento"

    Private Sub SetDataSourceConcepto(ByVal Lista As List(Of Entidades.RequerimientoGasto))
        Session.Remove("Concepto")
        Session.Add("Concepto", Lista)
    End Sub

    Private Function GetDataSourceConcepto() As List(Of Entidades.RequerimientoGasto)
        Return CType(Session.Item("Concepto"), List(Of Entidades.RequerimientoGasto))
    End Function

    Private Sub SetListaRequerimiento(ByVal idDocumento As Integer)
        list_RegistroGasto = (New Negocio.RequerimientoGasto).selectListaRequerimientoGasto(idDocumento)

        For x As Integer = 0 To list_RegistroGasto.Count - 1

            If x = 0 Then
                Dim Lista_Concepto As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(36)
                Lista_Concepto.Insert(0, New Entidades.Concepto(0, "------"))
                list_RegistroGasto(x).objConcepto = Lista_Concepto

                Dim Lista_Tipodocumento As List(Of Entidades.TipoDocumento) = (New Negocio.TipoDocumentoRef).SelectxIdtipoDocRef(36)
                Lista_Tipodocumento.Insert(0, New Entidades.TipoDocumento(0, "------"))
                list_RegistroGasto(x).objTipoDocumento = Lista_Tipodocumento

                list_Moneda = getListaMoneda()
                list_RegistroGasto(x).objMoneda = list_Moneda
            Else
                list_RegistroGasto(x).objConcepto = list_RegistroGasto(0).objConcepto
                list_RegistroGasto(x).objTipoDocumento = list_RegistroGasto(0).objTipoDocumento
                list_RegistroGasto(x).objMoneda = list_RegistroGasto(0).objMoneda
            End If

            If list_RegistroGasto(x).IdConcepto <> 0 Then
                Dim Lista_motivoGasto As List(Of Entidades.MotivoGasto) = (New Negocio.MotivoGasto).SelectxConceptoxMotivoGasto(list_RegistroGasto(x).IdConcepto)
                list_RegistroGasto(x).objMotivo = Lista_motivoGasto
            End If

        Next

        gvdetalle.DataSource = list_RegistroGasto
        SetDataSourceConcepto(list_RegistroGasto)
        gvdetalle.DataBind()
    End Sub
    Private Sub btagregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btagregar.Click
        Try
            llenarGrillaConcepto()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub llenarGrillaConcepto()
        list_RegistroGasto = getListaRegistroGasto()
        'list_RegistroGasto = GetDataSourceConcepto()

        For Each row As GridViewRow In Me.gvdetalle.Rows
            list_RegistroGasto(row.RowIndex).IdProveedor = CInt(CType(row.Cells(6).FindControl("ghdd_idpersona"), HiddenField).Value)
            list_RegistroGasto(row.RowIndex).NomPersona = HttpUtility.HtmlDecode(CType(row.Cells(6).FindControl("ghdd_nompersona"), HiddenField).Value)
        Next

        obj_RegistroGasto = New Entidades.RequerimientoGasto

        With obj_RegistroGasto
            .IdConcepto = 0
            If list_RegistroGasto.Count = 0 Then
                Dim Lista_Concepto As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(40)
                Lista_Concepto.Insert(0, New Entidades.Concepto(0, "------"))
                .objConcepto = Lista_Concepto
                Dim Lista_Tipodocumento As List(Of Entidades.TipoDocumento) = (New Negocio.TipoDocumentoRef).SelectxIdtipoDocRef(40)
                Lista_Tipodocumento.Insert(0, New Entidades.TipoDocumento(0, "------"))
                .objTipoDocumento = Lista_Tipodocumento
                list_Moneda = getListaMoneda()
                'list_Moneda.Insert(0, New Entidades.Moneda(0, "-------", "------"))
                .objMoneda = list_Moneda
                .IdMoneda = CInt(dlmonedaregistro.SelectedValue)
            Else
                .objConcepto = list_RegistroGasto(0).objConcepto
                .objTipoDocumento = list_RegistroGasto(0).objTipoDocumento
                .objMoneda = list_RegistroGasto(0).objMoneda
            End If
            .Descripcion = String.Empty
            .NroDocumento = String.Empty
            .Monto = Decimal.Zero
        End With

        list_RegistroGasto.Add(obj_RegistroGasto)
        gvdetalle.DataSource = list_RegistroGasto
        'SetDataSourceConcepto(list_RegistroGasto)
        gvdetalle.DataBind()
    End Sub
    Private Function getListaRegistroGasto() As List(Of Entidades.RequerimientoGasto)
        list_RegistroGasto = New List(Of Entidades.RequerimientoGasto)


        For Each row As GridViewRow In Me.gvdetalle.Rows
            obj_RegistroGasto = New Entidades.RequerimientoGasto
            With obj_RegistroGasto
                Dim gdlconcepto As DropDownList = CType(row.Cells(1).FindControl("gdlconcepto"), DropDownList)
                Dim gdlmotivo As DropDownList = CType(row.Cells(2).FindControl("gdlmotivo"), DropDownList)
                .Descripcion = CType(row.Cells(3).FindControl("gtbdescripcion"), TextBox).Text
                Dim gdltipodocumento As DropDownList = CType(row.Cells(4).FindControl("gdltipodocumento"), DropDownList)
                .NroDocumento = CType(row.Cells(5).FindControl("gtbnrodocumento"), TextBox).Text
                Dim gdlmoneda As DropDownList = CType(row.Cells(6).FindControl("gdlmoneda"), DropDownList)
                .Monto = CDec(CType(row.Cells(7).FindControl("gtbmonto"), TextBox).Text)

                .IdConcepto = CInt(gdlconcepto.SelectedValue)
                .IdMoneda = CInt(gdlmoneda.SelectedValue)
                .IdMotivo = CInt(IIf(gdlmotivo.Items.Count = 0, 0, gdlmotivo.SelectedValue))
                .IdTipoDocumento = CInt(gdltipodocumento.SelectedValue)

                If row.RowIndex = 0 Then
                    Dim Lista_Concepto As New List(Of Entidades.Concepto)
                    For x As Integer = 0 To gdlconcepto.Items.Count - 1
                        Lista_Concepto.Insert(x, New Entidades.Concepto(CInt(gdlconcepto.Items(x).Value), gdlconcepto.Items(x).Text))
                    Next
                    .objConcepto = Lista_Concepto

                    Dim Lista_Tipodocumento As New List(Of Entidades.TipoDocumento)
                    For x As Integer = 0 To gdltipodocumento.Items.Count - 1
                        Lista_Tipodocumento.Insert(x, New Entidades.TipoDocumento(CInt(gdltipodocumento.Items(x).Value), gdltipodocumento.Items(x).Text))
                    Next
                    .objTipoDocumento = Lista_Tipodocumento
                    .objMoneda = getListaMoneda()
                Else
                    .objConcepto = list_RegistroGasto(0).objConcepto
                    .objTipoDocumento = list_RegistroGasto(0).objTipoDocumento
                    .objMoneda = list_RegistroGasto(0).objMoneda
                End If

                Dim Lista_motivoGasto As New List(Of Entidades.MotivoGasto)
                For x As Integer = 0 To gdlmotivo.Items.Count - 1
                    Lista_motivoGasto.Insert(x, New Entidades.MotivoGasto(CInt(gdlmotivo.Items(x).Value), gdlmotivo.Items(x).Text))
                Next
                .objMotivo = Lista_motivoGasto

                .IdProveedor = CInt(CType(row.Cells(6).FindControl("ghdd_idpersona"), HiddenField).Value)
                .NomPersona = HttpUtility.HtmlDecode(CType(row.Cells(6).FindControl("ghdd_nompersona"), HiddenField).Value)
            End With
            list_RegistroGasto.Add(obj_RegistroGasto)
        Next

        Return list_RegistroGasto
    End Function

    Private Sub gvdetalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvdetalle.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gdlconcepto As DropDownList = CType(e.Row.Cells(1).FindControl("gdlconcepto"), DropDownList)
            Dim gdlmotivo As DropDownList = CType(e.Row.Cells(2).FindControl("gdlmotivo"), DropDownList)
            Dim gdltipodocumento As DropDownList = CType(e.Row.Cells(4).FindControl("gdltipodocumento"), DropDownList)
            Dim gdlmoneda As DropDownList = CType(e.Row.Cells(6).FindControl("gdlmoneda"), DropDownList)

            If list_RegistroGasto(e.Row.RowIndex).IdConcepto <> 0 Then
                gdlconcepto.SelectedValue = CStr(list_RegistroGasto(e.Row.RowIndex).IdConcepto)
            End If

            If list_RegistroGasto(e.Row.RowIndex).IdMotivo <> 0 And gdlmotivo.Items.Count > 0 Then
                gdlmotivo.SelectedValue = CStr(list_RegistroGasto(e.Row.RowIndex).IdMotivo)
            End If

            If list_RegistroGasto(e.Row.RowIndex).IdMoneda <> 0 Then
                gdlmoneda.SelectedValue = CStr(list_RegistroGasto(e.Row.RowIndex).IdMoneda)
            End If

            If list_RegistroGasto(e.Row.RowIndex).IdTipoDocumento <> 0 Then
                gdltipodocumento.SelectedValue = CStr(list_RegistroGasto(e.Row.RowIndex).IdTipoDocumento)
            End If

        End If
    End Sub
    Private Sub gvdetalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvdetalle.SelectedIndexChanged
        list_RegistroGasto = getListaRegistroGasto()
        'list_RegistroGasto = GetDataSourceConcepto()
        list_RegistroGasto.RemoveAt(gvdetalle.SelectedIndex)
        gvdetalle.DataSource = list_RegistroGasto
        'SetDataSourceConcepto(list_RegistroGasto)
        gvdetalle.DataBind()
    End Sub
    Public Function getListaMoneda() As List(Of Entidades.Moneda)
        list_Moneda = New List(Of Entidades.Moneda)
        With dlmoneda
            For x As Integer = 0 To Items.Count
                list_Moneda.Insert(x, New Entidades.Moneda(CInt(.Items(x).Value), "", .Items(x).Text))
            Next
        End With
        Return list_Moneda
    End Function

    Protected Sub gdlconcepto_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim gdlconcepto As DropDownList = CType(sender, DropDownList)
        Dim fila As GridViewRow = CType(gdlconcepto.NamingContainer, GridViewRow)
        Dim gdlmotivo As DropDownList = CType(fila.Cells(2).FindControl("gdlmotivo"), DropDownList)
        gdlmotivo.Items.Clear()
        Dim Lista_motivoGasto As List(Of Entidades.MotivoGasto) = (New Negocio.MotivoGasto).SelectxConceptoxMotivoGasto(CInt(gdlconcepto.SelectedValue))
        Lista_motivoGasto.Insert(0, New Entidades.MotivoGasto(0, "------"))
        For i As Integer = 0 To Lista_motivoGasto.Count - 1
            With gdlmotivo.Items
                .Insert(i, New ListItem(Lista_motivoGasto.Item(i).Nombre_MG, CStr(Lista_motivoGasto.Item(i).Id)))
            End With
        Next

    End Sub

#End Region


#Region "Busqueda de personas grilla"
    Protected Sub imgBuscarPersona_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim row As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
        hdd_index.Value = CStr(row.RowIndex)
        gvBuscar.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " mostrarCapaPersona('0'); ", True)
    End Sub
#End Region

#Region "centro de costo"

    Protected Sub dlunidadnegocio_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlunidadnegocio.SelectedIndexChanged
        Try
            UnidadNegocioSelect()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub UnidadNegocioSelect()
        If dlunidadnegocio.SelectedValue <> "00" Then
            drop = New Combo
            drop.LlenarCboCod_DepFunc(dldptofuncional, dlunidadnegocio.SelectedValue, True)
        Else
            dldptofuncional.Items.Clear() : dldptofuncional.Items.Insert(0, New ListItem("------", "00"))
        End If
        dlsubarea1.Items.Clear() : dlsubarea1.Items.Insert(0, New ListItem("------", "00"))
        dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
        dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
    End Sub

    Protected Sub dldptofuncional_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dldptofuncional.SelectedIndexChanged
        Try
            If dldptofuncional.SelectedValue <> "00" Then
                drop = New Combo
                drop.LlenarCboCod_SubCodigo2(dlsubarea1, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, True)
            Else
                dlsubarea1.Items.Clear() : dlsubarea1.Items.Insert(0, New ListItem("------", "00"))
            End If
            dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
            dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub dlsubarea1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlsubarea1.SelectedIndexChanged
        Try
            If dlsubarea1.SelectedValue <> "00" Then
                drop = New Combo
                drop.LlenarCboCod_SubCodigo3(dlsubarea2, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, dlsubarea1.SelectedValue, True)
            Else
                dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
            End If
            dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub dlsubarea2_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlsubarea2.SelectedIndexChanged
        Try
            If dlsubarea2.SelectedValue <> "00" Then
                drop = New Combo
                drop.LlenarCboCod_SubCodigo4(dlsubarea3, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, dlsubarea1.SelectedValue, dlsubarea2.SelectedValue, True)
            Else
                dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
            End If
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function getCentroCosto() As String
        Dim ID As String = String.Empty
        ID = dlunidadnegocio.SelectedValue + dldptofuncional.SelectedValue
        ID &= dlsubarea1.SelectedValue + dlsubarea2.SelectedValue + dlsubarea3.SelectedValue
        Return ID
    End Function

#End Region


    Private Sub dlmonedaregistro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlmonedaregistro.SelectedIndexChanged
        lblSimboloMoneda1.Text = dlmonedaregistro.SelectedItem.Text
        lblsimbolomoneda2.Text = dlmonedaregistro.SelectedItem.Text

        Util = New Negocio.Util
        list_RegistroGasto = getListaRegistroGasto()
        'list_RegistroGasto = GetDataSourceConcepto()
        Dim idMoneda As Integer = 0

        For Each row As GridViewRow In Me.gvdetalle.Rows
            Dim caja As TextBox = CType(row.Cells(7).FindControl("gtbmonto"), TextBox)
            Dim valor As Decimal = CDec(IIf(caja.Text = "", Decimal.Zero, caja.Text))
            list_RegistroGasto(row.RowIndex).Monto = valor
        Next
        For x As Integer = 0 To list_RegistroGasto.Count - 1
            idMoneda = list_RegistroGasto(x).IdMoneda
            Dim monto As Decimal = Util.SelectValorxIdMonedaOxIdMonedaD(list_RegistroGasto(x).IdMoneda, CInt(dlmonedaregistro.SelectedValue), list_RegistroGasto(x).Monto, "E")
            list_RegistroGasto(x).IdMoneda = CInt(dlmonedaregistro.SelectedValue)
            list_RegistroGasto(x).Monto = CDec(monto)
        Next

        For Each row As GridViewRow In Me.gvcentrocosto.Rows
            Dim caja As TextBox = CType(row.Cells(7).FindControl("tbmontocentrocosto"), TextBox)
            Dim valor As Decimal = CDec(IIf(caja.Text = "", Decimal.Zero, caja.Text))
            Dim monto As Decimal = Util.SelectValorxIdMonedaOxIdMonedaD(idMoneda, CInt(dlmonedaregistro.SelectedValue), valor, "E")
            caja.Text = FormatNumber(monto, 2)
        Next

        gvdetalle.DataSource = list_RegistroGasto
        'SetDataSourceConcepto(list_RegistroGasto)
        gvdetalle.DataBind()
    End Sub

    Private Sub imgBuscarDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBuscarDocumento.Click
        Try
            Dim obj As Entidades.RegistroGasto.CabeceraRegistroGasto = (New Negocio.RegistroGasto).selectRegistroGasto(CInt(dlserie.SelectedValue), CInt(tbcodigo.Text))
            hdd_operativo.Value = CStr(operativo.Actualizar)
            With obj
                hdd_iddocumento.Value = CStr(.Id)
                hdd_idrequerimientogasto.Value = CStr(.IdDocRelacionado)
                drop = New Combo
                'Proceso de llenar la empresa
                dlempresa.SelectedValue = CStr(.IdEmpresa)
                Empresa_Select()
                'Proceso de llenar la tienda
                dltienda.SelectedValue = CStr(.IdTienda)
                Tienda_Select()
                dlserie.SelectedValue = CStr(.IdSerie)
                tbcodigo.Text = CStr(.Codigo)
                'Proceso de llenar el area 
                drop.LLenarCboAreaxIdtiendaxIdUsuarioxSoloCentroCosto(dlarea, CInt(dltienda.SelectedValue), CInt(hdd_idusuario.Value), True)
                'proceso de seleccionar el area
                For x As Integer = 0 To dlarea.Items.Count - 1
                    Dim cad() As String = dlarea.Items(x).Value.Split(CChar(","))
                    If cad(0) = CStr(.IdArea) Then
                        dlarea.SelectedValue = dlarea.Items(x).Value
                        Exit For
                    End If
                Next
                'proceso de llenar al beneficiario
                tbcodigoref.Text = .codigoRef
                tbserieref.Text = .serieRef
                tbfechaaprobacion.Text = CStr(.FechaCancelacion) ''fecha aprobacion
                tbaprobadopor.Text = .NomSupervisor
                hdd_idpersona.Value = CStr(.IdPersona)
                tbbeneficiario.Text = CStr(.NombrePersona)
                tbruc.Text = CStr(.Ruc)
                tbdni.Text = CStr(.Dni)

                dloperacion.SelectedValue = CStr(.IdTipoOperacion)
                dlmediopago.SelectedValue = CStr(.IdMedioPagoCredito)
                dlmoneda.SelectedValue = CStr(.Idmoneda2)

                dlmonedaregistro.SelectedValue = CStr(.IdMoneda)
                lblSimboloMoneda1.Text = dlmonedaregistro.SelectedItem.Text
                lblsimbolomoneda2.Text = dlmonedaregistro.SelectedItem.Text

                tbmontoSolicitado.Text = FormatNumber(.doc_Total2, 2)

                tbtotal.Text = FormatNumber(.TotalAPagar, 2)
                tbtotalcentrocosto.Text = FormatNumber(.TotalAPagar, 2)


                obj_observacion = (New Negocio.Observacion).SelectxIdDocumento(CInt(hdd_iddocumento.Value))
                hdd_idObservacion.Value = CStr(obj_observacion.Id)
                tbobservaciones.Text = obj_observacion.Observacion

                SetListaRequerimiento(CInt(hdd_iddocumento.Value))

                list_Centro_Costo = (New Negocio.Centro_Costo).GastoPorCentroCosto_Select_IdDocumento(CInt(hdd_iddocumento.Value))
                gvcentrocosto.DataSource = list_Centro_Costo
                gvcentrocosto.DataBind()

            End With
            MostrarBotones("DocumentoEncontrado")
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btCentroCosto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btCentroCosto.Click
        Try
            LLenarGrillaCentroCosto()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub LLenarGrillaCentroCosto()
        list_Centro_Costo = getListaCentro_Costo()
        obj_Centro_Costo = New Entidades.Centro_costo
        With obj_Centro_Costo
            .codigo = getCentroCosto()
            .strCod_UniNeg = dlunidadnegocio.SelectedItem.Text
            .strCod_DepFunc = dldptofuncional.SelectedItem.Text
            .strCod_SubCodigo2 = dlsubarea1.SelectedItem.Text
            .strCod_SubCodigo3 = dlsubarea2.SelectedItem.Text
            .strCod_SubCodigo4 = dlsubarea3.SelectedItem.Text
            .monto = Decimal.Zero
        End With
        list_Centro_Costo.Add(obj_Centro_Costo)
        gvcentrocosto.DataSource = list_Centro_Costo
        gvcentrocosto.DataBind()
    End Sub

    Private Function getListaCentro_Costo() As List(Of Entidades.Centro_costo)
        list_Centro_Costo = New List(Of Entidades.Centro_costo)

        For Each row As GridViewRow In Me.gvcentrocosto.Rows
            obj_Centro_Costo = New Entidades.Centro_costo
            With obj_Centro_Costo
                .codigo = row.Cells(1).Text
                .strCod_UniNeg = HttpUtility.HtmlDecode(row.Cells(2).Text)
                .strCod_DepFunc = HttpUtility.HtmlDecode(row.Cells(3).Text)
                .strCod_SubCodigo2 = HttpUtility.HtmlDecode(row.Cells(4).Text)
                .strCod_SubCodigo3 = HttpUtility.HtmlDecode(row.Cells(5).Text)
                .strCod_SubCodigo4 = HttpUtility.HtmlDecode(row.Cells(6).Text)
                Dim tb As TextBox = CType(row.Cells(7).FindControl("tbmontocentrocosto"), TextBox)
                .monto = CDec(IIf(tb.Text = "", 0, tb.Text))
            End With
            list_Centro_Costo.Add(obj_Centro_Costo)
        Next

        Return list_Centro_Costo
    End Function


    Private Sub gvcentrocosto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvcentrocosto.SelectedIndexChanged
        Try
            list_Centro_Costo = getListaCentro_Costo()
            list_Centro_Costo.RemoveAt(gvcentrocosto.SelectedIndex)
            gvcentrocosto.DataSource = list_Centro_Costo
            gvcentrocosto.DataBind()

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class