<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmMantCtaProveedor.aspx.vb" Inherits="APPWEB.FrmMantCtaProveedor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='/Imagenes/Nuevo_A.JPG';"
                                onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" />
                        </td>
                        <td>
                            <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                onmouseover="this.src='/Imagenes/Guardar_A.JPG';" onmouseout="this.src='/Imagenes/Guardar_B.JPG';"
                                CausesValidation="true" OnClientClick="return(confirm('Desea continuar con la operaci�n?'));" />
                        </td>
                        <td>
                            <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG"
                                onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" onmouseout="this.src='/Imagenes/Cancelar_B.JPG';" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Asignar Cuentas a la Empresa por Proveedor
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Label">
                            Proveedor:
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtNombreProveedor" runat="server" CssClass="TextBox_ReadOnlyLeft"
                                onKeyDown="return (false);" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnBuscarPersona" runat="server" ImageUrl="~/Imagenes/Buscar_B.JPG"
                                onmouseout="this.src='/Imagenes/Buscar_B.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';"
                                Visible="True" OnClientClick="return (mostrarCapaPersona());" />
                        </td>
                    </tr>
                    <tr>
                        <td class="Label">
                            Ruc:
                        </td>
                        <td>
                            <asp:TextBox ID="txtRucProveedor" runat="server" CssClass="TextBox_ReadOnlyLeft"
                                onKeyDown="return (false);"></asp:TextBox>
                        </td>
                        <td class="Label">
                            Dni:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDniProveedor" runat="server" CssClass="TextBox_ReadOnlyLeft"
                                onKeyDown="return (false);"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Label">
                            Empresa:
                        </td>
                        <td colspan="5">
                            <asp:DropDownList ID="cboEmpresa" runat="server" Width="180">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label">
                            Cargo Maximo:
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="txtCargoMaximo" runat="server" Width="180px" onKeypress="return(validarNumeroPunto(event));"
                                onblur="return(valBlur(event));"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label">
                            Moneda:
                        </td>
                        <td colspan="5">
                            <asp:DropDownList ID="cboMoneda" runat="server" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" valign="top">
                            Observacion:
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="txtObs" runat="server" Width="100%" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" Height="50px"
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:ImageButton ID="btnAgregarCuentaPersona" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                    onmouseout="this.src='/Imagenes/Agregar_B.JPG';" onmouseover="this.src='/Imagenes/Agregar_A.JPG';"
                    OnClientClick="return(validaMonedaPersonaGrilla());" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel ID="pnGrillaCtaProveedor" runat="server">
                    <asp:GridView ID="dgvCuentaxPersona" runat="server" AutoGenerateColumns="False" Width="100%"
                        HeaderStyle-Height="25px">
                        <HeaderStyle CssClass="GrillaHeader" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtVerDetalle" runat="server" OnClick="lbtnMostrar_Click">Detalle</asp:LinkButton>
                                    <asp:HiddenField ID="hdfidMoneda" Value='<%# DataBinder.Eval(Container.DataItem,"IdMoneda")  %>'
                                        runat="server" />
                                    <asp:HiddenField ID="hdfidCtaProveedor" Value='<%# DataBinder.Eval(Container.DataItem,"IdCuentaProv")%>'
                                        runat="server" />
                                    <asp:HiddenField ID="hdfidPropietario" Value='<%# DataBinder.Eval(Container.DataItem,"IdPropietario")%>'
                                        runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Moneda" DataField="MonedaSimbolo" NullDisplayText="" />
                            <asp:TemplateField HeaderText="Cargo Maximo">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCargoMaxGvw" Width="100px" runat="server" onKeypress="return(validarNumeroPunto(event));"
                                        onblur="return(valBlur(event));" Text='<%# Bind("cprov_CargoMax","{0:F3}") %>' NullDisplayText="0"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Disponible" DataField="cprov_Saldo" DataFormatString="{0:F3}"
                                NullDisplayText="" />
                            <asp:TemplateField HeaderText="Observacion">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtObserGvw" Width="350px" runat="server" Text='<%# Bind("ObservProv") %>'
                                        onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                        NullDisplayText=""></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chbEstado" runat="server" onClick="return(valCheckPAT());" Checked='<%#DataBinder.Eval(Container.DataItem,"cpro_Estado")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Fecha Alta" DataField="cprov_FechaAlta" NullDisplayText="" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtVerHistCredito" runat="server" OnClick="btnVerHistCredito_Click">Historial</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtQuitar" runat="server" OnClick="btnEliminar_Click">Quitar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Detalle Cuentas por Pagar por Proveedor
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvwCxCxDocumento" HeaderStyle-Height="25px" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" SelectText="Ver Mov" />
                        <asp:BoundField DataField="idDocumento" HeaderText="Id" />
                        <asp:BoundField DataField="Numero" HeaderText="N�mero" />
                        <asp:BoundField DataField="tdoc_NombreCorto" HeaderText="Documento" />
                        <asp:BoundField DataField="doc_FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n" />
                        <asp:BoundField DataField="doc_FechaVencimiento" DataFormatString="{0:dd/MM/yyyy}"
                            HeaderText="FechaVencimiento" />
                        <asp:BoundField DataField="nroDias" HeaderText="D�as Vencidos" />
                        <asp:BoundField DataField="mon_simbolo" HeaderText="Moneda" />
                        <asp:BoundField DataField="doc_TotalPagar" DataFormatString="{0:F2}" HeaderText="Deuda" />
                        <asp:BoundField DataField="abono" DataFormatString="{0:F2}" HeaderText="Total Abonos" />
                        <asp:BoundField DataField="saldo" DataFormatString="{0:F2}" HeaderText="Saldo" />
                    </Columns>
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <HeaderStyle CssClass="GrillaHeader" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMensajeDocumento" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hdfidproveedor" runat="server" />
                            <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 3; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Value="J">Jur�dica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Raz�n Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this,event));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        R.U.C.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                    </td>
                                                                    <td>
<%--                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />--%>

                                                                         <asp:Button ID="btnBuscarPersonaGrilla" runat="server" Text="Buscar" CssClass="btnBuscar"
                                                                        OnClientClick="this.disabled;this.value='Procesando...'" UseSubmitBehavior="false"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="C�d." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Raz�n Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDetalleAbonos" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="upAbonos" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImageButton3" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('capaDetalleAbonos'));" />
                        </td>
                    </tr>
                    <tr>
                        <td class="TituloCeldaLeft">
                            Detalle De Abonos Por Documento
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:GridView ID="gvwDetalleAbonos" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                PageSize="20" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="Numero" HeaderText="N�mero" />
                                    <asp:BoundField DataField="tdoc_NombreCorto" HeaderText="Documento" />
                                    <asp:BoundField DataField="doc_FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Movimiento" />
                                    <asp:BoundField DataField="mon_simbolo" HeaderText="Moneda" />
                                    <asp:BoundField DataField="doc_TotalPagar" DataFormatString="{0:F2}" HeaderText="Importe" />
                                    <asp:BoundField DataField="abono" DataFormatString="{0:F2}" HeaderText="Total Abonos" />
                                    <asp:BoundField DataField="Saldo" DataFormatString="{0:F2}" HeaderText="Saldo" />
                                    <asp:BoundField DataField="CuentaTipo" HeaderText="Cuenta Tipo" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvwCxCxDocumento" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div id="CapaHistorialCredito" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="upnHistoiralCredito" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ibtnCerrarCHC" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('capaHistorialCredito'));" />
                        </td>
                    </tr>
                    <tr>
                        <td class="TituloCeldaLeft">
                            Historial de Asignacion de Creditos
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:GridView ID="gvwHistorialCreditos" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                PageSize="20" Width="100%">
                                <Columns>
                                    <asp:BoundField HeaderText="Moneda" DataField="MonedaSimbolo" NullDisplayText="" />
                                    <asp:BoundField HeaderText="CargosMaximos" DataField="cprov_CargoMax" NullDisplayText="" />
                                    <asp:BoundField HeaderText="Disponible" DataField="cprov_Saldo" NullDisplayText="" />
                                    <asp:BoundField HeaderText="Obs" DataField="ObservProv" NullDisplayText="" />
                                    <asp:BoundField HeaderText="Estado" DataField="DescEstado" NullDisplayText="" />
                                    <asp:BoundField HeaderText="Fecha Alta" DataField="cprov_FechaAlta" NullDisplayText="" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvwHistorialCreditos" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        
        function onCapaDetalleAbonos() {
            var grilla = document.getElementById('<%=gvwDetalleAbonos.ClientID %>');
            if (grilla == null) {
                alert("no tiene registros Para mostrar");
                return false;
            }

            onCapa("capaDetalleAbonos");
            return false;
        }

        function onCapaHistorialCreditos() {
            var grilla = document.getElementById('<%=gvwHistorialCreditos.ClientID %>');
            if (grilla == null) {
                alert("no tiene registros Para mostrar");
                return false;
            }

            onCapa("CapaHistorialCredito");
            return false;
        }


        function hoy() {
            var fechaActual = new Date();

            dia = fechaActual.getDate();
            mes = fechaActual.getMonth() + 1;
            anno = fechaActual.getYear();


            if (dia < 10) dia = "0" + dia;
            if (mes < 10) mes = "0" + mes;

            fechaHoy = dia + "/" + mes + "/" + anno;

            return fechaHoy;
        }

        function valCheckPAT() {
            //revisar para que sirve esta funcion
            var grilla = document.getElementById('<%=dgvCuentaxPersona.ClientID %>');
            var idperfil = 0;

            if (grilla != null) {
                //*********** obtengo el idPerfil elegido
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[5].children[0].id == event.srcElement.id) {
                        idperfil = parseFloat(rowElem.cells[0].children[1].value);  //revisar 
                        break;
                    }
                }
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[5].children[0].id != event.srcElement.id && parseFloat(rowElem.cells[0].children[1].value) == idperfil) {
                        rowElem.cells[5].children[0].status = false;
                    }
                }
            }
            return true;
        }


        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function validaMonedaPersonaGrilla() {
            {
                var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
                var grilla = document.getElementById('<%=dgvCuentaxPersona.ClientID%>');
                if (grilla != null) {
                    for (var i = 1; i < grilla.rows.length; i++) {
                        var rowElem = grilla.rows[i];
                        if (rowElem.cells[0].children[1].value == cboMoneda.value) {
                            alert('La Moneda Seleccionada Ya Ha Sido Ingresada.');
                            var cajaCargoMaximo = document.getElementById('<%= txtCargoMaximo.ClientID%>');
                            cajaCargoMaximo = "";
                            return false;
                        }
                    }
                }
                var cboMoneda = document.getElementById('<%=cboMoneda.ClientID %>');
                if (parseFloat(cboMoneda.value) == 0) {
                    alert('Seleccione Moneda.');
                    return false;
                }
                var cajaIdPersona = document.getElementById('<%= hdfidproveedor.ClientID%>');
                var cajaCargoMaximo = document.getElementById('<%= txtCargoMaximo.ClientID%>');
                if (CajaEnBlanco(cajaIdPersona)) {
                    alert('Debe seleccionar una persona antes de asignar una cuenta.');
                    return false;

                }
                if (CajaEnBlanco(cajaCargoMaximo)) {
                    alert('Ingrese un cargo m�ximo.');
                    return false;
                }
            }
            return true;

        }

        function validarNumeroPunto(elEvento) {

            var grillaPer = document.getElementById('<%=dgvCuentaxPersona.ClientID%>');

            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 46) {
                return true;

                for (var i = 1; i < grillaPer.rows.length; i++) {
                    var rowElem = grillaPer.rows[i];
                    rowElem.cells[7].innerHtml = hoy;

                }

            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }

        function valMoneda() {
            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID %>');
            if (parseFloat(cboMoneda.value) == 0) {
                alert('Seleccione Moneda.');
                return false;
            }
            return true;
        }

        function valGrillAdd() {
            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            var grilla = document.getElementById('<%=dgvCuentaxPersona.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[1].value == cboMoneda.value) {

                        alert('La Moneda Seleccionada Ya Ha Sido Ingresada.');
                        return false;
                    }
                }
            }
            return true;
        }
        function validaMonedaPersonaGrilla() {
            {
                var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
                var grilla = document.getElementById('<%=dgvCuentaxPersona.ClientID%>');
                if (grilla != null) {
                    for (var i = 1; i < grilla.rows.length; i++) {
                        var rowElem = grilla.rows[i];
                        if (rowElem.cells[0].children[1].value == cboMoneda.value) {
                            alert('La Moneda Seleccionada Ya Ha Sido Ingresada.');
                            var cajaCargoMaximo = document.getElementById('<%= txtCargoMaximo.ClientID%>');
                            cajaCargoMaximo = "";
                            return false;
                        }
                    }
                }
                var cboMoneda = document.getElementById('<%=cboMoneda.ClientID %>');
                if (parseFloat(cboMoneda.value) == 0) {
                    alert('Seleccione Moneda.');
                    return false;
                }
                var cajaIdPersona = document.getElementById('<%= hdfidproveedor.ClientID%>');
                var cajaCargoMaximo = document.getElementById('<%= txtCargoMaximo.ClientID%>');
                if (CajaEnBlanco(cajaIdPersona)) {
                    alert('Debe seleccionar una persona antes de asignar una cuenta.');
                    return false;

                }
                if (CajaEnBlanco(cajaCargoMaximo)) {
                    alert('Ingrese un cargo m�ximo.');
                    return false;
                }
            }
            return true;

        }

        function limpiaCargo() {
            var cajaCargoMaximo = document.getElementById('<%= txtCargoMaximo.ClientID%>');
            cajaCargoMaximo = "";
            return true;
        }

        function valPersona() {

            var cajaIdPersona = document.getElementById('<%= hdfidproveedor.ClientID%>');
            var cajaCargoMaximo = document.getElementById('<%= txtCargoMaximo.ClientID%>');

            if (CajaEnBlanco(cajaIdPersona)) {
                alert('Debe seleccionar una persona antes de asignar una cuenta.');
                return false;
            }

            if (CajaEnBlanco(cajaCargoMaximo)) {
                alert('Ingrese un cargo m�ximo.');
                return false;
            }
            return true;
        }


        /*Busqueda Personas ini*/

        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }

        function validarCajaBusquedaNumero(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btnBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btnBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        /*Busqueda Personas fin*/

    
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
