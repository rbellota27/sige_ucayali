﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmIngresarPorcentAdicionalRetazo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Dim objScript As New ScriptManagerClass
        Try
            cargarDatosLinea(Me.cmbLinea)
            cargarDatosSubLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue))
            verFrmInicio()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Ocurrieron problemas en la carga de datos Línea/SubLínea")
        End Try
    End Sub
    Private Sub verFrmInicio()
        Panel_Cabecera.Enabled = True
        Panel_detalle.Visible = False
        txtPorcentajeAdicional.Text = "0"
    End Sub
    Private Sub verFrmNuevo()
        Panel_Cabecera.Enabled = False
        Panel_detalle.Visible = True
    End Sub
    Private Sub cargarDatosLinea(ByVal combo As DropDownList)
        Dim nLinea As New Negocio.Linea
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo()
        combo.DataSource = lista
        combo.DataBind()
    End Sub
    Private Sub cargarDatosSubLinea(ByVal combo As DropDownList, ByVal idLinea As Integer)
        Dim nSubLinea As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectAllActivoxLinea(idLinea)
        combo.DataSource = lista
        combo.DataBind()
    End Sub

    Protected Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLinea.SelectedIndexChanged
        cargarDatosSubLinea(cmbSubLinea, CInt(cmbLinea.SelectedValue))
    End Sub

    Protected Sub btnAceptar_SubLinea_Cab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar_SubLinea_Cab.Click
        cargarDatosGrillaProductos()
    End Sub
    Private Sub cargarDatosGrillaProductos()
        Dim objScript As New ScriptManagerClass
        Try
            Dim objNProductoTipoPVRetazo As New Negocio.ProductoTipoPVRetazo
            DGV_ListaProductos.DataSource = objNProductoTipoPVRetazo.SelectPorcentRetazoxIdSubLinea(CInt(cmbSubLinea.SelectedValue))
            DGV_ListaProductos.DataBind()
            If DGV_ListaProductos.Rows.Count = 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros. Debe registrar Unidades de Medida Retazo para los productos.")
            Else
                verFrmNuevo()
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub

    Protected Sub btnAtras_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAtras.Click
        verFrmInicio()
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        registrarDatos()
    End Sub
    Private Sub registrarDatos()
        Dim objScript As New ScriptManagerClass
        Try
            Dim objProductoUM As New Negocio.ProductoUM
            Dim lista As New List(Of Entidades.ProductoUM)
            For i As Integer = 0 To DGV_ListaProductos.Rows.Count - 1
                Dim obj As New Entidades.ProductoUM
                With DGV_ListaProductos.Rows(i)
                    obj.IdProducto = CInt(.Cells(0).Text)
                    obj.IdUnidadMedida = CInt(.Cells(5).Text)
                    obj.PorcentRetazo = CDec(CType(.Cells(7).FindControl("txtPorcentAdd"), TextBox).Text)
                End With
                lista.Add(obj)
            Next
            If objProductoUM.ProductoUMUpdatePorcentRetazo(lista) Then
                objScript.mostrarMsjAlerta(Me, "La operación se realizó con éxito.")
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Ocurrieron problemas en la operación.")
        End Try
    End Sub
End Class