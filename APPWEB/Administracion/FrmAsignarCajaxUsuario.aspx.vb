﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmAsignarCajaxUsuario
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            ConfigurarDatos()
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try
            verFrmInicio()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub limpiarFrm()
        DGV_TiendaCajaUser.DataSource = Nothing
        DGV_TiendaCajaUser.DataBind()
        GV_EmpresaTienda.DataSource = Nothing
        GV_EmpresaTienda.DataBind()
    End Sub
#Region "***************** VISTAS FORMULARIOS"
    Private Sub verFrmInicio()
        limpiarFrm()
        Panel_AsignarCajas.Enabled = False
        Panel_AsignarEmpresaTienda.Enabled = False
        Panel_Cabecera.Enabled = True

        Me.btnGuardar.Visible = False
        Me.tbnAtras.Visible = False

    End Sub
    Private Sub verFrmNuevo(ByVal opcion As Integer)
        Select Case opcion
            Case -1
                '**************** tienda area
                Panel_AsignarEmpresaTienda.Enabled = False
                Panel_AsignarCajas.Enabled = False
                Panel_Cabecera.Enabled = True
                Me.btnGuardar.Visible = False
                Me.tbnAtras.Visible = False
            Case 0
                '**************** tienda area
                Panel_AsignarEmpresaTienda.Enabled = True
                Panel_AsignarCajas.Enabled = False
                Panel_Cabecera.Enabled = False
                Me.btnGuardar.Visible = True
                Me.tbnAtras.Visible = True
            Case 1
                '***************** caja usuario
                Panel_AsignarEmpresaTienda.Enabled = False
                Panel_AsignarCajas.Enabled = True
                Panel_Cabecera.Enabled = False
                Me.btnGuardar.Visible = True
                Me.tbnAtras.Visible = True
        End Select
    End Sub
#End Region
#Region "**************** CARGAR DATOS COMBO"
    Private Sub cargarDatosPerfil(ByVal idpersona As Integer)
        Dim objPerfil As New Negocio.PerfilUsuario
        Dim lista As List(Of Entidades.PerfilUsuario) = objPerfil.SelectCboxIdPersona(idpersona)

        Dim objPerfilusuario As New Entidades.PerfilUsuario(Nothing, 0, "-----", Nothing)
        lista.Insert(0, objPerfilusuario)


    End Sub


    Private Sub cargarDatosCaja(ByVal idtienda As Integer)
        Dim cbo As New Combo
        cbo.LlenarCboCajaxIdTienda(Me.cmbCaja_User, idtienda, False)
    End Sub

#End Region
    Private Sub btnBuscarUsuarioxApNombre_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarUsuarioxApNombre.Click
        buscarUsuarioxApNombre()
    End Sub
    Private Sub buscarUsuarioxApNombre()
        Try
            Dim objUsuarioView As New Negocio.UsuarioView

            '************ Buscar solo los usuario activos
            DGV_UserBuscar.DataSource = objUsuarioView.SelectxEstadoxNombre(txtApNombres_UserBuscar.Text.Trim, "1")
            DGV_UserBuscar.DataBind()
            objScript.onCapa(Me, "capaBuscarUsuario")
            txtApNombres_UserBuscar.Focus()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub DGV_UserBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_UserBuscar.SelectedIndexChanged
        cargarDatosUsuario()
    End Sub
    Private Sub cargarDatosUsuario()
        Try

            '******** mostramos los datos del usuario
            With DGV_UserBuscar
                txtNombre.Text = HttpUtility.HtmlDecode(.SelectedRow.Cells(2).Text).Trim
                txtIdPersona.Text = HttpUtility.HtmlDecode(.SelectedRow.Cells(1).Text)
                txtDNI.Text = HttpUtility.HtmlDecode(.SelectedRow.Cells(3).Text)
                lblFechaAlta.Text = .SelectedRow.Cells(5).Text
            End With

            '**************** oculto la capa
            objScript.offCapa(Me, "capaBuscarUsuario")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosAsignarCajasUsuario()


        Dim objCombo As New Combo
        objCombo.LlenarCboTiendaxIdUsuario(Me.cmbTienda_usuario, CInt(Me.txtIdPersona.Text), False)
        objCombo.LlenarCboCajaxIdTienda(Me.cmbCaja_User, CInt(Me.cmbTienda_usuario.SelectedValue), False)

        '************ obtengo la lista de cajas del usuario
        DGV_TiendaCajaUser.DataSource = (New Negocio.CajaUsuario).SelectxIdPersona(CInt(txtIdPersona.Text))
        DGV_TiendaCajaUser.DataBind()
    End Sub


    Private Sub cmbTienda_usuario_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTienda_usuario.SelectedIndexChanged
        Try
            cargarDatosCaja(CInt(cmbTienda_usuario.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub btnAddTiendaCaja_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddTiendaCaja.Click
        addTiendaCajaGrilla()
    End Sub
    Private Sub addTiendaCajaGrilla()
        Try

            '********** obtengo la lista desde la grilla
            Dim lista As List(Of Entidades.Caja) = obtenerListaCajaFrmGrilla()
            Dim obj As New Entidades.Caja(CInt(cmbCaja_User.SelectedValue), (New Util).decodificarTexto(cmbCaja_User.SelectedItem.ToString), _
                                          CInt(cmbTienda_usuario.SelectedValue), (New Util).decodificarTexto(cmbTienda_usuario.SelectedItem.ToString), "0")
            lista.Add(obj)
            DGV_TiendaCajaUser.DataSource = lista
            DGV_TiendaCajaUser.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaCajaFrmGrilla() As List(Of Entidades.Caja)
        Dim lista As New List(Of Entidades.Caja)
        For i As Integer = 0 To DGV_TiendaCajaUser.Rows.Count - 1
            With DGV_TiendaCajaUser.Rows(i)
                Dim estado As String = "0"
                If CType(.Cells(3).FindControl("chbEstado_TiendaCajaUser"), CheckBox).Checked Then
                    estado = "1"
                End If
                Dim obj As New Entidades.Caja(CInt(.Cells(5).Text), .Cells(2).Text, _
                                          CInt(.Cells(4).Text), .Cells(1).Text, estado)
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub DGV_TiendaCajaUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_TiendaCajaUser.SelectedIndexChanged
        quitarRegistroTiendaCaja()
    End Sub
    Private Sub quitarRegistroTiendaCaja()
        Try
            '***************** validar la existencias de movimientos
            Dim objUtil As New Negocio.Util
            If objUtil.ValidarxDosParametros("MovCaja", "IdCaja", DGV_TiendaCajaUser.SelectedRow.Cells(5).Text, "IdPersona", txtIdPersona.Text) > 0 Then
                objScript.mostrarMsjAlerta(Me, "La caja asignada no puede quitarse porque posee registros relacionados.")
                Return
            End If
            Dim lista As List(Of Entidades.Caja) = obtenerListaCajaFrmGrilla()
            lista.RemoveAt(DGV_TiendaCajaUser.SelectedIndex)
            DGV_TiendaCajaUser.DataSource = lista
            DGV_TiendaCajaUser.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Select Case CInt(cmbTipoAsignacion.SelectedValue)
            Case 0
                registrarEmpresaTienda()
            Case 1
                registrarCajaUsuario()
        End Select
    End Sub

    Private Sub registrarCajaUsuario()
        Try
            '********** obtengo la lista de la grilla
            Dim lista As List(Of Entidades.Caja) = obtenerListaCajaFrmGrilla()

            '************ obtengo la lista de caja usuario
            Dim listaCajaUsuario As New List(Of Entidades.CajaUsuario)
            For i As Integer = 0 To lista.Count - 1
                Dim obj As New Entidades.CajaUsuario(lista(i).IdCaja, CInt(txtIdPersona.Text), lista(i).Estado)
                listaCajaUsuario.Add(obj)
            Next

            '***************** registro la lista
            Dim objCajaUsuarioNegocio As New Negocio.CajaUsuario
            If objCajaUsuarioNegocio.registrarCajaUsuario(listaCajaUsuario, CInt(txtIdPersona.Text)) Then
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub tbnAtras_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles tbnAtras.Click
        verFrmInicio()
    End Sub

    Private Sub cargarTipoAsignacion(ByVal opcion As Integer)
        Try
            Select Case opcion
                Case -1
                Case 0
                    '*************** cargo los datos para area tienda
                    cargarDatosAsignarEmpresaTienda()
                Case 1
                    '*************** cargo los datos para caja usuario
                    cargarDatosAsignarCajasUsuario()
            End Select
            verFrmNuevo(opcion)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub cargarDatosAsignarEmpresaTienda()
        Try

            Dim objCombo As New Combo

            With objCombo

                .LlenarCboPropietario(Me.cboEmpresa, False)
                .llenarCboTiendaxIdEmpresa(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), False)

            End With

            Me.GV_EmpresaTienda.DataSource = (New Negocio.EmpresaTiendaUsuario).SelectxIdUsuario(CInt(Me.txtIdPersona.Text))
            Me.GV_EmpresaTienda.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub




    Private Sub cmbTipoAsignacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTipoAsignacion.SelectedIndexChanged
        cargarTipoAsignacion(CInt(cmbTipoAsignacion.SelectedValue))
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboEmpresa.SelectedIndexChanged

        cargarDatosTiendaxIdEmpresa()

    End Sub
    Private Sub cargarDatosTiendaxIdEmpresa()
        Try
            Dim objCbo As New Combo
            objCbo.llenarCboTiendaxIdEmpresa(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAddET_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddET.Click
        addEmpresaTienda()
    End Sub

    Private Sub addEmpresaTienda()

        Try

            Dim lista As List(Of Entidades.EmpresaTiendaUsuario) = obtenerListaEmpresaTienda()
            Dim objETU As New Entidades.EmpresaTiendaUsuario
            With objETU
                .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                .DescEmpresa = (New Util).decodificarTexto(Me.cboEmpresa.SelectedItem.ToString)
                .IdTienda = CInt(Me.cboTienda.SelectedValue)
                .DescTienda = (New Util).decodificarTexto(Me.cboTienda.SelectedItem.ToString)
                .Principal = False
                .Estado = True
            End With
            lista.Add(objETU)

            Me.GV_EmpresaTienda.DataSource = lista
            Me.GV_EmpresaTienda.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerListaEmpresaTienda() As List(Of Entidades.EmpresaTiendaUsuario)

        Dim lista As New List(Of Entidades.EmpresaTiendaUsuario)

        For i As Integer = 0 To Me.GV_EmpresaTienda.Rows.Count - 1

            Dim objETU As New Entidades.EmpresaTiendaUsuario

            With objETU

                .IdEmpresa = CInt(CType(Me.GV_EmpresaTienda.Rows(i).FindControl("hddIdEmpresa"), HiddenField).Value)
                .DescEmpresa = (New Util).decodificarTexto(CStr(CType(Me.GV_EmpresaTienda.Rows(i).FindControl("lblEmpresa"), Label).Text))
                .IdTienda = CInt(CType(Me.GV_EmpresaTienda.Rows(i).FindControl("hddIdTienda"), HiddenField).Value)
                .DescTienda = (New Util).decodificarTexto(CStr(CType(Me.GV_EmpresaTienda.Rows(i).FindControl("lblTienda"), Label).Text))

                .Principal = CBool(CType(Me.GV_EmpresaTienda.Rows(i).FindControl("chbPrincipal_ET"), CheckBox).Checked)
                .Estado = CBool(CType(Me.GV_EmpresaTienda.Rows(i).FindControl("chbEstado_ET"), CheckBox).Checked)

            End With
            lista.Add(objETU)
        Next

        Return lista

    End Function

    Protected Sub GV_EmpresaTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_EmpresaTienda.SelectedIndexChanged
        Dim IdPersona As Integer = 0
        If (IsNumeric(Me.txtIdPersona.Text)) Then
            IdPersona = CInt(Me.txtIdPersona.Text)
        End If
        quitarRegistroET(CInt(CType(Me.GV_EmpresaTienda.SelectedRow.FindControl("hddIdEmpresa"), HiddenField).Value), CInt(CType(Me.GV_EmpresaTienda.SelectedRow.FindControl("hddIdTienda"), HiddenField).Value), IdPersona)
    End Sub
    Private Sub quitarRegistroET(ByVal IdEmpresa As Integer, ByVal IdTienda As Integer, ByVal IdPersona As Integer)

        Try

            If ((New Negocio.Util).ValidarExistenciaxTablax3Campos("MovCaja", "IdEmpresa", CStr(IdEmpresa), "IdTienda", CStr(IdTienda), "IdPersona", CStr(IdPersona))) > 0 Then

                objScript.mostrarMsjAlerta(Me, "No puede quitarse el Registro porque posee Registros Relacionados en la tabla [Mov. Caja]")
                Return

            End If


            Dim lista As List(Of Entidades.EmpresaTiendaUsuario) = obtenerListaEmpresaTienda()
            lista.RemoveAt(Me.GV_EmpresaTienda.SelectedIndex)

            Me.GV_EmpresaTienda.DataSource = lista
            Me.GV_EmpresaTienda.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub registrarEmpresaTienda()


        Try

            Dim lista As List(Of Entidades.EmpresaTiendaUsuario) = obtenerListaEmpresaTienda()
            Dim IdUsuario As Integer = CInt(Me.txtIdPersona.Text)

            If ((New Negocio.EmpresaTiendaUsuario).RegistrarEmpresaTiendaUsuario(lista, IdUsuario)) Then
                objScript.mostrarMsjAlerta(Me, "La Operación se realizó con éxito.")
            Else
                Throw New Exception("Problemas en la Operación.")
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub


End Class