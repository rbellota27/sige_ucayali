﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmConfiguracionCorreo.aspx.vb" Inherits="APPWEB.frmConfiguracionCorreo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <asp:Button ID="btn_Nuevo" runat="server" Text="Nuevo" Style="cursor: hand; width: 85px;" />
                <asp:Button ID="btn_Guardar" runat="server" Text="Guardar" OnClientClick="return ( onClick_Guardar() );"
                    Style="cursor: hand; width: 85px;" />
                <asp:Button ID="btn_Cancelar" runat="server" Text="Cancelar" Style="cursor: hand;
                    width: 85px;" OnClientClick="return ( confirm('DESEA CONTINUAR CON LA OPERACIÓN') );" />
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                configuraci&oacute;n de correo
            </td>
        </tr>
        <tr>
            <td runat="server" id="tr_principal">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Texto">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_empresa" runat="server" Width="350px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_tienda" runat="server" Width="350px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Servidor Correo:
                        </td>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txt_servidor" runat="server" Width="350px"></asp:TextBox>
                                    </td>
                                    <td class="Texto" style="width: 50px;">
                                        Puerto:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txt_Puerto" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Host Inteligente:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_host" runat="server" Width="350px"></asp:TextBox>
                        </td>
                    </tr>
                    <%--<tr>
                        <td class="Texto">
                            Utilizar:
                        </td>
                        <td>
                            <asp:CheckBox ID="ckb_Credencial" runat="server" class="Texto" Text="Credencial" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="Texto" style="width: 100px;">
                            Cuenta Correo:
                        </td>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txt_CuentaCorreo" runat="server" Width="350px"></asp:TextBox>
                                    </td>
                                    <td class="Texto" style="width: 50px;">
                                        Clave:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txt_Clave" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Estado:
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rdb_Estado" runat="server" CssClass="Texto" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="gv_Editar" runat="server" Width="100%" AllowPaging="True" PageSize="15"
                    AutoGenerateColumns="False">
                    <RowStyle CssClass="GrillaRow" />
                    <Columns>
                        <asp:CommandField SelectText="Editar" ShowSelectButton="True" />
                        <asp:TemplateField HeaderText="Empresa">
                            <ItemTemplate>
                                <asp:HiddenField ID="hddIdConfiguracionCorreo" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdConfiguracionCorreo") %>' />
                                <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdEmpresa") %>' />
                                <asp:Label ID="lblempresa" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NomEmpresa") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tienda">
                            <ItemTemplate>
                                <asp:HiddenField ID="hddIdTienda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdTienda") %>' />
                                <asp:Label ID="lbltienda" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NomTienda") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Serv. Correo" DataField="servidorCorreo" />
                        <asp:BoundField HeaderText="Puerto" DataField="PuertoServidorCorreo" />
                        <asp:BoundField HeaderText="Host" DataField="HostInteligente" />                      
                        <asp:TemplateField HeaderText="Cta. Correo">
                            <ItemTemplate>
                                <asp:HiddenField ID="hddIdClave" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Clave") %>' />
                                <asp:Label ID="lblCuentaCorreo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CuentaCorreo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Estado">
                            <ItemTemplate>
                                <asp:CheckBox ID="ckbEstado" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Estado") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <HeaderStyle CssClass="GrillaCabecera" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                </asp:GridView>
                 <%-- <asp:TemplateField HeaderText="Credenciales">
                            <ItemTemplate>
                                <asp:CheckBox ID="ckbUsarCredencial" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"UsarCredencial") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>--%>
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButtonList ID="rdb_ver" runat="server" CssClass="Texto" RepeatDirection="Horizontal"
                    AutoPostBack="true">
                    <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                    <asp:ListItem Value="2">Todos</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hdd_IdOperativo" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_IdConfiguracionCorreo" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        function onClick_Guardar() {
            var txt_servidor = document.getElementById('<%=txt_servidor.ClientID %>');
            var txt_Puerto = document.getElementById('<%=txt_Puerto.ClientID %>');
            var txt_host = document.getElementById('<%=txt_host.ClientID %>');
            var txt_CuentaCorreo = document.getElementById('<%=txt_CuentaCorreo.ClientID %>');
            if (txt_servidor.value.length == 0) {
                alert('DEBE INGRESAR UN VALOR');
                txt_servidor.focus();
                txt_servidor.select();
                return false;
            }
            if (txt_Puerto.value.length == 0) {
                alert('DEBE INGRESAR UN VALOR');
                txt_Puerto.focus();
                txt_Puerto.select();
                return false;
            }
            if (txt_host.value.length == 0) {
                alert('DEBE INGRESAR UN VALOR');
                txt_host.focus();
                txt_host.select();
                return false;
            }
            if (txt_CuentaCorreo.value.length == 0) {
                alert('DEBE INGRESAR UN VALOR');
                txt_CuentaCorreo.focus();
                txt_CuentaCorreo.select();
                return false;
            }
            return (confirm('DESEA CONTINUAR CON LA OPERACIÓN.'));
        }
    
    
    </script>

</asp:Content>
