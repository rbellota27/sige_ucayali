﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmConfigCargoTarjeta
    Inherits System.Web.UI.Page

#Region "variables"
    Private objScript As New ScriptManagerClass
    Private Enum operativo
        Ninguno = 0
        GuardarNuevo = 1
        Actualizar = 2
    End Enum
    Private objCargoTarjeta As Entidades.CargoTarjeta
#End Region
    Protected Sub gvBusqueda_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBusqueda.PageIndexChanging
        Me.gvBusqueda.PageIndex = e.NewPageIndex
        Me.gvBusqueda.DataSource = Me.getDataSource
        Me.gvBusqueda.DataBind()
    End Sub

#Region "Procesos"

    Private Sub verBotones(ByVal tipo As String)

        Select Case tipo
            Case "Load"
                btnNuevo.Visible = True
                btnGuardar.Visible = False
                btnCancelar.Visible = False
                pnlPrincipal.Enabled = False
                pnlBusqueda.Enabled = True


            Case "Nuevo"
                btnNuevo.Visible = False
                btnGuardar.Visible = True
                btnCancelar.Visible = True
                pnlPrincipal.Enabled = True
                pnlBusqueda.Enabled = False

            Case "Actualizar"
                btnNuevo.Visible = False
                btnGuardar.Visible = True
                btnCancelar.Visible = True
                pnlPrincipal.Enabled = True
                pnlBusqueda.Enabled = False

            Case "Grabar"
                btnNuevo.Visible = True
                btnGuardar.Visible = False
                btnCancelar.Visible = False
                pnlPrincipal.Enabled = False
                pnlBusqueda.Enabled = True

        End Select

    End Sub
    Private Sub limpiarForm()
        txtCodigo.Text = ""
        'txtNombre.Text = ""
        txtValor.Text = ""
        txtTextoABuscar.Text = ""
        rdbEstado.SelectedValue = "1"
    End Sub
    Private Sub Desabilitar()
        txtCodigo.Enabled = False
        'txtNombre.Enabled = False
        txtValor.Enabled = False
        rdbEstado.Enabled = False
    End Sub
    Private Sub Mantenimiento(ByVal tipo As Integer)
        Dim objECargoTarjeta As New Entidades.CargoTarjeta
        Dim objNCargoTarjeta As New Negocio.CargoTarjeta
        Dim exito As Boolean = False

        With objECargoTarjeta
            .IdCargoTarjeta = CInt(IIf(txtCodigo.Text = "", 0, txtCodigo.Text))
            '.ct_Nombre = txtNombre.Text.Trim
            .ct_Valor = CDec(txtValor.Text)
            .Estado = rdbEstado.SelectedValue
        End With


        Select Case tipo
            Case operativo.GuardarNuevo
                exito = objNCargoTarjeta.InsertaCargoTarjeta(objECargoTarjeta)
                objScript.mostrarMsjAlerta(Me, "El registro ha sido guardado")
            Case operativo.Actualizar
                exito = objNCargoTarjeta.ActualizaCargoTarjeta(objECargoTarjeta)
                objScript.mostrarMsjAlerta(Me, "El registro ha sido actualizado")
        End Select
        llenarGrilla(CInt(rbver.SelectedValue)) 'Despues ingresar un registro se muestre en grilla
        ViewState.Add("operativo", operativo.Ninguno)
    End Sub

    ' para setear a una sesion el datasource de una grilla 
    Private Sub setDataSource(ByVal obj As Object)
        Session.Remove("DataSource")
        Session.Add("DataSource", obj)
    End Sub

    Private Function getDataSource() As Object
        Return Session.Item("DataSource")
    End Function

    Private Sub llenarGrilla(ByVal VerporEstado As Integer)
        Me.gvBusqueda.DataSource = (New Negocio.CargoTarjeta).SelectCargoTarjeta(VerporEstado)
        gvBusqueda.DataBind()
        setDataSource(gvBusqueda.DataSource)
    End Sub


#End Region
#Region "eventos Principales"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                btnNuevo.Focus()
                verBotones("Load")
                ViewState.Add("operativo", operativo.Ninguno)
                llenarGrilla(CInt(rbver.SelectedValue))
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        Try
            'txtNombre.Focus()
            verBotones("Nuevo")
            limpiarForm()
            ViewState.Add("operativo", operativo.GuardarNuevo)
            If Me.txtCodigo.Text = "" Then
                Me.txtValor.Enabled = True
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Try
            Mantenimiento(CInt(ViewState("operativo")))
            verBotones("Grabar")
            btnNuevo.Focus()
            'Desabilitar()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        Try
            verBotones("Load")
            limpiarForm()
            ViewState.Add("operativo", operativo.Ninguno)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
#Region "seccion de busqueda"
    Private Sub gvBusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBusqueda.SelectedIndexChanged
        Try
            cargarDatos()

            
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cargarDatos()
        'hddLogin.Value = DGVUsuario.SelectedRow.Cells(4).Text
        'hddLogin.Value = gvBusqueda.SelectedRow.Cells(4).Text
        'Dim objCargoTarjeta As Entidades.CargoTarjeta = (New Negocio.CargoTarjeta).SelectxIdCargoTarjeta(CInt(gvBusqueda.SelectedRow.Cells(1).Text))
        Try


            Me.txtCodigo.Text = gvBusqueda.SelectedRow.Cells(1).Text
            'Me.txtNombre.Text = gvBusqueda.SelectedRow.Cells(2).Text
            Me.txtValor.Text = gvBusqueda.SelectedRow.Cells(2).Text
            Dim estado As String = gvBusqueda.SelectedRow.Cells(3).Text
            Me.rdbEstado.SelectedValue = CStr(IIf(estado = "Activo", "1", "0"))

            ViewState.Add("operativo", operativo.Actualizar)
            verBotones("Actualizar")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos")
        End Try
    End Sub


#End Region
    Private Sub buscarxEstado()
        Dim obj As New ScriptManagerClass
        Try
            llenarGrilla(CInt(rbver.SelectedValue))
        Catch ex As Exception
            obj.mostrarMsjAlerta(Me, "Problemas en la búsqueda.")
        End Try
    End Sub
    Protected Sub rbver_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rbver.SelectedIndexChanged
        buscarxEstado()
    End Sub
    Protected Sub btnBuscarCargoTarjeta_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarCargoTarjeta.Click
        Dim obj As New ScriptManagerClass
        Try
            Dim objNCargoTarjeta As New Negocio.CargoTarjeta


            gvBusqueda.DataSource = objNCargoTarjeta.SelectAllActivoxCargoTarjeta_Cbo(txtTextoABuscar.Text)
            gvBusqueda.DataBind()

            'llenarGrilla(CInt(rbver.SelectedValue))

            txtTextoABuscar.Text = ""
            If gvBusqueda.Rows.Count = 0 Then
                obj.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If
        Catch ex As Exception
            obj.mostrarMsjAlerta(Me, "Problemas en la búsqueda.")
        End Try

    End Sub

End Class
