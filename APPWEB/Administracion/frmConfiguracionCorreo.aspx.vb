﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmConfiguracionCorreo
    Inherits System.Web.UI.Page

    Dim objScript As New ScriptManagerClass
    Dim cbo As Combo
    Dim obj_ConfiguracionCorreo As Entidades.ConfiguracionCorreo
    Dim list_ConfiguracionCorreo As List(Of Entidades.ConfiguracionCorreo)

    Enum operativo
        Wait = 0
        Insert = 1
        Update = 2
    End Enum





#Region "Rutinas"
    Private Sub Clear_frm()
        txt_servidor.Text = ""
        txt_Puerto.Text = ""
        txt_host.Text = ""
        txt_CuentaCorreo.Text = ""
        txt_Clave.Text = ""
        rdb_Estado.SelectedValue = "1" ' ====  +++ Activo +++
    End Sub

    Private Sub setDataSource(ByVal lista As List(Of Entidades.ConfiguracionCorreo))
        Session.Remove("Data")
        Session.Add("Data", lista)
    End Sub

    Private Function getDataSource() As List(Of Entidades.ConfiguracionCorreo)
        Return CType(Session.Item("Data"), List(Of Entidades.ConfiguracionCorreo))
    End Function

    Private Sub Update_List_ConfiguracionCorreo()
        list_ConfiguracionCorreo = (New Negocio.ConfiguracionCorreo).ConfiguracionCorreo_select(CInt(ddl_empresa.SelectedValue), Nothing, CInt(rdb_ver.SelectedValue))

        setDataSource(list_ConfiguracionCorreo)
        gv_Editar.DataSource = list_ConfiguracionCorreo
        gv_Editar.DataBind()
    End Sub

    'Private Sub habilitarEnpresaTienda(ByVal valor As Boolean)
    '    ddl_empresa.Enabled = valor
    '    ddl_tienda.Enabled = valor
    'End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        onLoad_frm()
    End Sub
    Private Sub onLoad_frm()
        If Not IsPostBack Then
            Try
                'hdd_IdUsuario.Value = CStr(Session("IdUsuario"))
                cbo = New Combo
                'cbo.LlenarCboEmpresaxIdUsuario(ddl_empresa, CInt(hdd_IdUsuario.Value), False)
                cbo.LlenarCboPropietario(ddl_empresa)
                'cbo.LlenarCboTiendaxIdEmpresaxIdUsuario(ddl_tienda, CInt(ddl_empresa.SelectedValue), CInt(hdd_IdUsuario.Value), False)
                cbo.llenarCboTiendaxIdEmpresa(ddl_tienda, CInt(ddl_empresa.SelectedValue))
                Update_List_ConfiguracionCorreo()

            Catch ex As Exception
                objScript.mostrarMsjAlerta(Me, ex.Message)
            End Try
        End If
    End Sub


    Protected Sub btn_Nuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Nuevo.Click
        hdd_IdOperativo.Value = CStr(operativo.Insert)
        Clear_frm()
        'habilitarEnpresaTienda(True)
    End Sub

    Protected Sub btn_Guardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Guardar.Click
        onClick_btn_Guardar()
    End Sub
    Private Sub onClick_btn_Guardar()
        Try
            obj_ConfiguracionCorreo = New Entidades.ConfiguracionCorreo

            With obj_ConfiguracionCorreo
                .IdConfiguracionCorreo = CInt(hdd_IdConfiguracionCorreo.Value)
                .IdEmpresa = CInt(ddl_empresa.SelectedValue)
                .IdTienda = CInt(ddl_tienda.SelectedValue)
                .servidorCorreo = CStr(txt_servidor.Text.Trim)
                .PuertoServidorCorreo = CInt(txt_Puerto.Text.Trim)
                .HostInteligente = CStr(txt_host.Text.Trim)
                .UsarCredencial = True  ' CBool(ckb_Credencial.Checked)
                .CuentaCorreo = CStr(txt_CuentaCorreo.Text.Trim)
                .Clave = CStr(txt_Clave.Text)
                .Estado = CBool(CInt(rdb_Estado.SelectedValue))
                .Ver_Estado = CInt(rdb_ver.SelectedValue)
            End With

            list_ConfiguracionCorreo = (New Negocio.ConfiguracionCorreo)._ConfiguracionCorreoTransaction(obj_ConfiguracionCorreo, True)

            If list_ConfiguracionCorreo IsNot Nothing Then

                setDataSource(list_ConfiguracionCorreo)
                gv_Editar.DataSource = list_ConfiguracionCorreo
                gv_Editar.DataBind()

                objScript.mostrarMsjAlerta(Me, "LA OPERACIÓN FINALIZÓ CON ÉXITO")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btn_Cancelar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Cancelar.Click
        hdd_IdOperativo.Value = CStr(operativo.Wait)
    End Sub

    Protected Sub rdb_ver_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdb_ver.SelectedIndexChanged
        Update_List_ConfiguracionCorreo()
    End Sub

    Private Sub gv_Editar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv_Editar.SelectedIndexChanged
        hdd_IdOperativo.Value = CStr(operativo.Update)

        hdd_IdConfiguracionCorreo.Value = CType(gv_Editar.SelectedRow.FindControl("hddIdConfiguracionCorreo"), HiddenField).Value
        ddl_empresa.SelectedValue = CType(gv_Editar.SelectedRow.FindControl("hddIdEmpresa"), HiddenField).Value
        val_Existe_IdTienda(CType(gv_Editar.SelectedRow.FindControl("hddIdTienda"), HiddenField).Value)
        txt_servidor.Text = HttpUtility.HtmlDecode(gv_Editar.SelectedRow.Cells(3).Text)
        txt_Puerto.Text = HttpUtility.HtmlDecode(gv_Editar.SelectedRow.Cells(4).Text)
        txt_host.Text = HttpUtility.HtmlDecode(gv_Editar.SelectedRow.Cells(5).Text)
        'ckb_Credencial.Checked = CType(gv_Editar.SelectedRow.FindControl("ckbUsarCredencial"), CheckBox).Checked
        txt_CuentaCorreo.Text = HttpUtility.HtmlDecode(CType(gv_Editar.SelectedRow.FindControl("lblCuentaCorreo"), Label).Text)
        txt_Clave.Text = HttpUtility.HtmlDecode(CType(gv_Editar.SelectedRow.FindControl("hddIdClave"), HiddenField).Value)
        rdb_Estado.SelectedValue = CStr(CInt(CType(gv_Editar.SelectedRow.FindControl("ckbEstado"), CheckBox).Checked))

        'habilitarEnpresaTienda(False)

    End Sub
    Private Function val_Existe_IdTienda(ByVal IdTienda As String) As Boolean
        For x As Integer = 0 To ddl_tienda.Items.Count - 1
            If ddl_tienda.Items(x).Value = IdTienda Then
                ddl_tienda.SelectedValue = IdTienda
                Return True
            End If
        Next
        onClick_Empresa()
        ddl_tienda.SelectedValue = IdTienda
    End Function

    Private Sub gv_Editar_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gv_Editar.PageIndexChanging
        gv_Editar.PageIndex = e.NewPageIndex
        gv_Editar.DataSource = getDataSource()
        gv_Editar.DataBind()
    End Sub


    Private Sub ddl_empresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_empresa.SelectedIndexChanged
        onClick_Empresa()
        Update_List_ConfiguracionCorreo()
    End Sub
    Private Sub onClick_Empresa()
        cbo = New Combo
        cbo.llenarCboTiendaxIdEmpresa(ddl_tienda, CInt(ddl_empresa.SelectedValue))
    End Sub
End Class