<%@ Page Title="Precios de Venta" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmAsignacionPrecios.aspx.vb" Inherits="APPWEB.FrmAsignacionPrecios" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td>
                <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                    onmouseout="this.src='../Imagenes/Guardar_B.JPG';" onmouseover="this.src='../Imagenes/Guardar_A.JPG';"
                    OnClientClick="return(validarSave());" />
                <asp:ImageButton ID="btnCancelar" runat="server" OnClientClick="return(confirm('Desea cancelar la operaci�n.'));"
                    ImageUrl="~/Imagenes/Cancelar_B.JPG" onmouseout="this.src='../Imagenes/Cancelar_B.JPG';"
                    onmouseover="this.src='../Imagenes/Cancelar_A.JPG';" />
            </td>
        </tr>
        <tr class="TituloCelda">
            <td>
                ASIGNACI�N DE PRECIOS&nbsp;
            </td>
        </tr>
        <tr>
            <td style="background-color: yellow">
                <asp:Label ID="Label2369" CssClass="LabelRojo" runat="server" Text="Cambio ($) :"></asp:Label>
                <asp:TextBox ID="txtTipoCambio" ReadOnly="true" Text="0" CssClass="TextBox_ReadOnly"
                    runat="server" Width="75px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_AsignarPreciosPor" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Asignar Precios por:
                            </td>
                            <td>
                                <asp:DropDownList ID="cmbAsignarPrecioPor" runat="server" Width="123px">
                                    <asp:ListItem Value="SL">Por SubL�nea</asp:ListItem>
                                    <asp:ListItem Value="PR">Por Producto</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnAceptarBuscarPor" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                    onmouseout="this.src='../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../Imagenes/Aceptar_A.JPG';" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_PorSubLinea" runat="server">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_PorSubLinea_Cab" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Tipo Existencia:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="CbotipoExistenciaXsublinea" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td style="text-align: right" class="Texto">
                                                L�nea:
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cmbLinea" runat="server" AutoPostBack="true" DataTextField="Descripcion"
                                                                DataValueField="Id">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Texto" style="text-align: right">
                                                            Sub L�nea:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cmbSubLinea" runat="server" AutoPostBack="true" DataTextField="Nombre"
                                                                DataValueField="Id">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Texto" style="text-align: right">
                                                            Tienda:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cmbTienda" runat="server" DataTextField="Nombre" DataValueField="Id">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnAceptar_SubLinea_Cab" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                                                onmouseout="this.src='../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../Imagenes/Aceptar_A.JPG';" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnAtras_Buscarpor" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                                                                onmouseout="this.src='../Imagenes/Arriba_B.JPG';" onmouseover="this.src='../Imagenes/Arriba_A.JPG';"
                                                                ToolTip="Atr�s" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Texto" style="text-align: right">
                                                Tipo Precio Venta:
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cmbTipoPrecioVenta_SubLinea" runat="server" DataTextField="Nombre"
                                                                DataValueField="IdTipoPv">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="Texto" style="text-align: right">
                                                            U.M.:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList Enabled="true" ID="cmbAsignarPorUM" runat="server">
                                                                <asp:ListItem Selected="True" Value="UMP">Unidad Medida Principal</asp:ListItem>
                                                                <asp:ListItem Value="UMO">Otras Unidades de Medida</asp:ListItem>
                                                                <asp:ListItem Enabled="false" Value="UMR">Unidades de Retazo</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="Panel_BusqAvanzadoProd2"
                                    CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_12"
                                    CollapseControlID="Image21_12" TextLabelID="Label21_12" ImageControlID="Image21_12"
                                    CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                    CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                                    SuppressPostBack="true">
                                </cc1:CollapsiblePanelExtender>
                                <asp:Image ID="Image21_12" runat="server" Height="16px" />
                                <asp:Label ID="Label21_12" runat="server" Text="B�squeda Avanzada" ForeColor="#4277AD"></asp:Label>
                                <asp:Panel ID="Panel_BusqAvanzadoProd2" runat="server">
                                    <table width="100">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="font-weight: bold; color: #4277AD">
                                                            Atributo:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="CboTipotabla_sublinea" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="Button1" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );"
                                                                Style="cursor: hand;" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="Button2" runat="server" Text="Limpiar" Width="80px" Style="cursor: hand;" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GvSublinea" runat="server" AutoGenerateColumns="False" Width="650px">
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <Columns>
                                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                            HeaderStyle-Height="25px" ItemStyle-Height="25px">
                                                            <HeaderStyle Height="25px" Width="75px" />
                                                            <ItemStyle Height="25px" />
                                                        </asp:CommandField>
                                                        <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label><asp:HiddenField
                                                                    ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="cboTTVsublinea" runat="server" DataValueField="IdTipoTablaValor"
                                                                    DataTextField="Nombre" DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>'
                                                                    Width="200px">
                                                                </asp:DropDownList>
                                                            </ItemTemplate>
                                                            <HeaderStyle Height="25px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <HeaderStyle CssClass="GrillaCabecera" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_PorSubLinea_Det" runat="server">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="Texto" style="text-align: right">
                                                            Adicionar al Precio de Compra (%):
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPUtilFijoSubLinea" runat="server" onfocus="return(aceptarFoco(this));"
                                                                            onKeypress="return(validarNumeroPunto(event));" Text="0" Width="60px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnAceptar_SublineaDet" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                                                            OnClientClick="return(validarAceptarDetalle());" onmouseout="this.src='../Imagenes/Aceptar_B.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Aceptar_A.JPG';" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnAtras" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                                                                            onmouseout="this.src='../Imagenes/Arriba_B.JPG';" onmouseover="this.src='../Imagenes/Arriba_A.JPG';"
                                                                            ToolTip="Atr�s" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 80px">
                                                        </td>
                                                        <td class="Label">
                                                            Porcent. Adicional - Pv P�blico (%):
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPorcentPVPublico" onKeypress="return(validarNumeroPuntoSigno(event));"
                                                                            onfocus="return(aceptarFoco(this));" Width="60px" Text="0" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnAceptarPorcentPVPublico" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                                                            OnClientClick="return(calcularxPorcentPVPublico());" onmouseout="this.src='../Imagenes/Aceptar_B.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Aceptar_A.JPG';" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnAtras_PVPublico" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                                                                            onmouseout="this.src='../Imagenes/Arriba_B.JPG';" onmouseover="this.src='../Imagenes/Arriba_A.JPG';"
                                                                            ToolTip="Atr�s" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right" class="Texto">
                                                            Estado:
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chbEstadoGeneral" runat="server" onclick="return(activarEstado());"
                                                                AutoPostBack="false" Checked="true" />
                                                        </td>
                                                        <td colspan="3">
                                                            <asp:CheckBox ID="cb_CambiarxEquivalencia" runat="server" Text="Calcular precio de venta (Otras U.M) a trav�s de la equivalencia"
                                                                CssClass="Texto" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="DGV_SL_UMP" runat="server" AutoGenerateColumns="False" Width="100%">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="C�d." DataField="CodigoProducto" NullDisplayText="" HeaderStyle-Height="25px"
                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                                                        <asp:BoundField HeaderText="Descripci�n" DataField="NomProducto" NullDisplayText="---"
                                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField HeaderText="U.M." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                            <asp:HiddenField ID="hddIdUMPrincipal_SL_UMP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUMPrincipal")%>' />

                                                                            <asp:Label ID="lblNomUMPrincipal_SL_UMP" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomUMPrincipal")%>'></asp:Label>

                                                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="P. Compra" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                            <asp:HiddenField ID="hddIdMonedaPC_SL_UMP" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                                            <asp:Label ID="lblMonedaPC_SL_UMP" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                                            <asp:Label ID="lblPCompra_SL_UMP" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCompra","{0:F3}")%>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Util. (%)" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:TextBox onblur="return(valGrillaBlur(event));" TabIndex="200" onKeypress="return(validarNumeroPunto(event));"
                                                                    ID="txtPorcentUtilidad_SL_UMP" onkeyup="return(calcularPrecioVenta_SL_UMP(event,'0','0'));"
                                                                    onfocus="return(aceptarFoco(this));" Width="55px" runat="server" Text="0"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Utilidad" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblMonUtilidad_SL_UMP" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                                            <asp:TextBox onblur="return(valGrillaBlur(event));" TabIndex="210" onKeypress="return(validarNumeroPunto(event));"
                                                                                ID="txtUtilidad_SL_UMP" onkeyup="return(calcularPrecioVenta_SL_UMP(event,'1','0'));"
                                                                                onfocus="return(aceptarFoco(this));" Width="60px" runat="server" Text="0"></asp:TextBox>

                                                            </ItemTemplate>
                                                            <ItemStyle  Width="100px"/>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="P.V. Final ($)" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:TextBox onblur="return(valGrillaBlur(event));" TabIndex="220" onKeypress="return(validarNumeroPunto(event));"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PrecioFinal","{0:F3}")%>' Width="60px"
                                                                    onkeyup="return(calcularPrecioVenta_SL_UMP(event,'2','0'));" onfocus="return(aceptarFoco(this));"
                                                                    ID="txtPVFinal_Dolares_SL_UMP" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="P.V. Final (S/.)" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:TextBox onblur="return(valGrillaBlur(event));" TabIndex="230" onKeypress="return(validarNumeroPunto(event));"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PrecioFinal","{0:F3}")%>' Width="60px"
                                                                    onkeyup="return(calcularPrecioVenta_SL_UMP(event,'3','0'));" onfocus="return(aceptarFoco(this));"
                                                                    ID="txtPVFinal_Soles_SL_UMP" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Moneda Destino" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="cboMonedaDestino_SL_UMP" onChange="return(calcularPrecioVenta_SL_UMP(event,'4','0'));"
                                                                    runat="server">
                                                                    <asp:ListItem Value="1" Selected="True">S/.</asp:ListItem>
                                                                    <asp:ListItem Value="2">$</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="PV. P�blico">
                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblMonedaPVPublico" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMonedaPVPublico")%>'></asp:Label>

                                                                            <asp:Label ID="lblValorPVPublico" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PVPublico","{0:F3}")%>'></asp:Label>

                                                                            <asp:HiddenField ID="hddIdMonedaPVPublico" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMonedaPVPublico")%>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Activo" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ToolTip="Haga clic para activar el registro." ID="chbEstado" runat="server"
                                                                    AutoPostBack="false" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No Afectar" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chbNoAfectar" runat="server" AutoPostBack="False" ToolTip="Haga clic para que el cambio por Sub L�nea no afecte este registro" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="" ItemStyle-BorderStyle="None">
                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblCodAntiguo" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CodigoAntiguo")%>'></asp:Label>

                                                                            <asp:HiddenField ID="hddCalOtraMoneda" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"CalOtraMoneda")%>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="P.V.Final S/.(Lima)">
                                                            <ItemTemplate>
                                                                            <asp:HiddenField ID="hddIdTiendaPrincipal" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTiendaPrincipal")%>' />

                                                                            <asp:HiddenField ID="hddIdMonedaTiendaPrincipal" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMonedaTiendaPrincipal")%>' />

                                                                            <asp:Label ID="lblPrecioTiendaPrincipal" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioTiendaPrincipal","{0:F2}")%>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_PorUM_Det" runat="server">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="Label">
                                                            Porcent. Adicional - Pv P�blico (%):
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPorcentPVPublicoUMO" onKeypress="return(validarNumeroPuntoSigno(event));"
                                                                            onfocus="return(aceptarFoco(this));" Width="60px" Text="0" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnAceptarPorcentPVUMO" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                                                            OnClientClick="return(calcularxPorcentPVPublicoUMO());" onmouseout="this.src='../Imagenes/Aceptar_B.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Aceptar_A.JPG';" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnAtras_PVUMO" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                                                                            onmouseout="this.src='../Imagenes/Arriba_B.JPG';" onmouseover="this.src='../Imagenes/Arriba_A.JPG';"
                                                                            ToolTip="Atr�s" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right" class="Texto">
                                                            Estado:
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chbEstadoGeneralUMO" runat="server" onclick="return(activarEstado());"
                                                                AutoPostBack="false" Checked="true" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="DGV_SL_UMO" runat="server" AutoGenerateColumns="False" Width="100%">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="C�d." DataField="CodigoProducto" NullDisplayText="" HeaderStyle-Height="25px"
                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField HeaderText="Descripci�n" DataField="NomProducto" NullDisplayText="---"
                                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField HeaderText="U.M." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                            <asp:HiddenField ID="hddIdUMPrincipal_SL_UMO" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUMPrincipal")%>' />

                                                                            <asp:Label ID="lblNomUMPrincipal_SL_UMO" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomUMPrincipal")%>'></asp:Label>

                                                                            <asp:HiddenField ID="hddIdProducto1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Equivalencia" HeaderText="Equivalencia" NullDisplayText="0" />
                                                        <asp:TemplateField HeaderText="P.V. Final ($)" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:TextBox onblur="return(valGrillaBlur(event));" TabIndex="220" onKeypress="return(validarNumeroPunto(event));"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PrecioFinal","{0:F3}")%>' Width="60px"
                                                                    onkeyup="return(calcularPrecioVenta_SL_UMO(event,'2','0'));" onfocus="return(aceptarFoco(this));"
                                                                    ID="txtPVFinal_Dolares_SL_UMO" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="P.V. Final (S/.)" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:TextBox onblur="return(valGrillaBlur(event));" TabIndex="230" onKeypress="return(validarNumeroPunto(event));"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PrecioFinal","{0:F3}")%>' Width="60px"
                                                                    onkeyup="return(calcularPrecioVenta_SL_UMO(event,'3','0'));" onfocus="return(aceptarFoco(this));"
                                                                    ID="txtPVFinal_Soles_SL_UMO" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Moneda Destino" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="cboMonedaDestino_SL_UMO" onChange="return(calcularPrecioVenta_SL_UMO(event,'4','0'));"
                                                                    runat="server">
                                                                    <asp:ListItem Value="1" Selected="True">S/.</asp:ListItem>
                                                                    <asp:ListItem Value="2">$</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="PV. P�blico">
                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblMonedaPVPublicoUMO" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMonedaPVPublico")%>'></asp:Label>
 
                                                                            <asp:Label ID="lblValorPVPublicoUMO" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PVPublico","{0:F3}")%>'></asp:Label>

                                                                            <asp:HiddenField ID="hddIdMonedaPVPublicoUMO" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMonedaPVPublico")%>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Activo" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ToolTip="Haga clic para activar el registro." ID="chbEstadoUMO" runat="server"
                                                                    AutoPostBack="false" Checked='<%#DataBinder.Eval(Container.DataItem,"Estado")%>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No Afectar" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chbNoAfectarUMO" runat="server" AutoPostBack="False" ToolTip="Haga clic para que el cambio por Sub L�nea no afecte este registro" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="" ItemStyle-BorderStyle="None">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCodAntiguo" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CodigoAntiguo")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="P.V.Final S/.(Lima)">
                                                            <ItemTemplate>

                                                                            <asp:HiddenField ID="hddIdTiendaPrincipal" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTiendaPrincipal")%>' />

                                                                            <asp:HiddenField ID="hddIdMonedaTiendaPrincipal" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMonedaTiendaPrincipal")%>' />

                                                                            <asp:Label ID="lblPrecioTiendaPrincipal" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioTiendaPrincipal","{0:F2}")%>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_SubLineaxRetazo" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label28123" runat="server" CssClass="Label" Text="Porcentaje (%):"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPunto(event));"
                                                ID="txtPorcent_SL_UMR" Width="75px" runat="server"></asp:TextBox>
                                            <asp:ImageButton ID="btnAceptar_SL_UMR" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../Imagenes/Aceptar_A.JPG';"
                                                OnClientClick="return(completarPorcentUtil_SL_UMR());" />
                                            <asp:ImageButton ID="btnAtras_SubLinea_retazo_Detalle" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                                                onmouseout="this.src='../Imagenes/Arriba_B.JPG';" onmouseover="this.src='../Imagenes/Arriba_A.JPG';"
                                                ToolTip="Atr�s" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="Label221235" runat="server" Text="Activo:" CssClass="Label"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chbEstadoGeneral_Retazo" runat="server" AutoPostBack="false" Checked="true"
                                                onclick="return(activarEstado());" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="DGV_SL_UMR" runat="server" Width="100%" AutoGenerateColumns="False"
                                    AutoGenerateSelectButton="false">
                                    <Columns>
                                        <asp:BoundField DataField="IdProducto" HeaderText="Id" NullDisplayText="0" />
                                        <asp:BoundField DataField="NomProducto" HeaderText="Descripci�n" NullDisplayText="---" />
                                        <asp:BoundField DataField="NomUMPrincipal" HeaderText="U.M. Principal" NullDisplayText="---" />
                                        <asp:TemplateField HeaderText="P.Compra U.M. Principal">
                                            <ItemTemplate>
                                                            <asp:HiddenField ID="hddIdMonedaPC_UMP_SL_UMR" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMonedaPC_UMPrincipal")%>' />

                                                            <asp:Label ID="Label2312" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMonedaPC_UMPrincipal")%>'></asp:Label>

                                                            <asp:Label ID="Label2415" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCompra","{0:F2}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P.Venta U.M. Principal">
                                            <ItemTemplate>
                                                            <asp:Label ID="Label2358" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                            <asp:Label ID="Label24965" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PV_UMPrincipal","{0:F2}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Equivalencia" DataFormatString="{0:F4}" HeaderText="Equivalencia"
                                            NullDisplayText="0" />
                                        <asp:TemplateField HeaderText="U.M. Retazo">
                                            <ItemTemplate>
                                                            <asp:HiddenField ID="hddIdUMRetazo_SL_UMR" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />

                                                            <asp:Label ID="Label9" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomUnidadMedida")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="PorcentAdicionalRetazo" DataFormatString="{0:F2}" HeaderText="Porc. Adicional (%)"
                                            NullDisplayText="0" />
                                        <asp:TemplateField HeaderText="Util. (%)">
                                            <ItemTemplate>
                                                <asp:TextBox onblur="return(valGrillaBlur(event));" onKeypress="return(validarNumeroPunto(event));"
                                                    ID="txtPorcentUtilidad_SL_UMR" onKeyup="return(calcularPrecioVenta_SL_UMR(event,'0','0'));"
                                                    onfocus="return(aceptarFoco(this));" Width="75px" runat="server" Text="0"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Utilidad">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonUtilidad_SL_UMR" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMonedaPC_UMPrincipal")%>'></asp:Label>

                                                            <asp:TextBox onblur="return(valGrillaBlur(event));" onKeypress="return(validarNumeroPunto(event));"
                                                                ID="txtUtilidad_SL_UMR" onKeyup="return(calcularPrecioVenta_SL_UMR(event,'1','0'));"
                                                                onfocus="return(aceptarFoco(this));" Width="75px" runat="server" Text="0"></asp:TextBox>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P.V. Final">
                                            <ItemTemplate>
                                                            <asp:HiddenField ID="hddIdMonedaPV_SL_UMR" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMoneda")%>' />

                                                            <asp:Label ID="Label27" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>

                                                            <asp:TextBox onblur="return(valGrillaBlur(event));" onKeypress="return(validarNumeroPunto(event));"
                                                                onKeyup="return(calcularPrecioVenta_SL_UMR(event,'2','0'));" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioFinal","{0:F2}")%>'
                                                                Width="75px" onfocus="return(aceptarFoco(this));" ID="txtPVFinal_SL_UMR" runat="server"></asp:TextBox>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Activo">
                                            <ItemTemplate>
                                                <asp:CheckBox ToolTip="Haga clic para activar el registro." ID="chbEstado_SL_UMR"
                                                    runat="server" AutoPostBack="false" Checked='<%#DataBinder.Eval(Container.DataItem,"FlagEstado")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Afectar">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chbNoAfectar_SL_UMR" runat="server" AutoPostBack="False" ToolTip="Haga clic para que el cambio por Sub L�nea no afecte este registro" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Prod_Cab_P" runat="server">
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:ImageButton ID="btnAgregar" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                                    OnClientClick="return(  valOnClick_btnBuscarProducto()   );" onmouseout="this.src='../Imagenes/BuscarProducto_b.JPG';"
                                    onmouseover="this.src='../Imagenes/BuscarProducto_A.JPG';" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="text-align: right">
                                Descripci�n:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCodigoProducto" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                Width="110px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtProducto" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                Width="520px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="text-align: right">
                                U.M.:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtNomUMPrincipal" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                Width="110px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" style="text-align: right; font-weight: bold">
                                Tienda:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cmbTienda_Prod" runat="server" DataTextField="Nombre" DataValueField="Id">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" style="text-align: right; font-weight: bold">
                                            Tipo Precio Venta:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cmbTipoPrecioVenta_Prod" runat="server" DataTextField="Nombre"
                                                DataValueField="IdTipoPv">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnAceptar_ProdCabecera" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                                OnClientClick="return(   validarAceptarProducto()    );" onmouseout="this.src='../Imagenes/Aceptar_B.JPG';"
                                                onmouseover="this.src='../Imagenes/Aceptar_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnAtras_ProdCabecera" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                                                onmouseout="this.src='../Imagenes/Arriba_B.JPG';" onmouseover="this.src='../Imagenes/Arriba_A.JPG';"
                                                ToolTip="Atr�s" />
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_ProductoDetalle" runat="server">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold; text-align: right">
                                            Unidad Medida:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cmbUMedida_Prod" runat="server" DataTextField="NombreCortoUM"
                                                DataValueField="IdUnidadMedida">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnAgregar_UMProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                OnClientClick="return(validarGrillaProd());" onmouseout="this.src='../Imagenes/Agregar_B.JPG';"
                                                onmouseover="this.src='../Imagenes/Agregar_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnAtras_ProdDetalle" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                                                onmouseout="this.src='../Imagenes/Arriba_B.JPG';" onmouseover="this.src='../Imagenes/Arriba_A.JPG';"
                                                ToolTip="Atr�s" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="ckProducto" runat="server" Text="Calcular precio de venta (Otras U.M) a trav�s de la equivalencia"
                                                CssClass="Texto" onClick=" return ( valEquivalenciaxProducto() ); " />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="DGV_ProdUM" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="false"
                                    Width="100%">
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ButtonType="Link" ShowSelectButton="true" />
                                        <asp:BoundField DataField="IdUnidadMedida" HeaderText="Id U.M." NullDisplayText="---" />
                                        <asp:BoundField DataField="NomUMedida" HeaderText="U.Medida" NullDisplayText="---" />
                                        <asp:BoundField DataField="Equivalencia" HeaderText="Equivalencia" NullDisplayText="0" />
                                        <asp:BoundField DataField="getPrecioCompraxRetazo" HeaderText="P.Compra x U.M." NullDisplayText="0" />
                                        <asp:TemplateField HeaderText="Util. Fijo (%)">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPorUtilFijoxProd_Prod" runat="server" onblur="return(valGrillaBlur(event));"
                                                    onKeypress="return(validarNumeroPunto(event));" onKeyup="return(calcularxFilaProd('0',event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PUtilFijo","{0:F2}")%>' Width="80px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Util. Var. (%)">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPorUtilVarxProd_Prod" runat="server" onblur="return(valGrillaBlur(event));"
                                                    onKeypress="return(validarNumeroPunto(event));" onKeyup="return(calcularxFilaProd('1',event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PUtilVariable","{0:F2}")%>' Width="80px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Valor Util.">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtEscalarUtilxProd_Prod" runat="server" onblur="return(valGrillaBlur(event));"
                                                    onKeypress="return(validarNumeroPunto(event));" onKeyup="return(calcularxFilaProd('2',event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Utilidad","{0:F2}")%>' Width="80px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P.V. Final">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtEscalarValxProd_Prod" runat="server" onblur="return(valGrillaBlur(event));"
                                                    onKeypress="return(validarNumeroPunto(event));" onKeyup="return(calcularxFilaProd('3',event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"Valor","{0:F2}")%>' Width="80px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Activo">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chbEstado_Prod" runat="server" AutoPostBack="false" Checked='<%#DataBinder.Eval(Container.DataItem,"EstadoBoolean")%>'
                                                    ToolTip="Haga clic para activar el registro." />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="DGV_PRODUCTO" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" SelectText="Quitar" ButtonType="Link" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="U. M." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:HiddenField ID="hddIdUM_PR" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />

                                                            <asp:Label ID="lblNomUMPrincipal_PR" runat="server" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NomUMedida")%>'></asp:Label>

                                                            <asp:HiddenField ID="hddUnidadPrincipal" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"UnidadPrincipal")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Equivalencia" HeaderText="Equivalencia" NullDisplayText="0" DataFormatString="{0:F4}" />
                                        <asp:BoundField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="getDescUMPrincipal" HeaderText="Principal" NullDisplayText="--" />
                                        <asp:BoundField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="getDescUMRetazo" HeaderText="Retazo" NullDisplayText="--" />
                                        <asp:BoundField DataField="PorcentRetazo" HeaderText="P. Retazo (%)" NullDisplayText="0"
                                            DataFormatString="{0:F2}" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Util. (%)" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPorcentUtilidad_PR" runat="server" onkeyup="return(calcularPrecioVenta_PR(event,'0','1','1'));"
                                                    onblur="return(valGrillaBlur(event));" onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPunto(event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PUtilFijo","{0:F2}")%>' Width="60px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Util." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonUtilidad_PR" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMonedaPC")%>'></asp:Label>

                                                            <asp:TextBox ID="txtUtilidad_PR" runat="server" onkeyup="return(calcularPrecioVenta_PR(event,'1','1','1'));"
                                                                onblur="return(valGrillaBlur(event));" onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPunto(event));"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Utilidad","{0:F2}")%>' Width="60px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P.V. Final ($)" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPVFinal_Dolares_PR" runat="server" onkeyup="return(calcularPrecioVenta_PR(event,'2','1','1'));"
                                                    onblur="return(valGrillaBlur(event));" onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPunto(event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PV_Dolares","{0:F2}")%>' Width="60px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P.V. Final (S/.)" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                            <%--qwe--%>
                                                <asp:TextBox ID="txtPVFinal_Soles_PR" runat="server" onkeyup="return(calcularPrecioVenta_PR(event,'3','1','1'));"
                                                    onblur="return(valGrillaBlur(event));" onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPunto(event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PV_Soles","{0:F2}")%>' Width="60px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Moneda Destino" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboMonedaDestino_PR" runat="server">
                                                    <asp:ListItem Selected="True" Value="1">S/.</asp:ListItem>
                                                    <asp:ListItem Value="2">$</asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Activo" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chbEstado_PR" runat="server" AutoPostBack="false" Checked='<%#DataBinder.Eval(Container.DataItem,"EstadoBoolean")%>'
                                                    ToolTip="Haga clic para activar el registro." />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P.V.Final S/.(Lima)">
                                            <ItemTemplate>
                                                            <asp:HiddenField ID="hddIdTiendaPrincipal" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTiendaPrincipal")%>' />

                                                            <asp:HiddenField ID="hddIdMonedaTiendaPrincipal" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdMonedaTiendaPrincipal")%>' />

                                                            <asp:Label ID="lblPrecioTiendaPrincipal" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioTiendaPrincipal","{0:F2}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdProducto" runat="server" />
                <asp:HiddenField ID="hddIdUnidadMedida_Principal" runat="server" />
                <asp:HiddenField ID="hddIdMoneda_PC" runat="server" />
                <asp:HiddenField ID="hddTipoCambio" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td style="height: 1000px">
            </td>
        </tr>
    </table>
    <div id="capaPrecioCompra" style="display: none;">
        <table>
            <tr>
                <td>
                    <asp:TextBox ID="txtMoneda_PrecioCompra" runat="server" CssClass="TextBoxReadOnly"
                        Width="30px" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtPrecioCompra_prod" runat="server" Width="80px" CssClass="TextBoxReadOnly"
                        ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Existencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                    DataTextField="Descripcion" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    DataValueField="Id" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );"></asp:TextBox>
                                        </td>
                                        <td class="Texto">
                                            C�d.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCodigoProducto_Find" Font-Bold="true" Width="100px" runat="server"
                                                onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );"
                                                TabIndex="205"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="C�digo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblCodigoProducto" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Codigo") %>'></asp:Label>

                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                            <asp:BoundField DataField="UnidadMedida" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderText="U.M." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                NullDisplayText="---" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function activarEstado() {
            var cbo = document.getElementById('<%=cmbAsignarPorUM.ClientID%>');
            var chb = document.getElementById('<%=chbEstadoGeneral.ClientID%>');
            var chb1 = document.getElementById('<%=chbEstadoGeneral_Retazo.ClientID%>');
            var chb2 = document.getElementById('<%=chbEstadoGeneralUMO.ClientID%>');
            switch (cbo.value) {
                case 'UMP':
                    var grilla = document.getElementById('<%=DGV_SL_UMP.ClientID %>');
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                            var rowElem = grilla.rows[i];
                            rowElem.cells[9].children[0].firstChild.status = chb.checked;
                        }
                    }
                    break;
                case 'UMO':
                    var grilla = document.getElementById('<%=DGV_SL_UMO.ClientID %>');
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                            var rowElem = grilla.rows[i];
                            rowElem.cells[7].children[0].firstChild.status = chb2.checked;
                        }
                    }
                    break;
                case 'UMR':
                    var grilla = document.getElementById('<%=DGV_SL_UMR.ClientID %>');
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                            var rowElem = grilla.rows[i];
                            rowElem.cells[11].children[0].firstChild.status = chb1.checked;
                        }
                    }
                    break;

            }
            return true;
        }

        function validarSave() {
            var tipoAsignacion = document.getElementById('<%=cmbAsignarPrecioPor.ClientID %>').value;
            switch (tipoAsignacion) {
                case 'SL':
                    var opcion = document.getElementById('<%=cmbAsignarPorUM.ClientID %>').value;
                    switch (opcion) {
                        case 'UMP':
                            var grilla = document.getElementById('<%=DGV_SL_UMP.ClientID %>');
                            if (grilla == null) {
                                alert('No se hallaron registros.');
                                return false;
                            }
                            // VALIDAR PRECIO DE TIENDA LIMA
                            var pvSoles = 0;
                            var pvTiendaPrincipal = 0;
                            var IdTienda = parseInt(document.getElementById('<%=cmbTienda.ClientID %>').value);
                            var IdTiendaPrincipal = 0;
                            var DescripcionProducto = '';
                            var CodigoProducto = '';
                            //
                            for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                                var rowElem = grilla.rows[i];

                                pvSoles = parseFloat(rowElem.cells[7].children[0].value);
                                if (!esDecimal(pvSoles)) { pvSoles = 0; }


                                IdTiendaPrincipal = parseFloat(rowElem.cells[13].children[0].cells[0].children[0].value);
                                if (isNaN(IdTiendaPrincipal)) { IdTiendaPrincipal = 0; }

                                pvTiendaPrincipal = parseFloat(rowElem.cells[13].children[0].cells[2].children[0].innerHTML);
                                if (!esDecimal(pvTiendaPrincipal)) { pvTiendaPrincipal = 0; }


                                DescripcionProducto = rowElem.cells[0].innerHTML;
                                CodigoProducto = rowElem.cells[1].innerHTML;

                                if (IdTiendaPrincipal != 0) {

                                    if (IdTiendaPrincipal != IdTienda && pvSoles < pvTiendaPrincipal) {

                                        alert('El precio del producto ' + DescripcionProducto + ' - ' + CodigoProducto + ' es menor que el precio de tienda principal. No se Permite la operaci�n.');
                                        return false;
                                    }

                                }

                                if (parseInt(rowElem.cells[3].children[0].cells[0].children[0].value) == 0 || !esDecimal(rowElem.cells[3].children[0].cells[0].children[0].value)) {
                                    alert('El producto ' + DescripcionProducto + ' - ' + CodigoProducto + ' no posee un precio de compra. Ingrese al Formulario de <Asignaci�n Precio de Compra> y as�gnele un precio de compra.');
                                    rowElem.cells[3].children[0].cells[2].children[0].select();
                                    rowElem.cells[3].children[0].cells[2].children[0].focus();
                                    return false;
                                }
                            }


                            break;

                        case 'UMO':

                            // VALIDAR PRECIO DE TIENDA LIMA
                            var pvSoles = 0;
                            var pvTiendaPrincipal = 0;
                            var IdTienda = parseInt(document.getElementById('<%=cmbTienda.ClientID %>').value);
                            var IdTiendaPrincipal = 0;
                            var DescripcionProducto = '';
                            var CodigoProducto = '';
                            //

                            var grilla = document.getElementById('<%=DGV_SL_UMO.ClientID %>');
                            if (grilla == null) {
                                alert('No se hallaron registros.');
                                return false;
                            }
                            for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                                var rowElem = grilla.rows[i];


                                pvSoles = parseFloat(rowElem.cells[7].children[0].value);
                                if (!esDecimal(pvSoles)) { pvSoles = 0; }


                                IdTiendaPrincipal = parseFloat(rowElem.cells[11].children[0].value);
                                if (isNaN(IdTiendaPrincipal)) { IdTiendaPrincipal = 0; }

                                pvTiendaPrincipal = parseFloat(rowElem.cells[11].children[2].innerHTML);
                                if (!esDecimal(pvTiendaPrincipal)) { pvTiendaPrincipal = 0; }


                                DescripcionProducto = rowElem.cells[0].innerHTML;
                                CodigoProducto = rowElem.cells[1].innerHTML;

                                if (IdTiendaPrincipal != 0) {

                                    if (IdTiendaPrincipal != IdTienda && pvSoles < pvTiendaPrincipal) {

                                        alert('El precio del producto ' + DescripcionProducto + ' - ' + CodigoProducto + ' es menor que el precio de tienda principal. No se Permite la operaci�n.');
                                        return false;
                                    }

                                }
                            }
                            break;
                    }
                    break;
                case 'PR':
                    // VALIDAR PRECIO DE TIENDA LIMA
                    var pvSoles = 0;
                    var pvTiendaPrincipal = 0;
                    var IdTienda = parseInt(document.getElementById('<%=cmbTienda_Prod.ClientID %>').value);
                    var IdTiendaPrincipal = 0;
                    var DescripcionProducto = '';
                    var CodigoProducto = '';
                    //
                    var grilla = document.getElementById('<%=DGV_PRODUCTO.ClientID %>');
                    var combo = document.getElementById('<%=cmbUMedida_Prod.ClientID %>');
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera

                            var rowElem = grilla.rows[i];

                            pvSoles = parseFloat(rowElem.cells[9].children[0].value);
                            if (!esDecimal(pvSoles)) { pvSoles = 0; }


                            IdTiendaPrincipal = parseFloat(rowElem.cells[12].children[0].value);
                            if (isNaN(IdTiendaPrincipal)) { IdTiendaPrincipal = 0; }

                            pvTiendaPrincipal = parseFloat(rowElem.cells[12].children[2].innerHTML);
                            if (!esDecimal(pvTiendaPrincipal)) { pvTiendaPrincipal = 0; }

                            if (IdTiendaPrincipal != 0) {

                                if (IdTiendaPrincipal != IdTienda && pvSoles < pvTiendaPrincipal) {

                                    alert('El precio del producto es menor que el precio de tienda principal. No se Permite la operaci�n.');
                                    return false;
                                }

                            }

                        }
                    }

                    break;
            }
            return (confirm('Desea continuar con la operaci�n?'));
        }

        function validarGrillaProd() {
            var grilla = document.getElementById('<%=DGV_PRODUCTO.ClientID %>');
            var combo = document.getElementById('<%=cmbUMedida_Prod.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comenzamos en 1 para no tomar la cabecera
                    var rowElem = grilla.rows[i];
                    if (parseInt(rowElem.cells[1].children[0].value) == parseInt(combo.value)) {
                        alert('La Unidad de Medida seleccionada ya ha sido ingresado');
                        return false;
                    }

                }
            }
            return true;
        }
        function calcularxFilaProd(opc,e) {
            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;
            var grilla = document.getElementById('<%=DGV_ProdUM.ClientID %>');
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                if (rowElem.cells[5].children[0].id == event_element.id || rowElem.cells[6].children[0].id == event_element.id || rowElem.cells[7].children[0].id == event_element.id || rowElem.cells[8].children[0].id == event_element.id) {
                    var rowElem = grilla.rows[i];
                    var precioCompra = parseFloat(rowElem.cells[4].innerHTML);
                    var porcentFijo = parseFloat(rowElem.cells[5].children[0].value);
                    if (isNaN(porcentFijo) || porcentFijo == null) {
                        porcentFijo = 0;
                    }
                    var porcentVar = parseFloat(rowElem.cells[6].children[0].value);
                    if (isNaN(porcentVar) || porcentVar == null) {
                        porcentVar = 0;
                    }
                    var valorUtil = parseFloat(rowElem.cells[7].children[0].value);
                    if (isNaN(valorUtil) || valorUtil == null) {
                        valorUtil = 0;
                    }
                    var precioFinal = parseFloat(rowElem.cells[8].children[0].value);
                    if (isNaN(precioFinal) || precioFinal == null) {
                        precioFinal = 0;
                    }
                    switch (opc) {
                        case '0':
                            if (porcentVar == 0) {
                                valorUtil = (porcentFijo * precioCompra) / 100;
                            }
                            precioFinal = precioCompra + valorUtil;
                            rowElem.cells[6].children[0].value = porcentVar;
                            rowElem.cells[7].children[0].value = valorUtil;
                            rowElem.cells[8].children[0].value = precioFinal;
                            break;
                        case '1':
                            if (porcentVar == 0) {
                                valorUtil = (porcentFijo * precioCompra) / 100;
                            } else {
                                valorUtil = (porcentVar * precioCompra) / 100;
                            }
                            precioFinal = precioCompra + valorUtil;
                            rowElem.cells[5].children[0].value = porcentFijo;
                            rowElem.cells[7].children[0].value = valorUtil;
                            rowElem.cells[8].children[0].value = precioFinal;
                            break;
                        case '2':
                            if (porcentVar == 0) {
                                if (precioCompra == 0 && valorUtil != 0) {
                                    porcentFijo = 100;
                                } else if (precioCompra == 0 && valorUtil == 0) {
                                    porcentFijo = 0;
                                } else {
                                    porcentFijo = (valorUtil / precioCompra) * 100;
                                }
                            } else {
                                porcentVar = (valorUtil / precioCompra) * 100;
                            }
                            precioFinal = precioCompra + valorUtil;
                            rowElem.cells[5].children[0].value = porcentFijo;
                            rowElem.cells[6].children[0].value = porcentVar;
                            rowElem.cells[8].children[0].value = precioFinal;
                            break;
                        case '3':
                            valorUtil = precioFinal - precioCompra;
                            if (porcentVar == 0) {
                                if (precioCompra == 0 && valorUtil != 0) {
                                    porcentFijo = 100;
                                } else if (precioCompra == 0 && valorUtil == 0) {
                                    porcentFijo = 0;
                                } else {
                                    porcentFijo = (valorUtil / precioCompra) * 100;
                                }
                            } else {
                                porcentVar = (valorUtil / precioCompra) * 100;
                            }
                            precioFinal = precioCompra + valorUtil;
                            rowElem.cells[5].children[0].value = porcentFijo;
                            rowElem.cells[6].children[0].value = porcentVar;
                            rowElem.cells[7].children[0].value = valorUtil;
                            break;
                    }
                    return true;
                }
            }
        }
        function validarAceptarProducto() {
            var hddIdProducto = document.getElementById('<%=hddIdProducto.ClientID %>');
            if (isNaN(parseInt(hddIdProducto.value)) || hddIdProducto.value.length <= 0) {
                alert('DEBE SELECCIONAR UN PRODUCTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var hddIdMoneda = document.getElementById('<%=hddIdMoneda_PC.ClientID %>');
            if (isNaN(parseInt(hddIdMoneda.value)) || hddIdMoneda.value.length <= 0) {
                alert('EL PRODUCTO SELECCIONADO NO POSEE UN PRECIO DE COMPRA. DEBE REGISTRAR UN PRECIO DE COMPRA ANTES DE REGISTRAR EL PRECIO DE VENTA. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            return true;
        }

        function validarEnter() {
            return (!esEnter('event'));
        }

        function valGrillaBlur(e) {
            //obtiene srcElement.Id
            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;
            //termina
            var id = event_element.id;
            var caja = document.getElementById(id);
            if (CajaEnBlanco(caja)) {
                caja.focus();
                alert('Debe ingresar un valor.');
                return true;
            }
            if (!esDecimal(caja.value)) {
                caja.focus();
                alert('Valor no v�lido.');
                return true;
            }
        }

        function validarAceptarDetalle() {
            var caja1 = document.getElementById("<%=txtPUtilFijoSubLinea.ClientID %>");
            if (CajaEnBlanco(caja1)) {
                caja1.value = 0;
            }
            if (!esDecimal(caja1.value)) {
                caja1.focus();
                caja1.select();
                alert("Valor no v�lido.");
                return false;
            }
            if (confirm("Esto generar� que todos los productos listados tomen los porcentajes ingresados y se calculen sus precios de venta autom�ticamente. Est� seguro que desea continuar?")) {
                calcularValores();
                return false;
            } else {
                return false;
            }
        }

        function calcularValores() {
            var pUtilFijo = parseFloat(document.getElementById("<%=txtPUtilFijoSubLinea.ClientID %>").value);
            var grilla = document.getElementById('<%=DGV_SL_UMP.ClientID %>');
            var estado = document.getElementById('<%=chbEstadoGeneral.ClientID %>').checked;
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera        
                var rowElem = grilla.rows[i];
                //imprimo los datos
                if (rowElem.cells[11].children[0].firstChild.status == false) {
                    rowElem.cells[4].children[0].value = redondear(pUtilFijo, 2);
                    rowElem.cells[10].children[0].firstChild.status = estado;
                }
            }
            calcularPrecioVenta_SL_UMP('event','0', '1');
            return false;
        }
        function validarNumeroPunto(e) {
            //obtener caracter pulsado en todos los exploradores
            var evento = e || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 46 || caracter == 45) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }



        //************************* NUEVOS SCRIPTS MONEDA VENTA != MONEDA COMPRA

        function onLoad_PrecioVenta_SL_UMP() {

            calcularPrecioVenta_SL_UMP('event','5', '1');

            return false;
        }
        function onLoad_calcularPrecioVenta_SL_UMO() {

            calcularPrecioVenta_SL_UMO('event','5', '1');

            return false;
        }
        function calcularPrecioVenta_SL_UMP(e,opcion, recorrerAll) {
            // Recorrer All
            // 0: FALSE
            // 1: TRUE

            // OPcion:
            // 0: Util (%)
            // 1: Utilidad
            // 2: PV Final $
            // 3: PV Final S/.
            // 4: COMBO MONEDA destino
            // 5: ONLOAD
            // 6: Porcent PV PUBLICO
            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;

            var grilla = document.getElementById('<%=DGV_SL_UMP.ClientID%>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }

            //************ declaramos las variables
            var tipocambio = parseFloat(document.getElementById('<%=txtTipoCambio.ClientID%>').value);
            var porcentUtil = 0;
            var util = 0;
            var pc = 0;
            var IdMonedaPC = 0;
            var IdMonedaPV = 0;
            var pvSoles = 0;
            var pvDolares = 0;

            var opcionOriginal = opcion;

            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                var controlId = '';                
                if (e != null) {
                    if (event_element != null) {
                        controlId = event_element.id;
                    }
                }


                if (rowElem.cells[4].children[0].id == controlId || rowElem.cells[5].children[1].id == controlId || rowElem.cells[6].children[0].id == controlId || rowElem.cells[7].children[0].id == controlId || rowElem.cells[8].children[0].id == controlId || controlId == '' || controlId == document.getElementById('<%=btnAceptar_SublineaDet.ClientID%>').id || recorrerAll == '1') {
                    porcentUtil = parseFloat(rowElem.cells[4].children[0].value);
                    if (!esDecimal(porcentUtil)) { porcentUtil = 0; }
                    util = parseFloat(rowElem.cells[5].children[1].value);
                    if (!esDecimal(util)) { util = 0; }
                    IdMonedaPC = parseFloat(rowElem.cells[3].children[0].value);
                    if (!esDecimal(IdMonedaPC)) { IdMonedaPC = 0; }
                    pc = parseFloat(rowElem.cells[3].children[2].innerHTML);
                    if (!esDecimal(pc)) { pc = 0; }
                    IdMonedaPV = parseFloat(rowElem.cells[8].children[0].value);
                    if (!esDecimal(IdMonedaPV)) { IdMonedaPV = 0; }
                    pvDolares = parseFloat(rowElem.cells[6].children[0].value);
                    if (!esDecimal(pvDolares)) { pvDolares = 0; }
                    pvSoles = parseFloat(rowElem.cells[7].children[0].value);
                    if (!esDecimal(pvSoles)) { pvSoles = 0; }

                    if (IdMonedaPC == 0) {
                        alert('El Producto ' + rowElem.cells[0].innerHTML + ' - ' + rowElem.cells[1].innerHTML + ' no posee una Moneda de Precio de Compra, esto producir� error en la operaci�n.');
                        return false;
                    }
                    if (tipocambio == 0) {
                        alert('El Tipo de Cambio debe ser mayor a cero');
                        return false;
                    }


                    if (controlId == '' && opcionOriginal == '5') {
                        if (IdMonedaPV == 1) {  //********* soles
                            opcion = '3';
                        }
                        if (IdMonedaPV == 2) {  //********* dolares
                            opcion = '2';
                        }
                    }

                    if (opcionOriginal == '6') { //******** x Porcent PV Publico

                        var IdMonedaPVPublico = parseInt(rowElem.cells[8].children[0].value);

                        if (IdMonedaPVPublico == 1) {
                            opcion = '3';
                        } else {
                            opcion = '2';
                        }


                    }

                    switch (opcion) {
                        case '0':  //*************UTIL (%)
                            if (IdMonedaPC == 1) {  //*********** SOLES
                                pvSoles = pc * (1 + porcentUtil / 100);
                                pvDolares = pvSoles / tipocambio;
                                util = pvSoles - pc;
                            }
                            if (IdMonedaPC == 2) {  //*********** DOLARES
                                pvDolares = pc * (1 + porcentUtil / 100);
                                pvSoles = pvDolares * tipocambio;
                                util = pvDolares - pc;
                            }
                            if (IdMonedaPC > 2) {  //*********** OTRA MONEDA
                                //usamos posicion 0 porque el label no renderiza
                                var CalOtraMoneda = parseFloat(rowElem.cells[12].children[0].value);
                                if (isNaN(CalOtraMoneda)) { CalOtraMoneda = 0; }
                                rowElem.cells[5].children[0].innerHTML = 'S/.';

                                pvSoles = CalOtraMoneda * (1 + porcentUtil / 100);
                                pvDolares = pvSoles / tipocambio;
                                util = pvSoles - CalOtraMoneda;
                            }
                            //************ IMPRIMIMOS


                            rowElem.cells[5].children[1].value = redondear(util, 2);
                            rowElem.cells[6].children[0].value = redondear(pvDolares, 2);
                            rowElem.cells[7].children[0].value = redondear(pvSoles, 2);
                            break;
                        case '1':  //************ UTILIDAD
                            if (IdMonedaPC == 1) {  //*********** SOLES
                                pvSoles = pc + util;
                                pvDolares = pvSoles / tipocambio;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvSoles - pc) / pc;
                                }

                            }
                            if (IdMonedaPC == 2) {  //*********** DOLARES
                                pvDolares = pc + util;
                                pvSoles = pvDolares * tipocambio;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvDolares - pc) / pc;
                                }

                            }
                            if (IdMonedaPC > 2) {  //*********** SOLES
                                //usamos posicion 0 porque el label no renderiza
                                var CalOtraMoneda = parseFloat(rowElem.cells[12].children[0].value);
                                if (isNaN(CalOtraMoneda)) { CalOtraMoneda = 0; }
                                rowElem.cells[5].children[0].innerHTML = 'S/.';

                                pvSoles = CalOtraMoneda + util;
                                pvDolares = pvSoles / tipocambio;

                                //*********** Validamos el PC
                                if (CalOtraMoneda == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvSoles - CalOtraMoneda) / CalOtraMoneda;
                                }

                            }
                            //************ IMPRIMIMOS

                            rowElem.cells[4].children[0].value = redondear(porcentUtil * 100, 2);
                            rowElem.cells[6].children[0].value = redondear(pvDolares, 2);
                            rowElem.cells[7].children[0].value = redondear(pvSoles, 2);
                            break;
                        case '2': //************* PV Final Dolares
                            pvSoles = pvDolares * tipocambio;
                            if (IdMonedaPC == 1) {  //*********** SOLES
                                util = pvSoles - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvSoles - pc) / pc;
                                }

                            }
                            if (IdMonedaPC == 2) {  //*********** DOLARES
                                util = pvDolares - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvDolares - pc) / pc;
                                }
                            }


                            if (IdMonedaPC > 2) {  //*********** OTRA MONEDA
                                //usamos posicion 0 porque el label no renderiza
                                var CalOtraMoneda = parseFloat(rowElem.cells[12].children[0].value);
                                if (isNaN(CalOtraMoneda)) { CalOtraMoneda = 0; }

                                rowElem.cells[5].children[0].innerHTML = 'S/.';

                                util = pvSoles - CalOtraMoneda;

                                //*********** Validamos el PC
                                if (CalOtraMoneda == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvSoles - CalOtraMoneda) / CalOtraMoneda;
                                }
                            }

                            //************ IMPRIMIMOS

                            rowElem.cells[4].children[0].value = redondear(porcentUtil * 100, 2);
                            rowElem.cells[5].children[1].value = redondear(util, 2);
                            rowElem.cells[7].children[0].value = redondear(pvSoles, 2);
                            break;
                        case '3':  //************* Pv Final SOLES
                            pvDolares = pvSoles / tipocambio;
                            if (IdMonedaPC == 1) {  //*********** SOLES
                                util = pvSoles - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvSoles - pc) / pc;
                                }

                            }
                            if (IdMonedaPC == 2) {  //*********** DOLARES
                                util = pvDolares - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvDolares - pc) / pc;
                                }


                            }
                            if (IdMonedaPC > 2) {  //*********** OTRA MONEDA
                                //usamos posicion 0 porque el label no renderiza
                                var CalOtraMoneda = parseFloat(rowElem.cells[12].children[0].value);
                                if (isNaN(CalOtraMoneda)) { CalOtraMoneda = 0; }

                                rowElem.cells[5].children[0].innerHTML = 'S/.';

                                util = pvSoles - CalOtraMoneda;

                                //*********** Validamos el PC
                                if (CalOtraMoneda == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvSoles - CalOtraMoneda) / CalOtraMoneda;
                                }


                            }
                            //************ IMPRIMIMOS

                            rowElem.cells[4].children[0].value = redondear(porcentUtil * 100, 2);
                            rowElem.cells[5].children[1].value = redondear(util, 2);
                            rowElem.cells[6].children[0].value = redondear(pvDolares, 2);

                            break;
                        case '4':
                            //************* NO se realiza ningun cambio
                            break;
                    }
                    if (recorrerAll == '0') {
                        //********** si no es ONLOAD no debo recorrer toda la grilla
                        return false;
                    }
                }
            }


            return false;
        }

        function calcularPrecioVenta_SL_UMO(e,opcion, recorrerAll) {
            // Recorrer All
            // 0: FALSE
            // 1: TRUE

            // OPcion:
            // 0: Util (%)
            // 1: Utilidad
            // 2: PV Final $
            // 3: PV Final S/.
            // 4: COMBO MONEDA destino
            // 5: ONLOAD
            // 6: Porcent PV PUBLICO
            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;


            var grilla = document.getElementById('<%=DGV_SL_UMO.ClientID%>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }

            //************ declaramos las variables
            var tipocambio = parseFloat(document.getElementById('<%=txtTipoCambio.ClientID%>').value);

            var IdMonedaPV = 0;
            var pvSoles = 0;
            var pvDolares = 0;

            var IdMonedaPC = 0;
            var opcionOriginal = opcion;

            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                var controlId = '';
                alert(e);
                if (e != null) {
                    if (event_element != null) {
                        controlId = event_element.id;
                    }
                }


                if (rowElem.cells[4].children[0].id == controlId || rowElem.cells[5].children[0].id == controlId || rowElem.cells[6].children[0].id == controlId || controlId == '' || recorrerAll == '1') {

                    IdMonedaPV = parseFloat(rowElem.cells[6].children[0].value);
                    if (!esDecimal(IdMonedaPV)) { IdMonedaPV = 0; }
                    pvDolares = parseFloat(rowElem.cells[4].children[0].value);
                    if (!esDecimal(pvDolares)) { pvDolares = 0; }
                    pvSoles = parseFloat(rowElem.cells[5].children[0].value);
                    if (!esDecimal(pvSoles)) { pvSoles = 0; }


                    if (tipocambio == 0) {
                        alert('El Tipo de Cambio debe ser mayor a cero');
                        return false;
                    }

                    if (controlId == '' && opcionOriginal == '5') {
                        if (IdMonedaPV == 1) {  //********* soles
                            opcion = '3';
                        }
                        if (IdMonedaPV == 2) {  //********* dolares
                            opcion = '2';
                        }
                    }


                    if (opcionOriginal == '6') { //******** x Porcent PV Publico

                        var IdMonedaPVPublico = parseInt(rowElem.cells[6].children[0].value);

                        if (IdMonedaPVPublico == 1) {
                            opcion = '3';
                        } else {
                            opcion = '2';
                        }


                    }

                    switch (opcion) {

                        case '2': //************* PV Final Dolares
                            pvSoles = pvDolares * tipocambio;

                            //************ IMPRIMIMOS



                            rowElem.cells[5].children[0].value = redondear(pvSoles, 2);
                            break;
                        case '3':  //************* Pv Final SOLES
                            pvDolares = pvSoles / tipocambio;

                            //************ IMPRIMIMOS


                            rowElem.cells[4].children[0].value = redondear(pvDolares, 2);

                            break;
                        case '4':
                            //************* NO se realiza ningun cambio
                            break;
                    }
                    if (recorrerAll == '0') {
                        //********** si no es ONLOAD no debo recorrer toda la grilla
                        return false;
                    }
                }
            }


            return false;
        }


        function calcularPrecioVenta_SL_UMR(e,opcion, recorrerAll) {
            //*********** OPcion:
            // 0: Util (%)
            // 1: Utilidad
            // 2: PV Final        
            // 3: ONLOAD 

            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;

            var grilla = document.getElementById('<%=DGV_SL_UMR.ClientID%>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }

            //************ declaramos las variables
            var tipocambio = parseFloat(document.getElementById('<%=txtTipoCambio.ClientID%>').value);
            var porcentUtil = 0;
            var util = 0;
            var pc = 0;
            var IdMonedaPC = 0;
            var IdMonedaPV = 0;
            var pv = 0;
            var equiv = 0;
            var porcentRetazo = 0;

            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                var controlId = '';
                if (event_element != null) {
                    controlId = event_element.id;
                }

                if (rowElem.cells[8].children[0].id == controlId || rowElem.cells[9].children[1].id == controlId || rowElem.cells[10].children[2].id == controlId || controlId == '' || controlId == document.getElementById('<%=btnAceptar_SL_UMR.ClientID%>').id) {
                    porcentUtil = parseFloat(rowElem.cells[8].children[0].value);
                    if (!esDecimal(porcentUtil)) { porcentUtil = 0; }
                    util = parseFloat(rowElem.cells[9].children[1].value);
                    if (!esDecimal(util)) { util = 0; }
                    IdMonedaPC = parseFloat(rowElem.cells[3].children[0].value);
                    if (!esDecimal(IdMonedaPC)) { IdMonedaPC = 0; }
                    pc = parseFloat(rowElem.cells[3].children[2].innerHTML);
                    if (!esDecimal(pc)) { pc = 0; }
                    IdMonedaPV = parseFloat(rowElem.cells[10].children[0].value);
                    if (!esDecimal(IdMonedaPV)) { IdMonedaPV = 0; }
                    pv = parseFloat(rowElem.cells[10].children[2].value);
                    if (!esDecimal(pv)) { pv = 0; }
                    porcentRetazo = parseFloat(rowElem.cells[7].innerHTML);
                    if (!esDecimal(porcentRetazo)) { porcentRetazo = 0; }
                    equiv = parseFloat(rowElem.cells[5].innerHTML);
                    if (!esDecimal(equiv)) { equiv = 0; }


                    if (IdMonedaPC == 0) {
                        alert('El Producto ' + rowElem.cells[0].innerHTML + ' - ' + rowElem.cells[1].innerHTML + ' no posee una Moneda de Precio de Compra, esto producir� error en la operaci�n.');
                        return false;
                    }
                    if (IdMonedaPV == 0) {
                        alert('El Producto ' + rowElem.cells[0].innerHTML + ' - ' + rowElem.cells[1].innerHTML + ' no posee una Moneda de Venta para la Unidad de Medida Principal, esto producir� error en la operaci�n.');
                        return false;
                    }
                    if (tipocambio == 0) {
                        alert('El Tipo de Cambio debe ser mayor a cero');
                        return false;
                    }

                    switch (opcion) {
                        case '0':  //*************UTIL (%)

                            pc = pc * equiv;
                            pv = pc * (1 + porcentUtil / 100);

                            util = pv - pc;

                            switch (IdMonedaPV) {
                                case 1:  //****** SOLES
                                    if (IdMonedaPC == 2) {
                                        pv = pv * tipocambio;
                                    }
                                    break;
                                case 2:  //****  DOLARES
                                    if (IdMonedaPC == 1) {
                                        pv = pv / tipocambio;
                                    }
                                    break;
                            }
                            //************ IMPRIMIMOS                        
                            rowElem.cells[9].children[1].value = redondear(util, 2);
                            rowElem.cells[10].children[2].value = redondear(pv, 2);
                            break;
                        case '1':  //************ UTILIDAD
                            pc = pc * equiv;
                            pv = pc + util;

                            switch (IdMonedaPV) {
                                case 1:  //****** SOLES
                                    if (IdMonedaPC == 2) {
                                        pv = pv * tipocambio;
                                    }
                                    break;
                                case 2:  //****  DOLARES
                                    if (IdMonedaPC == 1) {
                                        pv = pv / tipocambio;
                                    }
                                    break;
                            }

                            if (pc == 0) {
                                porcentUtil = 100;
                            } else {
                                porcentUtil = (util / pc) * 100;
                            }
                            //************ IMPRIMIMOS

                            rowElem.cells[8].children[0].value = redondear(porcentUtil, 2);
                            rowElem.cells[10].children[2].value = redondear(pv, 2);
                            break;

                        case '2': //************* PV Final 
                            switch (IdMonedaPV) {
                                case 1:  //***** Soles

                                    if (IdMonedaPC == 2) {
                                        pv = pv / tipocambio;
                                    }

                                    break;
                                case 2: //****** Dolares
                                    if (IdMonedaPC == 1) {
                                        pv = pv * tipocambio;
                                    }
                                    break;
                            }
                            util = pv - pc * equiv;
                            if (pc == 0) {
                                porcentUtil = 100;
                            } else {
                                porcentUtil = (util / (pc * equiv)) * 100;
                            }

                            //****** Imprimimos
                            rowElem.cells[8].children[0].value = redondear(porcentUtil, 2);
                            rowElem.cells[9].children[1].value = redondear(util, 2);
                    }
                    if (recorrerAll == '0') {
                        //********** si no es ONLOAD no debo recorrer toda la grilla
                        return false;
                    }
                }
            }


            return false;
        }

        function onLoad_calcularPrecioVenta_SL_UMR() {
            calcularPrecioVenta_SL_UMR('event','2', '1');
        }

        function completarPorcentUtil_SL_UMR() {
            var caja = document.getElementById('<%=txtPorcent_SL_UMR.ClientID%>');
            if (CajaEnBlanco(caja) || !esDecimal(caja.value)) {
                alert('Ingrese un valor');
                caja.select();
                caja.focus();
                return false;
            }
            if (!confirm('Los Valores se calcular�n de acuerdo al Porcentaje de Utilidad ingesado. Desea continuar con la operaci�n?')) {
                return false;
            }



            var grilla = document.getElementById('<%=DGV_SL_UMR.ClientID%>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];

                if (rowElem.cells[12].children[0].firstChild.status == false) {                    
                    rowElem.cells[8].children[0].value = redondear(parseFloat(caja.value), 2);
                }


            }
            calcularPrecioVenta_SL_UMR('event','0', '1');
            return false;
        }        

        function calcularPrecioVenta_PR(e , opcion, recorrerAll, Externo) {
            //*********** OPcion:
            // 0: Util (%)
            // 1: Utilidad
            // 2: PV Final $
            // 3: PV Final S/.
            // 4: COMBO MONEDA destino
            // 5: ONLOAD
            var e = e ? e : window.event;
            var event_element = e.target ? e.target : e.srcElement;

            var grilla = document.getElementById('<%=DGV_PRODUCTO.ClientID%>');
            if (grilla == null) {
                return false;
            }
            //************ declaramos las variables
            var tipocambio = parseFloat(document.getElementById('<%=txtTipoCambio.ClientID%>').value);
            var tipocambioOtraMoneda = parseFloat(document.getElementById('<%=hddTipoCambio.ClientID%>').value);
            if (isNaN(tipocambioOtraMoneda)) { tipocambioOtraMoneda = 0; }
            var porcentUtil = 0;
            var util = 0;
            var pc = 0;

            var IdMonedaPC = 0;
            var IdMonedaPV = 0;
            var pvSoles = 0;
            var pvDolares = 0;
            var pvOtraMoneda = 0;
            var equivalencia = 0;

            IdMonedaPC = parseFloat(document.getElementById('<%=hddIdMoneda_PC.ClientID%>').value);
            if (!esDecimal(IdMonedaPC)) { IdMonedaPC = 0; }

            var txtProducto = document.getElementById('<%=txtProducto.ClientID%>');
            var txtCodigoPR = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            if (IdMonedaPC == 0) {
                alert('El Producto ' + txtCodigoPR.value + ' - ' + txtProducto.value + ' no posee una Moneda de Precio de Compra, esto producir� error en la operaci�n.');
                return false;
            }

            if (tipocambio == 0) {
                alert('El Tipo de Cambio debe ser mayor a cero');
                return false;
            }


            var opcionOriginal = opcion;

            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera

                var rowElem = grilla.rows[i];
                var controlId = '';  // Utilizado para el ONLOAD

                pc = parseFloat(document.getElementById('<%=txtPrecioCompra_prod.ClientID%>').value);
                if (!esDecimal(pc)) { pc = 0; }

                if (e != null) {
                    if (event_element != null) {
                        controlId = event_element.id;
                    }
                }

                if (Externo == 1) { var controlId = ''; }
                if (isNaN(Externo)) { document.getElementById('<%=ckProducto.ClientID%>').checked = false; }

                if (rowElem.cells[6].children[0].id == controlId || rowElem.cells[7].children[1].id == controlId || rowElem.cells[8].children[0].id == controlId || rowElem.cells[9].children[0].id == controlId || rowElem.cells[10].children[0].id == controlId || controlId == '') {

                    equivalencia = parseFloat(rowElem.cells[2].innerHTML);
                    if (!esDecimal(equivalencia)) { equivalencia = 0; }

                    pc = pc * equivalencia;


                    porcentUtil = parseFloat(rowElem.cells[6].children[0].value);
                    if (!esDecimal(porcentUtil)) { porcentUtil = 0; }
                    util = parseFloat(rowElem.cells[7].children[1].value);
                    if (!esDecimal(util)) { util = 0; }
                    IdMonedaPV = parseFloat(rowElem.cells[10].children[0].value);
                    if (!esDecimal(IdMonedaPV)) { IdMonedaPV = 0; }
                    pvDolares = parseFloat(rowElem.cells[8].children[0].value);
                    if (!esDecimal(pvDolares)) { pvDolares = 0; }
                    pvSoles = parseFloat(rowElem.cells[9].children[0].value);
                    if (!esDecimal(pvSoles)) { pvSoles = 0; }

                    if (controlId == '' && opcionOriginal == '5') {
                        if (IdMonedaPV == 1) {  //********* soles
                            opcion = '3';
                        }
                        if (IdMonedaPV == 2) {  //********* dolares
                            opcion = '2';
                        }
                    }

                    switch (opcion) {
                        case '0':  //*************UTIL (%)

                            if (IdMonedaPC == 1) {  //*********** SOLES

                                pvSoles = pc * (1 + porcentUtil / 100);
                                pvDolares = pvSoles / tipocambio;
                                util = pvSoles - pc;

                            }

                            if (IdMonedaPC == 2) {  //*********** DOLARES
                                pvDolares = pc * (1 + porcentUtil / 100);
                                pvSoles = pvDolares * tipocambio;
                                util = pvDolares - pc;
                            }

                            if (IdMonedaPC > 2) {  //*********** OTRA MOMEDA
                                pvOtraMoneda = pc * (1 + porcentUtil / 100);
                                //*********** Validamos el PC
                                pvSoles = pvOtraMoneda * tipocambioOtraMoneda;
                                pvDolares = pvSoles / tipocambio;
                                util = pvOtraMoneda - pc;
                            }

                            //************ IMPRIMIMOS


                            rowElem.cells[7].children[1].value = redondear(util, 2);
                            rowElem.cells[8].children[0].value = redondear(pvDolares, 2);
                            rowElem.cells[9].children[0].value = redondear(pvSoles, 2);
                            break;
                        case '1':  //************ UTILIDAD
                            if (IdMonedaPC == 1) {  //*********** SOLES
                                pvSoles = pc + util;
                                pvDolares = pvSoles / tipocambio;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvSoles - pc) / pc;
                                }

                            }
                            if (IdMonedaPC == 2) {  //*********** DOLARES
                                pvDolares = pc + util;
                                pvSoles = pvDolares * tipocambio;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvDolares - pc) / pc;
                                }

                            }
                            if (IdMonedaPC > 2) {  //*********** OTRA MOMEDA
                                pvOtraMoneda = pc + util;
                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvOtraMoneda - pc) / pc;
                                }

                                pvSoles = pvOtraMoneda * tipocambioOtraMoneda;
                                pvDolares = pvSoles / tipocambio;
                                util = pvOtraMoneda - pc;
                            }

                            //************ IMPRIMIMOS

                            rowElem.cells[6].children[0].value = redondear(porcentUtil * 100, 2);
                            rowElem.cells[8].children[0].value = redondear(pvDolares, 2);
                            rowElem.cells[9].children[0].value = redondear(pvSoles, 2);
                            break;
                        case '2': //************* PV Final Dolares
                            pvSoles = pvDolares * tipocambio;

                            if (IdMonedaPC == 1) {  //*********** SOLES
                                util = pvSoles - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvSoles - pc) / pc;
                                }

                            }
                            if (IdMonedaPC == 2) {  //*********** DOLARES
                                util = pvDolares - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvDolares - pc) / pc;
                                }
                            }

                            if (IdMonedaPC > 2) {  //*********** OTRA MOMEDA
                                pvOtraMoneda = pvSoles / tipocambioOtraMoneda;
                                util = pvOtraMoneda - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvOtraMoneda - pc) / pc;
                                }
                            }


                            //************ IMPRIMIMOS

                            rowElem.cells[6].children[0].value = redondear(porcentUtil * 100, 2);
                            rowElem.cells[7].children[1].value = redondear(util, 2);
                            rowElem.cells[9].children[0].value = redondear(pvSoles, 2);
                            break;
                        case '3':  //************* Pv Final SOLES
                            pvDolares = pvSoles / tipocambio;
                            if (IdMonedaPC == 1) {  //*********** SOLES
                                util = pvSoles - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvSoles - pc) / pc;
                                }

                            }
                            if (IdMonedaPC == 2) {  //*********** DOLARES
                                util = pvDolares - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvDolares - pc) / pc;
                                }


                            }

                            if (IdMonedaPC > 2) {  //*********** OTRA MOMEDA
                                pvOtraMoneda = pvSoles / tipocambioOtraMoneda;
                                util = pvOtraMoneda - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvOtraMoneda - pc) / pc;
                                }
                            }


                            //************ IMPRIMIMOS

                            rowElem.cells[6].children[0].value = redondear(porcentUtil * 100, 2);
                            rowElem.cells[7].children[1].value = redondear(util, 2);
                            rowElem.cells[8].children[0].value = redondear(pvDolares, 2);

                            break;
                        case '4':
                            //************* NO se realiza ningun cambio
                            break;
                    }
                    if (recorrerAll == '0') {
                        //********** si no es ONLOAD no debo recorrer toda la grilla
                        return false;
                    }
                }
            }


            return false;
        }

        function calcularxPorcentPVPublico() {
            var caja = document.getElementById("<%=txtPorcentPVPublico.ClientID %>");
            if (!esDecimal(caja.value)) {
                alert('Valor no v�lido.');
                caja.select();
                caja.focus();
                return false;
            }
            if (!confirm('Desea cambiar el Precio de Venta Final seg�n el porcentaje adicional ingresado para el Precio Venta P�blico ?')) {
                return false;
            }
            var porcentPVPublico = parseFloat(caja.value);
            if (isNaN(porcentPVPublico)) { porcentPVPublico = 0; }
            var grilla = document.getElementById('<%=DGV_SL_UMP.ClientID %>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }
            /*
            Recorremos la grilla obteniendo el nuevo valor pv y lo reemplazo segun la moneda
            */
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera        
                var rowElem = grilla.rows[i];

                //****** Si es AFECTAR
                if (rowElem.cells[11].children[0].firstChild.status == false) {

                    var pvPublico = parseFloat(rowElem.cells[9].children[1].innerHTML);
                    var IdMonedaPublico = parseFloat(rowElem.cells[9].children[2].value);

                    var pvFinal = pvPublico * (1 + porcentPVPublico / 100);

                    //******* Pintamos los valores
                    switch (IdMonedaPublico) {
                        case 1: //****** Soles
                            rowElem.cells[7].children[0].value = redondear(pvFinal, 2);
                            break;
                        case 2: //****** Dolares
                            rowElem.cells[6].children[0].value = redondear(pvFinal, 2);
                            break;
                    }
                    rowElem.cells[8].children[0].value = IdMonedaPublico;
                    //****** Fin Pintar Valores


                } //******** Fin de la Operaci�n
            }
            calcularPrecioVenta_SL_UMP('event','6', '1');
            return false;
        }


        function calcularxPorcentPVPublicoUMO() {
            var caja = document.getElementById("<%=txtPorcentPVPublicoUMO.ClientID %>");
            if (!esDecimal(caja.value)) {
                alert('Valor no v�lido.');
                caja.select();
                caja.focus();
                return false;
            }

            if (!confirm('Desea cambiar el Precio de Venta Final seg�n el porcentaje adicional ingresado para el Precio Venta P�blico ?')) {
                return false;
            }


            var porcentPVPublico = parseFloat(caja.value);
            if (isNaN(porcentPVPublico)) { porcentPVPublico = 0; }
            var grilla = document.getElementById('<%=DGV_SL_UMO.ClientID %>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }
            /*
            Recorremos la grilla obteniendo el nuevo valor pv y lo reemplazo segun la moneda
            */
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera        
                var rowElem = grilla.rows[i];

                //****** Si es AFECTAR
                if (rowElem.cells[9].children[0].firstChild.status == false) {

                    var pvPublico = parseFloat(rowElem.cells[7].children[1].innerHTML);
                    var IdMonedaPublico = parseFloat(rowElem.cells[7].children[2].value);

                    var pvFinal = pvPublico * (1 + porcentPVPublico / 100);

                    //******* Pintamos los valores
                    switch (IdMonedaPublico) {
                        case 1: //****** Soles
                            rowElem.cells[5].children[0].value = redondear(pvFinal, 2);
                            break;
                        case 2: //****** Dolares
                            rowElem.cells[4].children[0].value = redondear(pvFinal, 2);
                            break;
                    }
                    rowElem.cells[6].children[0].value = IdMonedaPublico;
                    rowElem.cells[7].children[0].value = redondear(pvPublico, 2);
                    //****** Fin Pintar Valores


                } //******** Fin de la Operaci�n
            }
            calcularPrecioVenta_SL_UMO('event','6', '1');
            return false;
        }


        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        function valKeyPressCodigoSL(e) {
            //obtener caracter pulsado en todos los exploradores
            var evento = e || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(e) {
            //obtener caracter pulsado en todos los exploradores
            var evento = e || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }




        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto_Find.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        function valEquivalenciaxProducto() {
            if (document.getElementById('<%=ckProducto.ClientID%>').checked == true) {               

                var valor = 0;
                var grilla = document.getElementById('<%=DGV_PRODUCTO.ClientID%>');

                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[2].value == 'True') {
                        valor = parseFloat(rowElem.cells[9].children[0].value);
                        if (isNaN(valor)) { valor = 0; }
                        break;
                    }
                } // end for

                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    if (rowElem.cells[1].children[2].value == 'False' && valor > 0) {
                        var Equivalencia = parseFloat(rowElem.cells[2].innerHTML);
                        if (isNaN(Equivalencia)) { Equivalencia = 0; }
                        rowElem.cells[9].children[0].value = redondear((valor * Equivalencia), 2);
                        calcularPrecioVenta_PR('event','3', '1', '1');
                    }

                } // end for

            } // end if



            return true;
        }

        
    </script>

</asp:Content>
