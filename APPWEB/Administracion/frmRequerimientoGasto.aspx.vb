﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmRequerimientoGasto
    Inherits System.Web.UI.Page

#Region "variables"
    Private objscript As New ScriptManagerClass
    Private drop As Combo
    Private fecha As Negocio.FechaActual
    Private list_RequerimientoGasto As List(Of Entidades.RequerimientoGasto)
    Private obj_RequerimientoGasto As Entidades.RequerimientoGasto
    Private list_Moneda As List(Of Entidades.Moneda)
    Private obj_Mantenimiento As Entidades.RequerimientoGasto.CabeceraRequerimiento
    Private obj_observacion As Entidades.Observacion
    Private Util As Negocio.Util
    Private Enum operativo
        Ninguno = 0
        GuardarNuevo = 1
        Actualizar = 2
        Buscar = 3
    End Enum
#End Region

#Region "procedimientos y funciones"
    Private Sub ValidarPermisos(ByVal IdUsuario As Integer)

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(IdUsuario, New Integer() {111, 112, 113, 114, 115, 116})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnuevo.Enabled = True
            Me.btguardar.Enabled = True
        Else
            Me.btnuevo.Enabled = False
            Me.btguardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.bteditar.Enabled = True
        Else
            Me.bteditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btanular.Enabled = True
        Else
            Me.btanular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* Consultar
            Me.btbuscar.Enabled = True
        Else
            Me.btbuscar.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* AUTORIZAR (jefe Inmediato)
            Me.panelsupervisor.Visible = True
        Else
            Me.panelsupervisor.Visible = False
        End If

        If listaPermisos(5) > 0 Then  '********* Generar Req. Gasto
            Me.btgenerar.Visible = True
        Else
            Me.btgenerar.Visible = False
        End If

    End Sub
    Private Sub Mantenimiento(ByVal tipo As Integer)
        list_RequerimientoGasto = getListaRequerimientoGasto()

        obj_Mantenimiento = New Entidades.RequerimientoGasto.CabeceraRequerimiento
        With obj_Mantenimiento
            .Id = CInt(hdd_iddocumento.Value)
            .IdEmpresa = CInt(dlempresa.SelectedValue)
            .IdTienda = CInt(dltienda.SelectedValue)
            .IdTipoDocumento = 36 'Requerimiento de gasto
            .IdSerie = CInt(dlserie.SelectedValue)
            .IdTipoOperacion = CInt(dloperacion.SelectedValue)
            .FechaEmision = CDate(tbfechaemision.Text)
            Dim idarea() As String = dlarea.SelectedValue.Split(CChar(","))
            .IdArea = CInt(idarea(0))
            .IdMedioPagoCredito = CInt(dlmediopago.SelectedValue)
            .IdUsuario = CInt(hdd_idusuario.Value)
            .IdPersona = CInt(hdd_idpersona.Value)
            .anex_Aprobar = CBool(CInt(rbsupervisor.SelectedValue))

            If panelsupervisor.Visible = True Then
                .IdUsuarioSupervisor = CInt(hdd_idusuario.Value)
            End If

            .IdMoneda = CInt(dlmoneda.SelectedValue)
            .TotalAPagar = CDec(tbtotal.Text)
            '.IdCentroCosto = getCentroCosto()
            .EnviarTablaCuenta = ckenviarcuenta.Checked
        End With

        obj_observacion = New Entidades.Observacion
        obj_observacion.Id = CInt(hdd_idObservacion.Value)
        obj_observacion.Observacion = CStr(tbobservaciones.Text)

        Dim cad() As String
        Select Case tipo

            Case operativo.GuardarNuevo
                cad = (New Negocio.RequerimientoGasto).InsertRequerimientoGasto(obj_Mantenimiento, list_RequerimientoGasto, obj_observacion).Split(CChar(","))
                hdd_iddocumento.Value = cad(0)
                tbcodigo.Text = cad(1)
                objscript.mostrarMsjAlerta(Me, "El documento ha sido guardado")
            Case operativo.Actualizar
                cad = (New Negocio.RequerimientoGasto).UpdateRequerimientoGasto(obj_Mantenimiento, list_RequerimientoGasto, obj_observacion).Split(CChar(","))
                tbcodigo.Text = cad(0)
                tbfechaaprobacion.Text = cad(1)
                objscript.mostrarMsjAlerta(Me, "El documento ha sido actualizado")
        End Select

        MostrarBotones("Load")
        btimprimir.Visible = True

    End Sub
    Private Sub cargarSerie(ByVal cb As DropDownList)
        LimpiarCabeceraDoc()
        drop = New Combo
        drop.LLenarCboSeriexIdsEmpTienTipoDoc(cb, CInt(dlempresa.SelectedValue), _
                                              CInt(dltienda.SelectedValue), 36)
    End Sub
    Private Sub generarNroDoc(ByVal tb As TextBox)
        If dlserie.Items.Count = 0 Then
            tb.Text = String.Empty
            Exit Sub
        End If
        tb.Text = (New Negocio.Serie).GenerarCodigo(CInt(dlserie.SelectedValue))
    End Sub
    Private Function getDropEmpresaxIdUsuario() As List(Of Entidades.Propietario)
        Dim listaEmpresa As New List(Of Entidades.Propietario)
        With dlempresa
            For x As Integer = 0 To .Items.Count - 1
                listaEmpresa.Insert(x, New Entidades.Propietario(CInt(.Items(x).Value), .Items(x).Text))
            Next
        End With
        Return listaEmpresa
    End Function
    Private Function getDropAreaxIdUsuario() As List(Of Entidades.Area)
        Dim ListaArea As New List(Of Entidades.Area)
        With dlarea
            For x As Integer = 0 To .Items.Count - 1
                Dim obj As New Entidades.Area
                obj.IdCompuesto = .Items(x).Value
                obj.DescripcionCorta = .Items(x).Text
                ListaArea.Add(obj)
            Next
        End With
        Return ListaArea
    End Function
    Private Function getDropTiendaxIdUsuario() As List(Of Entidades.Tienda)
        Dim Listatienda As New List(Of Entidades.Tienda)
        With dltienda
            For x As Integer = 0 To .Items.Count - 1
                Listatienda.Insert(x, New Entidades.Tienda(CInt(.Items(x).Value), .Items(x).Text))
            Next
        End With
        Return Listatienda
    End Function

    Private Sub CargarAlIniciar()
        drop = New Combo
        drop.LlenarCboEmpresaxIdUsuario(dlempresa, CInt(hdd_idusuario.Value), False)
        drop.LlenarCboTiendaxIdEmpresaxIdUsuario(dltienda, CInt(dlempresa.SelectedValue), CInt(hdd_idusuario.Value), False)
        drop.LLenarCboAreaxIdtiendaxIdUsuarioxSoloCentroCosto(dlarea, CInt(dltienda.SelectedValue), CInt(hdd_idusuario.Value), True)
        drop.llenarCboTipoOperacionxIdTpoDocumento(dloperacion, 36, False)
        drop.LlenarCboMoneda(dlmoneda, False)
        drop.LLenarCboEstadoDocumento(dlestado)
        drop.LlenarCboMedioPagoxIdTipoDocumento(dlmediopago, 36, False)
        drop.LLenarCboEstadoCancelacion(dlestadocancelacion, False)
        cargarSerie(dlserie)
        generarNroDoc(tbcodigo)
        fecha = New Negocio.FechaActual
        tbfechaemision.Text = FormatDateTime(fecha.SelectFechaActual, DateFormat.ShortDate)
    End Sub
    Private Sub MostrarBotones(ByVal modo As String)
        Select Case modo
            Case "Load"
                btnuevo.Visible = True
                btbuscar.Visible = True
                bteditar.Visible = False
                btcancelar.Visible = False
                btguardar.Visible = False
                btanular.Visible = False
                btimprimir.Visible = False
                btBuscarDocumento.Visible = False
                pnlPrincipal.Enabled = False

            Case "Nuevo"
                btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = False
                btcancelar.Visible = True
                btguardar.Visible = True
                btanular.Visible = False
                btimprimir.Visible = False
                btBuscarDocumento.Visible = False
                pnlPrincipal.Enabled = True
                HabilitarControles(True)

            Case "Buscar"
                btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = False
                btcancelar.Visible = True
                btguardar.Visible = False
                btanular.Visible = False
                btimprimir.Visible = False
                btBuscarDocumento.Visible = True
                pnlPrincipal.Enabled = True

            Case "DocumentoEncontrado"
                btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = True
                btcancelar.Visible = True
                btguardar.Visible = False
                btanular.Visible = False
                btimprimir.Visible = True
                btBuscarDocumento.Visible = False
                pnlPrincipal.Enabled = False
                HabilitarControles(False)

            Case "Editar"
                btnuevo.Visible = False
                btbuscar.Visible = False
                bteditar.Visible = False
                btcancelar.Visible = True
                btguardar.Visible = True
                btanular.Visible = True
                btimprimir.Visible = False
                btBuscarDocumento.Visible = False
                pnlPrincipal.Enabled = True

        End Select
    End Sub
    Private Sub LimpiarForm()
        hdd_iddocumento.Value = "0"
        hdd_idpersona.Value = "0"
        hdd_idObservacion.Value = "0"
        hdd_operativo.Value = CStr(operativo.Ninguno)

        tbobservaciones.Text = String.Empty
        tbtotal.Text = "0"
        tbfechaaprobacion.Text = String.Empty
        tbbeneficiario.Text = String.Empty
        tbdni.Text = String.Empty
        tbruc.Text = String.Empty
        dlestado.SelectedValue = "1"
        gvdetalle.DataBind()
        dlarea.SelectedIndex = 0
        rbsupervisor.SelectedValue = "0"
        hdd_IdDocRelacionado.Value = "0"
        dlestadocancelacion.SelectedValue = "1" 'por pagar
    End Sub
    Private Sub LimpiarCabeceraDoc()
        dlserie.Items.Clear()
        tbcodigo.Text = String.Empty
    End Sub
    Private Sub HabilitarControles(ByVal valor As Boolean)
        dlempresa.Enabled = valor
        dltienda.Enabled = valor
        dlserie.Enabled = valor
    End Sub

    Private Sub cargarDatosDeBusqueda()
        PanelForm.Visible = False
        Panelbusqueda.Visible = True
        dlempresabusqueda.DataSource = getDropEmpresaxIdUsuario()
        dlempresabusqueda.DataBind()
        dltiendabusqueda.DataSource = getDropTiendaxIdUsuario()
        dltiendabusqueda.DataBind()
        dlareabusqueda.DataSource = getDropAreaxIdUsuario()
        dlareabusqueda.DataBind()
    End Sub

#End Region

#Region "Cabecera del documento"

    Private Sub dlempresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlempresa.SelectedIndexChanged
        Empresa_Select()
    End Sub
    Private Sub Empresa_Select()
        drop = New Combo
        drop.LlenarCboTiendaxIdEmpresaxIdUsuario(dltienda, CInt(dlempresa.SelectedValue), CInt(Session("IdUsuario")), False)
        cargarSerie(dlserie)
        generarNroDoc(tbcodigo)
    End Sub

    Private Sub dltienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dltienda.SelectedIndexChanged
        Tienda_Select()
    End Sub
    Private Sub Tienda_Select()
        drop = New Combo
        drop.LLenarCboAreaxIdtiendaxIdUsuarioxSoloCentroCosto(dlarea, CInt(dltienda.SelectedValue), CInt(hdd_idusuario.Value), True)
        cargarSerie(dlserie)
        generarNroDoc(tbcodigo)
    End Sub

    Private Sub dlserie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlserie.SelectedIndexChanged
        generarNroDoc(tbcodigo)
    End Sub
#End Region

#Region "Eventos Principales"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hdd_idusuario.Value = CStr(Session("IdUsuario"))
            CargarAlIniciar()
            hdd_operativo.Value = CStr(operativo.Ninguno)
            MostrarBotones("Load")
            ' dlunidadnegocio_SelectedIndexChanged(sender, e)
            ValidarPermisos(CInt(Session("IdUsuario")))
            If Request.QueryString("operacion") IsNot Nothing Then
                cargarDatosDeBusqueda()
            Else
                Panelbusqueda.Visible = False
            End If
        End If
    End Sub

    Private Sub btnuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnuevo.Click
        LimpiarForm()
        hdd_operativo.Value = CStr(operativo.GuardarNuevo)
        MostrarBotones("Nuevo")
    End Sub
    Private Sub btguardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btguardar.Click
        Try
            Me.Mantenimiento(CInt(hdd_operativo.Value))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btcancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btcancelar.Click
        LimpiarForm()
        MostrarBotones("Load")
        fecha = New Negocio.FechaActual
        Me.tbfechaemision.Text = Format(fecha.SelectFechaActual, "dd/MM/yyyy")
        'dlunidadnegocio.SelectedIndex = 0
        'dlunidadnegocio_SelectedIndexChanged(sender, e)
        cargarSerie(dlserie)
        generarNroDoc(tbcodigo)
    End Sub
    Private Sub btanular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btanular.Click
        Try
            If hdd_iddocumento.Value <> "0" Then
                Dim objOrdenCompra As New Negocio.OrdenCompra
                objOrdenCompra.AnularOrdenCompra(CInt(hdd_iddocumento.Value))
                dlestado.SelectedValue = "2"
                MostrarBotones("Load")
            End If
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub bteditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bteditar.Click
        hdd_operativo.Value = CStr(operativo.Actualizar)
        MostrarBotones("Editar")
    End Sub
    Private Sub btbuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btbuscar.Click
        LimpiarForm()
        cargarSerie(dlserie)
        hdd_operativo.Value = CStr(operativo.Buscar)
        MostrarBotones("Buscar")
        tbcodigo.Focus()
    End Sub
    Private Sub btBuscarDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarDocumento.Click
        Try
            BuscarDocumento(CInt(dlserie.SelectedValue), CInt(tbcodigo.Text))
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub BuscarDocumento(ByVal idserie As Integer, ByVal codigo As Integer)
        obj_Mantenimiento = (New Negocio.RequerimientoGasto).selectRequerimientoGasto(idserie, codigo)
        hdd_operativo.Value = CStr(operativo.Ninguno)
        With obj_Mantenimiento
            hdd_iddocumento.Value = CStr(.Id)
            dlempresa.SelectedValue = CStr(.IdEmpresa)
            Empresa_Select()
            dltienda.SelectedValue = CStr(.IdTienda)
            Tienda_Select()
            dlserie.SelectedValue = CStr(.IdSerie)
            tbcodigo.Text = .Codigo
            dloperacion.SelectedValue = CStr(.IdTipoOperacion)
            tbfechaemision.Text = CStr(.FechaEmision)
            tbfechaaprobacion.Text = CStr(IIf(.FechaCancelacion = Nothing, "", .FechaCancelacion))
            rbsupervisor.SelectedValue = CStr(IIf(.anex_Aprobar = False, "0", "1"))

            For x As Integer = 0 To dlarea.Items.Count - 1
                Dim cad() As String = dlarea.Items(x).Value.Split(CChar(","))
                If cad(0) = CStr(.IdArea) Then
                    dlarea.SelectedIndex = x
                    Exit For
                End If
            Next

            dlmediopago.SelectedValue = CStr(.IdMedioPagoCredito)

            hdd_idpersona.Value = CStr(.IdPersona)
            tbbeneficiario.Text = CStr(.NombrePersona)
            tbruc.Text = CStr(.Ruc)
            tbdni.Text = CStr(.Dni)
            dlmoneda.SelectedValue = CStr(.IdMoneda)
            tbtotal.Text = FormatNumber(.TotalAPagar, 2)
            hdd_IdDocRelacionado.Value = CStr(.IdDocRelacionado)

            'If .IdCentroCosto <> "" Then
            '    dlunidadnegocio.SelectedValue = .IdCentroCosto.Substring(0, 2) : UnidadNegocio()
            '    dldptofuncional.SelectedValue = .IdCentroCosto.Substring(2, 2) : DptoFuncional()
            '    dlsubarea1.SelectedValue = .IdCentroCosto.Substring(4, 2) : subArea1()
            '    dlsubarea2.SelectedValue = .IdCentroCosto.Substring(6, 2) : subArea2()
            '    dlsubarea3.SelectedValue = .IdCentroCosto.Substring(8, 2)
            '    dehabilitarControlesCentroCosto()
            'Else
            '    dlunidadnegocio.SelectedIndex = 0
            '    dlunidadnegocio.Enabled = False
            '    dldptofuncional.Items.Clear() : dldptofuncional.Items.Insert(0, New ListItem("------", "00"))
            '    dlsubarea1.Items.Clear() : dlsubarea1.Items.Insert(0, New ListItem("------", "00"))
            '    dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
            '    dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
            'End If

            obj_observacion = (New Negocio.Observacion).SelectxIdDocumento(CInt(hdd_iddocumento.Value))
            hdd_idObservacion.Value = CStr(obj_observacion.Id)
            tbobservaciones.Text = obj_observacion.Observacion

            SetListaRequerimiento(.Id)

        End With

        MostrarBotones("DocumentoEncontrado")
    End Sub

    Private Sub btgenerar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btgenerar.Click
        If hdd_iddocumento.Value = "0" Then objscript.mostrarMsjAlerta(Me, "Realize la busqueda de un documento") : Exit Sub
        If rbsupervisor.SelectedValue = "0" Then objscript.mostrarMsjAlerta(Me, "El documento no esta aprobado") : Exit Sub
        Response.Redirect("~/Administracion/frmRegistrosGastos.aspx?serie=" + dlserie.SelectedValue + "&codigo=" + _
                          tbcodigo.Text, True)
    End Sub

#End Region

#Region "Busqueda de Personas"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                      ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                      ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As List(Of Entidades.PersonaView) = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objscript.onCapa(Me, "capabeneficiario")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capabeneficiario');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("rol", 0)  '******************** TODOS
            ViewState.Add("estado", 1) '******************* ACTIVO

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objscript = New ScriptManagerClass
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region


#Region "Area"
    Private Function ValidarUsuarioCentroCosto(ByVal idcompuesto As String) As Boolean
        Dim cad() As String = idcompuesto.Split(CChar(","))
        If cad(1) = "0" Then objscript.mostrarMsjAlerta(Me, "El usuario no tiene el permiso para seleccionar esta área") : Return False
        Return True
    End Function

    Private Sub dlarea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlarea.SelectedIndexChanged
        Try
            'dlunidadnegocio.SelectedIndex = 0
            'dlunidadnegocio.Enabled = False
            'dldptofuncional.Items.Clear() : dldptofuncional.Items.Insert(0, New ListItem("------", "00"))
            'dlsubarea1.Items.Clear() : dlsubarea1.Items.Insert(0, New ListItem("------", "00"))
            'dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
            'dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))

            If dlarea.SelectedIndex <> 0 Then
                If ValidarUsuarioCentroCosto(dlarea.SelectedValue) = False Then dlarea.SelectedIndex = 0 : Exit Sub
                'Dim cad() As String = dlarea.SelectedValue.Split(CChar(","))
                'If dlunidadnegocio.Items.Count > 0 Then
                '    dlunidadnegocio.SelectedValue = cad(2).Substring(0, 2) : dlunidadnegocio_SelectedIndexChanged(sender, e)
                '    dldptofuncional.SelectedValue = cad(2).Substring(2, 2) : dldptofuncional_SelectedIndexChanged(sender, e)
                '    dlsubarea1.SelectedValue = cad(2).Substring(4, 2) : dlsubarea1_SelectedIndexChanged(sender, e)
                '    dlsubarea2.SelectedValue = cad(2).Substring(6, 2) : dlsubarea2_SelectedIndexChanged(sender, e)
                '    dlsubarea3.SelectedValue = cad(2).Substring(8, 2)
                '    dehabilitarControlesCentroCosto()
                'End If
            End If

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region



#Region "Detalle de Requerimiento"

    Private Sub SetListaRequerimiento(ByVal idDocumento As Integer)
        list_RequerimientoGasto = (New Negocio.RequerimientoGasto).selectListaRequerimientoGasto(idDocumento)

        For x As Integer = 0 To list_RequerimientoGasto.Count - 1

            If x = 0 Then
                Dim Lista_Concepto As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(36)
                Lista_Concepto.Insert(0, New Entidades.Concepto(0, "------"))
                list_RequerimientoGasto(x).objConcepto = Lista_Concepto

                Dim Lista_Tipodocumento As List(Of Entidades.TipoDocumento) = (New Negocio.TipoDocumentoRef).SelectxIdtipoDocRef(36)
                Lista_Tipodocumento.Insert(0, New Entidades.TipoDocumento(0, "------"))
                list_RequerimientoGasto(x).objTipoDocumento = Lista_Tipodocumento

                list_Moneda = getListaMoneda()
                'list_Moneda.Insert(0, New Entidades.Moneda(0, "-------", "------"))
                list_RequerimientoGasto(x).objMoneda = list_Moneda
                list_RequerimientoGasto(x).IdMoneda = CInt(dlmoneda.SelectedValue)
            Else
                list_RequerimientoGasto(x).objConcepto = list_RequerimientoGasto(0).objConcepto
                list_RequerimientoGasto(x).objTipoDocumento = list_RequerimientoGasto(0).objTipoDocumento
                list_RequerimientoGasto(x).objMoneda = list_RequerimientoGasto(0).objMoneda
            End If

            If list_RequerimientoGasto(x).IdConcepto <> 0 Then
                Dim Lista_motivoGasto As List(Of Entidades.MotivoGasto) = (New Negocio.MotivoGasto).SelectxConceptoxMotivoGasto(list_RequerimientoGasto(x).IdConcepto)
                list_RequerimientoGasto(x).objMotivo = Lista_motivoGasto
            End If

        Next

        gvdetalle.DataSource = list_RequerimientoGasto
        gvdetalle.DataBind()
    End Sub
    Private Sub btagregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btagregar.Click
        Try
            llenarGrillaConcepto()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub llenarGrillaConcepto()
        list_RequerimientoGasto = getListaRequerimientoGasto()
        obj_RequerimientoGasto = New Entidades.RequerimientoGasto

        With obj_RequerimientoGasto
            .IdConcepto = 0
            If list_RequerimientoGasto.Count = 0 Then
                Dim Lista_Concepto As List(Of Entidades.Concepto) = (New Negocio.Concepto_TipoDocumento).SelectCboxIdTipoDocumento(36)
                Lista_Concepto.Insert(0, New Entidades.Concepto(0, "------"))
                .objConcepto = Lista_Concepto
                Dim Lista_Tipodocumento As List(Of Entidades.TipoDocumento) = (New Negocio.TipoDocumentoRef).SelectxIdtipoDocRef(36)
                Lista_Tipodocumento.Insert(0, New Entidades.TipoDocumento(0, "------"))
                .objTipoDocumento = Lista_Tipodocumento
                list_Moneda = getListaMoneda()
                'list_Moneda.Insert(0, New Entidades.Moneda(0, "-------", "------"))
                .objMoneda = list_Moneda
                .IdMoneda = CInt(dlmoneda.SelectedValue)
            Else
                .objConcepto = list_RequerimientoGasto(0).objConcepto
                .objTipoDocumento = list_RequerimientoGasto(0).objTipoDocumento
                .objMoneda = list_RequerimientoGasto(0).objMoneda
            End If
            .Descripcion = String.Empty
            .NroDocumento = String.Empty
            .Monto = Decimal.Zero
            .IdMoneda = CInt(dlmoneda.SelectedValue)
        End With

        list_RequerimientoGasto.Add(obj_RequerimientoGasto)

        gvdetalle.DataSource = list_RequerimientoGasto
        gvdetalle.DataBind()
    End Sub
    Private Function getListaRequerimientoGasto() As List(Of Entidades.RequerimientoGasto)
        list_RequerimientoGasto = New List(Of Entidades.RequerimientoGasto)


        For Each row As GridViewRow In Me.gvdetalle.Rows
            obj_RequerimientoGasto = New Entidades.RequerimientoGasto
            With obj_RequerimientoGasto
                Dim gdlconcepto As DropDownList = CType(row.Cells(1).FindControl("gdlconcepto"), DropDownList)
                Dim gdlmotivo As DropDownList = CType(row.Cells(2).FindControl("gdlmotivo"), DropDownList)
                .Descripcion = CType(row.Cells(3).FindControl("gtbdescripcion"), TextBox).Text
                Dim gdltipodocumento As DropDownList = CType(row.Cells(4).FindControl("gdltipodocumento"), DropDownList)
                .NroDocumento = CType(row.Cells(5).FindControl("gtbnrodocumento"), TextBox).Text
                Dim gdlmoneda As DropDownList = CType(row.Cells(6).FindControl("gdlmoneda"), DropDownList)
                .Monto = CDec(CType(row.Cells(7).FindControl("gtbmonto"), TextBox).Text)

                .IdConcepto = CInt(gdlconcepto.SelectedValue)
                .IdMoneda = CInt(gdlmoneda.SelectedValue)
                .IdMotivo = CInt(IIf(gdlmotivo.Items.Count = 0, 0, gdlmotivo.SelectedValue))
                .IdTipoDocumento = CInt(gdltipodocumento.SelectedValue)

                If row.RowIndex = 0 Then
                    Dim Lista_Concepto As New List(Of Entidades.Concepto)
                    For x As Integer = 0 To gdlconcepto.Items.Count - 1
                        Lista_Concepto.Insert(x, New Entidades.Concepto(CInt(gdlconcepto.Items(x).Value), gdlconcepto.Items(x).Text))
                    Next
                    .objConcepto = Lista_Concepto

                    Dim Lista_Tipodocumento As New List(Of Entidades.TipoDocumento)
                    For x As Integer = 0 To gdltipodocumento.Items.Count - 1
                        Lista_Tipodocumento.Insert(x, New Entidades.TipoDocumento(CInt(gdltipodocumento.Items(x).Value), gdltipodocumento.Items(x).Text))
                    Next
                    .objTipoDocumento = Lista_Tipodocumento
                    .objMoneda = getListaMoneda()
                Else
                    .objConcepto = list_RequerimientoGasto(0).objConcepto
                    .objTipoDocumento = list_RequerimientoGasto(0).objTipoDocumento
                    .objMoneda = list_RequerimientoGasto(0).objMoneda
                End If

                Dim Lista_motivoGasto As New List(Of Entidades.MotivoGasto)
                For x As Integer = 0 To gdlmotivo.Items.Count - 1
                    Lista_motivoGasto.Insert(x, New Entidades.MotivoGasto(CInt(gdlmotivo.Items(x).Value), gdlmotivo.Items(x).Text))
                Next
                .objMotivo = Lista_motivoGasto
            End With
            list_RequerimientoGasto.Add(obj_RequerimientoGasto)
        Next

        Return list_RequerimientoGasto
    End Function
    Protected Sub gdlconcepto_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim gdlconcepto As DropDownList = CType(sender, DropDownList)
        Dim fila As GridViewRow = CType(gdlconcepto.NamingContainer, GridViewRow)
        Dim gdlmotivo As DropDownList = CType(fila.Cells(2).FindControl("gdlmotivo"), DropDownList)
        gdlmotivo.Items.Clear()
        Dim Lista_motivoGasto As List(Of Entidades.MotivoGasto) = (New Negocio.MotivoGasto).SelectxConceptoxMotivoGasto(CInt(gdlconcepto.SelectedValue))
        Lista_motivoGasto.Insert(0, New Entidades.MotivoGasto(0, "------"))
        For i As Integer = 0 To Lista_motivoGasto.Count - 1
            With gdlmotivo.Items
                .Insert(i, New ListItem(Lista_motivoGasto.Item(i).Nombre_MG, CStr(Lista_motivoGasto.Item(i).Id)))
            End With
        Next

    End Sub
    Private Sub gvdetalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvdetalle.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gdlconcepto As DropDownList = CType(e.Row.Cells(1).FindControl("gdlconcepto"), DropDownList)
            Dim gdlmotivo As DropDownList = CType(e.Row.Cells(2).FindControl("gdlmotivo"), DropDownList)
            Dim gdltipodocumento As DropDownList = CType(e.Row.Cells(4).FindControl("gdltipodocumento"), DropDownList)
            Dim gdlmoneda As DropDownList = CType(e.Row.Cells(6).FindControl("gdlmoneda"), DropDownList)

            If list_RequerimientoGasto(e.Row.RowIndex).IdConcepto <> 0 Then
                gdlconcepto.SelectedValue = CStr(list_RequerimientoGasto(e.Row.RowIndex).IdConcepto)
            End If

            If list_RequerimientoGasto(e.Row.RowIndex).IdMotivo <> 0 And gdlmotivo.Items.Count > 0 Then
                gdlmotivo.SelectedValue = CStr(list_RequerimientoGasto(e.Row.RowIndex).IdMotivo)
            End If

            If list_RequerimientoGasto(e.Row.RowIndex).IdMoneda <> 0 Then
                gdlmoneda.SelectedValue = dlmoneda.SelectedValue
            End If

            If list_RequerimientoGasto(e.Row.RowIndex).IdTipoDocumento <> 0 Then
                gdltipodocumento.SelectedValue = CStr(list_RequerimientoGasto(e.Row.RowIndex).IdTipoDocumento)
            End If

        End If
    End Sub
    Private Sub gvdetalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvdetalle.SelectedIndexChanged
        list_RequerimientoGasto = getListaRequerimientoGasto()
        list_RequerimientoGasto.RemoveAt(gvdetalle.SelectedIndex)
        gvdetalle.DataSource = list_RequerimientoGasto
        gvdetalle.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "javascript:CalcularMonto();", True)
    End Sub
    Public Function getListaMoneda() As List(Of Entidades.Moneda)
        list_Moneda = New List(Of Entidades.Moneda)
        With dlmoneda
            For x As Integer = 0 To Items.Count
                list_Moneda.Insert(x, New Entidades.Moneda(CInt(.Items(x).Value), "", .Items(x).Text))
            Next
        End With
        Return list_Moneda
    End Function

#End Region



    Private Sub dlmoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlmoneda.SelectedIndexChanged
        Util = New Negocio.Util
        list_RequerimientoGasto = getListaRequerimientoGasto()
        For x As Integer = 0 To list_RequerimientoGasto.Count - 1
            Dim valor As Decimal = CDec(IIf(list_RequerimientoGasto(x).Monto = Nothing, 0, list_RequerimientoGasto(x).Monto))
            Dim monto As Decimal = Util.SelectValorxIdMonedaOxIdMonedaD(list_RequerimientoGasto(x).IdMoneda, CInt(dlmoneda.SelectedValue), valor, "E")
            list_RequerimientoGasto(x).IdMoneda = CInt(dlmoneda.SelectedValue)
            list_RequerimientoGasto(x).Monto = monto
        Next
        gvdetalle.DataSource = list_RequerimientoGasto
        gvdetalle.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "javascript:CalcularMonto();", True)

    End Sub

    Private Sub gvbusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvbusqueda.SelectedIndexChanged
        Try
            BuscarDocumento(CInt(CType(gvbusqueda.SelectedRow.Cells(1).FindControl("ghdd_idserie"), HiddenField).Value), CInt(gvbusqueda.SelectedRow.Cells(3).Text))
            Panelbusqueda.Visible = False
            PanelForm.Visible = True
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub imgBuscarAprobacion_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBuscarAprobacion.Click
        Try
            Dim idarea() As String = dlareabusqueda.SelectedValue.Split(CChar(","))

            Dim Lista As List(Of Entidades.RequerimientoGasto.CabeceraRequerimiento) = (New Negocio.RequerimientoGasto).RequerimientoGasto_Select_Aprobar( _
                            CInt(dlempresabusqueda.SelectedValue), CInt(dltiendabusqueda.SelectedValue), CInt(idarea(0)), ckAutorizadas.Checked)
            If Lista.Count = 0 Then
                objscript.mostrarMsjAlerta(Me, "No se hallaron registros") : Exit Sub
            End If
            gvbusqueda.DataSource = Lista
            gvbusqueda.DataBind()
        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function ValidarUsuarioCentroCostoBusqueda(ByVal idcompuesto As String) As Boolean
        Dim cad() As String = idcompuesto.Split(CChar(","))
        If cad(1) = "0" Then objscript.mostrarMsjAlerta(Me, "El usuario no tiene el permiso para seleccionar esta área") : Return False
        Return True
    End Function

    Private Sub dlareabusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dlareabusqueda.SelectedIndexChanged
        Try
            
            If dlareabusqueda.SelectedIndex <> 0 Then
                If ValidarUsuarioCentroCostoBusqueda(dlareabusqueda.SelectedValue) = False Then dlareabusqueda.SelectedIndex = 0 : Exit Sub
            End If

        Catch ex As Exception
            objscript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class