﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmReplicaPreciosVenta
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Private IdUsuario As Integer = 0
    Dim cbo As Combo

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            inicializarFrm()
        End If
    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo
            With objCbo

                .llenarCboTipoExistencia(CboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(cboLinea, CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
                '.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

                .LLenarCboTienda(Me.cboTiendaOrigen, False)
                .LLenarCboTienda(Me.cboTiendaDestino, False)
                '.LlenarCboLinea(Me.cboLinea, True)
                '.LlenarCboSubLineaxIdLinea(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), True)

                Me.GV_PV.DataSource = (New Negocio.TipoPrecioV).SelectCbo1
                Me.GV_PV.DataBind()

            End With

            Try
                Me.IdUsuario = CInt(Session("IdUsuario"))
            Catch ex As Exception
                Me.IdUsuario = 0
            End Try



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub GV_PV_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_PV.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim cboTipoPrecioPV_Origen_1 As DropDownList = CType(e.Row.FindControl("cboTipoPV_Origen"), DropDownList)
            Dim objCombo As New Combo
            objCombo.LlenarCboTipoPV1(cboTipoPrecioPV_Origen_1, False)

        End If

    End Sub

    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged
        cargarCboSubLinea(CInt(Me.cboLinea.SelectedValue))
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub

    Private Sub cargarCboSubLinea(ByVal IdLinea As Integer)

        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cboSubLinea, IdLinea, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        registrarReplicaPV(CInt(Me.cboTiendaOrigen.SelectedValue), CInt(Me.cboTiendaDestino.SelectedValue))
    End Sub

    Private Sub registrarReplicaPV(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer)

        Try

            Dim D_Atributos As DataTable = obtenerDataTable_TipoTablaValor()

            Dim lista As List(Of Entidades.ProductoTipoPV_Replica) = obtenerLista_Replica()

            If ((New Negocio.ProductoTipoPV).ProductoTipoPV_ReplicarPrecioPV(lista, D_Atributos)) Then

                objScript.mostrarMsjAlerta(Me, "El Proceso de Replicación de Precios de Venta se realizó con éxito.")

            Else
                Throw New Exception("Problemas en la Operación.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerLista_Replica() As List(Of Entidades.ProductoTipoPV_Replica)

        Dim lista As New List(Of Entidades.ProductoTipoPV_Replica)


        For i As Integer = 0 To Me.GV_PV.Rows.Count - 1

            If (CType(Me.GV_PV.Rows(i).FindControl("chbTipoPV"), CheckBox).Checked) Then

                Dim objProductoTipoPV_Replica As New Entidades.ProductoTipoPV_Replica
                With objProductoTipoPV_Replica

                    .IdLinea = CInt(Me.cboLinea.SelectedValue)
                    .IdSubLinea = CInt(Me.cboSubLinea.SelectedValue)
                    .IdTienda_Origen = CInt(Me.cboTiendaOrigen.SelectedValue)
                    .IdTienda_Destino = CInt(Me.cboTiendaDestino.SelectedValue)

                    .IdTipoPV_Origen = CInt(CType(Me.GV_PV.Rows(i).FindControl("cboTipoPV_Origen"), DropDownList).SelectedValue)
                    .IdTipoPV_Destino = CInt(CType(Me.GV_PV.Rows(i).FindControl("hddIdTipoPV"), HiddenField).Value)
                    .PorcentPV = CDec(CType(Me.GV_PV.Rows(i).FindControl("txtPorcentPV"), TextBox).Text)
                    .addCostoFletexPeso = CType(Me.GV_PV.Rows(i).FindControl("chbCostoFlete"), CheckBox).Checked


                End With
                lista.Add(objProductoTipoPV_Replica)


            End If
        Next


        Return lista

    End Function

    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cboLinea, CInt(CboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


#Region "Filtro de busquedad avanza de productos"

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0

            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cboLinea.SelectedValue), CInt(Me.cboSubLinea.SelectedValue), CInt(cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1
            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)
        Next

        Return dt
    End Function

    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function

#End Region

    Private Sub cboSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubLinea.SelectedIndexChanged
        cbo = New Combo
        cbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cboSubLinea.SelectedValue), False)
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub

End Class