<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmConfigTipoCambio.aspx.vb" Inherits="APPWEB.FrmConfigTipoCambio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td>
                             <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                            onmouseout="this.src='/Imagenes/Guardar_B.JPG';" 
                                 onmouseover="this.src='/Imagenes/Guardar_A.JPG';" />
                             </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Configuración Tipo de Cambio</td>
        </tr>
        <tr>
            <td align="center">
            
                <table>
                    <tr>
                        <td style="text-align: center; width:150px; height:25px" class="SubTituloCelda">
                            Tipo de Cambio ($)</td>
                        <td style="text-align: center" class="TituloCelda">
                            Compra</td>
                        <td style="text-align: center" class="TituloCelda">
                            Venta</td>
                       
                    </tr>
                    <tr>
                        <td style="text-align: center" class="TituloCelda">
                            Comercial</td>
                        <td style="text-align: center">
                            <asp:TextBox ReadOnly="true" CssClass="TextBox_ReadOnly" ID="txtCompraC" Width="90px" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox   ReadOnly="true" CssClass="TextBox_ReadOnly" ID="txtVentaC" Width="90px" runat="server"></asp:TextBox>
                        </td>                        
                    </tr>
                    
                     <tr>
                        <td style="text-align: center" class="TituloCelda">
                            Oficial</td>
                        <td>
                            <asp:TextBox   ReadOnly="true" CssClass="TextBox_ReadOnly" ID="txtCompraOf" Width="90px" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox   ReadOnly="true" CssClass="TextBox_ReadOnly" ID="txtVentaOf" Width="90px" runat="server"></asp:TextBox>
                        </td>                        
                    </tr>
                </table>
            
            </td>
        </tr>
        <tr>
        <td></td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Asignación Tipo de cambio</td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:GridView ID="DGV_ConfigTipoCambio" AutoGenerateColumns="false" runat="server">
                <Columns>
                <asp:BoundField  DataField="IdConfigTipoCambio" HeaderText="Id" />
                <asp:BoundField HeaderStyle-Width="300px" DataField="Descripcion" HeaderText="Descripción" />
                
                <asp:TemplateField HeaderStyle-Width="100px" HeaderText="Compra Com."  ItemStyle-HorizontalAlign="Center">
                
                <ItemTemplate >
                
                    <asp:CheckBox   onClick="return(valChecked(this));" ID="chb_CompraC" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"CompraC")%>' />
                    
                </ItemTemplate>
                
                </asp:TemplateField>
                
                <asp:TemplateField HeaderStyle-Width="100px" HeaderText="Venta Com." ItemStyle-HorizontalAlign="Center">
                
                <ItemTemplate>
                
                    <asp:CheckBox onClick="return(valChecked(this));" ID="chb_VentaC" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"VentaC")%>' />
                    
                </ItemTemplate>
                
                </asp:TemplateField>
                
                <asp:TemplateField HeaderStyle-Width="100px" HeaderText="Compra Of." ItemStyle-HorizontalAlign="Center">
                
                <ItemTemplate>
                
                    <asp:CheckBox  onClick="return(valChecked(this));" ID="chb_CompraOf" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"CompraOf")%>' />
                    
                </ItemTemplate>
                
                </asp:TemplateField>
                
                <asp:TemplateField  HeaderStyle-Width="100px" HeaderText="Venta Of." ItemStyle-HorizontalAlign="Center">
                
                <ItemTemplate>
                
                    <asp:CheckBox onClick="return(valChecked(this));" ID="chb_VentaOf" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"VentaOf")%>' />
                    
                </ItemTemplate>
                
                </asp:TemplateField>
                
                
                </Columns>
                <RowStyle CssClass="GrillaRow" />
                <HeaderStyle CssClass="GrillaHeader" />
                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td style="width: 158px" class="Label">
                            Última actualización por:
                        </td>
                        <td>
                            <asp:Label ID="lblUsuario" runat="server" Text="Label" CssClass="LabelRojo"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    
    <script language="javascript" type="text/javascript">

        function valChecked(obj) {

            var grilla = document.getElementById('<%=DGV_ConfigTipoCambio.ClientID%>');
            var index = 0;
            
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];

                    if (obj.id == rowElem.cells[2].children[0].id) {

                        index = 2;
                        
                    }
                    if (obj.id == rowElem.cells[3].children[0].id) {

                        index = 3;

                    }
                    if (obj.id == rowElem.cells[4].children[0].id) {

                        index = 4;

                    }
                    if (obj.id == rowElem.cells[5].children[0].id) {

                        index = 5;

                    }
                    if (index != 0) {
                        rowElem.cells[2].children[0].status = false;
                        rowElem.cells[3].children[0].status = false;
                        rowElem.cells[4].children[0].status = false;
                        rowElem.cells[5].children[0].status = false;
                        rowElem.cells[index].children[0].status = true;
                        return true;
                    }
                }
            }

            return true;
        }
    
    </script>
    
</asp:Content>
