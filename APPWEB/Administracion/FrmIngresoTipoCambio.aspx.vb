﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System.IO

Partial Public Class FrmIngresoTipoCambio
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass 'Muestra los mensaje pa grabar o actualizar
    Private listaTipoCambio As List(Of Entidades.TipoCambio)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm() 'LLama al proceso inicializarFrm
        End If

    End Sub
    Private Sub inicializarFrm()
        Dim objScript As New ScriptManagerClass
        Try
            cargarDatosControles()
            verFrmInicio()
            If txtId.Text <> "" Then
                btnEditar.Visible = True
                btnEditar.Enabled = True
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
#Region "VISTAS FORMULARIOS"
    Private Sub verFrmInicio()
        limpiarControles() 'limpia los controles de texto
        Panel_Cabecera.Enabled = True
        Panel_Principal.Visible = True
        Panel_Principal.Enabled = True
        ObtenerDatosFrmGrilla() '
        btnBuscarTC.Visible = True
        btnBuscarTC.Enabled = True
        btnEditar.Enabled = False
        btnEditar.Visible = True
        btnAceptar_TipoMoneda.Enabled = False
        deshabilitar()
    End Sub
    Private Sub verFrmNuevo()
        Panel_Cabecera.Enabled = True
        Panel_Principal.Visible = True
        Panel_Principal.Enabled = True
        btnBuscarTC.Visible = True
        btnBuscarTC.Enabled = True
        cargarDatosFechaActual()
        cargarDatosGrilla2()
        limpiarControles()
        Habilitar()
    End Sub
    Private Sub verFrmBuscar()
        Panel_Cabecera.Enabled = True
        Panel_Principal.Visible = True
        cargarDatosFechaActual()
        cargarDatosGrilla2()
        ObtenerDatosFrmGrilla()
        deshabilitar()
        btnBuscarTC.Visible = True
        btnBuscarTC.Enabled = True
        btnAceptar_TipoMoneda.Enabled = True
    End Sub
    Private Sub verFrmEditar()
        btnEditar.Enabled = False
        btnGuardar.Enabled = True
        Habilitar()
    End Sub
    Private Sub verFrmGrabar()
        btnGuardar.Enabled = False
        deshabilitar()
    End Sub
    Private Sub limpiarControles()
        txtCompraComercial.Text = "0"
        txtCompraOficial.Text = "0"
        txtVentaComercial.Text = "0"
        txtVentaOficial.Text = "0"
        txtId.Text = ""
    End Sub
#End Region
#Region "CARGAR DATOS CONTROLES"

    Private Sub cargarDatosFechaActual() 'muestra la fecha actual del sistema y lo llena en la caja txto
        Dim objFecha As New Negocio.FechaActual
        If txtFecha.Text = "" Then ' si esta en blanco
            Me.Label1.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy") 'muestra fecha actual en el label1
            txtFecha.Text = Label1.Text 'asigna la fecha actual del label a la caja texto
        Else
            Exit Sub 'sale

        End If

    End Sub
    Private Sub cargarDatosControles() '
        cargarDatosFechaActual() 'Carga datos en el control label la fecha actual
        cargarDatosCboMoneda() 'Carga datos en el control combo moneda
        cargarDatosGrilla2() 'carga datos en control grilla
    End Sub

    Private Sub cargarDatosCboMoneda() 'Carga los tipo moneda de la tabla y muestra en el combo
        Dim objMoneda As New Negocio.Moneda
        cmbTipoMoneda.DataSource = objMoneda.SelectCboNoBase 'lee los datos de la tabla
        cmbTipoMoneda.DataBind() 'muestra los datos de la tabla
    End Sub

    Private Sub cargarDatosGrilla2() 'carga los datos en la grilla
        Dim objScript As New ScriptManagerClass
        Try

            Dim objTipoCambio As New Negocio.TipoCambio
            Dim listTipoCambio As List(Of Entidades.TipoCambio) = objTipoCambio.SelectTipoCambioBuscarFecha(CInt(cmbTipoMoneda.SelectedValue), CStr(txtFecha.Text)) 'llena la lista con el criterio de fecha y moneda

            DGV_SubLinea.DataSource = listTipoCambio 'asigna los datos de la lista al data source
            DGV_SubLinea.DataBind() 'muestra los de la lista en la grilla
            setDataSource(listTipoCambio) 'recupera los datos en la paginacion

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


#End Region

    Protected Sub btnAtras_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAtras.Click
        verFrmInicio()
        btnEditar.Visible = False
        btnEditar.Enabled = False
        If txtId.Text <> "" Then
            btnEditar.Visible = True
            btnEditar.Enabled = True
        End If
    End Sub
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        registrarTipoCambio() 'llama a registro tipo cambio
        deshabilitar() 'despues de grabar desabilita las caja de texto
        cargarDatosGrilla2() 'llama cargaDatosGrilla2 para mostrar el registro nuevo ingresado
        ObtenerDatosFrmGrilla() 'llama a ObtenerDatosFrmGrilla para mostrar datos en la caja texto

    End Sub
    Private Sub registrarTipoCambio() 'registra los tipo cambio
        Dim objScript As New ScriptManagerClass
        Dim obj As New Entidades.TipoCambio
        obj.CompraC = CDec(txtCompraComercial.Text)
        obj.CompraOf = CDec(txtCompraOficial.Text)

        If txtFecha.Text <> Label1.Text And txtFecha.Text = "" Then
            obj.Fecha = CStr(Me.Label1.Text)
        ElseIf txtFecha.Text <> "" Then
            obj.Fecha = CStr(txtFecha.Text)
        End If

        obj.IdMoneda = CInt(cmbTipoMoneda.SelectedValue)
        obj.IdUsuario = Nothing
        obj.VentaC = CDec(txtVentaComercial.Text)
        obj.VentaOf = CDec(txtVentaOficial.Text)
        If obj.Fecha = "" Then
            objScript.mostrarMsjAlerta(Me, "No tiene fecha")
            Exit Sub
        End If
        Try
            Dim objTipoCambio As New Negocio.TipoCambio

            If hddId.Value = "N" Then 'si es nuevo q registre los valores
                obj.Id = 0
                objTipoCambio.InsertaTipoCambio(obj)
                objScript.mostrarMsjAlerta(Me, "El Tipo de Cambio fue registrado con éxito:" & txtFecha.Text)
            Else 'caso contrario si es m q actualice
                obj.Id = CInt(txtId.Text)
                objTipoCambio.ActualizaTipoCambio(obj)
                objScript.mostrarMsjAlerta(Me, "El Tipo de Cambio se actualizo con éxito.")
            End If
            btnEditar.Enabled = True
            btnEditar.Visible = True

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
    Protected Sub btnBuscarTC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarTC.Click
        hddId.Value = "B"
        limpiarControles()
        cargarDatosGrilla2()
        ObtenerDatosFrmGrilla() '
        listaTipoCambio = CType(getDataSource(), List(Of Entidades.TipoCambio))
        Dim cont As Integer
        'Dim NPag As Decimal
        Try
            For Each obj As Entidades.TipoCambio In listaTipoCambio
                cont = cont + 1 'calcula el total de registros en la grilla

                If obj.Fecha = txtFecha.Text Then 'si obj.fecha es = caja texto muestra resultados en la caja texto

                    Me.txtCompraOficial.Text = CStr(obj.CompraOf) 'asigna valor de la grilla a la caja texto 
                    Me.txtVentaOficial.Text = CStr(obj.VentaOf) 'asigna valor de la grilla a la caja texto
                    Me.txtCompraComercial.Text = CStr(obj.CompraC) 'asigna valor de la grilla a la caja texto
                    Me.txtVentaComercial.Text = CStr(obj.VentaC) 'asigna valor de la grilla a la caja texto
                    Me.txtId.Text = CStr(obj.Id) 'asigna valor de la grilla a la caja texto
                    Me.txtFecha.Text = CStr(obj.Fecha) 'asigna valor de la grilla a la caja texto

                    ' NPag = CDec((listaTipoCambio.Count / 10))'calcula total de paginas en la grilla
                    Dim SelPag As Decimal = CDec(cont / 10) 'selecciona el nro pagina
                    Me.DGV_SubLinea.PageIndex = CInt(SelPag) 'muestra la pagina donde esta el resultado de busqueda
                    Me.DGV_SubLinea.DataSource = Me.getDataSource 'aki recupera
                    Me.DGV_SubLinea.DataBind()
                    Exit For 'sale del brucle
                End If
            Next 'siguiente recorrido

            If txtFecha.Text = "" Then 'si fecha esta en blanco muestra un mensaje
                objScript.mostrarMsjAlerta(Me, "Debe Ingresa un fecha")
                txtFecha.Focus()
                Exit Sub
            End If
            
            btnEditar.Visible = False
            btnEditar.Enabled = False
            If txtId.Text <> "" Then
                btnEditar.Visible = True
                btnEditar.Enabled = True
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
 
    Private Sub Habilitar()
        Me.txtCompraOficial.Enabled = True
        Me.txtCompraComercial.Enabled = True
        Me.txtVentaComercial.Enabled = True
        Me.txtVentaOficial.Enabled = True
        Me.txtId.Enabled = True
        Me.btnGuardar.Enabled = True
        Me.btnGuardar.Visible = True
        Me.btnAtras.Enabled = True
        Me.btnAtras.Visible = True
    End Sub
    Private Sub deshabilitar()
        Me.txtCompraOficial.Enabled = False
        Me.txtCompraComercial.Enabled = False
        Me.txtVentaComercial.Enabled = False
        Me.txtVentaOficial.Enabled = False
        Me.txtId.Enabled = False
        Me.btnGuardar.Enabled = False
        Me.btnGuardar.Visible = False
        Me.btnAtras.Enabled = False
        Me.btnAtras.Visible = False
    End Sub
    Protected Sub btNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btNuevo.Click
        verFrmNuevo() 'llama al verFrmNuevo
        hddId.Value = "N" 'asigna letra N
    End Sub
    Private Sub setDataSource(ByVal obj As Object)
        Session.Remove("DataSource") 'remueve sesion anterior
        Session.Add("DataSource", obj) 'agregar o actrualiza sesion
    End Sub

    Private Function getDataSource() As Object
        Return Session.Item("DataSource") 'recupera la sesion
    End Function

    Protected Sub DGV_SubLinea_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_SubLinea.PageIndexChanging
        Me.DGV_SubLinea.PageIndex = e.NewPageIndex 'indica el Nro de paginacion en la grilla
        Me.DGV_SubLinea.DataSource = Me.getDataSource 'aki recupera
        Me.DGV_SubLinea.DataBind() 'muestra datos en la paginacion
    End Sub

    Protected Sub DGV_SubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_SubLinea.SelectedIndexChanged
        Try
            cargarDatos() 'llama a la cargadatos
            If txtId.Text <> "" Then
                btnEditar.Visible = True
                btnEditar.Enabled = True
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cargarDatos() 'muestra los datos en la grilla cuando selecciona un registro de la grilla
        Try
            Me.txtCompraOficial.Text = DGV_SubLinea.SelectedRow.Cells(2).Text
            Me.txtVentaOficial.Text = DGV_SubLinea.SelectedRow.Cells(3).Text
            Me.txtCompraComercial.Text = DGV_SubLinea.SelectedRow.Cells(4).Text
            Me.txtVentaComercial.Text = DGV_SubLinea.SelectedRow.Cells(5).Text
            Me.txtId.Text = CType(Me.DGV_SubLinea.SelectedRow.FindControl("hddIdTipoCambio"), HiddenField).Value
            Me.txtFecha.Text = DGV_SubLinea.SelectedRow.Cells(1).Text
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos")
        End Try
    End Sub

    Private Sub ObtenerDatosFrmGrilla() ' Muestra valores en caja texto despues de recorrer la grilla de acuerdo fecha y moneda
        For Each fila As GridViewRow In DGV_SubLinea.Rows 'empieza el recorrido en la grilla
            If fila.Cells(1).Text = txtFecha.Text Then 'si el valor de la fila de grilla(fecha) =fecha de la caja texto muestra los valores en la caja texto
                Me.txtCompraOficial.Text = fila.Cells(2).Text 'asigna valor de la grilla a la caja texto 
                Me.txtVentaOficial.Text = fila.Cells(3).Text 'asigna valor de la grilla a la caja texto
                Me.txtCompraComercial.Text = fila.Cells(4).Text 'asigna valor de la grilla a la caja texto
                Me.txtVentaComercial.Text = fila.Cells(5).Text 'asigna valor de la grilla a la caja texto
                Me.txtId.Text = fila.Cells(6).Text 'asigna valor de la grilla a la caja texto
                Me.txtFecha.Text = fila.Cells(1).Text 'asigna valor de la grilla a la caja texto
            End If
            'Exit For'sale del bucle en el primer registro
        Next 'recorre la siguiente fila si la fecha  caja texto es diferente al valor de la fecha de la grilla

    End Sub
    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditar.Click
        verFrmEditar() 'llama al verFrmEditar
        hddId.Value = "M" 'asigna la M
    End Sub

    Private Sub btnExportarExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click

        ExportGrid(Me.listaTipoCambio, "TipoCambio")
    End Sub

    Private Sub ExportGrid(ByVal Lista As List(Of Entidades.TipoCambio), ByVal FilleNameExt As String)
        'Dim url As String = Page.ResolveClientUrl("~/Reportes/frmExcel.aspx?iReport=1")

        'Dim Script As String = "window.open('" + url + "', '_blank', 'height=700,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );"

        'ScriptManager.RegisterStartupScript(Page, GetType(Page), "Redirect", Script, True)


        Me.listaTipoCambio = CType(getDataSource(), List(Of Entidades.TipoCambio))


        For i As Integer = -1 To listaTipoCambio.Count - 1

            Dim trow As New TableRow()
            For p As Integer = 0 To 4
                Dim tcell As New TableCell()
                tcell.ControlStyle.BorderStyle = BorderStyle.NotSet
                tcell.ControlStyle.BorderWidth = 1

                If i = -1 Then
                    tcell.ControlStyle.BackColor = Drawing.Color.LightSteelBlue
                    tcell.ControlStyle.Font.Bold = True
                    tcell.ControlStyle.Height = 25
                    Select Case p
                        Case 0
                            tcell.Text = "Fecha"
                        Case 1
                            tcell.Text = "Compra - Oficial"
                        Case 2
                            tcell.Text = "Venta - Oficial"
                        Case 3
                            tcell.Text = "Compra - Comercial"
                        Case 4
                            tcell.Text = "Venta - Comercial"
                    End Select
                Else
                    Select Case p
                        Case 0
                            tcell.Text = listaTipoCambio(i).Fecha
                        Case 1
                            tcell.Text = listaTipoCambio(i).CompraOf.ToString
                        Case 2
                            tcell.Text = listaTipoCambio(i).VentaOf.ToString
                        Case 3
                            tcell.Text = listaTipoCambio(i).CompraC.ToString
                        Case 4
                            tcell.Text = listaTipoCambio(i).VentaC.ToString
                    End Select
                End If

                trow.Cells.Add(tcell)

            Next

            tablaPrincipal.Rows.Add(trow)
        Next




        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + FilleNameExt)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"

        Response.ContentEncoding = System.Text.Encoding.UTF8

        Dim strWriter As System.IO.StringWriter
        strWriter = New System.IO.StringWriter
        Dim htmlTextWriter As System.Web.UI.HtmlTextWriter
        htmlTextWriter = New System.Web.UI.HtmlTextWriter(strWriter)

        

        tablaPrincipal.RenderControl(htmlTextWriter)
      
        EnableViewState = False

        Response.Write(strWriter.ToString)

        Response.End()

        'ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(Me.tablaPrincipal)

    End Sub

    Private Sub agregarColumnaGrilla(ByVal objGridView As GridView, ByVal headerText As String, ByVal isVisible As Boolean, ByVal dataField As String)
        Dim column As New BoundField
        column.HeaderText = headerText
        column.Visible = isVisible
        column.DataField = dataField
        objGridView.Columns.Add(column)
    End Sub


End Class
