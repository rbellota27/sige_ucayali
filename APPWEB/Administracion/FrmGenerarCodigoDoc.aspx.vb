﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmGenerarCodigoDoc
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private objCbo As New Combo
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()

        Try

            objCbo.LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))
            txtCodACTUAL.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(IIf(IsNumeric(Me.cboSerie.SelectedValue) = True, Me.cboSerie.SelectedValue, 0)))
            Me.txtCodInicio.Text = txtCodACTUAL.Text
            Me.txtCodFIN.Text = Me.txtCodInicio.Text

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de los datos.")
        End Try

    End Sub
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click

        generarCorrelativos(CInt(Me.cboSerie.SelectedValue))

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged

        Try
            objCbo.llenarCboTiendaxIdEmpresa(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue))
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))
            txtCodACTUAL.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(IIf(IsNumeric(Me.cboSerie.SelectedValue) = True, Me.cboSerie.SelectedValue, 0)))
            Me.txtCodInicio.Text = txtCodACTUAL.Text
            Me.txtCodFIN.Text = Me.txtCodInicio.Text
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de los datos.")
        End Try

    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))
            txtCodACTUAL.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(IIf(IsNumeric(Me.cboSerie.SelectedValue) = True, Me.cboSerie.SelectedValue, 0)))
            Me.txtCodInicio.Text = txtCodACTUAL.Text
            Me.txtCodFIN.Text = Me.txtCodInicio.Text
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de los datos.")
        End Try
    End Sub

    Private Sub cboTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoDocumento.SelectedIndexChanged
        Try
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))
            txtCodACTUAL.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(IIf(IsNumeric(Me.cboSerie.SelectedValue) = True, Me.cboSerie.SelectedValue, 0)))
            Me.txtCodInicio.Text = txtCodACTUAL.Text
            Me.txtCodFIN.Text = Me.txtCodInicio.Text
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de los datos.")
        End Try
    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            txtCodACTUAL.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(IIf(IsNumeric(Me.cboSerie.SelectedValue) = True, Me.cboSerie.SelectedValue, 0)))
            Me.txtCodInicio.Text = txtCodACTUAL.Text
            Me.txtCodFIN.Text = Me.txtCodInicio.Text
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de los datos.")
        End Try
    End Sub

    Private Sub generarCorrelativos(ByVal IdSerie As Integer)

        Try
            If ((New Negocio.Documento).AutoGenerarDocumentos_Salteados(CInt(cboSerie.SelectedValue), CInt(Me.txtCodInicio.Text), CInt(Me.txtCodFIN.Text), CInt(Session("IdUsuario")))) Then
                txtCodACTUAL.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(IIf(IsNumeric(Me.cboSerie.SelectedValue) = True, Me.cboSerie.SelectedValue, 0)))
                Me.txtCodInicio.Text = Me.txtCodACTUAL.Text
                Me.txtCodFIN.Text = Me.txtCodInicio.Text
                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")
            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

End Class