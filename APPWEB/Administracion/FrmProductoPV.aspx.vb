﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmProductoPV
    Inherits System.Web.UI.Page


    Dim cbo As Combo
    Private objScript As New ScriptManagerClass
    Private listaProductoView As List(Of Entidades.ProductoView)
    Dim listaProductoTipoPrecio As List(Of Entidades.ProductoTipoPV)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FrmProductoPV_OnLoad()
    End Sub
    Private Sub FrmProductoPV_OnLoad()
        If Not IsPostBack Then

            cbo = New Combo

            With cbo

                .llenarCboTipoExistencia(Me.CboTipoExistencia)
                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTiendaOrigen, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTiendaDestino, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LlenarCboTipoPV(Me.cboTipoPrecio, False)

            End With

            TipoExistencia_OnChange(False)

            Me.GV_PV.DataSource = (New Negocio.TipoPrecioV).SelectCbo1
            Me.GV_PV.DataBind()

            Me.S_listaProductoTipoPrecio = New List(Of Entidades.ProductoTipoPV)

            Me.hddTipoCambio.Value = CStr(Math.Round((New Negocio.Util).SelectValorxIdMonedaOxIdMonedaD(2, 1, 1, "I"), 3))

        End If
    End Sub


    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        cbo = New Combo
        With cbo

            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTiendaOrigen, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTiendaDestino, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)

        End With

    End Sub

    Private Property S_listaProductoTipoPrecio() As List(Of Entidades.ProductoTipoPV)
        Get
            Return CType(Session("ProductoTipoPrecio"), List(Of Entidades.ProductoTipoPV))
        End Get
        Set(ByVal value As List(Of Entidades.ProductoTipoPV))
            Session.Remove("ProductoTipoPrecio")
            Session.Add("ProductoTipoPrecio", value)
        End Set
    End Property

#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            ModalPopup_Producto.Show()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function

    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            ModalPopup_Producto.Show()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region

#Region "****************************** Busqueda Producto"
    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
    End Sub

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", 0)
                    ViewState.Add("IdEmpresa_BuscarProducto", 0)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia", Me.CboTipoExistencia.SelectedValue)

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, ByVal nomProducto As String, ByVal IdAlmacen As Integer, _
                                          ByVal tipoMov As Integer, ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        '************Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        'Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)


        If listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  alert('No se hallaron registros.');  ", True)
            ModalPopup_Producto.Show()
            Return

        Else

            grilla.DataSource = listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)
        End If

        setListaProductoView(Me.listaProductoView)

        ModalPopup_Producto.Show()

    End Sub
    Private Sub setListaProductoView(ByVal lista As List(Of Entidades.ProductoView))
        Session.Remove("listaProductoView")
        Session.Add("listaProductoView", lista)
    End Sub
    Private Function getListaProductoView() As List(Of Entidades.ProductoView)
        Return CType(Session.Item("listaProductoView"), List(Of Entidades.ProductoView))
    End Function

    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            0, 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            0, 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            0, 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region

#Region "************************************  Funcionalidad de busqueda de producto"
    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        TipoExistencia_OnChange()
    End Sub
    Private Sub TipoExistencia_OnChange(Optional ByVal showProducto As Boolean = True)

        If IsNothing(cbo) Then cbo = New Combo
        With cbo

            .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

        End With

        If showProducto Then
            ModalPopup_Producto.Show()
        End If

    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged


        If IsNothing(cbo) Then cbo = New Combo

        With cbo
            .LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
        End With

        ModalPopup_Producto.Show()

    End Sub

    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            If IsNothing(cbo) Then cbo = New Combo

            With cbo
                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
            End With

            ModalPopup_Producto.Show()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub DGV_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_AddProd.SelectedIndexChanged
        cargarProducto(Me.DGV_AddProd.SelectedIndex)
    End Sub

    Private Sub cargarProducto(ByVal Index As Integer)

        Try

            Me.listaProductoView = getListaProductoView()

            Dim lista As New List(Of Entidades.ProductoView)
            lista.Add(Me.listaProductoView(Index))


            Me.GV_Producto.DataSource = lista
            Me.GV_Producto.DataBind()

            Dim objProducto As Entidades.ProductoView = (New Negocio.Producto).ProductoSelectxIdProducto(Me.listaProductoView(Index).Id)
            With objProducto

                Me.hddMoneda_PrecioCompra.Value = .Moneda
                Me.hddPrecioCompra_prod.Value = CStr(Math.Round(.PrecioCompra, 3))

                Me.hddIdProducto.Value = CStr(.Id)
                Me.hddIdMoneda_PC.Value = CStr(.IdMoneda)
                Me.hddIdUnidadMedida_Principal.Value = CStr(.IdUnidadMedida)

            End With

            LimpiarFrm()

            For i As Integer = 0 To Me.cboTipoPrecio.Items.Count - 1

                TipoPrecioAdd(CInt(Me.cboTipoPrecio.Items(i).Value))

            Next

            Me.DGV_PRODUCTO.DataSource = Me.listaProductoTipoPrecio
            Me.DGV_PRODUCTO.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularPrecioVenta_PR('5','1');", True)



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

#End Region





#Region "******************************** Tienda"

    Private ListaTienda As List(Of Entidades.Tienda)

    Private Property S_ListaTienda() As List(Of Entidades.Tienda)
        Get
            Return CType(Session("ListaTienda"), List(Of Entidades.Tienda))
        End Get
        Set(ByVal value As List(Of Entidades.Tienda))
            Session.Remove("ListaTienda")
            Session.Add("ListaTienda", value)
        End Set
    End Property

    Private Sub imgTiendaAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTiendaAdd.Click
        Tienda_Add(CInt(cboTiendaDestino.SelectedValue))
    End Sub
    Private Sub Tienda_Add(ByVal IdTienda As Integer)

        If IsNothing(S_ListaTienda) Then S_ListaTienda = New List(Of Entidades.Tienda)

        Me.S_ListaTienda.AddRange((New Negocio.Tienda).SelectxId(IdTienda))

        Me.GV_Tienda.DataSource = Me.S_ListaTienda
        Me.GV_Tienda.DataBind()

    End Sub


    Protected Sub lkbTiendaRemove_Click(ByVal sender As Object, ByVal e As EventArgs)
        Tienda_Remove(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub
    Private Sub Tienda_Remove(ByVal index As Integer)

        S_ListaTienda.RemoveAt(index)

        Me.GV_Tienda.DataSource = S_ListaTienda
        Me.GV_Tienda.DataBind()

    End Sub


#End Region

#Region "*********************** replica de precios"

    Dim ListaTipoPrecioV As List(Of Entidades.ProductoTipoPV)

    Private Property S_ListaTipoPrecioV() As List(Of Entidades.ProductoTipoPV)
        Get
            Return CType(Session("ListaTipoPrecioV"), List(Of Entidades.ProductoTipoPV))
        End Get
        Set(ByVal value As List(Of Entidades.ProductoTipoPV))
            Session.Remove("ListaTipoPrecioV")
            Session.Add("ListaTipoPrecioV", value)
        End Set
    End Property

    Private Sub GV_PV_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_PV.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim cboTipoPrecioPV_Origen_1 As DropDownList = CType(e.Row.FindControl("cboTipoPV_Origen"), DropDownList)
            Dim objCombo As New Combo
            objCombo.LlenarCboTipoPV1(cboTipoPrecioPV_Origen_1, False)

        End If

    End Sub

    Private Sub btnEjecutar_Replica_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEjecutar_Replica.Click
        registrarReplicaPV(CInt(Me.cboTiendaOrigen.SelectedValue), CInt(Me.cboTiendaDestino.SelectedValue))
    End Sub

    Private Sub registrarReplicaPV(ByVal IdTiendaOrigen As Integer, ByVal IdTiendaDestino As Integer)

        Try

            Dim lista As List(Of Entidades.ProductoTipoPV_Replica) = obtenerLista_Replica()
            Dim ListaTienda As List(Of Entidades.Tienda) = S_ListaTienda

            Me.Actualizar_ProductoPV()
            Me.listaProductoTipoPrecio = S_listaProductoTipoPrecio

            Dim IdProducto As Integer = CInt(Me.hddIdProducto.Value)

            Do While (Me.listaProductoTipoPrecio.Count) > 0
                Dim IdTipoPV As Integer = 0
                If Me.listaProductoTipoPrecio.Count > 0 Then IdTipoPV = Me.listaProductoTipoPrecio(0).IdTipoPv

                If IdTipoPV > 0 Then
                    Dim ListaProductoPV As List(Of Entidades.ProductoTipoPV) = Me.listaProductoTipoPrecio.FindAll(Function(k As Entidades.ProductoTipoPV) k.IdTipoPv = IdTipoPV)

                    If ((New Negocio.ProductoTipoPV).InsertaProductoTipoPVTxProducto(ListaProductoPV, CInt(Me.hddIdProducto.Value), CInt(Me.cboTiendaOrigen.SelectedValue), IdTipoPV)) Then
                        Me.listaProductoTipoPrecio.RemoveAll(Function(k As Entidades.ProductoTipoPV) k.IdTipoPv = IdTipoPV)
                    End If

                End If

            Loop

            If ((New Negocio.ProductoTipoPV).ReplicarPrecioPV_IdProducto(lista, ListaTienda)) Then

                objScript.mostrarMsjAlerta(Me, "El Proceso de Replicación de Precios de Venta se realizó con éxito.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerLista_Replica() As List(Of Entidades.ProductoTipoPV_Replica)

        Dim lista As New List(Of Entidades.ProductoTipoPV_Replica)


        For i As Integer = 0 To Me.GV_PV.Rows.Count - 1

            If (CType(Me.GV_PV.Rows(i).FindControl("chbTipoPV"), CheckBox).Checked) Then

                Dim objProductoTipoPV_Replica As New Entidades.ProductoTipoPV_Replica
                With objProductoTipoPV_Replica

                    .IdProducto = CInt(Me.hddIdProducto.Value)

                    '' ***********************                           REVISAR
                    .IdTienda_Origen = CInt(Me.cboTiendaOrigen.SelectedValue)

                    .IdTipoPV_Origen = CInt(CType(Me.GV_PV.Rows(i).FindControl("cboTipoPV_Origen"), DropDownList).SelectedValue)
                    .IdTipoPV_Destino = CInt(CType(Me.GV_PV.Rows(i).FindControl("hddIdTipoPV"), HiddenField).Value)
                    .PorcentPV = CDec(CType(Me.GV_PV.Rows(i).FindControl("txtPorcentPV"), TextBox).Text)
                    .addCostoFletexPeso = CType(Me.GV_PV.Rows(i).FindControl("chbCostoFlete"), CheckBox).Checked


                End With
                lista.Add(objProductoTipoPV_Replica)


            End If
        Next


        Return lista

    End Function

#End Region

    Dim ListaProductoTipoPV As List(Of Entidades.ProductoTipoPV)

    Private Sub btnProducto_TipoPrecioV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProducto_TipoPrecioV.Click

        Producto_TipoPrecioV()

    End Sub

    Private Sub Producto_TipoPrecioV()

        Try

            Dim lista As List(Of Entidades.ProductoTipoPV_Replica) = obtenerLista_Replica()
            Dim ListaTienda As List(Of Entidades.Tienda) = S_ListaTienda

            ListaProductoTipoPV = New List(Of Entidades.ProductoTipoPV)

            For j As Integer = 0 To ListaTienda.Count - 1

                For i As Integer = 0 To lista.Count - 1

                    Me.ListaProductoTipoPV.AddRange((New Negocio.ProductoTipoPV).ProductoPrecioV(CInt(Me.cboTiendaOrigen.SelectedValue), ListaTienda(j).Id, lista(i).IdTipoPV_Origen, lista(i).IdTipoPV_Destino, CInt(Me.hddIdProducto.Value), lista(i).PorcentPV, lista(i).addCostoFletexPeso))

                Next

            Next

            Me.GV_PrecioV.DataSource = Me.ListaProductoTipoPV
            Me.GV_PrecioV.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub imgTipoPrecioAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTipoPrecioAdd.Click

        TipoPrecioAdd()

        Me.DGV_PRODUCTO.DataSource = Me.listaProductoTipoPrecio
        Me.DGV_PRODUCTO.DataBind()

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularPrecioVenta_PR('5','1');", True)


    End Sub
    Private Sub TipoPrecioAdd(Optional ByVal IdTipoPrecioVenta As Integer = 0)

        Dim IdProducto As Integer = CInt(Me.hddIdProducto.Value)
        Dim IdTiendaOrigen As Integer = CInt(Me.cboTiendaOrigen.SelectedValue)
        Dim IdTipoPrecioOrigen As Integer = CInt(Me.cboTipoPrecio.SelectedValue)

        If IdTipoPrecioVenta > 0 Then
            IdTipoPrecioOrigen = IdTipoPrecioVenta
        End If

        Me.Actualizar_ProductoPV()
        Me.listaProductoTipoPrecio = Me.S_listaProductoTipoPrecio


        Me.listaProductoTipoPrecio.RemoveAll(Function(j As Entidades.ProductoTipoPV) j.IdProducto = IdProducto And j.IdTipoPv = IdTipoPrecioOrigen)

        Dim Lista_ProductoTipoPV As List(Of Entidades.ProductoTipoPV) = (New Negocio.ProductoTipoPV).SelectxIdProductoxIdTiendaxIdTipoPV(IdProducto, IdTiendaOrigen, IdTipoPrecioOrigen)


        If IsNothing(Lista_ProductoTipoPV) Then Lista_ProductoTipoPV = New List(Of Entidades.ProductoTipoPV)

        Dim listaUM As List(Of Entidades.ProductoUMView) = (New Negocio.ProductoUMView).SelectxIdProducto(IdProducto)

        For i As Integer = 0 To listaUM.Count - 1

            Dim IdUnidadMedida As Integer = listaUM(i).IdUnidadMedida

            If Lista_ProductoTipoPV.Find(Function(k As Entidades.ProductoTipoPV) k.IdTienda = IdTiendaOrigen And k.IdTipoPv = IdTipoPrecioOrigen And k.IdUnidadMedida = IdUnidadMedida) Is Nothing Then

                Dim ObjProductoTipoPV As New Entidades.ProductoTipoPV
                With ObjProductoTipoPV

                    .IdUnidadMedida = IdUnidadMedida
                    .NomUMedida = listaUM(i).NombreCortoUM
                    .PUtilFijo = 0
                    .PUtilVariable = 0
                    .Utilidad = 0
                    .Valor = 0
                    .PV_Soles = 0
                    .PV_Dolares = 0

                    .IdMoneda = 1  '********** SOLES por defecto
                    .Estado = "1"

                    .Equivalencia = listaUM(i).Equivalencia
                    .PorcentRetazo = listaUM(i).pum_PorcentRetazo

                    .NomMonedaPC = hddMoneda_PrecioCompra.Value

                    .UnidadPrincipal = listaUM(i).UnidadPrincipal
                    .Retazo = listaUM(i).Retazo

                    .IdProducto = IdProducto
                    .IdTienda = IdTiendaOrigen
                    .IdTipoPv = IdTipoPrecioOrigen
                    .TipoPV = (New Negocio.TipoPrecioV).SelectxId(IdTipoPrecioOrigen)(0).Nombre

                End With

                Lista_ProductoTipoPV.Add(ObjProductoTipoPV)


            End If

        Next


        Me.listaProductoTipoPrecio.AddRange(Lista_ProductoTipoPV)

        Me.S_listaProductoTipoPrecio = Me.listaProductoTipoPrecio

        If Me.listaProductoTipoPrecio.Count > 0 Then
            Me.cboEmpresa.Enabled = False
            Me.cboTiendaOrigen.Enabled = False
        Else
            Me.cboEmpresa.Enabled = True
            Me.cboTiendaOrigen.Enabled = True
        End If


    End Sub

    Private Sub imgLimpiarTipoPrecioAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgLimpiarTipoPrecioAdd.Click

        S_listaProductoTipoPrecio = New List(Of Entidades.ProductoTipoPV)

        Me.cboEmpresa.Enabled = True
        Me.cboTiendaOrigen.Enabled = True
        Me.DGV_PRODUCTO.DataBind()

    End Sub

    Private Sub Actualizar_ProductoPV()

        Me.listaProductoTipoPrecio = Me.S_listaProductoTipoPrecio




        For i As Integer = 0 To Me.listaProductoTipoPrecio.Count - 1

            If Me.DGV_PRODUCTO.Rows.Count > 0 Then

                Me.listaProductoTipoPrecio(i).PV_Soles = CDec(CType(Me.DGV_PRODUCTO.Rows(i).FindControl("txtPVFinal_Soles_PR"), TextBox).Text)
                Me.listaProductoTipoPrecio(i).PV_Dolares = CDec(CType(Me.DGV_PRODUCTO.Rows(i).FindControl("txtPVFinal_Dolares_PR"), TextBox).Text)

                Me.listaProductoTipoPrecio(i).PUtilFijo = CDec(CType(Me.DGV_PRODUCTO.Rows(i).Cells(6).FindControl("txtPorcentUtilidad_PR"), TextBox).Text)
                Me.listaProductoTipoPrecio(i).Utilidad = CDec(CType(Me.DGV_PRODUCTO.Rows(i).Cells(7).FindControl("txtUtilidad_PR"), TextBox).Text)

                Me.listaProductoTipoPrecio(i).IdMoneda = CInt(CType(Me.DGV_PRODUCTO.Rows(i).Cells(10).FindControl("cboMonedaDestino_PR"), DropDownList).SelectedValue)

                Me.listaProductoTipoPrecio(i).Estado = CStr(IIf(CType(Me.DGV_PRODUCTO.Rows(i).Cells(11).FindControl("chbEstado_PR"), CheckBox).Checked = True, "1", "0"))

            End If

            If Me.listaProductoTipoPrecio(i).IdMoneda = 1 Then

                '*********** SOLES
                Me.listaProductoTipoPrecio(i).Valor = Me.listaProductoTipoPrecio(i).PV_Soles

            ElseIf Me.listaProductoTipoPrecio(i).IdMoneda = 2 Then

                '************ DOLARES
                Me.listaProductoTipoPrecio(i).Valor = Me.listaProductoTipoPrecio(i).PV_Dolares

            End If


        Next
        Me.S_listaProductoTipoPrecio = Me.listaProductoTipoPrecio

    End Sub

    Private Sub DGV_PRODUCTO_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_PRODUCTO.RowDataBound

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboMonedaPV As DropDownList
                Dim gvrow As GridViewRow = CType(e.Row.Cells(10).NamingContainer, GridViewRow)
                cboMonedaPV = CType(gvrow.FindControl("cboMonedaDestino_PR"), DropDownList)
                cboMonedaPV.SelectedValue = CStr(Me.listaProductoTipoPrecio(e.Row.RowIndex).IdMoneda)

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub


    Protected Sub DGV_PRODUCTO_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_PRODUCTO.SelectedIndexChanged

        Try

            Me.Actualizar_ProductoPV()

            Me.listaProductoTipoPrecio = Me.S_listaProductoTipoPrecio

            Me.listaProductoTipoPrecio.RemoveAt(DGV_PRODUCTO.SelectedIndex)

            Me.S_listaProductoTipoPrecio = Me.listaProductoTipoPrecio

            Me.DGV_PRODUCTO.DataSource = Me.S_listaProductoTipoPrecio
            Me.DGV_PRODUCTO.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub
    Private Sub LimpiarFrm()

        Me.S_listaProductoTipoPrecio = New List(Of Entidades.ProductoTipoPV)
        Me.DGV_PRODUCTO.DataBind()
        ' ACTUALIZO LAS TIENDAS
        Dim ListaTienda As List(Of Entidades.Tienda) = Me.S_ListaTienda
        If IsNothing(ListaTienda) Then ListaTienda = New List(Of Entidades.Tienda)

        Me.S_ListaTienda = ListaTienda
        Me.GV_Tienda.DataSource = Me.S_ListaTienda
        Me.GV_Tienda.DataBind()

    End Sub
End Class