﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmCuentaPersona
    Inherits System.Web.UI.Page

    Dim listaPersona As List(Of Entidades.CuentaPersona)

#Region "Atributos"
    Dim Combo As New Combo
    Private objScript As New ScriptManagerClass
    Private objctaPersona As New Negocio.CuentaPersona
    Private IdTienda As Integer
    Private CuentaEmpTienda As New Entidades.CuentaEmpresaTienda
    'Dim CuentaEmpresa As New Entidades.CuentaEmpresaTienda

    'SListaCxP Sesion ListaCuentasxPersona
    Public Property SListaCxP() As List(Of Entidades.CuentaPersona)
        Get
            Return CType(Session("ListaCxP"), List(Of Entidades.CuentaPersona))
        End Get
        Set(ByVal value As List(Of Entidades.CuentaPersona))
            Session.Remove("ListaCxP")
            Session.Add("ListaCxP", value)
        End Set
    End Property

    'pIdPersona p: persistente, se refiere a que esta guardado
    Public Property pIdPersona() As Integer
        Get
            Return CInt(ViewState.Item("IdPersona"))
        End Get

        Set(ByVal value As Integer)
            ViewState.Remove("IdPersona")
            ViewState.Add("IdPersona", value)
        End Set
    End Property
    'gIdEmpresa g: grafico, se refiere a la interfaz de usuario
    Public Property gIdEmpresa() As Integer
        Get
            Return CInt(cboEmpresa.SelectedValue)
        End Get
        Set(ByVal value As Integer)
            cboEmpresa.SelectedValue = CStr(value)
        End Set
    End Property

    Public Property pSaldoTienda() As Decimal
        Get
            Return CDec(ViewState.Item("SaldoTienda"))
        End Get

        Set(ByVal value As Decimal)
            ViewState.Remove("SaldoTienda")
            ViewState.Add("SaldoTienda", value)
        End Set
    End Property

    Public Property pLineaCreditoTienda() As Decimal
        Get
            Return CDec(ViewState.Item("LineaCreditoTienda"))
        End Get

        Set(ByVal value As Decimal)
            ViewState.Remove("LineaCreditoTienda")
            ViewState.Add("LineaCreditoTienda", value)
        End Set
    End Property

    Public Property pCargoMaxCtasxTienda() As Decimal
        Get
            Return CDec(ViewState.Item("CargoMaxCtasxTienda"))
        End Get

        Set(ByVal value As Decimal)
            ViewState.Remove("CargoMaxCtasxTienda")
            ViewState.Add("CargoMaxCtasxTienda", value)
        End Set
    End Property


    Public Property VSCuentasDisponibles() As Integer
        Get
            Return CInt(ViewState.Item("CuentasDisponibles"))
        End Get

        Set(ByVal value As Integer)
            ViewState.Remove("CuentasDisponibles")
            ViewState.Add("CuentasDisponibles", value)
        End Set
    End Property

    Public Property VSFontColor() As System.Drawing.Color
        Get
            Return CType(ViewState.Item("FontColor"), System.Drawing.Color)
        End Get

        Set(ByVal value As System.Drawing.Color)
            ViewState.Remove("FontColor")
            ViewState.Add("FontColor", value)
        End Set
    End Property
    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session("IdUsuario"), Integer)
        End Get
    End Property
    Public Property VSPermisoRegistroyEdicion() As Boolean
        Get
            Return CType(ViewState("permisoRegistroyEdicion"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState.Remove("permisoRegistroyEdicion")
            ViewState("permisoRegistroyEdicion") = value
        End Set
    End Property
    Public Property VSPermisoEdicion() As Boolean
        Get
            Return CType(ViewState("permisoEdicion"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState.Remove("permisoEdicion")
            ViewState("permisoEdicion") = value
        End Set
    End Property
    Public Property VSPermisoEdicionSaldo() As Boolean
        Get
            Return CType(ViewState("permisoEdicionSaldo"), Boolean)
        End Get
        Set(ByVal value As Boolean)
            ViewState.Remove("permisoEdicionSaldo")
            ViewState("permisoEdicionSaldo") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                'PanelPadre es el panel de busqueda de personas
                'Me.PanelPadre.Visible = False
                ValidarPermisos()
                'Falta poner y validar cboEmpresa(validar el selectindexchanged)
                Combo.LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, SIdUsuario, False)
                Combo.LlenarCboMoneda(Me.cboMoneda)
                Combo.LlenarCboTiendaxIdEmpresaxIdUsuario(cboTienda, gIdEmpresa, SIdUsuario, False)
                'Porque el uso del cero esta reservado para hallar la cuenta gral. de la empresa

                'La sgte linea almacena el color del texto, que va a cambiar segun sea su valor
                'el saldo > que 0 estara en azul, sino estara en rojo
                VSFontColor = lblCtaTiendaDisponible.ForeColor
                informacion_cuentas_empresa_y_tienda()
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub ValidarPermisos()

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(SIdUsuario, New Integer() {162, 163, 164, 261})

        VSPermisoRegistroyEdicion = True
        VSPermisoEdicion = True
        VSPermisoEdicionSaldo = True

        If listaPermisos(0) > 0 Then    '********* REGISTRAR Y EDITAR
        Else
            btnNuevo.Visible = False
            VSPermisoRegistroyEdicion = False
            If listaPermisos(1) > 0 Then    '********* EDITAR
            Else
                VSPermisoEdicion = False
                pnlCuenta.Visible = False
                pnlInfoCtaTienda.Visible = False

                If listaPermisos(2) > 0 Then    '********* EDITAR Saldos
                Else
                    VSPermisoEdicionSaldo = False
                    btnGuardar.Visible = False
                End If
            End If
        End If

        If listaPermisos(3) > 0 Then  '********* REGISTRAR
            Me.chkCuentaFormal.Enabled = True
        Else
            Me.chkCuentaFormal.Enabled = False
        End If

    End Sub

#Region "Procedimientos auxiliares"

    Protected Sub informacion_cuentas_empresa_y_tienda()
        Try
            Dim cet As New Negocio.CuentaEmpresaTienda

            CuentaEmpTienda = cet.SelectxIdEmpresaxIdTiendaxIdMoneda(gIdEmpresa, _
                CInt(cboTienda.SelectedValue), CInt(cboMoneda.SelectedValue))
            If CuentaEmpTienda.IdEmpresa <> 0 Then

                'CuentaEmpresa = cet.SelectxIdEmpresaxIdTiendaxIdMoneda(gIdEmpresa, _
                '    0, CInt(cboMoneda.SelectedValue))

                pSaldoTienda = CuentaEmpTienda.cet_Saldo
                pLineaCreditoTienda = CuentaEmpTienda.cet_MontoMax
                pCargoMaxCtasxTienda = CuentaEmpTienda.CargoMaxCtasxTienda

                VSCuentasDisponibles = CuentaEmpTienda.CuentasDisponibles

                lblCtaTiendaDisponible.Text = CuentaEmpTienda.cstrMoneda + " " + Math.Round(pSaldoTienda, 2).ToString

                lblCtaTiendaLineaCredito.Text = CuentaEmpTienda.cstrMoneda + " " + Math.Round(pLineaCreditoTienda, 2).ToString

                lblTiendaCargoMaxCtas.Text = CuentaEmpTienda.cstrMoneda + " " + Math.Round(pCargoMaxCtasxTienda, 2).ToString


                'lblCtaGralDisponible.Text = CuentaEmpresa.cstrMoneda + " " + CuentaEmpresa.cet_Saldo.ToString("F")
                'lblCtaGralNroCtasDisponibles.Text = CStr(CuentaEmpresa.CuentasDisponibles)
            Else
                pSaldoTienda = 0
                pLineaCreditoTienda = 0
                pCargoMaxCtasxTienda = 0
                VSCuentasDisponibles = 0


                lblCtaTiendaDisponible.Text = Me.cboMoneda.SelectedItem.Text + " " + Math.Round(pSaldoTienda, 2).ToString
                lblCtaTiendaLineaCredito.Text = Me.cboMoneda.SelectedItem.Text + " " + Math.Round(pLineaCreditoTienda, 2).ToString
                lblTiendaCargoMaxCtas.Text = Me.cboMoneda.SelectedItem.Text + " " + Math.Round(pCargoMaxCtasxTienda, 2).ToString

            End If

            lblCtaTiendaNroCtasDisponibles.Text = CStr(VSCuentasDisponibles)


            If pSaldoTienda <= 0 Then
                lblCtaTiendaDisponible.ForeColor = Drawing.Color.Red
            Else
                lblCtaTiendaDisponible.ForeColor = VSFontColor
            End If

            If VSCuentasDisponibles <= 0 Then
                lblCtaTiendaNroCtasDisponibles.ForeColor = Drawing.Color.Red
            Else
                lblCtaTiendaNroCtasDisponibles.ForeColor = VSFontColor
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Public Sub limpiar()
        pIdPersona = 0
        txtPersona.Text = ""
        txtIdPersona.Text = ""
        txtDNI.Text = ""
        txtRUC.Text = ""
        Me.txtCargoMaximo.Text = ""
        Me.chkCuentaFormal.Checked = False
        Me.dgvCuentaxPersona.DataBind()
        Me.gvwCxCxDocumento.DataBind()
        Me.gvwDetalleAbonos.DataBind()
        SListaCxP = New List(Of Entidades.CuentaPersona)
        cboTienda.Enabled = True
    End Sub

    Public Sub validarxIdTienda()
        'Aqui se valida si una persona tiene cuentas, y segun eso se habilita el combo de tiendas. Cuando no tiene cuentas; puede elegir la tienda en donde se crearan cuentas. Si tuviera una cuenta, el combo señalaria a la tienda en la cual se tiene esa cuenta, y el combo seria desactivado
        Dim bool_cboTienda As Boolean
        bool_cboTienda = True    'Valor por defecto

        Try
            For i As Integer = 0 To SListaCxP.Count - 1
                If SListaCxP(i).IdTienda <> 0 Then
                    cboTienda.SelectedValue = CStr(SListaCxP(i).IdTienda)
                    bool_cboTienda = False
                    Exit For
                End If
            Next
            cboTienda.Enabled = bool_cboTienda
            'cboEmpresa.Enabled = bool_cboTienda
            HabilitarEdicion(True)
        Catch ex As Exception
            'Normalmente esto ocurre cuando desde buscar se accede 
            'a una persona que tiene una cuenta en una tienda diferente
            'a las tiendas asignadas al usuario
            objScript.mostrarMsjAlerta(Me, "Ud. no puede modificar esta cuenta")
            'Se debe de enabled =false el panel de ingreso de datos y de la grilla
            HabilitarEdicion(False)
        End Try
    End Sub

    Public Sub HabilitarEdicion(ByVal flag As Boolean)
        If VSPermisoRegistroyEdicion Or VSPermisoEdicion Then
            pnlInformacionCuentas.Enabled = flag
            pnlInfoCtaTienda.Visible = flag
        Else
            pnlInformacionCuentas.Visible = False
            pnlInfoCtaTienda.Visible = False
        End If
        pnlGrillaCuentaxPersona.Enabled = flag
    End Sub

    Private Sub cargarDatosCuentaPersona(ByVal grilla As GridView)
        Dim obj As New Negocio.CuentaPersona
        'SListaCxP = obj.SelectxIdPersona(CInt(ViewState.Item("IdPersona")))
        SListaCxP = obj.SelectxIdPersona(pIdPersona)
        grilla.DataSource = SListaCxP
        grilla.DataBind()
        validarxIdTienda()
    End Sub


#End Region

#Region "Botones Principales"

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        If pIdPersona > 0 Then
            GrabarCuentaPersona()
            'si graba los datos vuelve a obtener los datos grabado
            cargarDatosCuentaPersona(Me.dgvCuentaxPersona)
            informacion_cuentas_empresa_y_tienda()
            validarxIdTienda()
        Else
            objScript.mostrarMsjAlerta(Me, "No se puede grabar, no hay una persona seleccionada")
        End If

    End Sub

    Private Sub GrabarCuentaPersona()
        Try
            ''Dim x As Integer = CInt(ViewState.Item("IdPersona"))
            'Dim x As Integer = pIdPersona

            '********** obtengo la lista de la grilla
            'Dim lista As List(Of Entidades.CuentaPersona) = ObtenerListaCuentaPersonaFrmGrilla()

            '************ obtengo la lista de cada Persona y va
            'Dim listaCuentaPersona As New List(Of Entidades.CuentaPersona)
            'For i As Integer = 0 To lista.Count - 1
            '    Dim obj As New Entidades.CuentaPersona(lista(i).IdPersona, lista(i).IdMoneda, lista(i).CargoMaximo, lista(i).Saldo, lista(i).Estado, lista(i).CuentaFormal)
            '    listaCuentaPersona.Add(obj)
            'Next

            '********** actualizo la lista con los datos de la grilla
            ActualizarListaCuentaPersonaFrmGrilla()
            '***************** registro la lista
            Dim objCuentaPersonaNegocio As New Negocio.CuentaPersona
            'If objCuentaPersonaNegocio.GrabarCuentaPersona(lista, CInt(ViewState.Item("IdPersona"))) Then


            If objCuentaPersonaNegocio.GrabarCuentaPersona(SListaCxP, pIdPersona) Then
                objScript.mostrarMsjAlerta(Me, "El Proceso Finalizó Con Éxito.")
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAgregarCuentaPersona_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarCuentaPersona.Click
        'addCuentaPersonaGrilla(CInt(cboMoneda.SelectedValue), CDec(txtCargoMaximo.Text.Trim))
        addCuentaPersonaGrilla()
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        limpiar()
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        limpiar()
    End Sub

#End Region

#Region "Panel Cuentas - Eventos"
    Protected Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Combo.LlenarCboTiendaxIdEmpresaxIdUsuario(cboTienda, gIdEmpresa, SIdUsuario, False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTienda.SelectedIndexChanged
        informacion_cuentas_empresa_y_tienda()
    End Sub

    Protected Sub chkCuentaFormal_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkCuentaFormal.CheckedChanged
        If pIdPersona > 0 Then
            Me.validarxIdTienda()
            If cboTienda.Enabled Then   'Esto significa que paso la validacion anterior
                cboTienda.Enabled = Not (Me.chkCuentaFormal.Checked)
            End If
        Else
            cboTienda.Enabled = Not (Me.chkCuentaFormal.Checked)
        End If

        'cboEmpresa.Enabled = Not (Me.chkCuentaFormal.Checked)
        'Las sgtes 2 lineas es para poner un color de desactivado a los paneles
        ''pnlInfoCtaGral.Enabled = (Me.chkCuentaFormal.Checked)
        pnlInfoCtaTienda.Visible = Not (Me.chkCuentaFormal.Checked)
    End Sub

    Protected Sub cboMoneda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboMoneda.SelectedIndexChanged
        informacion_cuentas_empresa_y_tienda()
    End Sub
#End Region


#Region "Mantenimiento de la Grilla"

    'funcion q captura los valores de la fila de la grilla dgvCuentaxPersona y lo add al la lista
    'Private Function ObtenerListaCuentaPersonaFrmGrilla() As List(Of Entidades.CuentaPersona)
    '    Dim lista As New List(Of Entidades.CuentaPersona)
    '    For i As Integer = 0 To dgvCuentaxPersona.Rows.Count - 1
    '        With dgvCuentaxPersona.Rows(i)
    '            Dim estado As String = "0"
    '            Dim ctaformal As Boolean
    '            If CType(.Cells(4).FindControl("chbEstado"), CheckBox).Checked Then
    '                estado = "1"
    '            End If
    '            ctaformal = CType(.Cells(6).FindControl("chbCtaFormal"), CheckBox).Checked

    '            Dim txtcargo As TextBox = (CType(.Cells(4).FindControl("txtCargoMaximo"), TextBox))
    '            'para cada fila utiliza el constructor para asignarle los valores de la grilla ala lista
    '            Dim obj As New Entidades.CuentaPersona(CInt(ViewState.Item("IdPersona")), CInt(.Cells(1).Text), (.Cells(2).Text), CDec(txtcargo.Text), estado, CInt(.Cells(5).Text), ctaformal)
    '            ' add a obj IdCuentaPersona  sin contructor 
    '            obj.IdCuentaPersona = CInt(dgvCuentaxPersona.Rows(i).Cells(8).Text)

    '            lista.Add(obj)
    '        End With
    '    Next
    '    Return lista
    'End Function
    Private Sub ActualizarListaCuentaPersonaFrmGrilla()
        Dim idpersona_grilla As Integer
        Dim idmoneda_grilla As Integer
        For i As Integer = 0 To dgvCuentaxPersona.Rows.Count - 1
            With dgvCuentaxPersona.Rows(i)
                idpersona_grilla = CInt(CType(.Cells(0).FindControl("hddIdCuentaPersona"), HiddenField).Value.Trim())
                idmoneda_grilla = CInt(CType(.Cells(0).FindControl("hddIdMoneda"), HiddenField).Value.Trim())
                For j As Integer = 0 To SListaCxP.Count - 1
                    If (SListaCxP(j).IdCuentaPersona = idpersona_grilla And _
                        SListaCxP(j).IdMoneda = idmoneda_grilla) Then

                        SListaCxP(j).Estado = CStr(IIf(CType(.Cells(5).FindControl("chbEstado"), CheckBox).Checked, "1", "0"))
                        SListaCxP(j).CuentaFormal = CType(.Cells(4).FindControl("chbCtaFormal"), CheckBox).Checked
                        SListaCxP(j).CargoMaximo = CDec((CType(.Cells(2).FindControl("txtCargoMaximo_grilla"), TextBox)).Text)
                        'SListaCxP(j).IdCuentaPersona = CInt(dgvCuentaxPersona.Rows(i).Cells(8).Text)
                        'SListaCxP(j).IdPersona = CInt(ViewState.Item("IdPersona"))
                        SListaCxP(j).IdPersona = pIdPersona
                        'SListaCxP(j).Saldo = CDec((CType(.Cells(3).FindControl("txtSaldo_grilla"), TextBox)).Text)
                        SListaCxP(j).Saldo = CDec(.Cells(3).Text)
                        SListaCxP(j).cp_ObServ = CStr((CType(.Cells(6).FindControl("txtObserv_grilla"), TextBox)).Text)

                        'Esta funcion es llamada cuando se graba, asi que se debe guardar a quien edito las cuentas de esta persona
                        SListaCxP(j).IdSupervisor = SIdUsuario

                        If Not SListaCxP(j).CuentaFormal Then
                            SListaCxP(j).IdTienda = CInt(cboTienda.SelectedValue)
                        End If
                    End If
                Next
            End With
        Next
    End Sub

    Private Sub addCuentaPersonaGrilla()
        Try
            'Dim lista As List(Of Entidades.CuentaPersona) = ObtenerListaCuentaPersonaFrmGrilla()

            'agrega un registro a la lista (captura los valores y los asigna a la lista por medio del constructor y la lista add a la grilla)
            'Dim obj As New Entidades.CuentaPersona(CInt(ViewState.Item("IdPersona")), CInt(Me.cboMoneda.SelectedValue), (Me.cboMoneda.SelectedItem.Text), CDec(Me.txtCargoMaximo.Text), "1", 0, chkCuentaFormal.Checked)
            'Dim fechaHoy As String = (New Negocio.FechaActual).SelectFechaActual("YYYY/MM/DD").toString
            '''''falta agregar fecha
            Dim objFechaActual As New Negocio.FechaActual
            Dim fechaActual As String = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")

            Dim obj As New Entidades.CuentaPersona(pIdPersona, CInt(Me.cboMoneda.SelectedValue), (Me.cboMoneda.SelectedItem.Text), CDec(Me.txtCargoMaximo.Text), "1", CDec(Me.txtCargoMaximo.Text), Me.txtObs.Text, fechaActual, chkCuentaFormal.Checked)

            obj.IdEmpresa = gIdEmpresa
            If Not chkCuentaFormal.Checked Then
                obj.IdTienda = CInt(cboTienda.SelectedValue)
            End If

            Dim validacion As Integer
            validacion = 1 'Ok

            If obj.CuentaFormal = False Then
                If pSaldoTienda < obj.CargoMaximo Then
                    validacion = -1
                Else
                    If VSCuentasDisponibles <= 0 Then
                        validacion = -2
                    End If
                End If
            End If


            Select Case validacion
                Case 1
                    SListaCxP.Add(obj)
                    'lista.Add(obj)

                    'dgvCuentaxPersona.DataSource = lista
                    dgvCuentaxPersona.DataSource = SListaCxP
                    dgvCuentaxPersona.DataBind()

                    validarxIdTienda()
                Case -1
                    objScript.mostrarMsjAlerta(Me, "El Cargo Máximo debe de ser menor o igual que el Saldo Disponible.")
                Case -2
                    objScript.mostrarMsjAlerta(Me, "No hay Cuentas disponibles.")

            End Select

        Catch ex As Exception
        Finally
        End Try
    End Sub
    Private Sub quitarRegistroCuentaPersona(ByVal cuenta As String, ByVal DelFila As Integer)
        Dim objUtil As New Negocio.Util
        Try
            '****************** validamos que no tenga registros relacionados
            If objUtil.ValidarxDosParametros("MovCuenta", "IdPersona", txtIdPersona.Text, "IdCuentaPersona", cuenta) > 0 Then
                objScript.mostrarMsjAlerta(Me, "La cuenta no puede ser eliminada por que posee movimientos relacionados.")
                Return
            End If
            '**************** quitamos de la lista
            'Dim lista As List(Of Entidades.CuentaPersona) = ObtenerListaCuentaPersonaFrmGrilla()
            'lista.RemoveAt(DelFila)

            SListaCxP.RemoveAt(DelFila)
            'dgvCuentaxPersona.DataSource = lista

            dgvCuentaxPersona.DataSource = SListaCxP
            dgvCuentaxPersona.DataBind()

            validarxIdTienda()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminación del registro.")
        End Try
    End Sub

    Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim lbtnQuitar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
        Dim fila As GridViewRow
        fila = CType(lbtnQuitar.NamingContainer, GridViewRow)
        Me.quitarRegistroCuentaPersona(fila.Cells(6).Text, fila.RowIndex)

    End Sub

#End Region
#Region "Mostrar Documento"

    Private Function mostrardocumento(ByVal idpersona As Integer, ByVal idmoneda As Integer) As List(Of Entidades.CxCResumenView)
        Dim listactaperona As List(Of Entidades.CxCResumenView) = Nothing

        If (idpersona > 0) Then
            Me.lblMensajeDocumento.Text = ""
            listactaperona = Me.objctaPersona.SelectGrillaCxCResumenDocumento(idpersona, idmoneda)
            If listactaperona.Count = 0 Then
                Me.lblMensajeDocumento.Text = "No Tiene Documento"
            End If
        Else
            objScript.mostrarMsjAlerta(Me, "La Persona no Existe.")
        End If
        Return listactaperona
    End Function
    Protected Sub lbtnMostrar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lbtnMostrar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lbtnMostrar.NamingContainer, GridViewRow)
            'mostrar detalle documento
            Me.gvwCxCxDocumento.DataSource = Me.mostrardocumento(pIdPersona, CInt(CType(fila.Cells(0).FindControl("hddIdMoneda"), HiddenField).Value.Trim()))
            gvwCxCxDocumento.DataBind()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnHistorial_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lbtnMostrar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lbtnMostrar.NamingContainer, GridViewRow)

            gvwCtaPersHistorico.DataSource = Nothing
            gvwCtaPersHistorico.DataBind()

            gvwCtaPersHistorico.DataSource = objctaPersona.SelectxIdCtaPersona(CInt(CType(fila.Cells(0).FindControl("hddIdCuentaPersona"), HiddenField).Value.Trim()))
            gvwCtaPersHistorico.DataBind()

            'CInt(CType(fila.Cells(0).FindControl("hddIdCuentaPersona"), HiddenField).Value.Trim())

            objScript.onCapa(Me, "CapaHistPersona")


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "No Tiene Detalle.")
        End Try

    End Sub

#End Region
#Region "Mostrar Detalles Abono"


    Private Sub gvwCxCxDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvwCxCxDocumento.SelectedIndexChanged
        LevantaCapaDetalleAbono(CInt(gvwCxCxDocumento.SelectedRow.Cells(1).Text))
    End Sub

    Private Sub LevantaCapaDetalleAbono(ByVal iddocumento As Integer)
        Try
            'nroDias
            Dim objDetAbono As List(Of Entidades.CxCResumenView) = (New Negocio.CXCResumenView).GetDetalleAbonos(iddocumento)

            For Each _Row As Entidades.CxCResumenView In objDetAbono
                If (_Row.nroDias <= 0) Then
                    _Row.nroDias = 0
                End If
            Next
            Me.gvwDetalleAbonos.DataSource = objDetAbono
            Me.gvwDetalleAbonos.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "onCapaDetalleAbonos();", True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "No Tiene Detalle.")
        End Try
    End Sub
#End Region

#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        listaPersona = New List(Of Entidades.CuentaPersona)

        Select Case ddl_CuentaTienda.SelectedValue
            Case "0"
                listaPersona = (New Negocio.CuentaPersona).CuentaPersonaCtaEmpresaTienda(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado, 0, 0)
            Case "Empresa"
                listaPersona = (New Negocio.CuentaPersona).CuentaPersonaCtaEmpresaTienda(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado, CInt(cboEmpresa.SelectedValue), 0)
            Case "Tienda"
                listaPersona = (New Negocio.CuentaPersona).CuentaPersonaCtaEmpresaTienda(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado, CInt(cboEmpresa.SelectedValue), CInt(cboTienda.SelectedValue))
        End Select

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO

            ViewState.Add("rol", 1)
            'Select Case CInt(Me.hddOpcionBusquedaPersona.Value)

            '    Case 0  '****** REMITENTE
            '        ViewState.Add("rol", 0)
            '    Case 1  '****** DESTINATARIO
            '        ViewState.Add("rol", 0)  '*** TODOS LOS ROLES
            '    Case 2 '******* TRANSPORTISTA
            '        ViewState.Add("rol", 4)
            '    Case 3 '******* CHOFER

            'End Select

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBuscar.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If listaPersona(e.Row.RowIndex).cadMoneda = "" Then
                CType(e.Row.FindControl("ddlMoneda"), DropDownList).Visible = False
            End If
            If listaPersona(e.Row.RowIndex).cadTienda = "" Then
                CType(e.Row.FindControl("ddlTienda"), DropDownList).Visible = False
            End If
            If listaPersona(e.Row.RowIndex).cadCargoMaximo = "" Then
                CType(e.Row.FindControl("ddlCargoMax"), DropDownList).Visible = False
            End If
            If listaPersona(e.Row.RowIndex).cadDisponible = "" Then
                CType(e.Row.FindControl("ddlDisponible"), DropDownList).Visible = False
            End If
            If listaPersona(e.Row.RowIndex).cadSaldo = "" Then
                CType(e.Row.FindControl("ddlSaldo"), DropDownList).Visible = False
            End If

        End If
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try
            limpiar()
            Me.lblMensajeDocumento.Text = ""
            Me.gvwCxCxDocumento.DataSource = Nothing
            gvwCxCxDocumento.DataBind()
            Me.gvwDetalleAbonos.DataSource = Nothing
            gvwDetalleAbonos.DataBind()

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))
            cargarDatosCuentaPersona(dgvCuentaxPersona)
            informacion_cuentas_empresa_y_tienda()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")

        Me.txtPersona.Text = objPersona.Descripcion
        Me.txtIdPersona.Text = CStr(objPersona.IdPersona)
        Me.txtDNI.Text = objPersona.Dni
        Me.txtRUC.Text = objPersona.Ruc
        'Me.hddIdPersona.Value = CStr(objPersona.IdPersona)
        pIdPersona = objPersona.IdPersona

        objScript.offCapa(Me, "capaPersona")

    End Sub

#End Region

    Protected Sub dgvCuentaxPersona_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvCuentaxPersona.RowDataBound
        Try
            Dim i As Integer
            If Not VSPermisoRegistroyEdicion Then
                If Not VSPermisoEdicion Then
                    'i=0 apunta a un link que muestra otra tabla, no debe ser desactivado              
                    For i = 1 To e.Row.Cells.Count - 1
                        If i <> 4 Then
                            e.Row.Cells(i).Enabled = False
                        Else
                            If Not VSPermisoEdicionSaldo Then
                                'Se desactiva la edicion de saldos
                                e.Row.Cells(4).Enabled = False
                            End If
                        End If
                    Next
                End If
            End If
            If e.Row.RowType = DataControlRowType.DataRow And e.Row.RowState = DataControlRowState.Normal Or _
            e.Row.RowState = DataControlRowState.Alternate Or e.Row.RowState = DataControlRowState.Selected Then
                Dim sm As ScriptManager
                sm = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                sm.RegisterPostBackControl(DirectCast(e.Row.FindControl("btnHistorial"), LinkButton))
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try



        Try

            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(SIdUsuario, New Integer() {261, 262, 164, 264})

                If listaPermisos(0) <= 0 Then
                    CType(e.Row.FindControl("chbCtaFormal"), CheckBox).Enabled = False
                End If

                If listaPermisos(1) <= 0 Then
                    CType(e.Row.FindControl("chbEstado"), CheckBox).Enabled = False
                End If

                If listaPermisos(2) <= 0 Then ' ************** CUENTA FORMAL

                    If CType(e.Row.FindControl("chbCtaFormal"), CheckBox).Checked Then
                        CType(e.Row.FindControl("txtCargoMaximo_grilla"), TextBox).Enabled = False
                    End If

                End If

                If listaPermisos(3) <= 0 Then ' ************** CUENTA INFORMAL
                    If CType(e.Row.FindControl("chbCtaFormal"), CheckBox).Checked = False Then
                        CType(e.Row.FindControl("txtCargoMaximo_grilla"), TextBox).Enabled = False
                    End If
                End If


            End If

        Catch ex As Exception

        End Try



    End Sub


End Class