﻿'*************************************************
'Autor   : Nagamine Molina, Jorge
'Sistema : SIGE
'Empresa : Digrafic SRL
'Fecha   : 27-Oct-2009
'Hora    : 05:24 pm
'*************************************************



Partial Public Class AdministracionSistema
    Inherits System.Web.UI.MasterPage

    '*************************************************
    'Autor   : Nagamine Molina, Jorge
    'Sistema : SIGE
    'Empresa : Digrafic SRL
    'Fecha   : 15-Oct-2009
    'Hora    : 12:37 pm
    '*************************************************


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim LPerfil As New List(Of Entidades.Perfil)
        LPerfil = CType(Session("LPerfil"), List(Of Entidades.Perfil))
        If LPerfil Is Nothing Then
            Dim sc As New ScriptManagerClass
            sc.mostrarMsjAlerta(Page, "La sesión ha sido finalizada")
            Return
        End If

        Dim Perfil As New Entidades.Perfil
        For Each Perfil In LPerfil
            If Perfil.IdPerfil = 5 Then
                For i As Integer = 0 To Me.Menu1.Items(0).ChildItems.Count - 1
                    Me.Menu1.Items(0).ChildItems(i).Enabled = True
                Next
                Exit For
            ElseIf Perfil.IdPerfil = 1 Then
                Me.Menu1.Items(0).ChildItems(0).Enabled = True
            ElseIf Perfil.IdPerfil = 2 Then
                Me.Menu1.Items(0).ChildItems(1).Enabled = True
            ElseIf Perfil.IdPerfil = 3 Then
                Me.Menu1.Items(0).ChildItems(2).Enabled = True
            End If
        Next
        Session.Timeout = 60
    End Sub

    Protected Sub btnCerrarSesion_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCerrarSesion.Click
        Try
            Session.Clear()
            Session.Abandon()
            FormsAuthentication.SignOut()
            FormsAuthentication.RedirectToLoginPage()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class