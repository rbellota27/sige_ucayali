﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmConfigTipoCambio
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            inicializarFrm()

        End If
    End Sub

    Private Sub inicializarFrm()

        Try

            '********************* cargo el tipo de cambio vigente DOLARES
            Dim objTipoCambio As Entidades.TipoCambio = (New Negocio.TipoCambio).SelectVigentexIdMoneda(2)

            txtCompraC.Text = Math.Round(objTipoCambio.CompraC, 2).ToString
            txtCompraOf.Text = Math.Round(objTipoCambio.CompraOf, 2).ToString
            txtVentaC.Text = Math.Round(objTipoCambio.VentaC, 2).ToString
            txtVentaOf.Text = Math.Round(objTipoCambio.VentaOf, 2).ToString

            '************************ cargamos la grilla
            Dim listaC As List(Of Entidades.ConfigTipoCambio) = (New Negocio.ConfigTipoCambio).SelectAll

            DGV_ConfigTipoCambio.DataSource = listaC
            DGV_ConfigTipoCambio.DataBind()

            If listaC.Count > 0 Then

                lblUsuario.Text = listaC(0).getNombreAMostrar

            End If


        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")

        End Try

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click

        actualizarConfigTipoCambio()



    End Sub

    Private Sub actualizarConfigTipoCambio()

        Try

            Dim lista As New List(Of Entidades.ConfigTipoCambio)

            For i As Integer = 0 To DGV_ConfigTipoCambio.Rows.Count - 1

                Dim objConfig As New Entidades.ConfigTipoCambio

                With objConfig

                    .IdConfigTipoCambio = CInt(DGV_ConfigTipoCambio.Rows(i).Cells(0).Text)
                    .CompraC = CType(DGV_ConfigTipoCambio.Rows(i).Cells(2).FindControl("chb_CompraC"), CheckBox).Checked
                    .VentaC = CType(DGV_ConfigTipoCambio.Rows(i).Cells(3).FindControl("chb_VentaC"), CheckBox).Checked
                    .CompraOf = CType(DGV_ConfigTipoCambio.Rows(i).Cells(4).FindControl("chb_CompraOf"), CheckBox).Checked
                    .VentaOf = CType(DGV_ConfigTipoCambio.Rows(i).Cells(5).FindControl("chb_VentaOf"), CheckBox).Checked

                    If Session("IdPersona") IsNot Nothing Then

                        .IdUsuario = CInt(Session("IdPersona").ToString)

                    End If


                End With

                lista.Add(objConfig)

            Next

            If (New Negocio.ConfigTipoCambio).ConfigTipoCambioUpdate(lista) Then

                objScript.mostrarMsjAlerta(Me, "La Actualización se realizó con éxito.")

            Else

                Throw New Exception

            End If

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, "Problemas en la actualización de los registros.")

        End Try


    End Sub
End Class