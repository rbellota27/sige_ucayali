<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmCuentaProveedor.aspx.vb" Inherits="APPWEB.FrmCuentaProveedor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" language="javascript">
    function valSave() {
        var caja = document.getElementById('<%=txtCargoMaximo.ClientID %>');
        if (caja.value.length == 0 || !esDecimal(caja.value) || parseFloat(caja.value) <= 0) {
            caja.focus();
            caja.select();
            alert('Debe ingresar un valor.');
            return false;
        }
     
        return (confirm('Desea continuar con la operaci�n?'));
    }
    function validarNumeroPunto(elEvento) {
        //obtener caracter pulsado en todos los exploradores
        var evento = elEvento || window.event;
        var caracter = evento.charCode || evento.keyCode;
        //alert("El car�cter pulsado es: " + caracter);
        if (caracter == 46) {
            return true;
        } else {
            if (!onKeyPressEsNumero('event')) {
                return false;
            }
        }
        
        
    }
    function valCajaBlur() {
        var id = event.srcElement.id;
        var caja = document.getElementById(id);
        if (!esDecimal(caja.value)) {
            caja.select();
            caja.focus();
            alert('Valor no v�lido.');
        }
    }       
</script>

<asp:UpdatePanel ID="UpdatePanel" runat="server">
    <ContentTemplate>
        <table class="style1">
            <tr>
                <td style="width: 920px">
       
                    <table class="style1">
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" 
                                    onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" 
                                    onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" Visible="False" />
                                <asp:ImageButton ID="btnGuardar" runat="server" CausesValidation="true" 
                                    ImageUrl="~/Imagenes/Guardar_B.JPG" 
                                    onmouseout="this.src='/Imagenes/Guardar_B.JPG';" 
                                    onmouseover="this.src='/Imagenes/Guardar_A.JPG';" Visible="False" />
                                <asp:ImageButton ID="btnCancelar" runat="server" 
                                    ImageUrl="~/Imagenes/Cancelar_B.JPG" 
                                    onmouseout="this.src='/Imagenes/Cancelar_B.JPG';" 
                                    onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" Visible="False" />
                            </td>
                        </tr>
                    </table>
       
                </td>
            </tr>
            <tr>
                <td style="width: 920px">
                    <table style="width: 100%">
                        <tr>
                            <td class="TituloCelda">Asignaci�n De Credito </td>
                        </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td style="width: 920px">
                    <table class="style1">
                        <tr>
                            <td style="width: 132px; text-align: right">
                                Proveedor:</td>
                            <td style="width: 341px">
                                <asp:TextBox ID="txtPersona" runat="server" CssClass="TextBoxReadOnly" 
                                    ReadOnly="true" Width="338px" Enabled="False"></asp:TextBox>
                            </td>
                            <td style="width: 94px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" 
                                    ImageUrl="~/Imagenes/Buscar_B.JPG" 
                                    onclientclick="return( valOnClickBuscarCliente()  );" 
                                    onmouseout="this.src='/Imagenes/Buscar_B.JPG';" 
                                    onmouseover="this.src='/Imagenes/Buscar_A.JPG';" Visible="True" />
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: right">
                            Ruc:
                            </td>
                            <td>                                                              
                                <asp:TextBox ID="txtRuc" runat="server" CssClass="TextBoxReadOnly" 
                                    ReadOnly="true" Width="118px" Enabled="False"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtIdProveedor" runat="server" CssClass="TextBoxReadOnly" 
                                    ReadOnly="true" Width="39px" Enabled="False" Visible="False"></asp:TextBox>
                            </td>
                            <td>
                             </td>
                            <td>
                            </td>
                            <td>
                             </td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: right; height: 26px;">
                                Empresa:</td>
                            <td style="width: 341px; height: 26px;">
                                <asp:DropDownList ID="CboPropietario" runat="server" AutoPostBack="True" 
                                    Enabled="False">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 94px; height: 26px;">
                                </td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtId" runat="server" onblur="return(valBlur(event));" 
                                    onKeypress="return(validarNumeroPunto(event));" Width="36px" Visible="False"></asp:TextBox>
                            </td>
                            <td style="height: 26px">
                                </td>
                            <td style="height: 26px">
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: right">
                                Moneda:</td>
                            <td style="width: 341px">
                                <asp:DropDownList ID="cboMoneda" runat="server" DataTextField="Simbolo" 
                                    DataValueField="Id">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 94px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: right">
                                Monto:</td>
                            <td style="width: 341px">
                                <asp:TextBox ID="txtCargoMaximo" runat="server" onblur="return(valBlur(event));" 
                                    onKeypress="return(validarNumeroPunto(event));" Enabled="False"></asp:TextBox>
                            </td>
                            <td style="width: 94px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: right">
                                Saldo:</td>
                            <td style="width: 341px">
                                <asp:TextBox ID="txtSaldo" runat="server" onblur="return(valBlur(event));" 
                                    Enabled="False"></asp:TextBox>
                            </td>
                            <td style="width: 94px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: right">
                                Estado:</td>
                            <td style="width: 341px">
                                <asp:RadioButtonList ID="RblEstado" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td style="width: 94px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 920px">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 920px">
                    <table width="100%">
                        <tr>
                            <td style="text-align: right">
                                <asp:GridView ID="dgvProveedor" runat="server" AllowPaging="True" 
                                    AutoGenerateColumns="False" ForeColor="#333333" Height="16px" 
                                    Style="text-align: left" Width="105%">
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <Columns>
                                   
                                        <asp:CommandField EditText="Seleccionar" SelectText="Editar" 
                                            ShowSelectButton="True" />
                                        <asp:BoundField DataField="IdMoneda" HeaderText="IdMoneda" 
                                         NullDisplayText="---"/>
                                        <asp:BoundField DataField="mon_Simbolo" HeaderText="Moneda" 
                                            NullDisplayText="---" />
                                        <asp:BoundField DataField="cprov_CargoMax" HeaderText="Monto" 
                                            NullDisplayText="---" />
                                        <asp:BoundField DataField="cprov_Saldo" HeaderText="Saldo" 
                                            NullDisplayText="---" />
                                        <asp:BoundField DataField="DescEstado" HeaderText="Estado" 
                                            NullDisplayText="---" />
                                        <asp:BoundField DataField="IdCuentaProv" HeaderText="Id" 
                                            NullDisplayText="---" />
                                        <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnMostrar" runat="server" OnClick="lbtnMostrar_Click">Ver Detalle</asp:LinkButton>
                                            <asp:HiddenField ID="hddIdMoneda" Value='<%# DataBinder.Eval(Container.DataItem,"IdMoneda") %>' runat="server" />
                                            <asp:HiddenField ID="hddIdCuentaProveedor" Value='<%# DataBinder.Eval(Container.DataItem,"IdCuentaProv") %>' runat="server" />                                        
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>                   
                </tr>
            <tr>
                <td style="width: 920px">
                    <table class="style1" style="width: 98%">
                    <tr>
                        <td class="TituloCeldaLeft">
                               Resumen Cuentas Por Cobrar Por Documentos
                        </td>
                    </tr>                   
                        <tr>
                             <td style="text-align: right">
                                <asp:GridView ID="gvwCxCxDocumento" runat="server" AllowPaging="True" 
                                    AutoGenerateColumns="False" ForeColor="#333333" Height="16px" PageSize="5" 
                                    Style="text-align: left" Width="95%">
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <Columns>
                                        <asp:BoundField DataField="idDocumento" HeaderText="Id" NullDisplayText="---" />
                                        <asp:BoundField DataField="Numero" HeaderText="N�mero" NullDisplayText="---" />
                                        <asp:BoundField DataField="tdoc_NombreCorto" HeaderText="Tipo Doc." 
                                            NullDisplayText="---" />
                                        <asp:BoundField DataField="doc_FechaEmision" DataFormatString="{0:dd/MM/yyyy}" 
                                            HeaderText="Fecha Emision" NullDisplayText="---" />
                                        <asp:BoundField DataField="doc_FechaVencimiento" HeaderText="Fecha Venc" 
                                            NullDisplayText="---" />
                                        <asp:BoundField DataField="nroDias" HeaderText="Nro  Diaz" 
                                            NullDisplayText="---" />
                                        <asp:BoundField DataField="mon_simbolo" HeaderText="Moneda" 
                                            NullDisplayText="---" />
                                        <asp:BoundField DataField="doc_TotalPagar" DataFormatString="{0:F2}" 
                                            HeaderText="Total Pagar" NullDisplayText="---" />
                                        <asp:BoundField DataField="abono" DataFormatString="{0:F2}" HeaderText="Abono" 
                                            NullDisplayText="---" />
                                        <asp:BoundField DataField="saldo" DataFormatString="{0:F2}" HeaderText="Saldo" 
                                            NullDisplayText="---" />
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                </asp:GridView>
                            </td>
                         </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblMensajeDocumento" runat="server"></asp:Label>
                                </td>
                        </tr>
                        </tr>
                        <tr>
                            <td style="width: 920px">
                                &nbsp;</td>
                        </tr>
                    <tr>
                        <td style="width: 920px">
                            <asp:HiddenField ID="hddId" runat="server" />
                            <asp:HiddenField ID="hddIdPersona" runat="server" />
                            <asp:HiddenField ID="hddMonto" runat="server" />
                            <asp:HiddenField ID="hddSaldo" runat="server" />
                        </td>
            </tr>
                    </table>
                </td>
            </tr>
        </table>
    </ContentTemplate>


</asp:UpdatePanel> 

<div id="capapersona" style="border: 3px solid blue; padding: 8px; width: 750px;
        height: auto; position: absolute; background-color: white; top: 130px; z-index: 2;
        left: 40px; display: none;">
        <table width="100%">
            <tr>
                <td align="center">
                    <table>                      
                        <tr>
                            <td class="Texto">
                                Raz&oacute;n Social / Nombres:
                            </td>
                            <td colspan="4" align="left">
                                <asp:TextBox ID="tbrazonape" onKeyPress="return(validarCajaBusqueda(event));" runat="server"
                                    Width="450px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                D.N.I.:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                    MaxLength="8" runat="server"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                R.U.C.:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                    MaxLength="11"></asp:TextBox>
                            </td>
                            <td align="left">
                                <asp:Button ID="btPersona" runat="server" Text="Buscar" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="20" Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" />
                            <asp:BoundField DataField="idpersona" HeaderText="C�digo" NullDisplayText="0" />
                            <asp:BoundField DataField="Nombre" HeaderText="Raz�n Social / Nombres" NullDisplayText="---">
                                <ItemStyle HorizontalAlign="Left" Width="500px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                        ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                    <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                        ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                    <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                        runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                            Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                    <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                        onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

   
<script language="javascript"  type="text/javascript"  >
        //************************** BUSQUEDA PERSONA
        
        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
        
                tb.select();
                tb.focus();
               return true;
        }
        function validarCajaBusquedaNumero(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function valOnClickBuscarCliente() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function SelectPersona() {
            var grilla = document.getElementById('<%=gvBuscar.ClientID%>');
            if (grilla != null) {
                var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
                var txtRUC = document.getElementById('<%=txtRUC.ClientID%>');
                var txtPersona = document.getElementById('<%=txtPersona.ClientID%>');

                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[0].id == event.srcElement.id) {
                        hddIdPersona.value = rowElem.cells[1].innerHTML;
                        txtPersona.value = rowElem.cells[2].innerHTML;
                        txtRUC.value = rowElem.cells[3].innerHTML;
                        offCapa('capaPersona');
                        return false;
                    }
                }
            }
            return false;
        }
        function valOnFocus_Cliente() {
            document.getElementById('<%=btBuscarPersonaGrilla.ClientID%>').focus();
            return false;
        }
        //*********************************************************************************** FIN BUSCAR CLIENTE

                 
//            var hddIdPersona = document.getElementById('<%=hddIdPersona.ClientID%>');
//            if (isNaN(parseInt(hddIdPersona.value)) || hddIdPersona.value.length <= 0) {
//                alert('Debe seleccionar un Cliente.');
//                return false;
//            }        
        
    </script> 
</asp:Content>
