﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO
Partial Public Class FrmReporteImagen
    Inherits System.Web.UI.Page
    Dim sc As New ScriptManagerClass
    Dim objImagen As New Entidades.ReporteImagen
    Dim ngcImagen As New Negocio.ReporteImagen
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            MostrarImagen()
        End If
    End Sub

    Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGrabar.Click
        Dim NameFile, ExtFile As String
        Dim SizeFile As Integer
        Dim arc_type As String = ""
        If Not Me.Upload.PostedFile Is Nothing And Upload.PostedFile.ContentLength > 0 Then
            Dim fn As String = System.IO.Path.GetFileName(Upload.PostedFile.FileName)
            Dim SaveLocation As String = Server.MapPath("~\Reportes\ImgRpt") & "\" & fn
            NameFile = Path.GetFileName(Me.Upload.PostedFile.FileName)
            ExtFile = Path.GetExtension(Me.Upload.PostedFile.FileName)
            SizeFile = Me.Upload.PostedFile.ContentLength
            If (ExtFile = ".jpeg" Or ExtFile = ".jpg" Or ExtFile = ".bmp" Or ExtFile = ".png" Or ExtFile = ".JPEG" Or ExtFile = ".JPG" Or ExtFile = ".BMP" Or ExtFile = ".PNP") Then
                arc_type = "V"
            Else
                arc_type = "F"
            End If
            If (arc_type = "V") Then
                GrabarImagen(NameFile)
                Upload.PostedFile.SaveAs(SaveLocation)
                sc.mostrarMsjAlerta(Me, "Se grabo correctamente y el archivo ha sido enviado.")
                MostrarImagen()
            ElseIf (arc_type = "F") Then
                sc.mostrarMsjAlerta(Me, "No se pudo subir el archivo porque solo se aceptan archivos jpg,bmp,png ..!!!")
            End If
        Else
            sc.mostrarMsjAlerta(Me, "Por favor seleccione un archivo para cargar.")
        End If

    End Sub

    Sub GrabarImagen(ByVal NameFile As String)
        Dim sFile As String = String.Empty
        Dim byteImage(0 To Upload.PostedFile.ContentLength - 1) As Byte
        Dim ImgFile As HttpPostedFile = Upload.PostedFile
        ImgFile.InputStream.Read(byteImage, 0, Upload.PostedFile.ContentLength)
        objImagen.IdImagen = 1
        objImagen.Nombre = NameFile
        objImagen.Imagen = byteImage
        ngcImagen.InsertaReporteImagen(objImagen)
    End Sub
    Sub MostrarImagen()
        objImagen = Me.ngcImagen.ListNomImagen
        Me.ImgVerRuta.ImageUrl = "~\Reportes\ImgRpt\" + objImagen.Nombre
    End Sub
End Class