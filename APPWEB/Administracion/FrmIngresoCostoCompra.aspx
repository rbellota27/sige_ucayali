<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmIngresoCostoCompra.aspx.vb" Inherits="APPWEB.FrmIngresoCostoCompra" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td class="TituloCelda">
                PRECIOS DE COMPRA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cabecera" runat="server">
                    <table>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="Texto">
                                            Tipo Existencia:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cmbTipoExistencia" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            L&iacute;nea:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cmbLinea" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            SubL&iacute;nea:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cmbSubLinea" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnAceptar_SubLinea_Cab" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../Imagenes/Aceptar_A.JPG';"
                                                OnClientClick="onClick_btnAceptar_SubLinea_Cab()" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="Texto">
                                            <asp:RadioButtonList ID="rbTipoBusqueda" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="0">C�digo</asp:ListItem>
                                                <asp:ListItem Value="1" Selected="True">Descripci�n</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_descripcion" MaxLength="90" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                                    CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                                    CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                                    CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                    CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                                    SuppressPostBack="true">
                                </cc1:CollapsiblePanelExtender>
                                <asp:Image ID="Image21_11" runat="server" Height="16px" />
                                <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label><asp:Panel
                                    ID="Panel_BusqAvanzadoProd" runat="server">
                                    <table width="100">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="Texto" style="font-weight: bold">
                                                            Atributo:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );"
                                                                Style="cursor: hand;" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" Style="cursor: hand;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                                    Width="650px">
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <Columns>
                                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                            HeaderStyle-Height="25px" ItemStyle-Height="25px" />
                                                        <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label><asp:HiddenField
                                                                    ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                                    DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                                </asp:DropDownList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <HeaderStyle CssClass="GrillaCabecera" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Grilla" runat="server">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Label ID="Label84" runat="server" CssClass="Label" Text="Moneda:"></asp:Label>
                                <asp:DropDownList ID="cmbTipoMoneda" runat="server" DataTextField="Simbolo" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:Label ID="Label85" runat="server" CssClass="Label" Text="Porcentaje (%):"></asp:Label>
                                <asp:TextBox ID="TextBox1" Width="75px" runat="server" onKeypress=" return( validarNumeroPuntoSigno(event) ); "></asp:TextBox>
                                <asp:ImageButton ID="btnAceptar_SubLinea_Cab0" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                    OnClientClick="return(updateCosto());" onmouseout="this.src='../Imagenes/Aceptar_B.JPG';"
                                    onmouseover="this.src='../Imagenes/Aceptar_A.JPG';" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                    OnClientClick="return(valSave());" onmouseout="this.src='../Imagenes/Guardar_B.JPG';"
                                    onmouseover="this.src='../Imagenes/Guardar_A.JPG';" />
                                <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/Imagenes/Arriba_B.JPG"
                                    onmouseout="this.src='../Imagenes/Arriba_B.JPG';" onmouseover="this.src='../Imagenes/Arriba_A.JPG';"
                                    ToolTip="Atr�s" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="DGV_SubLinea" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo" NullDisplayText=""
                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            HeaderText="Producto">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblProducto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"nomProducto")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="nomUMPrincipal" HeaderText="U.M." HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cmbMoneda_Producto" runat="server" DataTextField="Simbolo"
                                                    DataValueField="Id">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P.Compra Actual" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtCostoActual" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCompra","{0:F3}")%>' Width="70px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Incremento (%)" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPorcentajeAdd" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                    Text="0" Width="70px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Incremento (Valor)" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtValorAdd" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                    Text="0" Width="70px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P. Compra Nuevo" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtCostoNuevo" runat="server" onblur="return(valGrillaBlur(event));"
                                                    onFocus=" return(  aceptarFoco(this)  ); " onKeypress="return(validarNumeroPunto(event));"
                                                    TabIndex="200" onKeyup="return(calcularFila(event));" Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCompra","{0:F3}")%>'
                                                    Width="70px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Afectar" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chbNOAFECTAR" runat="server" TabIndex="210" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Texto">
                            Moneda ($):
                        </td>
                        <td class="Texto">
                            Compra:
                        </td>
                        <td class="Texto">
                            <asp:TextBox ID="txtCompraC" runat="server" CssClass="TextBoxReadOnly" ReadOnly="False"
                                onKeypress="return(false);" Width="70px"></asp:TextBox>
                        </td>
                        <td class="Texto">
                            Venta:
                        </td>
                        <td>
                            <asp:TextBox ID="txtVentaC" onKeypress="return(false);" runat="server" CssClass="TextBoxReadOnly"
                                MaxLength="9" ReadOnly="False" Width="70px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
		
        //////////////////////
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function onClick_btnAceptar_SubLinea_Cab() {
            var idLinea = document.getElementById('<%= cmbLinea.ClientID%>');
            if (idLinea == 0) {
                alert('Debe Seleccionar una l�nea');
                return false;
            }

            var idSubLinea = document.getElementById('<%= cmbSubLinea.ClientID%>');
            if (idSubLinea == 0) {
                alert('Debe Seleccionar una subl�nea');
                return false;
            }
            return true;
        }

        //////////////////////////////

        function calcularFila(e) {
            //obtener caracter pulsado en todos los exploradores
            var evento = e || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            //obtiene srcElement.Id
            var targ;
            if (!e) var e = window.event;
            if (e.target) targ = e.target;
            else if (e.srcElement) targ = e.srcElement;
            //termina

            if (caracter == 9) {
                return true;
            }
            var grilla = document.getElementById("<%= DGV_SubLinea.ClientID %>");
            if (grilla == null) {
                alert('No se han encontrado registros.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                if (rowElem.cells[7].children[0].id == e.id) {
                    if (rowElem.cells[7].children[0].value.length == 0) {
                        return false;
                    }
                    if (!esDecimal(rowElem.cells[7].children[0].value)) {
                        alert('Valor no v�lido.');
                        rowElem.cells[7].children[0].select();
                        rowElem.cells[7].children[0].focus();
                        return false;
                    }
                    var valorNew = parseFloat(rowElem.cells[7].children[0].value);
                    var valorOld = parseFloat(rowElem.cells[4].children[0].value);
                    var dif = 0;
                    var porcent = 0;
                    if (valorOld != 0) {
                        dif = valorNew - valorOld;
                        porcent = ((dif) / valorOld) * 100;
                    }
                    //imprimo
                    rowElem.cells[5].children[0].value = redondear(porcent, 4);
                    rowElem.cells[6].children[0].value = redondear(dif, 4);
                    return true;
                }
            }
        }
        function updateCosto() {
            var grilla = document.getElementById("<%= DGV_SubLinea.ClientID %>");
            var caja = document.getElementById("<%= TextBox1.ClientID %>");
            var combo = document.getElementById("<%= cmbTipoMoneda.ClientID %>");
            if (!esDecimal(caja.value)) {
                alert('Valor no v�lido.');
                return false;
            }
            if (caja.value.length == 0) {
                caja.value = 0;
            }
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }
            if (!confirm('El porcentaje ingresado as� como el tipo de moneda seleccionado afectar�n a todos los registros que no se encuentren marcados con el check en NO Afectar. Desea continuar con la operaci�n?')) {
                return false;
            }
            var porcent = parseFloat(caja.value);
            var idMoneda = combo.value;
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                if (!rowElem.cells[8].children[0].status) {
                    rowElem.cells[3].children[0].value = idMoneda;
                    var valorNuevo = parseFloat(rowElem.cells[4].children[0].value) * (1 + porcent / 100);
                    var dif = valorNuevo - parseFloat(rowElem.cells[4].children[0].value);
                    rowElem.cells[5].children[0].value = redondear(porcent, 4);
                    rowElem.cells[6].children[0].value = redondear(dif, 4);
                    rowElem.cells[7].children[0].value = redondear(valorNuevo, 4);
                }
            }
            return false;
        }



        function valSave() {
            var grilla = document.getElementById("<%= DGV_SubLinea.ClientID %>");
            if (grilla == null) {
                alert('No se han encontrado registros.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                if (!esDecimal(rowElem.cells[7].children[0].value)) {
                    rowElem.cells[7].children[0].select();
                    rowElem.cells[7].children[0].focus();
                    alert('valor no v�lido');
                    return false;
                }
                if (rowElem.cells[3].children[0].value == '0') {
                    rowElem.cells[3].children[0].focus();
                    alert('Seleccione una moneda.');
                    return false;
                }
            }
            return (confirm('Desea continuar con la operaci�n?'));
        }
        /*function calcularCosto(opc) {
        if (event.keyCode == 9) {
        return true;
        }
        var grilla = document.getElementById("<%= DGV_SubLinea.ClientID %>");
        if (grilla == null) {
        alert('No se han encontrado registros.');
        return false;
        }
        var ventaEscalar = parseFloat(document.getElementById("<%= txtVentaC.ClientID %>").value);
        var compraEscalar = parseFloat(document.getElementById("<%= txtCompraC.ClientID %>").value);
        ventaEscalar = compraEscalar;
        if (!esDecimal(ventaEscalar) || ventaEscalar==0 || isNaN(ventaEscalar)){
        ventaEscalar=0;
        }
        if (!esDecimal(compraEscalar) || compraEscalar==0 || isNaN(compraEscalar)){
        compraEscalar=0;
        }            
        for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
        var rowElem = grilla.rows[i];
        if (rowElem.cells[3].children[0].id == event.srcElement.id || rowElem.cells[4].children[0].id == event.srcElement.id) {
        var costoSoles = parseFloat(rowElem.cells[3].children[0].value);
        var costoDolares = parseFloat(rowElem.cells[4].children[0].value);
        if (!esDecimal(costoSoles) || costoSoles == 0 || isNaN(costoSoles)) {
        costoSoles = 0;                        
        }
        if (!esDecimal(costoDolares) || costoDolares == 0 || isNaN(costoDolares)) {
        costoDolares = 0;
        }
        switch (opc) {
        case 0: //soles
        if (ventaEscalar != 0) {
        costoDolares = costoSoles / ventaEscalar;
        } else {
        costoDolares = 0;
        }
        rowElem.cells[4].children[0].value = redondear(costoDolares, 4);
        break;
        case 1: //dolares
        costoSoles = costoDolares * compraEscalar;
        rowElem.cells[3].children[0].value = redondear(costoSoles, 4);
        break;    
        }
        //imprimo valores                                        
        return true;    
        }                
        }
        }*/

        function validarNumeroPunto(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);           
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function valGrillaBlur(e) {
            //obtiene srcElement.Id
            var targ;
            if (!e) var e = window.event;
            if (e.target) targ = e.target;
            else if (e.srcElement) targ = e.srcElement;
            //termina
            var id = e.id;
            var caja = document.getElementById(id);
            if (CajaEnBlanco(caja)) {
                caja.focus();
                alert('Debe ingresar un valor.');
            }
            if (!esDecimal(caja.value)) {
                caja.select();
                caja.focus();
                alert('Valor no v�lido.');
            }
        }
        /*    function calcularCostoDolaresOnLoad() {
        var grilla = document.getElementById("<%= DGV_SubLinea.ClientID %>");
        if (grilla == null) {
        alert('No se han encontrado registros.');
        return false;
        }
        var cajaD = document.getElementById("<%= txtCompraC.ClientID %>");
        var cajaS = document.getElementById("<%= txtCompraC.ClientID %>");
        if (!esDecimal(cajaD.value) || cajaD.value.length == 0) {
        cajaD.value = 0;
        }
        if (!esDecimal(cajaS.value) || cajaS.value.length == 0) {
        cajaS.value = 0;
        }
        for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
        var rowElem = grilla.rows[i];
        var costoSoles = parseFloat(rowElem.cells[3].children[0].value);
        var costoDolares=0;
        if (!esDecimal(costoSoles) || costoSoles == 0 || isNaN(costoSoles)) {
        costoSoles = 0;
        rowElem.cells[3].children[0].value =0 ;
        }
        if (cajaD.value!=0){
        costoDolares=costoSoles/cajaD.value;    
        }
        rowElem.cells[4].children[0].value = redondear(costoDolares,4);
        }
        return true;
        }*/
    </script>

</asp:Content>
