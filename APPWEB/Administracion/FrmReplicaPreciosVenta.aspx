<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmReplicaPreciosVenta.aspx.vb" Inherits="APPWEB.FrmReplicaPreciosVenta" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                Precios de Venta - Proceso de R�plica
            </td>
        </tr>
        <tr>
            <td align="center" style="font-weight: bold">
                <table>
                    <tr>
                        <td class="Texto" style="text-align: right">
                            Seleccionar Tienda ORIGEN:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTiendaOrigen" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="text-align: right">
                            Seleccionar Tienda DESTINO:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTiendaDestino" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                L�nea - Sub L�nea
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel ID="UpdatePanel_Linea_SubLinea" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td class="Texto">
                                    Tipo Existencia:
                                </td>
                                <td>
                                    <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                                <td class="Texto" style="text-align: right">
                                    L�nea:
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="Texto" style="text-align: right">
                                    Sub L�nea:
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="cboSubLinea" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="left">
                                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                        CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                                        SuppressPostBack="true">
                                    </cc1:CollapsiblePanelExtender>
                                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                                    <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label><asp:Panel
                                        ID="Panel_BusqAvanzadoProd" runat="server">
                                        <table width="100">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td class="Texto" style="font-weight: bold">
                                                                Atributo:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );"
                                                                    Style="cursor: hand;" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" Style="cursor: hand;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                                        Width="650px">
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <Columns>
                                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                                HeaderStyle-Height="25px" ItemStyle-Height="25px" />
                                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label><asp:HiddenField
                                                                        ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                                    </asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                        <HeaderStyle CssClass="GrillaCabecera" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                Tipo Precio de Venta
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:GridView ID="GV_PV" Width="600px" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px">
                            <ItemTemplate>
                                <asp:CheckBox ID="chbTipoPV" onClick="return(   valOnClickChbSelect(this)  );" runat="server"
                                    ToolTip="Seleccione el Tipo de Precio de Venta Destino." />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Precio Venta Destino" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:HiddenField ID="hddIdTipoPV" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoPv")%>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTipoPV_Destino" Font-Bold="true" ForeColor="Red" runat="server"
                                                Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Precio Venta Origen"
                            ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:DropDownList ID="cboTipoPV_Origen" runat="server" ToolTip="Tipo de Precio de Venta Origen">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Porcentaje (%)"
                            ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:TextBox ID="txtPorcentPV" TabIndex="500" onblur="return(   valBlurClear('0',event)    );"
                                    onKeypress="return(  validarNumeroPuntoSigno(event)    );" onFocus="return(  aceptarFoco(this)   );"
                                    Text="0" runat="server" Width="80px" Font-Bold="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" HeaderText="Sumar Costo Flete" >
                            <ItemTemplate>
                                <asp:CheckBox ID="chbCostoFlete" onClick="return(   onClick_chbCostoFlete(this)  );" runat="server"
                                    ToolTip="Seleccione el Tipo de Precio de Venta Destino." />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <HeaderStyle CssClass="GrillaHeader" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center" style="height: 50px">
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnGuardar" OnClientClick="return(  valOnClickGuardar()  );" Height="30px"
                                Width="255px" runat="server" Text="Ejecutar R�plica de Precios de Venta" ToolTip="Ejecutar R�plica de Precios de Venta." />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="LabelRojo" style="font-weight: bold; text-align: left">
                *** Si no especifica una [ L�nea ] y/o [ Sub L�nea ] el Sistema replicar� los Precios
                de Venta para todas las [ L�neas ] y/o [ Sub L�neas ]
            </td>
        </tr>
        <tr>
            <td class="LabelRojo" style="font-weight: bold; text-align: left">
                *** El Sistema solo replicar� los Precios de Venta para los [ Tipos de Precio de
                Venta ] destino seleccionados.
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        function valOnClickGuardar() {

            var grilla = document.getElementById('<%=GV_PV.ClientID%>');
            if (grilla != null) {
                var cont = 0;
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[0].children[0].status) {
                        cont = cont + 1;
                    }

                }
                if (cont <= 0) {
                    alert('Debe seleccionar al menos un Tipo de Precio de Venta Destino.');
                    return false;
                }
            } else {
                alert('Debe seleccionar al menos un Tipo de Precio de Venta Destino.');
                return false;
            }
            return confirm('Los cambios solicitados no podr�n ser deshechos una vez realizados. Se recomienda verificar las opciones seleccionadas antes de Replicar los Precios de Venta. Desea continuar de todas formas con la Operaci�n ?');
        }

        function valOnClickChbSelect(chbSelect) {
            var grilla = document.getElementById('<%=GV_PV.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[0].children[0].id == chbSelect.id) {
                        rowElem.cells[3].children[0].value = '0';
                        return true;
                    }

                }
            }

            return true;
        }

        function onClick_chbCostoFlete(obj){
            var grilla = document.getElementById('<%=GV_PV.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[0].children[0].id == obj.id) {
                        rowElem.cells[3].children[0].value = '0';
                        return true;
                    }

                }
            }

            return true;        
        }
        
        //////////////////////
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
       

        //////////////////////////////

    </script>

</asp:Content>
