﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class frmMantEmpresaTiendaAlmacen
    Inherits System.Web.UI.Page
    Dim cbo As New Combo
    Dim sc As New ScriptManagerClass
    Dim objutil As New Util
    Dim ValidoRep As New Negocio.Util

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ConfigurarDatos()
            Session("Indicador") = "0"
            HabilitarBotonTienda(True, False, False, False)
            HabilitarMantTienda(False)
            LimpiarMantTienda()
            Me.PanelNuevoT.Visible = False : Me.PanelBuscarT.Visible = True
            cbo.LLenarCboDepartamento(Me.cboDptoT)
            CboTipoTienda(Me.cboTipoT)
            BuscarTienda(Me.dgvTiendaMant)

            Me.PanelNuevoTipoAlmacen.Visible = False : Me.panelBuscarTAlmacen.Visible = True
            HabilitarBotonTipoAlmacen(True, False, False, False)
            BuscarTipoAlmacen(Me.gvwTAlmacen)
            LimpiarMantTAlmacen()


            'llena combo tipo almacen
            HabilitarBotonAlmacen(True, False, False, False)
            HabilitarMantAlmacen(False)
            Me.PanelBuscaAlmacen.Visible = True : Me.PanelNuevoAlmacen.Visible = False
            cbo.LLenarCboDepartamento(Me.cboDptoAlma)
            cbo.LlenarCboTipoAlmacen(Me.cboTipoAlmacen, True)
            cbo.llenarCboAlmacenxTipoAlmacenPrincipal(Me.cboAlmacenRef, True)

            cargarDatosCboAreaMant(Me.cboAreaMantAlma)
            BuscarAlmacen(Me.dgvMantAlmacen)
            LimpiarMantAlmacen()




            'Me.PanelNuevoArea.Visible = False : Me.PanelBuscarArea.Visible = True
            'HabilitarBotonArea(True, False, False, False)
            'BuscarArea(Me.dgvMantArea)
            'LimpiarMantArea()

            Me.PanelNuevaCaja.Visible = False : Me.PanelBuscaCaja.Visible = True
            HabilitarBotonCaja(True, False, False)
            ListarGrillaAlmacen(Me.dgvCaja)
            LimpiarMantCaja()
        End If
    End Sub

    Private Sub cargarDatosCboTienda(ByVal cboTienda As DropDownList)
        '************** Cargamos los datos de las tiendas
        cbo.LLenarCboTienda(cboTienda, True)

    End Sub

    Private Sub cargarDatosCboAlmacen(ByVal cboAlmacen As DropDownList)
        Dim obj As New Negocio.Almacen
        Dim lista As List(Of Entidades.Almacen) = obj.SelectAllActivo(True)
        lista.Insert(0, (New Entidades.Almacen(0, "----")))
        cboAlmacen.DataTextField = "Nombre"
        cboAlmacen.DataValueField = "IdAlmacen"
        cboAlmacen.DataSource = lista
        cboAlmacen.DataBind()
    End Sub

    Private Sub cargarDatosCboArea(ByVal cboAlmacen As DropDownList)
        Dim lista As List(Of Entidades.Area) = (New Negocio.Area).SelectAllActivo()
        Dim objArea As New Entidades.Area
        Dim listaNew As New List(Of Entidades.Area)
        lista.Insert(0, (New Entidades.Area(0, "----", True)))

        For Each objArea In lista
            objArea.DescripcionCorta = HttpUtility.HtmlDecode(objArea.DescripcionCorta)
            'objArea.Id = objArea.Id
            listaNew.Add(objArea)
        Next
        cboAlmacen.DataTextField = "DescripcionCorta"
        cboAlmacen.DataValueField = "Id"
        cboAlmacen.DataSource = listaNew
        cboAlmacen.DataBind()
    End Sub

    '' MANTENIMIENTO TIENDA

#Region "Mantenimiento Tienda"

    Private Sub CboTipoTienda(ByVal cbo As DropDownList)
        Dim obj As New Negocio.TipoTienda
        cbo.DataSource = obj.SelectAllActivo()
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub

    Protected Sub cboDptoT_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboDptoT.SelectedIndexChanged
        cbo.LLenarCboProvincia(Me.cboProvT, Me.cboDptoT.SelectedValue)
    End Sub
    Protected Sub cboProvT_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboProvT.SelectedIndexChanged
        cbo.LLenarCboDistrito(Me.cboDistT, Me.cboDptoT.SelectedValue, Me.cboProvT.SelectedValue)
    End Sub
    Sub GrabarTienda(ByVal Indicador As String)
        Dim ngcTienda As New Negocio.Tienda
        Dim objTienda As New Entidades.Tienda
        If CStr(Indicador) = "I" Then
            With objTienda
                .Nombre = HttpUtility.HtmlDecode(Me.txtNomT.Text)
                .Ubigeo = Me.cboDptoT.SelectedValue + Me.cboProvT.SelectedValue + Me.cboDistT.SelectedValue
                .Direccion = HttpUtility.HtmlDecode(Me.txtDireccionT.Text)
                .Referencia = HttpUtility.HtmlDecode(Me.txtReferenciaT.Text)
                .Telefono = HttpUtility.HtmlDecode(Me.txtTelefonoT.Text)
                .Fax = HttpUtility.HtmlDecode(Me.txtFaxT.Text)
                .Estado = "1"
                .IdTipoTienda = CInt(Me.cboTipoT.SelectedValue)
                .Email = Me.txtMail.Text
                .CtaDebeVenta = CStr(tbCtaDebeVenta.Text.Trim)
                .CtaHaberVenta = CStr(tbCtaHaberVenta.Text.Trim)
            End With

            If ValidoRep.ValidarExistenciaxTablax1Campo("tienda", "tie_Nombre", objTienda.Nombre) <= 0 Then
                If ngcTienda.InsertaTienda(objTienda) = True Then
                Else
                    sc.mostrarMsjAlerta(Me, "Error al grabar la Tienda")
                End If
            Else
                sc.mostrarMsjAlerta(Me, "ya existe el nombre.")
            End If


        ElseIf CStr(Indicador) = "U" Then
            If Me.chkEliminar.Checked = False Then
                With objTienda
                    .Nombre = HttpUtility.HtmlDecode(Me.txtNomT.Text)
                    .Ubigeo = Me.cboDptoT.SelectedValue + Me.cboProvT.SelectedValue + Me.cboDistT.SelectedValue
                    .Direccion = HttpUtility.HtmlDecode(Me.txtDireccionT.Text)
                    .Referencia = HttpUtility.HtmlDecode(txtReferenciaT.Text)
                    .Telefono = HttpUtility.HtmlDecode(Me.txtTelefonoT.Text)
                    .Fax = HttpUtility.HtmlDecode(Me.txtFaxT.Text)
                    .Estado = "1"
                    .IdTipoTienda = CInt(Me.cboTipoT.SelectedValue)
                    .Id = CInt(Session("IdTiendaT"))
                    .Email = HttpUtility.HtmlDecode(Me.txtMail.Text)
                    .CtaDebeVenta = CStr(tbCtaDebeVenta.Text.Trim)
                    .CtaHaberVenta = CStr(tbCtaHaberVenta.Text.Trim)
                End With
                If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("tienda", "tie_Nombre", objTienda.Nombre, "IdTienda", objTienda.Id) <= 0 Then
                    If ngcTienda.ActualizaTienda(objTienda) = True Then
                        'sc.mostrarMsjAlerta(Me, "Se Actualizo correctamente los datos")
                    Else
                        sc.mostrarMsjAlerta(Me, "Error al Actualizar la Tienda")
                    End If
                Else
                    sc.mostrarMsjAlerta(Me, "ya existe el nombre.")
                End If

             

            ElseIf Me.chkEliminar.Checked = True Then
                With objTienda
                    .Nombre = HttpUtility.HtmlDecode(Me.txtNomT.Text)
                    .Ubigeo = Me.cboDptoT.SelectedValue + Me.cboProvT.SelectedValue + Me.cboDistT.SelectedValue
                    .Direccion = HttpUtility.HtmlDecode(Me.txtDireccionT.Text)
                    .Referencia = HttpUtility.HtmlDecode(Me.txtReferenciaT.Text)
                    .Telefono = HttpUtility.HtmlDecode(Me.txtTelefonoT.Text)
                    .Fax = HttpUtility.HtmlDecode(Me.txtFaxT.Text)
                    .Estado = "0"
                    .IdTipoTienda = CInt(Me.cboTipoT.SelectedValue)
                    .Id = CInt(Session("IdTiendaT"))
                End With
                If ngcTienda.ActualizaTienda(objTienda) = True Then
                    'sc.mostrarMsjAlerta(Me, "Se Elimino correctamente los datos")
                Else
                    sc.mostrarMsjAlerta(Me, "Error al Eliminar la Tienda")
                End If
            End If
        End If
        HabilitarMantTienda(False)
    End Sub
    Protected Sub cmd_GuardarMantT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_GuardarMantT.Click
        If Me.txtNomT.Text = "" Then : Exit Sub : End If
        If (Me.cboTipoT.SelectedValue = "") Or (Me.cboTipoT.SelectedValue = "0") Then : Exit Sub : End If
        If Me.txtDireccionT.Text = "" Then : Exit Sub : End If
        If Me.cboDptoT.SelectedValue = "00" Then : Exit Sub : End If
        If Me.cboProvT.SelectedValue = "00" Then : Exit Sub : End If
        If Me.cboDistT.SelectedValue = "00" Then : Exit Sub : End If

        GrabarTienda(CStr(Session("IndicadorTienda")))
        LimpiarMantTienda()
        HabilitarMantTienda(False)
        HabilitarBotonTienda(True, False, False, True)
        Me.PanelBuscarT.Visible = True : Me.PanelNuevoT.Visible = False
        BuscarTienda(Me.dgvTiendaMant)
    End Sub
    Sub LimpiarMantTienda()
        Try
            Me.txtNomT.Text = ""
            Me.txtMail.Text = ""
            Me.cboTipoT.SelectedIndex = -1
            Me.txtDireccionT.Text = ""
            Me.txtReferenciaT.Text = ""
            Me.cboDptoT.SelectedIndex = 0
            Me.cboProvT.SelectedIndex = 0
            Me.cboDistT.SelectedIndex = 0
            Me.txtTelefonoT.Text = ""
            Me.txtFaxT.Text = ""
            Me.chkEliminar.Checked = False
            'Me.txtBusNomT.Text = ""
            Me.dgvTiendaMant.DataBind()
        Catch ex As Exception
        Finally
        End Try
    End Sub
    Sub HabilitarMantTienda(ByVal Activar As Boolean)
        Me.txtNomT.Enabled = Activar
        Me.cboTipoT.Enabled = Activar
        Me.txtDireccionT.Enabled = Activar
        Me.txtReferenciaT.Enabled = Activar
        Me.cboDptoT.Enabled = Activar
        Me.cboProvT.Enabled = Activar
        Me.cboDistT.Enabled = Activar
        Me.txtTelefonoT.Enabled = Activar
        Me.txtFaxT.Enabled = Activar
        Me.chkEliminar.Enabled = Activar
    End Sub
    Sub HabilitarBotonTienda(ByVal ActivarNuevo As Boolean, ByVal ActivarGuardar As Boolean, ByVal ActivarEditar As Boolean, ByVal ActivarCancelar As Boolean)
        Me.btnNuevoMantT.Visible = ActivarNuevo
        Me.cmd_GuardarMantT.Visible = ActivarGuardar
        Me.cmd_EditarT.Visible = ActivarEditar
        Me.btnCancelarMantTienda.Visible = ActivarCancelar
    End Sub
    Protected Sub btnNuevoMantT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevoMantT.Click
        HabilitarBotonTienda(False, True, False, True)
        LimpiarMantTienda()
        Session("IndicadorTienda") = "I"
        Me.PanelBuscarT.Visible = False : Me.PanelNuevoT.Visible = True
        HabilitarMantTienda(True)
        Me.chkEliminar.Visible = False
    End Sub
    Protected Sub cmd_EditarT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_EditarT.Click
        Session("IndicadorTienda") = "U"
        Me.PanelBuscarT.Visible = True : Me.PanelNuevoT.Visible = False
        LimpiarMantTienda()
        HabilitarMantTienda(True)
    End Sub
    Private Sub btnCancelarMantTienda_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelarMantTienda.Click
        HabilitarBotonTienda(True, False, False, True)
        LimpiarMantTienda()
        BuscarTienda(Me.dgvTiendaMant)
        Me.PanelBuscarT.Visible = True : Me.PanelNuevoT.Visible = False
    End Sub
    Private Sub chkEliminar_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEliminar.CheckedChanged
        If Me.chkEliminar.Checked = True Then
            HabilitarMantTienda(False)
            Me.chkEliminar.Enabled = True
        ElseIf Me.chkEliminar.Checked = False Then
            HabilitarMantTienda(True)
            Me.chkEliminar.Enabled = True
        End If
    End Sub
    'Protected Sub cmd_BuscaMantT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_BuscaMantT.Click
    '    BuscarTienda(Me.dgvTiendaMant)
    'End Sub
    Sub BuscarTienda(ByVal grilla As GridView)
        Dim ngcTienda As New Negocio.Tienda
        grilla.DataSource = ngcTienda.BuscaAllxNombreActivoInactivo("")
        grilla.DataBind()
    End Sub
    Protected Sub dgvTiendaMant_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvTiendaMant.SelectedIndexChanged
        Try
            Session("IdTiendaT") = dgvTiendaMant.SelectedRow.Cells(11).Text
            Session("IndicadorTienda") = "U"
            HabilitarMantTienda(True)
            txtNomT.Text = HttpUtility.HtmlDecode(dgvTiendaMant.SelectedRow.Cells(1).Text).Trim
            txtDireccionT.Text = HttpUtility.HtmlDecode(dgvTiendaMant.SelectedRow.Cells(5).Text).Trim
            txtReferenciaT.Text = HttpUtility.HtmlDecode(dgvTiendaMant.SelectedRow.Cells(6).Text).Trim
            txtTelefonoT.Text = HttpUtility.HtmlDecode(dgvTiendaMant.SelectedRow.Cells(7).Text).Trim
            txtFaxT.Text = HttpUtility.HtmlDecode(dgvTiendaMant.SelectedRow.Cells(8).Text).Trim
            txtMail.Text = HttpUtility.HtmlDecode(dgvTiendaMant.SelectedRow.Cells(13).Text).Trim
            Me.cboDptoT.SelectedValue = Left(dgvTiendaMant.SelectedRow.Cells(12).Text, 2)
            cbo.LLenarCboProvincia(Me.cboProvT, Left(dgvTiendaMant.SelectedRow.Cells(12).Text, 2))
            Me.cboProvT.SelectedValue = Mid(dgvTiendaMant.SelectedRow.Cells(12).Text, 3, 2)
            cbo.LLenarCboDistrito(Me.cboDistT, Left(dgvTiendaMant.SelectedRow.Cells(12).Text, 2), Mid(dgvTiendaMant.SelectedRow.Cells(12).Text, 3, 2))
            Me.cboDistT.SelectedValue = Mid(dgvTiendaMant.SelectedRow.Cells(12).Text, 5, 2)

            cboTipoT.SelectedValue = LTrim(RTrim(dgvTiendaMant.SelectedRow.Cells(14).Text))

            Me.tbCtaDebeVenta.Text = HttpUtility.HtmlDecode(LTrim(RTrim(dgvTiendaMant.SelectedRow.Cells(15).Text))).Trim
            Me.tbCtaHaberVenta.Text = HttpUtility.HtmlDecode(LTrim(RTrim(dgvTiendaMant.SelectedRow.Cells(16).Text))).Trim

            If dgvTiendaMant.SelectedRow.Cells(10).Text = "Activo" Then
                chkEliminar.Checked = False
            ElseIf dgvTiendaMant.SelectedRow.Cells(10).Text = "Inactivo" Then
                chkEliminar.Checked = True
            End If
            Me.PanelBuscarT.Visible = False : Me.PanelNuevoT.Visible = True
            HabilitarBotonTienda(False, True, False, True)
            Me.chkEliminar.Visible = True
        Catch ex As Exception
            Me.PanelBuscarT.Visible = False : Me.PanelNuevoT.Visible = True
            HabilitarBotonTienda(False, True, False, True)
            If dgvTiendaMant.SelectedRow.Cells(10).Text = "Activo" Then
                chkEliminar.Checked = False
            ElseIf dgvTiendaMant.SelectedRow.Cells(10).Text = "Inactivo" Then
                chkEliminar.Checked = True
            End If
            Me.chkEliminar.Visible = True
        Finally
        End Try
    End Sub

#End Region


    '' MANTENIMIENTO Caja

#Region "'MANTENIMIENTO CAJA"
    Sub LimpiarMantCaja()
        Try
            Me.txtNomCaja.Text = ""
            Me.txtNroCaja.Text = ""
            Me.cboMantCajaTienda.SelectedIndex = -1
            Me.chkInactivarCaja.Checked = False
            Me.chkCajaChica.Checked = False
            Me.dgvCaja.DataBind()
        Catch ex As Exception
        Finally
        End Try
    End Sub
    Sub HabilitarBotonCaja(ByVal ActivarNuevo As Boolean, ByVal ActivarGuardar As Boolean, ByVal ActivarCancelar As Boolean)
        Me.btnMantCajaNuevo.Visible = ActivarNuevo
        Me.btnMantCajaGrabar.Visible = ActivarGuardar
        Me.btnMantCajaCancelar.Visible = ActivarCancelar
    End Sub
    Sub ListarGrillaAlmacen(ByVal grilla As GridView)
        Dim ngcCaja As New Negocio.Caja
        grilla.DataSource = ngcCaja.SelectAllActivoInactivo
        grilla.DataBind()
    End Sub
    Private Sub btnMantCajaNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMantCajaNuevo.Click
        cbo.LLenarCboTienda(Me.cboMantCajaTienda, True)
        LimpiarMantCaja()
        HabilitarBotonCaja(False, True, True)
        Me.PanelNuevaCaja.Visible = True : Me.PanelBuscaCaja.Visible = False
        Me.chkInactivarCaja.Checked = False
        Session("EstadoCaja") = "1"
        Session("IdCaja") = CInt(0)
        Me.chkInactivarCaja.Visible = False
    End Sub
    Private Function obtenerListaCajaFrmGrilla() As List(Of Entidades.Caja)
        Dim objcaja As New Entidades.Caja
        Dim lista As New List(Of Entidades.Caja)
        With objcaja
            .IdCaja = CInt(LTrim(RTrim(CStr(Session("IdCaja")))))
            .Nombre = HttpUtility.HtmlDecode(txtNomCaja.Text)
            .CajaNumero = Me.txtNroCaja.Text
            .IdTienda = CInt(Me.cboMantCajaTienda.SelectedValue)
            .Estado = CStr(Session("EstadoCaja"))
            .Caja_Chica = Me.chkCajaChica.Checked
        End With
        lista.Add(objcaja)
        Return lista
    End Function
    Sub GrabarCaja()
        Dim lCaja As List(Of Entidades.Caja) = obtenerListaCajaFrmGrilla()
        Dim ngcCaja As New Negocio.Caja
        If ngcCaja.GrabarCaja(lCaja) = True Then
            Session("IdCaja") = CInt(0)
            ListarGrillaAlmacen(Me.dgvCaja)
        Else
            sc.mostrarMsjAlerta(Me, "Error al grabar")
        End If
    End Sub
    Private Sub btnMantCajaGrabar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMantCajaGrabar.Click
        If Me.txtNomCaja.Text = "" Then : Exit Sub : End If
        If Me.txtNroCaja.Text = "" Then : Exit Sub : End If
        If Me.cboMantCajaTienda.SelectedIndex = 0 Then : Exit Sub : End If
        GrabarCaja()
        HabilitarBotonCaja(True, False, False)
        Me.PanelNuevaCaja.Visible = False : Me.PanelBuscaCaja.Visible = True
    End Sub
    Private Sub btnMantCajaCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMantCajaCancelar.Click
        HabilitarBotonCaja(True, False, False)
        ListarGrillaAlmacen(Me.dgvCaja)
        Me.PanelNuevaCaja.Visible = False : Me.PanelBuscaCaja.Visible = True
    End Sub
    Private Sub dgvCaja_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvCaja.SelectedIndexChanged
        Me.chkInactivarCaja.Visible = True
        Session("IdCaja") = dgvCaja.SelectedRow.Cells(6).Text
        txtNomCaja.Text = HttpUtility.HtmlDecode(dgvCaja.SelectedRow.Cells(1).Text)
        txtNroCaja.Text = HttpUtility.HtmlDecode(dgvCaja.SelectedRow.Cells(2).Text)
        cbo.LLenarCboTienda(Me.cboMantCajaTienda, True)
        cboMantCajaTienda.SelectedValue = dgvCaja.SelectedRow.Cells(7).Text
        If dgvCaja.SelectedRow.Cells(4).Text = "Activo" Then
            Me.chkInactivarCaja.Checked = False
        ElseIf dgvCaja.SelectedRow.Cells(4).Text = "Inactivo" Then
            Me.chkInactivarCaja.Checked = True
        End If

        Me.chkCajaChica.Checked = CType(dgvCaja.SelectedRow.Cells(5).Controls(0), CheckBox).Checked

        HabilitarBotonCaja(False, True, True)
        Me.PanelNuevaCaja.Visible = True : Me.PanelBuscaCaja.Visible = False

    End Sub
    Private Sub chkInactivarCaja_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkInactivarCaja.CheckedChanged
        If Me.chkInactivarCaja.Checked = True Then
            Session("EstadoCaja") = 0
        ElseIf Me.chkInactivarCaja.Checked = False Then
            Session("EstadoCaja") = 1
        End If
    End Sub
#End Region


    '' MANTENIMIENTO ALMACEN
#Region "MANTENIMIENTO ALMACEN"

    Sub GrabarAlmacen(ByVal Indicador As String)
        Dim ngcAlmacen As New Negocio.Almacen
        Dim objAlmacen As New Entidades.Almacen
        Try

            If CStr(Indicador) = "I" Then
                With objAlmacen
                    .Nombre = HttpUtility.HtmlDecode(Me.txtNomAlma.Text)
                    .Ubigeo = Me.cboDptoAlma.SelectedValue + Me.cboProvAlma.SelectedValue + Me.cboDistAlma.SelectedValue
                    .Direccion = HttpUtility.HtmlDecode(Me.txtDireccionAlma.Text)
                    .Referencia = HttpUtility.HtmlDecode(Me.txtReferenciaAlma.Text)
                    .Estado = "1"
                    .Area = CInt(Me.cboAreaMantAlma.SelectedValue)

                    .IdtipoAlmacen = CInt(Me.cboTipoAlmacen.SelectedValue)
                    .IdAlmacenRef = CInt(Me.cboAlmacenRef.SelectedValue)
                    .IdtipoAlmacen = CInt(Me.cboTipoAlmacen.SelectedValue)

                    If Me.chbCentroDistribuicion.Checked Then
                        .CtroDistribuicion = "Centro Distribuicion"
                    Else
                        .CtroDistribuicion = ""
                    End If

                End With
                If ValidoRep.ValidarExistenciaxTablax1Campo("almacen", "alm_Nombre", objAlmacen.Nombre) <= 0 Then
                    If ngcAlmacen.InsertaAlmacen(objAlmacen) = True Then
                        sc.mostrarMsjAlerta(Me, "Se grabarón correctamente los datos")
                        'Else
                        '    sc.mostrarMsjAlerta(Me, "Error al grabar la Almacen")
                    End If
                Else
                    sc.mostrarMsjAlerta(Me, "ya existe el nombre.")
                End If


            ElseIf CStr(Indicador) = "U" Then
                If Me.chkEliminaAlma.Checked = False Then
                    With objAlmacen
                        .Nombre = HttpUtility.HtmlDecode(Me.txtNomAlma.Text)
                        .Ubigeo = Me.cboDptoAlma.SelectedValue + Me.cboProvAlma.SelectedValue + Me.cboDistAlma.SelectedValue
                        .Direccion = HttpUtility.HtmlDecode(Me.txtDireccionAlma.Text)
                        .Referencia = HttpUtility.HtmlDecode(Me.txtReferenciaAlma.Text)
                        .Estado = "1"
                        .Area = CInt(Me.cboAreaMantAlma.SelectedValue)
                        .IdAlmacen = CInt(Session("IdAlmacenM"))

                        .IdtipoAlmacen = CInt(Me.cboTipoAlmacen.SelectedValue)
                        If Me.chbCentroDistribuicion.Checked Then
                            .CtroDistribuicion = "Centro Distribuicion"
                        Else
                            .CtroDistribuicion = ""
                        End If

                        If CInt(Me.cboAlmacenRef.SelectedValue) = 0 Then
                            sc.mostrarMsjAlerta(Me, "Selecione un Almacen de Referencia")
                        Else
                            .IdAlmacenRef = CInt(Me.cboAlmacenRef.SelectedValue)
                        End If

                    End With

                    If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("almacen", "alm_Nombre", objAlmacen.Nombre, "IdAlmacen", objAlmacen.IdAlmacen) <= 0 Then
                        If ngcAlmacen.ActualizaAlmacen(objAlmacen) = True Then
                            'sc.mostrarMsjAlerta(Me, "Se Actualizo correctamente los datos")
                        Else
                            sc.mostrarMsjAlerta(Me, "Error al Actualizar la Almacen")
                        End If
                    Else
                        sc.mostrarMsjAlerta(Me, "ya existe el nombre.")
                    End If


                ElseIf Me.chkEliminaAlma.Checked = True Then
                    With objAlmacen
                        .Nombre = HttpUtility.HtmlDecode(Me.txtNomAlma.Text)
                        .Ubigeo = Me.cboDptoAlma.SelectedValue + Me.cboProvAlma.SelectedValue + Me.cboDistAlma.SelectedValue
                        .Direccion = HttpUtility.HtmlDecode(Me.txtDireccionAlma.Text)
                        .Referencia = HttpUtility.HtmlDecode(Me.txtReferenciaAlma.Text)
                        .Estado = "0"
                        .Area = CInt(Me.cboAreaMantAlma.SelectedValue)
                        .IdAlmacen = CInt(Session("IdAlmacenM"))

                        .IdtipoAlmacen = CInt(Me.cboTipoAlmacen.SelectedValue)
                        .IdAlmacenRef = CInt(Me.cboAlmacenRef.SelectedValue)
                        If Me.chbCentroDistribuicion.Checked Then
                            .CtroDistribuicion = "Centro Distribuicion"
                        Else
                            .CtroDistribuicion = ""
                        End If

                    End With
                    If ngcAlmacen.ActualizaAlmacen(objAlmacen) = True Then
                        'sc.mostrarMsjAlerta(Me, "Se Elimino correctamente los datos")
                    Else
                        sc.mostrarMsjAlerta(Me, "Error al Eliminar la Almacen")
                    End If
                End If
            End If
            HabilitarMantAlmacen(False)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub LimpiarMantAlmacen()
        Try
            Me.txtNomAlma.Text = ""
            Me.cboAreaMantAlma.SelectedIndex = -1
            Me.txtDireccionAlma.Text = ""
            Me.txtReferenciaAlma.Text = ""
            Me.cboDptoAlma.SelectedIndex = -1
            Me.cboProvAlma.SelectedIndex = -1
            Me.cboDistAlma.SelectedIndex = -1

            Me.cboTipoAlmacen.SelectedIndex = -1
            Me.cboAlmacenRef.SelectedIndex = -1

            Me.chkEliminaAlma.Checked = False
            Me.chbCentroDistribuicion.Checked = False
            'Me.txtBuscaNomMantAlmacen.Text = ""
            Me.dgvMantAlmacen.DataBind()
        Catch ex As Exception
        Finally
        End Try
    End Sub
    Sub HabilitarMantAlmacen(ByVal Activar As Boolean)
        Me.txtNomAlma.Enabled = Activar
        Me.cboAreaMantAlma.Enabled = Activar
        Me.txtDireccionAlma.Enabled = Activar
        Me.txtReferenciaAlma.Enabled = Activar
        Me.cboDptoAlma.Enabled = Activar
        Me.cboProvAlma.Enabled = Activar
        Me.cboDistAlma.Enabled = Activar
        Me.chkEliminaAlma.Enabled = Activar

        Me.cboAlmacenRef.Enabled = Activar
        Me.cboTipoAlmacen.Enabled = Activar

    End Sub
    Sub HabilitarBotonAlmacen(ByVal ActivarNuevo As Boolean, ByVal ActivarGuardar As Boolean, ByVal ActivarEditar As Boolean, ByVal ActivarCancelar As Boolean)
        Me.btnMantAlmaNuevo.Visible = ActivarNuevo
        Me.btnMantAlmaGraba.Visible = ActivarGuardar
        Me.btnMantAlmaEditar.Visible = ActivarEditar
        Me.btnCancelarMantAlmacen.Visible = ActivarCancelar
    End Sub
    Private Sub cargarDatosCboAreaMant(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Area

        Dim objarea As New Entidades.Area
        Dim listaArea As New List(Of Entidades.Area)

        For Each objarea In obj.SelectAllActivo
            objarea.DescripcionCorta = HttpUtility.HtmlDecode(objarea.DescripcionCorta)
            listaArea.Add(objarea)
        Next
        cbo.DataSource = listaArea
        cbo.DataTextField = "DescripcionCorta"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub


    Private Sub btnMantAlmaNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMantAlmaNuevo.Click
        HabilitarBotonAlmacen(False, True, False, True)
        LimpiarMantAlmacen()
        Session("IndicadorAlmacen") = "I"
        Me.PanelBuscaAlmacen.Visible = False : Me.PanelNuevoAlmacen.Visible = True
        HabilitarMantAlmacen(True)
        chkEliminaAlma.Visible = False
        cbo.llenarCboAlmacenxTipoAlmacenPrincipal(Me.cboAlmacenRef, True)
    End Sub
    Private Sub btnMantAlmaGraba_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMantAlmaGraba.Click
        If Me.txtNomAlma.Text = "" Then : Exit Sub : End If
        If Me.cboAreaMantAlma.SelectedIndex = -1 Then : Exit Sub : End If
        If Me.cboDptoAlma.SelectedIndex = 0 Then : Exit Sub : End If
        If Me.cboProvAlma.SelectedIndex = 0 Then : Exit Sub : End If
        If Me.cboDistAlma.SelectedIndex = -0 Then : Exit Sub : End If
        If Me.txtDireccionAlma.Text = "" Then : Exit Sub : End If

        'If Me.cbo

        Try
            GrabarAlmacen(CStr(Session("IndicadorAlmacen")))
            LimpiarMantAlmacen()
            HabilitarMantAlmacen(False)
            HabilitarBotonAlmacen(True, False, False, False)
            Me.PanelBuscaAlmacen.Visible = True : Me.PanelNuevoAlmacen.Visible = False
            BuscarAlmacen(Me.dgvMantAlmacen)

        Catch ex As Exception
            sc.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnMantAlmaEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMantAlmaEditar.Click
        Me.PanelBuscaAlmacen.Visible = True : Me.PanelNuevoAlmacen.Visible = False
        Session("IndicadorAlmacen") = "U"
        LimpiarMantAlmacen()
        HabilitarMantAlmacen(True)
    End Sub
    'Private Sub btnBuscarMantAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarMantAlmacen.Click
    '    BuscarAlmacen(Me.dgvMantAlmacen)
    'End Sub
    Sub BuscarAlmacen(ByVal grilla As GridView)
        Dim ngcAlmacen As New Negocio.Almacen
        grilla.DataSource = ngcAlmacen.SelectGrillaActivoInactivoxNombre("")
        grilla.DataBind()

    End Sub
    Private Sub cboDptoAlma_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDptoAlma.SelectedIndexChanged
        cbo.LLenarCboProvincia(Me.cboProvAlma, Me.cboDptoAlma.SelectedValue)
    End Sub
    Private Sub cboProvAlma_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProvAlma.SelectedIndexChanged
        cbo.LLenarCboDistrito(Me.cboDistAlma, Me.cboDptoAlma.SelectedValue, Me.cboProvAlma.SelectedValue)
    End Sub
    Private Sub dgvMantAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMantAlmacen.SelectedIndexChanged
        Try
            'Session("IdAlmacenM") = dgvMantAlmacen.SelectedRow.Cells(9).Text
            Session("IdAlmacenM") = CType(Me.dgvMantAlmacen.SelectedRow.Cells(12).FindControl("hdfIdAlmacen"), HiddenField).Value

            Session("IndicadorAlmacen") = "U"
            HabilitarMantAlmacen(True)
            txtNomAlma.Text = HttpUtility.HtmlDecode(dgvMantAlmacen.SelectedRow.Cells(1).Text)

            'Me.cboDptoAlma.SelectedValue = HttpUtility.HtmlDecode(Left(dgvMantAlmacen.SelectedRow.Cells(10).Text, 2))
            Me.cboDptoAlma.SelectedValue = HttpUtility.HtmlDecode(Left(CType(Me.dgvMantAlmacen.SelectedRow.Cells(12).FindControl("hdfUbigeo"), HiddenField).Value, 2))
            'CType(Me.dgvMantAlmacen.SelectedRow.Cells(12).FindControl("idfIdAlmacen"), HiddenField).Value

            'cbo.LLenarCboProvincia(Me.cboProvAlma, Left(dgvMantAlmacen.SelectedRow.Cells(10).Text, 2))
            cbo.LLenarCboProvincia(Me.cboProvAlma, Left(CType(Me.dgvMantAlmacen.SelectedRow.Cells(12).FindControl("hdfUbigeo"), HiddenField).Value, 2))

            'Me.cboProvAlma.SelectedValue = Mid(dgvMantAlmacen.SelectedRow.Cells(10).Text, 3, 2)
            Me.cboProvAlma.SelectedValue = Mid(CType(Me.dgvMantAlmacen.SelectedRow.Cells(12).FindControl("hdfUbigeo"), HiddenField).Value, 3, 2)

            'cbo.LLenarCboDistrito(Me.cboDistAlma, Left(dgvMantAlmacen.SelectedRow.Cells(10).Text, 2), Mid(dgvMantAlmacen.SelectedRow.Cells(10).Text, 3, 2))
            cbo.LLenarCboDistrito(Me.cboDistAlma, Left(CType(Me.dgvMantAlmacen.SelectedRow.Cells(12).FindControl("hdfUbigeo"), HiddenField).Value, 2), Mid(CType(Me.dgvMantAlmacen.SelectedRow.Cells(12).FindControl("hdfUbigeo"), HiddenField).Value, 3, 2))
            Me.cboDistAlma.SelectedValue = Mid(CType(Me.dgvMantAlmacen.SelectedRow.Cells(12).FindControl("hdfUbigeo"), HiddenField).Value, 5, 2)


            txtDireccionAlma.Text = HttpUtility.HtmlDecode(dgvMantAlmacen.SelectedRow.Cells(5).Text)
            txtReferenciaAlma.Text = HttpUtility.HtmlDecode(dgvMantAlmacen.SelectedRow.Cells(6).Text)

            'cboAreaMantAlma.Text = HttpUtility.HtmlDecode(dgvMantAlmacen.SelectedRow.Cells(11).Text)
            cboAreaMantAlma.Text = HttpUtility.HtmlDecode(CType(Me.dgvMantAlmacen.SelectedRow.Cells(12).FindControl("hdfArea"), HiddenField).Value)


            If dgvMantAlmacen.SelectedRow.Cells(8).Text = "Activo" Then
                chkEliminaAlma.Checked = False
            ElseIf dgvMantAlmacen.SelectedRow.Cells(8).Text = "Inactivo" Then
                chkEliminaAlma.Checked = True
            End If

            If dgvMantAlmacen.SelectedRow.Cells(11).Text = "Centro Distribuicion" Then
                chbCentroDistribuicion.Checked = True
            ElseIf dgvMantAlmacen.SelectedRow.Cells(11).Text = "" Then
                chbCentroDistribuicion.Checked = False
            End If

            cbo.LlenarCboTipoAlmacen(cboTipoAlmacen, True)
            cboTipoAlmacen.SelectedValue = CType(Me.dgvMantAlmacen.SelectedRow.Cells(12).FindControl("hdfIdtipoAlmacen"), HiddenField).Value

            cbo.llenarCboAlmacenxTipoAlmacenPrincipal(Me.cboAlmacenRef, False)
            cboAlmacenRef.SelectedValue = CType(Me.dgvMantAlmacen.SelectedRow.Cells(12).FindControl("hdfIdAlmacenRef"), HiddenField).Value


            HabilitarBotonAlmacen(False, True, False, True)
            Me.PanelBuscaAlmacen.Visible = False : Me.PanelNuevoAlmacen.Visible = True

        Catch ex As Exception
            HabilitarBotonAlmacen(False, True, False, True)
            If dgvMantAlmacen.SelectedRow.Cells(8).Text = "Activo" Then
                chkEliminaAlma.Checked = False
            ElseIf dgvMantAlmacen.SelectedRow.Cells(8).Text = "Inactivo" Then
                chkEliminaAlma.Checked = True
            End If
            Me.PanelBuscaAlmacen.Visible = False : Me.PanelNuevoAlmacen.Visible = True
        Finally
        End Try
        Me.chkEliminaAlma.Visible = True
    End Sub


    Private Sub btnCancelarMantAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelarMantAlmacen.Click
        HabilitarBotonAlmacen(True, False, False, True)
        LimpiarMantAlmacen()
        Me.PanelBuscaAlmacen.Visible = True : Me.PanelNuevoAlmacen.Visible = False
        BuscarAlmacen(Me.dgvMantAlmacen)
    End Sub
    Private Sub chkEliminaAlma_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEliminaAlma.CheckedChanged
        If Me.chkEliminaAlma.Checked = True Then
            HabilitarMantAlmacen(False)
            Me.chkEliminaAlma.Enabled = True
        ElseIf Me.chkEliminaAlma.Checked = False Then
            HabilitarMantAlmacen(True)
            Me.chkEliminaAlma.Enabled = True
        End If
    End Sub

#End Region

    '' MANTENIMIENTO AREA
    '#Region "MANTENIMIENTO AREA"

    '    Private Sub btnNuevoArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevoArea.Click
    '        HabilitarBotonArea(False, True, False, True)
    '        LimpiarMantArea()
    '        Session("IndicadorArea") = "I"
    '        Me.PanelBuscarArea.Visible = False : Me.PanelNuevoArea.Visible = True
    '        HabilitarMantArea(True)
    '        chkEliminarArea.Visible = False
    '    End Sub
    '    Private Sub btnGrabarArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGrabarArea.Click
    '        If Me.txtNombreArea.Text = "" Then : Exit Sub : End If
    '        If Me.txtNomCortoArea.Text = "" Then : Exit Sub : End If
    '        GrabarArea(CStr(Session("IndicadorArea")))
    '        HabilitarMantArea(False)
    '        LimpiarMantArea()
    '        HabilitarBotonArea(True, False, False, False)
    '        Me.PanelBuscarArea.Visible = True : Me.PanelNuevoArea.Visible = False
    '        BuscarArea(Me.dgvMantArea)
    '    End Sub
    '    Private Sub btnEditarArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditarArea.Click
    '        Me.PanelBuscarArea.Visible = True : Me.PanelNuevoArea.Visible = False
    '        Session("IndicadorArea") = "U"
    '        LimpiarMantArea()
    '        HabilitarMantArea(True)
    '    End Sub
    '    Sub HabilitarMantArea(ByVal Activar As Boolean)
    '        Me.txtNombreArea.Enabled = Activar
    '        Me.txtNomCortoArea.Enabled = Activar
    '        Me.chkEliminarArea.Enabled = Activar
    '    End Sub
    '    Sub LimpiarMantArea()
    '        Me.txtNombreArea.Text = ""
    '        Me.txtNomCortoArea.Text = ""
    '        Me.chkEliminarArea.Checked = False
    '        Me.dgvMantArea.DataBind()
    '    End Sub
    '    Sub GrabarArea(ByVal Indicador As String)
    '        Dim ngcArea As New Negocio.Area
    '        Dim objArea As New Entidades.Area
    '        If CStr(Indicador) = "I" Then
    '            With objArea
    '                .Descripcion = HttpUtility.HtmlDecode(Me.txtNombreArea.Text)
    '                .DescripcionCorta = HttpUtility.HtmlDecode(Me.txtNomCortoArea.Text)
    '                .Estado = "1"
    '            End With
    '            If ngcArea.InsertaArea(objArea) = True Then
    '            Else
    '                sc.mostrarMsjAlerta(Me, "Error al grabar el Area")
    '            End If
    '        ElseIf CStr(Indicador) = "U" Then
    '            If Me.chkEliminarArea.Checked = False Then
    '                With objArea
    '                    .Descripcion = HttpUtility.HtmlDecode(Me.txtNombreArea.Text)
    '                    .DescripcionCorta = HttpUtility.HtmlDecode(Me.txtNomCortoArea.Text)
    '                    .Estado = "1"
    '                    .Id = CInt(Session("IdAreaMant"))
    '                End With
    '                If ngcArea.ActualizaArea(objArea) = True Then
    '                Else
    '                    sc.mostrarMsjAlerta(Me, "Error al Actualizar el Area")
    '                End If
    '            ElseIf Me.chkEliminarArea.Checked = True Then
    '                With objArea
    '                    .Descripcion = HttpUtility.HtmlDecode(Me.txtNombreArea.Text)
    '                    .DescripcionCorta = HttpUtility.HtmlDecode(Me.txtNomCortoArea.Text)
    '                    .Estado = "0"
    '                    .Id = CInt(Session("IdAreaMant"))
    '                End With
    '                If ngcArea.ActualizaArea(objArea) = True Then
    '                Else
    '                    sc.mostrarMsjAlerta(Me, "Error al Eliminar el Area")
    '                End If
    '            End If
    '        End If
    '        HabilitarMantArea(False)
    '    End Sub
    '    Sub BuscarArea(ByVal grilla As GridView)
    '        Dim ngcArea As New Negocio.Area
    '        grilla.DataSource = ngcArea.SelectAllxNombreActivoInactivo("")
    '        grilla.DataBind()
    '    End Sub

    '    Sub HabilitarBotonArea(ByVal ActivarNuevo As Boolean, ByVal ActivarGuardar As Boolean, ByVal ActivarEditar As Boolean, ByVal ActivarCancelar As Boolean)
    '        Me.btnNuevoArea.Visible = ActivarNuevo
    '        Me.btnGrabarArea.Visible = ActivarGuardar
    '        Me.btnEditarArea.Visible = ActivarEditar
    '        Me.btnCancelarMantArea.Visible = ActivarCancelar
    '    End Sub

    '    'Private Sub btnBuscarArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarArea.Click
    '    '    BuscarArea(Me.dgvMantArea)
    '    'End Sub
    '    Private Sub dgvMantArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMantArea.SelectedIndexChanged
    '        Try

    '            Session("IdAreaMant") = dgvMantArea.SelectedRow.Cells(4).Text
    '            Session("IndicadorArea") = "U"
    '            HabilitarMantArea(True)
    '            txtNombreArea.Text = HttpUtility.HtmlDecode(dgvMantArea.SelectedRow.Cells(1).Text)
    '            txtNomCortoArea.Text = HttpUtility.HtmlDecode(dgvMantArea.SelectedRow.Cells(2).Text)
    '            If dgvMantArea.SelectedRow.Cells(3).Text = "Activo" Then
    '                chkEliminarArea.Checked = False
    '            ElseIf dgvMantArea.SelectedRow.Cells(3).Text = "Inactivo" Then
    '                chkEliminarArea.Checked = True
    '            End If
    '            HabilitarBotonArea(False, True, False, True)
    '            Me.PanelBuscarArea.Visible = False : Me.PanelNuevoArea.Visible = True
    '        Catch ex As Exception
    '            HabilitarBotonArea(False, True, False, True)
    '            If dgvMantArea.SelectedRow.Cells(3).Text = "Activo" Then
    '                chkEliminarArea.Checked = False
    '            ElseIf dgvMantArea.SelectedRow.Cells(3).Text = "Inactivo" Then
    '                chkEliminarArea.Checked = True
    '            End If
    '            Me.PanelBuscarArea.Visible = False : Me.PanelNuevoArea.Visible = True
    '            HabilitarMantArea(True)
    '        Finally
    '        End Try
    '        chkEliminarArea.Visible = True
    '    End Sub

    '    Private Sub btnCancelarMantArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelarMantArea.Click
    '        HabilitarBotonArea(True, False, False, True)
    '        LimpiarMantArea()
    '        Me.PanelBuscarArea.Visible = True : Me.PanelNuevoArea.Visible = False
    '        BuscarArea(Me.dgvMantArea)
    '    End Sub
    '    Private Sub chkEliminarArea_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEliminarArea.CheckedChanged
    '        If Me.chkEliminarArea.Checked = True Then
    '            HabilitarMantArea(False)
    '            Me.chkEliminarArea.Enabled = True
    '        ElseIf Me.chkEliminarArea.Checked = False Then
    '            HabilitarMantArea(True)
    '            Me.chkEliminarArea.Enabled = True
    '        End If
    '    End Sub
    '#End Region


    '' MANTENIMIENTO tipo almacen

#Region "Mant TipoAlmacen"

    Private Sub ibtnNuevoTAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnNuevoTAlmacen.Click
        HabilitarBotonTipoAlmacen(False, True, False, True)
        LimpiarMantTAlmacen()
        Session("IndicadorTipoAlmacen") = "I"
        Me.panelBuscarTAlmacen.Visible = False : Me.PanelNuevoTipoAlmacen.Visible = True
        HabilitarMantTAlmacen(True)
        Me.chkTAlmacenPrincipal.Checked = False
        'chkEliminarArea.Visible = False

    End Sub

    Private Sub ibtnGuardartAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnGuardartAlmacen.Click

        If Me.txtNombreTAlmacen.Text = "" Then : Exit Sub : End If
        If Me.txtNombrecortoTAlmacen.Text = "" Then : Exit Sub : End If

        'If ValidaTAlmacenPrincipal() = True Then
        '    sc.mostrarMsjAlerta(Me, "ya Existe un tipo de Almacen Principal")

        If ValidaInactivoPrincipal() = True Then
            sc.mostrarMsjAlerta(Me, "el Tipo almacen es inactivo no puede ser principal")
        Else
            Try

                GrabarTipoAlmacen(CStr(Session("IndicadorTipoAlmacen")))
                HabilitarMantTAlmacen(False)
                LimpiarMantTAlmacen()
                HabilitarBotonTipoAlmacen(True, False, False, False)
                Me.panelBuscarTAlmacen.Visible = True : Me.PanelNuevoTipoAlmacen.Visible = False
                BuscarTipoAlmacen(Me.gvwTAlmacen)

            Catch ex As Exception
                sc.mostrarMsjAlerta(Me, ex.Message)
            End Try

        End If

    End Sub

    Private Sub ibtnEditarTAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnEditarTAlmacen.Click

        Me.panelBuscarTAlmacen.Visible = True : Me.PanelNuevoTipoAlmacen.Visible = False
        Session("IndicadorTipoAlmacen") = "U"
        LimpiarMantTAlmacen()
        HabilitarMantTAlmacen(True)

    End Sub

    Private Sub ibtnCancelarTAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnCancelarTAlmacen.Click
        HabilitarBotonTipoAlmacen(True, False, False, True)
        LimpiarMantTAlmacen()
        Me.panelBuscarTAlmacen.Visible = True : Me.PanelNuevoTipoAlmacen.Visible = False
        BuscarTipoAlmacen(Me.gvwTAlmacen)
    End Sub
    Sub HabilitarMantTAlmacen(ByVal Activar As Boolean)

        Me.txtNombreTAlmacen.Enabled = Activar
        Me.txtNombrecortoTAlmacen.Enabled = Activar
        Me.chkTAlmacenPrincipal.Enabled = Activar
        Me.rbtTAEstado.Enabled = Activar

    End Sub
    Sub LimpiarMantTAlmacen()

        Me.txtNombreTAlmacen.Text = ""
        Me.txtNombrecortoTAlmacen.Text = ""
        Me.chkTAlmacenPrincipal.Text = ""
        'Me.rbtTAEstado.Text = ""
        Me.gvwTAlmacen.DataBind()
    End Sub

    Sub GrabarTipoAlmacen(ByVal Indicador As String)

        Dim negTAlmacen As New Negocio.TipoAlmacen
        Dim objTAlmacen As New Entidades.TipoAlmacen

        Try

            If CStr(Indicador) = "I" Then
                With objTAlmacen
                    .Tipoalm_Nombre = HttpUtility.HtmlDecode(Me.txtNombreTAlmacen.Text)
                    .Tipoalm_NombreCorto = HttpUtility.HtmlDecode(Me.txtNombrecortoTAlmacen.Text)
                    If Me.chkTAlmacenPrincipal.Checked = True Then
                        .Tipoalm_Principal = "1"
                    Else
                        .Tipoalm_Principal = "0"
                    End If
                    If Me.rbtTAEstado.SelectedValue = "1" Then
                        .Tipoalm_Estado = 1
                    Else
                        .Tipoalm_Estado = 0
                    End If
                End With



                If ValidoRep.ValidarExistenciaxTablax1Campo("TipoAlmacen", "Tipoalm_Nombre", objTAlmacen.Tipoalm_Nombre) <= 0 Then
                    If negTAlmacen.insertTipoAlmacen(objTAlmacen) = True Then
                    Else
                        sc.mostrarMsjAlerta(Me, "Error al grabar el tipo de Almacen")
                    End If
                Else
                    sc.mostrarMsjAlerta(Me, "ya existe el nombre.")
                End If



            ElseIf CStr(Indicador) = "U" Then


                With objTAlmacen
                    .Tipoalm_Nombre = HttpUtility.HtmlDecode(Me.txtNombreTAlmacen.Text)
                    .Tipoalm_NombreCorto = HttpUtility.HtmlDecode(Me.txtNombrecortoTAlmacen.Text)
                    If Me.chkTAlmacenPrincipal.Checked = True Then
                        .Tipoalm_Principal = "1"
                    Else
                        .Tipoalm_Principal = "0"
                    End If
                    If Me.rbtTAEstado.SelectedValue = "1" Then
                        .Tipoalm_Estado = 1
                    Else
                        .Tipoalm_Estado = 0
                    End If
                    .IdTipoAlmacen = CInt(Session("idtipoalmacen"))
                End With

                If ValidoRep.ValidarExistenciaxTablax2CamposForUpdate("TipoAlmacen", "Tipoalm_Nombre", objTAlmacen.Tipoalm_Nombre, "", objTAlmacen.IdTipoAlmacen) <= 0 Then
                    If negTAlmacen.updateTipoAlmacen(objTAlmacen) = True Then
                    Else
                        sc.mostrarMsjAlerta(Me, "Error al Actualizar el tipo de almacen")
                    End If
                Else
                    sc.mostrarMsjAlerta(Me, "ya existe el nombre.")
                End If


                
            End If
            'HabilitarMantArea(False)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Sub BuscarTipoAlmacen(ByVal grilla As GridView)
        Dim ngcArea As New Negocio.Area

        Dim negtAlmacen As New Negocio.TipoAlmacen
        grilla.DataSource = negtAlmacen.SelectActivoInactivo
        grilla.DataBind()

        setListaTAlmacenView(negtAlmacen.SelectActivoInactivo)

    End Sub
    Private Function ValidaTAlmacenPrincipal() As Boolean
        Dim objtAlmacen As New Entidades.TipoAlmacen
        Dim princ As Boolean = False

        If Me.chkTAlmacenPrincipal.Checked = True Then
            For Each objtAlmacen In getListaTAlmacenView()
                If objtAlmacen.strTipoalm_Principal = "Principal" Then
                    princ = True
                End If
            Next
        End If
        Return princ

    End Function
    Private Function ValidaUnPrincipal() As Boolean
        Dim objtAlmacen As New Entidades.TipoAlmacen
        Dim princ As Boolean = False

    End Function
    Private Function ValidaInactivoPrincipal() As Boolean
        Dim princ As Boolean = False

        If Me.rbtTAEstado.SelectedValue = "0" Then
            If chkTAlmacenPrincipal.Checked = True Then
                princ = True
            End If
        End If
        Return princ
    End Function

    Sub HabilitarBotonTipoAlmacen(ByVal ActivarNuevo As Boolean, ByVal ActivarGuardar As Boolean, ByVal ActivarEditar As Boolean, ByVal ActivarCancelar As Boolean)

        Me.ibtnNuevoTAlmacen.Visible = ActivarNuevo
        Me.ibtnGuardartAlmacen.Visible = ActivarGuardar
        Me.ibtnEditarTAlmacen.Visible = ActivarEditar
        Me.ibtnCancelarTAlmacen.Visible = ActivarCancelar

    End Sub
    Private Sub gvwTAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvwTAlmacen.SelectedIndexChanged

        Try
            'Session("idTipoAlmacen") = dgvMantArea.SelectedRow.Cells(4).Text
            'Session.Add("idTipoAlmacen", CInt(CType((Me.gvwTAlmacen.Rows(Me.gvwTAlmacen.SelectedIndex).Cells(5).FindControl("hdfIdTipoAlmacen")), HiddenField).Value))
            Session("idTipoAlmacen") = CInt(CType((Me.gvwTAlmacen.Rows(Me.gvwTAlmacen.SelectedIndex).Cells(5).FindControl("hdfIdTipoAlmacen")), HiddenField).Value)

            Session.Add("IndicadorTipoAlmacen", "U")

            HabilitarMantTAlmacen(True)

            txtNombreTAlmacen.Text = HttpUtility.HtmlDecode(gvwTAlmacen.SelectedRow.Cells(1).Text)
            txtNombrecortoTAlmacen.Text = HttpUtility.HtmlDecode(gvwTAlmacen.SelectedRow.Cells(2).Text)

            If gvwTAlmacen.SelectedRow.Cells(3).Text = "Principal" Then
                Me.chkTAlmacenPrincipal.Checked = True
            Else
                Me.chkTAlmacenPrincipal.Checked = False
            End If

            If gvwTAlmacen.SelectedRow.Cells(4).Text = "Activo" Then
                rbtTAEstado.SelectedValue = "1"
            Else
                rbtTAEstado.SelectedValue = "0"
            End If

            HabilitarBotonTipoAlmacen(False, True, False, True)
            Me.panelBuscarTAlmacen.Visible = False : Me.PanelNuevoTipoAlmacen.Visible = True

        Catch ex As Exception
            'HabilitarBotonArea(False, True, False, True)
            'If dgvMantArea.SelectedRow.Cells(3).Text = "Activo" Then
            '    chkEliminarArea.Checked = False
            'ElseIf dgvMantArea.SelectedRow.Cells(3).Text = "Inactivo" Then
            '    chkEliminarArea.Checked = True
            'End If
            'Me.PanelBuscarArea.Visible = False : Me.PanelNuevoArea.Visible = True
            'HabilitarMantArea(True)
        Finally
        End Try


    End Sub

    Protected Sub btnEliminarTipoAlmacen_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim lbtnQuitar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
        Dim fila As GridViewRow
        fila = CType(lbtnQuitar.NamingContainer, GridViewRow)
        Me.quitarRegistroProducto(CInt(CType(fila.Cells(5).FindControl("hdfIdTipoAlmacen"), HiddenField).Value.Trim()), fila.RowIndex)
        'CInt(CType(fila.Cells(0).FindControl("hddIdMoneda"), HiddenField).Value.Trim())

    End Sub

    Private Sub quitarRegistroProducto(ByVal cuenta As Integer, ByVal DelFila As Integer)

        Dim objUtil As New Negocio.Util
        Try
            '****************** validamos que no tenga registros relacionados
            If objUtil.ValidarxDosParametros("almacen", "Idalmacen", CStr(cuenta), "IdCuentaPersona", CStr(cuenta)) > 0 Then
                sc.mostrarMsjAlerta(Me, "El tipo de Almacen Tiene Almacenes Asociados.")
                Return
            End If
            '**************** quitamos de la lista
            Dim lista As New List(Of Entidades.TipoAlmacen)
            lista = getListaTAlmacenView()

            lista.RemoveAt(DelFila)

            setListaTAlmacenView(lista)

            gvwTAlmacen.DataSource = lista
            gvwTAlmacen.DataBind()

        Catch ex As Exception
            sc.mostrarMsjAlerta(Me, "Problemas en la eliminación del registro.")
        End Try
    End Sub

    Private Sub setListaTAlmacenView(ByVal lista As List(Of Entidades.TipoAlmacen))
        'lista = New List(Of Entidades.ProductoView)
        Session.Remove("listaTipoAlmacen")
        Session.Add("listaTipoAlmacen", lista)
    End Sub
    Private Function getListaTAlmacenView() As List(Of Entidades.TipoAlmacen)
        Return CType(Session.Item("listaTipoAlmacen"), List(Of Entidades.TipoAlmacen))
    End Function



    Protected Sub lbtnMostrar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lbtnMostrar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
            Dim fila As GridViewRow = CType(lbtnMostrar.NamingContainer, GridViewRow)
            'mostrar detalle documento
            'Me.gvwCxCxDocumento.DataSource = Me.mostrardocumento(VSIdPersona, CInt(CType(fila.Cells(0).FindControl("hddIdMoneda"), HiddenField).Value.Trim()))
            'gvwCxCxDocumento.DataBind()

            'actualizar(CInt(CType(fila.Cells(5).FindControl("hdfIdTipoAlmacen"), HiddenField).Value.Trim()))

        Catch ex As Exception
        End Try
    End Sub


#End Region



End Class
