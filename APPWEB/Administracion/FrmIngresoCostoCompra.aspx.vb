﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmIngresoCostoCompra
    Inherits System.Web.UI.Page

    Private listaMoneda As List(Of Entidades.Moneda)
    Private cbo As Combo
    Private objScript As New ScriptManagerClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Dim objScript As New ScriptManagerClass
        Try
            cargarDatosControles()
        Catch ex As Exception
            txtCompraC.Text = "0"
            txtVentaC.Text = "0"
            objScript.mostrarMsjAlerta(Me, ex.Message)
        Finally
            Panel_Cabecera.Visible = True
            Panel_Grilla.Visible = False
        End Try
    End Sub
#Region "CARGAR DATOS CONTROLES"
    Private Sub cargarDatosControles()
        cbo = New Combo
        cbo.llenarCboTipoExistencia(cmbTipoExistencia)
        If cmbTipoExistencia.Items.FindByValue("1") IsNot Nothing Then ' Mecaderia es un Id del sistema
            cmbTipoExistencia.SelectedValue = "1"
        End If
        cbo.llenarCboLineaxTipoExistencia(cmbLinea, CInt(cmbTipoExistencia.SelectedValue), True)
        cbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cmbSubLinea, CInt(cmbLinea.SelectedValue), CInt(cmbTipoExistencia.SelectedValue), True)

        cargarDatosMoneda(Me.cmbTipoMoneda)
        cargarTipoDeCambio()
    End Sub
    Private Sub cargarDatosMoneda(ByVal cbo As DropDownList)
        Dim objMoneda As New Negocio.Moneda
        cbo.DataSource = objMoneda.SelectCbo
        cbo.DataBind()
    End Sub
    Private Sub cargarTipoDeCambio()
        Dim obj As New Negocio.TipoCambio
        Dim TipoCambio As New Entidades.TipoCambio
        TipoCambio = obj.SelectOficial(2)
        Me.txtCompraC.Text = CStr(Decimal.Round(TipoCambio.CompraOf, 4))
        Me.txtVentaC.Text = CStr(Decimal.Round(TipoCambio.VentaOf, 4))
    End Sub


#End Region
    Protected Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLinea.SelectedIndexChanged
        cbo = New Combo
        cbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(cmbSubLinea, CInt(cmbLinea.SelectedValue), CInt(cmbTipoExistencia.SelectedValue), True)
    End Sub

    Protected Sub btnAceptar_SubLinea_Cab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar_SubLinea_Cab.Click
        cargardatosgrilla()
    End Sub
    Private Sub cargardatosgrilla()
        Dim objScript As New ScriptManagerClass
        Try
            cargarListaMoneda()
            Dim objProducto As New Negocio.Producto
            Dim prod_Nombre As String = String.Empty
            Dim prod_Codigo As String = String.Empty
            Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

            Select Case CInt(rbTipoBusqueda.SelectedValue)
                Case 0
                    prod_Codigo = txt_descripcion.Text.Trim
                Case 1
                    prod_Nombre = txt_descripcion.Text.Trim
            End Select


            DGV_SubLinea.DataSource = _
            objProducto.ProductoCostoCompraSelectxIdSubLinea(CInt(cmbTipoExistencia.SelectedValue), _
                                                             CInt(cmbLinea.SelectedValue), CInt(Me.cmbSubLinea.SelectedValue), _
                                                            prod_Codigo, prod_Nombre, tableTipoTabla)
            DGV_SubLinea.DataBind()
            Panel_Cabecera.Enabled = False
            Panel_Grilla.Visible = True
            GV_FiltroTipoTabla.DataBind()

            'ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularCostoDolaresOnLoad();", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos")
        End Try
    End Sub


    Private Sub cargarListaMoneda()
        Dim objMoneda As New Negocio.Moneda
        Me.listaMoneda = objMoneda.SelectCbo
        Dim objMonedaEntidad As New Entidades.Moneda
        objMonedaEntidad.Id = 0
        objMonedaEntidad.Simbolo = "---"
        listaMoneda.Insert(0, objMonedaEntidad)
    End Sub
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        Panel_Cabecera.Enabled = True
        Panel_Grilla.Visible = False
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        registrarPrecioCompra()
    End Sub
    Private Sub registrarPrecioCompra()
        Dim idusuario As Integer
        idusuario = CType(Session("IdUsuario"), Integer)
        Dim objScript As New ScriptManagerClass
        Try
            Dim lista As New List(Of Entidades.Producto)
            For i As Integer = 0 To DGV_SubLinea.Rows.Count - 1
                Dim obj As New Entidades.Producto
                With DGV_SubLinea
                    obj.Id = CInt(CType(.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                    obj.PrecioCompra = CDec(CType(.Rows(i).Cells(7).FindControl("txtCostoNuevo"), TextBox).Text)
                    obj.IdMoneda = CInt(CType(.Rows(i).Cells(3).FindControl("cmbMoneda_Producto"), DropDownList).SelectedValue)
                End With
                lista.Add(obj)
            Next
            Dim objNegProd As New Negocio.Producto
            If objNegProd.ActualizaProductoPrecioCompra(idusuario, lista) Then
                objScript.mostrarMsjAlerta(Me, "La Operación se realizó con éxito.")
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Se presentaron problemas en la transacción.")
        End Try
    End Sub
    Private Sub DGV_SubLinea_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_SubLinea.RowDataBound
        Try
            Dim lista As List(Of Entidades.ProductoUMCostoCompraView) = CType(DGV_SubLinea.DataSource, List(Of Entidades.ProductoUMCostoCompraView))
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cmbUMedida1 As DropDownList
                Dim gvrow As GridViewRow = CType(e.Row.Cells(3).NamingContainer, GridViewRow)
                cmbUMedida1 = CType(gvrow.FindControl("cmbMoneda_Producto"), DropDownList)
                If cmbUMedida1 IsNot Nothing Then
                    cmbUMedida1.DataSource = Me.listaMoneda
                    cmbUMedida1.DataBind()
                End If
                cmbUMedida1.SelectedValue = lista.Item(e.Row.RowIndex).IdMoneda.ToString
            End If
        Catch ex As Exception
        End Try
    End Sub

#Region "Filtro de busquedad avanza de productos"

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0

            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea.SelectedValue), CInt(Me.cmbSubLinea.SelectedValue), CInt(cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1
            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)
        Next

        Return dt
    End Function

    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function

#End Region

    Private Sub cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubLinea.SelectedIndexChanged
        cbo = New Combo
        cbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea.SelectedValue), False)
    End Sub
End Class