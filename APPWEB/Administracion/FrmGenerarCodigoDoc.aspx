﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmGenerarCodigoDoc.aspx.vb" Inherits="APPWEB.FrmGenerarCodigoDoc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td class="TituloCelda">
                GENERACIÓN DE CORRELATIVOS
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Label" align="right">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true" Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td class="Label" align="right">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Label" align="right">
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="Label" align="right">
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="Label">
                            Tipo Documento:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTipoDocumento" runat="server" AutoPostBack="true" Font-Bold="true">
                                <asp:ListItem Value="14">Cotización</asp:ListItem>
                                <asp:ListItem Value="3">Boleta</asp:ListItem>
                                <asp:ListItem Value="30" Selected="True">Comprobante de Percepción</asp:ListItem>
                                <asp:ListItem Value="7">Comprobante de Retención</asp:ListItem>
                                <asp:ListItem Value="1">Factura</asp:ListItem>
                                <asp:ListItem Value="25">Guía de Recepción</asp:ListItem>
                                <asp:ListItem Value="6">Guía de Remisión - Remitente</asp:ListItem>
                                <asp:ListItem Value="47">Guia de Control Interno</asp:ListItem>
                                <asp:ListItem Value="20">Letra</asp:ListItem>
                                <asp:ListItem Value="4">Nota de Crédito</asp:ListItem>
                                <asp:ListItem Value="5">Nota de Débito</asp:ListItem>
                                <asp:ListItem Value="16">Orden de Compra</asp:ListItem>
                                <asp:ListItem Value="21">Orden de Despacho</asp:ListItem>
                                <asp:ListItem Value="18">Recibo de Egreso</asp:ListItem>
                                <asp:ListItem Value="17">Recibo de Ingreso</asp:ListItem>
                                <asp:ListItem Value="1101353001">Factura Electrónica</asp:ListItem>
                                <asp:ListItem Value="1101353002">Boleta Electrónica</asp:ListItem>
                                <asp:ListItem Value="1101353003">NOTA DE CREDITO ELECTRONICA</asp:ListItem>
                                <asp:ListItem Value="1101353004">HOJA DE PICKING</asp:ListItem>
                                
                            </asp:DropDownList>
                        </td>
                        <td align="right" class="Label">
                            Serie:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="true" Width="100%" CssClass="LabelRojo"
                                Font-Bold="true">
                            </asp:DropDownList>
                        </td>
                        <td align="right" class="Label">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="right" class="Label">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" align="right" style="font-weight: bold">
                            Nro. Actual:
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtCodACTUAL" ReadOnly="true" CssClass="TextBox_ReadOnly" runat="server"
                                Width="100px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="Label" align="right" style="font-weight: bold">
                            Inicio:
                        </td>
                        <td colspan="7">
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtCodInicio" onFocus=" return( aceptarFoco(this)  ); " onKeyPress="return( onKeyPressEsNumero('event') );"
                                            runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                    <td class="Label" align="right" style="font-weight: bold">
                                        Fin:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCodFIN" onFocus=" return( aceptarFoco(this)  ); " onKeyPress="return( onKeyPressEsNumero('event') );"
                                            runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Imagenes/Guardar_B.JPG"
                                            OnClientClick="return( valSave()  );" onmouseout="this.src='../Imagenes/Guardar_B.JPG';"
                                            onmouseover="this.src='../Imagenes/Guardar_A.JPG';" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function valSave() {

            //************* validamos que exista Serie
            var IdSerie = document.getElementById('<%=cboSerie.ClientID%>').value;
            if (IdSerie.length == 0 || isNaN(IdSerie)) {
                alert('DEBE SELECCIONAR UNA SERIE. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            var CodigoInicio = 0;
            var CodigoFin = 0;

            var txtCodInicio = document.getElementById('<%=txtCodInicio.ClientID%>');
            if (isNaN(parseInt(txtCodInicio.value)) || txtCodInicio.value.length <= 0) {
                alert('INGRESE UN VALOR VÁLIDO. NO SE PERMITE LA OPERACIÓN.');
                txtCodInicio.select();
                txtCodInicio.focus();
                return false;
            } else {
                CodigoInicio = parseFloat(txtCodInicio.value);
            }
            var txtCodFin = document.getElementById('<%=txtCodFIN.ClientID%>');
            if (isNaN(parseInt(txtCodFin.value)) || txtCodFin.value.length <= 0) {
                alert('INGRESE UN VALOR VÁLIDO. NO SE PERMITE LA OPERACIÓN.');
                txtCodFin.select();
                txtCodFin.focus();
                return false;
            } else {
                CodigoFin = parseFloat(txtCodFin.value);
            }

            if (CodigoFin < CodigoInicio) {
                alert('EL CÓDIGO FINAL ES MENOR AL CÓDIGO INICIAL. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var cad = 'Inicio : < ' + CodigoInicio + ' > Fin : < ' + CodigoFin + ' > Nro. de Documentos a Generar : < ' + (CodigoFin - CodigoInicio + 1) + ' >. Desea continuar con la Operación ?';
            return (confirm(cad));
        }        
        
    </script>

</asp:Content>
