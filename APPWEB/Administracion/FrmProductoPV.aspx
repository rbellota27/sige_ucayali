﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmProductoPV.aspx.vb" Inherits="APPWEB.FrmProductoPV" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Réplica de Precios Por Producto
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="Texto">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Tienda Origen:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTiendaOrigen" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend><span class="Texto"></span></legend>
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnOpen_Producto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                                    onmouseout="this.src='../Imagenes/BuscarProducto_b.JPG';" onmouseover="this.src='../Imagenes/BuscarProducto_A.JPG';" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Producto" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Código" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCodigo" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Descripcion" ItemStyle-HorizontalAlign="Center" HeaderText="Descripción"
                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                        <asp:BoundField DataField="UnidadMedida" ItemStyle-HorizontalAlign="Center" HeaderText="U.M."
                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---"
                                            ItemStyle-Font-Bold="true" />
                                        <asp:BoundField DataField="Linea" ItemStyle-HorizontalAlign="Center" HeaderText="Línea"
                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                        <asp:BoundField DataField="SubLinea" ItemStyle-HorizontalAlign="Center" HeaderText="Sub Línea"
                                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Tipo Precio:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboTipoPrecio" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="imgTipoPrecioAdd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                OnClientClick=" return ( TipoPrecioAdd() ); " ToolTip="Consultar Producto Precio Venta" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="imgLimpiarTipoPrecioAdd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                ToolTip="Limpiar Producto Tipo Precio Venta" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="ckProducto" runat="server" Text="Calcular precio de venta (Otras U.M) a través de la equivalencia"
                                                CssClass="Texto" onClick=" return ( valEquivalenciaxProducto() ); " />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="DGV_PRODUCTO" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" SelectText="Quitar" ButtonType="Link" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="U. M." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdUM_PR" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblNomUMPrincipal_PR" runat="server" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NomUMedida")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddUnidadPrincipal" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"UnidadPrincipal")%>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="Equivalencia" HeaderText="Equivalencia" NullDisplayText="0" DataFormatString="{0:F4}" />
                                        <asp:BoundField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="getDescUMPrincipal" HeaderText="Principal" NullDisplayText="--" />
                                        <asp:BoundField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="getDescUMRetazo" HeaderText="Retazo" NullDisplayText="--" />
                                        <asp:BoundField DataField="PorcentRetazo" HeaderText="P. Retazo (%)" NullDisplayText="0"
                                            DataFormatString="{0:F2}" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Util. (%)" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPorcentUtilidad_PR" runat="server" onkeyup="return(calcularPrecioVenta_PR('0','1'));"
                                                    onblur="return(valGrillaBlur());" onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPunto(event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PUtilFijo","{0:F2}")%>' Width="60px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Util." HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblMonUtilidad_PR" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMonedaPC")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtUtilidad_PR" runat="server" onkeyup="return(calcularPrecioVenta_PR('1','1'));"
                                                                onblur="return(valGrillaBlur());" onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPunto(event));"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Utilidad","{0:F2}")%>' Width="60px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P.V. Final ($)" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPVFinal_Dolares_PR" runat="server" onkeyup="return(calcularPrecioVenta_PR('2','1'));"
                                                    onblur="return(valGrillaBlur());" onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPunto(event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PV_Dolares","{0:F2}")%>' Width="60px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P.V. Final (S/.)" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPVFinal_Soles_PR" runat="server" onkeyup="return(calcularPrecioVenta_PR('3','1'));"
                                                    onblur="return(valGrillaBlur());" onfocus="return(aceptarFoco(this));" onKeypress="return(validarNumeroPunto(event));"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"PV_Soles","{0:F2}")%>' Width="60px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Moneda Destino" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboMonedaDestino_PR" runat="server">
                                                    <asp:ListItem Selected="True" Value="1">S/.</asp:ListItem>
                                                    <asp:ListItem Value="2">$</asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Activo" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chbEstado_PR" runat="server" AutoPostBack="false" Checked='<%#DataBinder.Eval(Container.DataItem,"EstadoBoolean")%>'
                                                    ToolTip="Haga clic para activar el registro." />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            DataField="TipoPV" HeaderText="Tipo Precio" NullDisplayText="--" ItemStyle-Font-Bold="true"
                                            ItemStyle-ForeColor="Red" />
                                    </Columns>
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                Tienda Destino
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="Texto">
                            Descripción:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTiendaDestino" runat="server" DataTextField="Nombre" DataValueField="Id">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:ImageButton ID="imgTiendaAdd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                OnClientClick=" return ( Tienda_add() ); " ToolTip="Replicar precios de venta" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="2">
                            <asp:GridView ID="GV_Tienda" runat="server" AutoGenerateColumns="False" Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lkbTiendaRemove" runat="server" OnClick="lkbTiendaRemove_Click">Quitar</asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hddIdTienda" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Tienda" DataField="Nombre" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                    </asp:BoundField>
                                </Columns>
                                <RowStyle CssClass="GrillaRow" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                Tipos de Precio de Venta
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:GridView ID="GV_PV" Width="600px" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px">
                            <ItemTemplate>
                                <asp:CheckBox ID="chbTipoPV" onClick="return(   valOnClickChbSelect(this)  );" runat="server"
                                    ToolTip="Seleccione el Tipo de Precio de Venta Destino." />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Precio Venta Destino" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:HiddenField ID="hddIdTipoPV" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoPv")%>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTipoPV_Destino" Font-Bold="true" ForeColor="Red" runat="server"
                                                Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Precio Venta Origen"
                            ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:DropDownList ID="cboTipoPV_Origen" runat="server" ToolTip="Tipo de Precio de Venta Origen">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Porcentaje (%)"
                            ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:TextBox ID="txtPorcentPV" TabIndex="500" onblur="return(   valBlurClear('0',event)    );"
                                    onKeypress="return(  validarNumeroPuntoSigno(event)    );" onFocus="return(  aceptarFoco(this)   );"
                                    Text="0" runat="server" Width="80px" Font-Bold="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" HeaderText="Sumar Costo Flete">
                            <ItemTemplate>
                                <asp:CheckBox ID="chbCostoFlete" onClick="return(   onClick_chbCostoFlete(this)  );"
                                    runat="server" ToolTip="Seleccione el Tipo de Precio de Venta Destino." />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <HeaderStyle CssClass="GrillaHeader" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="center" style="height: 50px">
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnEjecutar_Replica" OnClientClick="return(  valOnClickGuardar()  );"
                                Height="30px" Width="255px" runat="server" Text="Ejecutar Réplica de Precios de Venta"
                                ToolTip="Ejecutar Réplica de Precios de Venta." />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCeldaLeft">
                Consultar Precios < Tienda Destino >
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnProducto_TipoPrecioV" runat="server" Text="Consultar Precio Venta Destino"
                    OnClientClick="return ( Producto_TipoPrecioV() );" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="GV_PrecioV" Width="100%" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="TiendaDestino" HeaderText="Tienda Destino" />
                        <asp:BoundField DataField="TipoPV" HeaderText="Precio Venta" />
                        <asp:BoundField DataField="NomUMedida" HeaderText="U.M." />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Precio Venta Origen"
                            ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMoneda" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPrecioV" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Valor","{0:F2}")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Costo Flete" ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMonedaFlete" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCostoFlete" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CostoFlete","{0:F2}")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Nuevo P.V. Destino"
                            ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMonedaFlete" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCostoFlete" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NuevoPVDestino","{0:F2}")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Actual P.V. Destino"
                            ItemStyle-Font-Bold="true">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMonedaFlete" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCostoFlete" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ActualPVDestino","{0:F2}")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <HeaderStyle CssClass="GrillaHeader" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="height: 500px">
                <asp:HiddenField ID="hddIdProducto" runat="server" />
                <asp:HiddenField ID="hddMoneda_PrecioCompra" runat="server" />
                <asp:HiddenField ID="hddPrecioCompra_prod" runat="server" />
                <asp:HiddenField ID="hddIdMoneda_PC" runat="server" />
                <asp:HiddenField ID="hddIdUnidadMedida_Principal" runat="server" />
                <asp:HiddenField ID="hddTipoCambio" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <cc1:ModalPopupExtender ID="ModalPopup_Producto" runat="server" TargetControlID="btnOpen_Producto"
                                PopupControlID="Panel_Producto" BackgroundCssClass="modalBackground" Enabled="true"
                                RepositionMode="None" CancelControlID="btnClose_Producto" Y="50">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel_Producto" runat="server" CssClass="modalPopup" Style="display: none;"
                                Width="750px">
                                <table style="width: 100%;">
                                    <tr>
                                        <td align="right">
                                            <asp:ImageButton ID="btnClose_Producto" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto">
                                                        TipoExistencia:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Label ID="Label17" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                                            DataTextField="Descripcion" DataValueField="Id">
                                                        </asp:DropDownList>
                                                        <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub Línea:"></asp:Label>
                                                        <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                                            AutoPostBack="true" DataValueField="Id">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right">
                                                        <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripción:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                                        onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );"></asp:TextBox>
                                                                </td>
                                                                <td class="Texto" style="text-align: right">
                                                                    Cód.:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                                        onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );"
                                                                        TabIndex="205"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                        TabIndex="207" onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                                </td>
                                                                <td>
                                                                    <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                                        onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';"
                                                                        OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                                                CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                                                CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                                                CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                CollapsedText="Búsqueda Avanzada" ExpandedText="Búsqueda Avanzada" ExpandDirection="Vertical"
                                                SuppressPostBack="true">
                                            </cc1:CollapsiblePanelExtender>
                                            <asp:Image ID="Image21_11" runat="server" Height="16px" />
                                            <asp:Label ID="Label21_11" runat="server" Text="Búsqueda Avanzada" CssClass="Label"></asp:Label>
                                            <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                                                <table width="100">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td class="Texto" style="font-weight: bold">
                                                                        Atributo:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                                                Width="650px">
                                                                <Columns>
                                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                                        HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                        ItemStyle-Height="25px" />
                                                                    <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                                            <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Width="85px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                                                    <asp:TemplateField HeaderText="Código" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblCodigo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                                    <asp:BoundField DataField="UnidadMedida" ItemStyle-HorizontalAlign="Center" HeaderText="U.M."
                                                        HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                                                Width="50px" Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                                            <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                                                Width="50px" Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                                            <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                                                CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                                            <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                                                Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionProductos('2'));" />
                                            <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                                                runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
		

        function valGrillaBlur() {
            var id = event.srcElement.id;
            var caja = document.getElementById(id);
            if (CajaEnBlanco(caja)) {
                caja.focus();
                alert('Debe ingresar un valor.');
                return true;
            }
            if (!esDecimal(caja.value)) {
                caja.focus();
                alert('Valor no válido.');
                return true;
            }
        }

        function valKeyPressCodigoSL(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }


        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }

        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        //*****************
        function Tienda_add() {

            var cboTiendaDestino = document.getElementById('<%=cboTiendaDestino.ClientID %>');
            var GV_Tienda = document.getElementById('<%=GV_Tienda.ClientID %>');

            if (GV_Tienda != null) {

                for (var i = 1; i < GV_Tienda.rows.length; i++) {
                    var rowElem = GV_Tienda.rows[i];

                    if (rowElem.cells[0].children[0].cells[2].children[0].value == cboTiendaDestino.value) {

                        alert('LA LISTA CONTIENE LA TIENDA [ ' + cboTiendaDestino.options[cboTiendaDestino.selectedIndex].text + ' ] SELECCIONADA. \n NO SE PERMITE LA OPERACIÓN.');
                        return false;
                    }

                }

            }

            return true;
        }

        function valOnClickChbSelect(chbSelect) {
            var grilla = document.getElementById('<%=GV_PV.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[0].children[0].id == chbSelect.id) {
                        rowElem.cells[3].children[0].value = '0';
                        return true;
                    }

                }
            }

            return true;
        }

        function onClick_chbCostoFlete(obj) {
            var grilla = document.getElementById('<%=GV_PV.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[0].children[0].id == obj.id) {
                        rowElem.cells[3].children[0].value = '0';
                        return true;
                    }

                }
            }

            return true;
        }

        function valOnClickGuardar() {

            var grilla = document.getElementById('<%=GV_PV.ClientID%>');
            if (grilla != null) {
                var cont = 0;
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[0].children[0].children[0].status) {
                        cont = cont + 1;
                    }

                }
                if (cont <= 0) {
                    alert('Debe seleccionar al menos un Tipo de Precio de Venta Destino.');
                    return false;
                }
            } else {
                alert('Debe seleccionar al menos un Tipo de Precio de Venta Destino.');
                return false;
            }

            if (Producto_TipoPrecioV() == false) { return false; }

            return confirm('Los cambios solicitados no podrán ser deshechos una vez realizados. Se recomienda verificar las opciones seleccionadas antes de Replicar los Precios de Venta. Desea continuar de todas formas con la Operación ?');
        }

        function Producto_TipoPrecioV() {
            var IdProducto = parseInt(document.getElementById('<%=hddIdProducto.ClientID %>').value);
            if (isNaN(IdProducto)) { IdProducto = 0; }
            if (IdProducto == 0) {
                alert('Ingrese un Producto. No se permite la operación.');
                return false;
            }

            var grilla = document.getElementById('<%=GV_Tienda.ClientID%>');
            if (grilla == null) {
                alert('Agregue Tiendas de destino. No se permite la operación.');
                return false;
            }



            return true;
        }

        function TipoPrecioAdd() {

            var IdProducto = parseInt(document.getElementById('<%=hddIdProducto.ClientID %>').value);
            if (isNaN(IdProducto)) { IdProducto = 0; }
            if (IdProducto == 0) {
                alert('Ingrese un Producto. No se permite la operación.');
                return false;
            }

            return true;

        }




        //// ******************************** CALCULOS


        function calcularPrecioVenta_PR(opcion, recorrerAll, Externo) {
            //*********** OPcion:
            // 0: Util (%)
            // 1: Utilidad
            // 2: PV Final $
            // 3: PV Final S/.
            // 4: COMBO MONEDA destino
            // 5: ONLOAD

            var grilla = document.getElementById('<%=DGV_PRODUCTO.ClientID%>');
            if (grilla == null) {
                return false;
            }
            //************ declaramos las variables
            var tipocambio = parseFloat(document.getElementById('<%=hddTipoCambio.ClientID%>').value);
            var tipocambioOtraMoneda = parseFloat(document.getElementById('<%=hddTipoCambio.ClientID%>').value);
            if (isNaN(tipocambioOtraMoneda)) { tipocambioOtraMoneda = 0; }
            var porcentUtil = 0;
            var util = 0;
            var pc = 0;

            var IdMonedaPC = 0;
            var IdMonedaPV = 0;
            var pvSoles = 0;
            var pvDolares = 0;
            var pvOtraMoneda = 0;
            var equivalencia = 0;

            IdMonedaPC = parseFloat(document.getElementById('<%=hddIdMoneda_PC.ClientID%>').value);
            if (!esDecimal(IdMonedaPC)) { IdMonedaPC = 0; }

            if (IdMonedaPC == 0) {
                //alert('El Producto no posee una Moneda de Precio de Compra, esto producirá error en la operación.');
                //return false;
            }

            if (tipocambio == 0) {
                alert('El Tipo de Cambio debe ser mayor a cero');
                return false;
            }


            var opcionOriginal = opcion;

            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera

                var rowElem = grilla.rows[i];
                var controlId = '';  // Utilizado para el ONLOAD

                pc = parseFloat(document.getElementById('<%=hddPrecioCompra_prod.ClientID%>').value);
                if (!esDecimal(pc)) { pc = 0; }

                if (event != null) {
                    if (event.srcElement != null) {
                        controlId = event.srcElement.id;
                    }
                }

                if (Externo == 1) { var controlId = ''; }
                if (isNaN(Externo)) { document.getElementById('<%=ckProducto.ClientID%>').status = false; }
                
                if (rowElem.cells[6].children[0].id == controlId || rowElem.cells[7].children[0].cells[1].children[0].id == controlId || rowElem.cells[8].children[0].id == controlId || rowElem.cells[9].children[0].id == controlId || rowElem.cells[10].children[0].id == controlId || controlId == '') {

                    equivalencia = parseFloat(rowElem.cells[2].innerHTML);
                    if (!esDecimal(equivalencia)) { equivalencia = 0; }

                    pc = pc * equivalencia;


                    porcentUtil = parseFloat(rowElem.cells[6].children[0].value);
                    if (!esDecimal(porcentUtil)) { porcentUtil = 0; }
                    util = parseFloat(rowElem.cells[7].children[0].cells[1].children[0].value);
                    if (!esDecimal(util)) { util = 0; }
                    IdMonedaPV = parseFloat(rowElem.cells[10].children[0].value);
                    if (!esDecimal(IdMonedaPV)) { IdMonedaPV = 0; }
                    pvDolares = parseFloat(rowElem.cells[8].children[0].value);
                    if (!esDecimal(pvDolares)) { pvDolares = 0; }
                    pvSoles = parseFloat(rowElem.cells[9].children[0].value);
                    if (!esDecimal(pvSoles)) { pvSoles = 0; }

                    if (controlId == '' && opcionOriginal == '5') {
                        if (IdMonedaPV == 1) {  //********* soles
                            opcion = '3';
                        }
                        if (IdMonedaPV == 2) {  //********* dolares
                            opcion = '2';
                        }
                    }

                    switch (opcion) {
                        case '0':  //*************UTIL (%)

                            if (IdMonedaPC == 1) {  //*********** SOLES

                                pvSoles = pc * (1 + porcentUtil / 100);
                                pvDolares = pvSoles / tipocambio;
                                util = pvSoles - pc;

                            }

                            if (IdMonedaPC == 2) {  //*********** DOLARES
                                pvDolares = pc * (1 + porcentUtil / 100);
                                pvSoles = pvDolares * tipocambio;
                                util = pvDolares - pc;
                            }

                            if (IdMonedaPC > 2) {  //*********** OTRA MOMEDA
                                pvOtraMoneda = pc * (1 + porcentUtil / 100);
                                //*********** Validamos el PC
                                pvSoles = pvOtraMoneda * tipocambioOtraMoneda;
                                pvDolares = pvSoles / tipocambio;
                                util = pvOtraMoneda - pc;
                            }

                            //************ IMPRIMIMOS


                            rowElem.cells[7].children[0].cells[1].children[0].value = redondear(util, 2);
                            rowElem.cells[8].children[0].value = redondear(pvDolares, 2);
                            rowElem.cells[9].children[0].value = redondear(pvSoles, 2);
                            break;
                        case '1':  //************ UTILIDAD
                            if (IdMonedaPC == 1) {  //*********** SOLES
                                pvSoles = pc + util;
                                pvDolares = pvSoles / tipocambio;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvSoles - pc) / pc;
                                }

                            }
                            if (IdMonedaPC == 2) {  //*********** DOLARES
                                pvDolares = pc + util;
                                pvSoles = pvDolares * tipocambio;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvDolares - pc) / pc;
                                }

                            }
                            if (IdMonedaPC > 2) {  //*********** OTRA MOMEDA
                                pvOtraMoneda = pc + util;
                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvOtraMoneda - pc) / pc;
                                }

                                pvSoles = pvOtraMoneda * tipocambioOtraMoneda;
                                pvDolares = pvSoles / tipocambio;
                                util = pvOtraMoneda - pc;
                            }

                            //************ IMPRIMIMOS

                            rowElem.cells[6].children[0].value = redondear(porcentUtil * 100, 2);
                            rowElem.cells[8].children[0].value = redondear(pvDolares, 2);
                            rowElem.cells[9].children[0].value = redondear(pvSoles, 2);
                            break;
                        case '2': //************* PV Final Dolares
                            pvSoles = pvDolares * tipocambio;

                            if (IdMonedaPC == 1) {  //*********** SOLES
                                util = pvSoles - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvSoles - pc) / pc;
                                }

                            }
                            if (IdMonedaPC == 2) {  //*********** DOLARES
                                util = pvDolares - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvDolares - pc) / pc;
                                }
                            }

                            if (IdMonedaPC > 2) {  //*********** OTRA MOMEDA
                                pvOtraMoneda = pvSoles / tipocambioOtraMoneda;
                                util = pvOtraMoneda - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvOtraMoneda - pc) / pc;
                                }
                            }


                            //************ IMPRIMIMOS

                            rowElem.cells[6].children[0].value = redondear(porcentUtil * 100, 2);
                            rowElem.cells[7].children[0].cells[1].children[0].value = redondear(util, 2);
                            rowElem.cells[9].children[0].value = redondear(pvSoles, 2);
                            break;
                        case '3':  //************* Pv Final SOLES
                            pvDolares = pvSoles / tipocambio;
                            if (IdMonedaPC == 1) {  //*********** SOLES
                                util = pvSoles - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvSoles - pc) / pc;
                                }

                            }
                            if (IdMonedaPC == 2) {  //*********** DOLARES
                                util = pvDolares - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvDolares - pc) / pc;
                                }


                            }

                            if (IdMonedaPC > 2) {  //*********** OTRA MOMEDA
                                pvOtraMoneda = pvSoles / tipocambioOtraMoneda;
                                util = pvOtraMoneda - pc;

                                //*********** Validamos el PC
                                if (pc == 0) {
                                    porcentUtil = 100;
                                } else {
                                    porcentUtil = (pvOtraMoneda - pc) / pc;
                                }
                            }


                            //************ IMPRIMIMOS

                            rowElem.cells[6].children[0].value = redondear(porcentUtil * 100, 2);
                            rowElem.cells[7].children[0].cells[1].children[0].value = redondear(util, 2);
                            rowElem.cells[8].children[0].value = redondear(pvDolares, 2);

                            break;
                        case '4':
                            //************* NO se realiza ningun cambio
                            break;
                    }
                    if (recorrerAll == '0') {
                        //********** si no es ONLOAD no debo recorrer toda la grilla
                        return false;
                    }
                }
            }


            return false;
        }


        //// ******************************** FIN DEL CALCULO



        function valEquivalenciaxProducto() {
            if (document.getElementById('<%=ckProducto.ClientID%>').status == true) {


                var valor = 0;
                var grilla = document.getElementById('<%=DGV_PRODUCTO.ClientID%>');

                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var TipoPrecio = rowElem.cells[12].innerText;
                    
                    if (rowElem.cells[1].children[0].cells[2].children[0].value == 'True') {
                        valor = parseFloat(rowElem.cells[9].children[0].value);
                        if (isNaN(valor)) { valor = 0; }

                        for (var j = 1; j < grilla.rows.length; j++) {
                            var xrowElem = grilla.rows[j];

                            if (xrowElem.cells[1].children[0].cells[2].children[0].value == 'False' && valor > 0 && TipoPrecio == xrowElem.cells[12].innerText) {
                                var Equivalencia = parseFloat(xrowElem.cells[2].innerText);
                                if (isNaN(Equivalencia)) { Equivalencia = 0; }
                                xrowElem.cells[9].children[0].value = redondear(valor * Equivalencia, 2);
                                calcularPrecioVenta_PR('3', '1', 1);
                            }

                        } // end for
                        
                    }
                } // end for

               

            } // end if



            return true;
        }        
        
    </script>

</asp:Content>
