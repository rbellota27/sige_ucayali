
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmConfigCargoTarjeta.aspx.vb" Inherits="APPWEB.frmConfigCargoTarjeta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">

        function valSave() {
            codigo = document.getElementById('<%=txtCodigo.ClientID %>');
            Valor = document.getElementById('<%=txtValor.ClientID %>');

            grilla = document.getElementById('<%=gvBusqueda.ClientID %>');

            

            //if (nombre.value == '') {
              //  alert('No ha ingresado un Nombre');
                //nombre.focus();
                //return false;
           // }

            if (Valor.value == '') {
                alert('No ha ingresado un Valor');
                Valor.focus();
                return false;
            }
            
            

            return confirm('Desea continuar. ?');
        }

        //++++++++++++++++++++++++++++++++++++++++++++
        function HazAlgo(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                valor = document.getElementById('<%=txtValor.ClientID %>');
                //valor.select();
                valor.focus();
            }

        }


    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%;">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" 
                            CausesValidation="False" onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" 
                            onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" TabIndex="1"  />
                        <asp:ImageButton ID="btnGuardar" runat="server" 
                            ImageUrl="~/Imagenes/Guardar_B.JPG" OnClientClick="return(valSave());" 
                            onmouseout="this.src='/Imagenes/Guardar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Guardar_A.JPG';" TabIndex="6" />
                        <asp:ImageButton ID="btnCancelar" runat="server" 
                            ImageUrl="~/Imagenes/Cancelar_B.JPG" 
                            OnClientClick="return(confirm('Desea ir al modo Inicio?'));" 
                            onmouseout="this.src='/Imagenes/Cancelar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Cancelar_A.JPG';" Style="margin-bottom: 0px" 
                            TabIndex="7" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlPrincipal" runat="server">
                            <table style="width: 100%;">
                                <tr>
                                    <td colspan="2" class="TituloCelda">
                                        Configuracion Cargo Tarjeta
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LabelTdLeft">
                                        Codigo:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCodigo" runat="server" ReadOnly = "true" Width="100px" 
                                            TabIndex="2" CssClass="TextBox_ReadOnly" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="LabelTdLeft">
                                        Valor:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtValor" runat="server"  MaxLength="4" onkeypress="return(validarNumeroPunto(event));"
                                            Width="187px" TabIndex="4" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                
                                <tr>                                                       
                                    <td class="LabelTdLeft">
                                        Estado:
                                          </td>
                                          <td>
                                        <asp:RadioButtonList ID="rdbEstado" runat="server" CssClass="Label" 
                                            RepeatDirection="Horizontal" TabIndex="5">
                                            <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                                            <asp:ListItem Value="0">Inactivo</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel Width="100%" ID="pnlBusqueda" runat="server" Height="108px">
                            <table width="100%" style="height: 34px">
                                <tr>
                                                <asp:Label ID="Label24" runat="server" CssClass="Label" 
                                            Text="Nombre Carog Tarjeta:" Visible="False"></asp:Label>
                                        <asp:TextBox ID="txtTextoABuscar" runat="server" MaxLength="100" Width="300px" 
                                            TabIndex="9" Visible="False"></asp:TextBox>
                                        <asp:ImageButton ID="btnBuscarCargoTarjeta" runat="server" 
                                            ImageUrl="~/Imagenes/Buscar_B.JPG" 
                                            onmouseout="this.src='/Imagenes/Buscar_B.JPG';" 
                                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" Visible="False" 
                                            TabIndex="10" />
                   <tr>
                   
                       <asp:RadioButtonList ID="rbver" runat="server" AutoPostBack="true" 
                           CssClass="Label" Height="25px" RepeatDirection="Horizontal" TabIndex="11">
                           <asp:ListItem Value="2">Todos</asp:ListItem>
                           <asp:ListItem Selected="True" Value="1">Activo</asp:ListItem>
                           <asp:ListItem Value="0">Inactivo</asp:ListItem>
                       </asp:RadioButtonList>
                       <td>
                       </td>
                       <tr>
                           <td>
                               &nbsp;</td>
                       </tr>
                       <asp:GridView ID="gvBusqueda" runat="server" AllowPaging="True" 
                           AutoGenerateColumns="False" ForeColor="#333333" Height="16px" 
                           Style="text-align: left" Width="100%">
                           <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                           <EditRowStyle CssClass="GrillaEditRow" />
                           <Columns>
                               <asp:CommandField EditText="Seleccionar" SelectText="Seleccionar" 
                                   ShowSelectButton="True" />
                               <asp:BoundField DataField="IdCargoTarjeta" HeaderText="Id" />
                               <asp:BoundField DataField="ct_Valor" HeaderText="Valor" />
                               <asp:BoundField DataField="DescEstado" HeaderText="Estado" />
                               <asp:BoundField DataField="ct_FechaReg" HeaderText="Fecha Registro" />
                           </Columns>
                           <FooterStyle CssClass="GrillaFooter" />
                           <HeaderStyle CssClass="GrillaHeader" />
                           <PagerStyle CssClass="GrillaPager" />
                           <RowStyle CssClass="GrillaRow" />
                           <SelectedRowStyle CssClass="GrillaSelectedRow" />
                       </asp:GridView>
                   
                   </tr>
                   
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
