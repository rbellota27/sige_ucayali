﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmRegistrosGastos.aspx.vb" Inherits="APPWEB.frmRegistrosGastos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td class="TituloCelda">
                Registro de Gasto
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button Width="85px" ID="btnuevo" runat="server" Text="Nuevo" Visible="false" />
                <asp:Button Width="85px" ID="btguardar" runat="server" Text="Guardar" OnClientClick="return(validarGuardar());" />
                <asp:Button Width="85px" ID="btcancelar" runat="server" Text="Cancelar" OnClientClick="return(confirm('Esta seguro de cancelar la operacion ?'));" />
                <asp:Button Width="85px" ID="btbuscar" runat="server" Text="Buscar" />
                <asp:Button Width="85px" ID="bteditar" runat="server" Text="Editar" />
                <asp:Button Width="85px" ID="btanular" runat="server" Text="Anular" OnClientClick="return(confirm('Esta seguro de anular el documento ?'));" />
                <asp:Button Width="85px" ID="btimprimir" runat="server" Text="Imprimir" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlPrincipal" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <asp:Panel ID="PanelDocumento" runat="server">
                                    <table>
                                        <tr>
                                            <td class="LabelTab">
                                                Empresa:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlempresa" runat="server" AutoPostBack="True" Enabled="false"
                                                    Font-Bold="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTab">
                                                Tienda:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dltienda" runat="server" AutoPostBack="True" Enabled="false"
                                                    Font-Bold="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTab">
                                                Serie:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlserie" runat="server" AutoPostBack="true" Font-Bold="False"
                                                    Width="100%">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbcodigo" runat="server" CssClass="TextBox_ReadOnly" onKeyDown="return(validarCod(event));"
                                                    onKeyPress=" return(validarCodBuscar(event));" Width="100px" Font-Bold="true"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="imgBuscarDocumento" runat="server" ImageUrl="~/Caja/iconos/ok.gif" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTab">
                                                Operación:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dloperacion" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTab">
                                                Fecha Emisión:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbfechaemision" runat="server" CssClass="TextBox_Fecha" Width="100px"></asp:TextBox>
                                                <ajax:CalendarExtender ID="cefechaemision" runat="server" Format="d" TargetControlID="tbfechaemision">
                                                </ajax:CalendarExtender>
                                            </td>
                                            <td class="LabelTab">
                                                Estado:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlestado" runat="server" Enabled="false" Font-Bold="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTab">
                                                Medio Pago
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlMedioPagoRegistro" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTab">
                                                Moneda:
                                            </td>
                                            <td>
                                                <ajax:CalendarExtender ID="CalendarExtender1" runat="server" Format="d" TargetControlID="tbfechaemision">
                                                </ajax:CalendarExtender>
                                                <asp:DropDownList ID="dlmonedaregistro" runat="server" AutoPostBack="True" Font-Bold="False">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTab">
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCeldaLeft">
                                Datos del Documento (Requerimiento Gastos)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="panelRequerimiento" Enabled="false" runat="server">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td class="LabelTab">
                                                Serie:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbserieref" Width="100px" runat="server" Font-Bold="true"></asp:TextBox>
                                            </td>
                                            <td class="LabelTab">
                                                N&uacute;mero:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbcodigoref" Width="100px" runat="server" Font-Bold="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTab">
                                                Aprobado Por:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbaprobadopor" Width="100%" runat="server" Font-Bold="true"></asp:TextBox>
                                            </td>
                                            <td class="LabelTab">
                                                F. Aprobaci&oacute;n:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbfechaaprobacion" Width="100px" runat="server" Font-Bold="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTab">
                                                Beneficiario:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbbeneficiario" Width="100%" runat="server" Font-Bold="true"></asp:TextBox>
                                            </td>
                                            <td class="LabelTab">
                                                D.N.I.:
                                            </td>
                                            <td class="LabelLeft">
                                                <asp:TextBox Width="80px" ID="tbdni" Font-Bold="true" runat="server"></asp:TextBox>
                                                &nbsp;R.U.C.:<asp:TextBox Width="90px" ID="tbruc" Font-Bold="true" runat="server"></asp:TextBox>
                                                <asp:ImageButton ID="imgBuscarBeneficiario" Visible="false" OnClientClick="return(mostrarCapaPersona('1'));"
                                                    runat="server" ImageUrl="~/Caja/iconos/ok.gif" ToolTip="Buscar Beneficiario" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTab">
                                                Medio Pago:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlmediopago" runat="server" Font-Bold="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTab">
                                                Moneda:
                                            </td>
                                            <td class="LabelLeft">
                                                <asp:DropDownList ID="dlmoneda" runat="server" Font-Bold="true">
                                                </asp:DropDownList>
                                                &nbsp;Total:
                                                <asp:TextBox ID="tbmontoSolicitado" Width="100px" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LabelTab">
                                                &Aacute;rea:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dlarea" runat="server" Enabled="false" Font-Bold="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="LabelTab">
                                                &nbsp;
                                            </td>
                                            <td class="LabelLeft">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCeldaLeft" valign="bottom">
                                Detalle del Requerimiento de Gasto (Concepto)
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btagregar" runat="server" Text="Agregar" /><br />
                                <asp:GridView ID="gvdetalle" runat="server" AutoGenerateColumns="False" GridLines="None"
                                    Width="99%" RowStyle-VerticalAlign="Top" CellPadding="0" CellSpacing="0">
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="true" />
                                        <asp:TemplateField HeaderText="Concepto" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="gdlconcepto" DataTextField="Nombre" DataValueField="Id"
                                                    runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"objConcepto") %>'
                                                    AutoPostBack="true" OnSelectedIndexChanged="gdlconcepto_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Motivo" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="gdlmotivo"  DataValueField="Id" DataTextField="Nombre_MG"
                                                    runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"objMotivo") %>'>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Descripcion" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="gtbdescripcion" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"descripcion") %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tipo Documento" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="gdltipodocumento" DataValueField="Id" DataTextField="DescripcionCorto"
                                                    runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoDocumento") %>'>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="N° Documento" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="gtbnrodocumento" Width="80px" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"nroDocumento") %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Razón Social / Nombres" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgBuscarPersona" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                                    OnClientClick="return(getIndex(this,'0'));" ToolTip="Buscar Persona" />
                                                <asp:HiddenField ID="ghdd_idpersona" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdProveedor") %>' />
                                                <asp:HiddenField ID="ghdd_nompersona" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"NomPersona") %>' />
                                                <asp:Label ID="glbnompersona" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NomPersona") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Moneda" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="gdlmoneda" Width="50px" Enabled="false" runat="server" DataTextField="Simbolo"
                                                    DataValueField="Id" DataSource='<%# DataBinder.Eval(Container.DataItem,"objMoneda") %>'>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Monto" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="gtbmonto" Width="75px" onfocus="return(aceptarFoco(this));" runat="server"
                                                    onKeypress="return(validarNumeroPunto(event));" onKeyUp="javascript:CalcularMonto();"
                                                    Text='<%# DataBinder.Eval(Container.DataItem,"Monto","{0:F2}") %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="GrillaRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaCabecera" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="LabelTab">
                                Total:<asp:Label ID="lblSimboloMoneda1" runat="server" Text=""></asp:Label>
                                <asp:TextBox Width="90px" ID="tbtotal" Font-Bold="true" runat="server" onKeyPress="return(false);">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCeldaLeft">
                                Datos del Centro de Costo
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="LabelTdLeft">
                                            &nbsp;Unidad Negocio:<br />
                                            <asp:DropDownList ID="dlunidadnegocio" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="LabelTdLeft">
                                            &nbsp;Dpto. Funcional:<br />
                                            <asp:DropDownList ID="dldptofuncional" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="LabelTdLeft">
                                            &nbsp;Sub-Área 1:<br />
                                            <asp:DropDownList ID="dlsubarea1" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="LabelTdLeft">
                                            &nbsp;Sub-Área 2:<br />
                                            <asp:DropDownList ID="dlsubarea2" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="LabelTdLeft">
                                            &nbsp;Sub-Área 3:<br />
                                            <asp:DropDownList ID="dlsubarea3" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btCentroCosto" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                                ToolTip="[ Agregar Centro de Costo ]" OnClientClick="return(validarCentroCosto());" /><br />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="gvcentrocosto" Width="100%" runat="server" AutoGenerateColumns="False"
                                    GridLines="None">
                                    <RowStyle CssClass="GrillaRow" />
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                        <asp:BoundField HeaderText="Código" DataField="codigo" />
                                        <asp:BoundField HeaderText="Unidad Negocio" DataField="strCod_UniNeg" />
                                        <asp:BoundField HeaderText="Dpto Funcional" DataField="strCod_DepFunc" />
                                        <asp:BoundField HeaderText="Sub-Área 1" DataField="strCod_SubCodigo2" />
                                        <asp:BoundField HeaderText="Sub-Área 2" DataField="strCod_SubCodigo3" />
                                        <asp:BoundField HeaderText="Sub-Área 3" DataField="strCod_SubCodigo4" />
                                        <asp:TemplateField HeaderText="Monto">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tbmontocentrocosto" onKeyUp="javascript:CalcularMontoCentroCosto();"
                                                    onKeypress="return(validarNumeroPunto(event));" onfocus="return(aceptarFoco(this));"
                                                    runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"monto","{0:F2}") %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <HeaderStyle CssClass="GrillaCabecera" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="LabelTab">
                                Total:<asp:Label ID="lblsimbolomoneda2" runat="server" Text=""></asp:Label>
                                <asp:TextBox Width="90px" ID="tbtotalcentrocosto" Font-Bold="true" runat="server"
                                    onKeyPress="return(false);">0</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="TituloCeldaLeft">
                                Observaciones
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="tbobservaciones" TextMode="MultiLine" Width="100%" MaxLength="100"
                                    runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                <asp:HiddenField ID="hdd_idusuario" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_operativo" runat="server" />
                <asp:HiddenField ID="hdd_idpersona" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_iddocumento" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_idObservacion" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_index" runat="server" Value="0" />
                <asp:HiddenField ID="hdd_idrequerimientogasto" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <div id="capabeneficiario" style="border: 3px solid blue; padding: 8px; width: 750px;
        height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table width="100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td colspan="5">
                                <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                    AutoPostBack="false">
                                    <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                    <asp:ListItem Value="J">Juridica</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                Razon Social / Nombres:
                            </td>
                            <td colspan="4">
                                <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(event));" runat="server"
                                    Width="450px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                                D.N.I.:
                            </td>
                            <td>
                                <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                    MaxLength="8" runat="server"></asp:TextBox>
                            </td>
                            <td class="Texto">
                                Ruc:
                            </td>
                            <td>
                                <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                    MaxLength="11"></asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                    ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='/Imagenes/Buscar_b.JPG';"
                                    onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="20" Width="100%">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkbselect" runat="server" OnClientClick="return(selectPersona(this));">Seleccionar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                            <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                <ItemStyle HorizontalAlign="Left" Width="500px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                        ToolTip="Página Anterior" OnClientClick="return(valNavegacion('0'));" />
                    <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                        ToolTip="Página Posterior" OnClientClick="return(valNavegacion('1'));" />
                    <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                        runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                            Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion('2'));" />
                    <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                        onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                    <asp:HiddenField ID="hdd_select" Value="0" runat="server" />
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">

        function validarGuardar() {
            var idarea = document.getElementById('<%=dlarea.ClientID %>');
            if (idarea.value == '0,0,0') {
                alert('Debe seleccionar un area');
                return false;
            }
            var idpersona = document.getElementById('<%=hdd_idpersona.ClientID %>');
            if (idpersona.value == '0') {
                alert('Debe ingresar al beneficiario');
                return false;
            }
            var grilla = document.getElementById('<%=gvdetalle.ClientID %>');
            if (grilla == null) {
                alert('Debe ingresar los detalles del documento');
                return false;
            } else {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var monto = redondear(parseFloat(rowElem.cells[8].children[0].value), 2);
                    if (isNaN(monto)) {
                        alert('Debe ingresar un numero');
                        rowElem.cells[8].children[0].focus();
                        return false;
                    }
                    if (monto <= 0) {
                        alert('Debe ingresar un monto mayor a cero');
                        rowElem.cells[8].children[0].focus();
                        return false;
                    }
                }
            }

            var grillacentrocosto = document.getElementById('<%=gvcentrocosto.ClientID %>');
            if (grillacentrocosto != null) {
                var montocentro = document.getElementById('<%=tbtotalcentrocosto.ClientID %>');
                var montoconcepto = document.getElementById('<%=tbtotal.ClientID %>');
                var centro = redondear(parseFloat(montocentro.value), 2);

                for (var i = 1; i < grillacentrocosto.rows.length; i++) {
                    var rowElem = grillacentrocosto.rows[i];
                    var monto = redondear(parseFloat(rowElem.cells[7].children[0].value), 2);
                    if (isNaN(monto)) {
                        alert('Debe ingresar un numero');
                        rowElem.cells[7].children[0].focus();
                        return false;
                    }
                    if (monto <= 0) {
                        alert('Debe ingresar un monto mayor a cero');
                        rowElem.cells[7].children[0].focus();
                        return false;
                    }
                }
                
                if (isNaN(centro)) {
                    centro = 0;
                }
                var concepto = redondear(parseFloat(montoconcepto.value), 2);
                if (isNaN(concepto)) {
                    concepto = 0;
                }
                if (centro != concepto) {
                    alert('Los montos del centro de costo debe de ser iguales');
                    return false;
                }
            }
            return confirm('Desea continuar con la operacion?');
        }
        ////////////////////
        function verCapaPersona() {
            onCapa('capabeneficiario');
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            tb.focus();
            return false;
        }
        /////////////////////////
        function validarCodBuscar(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            var operativo = document.getElementById('<%=hdd_operativo.ClientID %>');
            var boton = document.getElementById('<%=imgbuscardocumento.ClientID %>');
            var codigo = document.getElementById('<%=tbcodigo.ClientID %>');
            if (operativo.value == '3') {
                if ((caracter == 13) && (codigo.value.length > 0)) {
                    boton.focus();
                    return true;
                }
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }

                return true;
            }
            return false;
        }
        function validarCod(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            var operativo = document.getElementById('<%=hdd_operativo.ClientID %>');
            if (operativo.value != '3') {
                if ((caracter == 46) || (caracter == 8)) {  // 3 es buscar
                    return false;
                }
            }
            return true;
        }

        ///////////////////////////////
        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        ////////////////
        function validarCajaBusquedaNumero(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        //////////////////
        function validarCajaBusqueda(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        ///++++++
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   ////////////// anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //////////// ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        ////////////////////////////////////////////////////
        function selectPersona(obj) {
            var grilla = document.getElementById('<%=gvBuscar.ClientID %>');
            var caso = document.getElementById('<%=hdd_select.ClientID %>');

            switch (caso.value) {
                case '1': //BOTON BENEFICIARIO
                    var idpersona = document.getElementById('<%=hdd_idpersona.ClientID %>');
                    var beneficiario = document.getElementById('<%=tbbeneficiario.ClientID %>');
                    var dni = document.getElementById('<%=tbdni.ClientID %>');
                    var ruc = document.getElementById('<%=tbruc.CLientID %>');
                    if (grilla != null) {
                        for (var i = 1; i < grilla.rows.length; i++) {
                            var rowElem = grilla.rows[i];
                            if (rowElem.cells[0].children[0].id == obj.id) {
                                idpersona.value = rowElem.cells[1].innerText;
                                beneficiario.value = rowElem.cells[2].innerText;
                                ruc.value = rowElem.cells[3].innerText;
                                dni.value = rowElem.cells[4].innerText;
                                offCapa('capabeneficiario');
                                return false;
                            }
                        }
                    }
                    break;
                case '0': //CUADRICULA
                    var index = document.getElementById('<%=hdd_index.ClientID %>');
                    var grillaDetalle = document.getElementById('<%=gvDetalle.ClientID %>');
                    for (var i = 1; i < grilla.rows.length; i++) {
                        var rowElem = grilla.rows[i];
                        if (rowElem.cells[0].children[0].id == obj.id) {
                            for (var p = 1; p < grillaDetalle.rows.length; p++) {
                                var rowDet = grillaDetalle.rows[p];
                                if (parseInt(index.value) == p) {
                                    rowDet.cells[6].children[1].value = rowElem.cells[1].innerText; //idpersona                                    
                                    rowDet.cells[6].children[2].value = rowElem.cells[2].innerText; //nompersona
                                    setText();
                                    offCapa('capabeneficiario');
                                    return false;
                                }
                            }
                            return false;
                        }
                    }
                    break;
            }

        }
        //////////////////////
        function CalcularMonto() {
            var grilla = document.getElementById('<%=gvdetalle.ClientID %>');
            if (grilla != null) {
                var montoTotal = 0;
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var monto = redondear(parseFloat(rowElem.cells[8].children[0].value), 2);
                    if (isNaN(monto)) {
                        monto = 0;
                    }
                    montoTotal = montoTotal + monto;
                }
                document.getElementById('<%=tbtotal.ClientID %>').value = redondear(montoTotal, 2);
            } else {
                document.getElementById('<%=tbtotal.ClientID %>').value = 0;
            }
        }
        ////////////////////////////
        function mostrarCapaPersona(caso) {
            onCapa('capabeneficiario');
            document.getElementById('<%=hdd_select.ClientID %>').value = caso;
            return false;
        }
        ///////////////////////////////
        function getIndex(obj, caso) {
            var grilla = document.getElementById('<%=gvDetalle.ClientID %>');
            var index = document.getElementById('<%=hdd_index.ClientID %>');
            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (rowElem.cells[6].children[0].id == obj.id) {
                    index.value = i;
                    onCapa('capabeneficiario');
                    document.getElementById('<%=hdd_select.ClientID %>').value = caso;
                    return false;
                }
            }
        }
        /////////////////////////
        function setText() {
            var grilla = document.getElementById('<%=gvDetalle.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    rowElem.cells[6].children[3].innerText = rowElem.cells[6].children[2].value;
                }
            }
        }
        ///////////////////////
        function validarCentroCosto() {
            var grilla = document.getElementById('<%=gvcentrocosto.ClientID %>');
            var strCod_UniNeg = document.getElementById('<%=dlunidadnegocio.ClientID %>');
            var strCod_DepFunc = document.getElementById('<%=dldptofuncional.ClientID %>');
            var strCod_SubCodigo2 = document.getElementById('<%=dlsubarea1.ClientID %>');
            var strCod_SubCodigo3 = document.getElementById('<%=dlsubarea2.ClientID %>');
            var strCod_SubCodigo4 = document.getElementById('<%=dlsubarea3.ClientID %>');
            var codigo = strCod_UniNeg.value + strCod_DepFunc.value + strCod_SubCodigo2.value + strCod_SubCodigo3.value + strCod_SubCodigo4.value

            if (codigo == '0000000000') {
                alert('Seleccione un centro de costo');
                return false;
            }
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].innerText == codigo) {
                        alert('El centro de costo ya ha sido agregado');
                        return false;
                    }
                }
            }
            return true;
        }
        /////
        function CalcularMontoCentroCosto() {
            var grilla = document.getElementById('<%=gvcentrocosto.ClientID %>');
            if (grilla != null) {
                var montoTotal = 0;
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var monto = redondear(parseFloat(rowElem.cells[7].children[0].value), 2);
                    if (isNaN(monto)) {
                        monto = 0;
                    }
                    montoTotal = montoTotal + monto;
                }
                document.getElementById('<%=tbtotalcentrocosto.ClientID %>').value = redondear(montoTotal, 2);
            } else {
                document.getElementById('<%=tbtotalcentrocosto.ClientID %>').value = 0;
            }
        }        
        
    </script>

</asp:Content>
