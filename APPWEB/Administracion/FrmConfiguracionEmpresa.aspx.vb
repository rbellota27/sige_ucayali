﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmConfiguracionEmpresa
    Inherits System.Web.UI.Page
    Dim cbo As New Combo
    Dim sc As New ScriptManagerClass
    Dim objutil As New Util
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            cbo.LlenarCboPropietario(Me.cboEmpresa, True)
            cargarDatosCboTienda(Me.cboTiendaT)
            OcultarControles(False)
            OcultarControlesTienda(False)
            'HabilitarCaja(True, True, True, True, False, "")
            Session("Indicador") = "0"
            HabilitarBotonGrabar(False, False)
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", "javascript:setText();", True)
    End Sub

    Sub HabilitarBotonGrabar(ByVal ActivarGrabaEmpresa As Boolean, ByVal ActivarGrabaTienda As Boolean)
        'Me.cmd_Guardar.Visible = ActivarGrabaEmpresa
        Me.cmd_GuardarA.Visible = ActivarGrabaEmpresa
        'Me.cmd_GuardarT.Visible = ActivarGrabaTienda
        Me.cmd_GuardarTB.Visible = ActivarGrabaTienda
    End Sub
    Sub Grabar()
        '' EMPRESA TIENDA
        Dim lEmpresaTienda As List(Of Entidades.EmpresaTienda) = obtenerListaTiendaFrmGrilla()
        '' EMPRESA ALMACEN
        Dim lEmpresaAlmacen As List(Of Entidades.EmpresaAlmacen) = obtenerListaAlmacenFrmGrilla()

        '' EMPRESA AREA
        Dim lAreaEmpresa As List(Of Entidades.AreaEmpresa) = obtenerListaAreaFrmGrilla()

        '' EMPRESA Rol
        Dim lRolEmpresa As List(Of Entidades.Rol_Empresa) = obtenerListaRolFrmGrilla()



        Dim ngcEmpresa As New Negocio.Empresa
        If ngcEmpresa.InsertaConfiguracionEmpresa(CInt(Me.cboEmpresa.SelectedValue), lEmpresaTienda, lEmpresaAlmacen, lAreaEmpresa, lRolEmpresa) = True Then
            sc.mostrarMsjAlerta(Me, "Se grabo correctamente los datos")
        Else
            sc.mostrarMsjAlerta(Me, "La Información ya fue grabado no se puede volver a actualizar la información porque existen datos relacionado con otras tablas")
        End If
        cargarDatosEmpresaxTienda(Me.dgvTienda)
        cargarDatosCboTienda(Me.cboTienda)
        cargarDatosCboAlmacen(Me.cboAlmacen)
        cargarDatosEmpresaxAlmacen(Me.dgvAlmacen)
        cargarDatosCboArea(Me.cboArea)
        cbo.LlenarCboRol(Me.cboRol, True)
        cargarDatosEmpresaxArea(Me.dgvArea)
        cargarDatosEmpresaxRol(Me.dgvRol)

        OcultarControles(True)
    End Sub
    '' EMPRESA X TIENDA
    Private Sub cargarDatosEmpresaxTienda(ByVal grilla As GridView)
        Dim obj As New Negocio.EmpresaTienda
        Dim objEmpresaTienda As New Entidades.EmpresaTienda
        Dim listaEmpresaTienda As New List(Of Entidades.EmpresaTienda)

        For Each objEmpresaTienda In obj.SelectGrillaxEmpresaTienda(CInt(Me.cboEmpresa.SelectedValue))
            objEmpresaTienda.CuentaContable = HttpUtility.HtmlDecode(objEmpresaTienda.CuentaContable)
            objEmpresaTienda.CentroCosto = HttpUtility.HtmlDecode(objEmpresaTienda.CentroCosto)
            listaEmpresaTienda.Add(objEmpresaTienda)
        Next
        grilla.DataSource = listaEmpresaTienda
        grilla.DataBind()
    End Sub
    Private Sub cargarDatosCboTienda(ByVal cboTienda As DropDownList)
        '************** Cargamos los datos de las tiendas
        cbo.LLenarCboTienda(cboTienda, True)

    End Sub
    Private Function obtenerListaTiendaFrmGrilla() As List(Of Entidades.EmpresaTienda)
        Dim lista As New List(Of Entidades.EmpresaTienda)
        For i As Integer = 0 To dgvTienda.Rows.Count - 1
            With dgvTienda.Rows(i)
                Dim obj As New Entidades.EmpresaTienda(CInt(.Cells(0).Text), .Cells(1).Text, CInt(Me.cboEmpresa.SelectedValue), .Cells(2).Text, .Cells(3).Text, CType(Me.dgvTienda.Rows(i).Cells(4).FindControl("chkEstadoTienda"), CheckBox).Checked)
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Private Sub addTiendaGrilla()
        Dim lista As List(Of Entidades.EmpresaTienda) = obtenerListaTiendaFrmGrilla()
        Dim obj As New Entidades.EmpresaTienda(CInt(Me.cboTienda.SelectedValue), Me.cboTienda.SelectedItem.Text, CInt(Me.cboEmpresa.SelectedValue), objutil.decodificarTexto(CStr(Me.txtCuentaContable.Text)), HttpUtility.HtmlDecode(CStr(Me.txtCentroCosto.Text)), True)
        lista.Add(obj)
        dgvTienda.DataSource = lista
        dgvTienda.DataBind()
    End Sub
    Private Sub quitarRegistroTienda()
        Dim objUtil As New Negocio.Util
        Dim lista As List(Of Entidades.EmpresaTienda) = obtenerListaTiendaFrmGrilla()
        lista.RemoveAt(dgvTienda.SelectedIndex)
        dgvTienda.DataSource = lista
        dgvTienda.DataBind()
    End Sub
    Protected Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        cargarDatosEmpresaxTienda(Me.dgvTienda)
        cargarDatosCboTienda(Me.cboTienda)
        cargarDatosCboAlmacen(Me.cboAlmacen)
        cargarDatosEmpresaxAlmacen(Me.dgvAlmacen)
        cargarDatosCboArea(Me.cboArea)
        cbo.LlenarCboRol(cboRol)
        cargarDatosEmpresaxArea(Me.dgvArea)
        cargarDatosEmpresaxRol(Me.dgvRol)
        OcultarControles(True)
        HabilitarBotonGrabar(True, False)
    End Sub
    Protected Sub dgvTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvTienda.SelectedIndexChanged
        quitarRegistroTienda()
    End Sub
    'Protected Sub btnAgregar_Tienda_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregar_Tienda.Click
    '    addTiendaGrilla()
    'End Sub
    Protected Sub btnAgregar_TiendaA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAgregar_TiendaA.Click
        addTiendaGrilla()
    End Sub

    '' EMPRESA X ALMACEN
    Private Sub cargarDatosCboAlmacen(ByVal cboAlmacen As DropDownList)
        'cbo.llenarCboAlmacen(cboAlmacen, True)
        Dim obj As New Negocio.Almacen
        Dim lista As List(Of Entidades.Almacen) = obj.SelectAllActivo(True)
        lista.Insert(0, (New Entidades.Almacen(0, "----")))
        cboAlmacen.DataTextField = "Nombre"
        cboAlmacen.DataValueField = "IdAlmacen"
        cboAlmacen.DataSource = lista
        cboAlmacen.DataBind()
    End Sub
    Private Sub cargarDatosEmpresaxAlmacen(ByVal grilla As GridView)
        Dim obj As New Negocio.Almacen
        grilla.DataSource = obj.SelectCboxIdEmpresa(CInt(Me.cboEmpresa.SelectedValue))
        grilla.DataBind()
    End Sub
    Private Function obtenerListaAlmacenFrmGrilla() As List(Of Entidades.EmpresaAlmacen)
        Dim lista As New List(Of Entidades.EmpresaAlmacen)
        For i As Integer = 0 To dgvAlmacen.Rows.Count - 1
            With dgvAlmacen.Rows(i)
                Dim obj As New Entidades.EmpresaAlmacen(CInt(.Cells(0).Text), .Cells(1).Text, CInt(Me.cboEmpresa.SelectedValue), CType(Me.dgvAlmacen.Rows(i).Cells(2).FindControl("chkEstadoAlmacen"), CheckBox).Checked)
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Private Sub addAlmacenGrilla()
        Dim lista As List(Of Entidades.EmpresaAlmacen) = obtenerListaAlmacenFrmGrilla()
        Dim obj As New Entidades.EmpresaAlmacen(CInt(Me.cboAlmacen.SelectedValue), Me.cboAlmacen.SelectedItem.Text, CInt(Me.cboEmpresa.SelectedValue), True)
        lista.Add(obj)
        dgvAlmacen.DataSource = lista
        dgvAlmacen.DataBind()
    End Sub
    Private Sub quitarRegistroAlmacen()
        Dim objUtil As New Negocio.Util
        Dim lista As List(Of Entidades.EmpresaAlmacen) = obtenerListaAlmacenFrmGrilla()
        lista.RemoveAt(dgvAlmacen.SelectedIndex)
        dgvAlmacen.DataSource = lista
        dgvAlmacen.DataBind()
    End Sub
    'Protected Sub btnAgregar_Almacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregar_Almacen.Click
    '    addAlmacenGrilla()
    'End Sub
    Protected Sub btnAgregar_AlmacenA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAgregar_AlmacenA.Click
        addAlmacenGrilla()
    End Sub

    Protected Sub dgvAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvAlmacen.SelectedIndexChanged
        quitarRegistroAlmacen()
    End Sub
    '' EMPRESA X AREA
    Private Sub cargarDatosCboArea(ByVal cboAlmacen As DropDownList)
        'Dim lista As List(Of Entidades.Area) = (New Negocio.Area).SelectAll
        Dim lista As List(Of Entidades.Area) = (New Negocio.Area).SelectAllActivo()
        Dim objArea As New Entidades.Area
        Dim listaNew As New List(Of Entidades.Area)
        lista.Insert(0, (New Entidades.Area(0, "----", True)))

        For Each objArea In lista
            objArea.DescripcionCorta = HttpUtility.HtmlDecode(objArea.DescripcionCorta)
            'objArea.Id = objArea.Id
            listaNew.Add(objArea)
        Next
        cboAlmacen.DataTextField = "DescripcionCorta"
        cboAlmacen.DataValueField = "Id"
        cboAlmacen.DataSource = listaNew
        cboAlmacen.DataBind()
    End Sub
    Private Sub cargarDatosEmpresaxArea(ByVal grilla As GridView)
        Dim obj As New Negocio.Area
        grilla.DataSource = obj.SelectCboxIdEmpresa(CInt(Me.cboEmpresa.SelectedValue))
        grilla.DataBind()
    End Sub
    Private Sub cargarDatosEmpresaxRol(ByVal grilla As GridView)
        Dim obj As New Negocio.Rol_Empresa
        grilla.DataSource = obj.SelectxIdEmpresa(CInt(Me.cboEmpresa.SelectedValue))
        grilla.DataBind()
    End Sub
    Private Function obtenerListaAreaFrmGrilla() As List(Of Entidades.AreaEmpresa)
        Dim lista As New List(Of Entidades.AreaEmpresa)
        For i As Integer = 0 To dgvArea.Rows.Count - 1
            With dgvArea.Rows(i)
                Dim obj As New Entidades.AreaEmpresa(CInt(.Cells(0).Text), HttpUtility.HtmlDecode(.Cells(1).Text), CInt(Me.cboEmpresa.SelectedValue), CType(Me.dgvArea.Rows(i).Cells(2).FindControl("chkEstadoArea"), CheckBox).Checked)
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Private Function obtenerListaRolFrmGrilla() As List(Of Entidades.Rol_Empresa)
        Dim lista As New List(Of Entidades.Rol_Empresa)
        For i As Integer = 0 To dgvRol.Rows.Count - 1
            With dgvRol.Rows(i)
                'Dim obj As New Entidades.Rol_Empresa(CInt(.Cells(0).Text), HttpUtility.HtmlDecode(.Cells(1).Text), CInt(Me.cboEmpresa.SelectedValue), CType(Me.dgvRol.Rows(i).Cells(2).FindControl("chkEstadoRol"), CheckBox).Checked)
                Dim obj As New Entidades.Rol_Empresa()
                obj.IdRol = CInt(.Cells(0).Text)
                obj.Rol = HttpUtility.HtmlDecode(.Cells(1).Text)
                obj.IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                'obj.Empresa=
                obj.Estado = CType(Me.dgvRol.Rows(i).Cells(2).FindControl("chkEstadoRol"), CheckBox).Checked

                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Private Sub addAreaGrilla()
        Dim lista As List(Of Entidades.AreaEmpresa) = obtenerListaAreaFrmGrilla()
        Dim obj As New Entidades.AreaEmpresa(CInt(Me.cboArea.SelectedValue), Me.cboArea.SelectedItem.Text, CInt(Me.cboEmpresa.SelectedValue), True)
        lista.Add(obj)
        dgvArea.DataSource = lista
        dgvArea.DataBind()
    End Sub

    Private Sub quitarRegistroArea()
        Dim objUtil As New Negocio.Util
        Dim lista As List(Of Entidades.AreaEmpresa) = obtenerListaAreaFrmGrilla()
        lista.RemoveAt(dgvArea.SelectedIndex)
        dgvArea.DataSource = lista
        dgvArea.DataBind()
    End Sub
    Private Sub addRolGrilla()
        Dim lista As List(Of Entidades.Rol_Empresa) = obtenerListaRolFrmGrilla()
        Dim obj As New Entidades.Rol_Empresa

        obj.IdRol = CInt(Me.cboRol.SelectedValue)
        obj.Rol = cboRol.SelectedItem.Text
        obj.IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
        obj.Estado = True

        lista.Add(obj)
        dgvRol.DataSource = lista
        dgvRol.DataBind()
    End Sub
    Private Sub quitarRegistroRol()
        Dim objUtil As New Negocio.Util
        Dim lista As List(Of Entidades.Rol_Empresa) = obtenerListaRolFrmGrilla()
        lista.RemoveAt(dgvRol.SelectedIndex)
        dgvRol.DataSource = lista
        dgvRol.DataBind()
    End Sub
    'Protected Sub btnAgregar_Area_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregar_Area.Click
    '    addAreaGrilla()
    'End Sub
    Protected Sub btnAgregar_AreaA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAgregar_AreaA.Click
        addAreaGrilla()
    End Sub

    Protected Sub dgvArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvArea.SelectedIndexChanged
        quitarRegistroArea()
    End Sub

    Protected Sub btnAgregar_RolA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAgregar_RolA.Click
        addRolGrilla()
    End Sub

    Protected Sub dgvRol_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvRol.SelectedIndexChanged
        quitarRegistroRol()
    End Sub

    Sub OcultarControles(ByVal Activar As Boolean)
        PnlEmpresaTienda.Visible = Activar
        Label2.Visible = Activar
        Label38.Visible = Activar
        Label39.Visible = Activar
        cboTienda.Visible = Activar
        Me.txtCuentaContable.Visible = Activar
        Me.txtCentroCosto.Visible = Activar
        btnAgregar_TiendaA.Visible = Activar
        Label3.Visible = Activar
        cboAlmacen.Visible = Activar
        'btnAgregar_Almacen.Visible = Activar
        btnAgregar_AlmacenA.Visible = Activar
        PnlEmpresaAlmacen.Visible = Activar
        Label4.Visible = Activar
        cboArea.Visible = Activar
        cboRol.Visible = Activar
        'btnAgregar_Area.Visible = Activar
        btnAgregar_AreaA.Visible = Activar
        btnAgregar_RolA.Visible = Activar
        PnlEmpresaArea.Visible = Activar
        PnlEmpresaRol.Visible = Activar
        'cmd_MantTienda.Visible = Activar
        'cmd_MantTiendaA.Visible = Activar
        'cmd_MantArea.Visible = Activar
        'cmd_MantAreaA.Visible = Activar
        'Me.cmd_MantAlmacen.Visible = Activar
        'Me.cmd_MantAlmacenA.Visible = Activar
    End Sub
    Sub OcultarControlesTienda(ByVal Activar As Boolean)
        Me.PnlTiendaArea.Visible = Activar
        Me.PnlTiendaAlmacen.Visible = Activar
        'Me.PnlTiendaCaja.Visible = Activar
    End Sub
    'Protected Sub cmd_Guardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_Guardar.Click
    '    Grabar()
    '    cargarDatosEmpresaxTienda(Me.dgvTienda)
    '    cargarDatosCboTienda(Me.cboTienda)
    '    cargarDatosCboAlmacen(Me.cboAlmacen)
    '    cargarDatosEmpresaxAlmacen(Me.dgvAlmacen)
    '    cargarDatosCboArea(Me.cboArea)

    '    cargarDatosEmpresaxArea(Me.dgvArea)
    'End Sub
    Protected Sub cmd_GuardarA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_GuardarA.Click
        Grabar()
        cargarDatosEmpresaxTienda(Me.dgvTienda)
        cargarDatosCboTienda(Me.cboTienda)
        cargarDatosCboAlmacen(Me.cboAlmacen)
        cargarDatosEmpresaxAlmacen(Me.dgvAlmacen)
        cargarDatosCboArea(Me.cboArea)
        cbo.LlenarCboRol(Me.cboRol, True)
        cargarDatosEmpresaxArea(Me.dgvArea)
        cargarDatosEmpresaxRol(Me.dgvRol)
    End Sub


    '' TIENDA X AREA
    Private Sub cargarDatosTiendaxArea(ByVal grilla As GridView)
        Dim obj As New Negocio.Area
        grilla.DataSource = obj.SelectCboxIdTienda(CInt(Me.cboTiendaT.SelectedValue))
        grilla.DataBind()
    End Sub
    Protected Sub cboTiendaT_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTiendaT.SelectedIndexChanged
        'HabilitarBotonCaja1(False, True, True)
        cargarDatosCboArea(Me.cboAreaT)
        cargarDatosCboUnidadNegocio(dlunidadnegocio)
        cargarDatosTiendaxArea(Me.dgvAreaT)
        cargarDatosCboAlmacen(Me.cboAlmacenT)
        cargarDatosTiendaxAlmacen(Me.dgvAlmacenT)
        'cargarDatosCboCaja(Me.cboCaja)
        'cargarDatosTiendaxCaja(Me.dgvCaja)
        OcultarControlesTienda(True)
        HabilitarBotonGrabar(False, True)
    End Sub
    Private Sub cargarDatosCboUnidadNegocio(ByVal combo As DropDownList)
        If combo.Items.Count = 0 Then
            cbo.LlenarCboCod_UniNeg(combo, True)
            selectedValueUnidadNegocio()
        End If
    End Sub

    Private Function obtenerListaAreaTFrmGrilla() As List(Of Entidades.TiendaArea)
        Dim lista As New List(Of Entidades.TiendaArea)
        For i As Integer = 0 To dgvAreaT.Rows.Count - 1
            With dgvAreaT.Rows(i)
                Dim obj As New Entidades.TiendaArea(CInt(.Cells(0).Text), HttpUtility.HtmlDecode(.Cells(1).Text), CInt(Me.cboTiendaT.SelectedValue), HttpUtility.HtmlDecode(CType(.Cells(2).FindControl("hdd_idcentrocosto"), HiddenField).Value), CType(.Cells(3).FindControl("chkEstadoTiendaArea"), CheckBox).Checked)
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Private Sub addAreaTGrilla()
        Dim lista As List(Of Entidades.TiendaArea) = obtenerListaAreaTFrmGrilla()
        Dim obj As New Entidades.TiendaArea(CInt(Me.cboAreaT.SelectedValue), Me.cboAreaT.SelectedItem.Text, CInt(Me.cboTiendaT.SelectedValue), True)
        lista.Add(obj)
        dgvAreaT.DataSource = lista
        dgvAreaT.DataBind()
    End Sub
    Private Sub quitarRegistroAreaT()
        Dim objUtil As New Negocio.Util
        Dim lista As List(Of Entidades.TiendaArea) = obtenerListaAreaTFrmGrilla()
        lista.RemoveAt(dgvAreaT.SelectedIndex)
        dgvAreaT.DataSource = lista
        dgvAreaT.DataBind()
    End Sub
    'Protected Sub btnAgregar_AreaT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregar_AreaT.Click
    '    addAreaTGrilla()
    'End Sub
    Protected Sub btnAgregar_AreaTB_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAgregar_AreaTB.Click
        addAreaTGrilla()
    End Sub

    Protected Sub dgvAreaT_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvAreaT.SelectedIndexChanged
        quitarRegistroAreaT()
    End Sub
    'Protected Sub cmd_GuardarT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_GuardarT.Click
    '    ''  TIENDA AREA
    '    Dim lTiendaArea As List(Of Entidades.TiendaArea) = obtenerListaAreaTFrmGrilla()

    '    '' TIENDA ALMACEN
    '    Dim lTiendaAlmacen As List(Of Entidades.TiendaAlmacen) = obtenerListaAlmacenTFrmGrilla()

    '    '' TIENDA CAJA
    '    Dim lTiendaCaja As List(Of Entidades.Caja) = obtenerListaCajaFrmGrilla()

    '    Dim ngcTienda As New Negocio.Tienda
    '    If ngcTienda.InsertaConfiguracionTiendaT(CInt(Me.cboTiendaT.SelectedValue), lTiendaArea, lTiendaAlmacen, lTiendaCaja) = True Then
    '        sc.mostrarMsjAlerta(Me, "Se grabo correctamente los datos")
    '    Else
    '        sc.mostrarMsjAlerta(Me, "La Información ya fue grabado no se puede volver a actualizar la información porque existen datos relacionado con otras tablas")
    '    End If
    '    cargarDatosCboArea(Me.cboAreaT)
    '    cargarDatosTiendaxArea(Me.dgvAreaT)
    '    cargarDatosCboAlmacen(Me.cboAlmacenT)
    '    cargarDatosTiendaxAlmacen(Me.dgvAlmacenT)
    '    cargarDatosCboCaja(Me.cboCaja)
    '    cargarDatosTiendaxCaja(Me.dgvCaja)
    '    OcultarControlesTienda(True)
    'End Sub

    Protected Sub cmd_GuardarTB_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_GuardarTB.Click
        ''  TIENDA AREA
        Dim lTiendaArea As List(Of Entidades.TiendaArea) = obtenerListaAreaTFrmGrilla()

        '' TIENDA ALMACEN
        Dim lTiendaAlmacen As List(Of Entidades.TiendaAlmacen) = obtenerListaAlmacenTFrmGrilla()

        '' TIENDA CAJA
        'Dim lTiendaCaja As List(Of Entidades.Caja) = obtenerListaCajaFrmGrilla()

        Dim ngcTienda As New Negocio.Tienda
        If ngcTienda.InsertaConfiguracionTiendaT(CInt(Me.cboTiendaT.SelectedValue), lTiendaArea, lTiendaAlmacen) = True Then
            sc.mostrarMsjAlerta(Me, "Se grabo correctamente los datos")
        Else
            sc.mostrarMsjAlerta(Me, "La Información ya fue grabado no se puede volver a actualizar la información porque existen datos relacionado con otras tablas")
        End If
        cargarDatosCboArea(Me.cboAreaT)
        cargarDatosTiendaxArea(Me.dgvAreaT)
        cargarDatosCboAlmacen(Me.cboAlmacenT)
        cargarDatosTiendaxAlmacen(Me.dgvAlmacenT)
        'cargarDatosCboCaja(Me.cboCaja)
        'cargarDatosTiendaxCaja(Me.dgvCaja)
        OcultarControlesTienda(True)
    End Sub


    '' TIENDA ALMACEN
    Private Sub cargarDatosTiendaxAlmacen(ByVal grilla As GridView)
        Dim obj As New Negocio.Almacen
        grilla.DataSource = obj.SelectCboxIdTienda(CInt(Me.cboTiendaT.SelectedValue))
        grilla.DataBind()
    End Sub
    Private Function obtenerListaAlmacenTFrmGrilla() As List(Of Entidades.TiendaAlmacen)
        Dim lista As New List(Of Entidades.TiendaAlmacen)
        For i As Integer = 0 To dgvAlmacenT.Rows.Count - 1
            With dgvAlmacenT.Rows(i)
                Dim obj As New Entidades.TiendaAlmacen(CInt(.Cells(0).Text), HttpUtility.HtmlDecode(.Cells(1).Text), CInt(Me.cboTiendaT.SelectedValue), CType(Me.dgvAlmacenT.Rows(i).Cells(2).FindControl("chkTiendaAlmancenPrincipal"), CheckBox).Checked, CType(Me.dgvAlmacenT.Rows(i).Cells(3).FindControl("chkTiendaAlmance2"), CheckBox).Checked)
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Private Sub addAlmacenTGrilla()
        Dim lista As List(Of Entidades.TiendaAlmacen) = obtenerListaAlmacenTFrmGrilla()
        Dim obj As New Entidades.TiendaAlmacen(CInt(Me.cboAlmacenT.SelectedValue), HttpUtility.HtmlDecode(Me.cboAlmacenT.SelectedItem.Text), CInt(Me.cboTiendaT.SelectedValue), False, True)
        lista.Add(obj)
        dgvAlmacenT.DataSource = lista
        dgvAlmacenT.DataBind()
    End Sub
    Private Sub quitarRegistroAlmacenT()
        Dim objUtil As New Negocio.Util
        Dim lista As List(Of Entidades.TiendaAlmacen) = obtenerListaAlmacenTFrmGrilla()
        lista.RemoveAt(dgvAlmacenT.SelectedIndex)
        dgvAlmacenT.DataSource = lista
        dgvAlmacenT.DataBind()
    End Sub
    'Protected Sub btnAgregar_AlmacenT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregar_AlmacenT.Click
    '    addAlmacenTGrilla()
    'End Sub
    Protected Sub btnAgregar_AlmacenTB_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAgregar_AlmacenTB.Click
        addAlmacenTGrilla()
    End Sub

    Protected Sub dgvAlmacenT_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvAlmacenT.SelectedIndexChanged
        quitarRegistroAlmacenT()
    End Sub
    '' TIENDA CAJA
    Private Sub cargarDatosCboCaja(ByVal cboCaja As DropDownList)
        Dim lista As List(Of Entidades.Caja) = (New Negocio.Caja).SelectAll
        lista.Insert(0, New Entidades.Caja(0, "-----"))
        cboCaja.DataTextField = "Nombre"
        cboCaja.DataValueField = "IdCaja"
        cboCaja.DataSource = lista
        cboCaja.DataBind()
    End Sub
    Private Sub cargarDatosTiendaxCaja(ByVal grilla As GridView)
        Dim obj As New Negocio.Caja
        grilla.DataSource = obj.SelectGrillaxIdTienda(CInt(Me.cboTiendaT.SelectedValue))
        grilla.DataBind()
    End Sub
    'Private Function obtenerListaCajaFrmGrilla() As List(Of Entidades.Caja)
    '    Dim lista As New List(Of Entidades.Caja)
    '    For i As Integer = 0 To dgvCaja.Rows.Count - 1
    '        With dgvCaja.Rows(i)
    '            Dim obj As New Entidades.Caja(CInt(.Cells(0).Text), CInt(Me.cboTiendaT.SelectedValue), HttpUtility.HtmlDecode(CStr(.Cells(1).Text)), HttpUtility.HtmlDecode(.Cells(2).Text), CType(Me.dgvCaja.Rows(i).Cells(3).FindControl("chkTiendaCaja2"), CheckBox).Checked)
    '            lista.Add(obj)
    '        End With
    '    Next
    '    Return lista
    'End Function
    'Private Sub addCajaGrilla()
    '    Dim lista As List(Of Entidades.Caja) = obtenerListaCajaFrmGrilla()
    '    If Session("Indicador").ToString = "0" Then
    '        Dim obj As New Entidades.Caja(CInt(Me.cboCaja.SelectedValue), CInt(Me.cboTiendaT.SelectedValue), CStr(Me.cboCaja.SelectedItem.Text), Me.txtNroCaja.Text, True)
    '        lista.Add(obj)
    '    End If
    '    If Session("Indicador").ToString = "1" Then
    '        Dim obj As New Entidades.Caja(CInt(0), CInt(Me.cboTiendaT.SelectedValue), HttpUtility.HtmlDecode(CStr(Me.txtNomCaja.Text)), HttpUtility.HtmlDecode(Me.txtNroCaja.Text), True)
    '        lista.Add(obj)
    '    End If
    '    dgvCaja.DataSource = lista
    '    dgvCaja.DataBind()
    'End Sub
    'Private Sub quitarRegistroCaja()
    '    Dim objUtil As New Negocio.Util
    '    Dim lista As List(Of Entidades.Caja) = obtenerListaCajaFrmGrilla()
    '    lista.RemoveAt(dgvCaja.SelectedIndex)
    '    dgvCaja.DataSource = lista
    '    dgvCaja.DataBind()
    'End Sub
    'Protected Sub btnAgregar_Caja_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregar_Caja.Click
    '    If CStr(Session("Indicador")) = "0" Then
    '        If Me.cboCaja.SelectedIndex = -1 Then
    '            sc.mostrarMsjAlerta(Me, "Seleccione Caja.")
    '            Exit Sub
    '        End If
    '    End If
    '    If CStr(Session("Indicador")) = "1" Then
    '        If Me.txtNomCaja.Text = "" Then
    '            sc.mostrarMsjAlerta(Me, "Ingrese Caja.")
    '            Me.txtNomCaja.Focus()
    '            Exit Sub
    '        End If
    '        If Me.txtNroCaja.Text = "" Then
    '            sc.mostrarMsjAlerta(Me, "Ingrese Nro Caja.")
    '            Me.txtNroCaja.Focus()
    '            Exit Sub
    '        End If
    '    End If
    '    addCajaGrilla()
    '    HabilitarBotonCaja1(False, True, True)
    'End Sub
    'Protected Sub btnAgregar_CajaB_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAgregar_CajaB.Click
    '    If CStr(Session("Indicador")) = "0" Then
    '        If Me.cboCaja.SelectedIndex = -1 Then
    '            sc.mostrarMsjAlerta(Me, "Seleccione Caja.")
    '            Exit Sub
    '        End If
    '    End If
    '    If CStr(Session("Indicador")) = "1" Then
    '        If Me.txtNomCaja.Text = "" Then
    '            sc.mostrarMsjAlerta(Me, "Ingrese Caja.")
    '            Me.txtNomCaja.Focus()
    '            Exit Sub
    '        End If
    '        If Me.txtNroCaja.Text = "" Then
    '            sc.mostrarMsjAlerta(Me, "Ingrese Nro Caja.")
    '            Me.txtNroCaja.Focus()
    '            Exit Sub
    '        End If
    '    End If
    '    addCajaGrilla()
    '    HabilitarBotonCaja1(False, True, True)
    'End Sub

    'Protected Sub dgvCaja_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvCaja.SelectedIndexChanged
    '    quitarRegistroCaja()
    'End Sub
    'Sub HabilitarCaja(ByVal ActivarNuevo As Boolean, ByVal ActivarCancelar As Boolean, ByVal ActivarAgregar As Boolean, ByVal ActivarCbo As Boolean, ByVal ActivarTexto As Boolean, ByVal Clear As String)
    '    'Me.btnNuevoCaja.Visible = ActivarNuevo
    '    Me.btnNuevoCajaB.Visible = ActivarNuevo
    '    'Me.btnCancelarCaja.Visible = ActivarCancelar
    '    Me.btnCancelarCajaB.Visible = ActivarCancelar
    '    'Me.btnAgregar_Caja.Visible = ActivarAgregar
    '    Me.btnAgregar_CajaB.Visible = ActivarAgregar
    '    Me.cboCaja.Visible = ActivarCbo
    '    Me.txtNomCaja.Visible = ActivarTexto
    '    Me.txtNroCaja.Visible = ActivarTexto
    '    Me.Label9.Visible = ActivarTexto
    '    Me.txtNomCaja.Text = Clear
    '    Me.txtNroCaja.Text = Clear
    'End Sub
    'Protected Sub btnNuevoCaja_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevoCaja.Click
    '    Session("Indicador") = "1"
    '    HabilitarCaja(True, True, True, False, True, "")
    '    HabilitarBotonCaja1(False, True, True)
    'End Sub
    'Protected Sub btnNuevoCajaB_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevoCajaB.Click
    '    Session("Indicador") = "1"
    '    HabilitarCaja(True, True, True, False, True, "")
    '    HabilitarBotonCaja1(False, True, True)
    'End Sub

    'Protected Sub btnCancelarCaja_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelarCaja.Click
    '    Session("Indicador") = "0"
    '    HabilitarCaja(True, True, True, True, False, "")
    '    HabilitarBotonCaja1(True, True, True)
    'End Sub
    'Protected Sub btnCancelarCajaB_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelarCajaB.Click
    '    Session("Indicador") = "0"
    '    HabilitarCaja(True, True, True, True, False, "")
    '    HabilitarBotonCaja1(True, True, True)
    'End Sub

    'Protected Sub cboCaja_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboCaja.SelectedIndexChanged
    '    SelectCajaxId(CInt(Me.cboCaja.SelectedValue))
    'End Sub
    'Sub SelectCajaxId(ByVal IdCaja As Integer)
    '    Dim nCaja As New Negocio.Caja
    '    Dim objCaja As Entidades.Caja = nCaja.SelectCajaxId(IdCaja)
    '    Me.txtNroCaja.Text = objCaja.CajaNumero
    'End Sub
    'Sub HabilitarBotonCaja1(ByVal ActivarNuevo As Boolean, ByVal ActivarCancelar As Boolean, ByVal ActivarAñadir As Boolean)
    '    'Me.btnNuevoCaja.Visible = ActivarNuevo
    '    Me.btnNuevoCajaB.Visible = ActivarNuevo
    '    'Me.btnCancelarCaja.Visible = ActivarCancelar
    '    Me.btnCancelarCajaB.Visible = ActivarCancelar
    '    'Me.btnAgregar_Caja.Visible = ActivarAñadir
    '    Me.btnAgregar_CajaB.Visible = ActivarAñadir
    'End Sub

    '' MANTENIMIENTO TIENDA
    Private Sub CboTipoTienda(ByVal cbo As DropDownList)
        Dim obj As New Negocio.TipoTienda
        cbo.DataSource = obj.SelectAllActivo()
        cbo.DataTextField = "Nombre"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    'Protected Sub cmd_MantTienda_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_MantTienda.Click
    '    HabilitarBotonTienda(True, False, False, False)
    '    HabilitarMantTienda(False)
    '    LimpiarMantTienda()
    '    Me.PanelNuevoT.Visible = False : Me.PanelBuscarT.Visible = True
    '    cbo.LLenarCboDepartamento(Me.cboDptoT)
    '    CboTipoTienda(Me.cboTipoT)
    '    Session("IndicadorTienda") = "I"
    'End Sub

    'Protected Sub cmd_MantTiendaA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_MantTiendaA.Click
    '    'HabilitarBotonTienda(True,  False, False, False)
    '    HabilitarBotonTienda(False, True, False, True)
    '    'HabilitarMantTienda(False)
    '    HabilitarMantTienda(True)
    '    LimpiarMantTienda()
    '    'Me.PanelNuevoT.Visible = False : Me.PanelBuscarT.Visible = True
    '    Me.PanelNuevoT.Visible = True : Me.PanelBuscarT.Visible = False
    '    cbo.LLenarCboDepartamento(Me.cboDptoT)
    '    CboTipoTienda(Me.cboTipoT)
    '    Session("IndicadorTienda") = "I"
    'End Sub


    Protected Sub cboDptoT_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboDptoT.SelectedIndexChanged
        cbo.LLenarCboProvincia(Me.cboProvT, Me.cboDptoT.SelectedValue)
    End Sub
    Protected Sub cboProvT_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboProvT.SelectedIndexChanged
        cbo.LLenarCboDistrito(Me.cboDistT, Me.cboDptoT.SelectedValue, Me.cboProvT.SelectedValue)
    End Sub
    Sub GrabarTienda(ByVal Indicador As String)
        Dim ngcTienda As New Negocio.Tienda
        Dim objTienda As New Entidades.Tienda
        If CStr(Indicador) = "I" Then
            With objTienda
                .Nombre = HttpUtility.HtmlDecode(Me.txtNomT.Text)
                .Ubigeo = Me.cboDptoT.SelectedValue + Me.cboProvT.SelectedValue + Me.cboDistT.SelectedValue
                .Direccion = HttpUtility.HtmlDecode(Me.txtDireccionT.Text)
                .Referencia = HttpUtility.HtmlDecode(Me.txtReferenciaT.Text)
                .Telefono = HttpUtility.HtmlDecode(Me.txtTelefonoT.Text)
                .Fax = HttpUtility.HtmlDecode(Me.txtFaxT.Text)
                .Estado = "1"
                .IdTipoTienda = CInt(Me.cboTipoT.SelectedValue)
                .Email = Me.txtMail.Text
            End With
            If ngcTienda.InsertaTienda(objTienda) = True Then
            Else
                sc.mostrarMsjAlerta(Me, "Error al grabar la Tienda")
            End If
        ElseIf CStr(Indicador) = "U" Then
            If Me.chkEliminar.Checked = False Then
                With objTienda
                    .Nombre = HttpUtility.HtmlDecode(Me.txtNomT.Text)
                    .Ubigeo = Me.cboDptoT.SelectedValue + Me.cboProvT.SelectedValue + Me.cboDistT.SelectedValue
                    .Direccion = HttpUtility.HtmlDecode(Me.txtDireccionT.Text)
                    .Referencia = HttpUtility.HtmlDecode(txtReferenciaT.Text)
                    .Telefono = HttpUtility.HtmlDecode(Me.txtTelefonoT.Text)
                    .Fax = HttpUtility.HtmlDecode(Me.txtFaxT.Text)
                    .Estado = "1"
                    .IdTipoTienda = CInt(Me.cboTipoT.SelectedValue)
                    .Id = CInt(Session("IdTiendaT"))
                    .Email = HttpUtility.HtmlDecode(Me.txtMail.Text)
                End With
                If ngcTienda.ActualizaTienda(objTienda) = True Then
                    'sc.mostrarMsjAlerta(Me, "Se Actualizo correctamente los datos")
                Else
                    sc.mostrarMsjAlerta(Me, "Error al Actualizar la Tienda")
                End If
            ElseIf Me.chkEliminar.Checked = True Then
                With objTienda
                    .Nombre = HttpUtility.HtmlDecode(Me.txtNomT.Text)
                    .Ubigeo = Me.cboDptoT.SelectedValue + Me.cboProvT.SelectedValue + Me.cboDistT.SelectedValue
                    .Direccion = HttpUtility.HtmlDecode(Me.txtDireccionT.Text)
                    .Referencia = HttpUtility.HtmlDecode(Me.txtReferenciaT.Text)
                    .Telefono = HttpUtility.HtmlDecode(Me.txtTelefonoT.Text)
                    .Fax = HttpUtility.HtmlDecode(Me.txtFaxT.Text)
                    .Estado = "0"
                    .IdTipoTienda = CInt(Me.cboTipoT.SelectedValue)
                    .Id = CInt(Session("IdTiendaT"))
                End With
                If ngcTienda.ActualizaTienda(objTienda) = True Then
                    'sc.mostrarMsjAlerta(Me, "Se Elimino correctamente los datos")
                Else
                    sc.mostrarMsjAlerta(Me, "Error al Eliminar la Tienda")
                End If
            End If
        End If
        HabilitarMantTienda(False)
    End Sub
    Protected Sub cmd_GuardarMantT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_GuardarMantT.Click
        If Me.txtNomT.Text = "" Then : Exit Sub : End If
        If (Me.cboTipoT.SelectedValue = "") Or (Me.cboTipoT.SelectedValue = "0") Then : Exit Sub : End If
        If Me.txtDireccionT.Text = "" Then : Exit Sub : End If
        If Me.cboDptoT.SelectedValue = "00" Then : Exit Sub : End If
        If Me.cboProvT.SelectedValue = "00" Then : Exit Sub : End If
        If Me.cboDistT.SelectedValue = "00" Then : Exit Sub : End If
        GrabarTienda(CStr(Session("IndicadorTienda")))
        LimpiarMantTienda()
        HabilitarMantTienda(False)
        HabilitarBotonTienda(True, False, False, False)
        Me.PanelBuscarT.Visible = True : Me.PanelNuevoT.Visible = False
        cargarDatosCboTienda(Me.cboTienda)
    End Sub
    Sub LimpiarMantTienda()
        Try
            Me.txtNomT.Text = ""
            Me.txtMail.Text = ""
            Me.cboTipoT.SelectedIndex = -1
            Me.txtDireccionT.Text = ""
            Me.txtReferenciaT.Text = ""
            Me.cboDptoT.SelectedIndex = 0
            Me.cboProvT.SelectedIndex = 0
            Me.cboDistT.SelectedIndex = 0
            Me.txtTelefonoT.Text = ""
            Me.txtFaxT.Text = ""
            Me.chkEliminar.Checked = False
            Me.txtBusNomT.Text = ""
            Me.dgvTiendaMant.DataBind()
        Catch ex As Exception
        Finally
        End Try
    End Sub
    Sub HabilitarMantTienda(ByVal Activar As Boolean)
        Me.txtNomT.Enabled = Activar
        Me.cboTipoT.Enabled = Activar
        Me.txtDireccionT.Enabled = Activar
        Me.txtReferenciaT.Enabled = Activar
        Me.cboDptoT.Enabled = Activar
        Me.cboProvT.Enabled = Activar
        Me.cboDistT.Enabled = Activar
        Me.txtTelefonoT.Enabled = Activar
        Me.txtFaxT.Enabled = Activar
        Me.chkEliminar.Enabled = Activar
    End Sub
    Sub HabilitarBotonTienda(ByVal ActivarNuevo As Boolean, ByVal ActivarGuardar As Boolean, ByVal ActivarEditar As Boolean, ByVal ActivarCancelar As Boolean)
        Me.btnNuevoMantT.Visible = ActivarNuevo
        Me.cmd_GuardarMantT.Visible = ActivarGuardar
        Me.cmd_EditarT.Visible = ActivarEditar
        Me.btnCancelarMantTienda.Visible = ActivarCancelar
    End Sub
    Protected Sub btnNuevoMantT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevoMantT.Click
        HabilitarBotonTienda(False, True, False, True)
        LimpiarMantTienda()
        Session("IndicadorTienda") = "I"
        Me.PanelBuscarT.Visible = False : Me.PanelNuevoT.Visible = True
        HabilitarMantTienda(True)
        Me.chkEliminar.Visible = False
    End Sub
    Protected Sub cmd_EditarT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_EditarT.Click
        Session("IndicadorTienda") = "U"
        Me.PanelBuscarT.Visible = True : Me.PanelNuevoT.Visible = False
        LimpiarMantTienda()
        HabilitarMantTienda(True)
    End Sub
    Private Sub btnCancelarMantTienda_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelarMantTienda.Click
        HabilitarBotonTienda(True, False, False, True)
        LimpiarMantTienda()
        Me.PanelBuscarT.Visible = True : Me.PanelNuevoT.Visible = False
    End Sub
    Private Sub chkEliminar_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEliminar.CheckedChanged
        If Me.chkEliminar.Checked = True Then
            HabilitarMantTienda(False)
            Me.chkEliminar.Enabled = True
        ElseIf Me.chkEliminar.Checked = False Then
            HabilitarMantTienda(True)
            Me.chkEliminar.Enabled = True
        End If
    End Sub
    Protected Sub cmd_BuscaMantT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_BuscaMantT.Click
        BuscarTienda(Me.dgvTiendaMant)
    End Sub
    Sub BuscarTienda(ByVal grilla As GridView)
        Dim ngcTienda As New Negocio.Tienda
        'grilla.DataSource = ngcTienda.BuscaAllxNombre(Me.txtBusNomT.Text)
        grilla.DataSource = ngcTienda.BuscaAllxNombreActivoInactivo(Me.txtBusNomT.Text)
        grilla.DataBind()
    End Sub
    Protected Sub dgvTiendaMant_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvTiendaMant.SelectedIndexChanged
        Try
            Session("IdTiendaT") = dgvTiendaMant.SelectedRow.Cells(11).Text
            Session("IndicadorTienda") = "U"
            HabilitarMantTienda(True)
            txtNomT.Text = HttpUtility.HtmlDecode(dgvTiendaMant.SelectedRow.Cells(1).Text).Trim
            txtDireccionT.Text = HttpUtility.HtmlDecode(dgvTiendaMant.SelectedRow.Cells(5).Text).Trim
            txtReferenciaT.Text = HttpUtility.HtmlDecode(dgvTiendaMant.SelectedRow.Cells(6).Text).Trim
            txtTelefonoT.Text = HttpUtility.HtmlDecode(dgvTiendaMant.SelectedRow.Cells(7).Text).Trim
            txtFaxT.Text = HttpUtility.HtmlDecode(dgvTiendaMant.SelectedRow.Cells(8).Text).Trim
            txtMail.Text = HttpUtility.HtmlDecode(dgvTiendaMant.SelectedRow.Cells(13).Text).Trim
            Me.cboDptoT.SelectedValue = Left(dgvTiendaMant.SelectedRow.Cells(12).Text, 2)
            cbo.LLenarCboProvincia(Me.cboProvT, Left(dgvTiendaMant.SelectedRow.Cells(12).Text, 2))
            Me.cboProvT.SelectedValue = Mid(dgvTiendaMant.SelectedRow.Cells(12).Text, 3, 2)
            cbo.LLenarCboDistrito(Me.cboDistT, Left(dgvTiendaMant.SelectedRow.Cells(12).Text, 2), Mid(dgvTiendaMant.SelectedRow.Cells(12).Text, 3, 2))
            Me.cboDistT.SelectedValue = Mid(dgvTiendaMant.SelectedRow.Cells(12).Text, 5, 2)
            cboTipoT.Text = dgvTiendaMant.SelectedRow.Cells(9).Text
            If dgvTiendaMant.SelectedRow.Cells(10).Text = "Activo" Then
                chkEliminar.Checked = False
            ElseIf dgvTiendaMant.SelectedRow.Cells(10).Text = "Inactivo" Then
                chkEliminar.Checked = True
            End If
            Me.PanelBuscarT.Visible = False : Me.PanelNuevoT.Visible = True
            HabilitarBotonTienda(False, True, False, True)
        Catch ex As Exception
            Me.PanelBuscarT.Visible = False : Me.PanelNuevoT.Visible = True
            HabilitarBotonTienda(False, True, False, True)
            If dgvTiendaMant.SelectedRow.Cells(10).Text = "Activo" Then
                chkEliminar.Checked = False
            ElseIf dgvTiendaMant.SelectedRow.Cells(10).Text = "Inactivo" Then
                chkEliminar.Checked = True
            End If
        Finally
        End Try
    End Sub
    '' MANTENIMIENTO AREA
    'Protected Sub cmd_MantArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_MantArea.Click
    '    HabilitarBotonArea(True, False, False, False)
    '    HabilitarMantArea(False)
    '    LimpiarMantArea()
    '    Me.PanelNuevoArea.Visible = False : Me.PanelBuscarArea.Visible = True
    '    Session("IndicadorArea") = "I"
    'End Sub

    'Protected Sub cmd_MantAreaA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_MantAreaA.Click
    '    'HabilitarBotonArea(True, False, False, False)
    '    HabilitarBotonArea(False, True, False, True)
    '    'HabilitarMantArea(False)
    '    HabilitarMantArea(True)
    '    LimpiarMantArea()
    '    'Me.PanelNuevoArea.Visible = False : Me.PanelBuscarArea.Visible = True
    '    Me.PanelNuevoArea.Visible = True : Me.PanelBuscarArea.Visible = False
    '    Session("IndicadorArea") = "I"
    'End Sub

    Private Sub btnNuevoArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevoArea.Click
        HabilitarBotonArea(False, True, False, True)
        LimpiarMantArea()
        Session("IndicadorArea") = "I"
        Me.PanelBuscarArea.Visible = False : Me.PanelNuevoArea.Visible = True
        HabilitarMantArea(True)
        chkEliminarArea.Visible = False
    End Sub
    Private Sub btnGrabarArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGrabarArea.Click
        If Me.txtNombreArea.Text = "" Then : Exit Sub : End If
        If Me.txtNomCortoArea.Text = "" Then : Exit Sub : End If
        GrabarArea(CStr(Session("IndicadorArea")))
        cargarDatosCboArea(Me.cboArea)
        HabilitarMantArea(False)
        LimpiarMantArea()
        HabilitarBotonArea(True, False, False, False)
        Me.PanelBuscarArea.Visible = True : Me.PanelNuevoArea.Visible = False
    End Sub
    Private Sub btnEditarArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditarArea.Click
        Me.PanelBuscarArea.Visible = True : Me.PanelNuevoArea.Visible = False
        Session("IndicadorArea") = "U"
        LimpiarMantArea()
        HabilitarMantArea(True)
    End Sub
    Sub HabilitarMantArea(ByVal Activar As Boolean)
        Me.txtNombreArea.Enabled = Activar
        Me.txtNomCortoArea.Enabled = Activar
        Me.chkEliminarArea.Enabled = Activar
    End Sub
    Sub LimpiarMantArea()
        Me.txtNombreArea.Text = ""
        Me.txtNomCortoArea.Text = ""
        Me.chkEliminarArea.Checked = False
        Me.dgvMantArea.DataBind()
    End Sub
    Sub GrabarArea(ByVal Indicador As String)
        Dim ngcArea As New Negocio.Area
        Dim objArea As New Entidades.Area
        If CStr(Indicador) = "I" Then
            With objArea
                .Descripcion = HttpUtility.HtmlDecode(Me.txtNombreArea.Text)
                .DescripcionCorta = HttpUtility.HtmlDecode(Me.txtNomCortoArea.Text)
                .Estado = "1"
            End With
            If ngcArea.InsertaArea(objArea) = True Then
            Else
                sc.mostrarMsjAlerta(Me, "Error al grabar el Area")
            End If
        ElseIf CStr(Indicador) = "U" Then
            If Me.chkEliminarArea.Checked = False Then
                With objArea
                    .Descripcion = HttpUtility.HtmlDecode(Me.txtNombreArea.Text)
                    .DescripcionCorta = HttpUtility.HtmlDecode(Me.txtNomCortoArea.Text)
                    .Estado = "1"
                    .Id = CInt(Session("IdAreaMant"))
                End With
                If ngcArea.ActualizaArea(objArea) = True Then
                Else
                    sc.mostrarMsjAlerta(Me, "Error al Actualizar el Area")
                End If
            ElseIf Me.chkEliminarArea.Checked = True Then
                With objArea
                    .Descripcion = HttpUtility.HtmlDecode(Me.txtNombreArea.Text)
                    .DescripcionCorta = HttpUtility.HtmlDecode(Me.txtNomCortoArea.Text)
                    .Estado = "0"
                    .Id = CInt(Session("IdAreaMant"))
                End With
                If ngcArea.ActualizaArea(objArea) = True Then
                Else
                    sc.mostrarMsjAlerta(Me, "Error al Eliminar el Area")
                End If
            End If
        End If
        HabilitarMantArea(False)
    End Sub
    Sub BuscarArea(ByVal grilla As GridView)
        Dim ngcArea As New Negocio.Area
        grilla.DataSource = ngcArea.SelectAllxNombreActivoInactivo(Me.txtBuscarArea.Text)
        grilla.DataBind()
    End Sub
    Private Sub btnBuscarArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarArea.Click
        BuscarArea(Me.dgvMantArea)
    End Sub
    Private Sub dgvMantArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMantArea.SelectedIndexChanged
        Try
            Session("IdAreaMant") = dgvMantArea.SelectedRow.Cells(4).Text
            Session("IndicadorArea") = "U"
            HabilitarMantArea(True)
            txtNombreArea.Text = HttpUtility.HtmlDecode(dgvMantArea.SelectedRow.Cells(1).Text)
            txtNomCortoArea.Text = HttpUtility.HtmlDecode(dgvMantArea.SelectedRow.Cells(2).Text)
            If dgvMantArea.SelectedRow.Cells(3).Text = "Activo" Then
                chkEliminarArea.Checked = False
            ElseIf dgvMantArea.SelectedRow.Cells(3).Text = "Inactivo" Then
                chkEliminarArea.Checked = True
            End If
            HabilitarBotonArea(False, True, False, True)
            Me.PanelBuscarArea.Visible = False : Me.PanelNuevoArea.Visible = True
        Catch ex As Exception
            HabilitarBotonArea(False, True, False, True)
            If dgvMantArea.SelectedRow.Cells(3).Text = "Activo" Then
                chkEliminarArea.Checked = False
            ElseIf dgvMantArea.SelectedRow.Cells(3).Text = "Inactivo" Then
                chkEliminarArea.Checked = True
            End If
            Me.PanelBuscarArea.Visible = False : Me.PanelNuevoArea.Visible = True
            HabilitarMantArea(True)
        Finally
        End Try
        chkEliminarArea.Visible = True
    End Sub
    'Private Sub cmb_MantTiendaT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmb_MantTiendaT.Click
    '    HabilitarMantTienda(False)
    '    Me.PanelNuevoT.Visible = False : Me.PanelBuscarT.Visible = True
    '    cbo.LLenarCboDepartamento(Me.cboDptoT)
    '    CboTipoTienda(Me.cboTipoT)

    '    Session("IndicadorTienda") = "I"
    'End Sub
    'Protected Sub cmb_MantTiendaTB_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmb_MantTiendaTB.Click
    '    'HabilitarBotonTienda(True, False, False, False)
    '    HabilitarBotonTienda(False, True, False, True)
    '    'HabilitarMantTienda(False)
    '    HabilitarMantTienda(True)
    '    LimpiarMantTienda()

    '    'HabilitarMantTienda(False)
    '    'Me.PanelNuevoT.Visible = False : Me.PanelBuscarT.Visible = True
    '    Me.PanelNuevoT.Visible = True : Me.PanelBuscarT.Visible = False
    '    cbo.LLenarCboDepartamento(Me.cboDptoT)
    '    CboTipoTienda(Me.cboTipoT)

    '    Session("IndicadorTienda") = "I"
    'End Sub

    'Private Sub cmd_MantAreaTienda_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_MantAreaTienda.Click
    '    Me.PanelNuevoArea.Visible = False : Me.PanelBuscarArea.Visible = True
    '    Session("IndicadorArea") = "I"

    '    HabilitarMantArea(False)
    'End Sub
    'Protected Sub cmd_MantAreaTiendaB_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_MantAreaTiendaB.Click
    '    'Me.PanelNuevoArea.Visible = False : Me.PanelBuscarArea.Visible = True
    '    Me.PanelNuevoArea.Visible = True : Me.PanelBuscarArea.Visible = False
    '    Session("IndicadorArea") = "I"

    '    'HabilitarBotonArea(True, False, False, False)
    '    HabilitarBotonArea(False, True, False, True)
    '    'HabilitarMantArea(False)
    '    HabilitarMantArea(True)
    '    LimpiarMantArea()
    'End Sub

    Sub HabilitarBotonArea(ByVal ActivarNuevo As Boolean, ByVal ActivarGuardar As Boolean, ByVal ActivarEditar As Boolean, ByVal ActivarCancelar As Boolean)
        Me.btnNuevoArea.Visible = ActivarNuevo
        Me.btnGrabarArea.Visible = ActivarGuardar
        Me.btnEditarArea.Visible = ActivarEditar
        Me.btnCancelarMantArea.Visible = ActivarCancelar
    End Sub
    Private Sub btnCancelarMantArea_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelarMantArea.Click
        HabilitarBotonArea(True, False, False, True)
        LimpiarMantArea()
        Me.PanelBuscarArea.Visible = True : Me.PanelNuevoArea.Visible = False
    End Sub
    Private Sub chkEliminarArea_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEliminarArea.CheckedChanged
        If Me.chkEliminarArea.Checked = True Then
            HabilitarMantArea(False)
            Me.chkEliminarArea.Enabled = True
        ElseIf Me.chkEliminarArea.Checked = False Then
            HabilitarMantArea(True)
            Me.chkEliminarArea.Enabled = True
        End If
    End Sub
    ''' MANTENIMIENTO ALMACEN
    Private Sub cargarDatosCboAreaMant(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Area

        Dim objarea As New Entidades.Area
        Dim listaArea As New List(Of Entidades.Area)

        For Each objarea In obj.SelectAllActivo
            objarea.DescripcionCorta = HttpUtility.HtmlDecode(objarea.DescripcionCorta)
            listaArea.Add(objarea)
        Next
        'cbo.DataSource = obj.SelectAllActivo
        cbo.DataSource = listaArea
        cbo.DataTextField = "DescripcionCorta"
        cbo.DataValueField = "Id"
        cbo.DataBind()
    End Sub
    Sub LimpiarMantAlmacen()
        Try
            Me.txtNomAlma.Text = ""
            Me.cboAreaMantAlma.SelectedIndex = -1
            Me.txtDireccionAlma.Text = ""
            Me.txtReferenciaAlma.Text = ""
            Me.cboDptoAlma.SelectedIndex = -1
            Me.cboProvAlma.SelectedIndex = -1
            Me.cboDistAlma.SelectedIndex = -1
            Me.chkEliminaAlma.Checked = False
            Me.txtBuscaNomMantAlmacen.Text = ""
            Me.dgvMantAlmacen.DataBind()
        Catch ex As Exception
        Finally
        End Try
    End Sub
    Sub HabilitarMantAlmacen(ByVal Activar As Boolean)
        Me.txtNomAlma.Enabled = Activar
        Me.cboAreaMantAlma.Enabled = Activar
        Me.txtDireccionAlma.Enabled = Activar
        Me.txtReferenciaAlma.Enabled = Activar
        Me.cboDptoAlma.Enabled = Activar
        Me.cboProvAlma.Enabled = Activar
        Me.cboDistAlma.Enabled = Activar
        Me.chkEliminaAlma.Enabled = Activar
    End Sub
    'Protected Sub cmd_MantAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_MantAlmacen.Click
    '    HabilitarBotonAlmacen(True, False, False, False)
    '    HabilitarMantAlmacen(False)
    '    LimpiarMantAlmacen()
    '    Me.PanelNuevoAlmacen.Visible = False : Me.PanelBuscaAlmacen.Visible = True
    '    cbo.LLenarCboDepartamento(Me.cboDptoAlma)
    '    cargarDatosCboAreaMant(Me.cboAreaMantAlma)
    '    Session("IndicadorAlmacen") = "I"
    'End Sub

    'Protected Sub cmd_MantAlmacenA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_MantAlmacenA.Click
    '    'HabilitarBotonAlmacen(True, False, False, False)
    '    HabilitarBotonAlmacen(False, True, False, True)
    '    'HabilitarMantAlmacen(False)
    '    HabilitarMantAlmacen(True)
    '    LimpiarMantAlmacen()
    '    'Me.PanelNuevoAlmacen.Visible = False : Me.PanelBuscaAlmacen.Visible = True
    '    Me.PanelNuevoAlmacen.Visible = True : Me.PanelBuscaAlmacen.Visible = False
    '    cbo.LLenarCboDepartamento(Me.cboDptoAlma)
    '    cargarDatosCboAreaMant(Me.cboAreaMantAlma)
    '    Session("IndicadorAlmacen") = "I"
    'End Sub

    Private Sub btnMantAlmaNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMantAlmaNuevo.Click
        HabilitarBotonAlmacen(False, True, False, True)
        LimpiarMantAlmacen()
        Session("IndicadorAlmacen") = "I"
        Me.PanelBuscaAlmacen.Visible = False : Me.PanelNuevoAlmacen.Visible = True
        HabilitarMantAlmacen(True)
        chkEliminaAlma.Visible = False
    End Sub
    Private Sub btnMantAlmaGraba_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMantAlmaGraba.Click
        If Me.txtNomAlma.Text = "" Then : Exit Sub : End If
        If Me.cboAreaMantAlma.SelectedIndex = -1 Then : Exit Sub : End If
        If Me.cboDptoAlma.SelectedIndex = 0 Then : Exit Sub : End If
        If Me.cboProvAlma.SelectedIndex = 0 Then : Exit Sub : End If
        If Me.cboDistAlma.SelectedIndex = -0 Then : Exit Sub : End If
        If Me.txtDireccionAlma.Text = "" Then : Exit Sub : End If
        GrabarAlmacen(CStr(Session("IndicadorAlmacen")))
        LimpiarMantAlmacen()
        HabilitarMantAlmacen(False)
        HabilitarBotonAlmacen(True, False, False, False)
        Me.PanelBuscaAlmacen.Visible = True : Me.PanelNuevoAlmacen.Visible = False
        cargarDatosCboAlmacen(Me.cboAlmacen)
    End Sub
    Sub GrabarAlmacen(ByVal Indicador As String)
        Dim ngcAlmacen As New Negocio.Almacen
        Dim objAlmacen As New Entidades.Almacen
        If CStr(Indicador) = "I" Then
            With objAlmacen
                .Nombre = HttpUtility.HtmlDecode(Me.txtNomAlma.Text)
                .Ubigeo = Me.cboDptoAlma.SelectedValue + Me.cboProvAlma.SelectedValue + Me.cboDistAlma.SelectedValue
                .Direccion = HttpUtility.HtmlDecode(Me.txtDireccionAlma.Text)
                .Referencia = HttpUtility.HtmlDecode(Me.txtReferenciaAlma.Text)
                .Estado = "1"
                .Area = CInt(Me.cboAreaMantAlma.SelectedValue)
            End With
            If ngcAlmacen.InsertaAlmacen(objAlmacen) = True Then
                'sc.mostrarMsjAlerta(Me, "Se grabarón correctamente los datos")
            Else
                sc.mostrarMsjAlerta(Me, "Error al grabar la Almacen")
            End If
        ElseIf CStr(Indicador) = "U" Then
            If Me.chkEliminaAlma.Checked = False Then
                With objAlmacen
                    .Nombre = HttpUtility.HtmlDecode(Me.txtNomAlma.Text)
                    .Ubigeo = Me.cboDptoAlma.SelectedValue + Me.cboProvAlma.SelectedValue + Me.cboDistAlma.SelectedValue
                    .Direccion = HttpUtility.HtmlDecode(Me.txtDireccionAlma.Text)
                    .Referencia = HttpUtility.HtmlDecode(Me.txtReferenciaAlma.Text)
                    .Estado = "1"
                    .Area = CInt(Me.cboAreaMantAlma.SelectedValue)
                    .IdAlmacen = CInt(Session("IdAlmacenM"))
                End With
                If ngcAlmacen.ActualizaAlmacen(objAlmacen) = True Then
                    'sc.mostrarMsjAlerta(Me, "Se Actualizo correctamente los datos")
                Else
                    sc.mostrarMsjAlerta(Me, "Error al Actualizar la Almacen")
                End If
            ElseIf Me.chkEliminaAlma.Checked = True Then
                With objAlmacen
                    .Nombre = HttpUtility.HtmlDecode(Me.txtNomAlma.Text)
                    .Ubigeo = Me.cboDptoAlma.SelectedValue + Me.cboProvAlma.SelectedValue + Me.cboDistAlma.SelectedValue
                    .Direccion = HttpUtility.HtmlDecode(Me.txtDireccionAlma.Text)
                    .Referencia = HttpUtility.HtmlDecode(Me.txtReferenciaAlma.Text)
                    .Estado = "0"
                    .Area = CInt(Me.cboAreaMantAlma.SelectedValue)
                    .IdAlmacen = CInt(Session("IdAlmacenM"))
                End With
                If ngcAlmacen.ActualizaAlmacen(objAlmacen) = True Then
                    'sc.mostrarMsjAlerta(Me, "Se Elimino correctamente los datos")
                Else
                    sc.mostrarMsjAlerta(Me, "Error al Eliminar la Almacen")
                End If
            End If
        End If
        HabilitarMantAlmacen(False)
    End Sub
    Private Sub btnMantAlmaEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMantAlmaEditar.Click
        Me.PanelBuscaAlmacen.Visible = True : Me.PanelNuevoAlmacen.Visible = False
        Session("IndicadorAlmacen") = "U"
        LimpiarMantAlmacen()
        HabilitarMantAlmacen(True)
    End Sub
    Private Sub btnBuscarMantAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarMantAlmacen.Click
        BuscarAlmacen(Me.dgvMantAlmacen)
    End Sub
    Sub BuscarAlmacen(ByVal grilla As GridView)
        Dim ngcAlmacen As New Negocio.Almacen
        grilla.DataSource = ngcAlmacen.SelectGrillaActivoInactivoxNombre(Me.txtBuscaNomMantAlmacen.Text)
        grilla.DataBind()
    End Sub
    Private Sub cboDptoAlma_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDptoAlma.SelectedIndexChanged
        cbo.LLenarCboProvincia(Me.cboProvAlma, Me.cboDptoAlma.SelectedValue)
    End Sub
    Private Sub cboProvAlma_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProvAlma.SelectedIndexChanged
        cbo.LLenarCboDistrito(Me.cboDistAlma, Me.cboDptoAlma.SelectedValue, Me.cboProvAlma.SelectedValue)
    End Sub
    Private Sub dgvMantAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMantAlmacen.SelectedIndexChanged
        Try
            Session("IdAlmacenM") = dgvMantAlmacen.SelectedRow.Cells(9).Text
            Session("IndicadorAlmacen") = "U"
            HabilitarMantAlmacen(True)
            txtNomAlma.Text = HttpUtility.HtmlDecode(dgvMantAlmacen.SelectedRow.Cells(1).Text)
            Me.cboDptoAlma.SelectedValue = HttpUtility.HtmlDecode(Left(dgvMantAlmacen.SelectedRow.Cells(10).Text, 2))
            cbo.LLenarCboProvincia(Me.cboProvAlma, Left(dgvMantAlmacen.SelectedRow.Cells(10).Text, 2))
            Me.cboProvAlma.SelectedValue = Mid(dgvMantAlmacen.SelectedRow.Cells(10).Text, 3, 2)
            cbo.LLenarCboDistrito(Me.cboDistAlma, Left(dgvMantAlmacen.SelectedRow.Cells(10).Text, 2), Mid(dgvMantAlmacen.SelectedRow.Cells(10).Text, 3, 2))
            Me.cboDistAlma.SelectedValue = Mid(dgvMantAlmacen.SelectedRow.Cells(10).Text, 5, 2)

            txtDireccionAlma.Text = HttpUtility.HtmlDecode(dgvMantAlmacen.SelectedRow.Cells(5).Text)
            txtReferenciaAlma.Text = HttpUtility.HtmlDecode(dgvMantAlmacen.SelectedRow.Cells(6).Text)
            cboAreaMantAlma.Text = HttpUtility.HtmlDecode(dgvMantAlmacen.SelectedRow.Cells(11).Text)
            If dgvMantAlmacen.SelectedRow.Cells(8).Text = "Activo" Then
                chkEliminaAlma.Checked = False
            ElseIf dgvMantAlmacen.SelectedRow.Cells(8).Text = "Inactivo" Then
                chkEliminaAlma.Checked = True
            End If
            HabilitarBotonAlmacen(False, True, False, True)
            Me.PanelBuscaAlmacen.Visible = False : Me.PanelNuevoAlmacen.Visible = True
        Catch ex As Exception
            HabilitarBotonAlmacen(False, True, False, True)
            If dgvMantAlmacen.SelectedRow.Cells(8).Text = "Activo" Then
                chkEliminaAlma.Checked = False
            ElseIf dgvMantAlmacen.SelectedRow.Cells(8).Text = "Inactivo" Then
                chkEliminaAlma.Checked = True
            End If
            Me.PanelBuscaAlmacen.Visible = False : Me.PanelNuevoAlmacen.Visible = True
        Finally
        End Try
        Me.chkEliminaAlma.Visible = True
    End Sub
    'Private Sub cmd_VerMantAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmd_VerMantAlmacen.Click
    '    Me.PanelNuevoAlmacen.Visible = False : Me.PanelBuscaAlmacen.Visible = True
    '    cbo.LLenarCboDepartamento(Me.cboDptoAlma)
    '    cargarDatosCboAreaMant(Me.cboAreaMantAlma)

    '    Session("IndicadorAlmacen") = "I"
    'End Sub

    'Protected Sub cmd_VerMantAlmacenB_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmd_VerMantAlmacenB.Click
    '    'HabilitarBotonAlmacen(True, False, False, False)
    '    HabilitarBotonAlmacen(False, True, False, True)
    '    'HabilitarMantAlmacen(False)
    '    HabilitarMantAlmacen(True)
    '    LimpiarMantAlmacen()

    '    'Me.PanelNuevoAlmacen.Visible = False : Me.PanelBuscaAlmacen.Visible = True
    '    Me.PanelNuevoAlmacen.Visible = True : Me.PanelBuscaAlmacen.Visible = False
    '    cbo.LLenarCboDepartamento(Me.cboDptoAlma)
    '    cargarDatosCboAreaMant(Me.cboAreaMantAlma)

    '    Session("IndicadorAlmacen") = "I"
    'End Sub


    Sub HabilitarBotonAlmacen(ByVal ActivarNuevo As Boolean, ByVal ActivarGuardar As Boolean, ByVal ActivarEditar As Boolean, ByVal ActivarCancelar As Boolean)
        Me.btnMantAlmaNuevo.Visible = ActivarNuevo
        Me.btnMantAlmaGraba.Visible = ActivarGuardar
        Me.btnMantAlmaEditar.Visible = ActivarEditar
        Me.btnCancelarMantAlmacen.Visible = ActivarCancelar
    End Sub
    Private Sub btnCancelarMantAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelarMantAlmacen.Click
        HabilitarBotonAlmacen(True, False, False, True)
        LimpiarMantAlmacen()
        Me.PanelBuscaAlmacen.Visible = True : Me.PanelNuevoAlmacen.Visible = False
    End Sub
    Private Sub chkEliminaAlma_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEliminaAlma.CheckedChanged
        If Me.chkEliminaAlma.Checked = True Then
            HabilitarMantAlmacen(False)
            Me.chkEliminaAlma.Enabled = True
        ElseIf Me.chkEliminaAlma.Checked = False Then
            HabilitarMantAlmacen(True)
            Me.chkEliminaAlma.Enabled = True
        End If
    End Sub

#Region "centro de costo"

    Private Function getCentroCosto() As String
        Dim ID As String = String.Empty
        ID = dlunidadnegocio.SelectedValue + dldptofuncional.SelectedValue
        ID &= dlsubarea1.SelectedValue + dlsubarea2.SelectedValue + dlsubarea3.SelectedValue
        Return ID
    End Function

    Protected Sub dlunidadnegocio_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlunidadnegocio.SelectedIndexChanged
        selectedValueUnidadNegocio()
    End Sub

    Private Sub selectedValueUnidadNegocio()
        If dlunidadnegocio.SelectedValue <> "00" Then
            cbo.LlenarCboCod_DepFunc(dldptofuncional, dlunidadnegocio.SelectedValue, True)
        Else
            dldptofuncional.Items.Clear() : dldptofuncional.Items.Insert(0, New ListItem("------", "00"))
        End If
        dlsubarea1.Items.Clear() : dlsubarea1.Items.Insert(0, New ListItem("------", "00"))
        dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
        dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
    End Sub

    Protected Sub dldptofuncional_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dldptofuncional.SelectedIndexChanged

        If dldptofuncional.SelectedValue <> "00" Then
            cbo.LlenarCboCod_SubCodigo2(dlsubarea1, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, True)
        Else
            dlsubarea1.Items.Clear() : dlsubarea1.Items.Insert(0, New ListItem("------", "00"))
        End If
        dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
        dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
    End Sub

    Protected Sub dlsubarea1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlsubarea1.SelectedIndexChanged
        If dlsubarea1.SelectedValue <> "00" Then
            cbo.LlenarCboCod_SubCodigo3(dlsubarea2, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, dlsubarea1.SelectedValue, True)
        Else
            dlsubarea2.Items.Clear() : dlsubarea2.Items.Insert(0, New ListItem("------", "00"))
        End If
        dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
    End Sub

    Protected Sub dlsubarea2_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dlsubarea2.SelectedIndexChanged
        If dlsubarea2.SelectedValue <> "00" Then
            cbo.LlenarCboCod_SubCodigo4(dlsubarea3, dlunidadnegocio.SelectedValue, dldptofuncional.SelectedValue, dlsubarea1.SelectedValue, dlsubarea2.SelectedValue, True)
        Else
            dlsubarea3.Items.Clear() : dlsubarea3.Items.Insert(0, New ListItem("------", "00"))
        End If
    End Sub

    Protected Sub imgVerCentroCosto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim row As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
        Dim hdd As HiddenField = CType(row.Cells(2).FindControl("hdd_idcentrocosto"), HiddenField)
        dlunidadnegocio.SelectedValue = hdd.Value.Substring(0, 2) : dlunidadnegocio_SelectedIndexChanged(sender, e)
        dldptofuncional.SelectedValue = hdd.Value.Substring(2, 2) : dldptofuncional_SelectedIndexChanged(sender, e)
        dlsubarea1.SelectedValue = hdd.Value.Substring(4, 2) : dlsubarea1_SelectedIndexChanged(sender, e)
        dlsubarea2.SelectedValue = hdd.Value.Substring(6, 2) : dlsubarea2_SelectedIndexChanged(sender, e)
        dlsubarea3.SelectedValue = hdd.Value.Substring(8, 2)
    End Sub


#End Region

    
 
End Class