﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmOrdenarProductos
    Inherits System.Web.UI.Page
    Private listaProductos As List(Of Entidades.Producto)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub
#Region "MANEJO DEL SESSION CON OBJETOS"
    Private Sub setListaProductos(ByVal lista As List(Of Entidades.Producto))
        Session.Remove("listaProductos")
        Session.Add("listaProductos", lista)
    End Sub
    Private Function getListaProductos() As List(Of Entidades.Producto)
        Return CType(Session.Item("listaProductos"), List(Of Entidades.Producto))
    End Function
    Private Sub saveListaProductos()
        listaProductos = getListaProductos()
        For i As Integer = 0 To DGV_ListaProductos.Rows.Count - 1
            With listaProductos.Item(i)
                .Orden = CInt(CType(DGV_ListaProductos.Rows(i).Cells(2).FindControl("txtNroOrden"), TextBox).Text)
            End With
        Next
        setListaProductos(listaProductos)
    End Sub
#End Region
    Private Sub inicializarFrm()
        Dim objScript As New ScriptManagerClass
        Try
            cargarDatosLinea(Me.cmbLinea)
            cargarDatosSubLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue))
            verFrmInicio()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Ocurrieron problemas en la carga de datos Línea/SubLínea")
        End Try
    End Sub
    Private Sub verFrmInicio()
        Panel_Cabecera.Enabled = True
        Panel_detalle.Visible = False
    End Sub
    Private Sub verFrmNuevo()
        Panel_Cabecera.Enabled = False
        Panel_detalle.Visible = True
    End Sub
    Private Sub cargarDatosLinea(ByVal combo As DropDownList)
        Dim nLinea As New Negocio.Linea
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo()
        combo.DataSource = lista
        combo.DataBind()
    End Sub
    Private Sub cargarDatosSubLinea(ByVal combo As DropDownList, ByVal idLinea As Integer)
        Dim nSubLinea As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectAllActivoxLinea(idLinea)
        combo.DataSource = lista
        combo.DataBind()
    End Sub
    Protected Sub btnAtras_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAtras.Click
        verFrmInicio()
    End Sub

    Protected Sub btnAceptar_SubLinea_Cab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar_SubLinea_Cab.Click
        cargarDatosProductosxIdSubLinea()
    End Sub

    Private Sub cargarDatosProductosxIdSubLinea()
        Dim objScript As New ScriptManagerClass
        Try
            Dim objNProducto As New Negocio.Producto
            Me.listaProductos = objNProducto.SelectOrdenxIdSubLinea(CInt(cmbSubLinea.SelectedValue))
            DGV_ListaProductos.DataSource = listaProductos
            DGV_ListaProductos.DataBind()
            If DGV_ListaProductos.Rows.Count = 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron productos.")
            End If
            setListaProductos(Me.listaProductos)
            verFrmNuevo()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Ocurrieron problemas en la carga de los productos x Sub línea")
        End Try
    End Sub
    Protected Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLinea.SelectedIndexChanged
        cargarDatosSubLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue))
    End Sub
    Protected Sub btnOrdenar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOrdenar.Click
        Dim objscript As New ScriptManagerClass
        If ordenarlista() Then
            objscript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
        Else
            objscript.mostrarMsjAlerta(Me, "Ocurrieron problemas en el proceso de ordenamiento.")
        End If
    End Sub
    Private Function ordenarlista() As Boolean
        Try
            saveListaProductos()
            Me.listaProductos = Me.getListaProductos
            For i As Integer = 0 To listaProductos.Count - 2
                For j As Integer = (i + 1) To listaProductos.Count - 1
                    With listaProductos
                        If .Item(i).Orden > .Item(j).Orden Then
                            Dim aux As Entidades.Producto = .Item(j)
                            .Item(j) = .Item(i)
                            .Item(i) = aux
                        End If
                    End With
                Next
            Next
            Dim cont As Integer = 0
            For i As Integer = 0 To listaProductos.Count - 1
                With listaProductos
                    If .Item(i).Orden <> 0 Then
                        cont = cont + 1
                        .Item(i).Orden = (cont)
                    End If
                End With
            Next
            setListaProductos(Me.listaProductos)
            DGV_ListaProductos.DataSource = listaProductos
            DGV_ListaProductos.DataBind()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        registrarOrdenamiento()
    End Sub
    Private Sub registrarOrdenamiento()
        Dim objScrip As New ScriptManagerClass
        Try
            If Not ordenarlista() Then
                Throw New Exception
            End If
            Dim objNProducto As New Negocio.Producto
            Me.listaProductos = getListaProductos()
            If objNProducto.UpdateOrdenProducto(Me.listaProductos) Then
                objScrip.mostrarMsjAlerta(Me, "La operación finalizó con éxito.")
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            objScrip.mostrarMsjAlerta(Me, "Ocurrieron problemas en la operación.")
        End Try
    End Sub
End Class