﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frmDocumentoPicking.aspx.vb" Inherits="APPWEB.frmDocumentoPicking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr style="background-color: Yellow">
            <td style="width: 350px">
                <asp:LinkButton CssClass="Label" Font-Bold="true" ID="lkb_CapaConfigFrm" OnClientClick="return(onCapa('capaConfiguracionFrm'));"
                    runat="server">Config. Formulario</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" ToolTip="Nuevo" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                                Text="Editar" ToolTip="Editar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                                Text="Anular" ToolTip="Anular" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" ToolTip="Guardar" OnClientClick=" return(     valSaveDocumento()       ); "
                                Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir1" runat="server" OnClientClick="return(  valOnClick_btnImprimir(1)  );"
                                Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  valOnClick_btnImprimir(2)  );"
                                Text="Imprimir Estandar" ToolTip="Imprimir Documento" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimirTonos" runat="server" OnClientClick="return(  valOnClick_btnImprimir(3)  );"
                                Text="Imprimir Tonos" ToolTip="Imprimir Documento" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscarDocumentoRef" runat="server" Text="Despachos" OnClientClick="  return(  onCapa('capaDocumentosReferencia')  );  "
                                ToolTip="Buscar Despachos Pendientes" Width="90px" />
                        </td>
                        <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                GUÍA DE REMISIÓN.
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarDocumentoxCodigo">
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( valOnKeyPressCodigoDoc(this,event)  );"
                                    ReadOnly="true" Width="100px"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" ToolTip="Buscar Documento por [ Serie - Número ]." />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="Búsqueda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisión:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Inicio Traslado:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaInicioTraslado" runat="server" CssClass="TextBox_Fecha"
                                    onblur="return(  valFecha(this) );" Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaInicioTraslado" runat="server"
                                    ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                    CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicioTraslado">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaInicioTraslado" runat="server"
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaInicioTraslado">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td align="right" class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Almacén:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboAlmacen" runat="server" Width="100%" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Operación:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" runat="server" Visible="true" AutoPostBack="true"
                                    Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Motivo Traslado:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboMotivoTraslado" runat="server" Width="100%" AutoPostBack="false">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="imgDocumentoRef" runat="server" />&nbsp;DOCUMENTO DE REFERENCIA
                            <cc1:CollapsiblePanelExtender ID="cpeDocumentoRef" runat="server" TargetControlID="Panel_DocumentoRef"
                                ImageControlID="imgDocumentoRef" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                Collapsed="true" CollapseControlID="imgDocumentoRef" ExpandControlID="imgDocumentoRef">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_DocumentoRef" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomTipoDocumento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red"
                                                        HeaderText="Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                                    <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                                        <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
 
                                                                        <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />

                                                                        <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                                        <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />

                                                                        <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoDocumento")%>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomAlmacen" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"
                                                        HeaderText="Almacén" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                                        <asp:ImageButton ID="imgVerDetalle" runat="server" OnClick="OnClick_imgVerDetalle"
                                                                            ImageUrl="~/Imagenes/search_add.ico" Width="25px" Height="25px" />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_DocumentoRef_Detalle" runat="server" AutoGenerateColumns="False"
                                                Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:BoundField DataField="CodigoProducto" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Cód." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                  
                                                  <asp:BoundField DataField="NroDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Cód." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />                                                 
                                                    <asp:BoundField DataField="NomProducto" ItemStyle-Width="400px" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Producto" ItemStyle-Font-Bold="true"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="UMedida" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="U.M." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="PrecioCD" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="P. Venta" ItemStyle-Font-Bold="true"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" Visible="false" />
                                                    <asp:BoundField DataField="Cantidad" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Cantidad" ItemStyle-Font-Bold="true"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="CantxAtender" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Pendiente" ItemStyle-Font-Bold="true"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="CantidadTransito" DataFormatString="{0:F3}" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Cant. Transito" ItemStyle-Font-Bold="true"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                REMITENTE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Remitente" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="right" style="text-align: left">
                                <table>
                                    <tr>
                                        <td class="Texto" align="right">
                                            Ap. y Nombres/Razón Social:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtNombre_Remitente" runat="server" Width="490px" ReadOnly="true"
                                                            CssClass="TextBoxReadOnly"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtIdRemitente" runat="server" CssClass="TextBoxReadOnly" onkeypress="return(false);"
                                                            Width="75px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarRemitente" runat="server" Text="Buscar" Width="80px" OnClientClick="return(  valOnClick_btnBuscarRemitente()  );" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" align="right">
                                            D.N.I.:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDNI_Remitente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="183px"></asp:TextBox>
                                                    </td>
                                                    <td align="right" class="Texto">
                                                        R.U.C.:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtRUC_Remitente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                            Width="202px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                PUNTO PARTIDA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_PuntoPartida" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Dirección:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDireccion_Partida" runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" Width="650px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                Depto.:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cboDepto_Partida" Enabled="true" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" align="right">
                                            Provincia:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboProvincia_Partida" Enabled="true" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" align="right">
                                            Distrito:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboDistrito_Partida" Enabled="true" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DESTINATARIO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Destinatario" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="right" style="text-align: left">
                                <table>
                                    <tr>
                                        <td class="Texto" align="right">
                                            Ap. y Nombres/Razón Social:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDestinatario" runat="server" Width="490px" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtIdDestinatario" runat="server" CssClass="TextBoxReadOnly" onkeypress="return(false);"
                                                            Width="75px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarDestinatario" runat="server" Text="Buscar" Width="80px"
                                                            OnClientClick=" return(   valOnClick_btnBuscarDestinatario()   ); " />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" align="right">
                                            Tipo de Almacén:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="cboTipoAlmacen" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Almacén:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboAlmacen_Destinatario" runat="server" AutoPostBack="true"
                                                            Enabled="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" align="right">
                                            D.N.I.:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDni_Destinatario" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="183px"></asp:TextBox>
                                                    </td>
                                                    <td class="Texto" align="right">
                                                        R.U.C.:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtRuc_Destinatario" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                            Width="202px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                PUNTO LLEGADA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_PuntoLlegada" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Dirección:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDireccion_Llegada" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" runat="server"
                                    Width="650px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                Depto.:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cboDepto_Llegada" Enabled="true" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" align="right">
                                            Provincia:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboProvincia_Llegada" Enabled="true" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" align="right">
                                            Distrito:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboDistrito_Llegada" Enabled="true" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DETALLE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Detalle" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                                                OnClientClick="return(valOnClick_btnBuscarProducto());" onmouseout="this.src='../Imagenes/BuscarProducto_b.JPG';"
                                                onmouseover="this.src='../Imagenes/BuscarProducto_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiarDetalleDocumento" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';" />
                                        </td>
                                        
                                          <td>
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="LblcantidadRegistros" runat="server" ForeColor="#0033CC"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%" ViewStateMode="Enabled">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                <asp:LinkButton CssClass="Texto" ID="lkbQuitar" Font-Bold="true" runat="server" OnClick="lkbQuitar_Click">Quitar</asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" Width="70px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CodigoProducto" HeaderText="Código" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="NroDocumento" HeaderText="NroDocumento" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="NomProducto" HeaderText="Descripción" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="U. Medida" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboUnidadMedida" runat="server" DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaUM")%>'
                                                    DataValueField="IdUnidadMedida" DataTextField="NombreCortoUM" OnSelectedIndexChanged="valOnChange_cboUnidadMedida"
                                                    AutoPostBack="true" onclick="return(  calcularPesoTotalmasdeUnaCaja();calcularPesoTotal(0)  );">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tono Disp." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemStyle VerticalAlign="NotSet" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBoxList ID="checkListTonos" runat="server" DataSource='<%#DataBinder.Eval(Container.DataItem,"listaTonos")%>'
                                                DataValueField="idTono" DataTextField="nomtono" onclick="return(  calcularPesoTotalmasdeUnaCaja()  );"></asp:CheckBoxList>
                                                <%--<asp:DropDownList ID="cboTonoDisponible" runat="server" DataSource='<%#DataBinder.Eval(Container.DataItem,"listaTonos")%>'
                                                    DataValueField="idTono" DataTextField="nomtono" AutoPostBack="true" OnSelectedIndexChanged="valOnChange_cboTonoDisponible">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
<%--                                            <asp:TextBox ID="txtCantidad" runat="server" onblur="return(valBlur(event));" TabIndex="500"
                                                                onKeypress="return(validarNumeroPunto(event));" onfocus="return(aceptarFoco(this));"
                                                                onKeyUp=" return(  calcularPesoTotal('0',this)  ); " Width="90px" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:G29}")%>'></asp:TextBox>--%>
                                            <%--<asp:Label ID="txtCantidadTono" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cantidadTono","{0:G29}")%>'></asp:Label>
                                            <asp:Label ID="lblUnidadMedidaTono" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedidaPrincipal")%>'></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                               <%--<asp:TextBox ID="txtCantidad" runat="server" onblur="return(valBlur(event));" TabIndex="500"
                                                onKeypress="return(validarNumeroPunto(event));" onfocus="return(aceptarFoco(this));"
                                                onKeyUp=" return(  calcularPesoTotalmasdeUnaCaja('0',this,'0')  ); " Width="80px" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:G29}")%>'></asp:TextBox>--%>
                                                <asp:PlaceHolder ID="phcantidades" runat="server"></asp:PlaceHolder>
                                                <asp:HiddenField ID="hddIdDetalleAfecto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDetalleDocumento","{0:F2}")%>' />

                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>' />

                                                <asp:HiddenField ID="HddIdProd" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto","{0:F2}")%>' />

                                                <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />

                                                <asp:HiddenField ID="hddCantidadFinal" runat="server" Value="0" />

                                                <asp:HiddenField ID="hddCantidadTecleadas" runat="server" Value="0" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CantidadxAtenderText" HeaderText="Pendiente" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true"
                                            ItemStyle-ForeColor="Red" />
                                        <asp:TemplateField HeaderText="Peso" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
  
                                                            <asp:TextBox ID="txtPeso" TabIndex="310" Width="70px" Font-Bold="true" runat="server"
                                                                Enabled="false" Text='<%#DataBinder.Eval(Container.DataItem,"Peso","{0:F3}")%>'
                                                                onKeyPress="return( validarNumeroPuntoPositivo('event') );" onblur=" return( valBlur(event) ); "
                                                                onFocus=" return( aceptarFoco(this) ); " onKeyUp=" return(  calcularPesoTotal('1')  ); "></asp:TextBox>

                                                            <asp:DropDownList ID="cboUnidadMedida_Peso" runat="server" Enabled="false" DataValueField="IdUnidadMedida"
                                                                DataTextField="NombreCortoUM">
                                                            </asp:DropDownList>

                                                            <asp:HiddenField ID="hddEquivalencia" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Equivalencia","{0:F6}")%>' />

                                                            <asp:HiddenField ID="hddEquivalencia1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Equivalencia1","{0:F6}")%>' />

                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="130px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>

                                                            <asp:ImageButton ID="btnEquivalencia_PR" runat="server" ToolTip="Calcular Equivalencia"
                                                                ImageUrl="~/Imagenes/Ok_b.bmp" OnClientClick="return(    valOnClick_btnEquivalencia_PR(this)    );"
                                                                OnClick="btnEquivalencia_PR_Click" Visible="false" />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Stock Real" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblStockReal" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Stock","{0:G29}")%>'></asp:Label>

                                                            <asp:Label ID="lblUnidadMedida" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedidaPrincipal")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comprometido" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblComprometido" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StockComprometido","{0:F4}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Disponible" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblDisponible" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StockDisponible","{0:F4}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold">
                                            Peso Total:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPesoTotal" Width="70px" CssClass="TextBox_ReadOnlyLeft" onKeyPress=" return( false ); "
                                                runat="server" Text="0"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboUnidadMedida_PesoTotal" runat="server" Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="Image_Transportista" runat="server" />&nbsp;TRANSPORTISTA
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_Transportista" runat="server"
                                TargetControlID="Panel_Transportista" ImageControlID="Image_Transportista" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" Collapsed="true" CollapseControlID="Image_Transportista"
                                ExpandControlID="Image_Transportista">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Transportista" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" align="right">
                                                        Transportista:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTransportista" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="450px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarTransportista" runat="server" Text="Buscar" Width="80px"
                                                            OnClientClick="return(  valOnClick_btnBuscarTransportista()  );" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnLimpiarTransportista" runat="server" Text="Limpiar" Width="80px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="Texto">
                                                        D.N.I.:
                                                    </td>
                                                    <td colspan="3">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtDni_Transportista" runat="server" CssClass="TextBoxReadOnly"
                                                                        ReadOnly="True" Width="150px"></asp:TextBox>
                                                                </td>
                                                                <td align="right" class="Texto">
                                                                    R.U.C.:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtRuc_Transportista" runat="server" CssClass="TextBoxReadOnly"
                                                                        ReadOnly="True" Width="150px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="Texto">
                                                        Dirección:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDireccion_Transportista" runat="server" CssClass="TextBoxReadOnly"
                                                            ReadOnly="True" Width="450px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="Image_Chofer" runat="server" />&nbsp;CHOFER
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_Chofer" runat="server"
                                TargetControlID="Panel_Chofer" ImageControlID="Image_Chofer" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" Collapsed="true" CollapseControlID="Image_Chofer"
                                ExpandControlID="Image_Chofer">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Chofer" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" align="right">
                                                        Chofer:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtChofer" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="520px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarChofer" runat="server" Text="Buscar" Width="80px" OnClientClick="return(  valOnClick_btnBuscarChofer()  );" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnLimpiarChofer" runat="server" Text="Limpiar" Width="80px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="Texto">
                                                        D.N.I.:
                                                    </td>
                                                    <td colspan="3">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtDni_Chofer" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                        Width="120px"></asp:TextBox>
                                                                </td>
                                                                <td align="right" class="Texto">
                                                                    R.U.C.:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtRuc_Chofer" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                        Width="120px"></asp:TextBox>
                                                                </td>
                                                                <td align="right" class="Texto">
                                                                    Nro. Licencia:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtNroLicencia" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                        Width="120px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="Image_Vehiculo" runat="server" />&nbsp;VEHÍCULO
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_Vehiculo" runat="server"
                                TargetControlID="Panel_Vehiculo" ImageControlID="Image_Vehiculo" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" Collapsed="true" CollapseControlID="Image_Vehiculo"
                                ExpandControlID="Image_Vehiculo">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Vehiculo" runat="server" Width="100%">
                                <table>
                                    <tr>
                                        <td align="right" class="Texto">
                                            Modelo / Marca:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtModelo_Vehiculo" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="200px"></asp:TextBox>
                                                    </td>
                                                    <td align="right" class="Texto">
                                                        Nro. Placa:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPlaca_Vehiculo" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="120px"></asp:TextBox>
                                                    </td>
                                                    <td align="right" class="Texto">
                                                        Nro. Certificado MTC:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCertificado_Vehiculo" runat="server" CssClass="TextBoxReadOnly"
                                                            ReadOnly="True" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarVehiculo" runat="server" Text="Buscar" Width="80px" OnClientClick=" return(   valOnClick_btnBuscarVehiculo()      ); " />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnLimpiarVehiculo" runat="server" Text="Limpiar" Width="80px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="Image_Agente" runat="server" />&nbsp;AGENCIA - TRANSPORTE
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_Agente" runat="server"
                                TargetControlID="Panel_Agente" ImageControlID="Image_Agente" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" Collapsed="true" CollapseControlID="Image_Agente"
                                ExpandControlID="Image_Agente">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Agente" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" align="right">
                                                        Agente:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAgente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="450px" Height="22px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarAgente" runat="server" Text="Buscar" Width="80px" OnClientClick="return(valOnClick_btnBuscarAgenteTransportista());" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnLimpiarAgencia" runat="server" Text="Limpiar" Width="80px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="Texto">
                                                        D.N.I.:
                                                    </td>
                                                    <td colspan="3">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtDni_Agente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                        Width="150px"></asp:TextBox>
                                                                </td>
                                                                <td align="right" class="Texto">
                                                                    R.U.C.:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtRuc_Agente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                        Width="150px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="Texto">
                                                        Dirección:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDireccion_Agente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="450px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                OBSERVACIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" Width="100%" runat="server"
                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos);"
                                    Height="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdRemitente" runat="server" />
                <asp:HiddenField ID="hddIdDestinatario" runat="server" />
                <asp:HiddenField ID="hddIdTransportista" runat="server" />
                <asp:HiddenField ID="hddIdChofer" runat="server" />
                <asp:HiddenField ID="hddIdVehiculo" runat="server" />
                <asp:HiddenField ID="hddIdAgente" runat="server" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddIndex_GV_Detalle" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="6" />
                <asp:HiddenField ID="hddOpcionBusquedaPersona" runat="server" Value="0" />
                <asp:HiddenField ID="hddOpcionBusquedaAgente" runat="server" Value="0" />
                <asp:HiddenField ID="hddNroFilasxDocumento" runat="server" Value="0" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="HddIdProd" runat="server" />
                <asp:HiddenField ID="hddPoseeGRecepcion" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
                <asp:HiddenField ID="hddNroFilasxGuia" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                TipoExistencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                    DataTextField="Descripcion" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub Línea:"></asp:Label>
                                <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    DataValueField="Id" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripción:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="Panel5" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">                                           
                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                onKeypress="return( valKeyPressDescripcionProd(this,event) );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                Cód.:
                                        </td>
                                        <td>
                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onKeypress="return( valKeyPressDescripcionProd(this,event) );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" TabIndex="205"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />--%>
                                                <asp:Button ID="btnBuscarGrilla_AddProducto" runat="server" Text="Buscar" CssClass="btnBuscar"
                                            OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" TabIndex="207" />
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../Imagenes/Agregar_A.JPG';"
                                                OnClientClick="return(valAddProductos());" TabIndex="208" />--%>
                                                <asp:Button ID="btnAddProductos_AddProducto" runat="server" Text="Añadir" CssClass="btnAñadir"
                                                    TabIndex="208" />
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />--%>
                                                <asp:Button ID="btnLimpiar_AddProducto" runat="server" Text="Limpiar" CssClass="btnLimpiar"
                                                TabIndex="209" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="Búsqueda Avanzada" ExpandedText="Búsqueda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="Búsqueda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel7" runat="server" DefaultButton="btnAddProductos_AddProducto">                    
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCantidad_AddProd" runat="server" onblur="return( valBlurClear('0.00',event) );"
                                        onfocus="return(  aceptarFoco(this) );" onKeypress="return( valKeyPressCantidadAddProd(event)  );"
                                        TabIndex="210" Text="0" Width="65px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Codigo" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Código" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Descripcion" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Descripción" NullDisplayText="---" />
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="U.M." ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:DropDownList ID="cboUnidadMedida" runat="server" AutoPostBack="false" DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaUM")%>'
                                        DataTextField="NombreCortoUM" DataValueField="IdUnidadMedida">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="StockDisponible_Cadena" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Stock Disponible" ItemStyle-HorizontalAlign="Center" NullDisplayText="0" />
                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:ImageButton ID="btnConsultarStockAlmacenes_BuscarProd" runat="server" ImageUrl="~/Imagenes/Ok_b.bmp"
                                                    OnClick="mostrarCapaStockPrecioxProducto" ToolTip="Consultar Lista de Precios y Stock por Tienda / Almacén."
                                                    OnClientClick="this.src='../Imagenes/loader.gif'" />

                                                <asp:HiddenField ID="hddIdProducto_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                <asp:HiddenField ID="hddIdUnidadMedida_PR" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />

                                                <asp:HiddenField ID="hddKit" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Kit")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarPersonaGrillas">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this,event));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrillas">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel4" runat="server" DefaultButton="btBuscarPersonaGrillas">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />--%>
                                                                            <asp:Button ID="btBuscarPersonaGrillas" runat="server" Text="Buscar" CssClass="btnBuscar"
                                                                        OnClientClick="this.disabled;this.value='Procesando...'" UseSubmitBehavior="false"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Rol:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="Ddl_Rol" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="Página Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="Página Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>

    <div id="capaGlosa" style="border: 3px solid blue; padding: 8px; width: auto; height: auto;
        position: absolute; top: 300px; left: 250px; background-color: white; z-index: 2;
        display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton7" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaGlosa'));" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table>
                        <tr>
                            <td class="Texto">
                                Glosa:
                            ">
                                Glosa:
                            </td>
                            <td>
                                <asp:TextBox ID="txtGlosa_CapaGlosa" runat="server" Width="350px" TextMode="MultiLine"
                                    Height="50px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIndexGlosa_Gv_Detalle" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:ImageButton ID="btnAddGlosa_CapaGlosa" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                        onmouseout="this.src='../Imagenes/Aceptar_B.JPG';" OnClientClick="return(     valOnClickAddGlosa()       );"
                        onmouseover="this.src='../Imagenes/Aceptar_B.JPG';" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%" AllowPaging="True" PagerStyle-ForeColor="White" PagerSettings-Position="TopAndBottom">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisión"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Destinatario" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de búsqueda realiza un filtrado de acuerdo al destinatario seleccionado.
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 950px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 20px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <td class="Texto">
                            Tipo Documento:
                        </td>
                        <td>
                            <asp:DropDownList ID="CboTipoDocumento" runat="server">
                            </asp:DropDownList>
                        </td>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisión</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:Panel ID="Panel9" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                </asp:Panel>
                                            </td>
                                            <td class="Texto">
                                                Nro. Código:
                                            </td>
                                            <td>
                                            <asp:Panel ID="Panel10" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%" AllowPaging="True" PagerSettings-Position="TopAndBottom" PagerStyle-ForeColor="White">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>

                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                    <asp:HiddenField ID="hddIdEmpresa_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />

                                                    <asp:HiddenField ID="hddIdAlmacen_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                    <asp:HiddenField ID="hddIdPersona_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisión"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Descripción" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomAlmacen" HeaderText="Almacén" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                            DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="MotivoTraslado" HeaderText="Motivo Traslado" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="TipoOperacion" HeaderText="Tipo Operacion" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="Texto" style="text-align: left; font-weight: bold">
                    *** El filtro de los despachos pendientes se realiza de acuerdo a la Empresa / Almacén
                    seleccionados.
                </td>
            </tr>
        </table>
    </div>
    <div id="capaConsultarStockPrecioxProducto" style="border: 3px solid blue; padding: 8px;
        width: 650px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa2('capaConsultarStockPrecioxProducto')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProductoConsultarStockPrecioxProd" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ConsultarStockPrecioxProd" runat="server" AutoGenerateColumns="False"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Almacén" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                <asp:Label ID="lblAlmacen" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Almacen")%>'></asp:Label>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Real">
                                <ItemTemplate>
                                                <asp:Label ID="lblStockReal" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"StockAReal","{0:F3}")%>'></asp:Label>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Comprometido">
                                <ItemTemplate>
                                                <asp:Label ID="lblStockComprometido" runat="server" Font-Bold="true" ForeColor="Red"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"StockComprometido","{0:F3}")%>'></asp:Label>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Disponible" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblStock" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StockDisponibleN","{0:F3}")%>'></asp:Label>

                                                <asp:Label ID="lblUM" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cadenaUM")%>'></asp:Label>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Transito">
                                <ItemTemplate>
                                                <asp:Label ID="lblStockTransito" runat="server" Font-Bold="true" ForeColor="Red"
                                                    Text='<%#DataBinder.Eval(Container.DataItem,"StockEnTransito","{0:F3}")%>'></asp:Label>

                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cantidad" Visible="false" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCantidad" Text="0" runat="server" Width="80px" Font-Bold="true"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnAddProducto_ConsultarStockPrecio" runat="server" Text="Aceptar"
                        Width="90px" OnClientClick="return(  offCapa2('capaConsultarStockPrecioxProducto')   );" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaVehiculo" style="border: 3px solid blue; padding: 8px; width: 560px;
        height: auto; position: absolute; top: 800px; left: 87px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton5" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaVehiculo'));" />
                </td>
            </tr>
            <tr class="Label">
                <td align="left">
                    Nro. Placa:
                    <asp:Panel ID="Panel8" runat="server" DefaultButton="btnBuscarVehiculo_Grilla">                    
                    <asp:TextBox ID="txtNroPlaca_BuscarVehiculo" runat="server" Width="120px"></asp:TextBox>
                    </asp:Panel>
                    <asp:Button ID="btnBuscarVehiculo_Grilla" runat="server" Text="Buscar" Width="80px" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_Vehiculo" runat="server" AutoGenerateColumns="false" Width="100%">
                        <Columns>
                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="true" ButtonType="Link"
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Nro. Placa" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroPlaca" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Placa")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdVehiculo" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Modelo" HeaderText="Modelo" NullDisplayText="---" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="ConstanciaIns" HeaderText="Const. Inscripción" NullDisplayText="---"
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaConfiguracionFrm" style="border: 3px solid blue; padding: 8px; width: auto;
        height: auto; position: absolute; top: 120px; left: 38px; background-color: white;
        z-index: 2; display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton9" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaConfiguracionFrm'));" />
                </td>
            </tr>
            <tr align="left">
                <td>
                    <table>
                        <tr>
                            <td class="Label" align="right">
                                Mover Stock Físico:
                            </td>
                            <td style="width: 75px">
                                <asp:CheckBox ID="chb_MoverAlmacen" Enabled="true" runat="server" Checked="true"
                                    ToolTip="Opción válido para Documentos NO afectos a despachos pendientes." />
                            </td>
                            <td class="Label" align="right">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="LabelRojo" style="font-weight: bold">
                                *** El Despacho siempre genera Mov. de Stock Físico.
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaEquivalenciaProducto" style="border: 3px solid blue; padding: 8px; width: 450px;
        height: auto; position: absolute; top: 237px; left: 250px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaEquivalenciaProducto')   );" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Ingrese Cantidad:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCantidad_Ingreso" onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); "
                                                onFocus=" return(  aceptarFoco(this)  ); " Width="100px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboUnidadMedida_Ingreso" runat="server" DataTextField="NombreCortoUM"
                                                DataValueField="IdUnidadMedida">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCalcular" OnClientClick=" return(  valOnClick_btnCalcular()   ); "
                                                runat="server" Text="Calcular" ToolTip="Calcular equivalencias" Width="80px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            Resultado:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCantidad_Salida" Font-Bold="true" CssClass="TextBox_ReadOnlyLeft"
                                                Text="" ReadOnly="true" Width="100px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboUnidadMedida_Salida" runat="server" DataTextField="NombreCortoUM"
                                                DataValueField="IdUnidadMedida">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GV_CalcularEquivalencia" runat="server" AutoGenerateColumns="False"
                                    Width="400px">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chb_UnidadMedida" runat="server" Checked="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unidad Medida" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblUnidadMedida" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida")%>'></asp:Label>

                                                            <asp:HiddenField ID="hddIdUnidadMedida" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblUnidadMedidaAbv" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NombreCortoUM")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GV_ResuldoEQ" runat="server" AutoGenerateColumns="False" Width="400px">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="UnidadMedida" HeaderText=""
                                            ItemStyle-Height="25px" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="UnidadMedidaAbv" HeaderText="U.M."
                                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="Cantidad" HeaderText="Cantidad"
                                            DataFormatString="{0:F4}" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:RadioButtonList ID="rbl_UtilizarRedondeo" runat="server" RepeatDirection="Horizontal"
                                    CssClass="Label">
                                    <asp:ListItem Selected="True" Value="0">No Utilizar Redondeo</asp:ListItem>
                                    <asp:ListItem Value="1">Redondeo [ arriba ]</asp:ListItem>
                                    <asp:ListItem Value="2">Redondeo [ abajo ]</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnAceptar_EquivalenciaPR" runat="server" Text="Aceptar" Width="80px"
                                    ToolTip="Aceptar Equivalencia" OnClientClick=" return(  valOnClick_btnAceptar_EquivalenciaPR()  );  " />
                            </td>
                            <td>
                                <asp:Button ID="btnCerrar_EquivalenciaPR" runat="server" Text="Cerra" Width="80px"
                                    ToolTip="Cerrar" OnClientClick="  return(  offCapa('capaEquivalenciaProducto') ); " />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
    </div>

    <div id="capaDatosCancelacion" style="border: 3px solid blue; padding: 8px; width: 450px;">
            
    </div>    

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function valKeyPressCodigoSL(e) {
            //obtener caracter pulsado en todos los exploradores
            var evento = e || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProducto.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(obj, e) {
            //obtener caracter pulsado en todos los exploradores
            var evento = e || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscarGrilla_AddProducto.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valAddProductos() {

            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            //        var txtIdCliente = 
            //        if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
            //            alert('Debe seleccionar un Cliente.');
            //            return false;
            //        }

            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('No se seleccionaron productos.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                var txtCantidad = rowElem.cells[0].children[0];
                if (parseFloat(txtCantidad.value) > 0) {
                    // if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
                    //                        alert('La cantidad ingresada excede a la cantidad disponible.');
                    //                        txtCantidad.select();
                    //                        txtCantidad.focus();
                    //     return false;
                    //  } else if (parseFloat(txtCantidad.value) > 0) {
                    cont = cont + 1;
                    //  }
                }
            }

            if (cont == 0) {
                alert('No se seleccionaron productos.');
                return false;
            }

            /*
            //************* Validamos que no agregue de otro almacen
            
            if (grillaDetalle != null) {

            if (grillaDetalle.rows.length > 1) {

            //************ valido el almacen
                    
            var hddIdAlmacen = 
                    

            if (parseFloat(cboAlmacen.value) != parseFloat(hddIdAlmacen.value)) {
                        
            return false;
            }
            }
            }
            */
            return true;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        function valStockDisponible_AddProd() {
            /*var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
            if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].id == event.srcElement.id) {
            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
            alert('La cantidad ingresada excede a la cantidad disponible.');
            rowElem.cells[0].children[0].select();
            rowElem.cells[0].children[0].focus();
            return false;
            }
            return false;
            }

            }
            }*/
            return false;
        }
        function valKeyPressCantidadAddProd(e) {
            //obtener caracter pulsado en todos los exploradores
            var evento = e || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                document.getElementById('<%=btnAddProductos_AddProducto.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }
        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero(e) {
            //obtener caracter pulsado en todos los exploradores
            var evento = e || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrillas.ClientID %>');
                boton.focus();
                return true;


            }
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }

        function validarCajaBusqueda(obj, e) {
            //obtener caracter pulsado en todos los exploradores
            var evento = e || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrillas.ClientID %>');
                boton.focus();
                return true;

            }
        }

        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }

        function valOnClick_btnBuscarRemitente() {
            //        document.getElementById('<%=hddOpcionBusquedaPersona.ClientID%>').value = '0';
            //        mostrarCapaPersona();
            //        return false;
            return true;
        }
        function valOnClick_btnBuscarDestinatario() {
            /*
            document.getElementById('<%=hddOpcionBusquedaPersona.ClientID%>').value = '1';
            mostrarCapaPersona();
            return false;
            */
            return true;
        }
        function valOnClick_btnBuscarTransportista() {

            document.getElementById('<%=hddOpcionBusquedaPersona.ClientID%>').value = '2';
            mostrarCapaPersona();
            return false;
            //return true;
        }

        function valOnClick_btnBuscarAgenteTransportista() {

            document.getElementById('<%=hddOpcionBusquedaPersona.ClientID%>').value = '-1';
            mostrarCapaPersona();
            return false;
            //return true;
        }

        function valOnClick_btnBuscarChofer() {
            document.getElementById('<%=hddOpcionBusquedaPersona.ClientID%>').value = 3;
            mostrarCapaPersona();
            return false;
        }
        function valOnClick_btnBuscarVehiculo() {
            onCapa('capaVehiculo');
            document.getElementById('<%=txtNroPlaca_BuscarVehiculo.ClientID%>').select();
            document.getElementById('<%=txtNroPlaca_BuscarVehiculo.ClientID%>').focus();
            return false;
        }
        function valSaveDocumento() {
            var ddlSerie = document.getElementById('<%=cboSerie.ClientID %>');
            var serie = ddlSerie.options[ddlSerie.selectedIndex].text
            if (serie == "006" || serie == "030" || serie == "028") {
                calcularPesoTotalmasdeUnaCaja();
            } else { calcularPesoTotal(0); }


            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie de Documento.');
                return false;
            }

            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operación.');
                return false;
            }

            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var cboAlmacen = document.getElementById('<%=cboAlmacen.ClientID%>');
            if (isNaN(parseInt(cboAlmacen.value)) || cboAlmacen.value.length <= 0) {
                alert('Debe seleccionar un Almacén.');
                return false;
            }
            if (parseInt(cboTipoOperacion.SelectedValue) != 2 || parseInt(cboTipoOperacion.SelectedValue) != 5) {
                var hddIdDestinatario = document.getElementById('<%=hddIdDestinatario.ClientID%>');
                if (isNaN(parseInt(hddIdDestinatario.value)) || hddIdDestinatario.value.length <= 0) {
                    alert('Debe seleccionar un Destinatario.');
                    return false;
                }
                var hddIdRemitente = document.getElementById('<%=hddIdRemitente.ClientID%>');
                if (isNaN(parseInt(hddIdRemitente.value)) || hddIdRemitente.value.length <= 0) {
                    alert('Debe seleccionar un Remitente.');
                    return false;
                }
            }

            var cboMotivoTraslado = document.getElementById('<%=cboMotivoTraslado.ClientID%>');
            if (isNaN(parseInt(cboMotivoTraslado.value)) || cboMotivoTraslado.value.length <= 0 || parseInt(cboMotivoTraslado.value) <= 0) {
                alert('Debe seleccionar un Motivo de Traslado.');
                return false;
            }
            //qweqwe

            //************* Validando cantidades y precios
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                var cantidad = 0;
                var txtCantidad;
                var unidadMedidaPrincipal;
                var unidadMedidadValidacion;
                var comboValidacion;

                if (parseInt(cboTipoOperacion.value) == 4) {

                    for (var i = 1; i < grilla.rows.length; i++) {
                        comboValidacion = grilla.rows[i].cells[4].children[0];
                        unidadMedidaPrincipal = grilla.rows[i].cells[11].children[1].innerHTML;
                        unidadMedidadValidacion = comboValidacion.options[comboValidacion.selectedIndex].text;
                        if (unidadMedidaPrincipal != unidadMedidadValidacion || unidadMedidadValidacion == 'UND' || unidadMedidadValidacion == 'PZA' || unidadMedidadValidacion == 'PAR') {
                            //cantidad = grilla.rows[i].cells[7].children[0].children[1].cells[7].children[0];
                            cantidad = grilla.rows[i].cells[7].getElementsByTagName('input')[0].value;
                            if (cantidad > 1 && unidadMedidadValidacion == "PZA") {
                                alert('No puede despachar mas de una unidad');
                                return false;
                            } else {
                                if (cantidad >= 1 && unidadMedidadValidacion != "PZA" && unidadMedidadValidacion != "UND" && unidadMedidadValidacion != "PAR") {
                                    alert('No puede despachar mas de una unidad');
                                    return false;
                                }
                            }
                        } //final del if "diferente unidad de medida"
                        else {
                            if (cantidad >= 1 && cantidad <= 1 && unidadMedidadValidacion == "PZA") {
                                break;
                            } else {
                                alert('No puede despachar mas de una unidad');
                                return false;
                            }
                        }
                    } //final del for
                } //final del if tipo de operacion

                if (serie == "006" || serie == "030" || serie == "028") {
                    for (var i = 1; i < grilla.rows.length; i++) {
                        var checkSeleccionado = false;
                        var checkboxList = grilla.rows[i].cells[5].getElementsByTagName('INPUT');
                        var checkCantidad = grilla.rows[i].cells[5].getElementsByTagName('input').length;

                        for (j = 0; j < checkboxList.length; j++) {
                            if (checkboxList[j].checked) {
                                checkSeleccionado = true;
                                break;
                            }
                        }
                        if (checkSeleccionado == false) {
                            alert('Debe marcar como mínimo un tono.');
                            return false;
                        }
                        var rowElem = grilla.rows[i];
                        //txtCantidad = rowElem.cells[4].children[0];
                        //                    txtCantidad = rowElem.cells[7].children[5];
                        //                    cantidad = parseFloat(txtCantidad.value);
                        var disponible = grilla.rows[i].cells[13].children[0].innerHTML;
                        var acumulativoCantidad = 0;

                        for (var h = 0; h < checkCantidad; h++) {
                            txtCantidad = rowElem.cells[7].getElementsByTagName('input')[h];
                            cantidad = parseFloat(txtCantidad.value);
                            acumulativoCantidad += cantidad;
                            if (isNaN(cantidad)) { cantidad = 0; }
                            if (checkboxList[h].checked) {
                                if (cantidad <= 0) {
                                    alert('Ingrese una cantidad mayor a cero.');
                                    txtCantidad.select();
                                    txtCantidad.focus();
                                    return false;
                                }
                            } else { break; }
                        }
                        //                        if (acumulativoCantidad > disponible) {
                        //                            alert('Cantidad no puede ser mayor que Disponible.');
                        //                            return false;
                        //                        }
                    }
                }
            } else {
                alert('No se han ingresado productos.');
                return false;
            }

            var grilla = document.getElementById('<%=GV_DocumentoRef.ClientID%>');
            if (grilla != null) {
                var IdEmpresa = parseInt(cboEmpresa.value);
                var IdAlmacen = parseInt(cboAlmacen.value);
                var IdDestinatario = parseInt(hddIdDestinatario.value);
                var IdEmpresaRef = 0;
                var IdAlmacenRef = 0;
                var IdDestinatarioRef = 0;

                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var valida = true;

                    IdEmpresaRef = parseInt(rowElem.cells[2].children[2].value);
                    IdAlmacenRef = parseInt(rowElem.cells[2].children[3].value);
                    IdDestinatarioRef = parseInt(rowElem.cells[2].children[4].value);
                    IdTipoDocumento = parseInt(rowElem.cells[2].children[5].value);

                    if (IdTipoDocumento == 15 && parseInt(cboTipoOperacion.value) == 1) { // OP/VP - VENTA
                        valida = false;
                    }

                    if (valida == true) {
                        if (parseInt(cboTipoOperacion.value) != 2 && parseInt(cboTipoOperacion.value) != 5) {

                            if (IdDestinatario != IdDestinatarioRef) {
                                alert('EL DESTINATARIO DEL DOCUMENTO DE REFERENCIA NO COINCIDE CON EL DESTINATARIO SELECCIONADO. NO SE PERMITE LA OPERACIÓN.');
                                return false;
                            }
                            if (IdAlmacen != IdAlmacenRef) {
                                alert('EL ALMACÉN DEL DOCUMENTO DE REFERENCIA NO COINCIDE CON EL ALMACÉN SELECCIONADO. NO SE PERMITE LA OPERACIÓN.');
                                return false;
                            }
                        }
                        if (IdEmpresa != IdEmpresaRef) {
                            alert('LA EMPRESA DEL DOCUMENTO DE REFERENCIA NO COINCIDE CON LA EMPRESA SELECCIONADA. NO SE PERMITE LA OPERACIÓN.');
                            return false;
                        }
                    }
                }
            }
            //*************** VALIDANDO LOS REMITENTES
            var chb_MoverAlmacen = document.getElementById('<%=chb_MoverAlmacen.ClientID%>');
            if ((chb_MoverAlmacen.status) && (parseInt(cboEmpresa.value) != parseInt(hddIdRemitente.value))) {
                alert('LA OPCIÓN < MOVER STOCK FÍSICO > ESTÁ ACTIVO. LA EMPRESA SELECCIONADA NO COINCIDE CON EL REMITENTE SELECCIONADO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var txtDireccion_Llegada = document.getElementById('<%=txtDireccion_Llegada.ClientID%>');
            if (txtDireccion_Llegada.value.length <= 0 || txtDireccion_Llegada.value == '') {
                alert('DEBE INGRESAR LA DIRECCION DEL PUNTO DE LLEGADA. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
            var mensaje = '';
            switch (parseInt(hddFrmModo.value)) {
                case 1: //******** NUEVO
                    mensaje = 'Desea continuar con la Operación ?';
                    break;
                case 2: //******** EDITAR
                    mensaje = 'Desea continuar con la Operación ?';
                    break;
            }
            if (confirm(mensaje)) {
                document.getElementById('<%=btnGuardar.ClientID %>').style.visibility = 'hidden';
                return true;
            }
            else {
                document.getElementById('<%=btnGuardar.ClientID %>').style.visibility = 'visible';
                return false;
            }
        }

        //***********************************
        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID %>');
            hddFrmModo.value = 2;
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la Búsqueda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }
        function valOnKeyPressCodigoDoc(txtCodigoDocumento, e) {
            //obtener caracter pulsado en todos los exploradores
            var evento = e || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }

        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la Búsqueda
                alert('Debe habilitar la Búsqueda del Documento haciendo clic en el botón [ Buscar ]');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor válido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }
        function valOnClickEditar() {

            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ningún Documento para Edición.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento está ANULADO. No se permite su edición.');
                return false;
            }
            var hddPoseeGRecepcion = document.getElementById('<%=hddPoseeGRecepcion.ClientID %>');
            if (isNaN(parseInt(hddPoseeGRecepcion.value))) { hddPoseeGRecepcion.value = 0; }
            if (parseInt(hddPoseeGRecepcion.value) > 0) {
                alert('El Documento Posee Guia de Recepcion. No se permite su edición.');
                return false;
            }

            return true;
        }
        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ningún Documento para su Anulación.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya está ANULADO. No se permite su anulación.');
                return false;
            }

            var hddPoseeGRecepcion = document.getElementById('<%=hddPoseeGRecepcion.ClientID %>');
            if (isNaN(parseInt(hddPoseeGRecepcion.value))) { hddPoseeGRecepcion.value = 0; }
            if (parseInt(hddPoseeGRecepcion.value) > 0) {
                alert('El Documento Posee Guia de Recepcion. No se permite su anulación.');
                return false;
            }

            return confirm('Desea continuar con el Proceso de ANULACIÓN del Documento ?');
        }

        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la Búsqueda
                alert('Debe habilitar la Búsqueda del Documento haciendo clic en el botón [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');
            return false;
        }

        function valOnClick_btnAceptarBuscarDocRefxCodigo() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('Ingrese un Nro. de Serie para la búsqueda.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('Ingrese un Nro. de Documento para la búsqueda.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }

        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        //**********************************    CALCULO DE EQUIVALENCIA PRODUCTO
        function valOnClick_btnEquivalencia_PR(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];
                    if (boton.id == rowElem.cells[7].children[0].id) {
                        //onCapa('capaEquivalenciaProducto');                                                
                        document.getElementById('<%=hddIndex_GV_Detalle.ClientID%>').value = (i - 1);
                        //document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').select();
                        //document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').focus();
                        return true;
                    }
                }
            }
            alert('Problemas en la ubicación de la Fila.');
            return false;
        }
        function mostrarCapaEquivalencia_PR() {
            onCapa('capaEquivalenciaProducto');
            //document.getElementById('<%=hddIndex_GV_Detalle.ClientID%>').value = (i - 1);
            document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').select();
            document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').focus();
            return false;
        }
        function valOnClick_btnCalcular() {
            var cboUnidadMedida_Ingreso = document.getElementById('<%=cboUnidadMedida_Ingreso.ClientID %>');
            if (isNaN(parseFloat(cboUnidadMedida_Ingreso.value)) || cboUnidadMedida_Ingreso.value.length <= 0) {
                alert('DEBE SELECCIONAR UNA UNIDAD DE MEDIDA DE INGRESO.');
                return false;
            }
            var cboUnidadMedida_Salida = document.getElementById('<%=cboUnidadMedida_Salida.ClientID %>');
            if (isNaN(parseFloat(cboUnidadMedida_Salida.value)) || cboUnidadMedida_Salida.value.length <= 0) {
                alert('DEBE SELECCIONAR UNA UNIDAD DE MEDIDA DE SALIDA.');
                return false;
            }
            var txtCantidad = document.getElementById('<%=txtCantidad_Ingreso.ClientID %>');
            if (isNaN(parseFloat(txtCantidad.value)) || txtCantidad.value.length <= 0) {
                alert('INGRESE UN VALOR VÁLIDO.');
                txtCantidad.select();
                txtCantidad.focus();
                return false;
            }
            return true;
        }
        function valOnClick_btnAceptar_EquivalenciaPR() {
            var grilla = document.getElementById('<%=GV_ResuldoEQ.ClientID %>');
            if (grilla == null) {
                alert('NO SE HA REALIZADO EL CÁLCULO RESPECTIVO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            return true;
        }
        //**********************************    FIN  CALCULO DE EQUIVALENCIA PRODUCTO

        function valOnClick_btnImprimir(tipoImpresion) {
            var hdd = document.getElementById('<%= hddIdDocumento.ClientID%>');
            if (hdd.value.length == 0 || hdd.value == '' || parseFloat(hdd.value) == 0) {
                alert('No se ha registrado ninguna Guía de Remisión para enviar a i mpresión.');
                return false;
            }
            //if (confirm('Desea mandar la impresión de la Guía de Remisión?')) {
            // mostrarVentana('../../Reportes/visor.aspx?iReporte=10&IdDocumento=' + hdd.value, 'Orden de Despacho', 500, 500);
            //window.parent.open('../../Reportes/visor.aspx?iReporte=10&IdDocumento=' + hdd.value, 'Reporte', 'resizable=yes', null);
            window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=6&IdDocumento=' + hdd.value + '&tipoImpresion=' + tipoImpresion, 'Remision', 'resizable=yes,width=1000,height=800,scrollbars=1', null);


            //}
            return false;
        }


        function calcularPesoTotal(opcion) {
            /*
            opcion  0: Cantidad 1: Peso
            */
            var txtPesoTotal = document.getElementById('<%=txtPesoTotal.ClientID%>');
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var pesoTotal = 0;
            var cantidad;
            var peso;
            var equivalencia;
            var equivalencia1;
            var txtCantidad;
            var txtPeso;
            var hddEquivalencia;
            var hddEquivalencia1;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];
                    var hiddenCantidad = rowElem.cells[7].children[5];
                    //**************  OBTENEMOS LOS DATOS
                    txtCantidad = rowElem.cells[7].getElementsByTagName('input')[0];
                    cantidad = parseFloat(txtCantidad.value);
                    hiddenCantidad.value = cantidad;
                    if (isNaN(cantidad) || txtCantidad.value.length <= 0) { cantidad = 0; }

                    txtPeso = rowElem.cells[9].children[0];
                    peso = parseFloat(txtPeso.value);
                    if (isNaN(peso) || txtPeso.value.length <= 0) { peso = 0; }

                    hddEquivalencia = rowElem.cells[9].children[2];
                    equivalencia = parseFloat(hddEquivalencia.value);
                    if (isNaN(equivalencia) || hddEquivalencia.value.length <= 0) { equivalencia = 0; }

                    hddEquivalencia1 = rowElem.cells[9].children[3];
                    equivalencia1 = parseFloat(hddEquivalencia1.value);
                    if (isNaN(equivalencia1) || hddEquivalencia1.value.length <= 0) { equivalencia1 = 0; }

                    switch (opcion) {
                        case '0':


                            peso = cantidad * equivalencia * equivalencia1;
                            txtPeso.value = redondear(peso, 3);


                            break;
                    }

                    pesoTotal = pesoTotal + peso;

                }
            }

            txtPesoTotal.value = redondear(pesoTotal, 3);
            return false;

        }

        function calcularPesoTotalmasdeUnaCaja() {
            /*
            opcion  0: Cantidad 1: Peso
            */
            var txtPesoTotal = document.getElementById('<%=txtPesoTotal.ClientID%>');
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var cantidad;

            var columnaPesototales = 0;
            var nuevoPeso = 0;
            var nuevoCantidadTotal = 0; //**variable que acumula la cantidad total de absolutamente todos los tonos.
            var txtCantidad;
            var txtPeso;
            var hddEquivalencia;
            var hddEquivalencia1;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO

                    var pesoTotal = 0;
                    var cantidadTotal = 0; //variable que guarda la cantidad total de tonos seleccionados
                    var peso = 0;
                    var txtPeso;
                    var equivalencia;
                    var equivalencia1;
                    var rowElem = grilla.rows[i];
                    var checkBoxList = grilla.rows[i].cells[5].getElementsByTagName('INPUT');
                    var checkCantidad = grilla.rows[i].cells[5].getElementsByTagName('input').length;
                    var hiddenCantidad = rowElem.cells[7].children[5];
                    var cadenaCantidades = "";
                    //**************  OBTENEMOS LOS DATOS
                    for (var h = 0; h < checkCantidad; h++) {
                        //if (checkBoxList[h].checked) {
                        txtCantidad = rowElem.cells[7].getElementsByTagName('input')[h];
                        //txtCantidad = rowElem.cells[7].children[indice];
                        cantidad = parseFloat(txtCantidad.value);
                        if (isNaN(cantidad) || txtCantidad.value.length <= 0) { cantidad = 0; }

                        txtPeso = rowElem.cells[9].children[0];
                        peso = parseFloat(txtPeso.value);
                        if (isNaN(peso) || txtPeso.value.length <= 0) { peso = 0; }

                        hddEquivalencia = rowElem.cells[9].children[2];
                        equivalencia = parseFloat(hddEquivalencia.value);
                        if (isNaN(equivalencia) || hddEquivalencia.value.length <= 0) { equivalencia = 0; }

                        hddEquivalencia1 = rowElem.cells[9].children[3];
                        equivalencia1 = parseFloat(hddEquivalencia1.value);
                        if (isNaN(equivalencia1) || hddEquivalencia1.value.length <= 0) { equivalencia1 = 0; }

                        peso = cantidad * equivalencia * equivalencia1;
                        txtPeso.value = redondear(peso + pesoTotal, 3);

                        cadenaCantidades = cadenaCantidades + cantidad + ",";

                        pesoTotal = pesoTotal + peso;
                        if (checkBoxList[h].checked) {
                            txtCantidad = rowElem.cells[7].getElementsByTagName('input')[h];
                            cantidad = parseFloat(txtCantidad.value);

                            cantidadTotal = cantidadTotal + cantidad;
                            //break;
                        }
                        //}//fin del if que valida los checkboxlist activos
                    } //fin del For para recorrer los textBox
                    nuevoPeso = nuevoPeso + pesoTotal;

                    grilla.rows[i].cells[7].children[6].value = cadenaCantidades.substr(0, cadenaCantidades.length - 1);

                    txtPesoTotal.value = redondear(nuevoPeso, 3);
                    hiddenCantidad.value = redondear(cantidadTotal, 4);
                    //return false;
                } //fin del For para recorrer la grilla
            } //fin del if
        } //fin de la funcion

    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>
</asp:Content>
