<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmInventarioInicial.aspx.vb" Inherits="APPWEB.FrmInventarioInicial" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">
        function validarSave() {
        var grilla = document.getElementById('<%=DGVDetalle.ClientID %>');
        if (grilla == null) {
            alert("No se han ingresado productos.");
            return false;
        }
         for (var i = 1; i < grilla.rows.length; i++) {
             var rowElem = grilla.rows[i];
             var cellCantidad = rowElem.cells[4];
             //var cellCosto = rowElem.cells[5];
            if ((!esDecimal(cellCantidad.children[0].value)) || (parseFloat(cellCantidad.children[0].value) == 0)) {
                alert('Cantidad no válida.');
                cellCantidad.children[0].select();
                cellCantidad.children[0].focus();                
                return false;
            }
            if (cellCantidad.children[0].value.length == 0) {
                alert('Ingrese una cantidad.');
                cellCantidad.children[0].select();
                cellCantidad.children[0].focus();
                return false;
            }
            /*if ((!esDecimal(cellCosto.children[0].value)) || (parseFloat(cellCosto.children[0].value) == 0)) {
                alert('Los productos a registrar deben tener un Precio de Compra.');
                cellCosto.children[0].select();
                cellCosto.children[0].focus();
                return false;
            }
            if (cellCosto.children[0].value.length == 0) {
                alert('Los productos a registrar deben tener un Precio de Compra.');
                cellCosto.children[0].select();
                cellCosto.children[0].focus();
                return false;
            } */          
        }
        return (confirm('Desea continuar con la operación?'));
    }    
    function validarEnter() {
        return (!esEnter(event));
    }
    function validarNumeroPunto(elEvento) {
        //obtener caracter pulsado en todos los exploradores
        var evento = elEvento || window.event;
        var caracter = evento.charCode || evento.keyCode;
        //alert("El carácter pulsado es: " + caracter);
        if (caracter == 46) {
            return true;
        } else {
            if (!onKeyPressEsNumero('event')) {
                alert('Caracter no válido.');
                return false;
            }
        }
    }
</script>
 <asp:UpdatePanel ID="UpdatePanel1" runat="server"> <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td>
                        <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" 
                            onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" 
                            onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" />
                        <asp:ImageButton ID="btnGuardar" runat="server" CausesValidation="true" 
                            ImageUrl="~/Imagenes/Guardar_B.JPG"  OnClientClick="return(validarSave());"
                            onmouseout="this.src='/Imagenes/Guardar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Guardar_A.JPG';" 
                             />                        
                        <asp:ImageButton ID="btnCancelar" runat="server"  OnClientClick="return(confirm('Desea retroceder?'));"
                            ImageUrl="~/Imagenes/Arriba_B.JPG" 
                            onmouseout="this.src='/Imagenes/Arriba_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Arriba_A.JPG';" ToolTip="Atrás" />
                        <asp:ImageButton ID="btnImprimir" runat="server" 
                            ImageUrl="~/Imagenes/Imprimir_B.JPG"  OnClientClick="return(confirm('Desea mandar la impresión del documento Inventario Inicial'));"
                            onmouseout="this.src='/Imagenes/Imprimir_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Imprimir_A.JPG';" />                     
                    </td>
                </tr>
                <tr>
                                    <td class="TituloCelda">
                                        INVENTARIO INICIAL</td>
                                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_InvInicial" runat="server">
                            <table style="width: 100%">
                                
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Empresa:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="True" 
                                                        DataTextField="NombreComercial" DataValueField="Id">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Tienda:"></asp:Label>
                                                    <asp:DropDownList ID="cmbTienda" runat="server" AutoPostBack="True" DataTextField="Nombre" DataValueField="Id">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="Label20" runat="server" CssClass="Label" Text="Almacén:"></asp:Label>
                                                    <asp:DropDownList ID="cmbAlmacen" runat="server" DataTextField="Nombre" 
                                                        DataValueField="IdAlmacen">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="Label25" runat="server" CssClass="Label" Text="Fecha:"></asp:Label>
                                                    <asp:Label ID="lblFechaActual" CssClass="Label" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="Label21" CssClass="Label" runat="server" Text="Tipo Documento:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList Enabled="false" ID="cmbTipoDocumento" runat="server" AutoPostBack="False">
                                                        <asp:ListItem Enabled="true" Selected="True" Value="26">Inventario Inicial</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="Label24" runat="server" CssClass="Label" Text="Estado Doc.:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cboEstadoDocumento" runat="server" CssClass="LabelRojo" Enabled="false" >
                                                    </asp:DropDownList>
                                                    <asp:Label ID="Label26" CssClass="Label" runat="server" Text="Tipo Operación:"></asp:Label>
                                                    <asp:DropDownList ID="cmbTipoOperacion" runat="server" Enabled="false">
                                                    <asp:ListItem Selected="True" Value="12">Inventario Inicial</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TituloCeldaLeft">
                                        <asp:ImageButton ID="btnAgregar" runat="server"  OnClientClick="return(onCapa('capaBuscarP'));"
                                            ImageUrl="~/Imagenes/BuscarProducto_b.JPG" 
                                            onmouseout="this.src='/Imagenes/BuscarProducto_b.JPG';" 
                                            onmouseover="this.src='/Imagenes/BuscarProducto_A.JPG';" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="DGVDetalle" runat="server" AutoGenerateColumns="False" 
                                            Width="100%">
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <Columns>
                                                <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                                <asp:BoundField DataField="IdProducto" HeaderText="IdProducto" />
                                                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" />
                                                <asp:TemplateField HeaderText="U. Medida">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="cmbUMedida" runat="server" DataTextField="NombreCortoUM" 
                                                            DataValueField="IdUnidadMedida">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cantidad">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCantidad" runat="server" 
                                                            onKeypress="return(validarNumeroPunto(event));" 
                                                            Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad")%>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Precio Compra" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCostoUnit" CssClass="TextBoxReadOnly" runat="server" onKeypress="return(false);" 
                                                            Text='<%#DataBinder.Eval(Container.DataItem,"PrecioCD")%>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="U. Medida Peso" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="cmbUMedidaPeso" runat="server" 
                                                            DataTextField="DescripcionCorto" DataValueField="Id">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Peso" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtPeso" runat="server" 
                                                            onKeypress="return(validarNumeroPunto(event));" 
                                                            Text='<%#DataBinder.Eval(Container.DataItem,"Peso")%>'></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="NomMoneda" HeaderText="Moneda" Visible="false" />
                                                <asp:BoundField DataField="TienePrecioCompra" HeaderText="" Visible="true" />
                                            </Columns>
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <RowStyle CssClass="GrillaRow" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="hddModo" runat="server" />
                        <asp:HiddenField ID="hddIdDocumento" Value="0" runat="server" />
                    </td>
                </tr>
            </table>
    </ContentTemplate> </asp:UpdatePanel>
  
    
    
    <div  id="capaBuscarP" style="border: 3px solid blue; padding: 8px; width:840px; height:auto; position:absolute; top:203px; left:32px; background-color:white; z-index:2; display :none; "        >
                        <asp:UpdatePanel ID="UpdatePanelBuscarP" runat="server">
                            <ContentTemplate>

                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="btnCerrarClientes" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaBuscarP'));" 
                                        />                                
                                </td>
                                </tr>
                                <tr>
                                <td>
                                    <asp:Label ID="Label17" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
                                    <asp:DropDownList ID="cmbLinea" runat="server" AutoPostBack="True" DataTextField="Descripcion" DataValueField="Id">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label18" runat="server" CssClass="Label" Text="Sub Línea:"></asp:Label>
                                    <asp:DropDownList ID="cmbSubLinea" runat="server" DataTextField="Nombre" DataValueField="Id" >
                                    </asp:DropDownList>                                    
                                    </td>
                                </tr>
                                <tr>
                                <td>
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripción:"></asp:Label>
                                    <asp:TextBox ID="txtDescripcionProd" runat="server" 
                                        onKeypress="return(validarEnter());" Width="300px"></asp:TextBox>
                                    <asp:ImageButton ID="btnBuscarGrilla" runat="server" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG" 
                                        onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                </td>
                                </tr>
                                <tr>
                                <td>
                                    <asp:GridView ID="DGVSelProd" runat="server" AllowPaging="True" 
                                        AutoGenerateColumns="False" Width="100%">
                                        <Columns>
                                            <asp:CommandField ButtonType="Link" SelectText="Agregar" 
                                                ShowSelectButton="True" />
                                            <asp:BoundField DataField="IdProducto" HeaderText="Código" 
                                                NullDisplayText="0" />
                                            <asp:BoundField DataField="NomProducto" HeaderText="Descripción" 
                                                NullDisplayText="---" />
                                            <asp:BoundField DataField="NomUMedida" HeaderText="U.M. Principal" 
                                                NullDisplayText="---" />
                                            <asp:BoundField DataField="NomLinea" HeaderText="Línea" NullDisplayText="---" />
                                            <asp:BoundField DataField="NomSubLinea" HeaderText="Sub Línea" 
                                                NullDisplayText="---" />                                            
                                            <asp:BoundField DataField="FlagPrecioCompra" HeaderText=""  Visible="true"
                                                NullDisplayText="---" />
                                        </Columns>
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    </asp:GridView>
                                    </td>
                                </tr>
                                                                    </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>    
</asp:Content>
