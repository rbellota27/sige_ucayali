﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmInventarioInicial
    Inherits System.Web.UI.Page
    Private listaDetalle As List(Of Entidades.DetalleDocumentoView)
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Dim objScript As New ScriptManagerClass
        Try
            cargarControles()
            verFrmInicio()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
#Region "CARGA DE DATOS"
    Private Sub cargarDatosLinea(ByVal comboBox As DropDownList)
        Dim nLinea As New Negocio.Linea
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo()
        Dim objLinea As New Entidades.Linea
        objLinea.Id = 0
        objLinea.Descripcion = "-----"
        lista.Insert(0, objLinea)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Protected Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLinea.SelectedIndexChanged
        cargarDatosSubLinea(cmbSubLinea, CInt(cmbLinea.SelectedValue))
    End Sub
    Private Sub cargarDatosSubLinea(ByVal comboBox As DropDownList, ByVal idLinea As Integer)
        Dim nSubLinea As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectAllActivoxLinea(idLinea)
        Dim objSubLinea As New Entidades.SubLinea
        objSubLinea.Id = 0
        objSubLinea.Nombre = "-----"
        lista.Insert(0, objSubLinea)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarControles()
        cargarDatosCboEmpresa(Me.cmbEmpresa)
        cargarDatosCboTienda(Me.cmbTienda)
        cargarDatosCboAlmacen(Me.cmbAlmacen)
        cargarDatosLinea(Me.cmbLinea)
        cargarDatosSubLinea(Me.cmbSubLinea, CInt(cmbLinea.SelectedValue))
        'cargarDatosCboTipoDocumento()
        'cargarDatosSerie()
        cargarDatosCboEstadoDocumento()
        cargarFechaActual()
    End Sub
    Private Sub cargarFechaActual()
        Dim objFecha As New Negocio.FechaActual
        lblFechaActual.Text = objFecha.SelectFechaActual.ToString
    End Sub
    Private Sub cargarDatosCboTipoDocumento()
        Dim cbo As New Combo
        cbo.LlenarCboTipoDocumento(Me.cmbTipoDocumento, CInt(Me.cmbEmpresa.SelectedValue), 4)
    End Sub
    Private Sub cargarDatosCboEstadoDocumento()
        Dim cbo As New Combo
        cbo.LLenarCboEstadoDocumento(Me.cboEstadoDocumento)
    End Sub
    Private Sub cargarDatosCboAlmacen(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Almacen
        cbo.DataSource = obj.SelectCboxIdTienda(CInt(cmbTienda.SelectedValue))
        cbo.DataBind()
    End Sub
    Private Sub cargarDatosCboEmpresa(ByVal cbo As DropDownList)
        Try
            Dim objCombo As New Combo
            objCombo.LlenarCboEmpresaxIdUsuario(cbo, CInt(Session("IdUsuario")), False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosCboTienda(ByVal cbo As DropDownList)
        Try
            Dim objCombo As New Combo
            objCombo.LlenarCboTiendaxIdEmpresaxIdUsuario(cbo, CInt(Me.cmbEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub cmbEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbEmpresa.SelectedIndexChanged
        cargarDatosCboTienda(Me.cmbTienda)
        cargarDatosCboAlmacen(Me.cmbAlmacen)
        'cargarDatosSerie()
    End Sub
    'Protected Sub cmbTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbTienda.SelectedIndexChanged
    '    cargarDatosCboAlmacen(Me.cmbAlmacen)
    '    cargarDatosSerie()
    'End Sub
#End Region
#Region "CONFIGURACION DEL CONTROLES"
    Private Sub cambiarBotonesControl()
        If hddModo.Value = "I" Then
            btnNuevo.Visible = True
            btnGuardar.Visible = False
            btnCancelar.Visible = False
            btnImprimir.Visible = False
        Else
            btnNuevo.Visible = False
            btnGuardar.Visible = True
            btnCancelar.Visible = True
            btnImprimir.Visible = False
        End If
    End Sub
#End Region
#Region "VISTAS FORMULARIOS"
    Private Sub verFrmEditar()
        hddModo.Value = "E"
        Panel_InvInicial.Visible = False
        cambiarBotonesControl()
    End Sub
    Private Sub verFrmInicio()
        hddModo.Value = "I"
        hddIdDocumento.Value = "0"
        cambiarBotonesControl()
        Panel_InvInicial.Visible = False
        Session.Remove("listaDetalle")
    End Sub
    Private Sub verFrmNuevo()
        hddModo.Value = "N"
        hddIdDocumento.Value = "0"
        DGVDetalle.DataSource = Nothing
        DGVDetalle.DataBind()
        Panel_InvInicial.Visible = True
        Panel_InvInicial.Enabled = True
        cambiarBotonesControl()
    End Sub
#End Region
#Region "BUSQUEDA DE PRODUCTOS"
    Protected Sub btnBuscarGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla.Click
        cargarDatosGrillaAddProd()
    End Sub
    Private Sub cargarDatosGrillaAddProd()
        Dim objScript As New ScriptManagerClass
        Try
            Dim nProducto As New Negocio.Producto
            Dim idLinea As Integer = 0
            Dim idSubLinea As Integer = 0
            If cmbLinea.SelectedValue <> "0" Then idLinea = CInt(cmbLinea.SelectedValue)
            If cmbSubLinea.SelectedValue <> "0" Then idSubLinea = CInt(cmbSubLinea.SelectedValue)
            Dim lista As List(Of Entidades.GrillaProducto_M) = nProducto.SelectUMPrincipalxIdSubLinea(idLinea, idSubLinea, txtDescripcionProd.Text.Trim)
            DGVSelProd.DataSource = lista
            DGVSelProd.DataBind()
            txtDescripcionProd.Focus()
            If lista.Count = 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la búsqueda de productos.")
        End Try
    End Sub
    Private Sub DGVSelProd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGVSelProd.PageIndexChanging
        DGVSelProd.PageIndex = e.NewPageIndex
        cargarDatosGrillaAddProd()
    End Sub
    Private Sub addProductoGrllaDetalle()
        Dim objScript As New ScriptManagerClass
        Try
            Me.saveGrillaInListaDetalle()
            Dim obj As New Entidades.DetalleDocumentoView
            Dim objPUM As New Negocio.ProductoUMView
            obj.IdProducto = CInt(DGVSelProd.SelectedRow.Cells(1).Text)
            obj.Descripcion = CStr(DGVSelProd.SelectedRow.Cells(2).Text)
            If IsNumeric(DGVSelProd.SelectedRow.Cells(6).Text) Then obj.TienePrecioCompra = CInt(DGVSelProd.SelectedRow.Cells(6).Text)
            If ProductoRepetido(obj) Then
                objScript.mostrarMsjAlerta(Me, "El producto ya ha sido ingresado.")
                Return
            End If
            obj.ListaProdUM = New List(Of Entidades.ProductoUMView)
            obj.ListaProdUM.Add(objPUM.SelectUMPrincipalxIdProducto(obj.IdProducto))
            Me.listaDetalle = Me.getListaDetalle
            Me.listaDetalle.Add(obj)
            DGVDetalle.DataSource = Me.listaDetalle
            DGVDetalle.DataBind()
            Me.setListaDetalle(Me.listaDetalle)
            objScript.offCapa(Me, "capaBuscarP")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Function ProductoRepetido(ByVal objProducto As Entidades.DetalleDocumentoView) As Boolean
        Me.listaDetalle = Me.getListaDetalle
        For i As Integer = 0 To Me.listaDetalle.Count - 1
            If Me.listaDetalle.Item(i).IdProducto = objProducto.IdProducto Then
                Return True
            End If
        Next
        Return False
    End Function
    Private Sub saveGrillaInListaDetalle()
        Me.listaDetalle = Me.getListaDetalle
        For i As Integer = 0 To DGVDetalle.Rows.Count - 1
            Dim cant As String = CType(DGVDetalle.Rows(i).Cells(4).FindControl("txtCantidad"), TextBox).Text
            Dim peso As String = CType(DGVDetalle.Rows(i).Cells(7).FindControl("txtPeso"), TextBox).Text
            Dim precio As String = CType(DGVDetalle.Rows(i).Cells(5).FindControl("txtCostoUnit"), TextBox).Text
            Dim cmbUM As DropDownList = CType(DGVDetalle.Rows(i).Cells(3).FindControl("cmbUMedida"), DropDownList)
            Dim cmbUMPeso As DropDownList = CType(DGVDetalle.Rows(i).Cells(6).FindControl("cmbUMedidaPeso"), DropDownList)
            Me.listaDetalle.Item(i).PrecioCD = CDec(IIf(IsNumeric(precio) = True, precio, Nothing))
            Me.listaDetalle.Item(i).Cantidad = CDec(IIf(IsNumeric(cant) = True, cant, Nothing))
            Me.listaDetalle.Item(i).Peso = CDec(IIf(IsNumeric(peso) = True, peso, Nothing))
            If cmbUM.SelectedItem Is Nothing Then
                Me.listaDetalle.Item(i).UM = Nothing
            Else
                Me.listaDetalle.Item(i).UM = cmbUM.SelectedItem.Text
            End If
            If cmbUMPeso.SelectedItem Is Nothing Then
                Me.listaDetalle.Item(i).UMPeso = Nothing
            Else
                Me.listaDetalle.Item(i).UMPeso = cmbUMPeso.SelectedItem.Text
            End If
            Me.listaDetalle.Item(i).IdUMedida = CInt(cmbUM.Text)
            Me.listaDetalle.Item(i).IdUMedidaPeso = cmbUMPeso.Text
        Next
        Me.setListaDetalle(Me.listaDetalle)
    End Sub
    Private Sub DGVSelProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGVSelProd.SelectedIndexChanged
        addProductoGrllaDetalle()
    End Sub
    Private Sub DGVDetalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGVDetalle.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cmbUMedida1 As DropDownList
                'Dim cmbUMedidaPeso As DropDownList
                Dim gvrow As GridViewRow = CType(e.Row.Cells(3).NamingContainer, GridViewRow)
                cmbUMedida1 = CType(gvrow.FindControl("cmbUMedida"), DropDownList)
                'cmbUMedidaPeso = CType(gvrow.FindControl("cmbUMedidaPeso"), DropDownList)
                Dim value As String = cmbUMedida1.SelectedValue
                'Dim valuePeso As String = cmbUMedidaPeso.SelectedValue
                Me.listaDetalle = Me.getListaDetalle
                If cmbUMedida1 IsNot DBNull.Value Then
                    cmbUMedida1.DataSource = Me.listaDetalle.Item(e.Row.RowIndex).ListaProdUM
                    cmbUMedida1.DataBind()
                End If
                'If cmbUMedidaPeso IsNot DBNull.Value Then
                'cmbUMedidaPeso.DataSource = Me.listaDetalle.Item(e.Row.RowIndex).ListaUMedida
                'cmbUMedidaPeso.DataBind()
                'End If
                cmbUMedida1.SelectedValue = CStr(Me.listaDetalle.Item(e.Row.RowIndex).IdUMedida)
                'cmbUMedidaPeso.SelectedValue = Me.listaDetalle.Item(e.Row.RowIndex).IdUMedidaPeso
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region "GESTION - SESSION"
    Private Function getListaDetalle() As List(Of Entidades.DetalleDocumentoView)
        Return CType(Session.Item("listaDetalle"), List(Of Entidades.DetalleDocumentoView))
    End Function
    Private Sub setListaDetalle(ByVal lista As List(Of Entidades.DetalleDocumentoView))
        Session.Remove("listaDetalle")
        Session.Add("listaDetalle", lista)
    End Sub
#End Region
#Region "MANEJO DEL DETALLE"
    Protected Sub DGVDetalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGVDetalle.SelectedIndexChanged
        quitarProductoGrllaDetalle()
    End Sub
    Private Sub quitarProductoGrllaDetalle()
        Me.saveGrillaInListaDetalle()
        Me.listaDetalle = Me.getListaDetalle
        Me.listaDetalle.RemoveAt(DGVDetalle.SelectedIndex)
        Me.setListaDetalle(Me.listaDetalle)
        DGVDetalle.DataSource = Me.listaDetalle
        DGVDetalle.DataBind()
    End Sub
#End Region
#Region "EVENTOS BOTONES DE CONTROL"
    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        Me.listaDetalle = New List(Of Entidades.DetalleDocumentoView)
        Session.Add("listaDetalle", Me.listaDetalle)
        verFrmNuevo()
    End Sub
    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        verFrmInicio()
    End Sub
#End Region
#Region "TIPO DOCUMENTO - SERIE - CODIGO"
    Protected Sub cmbTipoDocumento_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbTipoDocumento.SelectedIndexChanged
        'cargarDatosSerie()
    End Sub
    'Private Sub cargarDatosSerie()
    '    Dim cbo As New Combo
    '    cbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cmbSerie, CInt(Me.cmbEmpresa.SelectedValue), CInt(Me.cmbTienda.SelectedValue) _
    '       , CInt(Me.cmbTipoDocumento.SelectedValue))
    '    txtCodigoDocumento.Text = ""
    '    GenerarCodigo()
    'End Sub
    'Private Sub GenerarCodigo()
    '    Dim obj As New Negocio.Serie
    '    Dim objScript As New ScriptManagerClass
    '    Try
    '        'Me.txtSerie.Text = obj.GenerarSerie(CInt(Me.cboSerie.SelectedValue))

    '        If Me.cmbSerie.Items.Count = 0 Then
    '            objScript.mostrarMsjAlerta(Me, "No Existe Serie asignada para este Tipo Documento y esta Tienda")
    '        Else
    '            Me.txtCodigoDocumento.Text = obj.GenerarCodigo(CInt(Me.cmbSerie.SelectedValue))
    '        End If
    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, ex.Message)
    '    End Try
    'End Sub
    'Protected Sub cmbSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSerie.SelectedIndexChanged
    '    GenerarCodigo()
    'End Sub
#End Region
#Region "REGISTRAR INVENTARIO INICIAL"
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Dim objScript As New ScriptManagerClass
        If validarExistenciaSaldoInicial() Then
            objScript.mostrarMsjAlerta(Me, "Ya ha sido ingresado al Sistema un Saldo Inicial para esta Empresa/Almacén")
        Else
            registrar()
        End If
    End Sub
    Private Function validarExistenciaSaldoInicial() As Boolean
        Dim flag As Boolean = False
        Try
            Dim objUtil As New Negocio.Util
            Dim cont As Integer = objUtil.ValidarExistenciaxTablax3Campos("movalmacen", "idempresa", cmbEmpresa.SelectedValue, "idalmacen", cmbAlmacen.SelectedValue, "IdTipooperacion", cmbTipoOperacion.SelectedValue)
            If cont > 0 Then
                flag = True
            End If
        Catch ex As Exception
            flag = False
        End Try
        Return flag
    End Function
    Private Sub registrar()
        Me.saveGrillaInListaDetalle()
        Dim objDocumento As Entidades.Documento = obtenerDocumento()
        Dim objScript As New ScriptManagerClass
        Dim objDocumento_N As New Negocio.Documento
        Me.listaDetalle = Me.getListaDetalle()
        Try
            Dim listaDetalleGuia As New List(Of Entidades.DetalleGuia)
            Dim objDetalleDocView As Entidades.DetalleDocumentoView
            For Each objDetalleDocView In Me.listaDetalle
                Dim objDetalleGuia As New Entidades.DetalleGuia
                With objDetalleGuia
                    .Cantidad = objDetalleDocView.Cantidad
                    .IdDocumento = Nothing
                    .IdProducto = objDetalleDocView.IdProducto
                    .Peso = Nothing
                    .UMedida = objDetalleDocView.UM
                    .IdUnidadMedida = CInt(objDetalleDocView.IdUMedida)
                    .UMPeso = Nothing
                    .CostoUnitSaldoInicial = objDetalleDocView.PrecioCD
                End With
                listaDetalleGuia.Add(objDetalleGuia)
            Next
            Dim idDocumento As Integer = objDocumento_N.InsertaDocumentoT(objDocumento, listaDetalleGuia, True, 1, CInt(cmbAlmacen.SelectedValue), 1, False)
            If idDocumento <> -1 Then
                objScript.mostrarMsjAlerta(Me, "La Operación se realizó con éxito.")
                hddIdDocumento.Value = idDocumento.ToString
                btnImprimir.Visible = True
                btnGuardar.Visible = False
                Panel_InvInicial.Enabled = False
            Else
                objScript.mostrarMsjAlerta(Me, "Se presentaron problemas en la operación.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la operación.")
        End Try
    End Sub
    Private Function obtenerDocumento() As Entidades.Documento
        Dim objDoc As New Entidades.Documento
        objDoc.Codigo = Nothing
        objDoc.Descuento = Nothing
        objDoc.ExportadoConta = "0"
        objDoc.FechaAEntregar = Nothing
        objDoc.FechaEmision = Nothing
        objDoc.FechaEntrega = Nothing
        objDoc.FechaIniTraslado = Nothing
        objDoc.FechaRegistro = CDate(lblFechaActual.Text)
        objDoc.FechaVenc = Nothing
        objDoc.Id = Nothing
        objDoc.IdChofer = Nothing
        objDoc.IdCondicionPago = Nothing
        objDoc.IdDestinatario = Nothing
        objDoc.IdEmpresa = CInt(cmbEmpresa.SelectedValue)
        objDoc.IdEstadoDoc = CInt(cboEstadoDocumento.SelectedValue)
        objDoc.IdMoneda = Nothing
        objDoc.IdMotivoT = Nothing
        objDoc.IdPersona = Nothing
        objDoc.IdRemitente = Nothing
        objDoc.IdSerie = Nothing
        objDoc.IdTienda = CInt(cmbTienda.SelectedValue)
        objDoc.IdTipoDocumento = CInt(cmbTipoDocumento.SelectedValue)
        objDoc.IdTipoOperacion = CInt(cmbTipoOperacion.SelectedValue)
        objDoc.IdTransportista = Nothing
        objDoc.IdUsuario = Nothing
        objDoc.IdVehiculo = Nothing
        objDoc.IGV = Nothing
        objDoc.ImporteTotal = Nothing
        objDoc.LugarEntrega = Nothing
        objDoc.NroVoucherConta = Nothing
        objDoc.Serie = Nothing
        objDoc.SubTotal = Nothing
        objDoc.Total = Nothing
        objDoc.TotalAPagar = Nothing
        objDoc.TotalLetras = Nothing
        objDoc.Utilidad = Nothing
        objDoc.ValorReferencial = Nothing
        Return objDoc
    End Function
#End Region

    Protected Sub cmbTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbTienda.SelectedIndexChanged
        Try
            cargarDatosCboAlmacen(cmbAlmacen)
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        Response.Redirect("~/Reportes/visor.aspx?iReporte=1&IdDocumento=" & hddIdDocumento.Value, True)
    End Sub
    'Private Sub imprimirDocumento(ByVal idDocumento As Integer)
    '    Try
    '        Dim reporte As New CR_DocInventarioInicial
    '        Dim objReporte As New Negocio.Reportes
    '        reporte.SetDataSource(objReporte.getDataSetDocInventarioInicial(idDocumento))
    '        CRV.ReportSource = reporte
    '        CRV.PrintMode = CrystalDecisions.Web.PrintMode.Pdf
    '        'orientacion
    '        reporte.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape
    '        'papel
    '        reporte.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
    '        'impresion de numero de copias y paginas indicadas en 3 textbox
    '        reporte.PrintToPrinter(1, True, 0, 0)
    '    Catch ex As Exception
    '        objScript.mostrarMsjAlerta(Me, "Problemas en la impresión.")
    '    End Try
    'End Sub
End Class