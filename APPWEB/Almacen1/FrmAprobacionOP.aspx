<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmAprobacionOP.aspx.vb" Inherits="APPWEB.FrmAprobacionOP" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td class="TituloCelda">
                APROBACI�N - ORDEN DE PEDIDO
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:UpdatePanel ID="upnPrincipal" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Empresa:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Texto" style="font-weight: bold">
                                                Tienda:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="Texto" style="font-weight: bold">
                                                Almac�n:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboAlmacen" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="TituloCelda">
                                    ORDEN DE PEDIDO
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <fieldset class="FieldSetPanel">
                                        <legend>Filtro</legend>
                                        <table>
                                            <tr>
                                                <td style="text-align: right">
                                                    Filtrar por:
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="rdbFiltroFecha" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="0" Selected="True">Por Rango de Fechas</asp:ListItem>
                                                        <asp:ListItem Value="1">Por Semanas</asp:ListItem>
                                                        <asp:ListItem Value="2">Por N�mero Documento</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    Fecha Inicio:
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtFechaInicio" runat="server" onblur="return(  valFecha(this) );"
                                                                    Width="100px"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                                                                </cc1:MaskedEditExtender>
                                                                <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Enabled="True"
                                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                                                                </cc1:CalendarExtender>
                                                            </td>
                                                            <td>
                                                                Fecha Fin:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtFechaFin" runat="server" onblur="return(  valFecha(this) );"
                                                                    Width="100px"></asp:TextBox>
                                                                <cc1:MaskedEditExtender ID="txtFechaFin_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin">
                                                                </cc1:MaskedEditExtender>
                                                                <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Enabled="True"
                                                                    Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                                                                </cc1:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    A�o:
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList ID="cboAnio" runat="server" Width="100px" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                &nbsp;&nbsp;&nbsp;Semana:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboSemana" runat="server" Width="100px" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                Inicio:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtFechaInicio_ProgSemana" ReadOnly="true" runat="server" Width="90px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                Fin:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtFechaFin_ProgSemana" ReadOnly="true" runat="server" Width="90px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    Serie:
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtSerie" onFocus=" return(  aceptarFoco(this) ); " onKeyPress=" return(  validarNumeroPuntoPositivo('event')  ); "
                                                                    runat="server" Width="100px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;C�digo:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtCodigo" runat="server" onFocus=" return(  aceptarFoco(this) ); "
                                                                    onKeyPress=" return(  validarNumeroPuntoPositivo('event')  ); " Width="100px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    Estado:
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList ID="cboEstadoAprobacion" runat="server">
                                                                    <asp:ListItem Value="0" Selected="True">Pedidos por Aprobar</asp:ListItem>
                                                                    <asp:ListItem Value="1">Pedidos Aprobados</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="80px" ToolTip="Buscar pedidos"
                                                                    OnClientClick="return(  valOnClick_btnBuscar() );" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_DocumentoOP" runat="server" AutoGenerateColumns="False" Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SelectText="Seleccionar" ShowSelectButton="True" />
                                            <asp:BoundField DataField="TipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                                ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="FechaEmision" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Empresa" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Empresa" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Tienda" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Tienda" ItemStyle-HorizontalAlign="Center" />
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr class="SubTituloCelda">
                                <td>
                                    DOCUMENTO DE REFERENCIA
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SelectText="Ver Detalle" ShowSelectButton="True" />
                                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red"
                                                HeaderText="Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                    <asp:GridView ID="GV_DocumentoRef_Cab" runat="server" AutoGenerateColumns="False"
                                        Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Descripci�n" DataField="NomAlmacen" ItemStyle-Font-Bold="true"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:BoundField HeaderText="R.U.C." DataField="NomCondicionPago" ItemStyle-Font-Bold="true"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:BoundField HeaderText="D.N.I." DataField="NomEmpleado" ItemStyle-Font-Bold="true"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:BoundField HeaderText="Tipo Precio" DataField="NomEmpresaTomaInv" ItemStyle-Font-Bold="true"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                HeaderText="Total" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblMoneda" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"NomMoneda")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblTotal" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"TotalAPagar","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                    <asp:GridView ID="GV_DocumentoRef_Det" runat="server" AutoGenerateColumns="False"
                                        Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:BoundField HeaderText="C�d." DataField="CodigoProducto" ItemStyle-Font-Bold="true"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:BoundField HeaderText="Producto" DataField="NomProducto" ItemStyle-Font-Bold="true"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:BoundField HeaderText="Cantidad" DataField="Cantidad" DataFormatString="{0:F3}"
                                                ItemStyle-Font-Bold="true" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                            <asp:BoundField HeaderText="U.M." DataField="UMedida" ItemStyle-Font-Bold="true"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:BoundField HeaderText="P. Venta" DataField="PrecioCD" DataFormatString="{0:F3}"
                                                ItemStyle-Font-Bold="true" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                            <asp:BoundField HeaderText="Importe" DataField="Importe" DataFormatString="{0:F3}"
                                                ItemStyle-Font-Bold="true" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td class="SubTituloCelda">
                                    Documento
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%">
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>
                                            <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo" HeaderStyle-Height="25px"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Producto" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderText="Cantidad Ref." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblCantidadRef" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad_Ref","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblUnidadMedida_CantidadRef" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida_CantidadRef")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cantidad Pedido" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblCantidadPedida" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad_OP","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblUnidadMedida_CantidadPedida" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida_Cantidad_OP")%>'></asp:Label>
                                                            </td>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cantidad Aprobada" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="txtCantidadAprobar" Font-Bold="true" runat="server" TabIndex="500"
                                                                    Width="90px" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad_Aprobada","{0:F3}")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblUnidadMedida_CantidadAprobada" Font-Bold="true" runat="server"
                                                                    Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida_Cantidad_OP")%>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                                            </td>
                                                            <td>
                                                                <asp:HiddenField ID="hddIdDetalleDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDetalleDocumento")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="GV_DocumentoOP" />
                        <asp:PostBackTrigger ControlID="btnBuscar" />
                        <asp:AsyncPostBackTrigger ControlID="btnGuardar" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upnFiltGpD" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td valign="top">
                        <table>
                            <tr>
                                <td class="Label" valign="top">
                                    <asp:Label ID="lblTipoAlmacen" runat="server" Text="Tipo Almacen:"></asp:Label>
                                    <asp:DropDownList ID="cboTipoAlmacen" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td valign="top">
                                    <asp:ImageButton ID="btnAddTipoAlmacen" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                        onmouseout="this.src='/Imagenes/Agregar_B.JPG';" onmouseover="this.src='/Imagenes/Agregar_A.JPG';" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top">
                                    <asp:GridView ID="gvwTipoAlmacen" runat="server" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdfIdTipoAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoAlmacen")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Tipoalm_Nombre" HeaderText="Tipo Almacen" HeaderStyle-Height="25px"
                                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnEliminar" runat="server" OnClick="btnEliminar_Click">Quitar</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td colspan="3">
                                    <asp:RadioButtonList ID="rbtFechaOSemana" runat="server" RepeatDirection="Horizontal"
                                        CssClass="Label_fsp">
                                        <asp:ListItem Value="0" Selected="True">Por Rango de Fechas</asp:ListItem>
                                        <asp:ListItem Value="1">Por Semanas</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="Label_fsp">
                                    <asp:Label ID="lblDe" runat="server" Text="DE:"></asp:Label>
                                </td>
                                <td class="Label_fsp">
                                    <asp:Label ID="lblanoi" runat="server" Text="A�o:"></asp:Label>
                                    <asp:DropDownList ID="idYearI" runat="Server" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblSemanai" runat="server" Text="Semana:"></asp:Label>
                                    <asp:DropDownList ID="idSemanaI" runat="Server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                                <td class="Label">
                                    <asp:Label ID="lblFechaInicioSemanaI" runat="server" Text=" Fecha Inicio"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaInicioSemanaI" runat="server" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td class="Label">
                                    <asp:Label ID="lblFechaFinSemanaI" runat="server" Text=" Fecha fin"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFechaFinSemanaI" runat="server" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="Label_fsp">
                                    <asp:Label ID="lblHasta" runat="server" Text="HASTA:"></asp:Label>
                                </td>
                                <td class="Label_fsp">
                                    <asp:Label ID="lblanof" runat="server" Text="A�o:"></asp:Label>
                                    <asp:DropDownList ID="idYearF" runat="Server" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblSemanaf" runat="server" Text="Semana:"></asp:Label>
                                    <asp:DropDownList ID="idSemanaF" runat="Server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                                <td class="Label">
                                    <asp:Label ID="lblFechaInicioSemanaFin" runat="server" Text=" Fecha Inicio"></asp:Label>
                                </td>
                                <td class="Label">
                                    <asp:TextBox ID="txtFechaInicioSemanaFin" runat="server" ReadOnly="true"></asp:TextBox>
                                </td>
                                <td class="Label">
                                    <asp:Label ID="lblFechaFinSemanaFin" runat="server" Text=" Fecha Fin"></asp:Label>
                                </td>
                                <td class="Label">
                                    <asp:TextBox ID="txtFechaFinSemanafin" runat="server" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table>
                                        <tr>
                                            <td class="Label_fsp">
                                                <asp:Label ID="lblFechaI" runat="server" Text="Fecha Inicio:"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio1" runat="server" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio1">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio1">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Label_fsp">
                                                <asp:Label ID="lblFechaf" runat="server" Text="Fecha Fin:"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin1" runat="server" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin1">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin1">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="LabelLeft">
                        <asp:CheckBox ID="chbStockxTienda" Visible="false" runat="server" Text="Stock en Transito por Tienda" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>

                                <td valign="top">
                                    <asp:Button ID="btnMostrarReporte" runat="server" Text="Mostrar Reporte" Width="200px"
                                        OnClientClick="return(  ValidaMostrar()  );" />
                                </td>
                                <td valign="top">
                                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar Cantidades Aprobadas" Width="200px"
                                        OnClientClick="return(  GurdarCantAn()  );" />
                                </td>
                                <td valign="top">
                                    <asp:Button ID="btnExpToExcel" runat="server" Text="Exportar a Excel" Width="200px"
                                        OnClientClick="return(  ValExportarExcel()  );" />
                                </td>

                                <td valign="top">
                                    <asp:CheckBox ID="checkGuardaCopiaPed" CssClass="Texto" Font-Bold="true" runat="server"
                                        onClick=" return( valOnClick_chb_CopyCantidadPedidaGped(this) ); " TextAlign="Left"
                                        Text="Copiar Cantidad Pedida - Cantidad Aprobada" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnAprob" runat="server" UpdateMode="Conditional" RenderMode="Block">
        <ContentTemplate>
            <table>
                <tr>
                    <td class="TituloCelda">
                        <asp:Label ID ="lblTituloRpte" runat ="server" Text ="Ventas Mensuales y Stock"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="d1" style="overflow: auto; width: 950px; height: 350px">
                            <asp:GridView ID="gvwAproped" runat="server" AutoGenerateColumns="false" HorizontalAlign="Justify"
                                Width="4000px" >
                                <HeaderStyle CssClass="GrillaHeader"  />
                                <FooterStyle CssClass="GrillaFooter" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                        <asp:HiddenField ID="hdfProdCapAprob" runat="server" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                        <asp:HiddenField ID="hdfIdDocumento" runat="server" />
                    </td>
                    <td>
                        <asp:HiddenField ID="hdfNroOp" runat="server" />
                        &nbsp;
                    </td>
                    <td>
                        <asp:HiddenField ID="hdfConvPed" runat="server" />
                    </td>
                    <td> 
                    <asp:HiddenField ID ="hdfNvaCobertura" runat ="server" />
                    </td>
                    
                    <td> 
                     <asp:HiddenField ID ="hdfSaldotienda" runat ="server"     />
                    </td>
                    
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnMostrarReporte" EventName="Click" />
            <asp:PostBackTrigger ControlID="btnExpToExcel" />
        </Triggers>
        
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        function SiguienteObjeto(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) caracter = 9;
        }

        function AcceptNum(evt) {

            var nav4 = window.Event ? true : false;

            var key = nav4 ? evt.which : evt.keyCode;

            return (key <= 13 || (key >= 48 && key <= 57) || key == 44);

        }

        function ValExportarExcel() {
            var grillaanped = document.getElementById('<%=gvwAproped.ClientID%>');
            if (grillaanped == null) {
                alert("No hay registros Para Exportar");
                return false;
            }
            return true;
        }

        function ValidaTipoAlmacen() {
            var grillaTipoalmacen = document.getElementById('<%=gvwTipoAlmacen.ClientID%>');
            if (grillaTipoalmacen == null) {
                alert('debe seleccionar al menos un tipo de almacen');
                return false;
            }
        }

        //ocurre cuando se chequea el check de copiar cantidad

        function valOnClick_chb_CopyCantidadPedidaGped(chbCopy) {
            var grilla = document.getElementById('<%=gvwAproped.ClientID%>');
            var pos = document.getElementById('<%=hdfNroOp.ClientID%>').value;
            var posPedAproConv = parseFloat(pos) + 1;
            var posPedAproconvertido = parseFloat(pos) - 2;

            var posTiendaPide = parseFloat(pos) - 5;
            var posNvaCober = parseFloat(pos) + 2;
            var posCoberActual = parseFloat(pos) - 8;
            var posMedd = parseFloat(pos) + 5;

            var posTiendaDistribuye = parseFloat(pos) - 6;
            
            var posSaldoTiendaDistribuye = parseFloat(pos) + 4;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];

                    var txtCantidadAprobada = rowElem.cells[pos].children[0];
                    var colPedAprobConv = rowElem.cells[posPedAproConv];
                    var cantActualTiendaPide = rowElem.cells[posTiendaPide].innerHTML;
                    //var CantNvaCover = rowElem.cells[posNvaCober].innerHTML;
                    //var CantMedd = rowElem.cells[posMedd].innerHTML;
                    var CantPedido = parseFloat(rowElem.cells[4].innerHTML);
                    var CantTiendaDistribuye = rowElem.cells[posTiendaDistribuye].innerHTML;
                    //var CantSaldoTiendaDistribuye = rowElem.cells[posSaldoTiendaDistribuye].innerHTML;

                    if (chbCopy.status) {
                        txtCantidadAprobada.value = parseFloat(CantPedido);
                        rowElem.cells[posPedAproConv].innerHTML = rowElem.cells[posPedAproconvertido].innerHTML;

                        //rowElem.cells[posSaldoTiendaDistribuye].innerHTML = (parseFloat(CantTiendaDistribuye) - parseFloat(txtCantidadAprobada.value))


//                        if (CantMedd != 0) {
//                        rowElem.cells[posNvaCober].innerHTML= redondear((parseFloat(txtCantidadAprobada.value) + parseFloat(cantActualTiendaPide)) / parseFloat(CantMedd));
//                      
//                        } else {
//                            rowElem.cells[posNvaCober].innerHTML = 0;
//                        }
                        
                           aprobPedConv();
                        //CopyCantidadPedidaGrDoc(CantPedido, i);

                    } else {
                        txtCantidadAprobada.value = 0;
                        //rowElem.cells[posSaldoTiendaDistribuye].innerHTML = parseFloat(CantTiendaDistribuye)
                        rowElem.cells[posPedAproConv].innerHTML = 0;

//                        if (CantMedd != 0) {
//                            rowElem.cells[posNvaCober].innerHTML = rowElem.cells[posCoberActual].innerHTML;

//                        } else {
//                            rowElem.cells[posNvaCober].innerHTML = 0;
//                        }
                        
                    }
                }
            }
            return true;
        }

        //se ejecuta al soltar la tecla calcula.

        function aprobPedConv() {
            var grilla = document.getElementById('<%=gvwAproped.ClientID%>');
            var pos = document.getElementById('<%=hdfNroOp.ClientID%>').value;
            var nro = parseFloat(pos) + parseFloat(1);

            var posTiendaPide2 = parseFloat(pos) - 5;
            var posNvaCober2 = parseFloat(pos) + 2;
            var posCoberActual2 = parseFloat(pos) - 8;
            var posMedd2 = parseFloat(pos) + 5;

            var posTiendaDistribuye2 = parseFloat(pos) - 6;

            var posSaldoTiendaDistribuye2 = parseFloat(pos) + 4;
            
            
            var cad = '';
            var cadConped = '';
            var CadNvaCober = '';
            var CadSaldoTienda = '';
            
            if (grilla != null) {
                for (var j = 1; j < grilla.rows.length; j++) {
                    var rowElem = grilla.rows[j];

                    var idprod = parseFloat(rowElem.cells[2].innerHTML);
                    var CantSol = parseFloat(rowElem.cells[4].innerHTML);
                    var PosFila = parseFloat(rowElem.cells[0].innerHTML);

                    var cantActualTiendaPide2 = rowElem.cells[posTiendaPide2].innerHTML;
                    if (isNaN(cantActualTiendaPide2)) {
                        cantActualTiendaPide2 = 0;
                    }

                    var CantNvaCover2 = 0;  //rowElem.cells[posNvaCober2].innerHTML;
                    if (isNaN(CantNvaCover2)) {
                        CantNvaCover2 = 0;
                    }

                    var CantMedd2 = 0;  //rowElem.cells[posMedd2].innerHTML;
                    if (isNaN(CantMedd2)) {
                        CantMedd2 = 0;
                    }

                    var CantTiendaDistribuye2 = rowElem.cells[posTiendaDistribuye2].innerHTML;
                    if (isNaN(CantTiendaDistribuye2)) {
                        CantTiendaDistribuye2 = 0;
                    }

                  
                    var CantCoberActual2 = rowElem.cells[posCoberActual2].innerHTML;
                    if (isNaN(CantCoberActual2)) {
                        CantCoberActual2 = 0;
                    }
                    
                    if (isNaN(CantSol)) {
                        CantSol = 0;
                    } else {
                        CantSol = parseFloat(rowElem.cells[4].innerHTML);
                    }

                    if (parseFloat(CantSol) > 0) {
                            var cantSolConv = redondear(parseFloat(rowElem.cells[6].innerHTML), 2);
                            if (isNaN(cantSolConv)) {
                                cantSolConv = 0;
                            } else {
                                cantSolConv = parseFloat(rowElem.cells[6].innerHTML);
                            }

                            var CantPedAprob = parseFloat(rowElem.cells[pos].children[0].value);
                            if (isNaN(CantPedAprob)) {
                               CantPedAprob = 0;
                               rowElem.cells[posNvaCober2].innerHTML = CantCoberActual2;
                                
                            } else {

                                CantPedAprob = parseFloat(rowElem.cells[pos].children[0].value);
//                                //nueva covertura
//                                if (parseFloat(CantMedd2) != 0) {
//                                    rowElem.cells[posNvaCober2].innerHTML = redondear((parseFloat(CantPedAprob) + parseFloat(cantActualTiendaPide2)) / parseFloat(CantMedd2),2);
//                                } else {
//                                    rowElem.cells[posNvaCober2].innerHTML = 0;
//                                }
                                //rowElem.cells[posSaldoTiendaDistribuye2].innerHTML = parseFloat(CantTiendaDistribuye2) - CantPedAprob;
                                                                
                                //CopyCantidadPedidaGrDoc(CantPedAprob, PosFila);
                            }

                        var hdfPCA = document.getElementById('<%=hdfProdCapAprob.ClientID%>');
                        cad = cad + ';' + idprod + ',' + CantPedAprob;
                        hdfPCA.value = cad;

                        
                        var hdfCadNvaCober = document.getElementById('<%=hdfNvaCobertura.ClientID%>');
                        //CadNvaCober = CadNvaCober + ';' + rowElem.cells[posNvaCober2].innerHTML;
                        hdfCadNvaCober.value = CadNvaCober;
                        
                        var hdfSaldotienda = document.getElementById('<%=hdfSaldotienda.ClientID%>');
                       // CadSaldoTienda = CadSaldoTienda + ';' + rowElem.cells[posSaldoTiendaDistribuye2].innerHTML;
                        hdfSaldotienda.value = CadSaldoTienda; 

                        var hdfConPed = document.getElementById('<%=hdfConvPed.ClientID%>');
                        var CantSolAprobConv = (((parseFloat(cantSolConv) * parseFloat(CantPedAprob)) / parseFloat(CantSol)));
                        
                        if (isNaN(CantSolAprobConv)) {
                            rowElem.cells[nro].innerHTML = 0;
                        } else {
                        rowElem.cells[nro].innerHTML = CantSolAprobConv;
                        }
                        // cadConped = cadConped + ';' + idprod + ',' + CantSolAprobConv;
                        cadConped = cadConped + ';' + idprod + ',' + CantSolAprobConv;
                        hdfConPed.value = cadConped;
                        
                    
                    } else {
                    rowElem.cells[nro].innerHTML = 0;
                        
                    }
                }
            }
            else {
                confirm('No hay elementos')

            }


        }

        //copia la cantidad digitada de la caja de texto en la grilla a la grilla documento

        function CopyCantidadPedidaGrDoc(cantidad, posicion) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    var txtCantidadAprobada = rowElem.cells[4].children[0].cells[0].children[0];
                    var txttt = rowElem.cells[4].children[0].cells[0].children[0].value;
                    var lblCantidadPedida = rowElem.cells[3].children[0].cells[0].children[0];
                    var lvllll = rowElem.cells[3].children[0].cells[0].children[0].value;
                    if (posicion == i) {
                        txtCantidadAprobada.value = cantidad;
                        txtCantidadAprobada.innerHTML = cantidad;
                    } else {
                        txtCantidadAprobada.innerHTML = 0;
                    }

                }
            }
            return true;
        }




        function valOnClick_chb_CopyCantidadPedida(chbCopy) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var pos = document.getElementById('<%=hdfNroOp.ClientID%>').value;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    var txtCantidadAprobada = rowElem.cells[4].children[0].cells[0].children[0];
                    var lblCantidadPedida = rowElem.cells[3].children[0].cells[0].children[0];
                    if (chbCopy.status) {
                        txtCantidadAprobada.value = parseFloat(lblCantidadPedida.innerHTML);
                        habilitarFrm(false);

                    } else {
                        txtCantidadAprobada.value = 0;
                        habilitarFrm(true);
                    }
                }
            }
            return true;
        }



        function ValidaMostrar() {

            var grillaan = document.getElementById('<%=GV_Detalle.ClientID%>');
            var grillaTipoalmacen = document.getElementById('<%=gvwTipoAlmacen.ClientID%>');

            if (grillaan != null) {
                if (grillaTipoalmacen == null) {
                    alert('debe seleccionar al menos un tipo de almacen');
                    return false;
                } else {
                    return true;
                }

            } else {
                alert("Debe Selecionar un Documento ");
                return false;
            }
        }


        function GurdarCantAn() {

            var grillaan = document.getElementById('<%=gvwAproped.ClientID%>');
            if (grillaan != null) {
             return confirm('Desea continuar con la Operaci�n ?');
                         
            } if (grillaan == null) {
                alert("No hay Elementos");
                return false;
            }
           
        }


        function DesCheck() {
            var check = document.getElementById('<%=gvwAproped.ClientID%>');
            if (check.status = true) {
                check.status = false;
            }
        }



        function habilitarFrm(flag) {
            var btn = document.getElementById('<%=btnAddTipoAlmacen.ClientID%>');
            var btnm = document.getElementById('<%=btnMostrarReporte.ClientID%>');

            if (btnGd != null) {
                btnGd.disabled = (flag);
            }
            var cboTa = document.getElementById('<%=cboTipoAlmacen.ClientID%>');
            var rbt = document.getElementById('<%=rbtFechaOSemana.ClientID%>');
            var cboyi = document.getElementById('<%=idYearI.ClientID%>');
            var cbosi = document.getElementById('<%=idSemanaI.ClientID%>');
            var cboyf = document.getElementById('<%=idYearF.ClientID%>');
            var cbosf = document.getElementById('<%=idSemanaF.ClientID%>');
            var cbofi = document.getElementById('<%=txtFechaInicio1.ClientID%>');
            var cboff = document.getElementById('<%=txtFechaFin1.ClientID%>');
            var chkt = document.getElementById('<%=chbStockxTienda.ClientID%>');
            var btnGA = document.getElementById('<%=btnGuardar.ClientID%>');
            if (btnGA != null) {
                btnGA.disabled = !(flag);
            }
            var btnEex = document.getElementById('<%=btnExpToExcel.ClientID%>');

            btn.disabled = !(flag);
            btnm.disabled = !(flag);
            cboTa.disabled = !(flag);
            rbt.disabled = !(flag);
            cboyi.disabled = !(flag);
            cbosi.disabled = !(flag);
            cboyf.disabled = !(flag);
            cbosf.disabled = !(flag);
            cbofi.disabled = !(flag);
            cboff.disabled = !(flag);
            chkt.disabled = !(flag);
            btnEex.disabled = !(flag);
            var grillaTa = document.getElementById('<%=gvwTipoAlmacen.ClientID%>');
            if (grillaTa != null) {
                grillaTa.disabled = !(flag)
            }


            var grillaan = document.getElementById('<%=gvwAproped.ClientID%>');
            var pos = document.getElementById('<%=hdfNroOp.ClientID%>').value;
            if (grillaan != null) {
                for (var j = 1; j < grillaan.rows.lenght; j++) {
                    var rowElem = grillaan.rows[j];
                    var cantAprob = rowElem.cells(pos).children[0];
                    cantAprob.disabled = !(flag)
                }
                grillaan.disabled = !(flag)
            }
        }

        function valOnClick_btnBuscar() {
            var rdbOpcion = document.getElementById('<%=rdbFiltroFecha.ClientID%>');
            var radioFiltroFecha = rdbOpcion.getElementsByTagName("INPUT");
            if (radioFiltroFecha[2].checked) {
                //**** Opci�n x Nro Documento

                var txtSerie = document.getElementById('<%=txtSerie.ClientID%>');
                if (txtSerie.value.length <= 0) {
                    alert('DEBE INGRESAR UN NRO. DE SERIE. NO SE PERMITE LA OPERACI�N.');
                    txtSerie.select();
                    txtSerie.focus();
                    return false;
                }

                var txtCodigo = document.getElementById('<%=txtCodigo.ClientID%>');
                if (txtCodigo.value.length <= 0) {
                    alert('DEBE INGRESAR UN NRO. DE DOCUMENTO. NO SE PERMITE LA OPERACI�N.');
                    txtCodigo.select();
                    txtCodigo.focus();
                    return false;
                }

            }
            return true;
        }
        
        
    </script>

</asp:Content>
