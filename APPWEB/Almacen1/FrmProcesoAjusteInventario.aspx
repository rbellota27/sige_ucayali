<%@ Page Title="" Language="vb" AutoEventWireup="false"   MasterPageFile="~/Principal.Master"   CodeBehind="FrmProcesoAjusteInventario.aspx.vb" Inherits="APPWEB.FrmProcesoAjusteInventario" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" Width="80px" runat="server" Text="Nuevo" ToolTip="Generar un Doc. Ajuste de Inventario desde un Doc. Toma de Inventario." OnClientClick="  return(  valNuevo()     );" />
                          
                          
                          
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" Width="80px" OnClientClick=" return( valEditar()  );  " runat="server" Text="Editar" ToolTip="Editar" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" Width="80px" runat="server" Text="Buscar" ToolTip="Buscar" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" Width="80px" runat="server" Text="Anular" ToolTip="Anular Doc. Ajuste de Inventario" OnClientClick="return(  valAnular()  );" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" Width="80px" runat="server" Text="Guardar" ToolTip="Guardar" OnClientClick="return(  valSave() );" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" Width="80px" OnClientClick="return(   valImprimir()  );" runat="server" Text="Imprimir" ToolTip="Imprimir Documento" />
                          
                          
                          
                        </td>
                        <td>
                            <asp:Button ID="btnCancelar" Width="80px" runat="server" Text="Cancelar" ToolTip="Cancelar" OnClientClick="return(  confirm('Desea cancelar la operaci�n actual ?')       );" />
                        </td>
                        <td>
                            &nbsp;</td>
                        <td style="width:100%" align="right">
                        <table>
                <tr>
                    <td class="Texto">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="background-color:Yellow;">
     
        <td class="LabelRojo" style="font-weight:bold">
        <table>
        <tr >
        <td>Empresa seleccionada para el Proceso de Ajuste :</td>
        <td><%=Me.nomEmpresaAjuste%></td>
        </tr>
        </table>
           
        </td>
        
        </tr>
        <tr>
        <td class="TituloCelda">Proceso de Ajuste de Inventario</td>
        </tr>        
        <tr>
            <td>
            
                <asp:Panel ID="Panel_Cab" runat="server">
                
                <table>
                    <tr>
                        <td class="Label">
                            Empresa:</td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            Tienda:</td>
                        <td>
                            <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            Serie:</td>
                        <td>
                            <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" 
                                onKeypress="return(  onKeyPressEsNumero('event')  );" Width="100px"></asp:TextBox>
                            </td>
                        <td>
                            <asp:Button ID="btnBuscarDocumento" Width="135px" runat="server" 
                                OnClientClick="return(valBuscarDocumento());" Text="Buscar Documento" 
                                ToolTip="Buscar Documento" />
                        </td>
                    </tr>
                    <tr>
                        <td class="Label">
                            Tipo Doc.:</td>
                        <td>
                            <asp:DropDownList ID="cboTipoDocumento" runat="server">
                                <asp:ListItem Value="29">Doc. Ajuste de Inventario</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            Tipo Operaci�n:</td>
                        <td>
                            <asp:DropDownList ID="cboTipoOperacion" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            Estado:</td>
                        <td>
                          
                            <asp:DropDownList ID="cboEstado" runat="server" Enabled="false">
                            </asp:DropDownList>
                            </td>
                        <td  class="Texto">
                            Fecha Emisi�n:</td>
                        <td>
                            <asp:TextBox ID="txtFechaEmision" Enabled="false" runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(  valFecha(this) );" Width="100px"></asp:TextBox>
                                
                                
                                
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaEmision">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                            </cc1:CalendarExtender>
                                
                                
                                
                        </td>
                    </tr>
                    <tr>
                        <td class="Label">
                            Moneda:</td>
                        <td>
                            <asp:DropDownList CssClass="LabelRojo" ID="cboMoneda" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td class="Label">
                            &nbsp;</td>
                        <td>
                          
                            &nbsp;</td>
                        <td  class="Texto">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    </table>
                </asp:Panel>
            
                
                 
                 
                 
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td  class="Texto" style="text-align: right">
                            Doc. Toma de Inventario:</td>
                        <td style="width:360px">
                            <asp:Label CssClass="LabelRojo" Font-Bold="true" ID="lblNroDocTomaInventario" runat="server" Text=""></asp:Label>
                        </td>
                        <td > 
                            &nbsp;</td>
                        <td  class="Texto">
                            Fecha Emisi�n: </td>
                            <td>
                                <asp:Label ID="lblFechaEmisionDocTomaInv" CssClass="LabelRojo" Font-Bold="true"  runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td  class="Texto" style="text-align: right">
                            Encargado de la Toma de Inventario:</td>
                        <td>
                            <asp:Label ID="lblNomEmpleado" CssClass="LabelRojo" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td style="text-align: right" class="Texto">
                            Empresa:</td>
                        <td>
                          
                            <asp:Label ID="lblNomEmpresaTomaInv" runat="server" CssClass="LabelRojo" 
                                Font-Bold="true" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td  class="Texto" style="text-align: right">
                            Fecha de Toma de Inventario:</td>
                        <td>
                            <asp:Label CssClass="LabelRojo" Font-Bold="true" ID="lblFechaTomaInv" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td style="text-align: right" class="Texto">
                            Almac�n:</td>
                        <td>
                          
                            <asp:Label ID="lblNomAlmacen" runat="server" CssClass="LabelRojo" Font-Bold="true" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td  class="Texto" style="text-align: right">
                            Estado Doc. Toma Inventario:</td>
                        <td>
                            <asp:Label CssClass="LabelRojo" Font-Bold="true" ID="lblNomEstadoDocTomaInv" 
                                runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td style="text-align: right" class="Texto">
                            &nbsp;</td>
                        <td>
                          
                            &nbsp;</td>
                    </tr>
                    </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Det" runat="server">
                <table style="width: 100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="Texto">
                                        L�nea:</td>
                                    <td>
                                        <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="True"
                                             DataTextField="Descripcion" DataValueField="Id"    >
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Texto">
                                        Sub L�nea:</td>
                                    <td>
                                        <asp:DropDownList ID="cboSubLinea" runat="server" DataTextField="Nombre" 
                                DataValueField="Id"    >
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnMostrarDatos" Width="150px" runat="server" Text="Mostrar Datos" OnClientClick="return(  valMostrarDatos() );" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnRecalcularSaldos" Width="150px"  runat="server" Text="Recalcular Saldos" OnClientClick="return( valRecalcularSaldos() );" ToolTip="Recalcular Saldos del Sistema" />
                                    </td>
                                    <td align="left">
                                        <asp:CheckBox ID="chbSelectAll" TextAlign="Right" CssClass="Texto" runat="server" onClick="return( valSelectAllPendientes() );" Text="Seleccionar Pendientes" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7" class="LabelRojo">
                                        Se recomienda &lt; Recalcular los Saldos &gt; antes de iniciar el proceso de Ajuste. 
                                        Una vez ajustado un producto evite &lt; Recalcular los Saldos &gt;.
                                    </td>                                    
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Detalle" Width="100%" ScrollBars="Both"   runat="server">
                            <asp:GridView ID="GV_Detalle" Width="100%" runat="server" AutoGenerateColumns="False" >
                            <Columns>
                            
                            <asp:TemplateField HeaderText="C�digo">
                            <ItemTemplate>
                            
                            <table>
                            <tr>
                            <td>
                                <asp:Label ID="lblIdProducto" runat="server" 
                                    Text='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>'    ></asp:Label></td>
                            <td>
                                <asp:HiddenField ID="hddIdDetalleDocumento" runat="server" 
                                    Value='<%#DataBinder.Eval(Container.DataItem,"IdDetalleDocumento")%>' />
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIdDocumento" runat="server"  
                                    Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIdUnidadMedida" runat="server" 
                                    Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />
                            </td>
                            </tr>
                            </table>
                            
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                            
                       <asp:BoundField ItemStyle-Width="500px" DataField="NomProducto" HeaderText="Descripci�n" />
                            <asp:BoundField DataField="UMedida" HeaderStyle-Width="80px"  
                                    ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"  HeaderText="U. M." />
                            <asp:BoundField DataField="CantidadSistema" HeaderStyle-Width="100px"  
                                    ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"  
                                    HeaderText="Cant. Sistema" DataFormatString="{0:F2}" />
                         
                            <asp:BoundField DataField="CantidadTomaInv" HeaderStyle-Width="100px"  
                                    ItemStyle-Font-Bold="true"  
                                    HeaderText="Cant. Toma Inv." DataFormatString="{0:F2}" />
                         
                         <asp:BoundField DataField="CantidadAjuste" HeaderStyle-Width="100px"  
                                    ItemStyle-Font-Bold="true"  ItemStyle-ForeColor="Red"
                                    HeaderText="Cant. Ajuste" DataFormatString="{0:F2}" />
                                    
                            <asp:BoundField DataField="Faltante" HeaderStyle-Width="100px"  
                                    ItemStyle-Font-Bold="true"  
                                    HeaderText="Faltante" DataFormatString="{0:F2}" />
                                    
                                    
                                    <asp:BoundField DataField="Sobrante" HeaderStyle-Width="100px"  
                                    ItemStyle-Font-Bold="true"  
                                    HeaderText="Sobrante" DataFormatString="{0:F2}" />
                                    
                                    
                                    
                                    
                                    <asp:BoundField DataField="CostoUnit" HeaderStyle-Width="100px"  
                                    ItemStyle-Font-Bold="true"  
                                    HeaderText="Costo Unit." DataFormatString="{0:F2}" />
                                    
                                    
                                    
                                    
                                    
                                    <asp:BoundField DataField="CostoFaltante" HeaderStyle-Width="100px"  
                                    ItemStyle-Font-Bold="true"
                                    HeaderText="Costo Faltante" DataFormatString="{0:F2}" />
                            
                            <asp:BoundField DataField="CostoSobrante" HeaderStyle-Width="100px"  
                                    ItemStyle-Font-Bold="true"
                                    HeaderText="Costo Sobrante" DataFormatString="{0:F2}" />
                            
                            
                            <asp:BoundField DataField="DescEstado" HeaderStyle-Width="100px"  
                                    ItemStyle-Font-Bold="true"
                                    HeaderText="Estado"  />
                                    
                            <asp:TemplateField HeaderText="Ajustar">
                            <ItemTemplate>
                            <table>
                            <tr>
                            <td><asp:CheckBox ID="chbAjustar" onClick="return(valCheckedAjustar());" runat="server" /></td>
                            <td>
                                <asp:HiddenField ID="hddIsAjustado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Ajustado")%>' />
                            </td>
                            </tr>
                            </table>
                                
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField>
                            <ItemTemplate>
                            
                                <asp:ImageButton ID="btnEliminarAjuste" OnClick="btnEliminarAjuste_Click" OnClientClick="return( valEliminarAjuste()  );" runat="server" ImageUrl="~/Imagenes/Eliminar1.gif" ToolTip="Deshacer el proceso de Ajuste." />
                            
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />                            
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>      
                            </asp:Panel>
                          
                    
                       </td>
                    </tr>
                    <tr>
                    <td style="text-align: center">
                    
                    <table>
                    <tr>
                    <td class="Texto">Faltante:</td>
                    <td>
                        <asp:Label ID="lblMonedaFaltante" CssClass="LabelRojo" runat="server" Text=""></asp:Label>
                        </td>
                    <td width="100px">
                        <asp:TextBox ID="txtCostoFaltante" ReadOnly="true" Width="100px" CssClass="TextBox_ReadOnly"  runat="server"></asp:TextBox></td>
                        <td  style="width:30px">
                        </td>
                    <td  class="Texto">Sobrante:</td>
                        <td>
                            <asp:Label CssClass="LabelRojo"  ID="lblMonedaSobrante" runat="server" Text=""></asp:Label>
                        </td>
                    <td> 
                                <asp:TextBox ID="txtCostoSobrante" ReadOnly="true" Width="100px" CssClass="TextBox_ReadOnly"  runat="server"></asp:TextBox>
                    </td>
                    </tr>
                    </table>
                    
                    </td>
                    </tr>
                </table>
                </asp:Panel>
            
                
                
                
                
                
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdDocTomaInv" runat="server" Value="" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdDocumento" runat="server" Value="" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdAlmacen" runat="server" />
            </td>
        </tr>
    </table>
    
    
    
    
    
    
    
    
    <div id="capaDocTomaInventario"        
        style="border: 3px solid blue; padding: 8px; width:auto; height:auto; top:186px ; left:65px; position:absolute;background-color:white; z-index:2; display :none; ">
        <table>                
        
        <tr>
        <td colspan="3">
        
        <table>
        <tr>
        
        <td class="Texto" align="right">Almac�n:</td>
        <td align="left">
            <asp:DropDownList ID="cboAlmacen_TI" runat="server">
            </asp:DropDownList>
        </td>
          <td align="left">
            <asp:Button ID="btnBuscar_TI" Width="80px" runat="server" Text="Buscar" ToolTip="Buscar Doc. Toma de Inventario por Almac�n."  OnClientClick="return(  valBuscar_TI()  );" />
        </td>
        </tr>
        </table>
        
        </td>
        
        
        
        
      
        </tr>
        <tr>
        <td colspan="3"  align="left">
        <asp:GridView ID="GV_DocumentoTI" runat="server" AutoGenerateColumns="False" AllowPaging="true" PageSize="20" >
                            <Columns>                        
                            
                            <asp:CommandField   ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Width="90px" />
                            <asp:BoundField DataField="NomTipoDocumento" HeaderStyle-Width="130px" HeaderText="Tipo" NullDisplayText=""  />
                            <asp:TemplateField HeaderText="Nro. Documento" >
                            <ItemTemplate >
                                <table>
                                    <tr>
                                    <td>  
                                        <asp:HiddenField ID="hddIdDocumentoTomaInventario" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                     </td>
                                    <td> 
                                        <asp:Label ID="lblNroDocumento" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'   ></asp:Label>  </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            </asp:TemplateField>                                                        
                            <asp:BoundField DataField="FechaEmision" HeaderStyle-Width="130px" HeaderText="Fecha Emisi�n" NullDisplayText=""  DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="NomEmpresaTomaInv" HeaderText="Empresa" NullDisplayText=""  />
                            <asp:BoundField DataField="NomAlmacen" HeaderStyle-Width="90px" HeaderText="Almac�n" NullDisplayText=""  />
                            <asp:BoundField DataField="FechaEntrega" HeaderStyle-Width="130px" HeaderText="Fecha Toma Inv." NullDisplayText=""  DataFormatString="{0:dd/MM/yyyy}"  />                           
                            
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />                            
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>      
        </td>
        </tr>
        <tr>
        <td colspan="3" align="center">
            <asp:Button ID="btnCerrarCapaTI" Width="80px" OnClientClick="return( offCapa('capaDocTomaInventario')  );" runat="server" Text="Cerrar" ToolTip="Cerrar Ventana" />
        </td>
        </tr>
        
        </table>        
        
    </div>    
    
    
    
    
    <script language="javascript" type="text/javascript">

        function valCheckedAjustar() {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (rowElem.cells[12].children[0].cells[0].children[0].id == event.srcElement.id) {

                    if (parseInt(rowElem.cells[12].children[0].cells[1].children[0].value) == 1) {
                        alert('El Producto a seleccionar ya ha sido ajustado.');
                        return false;
                    }
                    return true;
                }
                
                
                
            }
            alert('Elemento no encontrado.');
            return false;
        }
        function valSelectAllPendientes() {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var chbSelectAll = document.getElementById('<%=chbSelectAll.ClientID%>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];                
                if (parseInt(rowElem.cells[12].children[0].cells[1].children[0].value) == 0) {
                    rowElem.cells[12].children[0].cells[0].children[0].status = chbSelectAll.status;
                }
            }
            return true;
        }
        function valRecalcularSaldos() {

            var hddIdDocumentoTomaInv = document.getElementById('<%=hddIdDocTomaInv.ClientID %>');
            if (isNaN(hddIdDocumentoTomaInv.value) || hddIdDocumentoTomaInv.value.length <= 0) {
                alert('No se ha seleccionado un Doc. Toma de Inventario al cual hacer referencia.');
                return false;
            }              
        
            var FechaTomaInv = document.getElementById('<%=lblFechaTomaInv.ClientID %>').innerHTML;
            return ( confirm('Desea recalcular los saldos por el sistema hasta la fecha : ' + FechaTomaInv + ' ? ' )  );
        }
        function valSave() {

            var IdEmpresa =parseInt( document.getElementById('<%=cboEmpresa.ClientID %>').value);
            if (isNaN(IdEmpresa) || IdEmpresa <= 0) {
                alert('Seleccione una Empresa.');
                return false;
            }
            var IdTienda = parseInt(document.getElementById('<%=cboTienda.ClientID %>').value);
            if (isNaN(IdTienda) || IdTienda <= 0) {
                alert('Seleccione una Tienda.');
                return false;
            }
            var IdSerie = parseInt(document.getElementById('<%=cboSerie.ClientID %>').value);
            if (isNaN(IdSerie) || IdSerie <= 0) {
                alert('Seleccione una Serie.');
                return false;
            }            

            var IdTipoOperacion = parseInt(document.getElementById('<%=cboTipoOperacion.ClientID %>').value);
            if (isNaN(IdTipoOperacion) || IdTipoOperacion <= 0) {
                alert('Seleccione un Tipo de Operaci�n.');
                return false;
            }
            var IdEstado = parseInt(document.getElementById('<%=cboEstado.ClientID %>').value);
            if (isNaN(IdEstado) || IdEstado <= 0) {
                alert('Seleccione un Estado del Documento.');
                return false;
            }
          
            var hddIdDocumentoTomaInv = document.getElementById('<%=hddIdDocTomaInv.ClientID %>');
            if (isNaN(hddIdDocumentoTomaInv.value) || hddIdDocumentoTomaInv.value.length <= 0) {
                alert('No se ha seleccionado un Doc. Toma de Inventario al cual hacer referencia.');
                return false;
            }

            var hddIdAlmacen = document.getElementById('<%=hddIdAlmacen.ClientID %>');
            if (isNaN(hddIdAlmacen.value) || hddIdAlmacen.value.length <= 0) {
                alert('No se posee un Almac�n al cual hacer referencia.');
                return false;
            }


            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            } else {
                var cont = 0;
                for (var i = 1; i < grilla.rows.length; i++) {                
                var rowElem = grilla.rows[i];
                    if ( rowElem.cells[12].children[0].cells[0].children[0].status==true) {
                        cont = cont + 1;
                    }
                }
                if (cont <= 0) {
                    alert('No se ha seleccionado ning�n Producto el cual ajustar la cantidad.');
                    return false;
                }
            }
            
            return (  confirm('Desea continuar con la Operaci�n ? ') );

        }
        function valEliminarAjuste() {

            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>');
            if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene un Doc. Ajuste de Inventario al cual hacer referencia.');
                return false;
            }
        
        
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla == null) {
                alert('No se hallaron registros.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {
                var rowElem = grilla.rows[i];
                if (rowElem.cells[13].children[0].id == event.srcElement.id) {

                    if (parseInt(rowElem.cells[12].children[0].cells[1].children[0].value) == 0) {
                        alert('El Producto no ha sido ajustado.');
                        return false;
                    } else {
                        return (confirm('Desea continuar con la Operaci�n ?'));       
                    
                    }                    
                }
            }
            alert('Registro no encontrado.');
            return false;
        }


        function valBuscarDocumento() {
            var IdSerie = parseInt(document.getElementById('<%=cboSerie.ClientID %>').value);
            if (isNaN(IdSerie) || IdSerie <= 0) {
                alert('Seleccione una Serie.');
                return false;
            }
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if ( txtCodigoDocumento.value.length <= 0) {
                alert('Ingrese un C�digo de Documento.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }

            return true;
        }
        function valMostrarDatos() {
            var hddIdDocumentoTomaInv = document.getElementById('<%=hddIdDocTomaInv.ClientID %>');
            if (isNaN(hddIdDocumentoTomaInv.value) || hddIdDocumentoTomaInv.value.length <= 0) {
                alert('No se ha seleccionado un Doc. Toma de Inventario al cual hacer referencia.');
                return false;
            }
            var IdSubLinea = parseInt(document.getElementById('<%=cboSubLinea.ClientID %>').value);
            if (isNaN(IdSubLinea) || IdSubLinea <= 0) {
                alert('Seleccione una Sub L�nea.');
                return false;
            }  

            return true;
        }
        function valAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>');
            if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length <= 0) {
                alert('No se ha seleccionado un Doc. Ajuste de Inventario al cual hacer referencia.');
                return false;
            }
            var IdEstado = parseInt(document.getElementById('<%=cboEstado.ClientID %>').value);
            if (isNaN(IdEstado) || IdEstado <= 0) {
                alert('No se ha seleccionado un Estado para el Documento.');
                return false;
            }
            if (   IdEstado==2  ) {
                alert('El Documento ya ha sido anulado.');
                return false;
            }
        
        
            return (   confirm('Desea continuar con la anulaci�n del Doc. Ajuste de Inventario ?') );
        }
        function valBuscar_TI() {
            var IdAlmacen= parseInt(document.getElementById('<%=cboAlmacen_TI.ClientID %>').value);
            if (isNaN(IdAlmacen) || IdAlmacen <= 0) {
                alert('Seleccione un Almac�n de b�squeda.');
                return false;
            }
            return true;
        }
        function valEditar() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID %>');
            if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length <= 0) {
                alert('No se ha seleccionado un Doc. Ajuste de Inventario al cual hacer referencia.');
                return false;
            }
            var IdEstado = parseInt(document.getElementById('<%=cboEstado.ClientID %>').value);
            if (isNaN(IdEstado) || IdEstado <= 0) {
                alert('No se ha seleccionado un Estado para el Documento.');
                return false;
            }
            if (IdEstado == 2) {
                alert('El Documento ha sido anulado.');
                return false;
            }
            return true;
        }
        function valNuevo() {


            var IdSerie = parseInt(document.getElementById('<%=cboSerie.ClientID %>').value);
            if (isNaN(IdSerie) || IdSerie <= 0) {
                alert('Seleccione una Serie.');
                return false;
            }
            
            var IdTipoOperacion = parseInt(document.getElementById('<%=cboTipoOperacion.ClientID %>').value);
            if (isNaN(IdTipoOperacion) || IdTipoOperacion <= 0) {
                alert('Seleccione un Tipo de Operaci�n.');
                return false;
            }
            
            onCapa('capaDocTomaInventario');
            return false;
        }
        function valImprimir() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length == 0) {
                alert('No se ha seleccionado ning�n Documento para Impresi�n.');
                return false;
            }

            window.open('../../Almacen1/Reportes/Visor.aspx?iReporte=5&IdDocumento=' + hddIdDocumento.value, 'Inventario', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
            return false;
        }        
        
        
    </script>
</asp:Content>

