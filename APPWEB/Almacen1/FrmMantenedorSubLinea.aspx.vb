﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmMantenedorSubLinea
    Inherits System.Web.UI.Page
    Private listaRegimen As List(Of Entidades.Regimen)
    Private objScript As New ScriptManagerClass

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            ConfigurarDatos()
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try
            cargarDatosLinea(Me.cmbLinea)
            cargarDatosLinea(Me.cmbLinea_Busqueda, "B")
            cargarDatosRegimen(Me.cmbRegimen)
            buscarSubLinea()
            verFrmInicio()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub setListaRegimen(ByVal lista As List(Of Entidades.Regimen))
        Session.Remove("listaRegimen")
        Session.Add("listaRegimen", lista)
    End Sub
    Private Function getListaRegimen() As List(Of Entidades.Regimen)
        Return CType(Session.Item("listaRegimen"), List(Of Entidades.Regimen))
    End Function
#Region "VISTAS FORMULARIOS"
    Private Sub verFrmInicio()
        Session.Remove("listaRegimen")
        Panel_SubLineaEdicion.Visible = False
        Panel_busqueda.Enabled = True
        btnNuevo.Visible = True
        btnGuardar.Visible = False
        btnCancelar.Visible = False
        DGV_Regimen.DataSource = Nothing
        DGV_Regimen.DataBind()
        limpiarControles()
    End Sub
    Private Sub verFrmNuevo()
        Panel_SubLineaEdicion.Visible = True
        Panel_busqueda.Enabled = False
        btnNuevo.Visible = False
        btnGuardar.Visible = True
        btnCancelar.Visible = True
    End Sub
#End Region
#Region "CARGA DE DATOS CONTROLES"
    Private Sub cargarDatosRegimen(ByVal cbo As DropDownList)
        Dim obj As New Negocio.Regimen
        cbo.DataSource = obj.SelectCbo
        cbo.DataBind()
    End Sub

    Private Sub cargarDatosLinea(ByVal combo As DropDownList, Optional ByVal opcion As String = "")
        Dim nLinea As New Negocio.Linea
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo()
        If opcion = "B" Then
            Dim obj As New Entidades.Linea
            obj.Id = 0
            obj.Descripcion = "-----"
            lista.Insert(0, obj)
        End If
        combo.DataSource = lista
        combo.DataBind()
    End Sub
#End Region
    Private Sub limpiarControles()
        txtCodigoSubLinea.Text = ""
        txtCostoCompra.Text = ""
        txtCostoVenta.Text = ""
        txtCtaDebeCompra.Text = ""
        txtCtaDebeVenta.Text = ""
        txtCtaHaberCompra.Text = ""
        txtCtaHaberVenta.Text = ""
        txtNomre.Text = ""
        txtCodigoSubLineaOld.Text = ""
        rdbAfectoDetraccion.SelectedIndex = 0
        rdbAfectoPercepcion.SelectedIndex = 0
        rdbEstado.SelectedIndex = 0
        rdbIGV.SelectedIndex = 0
    End Sub
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Dim ngcValSL As New Negocio.SubLinea
        If CStr(ViewState("Modo")) = "1" Then
            Dim objValIdLinea As Entidades.SubLinea = ngcValSL.ValidadNombrexIdLinea(CInt(Me.cmbLinea.SelectedValue), Me.txtNomre.Text.Trim)
            If objValIdLinea.Cantidad >= 1 Then
                objScript.mostrarMsjAlerta(Me, "Ya existe el nombre " + Me.txtNomre.Text.Trim.ToUpper + " para la linea " + Me.cmbLinea.SelectedItem.Text.ToUpper + " \n" + "ingrese un nombre diferente" + "\n ó este inactivo.")
                txtNomre.Focus()
                Exit Sub
            End If
        End If

        registrarSubLinea()
    End Sub
    Private Function getSubLinea() As Entidades.SubLinea
        Dim objSubLinea As New Entidades.SubLinea
        With objSubLinea
            .AfectoDetraccion = rdbAfectoDetraccion.SelectedValue
            .AfectoIgv = rdbIGV.SelectedValue
            .AfectoPercepcion = rdbAfectoPercepcion.SelectedValue
            .CostoCompra = txtCostoCompra.Text.Trim
            .CostoVenta = txtCostoVenta.Text.Trim
            .CtaDebeCompra = txtCtaDebeCompra.Text.Trim
            .CtaDebeVenta = txtCtaDebeVenta.Text.Trim
            .CtaHaberCompra = txtCtaHaberCompra.Text.Trim
            .CtaHaberVenta = txtCtaHaberVenta.Text.Trim
            .Estado = rdbEstado.SelectedValue
            .Id = CInt(IIf(IsNumeric(txtCodigoSubLinea.Text) = True, txtCodigoSubLinea.Text, 0))
            .IdLinea = CInt(cmbLinea.SelectedValue)
            .IdTipoExistencia = Nothing
            .Nombre = txtNomre.Text.Trim.ToUpper
            .CodigoSubLinea = (Me.txtCodigoSubLineaOld.Text.Trim.ToUpper)
            .Longitud = CInt(Me.txtLongitud.Text)
        End With
        Return objSubLinea
    End Function
    Private Sub registrarSubLinea()
        Dim hecho As Boolean = False
        Try
            saveListaRegimen()
            Dim objSubLinea As Entidades.SubLinea = getSubLinea()
            Dim objNSubLinea As New Negocio.SubLinea
            Me.listaRegimen = getListaRegimen()
            Dim lista As New List(Of Entidades.ProductoRegimen)
            For i As Integer = 0 To Me.listaRegimen.Count - 1
                Dim obj As New Entidades.ProductoRegimen
                obj.reg_Id = listaRegimen.Item(i).Id
                obj.Tasa = listaRegimen.Item(i).Tasa
                lista.Add(obj)
            Next
            If txtCodigoSubLinea.Text = "" Then
                hecho = objNSubLinea.InsertaSubLineaT(objSubLinea, lista)
            Else
                hecho = objNSubLinea.ActualizaSubLineaT(objSubLinea, lista)
            End If
            If hecho Then
                verFrmInicio()
                buscarSubLinea()
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
                'Else
                'Throw New Exception
            End If
        Catch ex As Exception
            'objScript.mostrarMsjAlerta(Me, "Ocurrieron problemas en la operación.")
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        ViewState.Add("Modo", "1")

        Me.listaRegimen = New List(Of Entidades.Regimen)
        setListaRegimen(listaRegimen)
        verFrmNuevo()

        Try
            'Dim objNSubLinea As New Negocio.SubLinea
            'Dim objSubLineaLong As Entidades.SubLinea = objNSubLinea.ValidadLongitud()
            'Me.txtLongitud.Text = CStr(objSubLineaLong.Longitud)


            'If objSubLineaLong.Cantidad <= 1 Then
            '    'If Me.txtLongitud.Text = "" Or Me.txtLongitud.Text = "0" Then
            '    txtLongitud.Enabled = True
            'ElseIf objSubLineaLong.Cantidad >= 2 Then
            '    txtLongitud.Enabled = False
            'End If

            txtLongitud.Enabled = True

            Me.txtCodigoSubLineaOld.Text = ""

        Catch ex As Exception
            txtLongitud.Enabled = True
            Me.txtCodigoSubLineaOld.Text = ""
        End Try
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        ViewState.Add("Modo", "0")
        verFrmInicio()
    End Sub
    Protected Sub rdbEstado_Busqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbEstado_Busqueda.SelectedIndexChanged
        buscarSubLinea()
    End Sub
    Protected Sub cmbLinea_Busqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLinea_Busqueda.SelectedIndexChanged
        buscarSubLinea()
    End Sub
    Private Sub buscarSubLinea()
        Try
            Dim objSubLinea As New Negocio.SubLinea
            DGV_SubLineaBusqueda.DataSource = objSubLinea.SelectxIdLineaxEstado(CInt(cmbLinea_Busqueda.SelectedValue), rdbEstado_Busqueda.SelectedValue)
            DGV_SubLineaBusqueda.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la búsqueda de datos.")
        End Try
    End Sub
    Private Sub DGV_SubLineaBusqueda_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_SubLineaBusqueda.PageIndexChanging
        DGV_SubLineaBusqueda.PageIndex = e.NewPageIndex
        buscarSubLinea()
    End Sub

    Protected Sub DGV_SubLineaBusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_SubLineaBusqueda.SelectedIndexChanged
        ViewState.Add("Modo", "0")
        cargarDatosSubLineaEdicion(CInt(DGV_SubLineaBusqueda.SelectedRow.Cells(5).Text))
    End Sub
    Private Sub cargarDatosSubLineaEdicion(ByVal idSubLinea As Integer)
        Try
            Dim objNSubLinea As New Negocio.SubLinea
            Dim objSubLinea As Entidades.SubLinea = objNSubLinea.SelectxIdSubLinea(idSubLinea)
            With objSubLinea
                If .Id = 0 Then
                    Throw New Exception
                End If
                txtCodigoSubLinea.Text = .Id.ToString
                rdbAfectoDetraccion.SelectedValue = .AfectoDetraccion
                rdbIGV.SelectedValue = .AfectoIgv
                rdbAfectoPercepcion.SelectedValue = .AfectoPercepcion
                txtCostoCompra.Text = .CostoCompra
                txtCostoVenta.Text = .CostoVenta
                txtCtaDebeCompra.Text = .CtaDebeCompra
                txtCtaDebeVenta.Text = .CtaDebeVenta
                txtCtaHaberCompra.Text = .CtaHaberCompra
                txtCtaHaberVenta.Text = .CtaHaberVenta
                rdbEstado.SelectedValue = .Estado
                cmbLinea.SelectedValue = .IdLinea.ToString
                txtNomre.Text = .Nombre
                Me.txtCodigoSubLineaOld.Text = .CodigoSubLinea.Trim
                txtLongitud.Text = .Longitud.ToString
            End With
            'cargo la lista de regimen
            Dim objNRegimen As New Negocio.Regimen
            Me.listaRegimen = objNRegimen.SelectxIdSubLinea(idSubLinea)
            setListaRegimen(Me.listaRegimen)
            DGV_Regimen.DataSource = Me.listaRegimen
            DGV_Regimen.DataBind()
            verFrmNuevo()

            Try
                Dim objSubLineaLong As Entidades.SubLinea = objNSubLinea.ValidadLongitud()
                ''If (CStr(objSubLineaLong.Longitud) = "" Or objSubLineaLong.Longitud = 0) Then

                '' se comento para habilitar  la longitud
                'If objSubLineaLong.Cantidad <= 1 Then
                '    txtLongitud.Enabled = True
                'ElseIf objSubLineaLong.Cantidad >= 2 Then
                '    txtLongitud.Enabled = False
                'End If
                txtLongitud.Enabled = True


            Catch ex As Exception
                txtLongitud.Enabled = True
            End Try

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub

    Protected Sub btnAgregarUM_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarUM.Click
        Try
            saveListaRegimen()
            Me.listaRegimen = getListaRegimen()
            Dim obj As New Entidades.Regimen
            With obj
                .Id = CInt(cmbRegimen.SelectedValue)
                .Nombre = CStr(cmbRegimen.SelectedItem.ToString)
            End With
            Me.listaRegimen.Add(obj)
            setListaRegimen(Me.listaRegimen)
            DGV_Regimen.DataSource = Me.listaRegimen
            DGV_Regimen.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Ocurrieron problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub saveListaRegimen()
        Me.listaRegimen = getListaRegimen()
        For i As Integer = 0 To Me.DGV_Regimen.Rows.Count - 1
            With DGV_Regimen.Rows(i)
                Me.listaRegimen.Item(i).Tasa = CDec(CType(.Cells(3).FindControl("txtTasa"), TextBox).Text)
            End With
        Next
        setListaRegimen(Me.listaRegimen)
    End Sub

    Protected Sub DGV_Regimen_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGV_Regimen.SelectedIndexChanged
        Try
            saveListaRegimen()
            Me.listaRegimen = getListaRegimen()
            Me.listaRegimen.RemoveAt(DGV_Regimen.SelectedIndex)
            setListaRegimen(Me.listaRegimen)
            DGV_Regimen.DataSource = Me.listaRegimen
            DGV_Regimen.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminación del Tipo Régimen.")
        End Try
    End Sub
End Class