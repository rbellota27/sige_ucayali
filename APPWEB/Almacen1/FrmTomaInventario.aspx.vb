﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmTomaInventario
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    '*************** ModoFrm
    '******** 0 : INICIO / nuevo - buscar
    '******** 1 : NUEVO / guardar - cancelar
    '******** 2 : EDITAR DOCUMENTO / guardar - cancelar
    '******** 3 : BUSCAR DOCUMENTO / buscar doc - habilitar busqueda
    '******** 4 : DOCUMENTO HALLADO CORRECTAMENTE / editar - anular - imprimir - despachar - remitir - cancelar
    '******** 5 : DOC. GUARDADO CORRECTAMENTE / imprimir - despachar - remitir - cancelar
    '******** 6 : OCULTAR TODOS LOS BOTONES
    '**********************************************

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            inicializarFrm()
        End If

    End Sub

    Private Sub inicializarFrm()
        Try




            '************************** cargamos los controles
            Dim objCombo As New Combo
            With objCombo

                '************ Controles del Frm Principal
                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboEstadoDocumento(Me.cboEstado)

                '************ Controles de Generación de Documento
                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa_GD, CInt(Session("IdUsuario")), False)
                Me.cboEmpresa_GD.Items.Insert(0, New ListItem("Saldos de Todas las Empresas", "0"))


                .llenarCboAlmacenxIdTienda(Me.cboAlmacen_GD, CInt(Me.cboTienda.SelectedValue), False)
                .LlenarCboLinea(Me.cboLinea_GD, True)
                .LlenarCboSubLineaxIdLinea(Me.cboSubLinea_GD, CInt(Me.cboLinea_GD.SelectedValue), True)



            End With

            GenerarCodigoDocumento()

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaTomaInv.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            hddFrmModo.Value = "0"  '*********** NUEVO
            actualizarControlesFrmModo()

            ValidarPermisos()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos [ inicializarFrm() ].")
        End Try
    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {69, 70, 71, 72, 73})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumento.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumento.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

    End Sub



    Private Sub actualizarControlesFrmModo()

        Select Case CInt(hddFrmModo.Value)

            Case 0 '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnImprimir.Visible = False

                Me.btnProcesarAjuste.Visible = False

                Me.btnRecalcularSaldosSistema.Visible = False

                Me.btnAnular.Visible = False

                Panel_Cab.Enabled = True
                Panel_Emp_FechaTomaInv.Enabled = True
                Panel_Detalle.Enabled = False


            Case 1 '************** Nuevo
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnImprimir.Visible = False

                Me.btnProcesarAjuste.Visible = False
                Me.btnRecalcularSaldosSistema.Visible = False
                Me.btnAnular.Visible = False

                Panel_Cab.Enabled = True
                Panel_Emp_FechaTomaInv.Enabled = True
                Panel_Detalle.Enabled = False

            Case 2 '*********************** Editar
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnImprimir.Visible = True

                Me.btnProcesarAjuste.Visible = True
                Me.btnRecalcularSaldosSistema.Visible = True
                Me.btnAnular.Visible = False

                Panel_Cab.Enabled = False
                Panel_Emp_FechaTomaInv.Enabled = True
                Panel_Detalle.Enabled = True



            Case 3 '**************************** Buscar documento
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumento.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = True
                Me.btnAnular.Visible = False

                Me.txtCodigoDocumento.Text = ""
                Me.txtCodigoDocumento.Focus()

                Me.btnProcesarAjuste.Visible = False

                Me.btnImprimir.Visible = False

                Me.btnRecalcularSaldosSistema.Visible = False


                Panel_Cab.Enabled = True
                Panel_Emp_FechaTomaInv.Enabled = False
                Panel_Detalle.Enabled = False

            Case 4 '********* documento hallado correctamente

                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = True
                Me.btnAnular.Visible = True
                Me.btnProcesarAjuste.Visible = True

                Me.btnImprimir.Visible = True
                Me.btnRecalcularSaldosSistema.Visible = False

                Panel_Cab.Enabled = False
                Panel_Emp_FechaTomaInv.Enabled = False
                Panel_Detalle.Enabled = False

            Case 5 '********* documento guardado correctamente

                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = True

                Me.btnProcesarAjuste.Visible = False

                Me.btnImprimir.Visible = True

                Me.btnRecalcularSaldosSistema.Visible = True
                Me.btnAnular.Visible = False

                Panel_Cab.Enabled = False
                Panel_Emp_FechaTomaInv.Enabled = True
                Panel_Detalle.Enabled = True

            Case 6  '*********** Inhabilitamos todos los controles excepto cancelar

                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = True

                Me.btnProcesarAjuste.Visible = False
                Me.btnRecalcularSaldosSistema.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnAnular.Visible = False

                Panel_Cab.Enabled = False
                Panel_Emp_FechaTomaInv.Enabled = False
                Panel_Detalle.Enabled = False

        End Select


    End Sub


    Private Sub cboLinea_GD_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea_GD.SelectedIndexChanged
        Try

            Dim objCombo As New Combo
            objCombo.LlenarCboSubLineaxIdLinea(Me.cboSubLinea_GD, CInt(Me.cboLinea_GD.SelectedValue), True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos - Sub Línea.")
        End Try
    End Sub
    Private Sub btnAceptar_GD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_GD.Click

        GenerarDocTomaInventario(CInt(Me.cboLinea_GD.SelectedValue), CInt(Me.cboSubLinea_GD.SelectedValue))

    End Sub
    Private Sub GenerarDocTomaInventario(ByVal IdLinea As Integer, ByVal IdSubLinea As Integer)

        Try

            Dim objDocumento As Entidades.Documento = obtenerDocumento()

            objDocumento.Id = (New Negocio.TomaInventario).GenerarDocTomaInventario(objDocumento, IdLinea, IdSubLinea)

            If objDocumento.Id > 0 Then

                cargarControlesDocGenerado(objDocumento)

                hddFrmModo.Value = "2" '******** EDITAR
                actualizarControlesFrmModo()

                objScript.mostrarMsjAlerta(Me, "La Generación del Documento < Toma de Inventario > se realizó con éxito.")
            Else
                Throw New Exception
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Ocurrieron Problemas en la operación.")
        End Try



    End Sub

    Private Sub cargarControlesDocGenerado(ByVal objDocumento As Entidades.Documento)

        hddIdDocumento.Value = CStr(objDocumento.Id)
        hddIdAlmacen.Value = CStr(objDocumento.IdAlmacen)
        lblNomAlmacen.Text = objDocumento.NomAlmacen



        hddIdEmpresaTomaInv.Value = CStr(objDocumento.IdDestinatario)
        lblNombreEmpresaSaldos.Text = objDocumento.NomEmpresaTomaInv

        '********** Cargamos los controles de Linea y SubLinea Detalle
        Me.cboLinea.DataSource = (New Negocio.TomaInventario).SelectLineaxIdDocumento(objDocumento.Id)
        Me.cboLinea.DataBind()

        Me.cboSubLinea.DataSource = (New Negocio.TomaInventario).SelectSubLineaxIdDocumentoxIdLinea(objDocumento.Id, CInt(Me.cboLinea.SelectedValue))
        Me.cboSubLinea.DataBind()

    End Sub

    Private Function obtenerDocumento() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)


            '************** Guardamos el Id del Empleado encargado de la Toma de Inventarios
            If (IsNumeric(hddIdEmpleado.Value) And hddIdEmpleado.Value.Trim.Length > 0) Then
                .IdPersona = CInt(hddIdEmpleado.Value)
            End If


            Select Case CInt(hddFrmModo.Value)
                Case 0  '******** INICIO
                    .IdAlmacen = CInt(Me.cboAlmacen_GD.SelectedValue)
                    .NomAlmacen = Me.cboAlmacen_GD.SelectedItem.ToString

                    '************** Guardamos el Id de la Empresa a la cual se ha hecho la cant. Saldo calculado
                    '************** por el Sistema
                    .IdDestinatario = CInt(Me.cboEmpresa_GD.SelectedValue)
                    .NomEmpresaTomaInv = Me.cboEmpresa_GD.SelectedItem.ToString
                    .Id = Nothing

                Case 2  '******** EDITAR
                    .IdAlmacen = CInt(Me.hddIdAlmacen.Value)
                    .NomAlmacen = Me.lblNomAlmacen.Text

                    '************** Guardamos el Id de la Empresa a la cual se ha hecho la cant. Saldo calculado
                    '************** por el Sistema
                    .IdDestinatario = CInt(Me.hddIdEmpresaTomaInv.Value)
                    .NomEmpresaTomaInv = Me.lblNombreEmpresaSaldos.Text
                    .Id = CInt(Me.hddIdDocumento.Value)

            End Select


            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = Me.cboSerie.SelectedItem.ToString
            .IdTipoDocumento = CInt(Me.cboTipoDocumento.SelectedValue)
            .IdEstadoDoc = CInt(Me.cboEstado.SelectedValue)
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .Codigo = Me.txtCodigoDocumento.Text

            '*************** Almacenamos la Fecha de Cálculo de Saldos del Sistema
            .FechaEntrega = CDate(IIf(IsDate(Me.txtFechaTomaInv.Text) = True, Me.txtFechaTomaInv.Text, Nothing))

        End With

        Return objDocumento

    End Function

    Private Sub GenerarCodigoDocumento()
        Try

            Dim objCombo As New Combo
            objCombo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))

            objCombo.llenarCboAlmacenxIdTienda(Me.cboAlmacen_GD, CInt(Me.cboTienda.SelectedValue), False)

            If (IsNumeric(Me.cboSerie.SelectedValue)) Then
                Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
            Else
                Me.txtCodigoDocumento.Text = ""
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged

        Dim objCombo As New Combo
        objCombo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)

        GenerarCodigoDocumento()
    End Sub

    Protected Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboTienda.SelectedIndexChanged
        GenerarCodigoDocumento()
    End Sub

    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged

        Try
            Me.cboSubLinea.DataSource = (New Negocio.TomaInventario).SelectSubLineaxIdDocumentoxIdLinea(CInt(hddIdDocumento.Value), CInt(Me.cboLinea.SelectedValue))
            Me.cboSubLinea.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try

    End Sub

    Protected Sub btnAceptar_Cab_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptar_Cab.Click

        cargarDetalleDocumento(CInt(hddIdDocumento.Value), CInt(Me.cboSubLinea.SelectedValue), Me.chbMostrarCantSistema.Checked)

    End Sub

    Private Sub cargarDetalleDocumento(ByVal IdDocumento As Integer, ByVal IdSubLinea As Integer, ByVal mostrarDatosSistema As Boolean)

        Try

            Me.GV_Detalle.DataSource = (New Negocio.TomaInventario).SelectDetallexIdDocumentoxIdSubLinea(IdDocumento, IdSubLinea)
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularCantidades('1');", True)


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try


    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click

        registrarDocumento(CInt(hddIdDocumento.Value))


    End Sub

    Private Sub registrarDocumento(ByVal IdDocumento As Integer)

        Try

            Dim objDocumento As Entidades.Documento = obtenerDocumento()
            Dim listaDetalleDocumento As List(Of Entidades.DetalleDocumento) = obtenerListaDetalleDocumento()

            If ((New Negocio.TomaInventario).UpdateDocTomaInventario(objDocumento, listaDetalleDocumento) <= 0) Then

                Throw New Exception

            Else

                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularCantidades('1');alert('La operación finalizó con éxito.');  ", True)
                '*********** objScript.mostrarMsjAlerta(Me, "La operación finalizó con éxito.")

            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la operación.")
        End Try


    End Sub
    Private Function obtenerListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)

        Dim lista As New List(Of Entidades.DetalleDocumento)

        For i As Integer = 0 To GV_Detalle.Rows.Count - 1

            Dim objDetalleDoc As New Entidades.DetalleDocumento

            With objDetalleDoc

                .IdProducto = CInt(CType(GV_Detalle.Rows(i).FindControl("lblIdProducto"), Label).Text)
                .IdDetalleDocumento = CInt(CType(GV_Detalle.Rows(i).FindControl("hddIdDetalleDocumento"), HiddenField).Value)
                .IdDocumento = CInt(CType(GV_Detalle.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value)

                .IdUnidadMedida = CInt(CType(GV_Detalle.Rows(i).FindControl("hddIdUnidadMedida"), HiddenField).Value)
                .UMedida = CStr(GV_Detalle.Rows(i).Cells(2).Text)
                .Cantidad = CDec(CType(GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)
                .CantxAtender = CDec(GV_Detalle.Rows(i).Cells(3).Text)

            End With

            lista.Add(objDetalleDoc)

        Next

        Return lista

    End Function

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click


        hddFrmModo.Value = "3"  '*********** BUSCAR
        actualizarControlesFrmModo()


    End Sub

    Protected Sub btnBuscarDocumento_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarDocumento.Click



        cargarDocumento(CInt(Me.cboSerie.SelectedValue), CInt(txtCodigoDocumento.Text))





    End Sub

    Private Sub cargarDocumento(ByVal IdSerie As Integer, ByVal codigo As Integer)

        Try


            Dim objDocumento As Entidades.Documento = (New Negocio.TomaInventario).SelectxIdSeriexCodigo(IdSerie, codigo)
            If (objDocumento Is Nothing) Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
                Return
            Else


                '************ Cargamos la cabecera
                cargarDocumentoCab(objDocumento)

                '********** Cargamos las Lineas y Sub lineas
                cargarControlesDocGenerado(objDocumento)

                '*********** Cargo el nombre del Empleado
                hddFrmModo.Value = "4"
                actualizarControlesFrmModo()


            End If




        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, "Problemas en la carga del Documento.")

        End Try


    End Sub

    Private Sub cargarDocumentoCab(ByVal objDocumento As Entidades.Documento)

        hddIdDocumento.Value = CStr(objDocumento.Id)
        hddPoseeDocAjusteInv.Value = objDocumento.PoseeDocRelacionado

        If (objDocumento.IdPersona <> 0) Then
            hddIdEmpleado.Value = CStr(objDocumento.IdPersona)
        Else
            hddIdEmpleado.Value = ""
        End If

        Me.txtNomEmpleado.Text = objDocumento.NomEmpleado
        Me.cboEmpresa.SelectedValue = CStr(objDocumento.IdEmpresa)
        Me.cboTienda.SelectedValue = CStr(objDocumento.IdTienda)

        '***************** Cargamos la Serie
        Dim objCombo As New Combo
        objCombo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, objDocumento.IdEmpresa, objDocumento.IdTienda, objDocumento.IdTipoDocumento)
        Me.cboSerie.SelectedValue = CStr(objDocumento.IdSerie)

        Me.txtCodigoDocumento.Text = objDocumento.Codigo
        Me.lblNomAlmacen.Text = objDocumento.NomAlmacen
        Me.hddIdAlmacen.Value = CStr(objDocumento.IdAlmacen)

        Me.cboEstado.SelectedValue = CStr(objDocumento.IdEstadoDoc)
        Me.txtFechaEmision.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
        Me.txtFechaTomaInv.Text = Format(objDocumento.FechaEntrega, "dd/MM/yyyy")
        '****************** cargamos la empresa de Toma Inv
        Me.lblNombreEmpresaSaldos.Text = objDocumento.NomEmpresaTomaInv
        Me.hddIdEmpresaTomaInv.Value = CStr(objDocumento.IdDestinatario)

    End Sub

    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditar.Click

        hddFrmModo.Value = "2"
        actualizarControlesFrmModo()

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "calcularCantidades('1');", True)

    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelar.Click

        limpiarFrm()
        hddFrmModo.Value = "0"
        actualizarControlesFrmModo()

    End Sub

    Private Sub limpiarFrm()

        hddIdEmpleado.Value = ""
        hddIdAlmacen.Value = ""
        hddIdDocumento.Value = ""
        hddIdEmpresaTomaInv.Value = ""
        hddPoseeDocAjusteInv.Value = ""

        Me.txtCodigoDocumento.Text = ""

        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaTomaInv.Text = Me.txtFechaEmision.Text
        Me.txtNomEmpleado.Text = ""

        lblNomAlmacen.Text = ""
        lblNombreEmpresaSaldos.Text = ""

        Me.cboEstado.SelectedIndex = 0

        Me.cboLinea.DataSource = Nothing
        Me.cboLinea.DataBind()

        Me.cboSubLinea.DataSource = Nothing
        Me.cboSubLinea.DataBind()

        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()

        Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))


    End Sub

    Private Sub btnBuscarEmpleado_Grilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarEmpleado_Grilla.Click

        buscarEmpleado(CInt(Me.cmbFiltro_BuscarEmpleado.SelectedValue), Me.txtBuscarEmpleado_Grilla.Text)


    End Sub

    Private Sub buscarEmpleado(ByVal opcion As Integer, ByVal textoBusqueda As String)

        Try

            Me.GV_Empleado.DataSource = (New Negocio.Empleado).SelectActivoxParams(opcion, textoBusqueda)
            Me.GV_Empleado.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos en la búsqueda de Empleados.")
        End Try

    End Sub


    Private Sub GV_Empleado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_Empleado.PageIndexChanging
        Me.GV_Empleado.PageIndex = e.NewPageIndex
        buscarEmpleado(CInt(Me.cmbFiltro_BuscarEmpleado.SelectedValue), Me.txtBuscarEmpleado_Grilla.Text)
    End Sub

    Private Sub btnRecalcularSaldosSistema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecalcularSaldosSistema.Click


        recalcularSaldosSistema(CDate(Me.txtFechaTomaInv.Text), CInt(hddIdDocumento.Value))


    End Sub

    Private Sub recalcularSaldosSistema(ByVal fechaTomaInv As Date, ByVal IdDocumento As Integer)

        Try

            If ((New Negocio.TomaInventario).RecalcularSaldosDocTomaInv(IdDocumento, fechaTomaInv)) Then

                '********** Blanqueamos el Grilla Detalle
                Me.GV_Detalle.DataSource = Nothing
                Me.GV_Detalle.DataBind()
                objScript.mostrarMsjAlerta(Me, "La Operación se realizó con éxito.")
            Else
                Throw New Exception
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la operación.")
        End Try



    End Sub

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click

        anularDocumentoTomaInv(CInt(Me.hddIdDocumento.Value))

    End Sub
    Private Sub anularDocumentoTomaInv(ByVal IdDocumento As Integer)

        Try

            If ((New Negocio.TomaInventario).DocumentTomaInventarioAnularxIdDocumento(IdDocumento)) Then


                hddFrmModo.Value = "6"
                actualizarControlesFrmModo()

                objScript.mostrarMsjAlerta(Me, "La anulación del documento se realizó con éxito.")

            Else

                Throw New Exception("Problemas en la anulación del Documento.")

            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub
End Class