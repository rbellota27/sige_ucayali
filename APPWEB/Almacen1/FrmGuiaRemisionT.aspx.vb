'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmGuiaRemisionT
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private listaCatalogo As List(Of Entidades.Catalogo)
    Private objCbo As New Combo
    Private Const _IdMagnitud_Peso As Integer = 1

    Private listaProductoView As List(Of Entidades.ProductoView)
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

#Region "************************ MANEJO DE SESSION"
    Private Function getListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item("listaDetalleDocumento"), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove("listaDetalleDocumento")
        Session.Add("listaDetalleDocumento", lista)
    End Sub
    Private Function getListaCatalogo() As List(Of Entidades.Catalogo)
        Return CType(Session.Item("listaCatalogo"), List(Of Entidades.Catalogo))
    End Function
    Private Sub setListaCatalogo(ByVal lista As List(Of Entidades.Catalogo))
        Session.Remove("listaCatalogo")
        Session.Add("listaCatalogo", lista)
    End Sub
    Private Function getListaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item("listaDocumentoRef"), List(Of Entidades.Documento))
    End Function
    Private Sub setListaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove("listaDocumentoRef")
        Session.Add("listaDocumentoRef", lista)
    End Sub

#End Region

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not Me.IsPostBack) Then
            ConfigurarDatos()
            ValidarPermisos()
            inicializarFrm()

        End If

    End Sub

    Private Sub ValidarPermisos()

    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

                .LLenarCboEstadoDocumento(Me.cboEstado)

                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), False)

                '************** PUNTO DE LLEGADA
                .LLenarCboDepartamento(Me.cboDepto_Llegada)

                '*************** PUNTO DE PARTIDA (SOLO DEPTO)
                .LLenarCboDepartamento(Me.cboDepto_Partida)

                'lineas---
                .llenarCboTipoExistencia(CboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
                'Listar Tipo Documento
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(CboTipoDocumento, CInt(Me.hddIdTipoDocumento.Value), 2, False)

                ' .LlenarCboLinea(Me.cmbLinea_AddProd, True)
                '.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)

                .llenarCboUnidadMedidaxIdMagnitud(Me.cboUnidadMedida_PesoTotal, _IdMagnitud_Peso, False)

            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicioTraslado.Text = Me.txtFechaEmision.Text

            actualizarOpcionesBusquedaDocRef(0, False)

            Me.hddNroFilasxDocumento.Value = CStr((New Negocio.DocumentoView).SelectCantidadFilasDocumento(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.hddIdTipoDocumento.Value)))

            '********************** TRANSPORTISTA
            Me.hddOpcionBusquedaPersona.Value = CStr(2)
            cargarPersona(CInt(Me.cboEmpresa.SelectedValue))


            '***************** PARAMETRO GENERAL
            Dim listaParametro() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer(0) {1})
            If (Me.cboUnidadMedida_PesoTotal.Items.FindByValue(CStr(CInt(listaParametro(0)))) IsNot Nothing) Then
                Me.cboUnidadMedida_PesoTotal.SelectedValue = CStr(CInt(listaParametro(0)))
            End If

            verFrm(FrmModo.Nuevo, False, True, True, True, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean, Optional ByVal inicializarTransportista As Boolean = False)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then
            Session.Remove("listaDetalleDocumento")
            Session.Remove("listaDocumentoRef")
        End If

        If (initSession) Then
            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setListaDocumentoRef(Me.listaDocumentoRef)
        End If

        If (initParametrosGral) Then

            ''******* FECHA DE VCTO
            'Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {1, 2})

        End If

        If (inicializarTransportista) Then

            '********************** TRANSPORTISTA
            Me.hddOpcionBusquedaPersona.Value = CStr(2)
            cargarPersona(CInt(Me.cboEmpresa.SelectedValue))

        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub
    Private Sub ActualizarBotonesControl()

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Transportista.Enabled = False
                Me.Panel_Chofer.Enabled = False
                Me.Panel_Vehiculo.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"

                '************* GENERAMOS CODIGO
                GenerarCodigoDocumento()

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.btnBuscarDocumentoRef.Visible = True


                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_Remitente.Enabled = True
                Me.Panel_PuntoPartida.Enabled = True
                Me.Panel_Destinatario.Enabled = True
                Me.Panel_PuntoLlegada.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Transportista.Enabled = True
                Me.Panel_Chofer.Enabled = True
                Me.Panel_Vehiculo.Enabled = True
                Me.Panel_Obs.Enabled = True


                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"

                '************* GENERAMOS CODIGO
                GenerarCodigoDocumento()

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboAlmacen.Enabled = True
                Me.btnBuscarRemitente.Enabled = True
                Me.btnBuscarDestinatario.Enabled = True
                Me.btnBuscarProducto.Enabled = True
                Me.btnLimpiarDetalleDocumento.Enabled = True

            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_Remitente.Enabled = True
                Me.Panel_PuntoPartida.Enabled = True
                Me.Panel_Destinatario.Enabled = True
                Me.Panel_PuntoLlegada.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Transportista.Enabled = True
                Me.Panel_Chofer.Enabled = True
                Me.Panel_Vehiculo.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Transportista.Enabled = False
                Me.Panel_Chofer.Enabled = False
                Me.Panel_Vehiculo.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True


            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True

                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Transportista.Enabled = False
                Me.Panel_Chofer.Enabled = False
                Me.Panel_Vehiculo.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False

                Me.btnBuscarDocumentoRef.Visible = False

                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Transportista.Enabled = False
                Me.Panel_Chofer.Enabled = False
                Me.Panel_Vehiculo.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Transportista.Enabled = False
                Me.Panel_Chofer.Enabled = False
                Me.Panel_Vehiculo.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboEstado.SelectedValue = "2"

        End Select
    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub

    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaInicioTraslado.Text = Me.txtFechaEmision.Text
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto

        If (Me.cboTipoOperacion.Items.FindByValue(CStr(0)) IsNot Nothing) Then
            Me.cboTipoOperacion.SelectedValue = "0"
        End If

        '************** DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        '************ REMITENTE
        Me.txtNombre_Remitente.Text = ""
        Me.txtIdRemitente.Text = ""
        Me.txtDNI_Remitente.Text = ""
        Me.txtRUC_Remitente.Text = ""
        Me.cboAlmacen.Items.Clear()



        '*********** PUNTO PARTIDA
        If (Me.cboDepto_Partida.Items.FindByValue("00") IsNot Nothing) Then
            Me.cboDepto_Partida.SelectedValue = "00"
            Me.cboProvincia_Partida.Items.Clear()
            Me.cboDistrito_Partida.Items.Clear()

        End If
        Me.txtDireccion_Partida.Text = ""

        '************ DESTINATARIO
        Me.txtDestinatario.Text = ""
        Me.txtIdDestinatario.Text = ""
        Me.txtDni_Destinatario.Text = ""
        Me.txtRuc_Destinatario.Text = ""
        Me.cboAlmacen_Destinatario.Items.Clear()

        '*********** PUNTO LLEGADA
        If (Me.cboDepto_Llegada.Items.FindByValue("00") IsNot Nothing) Then
            Me.cboDepto_Llegada.SelectedValue = "00"
            Me.cboProvincia_Llegada.Items.Clear()
            Me.cboDistrito_Llegada.Items.Clear()

        End If
        Me.txtDireccion_Llegada.Text = ""

        '*********** DETALLES
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()
        Me.txtPesoTotal.Text = "0"

        '************ TRANSPORTISTA
        Me.txtTransportista.Text = ""
        Me.txtDni_Transportista.Text = ""
        Me.txtRuc_Transportista.Text = ""
        Me.txtDireccion_Transportista.Text = ""

        '************ CHOFER
        Me.txtChofer.Text = ""
        Me.txtDni_Chofer.Text = ""
        Me.txtRuc_Chofer.Text = ""
        Me.txtNroLicencia.Text = ""

        '************ VEHICULO
        Me.txtModelo_Vehiculo.Text = ""
        Me.txtPlaca_Vehiculo.Text = ""
        Me.txtCertificado_Vehiculo.Text = ""

        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""

        '************* HIDDEN

        Me.hddCodigoDocumento.Value = ""
        Me.hddIdChofer.Value = ""
        Me.hddIdDestinatario.Value = ""
        Me.hddIdDocumento.Value = ""
        Me.hddIdRemitente.Value = ""
        Me.hddIdTransportista.Value = ""
        Me.hddIdVehiculo.Value = ""
        Me.hddIndex_GV_Detalle.Value = ""
        Me.hddIndexGlosa_Gv_Detalle.Value = ""
        Me.hddOpcionBusquedaPersona.Value = "0"  '**** REMITENTE

        '************* OTROS
        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()
        Me.gvBuscar.DataSource = Nothing
        Me.gvBuscar.DataBind()
        Me.GV_Vehiculo.DataSource = Nothing
        Me.GV_Vehiculo.DataBind()

    End Sub
    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

            GenerarCodigoDocumento()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)

        If (CInt(Me.hddOpcionBusquedaPersona.Value) = 3) Then   '*********** CHOFER
            listaPersona = (New Negocio.Chofer).ChoferSelectActivoxParams(dni, ruc, RazonApe, tipo, index, gv.PageSize, estado)
        Else
            listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        End If

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO

            Select Case CInt(Me.hddOpcionBusquedaPersona.Value)

                Case 0  '****** REMITENTE
                    ViewState.Add("rol", 0)
                Case 1  '****** DESTINATARIO
                    ViewState.Add("rol", 0)  '*** TODOS LOS ROLES
                Case 2 '******* TRANSPORTISTA
                    ViewState.Add("rol", 4)
                Case 3 '******* CHOFER

            End Select

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")

        Dim objCbo As New Combo

        Select Case CInt(Me.hddOpcionBusquedaPersona.Value)

            Case 0  '****** REMITENTE
                Me.txtNombre_Remitente.Text = objPersona.Descripcion
                Me.txtIdRemitente.Text = CStr(objPersona.IdPersona)
                Me.txtDNI_Remitente.Text = objPersona.Dni
                Me.txtRUC_Remitente.Text = objPersona.Ruc
                Me.hddIdRemitente.Value = CStr(objPersona.IdPersona)

                objCbo.llenarCboAlmacenxIdEmpresa(Me.cboAlmacen, CInt(objPersona.IdPersona), True)

                '**************** PARTIDA
                Dim ubigeo As String = objPersona.Ubigeo
                If (ubigeo.Trim.Length <= 0) Then
                    ubigeo = "000000"
                End If
                Dim codDepto As String = ubigeo.Substring(0, 2)
                Dim codProv As String = ubigeo.Substring(2, 2)
                Dim codDist As String = ubigeo.Substring(4, 2)

                If (Me.cboDepto_Partida.Items.FindByValue(CStr(codDepto)) IsNot Nothing) Then
                    Me.cboDepto_Partida.SelectedValue = codDepto
                    objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, codDepto)
                End If

                If (Me.cboProvincia_Partida.Items.FindByValue(CStr(codProv)) IsNot Nothing) Then
                    Me.cboProvincia_Partida.SelectedValue = codProv
                    objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, codDepto, codProv)
                End If

                If (Me.cboDistrito_Partida.Items.FindByValue(CStr(codDist)) IsNot Nothing) Then
                    Me.cboDistrito_Partida.SelectedValue = codDist
                End If

                Me.txtDireccion_Partida.Text = objPersona.Direccion

            Case 1  '****** DESTINATARIO

                Me.txtDestinatario.Text = objPersona.Descripcion
                Me.txtIdDestinatario.Text = CStr(objPersona.IdPersona)
                Me.txtDni_Destinatario.Text = objPersona.Dni
                Me.txtRuc_Destinatario.Text = objPersona.Ruc
                Me.hddIdDestinatario.Value = CStr(objPersona.IdPersona)

                objCbo.llenarCboAlmacenxIdEmpresa(Me.cboAlmacen_Destinatario, objPersona.IdPersona, True)

                '**************** LLEGADA
                Dim ubigeo As String = objPersona.Ubigeo
                If (ubigeo.Trim.Length <= 0) Then
                    ubigeo = "000000"
                End If
                Dim codDepto As String = ubigeo.Substring(0, 2)
                Dim codProv As String = ubigeo.Substring(2, 2)
                Dim codDist As String = ubigeo.Substring(4, 2)

                If (Me.cboDepto_Llegada.Items.FindByValue(CStr(codDepto)) IsNot Nothing) Then
                    Me.cboDepto_Llegada.SelectedValue = codDepto
                    objCbo.LLenarCboProvincia(Me.cboProvincia_Llegada, codDepto)
                End If

                If (Me.cboProvincia_Llegada.Items.FindByValue(CStr(codProv)) IsNot Nothing) Then
                    Me.cboProvincia_Llegada.SelectedValue = codProv
                    objCbo.LLenarCboDistrito(Me.cboDistrito_Llegada, codDepto, codProv)
                End If

                If (Me.cboDistrito_Llegada.Items.FindByValue(CStr(codDist)) IsNot Nothing) Then
                    Me.cboDistrito_Llegada.SelectedValue = codDist
                End If

                Me.txtDireccion_Llegada.Text = objPersona.Direccion

            Case 2 '******* TRANSPORTISTA

                Me.txtTransportista.Text = objPersona.Descripcion
                Me.txtDni_Transportista.Text = objPersona.Dni
                Me.txtRuc_Transportista.Text = objPersona.Ruc
                Me.txtDireccion_Transportista.Text = objPersona.Direccion
                Me.hddIdTransportista.Value = CStr(objPersona.IdPersona)

            Case 3 '******* CHOFER

                Me.txtChofer.Text = objPersona.Descripcion
                Me.txtDni_Chofer.Text = objPersona.Dni
                Me.txtRuc_Chofer.Text = objPersona.Ruc
                Me.txtNroLicencia.Text = objPersona.NroLicencia
                Me.hddIdChofer.Value = CStr(objPersona.IdPersona)

        End Select

        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()

        objScript.offCapa(Me, "capaPersona")

    End Sub

#End Region





    Private Sub btnaddproductos_addprod_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddProductos_AddProd.Click
        addproducto_detalledocumento()
    End Sub

    Private Sub addProducto_DetalleDocumento()
        Try

            '************* ACTUALIZAMOS LA GRILLA
            ActualizarListaDetalleDocumento()

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim listaUnidadMedida_Peso As List(Of Entidades.MagnitudUnidad) = (New Negocio.MagnitudUnidad)._MagnitudUnidadselectxIdMagnitud(_IdMagnitud_Peso)

            For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

                Dim cantidad As Decimal = CDec(CType(Me.DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text)

                '***************** UNIDAD MEDIDA PRINCIPAL
                Dim IdUnidadMedida_ST As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdUnidadMedida_PR"), HiddenField).Value)

                If (cantidad > 0) Then

                    Dim objDetalle As New Entidades.DetalleDocumento
                    With objDetalle

                        .IdUnidadMedidaPrincipal = IdUnidadMedida_ST
                        .CodigoProducto = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(2).Text))
                        .IdProducto = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                        .NomProducto = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(3).Text))
                        .Cantidad = cantidad
                        .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(.IdProducto)
                        .IdUnidadMedida = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)

                        '.IdProducto = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdProducto_Find"), HiddenField).Value)
                        '.NomProducto = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(2).Text))
                        '.Cantidad = cantidad
                        '.ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(.IdProducto)
                        '.IdUnidadMedida = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)

                        .ListaUnidadMedida_Peso = listaUnidadMedida_Peso
                        .IdUnidadMedida_Peso = CInt(Me.cboUnidadMedida_PesoTotal.SelectedValue)

                        '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                        .Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(.IdProducto, IdUnidadMedida_ST, 1, _IdMagnitud_Peso, .IdUnidadMedida_Peso)

                        '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                        .Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUnidadMedida, IdUnidadMedida_ST, 1)

                        .Peso = .Cantidad * .Equivalencia1 * .Equivalencia

                    End With
                    Me.listaDetalleDocumento.Add(objDetalle)

                End If

            Next

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "  calcularPesoTotal('1',null);   valOnClick_btnBuscarProducto();   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub ActualizarListaDetalleDocumento()

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            Me.listaDetalleDocumento(i).IdProducto = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("hdd_IdProducto"), HiddenField).Value)
            Me.listaDetalleDocumento(i).Cantidad = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)
            Me.listaDetalleDocumento(i).Peso = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtPeso"), TextBox).Text)
            Me.listaDetalleDocumento(i).IdUnidadMedida = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
            Me.listaDetalleDocumento(i).IdUnidadMedida_Peso = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida_Peso"), DropDownList).SelectedValue)
            Me.listaDetalleDocumento(i).UMedida = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedItem.ToString)
            Me.listaDetalleDocumento(i).UmPeso = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida_Peso"), DropDownList).SelectedItem.ToString)

        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)

    End Sub

    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
                cboUM.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM
                cboUM.DataBind()

                If (cboUM.Items.FindByValue(CStr(Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then
                    cboUM.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString
                End If

                Dim cboUM_Peso As DropDownList = CType(e.Row.FindControl("cboUnidadMedida_Peso"), DropDownList)
                cboUM_Peso.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUnidadMedida_Peso
                cboUM_Peso.DataBind()

                If (cboUM_Peso.Items.FindByValue(CStr(Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida_Peso)) IsNot Nothing) Then
                    cboUM_Peso.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida_Peso.ToString
                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_Detalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Detalle.SelectedIndexChanged
        quitarDetalleDocumento()
    End Sub
    Private Sub quitarDetalleDocumento()

        Try

            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Me.listaDetalleDocumento.RemoveAt(Me.GV_Detalle.SelectedRow.RowIndex)

            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "  calcularPesoTotal('1',null);     ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnLimpiarDetalleDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarDetalleDocumento.Click
        limpiarDetalleDocumento()
    End Sub
    Private Sub limpiarDetalleDocumento()
        Try

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "  calcularPesoTotal('1',null); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub DGV_AddProd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_AddProd.RowDataBound

        Try

            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
                cboUM.DataSource = Me.listaProductoView(e.Row.RowIndex).ListaUM
                cboUM.DataBind()
                cboUM.SelectedValue = Me.listaProductoView(e.Row.RowIndex).IdUnidadMedida.ToString

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub cboAlmacen_Destinatario_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboAlmacen_Destinatario.SelectedIndexChanged
        Try
            actualizarPuntoLlegadaxIdAlmacen(CInt(Me.cboAlmacen_Destinatario.SelectedValue), True, True, True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub actualizarPuntoLlegadaxIdAlmacen(ByVal IdAlmacen As Integer, ByVal updateDepto As Boolean, ByVal updateProv As Boolean, ByVal updateDist As Boolean)

        '******** VALIDAMOS EL ALMAC�N
        If (IdAlmacen = Nothing) Then Return

        Dim objAlmacen As Entidades.Almacen = (New Negocio.Almacen).SelectxId(IdAlmacen)
        Dim ubigeoAlmacen As String = objAlmacen.Ubigeo
        Dim direccionAlmacen As String = objAlmacen.Direccion

        '***************** PUNTO DE LLEGADA
        Dim objCbo As New Combo
        If (ubigeoAlmacen <> Nothing) Then

            If (updateDepto) Then

                If (Me.cboDepto_Llegada.Items.FindByValue(CStr(ubigeoAlmacen.Substring(0, 2))) IsNot Nothing) Then
                    Me.cboDepto_Llegada.SelectedValue = ubigeoAlmacen.Substring(0, 2)
                    objCbo.LLenarCboProvincia(Me.cboProvincia_Llegada, (Me.cboDepto_Llegada.SelectedValue))
                End If

            End If
            If (updateProv) Then

                If (Me.cboProvincia_Llegada.Items.FindByValue(CStr(ubigeoAlmacen.Substring(2, 2))) IsNot Nothing) Then
                    Me.cboProvincia_Llegada.SelectedValue = ubigeoAlmacen.Substring(2, 2)
                    objCbo.LLenarCboDistrito(Me.cboDistrito_Llegada, Me.cboDepto_Llegada.SelectedValue, (Me.cboProvincia_Llegada.SelectedValue))
                End If

            End If

            If (updateDist) Then

                If (Me.cboDistrito_Llegada.Items.FindByValue(CStr(ubigeoAlmacen.Substring(4, 2))) IsNot Nothing) Then
                    Me.cboDistrito_Llegada.SelectedValue = ubigeoAlmacen.Substring(4, 2)
                End If

            End If

        End If

        Me.txtDireccion_Llegada.Text = direccionAlmacen

    End Sub
    Private Sub cboDepto_Partida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepto_Partida.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, (Me.cboDepto_Partida.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, (Me.cboDepto_Partida.SelectedValue), (Me.cboProvincia_Partida.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboProvincia_Partida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProvincia_Partida.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, (Me.cboDepto_Partida.SelectedValue), (Me.cboProvincia_Partida.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboDepto_Llegada_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepto_Llegada.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboProvincia_Llegada, (Me.cboDepto_Llegada.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboDistrito_Llegada, (Me.cboDepto_Llegada.SelectedValue), (Me.cboProvincia_Llegada.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboProvincia_Llegada_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProvincia_Llegada.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboDistrito_Llegada, (Me.cboDepto_Llegada.SelectedValue), (Me.cboProvincia_Llegada.SelectedValue))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub cboAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboAlmacen.SelectedIndexChanged
        Try

            actualizarPuntoPartidaxIdAlmacen(CInt(Me.cboAlmacen.SelectedValue), True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub actualizarPuntoPartidaxIdAlmacen(ByVal IdAlmacen As Integer, ByVal updateDepto As Boolean, ByVal updateProv As Boolean, ByVal updateDist As Boolean)

        '******** VALIDAMOS EL ALMAC�N
        If (IdAlmacen = Nothing) Then Return

        Dim objAlmacen As Entidades.Almacen = (New Negocio.Almacen).SelectxId(IdAlmacen)
        Dim ubigeoAlmacen As String = objAlmacen.Ubigeo
        Dim direccionAlmacen As String = objAlmacen.Direccion

        '***************** PUNTO DE LLEGADA
        Dim objCbo As New Combo
        If (ubigeoAlmacen <> Nothing) Then

            If (updateDepto) Then

                If (Me.cboDepto_Partida.Items.FindByValue(CStr(ubigeoAlmacen.Substring(0, 2))) IsNot Nothing) Then
                    Me.cboDepto_Partida.SelectedValue = ubigeoAlmacen.Substring(0, 2)
                    objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, (Me.cboDepto_Partida.SelectedValue))
                End If

            End If

            If (updateProv) Then

                If (Me.cboProvincia_Partida.Items.FindByValue(CStr(ubigeoAlmacen.Substring(2, 2))) IsNot Nothing) Then
                    Me.cboProvincia_Partida.SelectedValue = ubigeoAlmacen.Substring(2, 2)
                    objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, Me.cboDepto_Partida.SelectedValue, (Me.cboProvincia_Partida.SelectedValue))
                End If

            End If
            If (updateDist) Then

                If (Me.cboDistrito_Partida.Items.FindByValue(CStr(ubigeoAlmacen.Substring(4, 2))) IsNot Nothing) Then
                    Me.cboDistrito_Partida.SelectedValue = ubigeoAlmacen.Substring(4, 2)
                End If

            End If

        End If

        Me.txtDireccion_Partida.Text = direccionAlmacen

    End Sub


    Private Sub buscarVehiculo()
        Try

            Dim lista As List(Of Entidades.Vehiculo) = (New Negocio.Vehiculo).SelectActivoxNroPlaca(Me.txtNroPlaca_BuscarVehiculo.Text)
            If (lista.Count <= 0) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaVehiculo');   alert('No se hallaron registros.');     ", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaVehiculo');     ", True)
            End If
            Me.GV_Vehiculo.DataSource = lista
            Me.GV_Vehiculo.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Vehiculo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Vehiculo.SelectedIndexChanged
        Try
            cargarVehiculo(CInt(CType(Me.GV_Vehiculo.SelectedRow.FindControl("hddIdVehiculo"), HiddenField).Value))
            objScript.offCapa(Me, "capaVehiculo")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarVehiculo(ByVal IdVehiculo As Integer)

        Dim objVehiculo As Entidades.Vehiculo = (New Negocio.Vehiculo).SelectxIdVehiculo(IdVehiculo)
        With objVehiculo

            Me.txtModelo_Vehiculo.Text = .Modelo
            Me.txtPlaca_Vehiculo.Text = .Placa
            Me.txtCertificado_Vehiculo.Text = .ConstanciaIns
            Me.hddIdVehiculo.Value = CStr(.IdProducto)

        End With

    End Sub

    Private Sub btnBuscarVehiculo_Grilla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarVehiculo_Grilla.Click
        buscarVehiculo()
    End Sub

#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            'If (Me.hddIdDestinatario.Value.Trim.Length > 0 And IsNumeric(Me.hddIdDestinatario.Value)) Then
            '    If (CInt(Me.hddIdDestinatario.Value) > 0) Then
            '        IdPersona = CInt(Me.hddIdDestinatario.Value)
            '    End If
            'End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, CInt(CboTipoDocumento.SelectedValue), fechaInicio, fechafin, False)
            'dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).DocumentoSelectDocxDespachar_V2(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), fechaInicio, fechafin, serie, codigo, IdPersona, CInt(Me.hddIdTipoDocumento.Value))

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub



    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value) _
        , CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdEmpresa_Find"), HiddenField).Value) _
        , CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdAlmacen_Find"), HiddenField).Value) _
        , CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdPersona_Find"), HiddenField).Value))
    End Sub



    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdPersona As Integer) As Boolean

        Me.listaDocumentoRef = getListaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACI�N.")
            End If

        Next

        If ((CInt(Me.cboEmpresa.SelectedValue) <> IdEmpresa) Or (CInt(Me.cboAlmacen.SelectedValue) <> IdAlmacen)) Then Throw New Exception("EL DOCUMENTO SELECCIONADO POSEE EMPRESA/ALMAC�N DISTINTOS A LOS SELECCIONADOS. NO SE PERMITE LA OPERACI�N.")

        If (CInt(Me.hddIdDestinatario.Value) <> IdPersona) Then Throw New Exception("EL DOCUMENTO SELECCIONADO POSEE UN DESTINATARIO DISTINTO AL SELECCIONADO.")

        Return True

    End Function

    Private Sub cargarDocumentoRef_GUI(ByVal objDocumento As Entidades.Documento, ByVal listaDocumento As List(Of Entidades.Documento), ByVal objRemitente As Entidades.PersonaView, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objDestinatario As Entidades.PersonaView, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objTransportista As Entidades.PersonaView, ByVal objChofer As Entidades.PersonaView, ByVal objVehiculo As Entidades.Vehiculo, ByVal objObservaciones As Entidades.Observacion, ByVal cargarGuiaRemision As Boolean)

        Dim objCbo As New Combo

        If (cargarGuiaRemision) Then
            With objDocumento

                Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)
                Me.txtCodigoDocumento.Text = .Codigo

                If (.FechaEmision <> Nothing) Then
                    Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
                End If
                If (.FechaIniTraslado <> Nothing) Then
                    Me.txtFechaInicioTraslado.Text = Format(.FechaIniTraslado, "dd/MM/yyyy")
                End If

                Me.hddIdDocumento.Value = CStr(.Id)
                Me.hddCodigoDocumento.Value = .Codigo



            End With
        End If

        If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
            Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
        End If

        '**************** LISTA DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()

        '******************* REMITENTE
        If (objRemitente IsNot Nothing) Then

            Me.txtNombre_Remitente.Text = objRemitente.Descripcion
            Me.txtDNI_Remitente.Text = objRemitente.Dni
            Me.txtRUC_Remitente.Text = objRemitente.Ruc
            Me.txtIdRemitente.Text = CStr(objRemitente.IdPersona)
            Me.hddIdRemitente.Value = CStr(objRemitente.IdPersona)

            objCbo.llenarCboAlmacenxIdEmpresa(Me.cboAlmacen, CInt(objRemitente.IdPersona), True)

        End If

        '****************** PUNTO PARTIDA
        If (objPuntoPartida IsNot Nothing) Then

            If (Me.cboDepto_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(0, 2))) IsNot Nothing) Then
                Me.cboDepto_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(0, 2)
                Me.cboProvincia_Partida.Items.Clear()
                objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, Me.cboDepto_Partida.SelectedValue)
            End If

            If (Me.cboProvincia_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(2, 2))) IsNot Nothing) Then
                Me.cboProvincia_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(2, 2)
                Me.cboDistrito_Partida.Items.Clear()
                objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, Me.cboDepto_Partida.SelectedValue, Me.cboProvincia_Partida.SelectedValue)
            End If

            If (Me.cboDistrito_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(4, 2))) IsNot Nothing) Then
                Me.cboDistrito_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(4, 2)
            End If

            If (Me.cboAlmacen.Items.FindByValue(CStr(objPuntoPartida.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacen.SelectedValue = CStr(objPuntoPartida.IdAlmacen)
            End If

            Me.txtDireccion_Partida.Text = objPuntoPartida.Direccion

        End If

        '***************** DESTINATARIO
        If (objDestinatario IsNot Nothing) Then

            Me.txtDestinatario.Text = objDestinatario.Descripcion
            Me.txtDni_Destinatario.Text = objDestinatario.Dni
            Me.txtRuc_Destinatario.Text = objDestinatario.Ruc
            Me.txtIdDestinatario.Text = CStr(objDestinatario.IdPersona)
            Me.hddIdDestinatario.Value = CStr(objDestinatario.IdPersona)

            objCbo.llenarCboAlmacenxIdEmpresa(Me.cboAlmacen_Destinatario, objDestinatario.IdPersona, True)

        End If

        '***************** PUNTO DE LLEGADA
        If (objPuntoLlegada IsNot Nothing) Then

            If (Me.cboDepto_Llegada.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(0, 2))) IsNot Nothing) Then
                Me.cboDepto_Llegada.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(0, 2)
                Me.cboProvincia_Llegada.Items.Clear()
                objCbo.LLenarCboProvincia(Me.cboProvincia_Llegada, Me.cboDepto_Llegada.SelectedValue)
            End If

            If (Me.cboProvincia_Llegada.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(2, 2))) IsNot Nothing) Then
                Me.cboProvincia_Llegada.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(2, 2)
                Me.cboDistrito_Llegada.Items.Clear()
                objCbo.LLenarCboDistrito(Me.cboDistrito_Llegada, Me.cboDepto_Llegada.SelectedValue, Me.cboProvincia_Llegada.SelectedValue)
            End If

            If (Me.cboDistrito_Llegada.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(4, 2))) IsNot Nothing) Then
                Me.cboDistrito_Llegada.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(4, 2)
            End If

            Me.txtDireccion_Llegada.Text = objPuntoLlegada.pll_Direccion

            If (Me.cboAlmacen_Destinatario.Items.FindByValue(CStr(objPuntoLlegada.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacen_Destinatario.SelectedValue = CStr(objPuntoLlegada.IdAlmacen)
            End If

        End If

        Me.GV_Detalle.DataSource = listaDetalleDocumento
        Me.GV_Detalle.DataBind()

        '******************* TRANSPORTISTA
        If (objTransportista IsNot Nothing) Then

            Me.txtTransportista.Text = objTransportista.Descripcion
            Me.txtDni_Transportista.Text = objTransportista.Dni
            Me.txtRuc_Transportista.Text = objTransportista.Ruc
            Me.txtDireccion_Transportista.Text = objTransportista.Direccion
            Me.hddIdTransportista.Value = CStr(objTransportista.IdPersona)

        End If

        '******************* CHOFER
        If (objChofer IsNot Nothing) Then

            Me.txtChofer.Text = objChofer.Descripcion
            Me.txtDni_Chofer.Text = objChofer.Dni
            Me.txtRuc_Chofer.Text = objChofer.Ruc
            Me.txtNroLicencia.Text = objChofer.NroLicencia
            Me.hddIdChofer.Value = CStr(objChofer.IdPersona)

        End If

        '******************* VEHICULO
        If (objVehiculo IsNot Nothing) Then

            Me.txtModelo_Vehiculo.Text = objVehiculo.Modelo
            Me.txtPlaca_Vehiculo.Text = objVehiculo.Placa
            Me.txtCertificado_Vehiculo.Text = objVehiculo.ConstanciaIns
            Me.hddIdVehiculo.Value = CStr(objVehiculo.IdProducto)

        End If

        '************** OBSERVACIONES
        If (objObservaciones IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservaciones.Observacion
        End If

        '**************** LISTA DOC REF
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()

    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getListaDocumentoRef()
            Me.listaDocumentoRef.RemoveAt(index)
            setListaDocumentoRef(Me.listaDocumentoRef)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region


    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click

        registrarDocumento()

    End Sub


    Private Sub validarFrm()

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        If (Me.listaDetalleDocumento.Count > CInt(Me.hddNroFilasxDocumento.Value)) Then
            Throw New Exception("EL N�MERO M�XIMO DE FILAS PARA EL DOCUMENTO ES DE < " + CStr(Math.Round(CInt(Me.hddNroFilasxDocumento.Value), 0)) + " >. NO SE PERMITE LA OPERACI�N.")
        End If

    End Sub


    Private Sub registrarDocumento()

        Try

            ActualizarListaDetalleDocumento()

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
            Dim objPuntoPartida As Entidades.PuntoPartida = obtenerPuntoPartida()
            Dim objPuntoLlegada As Entidades.PuntoLlegada = obtenerPuntoLlegada()
            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo

                    objDocumento.Id = (New Negocio.DocumentoGuiaRemisionT).registrarDocumento(objDocumento, listaRelacionDocumento, objPuntoPartida, objPuntoLlegada, Me.listaDetalleDocumento, objObservaciones)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se registr� con �xito.")

                Case FrmModo.Editar

                    objDocumento.Id = (New Negocio.DocumentoGuiaRemisionT).actualizarDocumento(objDocumento, listaRelacionDocumento, objPuntoPartida, objPuntoLlegada, Me.listaDetalleDocumento, objObservaciones)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se actualiz� con �xito.")

            End Select

            verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)
        Me.listaDocumentoRef = getListaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            Dim objRelacionDocumento As New Entidades.RelacionDocumento
            With objRelacionDocumento

                .IdDocumento1 = Me.listaDocumentoRef(i).Id

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento2 = Nothing
                    Case FrmModo.Editar
                        .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                End Select

            End With

            lista.Add(objRelacionDocumento)

        Next

        Return lista

    End Function
    Private Function obtenerPuntoLlegada() As Entidades.PuntoLlegada

        Dim objPuntoLlegada As Entidades.PuntoLlegada = Nothing

        objPuntoLlegada = New Entidades.PuntoLlegada
        With objPuntoLlegada

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            .pll_Direccion = Me.txtDireccion_Llegada.Text.Trim

            If (Me.cboAlmacen_Destinatario.Items.Count > 0) Then
                .IdAlmacen = CInt(Me.cboAlmacen_Destinatario.SelectedValue)
            Else
                .IdAlmacen = Nothing
            End If


            .pll_Ubigeo = Me.cboDepto_Llegada.SelectedValue + Me.cboProvincia_Llegada.SelectedValue + Me.cboDistrito_Llegada.SelectedValue

        End With

        Return objPuntoLlegada

    End Function
    Private Function obtenerPuntoPartida() As Entidades.PuntoPartida

        Dim objPuntoPartida As Entidades.PuntoPartida = Nothing


        objPuntoPartida = New Entidades.PuntoPartida

        With objPuntoPartida

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            .Direccion = Me.txtDireccion_Partida.Text.Trim

            If (CInt(Me.cboEmpresa.SelectedValue) = CInt(Me.hddIdRemitente.Value)) Then
                .IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)
            Else
                .IdAlmacen = Nothing
            End If

            .Ubigeo = Me.cboDepto_Partida.SelectedValue + Me.cboProvincia_Partida.SelectedValue + Me.cboDistrito_Partida.SelectedValue

        End With


        Return objPuntoPartida

    End Function
    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .FechaIniTraslado = CDate(Me.txtFechaInicioTraslado.Text)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))

            .IdPersona = CInt(Me.hddIdDestinatario.Value)
            .IdRemitente = CInt(Me.hddIdRemitente.Value)
            .IdDestinatario = CInt(Me.hddIdDestinatario.Value)

            If (IsNumeric(Me.hddIdTransportista.Value) And Me.hddIdTransportista.Value.Trim.Length > 0) Then
                .IdTransportista = CInt(Me.hddIdTransportista.Value)
            End If

            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)

            If (IsNumeric(Me.hddIdTransportista.Value) And Me.hddIdTransportista.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdTransportista.Value) > 0) Then
                    .IdTransportista = CInt(Me.hddIdTransportista.Value)
                End If
            End If

            If (IsNumeric(Me.hddIdChofer.Value) And Me.hddIdChofer.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdChofer.Value) > 0) Then
                    .IdChofer = CInt(Me.hddIdChofer.Value)
                End If
            End If
            If (IsNumeric(Me.hddIdVehiculo.Value) And Me.hddIdVehiculo.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdVehiculo.Value) > 0) Then
                    .IdVehiculo = CInt(Me.hddIdVehiculo.Value)
                End If
            End If

        End With

        Return objDocumento

    End Function
    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdPersona As Integer)

        Try

            ActualizarListaDetalleDocumento()

            '******************** INICIALIZAMOS EL FRM CUANDO NO EXISTE UN DOC. DE REFERENCIA
            If (Me.GV_DocumentoRef.Rows.Count <= 0) Then
                verFrm(FrmModo.Nuevo, True, True, True, True, False)
            Else
                If (Not validar_AddDocumentoRef(IdDocumentoRef, IdEmpresa, IdAlmacen, IdPersona)) Then Throw New Exception("No se permite la Operaci�n.")
            End If

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)
            Dim objRemitente As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdRemitente)
            Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumento.Id)
            Dim objDestinatario As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdDestinatario)
            Dim objPuntoLlegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(objDocumento.Id)

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDetalleDocumento.AddRange(obtenerListaDetalleDocumento_Ref(objDocumento.Id))

            Dim objTransportista As Entidades.PersonaView = Nothing '***(New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdTransportista)
            Dim objChofer As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdChofer)
            Dim objVehiculo As Entidades.Vehiculo = (New Negocio.Vehiculo).SelectxIdVehiculo(objDocumento.IdVehiculo)

            '*************** GUARDAMOS EN SESSION
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.listaDocumentoRef = getListaDocumentoRef()
            Me.listaDocumentoRef.Add(objDocumento)
            setListaDocumentoRef(Me.listaDocumentoRef)

            '************** EL PUNTO DE PARTIDA NO SE TOMA XQ PRODUCE PROBLEMAS
            cargarDocumentoRef_GUI(objDocumento, Me.listaDocumentoRef, objRemitente, objPuntoPartida, objDestinatario, objPuntoLlegada, Me.listaDetalleDocumento, objTransportista, objChofer, objVehiculo, Nothing, False)

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()


            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularPesoTotal('1',null); ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
    Private Function obtenerListaDetalleDocumento_Ref(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)
        Dim listaUnidadMedida_Peso As List(Of Entidades.MagnitudUnidad) = (New Negocio.MagnitudUnidad)._MagnitudUnidadselectxIdMagnitud(_IdMagnitud_Peso)

        For i As Integer = 0 To listaDetalle.Count - 1

            With listaDetalle(i)


                .IdDetalleDocumento = Nothing
                .ListaUnidadMedida_Peso = listaUnidadMedida_Peso
                .IdUnidadMedida_Peso = CInt(Me.cboUnidadMedida_PesoTotal.SelectedValue)
                .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)
                '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                .Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(.IdProducto, .IdUnidadMedidaPrincipal, 1, _IdMagnitud_Peso, .IdUnidadMedida_Peso)
                '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                .Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUnidadMedida, .IdUnidadMedidaPrincipal, 1)
                .Peso = .Cantidad * .Equivalencia1 * .Equivalencia

            End With

        Next

        Return listaDetalle

    End Function
#Region "************************ B�SQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdDestinatario.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            cargarDocumentoGuiaRemision(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarDocumentoGuiaRemision(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)

        '**************** LIMPIAMOS EL FORMULARIO
        verFrm(FrmModo.Inicio, True, True, True, True, True)

        '****************** OBTENEMOS LOS DATOS DEL DOCUMENTO
        Dim objDocumento As Entidades.Documento = Nothing
        If (IdDocumento <> Nothing) Then
            objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
        Else
            objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
        End If

        Dim objRemitente As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdRemitente)
        Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumento.Id)
        Dim objDestinatario As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdDestinatario)
        Dim objPuntoLlegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(objDocumento.Id)

        Me.listaDocumentoRef = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(objDocumento.Id)
        Me.listaDetalleDocumento = obtenerListaDetalleDocumento_onLoad(objDocumento.Id)

        Dim objTransportista As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdTransportista)
        Dim objChofer As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdChofer)
        Dim objVehiculo As Entidades.Vehiculo = (New Negocio.Vehiculo).SelectxIdVehiculo(objDocumento.IdVehiculo)
        Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

        '*************** GUARDAMOS EN SESSION
        setListaDetalleDocumento(Me.listaDetalleDocumento)
        setListaDocumentoRef(Me.listaDocumentoRef)

        '************** 
        cargarDocumentoRef_GUI(objDocumento, Me.listaDocumentoRef, objRemitente, objPuntoPartida, objDestinatario, objPuntoLlegada, Me.listaDetalleDocumento, objTransportista, objChofer, objVehiculo, objObservaciones, True)
        verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, False)

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularPesoTotal('1',null); ", True)

    End Sub

    Private Function obtenerListaDetalleDocumento_onLoad(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim lista As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)
        Dim listaUnidadMedida_Peso As List(Of Entidades.MagnitudUnidad) = (New Negocio.MagnitudUnidad)._MagnitudUnidadselectxIdMagnitud(_IdMagnitud_Peso)

        For i As Integer = 0 To lista.Count - 1

            lista(i).ListaUnidadMedida_Peso = listaUnidadMedida_Peso
            lista(i).ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(lista(i).IdProducto)

        Next

        Return lista

    End Function

#End Region

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Try

            verFrm(FrmModo.Editar, False, False, False, False)

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDocumentoRef = getListaDocumentoRef()

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, " onLoad ", " calcularPesoTotal('1',null); ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAnular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAnular.Click
        anularDocumentoGuiaRemision()
    End Sub
    Private Sub anularDocumentoGuiaRemision()
        Try

            Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)

            If ((New Negocio.DocumentoGuiaRemisionT).anularDocumento(IdDocumento)) Then
                objScript.mostrarMsjAlerta(Me, "El Proceso finaliz� con �xito.")
                verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False)
            Else
                Throw New Exception("PROBLEMAS EN LA OPERACI�N.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click

        valOnClick_btnBuscarDocumentoxCodigo()

    End Sub

    Private Sub valOnClick_btnBuscarDocumentoxCodigo()
        Try
            cargarDocumentoGuiaRemision(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value.Trim), 0)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True, True)
    End Sub

    Protected Sub btnBuscarRemitente_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarRemitente.Click

        mostrarCapaPersona("0")

    End Sub
    Private Sub mostrarCapaPersona(ByVal opcion As String)

        Try

            Me.gvBuscar.DataSource = Nothing
            Me.gvBuscar.DataBind()

            Me.hddOpcionBusquedaPersona.Value = opcion

            Select Case CInt(Me.hddOpcionBusquedaPersona.Value)
                Case 0  '************* REMITENTE
                    If (Me.rdbTipoPersona.Items.FindByValue(CStr("J")) IsNot Nothing) Then
                        Me.rdbTipoPersona.SelectedValue = "J"
                    End If
                Case 1  '************ DESTINATARIO
                    If (Me.rdbTipoPersona.Items.FindByValue(CStr("J")) IsNot Nothing) Then
                        Me.rdbTipoPersona.SelectedValue = "J"
                    End If
                Case 2 '**************** TRANSPORTISTA
                    If (Me.rdbTipoPersona.Items.FindByValue(CStr("J")) IsNot Nothing) Then
                        Me.rdbTipoPersona.SelectedValue = "J"
                    End If
                Case 3  '**************** CHOFER
                    If (Me.rdbTipoPersona.Items.FindByValue(CStr("N")) IsNot Nothing) Then
                        Me.rdbTipoPersona.SelectedValue = "N"
                    End If
            End Select

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad  ", "      mostrarCapaPersona();        ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Protected Sub btnBuscarDestinatario_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarDestinatario.Click

        mostrarCapaPersona("1")

    End Sub

    Private Sub btnBuscarTransportista_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarTransportista.Click

        mostrarCapaPersona("2")

    End Sub

    Private Sub btnBuscarChofer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarChofer.Click


        mostrarCapaPersona("3")

    End Sub



    Protected Sub valOnChange_cboUnidadMedida(ByVal sender As Object, ByVal e As System.EventArgs)
        actualizarEquivalencia_Peso(CType(CType(sender, DropDownList).NamingContainer, GridViewRow).RowIndex)
    End Sub
    Private Sub actualizarEquivalencia_Peso(ByVal index As Integer)
        Try

            ActualizarListaDetalleDocumento()

            Me.listaDetalleDocumento = getListaDetalleDocumento()


            With (Me.listaDetalleDocumento(index))

                '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                .Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(.IdProducto, .IdUnidadMedidaPrincipal, 1, _IdMagnitud_Peso, .IdUnidadMedida_Peso)

                '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                .Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUnidadMedida, .IdUnidadMedidaPrincipal, 1)

                .Peso = .Cantidad * .Equivalencia1 * .Equivalencia

            End With

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "  calcularPesoTotal('1',null);   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#Region "************************** BUSQUEDA PRODUCTO"
    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
    End Sub

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", Me.txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", -1)
                    ViewState.Add("IdEmpresa_BuscarProducto", -1)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia", Me.CboTipoExistencia.SelectedValue)

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, ByVal nomProducto As String, ByVal IdAlmacen As Integer, _
                                          ByVal tipoMov As Integer, ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        'Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)


        If listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscrProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
                                      -1, 1, -1, CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 2, -1, CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 3, -1, CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub mostrarCapaStockPrecioxProducto(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            'Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)
            ' .IdProducto = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
            '.NomProducto = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(3).Text))
            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProductoConsultarStockPrecioxProd.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto"), HiddenField).Value)

            Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, 0, 0, (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue))
            Me.GV_ConsultarStockPrecioxProd.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarStockPrecioxProducto');   onCapa('capaBuscarProducto_AddProd');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)


            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region

    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class