﻿Imports System.IO
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Configuration


Partial Public Class FrmInventariosxMxA
    Inherits System.Web.UI.Page

    Private grdTotal As Decimal = 0
    Private grdTotal1 As Decimal = 0
    Private grdTotal2 As Decimal = 0
    Dim totalstockini As Decimal = 0
    Dim totalstockfin As Decimal = 0
    Dim totalcompras As Decimal = 0
    Dim valorcalculadostockini As Decimal = 0
    Dim valorcalculadostockfin As Decimal = 0
    Dim valorcalculadocompras As Decimal = 0
    Dim calculadoiri As Decimal = 0
    Dim calculadondi As Decimal = 0

    Dim cadenaalmacenado As String = ""
    Dim mes As String = ""
    Dim anio As String = ""
    Private ListaAlmacen As List(Of Entidades.TipoAlmacen)
    Dim cbo As Combo
    Private objScript As New ScriptManagerClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            LimpiarFormulario()
            cbo = New Combo
            With cbo
                .LlenarCbo_TipoAlmacen(Me.cbotipoalmacen)
            End With
        End If
    End Sub
    Private Sub LimpiarFormulario()
        Session.Remove("ListaAlmacen")
        lblmensaje.Text = ""
        Me.cboanio.SelectedIndex = 4
        Me.cboMes.SelectedValue = "1"
        gvEscala.Visible = False
        idtexto.Text = ""
        GV_Inventrio.DataSource = Nothing
        GV_Inventrio.DataBind()
        GV_TipoAlmacen.DataSource = Nothing
        GV_TipoAlmacen.DataBind()
        gvEscala.DataSource = Nothing
        gvEscala.DataBind()
        textoiri.Text = ""
        textondi.Text = ""
        valorndi.Visible = False
        valoriri.Visible = False
    End Sub
    Private Sub CargarCrystal()

        Dim cadenagv As String = ""
        Dim valorrecorrido As String = ""
        ''Buscamos los valores elegidos en los ddl
        anio = cboanio.SelectedItem.Text
        mes = cboMes.SelectedValue
        ''Validamos que hayan elegido Tipos de Almacen
        If (GV_TipoAlmacen.Rows.Count) = 0 Then
            objScript.mostrarMsjAlerta(Me, "Elija un Tipo de Almacen para proseguir con la consulta")
        End If
        ''Hacemos el recorrido a la grid para buscar los id de cada  fila elegida
        For Each gvrow As GridViewRow In GV_TipoAlmacen.Rows
            Dim lblTipAlmacen As HiddenField = DirectCast(gvrow.FindControl("hddIdTipoAlmacen"), HiddenField)
            If lblTipAlmacen.Value >= "1" Then
                valorrecorrido = lblTipAlmacen.Value
            End If
            'Almacenamos la data que nos llega en una variable
            cadenagv = valorrecorrido
            ''Separamos por comas
            cadenaalmacenado += cadenagv + ","
        Next

        ''Aqui eliminamos la ultima coma asignada a la cadena
        cadenaalmacenado = cadenaalmacenado.Remove(cadenaalmacenado.Length - 1)

        Dim lista As List(Of Entidades.TipoAlmacen) = (New Negocio.TipoAlmacen).SelectxTipoAlmacenxanioxmes(cadenaalmacenado, anio, mes)
        GV_Inventrio.DataSource = lista
        GV_Inventrio.DataBind()
    End Sub

    Private Property S_ListaAlmacen() As List(Of Entidades.TipoAlmacen)
        Get
            Return CType(Session("ListaAlmacen"), List(Of Entidades.TipoAlmacen))
        End Get
        Set(ByVal value As List(Of Entidades.TipoAlmacen))
            Session.Remove("ListaAlmacen")
            Session.Add("ListaAlmacen", value)
        End Set
    End Property
    Protected Sub add_Click(ByVal sender As Object, ByVal e As EventArgs) Handles add.Click
        Almacen_Add(CInt(cbotipoalmacen.SelectedValue))
    End Sub
    Protected Sub lkbAlmacenRemove_Click(ByVal sender As Object, ByVal e As EventArgs)
        Almacen_Remove(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub Almacen_Remove(ByVal index As Integer)
        S_ListaAlmacen.RemoveAt(index)
        Me.GV_TipoAlmacen.DataSource = S_ListaAlmacen
        Me.GV_TipoAlmacen.DataBind()
    End Sub

    Private Sub Almacen_Add(ByVal IdtipoAlmacen As Integer)
        If IsNothing(S_ListaAlmacen) Then S_ListaAlmacen = New List(Of Entidades.TipoAlmacen)
        Me.S_ListaAlmacen.AddRange((New Negocio.TipoAlmacen).SelectxIdTipoAlmacenAll(IdtipoAlmacen))
        Me.GV_TipoAlmacen.DataSource = Me.S_ListaAlmacen
        Me.GV_TipoAlmacen.DataBind()
    End Sub


    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        lblmensaje.Text = "Reporte de Indicador de Rotacion de Inventario [IRI-NDI] del mes: " + "[" + cboMes.SelectedItem.Text + "]" + "del año: " + "[" + cboanio.SelectedItem.Text + "]"
        idtexto.Text = "ESCALA DE ROTACION DE MERCADERIAS  ANUAL"
        CargarCrystal()
        LlenaGridEscala()
    End Sub

    'Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click

    '    If (GV_Inventrio.Rows.Count = 0) Then
    '        objScript.mostrarMsjAlerta(Me, "Por Favor Ingrese los Criterios de Búsqueda")
    '    Else
    '        ExportToExcel("ReporteInventarioxMesxAnio.xls", GV_Inventrio)
    '    End If
    'End Sub
    'Private Sub btnExporter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExporter.Click
    '    If (GV_Inventrio.Rows.Count = 0) Then
    '        objScript.mostrarMsjAlerta(Me, "Por Favor Ingrese los Criterios de Búsqueda")
    '    Else
    '        ExportToExcel("ReporteInventarioxMesxAnio.xls", GV_Inventrio)
    '    End If    
    'End Sub
    Private Sub ExportToExcel(ByVal nameReport As String, ByVal wControl As GridView)
        Dim responsePage As HttpResponse = Response
        Dim sw As New StringWriter()
        Dim htw As New HtmlTextWriter(sw)
        Dim pageToRender As New Page()
        Dim form As New HtmlForm()
        form.Controls.Add(wControl)
        pageToRender.Controls.Add(form)
        responsePage.Clear()
        responsePage.Buffer = True
        responsePage.ContentType = "application/vnd.xls"
        responsePage.AddHeader("Content-Disposition", "attachment;filename=" & nameReport)
        responsePage.Charset = "UTF-8"
        responsePage.ContentEncoding = Encoding.UTF8
        pageToRender.RenderControl(htw)
        responsePage.Write(sw.ToString())
        responsePage.End()
    End Sub

    Protected Sub btnreporte_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnreporte.Click
        ReporteParameters()
    End Sub
    Private Sub ReporteParameters()

        Dim cadenagv As String = ""
        Dim valorrecorrido As String = ""
        ''Buscamos los valores elegidos en los ddl

        anio = cboanio.SelectedItem.Text
        mes = cboMes.SelectedValue

        ''Validamos que hayan elegido Tipos de Almacen
        If (GV_TipoAlmacen.Rows.Count) = 0 Then
            objScript.mostrarMsjAlerta(Me, "Elija un Tipo de Almacen para proseguir con la consulta")
        End If

        ''Hacemos el recorrido a la grid para buscar los id de cada  fila elegida
        For Each gvrow As GridViewRow In GV_TipoAlmacen.Rows
            Dim lblTipAlmacen As HiddenField = DirectCast(gvrow.FindControl("hddIdTipoAlmacen"), HiddenField)

            If lblTipAlmacen.Value >= "1" Then
                valorrecorrido = lblTipAlmacen.Value

            End If
            'Almacenamos la data que nos llega en una variable
            cadenagv = valorrecorrido

            ''Separamos por comas
            cadenaalmacenado += cadenagv + ","
        Next

        ''Aqui eliminamos la ultima coma asignada a la cadena
        cadenaalmacenado = cadenaalmacenado.Remove(cadenaalmacenado.Length - 1)

        Response.Redirect("../Almacen1/Reportes/Visor.aspx?iReporte=10&Anio=" + anio + "&Mes=" + mes + "&CadenaTipoAlmacen=" + cadenaalmacenado)
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        LimpiarFormulario()
    End Sub

    Protected Sub GV_Inventrio_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Inventrio.RowDataBound
        Dim mes_elegido As Integer = 0
        Dim anio_elegido As Integer = 0
    
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim rowTotal As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "stockinicial"))
            grdTotal = grdTotal + rowTotal

            Dim rowTotal1 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "stockfinal"))
            grdTotal1 = grdTotal1 + rowTotal1

            Dim rowTotal2 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "totalcompras"))
            grdTotal2 = grdTotal2 + rowTotal2

        End If

        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lbl As Label = DirectCast(e.Row.FindControl("lblTotal"), Label)
            lbl.Text = grdTotal.ToString("c")

            Dim lbl1 As Label = DirectCast(e.Row.FindControl("lblTotal1"), Label)
            lbl1.Text = grdTotal1.ToString("c")

            Dim lbl2 As Label = DirectCast(e.Row.FindControl("lblTotal2"), Label)
            lbl2.Text = grdTotal2.ToString("c")


            totalstockini = CDec(grdTotal)

            totalstockfin = CDec(grdTotal1)           

            totalcompras = CDec(grdTotal2)

            mes_elegido = CInt(cboMes.SelectedValue)
            anio_elegido = CInt(cboanio.SelectedValue)

            Dim days As Integer = DateTime.DaysInMonth(anio_elegido, mes_elegido)
            calculadoiri = CDec(totalstockini + totalcompras - totalstockfin) / CDec(totalstockfin) * CDec(365 / days)
            calculadondi = 365 / calculadoiri

            textoiri.Text = CStr(FormatNumber(calculadoiri, 2))
            textondi.Text = CStr(FormatNumber(calculadondi, 2))
            valorndi.Visible = True
            valoriri.Visible = True


        End If
    End Sub
    Private Sub LlenaGridEscala()

        Dim dt As New DataTable
        dt.Columns.Add("-")
        dt.Columns.Add("Indice")
        dt.Columns.Add("Rotación Almacen")

        Dim row1 As DataRow = dt.NewRow
        row1.Item("-") = "EXCELENTE"
        row1.Item("Indice") = "<10.00 - 12.00>"
        row1.Item("Rotación Almacen") = "30 días a 36 días"

        Dim row2 As DataRow = dt.NewRow
        row2.Item("-") = "BUENO"
        row2.Item("Indice") = "<8.00 - 10.00>"
        row2.Item("Rotación Almacen") = "36 días a 45 días"
        Dim row3 As DataRow = dt.NewRow
        row3.Item("-") = "PROMEDIO"
        row3.Item("Indice") = "<6.00 - 8.00>"
        row3.Item("Rotación Almacen") = "45 días a 60 días"

        dt.Rows.Add(row1)
        dt.Rows.Add(row2)
        dt.Rows.Add(row3)
        gvEscala.DataSource = dt
        gvEscala.DataBind()
        gvEscala.Visible = True


    End Sub

    'Protected Sub OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    For Each row As GridViewRow In GV_Inventrio.Rows
    '        If row.RowIndex = GV_Inventrio.SelectedIndex Then
    '            row.BackColor = ColorTranslator.FromHtml("#000099")
    '        Else
    '            row.BackColor = ColorTranslator.FromHtml("#FFFFFF")
    '        End If
    '    Next
    'End Sub

    'Private Sub btnExporter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExporter.Click


    '    Response.Clear()
    '    Response.Buffer = True
    '    Response.ClearContent()
    '    Response.ClearHeaders()
    '    Response.Charset = ""
    '    Dim FileName As String = "Vithal" + DateTime.Now + ".xls"
    '    Dim strwritter As New StringWriter()
    '    Dim htmltextwrtter As New HtmlTextWriter(strwritter)
    '    Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '    Response.ContentType = "application/vnd.ms-excel"
    '    Response.AddHeader("Content-Disposition", Convert.ToString("attachment;filename=") & FileName)
    '    Me.GV_Inventrio.GridLines = GridLines.Both
    '    GV_Inventrio.HeaderStyle.Font.Bold = True
    '    GV_Inventrio.RenderControl(htmltextwrtter)
    '    Response.Write(strwritter.ToString())
    '    Response.[End]()

    '    '=======================================================
    '    'Service provided by Telerik (www.telerik.com)
    '    'Conversion powered by NRefactory.
    '    'Twitter: @telerik
    '    'Facebook: facebook.com/telerik
    '    '=======================================================


    'End Sub
End Class
