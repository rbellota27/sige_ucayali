﻿Imports System.IO
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Configuration

Partial Public Class FrmInventariosxResumenIndica2
    Inherits System.Web.UI.Page

    Dim cbo As Combo
    Private ListaAlmacen As List(Of Entidades.TipoAlmacen)
    Private objScript As New ScriptManagerClass
    Dim varanio As String = ""
    Dim varmesini As String = ""
    Dim varmesfin As String = ""
    Dim cadenagv As String = ""
    Dim cadenaalmacenado As String = ""
    Dim valorrecorrido As String = ""
    Dim NumeroDias As Decimal = 0
    Dim resultadoempresa As Decimal = 0
    Dim totalstockini As Decimal = 0
    Dim totalstockfin As Decimal = 0
    Dim totalcompras As Decimal = 0
    Dim valorcalculadostockini As Decimal = 0
    Dim valorcalculadostockfin As Decimal = 0
    Dim valorcalculadocompras As Decimal = 0
    Dim calculadoiri As Decimal = 0
    Dim calculadondi As Decimal = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptmanager As ScriptManager = scriptmanager.GetCurrent(Me.Page)
        scriptmanager.RegisterPostBackControl(Me.btnExport)
        If Not Me.IsPostBack Then

            Limpiar()
            cbo = New Combo

            With cbo
                .LlenarCboPaisTodos(Me.cmbPais, True)
                .LlenarCbo_TipoAlmacen(Me.cbotipoalmacen)
            End With
            cargarDatosLinea(Me.cmbLinea)
            cargarDatosSubLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue))
        End If

    End Sub
    Private Property S_ListaAlmacen() As List(Of Entidades.TipoAlmacen)
        Get
            Return CType(Session("ListaAlmacen"), List(Of Entidades.TipoAlmacen))
        End Get
        Set(ByVal value As List(Of Entidades.TipoAlmacen))
            Session.Remove("ListaAlmacen")
            Session.Add("ListaAlmacen", value)
        End Set
    End Property
    Private Sub Limpiar()
        Session.Remove("ListaAlmacen")
        Me.cboanio.SelectedIndex = 4
        Me.cboMesFinal.SelectedValue = "1"
        Me.cboMesInicio.SelectedValue = "1"
        Me.GV_TipoAlmacen.DataSource = Nothing
        Me.GV_TipoAlmacen.DataBind()
        Me.GV_Inventrio.DataSource = Nothing
        Me.GV_Inventrio.DataBind()
        Me.lblmensajeiri.Visible = False
        Me.gvEscala.DataSource = Nothing
        Me.gvEscala.DataBind()
        Me.totalempresa.Text = ""
        Me.totalempresandi.Text = ""
        Me.totalempresa.Visible = False
        Me.totalempresandi.Visible = False
        Me.cmbPais.SelectedValue = "0"

    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        cbo = New Combo
        With cbo
            .LlenarCbo_TipoAlmacen(Me.cbotipoalmacen)
        End With
        cargarDatosLinea(Me.cmbLinea)
        cargarDatosSubLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue))
        Limpiar()

    End Sub
    Protected Sub add_Click(ByVal sender As Object, ByVal e As EventArgs) Handles add.Click
        Almacen_Add(CInt(cbotipoalmacen.SelectedValue))
    End Sub

    Protected Sub lkbAlmacenRemove_Click(ByVal sender As Object, ByVal e As EventArgs)
        Almacen_Remove(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub Almacen_Remove(ByVal index As Integer)

        S_ListaAlmacen.RemoveAt(index)

        Me.GV_TipoAlmacen.DataSource = S_ListaAlmacen
        Me.GV_TipoAlmacen.DataBind()

    End Sub
    Private Sub Almacen_Add(ByVal IdtipoAlmacen As Integer)

        If IsNothing(S_ListaAlmacen) Then S_ListaAlmacen = New List(Of Entidades.TipoAlmacen)

        Me.S_ListaAlmacen.AddRange((New Negocio.TipoAlmacen).SelectxIdTipoAlmacenAll(IdtipoAlmacen))

        Me.GV_TipoAlmacen.DataSource = Me.S_ListaAlmacen
        Me.GV_TipoAlmacen.DataBind()

    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        lblmensajeiri.Text = "Total compañia correspondiente al año " + "[" + cboanio.SelectedItem.Text + "]" + ": "
        lblmensajeiri.Visible = True
        LlenaGrillaxParametrosBusquedaIRI()
        LlenaGridEscala()
    End Sub
    Private Sub LlenaGrillaxParametrosBusquedaIRI()

        ''Buscamos los valores elegidos en los ddl
        varanio = cboanio.SelectedItem.Text
        varmesini = cboMesInicio.SelectedValue
        varmesfin = cboMesFinal.SelectedValue

        ''Validamos que hayan elegido Tipos de Almacen
        If (GV_TipoAlmacen.Rows.Count) = 0 Then
            objScript.mostrarMsjAlerta(Me, "Elija un Tipo de Almacen para proseguir con la consulta")
        End If

        ''Hacemos el recorrido a la grid para buscar los id de cada  fila elegida
        For Each gvrow As GridViewRow In GV_TipoAlmacen.Rows
            Dim lblTipAlmacen As HiddenField = DirectCast(gvrow.FindControl("hddIdTipoAlmacen"), HiddenField)

            If lblTipAlmacen.Value >= "1" Then
                valorrecorrido = lblTipAlmacen.Value
            End If
            'Almacenamos la data que nos llega en una variable
            cadenagv = valorrecorrido

            ''Separamos por comas
            cadenaalmacenado += cadenagv + ","
        Next

        ''Aqui eliminamos la ultima coma asignada a la cadena
        cadenaalmacenado = cadenaalmacenado.Remove(cadenaalmacenado.Length - 1)

        Dim obj As Entidades.TipoAlmacen = New Entidades.TipoAlmacen
        obj.CadenaTipoAlmacen = cadenaalmacenado
        obj.Anio = varanio
        obj.MesInicial = varmesini
        obj.MesFinal = varmesfin

        Dim resultempre As Decimal = 0
        Dim resultemprendi As Decimal = 0
        Dim Linea As String = ""
        Dim SubLinea As String = ""
        Dim Pais As String = ""

        Linea = cmbLinea.SelectedItem.Text
        SubLinea = cmbSubLinea.SelectedItem.Text
        Pais = cmbPais.SelectedItem.Text

        If (Linea = "-----") Then
            Linea = Nothing
        End If
        If (SubLinea = "-----") Then
            SubLinea = Nothing
        End If
        If (Pais = "-----") Then
            Pais = Nothing
        End If

        Dim lista As List(Of Entidades.TipoAlmacen) = (New Negocio.TipoAlmacen).ResumenIndicadoresGrillaIRILineaSubLineaAnio(obj.Anio, obj.MesInicial, obj.MesFinal, obj.CadenaTipoAlmacen, Linea, SubLinea, Pais)

        For k As Integer = 0 To lista.Count - 1

            totalstockini += lista(k).StockInicial
            totalstockfin += lista(k).StockFinal
            totalcompras += lista(k).TotalCompras
            NumeroDias = lista(k).Numdias
        Next
        resultadoempresa = (totalstockini + totalcompras - totalstockfin) / (totalstockfin) * (365 / NumeroDias)

        resultempre = CDec(FormatNumber(resultadoempresa, 2))
        resultemprendi = CDec(365 / resultadoempresa)
        totalempresa.Text = " IRI: " + CStr(resultempre)
        totalempresandi.Text = " y NDI: " + CStr(CDec(FormatNumber(resultemprendi, 2)))
        totalempresa.Visible = True
        totalempresandi.Visible = True
        GV_Inventrio.DataSource = lista
        GV_Inventrio.DataBind()




    End Sub

    Private Sub ReporteParameters()
        Dim Linea As String = ""
        Dim SubLinea As String = ""
        Dim Pais As String = ""
        ''Buscamos los valores elegidos en los ddl
        varanio = cboanio.SelectedItem.Text
        varmesini = cboMesInicio.SelectedValue
        varmesfin = cboMesFinal.SelectedValue

        ''Validamos que hayan elegido Tipos de Almacen
        If (GV_TipoAlmacen.Rows.Count) = 0 Then
            objScript.mostrarMsjAlerta(Me, "Elija un Tipo de Almacen para proseguir con la consulta")
        End If

        ''Hacemos el recorrido a la grid para buscar los id de cada  fila elegida
        For Each gvrow As GridViewRow In GV_TipoAlmacen.Rows
            Dim lblTipAlmacen As HiddenField = DirectCast(gvrow.FindControl("hddIdTipoAlmacen"), HiddenField)

            If lblTipAlmacen.Value >= "1" Then
                valorrecorrido = lblTipAlmacen.Value
            End If
            'Almacenamos la data que nos llega en una variable
            cadenagv = valorrecorrido

            ''Separamos por comas
            cadenaalmacenado += cadenagv + ","

        Next

        ''Aqui eliminamos la ultima coma asignada a la cadena
        cadenaalmacenado = cadenaalmacenado.Remove(cadenaalmacenado.Length - 1)

        Linea = cmbLinea.SelectedItem.Text
        SubLinea = cmbSubLinea.SelectedItem.Text
        Pais = cmbPais.SelectedItem.Text
        If (Linea = "-----") Then
            Linea = Nothing
        End If
        If (SubLinea = "-----") Then
            SubLinea = Nothing
        End If
        If (Pais = "-----") Then
            Pais = Nothing
        End If

        Response.Redirect("../../Almacen1/Reportes/Visor.aspx?iReporte=20&Anio=" + varanio + "&MesInicial=" + varmesini + "&MesFinal=" + varmesfin + "&CadenaTipoAlmacen=" + cadenaalmacenado + "&Linea=" + Linea + "&SubLinea=" + SubLinea + "&Pais=" + Pais)

    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click
        If (GV_Inventrio.Rows.Count = 0) Then
            objScript.mostrarMsjAlerta(Me, "Por Favor Ingrese los Criterios de Búsqueda")
        Else
            ExportToExcel("ReporteInventarioxMesxAnio.xls", GV_Inventrio)

        End If
    End Sub
    Private Sub ExportToExcel(ByVal nameReport As String, ByVal wControl As GridView)
        Dim responsePage As HttpResponse = Response
        Dim sw As New StringWriter()
        Dim htw As New HtmlTextWriter(sw)
        Dim pageToRender As New Page()
        Dim form As New HtmlForm()
        Dim form2 As New HtmlForm()
        form.Controls.Add(wControl)
        pageToRender.Controls.Add(form)
        responsePage.Clear()
        responsePage.Buffer = True
        responsePage.ContentType = "application/vnd.ms-excel"
        responsePage.AddHeader("Content-Disposition", "attachment;filename=" & nameReport)
        responsePage.Charset = "UTF-8"
        responsePage.ContentEncoding = Encoding.Default
        pageToRender.RenderControl(htw)
        responsePage.Write(sw.ToString())
        responsePage.End()
    End Sub
    Protected Sub btnreporte_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnreporte.Click
        ReporteParameters()
    End Sub
    'Protected Sub GV_Inventrio_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Inventrio.RowDataBound
    '    Dim mes As Decimal = 0
    '    Dim resultado As Decimal = 0
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        Dim rowTotal As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Enero"))
    '        grdTotal = grdTotal + rowTotal

    '        Dim rowTotal1 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Febrero"))
    '        grdTotal1 = grdTotal1 + rowTotal1

    '        Dim rowTotal2 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Marzo"))
    '        grdTotal2 = grdTotal2 + rowTotal2

    '        Dim rowTotal3 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Abril"))
    '        grdTotal3 = grdTotal3 + rowTotal3

    '        Dim rowTotal4 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Mayo"))
    '        grdTotal4 = grdTotal4 + rowTotal4

    '        Dim rowTotal5 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Junio"))
    '        grdTotal5 = grdTotal5 + rowTotal5

    '        Dim rowTotal6 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Julio"))
    '        grdTotal6 = grdTotal6 + rowTotal6

    '        Dim rowTotal7 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Agosto"))
    '        grdTotal7 = grdTotal7 + rowTotal7

    '        Dim rowTotal8 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Setiembre"))
    '        grdTotal8 = grdTotal8 + rowTotal8

    '        Dim rowTotal9 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Octubre"))
    '        grdTotal9 = grdTotal9 + rowTotal9

    '        Dim rowTotal10 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Noviembre"))
    '        grdTotal10 = grdTotal10 + rowTotal10

    '        Dim rowTotal11 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Diciembre"))
    '        grdTotal11 = grdTotal11 + rowTotal11


    '        mes = CDec(cboMesFinal.SelectedValue)
    '        resultado = CDec(CDec((grdTotal) + CDec(grdTotal1) + CDec(grdTotal2) + CDec(grdTotal3) + CDec(grdTotal4) + CDec(grdTotal5) + CDec(grdTotal6) + CDec(grdTotal7) + CDec(grdTotal8) + CDec(grdTotal9) + CDec(grdTotal10) + CDec(grdTotal11)) / mes)

    '        Dim lbl As Label = DirectCast(e.Row.FindControl("lblpromedio"), Label)
    '        lbl.Text = CStr(FormatNumber(resultado, 2))
    '        LimpiarVariablesPromedio()
    '        resultado = 0

    '    End If
    'End Sub
    'Protected Sub GV_InventarioNDI_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_InventarioNDI.RowDataBound
    '    Dim mes As Decimal = 0
    '    Dim resultado As Decimal = 0
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        Dim rowTotal As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Enero"))
    '        grdTotal = grdTotal + rowTotal

    '        Dim rowTotal1 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Febrero"))
    '        grdTotal1 = grdTotal1 + rowTotal1

    '        Dim rowTotal2 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Marzo"))
    '        grdTotal2 = grdTotal2 + rowTotal2

    '        Dim rowTotal3 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Abril"))
    '        grdTotal3 = grdTotal3 + rowTotal3

    '        Dim rowTotal4 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Mayo"))
    '        grdTotal4 = grdTotal4 + rowTotal4

    '        Dim rowTotal5 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Junio"))
    '        grdTotal5 = grdTotal5 + rowTotal5

    '        Dim rowTotal6 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Julio"))
    '        grdTotal6 = grdTotal6 + rowTotal6

    '        Dim rowTotal7 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Agosto"))
    '        grdTotal7 = grdTotal7 + rowTotal7

    '        Dim rowTotal8 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Setiembre"))
    '        grdTotal8 = grdTotal8 + rowTotal8

    '        Dim rowTotal9 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Octubre"))
    '        grdTotal9 = grdTotal9 + rowTotal9

    '        Dim rowTotal10 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Noviembre"))
    '        grdTotal10 = grdTotal10 + rowTotal10

    '        Dim rowTotal11 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Diciembre"))
    '        grdTotal11 = grdTotal11 + rowTotal11


    '        mes = CDec(cboMesFinal.SelectedValue)
    '        resultado = CDec(CDec((grdTotal) + CDec(grdTotal1) + CDec(grdTotal2) + CDec(grdTotal3) + CDec(grdTotal4) + CDec(grdTotal5) + CDec(grdTotal6) + CDec(grdTotal7) + CDec(grdTotal8) + CDec(grdTotal9) + CDec(grdTotal10) + CDec(grdTotal11)) / mes)

    '        Dim lbl As Label = DirectCast(e.Row.FindControl("lblpromedio"), Label)
    '        lbl.Text = CStr(FormatNumber(resultado, 2))
    '        LimpiarVariablesPromedio2()
    '        resultado = 0

    '    End If

    'End Sub
    'Protected Sub OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    For Each row As GridViewRow In GV_Inventrio.Rows
    '        If row.RowIndex = GV_Inventrio.SelectedIndex Then
    '            row.BackColor = ColorTranslator.FromHtml("#000099")
    '        Else
    '            row.BackColor = ColorTranslator.FromHtml("#FFFFFF")
    '        End If
    '    Next

    Private Sub LlenaGridEscala()

        Dim dt As New DataTable
        dt.Columns.Add("-")
        dt.Columns.Add("Indice")
        dt.Columns.Add("Rotación Almacen")

        Dim row1 As DataRow = dt.NewRow
        row1.Item("-") = "EXCELENTE"
        row1.Item("Indice") = "<10.00 - 12.00>"
        row1.Item("Rotación Almacen") = "30 días a 36 días"

        Dim row2 As DataRow = dt.NewRow
        row2.Item("-") = "BUENO"
        row2.Item("Indice") = "<8.00 - 10.00>"
        row2.Item("Rotación Almacen") = "36 días a 45 días"
        Dim row3 As DataRow = dt.NewRow
        row3.Item("-") = "PROMEDIO"
        row3.Item("Indice") = "<6.00 - 8.00>"
        row3.Item("Rotación Almacen") = "45 días a 60 días"

        dt.Rows.Add(row1)
        dt.Rows.Add(row2)
        dt.Rows.Add(row3)
        gvEscala.DataSource = dt
        gvEscala.DataBind()
        gvEscala.Visible = True


    End Sub
    Private Sub cargarDatosLinea(ByVal combo As DropDownList)
        Dim nLinea As New Negocio.Linea
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo()
        Dim objLinea As New Entidades.Linea(CStr(0), "-----", True)
        lista.Insert(0, objLinea)
        combo.DataSource = lista
        combo.DataBind()
    End Sub
    Private Sub cargarDatosSubLinea(ByVal combo As DropDownList, ByVal idLinea As Integer)
        Dim nSubLinea As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectAllActivoxLinea(idLinea)
        Dim objsubLinea As New Entidades.SubLinea(CStr(0), "-----", True)
        lista.Insert(0, objsubLinea)
        combo.DataSource = lista
        combo.DataBind()
    End Sub
    Protected Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLinea.SelectedIndexChanged
        cargarDatosSubLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue))
    End Sub
End Class