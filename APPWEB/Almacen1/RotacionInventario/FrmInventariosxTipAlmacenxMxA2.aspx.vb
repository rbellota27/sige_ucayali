﻿Partial Public Class FrmInventariosxTipAlmacenxMxA2
    Inherits System.Web.UI.Page

    Dim cbo As Combo
    Private ListaAlmacen As List(Of Entidades.TipoAlmacen)
    Private objScript As New ScriptManagerClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Me.IsPostBack Then
            cargarTiendas()
            Limpiar()
            cbo = New Combo

            With cbo
                .LlenarCbo_TipoAlmacen(Me.cbotipoalmacen)
                .LlenarCboPaisTodos(Me.cmbPais, True)
            End With
            cargarDatosLinea(Me.cmbLinea)
            cargarDatosSubLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue))
        End If

    End Sub

    Private Sub cargarTiendas()
        Dim objNegocioDropDownList As New Negocio.LNValorizadoCajas
        Dim dt As New DataTable
        dt = objNegocioDropDownList.LN_ReturnDataTable("", "TIENDA")
        Me.ddlTienda.DataSource = dt
        Me.ddlTienda.DataValueField = "IdTienda"
        Me.ddlTienda.DataTextField = "tie_Nombre"
        Me.ddlTienda.DataBind()
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        CargarCrystal()
    End Sub
    Private Property S_ListaAlmacen() As List(Of Entidades.TipoAlmacen)
        Get
            Return CType(Session("ListaAlmacen"), List(Of Entidades.TipoAlmacen))
        End Get
        Set(ByVal value As List(Of Entidades.TipoAlmacen))
            Session.Remove("ListaAlmacen")
            Session.Add("ListaAlmacen", value)
        End Set
    End Property

    Private Sub cargarDatosLinea(ByVal combo As DropDownList)
        Dim nLinea As New Negocio.Linea
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo()
        Dim objLinea As New Entidades.Linea(CStr(0), "-----", True)
        lista.Insert(0, objLinea)
        combo.DataSource = lista
        combo.DataBind()
    End Sub
    Private Sub cargarDatosSubLinea(ByVal combo As DropDownList, ByVal idLinea As Integer)
        Dim nSubLinea As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectAllActivoxLinea(idLinea)
        Dim objsubLinea As New Entidades.SubLinea(CStr(0), "-----", True)
        lista.Insert(0, objsubLinea)
        combo.DataSource = lista
        combo.DataBind()
    End Sub
    Private Sub CargarCrystal()
        Dim varanio As String = ""
        Dim varmesini As String = ""
        Dim varmesfin As String = ""
        Dim cadenagv As String = ""
        Dim cadenaalmacenado As String = ""
        Dim valorrecorrido As String = ""
        Dim Linea As String = ""
        Dim SubLinea As String = ""
        Dim Pais As String = ""
        ''Buscamos los valores elegidos en los ddl
        varanio = cboanio.SelectedItem.Text
        varmesini = cboMesInicio.SelectedValue
        varmesfin = cboMesFinal.SelectedValue

        ''Validamos que hayan elegido Tipos de Almacen
        If (GV_TipoAlmacen.Rows.Count) = 0 Then
            objScript.mostrarMsjAlerta(Me, "Elija un Tipo de Almacen para proseguir con la consulta")
        End If

        ''Hacemos el recorrido a la grid para buscar los id de cada  fila elegida
        For Each gvrow As GridViewRow In GV_TipoAlmacen.Rows
            Dim lblTipAlmacen As HiddenField = DirectCast(gvrow.FindControl("hddIdTipoAlmacen"), HiddenField)

            If lblTipAlmacen.Value >= "1" Then
                valorrecorrido = lblTipAlmacen.Value

            End If
            'Almacenamos la data que nos llega en una variable
            cadenagv = valorrecorrido

            ''Separamos por comas
            cadenaalmacenado += cadenagv + ","
        Next
        ''Aqui eliminamos la ultima coma asignada a la cadena
        cadenaalmacenado = cadenaalmacenado.Remove(cadenaalmacenado.Length - 1)


        Linea = cmbLinea.SelectedItem.Text
        SubLinea = cmbSubLinea.SelectedItem.Text
        Pais = cmbPais.SelectedItem.Text

        If (Linea = "-----") Then
            Linea = Nothing
        End If
        If (SubLinea = "-----") Then
            SubLinea = Nothing
        End If
        If (Pais = "-----") Then
            Pais = Nothing
        End If
        Response.Redirect("../../Almacen1/Reportes/Visor.aspx?iReporte=21&Anio=" + varanio + "&MesInicial=" + varmesini + "&MesFinal=" + varmesfin + "&CadenaTipoAlmacen=" + cadenaalmacenado + "&Linea=" + Linea + "&SubLinea=" + SubLinea + "&Pais=" + Pais + "&idTienda=" + Me.ddlTienda.SelectedValue)


    End Sub

    Protected Sub add_Click(ByVal sender As Object, ByVal e As EventArgs) Handles add.Click
        Almacen_Add(CInt(cbotipoalmacen.SelectedValue))
    End Sub


    Protected Sub lkbAlmacenRemove_Click(ByVal sender As Object, ByVal e As EventArgs)
        Almacen_Remove(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub Almacen_Remove(ByVal index As Integer)

        S_ListaAlmacen.RemoveAt(index)

        Me.GV_TipoAlmacen.DataSource = S_ListaAlmacen
        Me.GV_TipoAlmacen.DataBind()

    End Sub

    Private Sub Almacen_Add(ByVal IdtipoAlmacen As Integer)

        If IsNothing(S_ListaAlmacen) Then S_ListaAlmacen = New List(Of Entidades.TipoAlmacen)

        Me.S_ListaAlmacen.AddRange((New Negocio.TipoAlmacen).SelectxIdTipoAlmacenAll(IdtipoAlmacen))

        Me.GV_TipoAlmacen.DataSource = Me.S_ListaAlmacen
        Me.GV_TipoAlmacen.DataBind()

    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click

        cbo = New Combo
        With cbo
            .LlenarCbo_TipoAlmacen(Me.cbotipoalmacen)
            .LlenarCboPaisTodos(Me.cmbPais, True)
        End With
        cargarDatosLinea(Me.cmbLinea)
        cargarDatosSubLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue))
        Limpiar()

    End Sub

    Private Sub Limpiar()
        Session.Remove("ListaAlmacen")
        Me.cboanio.SelectedIndex = 4
        Me.cboMesFinal.SelectedValue = "1"
        Me.cboMesInicio.SelectedValue = "1"
        Me.cmbPais.SelectedValue = "0"
        Me.GV_TipoAlmacen.DataSource = Nothing
        Me.GV_TipoAlmacen.DataBind()
    End Sub

    Protected Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbLinea.SelectedIndexChanged
        cargarDatosSubLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue))
    End Sub
End Class