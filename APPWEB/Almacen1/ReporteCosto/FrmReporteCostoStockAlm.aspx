<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmReporteCostoStockAlm.aspx.vb" Inherits="APPWEB.FrmReporteCostoStockAlm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<table width="100%">
<tr>
    <td class="TituloCelda">
  REPORTE stock / costo / PVP / por almac�n
    </td>
</tr>
 <tr>
    <td>
    <table>
    <tr>
     <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nueva B�squeda" ToolTip="Nueva Busqueda" Width="120px" />
                        </td>
                       
                        <td>
                            <asp:Button ID="btnBuscar" runat="server"  Text="Generar Reporte" ToolTip="Generar Reporte" Width="140px"  />
                        </td>
                       
                       
                        <td>
                            <asp:Button ID="btnExport" runat="server" Text="Exportar a Excel" ToolTip="Exportar Excel" />
                        </td>
   
    </tr>
     </table>  
    </td>                
</tr>
</table>

<table>
<tr> 
    <td> 
    <asp:Label ID="lblTienda" runat="server" Text="Tienda: " CssClass="Texto"></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="ddlTienda" runat="server" AutoPostBack="true" 
     DataTextField="Descripcion" DataValueField="Id" ></asp:DropDownList>
    </td>
    <td></td>
     <td> 
    <asp:Label ID="lblAlmacen" runat="server" Text="Almac�n: " CssClass="Texto"></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="ddlAlmacen" runat="server"
    AutoPostBack="true"  DataTextField="Descripcion" DataValueField="Id" ></asp:DropDownList>
    </td>
</tr>
<tr>
        <td>
            <asp:Label ID="lblAlaFecha" runat="server" Text="A la Fecha: " CssClass="Texto">
            </asp:Label>
        </td>
        <td>
                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" 
                onblur="return(  valFecha(this) );"  Width="100px">  </asp:TextBox>
                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                </cc1:MaskedEditExtender>
                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                </cc1:CalendarExtender>
        </td>
        <td></td>
        <td>
            <asp:Label ID="lblLinea" runat="server" Text="Linea: " CssClass="Texto"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="ddlLinea" runat="server" AutoPostBack="true" 
            DataTextField="Descripcion" DataValueField="Id"></asp:DropDownList>
        </td>
</tr>
<tr>
    <td>
        <asp:Label ID="lblTipoExistencia" runat="server" Text="Tipo Existencia: " CssClass="Texto">
        </asp:Label>
    </td>
    <td>
        <asp:DropDownList ID="ddlTipoExistencia" runat="server"  AutoPostBack="true" 
         DataTextField="Descripcion" DataValueField="Id"></asp:DropDownList>
    </td>
    <td></td>
    <td> 
            <asp:Label ID="lblSubLinea" runat="server" Text="SubLinea: " CssClass="Texto">
        </asp:Label>
    </td>
     <td>
        <asp:DropDownList ID="ddlSubLinea" runat="server" AutoPostBack="true" 
         DataTextField="Nombre" DataValueField="Id"></asp:DropDownList>
    </td>
</tr>
</table>
<table width="100%">
            <tr>
            <td>
                        <asp:GridView ID="GV_ReporteAlmacenSCP" runat="server" AutoGenerateColumns="False" 
                        Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" 
                        BorderWidth="1px" CellPadding="3" >
                            <Columns>
                            <asp:BoundField HeaderText="IdProducto" DataField="IdProducto" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center"  >
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            
                            <%--<asp:BoundField HeaderText="Tipo Almacen" DataField="Tipoalm_Nombre" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>--%>
                            
                            <asp:BoundField HeaderText="Tienda" DataField="Tienda" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            
                             <asp:BoundField HeaderText="Tipo Existencia" DataField="TipoExistencia" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            
                             <asp:BoundField HeaderText="Linea" DataField="Linea" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                           <asp:BoundField HeaderText="SubLinea" DataField="SubLinea" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Pais" DataField="Pais" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="C�digo" DataField="CodigoSige" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            
                            <asp:BoundField HeaderText="Descripci�n" DataField="NombreProducto" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            
                            <asp:BoundField HeaderText="UM" DataField="UM" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            
                            <asp:BoundField HeaderText="Cantidad" DataField="Cantidad" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Costo" DataField="Costo" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            
                            <asp:BoundField HeaderText="PVP" DataField="PVP" HeaderStyle-Height="25px"
                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                            <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            
                            </Columns>
                        <RowStyle CssClass="GrillaRow" ForeColor="#000066" />
                        <FooterStyle CssClass="GrillaFooter" BackColor="White" ForeColor="#000066" />
                        <PagerStyle CssClass="GrillaPager" BackColor="White" ForeColor="#000066" 
                        HorizontalAlign="Left" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" BackColor="#669999" Font-Bold="True" 
                        ForeColor="White" />
                        <HeaderStyle CssClass="GrillaHeader" BackColor="#006699" Font-Bold="True" 
                        ForeColor="White" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        </asp:GridView>
                                                        
                            </td>
                            </tr>
</table>
</asp:Content>
