﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmGenerarDespachoAutomatico.aspx.vb" Inherits="APPWEB.FrmGenerarDespachoAutomatico" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td class="TituloCelda">
                GENERAR DESPACHO AUTOMÁTICO</td>
        </tr>
        <tr>
            <td style="text-align: center">
                <table>
                    <tr>
                        <td class="Texto">
                            Almacén:</td>
                        <td >
                            <asp:DropDownList ID="cboAlmacen" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                        </td>
                        <td>       
                                     
                        </td>
                        <td class="Texto">
                            Tipo Operación:</td>
                        <td>
                            <asp:DropDownList ID="cboTipoOperacion" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: center">
                            <table>
                                <tr>
                                    <td class="Texto" style="font-weight:bold">
                                        Fecha Inicio:</td>
                                    <td>
                                      <asp:TextBox ID="txtFecha_Inicio" runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(valFecha(this));" Width="90px"></asp:TextBox>
                            <cc1:maskededitextender ID="txtFecha_Inicio_MaskedEditExtender" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFecha_Inicio">
                            </cc1:maskededitextender>
                            <cc1:calendarextender ID="txtFecha_Inicio_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFecha_Inicio">
                            </cc1:calendarextender>      
                                    </td>
                                    <td class="Texto" style="font-weight:bold" >
                                        Fecha Fin:</td>
                                    <td>
                                    
                                    <asp:TextBox ID="txtFecha_Fin" runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(valFecha(this));" Width="90px"></asp:TextBox>
                            <cc1:maskededitextender ID="txtFecha_Fin_Maskededitextender" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFecha_Fin">
                            </cc1:maskededitextender>
                            <cc1:calendarextender ID="txtFecha_Fin_Calendarextender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFecha_Fin" >
                            </cc1:calendarextender>      
                                    
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: center">
                            <asp:Button ID="btnGuardar" OnClientClick="return(  valOnClick_btnGuardar()  );" runat="server" Width="195px" 
                                Text="Generar Despacho Automático" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="LabelRojo"  style="font-weight:bold" >
                *** Se recomienda que la fecha de Generación sea dividida en intervalos de [ 7 
                días aprox. ] para no saturar el proceso de Replicación.</td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdTipoDocumento_OD" runat="server" Value="21" />
            </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript" >
        function valOnClick_btnGuardar() {
            var cboAlmacen = document.getElementById('<%=cboAlmacen.ClientID%>');
            if (isNaN(parseInt(cboAlmacen.value)) || cboAlmacen.value.length <= 0 || parseInt(cboAlmacen.value) <= 0) {
                alert('Debe seleccionar un Almacén.');
                return false;
            }
            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operación.');
                return false;
            }
            var txtFechaInicio = document.getElementById('<%=txtFecha_Inicio.ClientID%>');
            var txtFechaFin = document.getElementById('<%=txtFecha_Fin.ClientID%>');

            if (confirm('El Almacén seleccionado es [ ' + getCampoxValorCombo(cboAlmacen, cboAlmacen.value) + ' ] con una Fecha de Generación del [ '+txtFechaInicio.value+' ] al [ '+txtFechaFin.value+' ]. Se recomienda que la fecha de Generación sea dividida en intervalos de [ 7 días aprox. ] para no saturar el proceso. Desea continuar con la operación ?')) {
                return confirm('Desea continuar con la Operación de todos modos ?');
            }
            return false;            
        }
    </script>
</asp:Content>
