<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmGuiaRemision_1.aspx.vb" Inherits="APPWEB.FrmGuiaRemision_1"  title="Gu�a de Remisi�n" %>    

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register assembly="Enlasys.WebControls" namespace="Enlasys.Web.UI.WebControls" tagprefix="Enlasys" %>

<%@ Register assembly="MsgBox" namespace="MsgBox" tagprefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">    <ContentTemplate>    
            <table width="100%">
                <tr>
                    <td >
                        <asp:ImageButton ID="btnNuevo" runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" 
                            onmouseover="this.src='/Imagenes/Nuevo_A.JPG';" 
                            onmouseout="this.src='/Imagenes/Nuevo_b.JPG';" Height="26px" />
                        <asp:ImageButton ID="btnEditar" runat="server" 
                            ImageUrl="~/Imagenes/Editar_B.JPG" 
                            onmouseout="this.src='/Imagenes/Editar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Editar_A.JPG';"  OnClientClick="return(validarEdicion());"
                            Visible="False" />
                        <asp:ImageButton ID="btnAnular" runat="server" 
                            ImageUrl="~/Imagenes/Anular_B.JPG" OnClientClick="return(valAnular());"
                            onmouseout="this.src='/Imagenes/Anular_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Anular_A.JPG';" />
                        <asp:ImageButton ID="btnBuscarGRem" runat="server" 
                            ImageUrl="~/Imagenes/Buscar_b.JPG" 
                            onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                            onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                        <asp:ImageButton ID="btnBuscarGRem_1" runat="server" 
                            ImageUrl="~/Imagenes/Busqueda_b.JPG" 
                            OnClientClick="return(validarBuscarDocumento());" 
                            onmouseout="this.src='/Imagenes/Busqueda_b.JPG';" 
                            onmouseover="this.src='/Imagenes/Busqueda_A.JPG';" style="width: 27px" />
                        <asp:ImageButton ID="btnGrabar" runat="server" 
                            ImageUrl="~/Imagenes/Guardar_B.JPG" OnClientClick="return(valSave());"
                            onmouseover="this.src='/Imagenes/Guardar_A.JPG';" 
                            onmouseout="this.src='/Imagenes/Guardar_B.JPG';" />
                        <asp:ImageButton ID="btnCancelar1" OnClientClick="return(confirm('Desea retroceder?'));" runat="server" 
                            ImageUrl="~/Imagenes/Arriba_B.JPG" 
                            onmouseout="this.src='/Imagenes/Arriba_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Arriba_A.JPG';" ToolTip="Atr�s"                        
                         />
                        <asp:ImageButton ID="btnImprimir" runat="server" Enabled="true"  
                            ImageUrl="~/Imagenes/Imprimir_B.JPG" 
                            OnClientClick="return(valImprimir());" 
                            onmouseout="this.src='/Imagenes/Imprimir_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Imprimir_A.JPG';" />
                        
                        <asp:ImageButton ID="btnDespachar" runat="server" 
                            ImageUrl="~/Imagenes/Despachar_B.JPG" 
                            OnClientClick="return(emitirOrdenDespacho());" 
                            onmouseout="this.src='/Imagenes/Despachar_B.JPG';" 
                            onmouseover="this.src='/Imagenes/Despachar_A.JPG';" TabIndex="5006" />                                                                        
                        </td>
                </tr>
                <tr>
                    <td>
                        
                        <asp:Panel ID="Panel_Frm" runat="server">
                            <table style="width: 100%">
                     <tr>
                    <td class="TituloCelda">
                        GU�A DE REMISI�N</td>
                </tr>
                <tr>
                    <td >
                    
                        <asp:Panel ID="Panel_Cabecera" runat="server">
                        <table>
                            <tr>
                                <td  align="right"> 
                                    <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Empresa:"></asp:Label></td>
                                <td>
                                     <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="True" 
                                            DataTextField="NombreComercial" DataValueField="Id">
                                        </asp:DropDownList>
                                    </td>
                                <td align="right">
                                    <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Tienda:"></asp:Label></td>
                                <td>
                                     <asp:DropDownList ID="cmbTienda" runat="server" AutoPostBack="True"  Width="100%"
                                            DataTextField="Nombre" DataValueField="Id">
                                        </asp:DropDownList></td>
                                <td align="right">
                                     <asp:Label ID="Label20" runat="server" CssClass="Label" Text="Almac�n:"></asp:Label></td>
                                <td>
                                     <asp:DropDownList ID="cmbAlmacen" runat="server" AutoPostBack="True"  Width="100%"
                                            DataTextField="Nombre" DataValueField="IdAlmacen">
                                        </asp:DropDownList>
                                    </td>
                                <td align="right">
                                    <asp:Label ID="Label21" runat="server" CssClass="Label" Text="Tipo Doc.:"></asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="cmbTipoDocumento" runat="server" AutoPostBack="False"  Width="100%"
                                            Enabled="false">
                                            <asp:ListItem Enabled="true" Selected="True" Value="6">Gu�a de Remisi�n</asp:ListItem>
                                        </asp:DropDownList></td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right">
                                   
                                    </td>
                                <td>
                                     
                                    </td>
                                <td align="right">
                                    <asp:Label ID="Label2224" runat="server" CssClass="Label" Text="Estado:"></asp:Label>
                                    </td>
                                <td>
                                    <asp:DropDownList ID="cmbEstadoDocumento" runat="server" CssClass="LabelRojo" Enabled="false">                                                                                    
                                        </asp:DropDownList>
                                    </td>
                                <td align="right">
                                    <asp:Label ID="Label2227" runat="server" Text="Est. Entrega:" CssClass="Label"></asp:Label>
                                    </td>
                                <td>
                                    <asp:DropDownList ID="cmbEstadoEntrega" Enabled="false" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                <td align="right">
                                    <asp:Label ID="Label77" runat="server" CssClass="Label" Text="N� Serie:"></asp:Label>
                                    </td>
                                <td>
                                     <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True" 
                                            Font-Bold="True"> </asp:DropDownList>
                                            
                                            <asp:TextBox ID="txtCodigoDocumento" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));" CssClass="TextBox_ReadOnly" 
                                           ReadOnly="true" Font-Bold="True" Width="104px"></asp:TextBox>
                                    </td>
                                <td align="right">                                    
                                    </td>
                                <td>
                                       
                                        
                                    </td>
                            </tr>                                                
                        </table>
                        </asp:Panel>
                    
                    
                        
                    </td>
                </tr> 
                <tr>
                <td>
                
                <asp:Label ID="Label1" runat="server" CssClass="Label" 
                                            Text="Fecha de Remisi�n:"></asp:Label>
                                    
                                     <cc1:CalendarExtender ID="txtFecha_CalendarExtender" runat="server" 
                                            TargetControlID="txtFecha" Enabled="True" Format="dd/MM/yyyy" >
                                        </cc1:CalendarExtender>    
                                        <asp:TextBox ID="txtFecha" runat="server" onblur="return(valFecha(this));" CssClass="TextBox_Fecha" 
                                             Width="90px"></asp:TextBox>
                                         <cc1:MaskedEditExtender ID="txtFecha_MaskedEditExtender" runat="server" 
                                 CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                                 CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                 CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"                                  
                                 TargetControlID="txtFecha"
                               ClearMaskOnLostFocus="false"    
                                  Mask="99/99/9999">
                             </cc1:MaskedEditExtender>
                                   
                                    <asp:Label ID="Label37" CssClass="Label" runat="server" Text="Fecha de Inicio de Traslado:"></asp:Label>
                                   
                                      <asp:TextBox  Width="90px" CssClass="TextBox_Fecha" ID="txtFechaTraslado" onblur="return(valFecha(this));"  runat="server"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtFechaTraslado_CalendarExtender" runat="server" 
                                            Enabled="True" TargetControlID="txtFechaTraslado" Format="dd/MM/yyyy">
                                        </cc1:CalendarExtender >
                                        
                                           <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" 
                                 CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                                 CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                 CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"                                  
                                 TargetControlID="txtFechaTraslado"
                               ClearMaskOnLostFocus="false"    
                                  Mask="99/99/9999">
                             </cc1:MaskedEditExtender>
                
                </td>
                </tr>                            
                                <tr>
                                    <td>                                                                                                        
                                            
                                        <asp:Panel ID="Panel_DatosDoc" runat="server">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender4" runat="server" 
                                                TargetControlID="Panel_DatosDoc11" 
                                                CollapsedSize="0" 
                                                ExpandedSize="30" 
                                                Collapsed="true"
                                                ExpandControlID="ImageDatosDoc"
                                                CollapseControlID="ImageDatosDoc" 
                                                TextLabelID="LabelDatosDoc" 
                                                ImageControlID="ImageDatosDoc"
                                                CollapsedImage="~/Imagenes/Mas_B.JPG"
                                              ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                CollapsedText="Documento Relacionado" 
                                                ExpandedText="Documento Relacionado"                               
                                                ExpandDirection="Vertical" SuppressPostBack="True">
                                                </cc1:CollapsiblePanelExtender>  
                                                <asp:Image ID="ImageDatosDoc" runat="server" />                                                             
                                            <asp:Label ID="LabelDatosDoc" runat="server" Text="Label" CssClass="Label"></asp:Label>
                                                        <asp:Panel ID="Panel_DatosDoc11" runat="server">
                                                        <table>
                                                        <tr>
                                                        <td class="LabelLeft" style="font-weight:bold;width:100px" align="left">Referencia:</td>
                                                        <td>
                                                            <asp:Label ID="lblTipoDocumento_DocRef" CssClass="LabelRojo" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td style="width:25px">
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblSerie_DocRef" CssClass="LabelRojo" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td style="font-weight:bold" class="LabelRojo">
                                                        -
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCodigo_DocRef" CssClass="LabelRojo" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                        </td>                                                        
                                                        <td style="width:25px" ></td>
                                                        <td>
                                                            <asp:Button ID="btnBuscarDocumentoRef" OnClientClick="return(onCapa('capaBuscarDocumento'));" runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documento de Referencia por Nro. Documento" />
                                                        </td>
                                                        </tr>
                                                        </table>                                                                                                                 
                                                        </asp:Panel>
                                                    </td>
                                                </tr>                                                
                                                <tr>
                                                    <td>     
                                                    <!--AQUI VA EL DIR PARTIDA-->                                 
                                                     <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" 
                                                TargetControlID="Panel_DirPartida" 
                                                CollapsedSize="0" 
                                                ExpandedSize="60" 
                                                Collapsed="true"
                                                ExpandControlID="ImageDirPartida"
                                                CollapseControlID="ImageDirPartida" 
                                                TextLabelID="LabelDirPartida" 
                                                ImageControlID="ImageDirPartida"
                                                CollapsedImage="~/Imagenes/Mas_B.JPG"
                                              ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                CollapsedText="Punto de Partida" 
                                                ExpandedText="Punto de Partida"                               
                                                ExpandDirection="Vertical" SuppressPostBack="True">
                                                </cc1:CollapsiblePanelExtender>                
                                            <asp:Image ID="ImageDirPartida" runat="server" />                                                             
                                            <asp:Label ID="LabelDirPartida" runat="server" Text="Label" CssClass="Label"></asp:Label>                                   
                                                        <asp:Panel ID="Panel_DirPartida" runat="server">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label CssClass="Label" ID="Label29" runat="server"  Text="Direcci�n:" 
                                                                            Enabled="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDirPartida" onfocus="return(aceptarFoco(this));"
                                                                            runat="server" Width="650px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right">
                                                                        <asp:Label CssClass="Label" ID="Label30" runat="server" Text="Depto.:" 
                                                                            Enabled="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cmbDeptoPartida" Enabled="true" runat="server" 
                                                                            AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                        <asp:Label CssClass="Label" ID="Label31" runat="server" Text="Provincia:" 
                                                                            Enabled="true"></asp:Label>
                                                                        <asp:DropDownList ID="cmbProvPartida" Enabled="true" runat="server" 
                                                                            AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                        <asp:Label CssClass="Label" ID="Label32"  runat="server" Text="Distrito:" 
                                                                            Enabled="true"></asp:Label>
                                                                        <asp:DropDownList ID="cmbDitritoPartida" Enabled="true" runat="server" AutoPostBack="false">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                          <!--DIR LLEGADA-->
                                                          <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" 
                                                TargetControlID="Panel_DirLlegada" 
                                                CollapsedSize="0" 
                                                ExpandedSize="60" 
                                                Collapsed="true"
                                                ExpandControlID="ImageDirLlegada"
                                                CollapseControlID="ImageDirLlegada" 
                                                TextLabelID="LabelDirLlegada" 
                                                ImageControlID="ImageDirLlegada"
                                                CollapsedImage="~/Imagenes/Mas_B.JPG"
                                              ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                CollapsedText="Punto de Llegada" 
                                                ExpandedText="Punto de Llegada"                               
                                                ExpandDirection="Vertical" SuppressPostBack="True">
                                                </cc1:CollapsiblePanelExtender>                                                                                  
                                                          
                                            <asp:Image ID="ImageDirLlegada" runat="server" />                                                             
                                            <asp:Label ID="LabelDirLlegada" runat="server" Text="Label" CssClass="Label"></asp:Label>                                   
                                                        <asp:Panel ID="Panel_DirLlegada" runat="server">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label CssClass="Label" ID="Label33" runat="server" Text="Direcci�n:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDirLlegada" onfocus="return(aceptarFoco(this));"  runat="server" Width="650px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right">
                                                                        <asp:Label ID="Label34" CssClass="Label" runat="server" Text="Depto.:"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cmbDeptoLlegada" runat="server" AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                        <asp:Label CssClass="Label" ID="Label35" runat="server" Text="Provincia:"></asp:Label>
                                                                        <asp:DropDownList ID="cmbProvLlegada" runat="server" AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                        <asp:Label ID="Label36" CssClass="Label" runat="server" Text="Distrito:"></asp:Label>
                                                                        <asp:DropDownList ID="cmbDistritoLlegada" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>                              
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="Label" align="right">
                                                   Motivo Traslado:</td>
                                                <td>
                                                  <asp:DropDownList ID="cmbMotivoTraslado" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>                                                     
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                     <asp:Label ID="Label2223" runat="server" CssClass="Label" Visible="true"
                                            Text="Operaci�n:"></asp:Label></td>
                                                <td>
                                                <asp:DropDownList ID="cmbTipoOperacion" runat="server" Visible="true" Width="100%"  >
                                        </asp:DropDownList>
                                                    </td>
                                            </tr>                                            
                                        </table>
                                    </td>
                                </tr>                                
                                <tr>
                                            <td class="LabelRojo" style="font-weight:bold">
                                                <asp:Label ID="lblAlertaComprometerStock" runat="server" Text="***** El Motivo de Traslado seleccionado compromete Stock en Almac�n."></asp:Label>
                                            </td>
                                            </tr>
                                
                                <tr>
                    <td class="TituloCelda">Remitente</td>
                </tr>               
                <tr>
                    <td style="width: 100%">
                        <asp:Panel ID="Panel_Remitente_1" runat="server">
                        <table width="100%">                            
                            <tr>
                                <td align="right" style="text-align: left">
    <!-- AQUI VA EL COLLAPSIBLE PANEL PARA DESC REMITENTE -->                                              
        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_Remitente" runat="server" 
                                                TargetControlID="PanelRemitente_Detalle" 
                                                CollapsedSize="0" 
                                                ExpandedSize="120" 
                                                Collapsed="true"
                                                ExpandControlID="Image_Remitente_1"
                                                CollapseControlID="Image_Remitente_1" 
                                                TextLabelID="lbl_Remitente1" 
                                                ImageControlID="Image_Remitente_1"
                                                CollapsedImage="~/Imagenes/Mas_B.JPG"
                                              ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                CollapsedText="Remitente" 
                                                ExpandedText="Remitente"                               
                                                ExpandDirection="Vertical" SuppressPostBack="True">
                                                </cc1:CollapsiblePanelExtender>       
                                                <asp:Image ID="Image_Remitente_1" runat="server" />                                                             
                                            <asp:Label ID="lbl_Remitente1" runat="server" Text="Label" CssClass="Label"></asp:Label>                     
                                            <asp:Panel ID="PanelRemitente_Detalle" runat="server">
                                    <table>
                                    <tr>
                                <td align="right">
                                    <asp:Label ID="Label3" runat="server" CssClass="Label" 
                                        Text="Ap. y Nombres/Raz�n Social:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNombre_Remitente" runat="server" Width="490px" ReadOnly="true" 
                                        CssClass="TextBoxReadOnly"></asp:TextBox>
                                    <asp:TextBox ID="txtIdRemitente" runat="server" CssClass="TextBoxReadOnly" 
                                        onkeypress="return(false);" Width="75px"></asp:TextBox>
                                    <asp:ImageButton ID="btnBuscarRemitente" runat="server" CausesValidation="false" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG" OnClientClick="return(mostrarCapaRemitente());"
                                        onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" Visible="true" />
                                </td>
                            </tr>                           
                            <tr>
                                <td align="right">
                                    <asp:Label ID="Label6" runat="server" Text="Almac�n:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="cmbAlmacen_Remitente" runat="server" AutoPostBack="false" Enabled="false" >
                                    </asp:DropDownList>
                                    <asp:Label ID="Label2229" runat="server" CssClass="Label" Text="Propietario:"></asp:Label>
                                    <asp:CheckBox ID="chbPropietario_Rem" Enabled="false" runat="server" />
                                </td>
                            </tr>
                                    <tr>
                                <td style="text-align: right;">
                                    <asp:Label ID="Label10" CssClass="Label" runat="server"  Text="D.N.I.:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDNI_Remitente" runat="server" CssClass="TextBoxReadOnly" 
                                        ReadOnly="True" Width="183px"></asp:TextBox>
                                    <asp:Label ID="Label11" CssClass="Label" runat="server"  Text="R.U.C.:"></asp:Label>
                                    <asp:TextBox ID="txtRUC_Remitente" runat="server" CssClass="TextBoxReadOnly" 
                                        ReadOnly="true" Width="202px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <asp:Label ID="Label12" CssClass="Label" runat="server"  Text="Direcci�n:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDireccion_Remitente" runat="server" ReadOnly="True" CssClass="TextBoxReadOnly" Width="676px" ></asp:TextBox>
                                </td>
                            </tr>
                                    </table>                                    
                                    </asp:Panel>                                                                        
                                </td>
                            </tr>
                        </table>
                        </asp:Panel>                        
                    </td>
                </tr>
                <tr>
                    <td class="TituloCelda">Destinatario</td>
                </tr>               
                <tr>
                    <td style="width: 100%">
                        <asp:Panel ID="Panel_Remitente" runat="server">
                        <table width="100%">                            
                            <tr>
                                <td align="right" style="text-align: left">
    <!-- AQUI VA EL COLLAPSIBLE PANEL PARA DESC REMITENTE -->                                              
        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" 
                                                TargetControlID="Panel_DescRemitente" 
                                                CollapsedSize="0" 
                                                ExpandedSize="120" 
                                                Collapsed="true"
                                                ExpandControlID="Image21_11"
                                                CollapseControlID="Image21_11" 
                                                TextLabelID="Label21_11" 
                                                ImageControlID="Image21_11"
                                                CollapsedImage="~/Imagenes/Mas_B.JPG"
                                              ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                CollapsedText="Destinatario" 
                                                ExpandedText="Destinatario"
                                                ExpandDirection="Vertical" SuppressPostBack="True">
                                                </cc1:CollapsiblePanelExtender>       
                                                <asp:Image ID="Image21_11" runat="server" />                                                             
                                            <asp:Label ID="Label21_11" runat="server" Text="Label" CssClass="Label"></asp:Label>                     
                                            <asp:Panel ID="Panel_DescRemitente" runat="server">
                                    <table>
                                    <tr>                                                                        
                                <td align="right">
                                    <asp:Label ID="Label7" runat="server" CssClass="Label" 
                                        Text="Ap. y Nombres/Raz�n Social:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRazonSocial" runat="server" Width="490px" ReadOnly="true" 
                                        CssClass="TextBoxReadOnly"></asp:TextBox>
                                    <asp:TextBox ID="txtIdPersona" runat="server" CssClass="TextBoxReadOnly" 
                                        onkeypress="return(false);" Width="75px"></asp:TextBox>
                                    <asp:ImageButton ID="btnBuscarP" runat="server" CausesValidation="false" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG" OnClientClick="return(mostrarCapaDestinatario());"
                                        onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                </td>
                            </tr>                           
                            <tr>
                                <td align="right">
                                    <asp:Label ID="Label2228" runat="server" Text="Almac�n:" CssClass="Label"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="cmbAlmacen_Destinatario" runat="server" 
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label2230" runat="server" CssClass="Label" Text="Propietario:"></asp:Label>
                                    <asp:CheckBox ID="chbPropietario_Dest" runat="server" Enabled="false" />
                                </td>
                            </tr>                                   
                                    
                                    <tr>
                                <td style="text-align: right;">
                                    <asp:Label ID="Label15" CssClass="Label" runat="server"  Text="D.N.I.:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDNI" runat="server" CssClass="TextBoxReadOnly" 
                                        ReadOnly="True" Width="183px"></asp:TextBox>
                                    <asp:Label ID="Label16" CssClass="Label" runat="server"  Text="R.U.C.:"></asp:Label>
                                    <asp:TextBox ID="txtRUC" runat="server" CssClass="TextBoxReadOnly" 
                                        ReadOnly="true" Width="202px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    <asp:Label ID="Label8" CssClass="Label" runat="server"  Text="Direcci�n:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDireccion" runat="server" ReadOnly="True" CssClass="TextBoxReadOnly" Width="676px" ></asp:TextBox>
                                </td>
                            </tr>
                                    </table>                                    
                                    </asp:Panel>                                                                        
                                </td>
                            </tr>
                        </table>
                        </asp:Panel>                        
                    </td>
                </tr>
                <tr class="TituloCelda">
                <td>Transportista
                </td>
                </tr>
                                <tr>
                                    <td>
                <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_Transportista" runat="server" 
                                                TargetControlID="Panel_Transportista" 
                                                CollapsedSize="0" 
                                                ExpandedSize="105" 
                                                Collapsed="true"
                                                ExpandControlID="ImageTransportista"
                                                CollapseControlID="ImageTransportista" 
                                                TextLabelID="LabelTransportista" 
                                                ImageControlID="ImageTransportista"
                                                CollapsedImage="~/Imagenes/Mas_B.JPG"
                                              ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                CollapsedText="Datos Transportista" 
                                                ExpandedText="Datos Transportista"                               
                                                ExpandDirection="Vertical" SuppressPostBack="True">
                                                </cc1:CollapsiblePanelExtender>  
                                                <asp:Image ID="ImageTransportista" runat="server" />                                                             
                                            <asp:Label ID="LabelTransportista" runat="server" Text="Label" CssClass="Label"></asp:Label>
                                                        <asp:Panel ID="Panel_Transportista" runat="server">
                                                        <table class="Label">
                                                        <tr>
                                                        <td>Raz�n Social:</td>
                                                        <td>
                                                            <asp:TextBox  CssClass="TextBoxReadOnly" Enabled="false" 
                                                                ID="txtRazonSocial_Trans" runat="server" Width="220px" MaxLength="100"></asp:TextBox>
                                                            <asp:TextBox ID="txtIdTransportista" Enabled="false" CssClass="TextBoxReadOnly" runat="server" Width="60px"></asp:TextBox>
                                                            </td>
                                                        <td style="text-align: right">R.U.C.:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtRUC_Trans" Enabled="false" CssClass="TextBoxReadOnly" 
                                                                runat="server" Width="100px" MaxLength="20"></asp:TextBox>
                                                            
                                                            </td>
                                                            
                                                        <td align="right" style="text-align: right">
                                                        <asp:ImageButton ID="btnBuscarEmpresa_Trans" runat="server" CausesValidation="false" 
                                                                ImageUrl="~/Imagenes/Buscar_b.JPG" OnClientClick="return(mostrarCapaTransportista());"
                                                                onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                                                onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />                                                                                                                        
                                                            </td>
                                                        <td style="text-align: left">
                                                            <asp:LinkButton ID="linkB_Transportista" runat="server" OnClientClick="return(addTransportista());">Nuevo Transportista</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                        <td style="text-align: right">Chofer:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtNomChofer_Trans" Enabled="false" CssClass="TextBoxReadOnly" 
                                                                runat="server" Width="220px" MaxLength="25"></asp:TextBox>
                                                            <asp:TextBox ID="txtIdChofer" Enabled="false" CssClass="TextBoxReadOnly" runat="server" Width="60px"></asp:TextBox>
                                                            </td>
                                                        <td>Nro. Licencia:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtNroLicencia_Trans" Enabled="false" 
                                                                CssClass="TextBoxReadOnly" runat="server" Width="100px" MaxLength="20"></asp:TextBox></td>
                                                        <td style="text-align: right">Categor�a:</td>
                                                        <td align="left">
                                                            <asp:TextBox ID="txtCategoria_Trans" Enabled="false" CssClass="TextBoxReadOnly" 
                                                                runat="server" Width="100px" MaxLength="20"></asp:TextBox>
                                                            <asp:ImageButton ID="btnBuscarChofer_Trans" runat="server" CausesValidation="false" 
                                                                ImageUrl="~/Imagenes/Buscar_b.JPG" OnClientClick="return(mostrarCapaChofer());"
                                                                onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                                                onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                            <asp:LinkButton ID="linkB_Chofer" runat="server" OnClientClick="return(addChofer());">Nuevo Chofer</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                        <td style="text-align: right">Modelo Veh.:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtMarcaVeh_Trans" Enabled="false" CssClass="TextBoxReadOnly" 
                                                                runat="server" Width="220px" MaxLength="20"></asp:TextBox>
                                                            <asp:TextBox ID="txtIdVehiculo" Enabled="false" CssClass="TextBoxReadOnly" runat="server" Width="60px"></asp:TextBox>
                                                            </td>
                                                        <td style="text-align: right">Nro. Placa:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtNroPlaca_Trans" Enabled="false" CssClass="TextBoxReadOnly" 
                                                                runat="server" Width="100px" MaxLength="20"></asp:TextBox></td>
                                                        <td>Cert. Inscripci�n:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtCertInscripcion_Trans" Enabled="false" 
                                                                CssClass="TextBoxReadOnly" runat="server" Width="100px" MaxLength="30"></asp:TextBox>
                                                            <asp:ImageButton ID="btnBuscarVeh_Trans" runat="server" CausesValidation="false" 
                                                                ImageUrl="~/Imagenes/Buscar_b.JPG" OnClientClick="return(mostrarCapaVehiculo());"
                                                                onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                                                onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />                                                            
                                                            </td>
                                                            </td>
                                                        </tr>                                                        
                                                        </table>                                                       
                         </asp:Panel></td>
                                </tr>
                <tr>
                    <td class="TituloCeldaLeft">
                        <asp:ImageButton ID="btnAgregarProducto" runat="server" 
                            ImageUrl="~/Imagenes/BuscarProducto_b.JPG" 
                            OnClientClick="return(mostrarCapaBuscarProd());" 
                            onmouseout="this.src='/Imagenes/BuscarProducto_b.JPG';" 
                            onmouseover="this.src='/Imagenes/BuscarProducto_A.JPG';" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%">
                        <asp:GridView ID="DGVDetalle" runat="server" AutoGenerateColumns="False" 
                            Width="100%">
                            <HeaderStyle CssClass="GrillaHeader" />
                            <Columns>
                                <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                <asp:BoundField DataField="IdProducto" HeaderText="Id" />
                                <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" />
                                <asp:TemplateField HeaderText="U. Medida">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="cmbUMedida" runat="server" DataTextField="NombreCortoUM" 
                                            DataValueField="IdUnidadMedida">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cantidad">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtCantidad" runat="server" onblur="return(valBlur(event));" TabIndex="500" 
                                            onKeypress="return(validarNumeroPunto(event));" onfocus="return(aceptarFoco(this));"
                                            Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                        
                        <!-- Grilla Relacionado -->
                        
                         
                    </td>
                </tr>
                <tr style="background-color: gray">
                    <td align="center" style="width:100%" class="TituloCelda">
                        Observaciones</td>
                </tr>
                <tr>
                    <td style="width:100%; height: 26px">
                        <asp:TextBox Width="100%"  onfocus="return(aceptarFoco(this));" ID="txtObservaciones" runat="server" Height="63px"  MaxLength="500"
                            TextMode="MultiLine"></asp:TextBox></td>
                </tr>
                            </table>                        
                        </asp:Panel>
                    </td>
                </tr>           
                <tr>
                                    <td>
                                        <asp:HiddenField ID="hddAccion" runat="server"  />
                                        <asp:HiddenField ID="hddIdDocumento" runat="server"  Value="0" />
                                        <asp:HiddenField ID="hddModoFrm" runat="server" Value="0" />
                                        <asp:HiddenField ID="hddPoseeOrdenDespacho" runat="server" Value="0" />
                                        <asp:HiddenField ID="hddCantidadFilas" runat="server" Value="0" />
                                        <asp:HiddenField ID="hddIdDocumentoRef" runat="server" />
                                    </td>
                                </tr>     
                            </table>
   </ContentTemplate>    </asp:UpdatePanel>
          
                
<div  id="capa_BuscarProducto_Relacionado"         
        style="border: 3px solid blue; padding: 8px; width:840px; height:auto; position:absolute; top:292px; left:23px; background-color:white; z-index:2; display :none; ">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>

                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="ImageButton8" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capa_BuscarProducto_Relacionado'));" 
                                        />                                
                                </td>
                                </tr>
                                <tr>
                                <td>
                                <asp:ImageButton ID="btnAddProd_DetalleRel" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG" 
                                    onmouseout="this.src='/Imagenes/Agregar_B.JPG';" onmouseover="this.src='/Imagenes/Agregar_A.JPG';"
                                    OnClientClick="return(valAddProd_DetalleRel());" /></td>
                                </tr>
                                <tr>
                                <td>
                                <asp:GridView ID="DGV_DetalleRel_AddProd" runat="server" AutoGenerateColumns="False"
                                Width="100%">                                
                                <Columns>                                                                  
                                <asp:TemplateField HeaderText="Adicionar">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chb_AddProducto_DocRel" runat="server" />
                                </ItemTemplate>                                
                                </asp:TemplateField>
                                <asp:BoundField DataField="IdProducto" HeaderText="Id" NullDisplayText="0" />
                                <asp:BoundField DataField="NomProducto" HeaderText="Descripci�n" NullDisplayText="---" />
                                <asp:BoundField DataField="UMedida" HeaderText="U.M." NullDisplayText="---" />
                                <asp:BoundField DataField="Cantidad"  DataFormatString="{0:F2}" HeaderText="Cantidad Total" NullDisplayText="0" />
                                <asp:BoundField DataField="CantxAtender" DataFormatString="{0:F2}" HeaderText="Cant. Pendiente" NullDisplayText="0" />                                  
                                <asp:BoundField DataField="IdDetalleDocumento" HeaderText="" NullDisplayText="0" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                         </asp:GridView>
                                
                                    </td>
                                </tr>
                                                                    </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                        
<div  id="capaBuscarP" style="border: 3px solid blue; padding: 8px; width:840px; height:auto; position:absolute; top:203px; left:32px; background-color:white; z-index:2; display :none;">
                        <asp:UpdatePanel ID="UpdatePanelBuscarP" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>

                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="ImageButton1" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaBuscarP'));" 
                                        />                                
                                </td>
                                </tr>
                                <tr>
                                <td>
                                    <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                                    <asp:DropDownList ID="cmbLinea" runat="server" AutoPostBack="True" DataTextField="Descripcion" DataValueField="Id">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label18" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                    <asp:DropDownList ID="cmbSubLinea" runat="server" DataTextField="Nombre" DataValueField="Id" >
                                    </asp:DropDownList>                                    
                                    
                                    <asp:TextBox ID="txtCodSubLinea_BuscarProd" Width="75px" onfocus="return(aceptarFoco(this));" onKeyPress="return(onKeyPressEsNumero('event'));"  runat="server"></asp:TextBox>
                                    
                                    </td>
                                </tr>
                                <tr>
                                <td>
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                                    <asp:TextBox onfocus="return(aceptarFoco(this));" ID="txtDescripcionProd" runat="server" Width="300px"></asp:TextBox>
                                    <asp:ImageButton ID="btnBuscarGrilla" runat="server" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG" 
                                        onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                        
                                        
                        <asp:ImageButton ID="btnAddProductos" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG" 
                            onmouseout="this.src='/Imagenes/Agregar_B.JPG';" onmouseover="this.src='/Imagenes/Agregar_A.JPG';"
                            OnClientClick="return(valAddProductos());" />
                                             
                                </td>
                                </tr>
                                <tr>
                                <td>
                                    <asp:GridView ID="DGVSelProd" runat="server" AllowPaging="false" 
                                        AutoGenerateColumns="False" Width="100%" PageSize="20">
                                        <Columns>
                                            <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chbSelectProducto" runat="server" />
                                            </ItemTemplate>                                            
                                            </asp:TemplateField>                                            
                                            <asp:BoundField DataField="IdProducto" HeaderText="C�digo" NullDisplayText="0" />                                            
                                            <asp:BoundField DataField="NomProducto" HeaderText="Descripci�n" NullDisplayText="---" />
                                            <asp:BoundField DataField="NomUMedida" HeaderText="U.M. Principal" NullDisplayText="---" />
                                            <asp:BoundField DataField="NomLinea" HeaderText="L�nea" NullDisplayText="---" />
                                            <asp:BoundField DataField="NomSubLinea" HeaderText="Sub L�nea" 
                                                NullDisplayText="---" />                                            
                                            <asp:BoundField DataField="IdUnidadMedida" HeaderText="" NullDisplayText="0" />                                                
                                        </Columns>
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    </asp:GridView></td>
                                </tr>
                                  <tr>
                                    
                                    <td>
                                    
                                        <asp:Button ID="btnAnterior_Productos" runat="server" Font-Bold="true" Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                                        <asp:Button ID="btnPosterior_Productos" runat="server" Font-Bold="true" Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                                        <asp:TextBox ID="txtPageIndex_Productos" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                                        <asp:TextBox ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                    
                                    </td>
                                    </tr>  
                            </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>   
                         
<div  id="capaBuscarDocumento" style="border: 3px solid blue; padding: 8px; width:840px; height:auto; position:absolute; top:280px; left:38px; background-color:white; z-index:2; display :none; ">                        
                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="ImageButton3" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaBuscarDocumento'));" 
                                        />                                
                                </td>
                                </tr>
                                <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="N� Serie:" CssClass="Label"></asp:Label>
                                    <asp:TextBox ID="txtSerieBuscarDoc" onKeypress="return(onKeyPressEsNumero('event'));"  Width="80px" Font-Bold="true" runat="server"></asp:TextBox>
                                    <asp:Label ID="Label2226" runat="server" Text="N� C�digo:" CssClass="Label"></asp:Label>
                                    <asp:TextBox ID="txtCodigoBuscarDoc" onKeypress="return(onKeyPressEsNumero('event'));" Width="90px" Font-Bold="true" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnBuscarDocumento" runat="server" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG"  OnClientClick="return(valBuscarDocumento());"
                                        onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                    </td>
                                </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="DGV_BuscarDoc" runat="server" AutoGenerateColumns="false" Width="100%"  >
                                            <Columns>
                                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="true" ButtonType="Link" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo Documento" NullDisplayText="---" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderText="Nro. Documento"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >
                                            <ItemTemplate>
                                            <table>
                                            <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdDocumento_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNroDocumento_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Numero")%>'   ></asp:Label></td>
                                            </tr>
                                            </table>
                                            </ItemTemplate>
                                            </asp:TemplateField>                                                                                                                                                                                
                                            <asp:BoundField DataField="FechaEmision" HeaderText="F. Emisi�n" DataFormatString="{0:dd/MM/yyyy}" NullDisplayText="---" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="NomPropietario" HeaderText="Empresa" NullDisplayText="---" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="NomTienda" HeaderText="Tienda" NullDisplayText="---" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />                                            
                                            </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>                                 
                                </table>                            
</div>                           


<div  id="capaTransportista" 
        
        style="border: 3px solid blue; padding: 8px; width:560px; height:auto; position:absolute; top:285px; left:71px; background-color:white; z-index:2; display :none; ">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="ImageButton2" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaTransportista'));" 
                                        />                                
                                </td>
                                </tr>
                                <tr class="Label">
                                <td align="left">                                    
                                    Raz�n Social:
                                    <asp:TextBox ID="txtTextoBusquedaTransportista" runat="server" Width="250px"></asp:TextBox>
                                 <asp:ImageButton ID="btnBuscarTransportista" runat="server" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG"
                                        onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                    </td>
                                </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:GridView ID="DGV_BuscarTransportista" runat="server" AutoGenerateColumns="false" Width="100%" AllowPaging="true" >
                                            <Columns>
                                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="true" ButtonType="Link" />
                                            <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                                            <asp:BoundField DataField="RazonSocial" HeaderText="Raz�n Social" NullDisplayText="---" />                                            
                                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." NullDisplayText="---" />                                            
                                            </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>                                 
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>                          
<div  id="capaChofer" 
        
        style="border: 3px solid blue; padding: 8px; width:560px; height:auto; position:absolute; top:276px; left:74px; background-color:white; z-index:2; display :none; ">
                        <asp:UpdatePanel ID="UpdatePanel_Chofer" runat="server">
                            <ContentTemplate>
                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="ImageButton4" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaChofer'));" 
                                        />                                
                                </td>
                                </tr>
                                <tr class="Label">
                                <td align="left">                                    
                                    Ap. y Nombres:
                                    <asp:TextBox ID="txtTextoBusqChofer" runat="server" Width="250px"></asp:TextBox>
                                 <asp:ImageButton ID="btnBuscarChofer" runat="server" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG" 
                                        onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                    </td>
                                </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:GridView ID="DGV_Chofer" runat="server" AutoGenerateColumns="false" Width="100%" AllowPaging="true" >
                                            <Columns>
                                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="true" ButtonType="Link" />
                                            <asp:BoundField DataField="Id" HeaderText="Id" NullDisplayText="0" />
                                            <asp:BoundField DataField="Nombre" HeaderText="Nombre" NullDisplayText="---" />
                                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." NullDisplayText="---" />
                                            <asp:BoundField DataField="Licencia" HeaderText="Nro. Licencia" NullDisplayText="---" />
                                            <asp:BoundField DataField="Categoria" HeaderText="Categor�a" NullDisplayText="---" />
                                            </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>                                 
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>    
<div  id="capaVehiculo" 
        
        style="border: 3px solid blue; padding: 8px; width:560px; height:auto; position:absolute; top:282px; left:79px; background-color:white; z-index:2; display :none; ">
                        <asp:UpdatePanel ID="UpdatePanel_Vehiculo" runat="server">
                            <ContentTemplate>
                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="ImageButton5" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaVehiculo'));" 
                                        />                                
                                </td>
                                </tr>
                                <tr class="Label">
                                <td align="left">                                    
                                    Nro. Placa:
                                    <asp:TextBox ID="txtBuscarVehiculo" runat="server" Width="250px"></asp:TextBox>
                                 <asp:ImageButton ID="btnBuscarVehiculoGrilla" runat="server" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG" 
                                        onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                    </td>
                                </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="DGV_Vehiculo" runat="server" AutoGenerateColumns="false" Width="100%" AllowPaging="true" >
                                            <Columns>
                                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="true" ButtonType="Link" />                                            
                                            <asp:BoundField DataField="IdProducto" HeaderText="Id" NullDisplayText="0" />
                                            <asp:BoundField DataField="Placa" HeaderText="Nro. Placa" NullDisplayText="---" />
                                            <asp:BoundField DataField="Modelo" HeaderText="Modelo" NullDisplayText="---" />
                                            <asp:BoundField DataField="ConstanciaIns" HeaderText="Const. Inscripci�n" NullDisplayText="---" />
                                            </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>                                 
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div> 
<div  id="capaRemitente" style="border: 3px solid blue; padding: 8px; width:auto; height:auto; position:absolute; top:282px; left:30px; background-color:white; z-index:2; display :none; ">
                        <asp:UpdatePanel ID="UpdatePanel_BuscarPersona" runat="server">
                            <ContentTemplate>
                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="ImageButton6" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaRemitente'));" />                                
                                </td>
                                </tr>
                                <tr class="Label">
                                <td align="left"> Descripci�n:
                                    <asp:TextBox ID="txtBuscarPersonaGrilla" runat="server" Width="250px"></asp:TextBox>
                                 <asp:ImageButton ID="btnBuscarPersonaGrilla" runat="server" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG" 
                                        onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                    </td>
                                </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:GridView ID="DGV_BuscarPersona" runat="server" AutoGenerateColumns="false" Width="100%" AllowPaging="true" >
                                            <Columns>
                                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="true" ButtonType="Link" />
                                            <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                                            <asp:BoundField DataField="getNombreParaMostrar" HeaderText="Descripci�n" NullDisplayText="---" />
                                            <asp:BoundField DataField="Dni" HeaderText="D.N.I." NullDisplayText="---" />
                                            <asp:BoundField DataField="Ruc" HeaderText="R.U.C." NullDisplayText="---" />
                                            <asp:BoundField DataField="Direccion" HeaderText="Direcci�n" NullDisplayText="---" />
                                            <asp:BoundField DataField="Ubigeo" HeaderText="Ubigeo" NullDisplayText="000000" />
                                            <asp:TemplateField HeaderText="Propietario">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chbPropietario_R"  Enabled="false" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Propietario")%>' />
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                            </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>                                 
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>    
                        
<div  id="capaDestinatario" style="border: 3px solid blue; padding: 8px; width:auto; height:auto; position:absolute; top:282px; left:30px; background-color:white; z-index:2; display :none; ">
                        <asp:UpdatePanel ID="UpdatePanel_Destinatario11" runat="server">
                            <ContentTemplate>
                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="ImageButton7" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='/Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='/Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaDestinatario'));" />                                
                                </td>
                                </tr>
                                <tr class="Label">
                                <td align="left"> Descripci�n:
                                    <asp:TextBox ID="txtBuscarDestinatarioGrilla" runat="server" Width="250px"></asp:TextBox>
                                 <asp:ImageButton ID="btnBuscarDestinatarioGrilla" runat="server" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG" 
                                        onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                    </td>
                                </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:GridView ID="DGV_BuscarDestinatario" runat="server" AutoGenerateColumns="false" Width="100%" AllowPaging="true" >
                                            <Columns>
                                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="true" ButtonType="Link" />
                                            <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                                            <asp:BoundField DataField="getNombreParaMostrar" HeaderText="Descripci�n" NullDisplayText="---" />
                                            <asp:BoundField DataField="Dni" HeaderText="D.N.I." NullDisplayText="---" />
                                            <asp:BoundField DataField="Ruc" HeaderText="R.U.C." NullDisplayText="---" />
                                            <asp:BoundField DataField="Direccion" HeaderText="Direcci�n" NullDisplayText="---" />
                                            <asp:BoundField DataField="Ubigeo" HeaderText="Ubigeo" NullDisplayText="000000" />
                                            <asp:TemplateField HeaderText="Propietario">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chbPropietario_D"  Enabled="false" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem,"Propietario")%>' />
                                            </ItemTemplate>
                                            </asp:TemplateField>                                            
                                            </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>                                 
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>                          
<script type="text/javascript" language="javascript">
    function valAddProductos() {
        var grilla = document.getElementById('<%=DGVSelProd.ClientID%>');
       
        var cont = 0;
        if (grilla == null) {
            alert('No se seleccionaron productos.');
            return false;
        }
        for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].status == true) {
                cont = cont + 1;
            }
        }


        if (cont == 0) {
            alert('No se seleccionaron productos.');
            return false;
        }
        return true;
    }
    function valGrillaBlur() {
        var id = event.srcElement.id;
        var caja = document.getElementById(id);
        if (CajaEnBlanco(caja)) {
            caja.value=0;
            return false;
        }
        if (!esDecimal(caja.value)) {
            caja.select();
            caja.focus();
            alert('Valor no v�lido.');
            return false;
        }
    }
    
    function addVehiculo() {
        var caja = document.getElementById('<%= txtNroPlaca_Trans.ClientID%>');
        caja.value = '';
        caja.disabled = false;
        caja.style.backgroundColor = 'Yellow';

        document.getElementById('<%=txtIdVehiculo.ClientID%>').value = '';

        caja = document.getElementById('<%= txtCertInscripcion_Trans.ClientID%>');
        caja.value = '';
        caja.style.backgroundColor = 'Yellow';
        caja.disabled = false;

        caja = document.getElementById('<%= txtMarcaVeh_Trans.ClientID%>');
        caja.value = '';
        caja.style.backgroundColor = 'Yellow';
        caja.disabled = false;
        caja.select();
        caja.focus();
        return false;
    }

    function addChofer() {
        var caja = document.getElementById('<%= txtNroLicencia_Trans.ClientID%>');
        caja.value = '';
        caja.disabled = false;
        caja.style.backgroundColor = 'Yellow';

        document.getElementById('<%=txtIdChofer.ClientID%>').value = '';

        caja = document.getElementById('<%= txtCategoria_Trans.ClientID%>');
        caja.value = '';
        caja.style.backgroundColor = 'Yellow';
        caja.disabled = false;

        caja = document.getElementById('<%= txtNomChofer_Trans.ClientID%>');
        caja.value = '';
        caja.style.backgroundColor = 'Yellow';
        caja.disabled = false;
        caja.select();
        caja.focus();
        return false;
    }
    
    function addTransportista(){
        var caja = document.getElementById('<%= txtRUC_Trans.ClientID%>');
        caja.value = '';
        caja.disabled = false;
        caja.style.backgroundColor = 'Yellow';

        document.getElementById('<%=txtIdTransportista.ClientID%>').value = '';
        
        caja = document.getElementById('<%= txtRazonSocial_Trans.ClientID%>');
        caja.value = '';
        caja.style.backgroundColor = 'Yellow';        
        caja.disabled = false;
        caja.select();
        caja.focus();
        return false;
    }


    function emitirOrdenDespacho() {
        var hdd = document.getElementById('<%= hddIdDocumento.ClientID%>');
        if (hdd.value.length == 0 || hdd.value == '' || parseFloat(hdd.value) == 0) {
            alert('No se ha registrado ning�n documento al cual emitir su Orden de Despacho.');
            return false;
        }
        //if (confirm('Desea mandar la impresi�n de la Gu�a de Remisi�n?')) {
        window.open('FrmOrdenDespacho_Load.aspx?IdDocumento=' + hdd.value,null, 'resizable=yes,width=1000,height=800,scrollbars=1', null);
        //}
        return false;
    }

    function mostrarCapaProducto() {
        var hddAccion = document.getElementById('<%=hddAccion.ClientID %>');
        switch(hddAccion.value){
            case '0': //************************* Doc. NO Relacionado
                onCapa('capaBuscarP');
                document.getElementById('<%=txtDescripcionProd.ClientID %>').select();
                document.getElementById('<%=txtDescripcionProd.ClientID %>').focus();    
                break;
            case '1': //************************* DOC. RELACIONADO
                onCapa('capa_BuscarProducto_Relacionado');   
                break;
        }
        return false;
    }
    
    function mostrarCapaTransportista() {
        onCapa('capaTransportista');
        document.getElementById('<%=txtTextoBusquedaTransportista.ClientID %>').select();
        document.getElementById('<%=txtTextoBusquedaTransportista.ClientID %>').focus();
        return false;
    }
    function mostrarCapaChofer() {
        onCapa('capaChofer');
        document.getElementById('<%=txtTextoBusqChofer.ClientID %>').select();
        document.getElementById('<%=txtTextoBusqChofer.ClientID %>').focus();        
        return false;
    }
    function mostrarCapaVehiculo() {
        onCapa('capaVehiculo');
        document.getElementById('<%=txtBuscarVehiculo.ClientID %>').select();
        document.getElementById('<%=txtBuscarVehiculo.ClientID %>').focus();
        return false;
    }
    function valSave() {
        //******************* validamos que las cajas esten llenas
        var caja;    
            
        caja = document.getElementById('<%=txtIdPersona.ClientID%>');
        if (CajaEnBlanco(caja)) {
            alert('Debe seleccionar un Destinatario.');
            return false;
        }
        caja = document.getElementById('<%=txtIdRemitente.ClientID%>');
        if (CajaEnBlanco(caja)) {
            alert('Debe seleccionar un Remitente.');
            return false;
        }
        //********************** validamos los combos
        var cbo;
        cbo = document.getElementById('<%=cmbEmpresa.ClientID%>');
        if (cbo.value == '' || parseFloat(cbo.value) == 0) {
            alert('Debe seleccionar una empresa.');
            return false;
        }
        cbo = document.getElementById('<%=cmbTienda.ClientID%>');
        if (cbo.value == '' || parseFloat(cbo.value) == 0) {
            alert('Debe seleccionar una tienda.');
            return false;
        }
        cbo = document.getElementById('<%=cmbAlmacen.ClientID%>');
        if (cbo.value == '' || parseFloat(cbo.value) == 0) {
            alert('Debe seleccionar un Almac�n.');
            return false;
        }        
        cbo = document.getElementById('<%=cmbEstadoDocumento.ClientID%>');
        if (cbo.value == '' || parseFloat(cbo.value) == 0) {
            alert('Debe seleccionar un Estado del Documento.');
            return false;
        }        
        cbo = document.getElementById('<%=cmbEstadoEntrega.ClientID%>');
        if (cbo.value == '' || parseFloat(cbo.value) == 0) {
            alert('Debe seleccionar un Estado de Entrega.');
            return false;
        }
        cbo = document.getElementById('<%=cboSerie.ClientID%>');
        if (cbo.value == '' || parseFloat(cbo.value) == 0) {
            alert('Debe seleccionar un N� de Serie para el documento. En caso de no existir debe registrar uno.');
            return false;
        }
        cbo = document.getElementById('<%=cmbMotivoTraslado.ClientID%>');
        if (cbo.value == '' || parseFloat(cbo.value) == 0) {
            alert('Debe seleccionar un Motivo de Traslado.');
            return false;
        }

        cbo = document.getElementById('<%=cmbTipoOperacion.ClientID%>');
        if (cbo.value == '' || parseFloat(cbo.value) == 0) {
            alert('Debe seleccionar un Tipo de Operaci�n.');
            return false;
        }
        //*********************** validamos que la grilla tenga registros

        var hddAccion = document.getElementById('<%=hddAccion.ClientID %>');

        switch (hddAccion.value) {
            case '0':
                var grilla = document.getElementById('<%=DGVDetalle.ClientID %>');
                if (grilla == null) {
                    alert('No ha ingresado productos.');
                    return false;
                }
                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];
                    var cellCantidad = rowElem.cells[4];
                    if ((!esDecimal(cellCantidad.children[0].value)) || (parseFloat(cellCantidad.children[0].value) == 0)) {
                        alert('Cantidad no v�lida.');
                        cellCantidad.children[0].select();
                        cellCantidad.children[0].focus();
                        return false;
                    }
                    if (cellCantidad.children[0].value.length == 0) {
                        alert('Ingrese una cantidad.');
                        cellCantidad.children[0].select();
                        cellCantidad.children[0].focus();
                        return false;
                    }
                }
                break;            
        }

        var IdMotivoTraslado = document.getElementById('<%=cmbMotivoTraslado.ClientID%>').value;
        
        //************* validar el IdAlmacen Remitente y IdAlmacen Destinatario
        var IdAlmacen_Rem = document.getElementById('<%=cmbAlmacen_Remitente.ClientID%>').value;
        var IdAlmacen_Dest = document.getElementById('<%=cmbAlmacen_Destinatario.ClientID%>').value;
        var chPropietario_D = document.getElementById('<%=chbPropietario_Dest.ClientID%>').status;        
        if (parseFloat(IdAlmacen_Rem) == parseFloat(IdAlmacen_Dest) && IdMotivoTraslado == '8') {
            alert('El Almac�n Origen no puede ser el mismo al Almac�n Destino.');
            return false;
        }
        if (chPropietario_D && (IdAlmacen_Dest == 0 || isNaN(IdAlmacen_Dest))) {
            alert('Debe seleccionar un Almac�n destino.');
            return false;
        }
        

        //***************** Valido el Motivo Traslado 'Traslado entre est. de la misma empresa'        
        var IdRemitente = document.getElementById('<%=txtIdRemitente.ClientID%>').value;
        var IdDestinatario = document.getElementById('<%=txtIdPersona.ClientID%>').value;        
        
        if (IdMotivoTraslado == '8') {
            if (IdRemitente != IdDestinatario) {
                alert('El Motivo de Traslado indica que el Remitente y Destinatario deben ser la misma empresa.');
                return false;                
            }
        }
        if (IdMotivoTraslado == '8' && IdAlmacen_Dest==0) {
            
                alert('Debe seleccionar un Almac�n de Destino.');
                return false;
            
        }



        var nrofilas = document.getElementById('<%=hddCantidadFilas.ClientID%>');
        if (grilla.rows.length - 1 > nrofilas.value) {
            alert('Este Documento debe tener como maximo ' + nrofilas.value + ' items en su detalle.');
            return false;
        }
        return (confirm('Desea continuar con la operaci�n?'));
    }
                                
                                
                                
                                
                   
                                
                                function valBuscarDocumento() {
                                var cajaSerie = document.getElementById('<%= txtSerieBuscarDoc.ClientID%>');
                                var cajaCodigo = document.getElementById('<%= txtCodigoBuscarDoc.ClientID%>');
                                if (CajaEnBlanco(cajaSerie) == true) {
                                    alert('Debe ingresar un N� Serie');
                                    cajaSerie.focus();
                                    return false;
                                }
                                if (CajaEnBlanco(cajaCodigo) == true) {
                                    alert('Debe ingresar un N� C�digo');
                                    cajaCodigo.focus();
                                    return false;
                                }
                                return true;
                            }
                            function validarNumeroPunto(elEvento) {
                                //obtener caracter pulsado en todos los exploradores
                                var evento = elEvento || window.event;
                                var caracter = evento.charCode || evento.keyCode;
                                //alert("El car�cter pulsado es: " + caracter);
                                if (caracter == 46) {
                                    return true;
                                } else {
                                    if (!onKeyPressEsNumero('event')) {
                                        alert('Caracter no v�lido.');
                                        return false;
                                    }
                                }
                            }
                            function valBlur(event) {
                                var id = event.srcElement.id;
                                var caja = document.getElementById(id);
                                if (CajaEnBlanco(caja)) {
                                    caja.value = 0;
                                    //alert('Debe ingresar un valor.');
                                }
                                if (!esDecimal(caja.value)) {
                                    caja.select();
                                    caja.focus();
                                    alert('Valor no v�lido.');
                                }
                            }

                            function valImprimir() {
                                var hdd = document.getElementById('<%= hddIdDocumento.ClientID%>');
                                if (hdd.value.length == 0 || hdd.value == '' || parseFloat(hdd.value) == 0) {
                                    alert('No se ha registrado ninguna Gu�a de Remisi�n para enviar a impresi�n.');
                                    return false;
                                }
                                if (confirm('Desea mandar la impresi�n de la Gu�a de Remisi�n?')) {
                                   // mostrarVentana('../../Reportes/visor.aspx?iReporte=10&IdDocumento=' + hdd.value, 'Orden de Despacho', 500, 500);
                                    window.parent.open('../../Reportes/visor.aspx?iReporte=10&IdDocumento=' + hdd.value, 'Reporte', 'resizable=yes', null);
                                }
                                return false;
                            }

                            function mostrarCapaRemitente() {
                                var hddAccion = document.getElementById('<%=hddAccion.ClientID %>');
                                if (hddAccion.value == '1') {
                                    alert('Acceso Denegado. El Documento se encuentra relacionado.');
                                    return false;
                                }
                            
                            
                                onCapa('capaRemitente');
                                document.getElementById('<%=txtBuscarPersonaGrilla.ClientID%>').focus();
                                document.getElementById('<%=txtBuscarPersonaGrilla.ClientID%>').select();
                                return false;
                            }
                            function mostrarCapaDestinatario() {
                                var hddAccion = document.getElementById('<%=hddAccion.ClientID %>');
                                if (hddAccion.value == '1') {
                                    alert('Acceso Denegado. El Documento se encuentra relacionado.');
                                    return false;
                                }                                
                            
                                onCapa('capaDestinatario');
                                document.getElementById('<%=txtBuscarDestinatarioGrilla.ClientID%>').focus();
                                document.getElementById('<%=txtBuscarDestinatarioGrilla.ClientID%>').select();
                                return false;
                            }

                            function valNavegacionProductos(tipoMov) {
                                var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
                                if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                                    alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                                    document.getElementById('<%=txtDescripcionProd.ClientID%>').select();
                                    document.getElementById('<%=txtDescripcionProd.ClientID%>').focus();
                                    return false;
                                }
                                switch (tipoMov) {
                                    case '0':   //************ anterior
                                        if (index <= 1) {
                                            alert('No existen p�ginas con �ndice menor a uno.');
                                            return false;
                                        }
                                        break;
                                    case '1':
                                        break;
                                    case '2': //************ ir
                                        index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                                        if (isNaN(index) || index == null || index.length == 0) {
                                            alert('Ingrese una P�gina de navegaci�n.');
                                            document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                                            document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                                            return false;
                                        }
                                        if (index < 1) {
                                            alert('No existen p�ginas con �ndice menor a uno.');
                                            document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                                            document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                                            return false;
                                        }
                                        break;
                                }
                                return true;
                            }

                            function mostrarCapaBuscarProd() {
                                onCapa('capaBuscarP');
                                document.getElementById('<%=txtCodSubLinea_BuscarProd.ClientID %>').select();
                                document.getElementById('<%=txtCodSubLinea_BuscarProd.ClientID %>').focus();
                                return false;
                            }


                            function validarBuscarDocumento() {
                                var caja = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
                                if (CajaEnBlanco(caja)) {
                                    alert('Ingrese un c�digo de b�squeda.');
                                    caja.focus();
                                    return false;
                                }
                                var IdSerie = parseInt(document.getElementById('<%=cboSerie.ClientID %>').value);
                                if (isNaN(IdSerie)) {
                                    alert('Seleccione un N�mero de Serie de Documento.');
                                    return false;
                                }

                                return true;
                            }
                            function validarEdicion() {

                                var IdEstadoDoc = document.getElementById('<%=cmbEstadoDocumento.ClientID%>').value;
                                if (parseInt(IdEstadoDoc) == 2) {
                                    alert('La Gu�a de Remisi�n no puede editarse porque ha sido anulada.');
                                    return false;
                                }
                            
                                var hdd = document.getElementById('<%=hddPoseeOrdenDespacho.ClientID%>');
                                if (parseInt(hdd.value) == 1) {
                                    alert('La Gu�a de Remisi�n no puede editarse porque posee �rdenes de Despacho asociados. Anule las �rdenes de Despacho asociados.');
                                    return false;
                                }                                
                                return true;
                            }
                            function valAnular() {

                                var IdEstadoDoc = document.getElementById('<%=cmbEstadoDocumento.ClientID%>').value;
                                if (parseInt(IdEstadoDoc) == 2) {
                                    alert('La Gu�a de Remisi�n ya ha sido anulada.');
                                    return false;
                                }
                            
                                var hdd = document.getElementById('<%=hddPoseeOrdenDespacho.ClientID%>');
                                if (parseInt(hdd.value) == 1) {
                                    alert('La Gu�a de Remisi�n posee �rdenes de Despacho asociados.');
                                    return (confirm('Si anula la Gu�a de Remisi�n autom�ticamente se anular�n las �rdenes de Despacho asociados. Desea anular de toda formas la Gu�a de Remisi�n?'));
                                }
                                return (confirm('Desea anular la Gu�a de Remisi�n?'));
                            }
</script>

</asp:Content>


