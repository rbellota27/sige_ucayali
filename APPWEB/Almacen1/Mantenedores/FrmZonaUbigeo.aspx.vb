﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmZonaUbigeo
    Inherits System.Web.UI.Page
    Private objDSBZonaUbigeo As Object
    Private objScript As New ScriptManagerClass

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
            ConfigurarDatos()
        End If
    End Sub

    Private Sub inicializarFrm()
        Try
            Me.objDSBZonaUbigeo = New Object
            Session.Add("objDSBZonaUbigeo", objDSBZonaUbigeo)
            cargarDatos()
            verFrmInicio()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub

    Private Sub cargarDatos()
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboMoneda(Me.cboMoneda, False)
            cargarDatosPerfil(Me.cboMoneda)
        Catch ex As Exception
            Dim objScrip As New ScriptManagerClass
            objScrip.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosPerfil(ByVal cbo As DropDownList)
        Dim objCombo As New Combo
        objCombo.LlenarCboMoneda(cbo, False)
    End Sub

    Private Sub verFrmInicio()
        hddModo.Value = "I"
        hddLogin.Value = ""
        Panel_Perfil.Visible = False
        Panel_Busqueda.Enabled = True
        mostrarBotonesControl()
        buscarxEstado()
        limpiarFrm()
    End Sub

    Private Sub mostrarBotonesControl()
        If hddModo.Value = "I" Then
            btnNuevo.Visible = True
            btnGuardar.Visible = False
            btnCancelar.Visible = False
        Else
            btnNuevo.Visible = False
            btnGuardar.Visible = True
            btnCancelar.Visible = True
        End If
    End Sub

    Private Sub buscarxEstado()
        Dim obj As New ScriptManagerClass
        Try
            Dim objUV As New Negocio.ZonaUbigeo
            Dim objpais As New Entidades.ZonaUbigeo

            DGVZonaUbigeo.DataSource = objUV.SelectxEstadoxNombre(rdbEstadoBusqueda.SelectedValue)
            DGVZonaUbigeo.DataBind()
            Me.setDSBZonaUbigeo(DGVZonaUbigeo.DataSource)
            If DGVZonaUbigeo.Rows.Count = 0 Then
                obj.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If
        Catch ex As Exception
            obj.mostrarMsjAlerta(Me, "Problemas en la búsqueda.")
        End Try
    End Sub

    Private Sub limpiarFrm()
        TxtNombre.Text = ""
        TxtDescripcion.Text = ""
        TxtValor.Text = ""
    End Sub

    Private Sub setDSBZonaUbigeo(ByVal objDSBusuario1 As Object)
        Session.Remove("objDSBZonaUbigeo")
        Session.Add("objDSBZonaUbigeo", objDSBusuario1)
    End Sub

    Protected Sub rdbEstadoBusqueda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbEstadoBusqueda.SelectedIndexChanged
        buscarxEstado()
    End Sub

    Private Sub activarEstado(ByVal enabled As Boolean, ByVal index As Integer)
        lblEstado.Enabled = enabled
        rdbEstado.Enabled = enabled
        rdbEstado.SelectedIndex = index
    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        hddModo.Value = "N"
        verFrmNuevo()
        limpiarFrm()
        TxtNombre.ReadOnly = False
        TxtDescripcion.ReadOnly = False
        TxtValor.ReadOnly = False
    End Sub

    Private Sub verFrmNuevo()
        activarEstado(False, 0)
        Panel_Perfil.Visible = True
        Panel_Busqueda.Enabled = False
        mostrarBotonesControl()
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        hddModo.Value = "I"
        verFrmInicio()
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
        Dim objScript As New ScriptManagerClass
        Dim objZonaUbigeo As New Negocio.ZonaUbigeo
        Try
            Select Case hddModo.Value
                Case "N"
                    If validarExistenciaZonaUbigeo() Then
                        objScript.mostrarMsjAlerta(Me, "La Zona ha sido registrado .")
                        Return
                    End If
            End Select
            Dim obj As New Entidades.ZonaUbigeo
            If rdbEstado.Items.Item(0).Selected = True Then
                obj.Estado = True
            Else
                obj.Estado = False
            End If
            obj.IdZona = CStr(TxtCodigo.Text)
            obj.Nombre = TxtNombre.Text
            obj.Descripcion = TxtDescripcion.Text
            obj.Moneda = CStr(cboMoneda.SelectedValue)
            obj.MontoMin = CStr(TxtValor.Text)

            Dim hecho As Boolean = False
            Select Case hddModo.Value
                Case "N"
                    hecho = objZonaUbigeo.InsertaZonaubigeo(obj)
                Case "E"
                    hecho = objZonaUbigeo.ActualizaZonaUbigeo(obj)
            End Select
            If hecho Then
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            Else
                Throw New Exception
            End If
            verFrmInicio()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Ocurrieron problemas en la operación.")
        End Try
    End Sub

    Private Function validarExistenciaZonaUbigeo() As Boolean
        Dim flag As Boolean = False
        Try
            Dim objUtil As New Negocio.Util
            If objUtil.ValidarExistenciaxTablax1Campo("ZonaUbigeo", "zo_Nombre", TxtNombre.Text) > 0 Then
                flag = True
            End If
        Catch ex As Exception
            flag = False
        End Try
        Return flag
    End Function

    Private Sub cargarDatosZonaUbigeo()
        hddLogin.Value = DGVZonaUbigeo.SelectedRow.Cells(7).Text
        Try
            Dim objZonaUbigeo As Entidades.ZonaUbigeo = (New Negocio.ZonaUbigeo).SelectxIdZona(CInt(DGVZonaUbigeo.SelectedRow.Cells(2).Text))

            TxtNombre.Text = objZonaUbigeo.Nombre
            TxtCodigo.Text = objZonaUbigeo.IdZona.ToString
            TxtDescripcion.Text = objZonaUbigeo.Descripcion
            TxtValor.Text = objZonaUbigeo.MontoMin

            cboMoneda.SelectedValue = objZonaUbigeo.Moneda

            If CInt(objZonaUbigeo.Estado) = 1 Then
                rdbEstado.Items.Item(0).Selected = True
            Else
                rdbEstado.Items.Item(1).Selected = True
            End If

            hddModo.Value = "E"

            verFrmEditar()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos")
        End Try
    End Sub

    Private Sub verFrmEditar()
        Panel_Perfil.Visible = True
        Panel_Busqueda.Enabled = False
        mostrarBotonesControl()
        activarEstado(True, rdbEstado.SelectedIndex)

    End Sub

    Protected Sub DGVZonaUbigeo_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGVZonaUbigeo.SelectedIndexChanged
        cargarDatosZonaUbigeo()
    End Sub

    Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim lbtnQuitar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
        Dim fila As GridViewRow
        fila = CType(lbtnQuitar.NamingContainer, GridViewRow)
        Me.quitarRegistroCuentaPersona(fila.Cells(2).Text, fila.RowIndex)
    End Sub

    Private Sub quitarRegistroCuentaPersona(ByVal IdZona As String, ByVal DelFila As Integer)
        Dim objUtil As New Negocio.Util
        Dim objZonaUbigeo As New Negocio.ZonaUbigeo
        Dim obj As New Entidades.ZonaUbigeo
        Try
            '****************** validamos que no tenga registros relacionados
            If objUtil.ValidarExistenciaxTablax1Campo("Ubigeo", "IdZona", IdZona) > 0 Then
                objScript.mostrarMsjAlerta(Me, "La zona no puede ser eliminada por que posee zonas relacionados.")
                Return
            End If
            '**************** quitamos de la lista
            Dim hecho As Boolean
            For i As Integer = 0 To DGVZonaUbigeo.Rows.Count - 1
                With DGVZonaUbigeo.Rows(i)
                    obj.IdZona = CStr(.Cells(2).Text)
                End With
            Next
            hecho = objZonaUbigeo.EliminaZonaubigeo(obj)
            If hecho Then
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminación del registro.")
        End Try
    End Sub

End Class