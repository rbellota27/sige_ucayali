<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmGuiaRecepcion_1.aspx.vb" Inherits="APPWEB.FrmGuiaRecepcion_1"  title="Gu�a de Recepci�n" %>    

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register assembly="Enlasys.WebControls" namespace="Enlasys.Web.UI.WebControls" tagprefix="Enlasys" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
            <table width="100%">
                <tr>
                    <td >
                        <asp:ImageButton ID="btnNuevo"  runat="server" ImageUrl="~/Imagenes/Nuevo_b.JPG" onmouseover="this.src='../Imagenes/Nuevo_A.JPG';" onmouseout="this.src='../Imagenes/Nuevo_b.JPG';" />
                        <asp:ImageButton ID="btnEditar" runat="server" ImageUrl="~/Imagenes/Editar_B.JPG"
                            onmouseout="this.src='../Imagenes/Editar_B.JPG';" OnClientClick="return(valEditar());"
                                 onmouseover="this.src='../Imagenes/Editar_A.JPG';" />
                                                         
                             <asp:ImageButton ID="btnAnular" runat="server" ImageUrl="~/Imagenes/Anular_B.JPG" 
                            onmouseout="this.src='../Imagenes/Anular_B.JPG';"  OnClientClick="return(valAnular());"
                                 onmouseover="this.src='../Imagenes/Anular_A.JPG';" />
                             
                        <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG" 
                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';"  
                            onmouseout="this.src='../Imagenes/Buscar_b.JPG';"   />    
                               <asp:ImageButton ID="btnBuscarDocumento_GRec" runat="server" ImageUrl="~/Imagenes/Busqueda_b.JPG"                             
                                            onmouseover="this.src='../Imagenes/Busqueda_A.JPG';"  OnClientClick="return(validarBuscarDocumento());"
                                              onmouseout="this.src='../Imagenes/Busqueda_b.JPG';" 
                                 style="width: 27px" />                         
                        <asp:ImageButton ID="btnGrabar" runat="server" 
                            ImageUrl="~/Imagenes/Guardar_B.JPG" OnClientClick="return(validarSave());" 
                            onmouseover="this.src='../Imagenes/Guardar_A.JPG';" 
                            onmouseout="this.src='../Imagenes/Guardar_B.JPG';" ValidationGroup="valSave" />                                                
                        <asp:ImageButton ID="btnCancelar1" runat="server" ImageUrl="~/Imagenes/Cancelar_B.JPG" onmouseover="this.src='../Imagenes/Cancelar_A.JPG';" onmouseout="this.src='../Imagenes/Cancelar_B.JPG';" />
                        
                        <cc1:ConfirmButtonExtender ID="btnCancelar1_ConfirmButtonExtender" 
                            runat="server" ConfirmText="Est� seguro que desea cancelar la operaci�n?" Enabled="True" TargetControlID="btnCancelar1">
                        </cc1:ConfirmButtonExtender>                        
                        <asp:ImageButton ID="btnImprimir" runat="server" Enabled="true" 
                            ImageUrl="~/Imagenes/Imprimir_B.JPG" 
                            OnClientClick="return(valImprimir());" 
                            onmouseout="this.src='../Imagenes/Imprimir_B.JPG';" 
                            onmouseover="this.src='../Imagenes/Imprimir_A.JPG';" />
                        </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_Frm" runat="server">
                            <table style="width: 100%">
                     <tr>
                    <td class="TituloCelda">
                        GU�A RECEPCI�N</td>
                </tr>
                <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel_Cabecera" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                <table>
                
                <tr>
                    <td >
                        <asp:Panel ID="Panel_Config" runat="server">
                        
                        <table>
                    <tr>
                    <td>
                    
                    <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Empresa:"></asp:Label>
                        <asp:DropDownList ID="cmbEmpresa" runat="server" AutoPostBack="True" 
                            DataTextField="NombreComercial" DataValueField="Id">
                        </asp:DropDownList>
                        <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Tienda:"></asp:Label>
                        <asp:DropDownList ID="cmbTienda" runat="server" AutoPostBack="True" 
                            DataTextField="Nombre" DataValueField="Id">
                        </asp:DropDownList>
                        <asp:Label ID="Label20" runat="server" CssClass="Label" Text="Almac�n:"></asp:Label>
                        <asp:DropDownList ID="cmbAlmacen" runat="server" DataTextField="Nombre" 
                            DataValueField="IdAlmacen">
                        </asp:DropDownList>
                        <asp:Label ID="Label77" runat="server" CssClass="Label" Text="N� Serie:"></asp:Label>
                        <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True" 
                            Font-Bold="True">
                        </asp:DropDownList>
                        <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" 
                            onKeyPress="return( valOnKeyPressCodigoDoc(this,event)  );" ReadOnly="True" Width="85px"></asp:TextBox>
                    
                    </td>
                    </tr>
                    </table>    
                        
                        </asp:Panel>
                    
                    
                        
                    </td>
                </tr>
                <tr>
                                    <td>
                                        <asp:Label ID="Label21" runat="server" CssClass="Label" Text="Tipo Documento:"></asp:Label>
                                        <asp:DropDownList ID="cmbTipoDocumento" runat="server" AutoPostBack="False" 
                                            Enabled="false">
                                            <asp:ListItem Enabled="true" Selected="True" Value="25">Gu�a de Recepci�n</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="Label2223" runat="server" Text="Tipo Operaci�n:" CssClass="Label"></asp:Label>
                                        <asp:DropDownList ID="cmbTipoOperacion" runat="server" DataTextField="Nombre" DataValueField="Id">
                                        </asp:DropDownList>
                                        <asp:Label ID="Label2224" runat="server" Text="Estado:" CssClass="Label"></asp:Label>
                                        <asp:DropDownList ID="cmbEstadoDocumento" runat="server" Enabled="false" >
                                        </asp:DropDownList>
                                                                      
                                     
                                    </td>
                                </tr>
                
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Fecha de Ingreso:"></asp:Label>
                            <asp:TextBox ID="txtFecha" runat="server" CssClass="TextBox_Fecha" 
                                onblur="return(valFecha(this));" Width="90px"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="txtFecha0_MaskedEditExtender" runat="server" 
                                ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFecha">
                            </cc1:MaskedEditExtender>
                            <cc1:CalendarExtender ID="txtFecha0_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFecha">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                
                </table>    
                    </ContentTemplate>
                    </asp:UpdatePanel>
                
                
                </td>
                </tr>
                <tr>
                    <td class="TituloCelda">Remitente</td>
                </tr>
                <tr>
                    <td style="width: 100%">
                        <asp:UpdatePanel ID="UpdatePanel_Remitente" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                        
                        <asp:Panel ID="Panel_Remitente" runat="server">
                        <table width="100%">
                            <tr>
                                <td align="right">
                                    <asp:Label ID="Label7" runat="server" CssClass="Label" 
                                        Text="Ap. y Nombres/Raz�n Social:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRazonSocial" runat="server" Width="450px"  onKeypress="return( false );" onFocus="return(   valOnFocusRemitente()  );"
                                        CssClass="TextBoxReadOnly" ></asp:TextBox>
                                    <asp:TextBox ID="txtIdCliente" Width="75px"  CssClass="TextBoxReadOnly" runat="server" onKeypress="return( false );" onFocus="return(   valOnFocusRemitente()  );" ></asp:TextBox>
                                    <asp:ImageButton ID="btnBuscarP" runat="server" CausesValidation="false" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG" 
                                        onmouseout="this.src='../Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='../Imagenes/Buscar_A.JPG';" OnClientClick="return(mostrarCapaPersona());" />
                                </td>
                            </tr>  
                            <tr>
                            <td colspan="2">
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" 
                                                TargetControlID="Panel_DescRemitente" 
                                                CollapsedSize="0" 
                                                ExpandedSize="60" 
                                                Collapsed="true"
                                                ExpandControlID="Image21_11"
                                                CollapseControlID="Image21_11" 
                                                TextLabelID="Label21_11" 
                                                ImageControlID="Image21_11"
                                                CollapsedImage="~/Imagenes/Mas_B.JPG"
                                              ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                CollapsedText="Datos Remitente" 
                                                ExpandedText="Datos Remitente"                               
                                                ExpandDirection="Vertical" SuppressPostBack="True">
                                                </cc1:CollapsiblePanelExtender>                                                                           
                                            <asp:Image ID="Image21_11" runat="server" Height="16px" />                                                             
                                            <asp:Label ID="Label21_11" runat="server" Text="Label" CssClass="Label"></asp:Label>
                                <asp:Panel ID="Panel_DescRemitente" runat="server">
                                <table>
                                    <tr>
                                <td style="text-align: right;">
                                    <asp:Label CssClass="Label" ID="Label15" runat="server"  Text="D.N.I.:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDNI" runat="server" CssClass="TextBoxReadOnly" Width="183px" onKeypress="return( false );" onFocus="return(   valOnFocusRemitente()  );"></asp:TextBox>
                                    <asp:Label CssClass="Label" ID="Label16" runat="server"  Text="R.U.C.:"></asp:Label>
                                    <asp:TextBox ID="txtRUC" runat="server" CssClass="TextBoxReadOnly" Width="202px"  onKeypress="return( false );" onFocus="return(   valOnFocusRemitente()  );"></asp:TextBox>
                                </td>
                            </tr>                            
                                    </table>  
                                </asp:Panel>
                                </td>
                            </tr>                         
                           
                            <tr>
                                <td style="text-align: right">
                                    <asp:Label ID="Label24" CssClass="Label" runat="server" Text="Doc. Relacionado:"></asp:Label>
                                    </td>
                                <td>
                                    
                                    <table>
                                    <tr>
                                    <td>
                                    <asp:Label ID="lblTipoDocumento_DocRelacional" CssClass="LabelRojo"  Font-Bold="true" runat="server" Text=""></asp:Label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td><asp:Label ID="lblSerie_DocRelacional" CssClass="LabelRojo"  Font-Bold="true" runat="server" Text=""></asp:Label></td>
                                    <td><asp:Label ID="Label26" runat="server" Text="-"  CssClass="LabelRojo"></asp:Label></td>
                                    <td><asp:Label ID="lblCodigo_DocRelacional" CssClass="LabelRojo"  Font-Bold="true" runat="server" Text=""></asp:Label></td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Button ID="btnBuscarDocumento_OnCapa" OnClientClick="return(  valOnClick_btnBuscarDocumento_OnCapa()  );" runat="server" Text="Buscar" Width="70px" /></td>                                        
                                    </tr>
                                    </table>                                    
                                    </td>
                            </tr>
                        </table>
                        </asp:Panel>                        
                        
                        </ContentTemplate>
                        <Triggers>
                        
                        <asp:AsyncPostBackTrigger ControlID="DGV_BuscarPersona" EventName="SelectedIndexChanged" />
                                                
                        </Triggers>
                        </asp:UpdatePanel>
                    
                        
                        
                        
                        
                    </td>
                </tr>
                <tr>
                    <td class="TituloCeldaLeft">                    
    <asp:ImageButton ID="btnAgregarProducto"  runat="server"  OnClientClick="return(mostrarCapaBuscarProd());"
        ImageUrl="~/Imagenes/BuscarProducto_b.JPG" onmouseout="this.src='../Imagenes/BuscarProducto_b.JPG';"                                             
    onmouseover="this.src='../Imagenes/BuscarProducto_A.JPG';" />
    </td>
                </tr>             
                <tr>
                    <td style="width: 100%">
                    
                        <asp:UpdatePanel ID="UpdatePanel_Detalle" runat="server" UpdateMode="Conditional">
                        <ContentTemplate >
                        
                        <asp:GridView ID="DGVDetalle" runat="server" AutoGenerateColumns="False" 
                            Width="100%">
                            <HeaderStyle CssClass="GrillaHeader" />
                            <Columns>
                                <asp:CommandField SelectText="Quitar" ShowSelectButton="True" />
                                <asp:BoundField DataField="IdProducto" HeaderText="IdProducto" />
                                <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" />
                                <asp:TemplateField HeaderText="U. Medida">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="cmbUMedida" runat="server" DataTextField="NombreCortoUM" 
                                            DataValueField="IdUnidadMedida">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cantidad">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtCantidad" TabIndex="100" onfocus="return(aceptarFoco(this));" runat="server" 
                                            onKeypress="return(validarNumeroPunto(event));" onblur="return(valBlur(event));"
                                            Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>                        
                        </ContentTemplate>                        
                        <Triggers >
                        <asp:AsyncPostBackTrigger ControlID="btnAddProductos" EventName="Click" />
                        
                        
                        
                        </Triggers>
                        </asp:UpdatePanel>
                    
                        
                        
                        
                        
                    </td>
                </tr>
               
                <tr class="TituloCelda"   >
                <td>Observaciones</td>
                </tr>
                <tr>
                    <td style="width:100%; height: 26px">
                        <asp:TextBox Width="100%" ID="txtObservaciones" runat="server" Height="63px" onKeypress="return(validarEnter());"
                            TextMode="MultiLine" MaxLength="500"></asp:TextBox></td>
                </tr>
                <tr>
                <td>
                    &nbsp;</td>
                </tr>           </table>                        
                        </asp:Panel>
                    </td>
                </tr>           
                <tr>
                                    <td>
                                        <asp:HiddenField ID="hddModoFrm" runat="server" Visible="true" />
                                        <asp:HiddenField ID="hddIdDocumento" runat="server" Visible="true" />
                                        <asp:HiddenField ID="hddCantidadFilas" runat="server" Visible="true" />
                                        <asp:HiddenField ID="hddIdDocumento_DocRelacional" runat="server" Visible="true" />
                                        
                                    </td>
                                </tr>     
                            </table>
 
    
                        
                          
<div  id="capaBuscarDocumento" style="border: 3px solid blue; padding: 8px; width:840px; height:auto; position:absolute; top:280px; left:38px; background-color:white; z-index:2; display :none; ">                        
                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="ImageButton3" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='../Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='../Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaBuscarDocumento'));" 
                                        />                                
                                </td>
                                </tr>
                                <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="N� Serie:" CssClass="Label"></asp:Label>
                                    <asp:TextBox ID="txtSerieBuscarDoc" onKeypress="return(onKeyPressEsNumero('event'));"  Width="80px" Font-Bold="true" runat="server"></asp:TextBox>
                                    <asp:Label ID="Label2226" runat="server" Text="N� C�digo:" CssClass="Label"></asp:Label>
                                    <asp:TextBox ID="txtCodigoBuscarDoc" onKeypress="return(onKeyPressEsNumero('event'));" Width="90px" Font-Bold="true" runat="server"></asp:TextBox>
                                    <asp:ImageButton ID="btnBuscarDocumento" runat="server" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG"  OnClientClick="return(valBuscarDocumento());"
                                        onmouseout="this.src='../Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                    </td>
                                </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="DGV_BuscarDoc" runat="server" AutoGenerateColumns="false" Width="100%"  >
                                            <Columns>
                                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="true" ButtonType="Link" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="NomTipoDocumento" HeaderText="Tipo Documento" NullDisplayText="---" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderText="Nro. Documento"  ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >
                                            <ItemTemplate>
                                            <table>
                                            <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdDocumento_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNroDocumento_Find" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"Numero")%>'   ></asp:Label></td>
                                            </tr>
                                            </table>
                                            </ItemTemplate>
                                            </asp:TemplateField>                                                                                                                                                                                
                                            <asp:BoundField DataField="FechaEmision" HeaderText="F. Emisi�n" DataFormatString="{0:dd/MM/yyyy}" NullDisplayText="---" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="NomPropietario" HeaderText="Empresa" NullDisplayText="---" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="NomTienda" HeaderText="Tienda" NullDisplayText="---" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />                                            
                                            </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>                                 
                                </table>                            
</div>                           

                       

<div  id="capaBuscarP" style="border: 3px solid blue; padding: 8px; width:840px; height:auto; position:absolute; top:203px; left:32px; background-color:white; z-index:2; display :none;">
                        <asp:UpdatePanel ID="UpdatePanelBuscarP" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>

                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                            <asp:ImageButton ID="ImageButton1" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='../Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='../Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaBuscarP'));" 
                                        />                                
                                </td>
                                </tr>
                                <tr>
                                <td>
                                    <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                                    <asp:DropDownList ID="cmbLinea" runat="server" AutoPostBack="True" DataTextField="Descripcion" DataValueField="Id">
                                    </asp:DropDownList>
                                    <asp:Label ID="Label18" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                    <asp:DropDownList ID="cmbSubLinea" runat="server" DataTextField="Nombre" DataValueField="Id" >
                                    </asp:DropDownList>                                    
                                    
                                    <asp:TextBox ID="txtCodSubLinea_BuscarProd" Width="75px" onfocus="return(aceptarFoco(this));" onKeyPress="return(onKeyPressEsNumero('event'));"  runat="server"></asp:TextBox>
                                    
                                    </td>
                                </tr>
                                <tr>
                                <td>
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                                    <asp:TextBox onfocus="return(aceptarFoco(this));" ID="txtDescripcionProd" runat="server" Width="300px"></asp:TextBox>
                                    <asp:ImageButton ID="btnBuscarGrilla" runat="server" 
                                        ImageUrl="~/Imagenes/Buscar_b.JPG" 
                                        onmouseout="this.src='../Imagenes/Buscar_b.JPG';" 
                                        onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                        
                                        
                        <asp:ImageButton ID="btnAddProductos" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG" 
                            onmouseout="this.src='../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../Imagenes/Agregar_A.JPG';"
                            OnClientClick="return(valAddProductos());" />
                                             
                                </td>
                                </tr>
                                <tr>
                                <td>
                                    <asp:GridView ID="DGVSelProd" runat="server" AllowPaging="false" 
                                        AutoGenerateColumns="False" Width="100%" PageSize="20">
                                        <Columns>
                                            <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chbSelectProducto" runat="server" />
                                            </ItemTemplate>                                            
                                            </asp:TemplateField>                                            
                                            
                                            <asp:BoundField DataField="IdProducto" HeaderText="C�digo" 
                                                NullDisplayText="0" />
                                            <asp:BoundField DataField="NomProducto" HeaderText="Descripci�n" 
                                                NullDisplayText="---" />                                                
                                            <asp:BoundField DataField="NomUMedida" HeaderText="U.M. Principal" 
                                                NullDisplayText="---" />
                                            <asp:BoundField DataField="NomLinea" HeaderText="L�nea" NullDisplayText="---" />
                                            <asp:BoundField DataField="NomSubLinea" HeaderText="Sub L�nea" 
                                                NullDisplayText="---" />                                            
                                            <asp:BoundField DataField="IdUnidadMedida" HeaderText="" NullDisplayText="0" />                                                
                                        </Columns>
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    </asp:GridView></td>
                                </tr>
                                  <tr>
                                    
                                    <td>
                                    
                                        <asp:Button ID="btnAnterior_Productos" runat="server" Font-Bold="true" Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                                        <asp:Button ID="btnPosterior_Productos" runat="server" Font-Bold="true" Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                                        <asp:TextBox ID="txtPageIndex_Productos" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                                        <asp:TextBox ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                    
                                    </td>
                                    </tr>  
                            </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>                        

                    

<div  id="capaPersona" 
        style="border: 3px solid blue; padding: 8px; width:900px; height:auto; position:absolute; top:239px; left:27px; background-color:white; z-index:2; display :none; ">
                        <asp:UpdatePanel ID="UpdatePanel_Persona" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table style="width: 100%;">                                    
                                <tr><td align="right">
                                       <asp:ImageButton ID="btnCerrar_Capa" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar_B.JPG" 
                                                onmouseout="this.src='../Imagenes/Cerrar_B.JPG';" 
                                                onmouseover="this.src='../Imagenes/Cerrar_A.JPG';"  
                                        OnClientClick="return(offCapa('capaPersona'));" />   
                                </td>
                                </tr>
                                    <tr class="Label">
                                        <td align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Filtro:</td>
                                                    <td>
                                                        <asp:DropDownList ID="cmbFiltro_BuscarPersona" runat="server">
                                                        <asp:ListItem Selected="True" Value="0">Descripci�n</asp:ListItem>
                                                        <asp:ListItem Value="1">R.U.C.</asp:ListItem>
                                                        <asp:ListItem Value="2">D.N.I.</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        Texto:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtBuscarPersona_Grilla" runat="server" Width="250px"></asp:TextBox>
                                                        <asp:ImageButton ID="btnBuscarPersona_Grilla" runat="server" 
                                                            CausesValidation="false" ImageUrl="~/Imagenes/Buscar_b.JPG"                                                             
                                                            onmouseout="this.src='../Imagenes/Buscar_b.JPG';" 
                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                         <asp:GridView ID="DGV_BuscarPersona" runat="server" AllowPaging="false" 
                                                                  AutoGenerateColumns="false" PageSize="20" Width="100%">
                                                                  <Columns>
                                                                     
                                                                     <asp:TemplateField>
                                                                     <ItemTemplate>
                                                                     
                                                                         <asp:ImageButton ID="btnSelectPersona" ImageUrl="~/Imagenes/Ok_b.bmp" runat="server" OnClientClick="return(  valOnClickSelectPersona(this)   );" ToolTip="Seleccionar Remitente"  />
                                                                     
                                                                     </ItemTemplate>
                                                                     </asp:TemplateField>
                                                                     
                                                                     
                                                                     
                                                                      <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                                                                      <asp:BoundField DataField="getNombreParaMostrar" HeaderText="Descripci�n" 
                                                                          NullDisplayText="---" />
                                                                      <asp:BoundField DataField="Dni" HeaderText="D.N.I." NullDisplayText="---" />
                                                                      <asp:BoundField DataField="Ruc" HeaderText="R.U.C." NullDisplayText="---" />
                                                                      <asp:BoundField DataField="Direccion" HeaderText="Direcci�n" 
                                                                          NullDisplayText="---" />                                                                      
                                                                  </Columns>
                                                                  <HeaderStyle CssClass="GrillaHeader" />
                                                                  <FooterStyle CssClass="GrillaFooter" />
                                                                  <PagerStyle CssClass="GrillaPager" />
                                                                  <RowStyle CssClass="GrillaRow" />
                                                                  <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                  <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                  <EditRowStyle CssClass="GrillaEditRow" />
                                                              </asp:GridView></td>
                                    </tr> 
                                    <tr>
                                    
                                    <td>
                                    
                                        <asp:Button ID="btnAnterior_Persona" runat="server" Font-Bold="true" Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionPersona('0'));" />
                                        <asp:Button ID="btnPosterior_Persona" runat="server" Font-Bold="true" Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionPersona('1'));" />
                                        <asp:TextBox ID="txtPageIndex_Persona" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnIr_Persona" runat="server" Font-Bold="true" Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionPersona('2'));" />
                                        <asp:TextBox ID="txtPageIndexGO_Persona" Width="50px" CssClass="TextBox_ReadOnly" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                    
                                    </td>
                                    </tr>                                     
                 
                 </table>      
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>   


<script language="javascript" type="text/javascript">
   
       function valAddProductos() {
        var grilla = document.getElementById('<%=DGVSelProd.ClientID%>');
        var cont = 0;
        if (grilla == null) {
            alert('No se seleccionaron productos.');
            return false;
        }
        for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].status == true) {
                cont = cont + 1;
            }
        }

        if (cont == 0) {
            alert('No se seleccionaron productos.');
            return false;
        }
        return true;
    }

    function valImprimir() {
        var hdd = document.getElementById('<%= hddIdDocumento.ClientID%>');
        if (hdd.value.length == 0 || hdd.value == '' || parseFloat(hdd.value) == 0) {
            alert('No se ha registrado ninguna Gu�a de Recepci�n para enviar a impresi�n.');
            return false;
        }
        if (confirm('Desea mandar la impresi�n de la Gu�a de Recepci�n?')) {
            window.open('../../Almacen1/Reportes/Visor.aspx?iReporte=6&IdDocumento=' + hdd.value, 'Reporte', 'resizable=yes,width=1000,height=800,scrollbars=1', null);
        }
        return false;
    }
    function valBuscarDoc() {
        var cajaSerie = document.getElementById('<%= txtSerieBuscarDoc.ClientID%>');
        var cajaCodigo = document.getElementById('<%= txtCodigoBuscarDoc.ClientID%>');
        if (CajaEnBlanco(cajaSerie) == true) {
            alert('Debe ingresar un N� Serie');
            cajaSerie.focus();
            return false;
        }
        if (CajaEnBlanco(cajaCodigo) == true) {
            alert('Debe ingresar un N� C�digo');
            cajaCodigo.focus();
            return false;
        }
        return true;
    }



    function xxx() {
        if (esEnter(event)) {
            /*var btn = document.getElementById('<%=btnNuevo.ClientID %>');
            btn.click();*/
            //__doPostBack('<%=btnNuevo.ClientID%>', '');
            __doPostBack('ctl00$ContentPlaceHolder1$btnNuevo', '');
        }
    }
    function validarNumeroPunto(e) {
        var evt = e ? e : event;
        var key = window.Event ? evt.which : evt.keyCode;
        if (key == 46) {
            return true;
        } else {
            if (!onKeyPressEsNumero('event')) {
                alert('Caracter no v�lido.');
                return false;
            }
        }
    }
    function validarEnter() {
        if (esEnter(event)) {
            return false;
        } else {
            return true;
        }
    }
    function validarSave() {
        //*********** verificamos que los combos esten seleccionados
        var cbo = document.getElementById('<%= cmbEmpresa.ClientID%>');
        if (parseFloat(cbo.value) == 0) {
            alert('Debe seleccionar una Empresa.');
            return false;
        }
        cbo = document.getElementById('<%= cmbTienda.ClientID%>');
        if (parseFloat(cbo.value) == 0) {
            alert('Debe seleccionar una Tienda.');
            return false;
        }
        cbo = document.getElementById('<%= cmbAlmacen.ClientID%>');
        if (parseFloat(cbo.value) == 0) {
            alert('Debe seleccionar un Almac�n.');
            return false;
        }
        cbo = document.getElementById('<%= cmbTipoOperacion.ClientID%>');
        if (parseFloat(cbo.value) == 0) {
            alert('Debe seleccionar un Tipo de Operaci�n.');
            return false;
        }
        cbo = document.getElementById('<%= cboSerie.ClientID%>');
        if (cbo.value.length == 0 || cbo.value == '') {
            alert('Debe asignar un N� de Serie.');
            return false;
        }
        if (parseFloat(cbo.value) == 0) {
            alert('Debe asignar un N� de Serie.');
            return false;
        }

        //***********************************
        var grilla = document.getElementById('<%=DGVDetalle.ClientID %>');
        if (grilla == null) {
            alert('No ha ingresado productos.');
            return false;
        }
        for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
            var rowElem = grilla.rows[i];
            /*for (var x = 0; x < rowElem.cells.length; x++) {
            var cell = rowElem.cells[x];               
                
            }*/
            var cellCantidad = rowElem.cells[4];
            if ((!esDecimal(cellCantidad.children[0].value)) || (parseFloat(cellCantidad.children[0].value) == 0)) {
                alert('Cantidad no v�lida.');
                cellCantidad.children[0].select();
                cellCantidad.children[0].focus();
                return false;
            }
            if (cellCantidad.children[0].value.length == 0) {
                alert('Ingrese una cantidad.');
                cellCantidad.children[0].select();
                cellCantidad.children[0].focus();
                return false;
            }
        }
        var cajaRemitente = document.getElementById('<%=txtIdCliente.ClientID %>');
        if (cajaRemitente.value.length == 0) {
            alert('Ingrese un Remitente.');
            cajaRemitente.select();
            cajaRemitente.focus();
            return false;
        }

        var nrofilas = document.getElementById('<%=hddCantidadFilas.ClientID%>');
        if (grilla.rows.length - 1 > nrofilas.value) {
            alert('Este Documento debe tener como maximo ' + nrofilas.value + ' items en su detalle.');
            return false;
        }
       return (confirm('Desea continuar con la operaci�n?'));
    }
    function valBlur(e) {
        var e = e ? e : window.event;
        var event_element = e.target ? e.target : e.srcElement;

        var id = event_element.id;
        var caja = document.getElementById(id);
        if (CajaEnBlanco(caja)) {
            caja.value = 0;
            //alert('Debe ingresar un valor.');
        }
        if (!esDecimal(caja.value)) {
            caja.select();
            caja.focus();
            alert('Valor no v�lido.');
        }
    }

    function valNavegacionPersona(tipoMov) {
        var index = parseInt(document.getElementById('<%=txtPageIndex_Persona.ClientID%>').value);
        if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
            alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
            return false;
        }
        switch (tipoMov) {
            case '0':   //************ anterior
                if (index <= 1) {
                    alert('No existen p�ginas con �ndice menor a uno.');
                    return false;
                }
                break;
            case '1':
                break;
            case '2': //************ ir
                index = parseInt(document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').value);
                if (isNaN(index) || index == null || index.length == 0) {
                    alert('Ingrese una P�gina de navegaci�n.');
                    document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                    document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                    return false;
                }
                if (index < 1) {
                    alert('No existen p�ginas con �ndice menor a uno.');
                    document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                    document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                    return false;
                }
                break;
        }
        return true;
    }
    function mostrarCapaPersona() {        
        onCapa('capaPersona');
        document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
        document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();        
        return false;
    }

    function valNavegacionProductos(tipoMov) {
        var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
        if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
            alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
            document.getElementById('<%=txtDescripcionProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd.ClientID%>').focus();
            return false;
        }
        switch (tipoMov) {
            case '0':   //************ anterior
                if (index <= 1) {
                    alert('No existen p�ginas con �ndice menor a uno.');
                    return false;
                }
                break;
            case '1':
                break;
            case '2': //************ ir
                index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                if (isNaN(index) || index == null || index.length == 0) {
                    alert('Ingrese una P�gina de navegaci�n.');
                    document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                    document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                    return false;
                }
                if (index < 1) {
                    alert('No existen p�ginas con �ndice menor a uno.');
                    document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                    document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                    return false;
                }
                break;
        }
        return true;
    }
    function mostrarCapaBuscarProd() {
        onCapa('capaBuscarP');
        document.getElementById('<%=txtCodSubLinea_BuscarProd.ClientID %>').select();
        document.getElementById('<%=txtCodSubLinea_BuscarProd.ClientID %>').focus();
        return false;
    }

    function validarBuscarDocumento() {
        var caja = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
        if (CajaEnBlanco(caja)) {
            alert('Ingrese un c�digo de b�squeda.');
            caja.focus();
            return false;
        }
        var IdSerie = parseInt(document.getElementById('<%=cboSerie.ClientID %>').value);
        if(isNaN(IdSerie)){
            alert('Seleccione un N�mero de Serie de Documento.');
            return false;
        }
        
        return true;
    }
    function valAnular() {
        //******************** Validamos
        var IdEstadoDoc = parseInt(document.getElementById('<%=cmbEstadoDocumento.ClientID %>').value);
        if (IdEstadoDoc == 2) {
            alert('El Documento ya se encuentra anulado.');            
            return false;
        }
        return confirm('Desea continuar con la operaci�n de anulaci�n?');
    }

    function valEditar() {
        var IdEstadoDoc = parseInt(document.getElementById('<%=cmbEstadoDocumento.ClientID %>').value);
        if (IdEstadoDoc == 2) {
            alert('El Documento se encuentra anulado. No se permite su edici�n.');
            return false;
        }
        return true;
    }
    function valOnClickSelectPersona(boton) {
        var grilla = document.getElementById('<%=DGV_BuscarPersona.ClientID%>');
        if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                if (rowElem.cells[0].children[0].id == boton.id) {
                    document.getElementById('<%=txtIdCliente.ClientID%>').value = rowElem.cells[1].innerHTML; //*** ID
                    document.getElementById('<%=txtRazonSocial.ClientID%>').value = rowElem.cells[2].innerHTML; //*** Descripcion
                    document.getElementById('<%=txtDNI.ClientID%>').value = rowElem.cells[3].innerHTML; //*** DNI
                    document.getElementById('<%=txtRUC.ClientID%>').value = rowElem.cells[4].innerHTML; //*** RUC
                    
                    offCapa('capaPersona');                
                    return false;
                }
            }                        
        }       
        alert('No se hall� el registro seleccionado.');
        return false;
    }
        
    function valOnFocusRemitente(){
        document.getElementById('<%=btnBuscarP.ClientID%>').focus();
        return false;
    }
    
    function valOnClick_btnBuscarDocumento_OnCapa() {
        onCapa('capaBuscarDocumento');
        document.getElementById('<%=txtSerieBuscarDoc.ClientID%>').select();
        document.getElementById('<%=txtSerieBuscarDoc.ClientID%>').focus();
        return false;
    }

    function valOnKeyPressCodigoDoc(txtCodigoDocumento,e) {
        var evt = e ? e : event;
        var key = window.Event ? evt.which : evt.keyCode;
        if (key == 13) {
            document.getElementById('<%=btnBuscarDocumento_GRec.ClientID %>').focus();
        } else {
            return onKeyPressEsNumero('event');
        }

    }

    function valBuscarDocumento() {
        var cajaSerie = document.getElementById('<%= txtSerieBuscarDoc.ClientID%>');
        var cajaCodigo = document.getElementById('<%= txtCodigoBuscarDoc.ClientID%>');
        if (CajaEnBlanco(cajaSerie) == true) {
            alert('Debe ingresar un N� Serie');
            cajaSerie.focus();
            return false;
        }
        if (CajaEnBlanco(cajaCodigo) == true) {
            alert('Debe ingresar un N� C�digo');
            cajaCodigo.focus();
            return false;
        }
        return true;        
    }
</script>
</asp:Content>

