﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="UbicacionesLayout.aspx.vb" Inherits="APPWEB.UbicacionesLayout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
    <table class="style1">
        <tr>
            <td style="width: 840px">
                <table class="style1" style="width: 126%">
                    <tr>
                        <td style="text-align: center; color: #0000FF; font-weight: 700;" colspan="7">
                            UBICACIONES EN ALMACEN</td>
                    </tr>
                    <tr>
                        <td style="text-align: center; color: #0000FF; height: 11px;" colspan="7">
                            </td>
                    </tr>
                    <tr>
                        <td style="width: 115px; text-align: right; color: #0000FF; height: 26px;">
                            <strong>Almacen :</strong></td>
                        <td style="width: 150px; height: 26px;">
                            <select id="cboTienda" name="D1" style="width: 271px">
                                <option></option>
                            </select></td>
                        <td style="width: 93px; text-align: right; color: #0000FF; height: 26px;" 
                            class="style2">
                            <strong>Zona :</strong></td>
                        <td style="width: 203px; height: 26px;">
                            <select id="cboZona" name="D2" style="width: 261px">
                                <option></option>
                            </select></td>
                        </td>
                        <td style="width: 20px; height: 26px;" class="style2">                        
                        <td style="width: 116px; height: 26px;">
                            </td>
                        <td style="height: 26px">
                            </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 840px">
                            <input id="btnConsultar" type="button" value="Consultar"  
                                onclick="return btnConsultar_onclick(this)" style="width: 132px" /></td>
        </tr>
        <tr>
            <td style="width: 840px">
        
              <div id="CapaLayout"></div></td>

            </div>
            </td>
        </tr>
    </table>
</div>

<div id="CapaMantenimiento" style="border: 3px solid blue; padding: 4px; width: 858px;
            height: 550px; position: absolute; top: 0px; left: 250px; background-color: #EFFBFB;
            z-index: 3; display:none;">
                   <table border="0" cellspacing="0px" style="width: 858px">
                   <tr>
                    <td>
                          <table border="0">
                                        <tr class="BarraTitulo">
                                        <td style="width:101%; background-color: #0066CC;" class="style1"><span id="TituloPopup"></span></td>
                                        <td style="width:4%"><img src='../../ImagenesForm/imgcerrar.png' class="Iconoventanas" 
                                                title="Cerrar Ventana" onclick="return cerrardetalle()"/></td>
                                        </tr>
                          </table>
                    </td>
                    </tr>
                    <tr>
                    <td>
                       <div id="TablaMantenimiento">


                           <table class="style1">
                         
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="text-align: right; color: #0000FF; width: 162px; background-color: #FFFFFF">
                                       <b>Id :</b></td>
                                   <td style="width: 112px">
                                       <input id="TxtIdUbicacion" type="text" style="width: 83px"  readonly/></td>
                                   <td style="width: 151px">
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="text-align: right; color: #0000FF; width: 162px; background-color: #FFFFFF">
                                       <b>Estanteria :</b></td>
                                   <td style="width: 112px">
                            <select id="cboEstanteria" name="D4" style="width: 294px">
                                <option></option>
                            </select></td>
                                   <td style="width: 151px">
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="text-align: right; color: #0000FF; width: 162px; background-color: #FFFFFF">
                                       <b>Zona :</b></td>
                                   <td style="width: 112px">
                            <select id="cboZonaAlmacenamiento" name="D2" style="width: 294px">
                                <option></option>
                            </select></td>
                                   <td style="width: 151px">
                                       &nbsp;</td>
                               </tr>
                   
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="text-align: right; color: #0000FF;" colspan="3">
                                       <table class="style1">
                                           <tr>
                                               <td style="width: 218px; text-align: right; font-weight: 700; color: #0000FF;">
                                                   Nro Columna :</td>
                                               <td style="width: 134px; text-align: right;">
                                       <input id="TxtNroColumna" type="text" /></td>
                                               <td style="width: 38px; color: #0000FF; text-align: right;">
                                                   &nbsp;</td>
                                               <td style="color: #0000FF; text-align: right; width: 94px">
                                                   <strong>Nro Fila :</strong></td>
                                               <td style="width: 142px">
                                       <input id="TxtNroFila" type="text" /></td>
                                               <td>
                                                   &nbsp;</td>
                                           </tr>
                                       </table>
                                   </td>
                               </tr>
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="width: 162px; text-align: right; color: #0000FF;">
                                       <b>Fondo :</b></td>
                                   <td style="width: 112px">
                                       <input id="TxtFondo" type="text" /></td>
                                   <td style="width: 151px" rowspan="10">
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="width: 162px; text-align: right; color: #0000FF;">
                                       <b>Ancho Real :</b></td>
                                   <td style="width: 112px">
                                       <input id="TxtAncho" type="text" /></td>
                               </tr>
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="width: 162px; text-align: right; color: #0000FF;">
                                       <b>Altura Real :</b></td>
                                   <td style="width: 112px">
                                       <input id="TxtAltura" type="text" /></td>
                               </tr>
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="width: 162px; text-align: right; color: #0000FF;">
                                       <b>Peso Maximo :</b></td>
                                   <td style="width: 112px">
                                       <input id="TxtPesoMax" type="text" /></td>
                               </tr>
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="width: 162px; text-align: right; color: #0000FF;">
                                       <b>Codigo Estanteria :</b></td>
                                   <td style="width: 112px">
                                       <input id="TxtCodEstanteria" type="text" /></td>
                               </tr>
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="width: 162px; text-align: right; color: #0000FF;">
                                       <b>Tipo Paleta :</b></td>
                                   <td style="width: 112px">
                            <select id="cboTipoPaleta" name="D3" style="width: 294px">
                                <option></option>
                            </select></td>
                               </tr>
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="width: 162px; text-align: right; color: #0000FF;">
                                       <b>Permite CrossDocking :</b></td>
                                   <td style="width: 112px">
                                       <input id="chkCrossDocking" type="checkbox" /></td>
                               </tr>
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="width: 162px; text-align: right; color: #0000FF;">
                                       <b>Permite Picking :</b></td>
                                   <td style="width: 112px">
                                       <b>
                                       <input id="ChkPicking" type="checkbox" /></b></td>
                               </tr>
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="width: 162px; text-align: right; color: #0000FF;">
                                       <b>Piso :</b></td>
                                   <td style="width: 112px">
                                       <input id="TxtPiso" type="text" /></td>
                               </tr>
                               <tr>
                                   <td style="width: 16px">
                                       &nbsp;</td>
                                   <td style="width: 162px; text-align: right; color: #0000FF;">
                                       <b>Orden recorrido :</b></td>
                                   <td style="width: 112px">
                                       <input id="TxtOrdenRecorrido" type="text" /></td>
                               </tr>
                               <tr>
                                   <td colspan="4">
                                       <table class="style1">
                                           <tr>
                                               <td style="text-align: right; width: 210px; color: #0000FF">
                                                   <strong>&nbsp;Ancho Layout :&nbsp;</strong></td>
                                               <td style="width: 116px">
                                       <input id="TxtAnchoLayout" type="text" /></td>
                                               <td style="width: 110px; color: #0000FF">
                                                   <strong>Altura Layout :</strong></td>
                                               <td>
                                       <input id="TxtAlturaLayout" type="text" /></td>
                                               <td>
                                                   &nbsp;</td>
                                               <td>
                                                   &nbsp;</td>
                                           </tr>
                                       </table>
                                   </td>
                               </tr>
                               <tr>
                                   <td colspan="4">
                                       <table cellspacing="3" class="style1">
                                           <tr>
                                               <td>
                                                   &nbsp;</td>
                                               <td style="width: 42px">
                                                   <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="110px" />
                                               </td>
                                               <td style="width: 33px">
                                                   &nbsp;</td>
                                               <td style="width: 97px">
                                                   <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="105px" />
                                               </td>
                                               <td style="width: 44px">
                                                   &nbsp;</td>
                                               <td>
                                                   &nbsp;</td>
                                               <td>
                                                   &nbsp;</td>
                                               <td>
                                                   &nbsp;</td>
                                           </tr>
                                       </table>
                                   </td>
                               </tr>
                               <tr>
                                   <td colspan="4">
                                       <table class="style1">
                                           <tr>
                                               <td>
                                                   &nbsp;</td>
                                               <td style="width: 97px">
                                                   &nbsp;</td>
                                               <td class="style2" style="width: 19px">
                                                   &nbsp;</td>
                                               <td style="width: 130px">
                                                   &nbsp;</td>
                                               <td style="width: 113px">
                                                   &nbsp;
                                                   </td>
                                               <td>
                                                   &nbsp;</td>
                                               <td>
                                                   &nbsp;</td>
                                               <td>
                                                   &nbsp;</td>
                                           </tr>
                                       </table>
                                   </td>
                               </tr>
                           </table>


                       </div>   
                    </td>
                    </tr>
                    </table>
                

</div>


<script type="text/javascript" language="javascript">
    window.onload = varios();


    function varios() {
        ConsultaComboTienda();
        ConsultaComboZonas();
        ConsultaComboEstanteria();
    }


    function ConsultaComboEstanteria() {
     
        var opcion = "llenarComboEstanteria";
        var IdEmpresa = 1;
        var datacomboEst = new FormData();

        var xhrrep = new XMLHttpRequest();

        xhrrep.open("POST", "UbicacionesLayout.aspx?flag=" + opcion + "&IdEmpresa=" + IdEmpresa, true);

        xhrrep.onreadystatechange = function () {
            if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                mostrarListasComboEstanteria(xhrrep.responseText);
            }
        }
        xhrrep.send(datacomboEst);
        return false;
    }

    function ConsultaComboTienda() {

        var opcion = "llenarComboTienda";
        var IdEmpresa = 1;
        var datacombo = new FormData();
        datacombo.append('flag', 'llenarComboTienda');
        datacombo.append('IdEmpresa', IdEmpresa);
        var xhrrep = new XMLHttpRequest();

        xhrrep.open("POST", "UbicacionesLayout.aspx?flag=" + opcion + "&IdEmpresa=" + IdEmpresa, true);

        xhrrep.onreadystatechange = function () {
            if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                mostrarListasCombos(xhrrep.responseText);
            }
        }
        xhrrep.send(datacombo);
        return false;

    }

    function ConsultaComboZonas() {

        var opcion = "llenarComboZonas";
        var IdTienda = 4;
        var datacombo = new FormData();
        datacombo.append('flag', 'llenarComboZonas');
        datacombo.append('IdTienda', IdTienda);
        var xhrrep = new XMLHttpRequest();

        xhrrep.open("POST", "UbicacionesLayout.aspx?flag=" + opcion + "&IdTienda=" + IdTienda, true);
        xhrrep.onreadystatechange = function () {
            if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                mostrarListasCombosZonas(xhrrep.responseText);
            }
        }
        xhrrep.send(datacombo);
        return false;

    }

    function mostrarListasCombosZonas(rptacombozonas) {
        if (rptacombozonas != "") {
            var listas3 = rptacombozonas;
            data3 = crearArray(listas3, "▼");
            listarcombosZonas();        
        }
      
        return false;
    }

//    listarcombosZonas


    function mostrarListasComboEstanteria(rptacomboEst) {
        if (rptacomboEst != "") {
            var listas4 = rptacomboEst;
            data4 = crearArray(listas4, "▼");
            listarcombosest();
        }

        return false;
    }


    function mostrarListasCombos(rptacombo) {
        if (rptacombo != "") {
            var listas2 = rptacombo;
            data1 = crearArray(listas2, "▼");
            listarcombos();
        }
        return false;
    }

    function listarcombosZonas() {
        var Tipozonas = [];
        var nRegistros = data3.length;
        var camposzon;
        var Tipozon;
        for (var i = 0; i < nRegistros; i++) {
            camposzon = data3[i].split("|");
            {
                Tipozon = camposzon[0];
                Tipozon += "|";
                Tipozon += camposzon[1];
                Tipozonas.push(Tipozonas);
            }
        }
        crearCombo(data3, "cboZona", "|");
    }



    function listarcombosest() {
        var Tipozonas = [];
        var nRegistros = data4.length;
        var camposzon;
        var Tipozon;
        for (var i = 0; i < nRegistros; i++) {
            camposzon = data4[i].split("|");
            {
                Tipozon = camposzon[0];
                Tipozon += "|";
                Tipozon += camposzon[1];
                Tipozonas.push(Tipozonas);
            }
        }
        crearCombo(data4, "cboEstanteria", "|");
    }

    function listarcombos()
     {
        var TipoServicio = [];
        var nRegistros = data1.length;
        var campos2;
        var TipoServ;
        for (var i = 0; i < nRegistros; i++) {
            campos2 = data1[i].split("|");
            {
                TipoServ = campos2[0];
                TipoServ += "|";
                TipoServ += campos2[1];
                TipoServicio.push(TipoServicio);
            }
        }
        crearCombo(data1, "cboTienda", "|");
    }


     function crearArray(lista, separador) {
         var data = lista.split(separador);
         if (data.length === 1 && data[0] === "") data = [];
         return data;
     }
 
     function crearCombo(lista, nombreCombo, separador, nombreItem, valorItem) {
         var contenido = "";
         if (nombreItem != null && valorItem != null) {
             contenido += "<option value='";
             contenido += valorItem;
             contenido += "'>";
             contenido += nombreItem;
             contenido += "</option>";
         }
         var nRegistros = lista.length;
         if (nRegistros > 0) {
             var campos;
             for (var i = 0; i < nRegistros; i++) {
                 campos = lista[i].split(separador);
                 contenido += "<option value='";
                 contenido += campos[0];
                 contenido += "'>";
                 contenido += campos[1];
                 contenido += "</option>";
             }
         }
         var cbo = document.getElementById(nombreCombo);
         if (cbo != null) cbo.innerHTML = contenido;

     }

    function btnConsultar_onclick(objeto) {
        ConsultarRegistros();
    }


    function ConsultarRegistros(objeto, flag) {

        var opcion = "ConsultaRegistros";
        var IdTienda = document.getElementById("cboTienda").value;
        var IdZona = document.getElementById("cboZona").value;
        var xhr = new XMLHttpRequest();

        xhr.open("POST", "UbicacionesLayout.aspx?flag=" + opcion + "&IdTienda=" + IdTienda + "&IdZona=" + IdZona, true);

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
             
                fun_listaUbicaciones(xhr.responseText);
            
            }
        }
        xhr.send(null);
        return false;

    }


    function fun_listaUbicaciones(lista) {

        filas = lista.split("▼");
        crearTabla();
        crearMatriz();
        mostrarMatriz();

    }


    function crearTabla() {
        var nRegistros = filas.length;
        var cabeceras = ["", "IdUbicacion", "IdTipoEstanteria","IdZona","NroColumna","NroFila","FondoD","AnchoL","AlturaH","PesoMax","CodEstanteria","IdTipoPaleta","crossdocking","picking","Piso","OrdenRecorrido","Ancho Layout","Alt Layout" ];
        var nCabeceras = cabeceras.length;
        var contenido = "<table><thead>";
        contenido += "<tr class='GrillaHeader'>";
        for (var j = 0; j < nCabeceras; j++) {
            if (j == 0) {
                contenido += "<th>";
                contenido += "<input id='btnNuevo' type='image' img src='../../ImagenesForm/imgnuevo.png' onclick='return NuevoUbicacionesLayout()' >";
                contenido += "</th>";
            }
            if (j > 0) {
                contenido += "<th>";
                contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
                contenido += "</th>";
            }

        }
        contenido += "</tr></thead><tbody id='tbDocumentos' class='GrillaRow'></tbody>";
        contenido += "</table>";
        var div = document.getElementById("CapaLayout");
        div.innerHTML = contenido;


    }


    function crearMatriz() {
        matriz = [];
        var nRegistros = filas.length;
        var nCampos;
        var campos;
        var c = 0;
        var exito;
        var textos = document.getElementsByClassName("texto");
        var nTextos = textos.length;
        var texto;
        var x;
        for (var i = 0; i < nRegistros; i++) {
            exito = true;
            campos = filas[i].split("|");
            nCampos = campos.length;

            for (var j = 0; j < nTextos; j++) {
                texto = textos[j];
                x = j;

                if (x < 10) {
                    if (texto.value.length > 0) {
                        if (campos[x] != campos[0]) {
                            exito = campos[x - 1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        }
                    }
                }

                if (x == 10 && x < 15) {
                    if (texto.value.length > 0) {
                        if (campos[x] != campos[0]) {
                            exito = campos[x - 1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        }
                    }
                }

                if (!exito) break;
            }

            if (exito) {
                matriz[c] = [];
                for (var j = 0; j < nCampos; j++) {
                    if (isNaN(campos[j])) matriz[c][j] = campos[j];
                    else matriz[c][j] = campos[j];
                }
                c++;
            }


        }
    }


    function mostrarMatriz() {
        var nRegistros = matriz.length;
        var contenido = "";
        if (nRegistros > 0) {
            var nCampos = matriz[0].length;
            for (var i = 0; i < nRegistros; i++) {

                for (var j = 0; j < nCampos; j++) {

                    if (j == 0) {
                        contenido += "<td>";
                        contenido += "<input id='btnmas' type='image' img src='../../ImagenesForm/imgeditar.png' onclick='return Mostrardetalle(";
                        contenido += matriz[i][0] + '';
                        contenido += "," + matriz[i][1] +''; 
                        contenido += "," + matriz[i][2] +'';
                        contenido += "," + matriz[i][3] +'';
                        contenido += "," + matriz[i][4] +'';
                        contenido += "," + matriz[i][5] +'';
                        contenido += "," + matriz[i][6] +'';
                        contenido += "," + matriz[i][7] +'';
                        contenido += "," + matriz[i][8] +'';
                        contenido += "," + '"' + matriz[i][9] + '"';
                        contenido += "," + '"' + matriz[i][10] + '"';
                        contenido += "," + '"' + matriz[i][11] + '"';
                        contenido += "," + '"' + matriz[i][12] + '"';
                        contenido += "," + '"' + matriz[i][13] + '"';
                        contenido += "," + '"' + matriz[i][14] + '"';
                        contenido += "," + '"' + matriz[i][15] + '"';
                        contenido += "," + '"' + matriz[i][16] + '"';
                        contenido += ")' >";
                        contenido += "</td>";
                        contenido += "<td>";
                        contenido += matriz[i][j];
                        contenido += "</td>";
                    }


                    if (j > 0) {
                        contenido += "<td>";
                        contenido += matriz[i][j];
                        contenido += "</td>";
                    }
                }
                contenido += "</tr>";
            }
        }
        var spnMensaje = document.getElementById("nroFilas");
        var tabla = document.getElementById("tbDocumentos");
        tabla.innerHTML = contenido;
    }


    function NuevoUbicacionesLayout() {

        document.getElementById('CapaMantenimiento').style.display = 'block';
 
        document.getElementById('TxtIdUbicacion').value = "";
        document.getElementById('cboEstanteria').value = "";
        document.getElementById('cboZonaAlmacenamiento').value = "";
        document.getElementById('TxtNroColumna').value = "";
        document.getElementById('TxtNroFila').value = "";
        document.getElementById('TxtFondo').value = "";
        document.getElementById('TxtAncho').value = "";
        document.getElementById('TxtAltura').value = "";
        document.getElementById('TxtPesoMax').value = "";
        document.getElementById('TxtCodEstanteria').value = "";
        document.getElementById('cboTipoPaleta').value = "";
        document.getElementById('chkCrossDocking').checked = false;
        document.getElementById('ChkPicking').checked = false;
        document.getElementById('TxtPiso').value = "";
        document.getElementById('TxtOrdenRecorrido').value = "";
        document.getElementById('TxtAnchoLayout').value = "";
        document.getElementById('TxtAlturaLayout').value = "";

        return false;

    }

    function Mostrardetalle(IdUbicacion, IdTipoEstanteria, IdZona, NroColumna, NroFila, FondoD, AnchoL, AlturaH, PesoMax, CodEstanteria, IdTipoPaleta, crossdocking, picking, Piso, OrdenRecorrido,medancho,medAltura)
     {

        document.getElementById('CapaMantenimiento').style.display = 'block';
        document.getElementById('TxtIdUbicacion').value = IdUbicacion;
        document.getElementById('cboEstanteria').value = IdTipoEstanteria;
        document.getElementById('cboZonaAlmacenamiento').value = IdZona;
        document.getElementById('TxtNroColumna').value = NroColumna;
        document.getElementById('TxtNroFila').value = NroFila;
        document.getElementById('TxtFondo').value = FondoD;
        document.getElementById('TxtAncho').value = AnchoL;
        document.getElementById('TxtAltura').value = AlturaH;
        document.getElementById('TxtPesoMax').value = PesoMax;
        document.getElementById('TxtCodEstanteria').value = CodEstanteria;
        document.getElementById('cboTipoPaleta').value = IdTipoPaleta;
        document.getElementById('chkCrossDocking').checked = crossdocking;
        document.getElementById('ChkPicking').checked = picking;
        document.getElementById('TxtPiso').value = Piso;
        document.getElementById('TxtOrdenRecorrido').value = OrdenRecorrido;
        document.getElementById('TxtAnchoLayout').value = medancho;
        document.getElementById('TxtAlturaLayout').value = medAltura;

        return false;

    }


    function cerrardetalle() {
        document.getElementById('CapaMantenimiento').style.display = 'none';
        ConsultarRegistros();
        return false;
    }



</script>
 
</asp:Content>

