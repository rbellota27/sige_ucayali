﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="ZonaAlmacenamiento.aspx.vb" Inherits="APPWEB.ZonaAlmacenamiento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div>
    <table class="style1">
        <tr>
            <td style="width: 840px">
                <table class="style1">
                    <tr>
                        <td style="text-align: center; color: #0000FF" colspan="8">
                            Zonas de Almacenamiento</td>
                    </tr>
                    <tr>
                        <td style="width: 173px; text-align: right; color: #0000FF">
                            <strong>Tienda :</strong></td>
                        <td style="width: 150px">
                            <select id="cboTienda" name="D1" style="width: 294px">
                                <option></option>
                            </select></td>
                        <td style="width: 33px">
                            &nbsp;</td>
                        <td style="width: 135px">
                            <input id="btnConsultar" type="button" value="Consultar"  onclick="return btnConsultar_onclick(this)" /></td>
                        </td>
                        <td>                        
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 840px">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 840px">
        
              <div id="CapaZonaAlmacenamiento"></div></td>

            </div>
            </td>
        </tr>
    </table>
</div>

<div id="CapaMantenimiento" style="border: 3px solid blue; padding: 4px; width: 650px;
            height: 267px; position: absolute; top: 0px; left: 350px; background-color: #EFFBFB;
            z-index: 3; display:none;">
                   <table border="0" cellspacing="0px" style="width: 650px">
                   <tr>
                    <td>
                          <table border="0">
                                        <tr class="BarraTitulo">
                                        <td style="width:90%; background-color: #0066CC;"><span id="TituloPopup"></span></td>
                                        <td style="width:4%"><img src='../../ImagenesForm/imgcerrar.png' class="Iconoventanas" 
                                                title="Cerrar Ventana" onclick="return cerrardetalle()"/></td>
                                        </tr>
                          </table>
                    </td>
                    </tr>
                    <tr>
                    <td>
                       <div id="TablaMantenimiento">


                           <table class="style1">
                               <tr>
                                   <td style="width: 86px">
                                       &nbsp;</td>
                                   <td style="text-align: right; color: #0000FF; width: 222px; background-color: #FFFFFF">
                                       &nbsp;</td>
                                   <td style="width: 312px">
                                       &nbsp;</td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 86px">
                                       &nbsp;</td>
                                   <td style="text-align: right; color: #0000FF; width: 222px; background-color: #FFFFFF">
                                       <b>Id :</b></td>
                                   <td style="width: 312px">
                                       <input id="txtIdZonaAlmacenamiento" type="text" style="width: 83px"  readonly/></td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 86px">
                                       &nbsp;</td>
                                   <td style="text-align: right; color: #0000FF; width: 222px; background-color: #FFFFFF">
                                       <b>Nombre Zona Almacenamiento :</b></td>
                                   <td style="width: 312px">
                                       <input id="txtNombreZona" style="width: 386px" type="text" /></td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 86px">
                                       &nbsp;</td>
                                   <td style="text-align: right; color: #0000FF; width: 222px; background-color: #FFFFFF">
                                       <b>Tienda :</b></td>
                                   <td style="width: 312px">
                            <select id="cboTiendaMant" name="D2" style="width: 294px">
                                <option></option>
                            </select></td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td style="width: 86px">
                                       &nbsp;</td>
                                   <td style="width: 222px">
                                       &nbsp;</td>
                                   <td style="width: 312px">
                                       &nbsp;</td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td colspan="4">
                                       <table class="style1">
                                           <tr>
                                               <td>
                                                   &nbsp;</td>
                                               <td style="width: 97px">
                                                   <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="110px" />
                                               </td>
                                               <td class="style2" style="width: 19px">
                                                   &nbsp;</td>
                                               <td style="width: 130px">
                                                   <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" Width="110px" />
                                               </td>
                                               <td style="width: 113px">
                                                   &nbsp;
                                                   <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="105px" />
                                               </td>
                                               <td>
                                                   &nbsp;</td>
                                               <td>
                                                   &nbsp;</td>
                                               <td>
                                                   &nbsp;</td>
                                           </tr>
                                       </table>
                                   </td>
                               </tr>
                           </table>


                       </div>   
                    </td>
                    </tr>
                    </table>
                

</div>

<%--CapaMantenimiento--%>

<script type="text/javascript" language="javascript">

    window.onload = varios();


    function varios() {
        ConsultaComboTienda();

    }


    function btnConsultar_onclick(objeto) 
    {


        ConsultarRegistros(objeto)
    }

    
    function cerrardetalle() {
        document.getElementById('CapaMantenimiento').style.display = 'none';
        ConsultarRegistros();
        return false;
    }


    function ConsultaComboTienda() {

        var opcion = "llenarComboTienda";
        var IdEmpresa = 1;
        var datacombo = new FormData();
        datacombo.append('flag', 'llenarComboTienda');
        datacombo.append('IdEmpresa', IdEmpresa);
        var xhrrep = new XMLHttpRequest();

        xhrrep.open("POST", "ZonaAlmacenamiento.aspx?flag=" + opcion + "&IdEmpresa=" + IdEmpresa, true);
        
        xhrrep.onreadystatechange = function () {
            if (xhrrep.readyState == 4 && xhrrep.status == 200) {
                mostrarListasCombos(xhrrep.responseText);
            }
        }

        xhrrep.send(datacombo);
        return false;
    }


        function ConsultarRegistros(objeto, flag) {
        
        var opcion = "ConsultaRegistros";
        var IdTienda = document.getElementById("cboTienda").value;

        var xhr = new XMLHttpRequest();

        xhr.open("POST", "ZonaAlmacenamiento.aspx?flag=" + opcion + "&IdTienda=" + IdTienda, true);
 
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                fun_listaZonaAlmacenamiento(xhr.responseText);
            }
        }
        xhr.send(null);
        return false;
        }

    function fun_listaZonaAlmacenamiento(lista) {
       
        filas = lista.split("▼");
        crearTabla();
        crearMatriz();
        mostrarMatriz();

    }



    function crearTabla() {
        var nRegistros = filas.length;

        var cabeceras = ["", "IdZonaAlmacenamiento", "NombreZonaAlmacenamiento","Tienda","IdTienda"];
        var nCabeceras = cabeceras.length;
        var contenido = "<table><thead>";

        contenido += "<tr class='GrillaHeader'>";


        for (var j = 0; j < nCabeceras; j++) {

            if (j == 0) {
                contenido += "<th>";
                contenido += "<input id='btnNuevo' type='image' img src='../../ImagenesForm/imgnuevo.png' onclick='return NuevaZonaAlmacenamiento()' >";
                contenido += "</th>";
            }
            if (j > 0) {
                contenido += "<th>";
                contenido += "<input type='text' class='texto' style='width:95%' placeholder='" + cabeceras[j] + "'/>";
                contenido += "</th>";

            }
           
        }
        contenido += "</tr></thead><tbody id='tbDocumentos' class='GrillaRow'></tbody>";
        contenido += "</table>";
        var div = document.getElementById("CapaZonaAlmacenamiento");
        div.innerHTML = contenido;

    }



    function mostrarMatriz() {
        var nRegistros = matriz.length;
        var contenido = "";
        if (nRegistros > 0) {
            var nCampos = matriz[0].length;
            for (var i = 0; i < nRegistros; i++) {

                for (var j = 0; j < nCampos ; j++) {
                   
                    if (j == 0) {
                        contenido += "<td>";
                        contenido += "<input id='btnmas' type='image' img src='../../ImagenesForm/imgeditar.png' onclick='return MostrarDetalle(";
                        contenido += matriz[i][0];
                        contenido += ',"' + matriz[i][1] + '"';
                        contenido += "," + '"' + matriz[i][2] + '"';
                        contenido += "," + '"' + matriz[i][3] + '"';
                        contenido += ")' >";
                        contenido += "</td>";
                        contenido += "<td>";
                        contenido += matriz[i][j];
                        contenido += "</td>";
                    }

                    if (j > 0) {
                        contenido += "<td>";
                        contenido += matriz[i][j];
                        contenido += "</td>";
                    }
                }
                contenido += "</tr>";
            }
        }
        var spnMensaje = document.getElementById("nroFilas");
        var tabla = document.getElementById("tbDocumentos");
        tabla.innerHTML = contenido;
    }


    function MostrarDetalle(IdZonaAlmacenamiento, NombreZona, Tienda, IdTienda) {

        document.getElementById('CapaMantenimiento').style.display = 'block';
        document.getElementById('txtIdZonaAlmacenamiento').value = IdZonaAlmacenamiento;
        document.getElementById('txtNombreZona').value = NombreZona;

      if (IdTienda > 0) {
            document.getElementById('cboTiendaMant').value = IdTienda;
        }
    
        return false;

    }

    function NuevaZonaAlmacenamiento() {

        document.getElementById('CapaMantenimiento').style.display = 'block';
        document.getElementById('txtIdZonaAlmacenamiento').value = "0";
        document.getElementById('txtNombreZona').value = "";
        document.getElementById('cboTiendaMant').value = 4;
        
        return false;

    }


    function crearMatriz() {
        matriz = [];
        var nRegistros = filas.length;
        var nCampos;
        var campos;
        var c = 0;
        var exito;
        var textos = document.getElementsByClassName("texto");
        var nTextos = textos.length;
        var texto;
        var x;
        for (var i = 0; i < nRegistros; i++) {
            exito = true;
            campos = filas[i].split("|");
            nCampos = campos.length;

            for (var j = 0; j < nTextos; j++) {
                texto = textos[j];
                x = j;

                if (x < 10) {
                    if (texto.value.length > 0) {
                        if (campos[x] != campos[0]) {
                            exito = campos[x - 1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        }
                    }
                }

                if (x == 10 && x < 13) {
                    if (texto.value.length > 0) {
                        if (campos[x] != campos[0]) {
                            exito = campos[x - 1].toLowerCase().indexOf(texto.value.toLowerCase()) > -1;
                        }
                    }
                }

                if (!exito) break;
            }

            if (exito) {
                matriz[c] = [];
                for (var j = 0; j < nCampos; j++) {
                    if (isNaN(campos[j])) matriz[c][j] = campos[j];
                    else matriz[c][j] = campos[j];
                }
                c++;
            }


        }
    }


    function mostrarListasCombos(rptacombo) {
        if (rptacombo != "") {
            var listas2 = rptacombo;
            data1 = crearArray(listas2, "▼");
            listarcombos();
        }
        return false;
    }

    function listarcombos() {
        var TipoServicio = [];
        var nRegistros = data1.length;
        var campos2;
        var TipoServ;
        for (var i = 0; i < nRegistros; i++) {
            campos2 = data1[i].split("|");
            {
                TipoServ = campos2[0];
                TipoServ += "|";
                TipoServ += campos2[1];
                TipoServicio.push(TipoServicio);
            }
        }
        crearCombo(data1, "cboTienda", "|");
        crearCombo(data1, "cboTiendaMant", "|");
        
    }

    function crearCombo(lista, nombreCombo, separador, nombreItem, valorItem) {
        var contenido = "";
        if (nombreItem != null && valorItem != null) {
            contenido += "<option value='";
            contenido += valorItem;
            contenido += "'>";
            contenido += nombreItem;
            contenido += "</option>";
        }
        var nRegistros = lista.length;
        if (nRegistros > 0) {
            var campos;
            for (var i = 0; i < nRegistros; i++) {
                campos = lista[i].split(separador);
                contenido += "<option value='";
                contenido += campos[0];
                contenido += "'>";
                contenido += campos[1];
                contenido += "</option>";
            }
        }
        var cbo = document.getElementById(nombreCombo);
        if (cbo != null) cbo.innerHTML = contenido;

    }
    function crearArray(lista, separador) {
        var data = lista.split(separador);
        if (data.length === 1 && data[0] === "") data = [];
        return data;
    }

</script>
 
</asp:Content>
