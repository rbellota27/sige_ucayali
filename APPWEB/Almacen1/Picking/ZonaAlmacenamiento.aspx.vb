﻿Imports Entidades
Imports Negocio
Imports general.librerias.serializador

Public Class ZonaAlmacenamiento
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass

    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim Usuario As Integer = CInt(Session("IdUsuario"))
        Dim nroVoucher As String = String.Empty
        Dim flag As String = Request.QueryString("flag")




        Select Case flag
            Case "ConsultaRegistros"
                Dim IdTienda As String = Request.QueryString("IdTienda")
                Dim lista As New List(Of beZonaAlmacenamiento)
                Dim rptaZonaAlmacen As String = ""
                Try
                    lista = (New blZonaAlmacenamiento).listarZonaAlmacenamiento(IdTienda)

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rptaZonaAlmacen = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(rptaZonaAlmacen)
                    Response.End()
                Else
                    Response.Write(rptaZonaAlmacen)
                    Response.End()
                End If


            Case "llenarComboTienda"
                Dim IdEmpresa As String = Request.QueryString("IdEmpresa")
                Dim rpta As String = ""


                Dim lista As New List(Of be_Tienda)
                Try

                    lista = (New bl_Tienda).selectTiendasN(Convert.ToInt32(IdEmpresa))
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rpta = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try
                    Response.Write(rpta)
                    Response.End()
                Else
                    Response.Write(rpta)
                    Response.End()
                End If


        End Select


    End Sub


         

End Class