﻿Imports Entidades
Imports Negocio
Imports general.librerias.serializador

Public Class TipoEstanteria
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass

    Public ReadOnly Property SIdUsuario() As Integer
        Get
            Return CType(Session.Item("IdUsuario"), Integer)
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim Usuario As Integer = CInt(Session("IdUsuario"))
        Dim flag As String = Request.QueryString("flag")


        Select Case flag

            Case "ConsultaRegistrosTipoEstanteria"

                Dim lista As New List(Of beTipoEstanteria)
                Dim rptaTipoEstanteria As String = ""
                Try
                    lista = (New blTipoEstanteria).listarTipoEstanteria()

                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try

                If lista.Count > 0 Then
                    Try
                        rptaTipoEstanteria = (New objeto).SerializarLista(lista, "|", "▼", False, "", False)
                    Catch ex As Exception
                        objScript.mostrarMsjAlerta(Me, ex.Message)
                    End Try

                    Response.Write(rptaTipoEstanteria)
                    Response.End()
                Else
                    Response.Write(rptaTipoEstanteria)
                    Response.End()
                End If


        End Select

    End Sub

End Class