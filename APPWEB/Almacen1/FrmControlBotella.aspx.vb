﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmControlBotella
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private objCbo As New Combo
    Private listaBotella As List(Of Entidades.Botella)
    Private Const _IdMagnitud_Volumen As Integer = 2

    Private listaProductoView As List(Of Entidades.ProductoView)
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        valOnLoad_Frm()

    End Sub

    Private Sub valOnLoad_Frm()
        Try

            If (Not Me.IsPostBack) Then
                inicializarFrm()
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub inicializarFrm()

        Me.txtFechaIngreso_1.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaMov_2.Text = Me.txtFechaIngreso_1.Text
        Me.txtFechaMov_3.Text = Me.txtFechaIngreso_1.Text

        With objCbo

            .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)

            '.LlenarCboLinea(Me.cmbLinea_AddProd, True)
            '.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            '.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
            '.LlenarCboLinea(Me.cmbLinea_AddProd, True)
            .llenarCboTipoExistencia(CboTipoExistencia, False)
            .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

        End With

        '************** COMBO CONTENIDO
        Me.cboContenido_3.DataSource = (New Negocio.Botella).Select_CboContenido()
        Me.cboContenido_3.DataBind()
        Me.cboContenido_3.Items.Insert(0, New ListItem("-----", "0"))

        verFrm(False, True, True)

    End Sub

    Private Sub verFrm(ByVal limpiar As Boolean, ByVal initSession As Boolean, ByVal addBotella_Default As Boolean)

        If (limpiar) Then
            limpiarFrm()
        End If

        If (initSession) Then

            Me.listaBotella = New List(Of Entidades.Botella)
            setListaBotella(Me.listaBotella)

            If (addBotella_Default) Then
                valOnClick_btnAgregar_1()
            End If


        End If

    End Sub
    Private Sub limpiarFrm()

        '************************ TAB 1
        Me.txtPropietario_1.Text = ""
        Me.txtNroDocumento_1.Text = ""

        If (Me.cboTipoDocumento_1.Items.FindByValue("0") IsNot Nothing) Then
            Me.cboTipoDocumento_1.SelectedValue = "0"
        End If

        Me.txtFechaIngreso_1.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.GV_Envase_1.DataSource = Nothing
        Me.GV_Envase_1.DataBind()

        Me.hddIdPersona_1.Value = ""
        Me.hddIndexGrilla_1.Value = ""
        Me.hddOpcion_BuscarPersona.Value = ""

        '************************* TAB 2
        Me.txtFechaMov_2.Text = Me.txtFechaIngreso_1.Text
        Me.txtNroDocumento_2.Text = ""
        Me.txtCliente_2.Text = ""
        Me.txtNroBotella_2.Text = ""
        Me.hddIdCliente_2.Value = ""
        Me.GV_Botella_2.DataSource = Nothing
        Me.GV_Botella_2.DataBind()

        '****************** TAB 3
        Me.txtDestinatario_3.Text = ""
        If (Me.cboTipoDocumento_3.Items.FindByValue(CStr(0)) IsNot Nothing) Then
            Me.cboTipoDocumento_3.SelectedValue = CStr(0)
        End If
        Me.txtNroDocumento_3.Text = ""
        Me.txtFechaMov_3.Text = Me.txtFechaIngreso_1.Text
        If (Me.cboContenido_3.Items.FindByValue(CStr(0)) IsNot Nothing) Then
            Me.cboContenido_3.SelectedValue = CStr(0)
        End If
        Me.GV_Botella_3.DataSource = Nothing
        Me.GV_Botella_3.DataBind()
        Me.hddIdDestinatario_3.Value = ""

    End Sub

    Private Function getListaBotella() As List(Of Entidades.Botella)

        Return CType(Session("listaBotella"), List(Of Entidades.Botella))

    End Function
    Private Sub setListaBotella(ByVal lista As List(Of Entidades.Botella))

        Session.Remove("listaBotella")
        Session.Add("listaBotella", lista)

    End Sub

#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)

        listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub

    Private Sub btBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btBuscarPersonaGrilla.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", 0)  '*** TODOS LOS ROLES

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")

        Dim objCbo As New Combo

        Select Case CInt(Me.hddOpcion_BuscarPersona.Value)

            Case 0  '****** PROPIETARIO TAB 1

                Me.txtPropietario_1.Text = objPersona.Descripcion
                Me.hddIdPersona_1.Value = CStr(objPersona.IdPersona)

            Case 1  '**************** CLIENTE TAB 2

                Me.txtCliente_2.Text = objPersona.Descripcion
                Me.hddIdCliente_2.Value = CStr(objPersona.IdPersona)

            Case 2 '****************  DESTINATARIO TAB 3

                Me.txtDestinatario_3.Text = objPersona.Descripcion
                Me.hddIdDestinatario_3.Value = CStr(objPersona.IdPersona)

        End Select

        objScript.offCapa(Me, "capaPersona")

    End Sub

#End Region

    Protected Sub btnAgregar_1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAgregar_1.Click
        valOnClick_btnAgregar_1()
    End Sub
    Private Sub valOnClick_btnAgregar_1()

        Try

            actualizarListaBotella()

            Me.listaBotella = getListaBotella()

            Dim objBotella As New Entidades.Botella
            With objBotella

                .ListaMagnitudUnidad = (New Negocio.MagnitudUnidad)._MagnitudUnidadselectxIdMagnitud(_IdMagnitud_Volumen)

            End With

            Me.listaBotella.Add(objBotella)
            setListaBotella(Me.listaBotella)

            Me.GV_Envase_1.DataSource = Me.listaBotella
            Me.GV_Envase_1.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub actualizarListaBotella()

        Me.listaBotella = getListaBotella()
        For i As Integer = 0 To Me.GV_Envase_1.Rows.Count - 1

            Me.listaBotella(i).NroBotella = CType(Me.GV_Envase_1.Rows(i).FindControl("txtNroBotella"), TextBox).Text
            Me.listaBotella(i).Observacion = CType(Me.GV_Envase_1.Rows(i).FindControl("txtObservacion"), TextBox).Text
            Me.listaBotella(i).Capacidad = CDec(CType(Me.GV_Envase_1.Rows(i).FindControl("txtCapacidad"), TextBox).Text)
            Me.listaBotella(i).IdUnidadMedida_Capacidad = CInt(CType(Me.GV_Envase_1.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)

        Next
        setListaBotella(Me.listaBotella)

    End Sub

    Private Sub GV_Envase_1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Envase_1.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim cboUnidadMedida As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)

            If (cboUnidadMedida.Items.FindByValue(CStr(Me.listaBotella(e.Row.RowIndex).IdUnidadMedida_Capacidad)) IsNot Nothing) Then
                cboUnidadMedida.SelectedValue = CStr(Me.listaBotella(e.Row.RowIndex).IdUnidadMedida_Capacidad)
            End If

        End If

    End Sub
    Private Sub GV_Envase_1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Envase_1.SelectedIndexChanged
        quitarElemento_ListaBotella(Me.GV_Envase_1.SelectedIndex)
    End Sub
    Private Sub quitarElemento_ListaBotella(ByVal index As Integer)
        Try

            actualizarListaBotella()
            Me.listaBotella = getListaBotella()
            Me.listaBotella.RemoveAt(index)
            setListaBotella(Me.listaBotella)

            Me.GV_Envase_1.DataSource = Me.listaBotella
            Me.GV_Envase_1.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#Region "************************** BUSQUEDA PRODUCTO"
    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
    End Sub

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", -1)
                    ViewState.Add("IdEmpresa_BuscarProducto", -1)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia", Me.CboTipoExistencia.SelectedValue)
            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                          ByVal codigoSubLinea As String, ByVal nomProducto As String, _
                                          ByVal IdAlmacen As Integer, ByVal tipoMov As Integer, _
                                          ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        'Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)


        If listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            -1, 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)


            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region

    Private Sub DGV_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_AddProd.SelectedIndexChanged

        agregarContenido(Me.DGV_AddProd.SelectedIndex)

    End Sub
    Private Sub agregarContenido(ByVal index As Integer)

        Try

            Dim IdProducto As Integer = CInt(CType(Me.DGV_AddProd.Rows(index).FindControl("hddIdProducto"), HiddenField).Value)
            Dim descripcion As String = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(index).Cells(2).Text))

            Dim indexListaBotella As Integer = CInt(Me.hddIndexGrilla_1.Value)

            actualizarListaBotella()
            Me.listaBotella = getListaBotella()
            Me.listaBotella(indexListaBotella).IdContenido = IdProducto
            Me.listaBotella(indexListaBotella).Contenido = descripcion
            setListaBotella(Me.listaBotella)

            Me.GV_Envase_1.DataSource = Me.listaBotella
            Me.GV_Envase_1.DataBind()

            objScript.offCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnGuardar_1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar_1.Click
        registrar_IngresoBotella()
    End Sub
    Private Sub registrar_IngresoBotella()
        Try

            Me.listaBotella = obtenerListaBotella_Save()
            Dim listaMovBotella As List(Of Entidades.MovBotella) = obtenerListaMovBotella_Save()

            If ((New Negocio.Botella).registrarIngresoBotella(Me.listaBotella, listaMovBotella)) Then
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito")
                verFrm(True, True, True)
            Else
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN")
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaMovBotella_Save() As List(Of Entidades.MovBotella)

        Dim listaMovBotella As New List(Of Entidades.MovBotella)
        Me.listaBotella = getListaBotella()

        For i As Integer = 0 To Me.listaBotella.Count - 1

            Dim obj As New Entidades.MovBotella
            With obj

                .Estado = True
                .FechaMov = CDate(Me.txtFechaIngreso_1.Text)
                .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                .IdTienda = CInt(Me.cboTienda.SelectedValue)
                .IdTipoDocumento = CInt(Me.cboTipoDocumento_1.SelectedValue)
                .IdUbicacion = CInt(Me.cboEmpresa.SelectedValue)
                .IdUsuarioInsert = CInt(Session("IdUsuario"))
                .IdUsuarioUpdate = CInt(Session("IdUsuario"))
                .NroDocumento = CStr(Me.txtNroDocumento_1.Text)
                .TipoMov = "E"
                .Vigencia = CInt(Me.txtVigencia_1.Text)
                .Observacion = Me.listaBotella(i).Observacion

            End With

            listaMovBotella.Add(obj)

        Next

        Return listaMovBotella

    End Function
    Private Function obtenerListaBotella_Save() As List(Of Entidades.Botella)

        actualizarListaBotella()
        Me.listaBotella = getListaBotella()

        For i As Integer = 0 To Me.listaBotella.Count - 1
            With Me.listaBotella(i)

                .Estado = True
                .IdPropietario = CInt(Me.hddIdPersona_1.Value)
                .IdUbicacion = CInt(Me.cboEmpresa.SelectedValue)
                .IdTienda = CInt(Me.cboTienda.SelectedValue)
                .IdUsuarioInsert = CInt(Session("IdUsuario"))
                .IdUsuarioUpdate = CInt(Session("IdUsuario"))

            End With
        Next

        Return Me.listaBotella

    End Function

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        valOnChange_cboEmpresa()
    End Sub
    Private Sub valOnChange_cboEmpresa()
        Try

            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnLimpiar_2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLimpiar_2.Click
        valOnClick_btnLimpiar_2()
    End Sub
    Private Sub valOnClick_btnLimpiar_2()
        Try

            Me.txtCliente_2.Text = ""
            Me.txtNroBotella_2.Text = ""
            Me.hddIdCliente_2.Value = ""

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnBuscarEnvase_2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarEnvase_2.Click
        valOnClick_btnBuscarEnvase_2()
    End Sub
    Private Sub valOnClick_btnBuscarEnvase_2()
        Try

            Dim IdCliente As Integer = 0

            If (IsNumeric(Me.hddIdCliente_2.Value) And Me.hddIdCliente_2.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdCliente_2.Value) > 0) Then
                    IdCliente = CInt(Me.hddIdCliente_2.Value)
                End If
            End If

            Me.GV_Botella_2.DataSource = (New Negocio.Botella).SelectPendientesDevolucionCliente(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdCliente, Me.txtNroBotella_2.Text.Trim)
            Me.GV_Botella_2.DataBind()

            If (Me.GV_Botella_2.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub btnGuardar_2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar_2.Click

        registrar_IngresoBotellaxDevolucion()

    End Sub
    Private Sub registrar_IngresoBotellaxDevolucion()
        Try

            Dim listaMovBotella As List(Of Entidades.MovBotella) = obtenerListaMovBotellaxDevolucionCliente_Save()

            If ((New Negocio.Botella).registrarIngresoBotellaxDevolucionCliente(listaMovBotella)) Then
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")
                limpiarFrm()
            Else
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaMovBotellaxDevolucionCliente_Save() As List(Of Entidades.MovBotella)

        Dim lista As New List(Of Entidades.MovBotella)

        For i As Integer = 0 To Me.GV_Botella_2.Rows.Count - 1

            If (CType(Me.GV_Botella_2.Rows(i).FindControl("chb_Estado"), CheckBox).Checked) Then

                Dim obj As New Entidades.MovBotella

                With obj

                    .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                    .IdTienda = CInt(Me.cboTienda.SelectedValue)
                    .IdUbicacion = CInt(Me.cboEmpresa.SelectedValue)
                    .FechaMov = CDate(Me.txtFechaMov_2.Text)
                    .TipoMov = "E"
                    .IdTipoDocumento = CInt(Me.cboTipoDocumento_2.SelectedValue)
                    .NroDocumento = Me.txtNroDocumento_2.Text
                    .Estado = True
                    .IdBotella = CInt(CType(Me.GV_Botella_2.Rows(i).FindControl("hddIdBotella"), HiddenField).Value)
                    .IdUsuarioInsert = CInt(Session("IdUsuario"))

                End With

                lista.Add(obj)

            End If

        Next

        Return lista

    End Function

    Protected Sub btnBuscar_3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar_3.Click

        valOnClick_btnBuscar_3()

    End Sub
    Private Sub valOnClick_btnBuscar_3()
        Try

            Me.GV_Botella_3.DataSource = (New Negocio.Botella).SelectBotellaDisponiblexParams(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboContenido_3.SelectedValue))
            Me.GV_Botella_3.DataBind()

            If (Me.GV_Botella_3.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnGuardar_3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar_3.Click

        registrarSalida_BotellaPendiente()

    End Sub
    Private Sub registrarSalida_BotellaPendiente()

        Try

            Dim listaMovBotella As List(Of Entidades.MovBotella) = obtenerListaMovBotella_Salida()

            If ((New Negocio.Botella).registrarSalida_BotellaDisponible(listaMovBotella)) Then

                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito")

            Else

                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN")

            End If

            limpiarFrm()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerListaMovBotella_Salida() As List(Of Entidades.MovBotella)
        Dim lista As New List(Of Entidades.MovBotella)

        For i As Integer = 0 To Me.GV_Botella_3.Rows.Count - 1

            If (CType(Me.GV_Botella_3.Rows(i).FindControl("chb_Estado"), CheckBox).Checked) Then

                Dim obj As New Entidades.MovBotella

                With obj

                    .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
                    .IdTienda = CInt(Me.cboTienda.SelectedValue)
                    .IdUbicacion = CInt(Me.hddIdDestinatario_3.Value)
                    .FechaMov = CDate(Me.txtFechaMov_3.Text)
                    .TipoMov = "S"
                    .IdTipoDocumento = CInt(Me.cboTipoDocumento_3.SelectedValue)
                    .NroDocumento = Me.txtNroDocumento_3.Text
                    .Estado = True
                    .IdBotella = CInt(CType(Me.GV_Botella_3.Rows(i).FindControl("hddIdBotella"), HiddenField).Value)
                    .IdUsuarioInsert = CInt(Session("IdUsuario"))

                End With

                lista.Add(obj)

            End If

        Next

        Return lista
    End Function

    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class