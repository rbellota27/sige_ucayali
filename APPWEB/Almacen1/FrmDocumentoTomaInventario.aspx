<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmDocumentoTomaInventario.aspx.vb" Inherits="APPWEB.FrmDocumentoTomaInventario" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Generar Documento" ToolTip="Nuevo"
                                OnClientClick=" return(  valSaveDocumento()  ); " Width="150px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                                Text="Editar" ToolTip="Editar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                                Text="Anular" ToolTip="Anular" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" ToolTip="Guardar" OnClientClick=" return(     valSaveDocumento()       ); "
                                Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  valOnClick_btnImprimir()  );"
                                Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnExportar" runat="server" OnClientClick="return(  valOnClick_btnExportar()  );"
                                Text="Exportar" ToolTip="Exportar Consolidado" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscarDocumentoRef" runat="server" Text="Documento Ref." OnClientClick="  return(  onCapa('capaDocumentosReferencia')  );  "
                                ToolTip="Buscar Despachos Pendientes" Width="120px" />
                        </td>
                        <td>
                            <asp:Button ID="btnInicio" runat="server" Text="Inicio" Width="80px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DOCUMENTO TOMA DE INVENTARIO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarDocumentoxCodigo">                                
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( valOnKeyPressCodigoDoc(this,event)  );"
                                    ReadOnly="true" Width="100px"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" ToolTip="Buscar Documento por [ Serie - N�mero ]." />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="B�squeda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Almac�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboAlmacen" runat="server" Width="100%" AutoPostBack="false">
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Operaci�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" runat="server" Visible="true" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Toma Inventario:
                            </td>
                            <td colspan="5">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtFechaTomaInv" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                Width="100px"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaTomaInv" runat="server" ClearMaskOnLostFocus="false"
                                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaTomaInv">
                                            </cc1:MaskedEditExtender>
                                            <cc1:CalendarExtender ID="CalendarExtender_txtFechaTomaInv" runat="server" Enabled="True"
                                                Format="dd/MM/yyyy" TargetControlID="txtFechaTomaInv">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnRecalcularSaldos" runat="server" Text="Recalcular Saldos" ToolTip="Recalcular Saldos del Sistema"
                                                OnClientClick=" return(  valOnClick_btnRecalcularSaldos()  ); " />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnInformacion" runat="server" ImageUrl="~/Imagenes/Ayuda_A.JPG" />
                                            <cc1:ModalPopupExtender ID="btnInformacion_ModalPopupExtender" runat="server" DynamicServicePath=""
                                                OkControlID="btnAceptar_Panel_Informacion" PopupControlID="Panel_Informacion"
                                                X="280" BackgroundCssClass="modalBackground" Y="150" Enabled="True" TargetControlID="btnInformacion">
                                            </cc1:ModalPopupExtender>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                &nbsp;
                            </td>
                            <td colspan="5">
                                <asp:CheckBox ID="chb_Consolidado" AutoPostBack="true" CssClass="Texto" runat="server"
                                    Font-Bold="true" Text="Toma de Inventario < Consolidado >" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="imgDocumentoRef" runat="server" />&nbsp;DOCUMENTO DE REFERENCIA
                            <cc1:CollapsiblePanelExtender ID="cpeDocumentoRef" runat="server" TargetControlID="Panel_DocumentoRef"
                                ImageControlID="imgDocumentoRef" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                Collapsed="true" CollapseControlID="imgDocumentoRef" ExpandControlID="imgDocumentoRef">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_DocumentoRef" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomTipoDocumento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red"
                                                        HeaderText="Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                                    <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                                        <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                                        <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />

                                                                        <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                                        <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Emisi�n"
                                                        HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="FechaEntrega" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Toma Inv."
                                                        HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomAlmacen" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"
                                                        HeaderText="Almac�n" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCad_DocumentoRef" Visible="false" CssClass="LabelRojo" runat="server"
                                                Text="-"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="imgEmpleado" runat="server" />&nbsp;ENCARGADOS
                            <cc1:CollapsiblePanelExtender ID="Collapsiblepanelextender_Empleado" runat="server"
                                TargetControlID="Panel_Empleado" ImageControlID="imgEmpleado" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" Collapsed="true" CollapseControlID="imgEmpleado"
                                ExpandControlID="imgEmpleado">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Empleado" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnBuscarEmpleado" runat="server" Text="Buscar Encargado" Width="160px"
                                                OnClientClick=" return( valOnClick_btnBuscarEmpleado() ); " />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_Empleado" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <Columns>
                                                    <asp:CommandField ButtonType="Link" SelectText="Quitar" ShowSelectButton="true" HeaderStyle-Width="70px"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px"
                                                        ItemStyle-Height="25px" />
                                                    <asp:BoundField DataField="IdPersona" HeaderText="Cod." NullDisplayText="0" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Descripcion" HeaderText="Nombre o Raz�n Social" NullDisplayText="---"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Ruc" HeaderText="RUC" NullDisplayText="---" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Dni" HeaderText="DNI" NullDisplayText="---" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" />
                                                </Columns>
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_CargaProductos" runat="server">
                                <fieldset class="FieldSetPanel">
                                    <legend>Cargar Productos al Detalle del Documento</legend>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnCargarProducto" runat="server" Text="Cargar Productos" OnClientClick="  return(     valOnClick_btnCargarProducto()       );  " />
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_FiltroDetalle" runat="server">
                                <fieldset class="FieldSetPanel">
                                    <legend>Filtrar Productos del Detalle del Documento</legend>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            Tipo Existencia:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList TabIndex="201" ID="CboTipoExistenciaDocumento" runat="server" AutoPostBack="True"
                                                                DataTextField="Descripcion" DataValueField="Id">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td align="right">
                                                            L�nea:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:DropDownList TabIndex="201" ID="cboLinea_Filtro" runat="server" AutoPostBack="True"
                                                                            DataTextField="Descripcion" DataValueField="Id">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        Sub L�nea:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList TabIndex="202" ID="cboSubLinea_Filtro" runat="server" DataTextField="Nombre"
                                                                            DataValueField="Id">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            Descripci�n:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox TabIndex="204" ID="txtDescripcionProd_Filtro" runat="server" Width="300px"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            onKeypress="return( valKeyPress_Filtro(this,event) );"></asp:TextBox>
                                                                    </td>
                                                                    <td style="text-align: right">
                                                                        C�d.:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCodigoProd_Filtro" Width="110px" runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                            onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" onKeypress="return( valKeyPress_Filtro(this,event) );"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnFiltrar_Filtro" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                            TabIndex="207" onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnLimpiar_Filtro" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                                            onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';"
                                                                            OnClientClick="return(limpiarBuscarProducto_Filtro());" TabIndex="209" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right">
                                                            Cant. Reg.:
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboPageSizeDoc" runat="server" AutoPostBack="true">
                                                                            <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                                                            <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="text-align: right">
                                                                        Pagina:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="cboPageIndexDoc" runat="server" AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DETALLE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Detalle" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnGuardar_Detalle" runat="server" Text="Guardar los cambios" Width="150px" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnGenerar_Lista" runat="server" Text="Generar Lista" Width="150px"
                                                ToolTip="Generar Lista" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboGenerarLista" runat="server">
                                                <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                                <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>

                                                            <asp:LinkButton CssClass="Texto" ID="lkbQuitar" Font-Bold="true" runat="server" OnClick="lkbQuitar_Click">Quitar</asp:LinkButton>

                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="C�digo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:TextBox ID="txtCodigoProducto" MaxLength="9" Width="80px" Text='<%#DataBinder.Eval(Container.DataItem,"CodigoProducto")%>'
                                                                runat="server" onKeyPress="return ( onKeyPress_txtCodigoProducto(this,event) );"></asp:TextBox>

                                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />

                                                            <asp:HiddenField ID="hddIdUnidadMedida" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />

                                                            <asp:ImageButton ID="imgBuscarCodigoProducto" runat="server" ImageUrl="~/Imagenes/Busqueda_B.jpg"
                                                                OnClick="imgBuscarCodigoProducto_Click" OnClientClick="return ( onClick_imgCodigoProducto(this) );" />

                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="NomProducto" HeaderText="Descripci�n" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UMedida" HeaderStyle-Width="80px" ItemStyle-ForeColor="Red"
                                            ItemStyle-Font-Bold="true" HeaderText="U.M." HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CantxAtender" HeaderStyle-Width="100px" ItemStyle-ForeColor="Red"
                                            ItemStyle-Font-Bold="true" HeaderText="SIGE" DataFormatString="{0:F4}" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="UM1" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:TextBox ID="txtCantidad1" TabIndex="200" Text='<%#DataBinder.Eval(Container.DataItem,"dc_Descuento1","{0:F4}")%>'
                                                                onkeyup="return( calcularCantidades('1',this) );" onFocus="return(  aceptarFoco(this)  );"
                                                                Width="60px" runat="server" Font-Bold="true" onKeypress="return( validarNumeroPuntoPositivo('event') );"
                                                                onblur="return( valBlur(event) );"></asp:TextBox>

                                                            <asp:Label ID="lblUM1" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UM1")%>'></asp:Label>

                                                            <asp:HiddenField ID="hddEquivalecia1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Equivalencia1")%>' />

                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="UM2" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:TextBox ID="txtCantidad2" TabIndex="200" Text='<%#DataBinder.Eval(Container.DataItem,"dc_Descuento2","{0:F4}")%>'
                                                                onkeyup="return( calcularCantidades('1',this) );" onFocus="return(  aceptarFoco(this)  );"
                                                                Width="60px" runat="server" Font-Bold="true" onKeypress="return( validarNumeroPuntoPositivo('event') );"
                                                                onblur="return( valBlur(event) );"></asp:TextBox>

                                                            <asp:Label ID="lblUM2" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UM2")%>'></asp:Label>

                                                            <asp:HiddenField ID="hddEquivalecia2" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Equivalencia2")%>' />

                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtCantidad" TabIndex="200" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F4}")%>'
                                                    Width="60px" runat="server" Font-Bold="true" onKeyPress=" return( false );  "
                                                    CssClass="TextBoxReadOnly_Right" onblur="return( valBlur(event) );"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Faltante" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtFaltante" onKeyPress=" return( false );  " CssClass="TextBoxReadOnly_Right"
                                                    onFocus="return(   valOnFocus_txtFaltante()   );" Enabled="true" Width="60px"
                                                    runat="server" Font-Bold="true" Text="0"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sobrante" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtSobrante" Enabled="true" onKeyPress=" return( false );  " CssClass="TextBoxReadOnly_Right"
                                                    onFocus="return(   valOnFocus_txtSobrante()   );" Width="60px" runat="server"
                                                    Font-Bold="true" Text="0"></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Observacion" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDetalleGlosa" Width="180px" runat="server" Font-Bold="true" Text='<%#DataBinder.Eval(Container.DataItem,"DetalleGlosa")%>'
                                                    TextMode="MultiLine" MaxLength="400" ></asp:TextBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Detalle_Consolidado" runat="server" AutoGenerateColumns="true"
                                    Width="100%">
                                    <Columns>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" Height="25px" HorizontalAlign="Center" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" HorizontalAlign="Center" Height="25px" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                OBSERVACIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <asp:TextBox ID="txtObservaciones" Width="100%" TextMode="MultiLine" Height="100px"
                        onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                        runat="server"></asp:TextBox>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="28" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Informacion" runat="server">
                    <div id="capa_Panel_Informacion" style="border: 3px solid blue; padding: 8px; width: 400px;
                        height: auto; position: absolute; background-color: white; z-index: 2; display: block;">
                        <table style="width: 100%;">
                            <tr>
                                <td align="center" class="Label" style="font-weight: bold; text-align: center">
                                    El proceso de C�lculo de Saldos del Sistema, hace referencia hasta un d�a antes
                                    de la Fecha de Toma de Inventario seleccionado.
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnAceptar_Panel_Informacion" runat="server" Text="Aceptar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 70px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Existencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                    DataTextField="Descripcion" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    DataValueField="Id" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="Panel5" runat="server" DefaultButton="btnBuscarGrilla_AddProd">
                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                onKeypress="return( valKeyPressDescripcionProd(this,event) );"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label1" runat="server" CssClass="Label" Text="C�d.:"></asp:Label>
                                        </td>
                                        <td>
                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnBuscarGrilla_AddProd">
                                            <asp:TextBox ID="txtCodigoProducto_AddProd" Font-Bold="true" Width="100px" runat="server"
                                                onKeypress="return( valKeyPressDescripcionProd(this,event) );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" TabIndex="205"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnBuscarGrilla_AddProd" runat="server" Text="Buscar" Width="75px"
                                                TabIndex="208" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnLimpiar_AddProd" runat="server" Text="Limpiar" Width="75px" OnClientClick="return(limpiarBuscarProducto());"
                                                TabIndex="209" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto">
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Cant. Reg.:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboPageSize" runat="server" AutoPostBack="true">
                                                <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                                <asp:ListItem Value="50" Text="50"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto">
                                            Pagina:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboPageIndex" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCargaProductoMasivo_AddProd" runat="server" Text="< L�nea / Sub L�nea >"
                                                Width="150px" OnClientClick="return(  valOnClick_btnCargaProductoMasivo_AddProd()  );"
                                                TabIndex="210" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Width="85px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:TemplateField HeaderText="C�digo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                <asp:Label ID="lblCodigo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                            <asp:BoundField DataField="UnidadMedida" ItemStyle-HorizontalAlign="Center" HeaderText="U.M."
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Selected="True" Value="N">Natural</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this,event));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel4" runat="server" DefaultButton="btBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td align="right" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisi�n</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                                    HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                    <asp:HiddenField ID="hddIdEmpresa_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />

                                                    <asp:HiddenField ID="hddIdAlmacen_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FechaEntrega" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Inv."
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomAlmacen" HeaderText="Almac�n" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="LabelTdLeft" style="font-weight: bold">
                    *** El filtro se realiza a trav�s de la Empresa / Tienda / Almac�n seleccionados.
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">

        function valKeyPressCodigoSL(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(obj, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto_AddProd.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }



        function mostrarCapaProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }
        function valOnClick_btnBuscarRemitente() {
            mostrarCapaPersona();
            return false;
        }
        //***********************************
        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }
        function valOnKeyPressCodigoDoc(txtCodigoDocumento, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }

        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor v�lido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }
        function valOnClickEditar() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Edici�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite su edici�n.');
                return false;
            }
            return true;
        }
        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para su Anulaci�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya est� ANULADO. No se permite su anulaci�n.');
                return false;
            }
            return confirm('Desea continuar con el Proceso de ANULACI�N del Documento ?');
        }

        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');

        }
        function valOnClick_btnAceptarBuscarDocRefxCodigo() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('Ingrese un Nro. de Serie para la b�squeda.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('Ingrese un Nro. de Documento para la b�squeda.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }
        function valOnClick_btnBuscarEmpleado() {
            mostrarCapaPersona();
            return false;
        }
        function valOnClick_btnCargarProducto() {
            mostrarCapaProducto();
            return false;
        }
        function calcularCantidades(recorrerAll, caja) {

            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla == null) {
                return false;
            }

            var IdEvento = '';
            if (event != null) {
                IdEvento = event.srcElement.id;
            }


            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];

                var rs1 = 0;
                var rs2 = 0;
                var xcaja = '';

                if (caja != null) {

                    if (rowElem.cells[5].children[0].id == caja.id || rowElem.cells[6].children[0].id == caja.id) {
                        var cantidad1 = parseFloat(rowElem.cells[5].children[0].value);
                        if (isNaN(cantidad1)) { cantidad1 = 0; }

                        var Equivalencia1 = parseFloat(rowElem.cells[5].children[2].value);
                        if (isNaN(Equivalencia1)) { Equivalencia1 = 0; }

                        var cantidad2 = parseFloat(rowElem.cells[6].children[0].value);
                        if (isNaN(cantidad2)) { cantidad2 = 0; }

                        var Equivalencia2 = parseFloat(rowElem.cells[6].children[2].value);
                        if (isNaN(Equivalencia2)) { Equivalencia2 = 0; }

                        rs1 = cantidad1 * Equivalencia1;
                        rs2 = cantidad2 * Equivalencia2;

                        recorrerAll = '0';
                        xcaja = rowElem.cells[7].children[0].id;
                    }

                }

                if (rowElem.cells[7].children[0].id == IdEvento || recorrerAll == '1' || rowElem.cells[7].children[0].id == xcaja) {

                    var cantidad = parseFloat(rowElem.cells[7].children[0].value);
                    if (isNaN(cantidad)) { cantidad = 0; }

                    if (rowElem.cells[7].children[0].id == xcaja && recorrerAll == '0') {
                        cantidad = redondear((rs1 + rs2), 4);
                        rowElem.cells[7].children[0].value = cantidad;
                    }


                    var cantidadSistema = parseFloat(rowElem.cells[4].innerHTML);
                    if (isNaN(cantidadSistema)) { cantidadSistema = 0; }

                    var diferencia = cantidadSistema - cantidad;
                    var faltante = 0
                    var sobrante = 0
                    if (diferencia > 0) {
                        faltante = diferencia;
                        sobrante = 0;
                    } else if (diferencia < 0) {
                        faltante = 0;
                        sobrante = diferencia * (-1);
                    }

                    //********* Imprimimos
                    rowElem.cells[8].children[0].value = redondear(faltante, 4);  //******** Faltante
                    rowElem.cells[9].children[0].value = redondear(sobrante, 4);  //******** Sobrante

                    if (recorrerAll == '0') {
                        return false;
                    }
                }
            }

            return false;
        }

        function valSaveDocumento() {
            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie de Documento.');
                return false;
            }

            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }

            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var cboAlmacen = document.getElementById('<%=cboAlmacen.ClientID%>');
            if (isNaN(parseInt(cboAlmacen.value)) || cboAlmacen.value.length <= 0) {
                alert('Debe seleccionar un Almac�n.');
                return false;
            }

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
            if (parseInt(hddFrmModo.value) == 0) { //*** INICIO

                return confirm('Desea continuar con la Generaci�n de un Nuevo Documento < Toma de Inventario > ? ' + '\n' + ' < Empresa:   ' + getCampoxValorCombo(cboEmpresa, cboEmpresa.value) + ' >' + '\n' + '< Almac�n :   ' + getCampoxValorCombo(cboAlmacen, cboAlmacen.value) + ' >');
            }

            return confirm('Desea continuar con la Operaci�n ?');
        }
        function valOnFocus_txtSobrante() {
            document.getElementById('<%=btnGuardar_Detalle.ClientID%>').focus();
            return false;
        }
        function valOnFocus_txtFaltante() {
            document.getElementById('<%=btnGuardar_Detalle.ClientID%>').focus();
            return false;
        }
        function valKeyPress_Filtro(obj, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnFiltrar_Filtro.ClientID%>').focus();
            }
            return true;
        }
        function limpiarBuscarProducto_Filtro() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cboLinea_Filtro.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cboSubLinea_Filtro.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_Filtro.ClientID%>');

            var txtCodigoProducto_Filtro = document.getElementById('<%=txtCodigoProd_Filtro.ClientID%>');

            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';

            txtCodigoProducto_Filtro.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }
        function valOnClick_btnCargaProductoMasivo_AddProd() {
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');

            if (parseInt(cboLinea.value) <= 0) {
                alert('DEBE SELECCIONAR UNA LINEA.');
                return false;
            }

            var str = 'Desea continuar con la carga de productos de la L�nea < ' + getCampoxValorCombo(cboLinea, cboLinea.value) + ' > SubL�nea < ' + getCampoxValorCombo(cboSubLinea, cboSubLinea.value) + ' > ?';
            return confirm(str);
        }
        function valOnClick_btnRecalcularSaldos() {
            var txtFecha = document.getElementById('<%=txtFechaTomaInv.ClientID%>');
            return confirm('La Fecha seleccionada es < ' + txtFecha.value + ' >. Desea continuar con la Operaci�n ?');
        }
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        //*************
        function valOnClick_btnImprimir() {

            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length == 0) {
                alert('No se ha seleccionado ning�n Documento para Impresi�n.');
                return false;
            }
            var Opcion = '1';

            window.open('../../Almacen1/Reportes/Visor.aspx?iReporte=4&IdDocumento=' + hddIdDocumento.value + '&Opcion=' + Opcion, 'Inventario', 'resizable=yes,width=1000,height=700,scrollbars=1', null);
            //window.open('../../Reportes/visor.aspx?iReporte=2&IdDoc=' + hdd.value, 'Factura', 'resizable=yes,width=1000,height=700', null);
            return false;
        }

        function valOnClick_btnExportar() {

            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(hddIdDocumento.value) || hddIdDocumento.value.length == 0) {
                alert('No se ha seleccionado ning�n Documento para Impresi�n.');
                return false;
            }

            var Consolidado = document.getElementById('<%=chb_Consolidado.ClientID%>');

            if (Consolidado.status == false) {
                alert('Exportaci�n disponible para el consolidado de tomas de inventario.');
                return false;
            }

            var grilla = document.getElementById('<%=GV_Detalle_Consolidado.ClientID %>');
            if (grilla == null) {
                alert('El consolidado de tomas de inventario no contine informaci�n.');
                return false;
            }
            
            return true;
        }

        function onKeyPress_txtCodigoProducto(caja, elEvento) {

            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {

                var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
                if (grilla != null) {

                    for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                        var rowElem = grilla.rows[i];

                        if (caja.id == rowElem.cells[1].children[0].id) {

                            if (rowElem.cells[1].children[0].value.length <= 0 || UnFormat(rowElem.cells[1].children[0].value, ' ', '') == '') {
                                alert('INGRESE UN C�DIGO DE PRODUCTO. \n NO SE PERMITE LA OPERACI�N.');
                                return false;
                            }
                            rowElem.cells[1].children[3].click();
                            break;
                            return false;
                        }

                    }
                } // ***** end if
            }
            return true;
        }

        function onClick_imgCodigoProducto(caja) {

            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {

                for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                    var rowElem = grilla.rows[i];

                    if (caja.id == rowElem.cells[1].children[3].id) {

                        if (rowElem.cells[1].children[0].value.length <= 0 || UnFormat(rowElem.cells[1].children[0].value, ' ', '') == '') {
                            alert('INGRESE UN C�DIGO DE PRODUCTO. \n NO SE PERMITE LA OPERACI�N.');
                            return false;
                        }
                        break;
                    }

                }
            } // ***** end if

            return true;
        }
        
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
