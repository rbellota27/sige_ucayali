﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmAsigObjetivo.aspx.vb" Inherits="APPWEB.FrmAsigObjetivo" 
    title="Página sin título" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <table class="style1">
   <tr style="background-color: Yellow">
            <td style="width: 350px">
                <asp:LinkButton CssClass="Label" Font-Bold="true" ID="lkb_CapaConfigFrm" OnClientClick="return(onCapa('capaConfiguracionFrm'));"
                    runat="server">Config. Formulario</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                   <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" ToolTip="Nuevo" Width="120px" />
                        </td>
                       
                        <td>
                            <asp:Button ID="btnBuscar" runat="server"  Text="Buscar Objetivos" ToolTip="Buscar" Width="120px" />
                        </td>
                                   
                          <td>
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" ToolTip="Guardar" Width="120px" />
                        </td>
                          <td>
                          <asp:Button  ID="btneditar" runat ="server" Text ="Modificar Datos" ToolTip="Modificar Datos" Width="120px"/>
                        </td>
                          <td>
                          <asp:Button  ID="btnsaveeditar" runat ="server" Text ="Guardar Cambios" ToolTip="Guardar Cambios" Width="120px"/>
                        </td>
                        <td><asp:Button runat="server" ID="btnRegresar" Text="Regresar a la opciones" Visible="false" /></td>
                          <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table> 
                </td>
                </tr>
        <tr>
            <td class="TituloCelda">
                ASIGNAR OBJETIVO POR TIENDA
            </td>
        </tr>
        
         <tr>
        <td>
        <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                                        <tr>
                                            <td align="right" class="Texto">
                                             <asp:Label ID="lbltienda" Text="Tienda:" runat="server"></asp:Label>  
                                            </td>
                                                
                                            <td >
                                                <asp:DropDownList ID="cbotienda" runat="server" Enabled="true" >
                                                </asp:DropDownList>
                                            </td>
                                            <td></td>
                                              <td  align="right" class="Texto">
                                              <asp:Label ID="lblobjetivo" Text="Objetivo:" runat="server"></asp:Label>  
                                               
                                        </td>
                                          <td >
                                               <asp:TextBox ID="txtobjetivo" runat="server" Text="0" ToolTip="Ingrese Objetivo" MaxLength ="2" 
                                               TextMode="SingleLine" onKeyPress="return(onKeyPressEsNumero('event'));" onpaste = "return false;"></asp:TextBox>
                                         </td>
                                            <td><asp:Label ID="lblNumeric" ForeColor = "Red" runat="server" Text="*Ingrese Solo Números" style ="display:none;font-size:small "></asp:Label>
                                               <asp:ImageButton id="btnbuscareditar" runat="server" ImageUrl="~/Imagenes/Buscar_A.JPG"  Visible="false"/>
                                            </td>
                                              
                                              <td>
                                         
                                            </td>
                                            <td></td>
                                           
                                            <td>
                                            
                                            </td>
                                        </tr>
                                        <tr>
                                      
                                            <td align="right" class="Texto">
                                                
                                                   <asp:Label ID="lblmes" Text="Mes:" runat="server"></asp:Label>  
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlmes" runat="server">
                                                    <asp:ListItem Value="1" Text="Enero"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Febrero" ></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="Marzo"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="Abril"></asp:ListItem>
                                                    <asp:ListItem Value="5" Text="Mayo"></asp:ListItem>
                                                    <asp:ListItem Value="6" Text="Junio"></asp:ListItem>
                                                    <asp:ListItem Value="7" Text="Julio"></asp:ListItem>
                                                    <asp:ListItem Value="8" Text="Agosto"></asp:ListItem>
                                                    <asp:ListItem Value="9" Text="Setiembre"></asp:ListItem>
                                                    <asp:ListItem Value="10" Text="Octubre"></asp:ListItem>
                                                    <asp:ListItem Value="11" Text="Noviembre"></asp:ListItem>
                                                    <asp:ListItem Value="12" Text="Diciembre"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td align="right" class="Texto">
                                             <asp:Label ID="lblanio" Text="Año:" runat="server"></asp:Label>  
                                                
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlanio" runat="server">
                                                    <asp:ListItem Text="2013" ></asp:ListItem>
                                                    <asp:ListItem Selected="True" Text="2014"></asp:ListItem>
                                                    <asp:ListItem Text="2015"></asp:ListItem>
                                                    <asp:ListItem Text="2016"></asp:ListItem>
                                                    <asp:ListItem Text="2017"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            <asp:ImageButton id="fastsearch" runat="server" ImageUrl="~/Imagenes/Buscar_A.JPG"  Visible="false"/>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                      
                                        </tr>
                                        </table>
                                        <table>
                                         <tr>
                                         <td>
                                         <asp:Label ID="lblnuevodato" Text="Ingrese los nuevos datos para la tienda seleccionada : [ " style="color:#000099;font-family:Comic Sans MS;font-size:small" runat="server" Visible="false" ></asp:Label>
                                          <asp:Label ID="lblnombretienda" Text=" " style="color:#000099;font-family:Comic Sans MS;font-size:small" Font-Bold="true" runat="server"></asp:Label>
                                         </td>
                                         <td></td>
                                         </tr>
                                        </table>
                                        <table>
                                        <tr>
                                        <td>
                                       
                                        <asp:Label ID="lblNuevoMes" runat="server" Text="Seleccione el nuevo Mes:"  style="color:#0000FF;font-family:Comic Sans MS;font-size:small" ></asp:Label>
                                        </td>
                                        
                                        <td> <asp:DropDownList ID="cbonuevomes" runat="server">
                                                    <asp:ListItem Value="1" Text="Enero"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Febrero" ></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="Marzo"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="Abril"></asp:ListItem>
                                                    <asp:ListItem Value="5" Text="Mayo"></asp:ListItem>
                                                    <asp:ListItem Value="6" Text="Junio"></asp:ListItem>
                                                    <asp:ListItem Value="7" Text="Julio"></asp:ListItem>
                                                    <asp:ListItem Value="8" Text="Agosto"></asp:ListItem>
                                                    <asp:ListItem Value="9" Text="Setiembre"></asp:ListItem>
                                                    <asp:ListItem Value="10" Text="Octubre"></asp:ListItem>
                                                    <asp:ListItem Value="11" Text="Noviembre"></asp:ListItem>
                                                    <asp:ListItem Value="12" Text="Diciembre"></asp:ListItem>
                                                </asp:DropDownList></td> <td></td>
                                                <td>
                                                <asp:Label ID="Nuevoanio"  runat="server" Text="Seleccione el nuevo Año: "  style="color:#0000FF;font-family:Comic Sans MS;font-size:small"></asp:Label>
                                                </td>
                                                <td>
                                                  <asp:DropDownList ID="cbonuevoanio" runat="server">
                                                    <asp:ListItem Text="2013" ></asp:ListItem>
                                                    <asp:ListItem Selected="True" Text="2014"></asp:ListItem>
                                                    <asp:ListItem Text="2015"></asp:ListItem>
                                                    <asp:ListItem Text="2016"></asp:ListItem>
                                                    <asp:ListItem Text="2017"></asp:ListItem>
                                                </asp:DropDownList>
                                                </td>
                                        </tr>
                                        <tr>
                                        <td>
                                        <asp:Label ID="lblnuevoobjetivo" runat="server" Text="Ingrese el nuevo Objetivo: "  style="color:#0000FF;font-family:Comic Sans MS;font-size:small"></asp:Label>
                                        </td>
                                           <td >
                                               <asp:TextBox ID="txtnuevoobjetivo" runat="server" Text="0" ToolTip="Ingrese Objetivo" MaxLength ="2" 
                                               TextMode="SingleLine"></asp:TextBox>
                                         </td>
                                         <td></td>
                                         <td>
                                         <asp:Label ID="lblidobj" runat="server" Text="IdObjetivo :"  style="color:#0000FF;font-family:Comic Sans MS;font-size:small"></asp:Label>
                                         </td>
                                         <td>
                                         <asp:TextBox runat="server" ID="txtIdObj" Enabled="false" Visible="false" ></asp:TextBox>
                                         </td>
                                         </tr>
                                        </table>
                                        <table>
                                        <tr>
                                        <td></td>
                                        <td>
                                        <asp:Label id="lbl" runat="server"  Text="Tiendas relacionadas a los parámetros." style="color:Red;font-family:Comic Sans MS;font-size:small     "></asp:Label>
                                        
                                        </td>
                                        
                                        </tr>
                                        <tr>
                                        <td></td>
                                        <td>
                                      <asp:GridView ID="GV_TiendasxObjetivo" runat="server" BackColor="White" 
                                                BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                                                GridLines="Vertical"  AutoGenerateColumns="False">
                                          <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                       <Columns>
                                   <asp:TemplateField HeaderText="Tienda">
                                   <ItemTemplate>
                                         <asp:Label ID="lblTienda"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                   </ItemTemplate>
                             
                                   </asp:TemplateField>
                                   
                                      <asp:TemplateField HeaderText="Objetivo">
                                   <ItemTemplate>
                                         <asp:Label ID="lblobjxtienda"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"ObjetivoxTienda")%>'></asp:Label>
                                   </ItemTemplate>
                                   
                                                                     
                                   </asp:TemplateField>
                                   
                                      <asp:TemplateField HeaderText="Mes">
                                   <ItemTemplate>
                                         <asp:Label ID="lblmes"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Mes")%>'></asp:Label>
                                   </ItemTemplate>
                                 
                                   </asp:TemplateField>
                                   
                                      <asp:TemplateField HeaderText="Año">
                                   <ItemTemplate>
                                         <asp:Label ID="lblAnio"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Anio")%>'></asp:Label>
                                   </ItemTemplate>
                                   
                                                                     
                                   </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Escala">
                                   <ItemTemplate>
                                         <asp:Label ID="lblEscala"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"ResultadoEscala")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                 
                                  
                                        </Columns>
                                          <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                          <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                          <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                          <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                          <AlternatingRowStyle BackColor="#DCDCDC" />
                                      </asp:GridView>                                       
                                        </td>
                                        
                                        
                                          <td><asp:GridView ID="gvEscala" runat="server" CellPadding="4" ForeColor="#333333" 
                                        GridLines="None"  >
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                             
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                              <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                                </td>
                                       
                                        </tr>
                                                
                     </table>
         </asp:Panel>
        </td>
        </tr>
        
 </table>



<script type = "text/javascript">

    Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
    Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
        if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
            offsetX = Math.floor(offsetX);
            offsetY = Math.floor(offsetY);
        }
        this._origOnFormActiveElement(element, offsetX, offsetY);
    };

    function isNumeric(keyCode)
        {  
            var charCode = (keyCode.which) ? keyCode.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                  {
                      document.getElementById('<%=lblNumeric.ClientID%>').style.display="block";
                      return false;
                   
                  }
                  else{
                      document.getElementById('<%=lblNumeric.ClientID%>').style.display="none";
                      return true;
                  }
                  
        }


</script>



</asp:Content>
