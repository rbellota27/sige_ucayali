﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmKardex1
    Inherits System.Web.UI.Page


    Private objScript As New ScriptManagerClass
    Private listaProductoView As List(Of Entidades.ProductoView)

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            inicializarFrm()
        End If
    End Sub

    Private Function getListaProductoView() As List(Of Entidades.ProductoView)
        Return CType(Session.Item("listaProductoView"), List(Of Entidades.ProductoView))
    End Function

    Private Sub setListaProductoView(ByVal lista As List(Of Entidades.ProductoView))
        Session.Remove("listaProductoView")
        Session.Add("listaProductoView", lista)
    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .llenarCboAlmacenxIdEmpresa(Me.cboAlmacen, CInt(Me.cboEmpresa.SelectedValue), False)

                Me.txtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
                Me.txtFechaFin.Text = Me.txtFechaInicio.Text

                '--lineas--
                .llenarCboTipoExistencia(CboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

                '.LlenarCboLinea(Me.cmbLinea_AddProd, True)
                '.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
                '.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

    


            End With

            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {259})

            If (listaPermisos(0) <= 0) Then '******  NO Visualizar Costos
                Me.GV_Kardex.Columns(10).Visible = False
                Me.GV_Kardex.Columns(9).Visible = False
                Me.GV_Kardex.Columns(7).Visible = False
                Me.GV_Kardex.Columns(6).Visible = False
                Me.tr_costo.Visible = False
            End If


            Dim objMoneda As Entidades.Moneda = (New Negocio.Moneda).SelectCboMonedaBase(0)
            Me.lblMonedaBase_Cab.Text = objMoneda.Simbolo
            Me.lblMoneda_Base.Text = objMoneda.Simbolo
            Me.lblMoneda.Text = objMoneda.Simbolo

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#Region "************************** BUSQUEDA PRODUCTO"
    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
    End Sub

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacen.SelectedValue)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia", Me.CboTipoExistencia.SelectedValue)

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, ByVal nomProducto As String, ByVal IdAlmacen As Integer, _
                                          ByVal tipoMov As Integer, ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        '************Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        'Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)


        If listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

        setListaProductoView(Me.listaProductoView)

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
            

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
  
#End Region

    Private Sub DGV_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_AddProd.SelectedIndexChanged

        cargarProducto(Me.DGV_AddProd.SelectedIndex)


    End Sub

    Private Sub cargarProducto(ByVal Index As Integer)

        Try

            Me.listaProductoView = getListaProductoView()

            Dim lista As New List(Of Entidades.ProductoView)
            lista.Add(Me.listaProductoView(Index))

            Me.GV_Producto.DataSource = lista
            Me.GV_Producto.DataBind()

            Me.GV_Equivalencia.DataSource = (New Negocio.ProductoUMView).SelectxIdProducto(Me.listaProductoView(Index).Id).FindAll(Function(ent As Entidades.ProductoUMView) ent.Estado = "1")
            Me.GV_Equivalencia.DataBind()

            Me.hddIdProducto.Value = CStr(Me.listaProductoView(Index).Id)

            Me.GV_ResuldoEQ.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.llenarCboAlmacenxIdEmpresa(Me.cboAlmacen, CInt(Me.cboEmpresa.SelectedValue), False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Kardex_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_Kardex.PageIndexChanging
        Me.GV_Kardex.PageIndex = e.NewPageIndex
        mostrarReporteKardex()
    End Sub

    Private Sub GV_Kardex_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Kardex.SelectedIndexChanged
        cargarCapaEdicionMov(GV_Kardex.SelectedIndex)
    End Sub

    Private Sub cargarCapaEdicionMov(ByVal Index As Integer)

        Try

            If (Index = 0) Then
                Throw New Exception("NO SE PERMITE LA OPERACIÓN.")
            End If



            Dim IdDetalleDocumento As Integer = CInt(CType(Me.GV_Kardex.Rows(Index).FindControl("hddIdDetalleDocumento"), HiddenField).Value)
            Dim IdDocumento As Integer = CInt(CType(Me.GV_Kardex.Rows(Index).FindControl("hddIdDocumento"), HiddenField).Value)

            Me.lblEmpresa.Text = Me.cboEmpresa.SelectedItem.ToString
            Me.lblAlmacen.Text = Me.cboAlmacen.SelectedItem.ToString

            Me.lblCodigoProducto.Text = CType(Me.GV_Producto.Rows(0).FindControl("lblCodigo"), Label).Text
            Me.lblProducto.Text = HttpUtility.HtmlDecode(Me.GV_Producto.Rows(0).Cells(1).Text)

            Dim stockFisico, stockComprometido, stockDisponible As Decimal
            ' ******** STOCK REAL
            stockFisico = (New Negocio.Util).fx_StockRealxIdEmpresaxIdAlmacenxIdProductoxFechaFin(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), CInt(Me.hddIdProducto.Value), (New Negocio.FechaActual).SelectFechaActual)
            Me.lblStockFisico.Text = CStr(Math.Round(stockFisico, 4))
            ' ******** STOCK COMPROMETIDO
            stockComprometido = (New Negocio.Util).fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), CInt(Me.hddIdProducto.Value))
            Me.lblComprometido.Text = CStr(Math.Round(stockComprometido, 4))
            ' ******** STOCK DISPONIBLE
            stockDisponible = stockFisico - stockComprometido
            Me.lblDisponible.Text = CStr(Math.Round(stockDisponible, 4))
            ' ******** STOCK TRANSITO
            Me.lblTransito.Text = CStr(Math.Round((New Negocio.Util).Fn_getStockTransito(CInt(Me.cboAlmacen.SelectedValue), CInt(Me.hddIdProducto.Value)), 4))

            Me.lblUnidadMedida.Text = HttpUtility.HtmlDecode(Me.GV_Producto.Rows(0).Cells(2).Text)
            Me.lblUnidadMedida_Update.Text = Me.lblUnidadMedida.Text
            Me.lblUnidadMedida_Mov.Text = Me.lblUnidadMedida.Text

           
            ' ******** DOCUMENTO
            Me.GV_DocumentoRef.DataSource = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(IdDocumento)
            Me.GV_DocumentoRef.DataBind()

            

            Me.lblFecha.Text = HttpUtility.HtmlDecode(Me.GV_Kardex.Rows(Index).Cells(3).Text)
            Me.lblTipoDocumento.Text = CType(Me.GV_Kardex.Rows(Index).FindControl("lblTipoDocumento"), Label).Text
            Me.lblNroDocumento.Text = CType(Me.GV_Kardex.Rows(Index).FindControl("lblNroDocumento"), Label).Text

            Me.lblCantidad.Text = CStr(Math.Round((New Negocio.Util).fx_getCantidadMovAlmacenxIdDetalleDocumento(IdDetalleDocumento), 2))
            Me.txtCantidad.Text = Me.lblCantidad.Text

            Me.hddIdDetalleDocumento.Value = CStr(IdDetalleDocumento)


            Dim tipoMov As String = HttpUtility.HtmlDecode(Me.GV_Kardex.Rows(Index).Cells(4).Text)

            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {165, 166, 167})

            If (tipoMov = "E") Then
                Dim costoMov As Decimal = CDec(CType(Me.GV_Kardex.Rows(Index).FindControl("hdddma_Costo"), HiddenField).Value)
                'Me.ViewState.Add("CostoMov", Math.Round(costoMov, 3))
                Me.lblCostoMov.Text = CStr(Math.Round(costoMov, 3))
                Me.lblTipoMov.Text = "ENTRADA"
                Me.txtCostoMov.Text = Me.lblCostoMov.Text
                Me.txtCostoMov.Enabled = True
            Else
                Me.lblCostoMov.Text = "---"
                Me.lblTipoMov.Text = "SALIDA"
                Me.txtCostoMov.Text = "0"
                Me.txtCostoMov.Enabled = False
            End If


            If (listaPermisos(0) > 0) Then '****** CANTIDAD
                Me.txtCantidad.Enabled = True
            Else
                Me.txtCantidad.Enabled = False
            End If

            If (listaPermisos(1) > 0 And tipoMov = "E") Then '****** COSTO
                Me.txtCostoMov.Enabled = True
            Else
                Me.txtCostoMov.Enabled = False
            End If

            If (listaPermisos(2) > 0) Then '****** REGISTRAR
                Me.btnGuardar.Enabled = True
            Else
                Me.btnGuardar.Enabled = False
            End If

            If (Me.tr_costo.Visible = False) Then '****** NO Visualizar Costos

                Me.lblCostoMov.Text = ""

            End If

            objScript.onCapa(Me, "capaEdicion")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnReporte_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReporte.Click

        mostrarReporteKardex()

    End Sub


    Dim ListaKardex As List(Of Entidades.Kardex)

    Private Sub mostrarReporteKardex()
        Try

            Dim Igv As Decimal = 0
            'If Me.ckIgv.Checked Then Igv = (New Negocio.Impuesto).SelectTasaIGV

            Me.ListaKardex = (New Negocio.MovAlmacen).CR_KardexProductoXMetVal(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), CInt(Me.hddIdProducto.Value), CStr(Me.txtFechaInicio.Text), CStr(Me.txtFechaFin.Text), Igv)

            Me.GV_Kardex.DataSource = Me.ListaKardex
            Me.GV_Kardex.DataBind()
            If ListaKardex IsNot Nothing Then
                Me.calcularEquivalencia(CInt(Me.hddIdProducto.Value), CDec(Me.ListaKardex(Me.ListaKardex.Count - 1).sk_CantidadSaldo))
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        actualizarMovimiento()
    End Sub


    Private Sub actualizarMovimiento()
        Try

            Dim cantidad As Decimal = CDec(Me.txtCantidad.Text)
            Dim costo As Decimal = CDec(Me.txtCostoMov.Text)
            Dim IdDetalleDocumento As Integer = CInt(Me.hddIdDetalleDocumento.Value)

            If ((New Negocio.MovAlmacen).MovAlmacenUpdate_Cantidad_CostoxIdDetalleDocumento(cantidad, costo, IdDetalleDocumento)) Then

                Me.GV_Kardex.DataSource = Nothing
                Me.GV_Kardex.DataBind()

                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")

            Else

                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Dim TableProductoEq As DataTable
    Dim listaUM As List(Of Entidades.ProductoUMView)
    Private Sub calcularEquivalencia(ByVal IdProducto As Integer, ByVal cantidad As Decimal)
        Try

            listaUM = (New Negocio.ProductoUMView).SelectxIdProducto(IdProducto)
            Dim tableUM As DataTable = obtenerTablaUM(IdProducto, listaUM)

            TableProductoEq = (New Negocio.Producto).Producto_ConsultaEquivalenciaProductoxParams(IdProducto, listaUM(0).IdUnidadMedida, listaUM(0).IdUnidadMedida, cantidad, tableUM, 0)

            If TableProductoEq IsNot Nothing Then

                TableProductoEq.Columns.Add("Equivalencia", GetType(String))

                Dim ListaUMEq As List(Of Entidades.ProductoUMView) = (New Negocio.ProductoUMView).SelectxIdProducto(IdProducto)

                For Each Row As DataRow In TableProductoEq.Rows

                    If Row("UnidadMedida").ToString.Trim <> "TOTAL" Then
                        Row("Equivalencia") = ListaUMEq.Find(Function(ent As Entidades.ProductoUMView) ent.IdUnidadMedida = CInt(Row("IdUnidadMedida"))).Equivalencia
                    End If

                Next

            End If


            Me.GV_ResuldoEQ.DataSource = TableProductoEq
            Me.GV_ResuldoEQ.DataBind()





        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerTablaUM(ByVal IdProducto As Integer, ByVal _ListaUM As List(Of Entidades.ProductoUMView)) As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")


        If _ListaUM IsNot Nothing Then

            For i As Integer = 0 To _ListaUM.Count - 1

                dt.Rows.Add(IdProducto, _ListaUM(i).IdUnidadMedida)
                'If i = 1 Then Exit For

            Next

        End If


        Return dt


    End Function

    Private Sub GV_ResuldoEQ_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_ResuldoEQ.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim Cantidad As Decimal = CDec(TableProductoEq.Rows(e.Row.RowIndex)("Cantidad"))




            If TableProductoEq.Rows(e.Row.RowIndex)("UnidadMedida").ToString = "TOTAL" Then

                CType(e.Row.FindControl("lblCantidad"), Label).Text = CStr(Math.Round(Cantidad, 3, MidpointRounding.AwayFromZero))

            Else

                CType(e.Row.FindControl("lblCantidad"), Label).Text = CStr(Math.Round(Cantidad, 2, MidpointRounding.AwayFromZero))

            End If

            If IsNumeric(TableProductoEq.Rows(e.Row.RowIndex)("Equivalencia")) Then

                Dim Equivalencia As Decimal = CDec(TableProductoEq.Rows(e.Row.RowIndex)("Equivalencia"))


                If Equivalencia = 1 Then

                    CType(e.Row.FindControl("lblCantidad"), Label).Text = CStr(Math.Round(Cantidad, MidpointRounding.AwayFromZero))
                    CType(e.Row.FindControl("lblEquivalencia"), Label).Text = CStr(Math.Round(Equivalencia, MidpointRounding.AwayFromZero))

                Else

                    Try
                        CType(e.Row.FindControl("lblEquivalencia"), Label).Text = Equivalencia.ToString("G29")
                    Catch ex As Exception
                        CType(e.Row.FindControl("lblEquivalencia"), Label).Text = Equivalencia.ToString
                    End Try


                End If

            End If

        End If

    End Sub


#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region

    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class