﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmValorReferencial_Transporte
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private objCbo As New Combo
    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        valOnLoad_Frm()
    End Sub
    Private Sub valOnLoad_Frm()
        Try
            If (Not Me.IsPostBack) Then
                ConfigurarDatos()
                inicializarFrm()
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub inicializarFrm()

        With objCbo
            .LLenarCboDepartamento(Me.cboDepto_Origen)
            .LLenarCboProvincia(Me.cboProvincia_Origen, Me.cboDepto_Origen.SelectedValue)
            .LLenarCboDistrito(Me.cboDistrito_Origen, Me.cboDepto_Origen.SelectedValue, Me.cboProvincia_Origen.SelectedValue)

            .LLenarCboDepartamento(Me.cboDepto_Destino)
            .LLenarCboProvincia(Me.cboProvincia_Destino, Me.cboDepto_Destino.SelectedValue)
            .LLenarCboDistrito(Me.cboDistrito_Destino, Me.cboDepto_Destino.SelectedValue, Me.cboProvincia_Destino.SelectedValue)

            .LlenarCboMoneda(Me.cboMoneda, False)
        End With

        verFrm(FrmModo.Inicio, True, True)

    End Sub

    Private Sub verFrm(ByVal modo As Integer, ByVal limpiarFrm As Boolean, ByVal initDatosGrilla As Boolean)

        If (limpiarFrm) Then
            limpiarFormulario()
        End If

        If (initDatosGrilla) Then

            Dim estado As Boolean = True
            If (Me.rdbEstado_Busqueda.SelectedValue = "0") Then
                estado = False
            End If

            Me.GV_ValorReferencial.DataSource = (New Negocio.ValorReferencia_Transporte).SelectxParams_DT(Nothing, estado, Me.txtDescripcion_Busqueda.Text)
            Me.GV_ValorReferencial.DataBind()


        End If
        Me.hddFrmModo.Value = CStr(modo)
        actualizarCtlFrm(modo)

    End Sub
    Private Sub actualizarCtlFrm(ByVal modo As Integer)

        Me.btnNuevo.Visible = False
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.Panel_Busqueda.Enabled = False
        Me.Panel_Busqueda.Visible = False
        Me.Panel_Registro.Enabled = False
        Me.Panel_Registro.Visible = False

        Select Case modo
            Case FrmModo.Inicio

                Me.btnNuevo.Visible = True
                Me.Panel_Busqueda.Visible = True
                Me.Panel_Busqueda.Enabled = True


            Case FrmModo.Nuevo

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True

                Me.Panel_Busqueda.Visible = True
                Me.Panel_Registro.Enabled = True
                Me.Panel_Registro.Visible = True


            Case FrmModo.Editar

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True

                Me.Panel_Busqueda.Visible = True
                Me.Panel_Registro.Enabled = True
                Me.Panel_Registro.Visible = True

        End Select

    End Sub
    Private Sub limpiarFormulario()

        Me.txtDescripcion.Text = ""
        Me.txtDescripcion_Busqueda.Text = ""
        Me.txtValorReferencial.Text = "0"

        Me.GV_ValorReferencial.DataSource = Nothing
        Me.GV_ValorReferencial.DataBind()

    End Sub

    Private Sub cboDepto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepto_Origen.SelectedIndexChanged, cboDepto_Destino.SelectedIndexChanged
        valOnChange_cboDepto(CType(sender, DropDownList).ID)
    End Sub

    Private Sub valOnChange_cboDepto(ByVal id As String)

        Select Case (id)
            Case Me.cboDepto_Origen.ID
                objCbo.LLenarCboProvincia(Me.cboProvincia_Origen, Me.cboDepto_Origen.SelectedValue)
                objCbo.LLenarCboDistrito(Me.cboDistrito_Origen, Me.cboDepto_Origen.SelectedValue, Me.cboProvincia_Origen.SelectedValue)
            Case Me.cboDepto_Destino.ID
                objCbo.LLenarCboProvincia(Me.cboProvincia_Destino, Me.cboDepto_Destino.SelectedValue)
                objCbo.LLenarCboDistrito(Me.cboDistrito_Destino, Me.cboDepto_Destino.SelectedValue, Me.cboProvincia_Destino.SelectedValue)
        End Select

    End Sub

    Private Sub cboProvincia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProvincia_Origen.SelectedIndexChanged, cboProvincia_Destino.SelectedIndexChanged
        valOnChange_cboProv(CType(sender, DropDownList).ID)
    End Sub

    Private Sub valOnChange_cboProv(ByVal id As String)
        Select Case id
            Case Me.cboProvincia_Origen.ID
                objCbo.LLenarCboDistrito(Me.cboDistrito_Origen, Me.cboDepto_Origen.SelectedValue, Me.cboProvincia_Origen.SelectedValue)
            Case Me.cboProvincia_Destino.ID
                objCbo.LLenarCboDistrito(Me.cboDistrito_Destino, Me.cboDepto_Destino.SelectedValue, Me.cboProvincia_Destino.SelectedValue)
        End Select
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        registrar()

    End Sub
    Private Sub registrar()

        Try

            Dim objValorReferencial As Entidades.ValorReferencia_Transporte = obtenerValorReferencia_Transporte()

            If ((New Negocio.ValorReferencia_Transporte).Registrar(objValorReferencial)) Then
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")
                verFrm(FrmModo.Inicio, True, True)
            Else
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function obtenerValorReferencia_Transporte() As Entidades.ValorReferencia_Transporte

        Dim obj As New Entidades.ValorReferencia_Transporte

        With obj

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdValorReferenciaT = Nothing
                Case FrmModo.Editar
                    .IdValorReferenciaT = CInt(Me.hddIdValorReferencial.Value)
            End Select

            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .Monto = CDec(Me.txtValorReferencial.Text)
            .Descripcion = Me.txtDescripcion.Text.Trim
            .IdUsuarioInsert = CInt(Session("IdUsuario"))
            .IdUsuarioUpdate = CInt(Session("IdUsuario"))

            Dim objUbigeo_Origen As Entidades.Ubigeo = (New Negocio.Ubigeo).SelectxCodDeptoxCodProvxCodDistrito(Me.cboDepto_Origen.SelectedValue, Me.cboProvincia_Origen.SelectedValue, Me.cboDistrito_Origen.SelectedValue)
            If (objUbigeo_Origen IsNot Nothing) Then
                .IdUbigeoOrigen = objUbigeo_Origen.Id
            End If
            Dim objUbigeo_Destino As Entidades.Ubigeo = (New Negocio.Ubigeo).SelectxCodDeptoxCodProvxCodDistrito(Me.cboDepto_Destino.SelectedValue, Me.cboProvincia_Destino.SelectedValue, Me.cboDistrito_Destino.SelectedValue)
            If (objUbigeo_Destino IsNot Nothing) Then
                .IdUbigeoDestino = objUbigeo_Destino.Id
            End If

            If (Me.rdbEstado.SelectedValue = "1") Then
                .Estado = True
            Else
                .Estado = False
            End If

        End With

        Return obj

    End Function
    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs)

        valOnClick_btnEditar(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)

    End Sub
    Private Sub valOnClick_btnEditar(ByVal index As Integer)

        Try

            Dim IdValorReferencial As Integer = CInt(CType(Me.GV_ValorReferencial.Rows(index).FindControl("hddIdValorReferencial"), HiddenField).Value)

            Dim objValorRef As Entidades.ValorReferencia_Transporte = (New Negocio.ValorReferencia_Transporte).SelectxIdValorReferenciaT(IdValorReferencial)

            If (objValorRef IsNot Nothing) Then
                cargarGUI(objValorRef)
                verFrm(FrmModo.Editar, False, False)
            Else
                Throw New Exception("NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cargarGUI(ByVal objValorRef As Entidades.ValorReferencia_Transporte)

        With objValorRef

            Me.hddIdValorReferencial.Value = CStr(.IdValorReferenciaT)
            Me.txtDescripcion.Text = CStr(.Descripcion)
            Me.txtValorReferencial.Text = Format(.Monto, "F3")

            If (Me.cboMoneda.Items.FindByValue(CStr(.IdMoneda)) IsNot Nothing) Then
                Me.cboMoneda.SelectedValue = CStr(.IdMoneda)
            End If

            If (.Estado) Then
                Me.rdbEstado.SelectedValue = "1"
            Else
                Me.rdbEstado.SelectedValue = "0"
            End If

            If (Me.cboDepto_Origen.Items.FindByValue(CStr(.CodDpto_Origen)) IsNot Nothing) Then
                Me.cboDepto_Origen.SelectedValue = CStr(.CodDpto_Origen)
                objCbo.LLenarCboProvincia(Me.cboProvincia_Origen, Me.cboDepto_Origen.SelectedValue)
                If (Me.cboProvincia_Origen.Items.FindByValue(CStr(.CodProv_Origen)) IsNot Nothing) Then
                    Me.cboProvincia_Origen.SelectedValue = CStr(.CodProv_Origen)
                    objCbo.LLenarCboDistrito(Me.cboDistrito_Origen, Me.cboDepto_Origen.SelectedValue, Me.cboProvincia_Origen.SelectedValue)
                    If (Me.cboDistrito_Origen.Items.FindByValue(CStr(.CodDist_Origen)) IsNot Nothing) Then
                        Me.cboDistrito_Origen.SelectedValue = CStr(.CodDist_Origen)
                    End If
                End If
            End If

            If (Me.cboDepto_Destino.Items.FindByValue(CStr(.CodDpto_Destino)) IsNot Nothing) Then
                Me.cboDepto_Destino.SelectedValue = CStr(.CodDpto_Destino)
                objCbo.LLenarCboProvincia(Me.cboProvincia_Destino, Me.cboDepto_Destino.SelectedValue)
                If (Me.cboProvincia_Destino.Items.FindByValue(CStr(.CodProv_Destino)) IsNot Nothing) Then
                    Me.cboProvincia_Destino.SelectedValue = CStr(.CodProv_Destino)
                    objCbo.LLenarCboDistrito(Me.cboDistrito_Destino, Me.cboDepto_Destino.SelectedValue, Me.cboProvincia_Destino.SelectedValue)
                    If (Me.cboDistrito_Destino.Items.FindByValue(CStr(.CodDist_Destino)) IsNot Nothing) Then
                        Me.cboDistrito_Destino.SelectedValue = CStr(.CodDist_Destino)
                    End If
                End If
            End If

        End With

    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelar.Click
        valOnClick_btnCancelar()
    End Sub
    Private Sub valOnClick_btnCancelar()
        Try


            verFrm(FrmModo.Inicio, True, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        valOnClick_btnNuevo()
    End Sub
    Private Sub valOnClick_btnNuevo()
        Try

            verFrm(FrmModo.Nuevo, True, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        valOnClick_btnBuscar()
    End Sub
    Private Sub valOnClick_btnBuscar()

        Try

            Dim descripcion As String = Me.txtDescripcion_Busqueda.Text
            Dim estado As Boolean = True

            If (Me.rdbEstado_Busqueda.SelectedValue = "0") Then
                estado = False
            End If

            Me.GV_ValorReferencial.DataSource = (New Negocio.ValorReferencia_Transporte).SelectxParams_DT(Nothing, estado, descripcion)
            Me.GV_ValorReferencial.DataBind()

            If (Me.GV_ValorReferencial.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub
End Class