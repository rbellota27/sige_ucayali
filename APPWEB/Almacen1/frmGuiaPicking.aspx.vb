﻿
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Public Class frmGuiaPicking
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private listaProductoView As List(Of Entidades.ProductoView)
    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor
    Dim valorx As Integer = 0
    Private Const _IdMagnitud_Peso As Integer = 1


#Region "************************ MANEJO DE SESSION"
    Private Function getListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item(CStr(ViewState("DetalleDocumento"))), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove(CStr(ViewState("DetalleDocumento")))
        Session.Add(CStr(ViewState("DetalleDocumento")), lista)
    End Sub
    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item(CStr(ViewState("listaDocumentoRef"))), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove(CStr(ViewState("listaDocumentoRef")))
        Session.Add(CStr(ViewState("listaDocumentoRef")), lista)
    End Sub

    Private Sub onLoad_Session()

        Dim Aleatorio As New Random()
        ViewState.Add("DetalleDocumento", "DetalleDocumentoGRR" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("listaDocumentoRef", "listaDocumentoRefGRR" + Aleatorio.Next(0, 100000).ToString)

    End Sub

#End Region

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum


    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptManagaer As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManagaer.RegisterPostBackControl(Me.btnImprimirPicking)
        If (Not Me.IsPostBack) Then

            onLoad_Session()

            ConfigurarDatos()
            ValidarPermisos()
            inicializarFrm()
            If (Request.QueryString("IdDocumentoRef") <> Nothing) Then
                cargarDocumentoReferencia(CInt(Request.QueryString("IdDocumentoRef")), Nothing, Nothing, Nothing)
            End If
            If (Request.QueryString("IdRef") <> Nothing) Then

                Dim serie As Integer = CInt(Request.QueryString("IdSerie"))
                Dim numero As Integer = CInt(Request.QueryString("doc_codigo"))
                Dim idTienda As Integer = CInt(Request.QueryString("idTienda"))
                cboTienda.SelectedValue = idTienda
                cboSerie.SelectedValue = serie
                Me.txtCodigoDocumento.Text = numero
                pruebas(serie, numero, 0, idTienda)
            End If
        End If

    End Sub
    Private Sub pruebas(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer, ByVal idTienda As Integer)

        Dim objCbo As New Combo
        objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), idTienda, CInt(Me.hddIdTipoDocumento.Value))
        objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, idTienda, False)

        If (CInt(Me.cboEmpresa.SelectedValue) = CInt(Me.hddIdRemitente.Value)) Then
            actualizarPuntoPartidaxIdAlmacen(CInt(Me.cboAlmacen.SelectedValue), True, True, True)
        End If
        GenerarCodigoDocumento()

        '****************** OBTENEMOS LOS DATOS DEL DOCUMENTO

        Dim objDocumento As Entidades.Documento = Nothing
        If (IdDocumento <> Nothing) Then
            objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
        Else
            objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
        End If

        Dim objRemitente As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdRemitente)
        Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumento.Id)
        Dim objDestinatario As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdDestinatario)
        Dim objPuntoLlegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(objDocumento.Id)

        Me.listaDetalleDocumento = obtenerListaDetalleDocumento_GuiaRemision(objDocumento.Id)
        Me.listaDocumentoRef = (New Negocio.DocGuiaRemision).DocumentoGuiaRemisionSelectDocumentoRef(objDocumento.Id)

        Dim objTransportista As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdTransportista)
        Dim objChofer As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdChofer)
        Dim objVehiculo As Entidades.Vehiculo = (New Negocio.Vehiculo).SelectxIdVehiculo(objDocumento.IdVehiculo)
        Dim objAgente As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdUsuarioComision)

        Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

        '*************** GUARDAMOS EN SESSION

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1
            listaDetalleDocumento(i).Stock = (New Negocio.Util).fx_StockReal(objDocumento.IdEmpresa, objDocumento.IdAlmacen, listaDetalleDocumento(i).IdProducto)
            listaDetalleDocumento(i).StockComprometido = (New Negocio.Util).fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(objDocumento.IdEmpresa, objDocumento.IdAlmacen, listaDetalleDocumento(i).IdProducto)
            listaDetalleDocumento(i).StockDisponible = listaDetalleDocumento(i).Stock - listaDetalleDocumento(i).StockComprometido

        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)
        setlistaDocumentoRef(Me.listaDocumentoRef)

        '************** 
        cargarDocumentoRef_GUI(objDocumento, Me.listaDocumentoRef, objRemitente, objPuntoPartida, objDestinatario, objPuntoLlegada, Me.listaDetalleDocumento, objTransportista, objChofer, objVehiculo, objAgente, objObservaciones, True)
        verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, False)

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        Dim activarControlesRef As Boolean = False
        If (Me.GV_DocumentoRef.Rows.Count <= 0) Then
            activarControlesRef = True
        End If

        '***************** CONFIGURAMOS LOS CONTROLES
        Me.cboEmpresa.Enabled = activarControlesRef
        Me.cboTienda.Enabled = activarControlesRef
        Me.cboAlmacen.Enabled = activarControlesRef
        Me.cboTipoOperacion.Enabled = activarControlesRef
        Me.btnBuscarRemitente.Enabled = activarControlesRef
        Me.btnBuscarDestinatario.Enabled = activarControlesRef
        Me.btnBuscarProducto.Enabled = activarControlesRef
        Me.btnLimpiarDetalleDocumento.Enabled = activarControlesRef
    End Sub

    Private Sub ValidarPermisos()

        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION / FECHA TRASLADO / OPCION MOVER STOCK FISICO / DESPACHAR / EDITAR DOC CON FECHAS ANTERIORES


        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {59, 60, 61, 62, 63, 147, 148, 143, 197, 2101100008})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True

        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False

        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If listaPermisos(5) > 0 Then  '********* FECHA TRASLADO
            Me.txtFechaInicioTraslado.Enabled = True
        Else
            Me.txtFechaInicioTraslado.Enabled = False
        End If

        If listaPermisos(6) > 0 Then  '********* OPCION MOVER STOCK FISICO
            Me.chb_MoverAlmacen.Enabled = True
        Else
            Me.chb_MoverAlmacen.Enabled = False
        End If

        If listaPermisos(7) > 0 Then  '********* DESPACHAR
            Me.btnBuscarDocumentoRef.Enabled = True
        Else
            Me.btnBuscarDocumentoRef.Enabled = False
        End If

        If listaPermisos(8) > 0 Then  '********* DESPACHAR
            Me.rbl_UtilizarRedondeo.Enabled = True
        Else
            Me.rbl_UtilizarRedondeo.Enabled = False
        End If


    End Sub

    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

                .LLenarCboEstadoDocumento(Me.cboEstado)
                '.llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True)
                'Buscar tipo de operación por perfiles                                
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True, CInt(Session("IdUsuario")))
                .LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)

                '************** PUNTO DE LLEGADA
                .LLenarCboDepartamento(Me.cboDepto_Llegada)
                .LLenarCboProvincia(Me.cboProvincia_Llegada, (Me.cboDepto_Llegada.SelectedValue))
                .LLenarCboDistrito(Me.cboDistrito_Llegada, (Me.cboDepto_Llegada.SelectedValue), (Me.cboProvincia_Llegada.SelectedValue))

                '*************** PUNTO DE PARTIDA (SOLO DEPTO)
                .LLenarCboDepartamento(Me.cboDepto_Partida)

                .LLenarCboTipoDocumentoRefxIdTipoDocumento(CboTipoDocumento, CInt(hddIdTipoDocumento.Value), 2, False)
                .llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

                '.LlenarCboLinea(Me.cmbLinea_AddProd, True)
                .llenarCboTipoExistencia(CboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
                '.LlenarCboLinea(Me.cmbLinea_AddProd, True)
                '.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
                '.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
                .LlenarCboTipoAlmacen(Me.cboTipoAlmacen, False)
                .LlenarCboRol(Ddl_Rol, True)

                .llenarCboUnidadMedidaxIdMagnitud(Me.cboUnidadMedida_PesoTotal, _IdMagnitud_Peso, False)

            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicioTraslado.Text = Me.txtFechaEmision.Text

            actualizarOpcionesBusquedaDocRef(0, False)

            If (Request.QueryString("IdDocumentoRef") = Nothing) Then
                Me.hddOpcionBusquedaPersona.Value = "0" '************* REMITENTE
                Me.hddOpcionBusquedaAgente.Value = "0" '*******agente
                cargarPersona(CInt(Me.cboEmpresa.SelectedValue))

            End If

            Me.hddNroFilasxDocumento.Value = CStr((New Negocio.DocumentoView).SelectCantidadFilasDocumento(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.hddIdTipoDocumento.Value)))

            verFrm(FrmModo.Nuevo, False, True, True, True, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean, Optional ByVal inicializarRemitente As Boolean = False)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then

            Session.Remove(CStr(ViewState("DetalleDocumento")))
            Session.Remove(CStr(ViewState("listaDocumentoRef")))

        End If

        If (initSession) Then
            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setlistaDocumentoRef(Me.listaDocumentoRef)
        End If

        If (initParametrosGral) Then

            ''******* FECHA DE VCTO
            'Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {1, 2})

        End If

        If (inicializarRemitente) Then

            Me.hddOpcionBusquedaPersona.Value = "0"
            Me.hddOpcionBusquedaAgente.Value = "0"
            cargarPersona(CInt(Me.cboEmpresa.SelectedValue), False)

        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()
        Me.LblcantidadRegistros.Text = ""

    End Sub
    Private Sub ActualizarBotonesControl()

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False

                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Transportista.Enabled = False
                Me.Panel_Chofer.Enabled = False
                Me.Panel_Vehiculo.Enabled = False
                Me.Panel_Agente.Enabled = False
                Me.Panel_Obs.Enabled = False


                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"

                '************* GENERAMOS CODIGO
                GenerarCodigoDocumento()

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False



                Me.btnBuscarDocumentoRef.Visible = True


                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_Remitente.Enabled = True
                Me.Panel_PuntoPartida.Enabled = True
                Me.Panel_Destinatario.Enabled = True
                Me.Panel_PuntoLlegada.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Transportista.Enabled = True
                Me.Panel_Chofer.Enabled = True
                Me.Panel_Vehiculo.Enabled = True
                Me.Panel_Agente.Enabled = True
                Me.Panel_Obs.Enabled = True


                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"

                '************* GENERAMOS CODIGO
                GenerarCodigoDocumento()

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboAlmacen.Enabled = True
                Me.cboTipoOperacion.Enabled = True
                Me.btnBuscarRemitente.Enabled = True
                Me.btnBuscarDestinatario.Enabled = True
                Me.btnBuscarProducto.Enabled = True
                Me.btnLimpiarDetalleDocumento.Enabled = True

            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False

                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True

                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_Remitente.Enabled = True
                Me.Panel_PuntoPartida.Enabled = True
                Me.Panel_Destinatario.Enabled = True
                Me.Panel_PuntoLlegada.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Transportista.Enabled = True
                Me.Panel_Chofer.Enabled = True
                Me.Panel_Vehiculo.Enabled = True
                Me.Panel_Agente.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False


                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Transportista.Enabled = False
                Me.Panel_Chofer.Enabled = False
                Me.Panel_Vehiculo.Enabled = False
                Me.Panel_Agente.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True


            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True

                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Transportista.Enabled = False
                Me.Panel_Chofer.Enabled = False
                Me.Panel_Vehiculo.Enabled = False
                Me.Panel_Agente.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False

                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Transportista.Enabled = False
                Me.Panel_Chofer.Enabled = False
                Me.Panel_Vehiculo.Enabled = False
                Me.Panel_Agente.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False

                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_Remitente.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Destinatario.Enabled = False
                Me.Panel_PuntoLlegada.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Transportista.Enabled = False
                Me.Panel_Chofer.Enabled = False
                Me.Panel_Vehiculo.Enabled = False
                Me.Panel_Agente.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboEstado.SelectedValue = "2"

        End Select
    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub


    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaInicioTraslado.Text = Me.txtFechaEmision.Text
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto


        '************** DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        '************ REMITENTE
        Me.txtNombre_Remitente.Text = ""
        Me.txtIdRemitente.Text = ""
        Me.txtDNI_Remitente.Text = ""
        Me.txtRUC_Remitente.Text = ""

        '*********** PUNTO PARTIDA
        Me.cboDepto_Partida.SelectedIndex = 0
        Me.cboProvincia_Partida.SelectedIndex = 0
        Me.cboDistrito_Partida.SelectedIndex = 0
        Me.txtDireccion_Partida.Text = ""

        '************ DESTINATARIO
        Me.txtDestinatario.Text = ""
        Me.txtIdDestinatario.Text = ""
        Me.txtDni_Destinatario.Text = ""
        Me.txtRuc_Destinatario.Text = ""


        '*********** PUNTO LLEGADA
        Me.cboDepto_Llegada.SelectedIndex = 0
        Me.cboProvincia_Llegada.SelectedIndex = 0
        Me.cboDistrito_Llegada.SelectedIndex = 0
        Me.txtDireccion_Llegada.Text = ""


        '*********** DETALLES
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()
        Me.txtPesoTotal.Text = ""


        '************ TRANSPORTISTA
        Me.txtTransportista.Text = ""
        Me.txtDni_Transportista.Text = ""
        Me.txtRuc_Transportista.Text = ""
        Me.txtDireccion_Transportista.Text = ""


        '************ CHOFER
        Me.txtChofer.Text = ""
        Me.txtDni_Chofer.Text = ""
        Me.txtRuc_Chofer.Text = ""
        Me.txtNroLicencia.Text = ""

        '************ VEHICULO
        Me.txtModelo_Vehiculo.Text = ""
        Me.txtPlaca_Vehiculo.Text = ""
        Me.txtCertificado_Vehiculo.Text = ""

        '************ AGENTE
        Me.txtAgente.Text = ""
        Me.txtDni_Agente.Text = ""
        Me.txtRuc_Agente.Text = ""
        Me.txtDireccion_Agente.Text = ""

        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""


        '************* HIDDEN
        Me.hddCodigoDocumento.Value = ""
        Me.hddIdChofer.Value = ""
        Me.hddIdDestinatario.Value = ""
        Me.hddIdDocumento.Value = ""
        Me.hddIdRemitente.Value = ""
        Me.hddIdTransportista.Value = ""
        Me.hddIdVehiculo.Value = ""
        Me.hddIdAgente.Value = ""
        Me.hddIndex_GV_Detalle.Value = ""
        Me.hddIndexGlosa_Gv_Detalle.Value = ""
        Me.hddOpcionBusquedaPersona.Value = "0"  '**** REMITENTE
        Me.hddOpcionBusquedaAgente.Value = "0"  '**** agente
        Me.hddPoseeGRecepcion.Value = "0"



        '************* OTROS
        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()
        Me.gvBuscar.DataSource = Nothing
        Me.gvBuscar.DataBind()
        Me.GV_Vehiculo.DataSource = Nothing
        Me.GV_Vehiculo.DataBind()

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            If (CInt(Me.cboEmpresa.SelectedValue) = CInt(Me.hddIdRemitente.Value)) Then
                actualizarPuntoPartidaxIdAlmacen(CInt(Me.cboAlmacen.SelectedValue), True, True, True)
            End If

            GenerarCodigoDocumento()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            If (CInt(Me.cboEmpresa.SelectedValue) = CInt(Me.hddIdRemitente.Value)) Then
                actualizarPuntoPartidaxIdAlmacen(CInt(Me.cboAlmacen.SelectedValue), True, True, True)
            End If

            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)

        If (CInt(Me.hddOpcionBusquedaPersona.Value) = 3) Then   '*********** CHOFER
            listaPersona = (New Negocio.Chofer).ChoferSelectActivoxParams(dni, ruc, RazonApe, tipo, index, gv.PageSize, estado)
        Else
            listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)
        End If

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub


    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
            'Crea los controles dinamicos y los vuelve a cargar
            crearControlesTextbox()
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
            'Crea los controles dinamicos y los vuelve a cargar
            crearControlesTextbox()
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
            'Crea los controles dinamicos y los vuelve a cargar
            crearControlesTextbox()
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub gvBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuscar.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.gvBuscar.SelectedRow.Cells(1).Text))
            'Crea los controles dinamicos y los vuelve a cargar
            crearControlesTextbox()
        Catch ex As Exception
            'Crea los controles dinamicos y los vuelve a cargar
            crearControlesTextbox()
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub cargarPersona(ByVal IdPersona As Integer, Optional ByVal OpenWindow As Boolean = True, Optional ByVal UpdateDestinatarioPuntoLLegada As Boolean = True)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")

        Dim objCbo As New Combo

        Select Case CInt(Me.hddOpcionBusquedaPersona.Value)

            Case -1 '****** AGENCIA TRANSPORTE

                Me.txtAgente.Text = objPersona.Descripcion
                Me.hddIdAgente.Value = CStr(objPersona.IdPersona)
                Me.txtDni_Agente.Text = objPersona.Dni
                Me.txtRuc_Agente.Text = objPersona.Ruc
                Me.txtDireccion_Agente.Text = objPersona.Direccion

            Case 0  '****** REMITENTE
                Me.txtNombre_Remitente.Text = objPersona.Descripcion
                Me.txtIdRemitente.Text = CStr(objPersona.IdPersona)
                Me.txtDNI_Remitente.Text = objPersona.Dni
                Me.txtRUC_Remitente.Text = objPersona.Ruc
                Me.hddIdRemitente.Value = CStr(objPersona.IdPersona)

                If (CInt(Me.cboEmpresa.SelectedValue) = objPersona.IdPersona) Then

                    actualizarPuntoPartidaxIdAlmacen(CInt(Me.cboAlmacen.SelectedValue), True, True, True)

                Else
                    '**************** PARTIDA
                    Dim ubigeo As String = objPersona.Ubigeo
                    If (ubigeo.Trim.Length <= 0) Then
                        ubigeo = "000000"
                    End If
                    Dim codDepto As String = ubigeo.Substring(0, 2)
                    Dim codProv As String = ubigeo.Substring(2, 2)
                    Dim codDist As String = ubigeo.Substring(4, 2)


                    Me.cboDepto_Partida.SelectedValue = codDepto
                    Me.cboProvincia_Partida.Items.Clear()
                    objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, codDepto)
                    Me.cboProvincia_Partida.SelectedValue = codProv
                    Me.cboDistrito_Partida.Items.Clear()
                    objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, codDepto, codProv)
                    Me.cboDistrito_Partida.SelectedValue = codDist
                    Me.txtDireccion_Partida.Text = objPersona.Direccion
                End If

            Case 1  '****** DESTINATARIO
                Me.txtDestinatario.Text = objPersona.Descripcion
                Me.txtIdDestinatario.Text = CStr(objPersona.IdPersona)
                Me.txtDni_Destinatario.Text = objPersona.Dni
                Me.txtRuc_Destinatario.Text = objPersona.Ruc
                Me.hddIdDestinatario.Value = CStr(objPersona.IdPersona)

                If UpdateDestinatarioPuntoLLegada Then

                    Try
                        objCbo.LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen_Destinatario, CInt(Me.cboTipoAlmacen.SelectedValue), CInt(objPersona.IdPersona), False)
                    Catch ex As Exception

                    End Try

                    If (objPersona.Propietario) Then

                        actualizarPuntoLlegadaxIdAlmacen(CInt(Me.cboAlmacen_Destinatario.SelectedValue), True, True, True)

                    Else
                        '**************** LLEGADA
                        Dim ubigeo As String = objPersona.Ubigeo
                        If (ubigeo.Trim.Length <= 0) Then
                            ubigeo = "000000"
                        End If
                        Dim codDepto As String = ubigeo.Substring(0, 2)
                        Dim codProv As String = ubigeo.Substring(2, 2)
                        Dim codDist As String = ubigeo.Substring(4, 2)
                        Me.cboDepto_Llegada.SelectedValue = codDepto
                        Me.cboProvincia_Llegada.Items.Clear()
                        objCbo.LLenarCboProvincia(Me.cboProvincia_Llegada, codDepto)
                        Me.cboProvincia_Llegada.SelectedValue = codProv
                        Me.cboDistrito_Llegada.Items.Clear()
                        objCbo.LLenarCboDistrito(Me.cboDistrito_Llegada, codDepto, codProv)
                        Me.cboDistrito_Llegada.SelectedValue = codDist
                        Me.txtDireccion_Llegada.Text = objPersona.Direccion
                    End If
                End If
            Case 2 '******* TRANSPORTISTA

                Me.txtTransportista.Text = objPersona.Descripcion
                Me.txtDni_Transportista.Text = objPersona.Dni
                Me.txtRuc_Transportista.Text = objPersona.Ruc
                Me.txtDireccion_Transportista.Text = objPersona.Direccion
                Me.hddIdTransportista.Value = CStr(objPersona.IdPersona)

            Case 3 '******* CHOFER

                Me.txtChofer.Text = objPersona.Descripcion
                Me.txtDni_Chofer.Text = objPersona.Dni
                Me.txtRuc_Chofer.Text = objPersona.Ruc
                Me.txtNroLicencia.Text = objPersona.NroLicencia
                Me.hddIdChofer.Value = CStr(objPersona.IdPersona)

        End Select

        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        If OpenWindow = True Then
            objScript.offCapa(Me, "capaPersona")
        End If

    End Sub

#End Region


#Region "************************** BUSQUEDA PRODUCTO"

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacen.SelectedValue)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia", Me.CboTipoExistencia.SelectedValue)
            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
            '            crearControlesTextbox()
            'ActualizarListaDetalleDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, _
                                          ByVal nomProducto As String, ByVal IdAlmacen As Integer, _
                                          ByVal tipoMov As Integer, ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        'Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)


        If listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen.SelectedValue), 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub mostrarCapaStockPrecioxProducto(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProductoConsultarStockPrecioxProd.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)

            Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, 0, 0, (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue))
            Me.GV_ConsultarStockPrecioxProd.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarStockPrecioxProducto');   onCapa('capaBuscarProducto_AddProd');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region


    Private Sub addProducto_DetalleDocumento()
        Try

            '************* ACTUALIZAMOS LA GRILLA
            ActualizarListaDetalleDocumento()

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim listaUnidadMedida_Peso As List(Of Entidades.MagnitudUnidad) = (New Negocio.MagnitudUnidad)._MagnitudUnidadselectxIdMagnitud(_IdMagnitud_Peso)

            For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

                '***************** UNIDAD MEDIDA PRINCIPAL
                Dim IdUnidadMedida_ST As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdUnidadMedida_PR"), HiddenField).Value)

                Dim cantidad As Decimal = CDec(CType(Me.DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text)

                If (cantidad > 0) Then

                    Dim IdProducto_Find As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdProducto_Find"), HiddenField).Value)
                    Dim IdUnidadMedida_Find As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
                    Dim Kit_Find As Boolean = False : Try : Kit_Find = CBool(CType(Me.DGV_AddProd.Rows(i).FindControl("hddKit"), HiddenField).Value) : Catch ex As Exception : End Try

                    Dim valCant As Boolean = False
                    For k As Integer = 0 To listaDetalleDocumento.Count - 1

                        If listaDetalleDocumento(k).IdProducto = IdProducto_Find And listaDetalleDocumento(k).IdUnidadMedida = IdUnidadMedida_Find Then

                            listaDetalleDocumento(k).Cantidad = listaDetalleDocumento(k).Cantidad + cantidad
                            listaDetalleDocumento(k).Kit = Kit_Find

                            '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                            listaDetalleDocumento(k).Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(listaDetalleDocumento(k).IdProducto, IdUnidadMedida_ST, 1, _IdMagnitud_Peso, listaDetalleDocumento(k).IdUnidadMedida_Peso)

                            '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                            listaDetalleDocumento(k).Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(listaDetalleDocumento(k).IdProducto, listaDetalleDocumento(k).IdUnidadMedida, IdUnidadMedida_ST, 1)

                            listaDetalleDocumento(k).Peso = listaDetalleDocumento(k).Cantidad * listaDetalleDocumento(k).Equivalencia1 * listaDetalleDocumento(k).Equivalencia

                            listaDetalleDocumento(k).Stock = (New Negocio.Util).fx_StockReal(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), listaDetalleDocumento(k).IdProducto)
                            listaDetalleDocumento(k).StockComprometido = (New Negocio.Util).fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), listaDetalleDocumento(k).IdProducto)
                            listaDetalleDocumento(k).StockDisponible = listaDetalleDocumento(k).Stock - listaDetalleDocumento(k).StockComprometido

                            valCant = True
                            Exit For

                        End If

                    Next

                    If valCant = False Then

                        Dim objDetalle As New Entidades.DetalleDocumento
                        With objDetalle

                            .IdUnidadMedidaPrincipal = IdUnidadMedida_ST
                            .IdProducto = IdProducto_Find
                            .Kit = Kit_Find

                            .NomProducto = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(2).Text))
                            .Cantidad = cantidad
                            .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(.IdProducto)
                            .listaTonos = (New Negocio.bl_Tonos).SelectTonosxProductos_Nuevo(.IdProducto, Me.cboAlmacen.SelectedValue, hddIdDocumento.Value)
                            .IdUnidadMedida = IdUnidadMedida_Find
                            .CodigoProducto = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(1).Text))

                            .ListaUnidadMedida_Peso = listaUnidadMedida_Peso
                            .IdUnidadMedida_Peso = CInt(Me.cboUnidadMedida_PesoTotal.SelectedValue)

                            '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                            .Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(.IdProducto, IdUnidadMedida_ST, 1, _IdMagnitud_Peso, .IdUnidadMedida_Peso)

                            '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                            .Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUnidadMedida, IdUnidadMedida_ST, 1)

                            .Peso = .Cantidad * .Equivalencia1 * .Equivalencia

                            .Stock = (New Negocio.Util).fx_StockReal(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), objDetalle.IdProducto)

                            .StockComprometido = (New Negocio.Util).fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), objDetalle.IdProducto)
                            .StockDisponible = .Stock - .StockComprometido

                            Try : .UnidadMedidaPrincipal = (New Negocio.UnidadMedida).SelectxId(IdUnidadMedida_ST)(0).DescripcionCorto : Catch ex As Exception : End Try


                            Me.listaDetalleDocumento.Add(objDetalle)

                        End With

                    End If

                    If Kit_Find Then ' AGREGANDO COMPONENTES

                        Dim ListaKit As List(Of Entidades.Kit) = (New Negocio.Kit).SelectComponentexIdKit(IdProducto_Find)

                        For k As Integer = 0 To ListaKit.Count - 1


                            Dim valCant2 As Boolean = False
                            For z As Integer = 0 To listaDetalleDocumento.Count - 1
                                If listaDetalleDocumento(z).IdProducto = ListaKit(k).IdComponente And listaDetalleDocumento(z).IdUnidadMedida = ListaKit(k).IdUnidadMedida_Comp Then
                                    listaDetalleDocumento(z).Cantidad = listaDetalleDocumento(z).Cantidad + cantidad

                                    '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                                    listaDetalleDocumento(z).Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(listaDetalleDocumento(z).IdProducto, IdUnidadMedida_ST, 1, _IdMagnitud_Peso, listaDetalleDocumento(z).IdUnidadMedida_Peso)

                                    '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                                    listaDetalleDocumento(z).Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(listaDetalleDocumento(z).IdProducto, listaDetalleDocumento(z).IdUnidadMedida, IdUnidadMedida_ST, 1)

                                    listaDetalleDocumento(z).Peso = listaDetalleDocumento(z).Cantidad * listaDetalleDocumento(z).Equivalencia1 * listaDetalleDocumento(z).Equivalencia
                                    listaDetalleDocumento(z).Stock = (New Negocio.Util).fx_StockReal(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), listaDetalleDocumento(z).IdProducto)

                                    listaDetalleDocumento(z).StockComprometido = (New Negocio.Util).fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), listaDetalleDocumento(z).IdProducto)
                                    listaDetalleDocumento(z).StockDisponible = listaDetalleDocumento(z).Stock - listaDetalleDocumento(z).StockComprometido

                                    valCant2 = True
                                    Exit For
                                End If
                            Next
                            If valCant2 = False Then

                                Dim objDetalle2 As New Entidades.DetalleDocumento
                                With objDetalle2

                                    .IdUnidadMedidaPrincipal = ListaKit(k).IdUnidadMedida_Comp
                                    .IdProducto = ListaKit(k).IdComponente
                                    .NomProducto = ListaKit(k).Componente
                                    .Cantidad = ListaKit(k).Cantidad_Comp * cantidad
                                    .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(.IdProducto)
                                    .IdUnidadMedida = ListaKit(k).IdUnidadMedida_Comp
                                    .CodigoProducto = ListaKit(k).CodigoProd_Comp
                                    .ListaUnidadMedida_Peso = listaUnidadMedida_Peso
                                    .IdUnidadMedida_Peso = CInt(Me.cboUnidadMedida_PesoTotal.SelectedValue)
                                    .listaTonos = (New Negocio.bl_Tonos).SelectTonosxProductos_Nuevo(.IdProducto, Me.cboAlmacen.SelectedValue, hddIdDocumento.Value)
                                    '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                                    .Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(.IdProducto, IdUnidadMedida_ST, 1, _IdMagnitud_Peso, .IdUnidadMedida_Peso)

                                    '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                                    .Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUnidadMedida, IdUnidadMedida_ST, 1)

                                    .Peso = .Cantidad * .Equivalencia1 * .Equivalencia
                                    .Stock = (New Negocio.Util).fx_StockReal(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), objDetalle2.IdProducto)
                                    .StockComprometido = (New Negocio.Util).fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), objDetalle2.IdProducto)
                                    .Stock = .Stock - .StockComprometido

                                    Try : .UnidadMedidaPrincipal = (New Negocio.UnidadMedida).SelectxId(objDetalle2.IdUnidadMedidaPrincipal)(0).DescripcionCorto : Catch ex As Exception : End Try

                                End With

                                Me.listaDetalleDocumento.Add(objDetalle2)
                            End If
                        Next

                    End If 'KIT

                End If ' CANTIDAD
            Next

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", " calcularPesoTotal('1',null);  onCapa('capaBuscarProducto_AddProd');    ", True)


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function BuscarControl(ByVal pform As Control, ByVal pControlPadre As Control, ByVal pControlNombre As String) As Control
        If pControlPadre.ID = pControlNombre Then
            Return pControlPadre
        End If

        For Each subcontrol As Control In pControlPadre.Controls
            Dim resultado As Control = BuscarControl(pform, subcontrol, pControlNombre)
            If Not IsNothing(resultado) Then
                Return resultado
            End If
        Next
        Return Nothing
    End Function

    Private Sub ActualizarListaDetalleDocumento(Optional ByVal Index As Integer = -1)

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            If Index = i Then
                Me.listaDetalleDocumento(i).IdUMold = Me.listaDetalleDocumento(i).IdUnidadMedida
            End If
            Dim cantidad As Decimal = 0
            Dim checkBoxList As CheckBoxList = CType(Me.GV_Detalle.Rows(i).FindControl("checkListTonos"), CheckBoxList)

            For j As Integer = 0 To checkBoxList.Items.Count - 1
                If checkBoxList.Items(j).Selected Then
                    Dim HiddenField As HiddenField = DirectCast(Me.GV_Detalle.Rows(i).FindControl("hddCantidadFinal"), HiddenField)
                    cantidad = HiddenField.Value
                End If
            Next
            If checkBoxList.Items.Count = 0 Then
                Dim HiddenField As HiddenField = DirectCast(Me.GV_Detalle.Rows(i).FindControl("hddCantidadFinal"), HiddenField)
                cantidad = HiddenField.Value
            End If
            Me.listaDetalleDocumento(i).IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)
            ' Me.listaDetalleDocumento(i).Cantidad = cantidad
            Me.listaDetalleDocumento(i).IdUnidadMedida = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
            Me.listaDetalleDocumento(i).UMedida = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedItem.ToString)
        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)
    End Sub

    Private Sub ActualizarListaDetalleDocumentoComboBox(Optional ByVal Index As Integer = -1)

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            If Index = i Then
                Me.listaDetalleDocumento(i).IdUMold = Me.listaDetalleDocumento(i).IdUnidadMedida
            End If
            Dim cantidad As Decimal = 0
            Dim checkBoxList As CheckBoxList = CType(Me.GV_Detalle.Rows(i).FindControl("checkListTonos"), CheckBoxList)

            For j As Integer = 0 To checkBoxList.Items.Count - 1
                If checkBoxList.Items(j).Selected Then
                    Dim HiddenField As HiddenField = DirectCast(Me.GV_Detalle.Rows(i).FindControl("hddCantidadFinal"), HiddenField)
                    cantidad = HiddenField.Value
                End If
            Next
            If checkBoxList.Items.Count = 0 Then
                Dim HiddenField As HiddenField = DirectCast(Me.GV_Detalle.Rows(i).FindControl("hddCantidadFinal"), HiddenField)
                cantidad = HiddenField.Value
            End If
            Me.listaDetalleDocumento(i).IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)
            Me.listaDetalleDocumento(i).Cantidad = cantidad
            Me.listaDetalleDocumento(i).IdUnidadMedida = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
            Me.listaDetalleDocumento(i).UMedida = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedItem.ToString)
        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)
    End Sub

    Protected Sub btnLimpiarDetalleDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarDetalleDocumento.Click
        limpiarDetalleDocumento()
    End Sub
    Private Sub limpiarDetalleDocumento()
        Try

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub DGV_AddProd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_AddProd.RowDataBound

        Try

            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
                cboUM.DataSource = Me.listaProductoView(e.Row.RowIndex).ListaUM
                cboUM.DataBind()
                cboUM.SelectedValue = Me.listaProductoView(e.Row.RowIndex).IdUnidadMedida.ToString

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub cboAlmacen_Destinatario_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboAlmacen_Destinatario.SelectedIndexChanged
        Try
            actualizarPuntoLlegadaxIdAlmacen(CInt(Me.cboAlmacen_Destinatario.SelectedValue), True, True, True)
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.GV_Detalle.DataSource = listaDetalleDocumento
            Me.GV_Detalle.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub actualizarPuntoLlegadaxIdAlmacen(ByVal IdAlmacen As Integer, ByVal updateDepto As Boolean, ByVal updateProv As Boolean, ByVal updateDist As Boolean)

        '******** VALIDAMOS EL ALMACÉN
        If (IdAlmacen = Nothing) Then Return

        Dim objAlmacen As Entidades.Almacen = (New Negocio.Almacen).SelectxId(IdAlmacen)
        Dim ubigeoAlmacen As String = objAlmacen.Ubigeo
        Dim direccionAlmacen As String = objAlmacen.Direccion

        '***************** PUNTO DE LLEGADA
        Dim objCbo As New Combo
        If (ubigeoAlmacen <> Nothing) Then

            If (updateDepto) Then
                Me.cboDepto_Llegada.Items.Clear()
                objCbo.LLenarCboDepartamento(Me.cboDepto_Llegada)
                Me.cboDepto_Llegada.SelectedValue = ubigeoAlmacen.Substring(0, 2)
            End If
            If (updateProv) Then
                Me.cboProvincia_Llegada.Items.Clear()
                objCbo.LLenarCboProvincia(Me.cboProvincia_Llegada, (Me.cboDepto_Llegada.SelectedValue))
                Me.cboProvincia_Llegada.SelectedValue = ubigeoAlmacen.Substring(2, 2)
            End If
            If (updateDist) Then
                Me.cboDistrito_Llegada.Items.Clear()
                objCbo.LLenarCboDistrito(Me.cboDistrito_Llegada, Me.cboDepto_Llegada.SelectedValue, (Me.cboProvincia_Llegada.SelectedValue))
                Me.cboDistrito_Llegada.SelectedValue = ubigeoAlmacen.Substring(4, 2)
            End If

        End If

        Me.txtDireccion_Llegada.Text = direccionAlmacen

    End Sub
    Private Sub cboDepto_Partida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepto_Partida.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, (Me.cboDepto_Partida.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, (Me.cboDepto_Partida.SelectedValue), (Me.cboProvincia_Partida.SelectedValue))
            crearControlesTextbox()
        Catch ex As Exception
            crearControlesTextbox()
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboProvincia_Partida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProvincia_Partida.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, (Me.cboDepto_Partida.SelectedValue), (Me.cboProvincia_Partida.SelectedValue))
            crearControlesTextbox()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
            crearControlesTextbox()
        End Try
    End Sub
    Private Sub cboDepto_Llegada_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepto_Llegada.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboProvincia(Me.cboProvincia_Llegada, (Me.cboDepto_Llegada.SelectedValue))
            objCbo.LLenarCboDistrito(Me.cboDistrito_Llegada, (Me.cboDepto_Llegada.SelectedValue), (Me.cboProvincia_Llegada.SelectedValue))
            crearControlesTextbox()
        Catch ex As Exception
            crearControlesTextbox()
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboProvincia_Llegada_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProvincia_Llegada.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboDistrito(Me.cboDistrito_Llegada, (Me.cboDepto_Llegada.SelectedValue), (Me.cboProvincia_Llegada.SelectedValue))
            crearControlesTextbox()
        Catch ex As Exception
            crearControlesTextbox()
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub cboAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboAlmacen.SelectedIndexChanged
        Try

            If (CInt(Me.cboEmpresa.SelectedValue) = CInt(Me.hddIdRemitente.Value)) Then
                actualizarPuntoPartidaxIdAlmacen(CInt(Me.cboAlmacen.SelectedValue), True, True, True)
            End If

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

            Me.DGV_AddProd.DataSource = Nothing
            Me.DGV_AddProd.DataBind()
            crearControlesTextbox()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarPuntoPartidaxIdAlmacen(ByVal IdAlmacen As Integer, ByVal updateDepto As Boolean, ByVal updateProv As Boolean, ByVal updateDist As Boolean)

        '******** VALIDAMOS EL ALMACÉN
        If (IdAlmacen = Nothing) Then Return

        Dim objAlmacen As Entidades.Almacen = (New Negocio.Almacen).SelectxId(IdAlmacen)
        Dim ubigeoAlmacen As String = objAlmacen.Ubigeo
        Dim direccionAlmacen As String = objAlmacen.Direccion

        '***************** PUNTO DE LLEGADA
        Dim objCbo As New Combo
        If (ubigeoAlmacen <> Nothing) Then

            If (updateDepto) Then
                '****************    Me.cboDepto_Partida.Items.Clear()
                '*****************    objCbo.LLenarCboDepartamento(Me.cboDepto_Partida)  (ES CARGADO EN LA INICIALIZACION DEL FRM)
                Me.cboDepto_Partida.SelectedValue = ubigeoAlmacen.Substring(0, 2)
            End If
            If (updateProv) Then
                Me.cboProvincia_Partida.Items.Clear()
                objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, (Me.cboDepto_Partida.SelectedValue))
                Me.cboProvincia_Partida.SelectedValue = ubigeoAlmacen.Substring(2, 2)
            End If
            If (updateDist) Then
                Me.cboDistrito_Partida.Items.Clear()
                objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, Me.cboDepto_Partida.SelectedValue, (Me.cboProvincia_Partida.SelectedValue))
                Me.cboDistrito_Partida.SelectedValue = ubigeoAlmacen.Substring(4, 2)
            End If

        End If

        Me.txtDireccion_Partida.Text = direccionAlmacen

    End Sub


    Private Sub buscarVehiculo()
        Try

            Dim lista As List(Of Entidades.Vehiculo) = (New Negocio.Vehiculo).SelectActivoxNroPlaca(Me.txtNroPlaca_BuscarVehiculo.Text)
            If (lista.Count <= 0) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaVehiculo');   alert('No se hallaron registros.');     ", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaVehiculo');     ", True)
            End If
            Me.GV_Vehiculo.DataSource = lista
            Me.GV_Vehiculo.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_Vehiculo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Vehiculo.SelectedIndexChanged
        Try
            cargarVehiculo(CInt(CType(Me.GV_Vehiculo.SelectedRow.FindControl("hddIdVehiculo"), HiddenField).Value))
            crearControlesTextbox()
            objScript.offCapa(Me, "capaVehiculo")
        Catch ex As Exception
            crearControlesTextbox()
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarVehiculo(ByVal IdVehiculo As Integer)
        Dim objVehiculo As Entidades.Vehiculo = (New Negocio.Vehiculo).SelectxIdVehiculo(IdVehiculo)
        With objVehiculo
            Me.txtModelo_Vehiculo.Text = .Modelo
            Me.txtPlaca_Vehiculo.Text = .Placa
            Me.txtCertificado_Vehiculo.Text = .ConstanciaIns
            Me.hddIdVehiculo.Value = CStr(.IdProducto)
        End With
    End Sub

    Private Sub btnBuscarVehiculo_Grilla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarVehiculo_Grilla.Click
        buscarVehiculo()
        crearControlesTextbox()
    End Sub

#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click
        mostrarDocumentosRef_Find()
    End Sub

    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As Integer = 0
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing
            Dim IdPersona As Integer = 0

            If (Me.hddIdDestinatario.Value.Trim.Length > 0 And IsNumeric(Me.hddIdDestinatario.Value)) Then
                If (CInt(Me.hddIdDestinatario.Value) > 0) Then
                    IdPersona = CInt(Me.hddIdDestinatario.Value)
                End If
            End If

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CInt(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            '***********Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), IdPersona, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, fechaInicio, fechafin, False)
            ' Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).DocumentoSelectDocxDespachar_V2(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), fechaInicio, fechafin, serie, codigo, IdPersona, CInt(Me.hddIdTipoDocumento.Value), CInt(CboTipoDocumento.SelectedValue))

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).DocumentoSelectDocxDespachar_picking(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), fechaInicio, fechafin, serie, codigo, IdPersona, CInt(Me.hddIdTipoDocumento.Value), CInt(Me.cboTipoOperacion.SelectedValue), CInt(CboTipoDocumento.SelectedValue))
            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub

    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub

    Private Sub GV_DocumentosReferencia_Find_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_DocumentosReferencia_Find.PageIndexChanging
        Me.GV_DocumentosReferencia_Find.PageIndex = e.NewPageIndex
        mostrarDocumentosRef_Find()
    End Sub



    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value) _
        , CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdEmpresa_Find"), HiddenField).Value) _
        , CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdAlmacen_Find"), HiddenField).Value) _
        , CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdPersona_Find"), HiddenField).Value))
        objScript.onCapa(Me, "capaDocumentosReferencia")
    End Sub



    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdPersona As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

        Next

        If ((CInt(Me.cboEmpresa.SelectedValue) <> IdEmpresa) Or (CInt(Me.cboAlmacen_Destinatario.SelectedValue) <> IdAlmacen)) Then Throw New Exception("EL DOCUMENTO SELECCIONADO POSEE EMPRESA/ALMACÉN DISTINTOS A LOS SELECCIONADOS. NO SE PERMITE LA OPERACIÓN.")

        If (CInt(Me.hddIdDestinatario.Value) <> IdPersona) Then Throw New Exception("EL DOCUMENTO SELECCIONADO POSEE UN DESTINATARIO DISTINTO AL SELECCIONADO.")

        Return True

    End Function
    ', ByVal IdTipoOperacion As Integer

    Private Function obtenerListaDetalleDocumento_Ref(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).DetalleDocumentoSelectDetallexAtenderxIdDocumentoDetalle(IdDocumento, CInt(Me.cboTipoOperacion.SelectedValue))

        For i As Integer = 0 To listaDetalle.Count - 1

            listaDetalle(i).ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)
            '
            'Carga tonos segun el id del producto y almacen
            '
            listaDetalle(i).listaTonos = (New Negocio.bl_Tonos).SelectTonosxProductos_Nuevo(listaDetalle(i).IdProducto, listaDetalle(i).IdAlmacen, IdDocumento)
            'Dim lista As List(Of Entidades.be_tonoXProducto) = listaDetalle(i).listaTonos
            'For j As Integer = 0 To lista.Count - 1
            '    listaDetalle(i).cantidadTono = lista(j).cantidadTono
            'Next

            If (CInt(Me.cboTipoOperacion.SelectedValue) = 2 Or CInt(Me.cboTipoOperacion.SelectedValue) = 4 Or CInt(Me.cboTipoOperacion.SelectedValue) = 5) Then
                listaDetalle(i).Cantidad = listaDetalle(i).Cantidad
            Else
                listaDetalle(i).Cantidad = listaDetalle(i).CantxAtender
            End If
            listaDetalle(i).CantidadxAtenderText = CStr(Math.Round(listaDetalle(i).CantxAtender, 4)) + " " + listaDetalle(i).UMedida
            listaDetalle(i).IdDetalleAfecto = listaDetalle(i).IdDetalleDocumento

        Next

        Return listaDetalle

    End Function
    Private Sub cargarDocumentoRef_GUI(ByVal objDocumento As Entidades.Documento, ByVal listaDocumento As List(Of Entidades.Documento), ByVal objRemitente As Entidades.PersonaView, ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objDestinatario As Entidades.PersonaView, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objTransportista As Entidades.PersonaView, ByVal objChofer As Entidades.PersonaView, ByVal objVehiculo As Entidades.Vehiculo, ByVal objAgente As Entidades.PersonaView, ByVal objObservaciones As Entidades.Observacion, ByVal cargarGuiaRemision As Boolean)

        Dim objCbo As New Combo

        With objDocumento

            If (Me.cboEmpresa.Items.FindByValue(CStr(objDocumento.IdEmpresa)) IsNot Nothing) Then
                Me.cboEmpresa.SelectedValue = CStr(objDocumento.IdEmpresa)
            End If

            If (Me.cboAlmacen.Items.FindByValue(CStr(objDocumento.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacen.SelectedValue = CStr(objDocumento.IdAlmacen)
            End If

            Dim cargarOperacion As Boolean = True

            If .IdTipoDocumento = 25 And CInt(Me.cboTipoOperacion.SelectedValue) = 5 Then cargarOperacion = False

            If cargarOperacion Then

                If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
                    Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
                End If

            End If

            objCbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)
            If (Me.cboMotivoTraslado.Items.FindByValue(CStr(objDocumento.IdMotivoT)) IsNot Nothing) Then
                Me.cboMotivoTraslado.SelectedValue = CStr(objDocumento.IdMotivoT)
            End If

            If (cargarGuiaRemision) Then

                Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)
                Me.txtCodigoDocumento.Text = .Codigo

                If (.FechaEmision <> Nothing) Then
                    Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
                End If
                If (.FechaIniTraslado <> Nothing) Then
                    Me.txtFechaInicioTraslado.Text = Format(.FechaIniTraslado, "dd/MM/yyyy")
                End If

                Me.hddIdDocumento.Value = CStr(.Id)
                Me.hddCodigoDocumento.Value = .Codigo
                If .PoseeGRecepcion = True Then Me.hddPoseeGRecepcion.Value = "1"

            End If

        End With


        '******************* REMITENTE
        If (objRemitente IsNot Nothing) Then

            Me.txtNombre_Remitente.Text = objRemitente.Descripcion
            Me.txtDNI_Remitente.Text = objRemitente.Dni
            Me.txtRUC_Remitente.Text = objRemitente.Ruc
            Me.txtIdRemitente.Text = CStr(objRemitente.IdPersona)
            Me.hddIdRemitente.Value = CStr(objRemitente.IdPersona)

        End If

        '****************** PUNTO PARTIDA
        If (objPuntoPartida IsNot Nothing) Then

            If objPuntoPartida.Ubigeo.Length > 0 And objPuntoPartida.Ubigeo <> "000000" And objPuntoPartida.Direccion.Length > 0 Then

                If (Me.cboDepto_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(0, 2))) IsNot Nothing) Then
                    Me.cboDepto_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(0, 2)
                    objCbo.LLenarCboProvincia(Me.cboProvincia_Partida, Me.cboDepto_Partida.SelectedValue)
                End If

                If (Me.cboProvincia_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(2, 2))) IsNot Nothing) Then
                    Me.cboProvincia_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(2, 2)
                    objCbo.LLenarCboDistrito(Me.cboDistrito_Partida, Me.cboDepto_Partida.SelectedValue, Me.cboProvincia_Partida.SelectedValue)
                End If

                If (Me.cboDistrito_Partida.Items.FindByValue(CStr(objPuntoPartida.Ubigeo.Substring(4, 2))) IsNot Nothing) Then
                    Me.cboDistrito_Partida.SelectedValue = objPuntoPartida.Ubigeo.Substring(4, 2)
                End If

                Me.txtDireccion_Partida.Text = objPuntoPartida.Direccion

            End If
        End If

        Dim CargarDestinatario As Boolean = True

        For i As Integer = 0 To listaDocumento.Count - 1

            If listaDocumento(i).IdTipoDocumento = 15 And CInt(Me.cboTipoOperacion.SelectedValue) = 1 Then ' ***************** OP/VP - VENTA

                Dim ListaRelacionDocumento As List(Of Entidades.RelacionDocumento) = (New Negocio.RelacionDocumento).Documento_Referencia(listaDocumento(i).Id)

                For k As Integer = 0 To ListaRelacionDocumento.Count - 1

                    With ListaRelacionDocumento(k)
                        If .IdTipoDocumento = 1 Or .IdTipoDocumento = 3 Or .IdTipoDocumento = 57 Or .IdTipoDocumento = 1101353001 Or .IdTipoDocumento = 1101353002 Then ' ********** F/V **********  B/V

                            Dim objDoc_Despacho As Entidades.Documento = (New Negocio.Documento).SelectActivoxIdDocumento(.IdDocumento)
                            If Not IsNothing(objDoc_Despacho) Then

                                If objDoc_Despacho.IdPersona > 0 Then
                                    Me.hddOpcionBusquedaPersona.Value = "1"  ' ************ DESTINATARIO
                                    cargarPersona(objDoc_Despacho.IdPersona, False, False)
                                    CargarDestinatario = False
                                    Exit For
                                End If

                            End If

                        End If
                    End With

                Next

            End If

        Next

        If CargarDestinatario Then

            '***************** DESTINATARIO
            If (objDestinatario IsNot Nothing) Then

                Me.txtDestinatario.Text = objDestinatario.Descripcion
                Me.txtDni_Destinatario.Text = objDestinatario.Dni
                Me.txtRuc_Destinatario.Text = objDestinatario.Ruc
                Me.txtIdDestinatario.Text = CStr(objDestinatario.IdPersona)
                Me.hddIdDestinatario.Value = CStr(objDestinatario.IdPersona)

                objCbo.llenarCboAlmacenxIdEmpresa(Me.cboAlmacen_Destinatario, CInt(objDestinatario.IdPersona), False)

            End If

        End If

        '***************** PUNTO DE LLEGADA
        If (objPuntoLlegada IsNot Nothing) Then

            If (Me.cboDepto_Llegada.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(0, 2))) IsNot Nothing) Then
                Me.cboDepto_Llegada.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(0, 2)
                Me.cboProvincia_Llegada.Items.Clear()
                objCbo.LLenarCboProvincia(Me.cboProvincia_Llegada, Me.cboDepto_Llegada.SelectedValue)
            End If

            If (Me.cboProvincia_Llegada.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(2, 2))) IsNot Nothing) Then
                Me.cboProvincia_Llegada.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(2, 2)
                Me.cboDistrito_Llegada.Items.Clear()
                objCbo.LLenarCboDistrito(Me.cboDistrito_Llegada, Me.cboDepto_Llegada.SelectedValue, Me.cboProvincia_Llegada.SelectedValue)
            End If

            If (Me.cboDistrito_Llegada.Items.FindByValue(CStr(objPuntoLlegada.pll_Ubigeo.Substring(4, 2))) IsNot Nothing) Then
                Me.cboDistrito_Llegada.SelectedValue = objPuntoLlegada.pll_Ubigeo.Substring(4, 2)
            End If

            Me.txtDireccion_Llegada.Text = objPuntoLlegada.pll_Direccion

            '****************** ALMACEN DE LLEGADA
            If (Me.cboTipoAlmacen.Items.FindByValue(CStr(objPuntoLlegada.IdTipoAlmacen)) IsNot Nothing) Then
                If (objPuntoPartida IsNot Nothing) Then
                    Me.cboTipoAlmacen.SelectedValue = CStr(objPuntoLlegada.IdTipoAlmacen)
                End If
                objCbo.LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen_Destinatario, CInt(Me.cboTipoAlmacen.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
            End If

            If (Me.cboAlmacen_Destinatario.Items.FindByValue(CStr(objPuntoLlegada.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacen_Destinatario.SelectedValue = CStr(objPuntoLlegada.IdAlmacen)
            End If

        End If



        Me.GV_Detalle.DataSource = listaDetalleDocumento
        Me.GV_Detalle.DataBind()

        '******************* TRANSPORTISTA
        If (objTransportista IsNot Nothing) Then

            Me.txtTransportista.Text = objTransportista.Descripcion
            Me.txtDni_Transportista.Text = objTransportista.Dni
            Me.txtRuc_Transportista.Text = objTransportista.Ruc
            Me.txtDireccion_Transportista.Text = objTransportista.Direccion
            Me.hddIdTransportista.Value = CStr(objTransportista.IdPersona)

        End If

        '******************* CHOFER
        If (objChofer IsNot Nothing) Then

            Me.txtChofer.Text = objChofer.Descripcion
            Me.txtDni_Chofer.Text = objChofer.Dni
            Me.txtRuc_Chofer.Text = objChofer.Ruc
            Me.txtNroLicencia.Text = objChofer.NroLicencia
            Me.hddIdChofer.Value = CStr(objChofer.IdPersona)

        End If

        '******************* VEHICULO
        If (objVehiculo IsNot Nothing) Then

            Me.txtModelo_Vehiculo.Text = objVehiculo.Modelo
            Me.txtPlaca_Vehiculo.Text = objVehiculo.Placa
            Me.txtCertificado_Vehiculo.Text = objVehiculo.ConstanciaIns
            Me.hddIdVehiculo.Value = CStr(objVehiculo.IdProducto)

        End If
        '******************* AGENTE
        If (objAgente IsNot Nothing) Then

            Me.txtAgente.Text = objAgente.Descripcion
            Me.txtDni_Agente.Text = objAgente.Dni
            Me.txtRuc_Agente.Text = objAgente.Ruc
            Me.txtDireccion_Agente.Text = objAgente.Direccion
            Me.hddIdAgente.Value = CStr(objAgente.IdPersona)

        End If
        '************** OBSERVACIONES
        If (objObservaciones IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservaciones.Observacion
        End If

        '**************** LISTA DOC REF
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()


        'Mostrar la suma Total del Campo Peso del Documento
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "  calcularPesoTotal('1',null);   ", True)

    End Sub

    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub

    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            '*************** QUITAMOS EL DETALLE DEL DOCUMENTO DE REFERENCIA
            Dim i As Integer = (Me.listaDetalleDocumento.Count - 1)
            While i >= 0

                If (CInt(Me.listaDetalleDocumento(i).IdDocumento) = Me.listaDocumentoRef(index).Id) Then
                    Me.listaDetalleDocumento.RemoveAt(i)
                End If

                i = i - 1

            End While

            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)
            setListaDetalleDocumento(Me.listaDetalleDocumento)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            '******************* 
            If (Me.GV_DocumentoRef.Rows.Count <= 0) Then
                '***************** ACTIVAMOS LOS COMBOS DE EMPRESA/ALMACÉN

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Editar
                        Me.cboEmpresa.Enabled = False
                        Me.cboTienda.Enabled = False
                    Case Else
                        Me.cboEmpresa.Enabled = True
                        Me.cboTienda.Enabled = True
                End Select

                Me.cboAlmacen.Enabled = True
                Me.cboTipoOperacion.Enabled = True
                Me.btnBuscarRemitente.Enabled = True
                Me.btnBuscarDestinatario.Enabled = True
                Me.btnBuscarProducto.Enabled = True
                Me.btnLimpiarDetalleDocumento.Enabled = True

                If (GV_Detalle.Rows.Count > 0) Then
                    VerificarCantidadProductos()
                End If
                If (GV_Detalle.Rows.Count = 0) Then
                    LblcantidadRegistros.Text = ""
                End If

            End If

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region


    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click

        registrarDocumento()

    End Sub


    Private Sub validarFrm()

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        If (Me.GV_Detalle.Rows.Count > 0) Then
            VerificarCantidadProductos()
            If ((CInt(hddNroFilasxGuia.Value)) > CInt(Me.hddNroFilasxDocumento.Value)) Then
                Throw New Exception("EL NÚMERO MÁXIMO DE FILAS PARA EL DOCUMENTO ES DE < " + CStr(Math.Round(CInt(Me.hddNroFilasxDocumento.Value), 0)) + " >. NO SE PERMITE LA OPERACIÓN.")
            End If
        End If

        If (GV_Detalle.Rows.Count = 0) Then
            LblcantidadRegistros.Text = ""
        End If

        If Me.GV_DocumentoRef.Rows.Count <= 0 And CInt(Me.cboTipoOperacion.SelectedValue) = 1 Then

            Throw New Exception("DEBE INGRESAR DOCUMENTOS DE REFERENCIA. NO SE PERMITE LA OPERACIÓN.")

        End If

        If IsNumeric(Me.cboAlmacen_Destinatario.SelectedValue) Then
            If CInt(Me.cboAlmacen.SelectedValue) = CInt(Me.cboAlmacen_Destinatario.SelectedValue) Then

                Throw New Exception("AL ALMACEN DE ORIGEN Y DESTINO NO PUEDE SER EL MISMO. NO SE PERMITE LA OPERACIÓN.")

            End If
        End If
    End Sub

    Private Sub registrarDocumento()

        Try
            ActualizarListaDetalleDocumentoComboBox()

            If (Me.GV_DocumentoRef.Rows.Count > 0) Then
                validarDocumentosRef()
            End If

            '******************* VALIDAMOS EL FRM
            validarFrm()

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
            Dim objPuntoPartida As Entidades.PuntoPartida = obtenerPuntoPartida()
            Dim objPuntoLlegada As Entidades.PuntoLlegada = obtenerPuntoLlegada()
            Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()

            Dim moverStockFisico As Boolean = False
            Dim validarDespacho As Boolean = False
            If (CInt(Me.cboTipoOperacion.SelectedValue) = 2102158001) Then
                If (Me.GV_DocumentoRef.Rows.Count > 0) Then  '************ DOCUMENTOS POR DESPACHAR
                    moverStockFisico = False
                    validarDespacho = False
                End If
            End If

            If ((moverStockFisico) And (objDocumento.IdEmpresa <> objDocumento.IdRemitente)) Then Throw New Exception("LA OPCIÓN < MOVER STOCK FÍSICO > ESTÁ ACTIVO. LA EMPRESA SELECCIONADA NO COINCIDE CON EL REMITENTE SELECCIONADO. NO SE PERMITE LA OPERACIÓN.")

            For Each row As GridViewRow In Me.GV_Detalle.Rows
                Dim checkBoxList As CheckBoxList = TryCast(row.FindControl("checkListTonos"), CheckBoxList)
                '
                'Busca cantidades tecleadas para volver a asignarlos a los textbox dinamicos
                '

                Dim HiddenField As HiddenField = TryCast(row.FindControl("hddCantidadTecleadas"), HiddenField)
                Dim splitCantidades As Array = HiddenField.Value.Split(",")
                Dim cantidadSplit As Integer = splitCantidades.Length

                'Carga TextBox segun cantidad de checkbox
                '                
                Dim placeHolder As New PlaceHolder()
                placeHolder = TryCast(row.FindControl("phcantidades"), PlaceHolder)
                Dim cantidadCheck As Integer = checkBoxList.Items.Count
                placeHolder.Controls.Add(New LiteralControl("<table>"))

                For i As Integer = 0 To cantidadCheck - 1
                    Dim textbox As New TextBox()
                    textbox.ID = "txt_" & i
                    textbox.Width = "80"
                    textbox.EnableViewState = True
                    textbox.Text = splitCantidades(i)
                    textbox.Attributes.Add("onblur", "return(valBlur(event));")
                    textbox.Attributes.Add("onKeypress", "return(validarNumeroPunto(event));")
                    textbox.Attributes.Add("onfocus", "return(aceptarFoco(this));")
                    textbox.Attributes.Add("onKeyUp", "return(  calcularPesoTotalmasdeUnaCaja()  );")
                    placeHolder.Controls.Add(New LiteralControl("<tr>"))
                    placeHolder.Controls.Add(New LiteralControl("<td>"))
                    placeHolder.Controls.Add(textbox)
                    placeHolder.Controls.Add(New LiteralControl("</td>"))
                    placeHolder.Controls.Add(New LiteralControl("</tr>"))
                Next
                placeHolder.Controls.Add(New LiteralControl("</table>"))
                '******************
                Dim contador As Integer = 0
                '
                'Creo la lista de objetos con los tonos seleccionados y los asigno al objeto listaDetalleDocumento
                '
                Dim lista As New List(Of Entidades.be_tonoXProducto)
                For Each ListItem As ListItem In checkBoxList.Items
                    If ListItem.Selected Then
                        Dim contadorCheckBoxList As Integer = checkBoxList.Items.Count
                        'If contadorCheckBoxList = 1 Then
                        placeHolder = New PlaceHolder()
                        placeHolder = TryCast(row.FindControl("phcantidades"), PlaceHolder)
                        Dim textboxCantidad As TextBox = placeHolder.FindControl("txt_" & contador)
                        Dim objeto As New Entidades.be_tonoXProducto
                        objeto.idTono = ListItem.Value
                        objeto.nomtono = ListItem.Text
                        objeto.cantidadTono = CDec(textboxCantidad.Text)
                        lista.Add(objeto)
                        listaDetalleDocumento(row.RowIndex).listaTonos = lista
                    End If
                    contador += 1
                Next
                'Dim objTono As New Negocio.DocGuiaRemision
                'objTono.registrarTono(objDocumento.Id, dt)
            Next

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo

                    objDocumento.Id = (New Negocio.DocGuiaRemision).registrarGuiaRemision(objDocumento, Me.listaDetalleDocumento, objObservaciones, objPuntoPartida, objPuntoLlegada, listaRelacionDocumento, -1, moverStockFisico, validarDespacho)
                    Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    'vuelve a coger los controles dinamicos y los vuelve a crear
                    Me.listaDetalleDocumento = getListaDetalleDocumento()
                    Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
                    Me.GV_Detalle.DataBind()
                    objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")

                Case FrmModo.Editar

                    objDocumento.Id = (New Negocio.DocGuiaRemision).actualizarGuiaRemision(objDocumento, Me.listaDetalleDocumento, objObservaciones, objPuntoPartida, objPuntoLlegada, listaRelacionDocumento, -1, moverStockFisico, validarDespacho)
                    'vuelve a coger los controles dinamicos y los vuelve a crear
                    Me.listaDetalleDocumento = getListaDetalleDocumento()
                    Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
                    Me.GV_Detalle.DataBind()
                    'Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                    objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")

            End Select

            verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False)

        Catch ex As Exception
            'vuelve a coger los controles dinamicos y los vuelve a crear
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub validarDocumentosRef()

        Me.listaDocumentoRef = getlistaDocumentoRef()
        Dim IdDestinatario As Integer = CInt(Me.hddIdDestinatario.Value)
        Dim IdEmpresa As Integer = CInt(Me.cboEmpresa.SelectedValue)
        Dim IdAlmacen As Integer = CInt(Me.cboAlmacen.SelectedValue)

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1
            Dim valida As Boolean = True

            If Me.listaDocumentoRef(i).IdTipoDocumento = 15 And CInt(Me.cboTipoOperacion.SelectedValue) = 1 Then ' ************* OP/VP - VENTA
                valida = False
            End If

            If valida Then

                If (CInt(Me.cboTipoOperacion.SelectedValue) <> 2 And CInt(Me.cboTipoOperacion.SelectedValue) <> 5) Then
                    If (Me.listaDocumentoRef(i).IdPersona <> IdDestinatario) Then Throw New Exception("EL DESTINATARIO DEL DOCUMENTO DE REFERENCIA NO COINCIDE CON EL DESTINATARIO SELECCIONADO. NO SE PERMITE LA OPERACIÓN.")
                    If (Me.listaDocumentoRef(i).IdAlmacen <> IdAlmacen) Then Throw New Exception("EL ALMACÉN DEL DOCUMENTO DE REFERENCIA NO COINCIDE CON EL ALMACÉN SELECCIONADO. NO SE PERMITE LA OPERACIÓN.")
                End If
                If (Me.listaDocumentoRef(i).IdEmpresa <> IdEmpresa) Then Throw New Exception("LA EMPRESA DEL DOCUMENTO DE REFERENCIA NO COINCIDE CON LA EMPRESA SELECCIONADA. NO SE PERMITE LA OPERACIÓN.")

            End If

        Next


    End Sub

    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)

        Dim lista As New List(Of Entidades.RelacionDocumento)
        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            Dim objRelacionDocumento As New Entidades.RelacionDocumento
            With objRelacionDocumento

                .IdDocumento1 = Me.listaDocumentoRef(i).Id

                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento2 = Nothing
                    Case FrmModo.Editar
                        .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                End Select

            End With

            lista.Add(objRelacionDocumento)

        Next

        Return lista

    End Function
    Private Function obtenerPuntoLlegada() As Entidades.PuntoLlegada

        Dim objPuntoLlegada As Entidades.PuntoLlegada = Nothing

        objPuntoLlegada = New Entidades.PuntoLlegada
        With objPuntoLlegada

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            .pll_Direccion = Me.txtDireccion_Llegada.Text.Trim

            If (Me.cboAlmacen_Destinatario.Items.Count > 0) Then
                .IdAlmacen = CInt(Me.cboAlmacen_Destinatario.SelectedValue)
            Else
                .IdAlmacen = Nothing
            End If


            .pll_Ubigeo = Me.cboDepto_Llegada.SelectedValue + Me.cboProvincia_Llegada.SelectedValue + Me.cboDistrito_Llegada.SelectedValue

        End With

        Return objPuntoLlegada

    End Function
    Private Function obtenerPuntoPartida() As Entidades.PuntoPartida

        Dim objPuntoPartida As Entidades.PuntoPartida = Nothing


        objPuntoPartida = New Entidades.PuntoPartida

        With objPuntoPartida

            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            .Direccion = Me.txtDireccion_Partida.Text.Trim

            If (CInt(Me.cboEmpresa.SelectedValue) = CInt(Me.hddIdRemitente.Value)) Then
                .IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)
            Else
                .IdAlmacen = Nothing
            End If

            .Ubigeo = Me.cboDepto_Partida.SelectedValue + Me.cboProvincia_Partida.SelectedValue + Me.cboDistrito_Partida.SelectedValue

        End With


        Return objPuntoPartida

    End Function
    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .FechaIniTraslado = CDate(Me.txtFechaInicioTraslado.Text)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdMotivoT = CInt(Me.cboMotivoTraslado.SelectedValue)
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdAlmacen = CInt(Me.cboAlmacen.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdEstadoEntrega = 1 '************* ENTREGADO

            .IdPersona = CInt(Me.hddIdDestinatario.Value)
            .IdRemitente = CInt(Me.hddIdRemitente.Value)
            .IdDestinatario = CInt(Me.hddIdDestinatario.Value)

            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)

            If (IsNumeric(Me.hddIdTransportista.Value) And Me.hddIdTransportista.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdTransportista.Value) > 0) Then
                    .IdTransportista = CInt(Me.hddIdTransportista.Value)
                End If
            End If
            If (IsNumeric(Me.hddIdChofer.Value) And Me.hddIdChofer.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdChofer.Value) > 0) Then
                    .IdChofer = CInt(Me.hddIdChofer.Value)
                End If
            End If
            If (IsNumeric(Me.hddIdVehiculo.Value) And Me.hddIdVehiculo.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdVehiculo.Value) > 0) Then
                    .IdVehiculo = CInt(Me.hddIdVehiculo.Value)
                End If
            End If

            If (IsNumeric(Me.hddIdAgente.Value) And Me.hddIdAgente.Value.Trim.Length > 0) Then
                If (CInt(Me.hddIdAgente.Value) > 0) Then
                    '********** AQUI VA LA AGENCIA TRANSPORTE EN USUARIO COMISION******** 
                    .IdUsuarioComision = CInt(Me.hddIdAgente.Value)
                End If
            End If
            '************* VALORES ADICIONALES
            .FactorMov = -1 '************** Mov de Almacén

        End With

        Return objDocumento

    End Function
    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdPersona As Integer)

        Try
            ActualizarListaDetalleDocumento()

            Dim TipoOperacion As Integer

            TipoOperacion = CInt(cboTipoOperacion.SelectedValue)

            If (TipoOperacion <> 19) Then

                '******************** INICIALIZAMOS EL FRM CUANDO NO EXISTE UN DOC. DE REFERENCIA Y TIPOOPERACION <> 19
                If (Me.GV_DocumentoRef.Rows.Count <= 0) Then

                    If CInt(hddFrmModo.Value) = FrmModo.Editar Then

                        verFrm(FrmModo.Editar, False, False, False, False)

                    Else

                        verFrm(FrmModo.Nuevo, True, True, True, True, False)

                    End If


                Else
                    If (Not validar_AddDocumentoRef(IdDocumentoRef, IdEmpresa, IdAlmacen, IdPersona)) Then Throw New Exception("No se permite la Operación.")
                End If

            End If

            If (TipoOperacion = 19) Then

                If (Not validar_AddDocumentoRef(IdDocumentoRef, IdEmpresa, IdAlmacen, IdPersona)) Then Throw New Exception("EL DESTINATARIO DEBE SER EL MISMO")


            End If

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)
            Dim objRemitente As Entidades.PersonaView
            Dim objPuntoPartida As Entidades.PuntoPartida
            Dim objDestinatario As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdDestinatario)
            Dim objPuntoLlegada As Entidades.PuntoLlegada

            If CInt(Me.cboTipoOperacion.SelectedValue) = 5 And objDocumento.IdTipoDocumento = 25 Then

                objRemitente = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdDestinatario)
                objDestinatario = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdRemitente)

                actualizarPuntoPartidaxIdAlmacen(CInt(Me.cboAlmacen.SelectedValue), True, True, True)

            Else

                objRemitente = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdRemitente)
                objPuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumento.Id)
                objDestinatario = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdDestinatario)

            End If

            If objDocumento.IdTipoDocumento = 15 And objDocumento.IdTipoOperacion = 1 Then ' O/P - VENTA
                Try

                    Dim lista_Doc As List(Of Entidades.Documento) = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(objDocumento.Id)
                    objPuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(lista_Doc(0).Id)

                Catch ex As Exception

                End Try
            Else

                If CInt(Me.cboTipoOperacion.SelectedValue) = 5 And objDocumento.IdTipoDocumento = 25 Then

                    objPuntoLlegada = New Entidades.PuntoLlegada
                    With objPuntoLlegada
                        .pll_Ubigeo = objDestinatario.Ubigeo
                        .pll_Direccion = objDestinatario.Direccion
                    End With

                Else

                    objPuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(objDocumento.Id)

                End If

            End If
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDetalleDocumento.AddRange(obtenerListaDetalleDocumento_Ref(objDocumento.Id))

            ' VALIDANDO ELEMENTOS
            'Dim index% = 0
            'Dim cont% = 0
            'Dim totalreg As Integer
            'totalreg = Me.listaDetalleDocumento.Count
            'Do While index < totalreg
            '    cont = 0
            '    For x As Integer = 0 To totalreg - 1
            '        If Me.listaDetalleDocumento(index).IdProducto = Me.listaDetalleDocumento(x).IdProducto And Me.listaDetalleDocumento(index).IdUnidadMedida = Me.listaDetalleDocumento(x).IdUnidadMedida Then
            '            cont += 1
            '            If cont > 1 Then
            '                Me.listaDetalleDocumento(index).Cantidad = Me.listaDetalleDocumento(index).Cantidad + Me.listaDetalleDocumento(x).Cantidad
            '                Me.listaDetalleDocumento.RemoveAt(x)
            '                index -= 1
            '                Exit For
            '            End If
            '        End If
            '    Next
            '    totalreg = Me.listaDetalleDocumento.Count
            '    index += 1
            'Loop

            ' Aqui Agrego la magnitud Peso

            Dim listaUnidadMedida_Peso As List(Of Entidades.MagnitudUnidad) = (New Negocio.MagnitudUnidad)._MagnitudUnidadselectxIdMagnitud(_IdMagnitud_Peso)

            For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1
                With Me.listaDetalleDocumento(i)

                    .IdUnidadMedidaPrincipal = (New Negocio.ProductoUM).SelectIdUMPrincipalxIdProducto(.IdProducto)

                    .ListaUnidadMedida_Peso = listaUnidadMedida_Peso

                    .IdUnidadMedida_Peso = CInt(Me.cboUnidadMedida_PesoTotal.SelectedValue)

                    '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                    .Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(.IdProducto, .IdUnidadMedidaPrincipal, 1, _IdMagnitud_Peso, .IdUnidadMedida_Peso)

                    '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                    .Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUnidadMedida, .IdUnidadMedidaPrincipal, 1)

                    .Peso = .Cantidad * .Equivalencia1 * .Equivalencia

                    .Stock = (New Negocio.Util).fx_StockReal(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), .IdProducto)
                    .StockComprometido = (New Negocio.Util).fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), .IdProducto)
                    .StockDisponible = .Stock - .StockComprometido

                End With
            Next


            Dim objTransportista As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdTransportista)
            Dim objChofer As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdChofer)
            Dim objVehiculo As Entidades.Vehiculo = (New Negocio.Vehiculo).SelectxIdVehiculo(objDocumento.IdVehiculo)
            'Dim objAgente As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdUsuarioComision)

            Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

            '*************** GUARDAMOS EN SESSION
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.Add(objDocumento)
            setlistaDocumentoRef(Me.listaDocumentoRef)



            '************** EL PUNTO DE PARTIDA NO SE TOMA XQ PRODUCE PROBLEMAS
            cargarDocumentoRef_GUI(objDocumento, Me.listaDocumentoRef, objRemitente, objPuntoPartida, objDestinatario, objPuntoLlegada, Me.listaDetalleDocumento, objTransportista, objChofer, objVehiculo, Nothing, objObservaciones, False)

            If CInt(Me.cboTipoOperacion.SelectedValue) = 5 And objDocumento.IdTipoDocumento = 25 Then
                actualizarPuntoPartidaxIdAlmacen(CInt(Me.cboAlmacen.SelectedValue), True, True, True)
            End If




            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

            '***************** DESACTIVAMOS LOS COMBOS DE EMPRESA/ALMACÉN
            Me.cboEmpresa.Enabled = False
            Me.cboTienda.Enabled = False
            If (CInt(cboTipoOperacion.SelectedValue) = 5 Or CInt(cboTipoOperacion.SelectedValue) = 4 Or CInt(cboTipoOperacion.SelectedValue) = 2) Then
                Me.btnBuscarRemitente.Enabled = True
                Me.btnBuscarDestinatario.Enabled = True
            Else
                Me.btnBuscarRemitente.Enabled = False
                Me.btnBuscarDestinatario.Enabled = False
            End If


            Me.cboAlmacen.Enabled = False
            Me.cboTipoOperacion.Enabled = False
            Me.btnBuscarProducto.Enabled = False
            Me.btnLimpiarDetalleDocumento.Enabled = False

            If IsNothing(objPuntoPartida) Then

                actualizarPuntoPartidaxIdAlmacen(CInt(Me.cboAlmacen.SelectedValue), True, True, True)

            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", " calcularPesoTotal('1',null);  ", True)

            If (Me.GV_Detalle.Rows.Count > 0) Then
                VerificarCantidadProductos()
            End If

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdPersona As Integer = 0
            Try
                IdPersona = CInt(Me.hddIdDestinatario.Value)
            Catch ex As Exception
                IdPersona = 0
            End Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, IdPersona, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub GV_BusquedaAvanzado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_BusquedaAvanzado.PageIndexChanging
        Me.GV_BusquedaAvanzado.PageIndex = e.NewPageIndex
        busquedaAvanzado()
    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            cargarDocumentoGuiaRemision(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarDocumentoGuiaRemision(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)

        '**************** LIMPIAMOS EL FORMULARIO
        'verFrm(FrmModo.Inicio, True, True, True, True, True) 'original
        verFrm(FrmModo.Editar, True, True, True, True, True) ' modificado al 18/21/2016

        '****************** OBTENEMOS LOS DATOS DEL DOCUMENTO

        Dim objDocumento As Entidades.Documento = Nothing
        If (IdDocumento <> Nothing) Then
            objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
        Else
            objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
        End If

        Dim objRemitente As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdRemitente)
        Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumento.Id)
        Dim objDestinatario As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdDestinatario)
        Dim objPuntoLlegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(objDocumento.Id)

        Me.listaDetalleDocumento = obtenerListaDetalleDocumento_GuiaRemision(objDocumento.Id)
        Me.listaDocumentoRef = (New Negocio.DocGuiaRemision).DocumentoGuiaRemisionSelectDocumentoRef(objDocumento.Id)

        Dim objTransportista As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdTransportista)
        Dim objChofer As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdChofer)
        Dim objVehiculo As Entidades.Vehiculo = (New Negocio.Vehiculo).SelectxIdVehiculo(objDocumento.IdVehiculo)
        Dim objAgente As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdUsuarioComision)

        Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

        '*************** GUARDAMOS EN SESSION

        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1
            listaDetalleDocumento(i).Stock = (New Negocio.Util).fx_StockReal(objDocumento.IdEmpresa, objDocumento.IdAlmacen, listaDetalleDocumento(i).IdProducto)
            listaDetalleDocumento(i).StockComprometido = (New Negocio.Util).fx_StockComprometidoxIdEmpresaxIdAlmacenxIdProducto(objDocumento.IdEmpresa, objDocumento.IdAlmacen, listaDetalleDocumento(i).IdProducto)
            listaDetalleDocumento(i).StockDisponible = listaDetalleDocumento(i).Stock - listaDetalleDocumento(i).StockComprometido

        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)
        setlistaDocumentoRef(Me.listaDocumentoRef)

        '************** 
        cargarDocumentoRef_GUI(objDocumento, Me.listaDocumentoRef, objRemitente, objPuntoPartida, objDestinatario, objPuntoLlegada, Me.listaDetalleDocumento, objTransportista, objChofer, objVehiculo, objAgente, objObservaciones, True)
        verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, False)

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        Dim activarControlesRef As Boolean = False
        If (Me.GV_DocumentoRef.Rows.Count <= 0) Then
            activarControlesRef = True
        End If

        '***************** CONFIGURAMOS LOS CONTROLES
        Me.cboEmpresa.Enabled = activarControlesRef
        Me.cboTienda.Enabled = activarControlesRef
        Me.cboAlmacen.Enabled = activarControlesRef
        Me.cboTipoOperacion.Enabled = activarControlesRef
        Me.btnBuscarRemitente.Enabled = activarControlesRef
        Me.btnBuscarDestinatario.Enabled = activarControlesRef
        Me.btnBuscarProducto.Enabled = activarControlesRef
        Me.btnLimpiarDetalleDocumento.Enabled = activarControlesRef

    End Sub

    Private Function obtenerListaDetalleDocumento_GuiaRemision(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim listaUnidadMedida_Peso As List(Of Entidades.MagnitudUnidad) = (New Negocio.MagnitudUnidad)._MagnitudUnidadselectxIdMagnitud(_IdMagnitud_Peso)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DocGuiaRemision).DocumentoGuiaRemisionSelectDetalle(IdDocumento)
        For i As Integer = 0 To listaDetalle.Count - 1

            listaDetalle(i).ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)
            listaDetalle(i).IdDocumento = listaDetalle(i).IdDocumentoRef '*********** COLOCAMOS EL DOCUMENTO REF
            'qwe
            Try
                listaDetalle(i).listaTonos = (New Negocio.bl_Tonos).SelectTonosxProductos_Editar(IdDocumento, listaDetalle(i).IdDetalleDocumento)
                listaDetalle(i).ListaUnidadMedida_Peso = listaUnidadMedida_Peso
                listaDetalle(i).IdUnidadMedida_Peso = listaUnidadMedida_Peso(0).idUnidadMedida

                '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                listaDetalle(i).Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(listaDetalle(i).IdProducto, listaDetalle(i).IdUnidadMedida, 1, _IdMagnitud_Peso, listaDetalle(i).IdUnidadMedida_Peso)

                '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                listaDetalle(i).Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(listaDetalle(i).IdProducto, listaDetalle(i).IdUnidadMedida, listaDetalle(i).IdUnidadMedida, 1)

                listaDetalle(i).Peso = listaDetalle(i).Cantidad * listaDetalle(i).Equivalencia1 * listaDetalle(i).Equivalencia

            Catch ex As Exception

            End Try


        Next

        Return listaDetalle

    End Function

#End Region

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Try
            'validar doc a editar
            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarEdicionDocxIdUsuarioxIdPermiso(CInt(Session("IdUsuario")), New Integer() {2101100008}, Me.cboSerie.SelectedItem.Text, Me.txtCodigoDocumento.Text, Me.hddIdTipoDocumento.Value)
            Select Case listaPermisos(0)
                Case 1
                    verFrm(FrmModo.Editar, False, False, False, False)

                    Me.listaDetalleDocumento = getListaDetalleDocumento()
                    Me.listaDocumentoRef = getlistaDocumentoRef()

                    Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
                    Me.GV_Detalle.DataBind()

                    Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
                    Me.GV_DocumentoRef.DataBind()

                    'Mostrar la suma Total del Campo Peso del Documento
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "  calcularPesoTotal('1',null);   ", True)
                    Me.txtPesoTotal.Enabled = False
                Case 0
                    crearControlesTextbox()
                    objScript.mostrarMsjAlerta(Me, "No procede editar un documento de un mes anterior al actual.")

            End Select

        Catch ex As Exception
            crearControlesTextbox()
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnAnular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAnular.Click
        anularDocumentoGuiaRemision()
    End Sub
    Private Sub anularDocumentoGuiaRemision()
        Try
            'validar doc a editar
            Dim listaPermisos() As Integer = (New Negocio.Util).ValidarEdicionDocxIdUsuarioxIdPermiso(CInt(Session("IdUsuario")), New Integer() {2101100008}, Me.cboSerie.SelectedItem.Text, Me.txtCodigoDocumento.Text, Me.hddIdTipoDocumento.Value)
            Select Case listaPermisos(0)
                Case 1
                    Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)

                    If ((New Negocio.DocGuiaRemision).anularGuiaRemision(IdDocumento)) Then
                        objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")
                        verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False)
                        crearControlesTextbox()
                    Else
                        crearControlesTextbox()
                        Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
                    End If
                Case 0
                    crearControlesTextbox()
                    objScript.mostrarMsjAlerta(Me, "No procede [Anular] un documento de un mes anterior al actual.")

            End Select
        Catch ex As Exception
            crearControlesTextbox()
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        Try

            cargarDocumentoGuiaRemision(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value.Trim), 0)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message.ToString)
        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True, True)

        Me.cboTipoOperacion.SelectedIndex = 0
        Dim objCbo As New Combo
        objCbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), False)
        Me.cboAlmacen_Destinatario.Items.Clear()

    End Sub

    Private Sub cboTipoOperacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoOperacion.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            validarDestinatario()
            objCbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)
            crearControlesTextbox()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub validarDestinatario()


        Me.listaDocumentoRef = getlistaDocumentoRef()
        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If Me.listaDocumentoRef(i).IdTipoDocumento = 15 And CInt(Me.cboTipoOperacion.SelectedValue) = 1 Then ' ***************** OP/VP - VENTA

                Dim ListaRelacionDocumento As List(Of Entidades.RelacionDocumento) = (New Negocio.RelacionDocumento).Documento_Referencia(Me.listaDocumentoRef(i).Id)

                For k As Integer = 0 To ListaRelacionDocumento.Count - 1
                    With ListaRelacionDocumento(k)

                        If .IdTipoDocumento = 1 Or .IdTipoDocumento = 3 Or .IdTipoDocumento = 57 Or .IdTipoDocumento = 1101353001 Or .IdTipoDocumento = 1101353002 Then ' ********** F/V **********  B/V

                            Dim IdPersonaRef As Integer = (New Negocio.Documento).SelectActivoxIdDocumento(.IdDocumento).IdPersona
                            Me.hddOpcionBusquedaPersona.Value = "1"  ' ************ DESTINATARIO
                            cargarPersona(IdPersonaRef)
                            Exit For
                        End If

                    End With
                Next

                Exit For

            ElseIf Me.listaDocumentoRef(i).IdTipoDocumento = 15 And CInt(Me.cboTipoOperacion.SelectedValue) <> 1 Then

                Dim objdestinatario As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(Me.listaDocumentoRef(i).IdDestinatario)

                If (objdestinatario IsNot Nothing) Then

                    Me.txtDestinatario.Text = objdestinatario.Descripcion
                    Me.txtDni_Destinatario.Text = objdestinatario.Dni
                    Me.txtRuc_Destinatario.Text = objdestinatario.Ruc
                    Me.txtIdDestinatario.Text = CStr(objdestinatario.IdPersona)
                    Me.hddIdDestinatario.Value = CStr(objdestinatario.IdPersona)

                    Dim cbo As New Combo
                    cbo.llenarCboAlmacenxIdEmpresa(Me.cboAlmacen_Destinatario, CInt(objdestinatario.IdPersona), False)

                    Dim objpuntollegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(Me.listaDocumentoRef(i).Id)

                    If objpuntollegada IsNot Nothing Then

                        If (Me.cboAlmacen_Destinatario.Items.FindByValue(CStr(objpuntollegada.IdAlmacen)) IsNot Nothing) Then
                            Me.cboAlmacen_Destinatario.SelectedValue = CStr(objpuntollegada.IdAlmacen)
                            actualizarPuntoLlegadaxIdAlmacen(CInt(Me.cboAlmacen_Destinatario.SelectedValue), True, True, True)
                        End If

                    End If




                End If

                Exit For

            End If

        Next
    End Sub

    Protected Sub btnBuscarRemitente_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarRemitente.Click

        mostrarCapaPersona("0")
        crearControlesTextbox()
    End Sub
    Private Sub mostrarCapaPersona(ByVal opcion As String)

        Try

            Me.gvBuscar.DataSource = Nothing
            Me.gvBuscar.DataBind()

            Me.hddOpcionBusquedaPersona.Value = opcion

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad  ", "      mostrarCapaPersona();        ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Protected Sub btnBuscarDestinatario_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarDestinatario.Click

        mostrarCapaPersona("1")
        crearControlesTextbox()
    End Sub

    Private Sub btnBuscarTransportista_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarTransportista.Click

        mostrarCapaPersona("2")

    End Sub

    Private Sub btnBuscarChofer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarChofer.Click

        mostrarCapaPersona("3")

    End Sub
    Private Sub btnBuscarAgente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarAgente.Click

        'mostrarCapaPersona("-1")

    End Sub

#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function


#End Region

    Private Sub cboTipoAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoAlmacen.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen_Destinatario, CInt(Me.cboTipoAlmacen.SelectedValue), CInt(Me.hddIdDestinatario.Value), False)
            crearControlesTextbox()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#Region "**************************    CALCULAR EQUIVALENCIAS"

    Protected Sub btnEquivalencia_PR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Try

            ActualizarListaDetalleDocumento()
            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)

            Me.listaDetalleDocumento = getListaDetalleDocumento()


            '******************** CARGAMOS LOS CONTROLES
            Me.cboUnidadMedida_Ingreso.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(index).IdProducto)
            Me.cboUnidadMedida_Ingreso.DataBind()

            Me.cboUnidadMedida_Salida.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(index).IdProducto)
            Me.cboUnidadMedida_Salida.DataBind()

            Me.GV_CalcularEquivalencia.DataSource = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalleDocumento(index).IdProducto)
            Me.GV_CalcularEquivalencia.DataBind()

            Me.GV_ResuldoEQ.DataSource = Nothing
            Me.GV_ResuldoEQ.DataBind()

            Me.txtCantidad_Ingreso.Text = CStr(Me.listaDetalleDocumento(index).Cantidad)
            Me.txtCantidad_Salida.Text = ""

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   mostrarCapaEquivalencia_PR();  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Protected Sub btnCalcular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCalcular.Click
        calcularEquivalencia()
    End Sub
    Private Sub calcularEquivalencia()
        Try

            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()


            Dim tableUM As DataTable = obtenerTablaUM(Me.listaDetalleDocumento(index).IdProducto)
            Me.GV_ResuldoEQ.DataSource = (New Negocio.Producto).Producto_ConsultaEquivalenciaProductoxParams(Me.listaDetalleDocumento(index).IdProducto, CInt(Me.cboUnidadMedida_Ingreso.SelectedValue), CInt(Me.cboUnidadMedida_Salida.SelectedValue), CDec(Me.txtCantidad_Ingreso.Text), tableUM, CInt(Me.rbl_UtilizarRedondeo.SelectedValue))
            Me.GV_ResuldoEQ.DataBind()

            Me.GV_ResuldoEQ.Rows(Me.GV_ResuldoEQ.Rows.Count - 1).BackColor = Drawing.Color.Yellow

            Dim cantidadEq As Decimal = CDec(Me.GV_ResuldoEQ.Rows(Me.GV_ResuldoEQ.Rows.Count - 1).Cells(2).Text)
            Me.txtCantidad_Salida.Text = CStr(cantidadEq)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "   mostrarCapaEquivalencia_PR();  ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerTablaUM(ByVal IdProducto As Integer) As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        For i As Integer = 0 To Me.GV_CalcularEquivalencia.Rows.Count - 1

            If (CType(Me.GV_CalcularEquivalencia.Rows(i).FindControl("chb_UnidadMedida"), CheckBox).Checked) Then
                Dim IdUnidadMedida As Integer = CInt(CType(Me.GV_CalcularEquivalencia.Rows(i).FindControl("hddIdUnidadMedida"), HiddenField).Value)
                dt.Rows.Add(IdProducto, IdUnidadMedida)
            End If

        Next

        Return dt


    End Function
    Private Sub aceptarEquivalencia_PR()
        Try

            Dim index As Integer = CInt(Me.hddIndex_GV_Detalle.Value)
            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Dim cantidadNew As Decimal = CDec(Me.txtCantidad_Salida.Text)
            Me.listaDetalleDocumento(index).Cantidad = cantidadNew

            If Not Me.listaDetalleDocumento(index).ListaUM.Find(Function(k As Entidades.ProductoUMView) k.IdUnidadMedida = CInt(Me.cboUnidadMedida_Salida.SelectedValue)) Is Nothing Then
                Me.listaDetalleDocumento(index).IdUnidadMedida = CInt(Me.cboUnidadMedida_Salida.SelectedValue)
                Me.listaDetalleDocumento(index).UMedida = CStr(Me.cboUnidadMedida_Salida.SelectedItem.ToString)
            End If

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "     offCapa('capaEquivalenciaProducto');    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub btnAceptar_EquivalenciaPR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar_EquivalenciaPR.Click

        aceptarEquivalencia_PR()

    End Sub
#End Region


    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(CboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub valOnChange_cboUnidadMedida(ByVal sender As Object, ByVal e As System.EventArgs)
        actualizarEquivalencia_Peso(CType(CType(sender, DropDownList).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Protected Sub valOnChange_cboTonoDisponible(ByVal sender As Object, ByVal e As System.EventArgs)
        actualizarCantidadxTono(CType(CType(sender, DropDownList).NamingContainer, GridViewRow).RowIndex)

    End Sub

    Private Sub actualizarCantidadxTono(ByVal index As Integer)
        Dim ddlTono As DropDownList = DirectCast(GV_Detalle.Rows(index).FindControl("cboTonoDisponible"), DropDownList)
        Dim valorComboActual As Integer = ddlTono.SelectedValue
        ActualizarListaDetalleDocumento(index)

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        With (Me.listaDetalleDocumento(index))

            Dim cantidadxTono As Decimal = (New Negocio.Util).fx_getCantidadxTono(.IdProducto, valorComboActual)
            .cantidadTono = cantidadxTono
        End With

        setListaDetalleDocumento(Me.listaDetalleDocumento)

        Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
        Me.GV_Detalle.DataBind()
        ddlTono.Items.FindByValue(valorComboActual).Selected = True
    End Sub



    Private Sub actualizarEquivalencia_Peso(ByVal index As Integer)
        Try

            ActualizarListaDetalleDocumentoComboBox(index)

            Me.listaDetalleDocumento = getListaDetalleDocumento()


            With (Me.listaDetalleDocumento(index))
                Dim NewCantidad As Decimal = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUMold, .IdUnidadMedida, .Cantidad)
                .Cantidad = NewCantidad

                '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                .Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(.IdProducto, .IdUnidadMedidaPrincipal, 1, _IdMagnitud_Peso, .IdUnidadMedida_Peso)

                '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                .Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUnidadMedida, .IdUnidadMedidaPrincipal, 1)


                .Peso = .Cantidad * .Equivalencia1 * .Equivalencia

            End With

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "  calcularPesoTotal('1',null);   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnLimpiarAgencia_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarAgencia.Click
        '************ AGENTE
        Me.hddIdAgente.Value = ""
        Me.txtAgente.Text = ""
        Me.txtDni_Agente.Text = ""
        Me.txtRuc_Agente.Text = ""
        Me.txtDireccion_Agente.Text = ""
        crearControlesTextbox()
    End Sub

    Private Sub btnLimpiarChofer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarChofer.Click
        '************ CHOFER
        Me.hddIdChofer.Value = ""
        Me.txtChofer.Text = ""
        Me.txtDni_Chofer.Text = ""
        Me.txtRuc_Chofer.Text = ""
        Me.txtNroLicencia.Text = ""
        crearControlesTextbox()
    End Sub

    Private Sub btnLimpiarTransportista_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarTransportista.Click
        '************ TRANSPORTISTA
        Me.hddIdTransportista.Value = ""
        Me.txtTransportista.Text = ""
        Me.txtDni_Transportista.Text = ""
        Me.txtRuc_Transportista.Text = ""
        Me.txtDireccion_Transportista.Text = ""
        crearControlesTextbox()
    End Sub

    Private Sub btnLimpiarVehiculo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarVehiculo.Click
        '************ VEHICULO
        Me.hddIdVehiculo.Value = ""
        Me.txtModelo_Vehiculo.Text = ""
        Me.txtPlaca_Vehiculo.Text = ""
        Me.txtCertificado_Vehiculo.Text = ""
        crearControlesTextbox()
    End Sub

    Protected Sub lkbQuitar_Click(ByVal sender As Object, ByVal e As EventArgs)
        quitarDetalleDocumento(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub quitarDetalleDocumento(ByVal Index As Integer)

        Try

            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Me.listaDetalleDocumento.RemoveAt(Index)

            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "  calcularPesoTotal('1',null);   ", True)

            If (GV_Detalle.Rows.Count > 0) Then
                VerificarCantidadProductos()
            End If

            If (GV_Detalle.Rows.Count = 0) Then
                LblcantidadRegistros.Text = ""
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Dim ListaDetalleDocumentoVenta As List(Of Entidades.DetalleDocumento)

    Protected Sub OnClick_imgVerDetalle(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim Index As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex

        Dim IdDocumento As Integer = CInt(CType(Me.GV_DocumentoRef.Rows(Index).FindControl("hddIdDocumento"), HiddenField).Value)


        Me.ListaDetalleDocumentoVenta = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)

        Me.GV_DocumentoRef_Detalle.DataSource = Me.ListaDetalleDocumentoVenta
        Me.GV_DocumentoRef_Detalle.DataBind()
        crearControlesTextbox()
    End Sub
    Dim cadena As String
    Dim acumulado As String
    Dim hdidprod As Integer
    Public Sub VerificarCantidadProductos()
        'Dim cantidadReg As Integer

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            'hddIdDocumento

            Dim HddIdProd As HiddenField = CType(GV_Detalle.Rows(i).Cells(1).FindControl("HddIdProd"), HiddenField)
            hdidprod = CInt(HddIdProd.Value)
            cadena = CStr(hdidprod)
            If (acumulado <> "") Then
                acumulado = (acumulado + "," + cadena)
            End If
            If (acumulado = "") Then
                acumulado = cadena
            End If

        Next

        Me.hddNroFilasxGuia.Value = (CStr((New Negocio.DocumentoView).SelectCantidadFilasDocumentoProducto(CStr(Me.acumulado))))

        LblcantidadRegistros.Text = Me.hddNroFilasxGuia.Value + " Productos  en Guia de Remision  "

    End Sub

    Private Sub btnBuscarGrilla_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarGrilla_AddProducto.Click
        BuscarProductoCatalogo(0)
        Me.crearControlesTextbox()
    End Sub

    Private Sub btnAddProductos_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProductos_AddProducto.Click
        addProducto_DetalleDocumento()
    End Sub

    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                '
                'obtengo una lista de tonos por cada producto
                '
                Me.listaDetalleDocumento = getListaDetalleDocumento()
                Dim lista As New List(Of Entidades.be_tonoXProducto)
                lista = listaDetalleDocumento(e.Row.RowIndex).listaTonos

                '
                'Carga Tonos en el combo
                '
                Dim checkBoxList As CheckBoxList = CType(e.Row.FindControl("checkListTonos"), CheckBoxList)
                checkBoxList.DataSource = lista
                checkBoxList.DataBind()
                '
                'Carga TextBox segun cantidad de checkbox
                '
                Dim placeHolder As New PlaceHolder()
                placeHolder = TryCast(e.Row.FindControl("phcantidades"), PlaceHolder)
                Dim cantidadCheck As Integer = checkBoxList.Items.Count
                placeHolder.Controls.Add(New LiteralControl("<table cellspacing='10px'>"))

                '
                'Carga un solo textbox por defecto
                '
                If cantidadCheck = 0 And cboAlmacen.SelectedValue <> 7 Then
                    Dim txtDefault As New TextBox()
                    txtDefault.ID = "txtDefault"
                    txtDefault.Width = "80"
                    txtDefault.Text = listaDetalleDocumento(e.Row.RowIndex).Cantidad

                    txtDefault.Attributes.Add("onblur", "return(valBlur(event));")
                    txtDefault.Attributes.Add("onKeypress", "return(validarNumeroPunto(event));")
                    txtDefault.Attributes.Add("onfocus", "return(aceptarFoco(this));")
                    txtDefault.Attributes.Add("onKeyUp", "return(  calcularPesoTotal('0',this)  );")
                    placeHolder.Controls.Add(New LiteralControl("<tr>"))
                    placeHolder.Controls.Add(New LiteralControl("<td>"))
                    placeHolder.Controls.Add(txtDefault)
                    placeHolder.Controls.Add(New LiteralControl("</td>"))
                    placeHolder.Controls.Add(New LiteralControl("</tr>"))
                End If

                Dim objeto As New Entidades.be_tonoXProducto
                For i As Integer = 0 To cantidadCheck - 1
                    If Val(lista(i).cantidadTono) <> 0 Then
                        checkBoxList.Items(i).Selected = True
                    End If
                    'If Not IsNothing(checkBoxList.Items.FindByValue(lista(i).idTono.ToString)) Then
                    '    checkBoxList.Items.FindByValue(lista(i).idTono.ToString).Selected = True
                    'End If
                    Dim textbox As New TextBox()

                    textbox.ID = "txt_" & i
                    textbox.Width = "80"
                    'textbox.AutoPostBack = True
                    textbox.EnableViewState = True
                    'If i = 0 Then
                    'textbox.Text = lista(i).cantidadTono.ToString()
                    'Select Case CInt(Me.hddFrmModo.Value)
                    '    Case FrmModo.Nuevo
                    textbox.Text = listaDetalleDocumento(e.Row.RowIndex).Cantidad.ToString()
                    '    Case FrmModo.Editar
                    '    textbox.Text = lista(i).cantidadTono.ToString()
                    'End Select
                    'Else

                    'textbox.Text = 0
                    'End If
                    'textbox.Attributes.Add("OnTextChanged", "onTextChanged_Cantidad")
                    textbox.Attributes.Add("onblur", "return(valBlur(event));")
                    textbox.Attributes.Add("onKeypress", "return(validarNumeroPunto(event));")
                    textbox.Attributes.Add("onfocus", "return(aceptarFoco(this));")
                    textbox.Attributes.Add("onKeyUp", "return(  calcularPesoTotalmasdeUnaCaja()  );")
                    placeHolder.Controls.Add(New LiteralControl("<tr>"))
                    placeHolder.Controls.Add(New LiteralControl("<td>"))
                    placeHolder.Controls.Add(textbox)
                    placeHolder.Controls.Add(New LiteralControl("</td>"))
                    placeHolder.Controls.Add(New LiteralControl("</tr>"))
                Next
                placeHolder.Controls.Add(New LiteralControl("</table>"))

                'If cantidadCheck > 0 Then
                '    checkBoxList.Items(0).Selected = True
                '    checkBoxList.DataBind()
                'End If
                '
                'Busca el tono elegido dentro del combobox
                '
                'If Not checkBoxList.Items.FindByValue(Me.listaDetalleDocumento(e.Row.RowIndex).idTono.ToString) Is Nothing Then
                '    checkBoxList.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).idTono.ToString
                'End If

                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
                cboUM.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM
                cboUM.DataBind()

                If Not cboUM.Items.FindByValue(Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString) Is Nothing Then
                    cboUM.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString
                End If


                Dim cboUM_Peso As DropDownList = CType(e.Row.FindControl("cboUnidadMedida_Peso"), DropDownList)
                cboUM_Peso.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUnidadMedida_Peso
                cboUM_Peso.DataBind()

                If (cboUM_Peso.Items.FindByValue(CStr(Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida_Peso)) IsNot Nothing) Then
                    cboUM_Peso.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida_Peso.ToString
                End If


                If Me.listaDetalleDocumento(e.Row.RowIndex).Kit Then
                    e.Row.ControlStyle.ForeColor = Drawing.Color.Red
                End If

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub onTextChanged_Cantidad(ByVal sender As Object, ByVal e As EventArgs)
        Dim txtCantidad As TextBox = TryCast(sender, TextBox)
        For Each row As GridViewRow In Me.GV_Detalle.Rows
            Dim CheckBoxList As CheckBoxList = TryCast(row.FindControl("checkListTonos"), CheckBoxList)
            For i As Integer = 0 To CheckBoxList.Items.Count - 1
                If CheckBoxList.Items(i).Selected Then
                    ViewState("txt_" & i) = txtCantidad.Text
                End If
            Next
        Next
    End Sub

    Private Sub btnBuscarDocumentoRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarDocumentoRef.Click
        Me.hddFrmModo.Value = CInt(FrmModo.Nuevo)
    End Sub

    Private Sub crearControlesTextbox()
        If getListaDetalleDocumento() IsNot Nothing Then
            'setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = getListaDetalleDocumento()
            Me.GV_Detalle.DataBind()
        End If
    End Sub

    Private Sub btBuscarPersonaGrillas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btBuscarPersonaGrillas.Click
        Try
            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO

            Select Case CInt(Me.hddOpcionBusquedaPersona.Value)
                Case -1  '****** AGENCIA TRANSPORTE

                Case 0  '****** REMITENTE

                Case 1  '****** DESTINATARIO

                Case 2 '******* TRANSPORTISTA
                    If Ddl_Rol.Items.FindByValue("4") IsNot Nothing Then
                        Ddl_Rol.SelectedValue = "4"
                    End If
                Case 3 '******* CHOFER

            End Select

            ViewState.Add("rol", CStr(Ddl_Rol.SelectedValue))


            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)
            'Crea los controles dinamicos
            crearControlesTextbox()
        Catch ex As Exception
            'Crea los controles dinamicos
            crearControlesTextbox()
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnImprimirPicking_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImprimirPicking.Click
        Dim idSerie As Integer = Me.cboSerie.SelectedValue
        Dim codigo As String = Me.txtCodigoDocumento.Text.Trim
        Dim idTipodocumento As Integer = Me.hddIdTipoDocumento.Value

        Dim objPicking As New Negocio.bl_FrmGenerales
        Dim dt As New DataTable
        dt = objPicking.reportePicking(idSerie, codigo, idTipodocumento)

        Dim reporte As New ReportDocument
        Dim tipoFormato As ExportFormatType

        'reporte.Load(Server.MapPath("/Almacen1/Reportes/CrystalReports/rpt_FrmPicking.rpt"))
        reporte = New CR_ReporteFrmPicking
        reporte.SetDataSource(dt)


        tipoFormato = ExportFormatType.PortableDocFormat
        Response.Buffer = False
        Response.ClearContent()
        Response.ClearHeaders()
        reporte.ExportToHttpResponse(tipoFormato, Response, True, "Picking")
        Response.End()
    End Sub


End Class