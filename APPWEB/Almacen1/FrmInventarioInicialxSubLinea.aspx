<%@ Page Title="" Language="vb" AutoEventWireup="false"  MasterPageFile="~/Principal.Master"  CodeBehind="FrmInventarioInicialxSubLinea.aspx.vb" Inherits="APPWEB.FrmInventarioInicialxSubLinea" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script language="javascript" type="text/javascript">
    function imprimir() {
        return (confirm('Desea imprimir el Documento Inv. Inicial para la Empresa / Almac�n seleccionados?'));
    }
    function validarNumeroPunto(elEvento) {
        //obtener caracter pulsado en todos los exploradores
        var evento = elEvento || window.event;
        var caracter = evento.charCode || evento.keyCode;
        //alert("El car�cter pulsado es: " + caracter);
        if (caracter == 46) {
            return true;
        } else {
            if (!onKeyPressEsNumero('event')) {                
                return false;
            }
        }
    }
    function valBlur(event) {
        var id = event.srcElement.id;
        var caja = document.getElementById(id);
        if (CajaEnBlanco(caja)) {
            caja.value=0;
            //alert('Debe ingresar un valor.');
        }
        if (!esDecimal(caja.value)) {
            caja.select();
            caja.focus();
            alert('Valor no v�lido.');
        }        
    }

</script>

    <table style="width: 100%">
        <tr>
            <td>
                <!--<asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>-->
                        <table>
                            <tr>
                                <td style="text-align: left" colspan="2">
                                    <asp:ImageButton ID="btnGuardar" runat="server" CausesValidation="true" 
                                        ImageUrl="~/Imagenes/Guardar_B.JPG" 
                                        onmouseout="this.src='/Imagenes/Guardar_B.JPG';" 
                                        onmouseover="this.src='/Imagenes/Guardar_A.JPG';" Enabled="False" />
                                    <asp:ImageButton ID="btnImprimir" runat="server" 
                                        ImageUrl="~/Imagenes/Imprimir_B.JPG"  Enabled="true"
                                        OnClientClick="return(imprimir());" 
                                        onmouseout="this.src='/Imagenes/Imprimir_B.JPG';" 
                                        onmouseover="this.src='/Imagenes/Imprimir_A.JPG';" />
                                </td>
                            </tr>
                            
                           
                            <tr>
                                <td colspan="2">
                                    <asp:Panel ID="Panel_Cabecera" runat="server">
                                    <table>
                                    <tr>
                                    <td align="right">
                                        <asp:Label ID="Label4" runat="server" CssClass="Texto" Text="Empresa:"></asp:Label>
                                        </td>
                                    <td>
                                        <asp:DropDownList ID="cmbEmpresa"  runat="server" AutoPostBack="True" 
                                            DataTextField="NombreComercial" DataValueField="Id">
                                        </asp:DropDownList>
                                        </td>
                                    <td align="right">
                                        <asp:Label ID="Label20" runat="server" CssClass="Texto" Text="Almac�n:"></asp:Label>
                                        </td>
                                    <td>
                                        <asp:DropDownList ID="cmbAlmacen"  runat="server" DataTextField="Nombre" 
                                            DataValueField="IdAlmacen">
                                        </asp:DropDownList>
                                        </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    </tr>
                                    <tr>
                                    <td align="right">
                                        <asp:Label ID="Label21" runat="server" CssClass="Texto" Text="Tipo Documento:"></asp:Label>
                                        </td>
                                    <td>
                                        <asp:DropDownList ID="cmbTipoDocumento" runat="server" AutoPostBack="False" 
                                            Enabled="false" >
                                            <asp:ListItem Enabled="true" Selected="True" Value="26">Inventario Inicial</asp:ListItem>
                                        </asp:DropDownList>
                                        </td>
                                    <td align="right">
                                        <asp:Label ID="Label26" runat="server" CssClass="Texto" Text="Tipo Operaci�n:"></asp:Label>
                                        </td>
                                    <td>
                                        <asp:DropDownList ID="cmbTipoOperacion" runat="server" Enabled="false" >
                                            <asp:ListItem Selected="True" Value="12">Inventario Inicial</asp:ListItem>
                                        </asp:DropDownList>
                                        </td>
                                    <td align="right">
                                        <asp:Label ID="Label24" runat="server" CssClass="Texto" Text="Estado Doc.:"></asp:Label>
                                        </td>
                                    <td>
                                        <asp:DropDownList ID="cboEstadoDocumento"   runat="server" CssClass="LabelRojo" 
                                            Enabled="false">
                                            <asp:ListItem Selected="True" Value="1">V�lido</asp:ListItem>
                                        </asp:DropDownList>
                                        </td>
                                    <td></td>
                                    <td></td>
                                    </tr>
                                    <tr>
                                    <td align="right">
                                        <asp:Label ID="Label2" runat="server" CssClass="Texto" Text="L�nea:"></asp:Label>
                                        </td>
                                    <td>
                                        <asp:DropDownList ID="cmbLinea"  runat="server" AutoPostBack="true" 
                                            DataTextField="Descripcion" DataValueField="Id">
                                        </asp:DropDownList>
                                        </td>
                                    <td align="right">
                                        <asp:Label ID="Label3" runat="server" CssClass="Texto" Text="SubL�nea:"></asp:Label>
                                        </td>
                                    <td>
                                        <asp:DropDownList ID="cmbSubLinea" runat="server" DataTextField="Nombre" 
                                            DataValueField="Id">
                                        </asp:DropDownList>
                                        </td>
                                    <td>
                                        <asp:ImageButton ID="btnAceptar_SubLinea_Cab" runat="server" 
                                            ImageUrl="~/Imagenes/Aceptar_B.JPG" 
                                            onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" 
                                            onmouseover="this.src='/Imagenes/Aceptar_A.JPG';" />
                                        </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    </tr>
                                    
                                    </table>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    </asp:Panel>
                                </td>
                            </tr>
                            
                           
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:ImageButton ID="btnAtras" runat="server" 
                                        ImageUrl="~/Imagenes/Arriba_B.JPG"                                         
                                        onmouseout="this.src='/Imagenes/Arriba_B.JPG';" 
                                        onmouseover="this.src='/Imagenes/Arriba_A.JPG';" ToolTip="Atr�s" />
                                    <asp:ImageButton ID="btnEditar" runat="server" 
                                        ImageUrl="~/Imagenes/Editar_B.JPG" 
                                        onmouseout="this.src='/Imagenes/Editar_B.JPG';" 
                                        onmouseover="this.src='/Imagenes/Editar_A.JPG';" ToolTip="Editar" />
                                </td>
                            </tr>
                            
                           
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:GridView ID="DGVDetalle" runat="server" AutoGenerateColumns="False" 
                                        Width="100%">
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <Columns>                                            
                                            <asp:BoundField DataField="IdProducto" HeaderText="Id" />
                                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" />
                                            <asp:BoundField DataField="IdUMedida" HeaderText="Id U.M." />
                                            <asp:BoundField DataField="UM" HeaderText="U.M. Principal" />
                                            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad Actual" />                                            
                                            <asp:TemplateField HeaderText="Cantidad">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtCantidad" runat="server" 
                                                        onKeypress="return(validarNumeroPunto(event));"  onblur="return(valBlur(event));"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>'></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="IdDetalleDocumento" HeaderText="Id Detalle" NullDisplayText="0" />
                                        </Columns>
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:HiddenField ID="hddIdDocumento" runat="server" />
                                </td>
                            </tr>
                        </table>
                    <!--</ContentTemplate></asp:UpdatePanel>-->
            </td>
        </tr>
    </table>
</asp:Content>

