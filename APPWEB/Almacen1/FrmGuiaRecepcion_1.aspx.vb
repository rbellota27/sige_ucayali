'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Est� prohibida su reproducci�n total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer p�rrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protecci�n de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmGuiaRecepcion_1
    Inherits System.Web.UI.Page

    Private listaDetalle As List(Of Entidades.DetalleDocumentoView)
    Private objScript As New ScriptManagerClass
    Private objDocumento As New Negocio.DocumentoView

    '*************** ModoFrm
    '******** 0 : INICIO / nuevo - buscar
    '******** 1 : NUEVO / guardar - cancelar
    '******** 2 : EDITAR DOCUMENTO / guardar - cancelar
    '******** 3 : BUSCAR DOCUMENTO / buscar doc - habilitar busqueda
    '******** 4 : DOCUMENTO HALLADO CORRECTAMENTE / editar - anular - imprimir - despachar - remitir - cancelar
    '******** 5 : DOC. GUARDADO CORRECTAMENTE / imprimir - despachar - remitir - cancelar
    '******** 6: OCULTAR TODOS LOS BOTONES

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()
        End If
        Try
            Me.hddCantidadFilas.Value = CStr(objDocumento.SelectCantidadFilasDocumento(CInt(Me.cmbEmpresa.SelectedValue), 25))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {54, 55, 56, 57, 58})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGrabar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGrabar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumento.Enabled = True
        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumento.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFecha.Enabled = True
        Else
            Me.txtFecha.Enabled = False
        End If

    End Sub

    Private Sub inicializarFrm()
        Try

            '********** cargar datos
            cargarDatosCboEmpresa(cmbEmpresa)
            cargarDatosCboTienda(cmbTienda)
            cargarDatosCboAlmacen(cmbAlmacen)
            cargarDatosLinea(cmbLinea)
            cargarDatosSubLinea(cmbSubLinea, CInt(cmbLinea.SelectedValue))
            cargarDatosSerie(Me.cboSerie)

            cargarDatosTipoOperacion(Me.cmbTipoOperacion, CInt(Me.cmbTipoDocumento.SelectedValue))

            Dim objCbo As New Combo
            objCbo.LLenarCboEstadoDocumento(Me.cmbEstadoDocumento)
            Me.hddCantidadFilas.Value = CStr(objDocumento.SelectCantidadFilasDocumento(CInt(Me.cmbEmpresa.SelectedValue), 25))
            verFrmInicio()

            If (Request.QueryString("IdDocumentoRef") <> Nothing) Then
                cargarDocumentoReferencia(CInt(Request.QueryString("IdDocumentoRef")), True)
            End If

            ValidarPermisos()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub actualizarBotonesControl()
        Select Case CInt(hddModoFrm.Value)
            Case 0
                '********* INICIO
                btnNuevo.Visible = True
                btnEditar.Visible = False
                btnAnular.Visible = False
                btnBuscar.Visible = True
                btnBuscarDocumento_GRec.Visible = False
                btnImprimir.Visible = False
                btnCancelar1.Visible = False
                btnGrabar.Visible = False
                txtCodigoDocumento.ReadOnly = True
                Panel_Frm.Enabled = False

                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.cmbAlmacen.Enabled = False
                Me.cboSerie.Enabled = False

            Case 1
                '********* NUEVO
                btnNuevo.Visible = False
                btnEditar.Visible = False
                btnAnular.Visible = False
                btnBuscar.Visible = False
                btnBuscarDocumento_GRec.Visible = False
                btnImprimir.Visible = False
                btnCancelar1.Visible = True
                btnGrabar.Visible = True
                txtCodigoDocumento.ReadOnly = True
                Panel_Frm.Enabled = True

                Me.cmbEmpresa.Enabled = True
                Me.cmbTienda.Enabled = True
                Me.cmbAlmacen.Enabled = True
                Me.cboSerie.Enabled = True

            Case 2

                '******************* EDITAR
                btnNuevo.Visible = False
                btnEditar.Visible = False
                btnAnular.Visible = False
                btnBuscar.Visible = False
                btnBuscarDocumento_GRec.Visible = False
                btnImprimir.Visible = False
                btnCancelar1.Visible = True
                btnGrabar.Visible = True
                txtCodigoDocumento.ReadOnly = True

                Panel_Frm.Enabled = True

                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.cmbAlmacen.Enabled = True
                Me.cboSerie.Enabled = False


            Case 3
                '******************* BUSCAR DOC
                btnNuevo.Visible = False
                btnEditar.Visible = False
                btnAnular.Visible = False
                btnBuscar.Visible = False
                btnBuscarDocumento_GRec.Visible = True
                btnImprimir.Visible = False
                btnCancelar1.Visible = True
                btnGrabar.Visible = False
                txtCodigoDocumento.ReadOnly = False
                txtCodigoDocumento.Focus()
                Panel_Frm.Enabled = True

                Me.cmbEmpresa.Enabled = True
                Me.cmbTienda.Enabled = True
                Me.cmbAlmacen.Enabled = True
                Me.cboSerie.Enabled = True

            Case 4
                '******************* BUSCAR hallado con exito
                btnNuevo.Visible = False
                btnEditar.Visible = True
                btnAnular.Visible = True
                btnBuscar.Visible = False
                btnBuscarDocumento_GRec.Visible = False
                btnImprimir.Visible = True
                btnCancelar1.Visible = True
                btnGrabar.Visible = False
                txtCodigoDocumento.ReadOnly = True
                Panel_Frm.Enabled = False

                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.cmbAlmacen.Enabled = False
                Me.cboSerie.Enabled = False

            Case 5
                '******************* Doc guardado con EXITO
                btnNuevo.Visible = False
                btnEditar.Visible = False
                btnAnular.Visible = False
                btnBuscar.Visible = False
                btnBuscarDocumento_GRec.Visible = False
                btnImprimir.Visible = True
                btnCancelar1.Visible = True
                btnGrabar.Visible = False
                txtCodigoDocumento.ReadOnly = True
                Panel_Frm.Enabled = False

                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.cmbAlmacen.Enabled = False
                Me.cboSerie.Enabled = False
            Case 6
                '******************* deshabilitar todos los botones
                btnNuevo.Visible = False
                btnEditar.Visible = False
                btnAnular.Visible = False
                btnBuscar.Visible = False
                btnBuscarDocumento_GRec.Visible = False
                btnImprimir.Visible = False
                btnCancelar1.Visible = True
                btnGrabar.Visible = False
                txtCodigoDocumento.ReadOnly = True
                Panel_Frm.Enabled = False

                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.cmbAlmacen.Enabled = False
                Me.cboSerie.Enabled = False

        End Select
    End Sub
#Region "************ VISTAS DE FORMULARIOS"
    Private Sub verFrmInicio()
        Panel_Frm.Visible = False
        Panel_Frm.Enabled = True
        Session.Remove("listaDetalle")
        hddModoFrm.Value = "0"
        limpiarFrm()
        actualizarBotonesControl()
    End Sub
    Private Sub verFrmNuevo()
        GenerarCodigo()
        Panel_Frm.Visible = True
        hddModoFrm.Value = "1"
        Me.listaDetalle = New List(Of Entidades.DetalleDocumentoView)
        setListaDetalle(Me.listaDetalle)
        actualizarBotonesControl()
    End Sub

    Private Sub verFrmBuscarDoc()
        Panel_Frm.Visible = True
        hddModoFrm.Value = "3"
        actualizarBotonesControl()
    End Sub
#End Region
#Region "********** CARGAR COMBOS"
    Private Sub cargarEstadoDocumento(ByVal combo As DropDownList)
        Dim obj As New Negocio.EstadoDocumento
        Me.cmbEstadoDocumento.DataSource = obj.SelectCbo
        Me.cmbEstadoDocumento.DataBind()
    End Sub
    Private Sub cargarDatosTipoOperacion(ByVal combo As DropDownList, ByVal IdTipoDocumento As Integer)
        Dim obj As New Negocio.TipoOperacion

        Dim lista As List(Of Entidades.TipoOperacion) = obj.SelectCboxIdTipoDocumento(IdTipoDocumento)
        Dim objTipoOperacion As New Entidades.TipoOperacion(0, "-----")
        lista.Insert(0, objTipoOperacion)

        Me.cmbTipoOperacion.DataSource = lista
        Me.cmbTipoOperacion.DataBind()
    End Sub
    Private Sub cargarDatosCboEmpresa(ByVal cbo As DropDownList)
        Dim objCombo As New Combo
        objCombo.LlenarCboEmpresaxIdUsuario(cbo, CInt(Session("IdUsuario")), False)
    End Sub
    Private Sub cargarDatosCboTienda(ByVal cbo As DropDownList)
        Dim objCombo As New Combo
        objCombo.LlenarCboTiendaxIdEmpresaxIdUsuario(cbo, CInt(Me.cmbEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
    End Sub
    Private Sub cargarDatosCboAlmacen(ByVal cbo As DropDownList)
        Dim objCombo As New Combo
        objCombo.llenarCboAlmacenxIdTienda(cbo, CInt(Me.cmbTienda.SelectedValue), False)
    End Sub
    Private Sub cargarDatosLinea(ByVal comboBox As DropDownList)
        Dim nLinea As New Negocio.Linea
        Dim lista As List(Of Entidades.Linea) = nLinea.SelectAllActivo()
        Dim objLinea As New Entidades.Linea
        objLinea.Id = 0
        objLinea.Descripcion = "-----"
        lista.Insert(0, objLinea)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
    Private Sub cargarDatosSubLinea(ByVal comboBox As DropDownList, ByVal idLinea As Integer)
        Dim nSubLinea As New Negocio.SubLinea
        Dim lista As List(Of Entidades.SubLinea) = nSubLinea.SelectAllActivoxLinea(idLinea)
        Dim objSubLinea As New Entidades.SubLinea
        objSubLinea.Id = 0
        objSubLinea.Nombre = "-----"
        lista.Insert(0, objSubLinea)
        comboBox.DataSource = lista
        comboBox.DataBind()
    End Sub
#End Region
#Region "******* LISTA DETALLE"
    Private Function getListaDetalle() As List(Of Entidades.DetalleDocumentoView)
        Return CType(Session.Item("listaDetalle"), List(Of Entidades.DetalleDocumentoView))
    End Function
    Private Sub setListaDetalle(ByVal lista As List(Of Entidades.DetalleDocumentoView))
        Session.Remove("listaDetalle")
        Session.Add("listaDetalle", lista)
    End Sub
#End Region
    Private Sub cargarDatosSerie(ByVal cbo As DropDownList, Optional ByVal opcion As String = "")
        Dim obj As New Combo
        obj.LLenarCboSeriexIdsEmpTienTipoDoc(cbo, CInt(cmbEmpresa.SelectedValue), CInt(cmbTienda.SelectedValue), CInt(cmbTipoDocumento.SelectedValue))
        cbo.DataBind()
    End Sub
    Private Sub GenerarCodigo()
        Dim obj As New Negocio.Serie
        Try

            If Me.cboSerie.Items.Count = 0 Then
                Me.txtCodigoDocumento.Text = ""
            Else
                Me.txtCodigoDocumento.Text = obj.GenerarCodigo(CInt(Me.cboSerie.SelectedValue))
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosGrillaAddProd()
        Dim nProducto As New Negocio.Producto
        Dim idLinea As Integer = 0
        Dim idSubLinea As Integer = 0
        If cmbLinea.SelectedValue <> "0" Then idLinea = CInt(cmbLinea.SelectedValue)
        If cmbSubLinea.SelectedValue <> "0" Then idSubLinea = CInt(cmbSubLinea.SelectedValue)
        Dim lista As List(Of Entidades.GrillaProducto_M) = nProducto.SelectGrillaProd_M(idLinea, idSubLinea, 0, 0, txtDescripcionProd.Text.Trim)
        If lista.Count = 0 Then

        Else

        End If
        DGVSelProd.DataSource = lista
        DGVSelProd.DataBind()
        txtDescripcionProd.Focus()
    End Sub
    Protected Sub cmbEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbEmpresa.SelectedIndexChanged
        cargarDatosCboTienda(cmbTienda)
        cargarDatosCboAlmacen(cmbAlmacen)
        cargarDatosSerie(Me.cboSerie)
        GenerarCodigo()
    End Sub
    Protected Sub cmbTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbTienda.SelectedIndexChanged
        cargarDatosCboAlmacen(cmbAlmacen)
        cargarDatosSerie(Me.cboSerie)
        GenerarCodigo()
    End Sub
    Private Sub addProductoGrllaDetalle()
        Try
            Me.saveGrillaInListaDetalle()

            '*********  agrego el detalle documento
            Me.listaDetalle = Me.getListaDetalle
            For i As Integer = 0 To DGVSelProd.Rows.Count - 1
                Dim chbAdd As CheckBox = CType(DGVSelProd.Rows(i).Cells(0).FindControl("chbSelectProducto"), CheckBox)
                If chbAdd.Checked Then
                    Dim obj As New Entidades.DetalleDocumentoView
                    Dim objPUM As New Negocio.ProductoUMView

                    obj.IdProducto = CInt(DGVSelProd.Rows(i).Cells(1).Text)
                    obj.Descripcion = CStr(DGVSelProd.Rows(i).Cells(2).Text)
                    obj.IdUMPrincipal = CInt(DGVSelProd.Rows(i).Cells(6).Text)
                    obj.NomUMPrincipal = CStr(DGVSelProd.Rows(i).Cells(3).Text)

                    obj.ListaProdUM = objPUM.SelectCboxIdProducto(obj.IdProducto)
                    Me.listaDetalle.Add(obj)
                End If
            Next

            DGVDetalle.DataSource = Me.listaDetalle
            DGVDetalle.DataBind()

            Me.setListaDetalle(Me.listaDetalle)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub DGVDetalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGVDetalle.SelectedIndexChanged
        quitarProductoGrllaDetalle()
    End Sub
    Private Sub quitarProductoGrllaDetalle()
        Me.saveGrillaInListaDetalle()
        Me.listaDetalle = Me.getListaDetalle
        Me.listaDetalle.RemoveAt(DGVDetalle.SelectedIndex)
        Me.setListaDetalle(Me.listaDetalle)
        DGVDetalle.DataSource = Me.listaDetalle
        DGVDetalle.DataBind()
    End Sub
    Private Sub saveGrillaInListaDetalle()
        Try
            Me.listaDetalle = Me.getListaDetalle
            For i As Integer = 0 To DGVDetalle.Rows.Count - 1
                With Me.listaDetalle(i)
                    .IdUMedida = CInt(CType(DGVDetalle.Rows(i).Cells(3).FindControl("cmbUMedida"), DropDownList).SelectedValue)
                    .UM = (CType(DGVDetalle.Rows(i).Cells(3).FindControl("cmbUMedida"), DropDownList).SelectedItem.ToString)
                    .Cantidad = CDec(CType(DGVDetalle.Rows(i).Cells(3).FindControl("txtCantidad"), TextBox).Text)
                End With
            Next
            Me.setListaDetalle(Me.listaDetalle)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub DGVDetalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGVDetalle.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cmbUMedida1 As DropDownList
                Dim gvrow As GridViewRow = CType(e.Row.Cells(3).NamingContainer, GridViewRow)
                cmbUMedida1 = CType(gvrow.FindControl("cmbUMedida"), DropDownList)

                Dim value As String = cmbUMedida1.SelectedValue

                Me.listaDetalle = Me.getListaDetalle
                If cmbUMedida1 IsNot Nothing Then
                    cmbUMedida1.DataSource = Me.listaDetalle.Item(e.Row.RowIndex).ListaProdUM
                    cmbUMedida1.DataBind()
                End If

                cmbUMedida1.SelectedValue = CStr(Me.listaDetalle.Item(e.Row.RowIndex).IdUMedida)

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub




#Region "BOTONES DE CONTROL"
    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        verFrmNuevo()
    End Sub
    Private Sub limpiarFrm()
        hddIdDocumento.Value = "0"
        lblCodigo_DocRelacional.Text = ""
        txtCodigoBuscarDoc.Text = ""
        txtCodigoDocumento.Text = ""
        txtDescripcionProd.Text = ""
        txtDNI.Text = ""

        Dim obj As New Negocio.FechaActual
        txtFecha.Text = Format(obj.SelectFechaActual, "dd/MM/yyyy")

        txtIdCliente.Text = ""
        hddIdDocumento_DocRelacional.Value = ""
        txtObservaciones.Text = ""
        txtRazonSocial.Text = ""
        txtRUC.Text = ""
        lblSerie_DocRelacional.Text = ""
        Me.lblTipoDocumento_DocRelacional.Text = ""
        txtSerieBuscarDoc.Text = ""

        cmbTipoOperacion.SelectedIndex = 0
        cmbEstadoDocumento.SelectedValue = "1"

        DGVDetalle.DataSource = Nothing
        DGVDetalle.DataBind()

        DGV_BuscarDoc.DataSource = Nothing
        DGV_BuscarDoc.DataBind()

    End Sub
    Private Sub mostrarBotonesControlCRUD(ByVal visible1 As Boolean)
        btnNuevo.Visible = visible1
        btnGrabar.Visible = Not visible1
        btnCancelar1.Visible = Not visible1
        btnImprimir.Visible = Not visible1
    End Sub
    Protected Sub btnCancelar1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar1.Click
        verFrmInicio()
    End Sub
#End Region
    Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGrabar.Click
        Me.Agregar()
    End Sub


    Private Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea.SelectedIndexChanged
        cargarDatosSubLinea(cmbSubLinea, CInt(cmbLinea.SelectedValue))
    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        GenerarCodigo()
    End Sub

    Protected Sub btnBuscarDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumento.Click
        cargarRegistrosBuscarDocumento(CInt(txtSerieBuscarDoc.Text), CInt(txtCodigoBuscarDoc.Text))
    End Sub
    
    Private Sub DGV_BuscarDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_BuscarDoc.SelectedIndexChanged

        Try
            cargarDocumentoReferencia(CInt(CType(Me.DGV_BuscarDoc.SelectedRow.FindControl("hddIdDocumento_Find"), HiddenField).Value), False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        imprimirDocGuiaRecepcion()
    End Sub
    Private Sub imprimirDocGuiaRecepcion()
        Try
            Response.Redirect("~/Reportes/visor.aspx?iReporte=8&IdDocumento=" & hddIdDocumento.Value)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAddProductos_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddProductos.Click
        addProductoGrllaDetalle()
    End Sub


    Private Sub cargarDatosPersonaGrilla(ByVal grilla As GridView, ByVal nombre As String)
        Try
            Dim objPersonaView As New Negocio.PersonaView
            grilla.DataSource = objPersonaView.SelectActivoxNombre(nombre)
            grilla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub mostrarDatosRemitente_Sel(ByVal IdPersona As Integer)
        Try
            Dim objPersonaView As New Negocio.PersonaView
            Dim obj As Entidades.PersonaView = objPersonaView.SelectxIdPersona(IdPersona)
            If obj IsNot Nothing Then
                txtRazonSocial.Text = obj.getNombreParaMostrar
                txtIdCliente.Text = obj.IdPersona.ToString
                txtRUC.Text = obj.Ruc
                txtDNI.Text = obj.Dni
            End If
            objScript.offCapa(Me, "capaRemitente")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub





#Region "************************** BUSQUEDA PRODUCTO"
    Protected Sub btnBuscarGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla.Click
        ViewState.Add("nombre_BuscarProducto", txtDescripcionProd.Text)
        ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea.SelectedValue)
        ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea.SelectedValue)

        If txtCodSubLinea_BuscarProd.Text.Trim.Length > 0 Then
            ViewState.Add("CodigoSubLinea_BuscarProducto", txtCodSubLinea_BuscarProd.Text)
        Else
            ViewState.Add("CodigoSubLinea_BuscarProducto", 0)
        End If

        cargarDatosProductoGrilla(Me.DGVSelProd, txtDescripcionProd.Text, CInt(Me.cmbLinea.SelectedValue), CInt(Me.cmbSubLinea.SelectedValue), CInt(ViewState("CodigoSubLinea_BuscarProducto")), 0)
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal nomProducto As String, ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal codSubLinea As Integer, ByVal tipomov As Integer)
        Try

            Dim index As Integer = 0
            Select Case tipomov
                Case 0 '**************** INICIO
                    index = 0
                Case 1 '**************** Anterior
                    index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
                Case 2 '**************** Posterior
                    index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
                Case 3 '**************** IR
                    index = (CInt(txtPageIndexGO_Productos.Text) - 1)
            End Select

            Dim lista As List(Of Entidades.GrillaProducto_M) = (New Negocio.Producto).SelectBusquedaProdxParams_Standar_Paginado(idlinea, idsublinea, nomProducto, codSubLinea, index, grilla.PageSize)

            If lista.Count <= 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            Else
                grilla.DataSource = lista
                grilla.DataBind()
                txtPageIndex_Productos.Text = CStr(index + 1)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        cargarDatosProductoGrilla(Me.DGVSelProd, CStr(ViewState("nombre_BuscarProducto")), CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CInt(ViewState("CodigoSubLinea_BuscarProducto")), 1)
    End Sub
    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        cargarDatosProductoGrilla(Me.DGVSelProd, CStr(ViewState("nombre_BuscarProducto")), CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CInt(ViewState("CodigoSubLinea_BuscarProducto")), 2)
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        cargarDatosProductoGrilla(Me.DGVSelProd, CStr(ViewState("nombre_BuscarProducto")), CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CInt(ViewState("CodigoSubLinea_BuscarProducto")), 3)
    End Sub
#End Region


#Region "************************** BUSQUEDA PERSONA"
    Private Sub LLenarCliente(ByVal objPersonaView As Entidades.PersonaView)
        If objPersonaView IsNot Nothing Then

            Me.txtRazonSocial.Text = objPersonaView.getNombreParaMostrar
            Me.txtDNI.Text = objPersonaView.Dni
            Me.txtRUC.Text = objPersonaView.Ruc
            Me.txtIdCliente.Text = objPersonaView.IdPersona.ToString

        End If

    End Sub
    Private Sub addPersona(ByVal IdPersona As Integer)
        Try

            'Dim objCliente As Entidades.PersonaView = (New Negocio.Cliente).SelectxId_Ventas(IdPersona, CInt(Me.cmbEmpresa.SelectedValue))
            Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

            Me.LLenarCliente(objPersonaView)

            objScript.offCapa(Me, "capaPersona")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub DGV_BuscarPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_BuscarPersona.SelectedIndexChanged
        addPersona(CInt(Me.DGV_BuscarPersona.SelectedRow.Cells(1).Text))
    End Sub
    Private Sub btnBuscarPersona_Grilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarPersona_Grilla.Click
        ViewState.Add("TextoBuscarPersona", txtBuscarPersona_Grilla.Text)
        ViewState.Add("OpcionBuscarPersona", Me.cmbFiltro_BuscarPersona.SelectedValue)
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, txtBuscarPersona_Grilla.Text, CInt(Me.cmbFiltro_BuscarPersona.SelectedValue), 0)
    End Sub
    Private Sub cargarDatosPersonaGrilla(ByVal grilla As GridView, ByVal texto As String, ByVal opcion As Integer, ByVal tipoMov As Integer)
        Try
            '********** opcion 0: descripcion
            '********** opcion 1: ruc
            '********** opcion 2: dni
            Dim index As Integer = 0
            Select Case tipoMov
                Case 0 '**************** INICIO
                    index = 0
                Case 1 '**************** Anterior
                    index = (CInt(txtPageIndex_Persona.Text) - 1) - 1
                Case 2 '**************** Posterior
                    index = (CInt(txtPageIndex_Persona.Text) - 1) + 1
                Case 3 '***************** IR
                    index = (CInt(txtPageIndexGO_Persona.Text) - 1)
            End Select

            Dim listaPV As List(Of Entidades.PersonaView) = (New Negocio.PersonaView).SelectActivoxParam_Paginado(texto, opcion, grilla.PageSize, index)

            If listaPV.Count <= 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            Else
                grilla.DataSource = listaPV
                grilla.DataBind()
                txtPageIndex_Persona.Text = CStr(index + 1)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnAnterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 1)
    End Sub
    Private Sub btnPosterior_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 2)
    End Sub
    Private Sub btnIr_Persona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Persona.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, CStr(ViewState("TextoBuscarPersona")), CInt((ViewState("OpcionBuscarPersona"))), 3)
    End Sub
#End Region



    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        verFrmBuscarDoc()
    End Sub

    Protected Sub btnBuscarDocumento_GRec_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumento_GRec.Click
        cargarDocumento(CInt(Me.cboSerie.SelectedValue), CInt(txtCodigoDocumento.Text))
    End Sub
    Private Sub cargarDocumento(ByVal IdSerie As Integer, ByVal codigo As Integer)
        Try

            Dim objDocumento As Entidades.DocGuiaRecepcion = (New Negocio.DocGuiaRecepcion).DocumentoGuiaRecepcionSelectCab(IdSerie, codigo, 0)
            Me.listaDetalle = obtenerListaDetalleDocumentoView(objDocumento.Id)

            '*********** CABECERA
            If objDocumento.IdAlmacen <> 0 Then
                Me.cmbAlmacen.SelectedValue = CStr(objDocumento.IdAlmacen)
            End If
            Me.cmbTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
            Me.cmbEstadoDocumento.SelectedValue = CStr(objDocumento.IdEstadoDoc)
            Me.txtFecha.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
            hddIdDocumento.Value = CStr(objDocumento.Id)

            '************* DOCUMENTO REFERENCIA
            Me.lblTipoDocumento_DocRelacional.Text = objDocumento.TipoDocumentoRef
            Me.lblSerie_DocRelacional.Text = objDocumento.DocRef_Serie
            Me.lblCodigo_DocRelacional.Text = objDocumento.DocRef_Codigo
            Me.hddIdDocumento_DocRelacional.Value = CStr(objDocumento.IdDocumentoRef)

            '********** CLIENTE
            LLenarCliente(New Entidades.PersonaView(objDocumento.IdPersona, objDocumento.DescripcionPersona, objDocumento.Ruc, objDocumento.Dni))

            '*********** DETALLE
            setListaDetalle(Me.listaDetalle)
            DGVDetalle.DataSource = Me.listaDetalle
            DGVDetalle.DataBind()

            '*********** OBSERVACIONES
            Me.txtObservaciones.Text = objDocumento.Observacion

            hddModoFrm.Value = "4"
            actualizarBotonesControl()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerListaDetalleDocumentoView(ByVal iddocumento As Integer) As List(Of Entidades.DetalleDocumentoView)
        Dim lista As New List(Of Entidades.DetalleDocumentoView)
        Try

            lista = (New Negocio.DetalleDocumento).DetalleDocumentoViewSelectxIdDocumento(iddocumento)
            For i As Integer = 0 To lista.Count - 1

                lista(i).ListaProdUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(lista(i).IdProducto)

            Next
        Catch ex As Exception
            Throw ex
        End Try
        Return lista
    End Function

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditar.Click

        hddModoFrm.Value = "2"
        actualizarBotonesControl()


    End Sub

    Private Sub Agregar()

        Try
            Me.saveGrillaInListaDetalle()

            Dim Documento As Entidades.Documento = obtenerDocumentoCab()

            Dim listaDetalleDoc As New List(Of Entidades.DetalleDocumento)
            Dim listaMovAlmacen As New List(Of Entidades.MovAlmacen)
            Me.listaDetalle = getListaDetalle()
            For i As Integer = 0 To Me.listaDetalle.Count - 1
                Dim obj As New Entidades.DetalleDocumento
                Dim objMovAlmacen As New Entidades.MovAlmacen
                With obj
                    .Cantidad = Me.listaDetalle(i).Cantidad
                    .IdProducto = Me.listaDetalle(i).IdProducto
                    .IdUnidadMedida = Me.listaDetalle(i).IdUMedida
                    .UMedida = Me.listaDetalle(i).UM
                End With
                listaDetalleDoc.Add(obj)

                With objMovAlmacen

                    .CantidadMov = Me.listaDetalle(i).Cantidad
                    .Comprometido = False
                    .Factor = 1
                    .IdAlmacen = CInt(Me.cmbAlmacen.SelectedValue)
                    .IdEmpresa = CInt(Me.cmbEmpresa.SelectedValue)

                    '******************* obtengo el metodo valuacion vigente
                    Dim objMetValuacion As New Negocio.MetodoValuacion
                    .IdMetodoV = objMetValuacion.SelectIdMetValVigente

                    .IdProducto = Me.listaDetalle(i).IdProducto
                    .IdTienda = CInt(Me.cmbTienda.SelectedValue)
                    .IdTipoOperacion = CInt(Me.cmbTipoOperacion.SelectedValue)
                    .IdUMPrincipal = Me.listaDetalle(i).IdUMPrincipal
                    .IdUnidadMedida = Me.listaDetalle(i).IdUMedida
                    .UMPrincipal = Me.listaDetalle(i).NomUMPrincipal
                End With
                listaMovAlmacen.Add(objMovAlmacen)
            Next

            '****************** Observaciones
            Dim objObservacion As Entidades.Observacion = Nothing
            If txtObservaciones.Text.Trim.Length > 0 Then
                objObservacion = New Entidades.Observacion
                objObservacion.Observacion = txtObservaciones.Text
                If CInt(hddModoFrm.Value) = 2 Then
                    objObservacion.IdDocumento = Documento.Id
                End If
            End If

            '********************* Obtengo el Id del documento Relacionado
            Dim IdDocumentoRelacionado As Integer = 0
            If IsNumeric(hddIdDocumento_DocRelacional.Value) Then
                IdDocumentoRelacionado = CInt(hddIdDocumento_DocRelacional.Value)
            End If
            Dim idusuario As Integer
            idusuario = CType(Session("IdUsuario"), Integer)
            Dim NegDoc As New Negocio.Documento
            Select Case CInt(hddModoFrm.Value)

                Case 1

                    '*********** NUEVO


                    Dim iddoc As Integer = NegDoc.InsertDocumentoGuia_Almacen(idusuario, Documento, listaDetalleDoc, listaMovAlmacen, objObservacion, IdDocumentoRelacionado, Nothing, Nothing, Nothing, Nothing, Nothing, False)
                    If iddoc <> -1 Then
                        hddIdDocumento.Value = iddoc.ToString
                        hddModoFrm.Value = "5"
                        actualizarBotonesControl()
                        objScript.mostrarMsjAlerta(Me, "La operaci�n finaliz� con �xito.")
                    Else
                        objScript.mostrarMsjAlerta(Me, "Se produjeron fallos en el registro.")
                    End If

                Case 2

                    '************** EDITAR
                    If ((New Negocio.Documento).actualizarGuiaRecepcion(2, Documento, listaDetalleDoc, objObservacion, IdDocumentoRelacionado)) Then
                        hddModoFrm.Value = "5"
                        actualizarBotonesControl()
                        objScript.mostrarMsjAlerta(Me, "El Documento fue actualizado con �xito.")
                    Else
                        Throw New Exception("Problemas en la Operaci�n.")
                    End If
            End Select

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento
            .FechaEntrega = CDate(Me.txtFecha.Text)
            .FechaEmision = CDate(Me.txtFecha.Text)
            .IdEmpresa = CInt(cmbEmpresa.SelectedValue)
            .IdTienda = CInt(cmbTienda.SelectedValue)
            .IdTipoDocumento = CInt(cmbTipoDocumento.SelectedValue)
            .IdTipoOperacion = CInt(Me.cmbTipoOperacion.SelectedValue)
            .IdSerie = CInt(cboSerie.SelectedValue)
            .Codigo = txtCodigoDocumento.Text
            .IdRemitente = CInt(txtIdCliente.Text)
            .IdPersona = CInt(txtIdCliente.Text)


            .IdEstadoDoc = CInt(Me.cmbEstadoDocumento.SelectedValue)

            '************** Si es edici�n el ESTADO = ACTIVO
            If (CInt(hddModoFrm.Value) = 2) Then .IdEstadoDoc = 1



            .Serie = Me.cboSerie.SelectedItem.ToString
            .IdAlmacen = CInt(cmbAlmacen.SelectedValue)
            If CInt(hddModoFrm.Value) = 1 Then
                '************ NUEVO
                .Id = Nothing
            ElseIf CInt(hddModoFrm.Value) = 2 Then
                '************ EDITAR
                .Id = CInt(hddIdDocumento.Value)
            End If
        End With

        Return objDocumento

    End Function

    Protected Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnular.Click

        anularGuiaRecepcion(CInt(hddIdDocumento.Value))

    End Sub
    Private Sub anularGuiaRecepcion(ByVal IdDocumento As Integer)
        Try
            If (New Negocio.Documento).AnularGuiaRecepcionxIdDocumento(IdDocumento) Then

                objScript.mostrarMsjAlerta(Me, "La anulaci�n del Documento se realiz� con �xito.")

                cmbEstadoDocumento.SelectedValue = "2"
                hddModoFrm.Value = "6"
                actualizarBotonesControl()

            Else
                Throw New Exception("Problemas en la Operaci�n.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer, ByVal verNuevo As Boolean)

        Try

            If (verNuevo) Then
                verFrmNuevo()
            End If

            '***************** Documento
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)
            Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)
            Dim listaDetalle As List(Of Entidades.DetalleDocumentoView) = (New Negocio.DetalleDocumento).DetalleDocumentoViewSelectxIdDocumento(IdDocumentoRef)

            cargarDocumentoRef_Interface(objDocumento, objPersona, listaDetalle)

            objScript.offCapa(Me, "capaBuscarDocumento")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarDocumentoRef_Interface(ByVal objDocumento As Entidades.Documento, ByVal objPersona As Entidades.PersonaView, ByVal listaDetalle As List(Of Entidades.DetalleDocumentoView))

        '**************** Datos Doc Referencia
        Me.hddIdDocumento_DocRelacional.Value = CStr(objDocumento.Id)
        Me.lblSerie_DocRelacional.Text = CStr(objDocumento.Serie)
        Me.lblCodigo_DocRelacional.Text = CStr(objDocumento.Codigo)
        Me.lblTipoDocumento_DocRelacional.Text = objDocumento.NomTipoDocumento

        '******************* Persona
        Me.txtRazonSocial.Text = objPersona.Descripcion
        Me.txtIdCliente.Text = CStr(objPersona.IdPersona)
        Me.txtDNI.Text = objPersona.Dni
        Me.txtRUC.Text = objPersona.Ruc

        '****************  DETALLE
        Me.listaDetalle = Me.getListaDetalle
        For i As Integer = 0 To listaDetalle.Count - 1

            Dim obj As New Entidades.DetalleDocumentoView

            obj.Cantidad = listaDetalle(i).Cantidad
            obj.IdProducto = listaDetalle(i).IdProducto
            obj.Descripcion = listaDetalle(i).Descripcion
            obj.IdUMPrincipal = listaDetalle(i).IdUMPrincipal
            obj.NomUMPrincipal = listaDetalle(i).NomUMPrincipal
            obj.IdUMedida = listaDetalle(i).IdUMedida
            obj.ListaProdUM = (New Negocio.ProductoUMView).SelectxIdProducto(obj.IdProducto)
            Me.listaDetalle.Add(obj)

        Next
        DGVDetalle.DataSource = Me.listaDetalle
        DGVDetalle.DataBind()
        Me.setListaDetalle(Me.listaDetalle)

    End Sub
    Private Sub cargarRegistrosBuscarDocumento(ByVal serie As Integer, ByVal codigo As Integer)
        Try
            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoView).DocumentoBuscarDocxSeriexCodigo(serie, codigo, CInt(Me.cmbTipoOperacion.SelectedValue))
            If (lista.Count = 0) Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            Else
                DGV_BuscarDoc.DataSource = lista
                DGV_BuscarDoc.DataBind()

                objScript.onCapa(Me, "capaBuscarDocumento")

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class
