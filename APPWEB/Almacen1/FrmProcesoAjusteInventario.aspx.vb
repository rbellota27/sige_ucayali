﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmProcesoAjusteInventario
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Public nomEmpresaAjuste As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then


            inicializarFrm()


        End If

        '*********** Seteo las monedas
        Me.lblMonedaFaltante.Text = Me.cboMoneda.SelectedItem.ToString
        Me.lblMonedaSobrante.Text = Me.cboMoneda.SelectedItem.ToString

        '********** Nombre de la Empresa a la cual hacer el ajuste
        Me.nomEmpresaAjuste = Me.cboEmpresa.SelectedItem.ToString

    End Sub

    '*************** ModoFrm
    '******** 0 : INICIO / nuevo - buscar
    '******** 1 : NUEVO / guardar - cancelar
    '******** 2 : EDITAR DOCUMENTO / guardar - cancelar
    '******** 3 : BUSCAR DOCUMENTO / buscar doc - habilitar busqueda
    '******** 4 : DOCUMENTO HALLADO CORRECTAMENTE / editar - anular - imprimir - despachar - remitir - cancelar
    '******** 5 : DOC. GUARDADO CORRECTAMENTE / imprimir - despachar - remitir - cancelar
    '******** 6 : OCULTAR TODOS LOS BOTONES
    '**********************************************

    Private Sub actualizarControlesFrmModo()

        Select Case CInt(hddFrmModo.Value)

            Case 0 '************* INICIO

                limpiarFrm()

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnAnular.Visible = False

                Panel_Cab.Enabled = True
                Panel_Det.Enabled = False

            Case 1 '************** Nuevo
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnImprimir.Visible = False

                Me.btnAnular.Visible = False

                Panel_Cab.Enabled = True
                Panel_Det.Enabled = True

            Case 2 '*********************** Editar
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnImprimir.Visible = True

                Me.btnAnular.Visible = False

                Panel_Cab.Enabled = False
                Panel_Det.Enabled = True
            Case 3 '**************************** Buscar documento
                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumento.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = True

                Me.txtCodigoDocumento.Text = ""
                Me.txtCodigoDocumento.Focus()
                Me.btnImprimir.Visible = False


                Me.btnAnular.Visible = False

                Panel_Cab.Enabled = True
                Panel_Det.Enabled = False
            Case 4 '********* documento hallado correctamente

                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = True
                Me.btnImprimir.Visible = True

                Me.btnAnular.Visible = True

                Panel_Cab.Enabled = False
                Panel_Det.Enabled = False
            Case 5 '********* documento guardado correctamente

                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.btnImprimir.Visible = True

                Me.GV_Detalle.DataSource = Nothing
                Me.GV_Detalle.DataBind()
                Me.txtCostoFaltante.Text = ""
                Me.txtCostoSobrante.Text = ""

                Me.btnAnular.Visible = False

                Panel_Cab.Enabled = False
                Panel_Det.Enabled = True
            Case 6  '*********** Inhabilitamos todos los controles excepto cancelar

                Me.btnNuevo.Visible = False
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumento.Visible = False
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = True
                Me.btnImprimir.Visible = False
                Me.btnAnular.Visible = False

                Panel_Cab.Enabled = False
                Panel_Det.Enabled = False

        End Select


    End Sub



    Private Sub inicializarFrm()
        Try

            '************************** cargamos los controles
            Dim objCombo As New Combo
            With objCombo

                '************ Controles del Frm Principal
                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .llenarCboTiendaxIdEmpresaxIdUsuario_Caja(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboEstadoDocumento(Me.cboEstado)
                .LlenarCboMoneda(Me.cboMoneda)
                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.cboTipoDocumento.SelectedValue), True)

                .llenarCboAlmacen(Me.cboAlmacen_TI, False)
            End With

            GenerarCodigoDocumento() '********* Llena la serie y genera el codigo

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            '********** Cargamos el Documento Toma Inventario
            If (Request.QueryString("IdDocumentoTI") IsNot Nothing) Then

                cargarDocumentoTomaInventario(CInt(Request.QueryString("IdDocumentoTI")))
                cargarDocumentoAjusteInventarioRelacionado(CInt(Request.QueryString("IdDocumentoTI")))
                '' '' ''cargarDocumentoTomaInventario(27767)
                '' '' ''cargarDocumentoAjusteInventarioRelacionado(27767)

            Else

                Me.hddFrmModo.Value = "0"
                actualizarControlesFrmModo()

            End If




        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos [ inicializarFrm() ].")
        End Try

    End Sub

    Private Sub cargarDocumentoTomaInventario(ByVal IdDocTomaInventario As Integer)
        Try

            Dim objDocumentoTI As Entidades.Documento = (New Negocio.TomaInventario).SelectxIdDocumento(IdDocTomaInventario)

            If (objDocumentoTI Is Nothing) Then
                objScript.mostrarMsjAlerta(Me, "El Documento no existe.")
                Return
            Else


                '******* Carga los datos de la cab. del documento Toma Inv.
                cargarDocumentoTomaInv(objDocumentoTI)


                '********** Cargamos los controles de Linea y SubLinea Detalle
                Me.cboLinea.DataSource = (New Negocio.TomaInventario).SelectLineaxIdDocumento(objDocumentoTI.Id)
                Me.cboLinea.DataBind()

                Me.cboSubLinea.DataSource = (New Negocio.TomaInventario).SelectSubLineaxIdDocumentoxIdLinea(objDocumentoTI.Id, CInt(Me.cboLinea.SelectedValue))
                Me.cboSubLinea.DataBind()


            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de Datos.")
        End Try
    End Sub

    Private Sub cargarDocumentoTomaInv(ByVal objDocumentoTI As Entidades.Documento)

        With objDocumentoTI
            Me.lblNroDocTomaInventario.Text = .Serie + " - " + .Codigo
            Me.lblFechaEmisionDocTomaInv.Text = Format(.FechaEmision, "dd/MM/yyyy")
            Me.lblNomEmpleado.Text = .NomEmpleado
            Me.lblNomAlmacen.Text = .NomAlmacen
            Me.lblFechaTomaInv.Text = Format(.FechaEntrega, "dd/MM/yyyy")
            Me.lblNomEmpresaTomaInv.Text = .NomEmpresaTomaInv
            Me.lblNomEstadoDocTomaInv.Text = .NomEstadoDocumento

            Me.hddIdDocTomaInv.Value = CStr(.Id)
            Me.hddIdAlmacen.Value = CStr(objDocumentoTI.IdAlmacen)

        End With

    End Sub

    Private Sub GenerarCodigoDocumento()
        Try

            Dim objCombo As New Combo
            objCombo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboTipoDocumento.SelectedValue))

            If (IsNumeric(Me.cboSerie.SelectedValue)) Then
                Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
            Else
                Me.txtCodigoDocumento.Text = ""
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub


    Protected Sub btnMostrarDatos_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnMostrarDatos.Click
        cargarDatosTomaInvDetalle(CInt(hddIdDocTomaInv.Value), CInt(Me.cboSubLinea.SelectedValue), CInt(Me.cboMoneda.SelectedValue))
    End Sub

    Private Sub cargarDatosTomaInvDetalle(ByVal IdDocumentoTomaInv As Integer, ByVal IdSubLinea As Integer, ByVal IdMoneda As Integer)

        Try

            Dim lista As List(Of Entidades.DetalleDocumentoTomaInventario) = (New Negocio.TomaInventario).SelectDetallexIdDocumentoxIdSubLinea_Ajuste(IdDocumentoTomaInv, IdSubLinea, IdMoneda)
            Me.GV_Detalle.DataSource = lista
            Me.GV_Detalle.DataBind()
            calcularCostoTotal(lista)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")


        End Try


    End Sub
    Private Sub calcularCostoTotal(ByVal lista As List(Of Entidades.DetalleDocumentoTomaInventario))
        Dim costoFaltante As Decimal = 0
        Dim costoSobrante As Decimal = 0

        For i As Integer = 0 To lista.Count - 1

            '******** Calculamos los costos de solo los PENDIENTES
            If (lista(i).Ajustado = "0") Then
                costoFaltante = costoFaltante + lista(i).CostoFaltante
                costoSobrante = costoSobrante + lista(i).CostoSobrante
            End If

        Next

        Me.txtCostoFaltante.Text = CStr(Math.Round(costoFaltante, 2))
        Me.txtCostoSobrante.Text = CStr(Math.Round(costoSobrante, 2))

    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCombo As New Combo
            objCombo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Dim objCombo As New Combo
        'objCombo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
        GenerarCodigoDocumento()
    End Sub

    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged

        Try
            If (IsNumeric(Me.cboSerie.SelectedValue)) Then
                Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
            Else
                Me.txtCodigoDocumento.Text = ""
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try


    End Sub

    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged
        Try
            Me.cboSubLinea.DataSource = (New Negocio.TomaInventario).SelectSubLineaxIdDocumentoxIdLinea(CInt(hddIdDocTomaInv.Value), CInt(Me.cboLinea.SelectedValue))
            Me.cboSubLinea.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub

    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound

        If (e.Row.RowType = DataControlRowType.DataRow) Then


            Dim gvrow As GridViewRow = CType(e.Row.Cells(12).NamingContainer, GridViewRow)

            If (CType(gvrow.FindControl("hddIsAjustado"), HiddenField).Value = "0") Then
                gvrow.BackColor = Drawing.Color.Yellow
            End If

        End If

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        registrarProcesoAjuste()

    End Sub
    Private Sub registrarProcesoAjuste()
        Try

            Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
            Dim listaDetalle As List(Of Entidades.DetalleDocumento) = obtenerListaDetalle()
            Dim IdDocumentoAjusteInv As Integer = (New Negocio.TomaInventario).registrarProcesoAjusteInventario(CInt(Me.hddIdDocTomaInv.Value), objDocumento, listaDetalle)

            If (IdDocumentoAjusteInv > 0) Then

                '********** Almacenamos el IdDocumentoAjusteInv
                Me.hddIdDocumento.Value = CStr(IdDocumentoAjusteInv)

                Me.hddFrmModo.Value = "5"
                actualizarControlesFrmModo()

                objScript.mostrarMsjAlerta(Me, "El registro se realizó con éxito.")

            Else

                Throw New Exception

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la Operación.")
        End Try
    End Sub

    Private Function obtenerListaDetalle() As List(Of Entidades.DetalleDocumento)

        '************ El detalle mostrado es del Doc. Toma Inventario

        Dim lista As New List(Of Entidades.DetalleDocumento)

        For i As Integer = 0 To GV_Detalle.Rows.Count - 1

            Dim fila As GridViewRow = GV_Detalle.Rows(i)

            If (CType(fila.FindControl("chbAjustar"), CheckBox).Checked) Then

                Dim objDetalle As New Entidades.DetalleDocumento

                With objDetalle

                    .IdProducto = CInt(CType(fila.FindControl("lblIdProducto"), Label).Text)
                    .IdDetalleAfecto = CInt(CType(fila.FindControl("hddIdDetalleDocumento"), HiddenField).Value)
                    .IdUnidadMedida = CInt(CType(fila.FindControl("hddIdUnidadMedida"), HiddenField).Value)
                    .UMedida = CStr(fila.Cells(2).Text)

                    If (CDec(fila.Cells(6).Text) > 0) Then
                        '********** Faltante
                        .Cantidad = CDec(fila.Cells(6).Text)
                        .Factor = -1
                    Else
                        '********* Sobrante
                        .Cantidad = CDec(fila.Cells(7).Text)
                        .Factor = 1
                    End If

                End With

                lista.Add(objDetalle)

            End If

        Next


        Return lista

    End Function

    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento


        With objDocumento

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Codigo = Me.txtCodigoDocumento.Text
            .Serie = Me.cboSerie.SelectedItem.ToString
            .IdTipoDocumento = CInt(Me.cboTipoDocumento.SelectedValue)
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdEstadoDoc = CInt(Me.cboEstado.SelectedValue)
            .FechaEmision = CDate(Me.txtFechaEmision.Text)

            '**************** Almaceno la Fecha de la Toma Inv. 
            .FechaEntrega = CDate(Me.lblFechaTomaInv.Text)

            .IdAlmacen = CInt(Me.hddIdAlmacen.Value)

            If (IsNumeric(Me.hddIdDocumento.Value) And Me.hddIdDocumento.Value.Trim.Length > 0) Then
                .Id = CInt(Me.hddIdDocumento.Value)
            Else
                .Id = Nothing
            End If



        End With

        Return objDocumento

    End Function


    Private Sub cargarDocumentoAjusteInventarioRelacionado(ByVal IdDocTomaInventario As Integer)

        Try

            Dim objDocumento As Entidades.Documento = (New Negocio.TomaInventario).SelectDocAjusteInventarioxIdDocTomaInv(IdDocTomaInventario)

            If (objDocumento IsNot Nothing) Then

                '************ Existe un Doc Ajuste de Inventario para el Doc Toma Inventario
                cargarDocumentoAjusteInventarioCab(objDocumento)

                Me.hddFrmModo.Value = "2"
                actualizarControlesFrmModo()

            Else

                Me.hddFrmModo.Value = "1"
                actualizarControlesFrmModo()

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos del Doc. Ajuste de Inventario x Doc. Toma Inventario.")
        End Try


    End Sub

    Private Sub cargarDocumentoAjusteInventarioCab(ByVal objDocumento As Entidades.Documento)

        Me.cboEmpresa.SelectedValue = CStr(objDocumento.IdEmpresa)
        Me.cboTienda.SelectedValue = CStr(objDocumento.IdTienda)

        Dim objCombo As New Combo
        objCombo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, objDocumento.IdEmpresa, objDocumento.IdTienda, objDocumento.IdTipoDocumento)
        Me.cboSerie.SelectedValue = CStr(objDocumento.IdSerie)

        Me.txtCodigoDocumento.Text = objDocumento.Codigo
        Me.cboTipoDocumento.SelectedValue = CStr(objDocumento.IdTipoDocumento)
        Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)
        Me.txtFechaEmision.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")

        Me.cboEstado.SelectedValue = CStr(objDocumento.IdEstadoDoc)

        Me.hddIdDocumento.Value = CStr(objDocumento.Id)


    End Sub

    Protected Sub btnRecalcularSaldos_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRecalcularSaldos.Click
        recalcularSaldosSistema(CDate(Me.lblFechaTomaInv.Text), CInt(Me.hddIdDocTomaInv.Value))
    End Sub



    Private Sub recalcularSaldosSistema(ByVal fechaTomaInv As Date, ByVal IdDocumento As Integer)

        Try

            If ((New Negocio.TomaInventario).RecalcularSaldosDocTomaInv(IdDocumento, fechaTomaInv)) Then

                '********** Blanqueamos el Grilla Detalle
                Me.GV_Detalle.DataSource = Nothing
                Me.GV_Detalle.DataBind()

                Me.txtCostoFaltante.Text = ""
                Me.txtCostoSobrante.Text = ""

                objScript.mostrarMsjAlerta(Me, "La Operación se realizó con éxito.")
            Else
                Throw New Exception
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la operación.")
        End Try



    End Sub

    Protected Sub btnEliminarAjuste_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim IdProducto As Integer = Nothing

            For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

                If (CType(sender, ImageButton).ClientID = CType(Me.GV_Detalle.Rows(i).FindControl("btnEliminarAjuste"), ImageButton).ClientID) Then

                    IdProducto = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("lblIdProducto"), Label).Text)
                    Exit For

                End If

            Next

            If (IdProducto = Nothing) Then
                Throw New Exception("No se halló el registro.")
            Else

                If ((New Negocio.TomaInventario).EliminarProcesoxIdProductoxIdDocumentoAjusteInv(CInt(Me.hddIdDocumento.Value), IdProducto)) Then


                    Me.GV_Detalle.DataSource = Nothing
                    Me.GV_Detalle.DataBind()

                    Me.txtCostoFaltante.Text = ""
                    Me.txtCostoSobrante.Text = ""

                    objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")
                Else
                    Throw New Exception("Problemas en la Operación")
                End If


            End If

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try


    End Sub

    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        hddFrmModo.Value = "3"
        actualizarControlesFrmModo()


    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        hddFrmModo.Value = "0"
        actualizarControlesFrmModo()
    End Sub

    Private Sub limpiarFrm()

        Try

            GenerarCodigoDocumento()
            Me.cboEstado.SelectedIndex = 0
            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            Me.lblFechaEmisionDocTomaInv.Text = ""
            Me.lblFechaTomaInv.Text = ""
            Me.lblNomAlmacen.Text = ""
            Me.lblNomEmpleado.Text = ""
            Me.lblNomEmpresaTomaInv.Text = ""
            Me.lblNomEstadoDocTomaInv.Text = ""
            Me.lblNroDocTomaInventario.Text = ""

            Me.cboLinea.DataSource = Nothing
            Me.cboLinea.DataBind()
            Me.cboSubLinea.DataSource = Nothing
            Me.cboSubLinea.DataBind()

            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

            Me.txtCostoFaltante.Text = ""
            Me.txtCostoSobrante.Text = ""

            Me.hddIdAlmacen.Value = ""
            Me.hddIdDocTomaInv.Value = ""
            Me.hddIdDocumento.Value = ""

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub btnBuscarDocumento_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarDocumento.Click

        buscarDocumento(CInt(Me.cboSerie.SelectedValue), CInt(Me.txtCodigoDocumento.Text))

    End Sub

    Private Sub buscarDocumento(ByVal IdSerie As Integer, ByVal codigo As Integer)
        Try

            Dim objDocumentoAjusteCab As Entidades.Documento = (New Negocio.TomaInventario).SelectDocAjusteInventarioxIdSeriexCodigo(IdSerie, codigo)


            If (objDocumentoAjusteCab Is Nothing) Then
                Throw New Exception("El Documento no existe.")
            Else

                cargarDocumentoAjusteInventarioCab(objDocumentoAjusteCab)

                If (objDocumentoAjusteCab.IdDocRelacionado <> 0) Then
                    cargarDocumentoTomaInventario(objDocumentoAjusteCab.IdDocRelacionado)
                End If

                Me.hddFrmModo.Value = "4"
                actualizarControlesFrmModo()

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub




    Protected Sub btnAnular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAnular.Click


        anularDocumentoAjusteInv(CInt(Me.hddIdDocumento.Value))

    End Sub

    Private Sub anularDocumentoAjusteInv(ByVal IdDocumento As Integer)

        Try


            If ((New Negocio.TomaInventario).DocumentoAjusteInventarioAnularxIdDocumento(IdDocumento)) Then

                hddFrmModo.Value = "6"
                actualizarControlesFrmModo()

                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            Else
                Throw New Exception("Problemas en la Operación.")
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Protected Sub btnBuscar_TI_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar_TI.Click

        buscarDocumentoTomaInventario(CInt(Me.cboAlmacen_TI.SelectedValue))

    End Sub
    Private Sub buscarDocumentoTomaInventario(ByVal IdAlmacen As Integer)
        Try
            Me.GV_DocumentoTI.DataSource = (New Negocio.TomaInventario).DocumentoTomaInventarioSelectxIdAlmacen(IdAlmacen)
            Me.GV_DocumentoTI.DataBind()

            objScript.onCapa(Me, "capaDocTomaInventario")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_DocumentoTI_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_DocumentoTI.PageIndexChanging
        Me.GV_DocumentoTI.PageIndex = e.NewPageIndex
        buscarDocumentoTomaInventario(CInt(Me.cboAlmacen_TI.SelectedValue))
    End Sub

    Private Sub GV_DocumentoTI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoTI.SelectedIndexChanged


        cargarDocumentoTomaInv_Nuevo(CInt(CType(Me.GV_DocumentoTI.SelectedRow.FindControl("hddIdDocumentoTomaInventario"), HiddenField).Value))

    End Sub

    Private Sub cargarDocumentoTomaInv_Nuevo(ByVal IdDocumentoTomaInv As Integer)

        cargarDocumentoTomaInventario(IdDocumentoTomaInv)
        cargarDocumentoAjusteInventarioRelacionado(IdDocumentoTomaInv)


    End Sub

    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditar.Click


        hddFrmModo.Value = "2"
        actualizarControlesFrmModo()

    End Sub
End Class