﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmOrdenPedido_Sucursal_V2
    Inherits System.Web.UI.Page


    Private objScript As New ScriptManagerClass
    Private listaDetalleDocumento As List(Of Entidades.DetalleDocumento)
    Private listaDocumentoRef As List(Of Entidades.Documento)
    Private listaProductoView As List(Of Entidades.ProductoView)

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor
    Dim valorx As Integer = 0
    Private Const _IdMagnitud_Peso As Integer = 1

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
    End Enum
#Region "************************ MANEJO DE SESSION"
    Private Function getListaDetalleDocumento() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item(ViewState("listaDetalleDocumento").ToString), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleDocumento(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove(ViewState("listaDetalleDocumento").ToString)
        Session.Add(ViewState("listaDetalleDocumento").ToString, lista)
    End Sub
    Private Function getlistaDocumentoRef() As List(Of Entidades.Documento)
        Return CType(Session.Item(ViewState("listaDocumentoRef").ToString), List(Of Entidades.Documento))
    End Function
    Private Sub setlistaDocumentoRef(ByVal lista As List(Of Entidades.Documento))
        Session.Remove(ViewState("listaDocumentoRef").ToString)
        Session.Add(ViewState("listaDocumentoRef").ToString, lista)
    End Sub


    Private Sub onLoad_Session()

        Dim Aleatorio As New Random()
        ViewState.Add("listaDetalleDocumento", "listaDetalleDocumentoOP" + Aleatorio.Next(0, 100000).ToString)
        ViewState.Add("listaDocumentoRef", "listaDocumentoRefOP" + Aleatorio.Next(0, 100000).ToString)

    End Sub


#End Region
    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then

            onLoad_Session()
            ConfigurarDatos()
            inicializarFrm()
            ValidarPermisos()

        End If

        Me.GV_ComponenteKit.DataBind()
    End Sub
    Private Sub ValidarPermisos()


        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {198, 199, 200, 201, 202, 203, 205})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGuardar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscar.Enabled = True
            Me.btnBuscarDocumentoxCodigo.Enabled = True

        Else
            Me.btnBuscar.Enabled = False
            Me.btnBuscarDocumentoxCodigo.Enabled = False

        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFechaEmision.Enabled = True
        Else
            Me.txtFechaEmision.Enabled = False
        End If

        If listaPermisos(5) > 0 Then  '********* COMPROMETER STOCK AUTOAPROBACION
            Me.chb_ComprometerStock.Enabled = True
            Me.chb_ComprometerStock.Checked = True
        Else
            Me.chb_ComprometerStock.Enabled = False
            Me.chb_ComprometerStock.Checked = False
        End If

        If listaPermisos(6) > 0 Then
            Me.btnBuscarProducto.Enabled = True
        Else
            Me.btnBuscarProducto.Enabled = False
        End If


    End Sub
    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo

            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

                .LLenarCboEstadoDocumento(Me.cboEstado)

                .llenarCboTipoOperacionxIdTpoDocumento(Me.cboTipoOperacion, CInt(Me.hddIdTipoDocumento.Value), True)
                .LLenarCboTipoDocumentoRefxIdTipoDocumento(CboTipoDocumento, CInt(Me.hddIdTipoDocumento.Value), 2)
                .LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)

                ''************** PUNTO DE LLEGADA
                '.LLenarCboDepartamento(Me.cboDepto_Llegada)
                '.LLenarCboProvincia(Me.cboProvincia_Llegada, (Me.cboDepto_Llegada.SelectedValue))
                '.LLenarCboDistrito(Me.cboDistrito_Llegada, (Me.cboDepto_Llegada.SelectedValue), (Me.cboProvincia_Llegada.SelectedValue))

                ''*************** PUNTO DE PARTIDA (SOLO DEPTO)
                '.LLenarCboDepartamento(Me.cboDepto_Partida)
                '--lineas---

                .llenarCboTipoExistencia(cboTipoExistencia, False)
                .llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cboTipoExistencia.SelectedValue), True)
                .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)
                .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

                ' .LlenarCboLinea(Me.cmbLinea_AddProd, True)
                '.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
                '.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

                .LlenarCboTipoAlmacen(Me.cboTipoAlmacen_Partida, False)
                .LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen_Partida, CInt(Me.cboTipoAlmacen_Partida.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), True)

                .llenarCboAlmacenxIdTienda(Me.cboAlmacen_Llegada, CInt(Me.cboTienda.SelectedValue), False)


                '*************** PESO
                .llenarCboUnidadMedidaxIdMagnitud(Me.cboUnidadMedida_PesoTotal, _IdMagnitud_Peso, False)
            End With

            Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

            Me.txtFechaI.Text = Me.txtFechaEmision.Text
            Me.txtFechaF.Text = Me.txtFechaEmision.Text
            Me.txtFechaInicio_DocRef.Text = Me.txtFechaEmision.Text
            Me.txtFechaFin_DocRef.Text = Me.txtFechaEmision.Text

            actualizarOpcionesBusquedaDocRef(1, False)



            verFrm(FrmModo.Nuevo, False, True, True, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean, ByVal generarCodDocumento As Boolean)

        If (limpiar) Then
            LimpiarFrm()
        End If

        If (removeSession) Then
            Session.Remove(ViewState("listaDetalleDocumento").ToString)
            Session.Remove(ViewState("listaDocumentoRef").ToString)
        End If

        If (initSession) Then
            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.listaDocumentoRef = New List(Of Entidades.Documento)
            setlistaDocumentoRef(Me.listaDocumentoRef)
        End If

        If (initParametrosGral) Then

            ''******* FECHA DE VCTO
            'Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {1, 2})

        End If

        If (generarCodDocumento) Then
            GenerarCodigoDocumento()
        End If

        Me.hddFrmModo.Value = CStr(FrmModo)
        ActualizarBotonesControl()

    End Sub
    Private Sub ActualizarBotonesControl()

        Select Case CInt(hddFrmModo.Value)

            Case FrmModo.Inicio '************* INICIO
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False
                Me.Panel_PuntoPartida.Enabled = False

                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"

            Case FrmModo.Nuevo '************** Nuevo
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.btnBuscarDocumentoRef.Visible = True


                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True
                Me.Panel_PuntoPartida.Enabled = True
                Me.Panel_Detalle.Enabled = True

                Me.Panel_Obs.Enabled = True


                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True

                Me.cboEstado.SelectedValue = "1"


                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.btnLimpiarDetalleDocumento.Enabled = True

            Case FrmModo.Editar '*********************** Editar
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = True
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False

                Me.btnBuscarDocumentoRef.Visible = True

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = True

                Me.Panel_PuntoPartida.Enabled = True
                Me.Panel_Detalle.Enabled = True
                Me.Panel_Obs.Enabled = True

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False

            Case FrmModo.Buscar_Doc '**************************** Buscar documento
                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = False
                Me.txtCodigoDocumento.Focus()
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = False


                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = True
                Me.Panel_DocumentoRef.Enabled = False

                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Detalle.Enabled = False
                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = True
                Me.cboTienda.Enabled = True
                Me.cboSerie.Enabled = True


            Case FrmModo.Documento_Buscar_Exito '********* documento hallado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = True
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = True
                Me.btnImprimir.Visible = True

                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False

                Me.Panel_PuntoPartida.Enabled = False
                Me.Panel_Detalle.Enabled = False

                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


            Case FrmModo.Documento_Save_Exito '********* documento guardado correctamente

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False

                Me.btnBuscarDocumentoRef.Visible = False

                Me.btnImprimir.Visible = True

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False

                Me.Panel_PuntoPartida.Enabled = False


                Me.Panel_Detalle.Enabled = False

                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False


            Case FrmModo.Documento_Anular_Exito  '*********** Anulacion exitosa

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = False
                Me.btnBuscarDocumentoxCodigo.Visible = True
                Me.txtCodigoDocumento.ReadOnly = True
                Me.btnEditar.Visible = False
                Me.btnGuardar.Visible = False
                Me.btnAnular.Visible = False
                Me.btnImprimir.Visible = True
                Me.btnBuscarDocumentoRef.Visible = False

                Me.Panel_Cab.Enabled = False
                Me.Panel_DocumentoRef.Enabled = False

                Me.Panel_PuntoPartida.Enabled = False


                Me.Panel_Detalle.Enabled = False

                Me.Panel_Obs.Enabled = False

                Me.cboEmpresa.Enabled = False
                Me.cboTienda.Enabled = False
                Me.cboSerie.Enabled = False
                Me.cboEstado.SelectedValue = "2"

        End Select
    End Sub
    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.cboSerie.SelectedValue)) Then
            Me.txtCodigoDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.cboSerie.SelectedValue))
        Else
            Me.txtCodigoDocumento.Text = ""
        End If

    End Sub
    Private Sub LimpiarFrm()

        ''************* CABECERA
        Me.txtCodigoDocumento.Text = ""
        Me.txtFechaEmision.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.cboEstado.SelectedValue = "1"  '******* Activo por defecto
        Me.cboAlmacen_Llegada.SelectedIndex = 0
        Me.cboAlmacen_Partida.SelectedIndex = 0
        Me.cboTipoOperacion.SelectedIndex = 0
        Dim objCbo As New Combo
        objCbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)

        '************** DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        Me.ckProductoComprometidos.Text = ""
        Me.ckProductoComprometidos.Checked = False

        '*********** DETALLES
        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()
        Me.GV_DocumentoRef_Detalle.DataBind()

        '*********** OBSERVACION
        Me.txtObservaciones.Text = ""


        '************* HIDDEN
        Me.hddCodigoDocumento.Value = ""
        Me.hddIdDocumento.Value = ""

        '************* OTROS
        Me.DGV_AddProd.DataSource = Nothing
        Me.DGV_AddProd.DataBind()
        Me.GV_BusquedaAvanzado.DataSource = Nothing
        Me.GV_BusquedaAvanzado.DataBind()
        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()
    End Sub
    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

            objCbo.LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen_Partida, CInt(Me.cboTipoAlmacen_Partida.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen_Llegada, CInt(Me.cboTienda.SelectedValue), False)

            GenerarCodigoDocumento()

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.cboSerie, CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))

            objCbo.llenarCboAlmacenxIdTienda(Me.cboAlmacen_Llegada, CInt(Me.cboTienda.SelectedValue), False)

            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        Try
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTipoOperacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoOperacion.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub addProducto_DetalleDocumento()
        Try

            '************* ACTUALIZAMOS LA GRILLA
            ActualizarListaDetalleDocumento()

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            ' Lista de UnidaMedida
            Dim listaUnidadMedida_Peso As List(Of Entidades.MagnitudUnidad) = (New Negocio.MagnitudUnidad)._MagnitudUnidadselectxIdMagnitud(_IdMagnitud_Peso)

            For i As Integer = 0 To Me.DGV_AddProd.Rows.Count - 1

                '***************** UNIDAD MEDIDA PRINCIPAL
                Dim IdUnidadMedida_ST As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdUnidadMedida_PR"), HiddenField).Value)

                Dim cantidad As Decimal = CDec(CType(Me.DGV_AddProd.Rows(i).FindControl("txtCantidad_AddProd"), TextBox).Text)

                If (cantidad > 0) Then

                    Dim IdProducto_Find As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("hddIdProducto_Find"), HiddenField).Value)
                    Dim IdUnidadMedida_Find As Integer = CInt(CType(Me.DGV_AddProd.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
                    Dim Kit_Find As Boolean = False : Try : Kit_Find = CBool(CType(Me.DGV_AddProd.Rows(i).FindControl("hddKit"), HiddenField).Value) : Catch ex As Exception : End Try


                    Dim valCant As Boolean = False
                    For k As Integer = 0 To listaDetalleDocumento.Count - 1

                        If listaDetalleDocumento(k).IdProducto = IdProducto_Find And listaDetalleDocumento(k).IdUnidadMedida = IdUnidadMedida_Find Then

                            listaDetalleDocumento(k).Cantidad = listaDetalleDocumento(k).Cantidad + cantidad
                            listaDetalleDocumento(k).Kit = Kit_Find

                            If listaDetalleDocumento(k).IdUnidadMedidaPrincipal <= 0 Then
                                listaDetalleDocumento(k).IdUnidadMedidaPrincipal = (New Negocio.ProductoUM).SelectIdUMPrincipalxIdProducto(listaDetalleDocumento(k).IdProducto)
                            End If
                            listaDetalleDocumento(k).IdUnidadMedida_Peso = CInt(Me.cboUnidadMedida_PesoTotal.SelectedValue)

                            listaDetalleDocumento(k).ListaUnidadMedida_Peso = listaUnidadMedida_Peso

                            '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                            listaDetalleDocumento(k).Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(listaDetalleDocumento(k).IdProducto, IdUnidadMedida_ST, 1, _IdMagnitud_Peso, listaDetalleDocumento(k).IdUnidadMedida_Peso)

                            '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                            listaDetalleDocumento(k).Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(listaDetalleDocumento(k).IdProducto, listaDetalleDocumento(k).IdUnidadMedida, IdUnidadMedida_ST, 1)
                            listaDetalleDocumento(k).Peso = listaDetalleDocumento(k).Cantidad * listaDetalleDocumento(k).Equivalencia1 * listaDetalleDocumento(k).Equivalencia


                            valCant = True
                            Exit For

                        End If

                    Next

                    If valCant = False Then

                        Dim objDetalle As New Entidades.DetalleDocumento
                        With objDetalle

                            .IdProducto = IdProducto_Find
                            .NomProducto = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(2).Text))
                            .Cantidad = cantidad
                            .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(.IdProducto)
                            .IdUnidadMedida = IdUnidadMedida_Find

                            .CodigoProducto = HttpUtility.HtmlDecode(CStr(Me.DGV_AddProd.Rows(i).Cells(1).Text))
                            .Kit = Kit_Find


                            If .IdUnidadMedidaPrincipal <= 0 Then
                                .IdUnidadMedidaPrincipal = (New Negocio.ProductoUM).SelectIdUMPrincipalxIdProducto(.IdProducto)
                            End If

                            .ListaUnidadMedida_Peso = listaUnidadMedida_Peso
                            .IdUnidadMedida_Peso = CInt(Me.cboUnidadMedida_PesoTotal.SelectedValue)

                            '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                            .Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(.IdProducto, IdUnidadMedida_ST, 1, _IdMagnitud_Peso, .IdUnidadMedida_Peso)

                            '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                            .Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUnidadMedida, IdUnidadMedida_ST, 1)

                            .Peso = .Cantidad * .Equivalencia1 * .Equivalencia


                        End With
                        Me.listaDetalleDocumento.Add(objDetalle)

                    End If

                    Kit_Find = False
                    If Kit_Find Then ' AGREGANDO COMPONENTES

                        Dim ListaKit As List(Of Entidades.Kit) = (New Negocio.Kit).SelectComponentexIdKit(IdProducto_Find)

                        For k As Integer = 0 To ListaKit.Count - 1


                            Dim valCant2 As Boolean = False
                            For z As Integer = 0 To listaDetalleDocumento.Count - 1
                                If listaDetalleDocumento(z).IdProducto = ListaKit(k).IdComponente And listaDetalleDocumento(z).IdUnidadMedida = ListaKit(k).IdUnidadMedida_Comp Then
                                    listaDetalleDocumento(z).Cantidad = listaDetalleDocumento(z).Cantidad + cantidad

                                    listaDetalleDocumento(z).ListaUnidadMedida_Peso = listaUnidadMedida_Peso

                                    If listaDetalleDocumento(z).IdUnidadMedidaPrincipal <= 0 Then
                                        listaDetalleDocumento(z).IdUnidadMedidaPrincipal = (New Negocio.ProductoUM).SelectIdUMPrincipalxIdProducto(listaDetalleDocumento(z).IdProducto)
                                    End If
                                    listaDetalleDocumento(z).IdUnidadMedida_Peso = CInt(Me.cboUnidadMedida_PesoTotal.SelectedValue)

                                    '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                                    listaDetalleDocumento(z).Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(listaDetalleDocumento(z).IdProducto, IdUnidadMedida_ST, 1, _IdMagnitud_Peso, listaDetalleDocumento(z).IdUnidadMedida_Peso)

                                    '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                                    listaDetalleDocumento(z).Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(listaDetalleDocumento(z).IdProducto, listaDetalleDocumento(z).IdUnidadMedida, IdUnidadMedida_ST, 1)
                                    listaDetalleDocumento(z).Peso = listaDetalleDocumento(z).Cantidad * listaDetalleDocumento(z).Equivalencia1 * listaDetalleDocumento(z).Equivalencia

                                    valCant2 = True
                                    Exit For
                                End If
                            Next
                            If valCant2 = False Then

                                Dim objDetalle2 As New Entidades.DetalleDocumento
                                With objDetalle2

                                    .CodigoProducto = ListaKit(k).CodigoProd_Comp
                                    .IdProducto = ListaKit(k).IdComponente
                                    .NomProducto = ListaKit(k).Componente
                                    .Cantidad = ListaKit(k).Cantidad_Comp * cantidad
                                    .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(.IdProducto)
                                    .IdUnidadMedida = ListaKit(k).IdUnidadMedida_Comp

                                End With

                                Me.listaDetalleDocumento.Add(objDetalle2)
                            End If
                        Next


                    End If ' KIT


                End If 'CANTIDAD


            Next

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", " calcularPesoTotal(null); onCapa('capaBuscarProducto_AddProd');    ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub ActualizarListaDetalleDocumento(Optional ByVal Index As Integer = -1)

        Me.listaDetalleDocumento = getListaDetalleDocumento()

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            If Index = i Then
                Me.listaDetalleDocumento(i).IdUMold = Me.listaDetalleDocumento(i).IdUnidadMedida
            End If


            Me.listaDetalleDocumento(i).Cantidad = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)
            Me.listaDetalleDocumento(i).IdUnidadMedida = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
            Me.listaDetalleDocumento(i).UMedida = CStr(CType(Me.GV_Detalle.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedItem.ToString)

        Next
        setListaDetalleDocumento(Me.listaDetalleDocumento)
    End Sub
    Private Sub GV_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_Detalle.RowDataBound
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
                cboUM.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUM
                cboUM.DataBind()
                cboUM.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida.ToString



                Dim cboUM_Peso As DropDownList = CType(e.Row.FindControl("cboUnidadMedida_Peso"), DropDownList)
                cboUM_Peso.DataSource = Me.listaDetalleDocumento(e.Row.RowIndex).ListaUnidadMedida_Peso
                cboUM_Peso.DataBind()

                If (cboUM_Peso.Items.FindByValue(CStr(Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida_Peso)) IsNot Nothing) Then
                    cboUM_Peso.SelectedValue = Me.listaDetalleDocumento(e.Row.RowIndex).IdUnidadMedida_Peso.ToString
                End If


                If (Me.listaDetalleDocumento(e.Row.RowIndex).Kit) Then  '********* KIT

                    CType(e.Row.FindControl("btnMostrarComponenteKit"), ImageButton).Visible = True

                Else

                    CType(e.Row.FindControl("btnMostrarComponenteKit"), ImageButton).Visible = False

                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnLimpiarDetalleDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarDetalleDocumento.Click
        limpiarDetalleDocumento()
    End Sub
    Private Sub limpiarDetalleDocumento()
        Try

            Me.listaDetalleDocumento = New List(Of Entidades.DetalleDocumento)

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Nothing
            Me.GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub DGV_AddProd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGV_AddProd.RowDataBound

        Try

            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim cboUM As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
                cboUM.DataSource = Me.listaProductoView(e.Row.RowIndex).ListaUM
                cboUM.DataBind()
                cboUM.SelectedValue = Me.listaProductoView(e.Row.RowIndex).IdUnidadMedida.ToString

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
#Region "************************** BUSQUEDA PRODUCTO"

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", Me.cboAlmacen_Partida.SelectedValue)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia", Me.cboTipoExistencia.SelectedValue)
            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, _
                                          ByVal codigoSubLinea As String, ByVal nomProducto As String, _
                                          ByVal IdAlmacen As Integer, ByVal tipoMov As Integer, _
                                          ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        'Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)


        If listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen_Partida.SelectedValue), 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen_Partida.SelectedValue), 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(Me.cboAlmacen_Partida.SelectedValue), 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub mostrarCapaStockPrecioxProducto(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim gvRow As GridViewRow = CType(CType(sender, ImageButton).NamingContainer, GridViewRow)
            Me.lblProductoConsultarStockPrecioxProd.Text = HttpUtility.HtmlDecode(gvRow.Cells(2).Text)
            Dim IdProducto As Integer = CInt(CType(gvRow.FindControl("hddIdProducto_Find"), HiddenField).Value)

            Me.GV_ConsultarStockPrecioxProd.DataSource = (New Negocio.Catalogo).Producto_Consultar_Stock_Precio_Venta(IdProducto, 0, 0, 0, (New Negocio.FechaActual).SelectFechaActual, CInt(Me.cboEmpresa.SelectedValue))
            Me.GV_ConsultarStockPrecioxProd.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa2('capaConsultarStockPrecioxProducto');   onCapa('capaBuscarProducto_AddProd');   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region
#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function


#End Region
    Private Sub cboTipoAlmacen_Partida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoAlmacen_Partida.SelectedIndexChanged

        Try

            Dim objCbo As New Combo
            With objCbo
                .LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen_Partida, CInt(Me.cboTipoAlmacen_Partida.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), True)
            End With


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
#Region "*************************** BUSQUEDA DOCUMENTO DE REFERENCIA"

    Protected Sub btnAceptarBuscarDocRef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRef.Click

        mostrarDocumentosRef_Find()

    End Sub
    Private Sub mostrarDocumentosRef_Find()
        Try

            Dim serie As Integer = 0
            Dim codigo As String = ""
            Dim fechaInicio As Date = Nothing
            Dim fechafin As Date = Nothing

            Select Case CInt(Me.rdbBuscarDocRef.SelectedValue)
                Case 0  '************ POR FECHA                    
                    fechaInicio = CDate(Me.txtFechaInicio_DocRef.Text)
                    fechafin = CDate(Me.txtFechaFin_DocRef.Text)
                Case 1  '*********** POR NRO DOCUMENTO
                    serie = CInt(Me.txtSerie_BuscarDocRef.Text)
                    codigo = CStr(Me.txtCodigo_BuscarDocRef.Text)
            End Select

            'Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).Documento_BuscarDocumentoRef(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), 0, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, CInt(Me.CboTipoDocumento.SelectedValue), fechaInicio, fechafin, False)

            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoOrdenPedidoSucursal).Documento_BuscarDocumentoRef2(1, CInt(Me.cboTienda.SelectedValue), 0, CInt(Me.hddIdTipoDocumento.Value), serie, codigo, fechaInicio, fechafin, CInt(Me.CboTipoDocumento.SelectedValue), cboTipoOperacion.SelectedValue)


            '*****************Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.Documento).DocumentoSelectDocxDespachar_V2(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), fechaInicio, fechafin, serie, codigo, IdPersona, CInt(Me.hddIdTipoDocumento.Value))

            If (lista.Count > 0) Then
                Me.GV_DocumentosReferencia_Find.DataSource = lista
                Me.GV_DocumentosReferencia_Find.DataBind()
            Else
                Throw New Exception("No se hallaron registros.")
            End If

            objScript.onCapa(Me, "capaDocumentosReferencia")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnAceptarBuscarDocRefxCodigo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAceptarBuscarDocRefxCodigo.Click
        mostrarDocumentosRef_Find()
    End Sub
    Protected Sub rdbBuscarDocRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdbBuscarDocRef.SelectedIndexChanged
        actualizarOpcionesBusquedaDocRef(CInt(Me.rdbBuscarDocRef.SelectedValue))
    End Sub
    Private Sub GV_DocumentosReferencia_Find_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentosReferencia_Find.SelectedIndexChanged

        cargarDocumentoReferencia(CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdDocumentoReferencia_Find"), HiddenField).Value) _
     , CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdEmpresa_Find"), HiddenField).Value) _
     , CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdAlmacen_Find"), HiddenField).Value) _
     , CInt(CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddIdPersona_Find"), HiddenField).Value) _
, (CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddDocReferencia_Find"), HiddenField).Value) _
, (CType(Me.GV_DocumentosReferencia_Find.SelectedRow.FindControl("hddDocTipoOperacion_Find"), HiddenField).Value))

    End Sub
    Private Sub cargarDocumentoReferencia(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdPersona As Integer, ByVal DocReferencia As String, ByVal TipoOperacion As String)

        Try

            Me.GV_DocumentoRef_Detalle.DataBind()

            ActualizarListaDetalleDocumento()
            If (DocReferencia = "" And TipoOperacion <> "PEDIDO DE REPOSICION POR STOCK") Then Throw New Exception("No se permite la Operación Debe Tener un Documento de Cancelacion..")


            ''******************** INICIALIZAMOS EL FRM CUANDO NO EXISTE UN DOC. DE REFERENCIA
            'If (Me.GV_DocumentoRef.Rows.Count <= 0) Then
            '    verFrm(FrmModo.Nuevo, True, True, True, True, False)
            'Else
            '    If (Not validar_AddDocumentoRef(IdDocumentoRef, IdEmpresa, IdAlmacen, IdPersona)) Then Throw New Exception("No se permite la Operación.")
            'End If

            If (Not validar_AddDocumentoRef(IdDocumentoRef, IdEmpresa, IdAlmacen, IdPersona)) Then Throw New Exception("No se permite la Operación ")

            '***************** OBTENEMOS EL DOCUMENTO DE REFERENCIA
            'Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)

            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumentoDocReferencia(IdDocumentoRef)

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDetalleDocumento.AddRange(obtenerListaDetalleDocumento_Ref(objDocumento.Id))

            '*************** GUARDAMOS EN SESSION
            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDocumentoRef.Add(objDocumento)
            setlistaDocumentoRef(Me.listaDocumentoRef)

            '************** EL PUNTO DE PARTIDA NO SE TOMA XQ PRODUCE PROBLEMAS
            cargarDocumentoRef_GUI(objDocumento, Me.listaDocumentoRef, Me.listaDetalleDocumento, Nothing, Nothing, Nothing, False)

            Me.GV_DocumentosReferencia_Find.DataSource = Nothing
            Me.GV_DocumentosReferencia_Find.DataBind()

            Dim cbo As New Combo
            If (New Negocio.Util).valDocComprometerStock(IdDocumentoRef) <= 0 Then

                If (Me.cboTipoOperacion.Items.FindByValue("1") IsNot Nothing) Then
                    Me.cboTipoOperacion.SelectedValue = "1"

                    cbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)

                    If (Me.cboMotivoTraslado.Items.FindByValue(CStr(objDocumento.IdMotivoT)) IsNot Nothing) Then
                        Me.cboMotivoTraslado.SelectedValue = CStr(objDocumento.IdMotivoT)
                    End If

                End If
            Else
                If (Me.cboTipoOperacion.Items.FindByValue("18") IsNot Nothing) Then
                    Me.cboTipoOperacion.SelectedValue = "18"

                    cbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)

                    If (Me.cboMotivoTraslado.Items.FindByValue(CStr(objDocumento.IdMotivoT)) IsNot Nothing) Then
                        Me.cboMotivoTraslado.SelectedValue = CStr(objDocumento.IdMotivoT)
                    End If

                End If

            End If


            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " calcularPesoTotal(null); ", True)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub
    Private Function validar_AddDocumentoRef(ByVal IdDocumentoRef As Integer, ByVal IdEmpresa As Integer, ByVal IdAlmacen As Integer, ByVal IdPersona As Integer) As Boolean

        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1

            If (Me.listaDocumentoRef(i).Id = IdDocumentoRef) Then
                Throw New Exception("EL DOCUMENTO SELECCIONADO YA HA SIDO INGRESADO. NO SE PERMITE LA OPERACIÓN.")
            End If

      

        Next

        If Me.listaDocumentoRef IsNot Nothing Then
            If Me.listaDocumentoRef.Count > 0 Then

                If (((New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {265})(0)) > 0) Then
                    verFrm(FrmModo.Nuevo, True, True, True, True, True)
                End If

            End If
        End If

        If (((New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {208})(0)) > 0) Then

            Dim objDOP As New Negocio.DocumentoOrdenPedidoSucursal
            objDOP.DocumentoOrdenPedidoSucursal_ValDocumentoRef(IdDocumentoRef)

        End If

        Return True

    End Function
    Private Function obtenerListaDetalleDocumento_Ref(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        '**************** Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).DetalleDocumentoSelectDetallexAtenderxIdDocumento(IdDocumento)
        'Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DocumentoOrdenPedidoSucursal).DetalleDocumento_Ref(IdDocumento)


        '************* QUITAMOS EL DETALLE DE LOS COMPONENTES
        For i As Integer = (listaDetalle.Count - 1) To 0 Step -1
            If (listaDetalle(i).ComponenteKit) Or listaDetalle(i).Cantidad <= 0 Then
                listaDetalle.RemoveAt(i)
            End If
        Next


        Dim listaRetorno As New List(Of Entidades.DetalleDocumento)


        Dim listaUnidadMedida_Peso As List(Of Entidades.MagnitudUnidad) = (New Negocio.MagnitudUnidad)._MagnitudUnidadselectxIdMagnitud(_IdMagnitud_Peso)

        For i As Integer = 0 To listaDetalle.Count - 1

            Dim obj As New Entidades.DetalleDocumento
            With obj

                .IdDocumentoRef = listaDetalle(i).IdDocumento
                .ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)
                .Cantidad = listaDetalle(i).Cantidad
                .IdDetalleAfecto = listaDetalle(i).IdDetalleDocumento
                '.CantidadxAtenderText = CStr(Math.Round(listaDetalle(i).Cantidad, 4)) + " " + listaDetalle(i).UMedida
                .CantidadxAtenderText = (Math.Round(listaDetalle(i).Cantidad, 4))
                .NomProducto = listaDetalle(i).NomProducto
                .IdUnidadMedida = listaDetalle(i).IdUnidadMedida
                .UMedida = listaDetalle(i).UMedida
                .CodigoProducto = listaDetalle(i).CodigoProducto
                .IdProducto = listaDetalle(i).IdProducto
                .CantADespacharOriginal = listaDetalle(i).Cantidad
                .Kit = listaDetalle(i).Kit

                .ListaUnidadMedida_Peso = listaUnidadMedida_Peso

                If .IdUnidadMedidaPrincipal <= 0 Then
                    .IdUnidadMedidaPrincipal = (New Negocio.ProductoUM).SelectIdUMPrincipalxIdProducto(.IdProducto)
                End If

                .IdUnidadMedida_Peso = CInt(Me.cboUnidadMedida_PesoTotal.SelectedValue)

                '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                .Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(.IdProducto, .IdUnidadMedidaPrincipal, 1, _IdMagnitud_Peso, .IdUnidadMedida_Peso)

                '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                .Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUnidadMedida, .IdUnidadMedidaPrincipal, 1)
                .Peso = .Cantidad * .Equivalencia1 * .Equivalencia

                If CInt(Me.cboAlmacen_Partida.SelectedValue) > 0 Then
                    .Stock = (New Negocio.Util).fx_StockDisponible(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen_Partida.SelectedValue), listaDetalle(i).IdProducto)
                End If
                .UM1 = (New Negocio.ProductoUMView).SelectUMPrincipalxIdProducto(listaDetalle(i).IdProducto).NombreCortoUM
                .StockTotalxAlmacen = listaDetalle(i).StockTotalxAlmacen
                .IdTipoCliente = listaDetalle(i).IdTipoCliente
            End With

            listaRetorno.Add(obj)

        Next

        Return listaRetorno

    End Function
    Private Sub cargarDocumentoRef_GUI(ByVal objDocumento As Entidades.Documento, ByVal listaDocumento As List(Of Entidades.Documento), ByVal listaDetalle As List(Of Entidades.DetalleDocumento), ByVal objPuntoPartida As Entidades.PuntoPartida, ByVal objPuntoLlegada As Entidades.PuntoLlegada, ByVal objObservaciones As Entidades.Observacion, ByVal cargarDocumentoOP As Boolean)

        Dim objCbo As New Combo

        With objDocumento

            If (Me.cboEmpresa.Items.FindByValue(CStr(objDocumento.IdEmpresa)) IsNot Nothing) Then
                Me.cboEmpresa.SelectedValue = CStr(objDocumento.IdEmpresa)
            End If

            If (Me.cboTipoOperacion.Items.FindByValue(CStr(objDocumento.IdTipoOperacion)) IsNot Nothing) Then
                Me.cboTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)

                objCbo.LlenarCboMotivoTrasladoxIdTipoOperacion(Me.cboMotivoTraslado, CInt(Me.cboTipoOperacion.SelectedValue), True)

                If (Me.cboMotivoTraslado.Items.FindByValue(CStr(objDocumento.IdMotivoT)) IsNot Nothing) Then
                    Me.cboMotivoTraslado.SelectedValue = CStr(objDocumento.IdMotivoT)
                End If

            End If

            If (cargarDocumentoOP) Then

                Me.cboEstado.SelectedValue = CStr(.IdEstadoDoc)
                Me.txtCodigoDocumento.Text = .Codigo

                If (.FechaEmision <> Nothing) Then
                    Me.txtFechaEmision.Text = Format(.FechaEmision, "dd/MM/yyyy")
                End If

                Me.hddIdDocumento.Value = CStr(.Id)
                Me.hddCodigoDocumento.Value = .Codigo

            End If

        End With

        '**************** LISTA DOC REFERENCIA
        Me.GV_DocumentoRef.DataSource = listaDocumento
        Me.GV_DocumentoRef.DataBind()

        '****************** PUNTO PARTIDA
        If (objPuntoPartida IsNot Nothing) Then

            If (Me.cboTipoAlmacen_Partida.Items.FindByValue(CStr(objPuntoPartida.IdTipoAlmacen)) IsNot Nothing) Then
                Me.cboTipoAlmacen_Partida.SelectedValue = CStr(objPuntoPartida.IdTipoAlmacen)
                objCbo.LlenarCboAlmacenxIdTipoAlmacenxIdEmpresa(Me.cboAlmacen_Partida, CInt(Me.cboTipoAlmacen_Partida.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), False)
            End If

            If (Me.cboAlmacen_Partida.Items.FindByValue(CStr(objPuntoPartida.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacen_Partida.SelectedValue = CStr(objPuntoPartida.IdAlmacen)
            End If

        End If

        '***************** PUNTO DE LLEGADA
        If (objPuntoLlegada IsNot Nothing) Then


            If (Me.cboAlmacen_Llegada.Items.FindByValue(CStr(objPuntoLlegada.IdAlmacen)) IsNot Nothing) Then
                Me.cboAlmacen_Llegada.SelectedValue = CStr(objPuntoLlegada.IdAlmacen)
            End If


        End If

        Me.GV_Detalle.DataSource = listaDetalleDocumento
        Me.GV_Detalle.DataBind()


        '************** OBSERVACIONES
        If (objObservaciones IsNot Nothing) Then
            Me.txtObservaciones.Text = objObservaciones.Observacion
        End If

        If Not IsNothing(listaDocumento) Then

            If listaDocumento.Count > 0 Then

                If (New Negocio.Util).valDocComprometerStock(listaDocumento(0).Id) <= 0 Then
                    Me.ckProductoComprometidos.Text = "Productos no comprometidos Documento Ref."
                    Me.ckProductoComprometidos.Checked = False
                Else
                    Me.ckProductoComprometidos.Text = "Productos comprometidos Documento Ref."
                    Me.ckProductoComprometidos.Checked = True
                End If

            End If


        Else

            Me.ckProductoComprometidos.Text = ""
            Me.ckProductoComprometidos.Checked = False

        End If




    End Sub
    Private Sub actualizarOpcionesBusquedaDocRef(ByVal opcion As Integer, Optional ByVal onCapa As Boolean = True)

        '*******  0: por fechas
        '*******  1: por nro documento

        Select Case opcion
            Case 0
                Me.Panel_BuscarDocRefxFecha.Visible = True
                Me.Panel_BuscarDocRefxCodigo.Visible = False
            Case 1
                Me.Panel_BuscarDocRefxFecha.Visible = False
                Me.Panel_BuscarDocRefxCodigo.Visible = True
        End Select

        Me.rdbBuscarDocRef.SelectedValue = CStr(opcion)
        If (onCapa) Then
            objScript.onCapa(Me, "capaDocumentosReferencia")
        End If

    End Sub
    Private Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        quitarDocumentoRef(Me.GV_DocumentoRef.SelectedRow.RowIndex)
    End Sub
    Private Sub quitarDocumentoRef(ByVal index As Integer)

        Try

            Me.listaDocumentoRef = getlistaDocumentoRef()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            '*************** QUITAMOS EL DETALLE DEL DOCUMENTO DE REFERENCIA
            Me.listaDetalleDocumento.RemoveAll(Function(ent As Entidades.DetalleDocumento) ent.IdDocumentoRef = Me.listaDocumentoRef(index).Id)
            Me.listaDocumentoRef.RemoveAt(index)

            '********* GUARDAMOS EN SESSION
            setlistaDocumentoRef(Me.listaDocumentoRef)
            setListaDetalleDocumento(Me.listaDetalleDocumento)

            '************ GUI
            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ''******************* 
            'If (Me.GV_DocumentoRef.Rows.Count <= 0) Then
            '    '***************** ACTIVAMOS LOS COMBOS DE EMPRESA/ALMACÉN

            '    Select Case CInt(Me.hddFrmModo.Value)
            '        Case FrmModo.Editar
            '            Me.cboEmpresa.Enabled = False
            '            Me.cboTienda.Enabled = False
            '        Case Else
            '            Me.cboEmpresa.Enabled = True
            '            Me.cboTienda.Enabled = True
            '    End Select

            '    Me.cboAlmacen.Enabled = True
            '    Me.btnBuscarRemitente.Enabled = True
            '    Me.btnBuscarDestinatario.Enabled = True
            '    Me.btnBuscarProducto.Enabled = True
            '    Me.btnLimpiarDetalleDocumento.Enabled = True
            'End If

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

#End Region
    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click
        registrarDocumento()
    End Sub
    Private Sub validarFrm()

        Me.listaDetalleDocumento = getListaDetalleDocumento()
        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1
            If ((Me.listaDetalleDocumento(i).Cantidad > Me.listaDetalleDocumento(i).CantADespacharOriginal) And (Me.listaDetalleDocumento(i).IdDetalleAfecto <> Nothing)) Then
                Throw New Exception("LA CANTIDAD INGRESADA DEL PRODUCTO < " + Me.listaDetalleDocumento(i).NomProducto + " > SUPERA LA CANTIDAD LÍMITE PERMITIDA < " + Me.listaDetalleDocumento(i).CantidadxAtenderText + " >. NO SE PERMITE LA OPERACIÓN.")
            End If
        Next

        If Me.GV_DocumentoRef.Rows.Count > 0 Then
            For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1
                If (Me.listaDetalleDocumento(i).IdDetalleAfecto <= 0) Then
                    Throw New Exception("EL PRODUCTO < " + Me.listaDetalleDocumento(i).NomProducto + " > NO ESTA ASOCIADO A UN DOCUMENTO DE REFERENCIA. NO SE PERMITE LA OPERACIÓN.")
                End If
            Next
        End If

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {206})
        If (listaPermisos(0) > 0 And Me.GV_DocumentoRef.Rows.Count <= 0) Then
            Throw New Exception("DEBE INGRESAR DOCUMENTOS DE REFERENCIA. NO SE PERMITE LA OPERACIÓN.")
        End If

    End Sub
    Private Sub registrarDocumento()

        Try
            Dim Validando As Integer = 0
            Dim CantidadIngresada As Decimal = 0
            Dim StockAlmacen As Decimal = 0
            Dim TipoCliente As Integer = 0
            Dim CantidadReferencia As Decimal = 0

            ''Verificando las Tiendas que pasarán la validación.
            Dim VerificandoTienda As Integer = New Negocio.Documento().VerificandoTiendaOPVP(CInt(Me.cboTienda.SelectedValue))

            ''Obteniendo el TipoCliente
            For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1
                TipoCliente = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("hddIdTipoCliente"), HiddenField).Value)
                Exit For
            Next

            ''Si el tipo cliente es diferente a cliente final y la tienda esta en la tabla de validaciones entonces cumplimos con la condicion

            If (TipoCliente = 1 And VerificandoTienda = 1) Then

                For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1
                    CantidadIngresada = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidad"), TextBox).Text)
                    StockAlmacen = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("lblStockxAlmacen"), Label).Text)

                    CantidadReferencia = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("lblStockRef"), Label).Text)

                    '  CantidadReferencia = (Math.Round(listaDetalle(i).Cantidad, 4))

                    'StockAlmacen = CDec(CType(Me.GV_Detalle.Rows(i).RenderControl("CantidadxAtenderText"), BoundField).Text)

                    If ((CantidadReferencia - StockAlmacen) >= CantidadIngresada) Then
                        'Validando = -1
                        Validando = 1
                        Exit For
                    Else
                        Validando = 1
                    End If
                Next
                'Validando que la cantidad ingresada sea menor o igual a 5
                'Si el resultado es afirmativo entonces no considera la validacion .
                If (Validando = -1 And CantidadIngresada <= 5) Then
                    Validando = 1
                End If
                ''Si no es el cliente
            Else
                Validando = 1
            End If



            If (Validando = 1) Then
                Try
                    ActualizarListaDetalleDocumento()
                    validarFrm()
                    Dim objDocumento As Entidades.Documento = obtenerDocumentoCab()
                    Me.listaDetalleDocumento = getListaDetalleDocumento()
                    Dim objObservaciones As Entidades.Observacion = obtenerObservaciones()
                    Dim objPuntoPartida As Entidades.PuntoPartida = obtenerPuntoPartida()
                    Dim objPuntoLlegada As Entidades.PuntoLlegada = obtenerPuntoLlegada()
                    Dim listaRelacionDocumento As List(Of Entidades.RelacionDocumento) = obtenerListaRelacionDocumento()
                    Dim moverAlmacen As Boolean = False
                    Dim comprometerStock As Boolean = Me.chb_ComprometerStock.Checked
                    Dim objAnexoDocumento As Entidades.Anexo_Documento = obtenerAnexoDocumento()
                    Dim listaAnexoDetalle As List(Of Entidades.Anexo_DetalleDocumento) = obtenerListaAnexoDetalleDocumento()

                    If (comprometerStock) Then
                        moverAlmacen = True
                    Else
                        moverAlmacen = False
                    End If

                    Select Case CInt(Me.hddFrmModo.Value)
                        Case FrmModo.Nuevo
                            objDocumento.Id = (New Negocio.DocumentoOrdenPedidoSucursal).registrarDocumento(objDocumento, objAnexoDocumento, Me.listaDetalleDocumento, listaAnexoDetalle, objPuntoPartida, objPuntoLlegada, objObservaciones, listaRelacionDocumento, moverAlmacen, comprometerStock, CInt(Me.cboAlmacen_Partida.SelectedValue))
                            Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                            objScript.mostrarMsjAlerta(Me, "El Documento se registró con éxito.")
                        Case FrmModo.Editar
                            objDocumento.Id = (New Negocio.DocumentoOrdenPedidoSucursal).actualizarDocumento(objDocumento, objAnexoDocumento, Me.listaDetalleDocumento, listaAnexoDetalle, objPuntoPartida, objPuntoLlegada, objObservaciones, listaRelacionDocumento, moverAlmacen, comprometerStock, CInt(Me.cboAlmacen_Partida.SelectedValue))
                            Me.hddIdDocumento.Value = CStr(objDocumento.Id)
                            objScript.mostrarMsjAlerta(Me, "El Documento se actualizó con éxito.")
                    End Select
                    verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False)
                Catch ex As Exception
                    objScript.mostrarMsjAlerta(Me, ex.Message)
                End Try
            Else
                objScript.mostrarMsjAlerta(Me, "No procede la operación ya que usted cuenta con stock en su propio almacén.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaAnexoDetalleDocumento() As List(Of Entidades.Anexo_DetalleDocumento)
        Dim lista As New List(Of Entidades.Anexo_DetalleDocumento)
        If (Me.chb_ComprometerStock.Checked) Then
            Me.listaDetalleDocumento = getListaDetalleDocumento()
            For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1
                Dim obj As New Entidades.Anexo_DetalleDocumento
                With obj
                    .CantidadAprobada = Me.listaDetalleDocumento(i).Cantidad
                End With
                lista.Add(obj)
            Next
        End If
        Return lista
    End Function
    Private Function obtenerAnexoDocumento() As Entidades.Anexo_Documento
        Dim objAnexoDocumento As New Entidades.Anexo_Documento
        With objAnexoDocumento
            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            If (Me.chb_ComprometerStock.Checked) Then
                .Aprobar = True
            Else
                .Aprobar = False
            End If

        End With

        Return objAnexoDocumento

    End Function
    Private Function obtenerListaRelacionDocumento() As List(Of Entidades.RelacionDocumento)
        Dim lista As New List(Of Entidades.RelacionDocumento)
        Me.listaDocumentoRef = getlistaDocumentoRef()

        For i As Integer = 0 To Me.listaDocumentoRef.Count - 1
            Dim objRelacionDocumento As New Entidades.RelacionDocumento
            With objRelacionDocumento
                .IdDocumento1 = Me.listaDocumentoRef(i).Id
                Select Case CInt(Me.hddFrmModo.Value)
                    Case FrmModo.Nuevo
                        .IdDocumento2 = Nothing
                    Case FrmModo.Editar
                        .IdDocumento2 = CInt(Me.hddIdDocumento.Value)
                End Select
            End With
            lista.Add(objRelacionDocumento)
        Next
        Return lista
    End Function
    Private Function obtenerPuntoLlegada() As Entidades.PuntoLlegada

        Dim objPuntoLlegada As Entidades.PuntoLlegada = Nothing
        objPuntoLlegada = New Entidades.PuntoLlegada
        With objPuntoLlegada
            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            If (Me.cboAlmacen_Llegada.Items.Count > 0) Then
                .IdAlmacen = CInt(Me.cboAlmacen_Llegada.SelectedValue)
                Dim objAlmacen As Entidades.Almacen = (New Negocio.Almacen).SelectxId(.IdAlmacen)
                .pll_Direccion = objAlmacen.Direccion
                If (objAlmacen.Ubigeo.Trim.Length <= 0) Then
                    .pll_Ubigeo = "000000"
                Else
                    .pll_Ubigeo = objAlmacen.Ubigeo
                End If
            Else
                .IdAlmacen = Nothing
            End If
        End With
        Return objPuntoLlegada
    End Function
    Private Function obtenerPuntoPartida() As Entidades.PuntoPartida

        Dim objPuntoPartida As Entidades.PuntoPartida = Nothing
        objPuntoPartida = New Entidades.PuntoPartida

        With objPuntoPartida
            Select Case CInt(Me.hddFrmModo.Value)
                Case FrmModo.Nuevo
                    .IdDocumento = Nothing
                Case FrmModo.Editar
                    .IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select

            If (Me.cboAlmacen_Partida.Items.Count > 0) Then
                .IdAlmacen = CInt(Me.cboAlmacen_Partida.SelectedValue)
                Dim objAlmacen As Entidades.Almacen = (New Negocio.Almacen).SelectxId(.IdAlmacen)
                .Direccion = objAlmacen.Direccion
                If (objAlmacen.Ubigeo.Trim.Length <= 0) Then
                    .Ubigeo = "000000"
                Else
                    .Ubigeo = objAlmacen.Ubigeo
                End If
            Else
                .IdAlmacen = Nothing
            End If
        End With
        Return objPuntoPartida
    End Function
    Private Function obtenerObservaciones() As Entidades.Observacion

        Dim objObservacion As Entidades.Observacion = Nothing

        If (Me.txtObservaciones.Text.Trim.Length > 0) Then
            objObservacion = New Entidades.Observacion

            Select Case CInt((Me.hddFrmModo.Value))
                Case FrmModo.Nuevo
                    objObservacion.IdDocumento = Nothing
                Case FrmModo.Editar
                    objObservacion.IdDocumento = CInt(Me.hddIdDocumento.Value)
            End Select
            objObservacion.Observacion = Me.txtObservaciones.Text.Trim

        End If

        Return objObservacion

    End Function
    Private Function obtenerDocumentoCab() As Entidades.Documento

        Dim objDocumento As New Entidades.Documento

        With objDocumento

            Select Case CInt(Me.hddFrmModo.Value)

                Case FrmModo.Nuevo
                    .Id = Nothing
                Case FrmModo.Editar
                    .Id = CInt(Me.hddIdDocumento.Value)
            End Select

            .IdEmpresa = CInt(Me.cboEmpresa.SelectedValue)
            .IdRemitente = CInt(Me.cboEmpresa.SelectedValue)
            .IdDestinatario = CInt(Me.cboEmpresa.SelectedValue)
            .IdPersona = CInt(Me.cboEmpresa.SelectedValue)
            .IdTienda = CInt(Me.cboTienda.SelectedValue)
            .IdSerie = CInt(Me.cboSerie.SelectedValue)
            .Serie = CStr(Me.cboSerie.SelectedItem.ToString)
            .Codigo = Me.txtCodigoDocumento.Text.Trim
            .FechaEmision = CDate(Me.txtFechaEmision.Text)
            .IdEstadoDoc = 1 '************** ACTIVO
            .IdMotivoT = CInt(Me.cboMotivoTraslado.SelectedValue)
            .IdTipoOperacion = CInt(Me.cboTipoOperacion.SelectedValue)
            .IdUsuario = CInt(Session("IdUsuario"))
            .IdEstadoEntrega = 2 '************* ENTREGADO
            .IdTipoDocumento = CInt(Me.hddIdTipoDocumento.Value)
            '************* VALORES ADICIONALES
            .FactorMov = -1 '************** Mov de Almacén

        End With

        Return objDocumento

    End Function

#Region "************************ BÚSQUEDA AVANZADO"

    Private Sub btnAceptarBusquedaAvanzado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptarBusquedaAvanzado.Click
        busquedaAvanzado()
    End Sub

    Private Sub busquedaAvanzado()

        Try

            Dim IdSerie As Integer = CInt(Me.cboSerie.SelectedValue)
            Dim fechaInicio As Date = CDate(Me.txtFechaI.Text)
            Dim fechaFin As Date = CDate(Me.txtFechaF.Text)

            Me.GV_BusquedaAvanzado.DataSource = (New Negocio.Documento).DocumentoSelectxIdTipoDocumentoxFechaIxFechaFxPersonaxIdSerie(CInt(Me.hddIdTipoDocumento.Value), IdSerie, 0, fechaInicio, fechaFin)
            Me.GV_BusquedaAvanzado.DataBind()

            objScript.onCapa(Me, "capaBusquedaAvanzado")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub GV_BusquedaAvanzado_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_BusquedaAvanzado.PageIndexChanging
        Me.GV_BusquedaAvanzado.PageIndex = e.NewPageIndex
        busquedaAvanzado()
    End Sub

    Protected Sub GV_BusquedaAvanzado_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_BusquedaAvanzado.SelectedIndexChanged
        Try
            cargarDocumentoOP(0, 0, CInt(CType(Me.GV_BusquedaAvanzado.SelectedRow.FindControl("hddIdDocumento_BusquedaAvanzado"), HiddenField).Value))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargarDocumentoOP(ByVal IdSerie As Integer, ByVal codigo As Integer, ByVal IdDocumento As Integer)

        '**************** LIMPIAMOS EL FORMULARIO
        verFrm(FrmModo.Inicio, True, True, True, True, False)

        '****************** OBTENEMOS LOS DATOS DEL DOCUMENTO
        Dim objDocumento As Entidades.Documento = Nothing
        If (IdDocumento <> Nothing) Then
            objDocumento = (New Negocio.Documento).SelectxIdDocumento(IdDocumento)
        Else
            objDocumento = (New Negocio.Documento).SelectxIdSeriexCodigo(IdSerie, codigo)
        End If

        Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumento.Id)
        Dim objPuntoLlegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(objDocumento.Id)

        Me.listaDetalleDocumento = obtenerListaDetalleDocumento_OP(objDocumento.Id)

        Me.listaDocumentoRef = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(objDocumento.Id)

        Dim objObservaciones As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)

        '*************** GUARDAMOS EN SESSION
        setListaDetalleDocumento(Me.listaDetalleDocumento)
        setlistaDocumentoRef(Me.listaDocumentoRef)

        '************** 
        cargarDocumentoRef_GUI(objDocumento, Me.listaDocumentoRef, Me.listaDetalleDocumento, objPuntoPartida, objPuntoLlegada, objObservaciones, True)
        verFrm(FrmModo.Documento_Buscar_Exito, False, False, False, False, False)

        Me.GV_DocumentosReferencia_Find.DataSource = Nothing
        Me.GV_DocumentosReferencia_Find.DataBind()

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onload", " calcularPesoTotal(null); ", True)

        ''***************** CONFIGURAMOS LOS CONTROLES
        'Me.cboEmpresa.Enabled = activarControlesRef
        'Me.cboTienda.Enabled = activarControlesRef
        'Me.cboAlmacen.Enabled = activarControlesRef
        'Me.btnBuscarRemitente.Enabled = activarControlesRef
        'Me.btnBuscarDestinatario.Enabled = activarControlesRef
        'Me.btnBuscarProducto.Enabled = activarControlesRef
        'Me.btnLimpiarDetalleDocumento.Enabled = activarControlesRef

    End Sub

    Private Function obtenerListaDetalleDocumento_OP(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumento)

        Dim listaDetalle As List(Of Entidades.DetalleDocumento) = (New Negocio.DocumentoOrdenPedidoSucursal).DocumentoOrdenPedidoSucursal_SelectDetalle(IdDocumento)


        '************* QUITAMOS EL DETALLE DE LOS COMPONENTES
        For i As Integer = (listaDetalle.Count - 1) To 0 Step -1
            If (listaDetalle(i).ComponenteKit) Then
                listaDetalle.RemoveAt(i)
            End If
        Next

        Dim listaUnidadMedida_Peso As List(Of Entidades.MagnitudUnidad) = (New Negocio.MagnitudUnidad)._MagnitudUnidadselectxIdMagnitud(_IdMagnitud_Peso)

        For i As Integer = 0 To listaDetalle.Count - 1

            listaDetalle(i).ListaUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(listaDetalle(i).IdProducto)
            listaDetalle(i).IdDocumento = listaDetalle(i).IdDocumentoRef '*********** COLOCAMOS EL DOCUMENTO REF

            listaDetalle(i).ListaUnidadMedida_Peso = listaUnidadMedida_Peso

            listaDetalle(i).IdUnidadMedida_Peso = CInt(Me.cboUnidadMedida_PesoTotal.SelectedValue)

            If listaDetalle(i).IdUnidadMedidaPrincipal <= 0 Then
                listaDetalle(i).IdUnidadMedidaPrincipal = (New Negocio.ProductoUM).SelectIdUMPrincipalxIdProducto(listaDetalle(i).IdProducto)
            End If

            '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
            listaDetalle(i).Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(listaDetalle(i).IdProducto, listaDetalle(i).IdUnidadMedidaPrincipal, 1, _IdMagnitud_Peso, listaDetalle(i).IdUnidadMedida_Peso)

            '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
            listaDetalle(i).Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(listaDetalle(i).IdProducto, listaDetalle(i).IdUnidadMedida, listaDetalle(i).IdUnidadMedidaPrincipal, 1)
            listaDetalle(i).Peso = listaDetalle(i).Cantidad * listaDetalle(i).Equivalencia1 * listaDetalle(i).Equivalencia
        Next

        Return listaDetalle

    End Function

#End Region
    Protected Sub btnAnular_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAnular.Click
        anularDocumento()
    End Sub
    Private Sub anularDocumento()
        Try
            Dim IdDocumento As Integer = CInt(Me.hddIdDocumento.Value)

            If ((New Negocio.DocumentoOrdenPedidoSucursal).anularDocumento(IdDocumento)) Then
                objScript.mostrarMsjAlerta(Me, "El Proceso finalizó con éxito.")
                Me.cboEstado.SelectedValue = "2"
                verFrm(FrmModo.Documento_Save_Exito, False, False, False, False, False)
            Else
                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnBuscarDocumentoxCodigo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumentoxCodigo.Click
        Try
            cargarDocumentoOP(CInt(Me.cboSerie.SelectedValue), CInt(Me.hddCodigoDocumento.Value), Nothing)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        verFrm(FrmModo.Nuevo, True, True, True, True, True)
    End Sub

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Try

            verFrm(FrmModo.Editar, False, False, False, False, False)

            Me.listaDetalleDocumento = getListaDetalleDocumento()
            Me.listaDocumentoRef = getlistaDocumentoRef()

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            Me.GV_DocumentoRef.DataSource = Me.listaDocumentoRef
            Me.GV_DocumentoRef.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cmbLinea_AddProd, CInt(cboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(cboTipoExistencia.SelectedValue), True)
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub lkbQuitar_Click(ByVal sender As Object, ByVal e As EventArgs)
        quitarDetalleDocumento(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub quitarDetalleDocumento(ByVal Index As Integer)

        Try

            ActualizarListaDetalleDocumento()
            Me.listaDetalleDocumento = getListaDetalleDocumento()

            Me.listaDetalleDocumento.RemoveAt(Index)

            setListaDetalleDocumento(Me.listaDetalleDocumento)
            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnMostrarComponenteKit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            ActualizarListaDetalleDocumento()

            Dim index As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex

            Me.listaDetalleDocumento = getListaDetalleDocumento()

            '****************** MOSTRAMOS LOS COMPONENTES DEL KIT

            Me.GV_ComponenteKit.DataSource = (New Negocio.Kit).SelectComponentexIdKit(Me.listaDetalleDocumento(index).IdProducto)
            Me.GV_ComponenteKit.DataBind()


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    Dim ListaDetalleDocumentoVenta As List(Of Entidades.DetalleDocumento)

    Protected Sub OnClick_imgVerDetalle(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim Index As Integer = CType(CType(sender, ImageButton).NamingContainer, GridViewRow).RowIndex

        Dim IdDocumento As Integer = CInt(CType(Me.GV_DocumentoRef.Rows(Index).FindControl("hddIdDocumento"), HiddenField).Value)


        Me.ListaDetalleDocumentoVenta = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumento)

        Me.GV_DocumentoRef_Detalle.DataSource = Me.ListaDetalleDocumentoVenta
        Me.GV_DocumentoRef_Detalle.DataBind()

    End Sub
    Private Sub GV_DocumentoRef_Detalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_DocumentoRef_Detalle.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                CType(e.Row.FindControl("lblDespachado"), Label).Text = CStr(Math.Round(Me.ListaDetalleDocumentoVenta(e.Row.RowIndex).Cantidad - Me.ListaDetalleDocumentoVenta(e.Row.RowIndex).CantxAtender, 4, MidpointRounding.AwayFromZero))
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboAlmacen_Partida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAlmacen_Partida.SelectedIndexChanged

        ActualizarListaDetalleDocumento()
        Me.listaDetalleDocumento = getListaDetalleDocumento()
        For i As Integer = 0 To Me.listaDetalleDocumento.Count - 1
            With Me.listaDetalleDocumento(i)
                .Stock = (New Negocio.Util).fx_StockDisponible(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen_Partida.SelectedValue), .IdProducto)
                .UM1 = (New Negocio.ProductoUMView).SelectUMPrincipalxIdProducto(.IdProducto).NombreCortoUM
            End With
        Next

        setListaDetalleDocumento(Me.listaDetalleDocumento)
        Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
        Me.GV_Detalle.DataBind()
    End Sub
    Protected Sub valOnChange_cboUnidadMedida(ByVal sender As Object, ByVal e As System.EventArgs)
        actualizarEquivalencia_Peso(CType(CType(sender, DropDownList).NamingContainer, GridViewRow).RowIndex)
    End Sub
    Private Sub actualizarEquivalencia_Peso(ByVal index As Integer)
        Try

            ActualizarListaDetalleDocumento(index)

            Me.listaDetalleDocumento = getListaDetalleDocumento()


            With (Me.listaDetalleDocumento(index))
                Dim NewCantidad As Decimal = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUMold, .IdUnidadMedida, .Cantidad)
                .Cantidad = NewCantidad

                '***************** ALMACENAMOS LA EQUIVALENCIA DE 1 UM PRINCIPAL EN LA UM DEL PESO
                .Equivalencia = (New Negocio.Util).fx_getMagnitudProducto(.IdProducto, .IdUnidadMedidaPrincipal, 1, _IdMagnitud_Peso, .IdUnidadMedida_Peso)

                '***************** ALMACENAMOS LA EQUIVALENCIA DE LA UM SELECCIONADA DEL PRODUCTO RESPECTO A LA UM PRINCIPAL
                .Equivalencia1 = (New Negocio.Util).fx_getValorEquivalenteProducto(.IdProducto, .IdUnidadMedida, .IdUnidadMedidaPrincipal, 1)


                .Peso = .Cantidad * .Equivalencia1 * .Equivalencia

            End With

            setListaDetalleDocumento(Me.listaDetalleDocumento)

            Me.GV_Detalle.DataSource = Me.listaDetalleDocumento
            Me.GV_Detalle.DataBind()

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "  onLoad ", "  calcularPesoTotal(null);   ", True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarGrilla_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarGrilla_AddProducto.Click
        BuscarProductoCatalogo(0)
    End Sub

    Private Sub btnAddProductos_AddProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProductos_AddProducto.Click
        addProducto_DetalleDocumento()
    End Sub
End Class