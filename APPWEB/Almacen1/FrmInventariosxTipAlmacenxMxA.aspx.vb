﻿Public Partial Class FrmInventariosxTipAlmacenxMxA
    Inherits System.Web.UI.Page

    Dim cbo As Combo
    Private ListaAlmacen As List(Of Entidades.TipoAlmacen)
    Private objScript As New ScriptManagerClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Me.IsPostBack Then

            Limpiar()
            cbo = New Combo

            With cbo
                .LlenarCbo_TipoAlmacen(Me.cbotipoalmacen)
            End With
        End If

    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        CargarCrystal()
    End Sub
    Private Property S_ListaAlmacen() As List(Of Entidades.TipoAlmacen)
        Get
            Return CType(Session("ListaAlmacen"), List(Of Entidades.TipoAlmacen))
        End Get
        Set(ByVal value As List(Of Entidades.TipoAlmacen))
            Session.Remove("ListaAlmacen")
            Session.Add("ListaAlmacen", value)
        End Set
    End Property
    Private Sub CargarCrystal()


        Dim varanio As String = ""
        Dim varmesini As String = ""
        Dim varmesfin As String = ""
        Dim cadenagv As String = ""
        Dim cadenaalmacenado As String = ""
        Dim valorrecorrido As String = ""

        ''Buscamos los valores elegidos en los ddl
        varanio = cboanio.SelectedItem.Text
        varmesini = cboMesInicio.SelectedValue
        varmesfin = cboMesFinal.SelectedValue

        ''Validamos que hayan elegido Tipos de Almacen
        If (GV_TipoAlmacen.Rows.Count) = 0 Then
            objScript.mostrarMsjAlerta(Me, "Elija un Tipo de Almacen para proseguir con la consulta")
        End If

        ''Hacemos el recorrido a la grid para buscar los id de cada  fila elegida
        For Each gvrow As GridViewRow In GV_TipoAlmacen.Rows
            Dim lblTipAlmacen As HiddenField = DirectCast(gvrow.FindControl("hddIdTipoAlmacen"), HiddenField)

            If lblTipAlmacen.Value >= "1" Then
                valorrecorrido = lblTipAlmacen.Value

            End If
            'Almacenamos la data que nos llega en una variable
            cadenagv = valorrecorrido

            ''Separamos por comas
            cadenaalmacenado += cadenagv + ","

        Next

        ''Aqui eliminamos la ultima coma asignada a la cadena
        cadenaalmacenado = cadenaalmacenado.Remove(cadenaalmacenado.Length - 1)

        Response.Redirect("../Almacen1/Reportes/Visor.aspx?iReporte=9&Anio=" + varanio + "&MesInicial=" + varmesini + "&MesFinal=" + varmesfin + "&CadenaTipoAlmacen=" + cadenaalmacenado)


    End Sub

    Protected Sub add_Click(ByVal sender As Object, ByVal e As EventArgs) Handles add.Click
        Almacen_Add(CInt(cbotipoalmacen.SelectedValue))
    End Sub


    Protected Sub lkbAlmacenRemove_Click(ByVal sender As Object, ByVal e As EventArgs)
        Almacen_Remove(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    End Sub

    Private Sub Almacen_Remove(ByVal index As Integer)

        S_ListaAlmacen.RemoveAt(index)

        Me.GV_TipoAlmacen.DataSource = S_ListaAlmacen
        Me.GV_TipoAlmacen.DataBind()

    End Sub

    Private Sub Almacen_Add(ByVal IdtipoAlmacen As Integer)

        If IsNothing(S_ListaAlmacen) Then S_ListaAlmacen = New List(Of Entidades.TipoAlmacen)

        Me.S_ListaAlmacen.AddRange((New Negocio.TipoAlmacen).SelectxIdTipoAlmacenAll(IdtipoAlmacen))

        Me.GV_TipoAlmacen.DataSource = Me.S_ListaAlmacen
        Me.GV_TipoAlmacen.DataBind()

    End Sub

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click

        cbo = New Combo
        With cbo
            .LlenarCbo_TipoAlmacen(Me.cbotipoalmacen)
        End With

        Limpiar()

    End Sub

    Private Sub Limpiar()
        Session.Remove("ListaAlmacen")
        Me.cboanio.SelectedIndex = 4
        Me.cboMesFinal.SelectedValue = "1"
        Me.cboMesInicio.SelectedValue = "1"
        Me.GV_TipoAlmacen.DataSource = Nothing
        Me.GV_TipoAlmacen.DataBind()
    End Sub
End Class