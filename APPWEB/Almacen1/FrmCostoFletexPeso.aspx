﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmCostoFletexPeso.aspx.vb" Inherits="APPWEB.FrmCostoFlete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td >
            
                  <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" Width="80px" runat="server" Text="Nuevo" ToolTip="Generar un Doc. Ajuste de Inventario desde un Doc. Toma de Inventario." />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" OnClientClick="return( valOnClick_btnGuardar()  );" Width="80px" runat="server" Text="Guardar" ToolTip="Guardar" />
                        </td>
                        <td>
                        
                            <asp:Button ID="btnCancelar"  Width="80px" runat="server" Text="Cancelar" ToolTip="Cancelar" />
                        
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td style="width:100%" align="right">
                        <table>
                <tr>
                    <td class="Texto">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
                        </td>
                    </tr>
                </table>
            
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                COSTO DE FLETE - PESO</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="text-align:right;font-weight:bold"  >
                            Tienda Origen:</td>
                        <td>
                            <asp:DropDownList ID="cboTiendaOrigen" runat="server" Width="300px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" ToolTip="Buscar" Width="70px" />
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="Texto" style="text-align:right;font-weight:bold" >
                            Tienda Destino:</td>
                        <td>
                            <asp:DropDownList ID="cboTiendaDestino" runat="server" Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Costo" runat="server">
                <table>
                <tr>
                <td class="Texto" style="text-align:right;font-weight:bold" >Costo:</td>
                <td>
                    <asp:DropDownList ID="cboMoneda" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                <asp:TextBox ID="txtCostoxPeso" Width="80px" onKeyPress=" return(  validarNumeroPuntoPositivo('event')  ); " onFocus=" return( aceptarFoco(this)  ); "  onBlur=" return(  valBlur(event) ); " runat="server"></asp:TextBox>
                </td>
                <td>
                
                </td>
                </tr>
                <tr>
                <td class="Texto" style="text-align:right;font-weight:bold" >Unidad Medida:</td>
                <td colspan="2">
                    <asp:DropDownList ID="cboUnidadMedida" runat="server" Width="100%">
                    </asp:DropDownList>
                </td>
                <td></td>
                </tr>
                </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Grilla" runat="server">
         <asp:GridView ID="GV_CostoxPeso" runat="server" AutoGenerateColumns="False" 
                        Width="100%" >
                <Columns>                  
                   <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-Height="25px" 
                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                        <ItemTemplate>
                            <asp:LinkButton ID="btnEditar" runat="server" OnClick ="btnEditar_Click">Editar</asp:LinkButton>
                            <asp:HiddenField ID ="hddIdTiendaOrigen" 
                                Value='<%# DataBinder.Eval(Container.DataItem,"IdTiendaOrigen") %>'  
                                runat ="server"  />
                            <asp:HiddenField ID ="hddIdTiendaDestino" 
                                Value='<%# DataBinder.Eval(Container.DataItem,"IdTiendaDestino") %>'  
                                runat ="server"  />                            
                        </ItemTemplate>
                    </asp:TemplateField>
                  <asp:TemplateField HeaderStyle-HorizontalAlign="Center" 
                        ItemStyle-HorizontalAlign="Center"  >
                        <ItemTemplate>
                            <asp:LinkButton ID="btnEliminar" ForeColor="Red"  
                                OnClientClick="   return(  valOnClick_btnEliminar() );  " runat="server" 
                                OnClick ="btnEliminar_Click" >Eliminar</asp:LinkButton>
                        </ItemTemplate>
                  </asp:TemplateField>
                                       
                  <asp:BoundField DataField="TiendaOrigen" HeaderText="Tienda Origen" 
                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                  </asp:BoundField>
                  
                  <asp:BoundField DataField="TiendaDestino" HeaderText="Tienda Destino" 
                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                  </asp:BoundField>
                  
                  <asp:TemplateField HeaderText="Costo" 
                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                  <ItemTemplate>
                  <table>
                  <tr>
                  <td> 
                      <asp:Label ID="lblMoneda" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"mon_Simbolo") %>'   ></asp:Label> </td>
                  <td>
                    <asp:Label ID="lblCosto" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"cfp_costoxPesoUnit","{0:F3}") %>'   ></asp:Label> 
                  </td>
                  <td></td>
                  </tr>
                  </table>
                  </ItemTemplate>
                  </asp:TemplateField>
                  
                  <asp:BoundField DataField="um_NombreCorto" HeaderText="U.M." 
                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                  </asp:BoundField>
                  
                </Columns>
                
                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                <EditRowStyle CssClass="GrillaEditRow" />
                <FooterStyle CssClass="GrillaFooter" />
                <HeaderStyle CssClass="GrillaHeader" />
                <PagerStyle CssClass="GrillaPager" />
                <RowStyle CssClass="GrillaRow" />
                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>              
                </asp:Panel>
         
                
                </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    
      <script language="javascript" type="text/javascript">

        function valOnClick_btnEliminar() {
            return confirm('Desea continuar con la Operación ?');
        }

        function valOnClick_btnGuardar() {

            var cboTiendaOrigen = document.getElementById('<%=cboTiendaOrigen.ClientID%>');
            if (isNaN(parseInt(cboTiendaOrigen.value)) || cboTiendaOrigen.value.length <= 0 || parseInt(cboTiendaOrigen.value) <= 0) {
                alert('Debe seleccionar una Tienda Origen');
                return false;
            }

            var cboTiendaDestino = document.getElementById('<%=cboTiendaDestino.ClientID%>');
            if (isNaN(parseInt(cboTiendaDestino.value)) || cboTiendaDestino.value.length <= 0 || parseInt(cboTiendaDestino.value) <= 0) {
                alert('Debe seleccionar una Tienda Destino');
                return false;
            }

            var cboMoneda = document.getElementById('<%=cboMoneda.ClientID%>');
            if (isNaN(parseInt(cboMoneda.value)) || cboMoneda.value.length <= 0 || parseInt(cboMoneda.value) <= 0) {
                alert('Debe seleccionar una Moneda');
                return false;
            }

            var cboUnidadMedida = document.getElementById('<%=cboUnidadMedida.ClientID%>');
            if (isNaN(parseInt(cboUnidadMedida.value)) || cboUnidadMedida.value.length <= 0 || parseInt(cboUnidadMedida.value) <= 0) {
                alert('Debe seleccionar una Unidad Medida');
                return false;
            }
            return confirm('Desea continuar con la Operación ?');
        }
    
    </script>
    
</asp:Content>
