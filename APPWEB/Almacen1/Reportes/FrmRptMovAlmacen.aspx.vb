﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmRptMovAlmacen
    Inherits System.Web.UI.Page
    Dim cbo As New Combo
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            cbo.LlenarCboPropietario(Me.cboEmpresa, True)
            cbo.llenarCboAlmacen(Me.cboAlmacenOrigen, True)
            cbo.llenarCboAlmacen(Me.cboAlmacenDestino, True)
            cbo.LlenarCboLinea(Me.cboLinea, True)
            Dim objFechaActual As New Negocio.FechaActual
            txtFechaInicio.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            txtFechaFin.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            Me.hddFechaActual.Value = CStr(Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy"))
        End If
    End Sub
    Protected Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboLinea.SelectedIndexChanged
        cbo.LlenarCboSubLineaxIdLinea(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), True)
    End Sub
End Class