﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmRptMovAlmacen.aspx.vb" Inherits="APPWEB.FrmRptMovAlmacen" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<table width="100%">

<tr>
   <td colspan="4" align="center" class="TituloCelda">
   <h5>Movimiento de Almacén - Consolidado por Producto</h5>
   </td>
</tr>
<tr>
   <td style="width: 100px">
   <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Empresa"></asp:Label>
   </td>
   <td style="width: 137px">
       <asp:DropDownList ID="cboEmpresa" runat="server">
       </asp:DropDownList>
    </td>
   <td style="width: 107px"></td>
   <td></td>
</tr>
<tr>
   <td style="width: 100px">
   <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Almacen Origen"></asp:Label>
   </td>
   <td style="width: 137px">
       <asp:DropDownList ID="cboAlmacenOrigen" runat="server">
       </asp:DropDownList>
    </td>
   <td style="width: 107px"><asp:Label ID="Label3" runat="server" CssClass="Label" Text="Almacen Destino"></asp:Label></td>
   <td>
       <asp:DropDownList ID="cboAlmacenDestino" runat="server">
       </asp:DropDownList>
    </td>
</tr>
<tr>
   <td style="width: 100px">
   <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Linea"></asp:Label>
   </td>
   <td style="width: 137px">
       <asp:DropDownList ID="cboLinea" runat="server" AutoPostBack="True">
       </asp:DropDownList>
    </td>
   <td style="width: 107px">
   <asp:Label ID="Label5" runat="server" CssClass="Label" Text="SubLinea"></asp:Label>
   </td>
   <td>
       <asp:DropDownList ID="cboSubLinea" runat="server">
       </asp:DropDownList>
    </td>
</tr>
<tr>
   <td style="width: 100px">
   <asp:Label ID="Label6" runat="server" CssClass="Label" Text="Fecha Inicial"></asp:Label>
   </td>
   <td style="width: 137px">
       <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
       <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" TargetControlID="txtFechaInicio"></cc1:CalendarExtender>
   </td>
   <td style="width: 107px">
   <asp:Label ID="Label7" runat="server" CssClass="Label" Text="Fecha Final"></asp:Label>
   </td>
   <td>
       <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
       <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" TargetControlID="txtFechaFin"></cc1:CalendarExtender>
       </td>
</tr>
<tr>
   <td style="width: 100px">
   <asp:ImageButton ID="btnAceptar_SubLinea_Cab" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG" OnClientClick="return(verReporte());" 
   onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';" />
   </td>
   <td style="width: 137px"></td>
   <td style="width: 107px"></td>
   <td><asp:HiddenField ID="hddFechaActual" runat="server" />
                <asp:HiddenField ID="hddRbtTipoReporte" runat="server" /></td>
</tr>
<tr>
   <td colspan="4">
   <iframe name="frame1" id="frame1" width="100%" scrolling="Auto" style="height: 1130px"></iframe>
   </td>   
</tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>
<script language="javascript" type="text/javascript">
    function getCampoxValorCombo(combo, value) {
        var campo = '';
        if (combo != null) {
            for (var i = 0; i < combo.length; i++) {
                if (combo[i].value == value) {
                    campo = combo[i].innerHTML;
                    return campo;
                }
                
            }
        }
        return campo;
    }  
    function verReporte() {
        var IdEmpresa = document.getElementById('<%= CboEmpresa.ClientID%>').value;
        var nEmpresa = getCampoxValorCombo(document.getElementById('<%= CboEmpresa.ClientID%>'), IdEmpresa);       
        var IdOrigen = document.getElementById('<%= cboAlmacenOrigen.ClientID%>').value;
        var nOrigen = getCampoxValorCombo(document.getElementById('<%= cboAlmacenOrigen.ClientID%>'), IdOrigen);
        var IdDestino = document.getElementById('<%= cboAlmacenDestino.ClientID%>').value;
        var nDestino = getCampoxValorCombo(document.getElementById('<%= cboAlmacenDestino.ClientID%>'), IdDestino);
        var IdLinea = document.getElementById('<%= cboLinea.ClientID%>').value;
        var IdSubLinea = document.getElementById('<%= cboSubLinea.ClientID%>').value;
        var fechaInicio;
        var fechaFin;
        if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
            fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value
            fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value
        } else {
            fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
            fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
        }
        frame1.location.href = 'visorMovAlmacen.aspx?iReporte=1&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + IdEmpresa + '&nEmpresa=' + nEmpresa + '&IdOrigen=' + IdOrigen + '&nOrigen=' + nOrigen + '&IdDestino=' + IdDestino + '&nDestino=' + nDestino + '&IdLinea=' + IdLinea + '&IdSubLinea=' + IdSubLinea ;
        return false;
    }
</script>
</asp:Content>
