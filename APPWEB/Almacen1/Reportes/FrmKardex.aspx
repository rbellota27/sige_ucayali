﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmKardex.aspx.vb" Inherits="APPWEB.FrmKardex" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
    <tr><td class="TituloCelda" > Kardex  </td> </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel_Principal" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                <table>
                    <tr>
                        <td class="Texto" >
                            Empresa:</td>
                        <td>
                            <asp:DropDownList ID="cboEmpresa" runat="server" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" >
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="Texto" >
                            Almacén:</td>
                        <td>
                            <asp:DropDownList ID="cboAlmacen" runat="server" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" >
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="Texto" >
                            Línea:</td>
                        <td>
                            <asp:DropDownList ID="cboLinea" Width="300px" runat="server" 
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto" >
                            SubLínea:</td>
                        <td>
                            <asp:DropDownList ID="cboSubLinea" Width="300px" runat="server" 
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="Texto" style="text-align: right">
                            Producto:</td>
                        <td colspan="4">
                            <asp:DropDownList ID="cboProducto" Width="665px" runat="server">
                            </asp:DropDownList>
                        </td>                        
                    </tr>
                    <tr>
                    <td colspan="5">
                    
                    <table>
                    <tr>
                        <td class="Texto">
                          Fecha Inicio:</td>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" onblur="return( valFecha(this) );" Width="100px" CssClass="TextBox_Fecha" Font-Bold="true" runat="server"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender" runat="server" 
                                 CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                                 CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                 CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"                                  
                                 TargetControlID="txtFechaInicio"
                               ClearMaskOnLostFocus="false"    
                                  Mask="99/99/9999">
                             </cc1:MaskedEditExtender>
                               <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" 
                                            TargetControlID="txtFechaInicio" Enabled="True" Format="dd/MM/yyyy" >
                                        </cc1:CalendarExtender>                                            
                        </td>
                        <td class="Texto">
                            Fecha Fin:</td>
                        <td>
                            <asp:TextBox ID="txtFechaFin" Width="100px" CssClass="TextBox_Fecha" Font-Bold="true" runat="server"></asp:TextBox>
                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" 
                                 CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                                 CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                 CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"                                  
                                 TargetControlID="txtFechaFin"
                               ClearMaskOnLostFocus="false"    
                                  Mask="99/99/9999">
                             </cc1:MaskedEditExtender>
                               <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                            TargetControlID="txtFechaFin" Enabled="True" Format="dd/MM/yyyy" >
                                        </cc1:CalendarExtender>                                            
                        </td>
                        <td >
                            <asp:Button ID="btnAceptar" Width="75px" runat="server" Text="Reporte" ToolTip="Mostrar Reporte Kárdex" />
                        </td>
                    </tr>
                    </table>
                    
                    
                    </td>
                    
                    </tr>
                    
                </table>
                </ContentTemplate>
                </asp:UpdatePanel>
                
                
            </td>
        </tr>
        </table>
</asp:Content>
