﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Partial Public Class visorKardexMovAlmacen
    Inherits System.Web.UI.Page
    Dim reporte As New ReportDocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim iReporte As String = Request.QueryString("iReporte")
        If Not Me.IsPostBack Then
            If iReporte Is Nothing Then
                iReporte = ""
            End If

            ViewState.Add("iReporte", iReporte)

            Select Case iReporte

                Case "1"

                Case "2"
                    ViewState.Add("Idempresa", Request.QueryString("Idempresa"))
                    ViewState.Add("idalmacen", Request.QueryString("idalmacen"))
                    ViewState.Add("idproducto", Request.QueryString("idproducto"))
                    ViewState.Add("FechaInicio", Request.QueryString("FechaInicio"))
                    ViewState.Add("FechaFin", Request.QueryString("FechaFin"))
            End Select

        End If
        MuestraReporte()
    End Sub
    Private Sub MuestraReporte()
        Select Case ViewState.Item("iReporte").ToString

            Case "1"
                mostrarReporte(CStr(ViewState.Item("iReporte")))

            Case "2"
                kardexporMetVal()

        End Select


    End Sub
    Private Sub mostrarReporte(ByVal NroReporte As String)
        Try
            If NroReporte = "1" Then
                Dim objReporte As New Negocio.MovAlmacen
                Dim ds As DataSet = objReporte.RptKardexMovAlmacen(CInt(Request.QueryString("IdEmpresa")), CInt(Request.QueryString("IdAlmacen")), CInt(Request.QueryString("IdLinea")), CInt(Request.QueryString("IdSubLinea")), CInt(Request.QueryString("IdProducto")), CStr(Request.QueryString("FechaInicio")), CStr(Request.QueryString("FechaFin")))
                reporte = New CR_KardexMovAlmacen
                reporte.SetDataSource(ds)
                reporte.SetParameterValue("nTitulo", "Kardex Movimiento Almacén")
                reporte.SetParameterValue("nEmpresa", Request.QueryString("nEmpresa"))
                reporte.SetParameterValue("nAlmacen", Request.QueryString("nAlmacen"))
                reporte.SetParameterValue("nLinea", Request.QueryString("nLinea"))
                reporte.SetParameterValue("nSubLinea", Request.QueryString("nSubLinea"))
                reporte.SetParameterValue("nFechas", "Del:" + Space(2) + Request.QueryString("FechaInicio") + Space(5) + "Al:" + Space(2) + Request.QueryString("FechaFin"))
                CRViewer.ReportSource = reporte
            End If
        Catch ex As Exception
            'Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
            Response.Write("<script>alert('" + ex.Message + "');</script>")
        End Try
    End Sub

    Private Sub kardexporMetVal()
        Dim idempresa, idalmacen, idproducto As Integer

        If CStr(ViewState.Item("Idempresa")) = "" Then
            idempresa = 0
        Else
            idempresa = CInt(ViewState.Item("Idempresa"))
        End If
        If CStr(ViewState.Item("idalmacen")) = "" Then
            idalmacen = 0
        Else
            idalmacen = CInt(ViewState.Item("idalmacen"))
        End If

        If CStr(ViewState.Item("idproducto")) = "" Then
            idproducto = 0
        Else
            idproducto = CInt(ViewState.Item("idproducto"))
        End If

        Try

            Dim objReporte As New Negocio.MovAlmacen
            Dim ds As DataSet = objReporte.kardexporMetVal(idempresa, idalmacen, idproducto, CStr(ViewState.Item("FechaInicio")), CStr(ViewState.Item("FechaFin")))
            reporte = New CR_KardexporMetVal


            Dim f1, f2 As String
            f1 = CStr(ViewState.Item("FechaInicio"))
            f2 = CStr(ViewState.Item("FechaInicio"))

            reporte.SetDataSource(ds)
            reporte.SetParameterValue("@fechaInicio", ViewState("FechaInicio"))
            reporte.SetParameterValue("@FechaFin", ViewState("FechaFin"))

            CRViewer.ReportSource = reporte


        Catch ex As Exception

            Response.Write("<script>alert('" + ex.Message + "');</script>")

        End Try

    End Sub

    Private Sub visorKardexMovAlmacen_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reporte.Close()
        reporte.Dispose()
    End Sub
End Class