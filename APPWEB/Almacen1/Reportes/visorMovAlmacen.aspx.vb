﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Partial Public Class visor2
    Inherits System.Web.UI.Page
    Dim reporte As New ReportDocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim iReporte As String = Request.QueryString("iReporte")
        If Not Me.IsPostBack Then
            If iReporte Is Nothing Then
                iReporte = ""
            End If
        End If
        mostrarReporte(iReporte)
    End Sub
    Private Sub mostrarReporte(ByVal NroReporte As String)
        Try
            If NroReporte = "1" Then
                Dim IdLinea, IdSubLinea, IdOrigen, IdDestino As String
                If Request.QueryString("IdOrigen") = "0" Then
                    IdOrigen = ""
                Else
                    IdOrigen = Request.QueryString("IdOrigen")
                End If
                If Request.QueryString("IdDestino") = "0" Then
                    IdDestino = ""
                Else
                    IdDestino = Request.QueryString("IdDestino")
                End If
                If Request.QueryString("IdLinea") = "0" Then
                    IdLinea = ""
                Else
                    IdLinea = Request.QueryString("IdLinea")
                End If
                If Request.QueryString("IdSubLinea") = "0" Then
                    IdSubLinea = ""
                Else
                    IdSubLinea = Request.QueryString("IdSubLinea")
                End If
                Dim objReporte As New Negocio.MovAlmacen
                Dim ds As DataSet = objReporte.RptMovAlmacen(CInt(Request.QueryString("IdEmpresa")), CInt(IdOrigen), CStr(IdDestino), CStr(IdLinea), CStr(IdSubLinea), CStr(Request.QueryString("FechaInicio")), CStr(Request.QueryString("FechaFin")))
                reporte = New CR_MovAlmacen
                reporte.SetDataSource(ds)
                reporte.SetParameterValue("nFechas", "Fecha Inicio:" + Space(2) + Request.QueryString("FechaInicio") + Space(5) + "Fecha Final:" + Space(2) + Request.QueryString("FechaFin"))
                reporte.SetParameterValue("nOrigen", Request.QueryString("nOrigen"))
                reporte.SetParameterValue("nDestino", Request.QueryString("nDestino"))
                reporte.SetParameterValue("nEmpresa", Request.QueryString("nEmpresa"))
                CRViewer.ReportSource = reporte

            End If
        Catch ex As Exception
            Response.Write("<script>alert('Problemas en la carga de datos.');</script>")
        End Try
    End Sub

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        reporte.Close()
        reporte.Dispose()
    End Sub
End Class