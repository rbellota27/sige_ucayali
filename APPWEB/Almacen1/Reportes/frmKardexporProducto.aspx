﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmKardexporProducto.aspx.vb" Inherits="APPWEB.frmKardexporProducto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%" class="style1">
        <tr>
            <td colspan="9" align="center " class="TituloCelda">
                Kardex por Producto
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="Label">
                                        Empresa:<asp:DropDownList ID="cmbEmpresa" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Label">
                                        Almacen:<asp:DropDownList ID="cmbAlmacen" runat="server" Enabled="false">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="Label_fsp">
                                        Fecha Inicio:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                            TargetControlID="txtFechaInicio">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td class="Label_fsp">
                                        Fecha Fin:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                            TargetControlID="txtFechaFin">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="btnBusProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                                            OnClientClick="return(valOnClick_btnBuscarProducto());" onmouseout="this.src='../../Imagenes/BuscarProducto_b.JPG';"
                                            onmouseover="this.src='../../Imagenes/BuscarProducto_A.JPG';" />
                                        <asp:ImageButton ID="btnLimpiarDetalleDocumento" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                            onmouseout="this.src='../../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                     <asp:UpdatePanel ID ="udpDetalle" runat ="server"  UpdateMode ="Conditional" >
                                     <ContentTemplate >
                                            <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%">
                                            <Columns>
                                                <asp:BoundField DataField="Prod_Codigo" HeaderText="Código" HeaderStyle-Height="25px"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" HeaderStyle-Height="25px"
                                                    HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                                <asp:BoundField DataField="UnidadMedida" HeaderText="UM Principal" HeaderStyle-Height="25px"
                                                    HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdfIdProductodt" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="GrillaHeader" />
                                            <FooterStyle CssClass="GrillaFooter" />
                                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            <EditRowStyle CssClass="GrillaEditRow" />
                                            <PagerStyle CssClass="GrillaPager" />
                                            <RowStyle CssClass="GrillaRow" />
                                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        </asp:GridView>
                                     <asp:HiddenField ID="hddidproducto" runat="server" />
                                     
                                     </ContentTemplate>
                                     <Triggers >
                                     <asp:AsyncPostBackTrigger ControlID ="DGV_AddProd" EventName ="SelectedIndexChanged" />
                                     </Triggers>
                                     </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                        onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';"
                                        OnClientClick="return(mostrarReporteDetallado());" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                
                <asp:HiddenField ID="hddFechaActual" runat="server" />
            </td>
        </tr>
    </table>
    
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="udpbProduc" runat="server">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="Label17" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                            DataTextField="Descripcion" DataValueField="Id">
                                        </asp:DropDownList>
                                        <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub Línea:"></asp:Label>
                                        <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                            DataValueField="Id" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripción:"></asp:Label>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarGrilla_AddProd">                                                    
                                                    <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                        onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );"></asp:TextBox>
                                                        </asp:Panel>
                                                </td>
                                                <td class="Texto">
                                                    Cód.:
                                                </td>
                                                <td>
                                                <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarGrilla_AddProd">
                                                    <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                        onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );"
                                                        TabIndex="205"></asp:TextBox>
                                                        </asp:Panel>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                        TabIndex="207" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                        onmouseout="this.src='../../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../../Imagenes/Agregar_A.JPG';"
                                                        OnClientClick="return(valAddProductos());" TabIndex="208" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                        onmouseout="this.src='../../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';"
                                                        OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                                CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                                CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedText="Búsqueda Avanzada" ExpandedText="Búsqueda Avanzada" ExpandDirection="Vertical"
                                SuppressPostBack="true">
                            </cc1:CollapsiblePanelExtender>
                            <asp:Image ID="Image21_11" runat="server" Height="16px" />
                            <asp:Label ID="Label21_11" runat="server" Text="Búsqueda Avanzada" CssClass="Label"></asp:Label>
                            <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                                <table width="100">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Atributo:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                                Width="650px">
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                        HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-Height="25px" />
                                                    <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                            <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="GrillaRow" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                                <Columns>
                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                    <asp:BoundField DataField="Codigo" HeaderText="Código" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                    <asp:BoundField DataField="UnidadMedida" HeaderText="UMP" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdfIdproducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                                Width="50px" Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                            <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                                Width="50px" Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                            <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                                CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                            <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                                Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionProductos('2'));" />
                            <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                                runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script language="javascript" type="text/javascript">


        function valKeyPressCodigoSL(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function CierraCapaProd() {
            offCapa('capaBuscarProducto_AddProd');
            return false;
        }

        function valAddProductos() {

            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            //        var txtIdCliente = 
            //        if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
            //            alert('Debe seleccionar un Cliente.');
            //            return false;
            //        }

            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('No se seleccionaron productos.');
                return false;
            }

            //            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
            //                var rowElem = grilla.rows[i];
            //                var txtCantidad = rowElem.cells[0].children[0];
            //                if (parseFloat(txtCantidad.value) > 0) {
            //                    // if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
            //                    //                        alert('La cantidad ingresada excede a la cantidad disponible.');
            //                    //                        txtCantidad.select();
            //                    //                        txtCantidad.focus();
            //                    //     return false;
            //                    //  } else if (parseFloat(txtCantidad.value) > 0) {
            //                    cont = cont + 1;
            //                    //  }
            //                }
            //            }

            //            if (cont == 0) {
            //                alert('No se seleccionaron productos.');
            //                return false;
            //            }

            /*
            //************* Validamos que no agregue de otro almacen
            
            if (grillaDetalle != null) {

                if (grillaDetalle.rows.length > 1) {

                    //************ valido el almacen
                    
            var hddIdAlmacen = 
                    

                    if (parseFloat(cboAlmacen.value) != parseFloat(hddIdAlmacen.value)) {
                        
            return false;
            }
            }
            }
            */
            return true;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        function valStockDisponible_AddProd() {
            /*var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
            if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].id == event.srcElement.id) {
            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
            alert('La cantidad ingresada excede a la cantidad disponible.');
            rowElem.cells[0].children[0].select();
            rowElem.cells[0].children[0].focus();
            return false;
            }
            return false;
            }

                }
            }*/
            return false;
        }
        function valKeyPressCantidadAddProd(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                document.getElementById('<%=btnAddProductos_AddProd.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }
        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        //***********************************

        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }


        function mostrarReporteDetallado() {
            var IdEmpresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
            if (document.getElementById('<%= cmbEmpresa.ClientID%>' == null)) {
                IdEmpresa = 0
            }

            var idalmacen = document.getElementById('<%= cmbAlmacen.ClientID%>').value;
            if (document.getElementById('<%= cmbAlmacen.ClientID%>') == null) {
                idalmacen = 0
            }


            var idproducto = 0;

            idproducto = document.getElementById('<%= hddidproducto.ClientID%>').value;
            if (document.getElementById('<%= hddidproducto.ClientID%>') == null) {
                idproducto = 0;
            }

            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('Debe Seleccionar un  Producto.');
                return false;
            }

            var fechaInicio;
            var fechaFin;
            if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
                fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
                fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
            } else {
                fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
                fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
            }
            frame1.location.href = 'visorKardexMovAlmacen.aspx?iReporte=2&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&Idempresa=' + IdEmpresa + '&idalmacen=' + idalmacen + '&idproducto=' + idproducto;
            return false;
        }
    </script>

</asp:Content>
