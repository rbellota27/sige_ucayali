﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="MovProductoIS.aspx.vb" Inherits="APPWEB.MovProductoIS" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="TituloCelda">
                Ingresos y Salidas de Productos
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="Texto">
                            Empresa:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlEmpresa" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Tienda:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTienda" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Almacen:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAlmacen" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto">
                            Tipo Existencia:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTipoExistencia" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            L&iacute;nea:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLinea" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Sub-L&iacute;nea::
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSubLinea" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Codigo:
                        </td>
                        <td>
                            <asp:Label ID="lblCodigo" Font-Bold="true" CssClass="LabelRojo" runat="server" Text="Codigo"></asp:Label>
                        </td>
                        <td colspan="2" class="Texto">
                            Descripcion:
                        </td>
                        <td>
                            <asp:Label ID="lblDescripcion" CssClass="Texto" runat="server" Text="Descripcion"></asp:Label>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                onmouseout="this.src='/Imagenes/Limpiar_B.JPG';" onmouseover="this.src='/Imagenes/Limpiar_A.JPG';"
                                OnClientClick="return( limpiarProducto() );" TabIndex="209" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="Texto">
                            Fecha Inicio:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaInicio" runat="server" onFocus="return ( onFocus_RealOnly(this)  );"
                                CssClass="TextBoxReadOnly"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" TargetControlID="txtFechaInicio">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="Texto">
                            Fecha Fin:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaFin" runat="server" onFocus="return ( onFocus_RealOnly(this)  );"
                                CssClass="TextBoxReadOnly"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" TargetControlID="txtFechaFin">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                    onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" onmouseover="this.src='/Imagenes/Aceptar_A.JPG';"
                    OnClientClick="return(mostrarReporte());" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdProducto" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1200px">
                </iframe>
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 70px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                TipoExistencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                    DataTextField="Descripcion" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub Línea:"></asp:Label>
                                <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    AutoPostBack="true" DataValueField="Id">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripción:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="text-align: right">
                                            Cód.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );"
                                                TabIndex="205"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='/Imagenes/Buscar_b.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='/Imagenes/Limpiar_B.JPG';" onmouseover="this.src='/Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="Búsqueda Avanzada" ExpandedText="Búsqueda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="Búsqueda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Width="85px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:TemplateField HeaderText="Código" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCodigo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripción" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                            <asp:BoundField DataField="UnidadMedida" ItemStyle-HorizontalAlign="Center" HeaderText="U.M."
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        //
        function valKeyPressDescripcionProd(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        //
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        //
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        //

        function mostrarReporte() {
            var IdEmpresa = parseInt(document.getElementById('<%=ddlEmpresa.ClientID %>').value);
            if (isNaN(IdEmpresa)) { IdEmpresa = 0; }
            if (IdEmpresa == 0) {
                alert('DEBE SELECCIONAR UNA EMPRESA.\nNO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            var IdTienda = parseInt(document.getElementById('<%=ddlTienda.ClientID %>').value);
            var IdAlmacen = parseInt(document.getElementById('<%=ddlAlmacen.ClientID %>').value);
            if (isNaN(IdTienda)) { IdTienda = 0; }
            if (IdTienda > 0) {


            }

            if (isNaN(IdAlmacen)) { IdAlmacen = 0; }

            var IdTipoExistencia = parseInt(document.getElementById('<%=ddlTipoExistencia.ClientID %>').value);
            if (isNaN(IdTipoExistencia)) { IdTipoExistencia = 0; }

            var IdLinea = parseInt(document.getElementById('<%=ddlLinea.ClientID %>').value);
            if (isNaN(IdLinea)) { IdLinea = 0; }

            var IdSubLinea = parseInt(document.getElementById('<%=ddlSubLinea.ClientID %>').value);
            if (isNaN(IdSubLinea)) { IdSubLinea = 0; }

            var FechaInicio = document.getElementById('<%=txtFechaInicio.ClientID %>').value;
            var FechaFin = document.getElementById('<%=txtFechaFin.ClientID %>').value;

            if (compararFechas(FechaInicio, FechaFin)) {
                alert('LA FECHA DE INICIO NO PUEDE SER MENOR A LA DE FIN.');
                return false;
            }

            var IdProducto = parseInt(document.getElementById('<%=hddIdProducto.ClientID %>').value);
            if (isNaN(IdProducto)) { IdProducto = 0; }

            frame1.location.href = 'Visor.aspx?iReporte=7&linea=' + linea + '&sublinea=' + sublinea + '&idproducto=' + idproducto;
            return false;

        }

        function onFocus_RealOnly(othis) {
            othis.readOnly = true;
            return false;
        }

        //
        function compararFechas(fecha, fecha2) {
            var xMonth = fecha.value.substring(3, 5);
            var xDay = fecha.value.substring(0, 2);
            var xYear = fecha.value.substring(6, 10);
            var yMonth = fecha2.value.substring(3, 5);
            var yDay = fecha2.value.substring(0, 2);
            var yYear = fecha2.value.substring(6, 10);

            if (xYear > yYear) {
                return (true)
            }
            else {
                if (xYear == yYear) {
                    if (xMonth > yMonth) {
                        return (true)
                    }
                    else {
                        if (xMonth == yMonth) {
                            if (xDay > yDay)
                                return (true);
                            else
                                return (false);
                        }
                        else
                            return (false);
                    }
                }
                else
                    return (false);
            }
        }
        //

        function limpiarProducto() {

            document.getElementById('<%=hddIdProducto.ClientID %>').value = '0';
            document.getElementById('<%=lblCodigo.ClientID %>').innerText = '';
            document.getElementById('<%=lblDescripcion.ClientID %>').innerText = '';

            return false;
        }
            
    </script>

</asp:Content>
