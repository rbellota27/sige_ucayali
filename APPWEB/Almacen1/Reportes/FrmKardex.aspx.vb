﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmKardex
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            inicializarFrm()
        End If
    End Sub
    Private Sub inicializarFrm()
        Try

            Dim objCombo As New Combo
            With objCombo

                .LlenarCboPropietario(Me.cboEmpresa, True)
                .llenarCboAlmacen(Me.cboAlmacen, True)

                .LlenarCboLinea(Me.cboLinea)
                .LlenarCboSubLineaxIdLinea(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue))
                .LlenarCboProductos(CInt(Me.cboLinea.SelectedValue), CInt(Me.cboSubLinea.SelectedValue), Me.cboProducto)

                Dim fechaActual As Date = (New Negocio.FechaActual).SelectFechaActual
                Me.txtFechaInicio.Text = Format(fechaActual, "dd/MM/yyyy")
                Me.txtFechaFin.Text = Format(fechaActual, "dd/MM/yyyy")

            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged
        Try
            Dim objCombo As New Combo
            With objCombo

                .LlenarCboSubLineaxIdLinea(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue))
                .LlenarCboProductos(CInt(Me.cboLinea.SelectedValue), CInt(Me.cboSubLinea.SelectedValue), Me.cboProducto)

            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubLinea.SelectedIndexChanged
        Try
            Dim objCombo As New Combo
            With objCombo
                .LlenarCboProductos(CInt(Me.cboLinea.SelectedValue), CInt(Me.cboSubLinea.SelectedValue), Me.cboProducto)
            End With
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class