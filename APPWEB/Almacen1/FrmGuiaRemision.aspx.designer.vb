﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmGuiaRemision

    '''<summary>
    '''Control lkb_CapaConfigFrm.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lkb_CapaConfigFrm As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control btnNuevo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnNuevo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnEditar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnEditar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnBuscar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnAnular.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAnular As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnGuardar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnGuardar As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnImprimir1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnImprimir1 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnImprimir.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnImprimir As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnImprimirTonos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnImprimirTonos As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnBuscarDocumentoRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarDocumentoRef As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnImprimirFormato30.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnImprimirFormato30 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control Panel_Cab.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Cab As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control cboEmpresa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboEmpresa As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboTienda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboTienda As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboSerie.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboSerie As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Panel1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel1 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtCodigoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCodigoDocumento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarDocumentoxCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarDocumentoxCodigo As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control btnBusquedaAvanzado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBusquedaAvanzado As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control txtFechaEmision.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaEmision As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtFechaEmision_MaskedEditExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaEmision_MaskedEditExtender As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control txtFechaEmision_CalendarExtender.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaEmision_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control cboEstado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboEstado As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control txtFechaInicioTraslado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaInicioTraslado As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender_txtFechaInicioTraslado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender_txtFechaInicioTraslado As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender_txtFechaInicioTraslado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender_txtFechaInicioTraslado As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control cboAlmacen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboAlmacen As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboTipoOperacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboTipoOperacion As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboMotivoTraslado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboMotivoTraslado As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control imgDocumentoRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents imgDocumentoRef As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''Control cpeDocumentoRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cpeDocumentoRef As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''Control Panel_DocumentoRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_DocumentoRef As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control GV_DocumentoRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_DocumentoRef As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control GV_DocumentoRef_Detalle.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_DocumentoRef_Detalle As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control Panel_Remitente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Remitente As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtNombre_Remitente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNombre_Remitente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtIdRemitente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtIdRemitente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarRemitente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarRemitente As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control txtDNI_Remitente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDNI_Remitente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtRUC_Remitente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRUC_Remitente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel_PuntoPartida.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_PuntoPartida As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtDireccion_Partida.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDireccion_Partida As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control cboDepto_Partida.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboDepto_Partida As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboProvincia_Partida.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboProvincia_Partida As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboDistrito_Partida.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboDistrito_Partida As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Panel_Destinatario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Destinatario As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtDestinatario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDestinatario As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtIdDestinatario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtIdDestinatario As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarDestinatario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarDestinatario As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control cboTipoAlmacen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboTipoAlmacen As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboAlmacen_Destinatario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboAlmacen_Destinatario As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control txtDni_Destinatario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDni_Destinatario As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtRuc_Destinatario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRuc_Destinatario As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel_PuntoLlegada.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_PuntoLlegada As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtDireccion_Llegada.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDireccion_Llegada As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control cboDepto_Llegada.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboDepto_Llegada As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboProvincia_Llegada.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboProvincia_Llegada As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control cboDistrito_Llegada.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboDistrito_Llegada As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Panel_Detalle.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Detalle As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control btnBuscarProducto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarProducto As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control btnLimpiarDetalleDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLimpiarDetalleDocumento As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control LblcantidadRegistros.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents LblcantidadRegistros As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control GV_Detalle.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_Detalle As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control txtPesoTotal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtPesoTotal As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control cboUnidadMedida_PesoTotal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboUnidadMedida_PesoTotal As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Image_Transportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Image_Transportista As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''Control CollapsiblePanelExtender_Transportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CollapsiblePanelExtender_Transportista As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''Control Panel_Transportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Transportista As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtTransportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtTransportista As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarTransportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarTransportista As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnLimpiarTransportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLimpiarTransportista As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control txtDni_Transportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDni_Transportista As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtRuc_Transportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRuc_Transportista As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtDireccion_Transportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDireccion_Transportista As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Image_Chofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Image_Chofer As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''Control CollapsiblePanelExtender_Chofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CollapsiblePanelExtender_Chofer As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''Control Panel_Chofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Chofer As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtChofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtChofer As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarChofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarChofer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnLimpiarChofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLimpiarChofer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control txtDni_Chofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDni_Chofer As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtRuc_Chofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRuc_Chofer As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtNroLicencia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNroLicencia As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Image_Vehiculo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Image_Vehiculo As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''Control CollapsiblePanelExtender_Vehiculo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CollapsiblePanelExtender_Vehiculo As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''Control Panel_Vehiculo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Vehiculo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtModelo_Vehiculo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtModelo_Vehiculo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtPlaca_Vehiculo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtPlaca_Vehiculo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtCertificado_Vehiculo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCertificado_Vehiculo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarVehiculo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarVehiculo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnLimpiarVehiculo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLimpiarVehiculo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control Image_Agente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Image_Agente As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''Control CollapsiblePanelExtender_Agente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CollapsiblePanelExtender_Agente As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''Control Panel_Agente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Agente As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtAgente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtAgente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarAgente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarAgente As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnLimpiarAgencia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLimpiarAgencia As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control txtDni_Agente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDni_Agente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtRuc_Agente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRuc_Agente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control txtDireccion_Agente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDireccion_Agente As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel_Obs.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_Obs As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtObservaciones.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtObservaciones As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control hddIdRemitente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdRemitente As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdDestinatario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdDestinatario As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdTransportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdTransportista As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdChofer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdChofer As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdVehiculo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdVehiculo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdAgente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdAgente As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdDocumento As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddFrmModo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddFrmModo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddCodigoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddCodigoDocumento As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIndex_GV_Detalle.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIndex_GV_Detalle As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddIdTipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIdTipoDocumento As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddOpcionBusquedaPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddOpcionBusquedaPersona As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddOpcionBusquedaAgente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddOpcionBusquedaAgente As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddNroFilasxDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddNroFilasxDocumento As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddConfigurarDatos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddConfigurarDatos As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control HddIdProd.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents HddIdProd As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddPoseeGRecepcion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddPoseeGRecepcion As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control hddNroFilasxGuia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddNroFilasxGuia As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control btnCerrar_capaAddProd.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCerrar_capaAddProd As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control CboTipoExistencia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CboTipoExistencia As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Label17.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label17 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control cmbLinea_AddProd.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmbLinea_AddProd As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Label16.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label16 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control cmbSubLinea_AddProd.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cmbSubLinea_AddProd As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Label19.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label19 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control Panel5.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel5 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtDescripcionProd_AddProd.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDescripcionProd_AddProd As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel6.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel6 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtCodigoProducto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCodigoProducto As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarGrilla_AddProducto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarGrilla_AddProducto As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnAddProductos_AddProducto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAddProductos_AddProducto As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnLimpiar_AddProducto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLimpiar_AddProducto As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control CollapsiblePanelExtender15.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CollapsiblePanelExtender15 As Global.AjaxControlToolkit.CollapsiblePanelExtender

    '''<summary>
    '''Control Image21_11.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Image21_11 As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''Control Label21_11.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label21_11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control Panel_BusqAvanzadoProd.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_BusqAvanzadoProd As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control cboTipoTabla.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboTipoTabla As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control btnAddTipoTabla.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAddTipoTabla As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnLimpiar_BA.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLimpiar_BA As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control GV_FiltroTipoTabla.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_FiltroTipoTabla As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control Panel7.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel7 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control DGV_AddProd.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DGV_AddProd As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btnAnterior_Productos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAnterior_Productos As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnPosterior_Productos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnPosterior_Productos As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control txtPageIndex_Productos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtPageIndex_Productos As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnIr_Productos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnIr_Productos As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control txtPageIndexGO_Productos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtPageIndexGO_Productos As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnCerrar_Capa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCerrar_Capa As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control pnlBusquedaPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlBusquedaPersona As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control rdbTipoPersona.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rdbTipoPersona As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Control Panel2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel2 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control tbRazonApe.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbRazonApe As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel3 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control tbbuscarDni.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbbuscarDni As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel4 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control tbbuscarRuc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbbuscarRuc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btBuscarPersonaGrillas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btBuscarPersonaGrillas As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control Ddl_Rol.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Ddl_Rol As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control gvBuscar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents gvBuscar As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btAnterior.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btAnterior As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btSiguiente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btSiguiente As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tbPageIndex.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbPageIndex As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btIr.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btIr As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control tbPageIndexGO.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents tbPageIndexGO As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control ImageButton7.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton7 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control txtGlosa_CapaGlosa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtGlosa_CapaGlosa As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control hddIndexGlosa_Gv_Detalle.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hddIndexGlosa_Gv_Detalle As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control btnAddGlosa_CapaGlosa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAddGlosa_CapaGlosa As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control btnCerrar_capaDocumentosReferencia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCerrar_capaDocumentosReferencia As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control Panel_BusquedaAvanzado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_BusquedaAvanzado As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtFechaI.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaI As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender2 As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender1 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control txtFechaF.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaF As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender3 As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender2 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control btnAceptarBusquedaAvanzado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAceptarBusquedaAvanzado As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control GV_BusquedaAvanzado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_BusquedaAvanzado As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control ImageButton6.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton6 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control CboTipoDocumento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CboTipoDocumento As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control rdbBuscarDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rdbBuscarDocRef As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Control Panel_BuscarDocRefxFecha.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_BuscarDocRefxFecha As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtFechaInicio_DocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaInicio_DocRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender4 As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender3 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control txtFechaFin_DocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaFin_DocRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control MaskedEditExtender5.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MaskedEditExtender5 As Global.AjaxControlToolkit.MaskedEditExtender

    '''<summary>
    '''Control CalendarExtender4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CalendarExtender4 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''Control btnAceptarBuscarDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAceptarBuscarDocRef As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control Panel_BuscarDocRefxCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_BuscarDocRefxCodigo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control Panel9.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel9 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtSerie_BuscarDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtSerie_BuscarDocRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Panel10.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel10 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtCodigo_BuscarDocRef.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCodigo_BuscarDocRef As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnAceptarBuscarDocRefxCodigo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAceptarBuscarDocRefxCodigo As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control Panel_DocRef_Find.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel_DocRef_Find As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control GV_DocumentosReferencia_Find.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_DocumentosReferencia_Find As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control ImageButton1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton1 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control lblProductoConsultarStockPrecioxProd.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblProductoConsultarStockPrecioxProd As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control GV_ConsultarStockPrecioxProd.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_ConsultarStockPrecioxProd As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control btnAddProducto_ConsultarStockPrecio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAddProducto_ConsultarStockPrecio As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control ImageButton5.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton5 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control Panel8.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Panel8 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control txtNroPlaca_BuscarVehiculo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNroPlaca_BuscarVehiculo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control btnBuscarVehiculo_Grilla.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBuscarVehiculo_Grilla As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control GV_Vehiculo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_Vehiculo As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control ImageButton9.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton9 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control chb_MoverAlmacen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents chb_MoverAlmacen As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''Control ImageButton2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ImageButton2 As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Control txtCantidad_Ingreso.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCantidad_Ingreso As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control cboUnidadMedida_Ingreso.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboUnidadMedida_Ingreso As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control btnCalcular.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCalcular As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control txtCantidad_Salida.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCantidad_Salida As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control cboUnidadMedida_Salida.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboUnidadMedida_Salida As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control GV_CalcularEquivalencia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_CalcularEquivalencia As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control GV_ResuldoEQ.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GV_ResuldoEQ As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control rbl_UtilizarRedondeo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents rbl_UtilizarRedondeo As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''Control btnAceptar_EquivalenciaPR.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAceptar_EquivalenciaPR As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control btnCerrar_EquivalenciaPR.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCerrar_EquivalenciaPR As Global.System.Web.UI.WebControls.Button
End Class
