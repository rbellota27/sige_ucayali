﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System
Imports System.Data
Imports System.IO
Partial Public Class FrmAprobacionOP
    Inherits System.Web.UI.Page
    Private objScript As New ScriptManagerClass
    Private objCombo As New Combo
    Private objNegUsu As New Negocio.UsuarioView
    Private objAOP As New Negocio.DocumentoOrdenPedidoSucursal
    Private objFechaActual As New Negocio.FechaActual
    'Private listaDocumentoRef As List(Of Entidades.Documento)
    'Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    'Private objSLTT As Entidades.SubLinea_TipoTabla
    'Private ListaTTV As List(Of Entidades.TipoTablaValor)
    'Private ObjTTV As Entidades.TipoTablaValor
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            inicializarFrm()
            ValidarPermisos()
            inicializarFrm2()
            ocultaFiltrosRpt(False)
            ' GV_DocumentoOP.Attributes.Add("OnSelectedIndexChanged", "habilitarFrm(False);")
        End If
        
    End Sub
    Private Sub ValidarPermisos()
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {204})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnGuardar.Enabled = True

        Else
            Me.btnGuardar.Enabled = False

        End If

    End Sub
    Private Sub inicializarFrm()

        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboPropietario(Me.cboEmpresa, False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

                '***************  PROGRAMACION SEMANA
                .LlenarCboYear(Me.cboAnio, False)
                .LlenarCboSemanaxIdYear(Me.cboSemana, CInt(Me.cboAnio.SelectedValue), False)

            End With

            Me.txtFechaInicio.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
            Me.txtFechaFin.Text = Me.txtFechaInicio.Text

            limpiarGrillas()

            actualizarProgramacionSemana((New Negocio.FechaActual).SelectFechaActual)


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub inicializarFrm2()
        Try
            'btnMostrarReporte.Visible = True

            'este cod es para llenar por permiso
            'If objNegUsu.FunVerTodasTiendas(CInt(Session("IdUsuario")), 8) > 0 Then
            '    objCombo.LlenarCboTiendaxIdUsuario(Me.cmbTienda, CInt(Session("IdUsuario")), True)
            'Else
            '    objCombo.LlenarCboTiendaxIdUsuario(Me.cmbTienda, CInt(Session("IdUsuario")), False)
            'End If

            objCombo.LlenarCboYear(Me.idYearI, False)
            objCombo.LlenarCboSemanaxIdYear(Me.idSemanaI, CInt(idYearI.SelectedValue), False)
            objCombo.LlenarCboYear(Me.idYearF, False)
            objCombo.LlenarCboSemanaxIdYear(Me.idSemanaF, CInt(idYearF.SelectedValue), False)

            actualizarProgramacionSemana((New Negocio.FechaActual).SelectFechaActual)



            objCombo.LlenarCboTipoAlmacen(Me.cboTipoAlmacen, False)

            Dim lista As New List(Of Entidades.TipoAlmacen)
            Dim obj As New Entidades.TipoAlmacen(1, CStr(Me.cboTipoAlmacen.SelectedItem.Text))
            lista.Add(obj)
            gvwTipoAlmacen.DataSource = lista
            gvwTipoAlmacen.DataBind()
            setListaTipoAlmacenView(lista)


            txtFechaInicio1.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            txtFechaFin1.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            'Me.hddFechaActual.Value = CStr(Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy"))

            llenarcboSemanaActual(Me.idYearI, idSemanaI, Me.txtFechaInicioSemanaI, Me.txtFechaFinSemanaI)
            llenarcboSemanaActual(Me.idYearF, idSemanaF, Me.txtFechaInicioSemanaFin, Me.txtFechaFinSemanafin)

            actualizaControlesFiltros((New Negocio.FechaActual).SelectFechaActual, idYearI, idSemanaI, txtFechaInicioSemanaI, txtFechaFinSemanaI)
            actualizaControlesFiltros((New Negocio.FechaActual).SelectFechaActual, idYearF, idSemanaF, txtFechaInicioSemanaFin, txtFechaFinSemanafin)

            ocultaCheck(False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la carga de datos.")
        End Try
    End Sub
    Private Sub actualizaControlesFiltros(ByVal Fecha As Date, ByVal cboAnoo As DropDownList, ByVal cbosemanaaa As DropDownList, ByVal txtInicio As TextBox, ByVal txtFin As TextBox)

        Dim objCbo As New Combo
        Dim objProgramacionSemana As Entidades.ProgramacionPedido = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectxFecha(Fecha)

        If (objProgramacionSemana IsNot Nothing) Then
            If (cboAnoo.Items.FindByValue(CStr(objProgramacionSemana.IdYear)) IsNot Nothing) Then
                cboAnoo.SelectedValue = CStr(objProgramacionSemana.IdYear)
                objCbo.LlenarCboSemanaxIdYear(cbosemanaaa, CInt(cboAnoo.SelectedValue), False)
            End If

            If (cbosemanaaa.Items.FindByValue(CStr(objProgramacionSemana.IdSemana)) IsNot Nothing) Then
                cbosemanaaa.SelectedValue = CStr(objProgramacionSemana.IdSemana)
            End If

            txtInicio.Text = Format(objProgramacionSemana.cal_FechaIni, "dd/MM/yyyy")
            txtFin.Text = Format(objProgramacionSemana.cal_FechaFin, "dd/MM/yyyy")

        End If

    End Sub
    Private Sub actualizarProgramacionSemana(ByVal Fecha As Date)

        Dim objCbo As New Combo
        Dim objProgramacionSemana As Entidades.ProgramacionPedido = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectxFecha(Fecha)

        If (objProgramacionSemana IsNot Nothing) Then
            If (Me.cboAnio.Items.FindByValue(CStr(objProgramacionSemana.IdYear)) IsNot Nothing) Then
                Me.cboAnio.SelectedValue = CStr(objProgramacionSemana.IdYear)
                objCbo.LlenarCboSemanaxIdYear(Me.cboSemana, CInt(Me.cboAnio.SelectedValue), False)
            End If

            If (Me.cboSemana.Items.FindByValue(CStr(objProgramacionSemana.IdSemana)) IsNot Nothing) Then
                Me.cboSemana.SelectedValue = CStr(objProgramacionSemana.IdSemana)
            End If

            Me.txtFechaInicio_ProgSemana.Text = Format(objProgramacionSemana.cal_FechaIni, "dd/MM/yyyy")
            Me.txtFechaFin_ProgSemana.Text = Format(objProgramacionSemana.cal_FechaFin, "dd/MM/yyyy")
        End If

    End Sub
    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            End With

            limpiarGrillas()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTienda.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            With objCbo

                .llenarCboAlmacenxIdTienda(Me.cboAlmacen, CInt(Me.cboTienda.SelectedValue), False)

            End With

            limpiarGrillas()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cboAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
        Try

            limpiarGrillas()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub limpiarGrillas()

        Me.GV_Detalle.DataSource = Nothing
        Me.GV_Detalle.DataBind()

        Me.GV_DocumentoOP.DataSource = Nothing
        Me.GV_DocumentoOP.DataBind()

        Me.GV_DocumentoRef.DataSource = Nothing
        Me.GV_DocumentoRef.DataBind()

        Me.GV_DocumentoRef_Cab.DataSource = Nothing
        Me.GV_DocumentoRef_Cab.DataBind()

        Me.GV_DocumentoRef_Det.DataSource = Nothing
        Me.GV_DocumentoRef_Det.DataBind()

    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click

        buscarDocumentoOrdenPedido()
        'If Me.GV_DocumentoOP.Rows.Count > 1 Then
        ocultaFiltrosRpt(False)
        Me.gvwAproped.DataSource = Nothing
        gvwAproped.DataBind()

        ocultaCheck(False)

        'Else
        '    ocultaFiltrosRpt(True)
        'End If

    End Sub

    Private Sub buscarDocumentoOrdenPedido()
        Try

            limpiarGrillas()

            Dim fechaInicio As Date = Nothing
            Dim fechaFin As Date = Nothing
            Dim serie As Integer = 0
            Dim codigo As Integer = 0

            If (Me.rdbFiltroFecha.SelectedValue = "0") Then
                fechaInicio = CDate(Me.txtFechaInicio.Text)
                fechaFin = CDate(Me.txtFechaFin.Text)
            ElseIf (Me.rdbFiltroFecha.SelectedValue = "1") Then
                fechaInicio = CDate(Me.txtFechaInicio_ProgSemana.Text)
                fechaFin = CDate(Me.txtFechaFin_ProgSemana.Text)
            ElseIf (Me.rdbFiltroFecha.SelectedValue = "2") Then
                serie = CInt(Me.txtSerie.Text)
                codigo = CInt(Me.txtCodigo.Text)
            Else
                Throw New Exception("NO SE HALLÓ LA OPCIÓN DE BÚSQUEDA SOLICITADA. NO SE PERMITE LA OPERACIÓN.")

            End If



            Me.GV_DocumentoOP.DataSource = (New Negocio.DocumentoOrdenPedidoSucursal).DocumentoOrdenPedido_SelectxParams_Aprobacion(CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboAlmacen.SelectedValue), fechaInicio, fechaFin, CBool(Me.cboEstadoAprobacion.SelectedValue), serie, codigo)
            Me.GV_DocumentoOP.DataBind()

            If (Me.GV_DocumentoOP.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub GV_DocumentoOP_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_DocumentoOP.SelectedIndexChanged
        cargarDatosDocumentoOP(CInt(CType(Me.GV_DocumentoOP.SelectedRow.FindControl("hddIdDocumento"), HiddenField).Value))
        ocultaFiltrosRpt(True)

        'postSincrono(GV_DocumentoOP)
        TituloRpt()
        Me.gvwTipoAlmacen.DataSource = getListaTipoAlmacenView()
        gvwTipoAlmacen.DataBind()

        
        'Try
        '    If e.Row.RowType = DataControlRowType.DataRow And e.Row.RowState = DataControlRowState.Normal Or _
        '    e.Row.RowState = DataControlRowState.Alternate Or e.Row.RowState = DataControlRowState.Selected Then
        '        Dim sm As ScriptManager
        '        sm = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
        '        sm.RegisterPostBackControl(DirectCast(e.Row.FindControl("lbtnEditar"), LinkButton))
        '    End If
        'Catch ex As Exception
        '    objScript.mostrarMsjAlerta(Me, ex.Message)
        'End Try

        Me.gvwAproped.DataSource = Nothing
        gvwAproped.DataBind()
        ocultaCheck(False)

    End Sub
    Public Sub postSincrono(ByVal control As Control)

        Dim sm As ScriptManager
        sm = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
        sm.RegisterPostBackControl(control)

    End Sub
    Private Sub cargarDatosDocumentoOP(ByVal IdDocumento As Integer)
        Try

            Me.GV_DocumentoRef.DataSource = (New Negocio.RelacionDocumento).RelacionDocumento_SelectDocumentoRefxIdDocumento2(IdDocumento)
            Me.GV_DocumentoRef.DataBind()

            Me.GV_Detalle.DataSource = (New Negocio.DocumentoOrdenPedidoSucursal).DocumentoOrdenPedidoSucursal_SelectDetalle_Aprobacion_DT(IdDocumento)
            Me.GV_Detalle.DataBind()



        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub registrarAprobacion()
        Try

            Dim listaCantidadAprobada As List(Of Entidades.Anexo_DetalleDocumento) = obtenerListaAnexoDetalleDocumento()
            Dim IdDocumento As Integer = CInt(CType(Me.GV_DocumentoOP.SelectedRow.FindControl("hddIdDocumento"), HiddenField).Value)

            If ((New Negocio.DocumentoOrdenPedidoSucursal).aprobarCantidadesOP(IdDocumento, listaCantidadAprobada)) Then
                limpiarGrillas()
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            Else
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaAnexoDetalleDocumento() As List(Of Entidades.Anexo_DetalleDocumento)

        Dim lista As New List(Of Entidades.Anexo_DetalleDocumento)

        For i As Integer = 0 To Me.GV_Detalle.Rows.Count - 1

            Dim cantidadAprobada As Decimal = CDec(CType(Me.GV_Detalle.Rows(i).FindControl("txtCantidadAprobar"), Label).Text)

            If (cantidadAprobada > 0) Then

                Dim obj As New Entidades.Anexo_DetalleDocumento

                With obj
                    .IdDetalleDocumento = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("hddIdDetalleDocumento"), HiddenField).Value)
                    .CantidadAprobada = cantidadAprobada
                    .IdDocumento = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value)
                End With

                lista.Add(obj)

            End If

        Next

        Return lista

    End Function

    Private Sub cboAnio_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAnio.SelectedIndexChanged

        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboSemanaxIdYear(Me.cboSemana, CInt(Me.cboAnio.SelectedValue), False)

            Dim objProgramacionPedido As Entidades.ProgramacionPedido = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectxIdYearxIdSemana(CInt(Me.cboAnio.SelectedValue), CInt(Me.cboSemana.SelectedValue))

            If (objProgramacionPedido IsNot Nothing) Then
                Me.txtFechaInicio_ProgSemana.Text = Format(objProgramacionPedido.cal_FechaIni, "dd/MM/yyyy")
                Me.txtFechaFin_ProgSemana.Text = Format(objProgramacionPedido.cal_FechaFin, "dd/MM/yyyy")
            End If



        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub cboSemana_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSemana.SelectedIndexChanged

        Try

            Dim objProgramacionPedido As Entidades.ProgramacionPedido = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectxIdYearxIdSemana(CInt(Me.cboAnio.SelectedValue), CInt(Me.cboSemana.SelectedValue))

            If (objProgramacionPedido IsNot Nothing) Then
                Me.txtFechaInicio_ProgSemana.Text = Format(objProgramacionPedido.cal_FechaIni, "dd/MM/yyyy")
                Me.txtFechaFin_ProgSemana.Text = Format(objProgramacionPedido.cal_FechaFin, "dd/MM/yyyy")
            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub GV_DocumentoRef_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_DocumentoRef.SelectedIndexChanged
        cargarDatos_DocumentoRef(CInt(CType(Me.GV_DocumentoRef.SelectedRow.FindControl("hddIdDocumento"), HiddenField).Value))
        hdfIdDocumento.Value = GV_DocumentoRef.SelectedRow.FindControl("hddIdDocumento").ToString

    End Sub
    Private Sub cargarDatos_DocumentoRef(ByVal IdDocumentoRef As Integer)
        Try

            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectxIdDocumento(IdDocumentoRef)

            If (objDocumento Is Nothing) Then
                Throw New Exception("NO SE HALLARON REGISTROS.")
            End If

            Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdPersona)
            If (objPersona IsNot Nothing) Then

                objDocumento.NomAlmacen = objPersona.Descripcion
                objDocumento.NomCondicionPago = objPersona.Ruc
                objDocumento.NomEmpleado = objPersona.Dni
                objDocumento.NomEmpresaTomaInv = objPersona.TipoPV

            End If


            Dim listaDocumento As New List(Of Entidades.Documento)
            listaDocumento.Add(objDocumento)

            Dim listaDetalleDocumento As List(Of Entidades.DetalleDocumento) = (New Negocio.DetalleDocumento).SelectxIdDocumento(IdDocumentoRef)

            Me.GV_DocumentoRef_Cab.DataSource = listaDocumento
            Me.GV_DocumentoRef_Cab.DataBind()

            Me.GV_DocumentoRef_Det.DataSource = listaDetalleDocumento
            Me.GV_DocumentoRef_Det.DataBind()


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#Region "TipoAlmacen"
    Private Sub setListaTipoAlmacenView(ByVal lista As List(Of Entidades.TipoAlmacen))
        Session.Remove("TipoAlmacen")
        Session.Add("TipoAlmacen", lista)
    End Sub
    Private Function getListaTipoAlmacenView() As List(Of Entidades.TipoAlmacen)
        Return CType(Session.Item("TipoAlmacen"), List(Of Entidades.TipoAlmacen))
    End Function
    Private Sub addTipoAlmacenGrilla()
        Try
            Dim lista As List(Of Entidades.TipoAlmacen) = ObtenerListaTipoaAlmacenFrmGrilla()
            'agrega un registro a la lista (captura los valores y los asigna a la lista por medio del constructor y la lista add a la grilla)
            Dim dse As String = CStr(Me.cboTipoAlmacen.SelectedItem.Text)
            If ValidaRepetido(CInt(Me.cboTipoAlmacen.SelectedValue), lista) = False Then
                Dim obj As New Entidades.TipoAlmacen(CInt(Me.cboTipoAlmacen.SelectedValue), CStr(Me.cboTipoAlmacen.SelectedItem.Text))
                lista.Add(obj)
                gvwTipoAlmacen.DataSource = lista
                gvwTipoAlmacen.DataBind()

                setListaTipoAlmacenView(lista)

            Else
                objScript.mostrarMsjAlerta(Me, "El tipo de almacen ya fue Selecionado.")
            End If

        Catch ex As Exception
        Finally
        End Try
    End Sub
    Private Function ValidaRepetido(ByVal idtipoalmacen As Integer, ByVal tiposAlmacen As List(Of Entidades.TipoAlmacen)) As Boolean
        Dim rep As Boolean = False
        For Each tipoalm As Entidades.TipoAlmacen In tiposAlmacen
            If tipoalm.IdTipoAlmacen = idtipoalmacen Then
                rep = True
            End If
        Next
        Return rep
    End Function

    Private Function ObtenerListaTipoaAlmacenFrmGrilla() As List(Of Entidades.TipoAlmacen)
        Dim lista As New List(Of Entidades.TipoAlmacen)

        For i As Integer = 0 To gvwTipoAlmacen.Rows.Count - 1
            With gvwTipoAlmacen.Rows(i)

                Dim hdf As HiddenField = (CType(.Cells(0).FindControl("hdfIdTipoAlmacen"), HiddenField))

                'para cada fila utiliza el constructor para asignarle los valores de la grilla ala lista
                Dim obj As New Entidades.TipoAlmacen(CInt(hdf.Value), CStr(.Cells(1).Text))

                ' add a obj IdCuentaPersona  sin contructor 
                'obj.IdCuentaPersona = CInt(gvwTipoAlmacen.Rows(i).Cells(7).Text)

                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim lbtnQuitar As System.Web.UI.WebControls.LinkButton = CType(sender, LinkButton)
        Dim fila As GridViewRow
        fila = CType(lbtnQuitar.NamingContainer, GridViewRow)
        Me.quitarRegistroTipoAlmacen(fila.Cells(0).Text, fila.RowIndex)

    End Sub


    Private Sub quitarRegistroTipoAlmacen(ByVal cuenta As String, ByVal DelFila As Integer)
        Dim objUtil As New Negocio.Util
        Try
            '**************** quitamos de la lista
            Dim lista As List(Of Entidades.TipoAlmacen) = ObtenerListaTipoaAlmacenFrmGrilla()
            lista.RemoveAt(DelFila)
            gvwTipoAlmacen.DataSource = lista
            gvwTipoAlmacen.DataBind()
            setListaTipoAlmacenView(lista)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "Problemas en la eliminación del registro.")
        End Try
    End Sub

    Private Sub btnAddTipoAlmacen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddTipoAlmacen.Click
        addTipoAlmacenGrilla()
    End Sub
#End Region


#Region "Reporte"
    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        'registrarAprobacion()
        setGrillaPedAprob(Me.gvwAproped)
        registrarAprobacionAna()
        cargarDatosDocumentoOP(CInt(CType(Me.GV_DocumentoOP.SelectedRow.FindControl("hddIdDocumento"), HiddenField).Value))

    End Sub
    Private Sub setGrillaPedAprob(ByVal grilla As GridView)
        Session.Remove("rptpedAprob")
        Session.Add("rptpedAprob", grilla)

    End Sub
    Private Function getGrillaPedAprob() As GridView
        Return CType(Session.Item("rptpedAprob"), GridView)
    End Function


    Private Sub btnMostrarReporte_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMostrarReporte.Click
        llenarGrilla()
    End Sub
    Private Sub registrarAprobacionAna()
        Try

            Dim listaCantidadAprobada As List(Of Entidades.Anexo_DetalleDocumento) = obtenerListaAnexoDetalleDocumentoAna()
            Dim IdDocumento As Integer = CInt(CType(Me.GV_DocumentoOP.SelectedRow.FindControl("hddIdDocumento"), HiddenField).Value)

            If ((New Negocio.DocumentoOrdenPedidoSucursal).aprobarCantidadesOP(IdDocumento, listaCantidadAprobada)) Then
                'limpiarGrillas()
                objScript.mostrarMsjAlerta(Me, "El proceso finalizó con éxito.")
            Else
                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaAnexoDetalleDocumentoAna() As List(Of Entidades.Anexo_DetalleDocumento)
        Dim lista As New List(Of Entidades.Anexo_DetalleDocumento)

        Dim grilla As GridView = getGrillaPedAprob()
        Dim col As Integer = grilla.Columns.Count

        Dim cantidadAprob As Decimal

        Dim cadena As String
        cadena = Me.hdfConvPed.Value
        Dim cad() As String
        Dim cad1() As String
        cad = Split(cadena, ";", -1)
        Dim j As Int32
        Dim ord As Integer

        For i As Integer = 0 To grilla.Rows.Count - 1
            'Dim idproducto As Integer = CInt(grilla.Rows(i).Cells(2).Text)
            ord = CInt(grilla.Rows(i).Cells(2).Text)
            j = i + 1
            cad1 = Split(CStr(cad(j)), ",", -1)
            cantidadAprob = CDec(cad1(1).ToString)
            If (cantidadAprob > 0) Then
                Dim obj As New Entidades.Anexo_DetalleDocumento
                With obj
                    .IdDetalleDocumento = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("hddIdDetalleDocumento"), HiddenField).Value)
                    .CantidadAprobada = cantidadAprob
                    .IdDocumento = CInt(CType(Me.GV_Detalle.Rows(i).FindControl("hddIdDocumento"), HiddenField).Value)
                End With
                lista.Add(obj)
            End If
        Next
        Return lista

    End Function

    Private Sub ocultaFiltrosRpt(ByVal v As Boolean)
        btnAddTipoAlmacen.Visible = v

        Me.checkGuardaCopiaPed.Visible = v
        cboTipoAlmacen.Visible = v
        rbtFechaOSemana.Visible = v
        idYearI.Visible = v
        idSemanaI.Visible = v
        idYearF.Visible = v
        idSemanaF.Visible = v

        If v = True Then
            txtFechaInicio1.Visible = v
            txtFechaFin1.Visible = v
            txtFechaInicio1.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
            txtFechaFin1.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
        Else
            txtFechaInicio1.Visible = v
            txtFechaFin1.Visible = v
        End If
        chbStockxTienda.Visible = v


        btnMostrarReporte.Visible = v
        btnGuardar.Visible = v
        btnExpToExcel.Visible = v

        Me.lblanof.Visible = v
        lblanoi.Visible = v
        lblDe.Visible = v
        lblFechaf.Visible = v
        lblFechaI.Visible = v
        lblHasta.Visible = v
        lblSemanaf.Visible = v
        lblSemanai.Visible = v
        lblTipoAlmacen.Visible = v
        gvwTipoAlmacen.Visible = v
        gvwAproped.Visible = v
        txtFechaInicioSemanaI.Visible = v
        txtFechaFinSemanaI.Visible = v
        txtFechaInicioSemanaFin.Visible = v
        txtFechaFinSemanafin.Visible = v

        Me.lblFechaFinSemanaFin.Visible = v
        Me.lblFechaFinSemanaI.Visible = v
        Me.lblFechaInicioSemanaFin.Visible = v
        Me.lblFechaInicioSemanaI.Visible = v



    End Sub
    Public Sub DesabilitarGrillas()
        Me.gvwTipoAlmacen.DataSource = Nothing
        gvwTipoAlmacen.DataBind()

        Me.gvwAproped.DataSource = Nothing
        gvwAproped.DataBind()
    End Sub

    Private Sub llenarGrilla()

        Dim ds As New DataSet
        'Dim ltTAlmacen As New List(Of Entidades.TipoAlmacen)
        'ltTAlmacen = getListaTipoAlmacenView()
        Dim tabla As New DataTable
        Dim tb As New DataTable
        tb.Columns.Add("idproducto", GetType(System.Int64))

        For Each objtAlmacen As Entidades.TipoAlmacen In getListaTipoAlmacenView()
            Dim dr As DataRow = tb.NewRow()
            dr(0) = objtAlmacen.IdTipoAlmacen
            tb.Rows.Add(dr)
        Next
        Dim almacenDA, idempresa, idtienda, iddocumento, yeari, semanaI, yearf, semanaF, fechaSemana, StockxTienda As Integer
        idempresa = CInt(Me.cboEmpresa.SelectedValue)
        idtienda = CInt(Me.cboTienda.SelectedValue)
        yeari = CInt(idYearI.SelectedValue)
        semanaI = CInt(idSemanaI.SelectedValue)
        yearf = CInt(Me.idYearF.SelectedValue)
        semanaF = CInt(Me.idSemanaF.SelectedValue)
        fechaSemana = CInt(Me.rbtFechaOSemana.SelectedValue)
        almacenDA = CInt(Me.cboAlmacen.SelectedValue)
        iddocumento = CInt(CType(Me.GV_DocumentoOP.SelectedRow.FindControl("hddIdDocumento"), HiddenField).Value)


        Dim fechainicio, fechafin As String
        fechainicio = Me.txtFechaInicio1.Text
        fechafin = Me.txtFechaFin1.Text
        If chbStockxTienda.Checked Then
            StockxTienda = 1
        Else
            StockxTienda = 0
        End If

        Try
            'tabla = objAOP.ReporteAprobacionPedido(idempresa, idtienda, almacenDA, iddocumento, fechainicio, fechafin, yeari, semanaI, yearf, semanaF, fechaSemana, StockxTienda, tb).Tables(0)
            tabla = objAOP.OrdenPedidoAprobacion(idempresa, iddocumento, fechainicio, fechafin, yeari, semanaI, yearf, semanaF, fechaSemana, tb).Tables(0)

            If tabla.Columns.Count > 4 Then
                If tabla.Rows.Count > 0 Then
                    llenarGrillaProd_E(Me.gvwAproped, tabla)
                    Me.gvwAproped.DataSource = tabla
                    gvwAproped.DataBind()
                    Dim txt As TextBox
                    For Each row As GridViewRow In gvwAproped.Rows
                        'buscamos las cajas de texto de cada fila de la grilla.
                        txt = CType(row.Cells(CInt(hdfNroOp.Value)).Controls(0), TextBox)
                        txt.Attributes.Add("onKeyUp", "aprobPedConv();")
                        txt.Attributes.Add("OnKeyPress", "return AcceptNum(event)")

                    Next
                End If
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", " aprobPedConv();", True)
            Else
                objScript.mostrarMsjAlerta(Me, "No hay Datos")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, "No hay Datos")
        End Try


    End Sub

    Public Sub SiguienteCtrl()

        For Each Con As Control In Page.FindControl("FrmAprobacionOP").Controls
            If (TypeOf Con Is TextBox) Then
                CType(Con, TextBox).Attributes.Add("onKeyDown", "SiguienteObjeto();")
            ElseIf (TypeOf Con Is DropDownList) Then
                CType(Con, DropDownList).Attributes.Add("onKeyDown", "SiguienteObjeto();")
            ElseIf (TypeOf Con Is RadioButtonList) Then
                CType(Con, RadioButtonList).Attributes.Add("onKeyDown", "SiguienteObjeto();")
                'ElseIf (TypeOf Con Is CheckBox) Then ' para los CheckBox
                ' CType(Con, CheckBox).Attributes.Add("onKeyDown", "SiguienteObjeto();")
                'ElseIf (TypeOf Con Is CheckBoxList) Then 'para los CheckBoxList
                ' CType(Con, CheckBoxList).Attributes.Add("onKeyDown", "SiguienteObjeto();")
                ' si hay controles de otro tipo solo agregar como los ejemplos anteriores
            End If
        Next

    End Sub

    Public Sub llenarGrillaProd_E(ByVal objGridView As GridView, ByVal tab As DataTable)
        objGridView.Columns.Clear()
        For nrocol As Integer = 0 To tab.Columns.Count - 1
            If tab.Columns.Item(nrocol).ColumnName = "PedidoAprobado" Then
                agregarColumnaGrillaTextbox(objGridView, tab.Columns.Item(nrocol).ColumnName, True, tab.Columns.Item(nrocol).ColumnName)
                hdfNroOp.Value = CStr(nrocol)
            ElseIf tab.Columns.Item(nrocol).ColumnName = "idproducto" Then
                agregarColumnaGrilla(objGridView, tab.Columns.Item(nrocol).ColumnName, True, tab.Columns.Item(nrocol).ColumnName)
            Else
                agregarColumnaGrilla(objGridView, tab.Columns.Item(nrocol).ColumnName, True, tab.Columns.Item(nrocol).ColumnName)
            End If
        Next


    End Sub
    Private Sub agregarColumnaGrilla(ByVal objGridView As GridView, ByVal headerText As String, ByVal isVisible As Boolean, ByVal dataField As String)
        Dim column As New BoundField
        column.HeaderText = headerText
        column.Visible = isVisible
        column.DataField = dataField
        objGridView.Columns.Add(column)
    End Sub

    Private Sub agregarColumnaGrillaCheckBox(ByVal objGridView As GridView, ByVal headerText As String, ByVal isVisible As Boolean, ByVal dataField As String)

        Dim column As New CheckBoxField
        column.HeaderText = headerText
        column.Visible = isVisible
        column.DataField = dataField
        objGridView.Columns.Add(column)
    End Sub
    Private Sub agregarColumnaGrillaTextbox(ByVal objGridView As GridView, ByVal headerText As String, ByVal isVisible As Boolean, ByVal dataField As String)
        Dim ctrl As TextBox = New TextBox
        ctrl.ID = "txtPedAprod"
        ctrl.Text = "0"
        Dim col As New TemplateField
        col.HeaderText = headerText
        col.Initialize(True, ctrl)
        col.Visible = isVisible
        col.ItemTemplate = New GridViewTemplate(ListItemType.Item, headerText)
        objGridView.Columns.Add(col)
        'objGridView.TemplateControl.Controls.Add(CType(col.ItemTemplate, Control))

        'Dim tpcol As TemplateColumn = New TemplateColumn
        'tpcol.HeaderText = headerText
        'tpcol.Visible = isVisible
        'tpcol.ItemTemplate = New GridViewTemplate(ListItemType.Item, headerText)

        'For j As Integer = 0 To nrofilas
        '    'tpcol.ItemTemplate = New gridtemplate
        'Next

        'objGridView.Columns.Add(col)
        'objGridView.TemplateControl.Controls.Add(ctrl)

    End Sub
    Private Sub gvwAproped_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwAproped.RowCreated
        'gvwAproped.Columns(CInt(hdfNroOp.Value)).Visible = True
    End Sub

#End Region
    Public Sub TituloRpt()
        lblTituloRpte.Text = "Empresa:" & GV_DocumentoOP.SelectedRow.Cells(4).Text & "  Tienda: " & GV_DocumentoOP.SelectedRow.Cells(5).Text & " Documento:" & (CStr(CType(Me.GV_DocumentoOP.SelectedRow.Cells(2).FindControl("lblNroDocumento"), Label).Text) & " Semana: " & cboSemana.SelectedItem.Text & " Del: " & txtFechaInicio_ProgSemana.Text & " Hasta: " & txtFechaFin_ProgSemana.Text)
    End Sub


    Private Sub ExportGrid(ByVal grilla As GridView, ByVal FilleNameExt As String)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + FilleNameExt)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"

        Response.ContentEncoding = System.Text.Encoding.UTF8

        Dim strWriter As System.IO.StringWriter
        strWriter = New System.IO.StringWriter
        Dim htmlTextWriter As System.Web.UI.HtmlTextWriter
        htmlTextWriter = New System.Web.UI.HtmlTextWriter(strWriter)

        Dim grillaTitulo As New GridView
        grillaTitulo.AutoGenerateColumns = False

        agregarColumnaGrilla(grillaTitulo, "", True, "ventas")
        agregarColumnaGrilla(grillaTitulo, "", True, "Stock")
        agregarColumnaGrilla(grillaTitulo, "", True, "Almacen")
        agregarColumnaGrilla(grillaTitulo, "VENTAS POR TIENDA Y STOCK POR ALMACEN", True, "Titulo")


        Dim tabla As New DataTable
        Dim col1 As New DataColumn
        Dim col2 As New DataColumn
        Dim col3 As New DataColumn
        Dim col4 As New DataColumn


        col1.ColumnName = "Ventas"
        col1.Caption = "Ventas Por Tienda y Stock por Almacen"

        col2.ColumnName = "Stock"
        col2.Caption = ""

        col3.ColumnName = "Almacen"
        col3.Caption = ""

        col4.ColumnName = "Titulo"
        col4.Caption = ""


        With tabla.Columns
            .Add(col1)
            .Add(col2)
            .Add(col3)
            .Add(col4)

        End With

        Dim dr As DataRow
        Dim dr1 As DataRow
        Dim dr2 As DataRow
        Dim dr3 As DataRow

        dr = tabla.NewRow
        dr.Item(0) = ""
        dr.Item(1) = ""
        dr.Item(2) = GV_DocumentoOP.SelectedRow.Cells(1).Text
        dr.Item(3) = CStr(CType(Me.GV_DocumentoOP.SelectedRow.Cells(2).FindControl("lblNroDocumento"), Label).Text) & " Semana: " & cboSemana.SelectedItem.Text & " Del: " & txtFechaInicio_ProgSemana.Text & " Hasta: " & txtFechaFin_ProgSemana.Text
        tabla.Rows.Add(dr)


        dr1 = tabla.NewRow
        dr1.Item(0) = ""
        dr1.Item(1) = ""
        dr1.Item(2) = "Empresa:"
        dr1.Item(3) = GV_DocumentoOP.SelectedRow.Cells(4).Text & "  Tienda: " & GV_DocumentoOP.SelectedRow.Cells(5).Text

        tabla.Rows.Add(dr1)

        Dim Fecha As String = ""
        If rbtFechaOSemana.SelectedValue = CStr(1) Then
            Fecha = "Del: " & Me.txtFechaInicioSemanaI.Text & " " + "Hasta:" + " " & Me.txtFechaFinSemanafin.Text
        Else
            Fecha = "Del: " & Me.txtFechaInicio1.Text & " " + "Hasta:" + " " & Me.txtFechaFin1.Text
        End If

        dr2 = tabla.NewRow
        dr2.Item(0) = ""
        dr2.Item(1) = ""
        dr2.Item(2) = "Fecha:"
        dr2.Item(3) = Fecha
        tabla.Rows.Add(dr2)

        dr3 = tabla.NewRow
        dr3.Item(0) = ""
        dr3.Item(1) = ""
        dr3.Item(2) = ""
        dr3.Item(3) = ""
        tabla.Rows.Add(dr3)

        grillaTitulo.DataSource = tabla
        grillaTitulo.DataBind()

        grillaTitulo.HeaderStyle.BackColor = Drawing.Color.SkyBlue
        grillaTitulo.HeaderStyle.ForeColor = Drawing.Color.Black
        grillaTitulo.HeaderStyle.Height = 40
        grillaTitulo.HeaderStyle.Font.Underline = True
        grillaTitulo.HeaderStyle.Font.Bold = True
        grillaTitulo.BorderColor = Drawing.Color.White

        For Each row As GridViewRow In grillaTitulo.Rows
            row.BorderColor = Drawing.Color.White
        Next

        grillaTitulo.Columns(2).ItemStyle.Font.Bold = True
        grillaTitulo.Columns(3).ItemStyle.Font.Bold = True
        grillaTitulo.Columns(0).ItemStyle.BorderColor = Drawing.Color.White
        grillaTitulo.Columns(1).ItemStyle.BorderColor = Drawing.Color.White
        grillaTitulo.Columns(2).ItemStyle.BorderColor = Drawing.Color.White
        grillaTitulo.Columns(3).ItemStyle.BorderColor = Drawing.Color.White


        grillaTitulo.RenderControl(htmlTextWriter)
        grilla.RenderControl(htmlTextWriter)

        EnableViewState = False

        Response.Write(strWriter.ToString)

        grilla.Dispose()

        Response.End()

    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
    End Sub
    Public Shared Sub PrepareGrid(ByVal gvr As Control)
        Dim l As New Literal()
        For i As Integer = 0 To gvr.Controls.Count - 1
            If gvr.Controls(i).[GetType]() Is GetType(LinkButton) Then
                l.Text = DirectCast(gvr.Controls(i), LinkButton).Text
                gvr.Controls.Remove(gvr.Controls(i))
                gvr.Controls.AddAt(i, l)
            ElseIf gvr.Controls(i).[GetType]() Is GetType(Label) Then
                l.Text = DirectCast(gvr.Controls(i), Label).Text
                gvr.Controls.Remove(gvr.Controls(i))
                gvr.Controls.AddAt(i, l)
            ElseIf gvr.Controls(i).[GetType]() Is GetType(TextBox) Then
                l.Text = DirectCast(gvr.Controls(i), TextBox).Text
                gvr.Controls.Remove(gvr.Controls(i))
                gvr.Controls.AddAt(i, l)
            ElseIf gvr.Controls(i).[GetType]() Is GetType(CheckBox) Then
                l.Text = If(DirectCast(gvr.Controls(i), CheckBox).Checked, "True", "False")
                gvr.Controls.Remove(gvr.Controls(i))
                gvr.Controls.AddAt(i, l)
            End If
            If gvr.Controls(i).HasControls() Then
                PrepareGrid(gvr.Controls(i))
            End If

        Next
    End Sub

    Public Sub PrepareGridCol(ByVal grilla As GridView)
        Dim cols As Integer = grilla.Columns.Count

        agregarColumnaGrilla(grilla, "PedidoAprobado", True, "PedidoAprobado")
        agregarColumnaGrilla(grilla, "PedidoAprobConv", True, "PedidoAprobConv")

        Dim col As Integer = grilla.Columns.Count

        Dim cantidadAprob As Decimal
        Dim cantidadAprobadaconv As Decimal

        Dim cadena, cadena1, cadNvaCober, cadSaldoTienda As String

        cadena = Me.hdfConvPed.Value
        cadena1 = Me.hdfProdCapAprob.Value
        cadNvaCober = Me.hdfNvaCobertura.Value
        cadSaldoTienda = Me.hdfSaldotienda.Value

        Dim cad(), cad2() As String
        Dim cad1(), cad3() As String

        Dim cadNC1() As String
        Dim cadST1() As String

        cad = Split(cadena, ";", -1)
        cad2 = Split(cadena1, ";", -1)
        cadNC1 = Split(cadNvaCober, ";", -1)
        cadST1 = Split(cadSaldoTienda, ";", -1)

        Dim j As Int32
        Dim ord As Integer
        Dim tolCol As Integer = grilla.Rows.Count - 1


        For i As Integer = 0 To tolCol
            'Dim idproducto As Integer = CInt(grilla.Rows(i).Cells(2).Text)
            ord = CInt(grilla.Rows(i).Cells(2).Text)
            j = i + 1
            cad1 = Split(CStr(cad(j)), ",", -1)
            cantidadAprobadaconv = CDec(cad1(1).ToString)

            cad3 = Split(CStr(cad2(j)), ",", -1)
            cantidadAprob = CDec(cad3(1).ToString)


            With grilla.Rows(i)
                .Cells(cols - 2).Text = CStr(cantidadAprob)
                .Cells(cols - 1).Text = CStr(cantidadAprobadaconv)
                '.Cells(cols - 4).Text = cadNC1(j)
                '.Cells(cols - 2).Text = cadST1(j)

            End With
        Next


        Dim nrocol As Integer = grilla.Columns.Count - 1
        Dim nomcol(nrocol) As String

        For y As Integer = 0 To nrocol
            nomcol(y) = gvwAproped.Columns.Item(y).HeaderText
            gvwAproped.Columns.Item(y).HeaderStyle.BackColor = Drawing.Color.SkyBlue
            gvwAproped.Columns.Item(y).HeaderStyle.BorderColor = Drawing.Color.AliceBlue
            'gvwAproped.Columns.Item(y).HeaderStyle.Font.Name
            gvwAproped.Columns.Item(y).HeaderStyle.Height = 30
            gvwAproped.Columns.Item(y).HeaderStyle.HorizontalAlign = HorizontalAlign.Left

        Next




        'Session.Add("grillaPedAprob", grilla)
        Session.Add("nro", nrocol)
        Session.Add("nomcol", nomcol)

    End Sub


    Private Sub btnExpToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExpToExcel.Click
        'PrepareGrid(Me.gvwAproped)
        If gvwAproped.Rows.Count > 0 Then

            PrepareGridCol(Me.gvwAproped)
            ExportGrid(Me.gvwAproped, "Ventas y Stock")

            'ExportGrid(Me.GV_Detalle, "Ventas y Stock")

            'GridViewExportUtil.Export(gvwAproped)
            'Response.Redirect("visorGrillaEE.aspx")


        End If
    End Sub
    Public Sub mostrarBotondoc(ByVal v As Boolean)

    End Sub

    'Private Sub btnGuardaDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardaDoc.Click
    '    registrarAprobacion()
    'End Sub
    Public Sub ocultaCheck(ByVal v As Boolean)
        checkGuardaCopiaPed.Checked = v
    End Sub


    Public Sub llenarcboSemanaActual(ByVal cboano As DropDownList, ByVal cboSem As DropDownList, ByVal txtFechaInicio As TextBox, ByVal txtFechafin As TextBox)
        Try
            Dim objProgramacionPedido As Entidades.ProgramacionPedido = (New Negocio.ProgramacionPedido).ProgramacionPedidoSelectxIdYearxIdSemana(CInt(cboano.SelectedValue), CInt(cboSem.SelectedValue))
            If (objProgramacionPedido IsNot Nothing) Then

                txtFechaInicio.Text = Format(objProgramacionPedido.cal_FechaIni, "dd/MM/yyyy")
                txtFechafin.Text = Format(objProgramacionPedido.cal_FechaFin, "dd/MM/yyyy")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub idSemanaI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles idSemanaI.SelectedIndexChanged
        llenarcboSemanaActual(Me.idYearI, idSemanaI, Me.txtFechaInicioSemanaI, Me.txtFechaFinSemanaI)
    End Sub
    Private Sub idYearI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles idYearI.SelectedIndexChanged
        llenarcboSemanaActual(Me.idYearI, idSemanaI, Me.txtFechaInicioSemanaI, Me.txtFechaFinSemanaI)
    End Sub
    Private Sub idSemanaF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles idSemanaF.SelectedIndexChanged
        llenarcboSemanaActual(Me.idYearF, idSemanaF, Me.txtFechaInicioSemanaFin, Me.txtFechaFinSemanafin)
    End Sub
    Private Sub idYearF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles idYearF.SelectedIndexChanged
        llenarcboSemanaActual(Me.idYearF, idSemanaF, Me.txtFechaInicioSemanaFin, Me.txtFechaFinSemanafin)
    End Sub


End Class




Public Class GridViewTemplate
    Implements ITemplate
    'A variable to hold the type of ListItemType.
    Private _templateType As ListItemType
    'A variable to hold the column name.
    Private _columnName As String
    'Constructor where we define the template type and column name.
    Public Sub New(ByVal type As ListItemType, ByVal colname As String)
        'Stores the template type.
        _templateType = type
        'Stores the column name.
        _columnName = colname
    End Sub
    Private Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements ITemplate.InstantiateIn

        Select Case _templateType
            Case (ListItemType.Header)
                'Creates a new label control and add it to the container.
                Dim lbl As Label = New Label
                'Allocates the new label object.
                lbl.Text = _columnName
                'Assigns the name of the column in the lable.
                container.Controls.Add(lbl)
                'Adds the newly created label control to the container.
                Exit Select
            Case (ListItemType.Item)
                'Creates a new text box control and add it to the container.
                Dim tb1 As New TextBox()
                tb1.ID = "txtPedAprod"
                tb1.Text = "0"
                'Allocates the new text box object.
                AddHandler (tb1.DataBinding), AddressOf tb1_DataBinding
                'AddHandler gvData.RowCreated, AddressOf gvData_RowCreated

                'Attaches the data binding event.
                tb1.Columns = 5
                'Creates a column with size 4.
                container.Controls.Add(tb1)
                'Adds the newly created textbox to the container.
                Exit Select
            Case (ListItemType.EditItem)
                'As, I am not using any EditItem, I didnot added any code here.
                Exit Select

            Case (ListItemType.Footer)
                Dim chkColumn As New CheckBox()
                chkColumn.ID = "Chk" & _columnName
                container.Controls.Add(chkColumn)
                Exit Select
        End Select
    End Sub

    ''' <summary>
    ''' This is the event, which will be raised when the binding happens.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub tb1_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
        Dim txtdata As TextBox = DirectCast(sender, TextBox)
        Dim container As GridViewRow = DirectCast(txtdata.NamingContainer, GridViewRow)
        Dim dataValue As Object = DataBinder.Eval(container.DataItem, _columnName)

        'If dataValue <> CType(DBNull.Value, Object) Then
        '    txtdata.Text = dataValue.ToString()
        'End If

        If dataValue Is CType(DBNull.Value, Object) Then
            txtdata.Text = CStr(0)
        Else
            txtdata.Text = dataValue.ToString()
        End If

    End Sub

    'Protected Sub gvData_RowCreated(ByVal sender As Object, _
    '            ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs, ByVal gvdata As GridView)
    '    Dim index As Integer
    '    Dim col As DataControlField = Nothing

    '    Dim image As HtmlImage = Nothing

    '    If (e.Row.RowType = DataControlRowType.Header) Then
    '        'loop through the columns in the gridview updating the header to
    '        'mark which column is the sort column and the sort order
    '        For index = 0 To gvdata.Columns.Count - 1
    '            col = gvData.Columns(index)

    '            'check to see if this is the sort column
    '            If (col.SortExpression.Equals(gvData.SortExpression)) Then
    '                'this is the sort column so determine whether the ascending or
    '                'descending image needs to be included
    '                image = New HtmlImage()
    '                image.Border = 0
    '                'If (gvData.SortDirection = SortDirection.Ascending) Then
    '                '    image.Src = "images/sort_ascending.gif"
    '                'Else
    '                '    image.Src = "images/sort_descending.gif"
    '                'End If

    '                'add the image to the column header
    '                e.Row.Cells(index).Controls.Add(image)
    '            End If  'If (col.SortExpression = sortExpression)
    '        Next index
    '    End If  'If (gvData.SortExpression.Equals(String.Empty))
    'End Sub  'gvData_RowCreated


End Class


Public Class GridViewExportUtil

    Public Shared Function Export(ByVal gv As GridView) As String
        HttpContext.Current.Response.Clear()

        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        With table
            .GridLines = gv.GridLines
            .Width = CType(gv.Width.Value, Unit)
        End With


        'AGREGANDO EL HEADER.
        If Not (gv.HeaderRow Is Nothing) Then
            GridViewExportUtil.PrepareControlport(gv.HeaderRow)
            With gv.HeaderRow
                .Cells(gv.Columns.Count - 1).Visible = True 'kitando la ultima KOLUMNA DEL HEADER
                .HorizontalAlign = HorizontalAlign.Center
                .BackColor = Drawing.ColorTranslator.FromHtml("#4A3C8C")
                .ForeColor = Drawing.Color.White
            End With

            table.Rows.Add(gv.HeaderRow)
        End If

        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            GridViewExportUtil.PrepareControlport(row)

            With row
                .BackColor = Drawing.ColorTranslator.FromHtml("#E7E7FF")
                .ForeColor = Drawing.ColorTranslator.FromHtml("#4A3C8C")
                .Cells(gv.Columns.Count - 1).Visible = False
                '.BorderWidth = 1
                .HorizontalAlign = HorizontalAlign.Center
                '.Font.Bold = True
                '<AlternatingRowStyle BackColor="#F7F7F7" />
                '<RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
            End With

            table.Rows.Add(row)
        Next

        'AGREGANDO EL footer.
        If Not (gv.FooterRow Is Nothing) Then
            GridViewExportUtil.PrepareControlport(gv.FooterRow)
            With gv.FooterRow
                .Cells(gv.Columns.Count - 1).Visible = True 'kitando la ultima KOLUMNA DEL HEADER
                .HorizontalAlign = HorizontalAlign.Center
                .BackColor = Drawing.ColorTranslator.FromHtml("#B5C7DE")
                .ForeColor = Drawing.ColorTranslator.FromHtml("#4A3C8C")
                .Font.Bold = True

            End With

            table.Rows.Add(gv.FooterRow)
        End If

        '  render the table into the htmlwriter
        table.BorderWidth = 2
        table.RenderControl(htw)
        Export = sw.ToString

    End Function

    ' Replace any of the contained controls with literals
    Public Shared Sub PrepareControlport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < Control.Controls.Count)
            Dim current As Control = Control.Controls(i)

            If (TypeOf current Is LinkButton) Then
                Control.Controls.Remove(current)
            ElseIf (TypeOf current Is ImageButton) Then
                Control.Controls.Remove(current)
            ElseIf (TypeOf current Is HyperLink) Then
                Control.Controls.Remove(current)
            ElseIf (TypeOf current Is DropDownList) Then
                Control.Controls.Remove(current)
            ElseIf (TypeOf current Is CheckBox) Then
                Control.Controls.Remove(current)
            ElseIf (TypeOf current Is RadioButton) Then
                Control.Controls.Remove(current)
            ElseIf (TypeOf current Is TextBox) Then
                Control.Controls.Remove(current)
                Control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is RegularExpressionValidator) Then
                Control.Controls.Remove(current)
            ElseIf (TypeOf current Is Button) Then
                Control.Controls.Remove(current)
            End If

            If current.HasControls Then
                GridViewExportUtil.PrepareControlport(current)
            End If

            i = (i + 1)
        Loop
    End Sub


End Class
