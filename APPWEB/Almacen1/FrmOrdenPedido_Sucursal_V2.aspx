<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmOrdenPedido_Sucursal_V2.aspx.vb" Inherits="APPWEB.FrmOrdenPedido_Sucursal_V2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr style="background-color: Yellow">
            <td style="width: 350px">
                <asp:LinkButton CssClass="Label" Font-Bold="true" ID="lkb_CapaConfigFrm" OnClientClick="return(onCapa('capaConfiguracionFrm'));"
                    runat="server">Config. Formulario</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" ToolTip="Nuevo" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                                Text="Editar" ToolTip="Editar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                                Text="Anular" ToolTip="Anular" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" ToolTip="Guardar" OnClientClick=" return(     valSaveDocumento()       ); "
                                Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  valOnClick_btnImprimir()  );"
                                Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscarDocumentoRef" runat="server" Text="Doc. Referencia" OnClientClick="  return(  valOnClick_btnBuscarDocumentoRef()  );  "
                                ToolTip="Buscar Documentos de Referencia" Width="120px" />
                        </td>
                        <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                ORDEN DE PEDIDO -- POR VENTA PUNTUAL
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa .:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarDocumentoxCodigo">
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( valOnKeyPressCodigoDoc(this,event)  );"
                                    ReadOnly="true" Width="100px"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" ToolTip="Buscar Documento por [ Serie - N�mero ]." />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="B�squeda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Almac�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboAlmacen_Llegada" runat="server" AutoPostBack="false" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Operaci�n:
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="cboTipoOperacion" runat="server" AutoPostBack="true" Width="100%"
                                    Visible="true">
                                </asp:DropDownList>
                            </td>
                            <td colspan="5" class="TextoRojo">
                                <asp:CheckBox ID="ckProductoComprometidos" runat="server" Text="" Enabled="false"
                                    Font-Bold="true" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Motivo Traslado:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboMotivoTraslado" runat="server" AutoPostBack="false" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td colspan="8" class="TextoRojo" style="font-weight: bold;">
                                Antes de realizar la b�squeda del documento de referencia seleccione su almac�n
                                suministrador
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="imgDocumentoRef" runat="server" Height="16px" />
                            &nbsp;DOCUMENTO DE REFERENCIA
                            <cc1:CollapsiblePanelExtender ID="cpeDocumentoRef" runat="server" CollapseControlID="imgDocumentoRef"
                                Collapsed="true" CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandControlID="imgDocumentoRef"
                                ExpandedImage="~/Imagenes/Menos_B.JPG" ImageControlID="imgDocumentoRef" TargetControlID="Panel_DocumentoRef">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_DocumentoRef" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:CommandField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" SelectText="Quitar" ShowSelectButton="True" />
                                                        
                                                    <asp:BoundField DataField="NomTipoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Documento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" />
                                                        
                                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Nro. Documento" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblNroDocumento" runat="server" Font-Bold="true" ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                                        <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                                        <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />

                                                                        <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                                        <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />

                                                                        <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoDocumento")%>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Empresa" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Empresa" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Tienda" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Tienda" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomAlmacen" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Almac�n" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomEstadoDocumento" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Estado" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                                        <asp:ImageButton ID="imgVerDetalle" runat="server" OnClick="OnClick_imgVerDetalle"
                                                                            ImageUrl="~/Imagenes/search_add.ico" Width="25px" Height="25px" />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_DocumentoRef_Detalle" runat="server" AutoGenerateColumns="False"
                                                Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:BoundField DataField="CodigoProducto" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="C�d." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomProducto" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Producto" ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="UMedida" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="U.M." ItemStyle-Font-Bold="true" ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Cantidad" DataFormatString="{0:F4}" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Cantidad" ItemStyle-Font-Bold="true"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderText="Despachado" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblDespachado" runat="server" Font-Bold="true" Text=""></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="CantxAtender" DataFormatString="{0:F4}" HeaderStyle-Height="25px"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderText="Pendiente" ItemStyle-Font-Bold="true"
                                                        ItemStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                ALMACEN REMITENTE < SUMINISTRADOR >
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_PuntoPartida" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto" style="font-weight: bold">
                                Tipo Almac�n:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cboTipoAlmacen_Partida" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" style="font-weight: bold">
                                            Almac�n:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboAlmacen_Partida" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DETALLE DEL PEDIDO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Detalle" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                                                OnClientClick="return(valOnClick_btnBuscarProducto());" onmouseout="this.src='../Imagenes/BuscarProducto_b.JPG';"
                                                onmouseover="this.src='../Imagenes/BuscarProducto_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiarDetalleDocumento" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_ComponenteKit" runat="server" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="CodigoProd_Comp" HeaderText="C�digo" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                            ItemStyle-Font-Bold="true" />
                                        <asp:BoundField DataField="Componente" HeaderText="Componente" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px"
                                            ItemStyle-Font-Bold="true" />
                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblCantidad_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad_Comp","{0:F3}")%>'></asp:Label>

                                                            <asp:Label ID="lblUnidadMedida_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida_Comp")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Precio" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Height="25px" ItemStyle-Height="25px" ItemStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblMonedaPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MonedaPrecio_Comp")%>'></asp:Label>

                                                            <asp:Label ID="lblPrecio_Comp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Precio_Comp","{0:F3}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>

                                                            <asp:LinkButton CssClass="Texto" ID="lkbQuitar" Font-Bold="true" runat="server" OnClick="lkbQuitar_Click">Quitar</asp:LinkButton>

                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="NomProducto" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="U. Medida" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboUnidadMedida" runat="server" DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaUM")%>' AutoPostBack="true"
                                                    DataValueField="IdUnidadMedida" DataTextField="NombreCortoUM" OnSelectedIndexChanged="valOnChange_cboUnidadMedida" >
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:TextBox ID="txtCantidad" runat="server" onblur="return(valBlur(event));" TabIndex="500"
                                                                onKeypress="return(validarNumeroPunto(event));" onfocus="return(aceptarFoco(this));" onKeyUp=" return ( calcularPesoTotal(this) ); "
                                                                Width="90px" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:G29}")%>'></asp:TextBox>

                                                            <asp:HiddenField ID="hddIdDetalleAfecto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDetalleDocumento","{0:F2}")%>' />

                                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F2}")%>' />

                                                            <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />

                                                            <asp:HiddenField ID="hddIdTipoCliente" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoCliente")%>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                           <asp:TemplateField HeaderText="Stock Almacen" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblStockxAlmacen" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StockTotalxAlmacen","{0:F2}")%>' />
   
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Peso" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>

                                                            <asp:TextBox ID="txtPeso" TabIndex="310" Width="70px" Font-Bold="true" runat="server"
                                                                Enabled="false" Text='<%#DataBinder.Eval(Container.DataItem,"Peso","{0:F3}")%>'
                                                                onKeyPress="return( validarNumeroPuntoPositivo('event') );" onblur=" return( valBlur(event) ); "
                                                                onFocus=" return( aceptarFoco(this) ); " onKeyUp=" return(  calcularPesoTotal(this)  ); "></asp:TextBox>

                                                            <asp:DropDownList ID="cboUnidadMedida_Peso" runat="server" Enabled="false" DataValueField="IdUnidadMedida"
                                                                DataTextField="NombreCortoUM">
                                                            </asp:DropDownList>

                                                            <asp:HiddenField ID="hddEquivalencia" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Equivalencia","{0:F6}")%>' />

                                                            <asp:HiddenField ID="hddEquivalencia1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Equivalencia1","{0:F6}")%>' />

                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Cantidad Ref." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                              <ItemTemplate>
                                                <asp:Label ID="lblStockRef" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CantidadxAtenderText","{0:F2}")%>' />
   
                                              </ItemTemplate>
                                          </asp:TemplateField>
                                     <%--   <asp:BoundField DataField="CantidadxAtenderText" HeaderText="Cantidad Ref." HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                              --%>         
                               
                                        <asp:TemplateField HeaderText="Stock Disp. Ref." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:ImageButton ID="btnMostrarComponenteKit" runat="server" ImageUrl="~/Imagenes/search_add.ico"
                                                                Width="20px" ToolTip="Visualizar Componentes del Kit" OnClick="btnMostrarComponenteKit_Click" />

                                                            <asp:Label ID="lblUMSuministrador" runat="server" Font-Bold="true" ForeColor="Red"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Stock","{0:G29}")%>'></asp:Label>

                                                            &nbsp;<asp:Label ID="lblStockSuministrador" runat="server" Font-Bold="true" ForeColor="Red"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"UM1","{0:F2}")%>'></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right">
                <table>
                    <tr>
                        <td class="Texto" style="font-weight: bold">
                            Peso Total:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPesoTotal" Width="70px" CssClass="TextBox_ReadOnlyLeft" onKeyPress=" return( false ); "
                                runat="server" Text="0"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="cboUnidadMedida_PesoTotal" runat="server" Enabled="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                OBSERVACIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" Width="100%" runat="server"
                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                    Height="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="15" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hddDocReferencia" runat="server" />
                <asp:HiddenField ID="hddDocTipoOperacion" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    <div id="capaConfiguracionFrm" style="border: 3px solid blue; padding: 8px; width: auto;
        height: auto; position: absolute; top: 120px; left: 38px; background-color: white;
        z-index: 2; display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton9" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaConfiguracionFrm'));" />
                </td>
            </tr>
            <tr align="left">
                <td>
                    <table>
                        <tr>
                            <td class="Label" align="right">
                                Comprometer Stock del Almac�n < Auto Aprobaci�n >:
                            </td>
                            <td style="width: 75px">
                                <asp:CheckBox ID="chb_ComprometerStock" runat="server" Checked="false" ToolTip="Comprometer Stock del Almac�n de Partida." />
                            </td>
                            <td class="Label" align="right">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                TipoExistencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                    DataTextField="Descripcion" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    DataValueField="Id" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">
                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                onKeypress="return( valKeyPressDescripcionProd(this,event) );"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td class="Texto">
                                            C�d.:
                                        </td>
                                        <td>
                                        <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                onKeypress="return( valKeyPressDescripcionProd(this,event) );" TabIndex="205"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />--%>
                                                <asp:Button ID="btnBuscarGrilla_AddProducto" runat="server" Text="Buscar" CssClass="btnBuscar"
                                            OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" TabIndex="207" />
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../Imagenes/Agregar_A.JPG';"
                                                OnClientClick="return(valAddProductos());" TabIndex="208" />--%>
                                                <asp:Button ID="btnAddProductos_AddProducto" runat="server" Text="A�adir" CssClass="btnA�adir"
                                                    TabIndex="208" />
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />--%>
                                                <asp:Button ID="btnLimpiar_AddProducto" runat="server" Text="Limpiar" CssClass="btnLimpiar"
                                                TabIndex="209" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel6" runat="server" DefaultButton="btnAddProductos_AddProducto">                    
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCantidad_AddProd" TabIndex="210" Width="65px" onblur="return( valBlurClear('0.00',event) );"
                                        onfocus="return(  aceptarFoco(this) );" onKeypress="return( valKeyPressCantidadAddProd(event)  );"
                                        Text="0" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Codigo" HeaderText="C�digo" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                            <asp:TemplateField HeaderText="U.M." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:DropDownList ID="cboUnidadMedida" runat="server" AutoPostBack="false" DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaUM")%>'
                                        DataTextField="NombreCortoUM" DataValueField="IdUnidadMedida">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="StockDisponible_Cadena" HeaderText="Stock Disponible"
                                NullDisplayText="0" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:ImageButton ID="btnConsultarStockAlmacenes_BuscarProd" runat="server" OnClick="mostrarCapaStockPrecioxProducto"
                                                    ImageUrl="~/Imagenes/Ok_b.bmp" ToolTip="Consultar Lista de Precios y Stock por Tienda / Almac�n." 
                                                    OnClientClick="this.src='../Imagenes/loader.gif'"/>

                                                <asp:HiddenField ID="hddIdProducto_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                <asp:HiddenField ID="hddKit" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Kit")%>' />

                                                <asp:HiddenField ID="hddIdUnidadMedida_PR" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%" AllowPaging="True" PagerStyle-ForeColor="White">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Destinatario" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 1200px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoDocumento" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" style="text-align: left">
                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                        CssClass="Texto" AutoPostBack="true">
                        <asp:ListItem Value="0" Selected="True">Por Fecha Emision</asp:ListItem>
                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:Panel ID="Panel4" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td class="Texto">
                                                Nro. C�digo:
                                            </td>
                                            <td>
                                            <asp:Panel ID="Panel5" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="False"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                    <asp:HiddenField ID="hddIdEmpresa_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                                    <asp:HiddenField ID="hddIdAlmacen_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />
                                                    <asp:HiddenField ID="hddIdPersona_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                                    <asp:HiddenField ID="hddDocReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>' />
                                                    <asp:HiddenField ID="hddDocTipoOperacion_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"strTipoOperacionRef")%>' />
                                                 
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Descripci�n" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomAlmacen" HeaderText="Almac�n" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                 <asp:BoundField DataField="strTipoOperacionRef" HeaderText="TipoOperacion" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                            DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              
                                

                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaConsultarStockPrecioxProducto" style="border: 3px solid blue; padding: 8px;
        width: 450px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa2('capaConsultarStockPrecioxProducto')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProductoConsultarStockPrecioxProd" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ConsultarStockPrecioxProd" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Almac�n" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                <asp:Label ID="lblAlmacen" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Almacen")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Disponible" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblStock" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StockDisponibleN","{0:F3}")%>'></asp:Label>

                                                <asp:Label ID="lblUM" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cadenaUM")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cantidad" Visible="false" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCantidad" Text="0" runat="server" Width="80px" Font-Bold="true"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnAddProducto_ConsultarStockPrecio" runat="server" Text="Aceptar"
                        Width="90px" OnClientClick="return(  offCapa2('capaConsultarStockPrecioxProducto')   );" />
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function valKeyPressCodigoSL(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProducto.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(obj, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscarGrilla_AddProducto.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valAddProductos() {

            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            //        var txtIdCliente = 
            //        if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
            //            alert('Debe seleccionar un Cliente.');
            //            return false;
            //        }

            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('No se seleccionaron productos.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                var txtCantidad = rowElem.cells[0].children[0];
                if (parseFloat(txtCantidad.value) > 0) {
                    // if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
                    //                        alert('La cantidad ingresada excede a la cantidad disponible.');
                    //                        txtCantidad.select();
                    //                        txtCantidad.focus();
                    //     return false;
                    //  } else if (parseFloat(txtCantidad.value) > 0) {
                    cont = cont + 1;
                    //  }
                }
            }

            if (cont == 0) {
                alert('No se seleccionaron productos.');
                return false;
            }

            /*
            //************* Validamos que no agregue de otro almacen
            
            if (grillaDetalle != null) {

                if (grillaDetalle.rows.length > 1) {

                    //************ valido el almacen
                    
            var hddIdAlmacen = 
                    

                    if (parseFloat(cboAlmacen.value) != parseFloat(hddIdAlmacen.value)) {
                        
            return false;
            }
            }
            }
            */
            return true;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        function valStockDisponible_AddProd() {
            /*var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
            if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].id == event.srcElement.id) {
            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
            alert('La cantidad ingresada excede a la cantidad disponible.');
            rowElem.cells[0].children[0].select();
            rowElem.cells[0].children[0].focus();
            return false;
            }
            return false;
            }

                }
            }*/
            return false;
        }
        function valKeyPressCantidadAddProd(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                document.getElementById('<%=btnAddProductos_AddProducto.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }
        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function valOnClick_btnAceptarBuscarDocRefxCodigo() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('Ingrese un Nro. de Serie para la b�squeda.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('Ingrese un Nro. de Documento para la b�squeda.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }

        function valSaveDocumento() {

            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie de Documento.');
                return false;
            }

            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) == 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }

            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var cboAlmacen_Partida = document.getElementById('<%=cboAlmacen_Partida.ClientID%>');
            if (isNaN(parseInt(cboAlmacen_Partida.value)) || cboAlmacen_Partida.value.length <= 0 || parseInt(cboAlmacen_Partida.value) == 0) {
                alert('DEBE SELECCIONAR UN ALMAC�N SUMINISTRADOR. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var cboAlmacen_Llegada = document.getElementById('<%=cboAlmacen_Llegada.ClientID%>');
            if (isNaN(parseInt(cboAlmacen_Llegada.value)) || cboAlmacen_Llegada.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ALMAC�N DE LLEGADA. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var cboMotivoTraslado = document.getElementById('<%=cboMotivoTraslado.ClientID%>');
            if (isNaN(parseInt(cboMotivoTraslado.value)) || cboMotivoTraslado.value.length <= 0 || parseInt(cboMotivoTraslado.value) == 0) {
                alert('DEBE SELECCIONAR UN MOTIVO DE TRASLADO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            if (parseInt(cboAlmacen_Llegada.value) == parseInt(cboAlmacen_Partida.value)) {
                alert('EL ALMAC�N SUMINISTRADOR NO PUEDE SER IGUAL AL DEL DOCUMENTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }


            var IdTipoDocumentoRef = getIdTipoDocumentoRef();

            if (IdTipoDocumentoRef == 1 || IdTipoDocumentoRef == 3) {

                var ckProductoComprometidos = document.getElementById('<%=ckProductoComprometidos.ClientID %>');
                if (parseInt(cboTipoOperacion.value) == 1 && ckProductoComprometidos.checked == true) {
                    alert('LA OPERACI�N NO PUEDE SER VENTA SI LOS PRODUCTOS DEL DOCUMENTO DE REF. HAN COMPROMETIDO ALMAC�N. NO PUEDE. NO SE PERMITE LA OPERACI�N.');
                    return false;
                }

                if (parseInt(cboTipoOperacion.value) != 1 && ckProductoComprometidos.checked == false) {
                    alert('LA OPERACI�N DEBE SER VENTA SI LOS PRODUCTOS DEL DOCUMENTO DE REF. NO HAN COMPROMETIDO ALMAC�N. NO PUEDE. NO SE PERMITE LA OPERACI�N.');
                    return false;
                }

            }
            //************* Validando cantidades y precios
            var tipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            var opcionCombo = tipoOperacion.options[tipoOperacion.selectedIndex].value;
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                var cantidad = 0;
                var txtCantidad;
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    txtCantidad = rowElem.cells[4].children[0];
                    cantidad = parseFloat(txtCantidad.value);
                    if (isNaN(cantidad)) { cantidad = 0; }
                    if (cantidad <= 0) {
                        alert('Ingrese una cantidad mayor a cero.');
                        txtCantidad.select();
                        txtCantidad.focus();
                        return false;
                    }
                    stock = parseFloat(rowElem.cells[5].children[0].innerHTML);
                    if (opcionCombo == 9 || opcionCombo == 18) {                        
                        if (stock != 0 && stock > 1) {
                            alert("Existen productos que si cuentan con stock, no procede la operaci�n..");
                            return false;
                        }
                    }

                }
            } else {
                alert('No se han ingresado productos.');
                return false;
            }

            var grilla = document.getElementById('<%=GV_DocumentoRef.ClientID%>');
            /*if (grilla == null) {
            //****************alert('DEBE INGRESAR DOCUMENTOS DE REFERENCIA. NO SE PERMITE LA OPERACI�N.');
            return false;
            } */

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
            var mensaje = '';
            switch (parseInt(hddFrmModo.value)) {
                case 1: //******** NUEVO
                    mensaje = 'Desea continuar con la Operaci�n ?';
                    break;
                case 2: //******** EDITAR
                    mensaje = 'Desea continuar con la Operaci�n ?';
                    break;
            }            
            if (confirm(mensaje)) {
                document.getElementById('<%=btnGuardar.ClientID %>').style.visibility = 'hidden';
                return true;
            }
            else {
                document.getElementById('<%=btnGuardar.ClientID %>').style.visibility = 'visible';
                return false;
            }
        }
        function valOnClick_btnBuscarDocumentoRef() {
            onCapa('capaDocumentosReferencia');
            return false;
        }
        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }
        function valOnKeyPressCodigoDoc(txtCodigoDocumento, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }

        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor v�lido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }
        function valOnClickEditar() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Edici�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite su edici�n.');
                return false;
            }
            return true;
        }
        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para su Anulaci�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya est� ANULADO. No se permite su anulaci�n.');
                return false;
            }
            return confirm('Desea continuar con el Proceso de ANULACI�N del Documento ?');
        }
        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');
            return false;
        }


        function valOnClick_btnImprimir() {

            var IdDocumento = parseInt(document.getElementById('<%=hddIdDocumento.ClientID%>').value);
            if (isNaN(IdDocumento)) { IdDocumento = 0; }

            if (IdDocumento == 0) {
                alert('No se ha registrado ninguna Gu�a de Remisi�n para enviar a impresi�n.');
                return false;
            }

            window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=15&IdDocumento=' + IdDocumento, 'Despacho', 'resizable=yes,width=1000,height=800,scrollbars=1', null);

            return false;

        }

        function getIdTipoDocumentoRef() {
            var GV_DocumentoRef = document.getElementById('<%=GV_DocumentoRef.ClientID %>');
            var IdTipoDocumento = 0;
            if (GV_DocumentoRef != null) {
                for (var i = 1; i < GV_DocumentoRef.rows.length; i++) {
                    var rowElem = GV_DocumentoRef.rows[i];

                    IdTipoDocumento = parseInt(rowElem.cells[2].children[5].value);
                    if (isNaN(IdTipoDocumento)) { IdTipoDocumento = 0; }
                    break;
                }
            }

            return IdTipoDocumento;
        }


        function calcularPesoTotal(txtCaja) {
            /*
            opcion  0: Cantidad 1: Peso
            */
            var txtPesoTotalFinal = document.getElementById('<%=txtPesoTotal.ClientID%>');
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var pesoTotal = 0;
            var peso = 0;
            var cantidad;
            var pesoTotal;
            var equivalencia;
            var equivalencia1;
            var txtCantidad;
            var txtPeso;
            var txtPesoTotal;
            var hddEquivalencia;
            var hddEquivalencia1;            
            var pesoTotalFinal = 0;            

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];

                    //**************  OBTENEMOS LOS DATOS
                    txtCantidad = rowElem.cells[4].children[0];
                    cantidad = parseFloat(txtCantidad.value);
                    if (isNaN(cantidad) || txtCantidad.value.length <= 0) { cantidad = 0; }

                    txtPeso = rowElem.cells[6].children[0];
                    peso = parseFloat(txtPeso.value);
                    if (isNaN(peso) || txtPeso.value.length <= 0) { peso = 0; }

                    hddEquivalencia = rowElem.cells[6].children[2];
                    equivalencia = parseFloat(hddEquivalencia.value);
                    if (isNaN(equivalencia) || hddEquivalencia.value.length <= 0) { equivalencia = 0; }

                    hddEquivalencia1 = rowElem.cells[6].children[3];
                    equivalencia1 = parseFloat(hddEquivalencia1.value);
                    if (isNaN(equivalencia1) || hddEquivalencia1.value.length <= 0) { equivalencia1 = 0; }

                    pesoTotal = cantidad * equivalencia * equivalencia1;
                    txtPeso.value = redondear(pesoTotal, 2);            
                    pesoTotalFinal = pesoTotalFinal + pesoTotal;                    

                }
            }

            txtPesoTotalFinal.value = redondear(pesoTotalFinal, 3);            
            return false;

        }

        
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
