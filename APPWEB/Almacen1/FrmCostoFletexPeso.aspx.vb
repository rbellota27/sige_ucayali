﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmCostoFlete
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Private Const _IdMagnitudPeso As Integer = 1 '************** PESO

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        valOnLoad_Frm()
    End Sub

    Private Sub valOnLoad_Frm()
        Try

            If (Not Me.IsPostBack) Then

                inicializarFrm()

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub inicializarFrm()

        Dim objCbo As New Combo
        With objCbo

            .LLenarCboTienda(Me.cboTiendaOrigen, False)
            .LLenarCboTienda(Me.cboTiendaDestino, False)

            .LlenarCboMoneda(Me.cboMoneda, False)
            .llenarCboUnidadMedidaxIdMagnitud(Me.cboUnidadMedida, _IdMagnitudPeso, False)

        End With

        verFrm(FrmModo.Inicio, True, True)

    End Sub

    Private Sub limpiarFrm()

        '************** COMBOS
        Me.txtCostoxPeso.Text = "0"

    End Sub

    Private Sub actualizarControles()

        Me.btnNuevo.Visible = False
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnBuscar.Visible = False
        Me.Panel_Grilla.Enabled = False
        Me.Panel_Costo.Visible = False

        Me.cboTiendaOrigen.Enabled = True
        Me.cboTiendaDestino.Enabled = True

        Select Case CInt(Me.hddFrmModo.Value)
            Case FrmModo.Inicio

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.Panel_Grilla.Enabled = True

            Case FrmModo.Nuevo

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.Panel_Costo.Visible = True

            Case FrmModo.Editar

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.Panel_Costo.Visible = True

                Me.cboTiendaOrigen.Enabled = False
                Me.cboTiendaDestino.Enabled = False

        End Select

    End Sub

    Private Sub verFrm(ByVal modo As Integer, ByVal limpiar As Boolean, ByVal cargarGrilla As Boolean)

        If (limpiar) Then
            limpiarFrm()
        End If

        If (cargarGrilla) Then

            Dim objCbo As New Combo
            objCbo.LLenarCboTienda(Me.cboTiendaOrigen, True)
            objCbo.LLenarCboTienda(Me.cboTiendaDestino, True)

            Me.GV_CostoxPeso.DataSource = (New Negocio.CostoFletexPeso).CostoFletexPeso_SelectxId_DT(CInt(Me.cboTiendaOrigen.SelectedValue), CInt(Me.cboTiendaDestino.SelectedValue))
            Me.GV_CostoxPeso.DataBind()
        End If

        Me.hddFrmModo.Value = CStr(modo)
        actualizarControles()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        valOnClick_btnCancelar()
    End Sub


    Private Sub valOnClick_btnCancelar()
        Try

            verFrm(FrmModo.Inicio, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        registrar()
    End Sub
    Private Sub registrar()

        Try

            Dim objCostoFletexPeso As Entidades.CostoFletexPeso = obtenerCostoFletexPeso()

            If ((New Negocio.CostoFletexPeso).CostoFletexPeso_Registrar(objCostoFletexPeso)) Then

                verFrm(FrmModo.Inicio, True, True)
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")

            Else

                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN")

            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerCostoFletexPeso() As Entidades.CostoFletexPeso

        Dim obj As New Entidades.CostoFletexPeso

        With obj
            .IdTiendaOrigen = CInt(Me.cboTiendaOrigen.SelectedValue)
            .IdTiendaDestino = CInt(Me.cboTiendaDestino.SelectedValue)
            .IdMoneda = CInt(Me.cboMoneda.SelectedValue)
            .IdUnidadMedida_Peso = CInt(Me.cboUnidadMedida.SelectedValue)
            .costoxPesoUnit = CDec(Me.txtCostoxPeso.Text)
        End With

        Return obj

    End Function

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        valOnClick_btnNuevo()
    End Sub
    Private Sub valOnClick_btnNuevo()
        Try

            verFrm(FrmModo.Nuevo, True, False)

            Dim objCbo As New Combo
            objCbo.LLenarCboTienda(Me.cboTiendaOrigen, False)
            objCbo.LLenarCboTienda(Me.cboTiendaDestino, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        valOnClick_btnBuscar()
    End Sub
    Private Sub valOnClick_btnBuscar()
        Try

            Me.GV_CostoxPeso.DataSource = (New Negocio.CostoFletexPeso).CostoFletexPeso_SelectxId_DT(CInt(Me.cboTiendaOrigen.SelectedValue), CInt(Me.cboTiendaDestino.SelectedValue))
            Me.GV_CostoxPeso.DataBind()

            If (Me.GV_CostoxPeso.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As EventArgs)

        valOnClick_btnEliminar(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)

    End Sub

    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs)

        valOnClick_btnEditar(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)

    End Sub

    Private Sub valOnClick_btnEditar(ByVal index As Integer)

        Try

            Dim IdTiendaOrigen As Integer = CInt(CType(Me.GV_CostoxPeso.Rows(index).FindControl("hddIdTiendaOrigen"), HiddenField).Value)
            Dim IdTiendaDestino As Integer = CInt(CType(Me.GV_CostoxPeso.Rows(index).FindControl("hddIdTiendaDestino"), HiddenField).Value)

            Dim objCostoFletexPeso As Entidades.CostoFletexPeso = (New Negocio.CostoFletexPeso).CostoFletexPeso_SelectxId(IdTiendaOrigen, IdTiendaDestino)

            If (objCostoFletexPeso IsNot Nothing) Then

                cargar_GUI(objCostoFletexPeso)
                verFrm(FrmModo.Editar, False, False)

            Else

                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")

            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargar_GUI(ByVal objCostoFletexPeso As Entidades.CostoFletexPeso)

        Dim objCbo As New Combo

        With objCostoFletexPeso

            If (Me.cboTiendaOrigen.Items.FindByValue(CStr(.IdTiendaOrigen)) IsNot Nothing) Then
                Me.cboTiendaOrigen.SelectedValue = CStr(.IdTiendaOrigen)
            End If

            If (Me.cboTiendaDestino.Items.FindByValue(CStr(.IdTiendaDestino)) IsNot Nothing) Then
                Me.cboTiendaDestino.SelectedValue = CStr(.IdTiendaDestino)
            End If

            If (Me.cboMoneda.Items.FindByValue(CStr(.IdMoneda)) IsNot Nothing) Then
                Me.cboMoneda.SelectedValue = CStr(.IdMoneda)
            End If

            If (Me.cboUnidadMedida.Items.FindByValue(CStr(.IdUnidadMedida_Peso)) IsNot Nothing) Then
                Me.cboUnidadMedida.SelectedValue = CStr(.IdUnidadMedida_Peso)
            End If

            Me.txtCostoxPeso.Text = Format(.costoxPesoUnit, "F3")

        End With

    End Sub

    Private Sub valOnClick_btnEliminar(ByVal index As Integer)

        Try

            Dim IdTiendaOrigen As Integer = CInt(CType(Me.GV_CostoxPeso.Rows(index).FindControl("hddIdTiendaOrigen"), HiddenField).Value)
            Dim IdTiendaDestino As Integer = CInt(CType(Me.GV_CostoxPeso.Rows(index).FindControl("hddIdTiendaDestino"), HiddenField).Value)

            If ((New Negocio.CostoFletexPeso).CostoFletexPeso_Delete(IdTiendaOrigen, IdTiendaDestino)) Then

                verFrm(FrmModo.Inicio, True, True)
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")

            Else

                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub


End Class