﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmCostoFletexUnd.aspx.vb" Inherits="APPWEB.FrmCostoFletexUnd" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" Width="80px" runat="server" Text="Nuevo" ToolTip="Generar un Doc. Ajuste de Inventario desde un Doc. Toma de Inventario." />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" OnClientClick="return( valOnClick_btnGuardar()  );" Width="80px"
                                runat="server" Text="Guardar" ToolTip="Guardar" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancelar" Width="80px" runat="server" Text="Cancelar" ToolTip="Cancelar" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td style="width: 100%" align="right">
                            <table>
                                <tr>
                                    <td class="Texto">
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                COSTO DE FLETE - POR PRODUCTO
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Texto" style="text-align: right; font-weight: bold">
                            Tienda Origen:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTiendaOrigen" runat="server" Width="300px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" ToolTip="Buscar" Width="70px" CssClass="btnBuscar"
                            OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto" style="text-align: right; font-weight: bold">
                            Tienda Destino:
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTiendaDestino" runat="server" Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Producto" runat="server">
                    <fieldset class="FieldSetPanel">
                        <legend>Producto</legend>
                        <table width="100%">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td align="right">
                                                Tipo Existencia:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Línea:
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList TabIndex="201" ID="cboLinea_Filtro" runat="server" AutoPostBack="True"
                                                                DataTextField="Descripcion" DataValueField="Id">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            Sub Línea:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList TabIndex="202" ID="cboSubLinea_Filtro" runat="server" DataTextField="Nombre"
                                                                DataValueField="Id" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right">
                                                Descripción:
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_Filtro" runat="server" Width="300px"
                                                                onKeypress="return( valKeyPress_Filtro(this,event) );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align: right">
                                                            Cód.:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCodigoProd_Filtro" Width="110px" runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" onKeypress="return( valKeyPress_Filtro(this,event) );"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnBuscarProd" runat="server" Text="Buscar" Width="70px" UseSubmitBehavior="false"
                                                            OnClientClick="this.disabled=true;this.value='Procesando...'" ToolTip="Buscar Producto" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar" Width="70px" ToolTip="Limpiar" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                        CollapsedText="Búsqueda Avanzada" ExpandedText="Búsqueda Avanzada" ExpandDirection="Vertical"
                                        SuppressPostBack="true">
                                    </cc1:CollapsiblePanelExtender>
                                    <asp:Image ID="Image21_11" runat="server" Height="16px" ForeColor="#4277AD" />
                                    <asp:Label ID="Label21_11" runat="server" Text="Búsqueda Avanzada"></asp:Label><asp:Panel
                                        ID="Panel_BusqAvanzadoProd" runat="server">
                                        <table width="100">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td style="font-weight: bold; color: #4277AD">
                                                                Atributo:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );"
                                                                    Style="cursor: hand;" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" Style="cursor: hand;" />
                                                            </td>
                                                            <td>
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                                        Width="650px">
                                                        <RowStyle CssClass="GrillaRow" />
                                                        <Columns>
                                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                                HeaderStyle-Height="25px" ItemStyle-Height="25px" />
                                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label><asp:HiddenField
                                                                        ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                                    </asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                        <HeaderStyle CssClass="GrillaCabecera" />
                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Costo" runat="server" ScrollBars="Both" Height="2000px">
                    <table width="100%">
                        <tr>
                            <td>
                                <tr>
                                    <td>
                                        <fieldset title="Registrar Volumen Mínimo de Venta por Productos" class="FieldSetPanel">
                                            <legend>Replicación Costo Flete Por Unidad</legend>
                                            <table>
                                                <tr>
                                                    <td>
                                                        Ingreso Costo Flete Por Tarifa:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbCantidad" runat="server" onKeyPress="return(    validarNumeroPuntoPositivo('event')       );"
                                                            onFocus="return(   aceptarFoco(this)   );"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btn_AceptarCostoxFlete" runat="server" Text="Aceptar" ToolTip="Cambiar Cantidad Minima de Venta"
                                                            OnClientClick="return ( onClick_AceptarCostoxFlete() );" />
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_ProductoCostoFletexUnd_Save" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblCodigoProducto" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CodigoProducto") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdTiendaOrigen" Value='<%# DataBinder.Eval(Container.DataItem,"IdTiendaOrigen") %>'
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdTiendaDestino" Value='<%# DataBinder.Eval(Container.DataItem,"IdTiendaDestino") %>'
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdProducto" Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>'
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Producto" HeaderText="Producto" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                        <asp:TemplateField HeaderText="U.M." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cboUnidadMedida" runat="server" DataValueField="Id" DataTextField="DescripcionCorto"
                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"getListaUnidadMedida") %>'>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Costo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cboMoneda" runat="server" DataValueField="Id" DataTextField="Simbolo">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCostoFlete" Width="80px" onFocus="  return( aceptarFoco(this) );  "
                                                                onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); " onblur="return( valBlur(event)  );"
                                                                runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CostoxUnd","{0:F3}") %>'
                                                                TabIndex="200"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Grilla" runat="server" ScrollBars="Both" Height="2000">
                    <asp:GridView ID="GV_CostoxUnd" runat="server" AutoGenerateColumns="False" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEditar" runat="server" OnClick="btnEditar_Click">Editar</asp:LinkButton>
                                    <asp:HiddenField ID="hddIdTiendaOrigen" Value='<%# DataBinder.Eval(Container.DataItem,"IdTiendaOrigen") %>'
                                        runat="server" />
                                    <asp:HiddenField ID="hddIdTiendaDestino" Value='<%# DataBinder.Eval(Container.DataItem,"IdTiendaDestino") %>'
                                        runat="server" />
                                    <asp:HiddenField ID="hddIdProducto" Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>'
                                        runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEliminar" ForeColor="Red" OnClientClick="   return(  valOnClick_btnEliminar() );  "
                                        runat="server" OnClick="btnEliminar_Click">Eliminar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="TiendaOrigen" HeaderText="Tienda Origen" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="TiendaDestino" HeaderText="Tienda Destino" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="CodigoProd" HeaderText="Código" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="Producto" HeaderText="Producto" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:TemplateField HeaderText="Costo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMoneda" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Moneda") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCosto" Font-Bold="true" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CostoxUnd","{0:F3}") %>'></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="UnidadMedida" HeaderText="U.M." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        </Columns>
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <HeaderStyle CssClass="GrillaHeader" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddFrmModo" runat="server" Value="0" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        function valOnClick_btnEliminar() {
            return confirm('Desea continuar con la Operación ?');
        }

        function valOnClick_btnGuardar() {

            var cboTiendaOrigen = document.getElementById('<%=cboTiendaOrigen.ClientID%>');
            if (isNaN(parseInt(cboTiendaOrigen.value)) || cboTiendaOrigen.value.length <= 0 || parseInt(cboTiendaOrigen.value) <= 0) {
                alert('Debe seleccionar una Tienda Origen');
                return false;
            }

            var cboTiendaDestino = document.getElementById('<%=cboTiendaDestino.ClientID%>');
            if (isNaN(parseInt(cboTiendaDestino.value)) || cboTiendaDestino.value.length <= 0 || parseInt(cboTiendaDestino.value) <= 0) {
                alert('Debe seleccionar una Tienda Destino');
                return false;
            }


            return confirm('Desea continuar con la Operación ?');
        }

        function valKeyPress_Filtro(obj, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btnBuscarProd.ClientID%>');
                var btnBuscar = document.getElementById('<%=btnBuscar.ClientID%>');
                if (boton != null) {
                    boton.focus();
                } else if (btnBuscar != null) {
                    btnBuscar.focus();
                }
            }
            return true;
        }

        //
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function onClick_AceptarCostoxFlete() {
            var Cantidad = parseFloat(document.getElementById('<%=tbCantidad.ClientID %>').value);
            if( isNaN(Cantidad)) { Cantidad = 0; }
                        
            var GV_ProductoCostoFletexUnd_Save = document.getElementById('<%=GV_ProductoCostoFletexUnd_Save.ClientID %>');
            if (GV_ProductoCostoFletexUnd_Save != null) {
                for (var i = 1; i < GV_ProductoCostoFletexUnd_Save.rows.length; i++) {
                    var rowElem = GV_ProductoCostoFletexUnd_Save.rows[i];

                    rowElem.cells[3].children[0].cells[1].children[0].value = Cantidad;
                    
                }
            }
            return false;
        }     
        
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
