'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmGuiaRemision_1
    Inherits System.Web.UI.Page
    Private listaDetalle As List(Of Entidades.DetalleDocumentoView)
    Private objScript As New ScriptManagerClass
    Private listaDetalleRelacionado As List(Of Entidades.DetalleDocumento)
    Private listaDetalleDocRel_AddProd As List(Of Entidades.DetalleDocumento)
    Private objdoc As New Negocio.DocumentoView

    '****************** Valores hddAccion
    '0: Documento NO relacionado
    '1: Documento RELACIONADO

    '*************** ModoFrm
    '******** 0 : INICIO / nuevo - buscar
    '******** 1 : NUEVO / guardar - cancelar
    '******** 2 : EDITAR DOCUMENTO / guardar - cancelar
    '******** 3 : BUSCAR DOCUMENTO / buscar doc - habilitar busqueda
    '******** 4 : DOCUMENTO HALLADO CORRECTAMENTE / editar - anular - imprimir - despachar - remitir - cancelar
    '******** 5 : DOC. GUARDADO CORRECTAMENTE / imprimir - despachar - remitir - cancelar
    '******** 6: OCULTAR TODOS LOS BOTONES

    Private Sub actualizarBotonesControl()

        Select Case CInt(hddModoFrm.Value)
            Case 0
                '********* INICIO
                btnNuevo.Visible = True
                btnEditar.Visible = False
                btnAnular.Visible = False

                btnDespachar.Visible = False

                btnBuscarGRem.Visible = True
                btnBuscarGRem_1.Visible = False
                btnImprimir.Visible = False
                btnCancelar1.Visible = False
                btnGrabar.Visible = False
                txtCodigoDocumento.ReadOnly = True
                Panel_Frm.Enabled = False
                Panel_Frm.Visible = False


                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.cmbAlmacen.Enabled = False
                Me.cboSerie.Enabled = False


            Case 1
                '********* NUEVO
                btnNuevo.Visible = False
                btnEditar.Visible = False
                btnAnular.Visible = False

                btnDespachar.Visible = False

                btnBuscarGRem.Visible = False
                btnBuscarGRem_1.Visible = False
                btnImprimir.Visible = False
                btnCancelar1.Visible = True
                btnGrabar.Visible = True
                txtCodigoDocumento.ReadOnly = True
                Panel_Frm.Enabled = True
                Panel_Frm.Visible = True

                Me.cmbEmpresa.Enabled = True
                Me.cmbTienda.Enabled = True
                Me.cmbAlmacen.Enabled = True
                Me.cboSerie.Enabled = True


            Case 2
                '******************* EDITAR
                btnNuevo.Visible = False
                btnEditar.Visible = False
                btnAnular.Visible = False

                btnDespachar.Visible = False

                btnBuscarGRem.Visible = False
                btnBuscarGRem_1.Visible = False
                btnImprimir.Visible = False
                btnCancelar1.Visible = True
                btnGrabar.Visible = True
                txtCodigoDocumento.ReadOnly = True
                Panel_Frm.Enabled = True
                Panel_Frm.Visible = True

                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.cmbAlmacen.Enabled = True
                Me.cboSerie.Enabled = False


            Case 3
                '******************* BUSCAR DOC
                btnNuevo.Visible = False
                btnEditar.Visible = False
                btnAnular.Visible = False

                btnDespachar.Visible = False

                btnBuscarGRem.Visible = False
                btnBuscarGRem_1.Visible = True
                btnImprimir.Visible = False
                btnCancelar1.Visible = True
                btnGrabar.Visible = False
                txtCodigoDocumento.ReadOnly = False
                txtCodigoDocumento.Focus()
                Panel_Frm.Enabled = True
                Panel_Frm.Visible = True


                Me.cmbEmpresa.Enabled = True
                Me.cmbTienda.Enabled = True
                Me.cmbAlmacen.Enabled = True
                Me.cboSerie.Enabled = True

            Case 4
                '******************* BUSCAR hallado con exito
                btnNuevo.Visible = False
                btnEditar.Visible = True
                btnAnular.Visible = True


                btnDespachar.Visible = True

                btnBuscarGRem.Visible = False
                btnBuscarGRem_1.Visible = False
                btnImprimir.Visible = True
                btnCancelar1.Visible = True
                btnGrabar.Visible = False
                txtCodigoDocumento.ReadOnly = True
                Panel_Frm.Enabled = False
                Panel_Frm.Visible = True

                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.cmbAlmacen.Enabled = False
                Me.cboSerie.Enabled = False

            Case 5
                '******************* Doc guardado con EXITO
                btnNuevo.Visible = False
                btnEditar.Visible = False
                btnAnular.Visible = False

                btnDespachar.Visible = True

                btnBuscarGRem.Visible = False
                btnBuscarGRem_1.Visible = False
                btnImprimir.Visible = True
                btnCancelar1.Visible = True
                btnGrabar.Visible = False
                txtCodigoDocumento.ReadOnly = True
                Panel_Frm.Enabled = False
                Panel_Frm.Visible = True



                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.cmbAlmacen.Enabled = False
                Me.cboSerie.Enabled = False

            Case 6
                '******************* deshabilitar todos los botones
                btnNuevo.Visible = False
                btnEditar.Visible = False
                btnAnular.Visible = False

                btnDespachar.Visible = False

                btnBuscarGRem.Visible = False
                btnBuscarGRem_1.Visible = False
                btnImprimir.Visible = False
                btnCancelar1.Visible = False
                btnGrabar.Visible = False
                txtCodigoDocumento.ReadOnly = True
                Panel_Frm.Enabled = False
                Panel_Frm.Visible = False

                Me.cmbEmpresa.Enabled = False
                Me.cmbTienda.Enabled = False
                Me.cmbAlmacen.Enabled = False
                Me.cboSerie.Enabled = False

        End Select
    End Sub

    Private Sub ValidarPermisos()
        '**** REGISTRAR / EDITAR / ANULAR / CONSULTAR / FECHA EMISION 
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {59, 60, 61, 62, 63})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnNuevo.Enabled = True
            Me.btnGrabar.Enabled = True
        Else
            Me.btnNuevo.Enabled = False
            Me.btnGrabar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* EDITAR
            Me.btnEditar.Enabled = True
        Else
            Me.btnEditar.Enabled = False
        End If

        If listaPermisos(2) > 0 Then  '********* ANULAR
            Me.btnAnular.Enabled = True
        Else
            Me.btnAnular.Enabled = False
        End If

        If listaPermisos(3) > 0 Then  '********* CONSULTAR
            Me.btnBuscarGRem.Enabled = True
            Me.btnBuscarGRem_1.Enabled = True
        Else
            Me.btnBuscarGRem.Enabled = False
            Me.btnBuscarGRem_1.Enabled = False
        End If

        If listaPermisos(4) > 0 Then  '********* FECHA EMISION
            Me.txtFecha.Enabled = True
        Else
            Me.txtFecha.Enabled = False
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            inicializarFrm()


            Dim param As String = Request.QueryString("IdDocumento")
            If param IsNot Nothing Then
                cargarDetalleFrmDocRelacional(CInt(param))
            End If

            ValidarPermisos()

        End If

    End Sub
    Private Sub inicializarFrm()
        Try
            Dim objCombo As New Combo
            With objCombo

                '***************** empresa/tienda/almacen
                .LlenarCboEmpresaxIdUsuario(Me.cmbEmpresa, CInt(Session("IdUsuario")), False)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cmbTienda, CInt(Me.cmbEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
                .llenarCboAlmacenxIdTienda(Me.cmbAlmacen, CInt(Me.cmbTienda.SelectedValue), False)

                '******************* linea/sublinea
                .LlenarCboLinea(Me.cmbLinea, True)
                .LlenarCboSubLineaxIdLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue), True)

                '******************* motivo traslado/tipo operacion
                .llenarCboMotivoTraslado(Me.cmbMotivoTraslado, True)

                '***************** COMBOS UBIGEO
                .LLenarCboDepartamento(cmbDeptoPartida)
                .LLenarCboDepartamento(cmbDeptoLlegada)
                .LLenarCboProvincia(cmbProvPartida, cmbDeptoPartida.SelectedValue)
                .LLenarCboProvincia(cmbProvLlegada, cmbDeptoLlegada.SelectedValue)
                .LLenarCboDistrito(cmbDitritoPartida, cmbDeptoPartida.SelectedValue, cmbProvPartida.SelectedValue)
                .LLenarCboDistrito(cmbDistritoLlegada, cmbDeptoLlegada.SelectedValue, cmbProvLlegada.SelectedValue)




                '********************* estado de entrega/estado documento
                .LLenarCboEstadoEntrega(Me.cmbEstadoEntrega)
                .LLenarCboEstadoDocumento(Me.cmbEstadoDocumento)

                '*************** serie
                cargarDatosSerie(Me.cboSerie)

                '***************** llenamos el tipo operacion segun el motivo traslado
                .llenarCboTipoOperacionxIdMotivoT(Me.cmbTipoOperacion, CInt(Me.cmbMotivoTraslado.SelectedValue), True)

                '****************** VALIDAMOS EL MOTIVO DE TRASLADO COMPROMETEDOR
                If ((New Negocio.MotivoTraslado).SelectComprometedorxIdMotivoT(CInt(Me.cmbMotivoTraslado.SelectedValue))) Then
                    Me.lblAlertaComprometerStock.Visible = True
                Else
                    Me.lblAlertaComprometerStock.Visible = False
                End If

                Me.hddCantidadFilas.Value = CStr(objdoc.SelectCantidadFilasDocumento(CInt(cmbEmpresa.SelectedValue), 6))

                Me.hddAccion.Value = "0"

            End With

            verFrnNuevo()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#Region "******************** VISTAS FRM"
    Private Sub verFrmInicio()
        limpiarFrm()
        Panel_Frm.Visible = False

        mostrarBotones(0)

        hddIdDocumento.Value = "0"
        hddModoFrm.Value = "0"
        actualizarBotonesControl()

        '****************** lleno la fecha actual
        Dim objFechaActual As New Negocio.FechaActual
        Dim fechaActual As Date = objFechaActual.SelectFechaActual
        txtFecha.Text = Format(fechaActual, "dd/MM/yyyy")
        txtFechaTraslado.Text = Format(fechaActual, "dd/MM/yyyy")

        Session.Remove("listaDetalle")
        Session.Remove("listaDetalleRelacionado")
        Session.Remove("listaDetalleDocRel_AddProd")

        hddAccion.Value = "0"

        '********************* seteo x defecto el Estado de Entrega: Por Entregar
        Me.cmbEstadoEntrega.SelectedValue = "1"

        Me.cmbMotivoTraslado.Enabled = True

    End Sub
    Private Sub verFrnNuevo()

        Panel_Frm.Visible = True
        Panel_Frm.Enabled = True

        Me.listaDetalle = New List(Of Entidades.DetalleDocumentoView)
        setListaDetalle(Me.listaDetalle)

        hddModoFrm.Value = "1"
        actualizarBotonesControl()

        '***************** Remitente
        llenarRemitentexIdPropietario(CInt(Me.cmbEmpresa.SelectedValue))
        cargarDirPartidaxAlmacen(CInt(Me.cmbAlmacen.SelectedValue))
        cargarCboAlmacen_Remitente_Propietario(CInt(Me.cmbAlmacen.SelectedValue))

        Me.txtFecha.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")
        Me.txtFechaTraslado.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy")

        GenerarCodigo()
    End Sub
#End Region
#Region "*************************** CARGAR COMBOS"
    Private Sub cargarDatosSerie(ByVal cbo As DropDownList)
        Dim obj As New Combo
        obj.LLenarCboSeriexIdsEmpTienTipoDoc(cbo, CInt(cmbEmpresa.SelectedValue), CInt(cmbTienda.SelectedValue), CInt(cmbTipoDocumento.SelectedValue))
        cbo.DataBind()
    End Sub
    Private Sub GenerarCodigo()
        Dim obj As New Negocio.Serie
        Try
            If Me.cboSerie.Items.Count = 0 Then
                Me.txtCodigoDocumento.Text = ""
            Else
                Me.txtCodigoDocumento.Text = obj.GenerarCodigo(CInt(Me.cboSerie.SelectedValue))
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosLinea(ByVal comboBox As DropDownList)
        Try
            Dim objCombo As New Combo
            objCombo.LlenarCboLinea(Me.cmbLinea, True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosSubLinea(ByVal comboBox As DropDownList, ByVal idLinea As Integer)
        Try
            Dim objCombo As New Combo
            objCombo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea, CInt(Me.cmbLinea.SelectedValue), True)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#Region "********************** UBIGEO"
    Private Sub cargarDirPartidaxAlmacen(ByVal idalmacen As Integer)
        Try
            Dim obj As New Negocio.Almacen
            Dim objCombo As New Combo
            Dim objAlmacen As Entidades.Almacen
            objAlmacen = obj.SelectxId(idalmacen)
            If objAlmacen.IdAlmacen <> Nothing And objAlmacen.IdAlmacen <> 0 Then
                Dim depto As String = objAlmacen.Ubigeo.Substring(0, 2)
                Dim prov As String = objAlmacen.Ubigeo.Substring(2, 2)
                Dim dist As String = objAlmacen.Ubigeo.Substring(4, 2)
                cmbDeptoPartida.SelectedValue = depto
                objCombo.LLenarCboProvincia(Me.cmbProvPartida, depto)
                objCombo.LLenarCboDistrito(Me.cmbDitritoPartida, depto, prov)
                cmbProvPartida.SelectedValue = prov
                cmbDitritoPartida.SelectedValue = dist
                txtDirPartida.Text = objAlmacen.Direccion
            Else
                cmbDeptoPartida.SelectedValue = "00"
                objCombo.LLenarCboProvincia(Me.cmbProvPartida, "00")
                objCombo.LLenarCboDistrito(Me.cmbDitritoPartida, "00", "00")
                txtDirPartida.Text = ""
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDirLlegadaxAlmacen(ByVal idalmacen As Integer)
        Try
            Dim obj As New Negocio.Almacen
            Dim objCombo As New Combo
            Dim objAlmacen As Entidades.Almacen
            objAlmacen = obj.SelectxId(idalmacen)
            If objAlmacen.IdAlmacen <> Nothing And objAlmacen.IdAlmacen <> 0 Then
                Dim depto As String = objAlmacen.Ubigeo.Substring(0, 2)
                Dim prov As String = objAlmacen.Ubigeo.Substring(2, 2)
                Dim dist As String = objAlmacen.Ubigeo.Substring(4, 2)
                cmbDeptoLlegada.SelectedValue = depto
                objCombo.LLenarCboProvincia(Me.cmbProvLlegada, depto)
                objCombo.LLenarCboDistrito(Me.cmbDistritoLlegada, depto, prov)
                cmbProvLlegada.SelectedValue = prov
                cmbDistritoLlegada.SelectedValue = dist
                txtDirLlegada.Text = objAlmacen.Direccion
            Else
                cmbDeptoLlegada.SelectedValue = "00"
                objCombo.LLenarCboProvincia(Me.cmbProvLlegada, "00")
                objCombo.LLenarCboDistrito(Me.cmbDistritoLlegada, "00", "00")
                txtDirLlegada.Text = ""
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub cmbDeptoLlegada_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbDeptoLlegada.SelectedIndexChanged
        Try
            Dim objCombo As New Combo
            objCombo.LLenarCboProvincia(cmbProvLlegada, Me.cmbDeptoLlegada.SelectedValue)
            objCombo.LLenarCboDistrito(cmbDistritoLlegada, Me.cmbDeptoLlegada.SelectedValue, Me.cmbProvLlegada.SelectedValue)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub cmbProvLlegada_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbProvLlegada.SelectedIndexChanged
        Try
            Dim objCombo As New Combo
            objCombo.LLenarCboDistrito(cmbDistritoLlegada, Me.cmbDeptoLlegada.SelectedValue, Me.cmbProvLlegada.SelectedValue)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region
    Private Sub cargarDatosCboAlmacen(ByVal cbo As DropDownList)
        Try
            Dim objCombo As New Combo
            objCombo.llenarCboAlmacenxIdTienda(cbo, CInt(Me.cmbTienda.SelectedValue), False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub cmbAlmacen_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbAlmacen.SelectedIndexChanged
        If txtIdRemitente.Text.Trim.Length = 0 Or chbPropietario_Rem.Checked Then
            cargarDirPartidaxAlmacen(CInt(Me.cmbAlmacen.SelectedValue))
        End If
        cargarCboAlmacen_Remitente_Propietario(CInt(Me.cmbAlmacen.SelectedValue))
    End Sub
    Private Sub cargarCboAlmacen_Remitente_Propietario(ByVal IdAlmacen As Integer)
        If chbPropietario_Rem.Checked Then
            Me.cmbAlmacen_Remitente.SelectedValue = IdAlmacen.ToString
        End If
    End Sub
    Private Sub cargarDatosCboTienda(ByVal cbo As DropDownList)
        Try
            Dim objCombo As New Combo
            objCombo.LlenarCboTiendaxIdEmpresaxIdUsuario(cbo, CInt(Me.cmbEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
#End Region
    Private Function getListaDetalle() As List(Of Entidades.DetalleDocumentoView)
        Return CType(Session.Item("listaDetalle"), List(Of Entidades.DetalleDocumentoView))
    End Function
    Private Sub setListaDetalle(ByVal lista As List(Of Entidades.DetalleDocumentoView))
        Session.Remove("listaDetalle")
        Session.Add("listaDetalle", lista)
    End Sub
    Private Function getListaDetalleRelacionado() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item("listaDetalleRelacionado"), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleRelacionado(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove("listaDetalleRelacionado")
        Session.Add("listaDetalleRelacionado", lista)
    End Sub
    Private Function getListaDetalleRelacionado_AddProd() As List(Of Entidades.DetalleDocumento)
        Return CType(Session.Item("listaDetalleDocRel_AddProd"), List(Of Entidades.DetalleDocumento))
    End Function
    Private Sub setListaDetalleRelacionado_AddProd(ByVal lista As List(Of Entidades.DetalleDocumento))
        Session.Remove("listaDetalleDocRel_AddProd")
        Session.Add("listaDetalleDocRel_AddProd", lista)
    End Sub
    Protected Sub cmbEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbEmpresa.SelectedIndexChanged
        cargarDatosCboTienda(cmbTienda)
        cargarDatosCboAlmacen(cmbAlmacen)

        cargarDatosSerie(Me.cboSerie)
        GenerarCodigo()
        '****************      Me.txtCodigoDocumento.Text = ""

        '**************** cargo los datos del remitente por defecto
        If txtIdRemitente.Text.Trim.Length = 0 Or chbPropietario_Rem.Checked Then
            llenarRemitentexIdPropietario(CInt(Me.cmbEmpresa.SelectedValue))
            cargarDirPartidaxAlmacen(CInt(Me.cmbAlmacen.SelectedValue))
        End If

        cargarCboAlmacen_Remitente_Propietario(CInt(Me.cmbAlmacen.SelectedValue))

    End Sub
    Private Sub llenarRemitentexIdPropietario(ByVal idpropietario As Integer)
        Try
            Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(idpropietario)
            Dim objCbo As New Combo
            '************** lleno los datos del remitente
            If objPersonaView IsNot Nothing Then
                With objPersonaView
                    txtNombre_Remitente.Text = .getNombreParaMostrar
                    txtDireccion_Remitente.Text = .Direccion
                    txtDNI_Remitente.Text = .Dni
                    txtRUC_Remitente.Text = .Ruc
                    txtIdRemitente.Text = .IdPersona.ToString
                    objCbo.llenarCboAlmacenxIdEmpresa(Me.cmbAlmacen_Remitente, idpropietario, True)
                    chbPropietario_Rem.Checked = .Propietario
                End With
            Else
                txtNombre_Remitente.Text = ""
                txtDireccion_Remitente.Text = ""
                txtDNI_Remitente.Text = ""
                txtRUC_Remitente.Text = ""
                txtIdRemitente.Text = ""
                'objCbo.llenarCboAlmacenxIdEmpresa(Me.cmbAlmacen_Remitente, idpropietario, True)
                Me.cmbAlmacen_Remitente.DataSource = Nothing
                Me.cmbAlmacen_Remitente.DataBind()
                chbPropietario_Rem.Checked = False
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub cmbTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbTienda.SelectedIndexChanged
        Try

            cargarDatosCboAlmacen(cmbAlmacen)
            If txtIdRemitente.Text.Trim.Length = 0 Or chbPropietario_Rem.Checked Then
                cargarDirPartidaxAlmacen(CInt(Me.cmbAlmacen.SelectedValue))
            End If
            cargarDatosSerie(Me.cboSerie)
            Me.txtCodigoDocumento.Text = ""
            GenerarCodigo()
            cargarCboAlmacen_Remitente_Propietario(CInt(Me.cmbAlmacen.SelectedValue))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function ProductoRepetido(ByVal objProducto As Entidades.DetalleDocumentoView) As Boolean
        Me.listaDetalle = Me.getListaDetalle
        For i As Integer = 0 To Me.listaDetalle.Count - 1
            If Me.listaDetalle.Item(i).IdProducto = objProducto.IdProducto Then
                Return True
            End If
        Next
        Return False
    End Function
    Protected Sub DGVDetalle_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGVDetalle.SelectedIndexChanged
        quitarProductoGrllaDetalle()
    End Sub
    Private Sub quitarProductoGrllaDetalle()
        Me.saveGrillaInListaDetalle()
        Me.listaDetalle = Me.getListaDetalle
        Me.listaDetalle.RemoveAt(DGVDetalle.SelectedIndex)
        Me.setListaDetalle(Me.listaDetalle)
        DGVDetalle.DataSource = Me.listaDetalle
        DGVDetalle.DataBind()
    End Sub
    Private Sub saveGrillaInListaDetalle()
        Try
            Me.listaDetalle = Me.getListaDetalle
            For i As Integer = 0 To DGVDetalle.Rows.Count - 1
                With Me.listaDetalle(i)
                    .IdUMedida = CInt(CType(DGVDetalle.Rows(i).Cells(3).FindControl("cmbUMedida"), DropDownList).SelectedValue)
                    .UM = (CType(DGVDetalle.Rows(i).Cells(3).FindControl("cmbUMedida"), DropDownList).SelectedItem.ToString)
                    .Cantidad = CDec(CType(DGVDetalle.Rows(i).Cells(3).FindControl("txtCantidad"), TextBox).Text)
                End With
            Next
            Me.setListaDetalle(Me.listaDetalle)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Protected Sub DGVDetalle_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGVDetalle.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cmbUMedida1 As DropDownList
                Dim gvrow As GridViewRow = CType(e.Row.Cells(3).NamingContainer, GridViewRow)
                cmbUMedida1 = CType(gvrow.FindControl("cmbUMedida"), DropDownList)



                Me.listaDetalle = Me.getListaDetalle
                If cmbUMedida1 IsNot DBNull.Value Then
                    cmbUMedida1.DataSource = Me.listaDetalle.Item(e.Row.RowIndex).ListaProdUM
                    cmbUMedida1.DataBind()
                End If

                cmbUMedida1.SelectedValue = CStr(Me.listaDetalle.Item(e.Row.RowIndex).IdUMedida)

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnBuscarP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarP.Click
        'Panel_BuscarPersona.Visible = True
        Panel_Remitente.Visible = False
        'txtTextoBusqCliente.Text = ""
        'txtTextoBusqCliente.Focus()
    End Sub
    Private Sub LLenarCliente(ByVal cliente As Entidades.PersonaView)
        Me.txtRazonSocial.Text = cliente.RazonSocial
        Me.txtDNI.Text = cliente.Dni
        Me.txtRUC.Text = cliente.Ruc
        Me.txtDireccion.Text = cliente.Direccion
        Me.txtDirLlegada.Text = cliente.Direccion
    End Sub
    Private Function obtenerObjDocumento() As Entidades.Documento
        Dim obj As New Entidades.Documento
        With obj

            '********************************** EL SP GENERA EL CODIGO, SI ES UN UPDATE 
            '********************************** NECESITO EL CODIGO GenerarCodigo()

            .Id = CInt(hddIdDocumento.Value)
            .Codigo = txtCodigoDocumento.Text
            .Serie = cboSerie.SelectedItem.ToString
            .FechaEmision = CDate(txtFecha.Text)
            .FechaIniTraslado = CDate(txtFechaTraslado.Text)
            .IdRemitente = CInt(txtIdRemitente.Text)
            .IdDestinatario = CInt(txtIdPersona.Text)


            .IdEstadoDoc = CInt(cmbEstadoDocumento.SelectedValue)

            '********* Si es Editar ESTADO = ACTIVO
            If (CInt(hddModoFrm.Value) = 2) Then .IdEstadoDoc = 1


            .IdTienda = CInt(cmbTienda.SelectedValue)
            .IdEmpresa = CInt(cmbEmpresa.SelectedValue)
            .IdSerie = CInt(cboSerie.SelectedValue)
            .IdMotivoT = CInt(cmbMotivoTraslado.SelectedValue)
            .IdTipoDocumento = CInt(cmbTipoDocumento.SelectedValue)
            .IdEstadoEntrega = CInt(Me.cmbEstadoEntrega.SelectedValue)
            .IdPersona = CInt(txtIdPersona.Text)

            '************ obtengo los datos del transportista
            If IsNumeric(txtIdTransportista.Text) Then .IdTransportista = CInt(txtIdTransportista.Text)
            If IsNumeric(txtIdChofer.Text) Then .IdChofer = CInt(txtIdChofer.Text)
            If IsNumeric(txtIdVehiculo.Text) Then .IdVehiculo = CInt(txtIdVehiculo.Text)

            If Session("IdPersona") IsNot Nothing Then .IdUsuario = CInt(Session("IdPersona"))

            .IdTipoOperacion = CInt(cmbTipoOperacion.SelectedValue)
            .FactorMov = -1
            .IdAlmacen = CInt(Me.cmbAlmacen.SelectedValue)

        End With
        Return obj
    End Function

    Private Function obtenerPuntoPartida() As Entidades.PuntoPartida
        Dim obj As New Entidades.PuntoPartida
        With obj
            If txtDirPartida.Text.Trim.Length > 0 Then .Direccion = txtDirPartida.Text.Trim
            If CInt(Me.cmbAlmacen_Remitente.SelectedValue) <> 0 Then .IdAlmacen = CInt(Me.cmbAlmacen_Remitente.SelectedValue)
            .Ubigeo = Me.cmbDeptoPartida.SelectedValue & Me.cmbProvPartida.SelectedValue & Me.cmbDitritoPartida.SelectedValue
        End With
        Return obj
    End Function
    Private Function obtenerPuntoLlegada() As Entidades.PuntoLlegada
        Dim obj As New Entidades.PuntoLlegada
        With obj
            If txtDirLlegada.Text.Trim.Length > 0 Then .pll_Direccion = txtDirLlegada.Text.Trim
            If CInt(Me.cmbAlmacen_Destinatario.SelectedValue) <> 0 Then .IdAlmacen = CInt(Me.cmbAlmacen_Destinatario.SelectedValue)
            .pll_Ubigeo = Me.cmbDeptoLlegada.SelectedValue & Me.cmbProvLlegada.SelectedValue & Me.cmbDistritoLlegada.SelectedValue
        End With
        Return obj
    End Function
#Region "BOTONES DE CONTROL"
    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNuevo.Click
        verFrnNuevo()

    End Sub
    Private Sub limpiarFrm()
        txtBuscarDestinatarioGrilla.Text = ""
        txtBuscarPersonaGrilla.Text = ""
        txtBuscarVehiculo.Text = ""
        txtCategoria_Trans.Text = ""
        txtCertInscripcion_Trans.Text = ""
        txtCodigoBuscarDoc.Text = ""
        Me.lblCodigo_DocRef.Text = ""
        txtCodigoDocumento.Text = ""
        txtDescripcionProd.Text = ""
        txtDireccion.Text = ""
        txtDireccion_Remitente.Text = ""
        txtDirLlegada.Text = ""
        txtDirPartida.Text = ""
        txtDNI.Text = ""
        txtDNI_Remitente.Text = ""
        'txtFecha.Text = ""
        'txtFechaTraslado.Text = ""
        txtIdChofer.Text = ""
        Me.hddIdDocumentoRef.Value = ""
        txtIdPersona.Text = ""
        txtIdRemitente.Text = ""
        txtIdTransportista.Text = ""
        txtIdVehiculo.Text = ""
        txtMarcaVeh_Trans.Text = ""
        txtNombre_Remitente.Text = ""
        txtNomChofer_Trans.Text = ""
        txtNroLicencia_Trans.Text = ""
        txtNroPlaca_Trans.Text = ""
        txtObservaciones.Text = ""
        txtRazonSocial.Text = ""
        txtRazonSocial_Trans.Text = ""
        txtRUC.Text = ""
        txtRUC_Trans.Text = ""
        txtSerieBuscarDoc.Text = ""
        Me.lblSerie_DocRef.Text = ""
        txtTextoBusqChofer.Text = ""
        txtTextoBusquedaTransportista.Text = ""

        '******************** limpiamos las grillas
        DGV_BuscarDoc.DataSource = Nothing
        DGV_BuscarDoc.DataBind()
        DGV_BuscarPersona.DataSource = Nothing
        DGV_BuscarPersona.DataBind()
        DGV_BuscarTransportista.DataSource = Nothing
        DGV_BuscarTransportista.DataBind()
        DGV_Chofer.DataSource = Nothing
        DGV_Chofer.DataBind()
        DGV_Vehiculo.DataSource = Nothing
        DGV_Vehiculo.DataBind()
        DGVDetalle.DataSource = Nothing
        DGVDetalle.DataBind()
        DGVSelProd.DataSource = Nothing
        DGVSelProd.DataBind()

        DGV_DetalleRel_AddProd.DataSource = Nothing
        DGV_DetalleRel_AddProd.DataBind()

        chbPropietario_Rem.Checked = False
        chbPropietario_Dest.Checked = False


        cmbAlmacen_Destinatario.Items.Clear()
        hddPoseeOrdenDespacho.Value = "0"
        hddIdDocumento.Value = "0"

    End Sub



    Protected Sub btnCancelar1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar1.Click


        verFrmInicio()
    End Sub
#End Region
    Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGrabar.Click

        '************* verificamos si es doc. comprometido
        Dim objMotivoT As New Negocio.MotivoTraslado
        Dim comprometedor As Boolean = objMotivoT.SelectComprometedorxIdMotivoT(CInt(Me.cmbMotivoTraslado.SelectedValue))
        If Not comprometedor And chbPropietario_Rem.Checked Then
            If Me.hddIdDocumentoRef.Value.Trim.Length = 0 Then
                objScript.mostrarMsjAlerta(Me, "El Motivo de Traslado seleccionado requiere de un documento relacionado al cual hacer referencia.")
                Return
            End If
        End If

        'If (Not chbPropietario_Rem.Checked) Then comprometedor = False

        Me.Agregar(comprometedor)

    End Sub






    Private Sub DGVSelProd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGVSelProd.PageIndexChanging
        DGVSelProd.PageIndex = e.NewPageIndex
        cargarDatosGrillaAddProd()
    End Sub
    Private Sub cargarDatosGrillaAddProd()
        Dim nProducto As New Negocio.Producto
        Dim idLinea As Integer = 0
        Dim idSubLinea As Integer = 0
        If cmbLinea.SelectedValue <> "0" Then idLinea = CInt(cmbLinea.SelectedValue)
        If cmbSubLinea.SelectedValue <> "0" Then idSubLinea = CInt(cmbSubLinea.SelectedValue)
        Dim lista As List(Of Entidades.GrillaProducto_M) = nProducto.SelectGrillaProd_M(idLinea, idSubLinea, 0, 0, txtDescripcionProd.Text.Trim)
        If lista.Count = 0 Then

        Else

        End If
        DGVSelProd.DataSource = lista
        DGVSelProd.DataBind()
        txtDescripcionProd.Focus()
    End Sub
    Private Sub cmbLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea.SelectedIndexChanged
        cargarDatosSubLinea(Me.cmbSubLinea, CInt(cmbLinea.SelectedValue))
    End Sub

    Private Sub addProductoGrllaDetalle()
        Try
            Me.saveGrillaInListaDetalle()

            '*********  agrego el detalle documento
            Me.listaDetalle = Me.getListaDetalle
            For i As Integer = 0 To DGVSelProd.Rows.Count - 1
                Dim chbAdd As CheckBox = CType(DGVSelProd.Rows(i).Cells(0).FindControl("chbSelectProducto"), CheckBox)
                If chbAdd.Checked Then
                    Dim obj As New Entidades.DetalleDocumentoView
                    Dim objPUM As New Negocio.ProductoUMView

                    obj.IdProducto = CInt(DGVSelProd.Rows(i).Cells(1).Text)
                    obj.Descripcion = CStr(DGVSelProd.Rows(i).Cells(2).Text)
                    obj.IdUMPrincipal = CInt(DGVSelProd.Rows(i).Cells(6).Text)
                    obj.NomUMPrincipal = CStr(DGVSelProd.Rows(i).Cells(3).Text)

                    obj.ListaProdUM = objPUM.SelectxIdProducto(obj.IdProducto)
                    Me.listaDetalle.Add(obj)
                End If
            Next

            DGVDetalle.DataSource = Me.listaDetalle
            DGVDetalle.DataBind()

            Me.setListaDetalle(Me.listaDetalle)
            '****** objScript.offCapa(Me, "capaBuscarP")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cboSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSerie.SelectedIndexChanged
        GenerarCodigo()
    End Sub
    Private Sub cargarDetalleFrmDocRelacional(ByVal IdDocumento As Integer)

        Try

            Dim objCombo As New Combo

            '******************************** obtengo el objDocumento
            Dim objDocumento As Entidades.Documento = (New Negocio.Documento).SelectActivoxIdDocumento(IdDocumento)
            If objDocumento Is Nothing Then
                objScript.mostrarMsjAlerta(Me, "El Documento no se encuentra registrado.")
                Return
            End If

            Me.lblTipoDocumento_DocRef.Text = objDocumento.NomTipoDocumento
            Me.lblSerie_DocRef.Text = objDocumento.Serie
            Me.lblCodigo_DocRef.Text = objDocumento.Codigo
            Me.hddIdDocumentoRef.Value = objDocumento.Id.ToString

            '******************************** obtengo el destinatario
            Dim objPersonaView As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdDestinatario)
            If objPersonaView IsNot Nothing Then

                txtRazonSocial.Text = objPersonaView.getNombreParaMostrar
                txtIdPersona.Text = objPersonaView.IdPersona.ToString
                chbPropietario_Dest.Checked = objPersonaView.Propietario
                txtDNI.Text = objPersonaView.Dni
                txtRUC.Text = objPersonaView.Ruc
                txtDireccion.Text = objPersonaView.Direccion

                objCombo.llenarCboAlmacenxIdEmpresa(Me.cmbAlmacen_Destinatario, objDocumento.IdDestinatario, True)

                '********************* cargo el Punto de Llegada
                txtDirLlegada.Text = objPersonaView.Direccion
                Me.cmbDeptoLlegada.SelectedValue = objPersonaView.Ubigeo.Substring(0, 2)
                objCombo.LLenarCboProvincia(Me.cmbProvLlegada, Me.cmbDeptoLlegada.SelectedValue)
                Me.cmbProvLlegada.SelectedValue = objPersonaView.Ubigeo.Substring(2, 2)
                objCombo.LLenarCboDistrito(Me.cmbDistritoLlegada, Me.cmbDeptoLlegada.SelectedValue, Me.cmbProvLlegada.SelectedValue)
                Me.cmbDistritoLlegada.SelectedValue = objPersonaView.Ubigeo.Substring(4, 2)

            End If

            '************************************* obtengo el remitente
            Dim objPersonaView_Rem As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdEmpresa)
            If objPersonaView IsNot Nothing Then

                txtNombre_Remitente.Text = objPersonaView_Rem.getNombreParaMostrar
                txtIdRemitente.Text = objPersonaView_Rem.IdPersona.ToString
                chbPropietario_Rem.Checked = objPersonaView_Rem.Propietario
                txtDNI_Remitente.Text = objPersonaView_Rem.Dni
                txtRUC_Remitente.Text = objPersonaView_Rem.Ruc
                txtDireccion_Remitente.Text = objPersonaView_Rem.Direccion

                objCombo.llenarCboAlmacenxIdEmpresa(Me.cmbAlmacen_Remitente, objDocumento.IdEmpresa, True)
                Me.cmbAlmacen_Remitente.SelectedValue = objDocumento.IdAlmacen.ToString

                '********************* cargo el Punto de Partida
                cargarDirPartidaxAlmacen(objDocumento.IdAlmacen)

            End If

            '************** Cargamos el detalle
            Me.listaDetalle = obtenerListaDetalle_Relacional(objDocumento.Id)

            setListaDetalle(Me.listaDetalle)

            Me.DGVDetalle.DataSource = Me.listaDetalle
            Me.DGVDetalle.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerListaDetalle_Relacional(ByVal IdDocumento As Integer) As List(Of Entidades.DetalleDocumentoView)

        Dim lista As List(Of Entidades.DetalleDocumentoView) = (New Negocio.DetalleDocumento).DetalleDocumentoViewSelectxIdDocumento(IdDocumento)
        For i As Integer = 0 To lista.Count - 1
            Dim listaProductoUM As List(Of Entidades.ProductoUMView) = (New Negocio.ProductoUMView).SelectCboxIdProducto(lista(i).IdProducto)
            lista(i).ListaProdUM = listaProductoUM
        Next

        Return lista
    End Function

    Private Sub btnBuscarDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDocumento.Click
        cargarRegistrosBuscarDocumento(CInt(txtSerieBuscarDoc.Text), CInt(txtCodigoBuscarDoc.Text))
    End Sub
    Private Sub cargarRegistrosBuscarDocumento(ByVal serie As Integer, ByVal codigo As Integer)
        Try
            Dim lista As List(Of Entidades.DocumentoView) = (New Negocio.DocumentoView).DocumentoBuscarDocxSeriexCodigo(serie, codigo, CInt(Me.cmbTipoOperacion.SelectedValue))
            If (lista.Count = 0) Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            Else
                DGV_BuscarDoc.DataSource = lista
                DGV_BuscarDoc.DataBind()

                objScript.onCapa(Me, "capaBuscarDocumento")

            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub DGV_BuscarDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_BuscarDoc.SelectedIndexChanged
        cargarDetalleFrmDocRelacional(CInt(CType(Me.DGV_BuscarDoc.SelectedRow.FindControl("hddIdDocumento_Find"), HiddenField).Value))
        objScript.offCapa(Me, "capaBuscarDocumento")
    End Sub
    Protected Sub btnBuscarTransportista_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarTransportista.Click
        cargarDatosGrillaTransportista()
    End Sub
    Private Sub cargarDatosGrillaTransportista()
        Try
            Dim objJuridicaNegocio As New Negocio.Juridica
            DGV_BuscarTransportista.DataSource = objJuridicaNegocio.SelectActivoxRazonSocial(txtTextoBusquedaTransportista.Text.Trim)
            DGV_BuscarTransportista.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub DGV_BuscarTransportista_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_BuscarTransportista.PageIndexChanging
        DGV_BuscarTransportista.PageIndex = e.NewPageIndex
        cargarDatosGrillaTransportista()
    End Sub
    Private Sub DGV_BuscarTransportista_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_BuscarTransportista.SelectedIndexChanged
        addTransportista()
    End Sub
    Private Sub addTransportista()
        Try
            txtRazonSocial_Trans.Text = DGV_BuscarTransportista.SelectedRow.Cells(2).Text
            txtRUC_Trans.Text = DGV_BuscarTransportista.SelectedRow.Cells(3).Text
            txtIdTransportista.Text = DGV_BuscarTransportista.SelectedRow.Cells(1).Text
            objScript.offCapa(Me, "capaTransportista")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub DGV_Chofer_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_Chofer.PageIndexChanging
        DGV_Chofer.PageIndex = e.NewPageIndex
        cargarDatosGrillaChofer()
    End Sub
    Private Sub cargarDatosGrillaChofer()
        Try
            Dim objChoferNegocio As New Negocio.Chofer
            DGV_Chofer.DataSource = objChoferNegocio.SelectActivoxNombre(txtTextoBusqChofer.Text.Trim)
            DGV_Chofer.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub btnBuscarChofer_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarChofer.Click
        cargarDatosGrillaChofer()
    End Sub
    Private Sub DGV_Chofer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_Chofer.SelectedIndexChanged
        addChofer()
    End Sub
    Private Sub addChofer()
        Try
            txtNomChofer_Trans.Text = DGV_Chofer.SelectedRow.Cells(2).Text
            txtIdChofer.Text = DGV_Chofer.SelectedRow.Cells(1).Text
            txtNroLicencia_Trans.Text = DGV_Chofer.SelectedRow.Cells(4).Text
            txtCategoria_Trans.Text = DGV_Chofer.SelectedRow.Cells(5).Text
            objScript.offCapa(Me, "capaChofer")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnBuscarVehiculoGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarVehiculoGrilla.Click
        cargarDatosVehiculoGrilla()
    End Sub
    Private Sub cargarDatosVehiculoGrilla()
        Try
            Dim objVehiculo As New Negocio.Vehiculo
            DGV_Vehiculo.DataSource = objVehiculo.SelectActivoxNroPlaca(txtBuscarVehiculo.Text.Trim)
            DGV_Vehiculo.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub DGV_Vehiculo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_Vehiculo.PageIndexChanging
        DGV_Vehiculo.PageIndex = e.NewPageIndex
        cargarDatosVehiculoGrilla()
    End Sub
    Private Sub DGV_Vehiculo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_Vehiculo.SelectedIndexChanged
        addVehiculo()
    End Sub
    Private Sub addVehiculo()
        Try
            txtMarcaVeh_Trans.Text = DGV_Vehiculo.SelectedRow.Cells(3).Text
            txtIdVehiculo.Text = DGV_Vehiculo.SelectedRow.Cells(1).Text
            txtNroPlaca_Trans.Text = DGV_Vehiculo.SelectedRow.Cells(2).Text
            txtCertInscripcion_Trans.Text = DGV_Vehiculo.SelectedRow.Cells(4).Text
            objScript.offCapa(Me, "capaVehiculo")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarPersonaGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarPersonaGrilla.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, txtBuscarPersonaGrilla.Text.Trim)
    End Sub

    Private Sub cargarDatosPersonaGrilla(ByVal grilla As GridView, ByVal nombre As String)
        Try
            Dim objPersonaView As New Negocio.PersonaView
            grilla.DataSource = objPersonaView.SelectActivoxNombre(nombre)
            grilla.DataBind()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub DGV_BuscarPersona_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_BuscarPersona.PageIndexChanging
        DGV_BuscarPersona.PageIndex = e.NewPageIndex
        cargarDatosPersonaGrilla(Me.DGV_BuscarPersona, txtBuscarPersonaGrilla.Text.Trim)
    End Sub
    Private Sub DGV_BuscarPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_BuscarPersona.SelectedIndexChanged
        addRemitente()
    End Sub
    Private Sub addRemitente()
        Try
            txtNombre_Remitente.Text = HttpUtility.HtmlDecode(DGV_BuscarPersona.SelectedRow.Cells(2).Text)
            txtIdRemitente.Text = HttpUtility.HtmlDecode(DGV_BuscarPersona.SelectedRow.Cells(1).Text)
            txtDNI_Remitente.Text = HttpUtility.HtmlDecode(DGV_BuscarPersona.SelectedRow.Cells(3).Text)
            txtRUC_Remitente.Text = HttpUtility.HtmlDecode(DGV_BuscarPersona.SelectedRow.Cells(4).Text)
            txtDireccion_Remitente.Text = HttpUtility.HtmlDecode(DGV_BuscarPersona.SelectedRow.Cells(5).Text)

            chbPropietario_Rem.Checked = CType(DGV_BuscarPersona.SelectedRow.Cells(7).FindControl("chbPropietario_R"), CheckBox).Checked

            Dim ubigeo As String = HttpUtility.HtmlDecode(DGV_BuscarPersona.SelectedRow.Cells(6).Text)

            '*********************** lleno el punto partida
            Dim objCombo As New Combo
            txtDirPartida.Text = HttpUtility.HtmlDecode(DGV_BuscarPersona.SelectedRow.Cells(5).Text)
            cmbDeptoPartida.SelectedValue = ubigeo.Substring(0, 2)
            objCombo.LLenarCboProvincia(Me.cmbProvPartida, ubigeo.Substring(0, 2))
            cmbProvPartida.SelectedValue = ubigeo.Substring(2, 2)
            objCombo.LLenarCboDistrito(Me.cmbDitritoPartida, ubigeo.Substring(0, 2), ubigeo.Substring(2, 2))
            cmbDitritoPartida.SelectedValue = ubigeo.Substring(4, 2)

            '************************** lleno los almacenes
            objCombo.llenarCboAlmacenxIdEmpresa(Me.cmbAlmacen_Remitente, CInt(DGV_BuscarPersona.SelectedRow.Cells(1).Text), True)



            objScript.offCapa(Me, "capaRemitente")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnBuscarDestinatarioGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarDestinatarioGrilla.Click
        cargarDatosPersonaGrilla(Me.DGV_BuscarDestinatario, txtBuscarDestinatarioGrilla.Text.Trim)
    End Sub
    Private Sub DGV_BuscarDestinatario_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles DGV_BuscarDestinatario.PageIndexChanging
        DGV_BuscarDestinatario.PageIndex = e.NewPageIndex
        cargarDatosPersonaGrilla(Me.DGV_BuscarDestinatario, txtBuscarDestinatarioGrilla.Text.Trim)
    End Sub
    Private Sub DGV_BuscarDestinatario_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_BuscarDestinatario.SelectedIndexChanged
        addDestinatario()
    End Sub

    Private Sub addDestinatario()
        Try



            txtRazonSocial.Text = HttpUtility.HtmlDecode(DGV_BuscarDestinatario.SelectedRow.Cells(2).Text)
            txtIdPersona.Text = HttpUtility.HtmlDecode(DGV_BuscarDestinatario.SelectedRow.Cells(1).Text)
            txtDNI.Text = HttpUtility.HtmlDecode(DGV_BuscarDestinatario.SelectedRow.Cells(3).Text)
            txtRUC.Text = HttpUtility.HtmlDecode(DGV_BuscarDestinatario.SelectedRow.Cells(4).Text)
            txtDireccion.Text = HttpUtility.HtmlDecode(DGV_BuscarDestinatario.SelectedRow.Cells(5).Text)

            chbPropietario_Dest.Checked = CType(DGV_BuscarDestinatario.SelectedRow.Cells(7).FindControl("chbPropietario_D"), CheckBox).Checked

            Dim ubigeo As String = HttpUtility.HtmlDecode(DGV_BuscarDestinatario.SelectedRow.Cells(6).Text)

            '*********************** lleno el punto partida
            Dim objCombo As New Combo
            txtDirLlegada.Text = HttpUtility.HtmlDecode(DGV_BuscarDestinatario.SelectedRow.Cells(5).Text)
            cmbDeptoLlegada.SelectedValue = ubigeo.Substring(0, 2)
            objCombo.LLenarCboProvincia(Me.cmbProvLlegada, ubigeo.Substring(0, 2))
            cmbProvLlegada.SelectedValue = ubigeo.Substring(2, 2)
            objCombo.LLenarCboDistrito(Me.cmbDistritoLlegada, ubigeo.Substring(0, 2), ubigeo.Substring(2, 2))
            cmbDistritoLlegada.SelectedValue = ubigeo.Substring(4, 2)

            '************************ lleno el almacen
            objCombo.llenarCboAlmacenxIdEmpresa(Me.cmbAlmacen_Destinatario, CInt(DGV_BuscarDestinatario.SelectedRow.Cells(1).Text), True)
            objScript.offCapa(Me, "capaDestinatario")


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbAlmacen_Remitente_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAlmacen_Remitente.SelectedIndexChanged
        cargarDirPartidaxAlmacen(CInt(Me.cmbAlmacen_Remitente.SelectedValue))
    End Sub

    Private Sub cmbAlmacen_Destinatario_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAlmacen_Destinatario.SelectedIndexChanged
        cargarDirLlegadaxAlmacen(CInt(Me.cmbAlmacen_Destinatario.SelectedValue))
    End Sub

    Private Sub mostrarBotones(ByVal modo As Integer)
        '**** modo=0 : Inicio
        '**** modo=1 : Nuevo
        '**** modo=2 : Documento Guardado con éxito
        Select Case modo
            Case 0
                btnNuevo.Visible = True
                btnGrabar.Visible = False
                btnCancelar1.Visible = False
                btnImprimir.Visible = False
                btnDespachar.Visible = False
            Case 1
                btnNuevo.Visible = False
                btnGrabar.Visible = True
                btnCancelar1.Visible = True
                btnImprimir.Visible = False
                btnDespachar.Visible = False
            Case 2
                btnNuevo.Visible = False
                btnGrabar.Visible = False
                btnCancelar1.Visible = True
                btnImprimir.Visible = True
                If CInt(hddAccion.Value) = 0 Then '********* NO Relacionado
                    btnDespachar.Visible = True
                ElseIf CInt(hddAccion.Value) = 1 Then '********* Doc. RELACIONADO
                    btnDespachar.Visible = False
                End If
        End Select
    End Sub

    Private Sub cmbDeptoPartida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbDeptoPartida.SelectedIndexChanged
        Try
            Dim objCombo As New Combo
            objCombo.LLenarCboProvincia(cmbProvPartida, Me.cmbDeptoPartida.SelectedValue)
            objCombo.LLenarCboDistrito(cmbDitritoPartida, Me.cmbDeptoPartida.SelectedValue, Me.cmbProvPartida.SelectedValue)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cmbProvPartida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbProvPartida.SelectedIndexChanged
        Try
            Dim objCombo As New Combo
            objCombo.LLenarCboDistrito(cmbDitritoPartida, Me.cmbDeptoPartida.SelectedValue, Me.cmbProvPartida.SelectedValue)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub btnAddProductos_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddProductos.Click
        addProductoGrllaDetalle()
    End Sub


#Region "************************** BUSQUEDA PRODUCTO"
    Protected Sub btnBuscarGrilla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla.Click
        ViewState.Add("nombre_BuscarProducto", txtDescripcionProd.Text)
        ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea.SelectedValue)
        ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea.SelectedValue)

        If txtCodSubLinea_BuscarProd.Text.Trim.Length > 0 Then
            ViewState.Add("CodigoSubLinea_BuscarProducto", txtCodSubLinea_BuscarProd.Text)
        Else
            ViewState.Add("CodigoSubLinea_BuscarProducto", 0)
        End If

        cargarDatosProductoGrilla(Me.DGVSelProd, txtDescripcionProd.Text, CInt(Me.cmbLinea.SelectedValue), CInt(Me.cmbSubLinea.SelectedValue), CInt(ViewState("CodigoSubLinea_BuscarProducto")), 0)
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal nomProducto As String, ByVal idlinea As Integer, ByVal idsublinea As Integer, ByVal codSubLinea As Integer, ByVal tipomov As Integer)
        Try

            Dim index As Integer = 0
            Select Case tipomov
                Case 0 '**************** INICIO
                    index = 0
                Case 1 '**************** Anterior
                    index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
                Case 2 '**************** Posterior
                    index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
                Case 3 '**************** IR
                    index = (CInt(txtPageIndexGO_Productos.Text) - 1)
            End Select

            Dim lista As List(Of Entidades.GrillaProducto_M) = (New Negocio.Producto).SelectBusquedaProdxParams_Standar_Paginado(idlinea, idsublinea, nomProducto, codSubLinea, index, grilla.PageSize)

            If lista.Count <= 0 Then
                objScript.mostrarMsjAlerta(Me, "No se hallaron registros.")
            Else
                grilla.DataSource = lista
                grilla.DataBind()
                txtPageIndex_Productos.Text = CStr(index + 1)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        cargarDatosProductoGrilla(Me.DGVSelProd, CStr(ViewState("nombre_BuscarProducto")), CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CInt(ViewState("CodigoSubLinea_BuscarProducto")), 1)
    End Sub
    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        cargarDatosProductoGrilla(Me.DGVSelProd, CStr(ViewState("nombre_BuscarProducto")), CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CInt(ViewState("CodigoSubLinea_BuscarProducto")), 2)
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        cargarDatosProductoGrilla(Me.DGVSelProd, CStr(ViewState("nombre_BuscarProducto")), CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CInt(ViewState("CodigoSubLinea_BuscarProducto")), 3)
    End Sub
#End Region

    Private Sub btnBuscarGRem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGRem.Click

        hddModoFrm.Value = "3"
        actualizarBotonesControl()

    End Sub

    Private Sub btnBuscarGRem_1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGRem_1.Click

        cargarGuiaRemision()

    End Sub
    Private Sub cargarGuiaRemision()

        Try

            Dim objCbo As New Combo

            Dim objDocumento As Entidades.DocGuiaRemision = (New Negocio.DocGuiaRemision).DocumentoGuiaRemisionSelectxParams(CInt(Me.cboSerie.SelectedValue), CInt(Me.txtCodigoDocumento.Text))

            If objDocumento Is Nothing Then

                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS.")

                Return

            End If

            hddIdDocumento.Value = CStr(objDocumento.Id)


            Me.hddPoseeOrdenDespacho.Value = CStr(IIf(objDocumento.PoseeOrdenDespacho = True, "1", "0"))

            If (objDocumento.IdAlmacen <> 0) Then
                Me.cmbAlmacen.SelectedValue = CStr(objDocumento.IdAlmacen)
            End If


            Me.cmbMotivoTraslado.SelectedValue = CStr(objDocumento.IdMotivoT)
            objCbo.llenarCboTipoOperacionxIdMotivoT(Me.cmbTipoOperacion, CInt(Me.cmbMotivoTraslado.SelectedValue), True)
            Me.cmbTipoOperacion.SelectedValue = CStr(objDocumento.IdTipoOperacion)

            Me.cmbEstadoDocumento.SelectedValue = CStr(objDocumento.IdEstadoDoc)

            If (objDocumento.IdEstadoEntrega <> Nothing) Then
                Me.cmbEstadoEntrega.SelectedValue = CStr(objDocumento.IdEstadoEntrega)
            End If

            Me.txtFecha.Text = Format(objDocumento.FechaEmision, "dd/MM/yyyy")
            Me.txtFechaTraslado.Text = Format(objDocumento.FechaIniTraslado, "dd/MM/yyyy")

            '******************* Cargamos el REMITENTE
            Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdRemitente)
            If objPersona IsNot Nothing Then
                txtDireccion_Remitente.Text = objPersona.Direccion
                txtIdRemitente.Text = CStr(objPersona.IdPersona)
                txtDNI_Remitente.Text = CStr(objPersona.Dni)
                txtRUC_Remitente.Text = CStr(objPersona.Ruc)
                txtNombre_Remitente.Text = objPersona.getNombreParaMostrar
                chbPropietario_Rem.Checked = objPersona.Propietario
                objCbo.llenarCboAlmacenxIdEmpresa(Me.cmbAlmacen_Remitente, objPersona.IdPersona, True)
            End If


            '***************** LLENAMOS LOS PUNTOS DE PARTIDA 

            Dim objPuntoPartida As Entidades.PuntoPartida = (New Negocio.PuntoPartida).SelectxIdDocumento(objDocumento.Id)
            Dim objUBIGEO As Entidades.Ubigeo = Nothing

            If objPuntoPartida IsNot Nothing Then

                Me.txtDirPartida.Text = objPuntoPartida.Direccion

                objUBIGEO = New Entidades.Ubigeo(objPuntoPartida.Ubigeo)

                Me.cmbDeptoPartida.SelectedValue = objUBIGEO.CodDpto
                objCbo.LLenarCboProvincia(Me.cmbProvPartida, objUBIGEO.CodDpto)
                Me.cmbProvPartida.SelectedValue = objUBIGEO.CodProv
                objCbo.LLenarCboDistrito(Me.cmbDitritoPartida, objUBIGEO.CodDpto, objUBIGEO.CodProv)
                Me.cmbDitritoPartida.SelectedValue = objUBIGEO.CodDist

                Me.cmbAlmacen_Remitente.SelectedValue = CStr(objPuntoPartida.IdAlmacen)

            End If


            '******************** Cargamos el DESTINATARIO y el punto de llegada
            objPersona = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdDestinatario)

            If objPersona IsNot Nothing Then
                txtDireccion.Text = objPersona.Direccion
                txtIdPersona.Text = CStr(objPersona.IdPersona)
                txtDNI.Text = objPersona.Dni
                txtRUC.Text = objPersona.Ruc

                txtRazonSocial.Text = objPersona.getNombreParaMostrar
                chbPropietario_Dest.Checked = objPersona.Propietario
                objCbo.llenarCboAlmacenxIdEmpresa(Me.cmbAlmacen_Destinatario, objPersona.IdPersona, True)
            End If




            Dim objPuntoLlegada As Entidades.PuntoLlegada = (New Negocio.PuntoLlegada).SelectxIdDocumento(objDocumento.Id)


            If objPuntoLlegada IsNot Nothing Then

                Me.txtDirLlegada.Text = objPuntoLlegada.pll_Direccion

                objUBIGEO = New Entidades.Ubigeo(objPuntoPartida.Ubigeo)

                Me.cmbDeptoLlegada.SelectedValue = objUBIGEO.CodDpto
                objCbo.LLenarCboProvincia(Me.cmbProvLlegada, objUBIGEO.CodDpto)
                Me.cmbProvLlegada.SelectedValue = objUBIGEO.CodProv
                objCbo.LLenarCboDistrito(Me.cmbDistritoLlegada, objUBIGEO.CodDpto, objUBIGEO.CodProv)
                Me.cmbDistritoLlegada.SelectedValue = objUBIGEO.CodDist
                Me.cmbAlmacen_Destinatario.SelectedValue = CStr(objPuntoLlegada.IdAlmacen)
            End If



            '*************** CARGAMOS DATOS TRANSPORTISTA

            objPersona = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdTransportista)
            If objPersona IsNot Nothing Then

                txtRazonSocial_Trans.Text = objPersona.getNombreParaMostrar
                txtIdTransportista.Text = CStr(objPersona.IdPersona)
                txtRUC_Trans.Text = objPersona.Ruc

            End If

            '*************** CARGAMOS DATOS CHOFER      

            objPersona = (New Negocio.PersonaView).SelectxIdPersona(objDocumento.IdChofer)
            If objPersona IsNot Nothing Then

                txtNomChofer_Trans.Text = objPersona.getNombreParaMostrar
                txtIdChofer.Text = CStr(objPersona.IdPersona)

            End If

            '****************** CARGAMOS EL VEHICULO
            Dim objVehiculo As Entidades.Vehiculo = (New Negocio.Vehiculo).SelectxIdVehiculo(objDocumento.IdVehiculo)
            If objVehiculo IsNot Nothing Then

                txtMarcaVeh_Trans.Text = objVehiculo.Modelo
                txtIdVehiculo.Text = CStr(objVehiculo.IdProducto)
                txtNroPlaca_Trans.Text = objVehiculo.Placa
                txtCertInscripcion_Trans.Text = objVehiculo.ConstanciaIns

            End If

            '***************** CARGAMOS LAS OBSERVACIONES

            Dim objObservacion As Entidades.Observacion = (New Negocio.Observacion).SelectxIdDocumento(objDocumento.Id)
            If objObservacion IsNot Nothing Then

                txtObservaciones.Text = objObservacion.Observacion

            End If



            '******************* CARGAMOS EL DETALLE
            Me.listaDetalle = (New Negocio.DetalleDocumento).DetalleDocumentoViewSelectxIdDocumento(objDocumento.Id)


            For i As Integer = 0 To Me.listaDetalle.Count - 1

                Me.listaDetalle(i).ListaProdUM = (New Negocio.ProductoUMView).SelectCboxIdProducto(Me.listaDetalle(i).IdProducto)

            Next


            setListaDetalle(Me.listaDetalle)

            Me.DGVDetalle.DataSource = Me.listaDetalle
            Me.DGVDetalle.DataBind()


            '******************* CARGAMOS LOS DATOS DEL DOCUMENTO RELACIONADO
            Me.hddIdDocumentoRef.Value = CStr(objDocumento.IdDocumentoRef)
            Me.lblSerie_DocRef.Text = objDocumento.SerieDocumentoRef
            Me.lblCodigo_DocRef.Text = objDocumento.CodigoDocumentoRef
            Me.lblTipoDocumento_DocRef.Text = objDocumento.TipoDocumentoRef


            '****************** VALIDAMOS EL MOTIVO DE TRASLADO COMPROMETEDOR
            If ((New Negocio.MotivoTraslado).SelectComprometedorxIdMotivoT(CInt(Me.cmbMotivoTraslado.SelectedValue))) Then
                Me.lblAlertaComprometerStock.Visible = True
            Else
                Me.lblAlertaComprometerStock.Visible = False
            End If

            hddModoFrm.Value = "4"

            actualizarBotonesControl()

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try

    End Sub

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditar.Click
        hddModoFrm.Value = "2"
        actualizarBotonesControl()
    End Sub


    Private Sub Agregar(ByVal comprometedor As Boolean)
        Try
            Dim listaMovAlmacen As New List(Of Entidades.MovAlmacen)

            Select Case CInt(hddAccion.Value)

                Case 0 '******************** Documento NO Relacionado

                    saveGrillaInListaDetalle()

                    Me.listaDetalle = getListaDetalle()
                    Me.listaDetalleRelacionado = New List(Of Entidades.DetalleDocumento)

                    For i As Integer = 0 To Me.listaDetalle.Count - 1
                        Dim obj As New Entidades.DetalleDocumento

                        With obj
                            .Cantidad = Me.listaDetalle(i).Cantidad
                            .IdProducto = Me.listaDetalle(i).IdProducto
                            .IdUnidadMedida = Me.listaDetalle(i).IdUMedida
                            .UMedida = Me.listaDetalle(i).UM
                            If comprometedor Then .CantxAtender = .Cantidad
                        End With

                        Me.listaDetalleRelacionado.Add(obj)

                        '**************** si el mot. traslado es comprometedor = true movemos la tabla MovAlmacen
                        If comprometedor Then
                            Dim objMovAlmacen As New Entidades.MovAlmacen
                            With objMovAlmacen

                                .IdTipoOperacion = CInt(Me.cmbTipoOperacion.SelectedValue)
                                .CantidadMov = Me.listaDetalle(i).Cantidad
                                .CantxAtender = Me.listaDetalle(i).Cantidad
                                .Comprometido = comprometedor

                                '***************** valido el factor de movimiento
                                Dim objUtil As New Negocio.Util
                                If objUtil.ValidarExistenciaxTablax1Campo("propietario", "IdPersona", txtIdRemitente.Text) > 0 Then
                                    .Factor = -1
                                Else
                                    .Factor = 0
                                End If

                                .IdAlmacen = CInt(Me.cmbAlmacen.SelectedValue)
                                .IdEmpresa = CInt(Me.cmbEmpresa.SelectedValue)

                                '******************* obtengo el metodo valuacion vigente
                                Dim objMetValuacion As New Negocio.MetodoValuacion
                                .IdMetodoV = objMetValuacion.SelectIdMetValVigente

                                .IdProducto = Me.listaDetalle(i).IdProducto
                                .IdTienda = CInt(Me.cmbTienda.SelectedValue)
                                .IdUMPrincipal = Me.listaDetalle(i).IdUMPrincipal
                                .IdUnidadMedida = Me.listaDetalle(i).IdUMedida
                                .UMPrincipal = Me.listaDetalle(i).NomUMPrincipal
                            End With

                            listaMovAlmacen.Add(objMovAlmacen)

                        End If

                    Next

                Case 1 '************************* Documento RELACIONADO

                    Me.listaDetalleRelacionado = getListaDetalleRelacionado()

                    For i As Integer = 0 To Me.listaDetalleRelacionado.Count - 1

                        Me.listaDetalleRelacionado(i).Cantidad = Me.listaDetalleRelacionado(i).CantADespachar
                        Me.listaDetalleRelacionado(i).CantADespachar = Nothing
                        Me.listaDetalleRelacionado(i).CantxAtender = Nothing

                    Next

            End Select


            '******************* Obtengo el Documento
            Dim Documento As Entidades.Documento = obtenerObjDocumento()
            Dim objObservacion As Entidades.Observacion = obtenerObservaciones()

            Dim IdDocumentoRelacionado As Integer = obtenerIdDocumentoRelacionado()

            '******************** obtengo el punto de partida y llegada
            Dim objPuntoPartida As Entidades.PuntoPartida = obtenerPuntoPartida()
            Dim objPuntoLlegada As Entidades.PuntoLlegada = obtenerPuntoLlegada()

            '******************** Obtengo los datos Transportista
            Dim objJuridica As Entidades.Juridica = obtenerTransportista()
            Dim objChofer As Entidades.Chofer = obtenerChofer()
            Dim objVehiculo As Entidades.Vehiculo = obtenerVehiculo()

            Dim IdDoc As Integer = 0

            Dim idusuario As Integer
            idusuario = CType(Session("IdUsuario"), Integer)
            Select Case CInt(hddModoFrm.Value)

                Case 1
                    '*****************  Registrar NUEVO 
                    '************** GenerarCodigo()  NO ES NECESARIO GENERAR EL CODIGO, EL SP LO AUTOGENERA
                    '************** Documento.Codigo = txtCodigoDocumento.Text
                    IdDoc = (New Negocio.Documento).InsertDocumentoGuia_Almacen(idusuario, Documento, Me.listaDetalleRelacionado, listaMovAlmacen, objObservacion, IdDocumentoRelacionado, objPuntoPartida, objPuntoLlegada, objJuridica, objChofer, objVehiculo)

                Case 2
                    '*****************  Actualizar
                    IdDoc = (New Negocio.Documento).UpdateDocumentoGuiaAlmacen(idUsuario, Documento, Me.listaDetalleRelacionado, objObservacion, IdDocumentoRelacionado, objPuntoPartida, objPuntoLlegada, objJuridica, objChofer, objVehiculo, comprometedor, True)

            End Select

            If IdDoc > 0 Then

                hddIdDocumento.Value = IdDoc.ToString

                hddModoFrm.Value = "5"
                actualizarBotonesControl()

                objScript.mostrarMsjAlerta(Me, "La operación finalizó con éxito.")
            Else
                objScript.mostrarMsjAlerta(Me, "Se produjeron fallos en el registro.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Public Function obtenerTransportista() As Entidades.Juridica

        '******************* obtengo el Transportista
        Dim objJuridica As Entidades.Juridica = Nothing

        If (Not IsNumeric(txtIdTransportista.Text)) And ((txtRazonSocial_Trans.Text.Trim.Length > 0) Or (txtRUC_Trans.Text.Trim.Length > 0)) Then
            objJuridica = New Entidades.Juridica
            objJuridica.RazonSocial = txtRazonSocial_Trans.Text
            objJuridica.RUC = txtRUC_Trans.Text
        End If

        Return objJuridica

    End Function

    Public Function obtenerChofer() As Entidades.Chofer

        '******************** obtengo el Chofer
        Dim objChofer As Entidades.Chofer = Nothing
        If (Not IsNumeric(txtIdChofer.Text)) And ((txtNomChofer_Trans.Text.Trim.Length > 0) Or (txtNroLicencia_Trans.Text.Trim.Length > 0) Or (txtCategoria_Trans.Text.Trim.Length > 0)) Then
            objChofer = New Entidades.Chofer
            objChofer.Nombre = txtNomChofer_Trans.Text
            objChofer.Licencia = txtNroLicencia_Trans.Text
            objChofer.Categoria = txtCategoria_Trans.Text
        End If

        Return objChofer

    End Function

    Public Function obtenerVehiculo() As Entidades.Vehiculo

        '******************** obtengo el Vehiculo
        Dim objVehiculo As Entidades.Vehiculo = Nothing
        If (Not IsNumeric(txtIdVehiculo.Text)) And ((txtNroPlaca_Trans.Text.Trim.Length > 0) Or (txtMarcaVeh_Trans.Text.Trim.Length > 0) Or (txtCertInscripcion_Trans.Text.Trim.Length > 0)) Then
            objVehiculo = New Entidades.Vehiculo
            objVehiculo.Placa = txtNroPlaca_Trans.Text
            objVehiculo.Modelo = txtMarcaVeh_Trans.Text
            objVehiculo.ConstanciaIns = txtCertInscripcion_Trans.Text
        End If

        Return objVehiculo

    End Function

    Public Function obtenerObservaciones() As Entidades.Observacion

        '****************** Observaciones
        Dim objObservacion As Entidades.Observacion = Nothing
        If txtObservaciones.Text.Trim.Length > 0 Then
            objObservacion = New Entidades.Observacion
            objObservacion.Observacion = txtObservaciones.Text
        End If

        Return objObservacion

    End Function

    Public Function obtenerIdDocumentoRelacionado() As Integer

        '********************* Obtengo el Id del documento Relacionado

        Dim IdDocumentoRelacionado As Integer = 0

        If (IsNumeric(Me.hddIdDocumentoRef.Value) And (Me.hddIdDocumentoRef.Value.Trim.Length > 0)) Then
            IdDocumentoRelacionado = CInt(Me.hddIdDocumentoRef.Value)
        End If

        Return IdDocumentoRelacionado

    End Function






    Private Sub cmbMotivoTraslado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbMotivoTraslado.SelectedIndexChanged

        Try

            Dim cbo As New Combo
            cbo.llenarCboTipoOperacionxIdMotivoT(Me.cmbTipoOperacion, CInt(Me.cmbMotivoTraslado.SelectedValue), True)

            '****************** VALIDAMOS EL MOTIVO DE TRASLADO COMPROMETEDOR
            If ((New Negocio.MotivoTraslado).SelectComprometedorxIdMotivoT(CInt(Me.cmbMotivoTraslado.SelectedValue))) Then
                Me.lblAlertaComprometerStock.Visible = True
            Else
                Me.lblAlertaComprometerStock.Visible = False
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub


    ''******************** ProcedĂmiento que permite levantar PopUps desde VB.Net
    'Private Sub LevantarPopUp(ByVal Url As String, ByVal Width As Integer, ByVal Height As Integer, ByVal Top As Integer, ByVal Left As Integer)
    '    Dim popupScript As String = "<script language='JavaScript'>window.open('" & Url & "', 'CustomPopUp', " & "'width=" & Width & ", height=" & Height & ", top=" & Top & ", left=" & Left & ", menubar=no, resizable=no,scrollbars=no')" & "</script>"

    '    Dim objSB As New StringBuilder
    '    With objSB

    '        .Append("<html>")
    '        .Append("<head>")
    '        .Append("<title>")
    '        .Append("</title>")
    '        .Append("</head>")
    '        .Append("<body>")
    '        .Append("Esto es la generacion de un documento de venta")
    '        .Append("</body>")
    '        .Append("</html>")

    '    End With

    '    Response.Write(objSB.ToString)
    '    Response.End()
    '    'ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", objSB.ToString, False)

    'End Sub




    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnular.Click

        anularGuiaRemision(CInt(hddIdDocumento.Value))


    End Sub

    Private Sub anularGuiaRemision(ByVal IdDocumento As Integer)

        Try

            If (New Negocio.Documento).DocumentoGuiaRemisionAnular(IdDocumento) Then
                objScript.mostrarMsjAlerta(Me, "La Anulación se realizó con éxito.")
            Else
                Throw New Exception("Problemas en la Operación.")
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Protected Sub btnAgregarProducto_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgregarProducto.Click

    End Sub
End Class
