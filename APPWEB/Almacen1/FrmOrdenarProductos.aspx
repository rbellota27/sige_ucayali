﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"   MasterPageFile="~/Principal.Master"  CodeBehind="FrmOrdenarProductos.aspx.vb" Inherits="APPWEB.FrmOrdenarProductos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script language="javascript" type="text/javascript">
    function valSave() {
        var grilla = document.getElementById('<%=DGV_ListaProductos.ClientID %>');
        if (grilla == null) {
            alert('No se hallaron registros.');
            return false;
        } else {
            return (confirm('Desea continuar con la operación?'));
        }        
    }
    function valGrilla() {
        var grilla = document.getElementById('<%=DGV_ListaProductos.ClientID %>');       
        if (grilla == null) {
            alert('No se hallaron registros.');
            return false;
        } /*else {
            for (var i = 1; i < grilla.rows.length - 1; i++) {//comenzamos en 1 para no tomar la cabecera
                for (var j = (i + 1); j < grilla.rows.length; j++) {                    
                    if (parseFloat(grilla.rows[i].cells[2].children[0].value) > parseFloat(grilla.rows[j].cells[2].children[0].value)) {
                        var aux = grilla.rows[i];
                        grilla.rows[i] = grilla.rows[j];
                        grilla.rows[j] = aux;
                    }
                }                                            
            }            
        }        
        return false;*/
        return true;
    }
    function valBlur(event) {
        var id = event.srcElement.id;
        var caja = document.getElementById(id);
        if (CajaEnBlanco(caja)) {
            caja.focus();
            alert('Debe ingresar un valor.');
        }
        if (!esDecimal(caja.value)) {
            caja.select();
            caja.focus();
            alert('Valor no válido.');
        }
    }
</script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td class="TituloCelda">
                        ORDEN - PRODUCTOS</td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_Cabecera" runat="server">
                            <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
                            <asp:DropDownList ID="cmbLinea" runat="server" AutoPostBack="true" 
                                DataTextField="Descripcion" DataValueField="Id">
                            </asp:DropDownList>
                            <asp:Label ID="Label3" runat="server" CssClass="Label" Text="SubLínea:"></asp:Label>
                            <asp:DropDownList ID="cmbSubLinea" runat="server" DataTextField="Nombre" 
                                DataValueField="Id">
                            </asp:DropDownList>
                            <asp:ImageButton ID="btnAceptar_SubLinea_Cab" runat="server" 
                                ImageUrl="~/Imagenes/Aceptar_B.JPG" 
                                onmouseout="this.src='../Imagenes/Aceptar_B.JPG';" 
                                onmouseover="this.src='../Imagenes/Aceptar_A.JPG';" />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="Panel_detalle" runat="server">
                        <table width="100%">
                        <tr>
                        <td>
                            <asp:ImageButton ID="btnOrdenar" runat="server" 
                                OnClientClick="return(valGrilla());" Width="16px" />
                            <asp:ImageButton ID="btnGuardar" runat="server" 
                                ImageUrl="~/Imagenes/Guardar_B.JPG" 
                                OnClientClick="return(valSave());" 
                                onmouseout="this.src='../Imagenes/Guardar_B.JPG';" 
                                onmouseover="this.src='../Imagenes/Guardar_A.JPG';" />
                            <asp:ImageButton ID="btnAtras" runat="server" 
                                ImageUrl="~/Imagenes/Arriba_B.JPG" 
                                onmouseout="this.src='../Imagenes/Arriba_B.JPG';" 
                                onmouseover="this.src='../Imagenes/Arriba_A.JPG';" ToolTip="Atrás" />
                            </td>
                        </tr>
                        <tr>
                        <td>
                            <asp:GridView ID="DGV_ListaProductos" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="Id" NullDisplayText="---" />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción" 
                                        NullDisplayText="---" />                                    
                                    <asp:TemplateField HeaderText="Nº Orden">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNroOrden" runat="server" 
                                                onblur="return(valBlur(event));" onKeypress="return(onKeyPressEsNumero('event'));"                                                 
                                                Text='<%#DataBinder.Eval(Container.DataItem,"Orden")%>' Width="80px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                    
                                </Columns>
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <HeaderStyle CssClass="GrillaHeader" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                            </td>
                        </tr>
                        </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
