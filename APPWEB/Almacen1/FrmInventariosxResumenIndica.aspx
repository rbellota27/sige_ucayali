<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmInventariosxResumenIndica.aspx.vb" Inherits="APPWEB.FrmInventariosxResumenIndica" 
    title="P�gina sin t�tulo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   <table class="style1">
   <tr style="background-color: Yellow">
            <td style="width: 350px">
                <asp:LinkButton CssClass="Label" Font-Bold="true" ID="lkb_CapaConfigFrm" OnClientClick="return(onCapa('capaConfiguracionFrm'));"
                    runat="server">Config. Formulario</asp:LinkButton>
            </td>
        </tr>
<tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nueva B�squeda" ToolTip="Nueva Busqueda" Width="120px" />
                        </td>
                       
                        <td>
                            <asp:Button ID="btnBuscar" runat="server"  Text="Buscar" ToolTip="Buscar" Width="80px"  />
                        </td>
                                   
                          <td>
                            <asp:Button ID="btnExport" runat="server" Text="Exportar a Excel" ToolTip="Exportar Documento" />
                        </td>
                          <td>
                          <asp:Button  ID="btnreporte" runat ="server" Text ="Visualizar Reporte" ToolTip="Visualizar Documento"/>
                        </td>
                          <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
           <tr>
            <td class="TituloCelda">
                RESUMEN DE INDICADORES [IRI] - [NDI]
            </td>
        </tr>
        <tr>
        <td>
        <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                                        <tr>
                                                <td align="right" class="Texto">
                                                Tipo Almacen:
                                                </td>
                                                
                                                <td >
                                                <asp:DropDownList ID="cbotipoalmacen" runat="server" Enabled="true" >
                                                </asp:DropDownList>
                                                </td>
                                              
                                              
                                                  <td> &nbsp;</td>
                                                  <td>
                                              &nbsp;
                                                  </td>
                                                  <td align="right" class="Texto">
                                                A�o:
                                                </td>
                                                <td>
                                                <asp:DropDownList ID="cboanio" runat="server" Enabled="true" >
                                                     <asp:ListItem Text="2010" Value="1" ></asp:ListItem>
                                                     <asp:ListItem Text="2011" Value="2" ></asp:ListItem>
                                                     <asp:ListItem Text="2012" Value="3" ></asp:ListItem>
                                                     <asp:ListItem Text="2013" Value="4" ></asp:ListItem>
                                                     <asp:ListItem Text="2014" Value="5" ></asp:ListItem>
                                                     <asp:ListItem Text="2015" Value="6" ></asp:ListItem>
                                                     <asp:ListItem Text="2016" Value="7" ></asp:ListItem>
                                                     <asp:ListItem Text="2017" Value="8" ></asp:ListItem>
                                                     <asp:ListItem Text="2018" Value="9" ></asp:ListItem>
                                                     <asp:ListItem Text="2019" Value="10" ></asp:ListItem>
                                                      <asp:ListItem Text="2020" Value="11" ></asp:ListItem>
                                                     <asp:ListItem Text="2021" Value="12" ></asp:ListItem>    
                                                      <asp:ListItem Text="2022" Value="13" ></asp:ListItem>                           
                                                </asp:DropDownList>
                                                </td>
                                                <td> &nbsp;</td>
                                        </tr>
                                        <tr>
                                        <td></td>
                                          <td>
                                                <asp:Button ID="add" Text="Agregar Almacen" runat="server" OnClientClick=" return ( Almacen_add() ); " ToolTip="Replicar almacenes"  />
                                                </td>
                                                <td>&nbsp;
                                                </td>
                                                <td>&nbsp;</td>
                                                 <td align="right" class="Texto">
                                                Mes Inicio:
                                                </td>
                                                <td>
                                                <asp:DropDownList ID="cboMesInicio" runat="server" Enabled="true" >
                                                         <asp:ListItem Text="Enero" Value="1" ></asp:ListItem>
                                                         <asp:ListItem Text="Febrero" Value="2" ></asp:ListItem>
                                                         <asp:ListItem Text="Marzo" Value="3" ></asp:ListItem>
                                                         <asp:ListItem Text="Abril" Value="4" ></asp:ListItem>
                                                         <asp:ListItem Text="Mayo" Value="5" ></asp:ListItem>
                                                         <asp:ListItem Text="Junio" Value="6" ></asp:ListItem>
                                                         <asp:ListItem Text="Julio" Value="7" ></asp:ListItem>
                                                         <asp:ListItem Text="Agosto" Value="8" ></asp:ListItem>
                                                         <asp:ListItem Text="Septiembre" Value="9" ></asp:ListItem>
                                                         <asp:ListItem Text="Octubre" Value="10" ></asp:ListItem>
                                                          <asp:ListItem Text="Noviembre" Value="11" ></asp:ListItem>
                                                         <asp:ListItem Text="Diciembre" Value="12" ></asp:ListItem>      
                                                </asp:DropDownList>
                                                </td>
                                        </tr>
                                        <tr>
                                        <td>&nbsp;</td>
                                         <td>&nbsp;</td>
                                          <td>&nbsp;</td>
                                           <td>&nbsp;</td>
                                              
                                                 <td align="right" class="Texto">
                                                Mes Final:
                                                </td>
                                                <td>
                                                        <asp:DropDownList ID="cboMesFinal" runat="server"  Enabled="true" >
                                                                 <asp:ListItem Text="Enero" Value="1" ></asp:ListItem>
                                                                 <asp:ListItem Text="Febrero" Value="2" ></asp:ListItem>
                                                                 <asp:ListItem Text="Marzo" Value="3" ></asp:ListItem>
                                                                 <asp:ListItem Text="Abril" Value="4" ></asp:ListItem>
                                                                 <asp:ListItem Text="Mayo" Value="5" ></asp:ListItem>
                                                                 <asp:ListItem Text="Junio" Value="6" ></asp:ListItem>
                                                                 <asp:ListItem Text="Julio" Value="7" ></asp:ListItem>
                                                                 <asp:ListItem Text="Agosto" Value="8" ></asp:ListItem>
                                                                 <asp:ListItem Text="Septiembre" Value="9" ></asp:ListItem>
                                                                 <asp:ListItem Text="Octubre" Value="10" ></asp:ListItem>
                                                                  <asp:ListItem Text="Noviembre" Value="11" ></asp:ListItem>
                                                                 <asp:ListItem Text="Diciembre" Value="12" ></asp:ListItem>      
                                                        </asp:DropDownList>
                                                </td> 
                                                  <td> &nbsp;</td> 
                                             <td>&nbsp;</td>
                                              <td>&nbsp;</td>
                                               <td>&nbsp;</td>
                                               
                                        </tr>
                                        <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                        <asp:GridView ID="GV_TipoAlmacen" runat="server" AutoGenerateColumns="False" 
                                                Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" 
                                                BorderWidth="1px" CellPadding="3" >
                                <Columns>
                                    <asp:TemplateField HeaderText="Opciones">
                                        <ItemTemplate>
                                                        <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                        <asp:LinkButton ID="lkbAlmacenRemove" runat="server" OnClick="lkbAlmacenRemove_Click">Quitar</asp:LinkButton>
                                                        <asp:HiddenField ID="hddIdTipoAlmacen" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdtipoAlmacen") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Tipo Almacen">
                                   <ItemTemplate>
                                         <asp:Label ID="lbltipoalmacen"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Tipoalm_Nombre")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                    
                                </Columns>
                                <RowStyle CssClass="GrillaRow" ForeColor="#000066" />
                                <FooterStyle CssClass="GrillaFooter" BackColor="White" ForeColor="#000066" />
                                <PagerStyle CssClass="GrillaPager" BackColor="White" ForeColor="#000066" 
                                                HorizontalAlign="Left" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" BackColor="#669999" Font-Bold="True" 
                                                ForeColor="White" />
                                <HeaderStyle CssClass="GrillaHeader" BackColor="#006699" Font-Bold="True" 
                                                ForeColor="White" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            </asp:GridView>
                                        </td>
                                          <td>&nbsp;
                                                </td>
                                         </tr> 
                                         <tr>
                                         <td></td> <td></td>
                                         </tr>
                                           <tr>
                                <td></td>
                                <td>
                               
                                </td>
                                </tr>                           
                                </table>
                                <table>
                                <tr>
                                <td></td>
                                <td>
                                <asp:Label  ID="lblmensajeiri" runat ="server" Text=" " style="color:Red;font-family:Comic Sans MS ;font-size:small   "></asp:Label>
                                 <asp:Label  ID="totalempresa" runat ="server" Text=" " style="color:Blue;font-family:Comic Sans MS ;font-size:small   "></asp:Label>
                                  <asp:Label  ID="totalempresandi" runat ="server" Text=" " style="color:Blue;font-family:Comic Sans MS ;font-size:small   "></asp:Label>
                                </td>
                               
                                </tr>
                                
                                <tr>
                                <td> <%--OnSelectedIndexChanged="OnSelectedIndexChanged"--%></td>
                                <td>  
                                <asp:GridView ID="GV_Inventrio" runat="server" AutoGenerateColumns="False" 
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None" 
                                        BorderWidth="1px" CellPadding="3" GridLines="Vertical"  ShowFooter="false" >
                                       
                                <Columns>
                                  <%--  <asp:ButtonField Text = "Seleccionar" CommandName = "Select"  />--%>
                                   <asp:TemplateField HeaderText="Tienda">
                                   <ItemTemplate>
                                         <asp:Label ID="lbltienda"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Tienda")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Enero">
                                   <ItemTemplate>
                                         <asp:Label ID="lblenero"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Enero","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                                                    
                                   </asp:TemplateField>
                             
                                     <asp:TemplateField HeaderText="Febrero">
                                   <ItemTemplate>
                                         <asp:Label ID="lblfebrero"  runat="server" 
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Febrero","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                               
                                  </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Marzo">
                                   <ItemTemplate>
                                         <asp:Label ID="lblmarzo"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Marzo","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Abril">
                                   <ItemTemplate>
                                         <asp:Label ID="lblabril"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Abril","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Mayo">
                                   <ItemTemplate>
                                         <asp:Label ID="lblmayo"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Mayo","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Junio">
                                   <ItemTemplate>
                                         <asp:Label ID="lbljunio"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Junio","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                   
                                                                       
                                   </asp:TemplateField>
                                   
                                        <asp:TemplateField HeaderText="Julio">
                                   <ItemTemplate>
                                         <asp:Label ID="lbljulio"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Julio","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                   
                                   </asp:TemplateField>
                                   
                                        <asp:TemplateField HeaderText="Agosto">
                                   <ItemTemplate>
                                         <asp:Label ID="lblagosto"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Agosto","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                     
                                   </asp:TemplateField>
                                   
                                      <asp:TemplateField HeaderText="Septiembre">
                                   <ItemTemplate>
                                         <asp:Label ID="lblsetiembre"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Septiembre","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                     
                                   </asp:TemplateField>
                                   
                                      <asp:TemplateField HeaderText="Octubre">
                                   <ItemTemplate>
                                         <asp:Label ID="lbloctubre"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Octubre","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                     
                                   </asp:TemplateField>
                                   
                                      <asp:TemplateField HeaderText="Noviembre">
                                   <ItemTemplate>
                                         <asp:Label ID="lblnoviembre"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Noviembre","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                     
                                   </asp:TemplateField>
                                   
                                   <asp:TemplateField HeaderText="Diciembre">
                                   <ItemTemplate>
                                         <asp:Label ID="lbldiciembre"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Diciembre","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                     </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Promedio IRI">
                                   <ItemTemplate>
                                         <asp:Label ID="lbliri"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"IRI","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                   
                                   <asp:TemplateField HeaderText="Promedio NDI">
                                   <ItemTemplate>
                                         <asp:Label ID="lblndi"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"NDi","{0:F}")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                                                     
                                </Columns>
                                <RowStyle CssClass="GrillaRow" BackColor="#EEEEEE" ForeColor="Black" />
                                <FooterStyle CssClass="GrillaFooter" BackColor="#CCCCCC" ForeColor="Black" />
                                <PagerStyle CssClass="GrillaPager" BackColor="#999999" ForeColor="Black" 
                                        HorizontalAlign="Center" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" BackColor="#008A8C" Font-Bold="True" 
                                        ForeColor="White" />
                                <HeaderStyle CssClass="GrillaHeader" BackColor="#000084" Font-Bold="True" 
                                        ForeColor="White" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" BackColor="#DCDCDC" />
                            </asp:GridView></td>
                                </tr>
                                
                                <tr>
                                <td></td>
                                <td></td>
                                </tr>
                                  <tr>
                                <td></td>
                                
                                <td><asp:GridView ID="gvEscala" runat="server" CellPadding="4" ForeColor="#333333" 
                                        GridLines="None"  Visible="true" >
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                             
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                                </td>
                                </tr>
                                    </table>
                                                       
             </asp:Panel>
        </td>
          
      
        </tr>
              
</table>


 <script language="javascript" type="text/javascript">

  function Almacen_add() {

            var cbotipoalmacen = document.getElementById('<%=cbotipoalmacen.ClientID %>');
            var GV_TipoAlmacen = document.getElementById('<%=GV_TipoAlmacen.ClientID %>');

            if (GV_TipoAlmacen != null) {

                for (var i = 1; i < GV_TipoAlmacen.rows.length; i++) {
                    var rowElem = GV_TipoAlmacen.rows[i];

                    if (rowElem.cells[0].children[2].value == cbotipoalmacen.value) {

                        alert('LA LISTA CONTIENE EL TIPO ALMACEN [ ' + cbotipoalmacen.options[cbotipoalmacen.selectedIndex].text + ' ] SELECCIONADO. \n NO SE PERMITE LA OPERACI�N.');
                        return false;
                    }

                }

            }

            return true;
        }
        
           </script>

</asp:Content>