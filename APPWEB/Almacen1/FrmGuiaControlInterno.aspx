﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmGuiaControlInterno.aspx.vb" Inherits="APPWEB.FrmGuiaControlInterno" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr style="background-color: Yellow">
            <td style="width: 350px">
                <asp:LinkButton CssClass="Label" Font-Bold="true" ID="lkb_CapaConfigFrm" OnClientClick="return(onCapa('capaConfiguracionFrm'));"
                    runat="server">Config. Formulario</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" ToolTip="Nuevo" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                                Text="Editar" ToolTip="Editar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                                Text="Anular" ToolTip="Anular" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" ToolTip="Guardar" OnClientClick=" return(     valSaveDocumento()       ); "
                                Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscarDocumentoRef" runat="server" Text="Documento Ref." OnClientClick="  return(  onCapa('capaDocumentosReferencia')  );  "
                                ToolTip="Buscar Documento Referencia" Width="115px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  valOnClick_btnImprimir()  );"
                                Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" />
                        </td>
                        <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                GUÍA DE CONTROL INTERNO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarDocumentoxCodigo">                                
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( valOnKeyPressCodigoDoc(this,event)  );"
                                    ReadOnly="true" Width="100px"></asp:TextBox>
                                    </asp:Panel>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" ToolTip="Buscar Documento por [ Serie - Número ]." />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="Búsqueda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisión:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Operación:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" runat="server" Width="100%" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Motivo de Traslado:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboMotivoTraslado" Width="100%" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                DOCUMENTO DE REFERENCIA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_DocRef" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td class="Texto" align="right">
                                            Descripción:
                                        </td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtDescripcionPersona" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft"
                                                Width="400px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" align="right">
                                            D.N.I.:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDni" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="right" class="Texto">
                                            R.U.C.:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtRuc" ReadOnly="true" CssClass="TextBox_ReadOnlyLeft" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="NomTipoDocumento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red"
                                            HeaderText="Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                        <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                            <asp:HiddenField ID="hddIdDocumento0" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                            <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />
                                                            <asp:HiddenField ID="hddIdAlmacen0" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />
                                                            <asp:HiddenField ID="hddIdPersona0" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                REMITENTE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Remitente" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="right" style="text-align: left">
                                <table>
                                    <tr>
                                        <td class="Texto" align="right">
                                            Ap. y Nombres/Razón Social:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtRemitente" runat="server" Width="490px" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtIdRemitente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                            Width="75px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarRemitente" runat="server" Text="Buscar" Width="80px" CssClass="btnBuscar"
                                                         OnClientClick=" return(   valOnClick_btnBuscarRemitente()   ); " />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" align="right">
                                            Tipo de Almacén:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="cboTipoAlmacen_Remitente" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="Texto">
                                                        Almacén:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboAlmacen_Remitente" runat="server" Enabled="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" align="right">
                                            D.N.I.:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDni_Remitente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="183px"></asp:TextBox>
                                                    </td>
                                                    <td class="Texto" align="right">
                                                        R.U.C.:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtRuc_Remitente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                            Width="202px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DESTINATARIO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Destinatario" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="right" style="text-align: left">
                                <table>
                                    <tr>
                                        <td class="Texto" align="right">
                                            Ap. y Nombres/Razón Social:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDestinatario" runat="server" Width="490px" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtIdDestinatario" runat="server" CssClass="TextBoxReadOnly" onkeypress="return(false);"
                                                            Width="75px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarDestinatario" runat="server" Text="Buscar" Width="80px" CssClass="btnBuscar"
                                                            OnClientClick=" return(   valOnClick_btnBuscarDestinatario()   ); " />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" align="right">
                                            Tipo de Almacén::
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="cboTipoAlmacen_Destinatario" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="Texto">
                                                        Almacén:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboAlmacen_Destinatario" runat="server"
                                                            Enabled="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" align="right">
                                            D.N.I.:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDni_Destinatario" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="183px"></asp:TextBox>
                                                    </td>
                                                    <td class="Texto" align="right">
                                                        R.U.C.:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtRuc_Destinatario" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                            Width="202px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DETALLE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Detalle" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                                                OnClientClick="return(valOnClick_btnBuscarProducto());" onmouseout="this.src='../Imagenes/BuscarProducto_b.JPG';"
                                                onmouseover="this.src='../Imagenes/BuscarProducto_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiarDetalleDocumento" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="CodigoProducto" HeaderText="Código" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NomProducto" HeaderText="Descripción" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="U. Medida" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboUnidadMedida" runat="server" DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaUM")%>'
                                                    DataValueField="IdUnidadMedida" DataTextField="NombreCortoUM" AutoPostBack="true"
                                                    OnSelectedIndexChanged="cboUnidadMedida_GV_Detalle">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate> 
                                                            <asp:TextBox ID="txtCantidad" runat="server" onblur="return(valBlur(event));" TabIndex="500"
                                                                onKeypress="return(validarNumeroPunto(event));" onfocus="return(aceptarFoco(this));" Visible="true"
                                                                Width="90px" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:G29}")%>'></asp:TextBox>
                                                            <asp:ImageButton ID="btnEquivalencia_PR" runat="server" ToolTip="Calcular Equivalencia"
                                                                ImageUrl="~/Imagenes/Ok_b.bmp" OnClientClick="return(    valOnClick_btnEquivalencia_PR(this)    );"
                                                                OnClick="btnEquivalencia_PR_Click" />
                                                            <br runat="server" visible="false" />
                                                            <asp:TextBox ID="txtCantidad_2" runat="server" onblur="return(valBlur(event));" TabIndex="500"
                                                                onKeypress="return(validarNumeroPunto(event));" onfocus="return(aceptarFoco(this));" Visible="false"
                                                                Width="90px" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:G29}")%>'></asp:TextBox>
                                                            <br id="Br1" runat="server" visible="false" />
                                                            <asp:TextBox ID="txtCantidad_3" runat="server" onblur="return(valBlur(event));" TabIndex="500"
                                                                onKeypress="return(validarNumeroPunto(event));" onfocus="return(aceptarFoco(this));" Visible="false"
                                                                Width="90px" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:G29}")%>'></asp:TextBox>
                                                            <br id="Br2" runat="server" visible="false" />
                                                            <asp:TextBox ID="txtCantidad_4" runat="server" onblur="return(valBlur(event));" TabIndex="500"
                                                                onKeypress="return(validarNumeroPunto(event));" onfocus="return(aceptarFoco(this));" Visible="false"
                                                                Width="90px" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:G29}")%>'></asp:TextBox>
                                                            <br id="Br3" runat="server" visible="false" />
                                                            <asp:TextBox ID="txtCantidad_5" runat="server" onblur="return(valBlur(event));" TabIndex="500"
                                                                onKeypress="return(validarNumeroPunto(event));" onfocus="return(aceptarFoco(this));" Visible="false"
                                                                Width="90px" Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:G29}")%>'></asp:TextBox>
                                                            <asp:HiddenField ID="hddCantidadControles" runat="server" Value="0" />
                                                            <asp:HiddenField ID="cantidad" runat="server" Value="0" />
                                                            <asp:HiddenField ID="hddCantidadesArray" runat="server" Value="0" />
                                                            <asp:HiddenField ID="hddIdDetalleAfecto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDetalleDocumento","{0:F2}")%>' />
                                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />
                                                            <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                                            <%--<asp:PlaceHolder ID="phCantidades" runat="server"></asp:PlaceHolder>--%>
                                            </ItemTemplate>                                                                                        
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tono">
                                            <ItemTemplate>                                            
                                                <asp:DropDownList ID="ddlTonos" runat="server" DataSource='<%#DataBinder.Eval(Container.DataItem,"listaTonos") %>'
                                                DataValueField="idTono" DataTextField="nomtono" Visible="true" ></asp:DropDownList>
                                                <asp:ImageButton ID="imgBtnAddTono" runat="server" ToolTip="Agregar Tono" ImageUrl="~/Imagenes/addTono.png"
                                                CommandArgument='<%# Container.DataItemIndex %>' CommandName="btnAgregarTono" />
                                                <br id="Br4" runat="server" visible="false" />
                                                <asp:DropDownList ID="ddlTonos_2" runat="server" DataSource='<%#DataBinder.Eval(Container.DataItem,"listaTonos") %>'
                                                DataValueField="idTono" DataTextField="nomtono" Visible="false" ></asp:DropDownList>
                                                <br id="Br5" runat="server" visible="false" />
                                                <asp:DropDownList ID="ddlTonos_3" runat="server" DataSource='<%#DataBinder.Eval(Container.DataItem,"listaTonos") %>'
                                                DataValueField="idTono" DataTextField="nomtono" Visible="false" ></asp:DropDownList>
                                                <br id="Br6" runat="server" visible="false" />
                                                <asp:DropDownList ID="ddlTonos_4" runat="server" DataSource='<%#DataBinder.Eval(Container.DataItem,"listaTonos") %>'
                                                DataValueField="idTono" DataTextField="nomtono" Visible="false" ></asp:DropDownList>
                                                <br id="Br7" runat="server" visible="false" />
                                                <asp:DropDownList ID="ddlTonos_5" runat="server" DataSource='<%#DataBinder.Eval(Container.DataItem,"listaTonos") %>'
                                                DataValueField="idTono" DataTextField="nomtono" Visible="false" ></asp:DropDownList>

                                                <%--<asp:HiddenField ID="hddTonosArray" runat="server" Value="0" />--%>
                                                <%--<asp:PlaceHolder ID="phTonos" runat="server"></asp:PlaceHolder>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Stock Real" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblStockReal" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Stock","{0:G29}")%>'></asp:Label>
                                                            <asp:Label ID="lblUnidadMedida" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedidaPrincipal")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                OBSERVACIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtObservaciones" Width="100%" TextMode="MultiLine" runat="server"
                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                    Height="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdRemitente" runat="server" />
                <asp:HiddenField ID="hddIdDestinatario" runat="server" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="47" />
                <asp:HiddenField ID="hddOpcionBusquedaPersona" runat="server" Value="0" />
                <asp:HiddenField ID="hddNroFilasxDocumento" runat="server" Value="0" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
                <asp:HiddenField ID="hddIndex_GV_Detalle" runat="server" />
                <asp:HiddenField ID="hddIdPersonaDocRef" runat="server" />
                <asp:HiddenField ID="hddIdDocumentoRef" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <div id="capaConfiguracionFrm" style="border: 3px solid blue; padding: 8px; width: auto;
        height: auto; position: absolute; top: 120px; left: 38px; background-color: white;
        z-index: 2; display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton9" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaConfiguracionFrm'));" />
                </td>
            </tr>
            <tr align="left">
                <td>
                    <table>
                        <tr>
                            <td class="Label" align="right">
                                Mover Stock Físico:
                            </td>
                            <td style="width: 75px">
                                <asp:CheckBox ID="chb_MoverAlmacen" Enabled="true" runat="server" Checked="true"
                                    ToolTip="Opción válido para Documentos NO afectos a despachos pendientes." />
                            </td>
                            <td align="right">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaConsultarStockPrecioxProducto" style="border: 3px solid blue; padding: 8px;
        width: 450px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa2('capaConsultarStockPrecioxProducto')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProductoConsultarStockPrecioxProd" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ConsultarStockPrecioxProd" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Almacén" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />
                                                <asp:Label ID="lblAlmacen" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Almacen")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Disponible" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblStock" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StockDisponibleN","{0:F3}")%>'></asp:Label>
                                                <asp:Label ID="lblUM" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cadenaUM")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cantidad" Visible="false" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCantidad" Text="0" runat="server" Width="80px" Font-Bold="true"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnAddProducto_ConsultarStockPrecio" runat="server" Text="Aceptar"
                        Width="90px" OnClientClick="return(  offCapa2('capaConsultarStockPrecioxProducto')   );" />
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%" PagerStyle-ForeColor="White" AllowPaging="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisión"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Destinatario" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de búsqueda realiza un filtrado de acuerdo al destinatario seleccionado.
                </td>
            </tr>
        </table>
    </div>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                TipoExistencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="Línea:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                                DataTextField="Descripcion" DataValueField="Id">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" style="text-align: right">
                                            Sub Línea:
                                        </td>
                                        <td>
                                            <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                                DataValueField="Id" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right" class="Texto">
                                Descripción:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="Panel5" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">
                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                onKeypress="return( valKeyPressDescripcionProd(this,event) );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td class="Texto">
                                            Cód.:
                                        </td>
                                        <td>
                                        <asp:Panel ID="Panel6" runat="server" DefaultButton="btnBuscarGrilla_AddProducto">
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onKeypress="return( valKeyPressDescripcionProd(this,event) );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" TabIndex="205"></asp:TextBox>
                                                </asp:Panel>
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />--%>
                                                <asp:Button ID="btnBuscarGrilla_AddProducto" runat="server" Text="Buscar" TabIndex="207" CssClass="btnBuscar"
                                            OnClientClick="this.disabled;this.value='Procesando...'" UseSubmitBehavior="false"/>
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../Imagenes/Agregar_A.JPG';"
                                                OnClientClick="return(valAddProductos());" TabIndex="208" />--%>
                                               
                                                <asp:Button ID="btnAddProductos_AddProducto" runat="server" Text="Añadir" TabIndex="208"  CssClass="btnAñadir"
                                            OnClientClick="return(valAddProductos());"  />
                                        </td>
                                        <td>
                                            <%--<asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />--%>
                                                <asp:Button ID="btnLimpiar_AddProducto" runat="server" Text="Limpiar" TabIndex="209" CssClass="btnLimpiar"
                                            OnClientClick="return(limpiarBuscarProducto());"     />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="Búsqueda Avanzada" ExpandedText="Búsqueda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="Búsqueda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel9" runat="server" DefaultButton="btnAddProductos_AddProducto">                    
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCantidad_AddProd" TabIndex="210" Width="65px" onblur="return( valBlurClear('0.00',event) );"
                                        onfocus="return(  aceptarFoco(this) );" onKeypress="return( valKeyPressCantidadAddProd(event)  );"
                                        Text="0" runat="server"></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Codigo" HeaderText="Código" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripción" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="U.M." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:DropDownList ID="cboUnidadMedida" runat="server" AutoPostBack="false" DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaUM")%>'
                                        DataTextField="NombreCortoUM" DataValueField="IdUnidadMedida">
                                    </asp:DropDownList>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                            <asp:BoundField DataField="StockDisponible_Cadena" HeaderText="Stock Disponible"
                                NullDisplayText="0" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:ImageButton ID="btnConsultarStockAlmacenes_BuscarProd" runat="server" OnClick="mostrarCapaStockPrecioxProducto"
                                                    ImageUrl="~/Imagenes/Ok_b.bmp" ToolTip="Consultar Lista de Precios y Stock por Tienda / Almacén." />
                                                <asp:HiddenField ID="hddIdProducto_Find" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                                <asp:HiddenField ID="hddKit" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Kit")%>' />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="Página Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="Página Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaPersona'));" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlBusquedaPersona" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                            AutoPostBack="false">
                                                                            <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                            <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        Razon Social / Nombres:
                                                                    </td>
                                                                    <td colspan="4">
                                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarPersonaGrilla">                                                                        
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this,event));" runat="server"
                                                                            onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                            Width="450px"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Texto">
                                                                        D.N.I.:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                            MaxLength="8" runat="server"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td class="Texto">
                                                                        Ruc:
                                                                    </td>
                                                                    <td>
                                                                    <asp:Panel ID="Panel4" runat="server" DefaultButton="btnBuscarPersonaGrilla">
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                            MaxLength="11"></asp:TextBox>
                                                                            </asp:Panel>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                            onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />--%>

                                                                        <asp:Button ID="btnBuscarPersonaGrilla" runat="server" Text="Buscar" CssClass="btnBuscar"
                                                                         OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                PageSize="20" Width="100%">
                                                                <Columns>
                                                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                    <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="GrillaHeader" />
                                                                <FooterStyle CssClass="GrillaFooter" />
                                                                <PagerStyle CssClass="GrillaPager" />
                                                                <RowStyle CssClass="GrillaRow" />
                                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                <EditRowStyle CssClass="GrillaEditRow" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                ToolTip="Página Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                            <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                ToolTip="Página Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                            <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                    Width="50px" Text="Ir" ToolTip="Ir a la Página" OnClientClick="return(valNavegacion('2'));" />
                                                            <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaEquivalenciaProducto" style="border: 3px solid blue; padding: 8px; width: 450px;
        height: auto; position: absolute; top: 237px; left: 250px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaEquivalenciaProducto')   );" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td class="Texto">
                                            Ingrese Cantidad:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCantidad_Ingreso" onKeyPress=" return( validarNumeroPuntoPositivo('event')  ); "
                                                onFocus=" return(  aceptarFoco(this)  ); " Width="100px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboUnidadMedida_Ingreso" runat="server" DataTextField="NombreCortoUM"
                                                DataValueField="IdUnidadMedida">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCalcular" OnClientClick=" return(  valOnClick_btnCalcular()   ); "
                                                runat="server" Text="Calcular" ToolTip="Calcular equivalencias" Width="80px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto">
                                            Resultado:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCantidad_Salida" Font-Bold="true" CssClass="TextBox_ReadOnlyLeft"
                                                Text="" ReadOnly="true" Width="100px" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboUnidadMedida_Salida" runat="server" DataTextField="NombreCortoUM"
                                                DataValueField="IdUnidadMedida">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GV_CalcularEquivalencia" runat="server" AutoGenerateColumns="False"
                                    Width="400px">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chb_UnidadMedida" runat="server" Checked="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unidad Medida" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblUnidadMedida" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida")%>'></asp:Label>
                                                            <asp:HiddenField ID="hddIdUnidadMedida" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblUnidadMedidaAbv" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NombreCortoUM")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GV_ResuldoEQ" runat="server" AutoGenerateColumns="False" Width="400px">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="UnidadMedida" HeaderText=""
                                            ItemStyle-Height="25px" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="UnidadMedidaAbv" HeaderText="U.M."
                                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="Cantidad" HeaderText="Cantidad"
                                            DataFormatString="{0:F4}" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:RadioButtonList ID="rbl_UtilizarRedondeo" runat="server" RepeatDirection="Horizontal"
                                    CssClass="Label">
                                    <asp:ListItem Selected="True" Value="0">No Utilizar Redondeo</asp:ListItem>
                                    <asp:ListItem Value="1">Redondeo [ arriba ]</asp:ListItem>
                                    <asp:ListItem Value="2">Redondeo [ abajo ]</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnAceptar_EquivalenciaPR" runat="server" Text="Aceptar" Width="80px"
                                    ToolTip="Aceptar Equivalencia" OnClientClick=" return(  valOnClick_btnAceptar_EquivalenciaPR()  );  " />
                            </td>
                            <td>
                                <asp:Button ID="btnCerrar_EquivalenciaPR" runat="server" Text="Cerra" Width="80px"
                                    ToolTip="Cerrar" OnClientClick="  return(  offCapa('capaEquivalenciaProducto') ); " />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton8" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                Tipo Documento:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoDocumento" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <td align="right" style="text-align: left">
                                    <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                                        CssClass="Texto" AutoPostBack="true">
                                        <asp:ListItem Value="0" Selected="True">Por Fecha de Emisión</asp:ListItem>
                                        <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </td>
                            <td>
                                <asp:Button ID="btnPersona" runat="server" Text="Buscar Persona" ToolTip="Buscar Persona"
                                    OnClientClick=" return ( valOnClick_btnBuscarDocRef() ); " />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaInicio_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaFin_DocRef">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                    runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="Texto">
                                                Nro. Serie:
                                            </td>
                                            <td>
                                                <asp:Panel ID="Panel7" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td class="Texto">
                                                Nro. Código:
                                            </td>
                                            <td>
                                            <asp:Panel ID="Panel8" runat="server" DefaultButton="btnAceptarBuscarDocRefxCodigo">
                                                <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                    Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </asp:Panel>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                    runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                        <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="50px" />
                                <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderText="Tipo" />
                                <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                                    <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisión"
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="FechaVenc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Vcto."
                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DescripcionPersona" HeaderText="Cliente" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                            DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GrillaHeader" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            <EditRowStyle CssClass="GrillaEditRow" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function valKeyPressCodigoSL(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProducto.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(obj, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) { // Enter
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscarGrilla_AddProducto.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valAddProductos() {

            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            //        var txtIdCliente = 
            //        if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
            //            alert('Debe seleccionar un Cliente.');
            //            return false;
            //        }

            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('No se seleccionaron productos.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                var txtCantidad = rowElem.cells[0].children[0];
                if (parseFloat(txtCantidad.value) > 0) {
                    // if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
                    //                        alert('La cantidad ingresada excede a la cantidad disponible.');
                    //                        txtCantidad.select();
                    //                        txtCantidad.focus();
                    //     return false;
                    //  } else if (parseFloat(txtCantidad.value) > 0) {
                    cont = cont + 1;
                    //  }
                }
            }

            if (cont == 0) {
                alert('No se seleccionaron productos.');
                return false;
            }

            /*
            //************* Validamos que no agregue de otro almacen
            
            if (grillaDetalle != null) {

                if (grillaDetalle.rows.length > 1) {

                    //************ valido el almacen
                    
            var hddIdAlmacen = 
                    

                    if (parseFloat(cboAlmacen.value) != parseFloat(hddIdAlmacen.value)) {
                        
            return false;
            }
            }
            }
            */
            return true;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }
        function valStockDisponible_AddProd() {
            /*var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
            if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].id == event.srcElement.id) {
            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
            alert('La cantidad ingresada excede a la cantidad disponible.');
            rowElem.cells[0].children[0].select();
            rowElem.cells[0].children[0].focus();
            return false;
            }
            return false;
            }

                }
            }*/
            return false;
        }
        function valKeyPressCantidadAddProd(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                document.getElementById('<%=btnAddProductos_AddProducto.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }

        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }

        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btnBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btnBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegación. Realice una Búsqueda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen páginas con índice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una Página de navegación.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen páginas con índice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }
        function valOnClick_btnBuscarRemitente() {
            document.getElementById('<%=hddOpcionBusquedaPersona.ClientID%>').value = '0';
            mostrarCapaPersona();
            return false;
        }
        function valOnClick_btnBuscarDestinatario() {
            document.getElementById('<%=hddOpcionBusquedaPersona.ClientID%>').value = '1';
            mostrarCapaPersona();
            return false;
        }
        function valOnClick_btnBuscarDocRef() {
            document.getElementById('<%=hddOpcionBusquedaPersona.ClientID%>').value = '2';

            offCapa('capaDocumentosReferencia')
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }
        function valSaveDocumento() {


            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie de Documento.');
                return false;
            }

            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operación.');
                return false;
            }

            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var hddIdDestinatario = document.getElementById('<%=hddIdDestinatario.ClientID%>');
            if (isNaN(parseInt(hddIdDestinatario.value)) || hddIdDestinatario.value.length <= 0) {
                alert('Debe seleccionar un Destinatario.');
                return false;
            }

            var hddIdRemitente = document.getElementById('<%=hddIdRemitente.ClientID%>');
            if (isNaN(parseInt(hddIdRemitente.value)) || hddIdRemitente.value.length <= 0) {
                alert('Debe seleccionar un Remitente.');
                return false;
            }
            var cboAlmacen_Remitente = document.getElementById('<%=cboAlmacen_Remitente.ClientID %>')
            var cboAlmacen_Destinatario = document.getElementById('<%=cboAlmacen_Destinatario.ClientID %>')

            var idvalorRemitente = cboAlmacen_Remitente.options[cboAlmacen_Remitente.selectedIndex].value;
            var idvalorDestinatario = cboAlmacen_Destinatario.options[cboAlmacen_Destinatario.selectedIndex].value;

            var txtvalorRemitente = cboAlmacen_Remitente.options[cboAlmacen_Remitente.selectedIndex].text;
            var txtvalorDestinatario = cboAlmacen_Destinatario.options[cboAlmacen_Destinatario.selectedIndex].text;

            if (idvalorDestinatario == idvalorRemitente) {
                alert("No está permitido tener el mismo almacen [" + txtvalorRemitente + "] en Remitente y destinatario");
                return false;
            }

            //************* Validando cantidades y precios
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                var cantidad = 0;
                var txtCantidad;
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    txtCantidad = rowElem.cells[4].children[0];
                    cantidad = parseFloat(txtCantidad.value);
                    if (isNaN(cantidad)) { cantidad = 0; }
                    if (cantidad <= 0) {
                        alert('Ingrese una cantidad mayor a cero.');
                        txtCantidad.select();
                        txtCantidad.focus();
                        return false;
                    }
                }
            } else {
                alert('No se han ingresado productos.');
                return false;
            }

            var hddFrmModo = document.getElementById('<%=hddFrmModo.ClientID%>');
            var mensaje = '';
            switch (parseInt(hddFrmModo.value)) {
                case 1: //******** NUEVO
                    mensaje = 'Desea continuar con la Operación ?';
                    break;
                case 2: //******** EDITAR
                    mensaje = 'Desea continuar con la Operación ?';
                    break;
            }
            if (confirm(mensaje)) {
                document.getElementById('<%=btnGuardar.ClientID %>').style.visibility = 'hidden';
                return true;
            }
            else {
                document.getElementById('<%=btnGuardar.ClientID %>').style.visibility = 'visible';
                return false;
            }
        }

        //***********************************
        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la Búsqueda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }
        function valOnKeyPressCodigoDoc(txtCodigoDocumento, elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El carácter pulsado es: " + caracter);
            if (caracter == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }

        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la Búsqueda
                alert('Debe habilitar la Búsqueda del Documento haciendo clic en el botón [ Buscar ]');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor válido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }
        function valOnClickEditar() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ningún Documento para Edición.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento está ANULADO. No se permite su edición.');
                return false;
            }
            return true;
        }
        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ningún Documento para su Anulación.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya está ANULADO. No se permite su anulación.');
                return false;
            }
            return confirm('Desea continuar con el Proceso de ANULACIÓN del Documento ?');
        }
        function valOnClick_btnImprimir() {
            var hdd = document.getElementById('<%= hddIdDocumento.ClientID%>');
            if (hdd.value.length == 0 || hdd.value == '' || parseFloat(hdd.value) == 0) {
                alert('No se ha registrado ninguna Guía de Remisión para enviar a impresión.');
                return false;
            }
            //if (confirm('Desea mandar la impresión de la Guía de Remisión?')) {
            // mostrarVentana('../../Reportes/visor.aspx?iReporte=10&IdDocumento=' + hdd.value, 'Orden de Despacho', 500, 500);
            //window.parent.open('../../Reportes/visor.aspx?iReporte=10&IdDocumento=' + hdd.value, 'Reporte', 'resizable=yes', null);
            window.open('../../DocumentosMercantiles/VisorDocMercantiles.aspx?iReporte=47&IdDocumento=' + hdd.value, 'ControlInterno', 'resizable=yes,width=1000,height=800,scrollbars=1', null);


            //}
            return false;
        }
        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la Búsqueda
                alert('Debe habilitar la Búsqueda del Documento haciendo clic en el botón [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');
            return false;
        }

        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        //**********************************    CALCULO DE EQUIVALENCIA PRODUCTO
        function valOnClick_btnEquivalencia_PR(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];
                    if (boton.id == rowElem.cells[4].children[1].id) {
                        //onCapa('capaEquivalenciaProducto');                                                
                        document.getElementById('<%=hddIndex_GV_Detalle.ClientID%>').value = (i - 1);
                        //document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').select();
                        //document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').focus();
                        return true;
                    }
                }
            }
            alert('Problemas en la ubicación de la Fila.');
            return false;
        }
        function mostrarCapaEquivalencia_PR() {
            onCapa('capaEquivalenciaProducto');
            //document.getElementById('<%=hddIndex_GV_Detalle.ClientID%>').value = (i - 1);
            document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').select();
            document.getElementById('<%=txtCantidad_Ingreso.ClientID%>').focus();
            return false;
        }
        function valOnClick_btnCalcular() {
            var cboUnidadMedida_Ingreso = document.getElementById('<%=cboUnidadMedida_Ingreso.ClientID %>');
            if (isNaN(parseFloat(cboUnidadMedida_Ingreso.value)) || cboUnidadMedida_Ingreso.value.length <= 0) {
                alert('DEBE SELECCIONAR UNA UNIDAD DE MEDIDA DE INGRESO.');
                return false;
            }
            var cboUnidadMedida_Salida = document.getElementById('<%=cboUnidadMedida_Salida.ClientID %>');
            if (isNaN(parseFloat(cboUnidadMedida_Salida.value)) || cboUnidadMedida_Salida.value.length <= 0) {
                alert('DEBE SELECCIONAR UNA UNIDAD DE MEDIDA DE SALIDA.');
                return false;
            }
            var txtCantidad = document.getElementById('<%=txtCantidad_Ingreso.ClientID %>');
            if (isNaN(parseFloat(txtCantidad.value)) || txtCantidad.value.length <= 0) {
                alert('INGRESE UN VALOR VÁLIDO.');
                txtCantidad.select();
                txtCantidad.focus();
                return false;
            }
            return true;
        }
        function valOnClick_btnAceptar_EquivalenciaPR() {
            var grilla = document.getElementById('<%=GV_ResuldoEQ.ClientID %>');
            if (grilla == null) {
                alert('NO SE HA REALIZADO EL CÁLCULO RESPECTIVO. NO SE PERMITE LA OPERACIÓN.');
                return false;
            }
            return true;
        }
        //**********************************    FIN  CALCULO DE EQUIVALENCIA PRODUCTO
        
        
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////

    </script>

</asp:Content>
