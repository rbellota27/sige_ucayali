<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmKardex.aspx.vb" Inherits="APPWEB.FrmKardex1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                MANTENIMIENTO - K�RDEX
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="Label" style="font-weight: bold">
                                        Empresa:
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="cboEmpresa" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="Label" style="font-weight: bold">
                                                    Almac�n:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cboAlmacen" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnBuscarProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                                                        OnClientClick="return(valOnClick_btnBuscarProducto());" onmouseout="this.src='/Imagenes/BuscarProducto_b.JPG';"
                                                        onmouseover="this.src='/Imagenes/BuscarProducto_A.JPG';" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="Label" style="font-weight: bold">
                                        Fecha Inicio:
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                        Width="100px"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="txtFechaInicio_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                        CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio">
                                                    </cc1:MaskedEditExtender>
                                                    <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MM/yyyy" TargetControlID="txtFechaInicio">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td class="Label" style="font-weight: bold">
                                                    Fecha Fin:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                        Width="100px"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="txtFechaFin_MaskedEditExtender" runat="server" ClearMaskOnLostFocus="false"
                                                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                        CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin">
                                                    </cc1:MaskedEditExtender>
                                                    <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MM/yyyy" TargetControlID="txtFechaFin">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnReporte" runat="server" OnClientClick="return(  valOnClick_btnReporte()  );"
                                                        Text="Aceptar" Width="80px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" >
                            <asp:GridView ID="GV_Equivalencia" runat="server" AutoGenerateColumns="false" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="NombreCortoUM" HeaderText="U. Medida" />
                                    <asp:BoundField DataField="Equivalencia" HeaderText="Equivalencia" DataFormatString="{0:F4}" />
                                    <asp:CheckBoxField DataField="UnidadPrincipal" HeaderText="Principal" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GV_Producto" runat="server" AutoGenerateColumns="False" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="C�digo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCodigo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Descripcion" ItemStyle-HorizontalAlign="Center" HeaderText="Descripci�n"
                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        <asp:BoundField DataField="UnidadMedida" ItemStyle-HorizontalAlign="Center" HeaderText="U.M."
                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        <asp:BoundField DataField="Linea" ItemStyle-HorizontalAlign="Center" HeaderText="L�nea"
                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        <asp:BoundField DataField="SubLinea" ItemStyle-HorizontalAlign="Center" HeaderText="Sub L�nea"
                            HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                    </Columns>
                    <HeaderStyle CssClass="GrillaHeader" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                K�RDEX
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="LabelRojo" style="font-weight: bold">
                            Moneda Base:
                        </td>
                        <td class="LabelRojo" style="font-weight: bold">
                            <asp:Label ID="lblMonedaBase_Cab" runat="server" Text="-"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GV_Kardex" runat="server" AutoGenerateColumns="false" Width="100%"
                    AllowPaging="True" PageSize="50">
                    <Columns>
                        <asp:CommandField ShowSelectButton="true" SelectText="Editar" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center"
                            ItemStyle-Height="25px" />
                        <asp:BoundField DataField="idregistro" HeaderText="Nro" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="Documento" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblTipoDocumento" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Documento")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblNroDocumento" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nro")%>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddIdDetalleDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDetalleDocumento")%>' />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdDocumento")%>' />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hdddma_Costo" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"dma_Costo","{0:F3}")%>' />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="doc_FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Mov."
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="EntSal" HeaderText="Mov." HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="dma_Cantidad" DataFormatString="{0:F4}" HeaderText="Cant. Mov."
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="dma_Costo" DataFormatString="{0:F3}" HeaderText="Costo Mov."
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="dma_Total" DataFormatString="{0:F3}" HeaderText="Total Mov."
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="sk_CantidadSaldo" HeaderStyle-BackColor="Yellow" HeaderStyle-ForeColor="Blue"
                            DataFormatString="{0:F4}" HeaderText="Cant. Saldo" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="sk_CostoSaldo" HeaderStyle-BackColor="Yellow" HeaderStyle-ForeColor="Blue"
                            DataFormatString="{0:F3}" HeaderText="Costo Saldo" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="sk_TotalSaldo" HeaderStyle-BackColor="Yellow" HeaderStyle-ForeColor="Blue"
                            DataFormatString="{0:F3}" HeaderText="Total Saldo" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="DocRef" HeaderText="DocRef" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="DocRefNumero" HeaderText="DocRef S-N" HeaderStyle-HorizontalAlign="Center"
                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" />
                    </Columns>
                    <HeaderStyle CssClass="GrillaHeader" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr align="right">
            <td align="center">
                <asp:GridView ID="GV_ResuldoEQ" runat="server" AutoGenerateColumns="False" Width="400px">
                    <HeaderStyle CssClass="GrillaHeader" />
                    <Columns>
                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="UnidadMedida" HeaderText=""
                            ItemStyle-Height="25px" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center" />
                        <asp:BoundField ItemStyle-Font-Bold="true" DataField="UnidadMedidaAbv" HeaderText="U.M."
                            HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="Cantidad">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCantidad" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Equivalencia">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblEquivalencia" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="GrillaFooter" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdProducto" runat="server" />
                <asp:HiddenField ID="hddIdDetalleDocumento" runat="server" />
            </td>
        </tr>
    </table>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 70px; left: 32px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="Texto">
                                TipoExistencia:
                            </td>
                            <td>
                                <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                    DataTextField="Descripcion" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                    AutoPostBack="true" DataValueField="Id">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );"></asp:TextBox>
                                        </td>
                                        <td class="Texto" style="text-align: right">
                                            C�d.:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );"
                                                TabIndex="205"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                TabIndex="207" onmouseout="this.src='/Imagenes/Buscar_b.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='/Imagenes/Limpiar_B.JPG';" onmouseover="this.src='/Imagenes/Limpiar_A.JPG';"
                                                OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                        CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                        CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                        CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                        SuppressPostBack="true">
                    </cc1:CollapsiblePanelExtender>
                    <asp:Image ID="Image21_11" runat="server" Height="16px" />
                    <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                    <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                        <table width="100">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="Texto" style="font-weight: bold">
                                                Atributo:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                        Width="650px">
                                        <Columns>
                                            <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Height="25px" />
                                            <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                    <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="GrillaRow" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Width="85px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" />
                            <asp:TemplateField HeaderText="C�digo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCodigo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                            <asp:BoundField DataField="UnidadMedida" ItemStyle-HorizontalAlign="Center" HeaderText="U.M."
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                    <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                        Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                    <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                        CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                    <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                        Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                    <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                        runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="capaEdicion" style="border: 3px solid blue; padding: 8px; width: 750px;
        height: auto; position: absolute; top: 200px; left: 170px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa('capaEdicion')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset class="FieldSetPanel">
                        <legend>Informaci�n General</legend>
                        <table width="100%">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td style="text-align: right">
                                                Empresa:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblEmpresa" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right">
                                                Almac�n:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAlmacen" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right">
                                                Producto:
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblCodigoProducto" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                                        </td>
                                                        <td style="width: 20px">
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblProducto" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right">
                                                Stock F�sico:
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblStockFisico" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblUnidadMedida" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                                        </td>
                                                        <td>
                                                            (Actual)
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right">
                                                Stock Comprometido:
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblComprometido" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right">
                                                Stock Disponible:
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblDisponible" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right">
                                                Stock Tr�nsito:
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblTransito" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset class="FieldSetPanel">
                        <legend>Informaci�n del Movimiento</legend>
                        <table>
                            <tr>
                                <td style="text-align: right">
                                    Documento:
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblTipoDocumento" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                            </td>
                                            <td style="width: 10px">
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNroDocumento" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                            </td>
                                            <td style="width: 25px">
                                            </td>
                                            <td>
                                                Fecha:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblFecha" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right">
                                    Cantidad Mov.:
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCantidad" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUnidadMedida_Mov" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                            </td>
                                            <td style="width: 25px">
                                            </td>
                                            <td>
                                                Costo Mov.:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMoneda" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCostoMov" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                            </td>
                                            <td style="width: 25px">
                                            </td>
                                            <td>
                                                Tipo Mov.:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTipoMov" class="Label_fsp" runat="server" Text="-"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                    <Columns>
                                                        <asp:BoundField DataField="NomTipoDocumento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red"
                                                            HeaderText="Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                                        <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="DescripcionPersona" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="FechaEmision" HeaderText="Emisi�n" HeaderStyle-Height="25px"
                                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />
                                                        <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="NomAlmacen" HeaderText="Almacen" HeaderStyle-Height="25px"
                                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                    <FooterStyle CssClass="GrillaFooter" />
                                                    <PagerStyle CssClass="GrillaPager" />
                                                    <RowStyle CssClass="GrillaRow" />
                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td class="Texto" style="font-weight: bold">
                                Cantidad:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label CssClass="LabelRojo" ID="lblUnidadMedida_Update" runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCantidad" onFocus=" return( aceptarFoco(this) ); " onblur=" return( valBlur(event)  ); "
                                                onKeypress=" return(  validarNumeroPuntoPositivo('event') ); " Width="75px" Font-Bold="true"
                                                runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="tr_costo" runat="server">
                            <td class="Texto" style="font-weight: bold">
                                Costo:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label CssClass="LabelRojo" ID="lblMoneda_Base" runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCostoMov" Font-Bold="true" onFocus=" return( aceptarFoco(this) ); "
                                                Width="75px" onblur=" return( valBlur(event)  ); " onKeypress=" return(  validarNumeroPuntoPositivo('event') ); "
                                                runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" ToolTip="Guardar cambios"
                                    Width="80px" OnClientClick=" return(  valOnClick_btnGuardar()  ); " />
                            </td>
                            <td>
                                <asp:Button ID="btnCancelar" runat="server" Text="Cerrar" ToolTip="Cerrar" Width="80px"
                                    OnClientClick="  return(  offCapa('capaEdicion') ); " />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript" type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
        Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
            if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
                offsetX = Math.floor(offsetX);
                offsetY = Math.floor(offsetY);
            }
            this._origOnFormActiveElement(element, offsetX, offsetY);
        };
        function valKeyPressCodigoSL(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }


        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }

        function valOnClick_btnReporte() {
            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('DEBE SELECCIONAR UN EMPRESA. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var cboAlmacen = document.getElementById('<%=cboAlmacen.ClientID%>');
            if (isNaN(parseInt(cboAlmacen.value)) || cboAlmacen.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ALMAC�N. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var hddIdProducto = document.getElementById('<%=hddIdProducto.ClientID%>');
            if (isNaN(parseInt(hddIdProducto.value)) || hddIdProducto.value.length <= 0) {
                alert('NO SE HA SELECCIONADO UN PRODUCTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            return true;
        }

        function valOnClick_btnGuardar() {
            var txtCantidad = document.getElementById('<%=hddIdProducto.ClientID%>');
            var txtCosto = document.getElementById('<%=hddIdProducto.ClientID%>');

            if (isNaN(parseFloat(txtCantidad.value)) || txtCantidad.value.length <= 0 || parseFloat(txtCantidad.value) < 0) {
                alert('CANTIDAD NO V�LIDA. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            if (isNaN(parseFloat(txtCosto.value)) || txtCosto.value.length <= 0 || parseFloat(txtCosto.value) < 0) {
                alert('COSTO NO V�LIDO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var hddIdProducto = document.getElementById('<%=hddIdProducto.ClientID%>');
            if (isNaN(parseInt(hddIdProducto.value)) || hddIdProducto.value.length <= 0) {
                alert('NO SE HA SELECCIONADO UN PRODUCTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            var hddIdDetalleDocumento = document.getElementById('<%=hddIdDetalleDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDetalleDocumento.value)) || hddIdDetalleDocumento.value.length <= 0) {
                alert('NO SE HA SELECCIONADO UN REGISTRO < NO SE TIENE LA VARIABLE IdDetalleDocumento >. NO SE PERMITE LA OPERACI�N.');
                return false;
            }

            return confirm('Desea continuar con la Operaci�n ?');
        }
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
    </script>

</asp:Content>
