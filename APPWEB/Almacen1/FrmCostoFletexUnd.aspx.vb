﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmCostoFletexUnd
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass
    Private listaMoneda As List(Of Entidades.Moneda)
    Private listaCostoFletexUnd As List(Of Entidades.CostoFletexUnd)

    Private Const _IdMagnitudPeso As Integer = 1 '************** PESO

    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
    End Enum

    Private Sub ConfigurarDatos()
        If Session("ConfigurarDatos") IsNot Nothing Then
            hddConfigurarDatos.Value = CStr(CInt(Session("ConfigurarDatos")))
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        valOnLoad_Frm()
    End Sub

    Private Sub valOnLoad_Frm()
        Try
            If (Not Me.IsPostBack) Then
                ConfigurarDatos()
                inicializarFrm()
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub inicializarFrm()

        Dim objCbo As New Combo
        With objCbo

            .LLenarCboTienda(Me.cboTiendaOrigen, False)
            .LLenarCboTienda(Me.cboTiendaDestino, False)
            ' --- Lineas
            .llenarCboTipoExistencia(CboTipoExistencia, False)
            .llenarCboLineaxTipoExistencia(cboLinea_Filtro, CInt(CboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea_Filtro, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            .LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cboSubLinea_Filtro.SelectedValue), False)

            '.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)
            '.LlenarCboLinea(Me.cboLinea_Filtro, True)
            '.LlenarCboSubLineaxIdLinea(Me.cboSubLinea_Filtro, CInt(Me.cboLinea_Filtro.SelectedValue), True)

        End With

        verFrm(FrmModo.Inicio, True, False)

    End Sub

    Private Sub limpiarFrm()

        '************** COMBOS
        Me.txtCodigoProd_Filtro.Text = ""
        Me.txtDescripcionProd_Filtro.Text = ""

        Me.GV_ProductoCostoFletexUnd_Save.DataSource = Nothing
        Me.GV_ProductoCostoFletexUnd_Save.DataBind()

    End Sub

    Private Sub actualizarControles()

        Me.btnNuevo.Visible = False
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnBuscar.Visible = False
        Me.Panel_Grilla.Enabled = False
        Me.Panel_Costo.Visible = False

        Me.cboTiendaOrigen.Enabled = True
        Me.cboTiendaDestino.Enabled = True
        Me.btnBuscarProd.Visible = False

        Select Case CInt(Me.hddFrmModo.Value)
            Case FrmModo.Inicio

                Me.btnNuevo.Visible = True
                Me.btnBuscar.Visible = True
                Me.Panel_Grilla.Enabled = True

            Case FrmModo.Nuevo

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.Panel_Costo.Visible = True
                Me.btnBuscarProd.Visible = True

            Case FrmModo.Editar

                Me.btnGuardar.Visible = True
                Me.btnCancelar.Visible = True
                Me.Panel_Costo.Visible = True
                Me.btnBuscarProd.Visible = True
                Me.cboTiendaOrigen.Enabled = False
                Me.cboTiendaDestino.Enabled = False

        End Select

    End Sub

    Private Sub verFrm(ByVal modo As Integer, ByVal limpiar As Boolean, ByVal cargarGrilla As Boolean)

        If (limpiar) Then
            limpiarFrm()
        End If

        If (cargarGrilla) Then

            Me.GV_CostoxUnd.DataSource = (New Negocio.CostoFletexUnd).CostoFletexUnd_SelectxParams_DT(CInt(Me.cboTiendaOrigen.SelectedValue), CInt(Me.cboTiendaDestino.SelectedValue), Nothing, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(Me.cboSubLinea_Filtro.SelectedValue), _
                                                                                                      Me.txtDescripcionProd_Filtro.Text, _
                                                                                                      Me.txtCodigoProd_Filtro.Text, CInt(CboTipoExistencia.SelectedValue))
            Me.GV_CostoxUnd.DataBind()
        End If

        Me.hddFrmModo.Value = CStr(modo)
        actualizarControles()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        valOnClick_btnCancelar()
    End Sub


    Private Sub valOnClick_btnCancelar()
        Try

            verFrm(FrmModo.Inicio, True, True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        registrar()
    End Sub
    Private Sub registrar()

        Try

            Dim lista As List(Of Entidades.CostoFletexUnd) = obtenerListaCostoFletexUnd()

            If ((New Negocio.CostoFletexUnd).CostoFletexUnd_Registrar(lista)) Then

                verFrm(FrmModo.Inicio, False, True)
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")

            Else

                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN")

            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Function obtenerListaCostoFletexUnd() As List(Of Entidades.CostoFletexUnd)

        Dim listaCostoFletexUnd As New List(Of Entidades.CostoFletexUnd)

        For i As Integer = 0 To Me.GV_ProductoCostoFletexUnd_Save.Rows.Count - 1

            Dim obj As New Entidades.CostoFletexUnd
            With obj

                .IdTiendaOrigen = CInt(Me.cboTiendaOrigen.SelectedValue)
                .IdTiendaDestino = CInt(Me.cboTiendaDestino.SelectedValue)
                .IdProducto = CInt(CType(Me.GV_ProductoCostoFletexUnd_Save.Rows(i).FindControl("hddIdProducto"), HiddenField).Value)
                .IdUnidadMedida = CInt(CType(Me.GV_ProductoCostoFletexUnd_Save.Rows(i).FindControl("cboUnidadMedida"), DropDownList).SelectedValue)
                .CostoxUnd = CDec(CType(Me.GV_ProductoCostoFletexUnd_Save.Rows(i).FindControl("txtCostoFlete"), TextBox).Text)
                .IdMoneda = CInt(CType(Me.GV_ProductoCostoFletexUnd_Save.Rows(i).FindControl("cboMoneda"), DropDownList).SelectedValue)

            End With
            listaCostoFletexUnd.Add(obj)

        Next

        Return listaCostoFletexUnd

    End Function

    Protected Sub btnNuevo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNuevo.Click
        valOnClick_btnNuevo()
    End Sub
    Private Sub valOnClick_btnNuevo()
        Try

            verFrm(FrmModo.Nuevo, True, False)

            Dim objCbo As New Combo
            objCbo.LLenarCboTienda(Me.cboTiendaOrigen, False)
            objCbo.LLenarCboTienda(Me.cboTiendaDestino, False)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnBuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscar.Click
        valOnClick_btnBuscar()
    End Sub
    Private Sub valOnClick_btnBuscar()
        Try

            Me.GV_CostoxUnd.DataSource = (New Negocio.CostoFletexUnd).CostoFletexUnd_SelectxParams_DT(CInt(Me.cboTiendaOrigen.SelectedValue), CInt(Me.cboTiendaDestino.SelectedValue), Nothing, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(Me.cboSubLinea_Filtro.SelectedValue), Me.txtDescripcionProd_Filtro.Text, Me.txtCodigoProd_Filtro.Text, CInt(CboTipoExistencia.SelectedValue))
            Me.GV_CostoxUnd.DataBind()

            If (Me.GV_CostoxUnd.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As EventArgs)

        valOnClick_btnEliminar(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)

    End Sub

    Protected Sub btnEditar_Click(ByVal sender As Object, ByVal e As EventArgs)

        valOnClick_btnEditar(CType(CType(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)

    End Sub

    Private Sub valOnClick_btnEditar(ByVal index As Integer)

        Try

            Dim IdTiendaOrigen As Integer = CInt(CType(Me.GV_CostoxUnd.Rows(index).FindControl("hddIdTiendaOrigen"), HiddenField).Value)
            Dim IdTiendaDestino As Integer = CInt(CType(Me.GV_CostoxUnd.Rows(index).FindControl("hddIdTiendaDestino"), HiddenField).Value)
            Dim IdProducto As Integer = CInt(CType(Me.GV_CostoxUnd.Rows(index).FindControl("hddIdProducto"), HiddenField).Value)

            Dim lista As List(Of Entidades.CostoFletexUnd) = (New Negocio.CostoFletexUnd).CostoFletexUnd_SelectProductoBasexParams(IdTiendaOrigen, IdTiendaDestino, IdProducto, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)

            If (lista.Count > 0) Then

                cargar_GUI(lista)
                verFrm(FrmModo.Editar, False, False)

            Else

                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")

            End If


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub

    Private Sub cargar_GUI(ByVal lista As List(Of Entidades.CostoFletexUnd))

        If (Me.cboTiendaOrigen.Items.FindByValue(CStr(lista(0).IdTiendaOrigen)) IsNot Nothing) Then
            Me.cboTiendaOrigen.SelectedValue = CStr(lista(0).IdTiendaOrigen)
        End If

        If (Me.cboTiendaDestino.Items.FindByValue(CStr(lista(0).IdTiendaDestino)) IsNot Nothing) Then
            Me.cboTiendaDestino.SelectedValue = CStr(lista(0).IdTiendaDestino)
        End If

        '**************** cargamos la lista Moneda
        Me.listaMoneda = (New Negocio.Moneda).SelectCbo
        Me.listaCostoFletexUnd = lista
        Me.GV_ProductoCostoFletexUnd_Save.DataSource = Me.listaCostoFletexUnd
        Me.GV_ProductoCostoFletexUnd_Save.DataBind()

    End Sub

    Private Sub valOnClick_btnEliminar(ByVal index As Integer)

        Try

            Dim IdTiendaOrigen As Integer = CInt(CType(Me.GV_CostoxUnd.Rows(index).FindControl("hddIdTiendaOrigen"), HiddenField).Value)
            Dim IdTiendaDestino As Integer = CInt(CType(Me.GV_CostoxUnd.Rows(index).FindControl("hddIdTiendaDestino"), HiddenField).Value)
            Dim IdProducto As Integer = CInt(CType(Me.GV_CostoxUnd.Rows(index).FindControl("hddIdProducto"), HiddenField).Value)

            If ((New Negocio.CostoFletexUnd).CostoFletexUnd_Delete(IdTiendaOrigen, IdTiendaDestino, IdProducto)) Then

                verFrm(FrmModo.Inicio, True, True)
                objScript.mostrarMsjAlerta(Me, "La Operación finalizó con éxito.")

            Else

                objScript.mostrarMsjAlerta(Me, "PROBLEMAS EN LA OPERACIÓN.")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try


    End Sub

    Private Sub btnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        valOnClick_btnLimpiar()
    End Sub
    Private Sub valOnClick_btnLimpiar()
        Try
            limpiarFrm()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnBuscarProd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuscarProd.Click
        valOnClick_btnBuscarProd()
    End Sub
    Private Sub valOnClick_btnBuscarProd()
        Try

            '*****
            Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()

            Me.listaMoneda = (New Negocio.Moneda).SelectCbo
            Me.listaCostoFletexUnd = (New Negocio.CostoFletexUnd).CostoFletexUnd_SelectProductoBasexParams(CInt(Me.cboTiendaOrigen.SelectedValue), CInt(Me.cboTiendaDestino.SelectedValue), Nothing, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(Me.cboSubLinea_Filtro.SelectedValue), Me.txtDescripcionProd_Filtro.Text, Me.txtCodigoProd_Filtro.Text, CInt(CStr(CboTipoExistencia.SelectedValue)), tableTipoTabla)

            Me.GV_ProductoCostoFletexUnd_Save.DataSource = Me.listaCostoFletexUnd
            Me.GV_ProductoCostoFletexUnd_Save.DataBind()

            If (Me.GV_ProductoCostoFletexUnd_Save.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_ProductoCostoFletexUnd_Save_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_ProductoCostoFletexUnd_Save.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim cboMoneda As DropDownList = CType(e.Row.FindControl("cboMoneda"), DropDownList)
            cboMoneda.DataSource = Me.listaMoneda
            cboMoneda.DataBind()

            If (cboMoneda.Items.FindByValue(CStr(Me.listaCostoFletexUnd(e.Row.RowIndex).IdMoneda)) IsNot Nothing) Then

                cboMoneda.SelectedValue = CStr(Me.listaCostoFletexUnd(e.Row.RowIndex).IdMoneda)

            End If

            Dim cboUnidadMedida As DropDownList = CType(e.Row.FindControl("cboUnidadMedida"), DropDownList)
            cboUnidadMedida.DataSource = Me.listaCostoFletexUnd(e.Row.RowIndex).getListaUnidadMedida
            cboUnidadMedida.DataBind()

            If (cboUnidadMedida.Items.FindByValue(CStr(Me.listaCostoFletexUnd(e.Row.RowIndex).IdUnidadMedida)) IsNot Nothing) Then

                cboUnidadMedida.SelectedValue = CStr(Me.listaCostoFletexUnd(e.Row.RowIndex).IdUnidadMedida)

            End If

        End If
    End Sub

    Private Sub cboLinea_Filtro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea_Filtro.SelectedIndexChanged
        valOnChange_cboLinea_Filtro()
    End Sub
    Private Sub valOnChange_cboLinea_Filtro()
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cboSubLinea_Filtro, CInt(Me.cboLinea_Filtro.SelectedValue), True)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub CboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia.SelectedIndexChanged
        Try
            Dim objcbo As New Combo
            objcbo.llenarCboLineaxTipoExistencia(cboLinea_Filtro, CInt(CboTipoExistencia.SelectedValue), True)
            objcbo.LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea_Filtro, CInt(Me.cboLinea_Filtro.SelectedValue), CInt(CboTipoExistencia.SelectedValue), True)
            'objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



#Region "Filtro de busquedad avanza de productos"

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0

            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cboLinea_Filtro.SelectedValue), CInt(Me.cboSubLinea_Filtro.SelectedValue), CInt(cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1
            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)
        Next

        Return dt
    End Function

    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function

#End Region


    Protected Sub cboSubLinea_Filtro_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboSubLinea_Filtro.SelectedIndexChanged
        Dim objCbo As New Combo
        objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cboSubLinea_Filtro.SelectedValue), False)
        GV_FiltroTipoTabla.DataBind()
    End Sub
End Class