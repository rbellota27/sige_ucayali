﻿Imports Entidades
Imports Negocio
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class frm_trazabildad_almacen
    Inherits System.Web.UI.Page
    Dim clsNegocio As New LN_Despachos
    Private objScript As New ScriptManagerClass
    Dim objNegocioTrazabilidad As New LN_TRAZABILIDAD_DISTRIBUCION
    Dim objNegocioDropDownList As New LNValorizadoCajas
    Dim objNegocioSector As New LN_Sector
    Dim script As New ScriptManagerClass
    Public Shared _indiceDistribucion As Integer = 0
    Public Shared _indiceSector As Integer = 0
    Public Shared _idProductoFormularioSector As String = String.Empty
    Public Shared _idDocumentoBusqueda As Integer = 0

    Private Property indiceProductoFormularioSector() As String
        Get
            Return _idProductoFormularioSector
        End Get
        Set(ByVal value As String)
            _idProductoFormularioSector = value
        End Set
    End Property

    Private Property indiceDistribucion() As Integer
        Get
            Return _indiceDistribucion
        End Get
        Set(ByVal value As Integer)
            _indiceDistribucion = value
        End Set
    End Property
    Private Enum FrmModo
        Inicio = 0
        Nuevo = 1
        Editar = 2
        Buscar_Doc = 3
        Documento_Buscar_Exito = 4
        Documento_Save_Exito = 5
        Documento_Anular_Exito = 6
        Documento_EnProceso = 7
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptmanager As ScriptManager = scriptmanager.GetCurrent(Me.Page)
        scriptmanager.RegisterPostBackControl(Me.btnImprimir)
        If Not IsPostBack Then

            inicializarFormulario()
            verFrm(FrmModo.Nuevo, False, True, True, True)
        End If
    End Sub

    Private Sub cargarSectores(ByVal idDocumento As Integer, ByVal idProducto As Integer)
        Dim ds As New DataSet
        ds = objNegocioSector.LN_listarSectores(idDocumento, idProducto)
        Me.gvSectores.DataSource = ds.Tables(0)
        Me.gvSectores.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaSector');", True)
    End Sub

    Protected Sub inicializarFormulario()
        armarEstructuraTono()
        Me.btnEditar.Visible = False
        Me.btnAnular.Visible = False
        Me.btnImprimir.Visible = False
        Me.btnImprimirEstandar.Visible = False
        Me.gvDocumentoReferencia.DataSource = Nothing
        Me.gvDocumentoReferencia.DataBind()
        Me.gvistribucionMercaderia.DataSource = Nothing
        Me.gvistribucionMercaderia.DataBind()
        Me.pnlDatosGenerales.Enabled = True
        Me.pnlDocumentoReferencia.Enabled = True
        Me.pnlDistribucionMercaderia.Enabled = True
        Dim objNegocioCombo As New LNValorizadoCajas
        Me.txtFechaEmision.Text = Date.Now.ToShortDateString
        Me.txtFechainiciofiltro.Text = Date.Now.ToShortDateString
        Me.txtFechaFinFiltro.Text = Date.Now.ToShortDateString
        ddlTipoDocumento.DataSource = objNegocioCombo.LN_ReturnDataTable("", "TIPO_DOCUMENTO")
        ddlTipoDocumento.DataTextField = "tdoc_NombreLargo"
        ddlTipoDocumento.DataValueField = "IdTipoDocumento"
        ddlTipoDocumento.DataBind()
        Dim cbo As New Combo
        With cbo
            .LlenarCboEmpresaxIdUsuario(Me.ddlEmpresa, CInt(Session("IdUsuario")), False)
            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.ddlTienda, CInt(Me.ddlEmpresa.SelectedValue), CInt(Session("IdUsuario")), False)
            .LLenarCboSeriexIdsEmpTienTipoDoc(Me.ddlSerie, CInt(Me.ddlEmpresa.SelectedValue), CInt(Me.ddlTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            .LLenarCboEstadoDocumento(Me.ddlEstado)
        End With
        GenerarCodigoDocumento()
    End Sub

    Private Sub GenerarCodigoDocumento()

        If (IsNumeric(Me.ddlSerie.SelectedValue)) Then
            Me.txtnroDocumento.Text = (New Negocio.Documento).GenerarNroDocumentoxIdSerie(CInt(Me.ddlSerie.SelectedValue))
        Else
            Me.txtnroDocumento.Text = ""
        End If

    End Sub

    Private Sub ddlEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpresa.SelectedIndexChanged
        GenerarCodigoDocumento()
    End Sub

    Private Sub ddlTienda_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTienda.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LLenarCboSeriexIdsEmpTienTipoDoc(Me.ddlSerie, CInt(Me.ddlEmpresa.SelectedValue), CInt(Me.ddlTienda.SelectedValue), CInt(Me.hddIdTipoDocumento.Value))
            GenerarCodigoDocumento()
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub ddlSerie_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSerie.SelectedIndexChanged
        GenerarCodigoDocumento()
    End Sub

    Private Sub verFrm(ByVal FrmModo As Integer, ByVal limpiar As Boolean, ByVal removeSession As Boolean, ByVal initSession As Boolean, ByVal initParametrosGral As Boolean)

        If (limpiar) Then
            'LimpiarFrm()
        End If

        If (removeSession) Then
            Session.Remove(CStr(ViewState("DetalleDocumento")))
            Session.Remove(CStr(ViewState("listaDocumentoRef")))
        End If

        If (initSession) Then
        End If

        If (initParametrosGral) Then

            '******* FECHA DE VCTO
            'Dim listaParametros() As Decimal = (New Negocio.ParametroGeneral).getValorParametroGeneral(New Integer() {1, 2})
        End If

    End Sub

    Protected Sub rblistFiltros_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rblistFiltros.SelectedIndexChanged
        Select Case rblistFiltros.SelectedValue
            Case 1 'por fecha de emisión
                Me.pnlFiltroFechas.Visible = True
                Me.pnlFiltroNroDocumento.Visible = False
            Case 2 'Por numero de documento
                Me.pnlFiltroFechas.Visible = False
                Me.pnlFiltroNroDocumento.Visible = True
        End Select
        objScript.onCapa(Me, "capaOrdenCompra")
    End Sub

    Protected Sub btnBuscarDocumento(ByVal sender As Object, ByVal e As EventArgs)
        Dim btn As Button = DirectCast(sender, Button)
        Dim commandName As String = btn.CommandName.ToString()
        Dim dt As New DataTable
        Select Case Me.rblistFiltros.SelectedValue
            Case 1 ' por fechas
                Dim fechaInicioFiltro As Date = CDate(Me.txtFechainiciofiltro.Text)
                Dim fechaFinFiltro As Date = CDate(Me.txtFechaFinFiltro.Text)
                dt = objNegocioTrazabilidad.LN_listarDocReferencia(0, 0, ddlTienda.SelectedValue, fechaInicioFiltro, fechaFinFiltro)
            Case 2 'por nro documento
                Dim nroSerieFiltro As Integer = CInt(Val(Me.txtnroSerie.Text))
                Dim nroDocumentoFiltro As Integer = CInt(Val(Me.txtnroCodigo.Text))
                dt = objNegocioTrazabilidad.LN_listarDocReferencia(nroSerieFiltro, nroDocumentoFiltro, ddlTienda.SelectedValue, 0, 0)
                Session("dt_ref_orden_compra") = dt
        End Select
        Me.gvOrdenCompra.DataSource = dt
        Me.DataBind()
        objScript.onCapa(Me, "capaOrdenCompra")
    End Sub

    Private Sub gvOrdenCompra_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvOrdenCompra.SelectedIndexChanged
        Dim i As Integer = Me.gvOrdenCompra.SelectedIndex
        Dim dt As New DataTable
        dt.TableName = "dt_OCompra_Referencias"
        dt.Columns.Add("Nombre")
        dt.Columns.Add("nroDocumento")
        dt.Columns.Add("FechaEmision")
        dt.Columns.Add("Estado")
        'For Each row As GridViewRow In Me.gvOrdenCompra.Rows
        Dim dr As DataRow = dt.NewRow
        dr("Nombre") = gvOrdenCompra.Rows(i).Cells(1).Text
        dr("nroDocumento") = gvOrdenCompra.Rows(i).Cells(2).Text
        dr("FechaEmision") = gvOrdenCompra.Rows(i).Cells(3).Text
        dr("Estado") = gvOrdenCompra.Rows(i).Cells(4).Text
        dt.Rows.Add(dr)
        'Next
        If dt.Rows.Count > 0 Then
            Dim idDocumento As Integer = CInt(gvOrdenCompra.SelectedDataKey.Values("idDocumento"))
            cargarProductos(idDocumento)
            cargarTonosxProducto()
            gvDocumentoReferencia.DataSource = dt
            gvDocumentoReferencia.DataBind()
        End If
    End Sub

    Private Sub cargarProductos(ByVal idDocumento As Integer)
        Dim dt As New DataTable
        dt = objNegocioTrazabilidad.LN_listarProductos(idDocumento)
        gvistribucionMercaderia.DataSource = dt
        gvistribucionMercaderia.DataBind()
    End Sub
    Private Sub cargarTonosxProducto()
        For Each row As GridViewRow In Me.gvistribucionMercaderia.Rows
            Dim datakey As DataKey = Me.gvistribucionMercaderia.DataKeys(row.RowIndex)
            Dim idProducto_principal As Integer = datakey(0)
            Dim dt As New DataTable
            dt = objNegocioDropDownList.LN_ReturnDataTable(idProducto_principal, "LISTAR_TONO_X_PRODUCTO")
            If dt.Rows.Count = 1 Then 'Esto sucede si encuentra un solo tono para el producto seleccionado, toma la cantidad total del producto en el único tono.
                dt.Rows(0).Item(2) = CDec(row.Cells(5).Text).ToString()
            ElseIf dt.Rows.Count = 0 Then 'Esto sucede si NO encuentra tonos relacionados con el producto seleccionado
                script.mostrarMsjAlerta(Me.Page, "El Producto " & row.Cells(3).Text & " no tiene tono relacionados. Deberá crear como mínimo un tono para continuar la distribución.")
                row.BackColor = Drawing.Color.Red
            End If
            ViewState("TonoxProducto_" & idProducto_principal) = dt
        Next
    End Sub

    Private Sub imgbtnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnBuscar.Click
        clickBtnBuscar()
        Me.pnlDatosGenerales.Enabled = False
        Me.pnlDocumentoReferencia.Enabled = False
        Me.pnlDistribucionMercaderia.Enabled = False
        Dim txtnroDocumento As String = Me.txtnroDocumento.Text
        Dim dt_doc_ref As New DataTable
        Dim dt_prod As New DataTable
        _idDocumentoBusqueda = objNegocioTrazabilidad.LN_obtenerIdDocumento(Me.ddlSerie.SelectedItem.Text, Me.txtnroDocumento.Text.Trim(), Me.ddlTipoDocumento.SelectedValue)
        If _idDocumentoBusqueda = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     alert('Documento no existe');", True)
        Else
            dt_doc_ref = objNegocioTrazabilidad.LN_trazabilidad_Ver_Registro("DOC_REF", ddlSerie.SelectedItem.Text, txtnroDocumento, Me.ddlEmpresa.SelectedValue, ddlTienda.SelectedValue, Me.ddlTipoDocumento.SelectedValue)
            gvDocumentoReferencia.DataSource = dt_doc_ref
            gvDocumentoReferencia.DataBind()
            dt_prod = objNegocioTrazabilidad.LN_trazabilidad_Ver_Registro("PRODUCTOS", ddlSerie.SelectedItem.Text, txtnroDocumento, Me.ddlEmpresa.SelectedValue, ddlTienda.SelectedValue, Me.ddlTipoDocumento.SelectedValue)
            gvistribucionMercaderia.DataSource = dt_prod
            gvistribucionMercaderia.DataBind()
        End If

    End Sub

    Private Sub gvistribucionMercaderia_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvistribucionMercaderia.RowCommand
        Dim indice As Integer = e.CommandArgument
        Dim key = Me.gvistribucionMercaderia.DataKeys(indice)
        _idProductoFormularioSector = key(0) ' al abrir el formulario de edición de cantidades por sector, obtengo el indice del producto único
        _indiceDistribucion = indice 'obtengo el indice seleccionado de la grilla distribucion
        cargarTonos(_idProductoFormularioSector)
        cargarSectores()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaAsignarTono');", True)


        ''Se llena descripciones del producto seleccionado
        'Me.lblTextoCabecera.Text = Me.gvistribucionMercaderia.Rows(indice).Cells(2).Text
        'Me.lblUnidadMedida.Text = Me.gvistribucionMercaderia.Rows(indice).Cells(3).Text
        'Me.lblCantidad.Text = Me.gvistribucionMercaderia.Rows(indice).Cells(4).Text
        'Me.lblLinea.Text = Me.gvistribucionMercaderia.Rows(indice).Cells(5).Text
        'Me.lblSublinea.Text = Me.gvistribucionMercaderia.Rows(indice).Cells(6).Text
        'Me.lblProveedor.Text = Me.gvistribucionMercaderia.Rows(indice).Cells(7).Text
        'cargarSectores(_idDocumentoBusqueda, indiceProductoFormularioSector) 'qwe
        'If Not IsNothing(ViewState("vista_" & indiceProductoFormularioSector)) Then
        '    Dim ls As List(Of String) = ViewState("vista_" & indiceProductoFormularioSector)
        '    For Each row As GridViewRow In Me.gvSectores.Rows
        '        Dim txt As TextBox = TryCast(row.FindControl("txtCantidadxDistribuir"), TextBox)
        '        Dim keySector As DataKey = Me.gvSectores.DataKeys(row.RowIndex)
        '        Dim idSector As Integer = keySector(0)
        '        For i As Integer = 0 To ls.Count - 1
        '            Dim array As Array = ls.Item(i).Split(",")
        '            If array(0) = idSector Then
        '                txt.Text = array(1)
        '            End If
        '        Next
        '    Next
        'End If
    End Sub

    Private Sub gvistribucionMercaderia_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvistribucionMercaderia.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dt As New DataTable
            Dim chkList As CheckBoxList = TryCast(e.Row.FindControl("CheckBoxList1"), CheckBoxList)
            Dim btn As Button = TryCast(e.Row.FindControl("btnAsignarSector"), Button)
            Dim img As Image = TryCast(e.Row.FindControl("imgAlerta"), Image)
            dt = objNegocioDropDownList.LN_ReturnDataTable("", "TBL_ALMACEN_SECTOR")
            chkList.DataSource = dt
            chkList.DataBind()

            Dim dataKey = gvistribucionMercaderia.DataKeys(e.Row.RowIndex).Values
            Dim idSector As String = IIf(IsDBNull(dataKey(2)), 0, dataKey(2))
            Dim idProducto As Integer = dataKey(0)
            Dim arraySector As Array = idSector.Split(",")
            For i As Integer = 0 To arraySector.Length - 1
                Dim valor As Integer = CInt(arraySector(i))
                If Not IsNothing(chkList.Items.FindByValue(valor)) Then
                    chkList.Items.FindByValue(valor).Selected = True
                End If
            Next
            '
            'Valida si Tiene un solo tono o más de un tono
            If objNegocioDropDownList.LN_ReturnDataTable(idProducto, "VALIDA_CANTIDAD_TONO").Rows(0).Item(0) = 1 Then
                'btn.Enabled = False
                'btn.ToolTip = "Este producto solo tiene un Tono disponible, en el " & chkList.SelectedItem.Text & ". La cantidad total se distribuirá al " & chkList.SelectedItem.Text
                e.Row.BackColor = Drawing.Color.LightGreen
            ElseIf objNegocioDropDownList.LN_ReturnDataTable(idProducto, "VALIDA_CANTIDAD_TONO").Rows(0).Item(0) > 1 Then
                btn.Enabled = True
                'btn.ToolTip = "Este producto contiene mas de un tono disponible, deberá asignar manualmente el tono."
                e.Row.BackColor = Drawing.Color.LightYellow
                '
                'Asigna Valor 0 si el producto tiene mas de un tono
                '
                TryCast(e.Row.FindControl("hdfEstadoAsignacionTonos"), HiddenField).Value = 0
            ElseIf objNegocioDropDownList.LN_ReturnDataTable(idProducto, "VALIDA_CANTIDAD_TONO").Rows(0).Item(0) = 0 Then
                btn.Enabled = True
                'btn.ToolTip = "Este producto no tiene tonos asociados, para continuar deberá asociar por lo menos un tono."
                Me.btnGuardar.Enabled = False
            ElseIf arraySector.Length = 1 And arraySector(0) = 0 Then
                chkList.Enabled = False
                'btn.ToolTip = "No existe una relación entre esta Sublinea y el sector. Deberá ir a la tabla maestra, realizar la relación para luego continuar con la distribución."
                e.Row.BackColor = Drawing.Color.Red
            End If
            Dim contadorCheck As Integer = 0
            For i As Integer = 0 To chkList.Items.Count - 1
                If Not chkList.Items(i).Selected Then
                    chkList.Items(i).Enabled = False
                    contadorCheck += 1
                End If
            Next
            If contadorCheck = chkList.Items.Count Then
                divSector.Visible = True
            Else
                divSector.Visible = False
            End If
        End If
    End Sub

    Protected Sub cargarSectoresMasivo(ByVal sender As Object, ByVal e As EventArgs)
        Dim txtnroDocumento As String = Me.txtnroDocumento.Text
        Dim dt_prod As New DataTable
        dt_prod = objNegocioTrazabilidad.LN_trazabilidad_Ver_Registro("PRODUCTOS", ddlSerie.SelectedItem.Text, txtnroDocumento, Me.ddlEmpresa.SelectedValue, ddlTienda.SelectedValue, Me.ddlTipoDocumento.SelectedValue)

        gvistribucionMercaderia.DataSource = dt_prod
        gvistribucionMercaderia.DataBind()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        clickBtnNuevo()
        nuevoDocumento()
    End Sub

    Private Sub nuevoDocumento()
        Me.gvDocumentoReferencia.DataSource = Nothing
        Me.gvDocumentoReferencia.DataBind()
        Me.gvistribucionMercaderia.DataSource = Nothing
        Me.gvistribucionMercaderia.DataBind()
        Me.pnlDatosGenerales.Enabled = True
        Me.pnlDocumentoReferencia.Enabled = True
        Me.pnlDistribucionMercaderia.Enabled = True
        Dim objNegocioCombo As New LNValorizadoCajas
        Me.txtFechaEmision.Text = Date.Now.ToShortDateString
        Me.txtFechainiciofiltro.Text = Date.Now.ToShortDateString
        Me.txtFechaFinFiltro.Text = Date.Now.ToShortDateString
        GenerarCodigoDocumento()
    End Sub

    Private Sub cargarTonos(ByVal idProducto As Integer)
        'llena el dropdownllist con los tonos correspondientes del producto
        Dim dt As New DataTable
        dt = objNegocioDropDownList.LN_ReturnDataTable(idProducto, "TBL_TONO")
        ddltono.DataSource = dt
        ddltono.DataBind()
        ddltono.Items.Insert(0, New ListItem("Seleccione un tono", "-1"))
        'llena la grilla si es que tuviese tonos asociados
        Me.gvTono.DataSource = ViewState("TonoxProducto_" & idProducto)
        Me.gvTono.DataBind()
    End Sub

    Private Sub cargarSectores()
        'llena el dropdownllist con los tonos correspondientes del producto
        Dim dt As New DataTable
        dt = objNegocioDropDownList.LN_ReturnDataTable("", "TBL_ALMACEN_SECTOR_MANTENIMIENTO")
        Me.rBListSectores.DataSource = dt
        Me.rBListSectores.DataBind()
        ddlSector.DataSource = dt
        ddlSector.DataBind()
        ddlSector.Items.Insert(0, New ListItem("Seleccione un sector", "-1"))

        Dim dataKey = gvistribucionMercaderia.DataKeys(_indiceDistribucion).Values
        Dim idSector As String = IIf(IsDBNull(dataKey(2)), 0, dataKey(2))
        Dim idProducto As Integer = dataKey(0)
        Dim arraySector As Array = idSector.Split(",")
        For i As Integer = 0 To arraySector.Length - 1
            Dim valor As Integer = CInt(arraySector(i))
            If Not IsNothing(Me.rBListSectores.Items.FindByValue(valor)) Then
                Me.rBListSectores.Items.FindByValue(valor).Selected = True
            End If
        Next
    End Sub

    Private Sub btnEditar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Me.pnlDatosGenerales.Enabled = True
        Me.pnlDocumentoReferencia.Enabled = True
        Me.pnlDistribucionMercaderia.Enabled = True
    End Sub

    Private Sub clickBtnNuevo()
        Me.btnNuevo.Visible = True
        Me.btnBuscar.Visible = True
        Me.btnGuardar.Visible = True
        Me.btnDocumentoReferencia.Visible = True
        Me.btnImprimir.Visible = False
        Me.btnEditar.Visible = False
        Me.btnAnular.Visible = False
    End Sub

    Private Sub clickBtnBuscar()
        Me.btnNuevo.Visible = True
        Me.btnEditar.Visible = True
        Me.btnAnular.Visible = True
        Me.btnImprimir.Visible = True
        Me.btnBuscar.Visible = False
        Me.btnGuardar.Visible = False
        Me.btnDocumentoReferencia.Visible = False
    End Sub

    Private Sub clickBtnEditar()
        Me.btnNuevo.Visible = True
        Me.btnEditar.Visible = False
        Me.btnAnular.Visible = False
        Me.btnImprimir.Visible = False
        Me.btnBuscar.Visible = True
        Me.btnGuardar.Visible = True
        Me.btnDocumentoReferencia.Visible = True
    End Sub

    Private Function armarEstructuraTono() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add(New DataColumn("ID_REL_TONO_PROD", GetType(Int32)))
        dt.Columns.Add(New DataColumn("NOM_TONO", GetType(String)))
        dt.Columns.Add(New DataColumn("CANTIDAD", GetType(Decimal)))
        ViewState("dt_Tonos") = dt
        Return dt
    End Function

    Private Sub btAgregarTono_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregarTono.Click
        If Me.ddltono.SelectedValue <> -1 Then
            Dim dt As New DataTable
            dt = ViewState("TonoxProducto_" & _idProductoFormularioSector)
            Dim datarow As DataRow() = dt.Select("ID_REL_TONO_PROD = " & Me.ddltono.SelectedValue)
            If datarow.Length = 0 Then
                Dim fila As DataRow = dt.NewRow
                fila("ID_REL_TONO_PROD") = Me.ddltono.SelectedValue
                fila("NOM_TONO") = Me.ddltono.SelectedItem.Text
                fila("CANTIDAD") = 0
                dt.Rows.Add(fila)
            End If
            Me.gvTono.DataSource = dt
            Me.gvTono.DataBind()
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaAsignarTono');", True)
    End Sub

    Private Sub gvSectores_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSectores.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim btn As Button = TryCast(e.Row.FindControl("btnAsignarTono"), Button)
            Dim key_sector As DataKey = Me.gvSectores.DataKeys(e.Row.RowIndex)
            Dim id_sector As Integer = key_sector(0)
            Dim chck As CheckBoxList = TryCast(gvistribucionMercaderia.Rows(_indiceDistribucion).FindControl("CheckBoxList1"), CheckBoxList)
            For i As Integer = 0 To chck.Items.Count - 1
                If chck.Items(i).Selected Then
                    If chck.Items(i).Value = id_sector Then
                        btn.Enabled = True
                    End If
                Else
                    If chck.Items(i).Value = id_sector Then
                        btn.Enabled = False
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub btnGuardarTonos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardarTonos.Click
        Dim ls As New List(Of String)
        Dim idProdcuto As Integer = _idProductoFormularioSector
        Dim dt As New DataTable
        dt = armarEstructuraTono()
        For Each row As GridViewRow In Me.gvTono.Rows
            Dim txt As TextBox = DirectCast(row.FindControl("txtCantidadTono"), TextBox)
            Dim dr As DataRow = dt.NewRow
            dr("ID_REL_TONO_PROD") = CInt(row.Cells(1).Text)
            dr("NOM_TONO") = row.Cells(2).Text.ToString()
            dr("CANTIDAD") = CDec(txt.Text)
            dt.Rows.Add(dr)
        Next
        ViewState("TonoxProducto_" & _idProductoFormularioSector) = dt

        'Dim keySector As DataKey = Me.gvSectores.DataKeys(_indiceSector)
        'Dim idSector As Integer = keySector(0)

        'ViewState("Tonos_" & _idProductoFormularioSector & "_" & idSector) = dt
        'ls.Add(_idProductoFormularioSector & "," & idSector & "," & "Tonos" & _idProductoFormularioSector & "_" & idSector)
        '
        'Asigna el valor 1 al hiddenField de la tabla gvistribucionMercaderia y su respectiva fila, esto significa que ya se asignó un tono manual.
        '
        TryCast(Me.gvistribucionMercaderia.Rows(_indiceDistribucion).FindControl("hdfEstadoAsignacionTonos"), HiddenField).Value = 1
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        '
        'Validar Si todos los productos tienen la cantidad asociados al tono
        '==================================================================
        '
        'recupero los tonos para cada producto
        '
        For Each row_validacion As GridViewRow In Me.gvistribucionMercaderia.Rows
            Dim dataKey = gvistribucionMercaderia.DataKeys(row_validacion.RowIndex).Values
            Dim idProducto As Integer = dataKey(0)

            Dim suma_cantidades_x_tono As Decimal = 0
            Dim cantidadSaldoMasivo As Decimal = CDec(row_validacion.Cells(5).Text)
            Dim dt As New DataTable
            dt = ViewState("TonoxProducto_" & idProducto)
            If dt.Rows.Count = 0 Then
                script.mostrarMsjAlerta(Me.Page, "El Producto con código " & row_validacion.Cells(2).Text & " no tiene tonos disponibles.")
                Exit Sub
            ElseIf dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    suma_cantidades_x_tono += CDec(Val(dt.Rows(i).Item(2)))
                Next
                If suma_cantidades_x_tono < cantidadSaldoMasivo Or suma_cantidades_x_tono > cantidadSaldoMasivo Then
                    script.mostrarMsjAlerta(Me.Page, "La cantidad distribuida por tonos para el producto " & row_validacion.Cells(2).Text & " excedió lo permitido.")
                    Exit Sub
                End If
            End If
        Next

        '
        'Si pasa todas las validaciones de tonos y cantidades se procede a recorrer y guardar
        '
        Dim objEntidadesDocumento As New Entidades.Documento
        Dim idDocumentoRetorno As Integer
        With objEntidadesDocumento
            .Serie = ddlSerie.SelectedItem.Text
            GenerarCodigoDocumento()
            .Codigo = Me.txtnroDocumento.Text.Trim()
            .IdEmpresa = Me.ddlEmpresa.SelectedValue
            .IdTipoDocumento = Me.ddlTipoDocumento.SelectedValue
            .IdTienda = Me.ddlTienda.SelectedValue
            .FechaEmision = Me.txtFechaEmision.Text.Trim()
        End With
        Dim ind As Integer = CInt(gvOrdenCompra.SelectedDataKey.Values("idDocumento"))
        '
        'Guarda Cabecera del documento
        '
        idDocumentoRetorno = objNegocioTrazabilidad.LN_trazabilidad_GuardarKardex(CInt(ind), Session("IdUsuario"), objEntidadesDocumento)

        For Each row As GridViewRow In Me.gvistribucionMercaderia.Rows
            Dim checkbox As CheckBox = TryCast(row.FindControl("chkSeleccionarRow"), CheckBox)
            If checkbox.Checked Then
                Dim dataKey = gvistribucionMercaderia.DataKeys(row.RowIndex).Values
                Dim idProducto As Integer = dataKey(0)
                Dim idUnidadMedida As Integer = dataKey(1)
                Dim idSector As Integer = dataKey(2)
                'Dim btn As Button = TryCast(row.FindControl("btnAsignarSector"), Button)
                'Dim cantidadSaldoMasivo As Decimal = CDec(row.Cells(5).Text)
                'Dim codigoProducto As Decimal = CDec(row.Cells(2).Text)
                'Dim cantidadSaldo As Decimal = 0
                'Dim idTono As Integer = 0

                '
                'inserta productos TBL_DISTRIB_SALDO_KARDEX
                '
                objNegocioTrazabilidad.LN_trazabilidad_GuardarSaldoKardex(idProducto, idUnidadMedida, checkbox.Checked, Session("IdUsuario"), idDocumentoRetorno)

                Dim dt_tonos_x_producto As New DataTable
                dt_tonos_x_producto = ViewState("TonoxProducto_" & idProducto)
                For i As Integer = 0 To dt_tonos_x_producto.Rows.Count - 1

                    '
                    'inserta productos TBL_DISTRIB_SALDO_KARDEX_DETALLE, 
                    '
                    objNegocioTrazabilidad.LN_trazabilidad_GuardarDetalleSaldoKardex(idProducto, idSector, dt_tonos_x_producto.Rows(i).Item(2), idDocumentoRetorno, dt_tonos_x_producto.Rows(i).Item(0))

                    '
                    'inserta tbl_trazabilidad_detalle_stock, esta tabla es el detalle del kardex entradas y salidas generales de los sectores
                    '
                    objNegocioTrazabilidad.LN_Insert_Kardex_detallado(idProducto, 0, idSector, dt_tonos_x_producto.Rows(i).Item(2), dt_tonos_x_producto.Rows(i).Item(0))
                Next
            End If
        Next
        Me.pnlDatosGenerales.Enabled = False
        Me.pnlDocumentoReferencia.Enabled = False
        Me.pnlDistribucionMercaderia.Enabled = False
        'Recargo la grilla de oredenes de compra, con la finalidad de desaparecer la orden que acabo de guardar y seguir un nuevo registro si se desea.
        Me.gvOrdenCompra.DataSource = Nothing
        Me.gvOrdenCompra.DataBind()
        'muestro una alerta de registro guardado satisfactoriamente
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     alert('Distribución Exitosa...');", True)
        'Me.pnlDatosGenerales.Enabled = False
        'Me.pnlDocumentoReferencia.Enabled = False
        'Me.pnlDistribucionMercaderia.Enabled = False
        ''Recargo la grilla de oredenes de compra, con la finalidad de desaparecer la orden que acabo de guardar y seguir un nuevo registro si se desea.
        'Me.gvOrdenCompra.DataSource = Nothing
        'Me.gvOrdenCompra.DataBind()
        ''muestro una alerta de registro guardado satisfactoriamente
        'ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     alert('Distribución Exitosa...');", True)
    End Sub

    Private Sub btnCrearTono_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearTono.Click
        If Me.ddlSector.Visible Then
            If Me.ddlSector.SelectedValue = -1 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaAsignarTono');alert('Seleccione un Sector');", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaAsignarTono');onCapa('capaCrearTono');", True)
            End If
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaAsignarTono');onCapa('capaCrearTono');", True)
        End If

    End Sub

    Private Sub btnTono_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTono.Click
        Dim idSector As Integer = 0
        If ddlSector.Visible Then
            idSector = ddlSector.SelectedValue
        End If
        If objNegocioTrazabilidad.LN_Insert_Tono(Me.txtNombreTono.Text.Trim(), Me.txtDescripcionTono.Text.Trim(), _idProductoFormularioSector) Then
            cargarTonos(_idProductoFormularioSector)
            Me.btnGuardar.Enabled = True
            gvOrdenCompra_SelectedIndexChanged(gvistribucionMercaderia, e)
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaAsignarTono')", True)
            script.mostrarMsjAlerta(Me.Page, "alert('Tono " & Me.txtNombreTono.Text.Trim() & " creado.');")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaAsignarTono')", True)
            script.mostrarMsjAlerta(Me.Page, "No se creó el tono, inconsistencia de datos...")
        End If
    End Sub

    Private Sub gvTono_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTono.SelectedIndexChanged
        Dim indice As Integer = gvTono.SelectedIndex
        Dim dt As New DataTable
        dt = ViewState("TonoxProducto_" & _idProductoFormularioSector)
        Dim dr As DataRow() = dt.Select("ID_REL_TONO_PROD = " & gvTono.Rows(indice).Cells(1).Text.ToString())
        dt.Rows.Remove(dr(0))
        ViewState("TonoxProducto_" & _idProductoFormularioSector) = dt
        gvTono.DataSource = dt
        gvTono.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaAsignarTono')", True)
    End Sub


    Protected Sub btnExportar_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim button As New Button
        button = DirectCast(sender, Button)
        Dim reporte As New ReportDocument
        Dim dt As New DataTable
        Dim formatType As ExportFormatType
        dt = clsNegocio.LN_Reporte_Distribucion(_idDocumentoBusqueda)
        'dt = clsNegocio.LN_Reporte(Me.txt3.Text, Me.txt4.Text, ddlFiltroTurno.SelectedItem.ToString(), ddlFiltroPlaca.SelectedItem.ToString())
        reporte = New rpt_distribucion
        'Dim ruta As String = Server.MapPath("rpt_programacion_despacho2.rpt")
        'reporte.Load(ruta)

        reporte.SetDataSource(dt)

        Select Case button.CommandName.ToLower()
            Case "pdf"
                formatType = ExportFormatType.PortableDocFormat
                Exit Select
            Case "word"
                formatType = ExportFormatType.WordForWindows
                Exit Select
            Case "excel"
                formatType = ExportFormatType.Excel
                Exit Select
        End Select
        Response.Buffer = False
        Response.ClearContent()
        Response.ClearHeaders()
        reporte.ExportToHttpResponse(formatType, Response, True, "Programación - Despachos")
        Response.[End]()
    End Sub
End Class