﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frm_VistaDespacho_Zona.aspx.vb" Inherits="APPWEB.frm_VistaDespacho_Zona" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div style="width:100%" id="capanormal">
                <div style="margin:5px;width:100%" class="TituloCelda">
                    <span>Despacho Guias Programadas - PICKING</span>
                </div>
                <div style="width:100%">
                    <table>
                        <tr>
                            <td class="Texto">
                                Fecha Despacho:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaFiltro" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaFiltro" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </td>
                            <td class="Texto">
                               Sector:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSector" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                               Turno:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTurno" runat="server">
                                <asp:ListItem Value="Todo" Text="Todo"></asp:ListItem>
                                <asp:ListItem Value="Mañana" Text="Mañana"></asp:ListItem>
                                <asp:ListItem Value="Tarde" Text="Tarde"></asp:ListItem>
                                <asp:ListItem Value="Noche" Text="Noche"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Transportista:
                            </td>
                            <td>
                                <asp:TextBox ID="txtTransportista" runat="server" width="180px" ReadOnly="true"></asp:TextBox>
                                <asp:TextBox ID="txtCodigoTransportista" runat="server" width="80px" onfocus="return(filtroTransporte());"></asp:TextBox>
                                <asp:CheckBox ID="checkFiltroTransporte" runat="server" ToolTip="Active para buscar por Transportista" />
                            </td>
                            <td>
                                <asp:Button ID="btnBuscarPicking" Text="Buscar" CssClass="btnBuscar" runat="server" OnClientClick="this.value='Procesando...';this.disabled=true" UseSubmitBehavior="false" />
                            </td>
                            <td>
                                <asp:Button ID="btnimprimir" runat="server" Text="Imprimir"  />
                            </td>
                        </tr>
                    </table>
                </div>
                
                <div style="width:100%;float:left;overflow:scroll;height:500px">
                    <%--<asp:Timer ID="timer_refresh" runat="server" OnTick="UpdateTimer_Tick" Interval="10000"></asp:Timer>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="timer_refresh" EventName="Tick" />
                    </Triggers>
                    <ContentTemplate>--%>
                       <asp:GridView ID="gv_Zonas_Despachos" runat="server" Width="100%" DataKeyNames="id_despacho_controladores,idSector,idProducto,unidadMedidaPrincipal,unidadMedidaSalida,IdDocumento,idDetalleDocumento"
                        AutoGenerateColumns="False" ShowHeaderWhenEmpty="True"
                        EmptyDataText="Este Usuario no tiene productos asignados por despachar.">
                        <Columns>                            
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <span><%# Container.DataItemIndex + 1 %></span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nro. Doc">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("nroDocumento") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vehículo">
                                <HeaderTemplate>
                                    Placa:
                                    <asp:DropDownList ID="ddlPlacaVehiculo"  runat="server" AutoPostBack="true" 
                                    AppendDataBoundItems="true" OnSelectedIndexChanged="evento_cambiarVehiculo" >
                                    <asp:ListItem Text = "TODO" Value = "TODO"></asp:ListItem>
                                    </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("veh_Placa") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="prod_codigo" HeaderText="Código" />
                            <asp:BoundField DataField="prod_Nombre" HeaderText="Producto" />
                            <asp:BoundField DataField="lin_Nombre" HeaderText="Linea" />
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Vuelta:
                                    <asp:DropDownList ID="ddlVueltas"  runat="server" AutoPostBack="true" 
                                    AppendDataBoundItems="true" OnSelectedIndexChanged="evento_filtroVueltas" >
                                    <asp:ListItem Text = "TODO" Value = "TODO"></asp:ListItem>
                                    <asp:ListItem Text = "1" Value = "1"></asp:ListItem>
                                    <asp:ListItem Text = "2" Value = "2"></asp:ListItem>
                                    <asp:ListItem Text = "3" Value = "3"></asp:ListItem>
                                    <asp:ListItem Text = "4" Value = "4"></asp:ListItem>
                                    <asp:ListItem Text = "5" Value = "5"></asp:ListItem>
                                    </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                <asp:Label ID="lblVuelta" runat="server" Text='<%# Bind("nroVuelta") %>' ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="dc_Cantidad" HeaderText="Pendiente" />
                            <asp:BoundField DataField="nombreTono" HeaderText="Tono" />
                            <asp:BoundField DataField="cantidadTono" HeaderText="Cantidad" ControlStyle-ForeColor="Red"/>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:PlaceHolder ID="phTonosDespachar" runat="server"></asp:PlaceHolder>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle Font-Bold="True" Font-Size="Smaller" ForeColor="Red" 
                            HorizontalAlign="Center" VerticalAlign="Middle" />
                     <HeaderStyle CssClass="GrillaHeader" />
                     <RowStyle CssClass="GrillaRow" />
                    </asp:GridView>
<%--                    </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
            </div>            
            <div id="capaTransporte" style="border: 3px solid blue; padding: 8px; width: 560px;
            height: auto; position: absolute; top: 100px; left: 87px; background-color: white;
            z-index: 2; display: none;">
            <div style="width:100%" align="right">
                <img alt="" src="../../Imagenes/Cerrar.gif" onclick="return(offCapa('capaTransporte'));" />
            </div>
            <asp:GridView ID="GV_Transportista" runat="server" AutoGenerateColumns="False" EmptyDataText="No se encontrarón registros" EmptyDataRowStyle-ForeColor="Red" ShowHeader="true" 
                                    Width="100%" AllowPaging="True" DataKeyNames="IdPersona" PageSize="5">
                                    <RowStyle CssClass="GrillaRow" />
                                    <HeaderStyle CssClass="GrillaHeader"  VerticalAlign="Middle" />
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False"
                                                    CommandName="Select" Text="Seleccionar"></asp:LinkButton>
                                                <asp:HiddenField ID="hddindice" runat="server" Value='<%# Container.DataItemIndex %>' />
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>                                                                                
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:TextBox ID="TxtFiltroTransportista" runat="server" Text=""></asp:TextBox>
                                                <asp:Button ID="btnBuscaTranspo" runat="server" OnClick="ontextChanged" Text="Buscar" CssClass="btnBuscar" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTransportista" runat="server" Font-Bold="true" 
                                                    ForeColor="Red" Text='<%#DataBinder.Eval(Container.DataItem,"Transportista")%>'></asp:Label>
                                                <asp:HiddenField ID="HiddenField2" runat="server" 
                                                    Value='<%#DataBinder.Eval(Container.DataItem,"idPersona")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Doc_identidad" HeaderText="R.U.C/D.N.I" />
                                        <asp:BoundField DataField="tel_numero" HeaderText="Telefono" />
                                        <asp:BoundField DataField="distrito" HeaderText="Distrito"  />
                                    </Columns>
                                </asp:GridView>
            </div>
            <script language="javascript" type="text/javascript">
                function filtroTransporte() {
                    onCapa('capaTransporte');
                    return true;
                }
            </script>
</asp:Content>
