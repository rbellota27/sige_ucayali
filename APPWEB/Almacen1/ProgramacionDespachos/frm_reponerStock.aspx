﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="frm_reponerStock.aspx.vb" Inherits="APPWEB.frm_reponerStock" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div style="width:100%"><!--Div Contenedor-->
<div style="width:100%" class="TituloCelda">
    <span>ACTUALIZAR STOCK</span>
</div>
<div style="width:100%">
<div style="float:left">
    <table>
        <tr>
            <td class="Texto">
                Empresa:
            </td>
            <td>
                <asp:DropDownList ID="ddlEmpresa" runat="server"></asp:DropDownList>
            </td>
            <td class="Texto">
                Tipo Almacen:
            </td>
            <td>
                <asp:DropDownList ID="ddlTipoAlmacen" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td class="Texto">
                Almacén:
            </td>
            <td>
                <asp:DropDownList ID="ddlAlmacen" runat="server"></asp:DropDownList>
            </td>
            <td class="Texto">
                Sector:
            </td>
            <td>
                <asp:DropDownList ID="ddlSector" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <table>
                    <tr>
                        <td class="Texto">
                            Tipo Existencia:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTipoExistencia" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr runat="server" visible="false">
                        <td class="Texto">
                            Linea:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLinea" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </td>
                        <td class="Texto">
                            Sublinea
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSublinea" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Descripción:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDescripcion" runat="server" Text=""></asp:TextBox>
                        </td>
                        <td class="Texto">
                            Cód:
                        </td>
                        <td>
                            <asp:TextBox ID="txtCodigo" runat="server" Text=""></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btnBuscar" />                
                        </td>
                    </tr>
                </table>                
            </td>            
        </tr>
    </table>
    </div>
    <div>
    <table>
        <tr>
            <td>
                <asp:GridView ID="GV_Equivalencia" runat="server" AutoGenerateColumns="false" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="DescripcionCorto" HeaderText="U. Medida" />
                        <asp:BoundField DataField="pumEquivalencia" HeaderText="Equivalencia" DataFormatString="{0:F4}" />
                        <asp:CheckBoxField DataField="Estado" HeaderText="Principal" />
                    </Columns>
                    <HeaderStyle CssClass="GrillaHeader" />
                    <FooterStyle CssClass="GrillaFooter" />
                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                    <EditRowStyle CssClass="GrillaEditRow" />
                    <PagerStyle CssClass="GrillaPager" />
                    <RowStyle CssClass="GrillaRow" />
                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                 </asp:GridView>
            </td>
        </tr>
    </table>
    </div>
</div>
<div style="width:100%;overflow:scroll;height:500px"><!--Aquí empieza div de grilla-->
<asp:GridView ID="gvProductos" runat="server" Width="100%" DataKeyNames="idproducto,idalmacen,idsector,idTono"
        AutoGenerateColumns="False" HeaderStyle-HorizontalAlign="Center">
<RowStyle CssClass="GrillaRow"/>
<HeaderStyle CssClass="GrillaHeader" />
    <Columns>
        <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblContador" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
        <asp:TemplateField HeaderText="Existencia">
            <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# Bind("existencia") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Linea">
            <ItemTemplate>
                <asp:Label ID="Label3" runat="server" Text='<%# Bind("linea") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Sub Linea">
            <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text='<%# Bind("subLinea") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Cod SIGE">
            <ItemTemplate>
                <asp:Label ID="Label5" runat="server" Text='<%# Bind("codSige") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Producto">
            <ItemTemplate>
                <asp:Label ID="Label6" runat="server" Text='<%# Bind("producto") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Almacen">
            <ItemTemplate>
                <asp:Label ID="Label7" runat="server" Text='<%# Bind("almacen") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Sector">
            <EditItemTemplate>
                <asp:DropDownList ID="ddlSectorGrilla" runat="server" ></asp:DropDownList>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label8" runat="server" Text='<%# Bind("sector") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Un. Medida">
            <ItemTemplate>
                <asp:Label ID="Label9" runat="server" Text='<%# Eval("UnidadMedida") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Eval("stockKardex") %>' ForeColor="Red"></asp:Label>
            </ItemTemplate>
            <HeaderTemplate>
            <asp:Label ID="Label10" runat="server" Text="Stock Kardex"></asp:Label>
                <br></br>
                <asp:Label ID="lblStockKardex" runat="server" Text='<%# Eval("stockKardex") %>' ForeColor="Black"></asp:Label>
            </HeaderTemplate>
            <ItemStyle HorizontalAlign="Right" />
            <HeaderStyle HorizontalAlign="Right" />
        </asp:TemplateField>
        <asp:TemplateField>
            <EditItemTemplate>
                <asp:TextBox ID="txtCantidadStock" runat="server" ForeColor="Red" Width="60" Text='<%# Eval("stockTono") %>' onkeyup="sumCantidades(this);"></asp:TextBox>
            </EditItemTemplate>
            <ItemTemplate>                
                <asp:Label ID="lblStockTono" runat="server" Text='<%# Eval("stockTono") %>' ForeColor="Red"></asp:Label>
            </ItemTemplate>
            <HeaderTemplate>
                <asp:Label ID="lblNombre" runat="server" Text='Stock Tono'></asp:Label>
                <br></br>
                <asp:Label ID="lblTituloStockTono" runat="server" Text='Stock Tono' ForeColor="Black"></asp:Label>
            </HeaderTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <HeaderStyle HorizontalAlign="Right" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Tonos Dispo.">
            <ItemTemplate>
                <asp:Label ID="Label20" runat="server" Text='<%# Eval("tonoDisponible") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>        

        <asp:CommandField EditText="Editar" ShowEditButton="True" ShowDeleteButton="True" />
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Button ID="btnCrearTono" runat="server" Text="Crear Tono" CommandName="CrearTono"
                 CommandArgument='<%#Container.DataItemIndex %>' />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:HiddenField ID="idGvProducto" Value="-1" runat="server" />
</div>
</div><!--Termina div contenedor-->
<div id="capaCrearTono" style="border: 3px solid blue; padding: 8px;
            width: 500px; height: auto; position: fixed; top: 150px; left: 180px; background-color: white;
            z-index: 5; display: none;">
            <div style="width:100%" align="right">
                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Imagenes/Cerrar.gif" />
            </div>
            <div style="width:100%" align="center" class="TituloCelda">
                <span>Crear nuevo tono</span>
            </div>
            <div style="width:100%">
                <table>
                    <tr>
                        <td class="Texto">
                            Nombre del tono:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNombreTono" runat="server" Text=""></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="Texto">
                            Descripción:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDescripcionTono" runat="server" Text=""></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button runat="server" id="btnNuevoTono" Text="Guardar" />
                        </td>
                    </tr>
                </table>
            </div>
         </div>
<script language="javascript" type="text/javascript">
    function capaCrearTono() {
        onCapa("capaCrearTono");
        return true;
    }
    function sumCantidades(textbox) {
        var suma = 0;        
        var grilla = document.getElementById('<%=gvProductos.ClientID %>');
        for (var i = 1; i < grilla.rows.length; i++) {
            if (grilla.rows[i].cells[10].children[0].id == textbox.id) {
                suma = parseFloat(suma) + parseFloat(grilla.rows[i].cells[10].children[0].value);
            } else {
                suma = parseFloat(suma) + parseFloat(grilla.rows[i].cells[10].children[0].innerHTML);
            }
        }
        grilla.rows[0].cells[10].innerHTML = parseFloat(grilla.rows[1].cells[9].children[0].innerHTML) - parseFloat(suma);
    }

    function sumaXDefault() {
        var suma = 0;
        var grilla = document.getElementById('<%=gvProductos.ClientID %>');
        for (var i = 1; i < grilla.rows.length; i++) {
                suma = parseFloat(suma) + parseFloat(grilla.rows[i].cells[10].children[0].innerHTML);
        }
        grilla.rows[0].cells[10].innerHTML = parseFloat(grilla.rows[1].cells[9].children[0].innerHTML) - parseFloat(suma);
    }
</script>
</asp:Content>
