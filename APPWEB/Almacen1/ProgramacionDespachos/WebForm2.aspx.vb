﻿Imports Entidades
Imports Negocio
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Linq
Imports MsgBox
Imports System.Globalization
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Reflection
Imports System.ComponentModel

Public Class WebForm2
    Inherits System.Web.UI.Page
    Public Shared idDocumentos_Deseleccionados As String = ""
    Public Shared idVehiculoSeleccionado As Integer = 0
    Public idTransporteSeleccionado As Integer = 0
    Dim clsNegocio As New LN_Despachos
    Dim clsEntidades As New Entidades.Vehiculo
    Private objScript As New ScriptManagerClass

    Private Sub ValidarPermisos()
        '**** REGISTRAR / PROGRAMACIONES DE DESPACHOS
        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {2100899001})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnBuscarProgramacion.Enabled = True            
        Else            
            Me.btnBuscarProgramacion.Enabled = False
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptmanager As ScriptManager = scriptmanager.GetCurrent(Me.Page)
        scriptmanager.RegisterPostBackControl(Me.btnExcel)
        scriptmanager.RegisterPostBackControl(Me.btnPdf)
        scriptmanager.RegisterPostBackControl(Me.btnWord)
        scriptmanager.RegisterPostBackControl(Me.btnDescpachoClienteFinal)
        scriptmanager.RegisterPostBackControl(Me.btnDespachoSanicenterSubcontratado)
        If Not IsPostBack Then
            ValidarPermisos()
            JavaScript()
            Me.txtCalendarioInicio.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
            Me.txtCalendarioFin.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
            Me.txt3.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
            Me.txt4.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
            ViewState("Filtro") = "TODO"
            ViewState("Filtro_Turno") = "TODO"
            Dim condicion As String = "and a.idTienda in (" & Session("idTiendaUsuario") & ") and cast(a.fecInicioDespacho as date) >= CAST('" & Me.txtCalendarioInicio.Text & "' AS DATE) and cast(a.fecInicioDespacho as date) <= CAST('" & Me.txtCalendarioFin.Text & "' AS DATE) "
            Dim listaObjetos As New List(Of Object)
            listaObjetos = (New Negocio.LN_Despachos).eventoLoad(ViewState("Filtro"), "LISTAR_VEHICULOS_PROGRAMADOS_X_ATENDER", condicion, 0, 0, 0, Date.Now, False)
            'Muestra de la lista de programaciones
            Call verListaProgramacion(listaObjetos(3))
            Session("lista_cronograma") = listaObjetos(3)

            'Carga filtro de placas
            Call listaPlacas(listaObjetos(2))
            Session("lista_placa") = listaObjetos(2)

            'Muestra Ayudante
            Call cargarListaPersonas(listaObjetos(0), ddlAyudante, "Seleccione Ayudante")

            'Muestra Chofer
            Call cargarListaPersonas(listaObjetos(1), ddlConductor, "Seleccione Conductor")

            Me.txtFechaFiltro.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
            Me.txtFecha.Text = FormatDateTime(Date.Now, DateFormat.ShortDate)
            Me.txthora.Text = FormatDateTime(Date.Now, DateFormat.ShortTime)
        End If
    End Sub

    Private Sub verListaProgramacion(ByVal listaProgramacion As List(Of Entidades.be_cronograma))
        Me.gvVehiculos.DataSource = listaProgramacion
        Me.gvVehiculos.DataBind()
    End Sub

    Private Sub cargarListaPersonas(ByVal lista As List(Of Entidades.Persona), ByVal ddl As DropDownList, ByVal mensaje As String)
        ddl.DataSource = lista
        ddl.DataValueField = "Id"
        ddl.DataTextField = "NComercial"
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem(mensaje, "-1"))
    End Sub

    Private Sub listaPlacas(ByVal listaPlaca As List(Of Entidades.Vehiculo))
        Dim ddlPlacaVehiculo As DropDownList = TryCast(Me.gvVehiculos.HeaderRow.FindControl("ddlPlacaVehiculo"), DropDownList)
        ddlPlacaVehiculo.DataSource = listaPlaca
        ddlPlacaVehiculo.DataTextField = "placa"
        ddlPlacaVehiculo.DataValueField = "placa"
        ddlPlacaVehiculo.DataBind()
        ddlPlacaVehiculo.SelectedValue = ViewState("Filtro")
    End Sub

    Protected Sub evento_filtrarplaca(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddlPlacaVehiculo As DropDownList = DirectCast(sender, DropDownList)
        ViewState("Filtro") = ddlPlacaVehiculo.SelectedValue
        Dim lista As New List(Of Entidades.be_cronograma)
        lista = Session("lista_cronograma")
        If ViewState("Filtro") = "TODO" Then
            Me.gvVehiculos.DataSource = lista
            Me.gvVehiculos.DataBind()
            listaPlacas(Session("lista_placa"))
        Else
            lista = (From item In lista
               Where (item.vehplaca.Contains(ViewState("Filtro")))
               Select item).ToList()
            Me.gvVehiculos.DataSource = lista
            Me.gvVehiculos.DataBind()
            listaPlacas(Session("lista_placa"))
        End If
    End Sub

    Protected Sub evento_cambiarTurno(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddlCambiarTurno As DropDownList = DirectCast(sender, DropDownList)
        ViewState("Filtro_Turno") = ddlCambiarTurno.SelectedValue
        Dim busqueda As String = ddlCambiarTurno.SelectedValue.ToString().ToLower()
        Dim lista As New List(Of Entidades.be_cronograma)

        lista = Session("lista_cronograma")
        If ViewState("Filtro_Turno") = "TODO" Then
            Me.gvVehiculos.DataSource = lista
            Me.gvVehiculos.DataBind()
            Dim ddlTurno As DropDownList = TryCast(Me.gvVehiculos.HeaderRow.FindControl("ddlTurnoFiltro"), DropDownList)
            ddlTurno.SelectedValue = ViewState("Filtro_Turno")
        Else
            lista = (From item In lista
                   Where (item.turno.ToLower().Contains(busqueda))
                   Select item).ToList()
            Me.gvVehiculos.DataSource = lista
            Me.gvVehiculos.DataBind()

            Dim ddlTurno As DropDownList = TryCast(Me.gvVehiculos.HeaderRow.FindControl("ddlTurnoFiltro"), DropDownList)
            ddlTurno.SelectedValue = ViewState("Filtro_Turno")
        End If
        listaPlacas(Session("lista_placa"))
    End Sub

    Public Shared Function ListaATabla(Of T)(ByVal lista As IList(Of T)) As dtGuias.GuiasDataTable
        Dim tabla As New dtGuias.GuiasDataTable
        'Crear la Estructura de la Tabla a partir de la Lista de Objetos
        Dim propiedades As PropertyInfo() = lista(0).[GetType]().GetProperties()
        For i As Integer = 0 To propiedades.Length - 1
            tabla.Columns.Add(propiedades(i).Name, propiedades(i).PropertyType)
        Next
        'Llenar la Tabla desde la Lista de Objetos
        Dim fila As DataRow = Nothing
        For i As Integer = 0 To lista.Count - 1
            propiedades = lista(i).[GetType]().GetProperties()
            fila = tabla.NewRow()
            For j As Integer = 0 To propiedades.Length - 1
                fila(j) = propiedades(j).GetValue(lista(i), Nothing)
            Next
            tabla.Rows.Add(fila)
        Next
        Return (tabla)
    End Function

    'Public Function ConvertirListaToDataTable(ByVal data As IList) As DataTable

    '    Dim properties As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))

    '    Dim table As New DataTable()

    '    For Each prop As PropertyDescriptor In properties
    '        table.Columns.Add(prop.Name, If(Nullable.GetUnderlyingType(prop.PropertyType), prop.PropertyType))
    '    Next

    '    For Each item As T In data
    '        Dim row As DataRow = table.NewRow()
    '        For Each prop As PropertyDescriptor In properties
    '            row(prop.Name) = If(prop.GetValue(item), DBNull.Value)
    '        Next
    '        table.Rows.Add(row)
    '    Next
    '    Return table
    'End Function

    'Public Shared Function ConvertToDataTable(Of T)(ByVal list As IList(Of T)) As DataTable
    '    Dim dt As New DataTable()
    '    Dim propiedades As PropertyInfo() = GetType(T).GetProperties
    '    For Each p As PropertyInfo In propiedades
    '        dt.Columns.Add(p.Name, p.PropertyType)
    '    Next
    '    For Each item As T In list
    '        Dim row As DataRow = dt.NewRow
    '        For Each p As PropertyInfo In propiedades
    '            row(p.Name) = p.GetValue(item, Nothing)
    '        Next
    '        dt.Rows.Add(row)
    '    Next
    '    Return dt
    'End Function

    Private Sub JavaScript()
        'ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');", True)
        btnAnular.Attributes.Add("onClick", "return (confirm('Se Anulará esta programación, las guias asociadas estaran disponibles para futuras programaciones'));")
    End Sub

    'Protected Sub evento_cambiarVehiculo2(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim ddlPlacaVehiculo2 As DropDownList = DirectCast(sender, DropDownList)
    '    ViewState("Filtro") = ddlPlacaVehiculo2.SelectedValue
    '    'Me.cargarVehiculosProgramados2()
    'End Sub

    Protected Sub evento_cambiarVehiculo(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddlPlacaVehiculo As DropDownList = DirectCast(sender, DropDownList)
        ViewState("Filtro") = ddlPlacaVehiculo.SelectedValue
        'Me.cargarVehiculosProgramados()
    End Sub

    Protected Sub OnPaging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        Me.gvVehiculos.PageIndex = e.NewPageIndex
        'Me.cargarVehiculosProgramados()
    End Sub

    Private Function obtenerAnio()
        Dim anio As String = Date.Now.Year
        Return anio
    End Function

    Private Function obtenerMes()
        Dim mes As String = Date.Now.Month
        Return mes
    End Function

    Private Function obtenerDia()
        Dim dia As String = Date.Now.Day
        Return dia
    End Function

#Region "METODOS"
    'Private Sub listaPlacas(ByVal ddlPlacaVehiculo As DropDownList)
    '    Dim strConnString As String = ConfigurationManager.ConnectionStrings("conexion").ConnectionString()
    '    Dim con As New SqlConnection(strConnString)
    '    Dim sda As New SqlDataAdapter()
    '    Dim cmd As New SqlCommand(" SELECT distinct a.veh_Placa FROM Vehiculo a inner join Producto b on b.IdProducto = a.IdProducto")
    '    cmd.Connection = con
    '    con.Open()
    '    ddlPlacaVehiculo.DataSource = cmd.ExecuteReader()
    '    ddlPlacaVehiculo.DataTextField = "veh_Placa"
    '    ddlPlacaVehiculo.DataValueField = "veh_Placa"
    '    ddlPlacaVehiculo.DataBind()
    '    con.Close()
    '    ddlPlacaVehiculo.Items.FindByValue(ViewState("Filtro").ToString()).Selected = True
    'End Sub

    'Private Sub CargarPersonasVehiculo()
    '    Dim obj As New LNValorizadoCajas
    '    Dim dt As New DataTable
    '    Dim dt_ayudantes As New DataTable
    '    dt_ayudantes = obj.LN_ReturnDataTable("", "AYUDANTES")
    '    dt = clsNegocio.LN_ListarPersonasVehiculo()
    '    ddlAyudante.DataSource = dt_ayudantes
    '    ddlAyudante.DataTextField = "Nombre"
    '    ddlAyudante.DataValueField = "IdPersona"
    '    ddlAyudante.DataBind()

    '    ddlConductor.DataSource = dt
    '    ddlConductor.DataTextField = "nat_Nombres"
    '    ddlConductor.DataValueField = "IdPersona"
    '    ddlConductor.DataBind()

    '    ddlAyudante.Items.Insert(0, New ListItem("Seleccione Ayudante", "-1"))
    '    ddlConductor.Items.Insert(0, New ListItem("Seleccione Conductor", "-1"))
    'End Sub

    'Private Sub Tiendas()
    '    Dim condicion As String = ""
    '    If ddlTiendas.SelectedValue <> -1 Then
    '        condicion = ddlTiendas.SelectedValue.ToString
    '    End If
    '    Dim dt As New DataTable
    '    dt = clsNegocio.LN_ListarDatosGenerales(ViewState("Filtro"), dt, "LISTAR_TIENDAS", condicion, 0, 0, 0, Date.Now, False)
    '    Dim dr As DataRow = dt.NewRow
    '    dr("IdTienda") = 0
    '    dr("tie_Nombre") = "---"
    '    dt.Rows.Add(dr)
    '    ddlTiendas.DataSource = dt
    '    ddlTiendas.DataTextField = "tie_Nombre"
    '    ddlTiendas.DataValueField = "IdTienda"
    '    ddlTiendas.DataBind()
    'End Sub

    'Private Sub cargarVehiculosProgramados2()
    '    Dim condicion As String = "and cast(a.fecInicioDespacho as date) >= CAST('" & Me.txtCalendarioInicio.Text & "' AS DATE) and cast(a.fecInicioDespacho as date) <= CAST('" & Me.txtCalendarioFin.Text & "' AS DATE) "
    '    Dim dtxAtendidos As New DataTable
    '    dtxAtendidos = clsNegocio.LN_ListarDatosGenerales(ViewState("Filtro"), dtxAtendidos, "LISTAR_VEHICULOS_PROGRAMADOS_ATENDIDOS", condicion, 0, 0, 0, Date.Now, False)
    '    Session("XAtendidos") = dtxAtendidos
    '    'Dim dr As DataRow() = dtxAtender.Select("veh_Placa = '" & ViewState("Filtro") & "'")
    '    GridView1.DataSource = dtxAtendidos
    '    GridView1.DataBind()

    '    For Each row As GridViewRow In Me.GridView1.Rows
    '        Dim id_estadoDespacho As Integer = DirectCast(row.Cells(1).FindControl("HiddenField1"), HiddenField).Value
    '        Dim img As Image = DirectCast(row.Cells(0).FindControl("imgEstadoDespacho"), Image)
    '        If id_estadoDespacho = 1 Then
    '            img.ImageUrl = "~/Imagenes/Estado_Plomo.png"
    '        ElseIf id_estadoDespacho = 0 Then
    '            img.ImageUrl = "~/Imagenes/Estado_Verde.png"
    '        End If
    '    Next
    '    'Next
    '    'If gvVehiculos.Rows.Count <> 0 Then
    '    Dim ddlPlacaVehiculo2 As DropDownList = DirectCast(GridView1.HeaderRow.FindControl("ddlPlacaVehiculo2"), DropDownList)
    '    Me.listaPlacas(ddlPlacaVehiculo2)
    '    'End If
    'End Sub

    'Private Sub cargarVehiculosProgramados()
    '    Dim condicion As String = "and cast(a.fecInicioDespacho as date) >= CAST('" & Me.txtCalendarioInicio.Text & "' AS DATE) and cast(a.fecInicioDespacho as date) <= CAST('" & Me.txtCalendarioFin.Text & "' AS DATE) "
    '    Dim dtxAtender As New DataTable
    '    Dim listaAuxiliar As New List(Of DataRow)
    '    dtxAtender = clsNegocio.LN_ListarDatosGenerales(ViewState("Filtro"), dtxAtender, "LISTAR_VEHICULOS_PROGRAMADOS_X_ATENDER", condicion, 0, 0, 0, Date.Now, False)
    '    Session("XAtender") = dtxAtender
    '    'Dim dr As DataRow() = dtxAtender.Select("veh_Placa = '" & ViewState("Filtro") & "'")
    '    If ViewState("Filtro_Turno") <> "TODO" Then
    '        For Each row As DataRow In dtxAtender.Rows
    '            If row.Item("turno").ToString().ToUpper() <> ViewState("Filtro_Turno").ToString() Then
    '                listaAuxiliar.Add(row)
    '            End If
    '        Next
    '        For Each dr As DataRow In listaAuxiliar
    '            dtxAtender.Rows.Remove(dr)
    '        Next
    '    End If
    '    gvVehiculos.DataSource = dtxAtender
    '    gvVehiculos.DataBind()
    '    For Each row As GridViewRow In Me.gvVehiculos.Rows
    '        Dim id_estadoDespacho As Integer = DirectCast(row.Cells(1).FindControl("HiddenField1"), HiddenField).Value
    '        Dim img As Image = DirectCast(row.Cells(0).FindControl("imgEstadoDespacho"), Image)
    '        If id_estadoDespacho = 1 Then
    '            img.ImageUrl = "~/Imagenes/Estado_Plomo.png"
    '        ElseIf id_estadoDespacho = 0 Then
    '            img.ImageUrl = "~/Imagenes/Estado_Verde.png"
    '        End If
    '    Next
    '    'Next
    '    'If gvVehiculos.Rows.Count <> 0 Then
    '    Dim ddlPlacaVehiculo As DropDownList = DirectCast(gvVehiculos.HeaderRow.FindControl("ddlPlacaVehiculo"), DropDownList)
    '    Dim ddlFiltroTurno As DropDownList = DirectCast(gvVehiculos.HeaderRow.FindControl("ddlTurnoFiltro"), DropDownList)
    '    ddlFiltroTurno.Items.FindByValue(ViewState("Filtro_Turno").ToString()).Selected = True
    '    Me.listaPlacas(ddlPlacaVehiculo)
    '    'End If
    'End Sub

    

    Private Sub buscarVehiculo()
        Try
            Dim obj As Entidades.Vehiculo
            Dim lista As List(Of Entidades.Vehiculo) = (New Negocio.Vehiculo).SelectActivoxNroPlaca(Me.txtPlaca.Text)
            'If lista.Exists(Function(x As Entidades.Vehiculo) x.Placa) Then
            '    Dim encontrado As String = obj.IdProducto
            'End If
            If (lista.Count <= 0) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaVehiculo');   alert('No se hallaron registros.');     ", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');    onCapa('capaVehiculo'); ", True)
                'ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');     ", True)
            End If
            Me.GV_Vehiculo.DataSource = lista
            Me.GV_Vehiculo.DataBind()            
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    

    Private Sub CrearProgramaciones()
        Me.gv_Controladores.DataSource = Nothing
        Me.gv_Controladores.DataBind()
        Session("seleccion") = New dtGuias.GuiasDataTable
        Session("guias") = New dtGuias.GuiasDataTable
        Me.btnAnular.Enabled = False
        Me.hhdlblEstado.Value = 0
        Me.hhdidCronogramaDespacho.Value = 0
        Me.lblEstado.Text = "NUEVO - [CRONOGRAMA DE DESPACHO]"
        Dim entidades As New Entidades.Vehiculo
        Me.txtPlaca.Text = entidades.Placa
        Me.txtmodelo.Text = entidades.Modelo
        Me.txtCapacidadTotal.Text = entidades.veh_Capacidad.Round(2)
        Me.txtCapacidadUsada.Text = 0
        Me.ddlConductor.SelectedIndex = "-1"
        Me.ddlAyudante.SelectedIndex = "-1"
        Me.txtCapacidadTotal.Text = entidades.veh_Capacidad
        Me.gvGuias.DataSource = New dtGuias.GuiasDataTable
        Me.gvGuias.DataBind()
        Me.gvGuiasSeleccionadas.DataSource = New dtGuias.GuiasDataTable
        Me.gvGuiasSeleccionadas.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');", True)
    End Sub
#End Region

#Region "Eventos Click"

    Private Sub btnBuscarFiltros_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarFiltros.Click
        BuscarProgramaciones()
    End Sub

    Private Sub BuscarProgramaciones()
        Dim condicion As String = "and a.idTienda in (" & Session("idTiendaUsuario") & ") and cast(a.fecInicioDespacho as date) >= CAST('" & Me.txtCalendarioInicio.Text & "' AS DATE) and cast(a.fecInicioDespacho as date) <= CAST('" & Me.txtCalendarioFin.Text & "' AS DATE)"
        'Programados
        Dim lista_programados As New List(Of Entidades.be_cronograma)
        lista_programados = (New Negocio.LN_Despachos).eventoBuscarProgramacion(ViewState("Filtro"), "LISTAR_VEHICULOS_PROGRAMADOS_X_ATENDER", condicion, 0, 0, 0, Date.Now, False)
        Session("lista_cronograma") = lista_programados
        Me.gvVehiculos.DataSource = lista_programados
        Me.gvVehiculos.DataBind()
        'Despachados
        Dim lista_despachados As New List(Of Entidades.be_cronograma)
        lista_despachados = (New Negocio.LN_Despachos).eventoBuscarProgramacion(ViewState("Filtro"), "LISTAR_VEHICULOS_PROGRAMADOS_ATENDIDOS", condicion, 0, 0, 0, Date.Now, False)
        Me.GridView1.DataSource = lista_despachados
        Me.GridView1.DataBind()

        Call listaPlacas(Session("lista_placa"))
    End Sub

    Private Sub btnBuscarProgramacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarProgramacion.Click
        CrearProgramaciones()
    End Sub

    Private Sub btnBuscarVehiculo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarVehiculo.Click
        Call buscarVehiculo()
    End Sub

    Private Sub btnBuscarDocumento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarDocumento.Click
        cargarDatosGuias()
    End Sub

    Protected Sub SeleccionarGuias(ByVal sender As Object, ByVal e As EventArgs)
        Dim idDocumentoValidator As Integer = 0
        Dim dtGuias As dtGuias.GuiasDataTable = TryCast(Session("guias"), dtGuias.GuiasDataTable)
        Dim dtSeleccionadas As dtGuias.GuiasDataTable = TryCast(Session("seleccion"), dtGuias.GuiasDataTable)
        Dim peso As Double = 0
        Dim diferencia As Double = 0
        Dim objetoValida As New Negocio.bl_pagoProveedor

        For Each row As GridViewRow In Me.gvGuias.Rows
            Dim check As CheckBox = TryCast(row.FindControl("ckbSeleccionar"), CheckBox)
            If check.Checked Then
                Dim columnaNoVisible = Me.gvGuias.DataKeys(row.RowIndex).Values
                idDocumentoValidator = columnaNoVisible(2)
                If objetoValida.validaSiGuiaFueProgramada(idDocumentoValidator) Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaDespachos');alert('El documento seleccionado ya fue asignado a otra programación,\n Actualice esta lista para visualizar los últimos documentos disponibles.');", True)
                    Exit Sub
                Else ' SI EL DOCUMENTO YA FUE ASIGNADO A UNA PROGRAMACIÓN
                    'se arma la fila para el dataset seleccion
                    Dim guias As dtGuias.GuiasRow = dtSeleccionadas.NewGuiasRow()

                    guias.IdDocumento = columnaNoVisible(2)
                    guias.IdSerie = columnaNoVisible(1)
                    guias.IdTienda = columnaNoVisible(0)
                    guias.doc_Codigo = row.Cells(1).Text
                    guias.numero = row.Cells(4).Text
                    guias.peso = IIf(row.Cells(5).Text = "&nbsp;", 0, row.Cells(5).Text)
                    guias.cliente = row.Cells(6).Text
                    guias.distrito = row.Cells(7).Text
                    guias.tie_Nombre = row.Cells(8).Text
                    If dtSeleccionadas.Select("IdDocumento = " & guias.IdDocumento).Length > 0 Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "alert('Uno o mas documento seleccionado ya pertenece a [GUIAS ASIGNADAS]\n');", True)
                        Exit Sub
                    End If
                    If Val(guias.peso) = 0 Then
                        'ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaDespachos');alert('Guia Nro: " & guias.numero & " tiene un peso igual a 0\n Para continuar con la programación deberá editar la guia. ');", True)
                        'Exit For
                    End If
                    If Val(guias.peso) <> 0 And peso <= Val(Me.txtCapacidadTotal.Text) Then
                        peso = guias.peso + Val(Me.txtCapacidadUsada.Text)
                        If peso > Val(Me.txtCapacidadTotal.Text) Then
                            diferencia = CDbl(Val(Me.txtCapacidadTotal.Text)) - peso
                            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaDespachos');alert('Capacidad Excedida en : " & Math.Abs(diferencia) & " ');", True)
                            Exit For
                        Else
                            diferencia = CDbl(Val(Me.txtCapacidadTotal.Text)) - peso
                            Me.txtCapacidadUsada.Text = peso
                            Me.txtCapacidadSinUsar.Text = Math.Abs(CDbl(Val(Me.txtCapacidadTotal.Text)) - peso)
                        End If
                    End If
                    dtSeleccionadas.Rows.Add(guias)

                    Dim rowDelete As DataRow() = dtGuias.Select("IdDocumento =" & guias.IdDocumento)
                    If rowDelete.Length > 0 Then
                        dtGuias.Rows.Remove(rowDelete(0))
                    End If
                    'carga las zonas
                End If
            End If
        Next
        gvGuias.DataSource = dtGuias
        gvGuias.DataBind()

        gvGuiasSeleccionadas.DataSource = dtSeleccionadas
        gvGuiasSeleccionadas.DataBind()
        Session("guias") = dtGuias
        Session("seleccion") = dtSeleccionadas
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');    ", True)
    End Sub

    Public Sub filtrarGuias()
        Dim lista As New List(Of Entidades.be_guiaRemision)
        lista = (New Negocio.LN_Despachos).eventoFiltrarGuias(ViewState("Filtro"), "FILTRO_GUIAS", 2, ddlTiendas.SelectedValue, Val(Me.txtSerie.Text), Val(Me.txtNrodocumento.Text), Me.txtFechaFiltro.Text, Me.chkActivo.Checked)
        'Inicializa los datatable
        Dim dtGuias As dtGuias.GuiasDataTable = TryCast(Session("guias"), dtGuias.GuiasDataTable)
        Dim dtSeleccionadas As dtGuias.GuiasDataTable = TryCast(Session("seleccion"), dtGuias.GuiasDataTable)

        dtGuias = ListaATabla(lista)
        For Each row As dtGuias.GuiasRow In dtSeleccionadas.Rows
            Dim rowDelete As DataRow() = dtGuias.Select("IdDocumento = " & row.IdDocumento)
            If rowDelete.Length > 0 Then
                dtGuias.Rows.Remove(rowDelete(0))
            End If
        Next

        Session("guias") = dtGuias
        Session("seleccion") = dtSeleccionadas

        gvGuias.DataSource = dtGuias
        gvGuias.DataBind()
        If Me.gvGuias.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');   alert('No se hallaron registros.');     ", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');    ", True)
        End If
    End Sub

    Private Sub cargarDatosGuias()

        Dim dtGuias As dtGuias.GuiasDataTable = TryCast(Session("guias"), dtGuias.GuiasDataTable)
        Dim dtSeleccionadas As dtGuias.GuiasDataTable = TryCast(Session("seleccion"), dtGuias.GuiasDataTable)

        dtGuias = clsNegocio.LN_ListarDatosGenerales(ViewState("Filtro"), New dtGuias.GuiasDataTable, "FILTRO_GUIAS", 2, ddlTiendas.SelectedValue, _
                                                     Val(Me.txtSerie.Text), Val(Me.txtNrodocumento.Text), Me.txtFechaFiltro.Text, Me.chkActivo.Checked)
        For Each row As dtGuias.GuiasRow In dtSeleccionadas.Rows
            Dim rowDelete As DataRow() = dtGuias.Select("IdDocumento = " & row.IdDocumento)
            If rowDelete.Length > 0 Then
                dtGuias.Rows.Remove(rowDelete(0))
            End If
        Next

        Session("guias") = dtGuias
        Session("seleccion") = dtSeleccionadas

        gvGuias.DataSource = dtGuias
        gvGuias.DataBind()
        If Me.gvGuias.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');   alert('No se hallaron registros.');     ", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');    ", True)
        End If
    End Sub

    Private Function validarVehiculos() As Boolean
        If Not IsNothing(Me.GV_Vehiculo.SelectedDataKey.Values("Placa").ToString()) Then
            Dim placa_Seleccionada As String = Me.GV_Vehiculo.SelectedDataKey.Values("Placa").ToString()
            Dim fechaInicio_Seleccionada As Date = CDate(Me.txtFecha.Text.Trim())
            Dim turno_Seleccionado As String = Me.ddlTurno.SelectedItem.Text()
            For Each row As GridViewRow In Me.gvVehiculos.Rows
                Dim fechaInicio_existente As Date = CDate(row.Cells(3).Text).ToShortDateString
                Dim turno_existente As String = CStr(HttpUtility.HtmlDecode(row.Cells(2).Text))
                Dim placa_existente As String = CStr(TryCast(row.FindControl("lblplaca"), Label).Text)
                If fechaInicio_existente = fechaInicio_Seleccionada And turno_existente = turno_Seleccionado And placa_existente = placa_Seleccionada Then
                    objScript.mostrarMsjAlerta(Me, "El vehiculo seleccionado de placa " & placa_Seleccionada & " actualmente ya tiene una programación por cumplir Fecha: " & fechaInicio_existente & " Turno : " & turno_existente)
                    Return False
                Else
                    Return True
                End If
            Next
            Return True
        End If
    End Function



    Private Sub gvGuias_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGuias.RowCommand
        If e.CommandName = "Seleccionar" Then
            'SeleccionarGuias()
            'seleccionar controladores
            MostrarControladores()
        End If
    End Sub

    Private Sub gvGuias_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGuias.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim img As ImageButton = DirectCast(e.Row.FindControl("imgVerGuiasReferencias"), ImageButton)
            'Dim lb As LinkButton = DirectCast(e.Row.FindControl("lbSeleccionarGuias"), LinkButton)
            img.Attributes.Add("onclick", "Abrir_ventana('../FrmGuiaRemision.aspx?IdRef=0&IdSerie=" & DataBinder.Eval(e.Row.DataItem, "IdSerie") & "&doc_codigo=" & DataBinder.Eval(e.Row.DataItem, "doc_codigo") & "&IdTienda=" & DataBinder.Eval(e.Row.DataItem, "IdTienda") & "');return(onCapa('capaDespachos'))")
            'lb.Attributes.Add("onclick", "return(ValidarPeso());")
        End If
    End Sub

    Private Sub gvGuiasSeleccionadas_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGuiasSeleccionadas.RowCommand
        If e.CommandName = "Quitar" Then
            Dim i As Integer = e.CommandArgument
            QuitarGuias(i)
            'calcular controladores
            MostrarControladores()
        End If
    End Sub

    Private Sub gvGuiasSeleccionadas_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvGuiasSeleccionadas.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim indice As Integer = e.Row.RowIndex
            Dim textbox As TextBox = DirectCast(e.Row.FindControl("txtTemplatePeso"), TextBox)
            If textbox.Text = "0" Then
                'textbox.Enabled = True
            ElseIf textbox.Text = "" Then
                'textbox.Enabled = True
            Else
                'textbox.Enabled = False
            End If
        End If
    End Sub

    Private Sub QuitarGuias(ByVal indice As Integer)
        Dim capDisponible As Double = 0
        Dim capDiferencias As Double = 0
        Dim dtGuias As dtGuias.GuiasDataTable = TryCast(Session("guias"), dtGuias.GuiasDataTable)
        Dim dtSeleccionadas As dtGuias.GuiasDataTable = TryCast(Session("seleccion"), dtGuias.GuiasDataTable)

        Dim guia As dtGuias.GuiasRow = dtGuias.NewGuiasRow
        Dim campoOculto = Me.gvGuiasSeleccionadas.DataKeys(indice).Values
        guia.IdDocumento = campoOculto(2)
        idDocumentos_Deseleccionados = campoOculto(2) & "," & idDocumentos_Deseleccionados
        guia.IdSerie = campoOculto(1)
        guia.IdTienda = campoOculto(0)
        guia.doc_Codigo = Me.gvGuiasSeleccionadas.Rows(indice).Cells(1).Text
        guia.numero = Me.gvGuiasSeleccionadas.Rows(indice).Cells(1).Text
        guia.peso = DirectCast(Me.gvGuiasSeleccionadas.Rows(indice).FindControl("txtTemplatePeso"), TextBox).Text
        guia.cliente = Me.gvGuiasSeleccionadas.Rows(indice).Cells(3).Text
        guia.distrito = Me.gvGuiasSeleccionadas.Rows(indice).Cells(4).Text
        guia.tie_Nombre = Me.gvGuiasSeleccionadas.Rows(indice).Cells(5).Text

        dtGuias.Rows.Add(guia)
        Dim idDocumento As String = campoOculto(2)
        Dim datarowOut As DataRow() = dtSeleccionadas.Select("IdDocumento = " & idDocumento)

        Me.txtCapacidadUsada.Text = Val(CDbl(Me.txtCapacidadUsada.Text)) - guia.peso
        Me.txtCapacidadSinUsar.Text = Val(guia.peso + Val(CDbl(Me.txtCapacidadSinUsar.Text)))
        dtSeleccionadas.Rows.Remove(datarowOut(0))

        Me.gvGuiasSeleccionadas.DataSource = dtSeleccionadas
        Me.gvGuiasSeleccionadas.DataBind()
        Me.gvGuias.DataSource = dtGuias
        Me.gvGuias.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');", True)
    End Sub

    Private Sub gvVehiculos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvVehiculos.SelectedIndexChanged

        Me.btnAnular.Enabled = True
        Me.hhdlblEstado.Value = 1
        Me.hhdidCronogramaDespacho.Value = Me.gvVehiculos.SelectedDataKey.Values("idCronograma")
        Me.lblEstado.Text = "EDITAR - [CRONOGRAMA DE DESPACHO]"
        Dim indice As Integer = Me.gvVehiculos.SelectedIndex
        Dim row As GridViewRow = Me.gvVehiculos.SelectedRow
        Dim lista As New List(Of Entidades.be_cronograma)
        lista = Session("lista_cronograma")

        Dim datakey As DataKey = Me.gvVehiculos.DataKeys(indice)
        Dim idCronograma As Integer = datakey(0)
        lista = (From item In lista
                Where (item.idCronograma = idCronograma)
                Select item).ToList()
        IIf(lista(0).idVehiculo = 0, Me.ddlConductor.Enabled = False, Me.ddlConductor.Enabled = True)
        IIf(lista(0).idVehiculo = 0, Me.ddlAyudante.Enabled = False, Me.ddlAyudante.Enabled = True)
        IIf(lista(0).idVehiculo = 0, Me.txtCapacidadTotal.Enabled = True, Me.txtCapacidadTotal.Enabled = False)
        ViewState("idTransportista") = lista(0).idAgencia
        ViewState("idVehiculo") = lista(0).idVehiculo

        Me.ddlTurno.SelectedValue = lista(0).turno
        Me.txtFecha.Text = lista(0).fecInicio
        Me.txthora.Text = Format(lista(0).fecInicio, "HH:mm")
        Me.txtNroVuelta.Text = lista(0).nroVuelta
        Me.txtPlaca.Text = lista(0).vehplaca
        Me.txtmodelo.Text = IIf(lista(0).vehModelo = "---", lista(0).nomAgencia, lista(0).vehModelo)
        Me.ddlConductor.SelectedValue = lista(0).idConductor
        Me.ddlAyudante.SelectedValue = lista(0).idAyudante
        Me.txtCapacidadTotal.Text = lista(0).capacidadMaxima
        Me.txtCapacidadUsada.Text = lista(0).capacidadAlcanzada
        Me.txtCapacidadSinUsar.Text = lista(0).capacidadMaxima - lista(0).capacidadAlcanzada

        Dim condicion As String = lista(0).idDocumentoRelacion
        'Dim dt As DataTable = New dtGuias.GuiasDataTable
        'dt = clsNegocio.LN_ListarDatosGenerales("LISTAR_GUIAS_PROGRAMADAS", condicion, 0, 0, 0)
        'dt.TableName = "Guias"
        Dim dt As New dtGuias.GuiasDataTable
        dt = clsNegocio.LN_DataSetTipados(dt, "LISTAR_GUIAS_PROGRAMADAS", condicion, 0, 0, 0, Date.Now, False)
        'Dim columnPeso As Object
        'columnPeso = dt.Compute("SUM(peso)", "")
        'Me.txtCapacidadUsada.Text = columnPeso.ToString()
        'Me.txtCapacidadSinUsar.Text = Math.Abs(Convert.ToDecimal(Me.txtCapacidadTotal.Text) - Convert.ToDecimal(columnPeso))
        Me.gvGuiasSeleccionadas.DataSource = dt
        Me.gvGuiasSeleccionadas.DataBind()
        Session("seleccion") = dt
        Session("guias") = New dtGuias.GuiasDataTable()
        Me.gvGuias.DataSource = Session("guias")
        Me.gvGuias.DataBind()
        'mostrar controladores
        MostrarControladores()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');", True)

        'ViewState("idVehiculo") = IIf(IsDBNull(datakey(10)), 0, datakey(10))
        'ViewState("idTransportista") = IIf(IsDBNull(datakey(13)), 0, datakey(13))

        ''Asignando datos a las entidades
        'Me.ddlTurno.SelectedIndex = -1
        'Me.ddlTurno.Items.FindByValue(Me.gvVehiculos.SelectedDataKey.Values("turno")).Selected = True
        'Me.txtFecha.Text = Format(Convert.ToDateTime(Me.gvVehiculos.SelectedDataKey.Values("fecInicio")), "dd/MM/yyyy")
        'Me.txthora.Text = Format(Convert.ToDateTime(Me.gvVehiculos.SelectedDataKey.Values("fecInicioDespacho")), "HH:mm")
        'clsEntidades.Placa = DirectCast(Me.gvVehiculos.SelectedRow.FindControl("lblplaca"), Label).Text
        'clsEntidades.Modelo = IIf(Me.gvVehiculos.SelectedDataKey.Values("vehModelo") = "---", Me.gvVehiculos.SelectedDataKey.Values("nomAgencia"), Me.gvVehiculos.SelectedDataKey.Values("vehModelo"))
        'clsEntidades.veh_Capacidad = IIf(IsDBNull(Me.gvVehiculos.SelectedDataKey.Values("capacidadMaxima")), 0, Me.gvVehiculos.SelectedDataKey.Values("capacidadMaxima"))
        'clsEntidades.IdVehiculo = Me.gvVehiculos.SelectedDataKey.Values("IdVehiculo")
        ''Asignando entidades a objetos
        'Me.txtPlaca.Text = IIf(clsEntidades.Placa = "---", "", clsEntidades.Placa)
        'Me.txtmodelo.Text = clsEntidades.Modelo
        'Me.txtCapacidadTotal.Text = clsEntidades.veh_Capacidad
        'Me.txtCapacidadUsada.Text = IIf(IsDBNull(Me.gvVehiculos.SelectedDataKey.Values("capacidadAlcanzada")), 0, Me.gvVehiculos.SelectedDataKey.Values("capacidadAlcanzada"))
        'ddlConductor.SelectedIndex = -1
        'ddlAyudante.SelectedIndex = -1
        ''ddlControl.SelectedIndex = -1
        'Me.ddlConductor.Items.FindByValue(Me.gvVehiculos.SelectedDataKey.Values("IdConductor").ToString()).Selected = True
        'Me.ddlAyudante.Items.FindByValue(Me.gvVehiculos.SelectedDataKey.Values("IdAyudante").ToString()).Selected = True
        ''Me.ddlControl.Items.FindByValue(Me.gvVehiculos.SelectedDataKey.Values("IdControl").ToString()).Selected = True
        'Dim condicion As String
        'condicion = Me.gvVehiculos.SelectedDataKey.Values("idDocumentoRelacion")
        ''Dim dt As DataTable = New dtGuias.GuiasDataTable
        ''dt = clsNegocio.LN_ListarDatosGenerales("LISTAR_GUIAS_PROGRAMADAS", condicion, 0, 0, 0)
        ''dt.TableName = "Guias"
        'Dim dt As New dtGuias.GuiasDataTable
        'dt = clsNegocio.LN_DataSetTipados(dt, "LISTAR_GUIAS_PROGRAMADAS", condicion, 0, 0, 0, Date.Now, False)
        'Dim columnPeso As Object
        'columnPeso = dt.Compute("SUM(peso)", "")
        'Me.txtCapacidadUsada.Text = columnPeso.ToString()
        'Me.txtCapacidadSinUsar.Text = Math.Abs(Convert.ToDecimal(Me.txtCapacidadTotal.Text) - Convert.ToDecimal(columnPeso))
        'Me.gvGuiasSeleccionadas.DataSource = dt
        'Me.gvGuiasSeleccionadas.DataBind()
        'Session("seleccion") = dt
        'Session("guias") = New dtGuias.GuiasDataTable()
        'Me.gvGuias.DataSource = Session("guias")
        'Me.gvGuias.DataBind()
        ''mostrar controladores
        'MostrarControladores()
        'ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');", True)
    End Sub

    Private Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        Me.btnAnular.Enabled = False
        Me.hhdlblEstado.Value = 1
        Me.hhdidCronogramaDespacho.Value = Me.GridView1.SelectedDataKey.Values("idCronogramaDespacho")
        Me.lblEstado.Text = "EDITAR - [CRONOGRAMA DE DESPACHO]"
        Dim indice As Integer = Me.GridView1.SelectedIndex
        Dim row As GridViewRow = Me.GridView1.SelectedRow
        Me.ddlTurno.SelectedIndex = -1
        Me.ddlTurno.Items.FindByValue(Me.GridView1.SelectedDataKey.Values("turno")).Selected = True
        'Asignando datos a las entidades
        Me.txtFecha.Text = Format(Convert.ToDateTime(Me.GridView1.SelectedDataKey.Values("fecInicioDespacho")), "dd/MM/yyyy")
        Me.txthora.Text = Format(Convert.ToDateTime(Me.GridView1.SelectedDataKey.Values("fecInicioDespacho")), "HH:mm")

        clsEntidades.Placa = DirectCast(Me.GridView1.SelectedRow.FindControl("lblplaca"), Label).Text
        clsEntidades.Modelo = IIf(Me.GridView1.SelectedDataKey.Values("veh_Modelo") = "---", Me.GridView1.SelectedDataKey.Values("agencia"), Me.GridView1.SelectedDataKey.Values("veh_Modelo"))
        clsEntidades.veh_Capacidad = IIf(IsDBNull(Me.GridView1.SelectedDataKey.Values("capmaxima")), 0, Me.GridView1.SelectedDataKey.Values("capmaxima"))
        clsEntidades.IdVehiculo = Me.GridView1.SelectedDataKey.Values("IdVehiculo")

        'Asignando entidades a objetos
        Me.txtPlaca.Text = IIf(clsEntidades.Placa = "---", "", clsEntidades.Placa)
        Me.txtmodelo.Text = clsEntidades.Modelo
        Me.txtCapacidadTotal.Text = clsEntidades.veh_Capacidad
        Me.txtCapacidadUsada.Text = IIf(IsDBNull(Me.GridView1.SelectedDataKey.Values("capacidadAlcanzada")), 0, Me.GridView1.SelectedDataKey.Values("capacidadAlcanzada"))
        ddlConductor.SelectedIndex = -1
        ddlAyudante.SelectedIndex = -1
        'ddlControl.SelectedIndex = -1
        Me.ddlConductor.Items.FindByValue(Me.GridView1.SelectedDataKey.Values("IdConductor").ToString()).Selected = True
        Me.ddlAyudante.Items.FindByValue(Me.GridView1.SelectedDataKey.Values("IdAyudante").ToString()).Selected = True
        'Me.ddlControl.Items.FindByValue(Me.GridView1.SelectedDataKey.Values("IdControl").ToString()).Selected = True
        Dim condicion As String
        condicion = Me.GridView1.SelectedDataKey.Values("idDocumentoRelacion")
        'Dim dt As DataTable = New dtGuias.GuiasDataTable
        'dt = clsNegocio.LN_ListarDatosGenerales("LISTAR_GUIAS_PROGRAMADAS", condicion, 0, 0, 0)
        'dt.TableName = "Guias"
        Dim dt As New dtGuias.GuiasDataTable
        dt = clsNegocio.LN_DataSetTipados(dt, "LISTAR_GUIAS_PROGRAMADAS", condicion, 0, 0, 0, Date.Now, False)
        Me.gvGuiasSeleccionadas.DataSource = dt
        Me.gvGuiasSeleccionadas.DataBind()
        Session("seleccion") = dt
        Session("guias") = New dtGuias.GuiasDataTable()
        Me.gvGuias.DataSource = Session("guias")
        Me.gvGuias.DataBind()
        'mostrar controladores
        MostrarControladores()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');", True)
    End Sub

    Private Sub btnGuardarDespacho_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardarDespacho.Click
        Dim contadorGuias As Integer = Me.gvGuiasSeleccionadas.Rows.Count - 1
        Dim sb As StringBuilder
        'Dim modelo As String = Me.GV_Vehiculo.SelectedRow.Cells("veh_Capacidad").Text
        Dim str As String = ""
        Dim contador As Integer = 0
        'Dim objetoValida As New bl_pagoProveedor

        For Each fila As GridViewRow In Me.gvGuiasSeleccionadas.Rows
            Dim campoOculto = Me.gvGuiasSeleccionadas.DataKeys(fila.RowIndex).Values ' lista los datakey
            str = campoOculto(2) + "," + str 'obtiene el idDocumentoRelacion
            contador = contador + 1
            'If objetoValida.validaSiGuiaFueProgramada(CInt(campoOculto(2))) Then
            '    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "onCapa('capaDespachos');alert('El documento seleccionado ya fue asignado a otra programación,\nNroDocumento " + campoOculto(2) + "');", True)
            '    Exit Sub
            'Else ' SI EL DOCUMENTO YA FUE ASIGNADO A UNA PROGRAMACIÓN
            '    str = campoOculto(2) + "," + str 'obtiene el idDocumentoRelacion
            '    contador = contador + 1
            'End If
        Next

        Dim fechaInicio = Format(Convert.ToDateTime(Me.txtFecha.Text & " " & Me.txthora.Text), "dd/MM/yyyy HH:mm")
        clsEntidades.Turno = Me.ddlTurno.SelectedValue
        clsEntidades.Placa = Me.txtPlaca.Text
        clsEntidades.Modelo = txtmodelo.Text
        clsEntidades.IdConductor = Me.ddlConductor.SelectedValue
        clsEntidades.IdAyudante = Me.ddlAyudante.SelectedValue
        clsEntidades.veh_Capacidad = Val(Me.txtCapacidadTotal.Text)

        'clsEntidades.IdControl = Me.ddlControl.SelectedValue

        Try
            If hhdlblEstado.Value = 0 Then 'Nuevo Documento
                If Me.RadioButtonList1.Items(0).Selected Then
                    ' Dim algo3 = Me.gvVehiculos.SelectedDataKey.Values("idAgencia")
                    clsEntidades.IdVehiculo = TryCast(Me.GV_Vehiculo.SelectedRow.FindControl("hddIdVehiculo"), HiddenField).Value
                    clsEntidades.idPersona = 0
                ElseIf Me.RadioButtonList1.Items(1).Selected Then
                    clsEntidades.idPersona = TryCast(Me.GV_Transportista.SelectedRow.FindControl("HiddenField2"), HiddenField).Value
                    clsEntidades.IdVehiculo = 0
                End If
                clsNegocio.LN_InsertarGuias(Me.txtCapacidadUsada.Text, fechaInicio, Me.txtCapacidadUsada.Text, contador, 0, str.Remove(str.Length - 1).ToString, _
                                            clsEntidades, "INSERT", hhdidCronogramaDespacho.Value, idDocumentos_Deseleccionados, Val(Me.txtNroVuelta.Text))
                For Each fila As GridViewRow In Me.gvGuiasSeleccionadas.Rows
                    Dim key = Me.gvGuiasSeleccionadas.DataKeys(fila.RowIndex).Values ' lista los datakey
                    clsNegocio.DAO_UPDATE_PESO_DESPACHOS(key(2).ToString(), DirectCast(fila.FindControl("txtTemplatePeso"), TextBox).Text)
                Next
            ElseIf hhdlblEstado.Value = 1 Then 'Editar Documento                
                clsEntidades.idPersona = ViewState("idTransportista")
                clsEntidades.IdVehiculo = ViewState("idVehiculo")
                'Captura idDocumento de guias asignadas
                Dim id_documentoRelacionCheck As String = ""
                Dim id_documentoRelacionGenerico As String = ""
                'For Each row As GridViewRow In Me.gvGuiasSeleccionadas.Rows
                For Each fila As GridViewRow In Me.gvGuiasSeleccionadas.Rows
                    Dim key = Me.gvGuiasSeleccionadas.DataKeys(fila.RowIndex).Values
                    id_documentoRelacionCheck = key(2) + "," + id_documentoRelacionCheck

                    If DirectCast(fila.FindControl("CheckBox1"), CheckBox).Checked Then
                        Dim txtFecha = DirectCast(fila.FindControl("txtFecDespachoFinal"), TextBox).Text
                        If txtFecha.ToString() <> "" Then
                            Dim v_id = key(2)
                            If Not txtFecha = "__/__/____ __:__" Then
                                clsNegocio.DAO_UPDATE_FEC_DESPACHOS(v_id, Format(Convert.ToDateTime(txtFecha), "dd/MM/yyyy HH:mm"))
                            End If
                        End If
                    Else
                        id_documentoRelacionGenerico = key(2) + "," + id_documentoRelacionGenerico
                    End If
                    Dim txt As New TextBox
                    txt = DirectCast(fila.FindControl("txtTemplatePeso"), TextBox)
                    'If txt.Enabled Then
                    clsNegocio.DAO_UPDATE_PESO_DESPACHOS(key(2).ToString(), txt.Text)
                    ' End If
                Next

                If id_documentoRelacionCheck = "" Then
                    If idDocumentos_Deseleccionados <> "" Then
                        idDocumentos_Deseleccionados = idDocumentos_Deseleccionados.Remove(idDocumentos_Deseleccionados.Length - 1)
                    End If
                    id_documentoRelacionGenerico = id_documentoRelacionGenerico.Remove(id_documentoRelacionGenerico.Length - 1)
                    clsNegocio.LN_InsertarGuias(CDec(Me.txtCapacidadUsada.Text), fechaInicio, CDec(Val(Me.txtCapacidadUsada.Text)), contador, 0, id_documentoRelacionGenerico, clsEntidades, "UPDATE", hhdidCronogramaDespacho.Value.ToString(), idDocumentos_Deseleccionados, Val(txtNroVuelta.Text))
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     alert('Edición realizada correctamente');", True)
                Else
                    If idDocumentos_Deseleccionados <> "" Then
                        idDocumentos_Deseleccionados = idDocumentos_Deseleccionados.Remove(idDocumentos_Deseleccionados.Length - 1)
                    End If
                    id_documentoRelacionCheck = id_documentoRelacionCheck.Remove(id_documentoRelacionCheck.Length - 1)
                    clsNegocio.LN_InsertarGuias(CDec(Me.txtCapacidadUsada.Text), fechaInicio, CDec(Val(Me.txtCapacidadUsada.Text)), contador, 0, id_documentoRelacionCheck, clsEntidades, "UPDATE", hhdidCronogramaDespacho.Value.ToString(), idDocumentos_Deseleccionados, Val(txtNroVuelta.Text))
                    ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     alert('Edición realizada correctamente');", True)
                End If
                'Next
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
        Call BuscarProgramaciones()
        idDocumentos_Deseleccionados = ""
    End Sub
#End Region

    Private Sub btnAnular_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');onCapa('capaAnular');", True)
    End Sub

    Protected Sub btnExportar_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim button As New Button
        button = DirectCast(sender, Button)
        Dim reporte As New ReportDocument
        Dim dt As New DataTable
        Dim formatType As ExportFormatType
        Dim ddlFiltroTurno As DropDownList = DirectCast(gvVehiculos.HeaderRow.FindControl("ddlTurnoFiltro"), DropDownList)
        Dim ddlFiltroPlaca As DropDownList = DirectCast(gvVehiculos.HeaderRow.FindControl("ddlPlacaVehiculo"), DropDownList)
        Dim idTienda As String = Session("idTiendaUsuario")
        dt = clsNegocio.LN_Reporte(Me.txt3.Text, Me.txt4.Text, ddlFiltroTurno.SelectedItem.ToString(), ddlFiltroPlaca.SelectedItem.ToString(), idTienda)
        reporte = New rpt_programacion
        'Dim ruta As String = Server.MapPath("rpt_programacion_despacho2.rpt")
        'reporte.Load(ruta)

        reporte.SetDataSource(dt)

        Select Case button.CommandName.ToLower()
            Case "pdf"
                formatType = ExportFormatType.PortableDocFormat
                Exit Select
            Case "word"
                formatType = ExportFormatType.WordForWindows
                Exit Select
            Case "excel"
                formatType = ExportFormatType.Excel
                Exit Select
        End Select
        Response.Buffer = False
        Response.ClearContent()
        Response.ClearHeaders()
        reporte.ExportToHttpResponse(formatType, Response, True, "Programación - Despachos")
        Response.[End]()
    End Sub

    Protected Sub btnControladores_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnControladores.Click
        MostrarControladores()
    End Sub

    Private Sub MostrarControladores()
        Dim dt As New DataTable
        Dim idDocumento As String = String.Empty
        For Each row As GridViewRow In Me.gvGuiasSeleccionadas.Rows
            Dim columnaNoVisible = Me.gvGuiasSeleccionadas.DataKeys(row.RowIndex).Values
            idDocumento = idDocumento & columnaNoVisible(2) & ","
        Next
        If Not String.IsNullOrEmpty(idDocumento) Then
            idDocumento = idDocumento.Remove(idDocumento.Length - 1)
        End If
        dt = clsNegocio.LN_Controladores(idDocumento, "ZONA_CONTROLADOR", Session("IdUsuario"), True, Me.txtFechaFiltro.Text, 0)
        Me.gv_Controladores.DataSource = dt
        Me.gv_Controladores.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');", True)
    End Sub

    Protected Sub btnTipoanulacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnTipoanulacion.Click
        Dim indice As Integer = Me.gvVehiculos.SelectedIndex
        Dim idCronograma As String = Me.gvVehiculos.SelectedDataKey.Values("idCronograma")
        clsNegocio.LN_UpdateProgramacion(idCronograma, Me.ddlTipoAnulacion.SelectedValue, Me.txtComentarioAnulacion.Text.Trim())
        BuscarProgramaciones()
    End Sub

    Private Sub CargarAgenciasTransporte()
        Dim obj As Entidades.Vehiculo
        Dim dt As New DataTable
        dt = (New Negocio.Vehiculo).SeleccionarTransportistas()
        Session("vistaTransportistas") = dt        
        Me.GV_Transportista.DataSource = dt
        Me.GV_Transportista.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');    onCapa('capaVehiculo'); ", True)
    End Sub

    Private Sub GV_Transportista_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GV_Transportista.PageIndexChanging
        Me.GV_Transportista.PageIndex = e.NewPageIndex
        CargarAgenciasTransporte()
    End Sub

    Protected Sub ontextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim filtro As String = DirectCast(GV_Transportista.HeaderRow.FindControl("TxtFiltroTransportista"), TextBox).Text
        Dim dt As New DataTable
        dt = Session("vistaTransportistas")
        Dim dv As New DataView(dt)
        dv.RowFilter = " ':' + Transportista + ':' + Doc_Identidad + ':' like '%" & filtro & "%'"
        Me.GV_Transportista.DataSource = dv
        Me.GV_Transportista.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');    onCapa('capaVehiculo'); ", True)
    End Sub

    Private Sub modificarFormularioSegunTransporte()
        Me.txtPlaca.Enabled = False
        Me.ddlConductor.Enabled = False
        Me.ddlAyudante.Enabled = False
        Me.RadioButtonList1.Items(1).Selected = True
        Me.txtCapacidadTotal.Enabled = True
        Me.lblTituloModelo.Text = "Transportista"
        Me.txtPlaca.Text = ""
        Me.txtCapacidadTotal.Text = 0
    End Sub

    Private Sub modificarFormularioSegunVehiculo()
        Me.txtPlaca.Enabled = True
        Me.ddlConductor.Enabled = True
        Me.RadioButtonList1.Items(0).Selected = True
        Me.ddlAyudante.Enabled = True
        Me.txtCapacidadTotal.Enabled = False
        lblTituloModelo.Text = "Modelo"
        Me.txtPlaca.Text = ""
        Me.txtCapacidadTotal.Text = 0
    End Sub

    Private Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonList1.SelectedIndexChanged
        If RadioButtonList1.Items(0).Selected Then
            Me.GV_Vehiculo.Visible = True
            Me.GV_Transportista.Visible = False
            Call buscarVehiculo()
        ElseIf RadioButtonList1.Items(1).Selected Then
            Me.GV_Vehiculo.Visible = False
            Me.GV_Transportista.Visible = True
            CargarAgenciasTransporte()
        End If

    End Sub

    Private Sub GV_Vehiculo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Vehiculo.SelectedIndexChanged
        Dim indice As Integer = GV_Vehiculo.SelectedIndex
        Dim datakey As DataKey = GV_Vehiculo.DataKeys(indice)
        ViewState("idVehiculo") = datakey(4)
        ViewState("idTransportista") = 0
        modificarFormularioSegunVehiculo()
        Me.txtPlaca.Text = IIf(Me.GV_Vehiculo.SelectedDataKey.Values("Placa") = "---", "", Me.GV_Vehiculo.SelectedDataKey.Values("Placa"))
        Me.txtmodelo.Text = Me.GV_Vehiculo.SelectedDataKey.Values("Modelo")
        Me.txtCapacidadTotal.Text = Me.GV_Vehiculo.SelectedDataKey.Values("veh_Capacidad")


        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');    ", True)
    End Sub

    Private Sub GV_Transportista_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Transportista.SelectedIndexChanged
        Dim indice As Integer = GV_Transportista.SelectedIndex
        Dim datakey As DataKey = GV_Transportista.DataKeys(indice)
        ViewState("idTransportista") = datakey(0)
        ViewState("idVehiculo") = 0
        modificarFormularioSegunTransporte()
        Me.txtPlaca.Text = "---"
        Me.txtmodelo.Text = TryCast(Me.GV_Transportista.Rows(indice).FindControl("lblTransportista"), Label).Text
        Me.txtCapacidadTotal.Text = 0
        Me.ddlAyudante.SelectedValue = -1
        Me.ddlConductor.SelectedValue = -1
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "     onCapa('capaDespachos');    ", True)
    End Sub

    Private Sub btnBuscarDocumentoGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarDocumentoGeneral.Click
        Dim nroSerie As Integer = CInt(Val(Me.txtNroSerie.Text.Trim()))
        Dim nroDocumento As Integer = CInt(Val(Me.txtBuscarDocumento.Text.Trim()))
        Dim idTienda As String = Session("idTiendaUsuario")
        Dim obj As New LN_Despachos
        Try
            obj.LN_BuscarDocumentoGenearl(nroSerie, nroDocumento, idTienda)
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnClientefinal_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim button As New Button
        button = DirectCast(sender, Button)
        Dim reporte As New ReportDocument

        Dim formatType As ExportFormatType
        
        reporte = New rpt_despacho_cliente_final
        Dim dt As New DataTable
        Dim lista As List(Of be_rpt_despacho)
        dt = (New bl_Reportes).reporteDespacho_clienteFinal(True, Me.txt3.Text, Me.txt4.Text)
        reporte.SetDataSource(dt)

        Select Case button.CommandName.ToLower()
            Case "pdf"
                formatType = ExportFormatType.PortableDocFormat
                Exit Select
            Case "word"
                formatType = ExportFormatType.WordForWindows
                Exit Select
            Case "excel"
                formatType = ExportFormatType.Excel
                Exit Select
        End Select
        Response.Buffer = False
        Response.ClearContent()
        Response.ClearHeaders()
        reporte.ExportToHttpResponse(formatType, Response, True, "Despachos - Vehiculos sanicenter : Cliente final")
        Response.[End]()
    End Sub

    Protected Sub btnSanicenter_sucursal_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim button As New Button
        button = DirectCast(sender, Button)
        Dim reporte As New ReportDocument

        Dim formatType As ExportFormatType

        reporte = New rpt_despacho_sucursal_terceros
        Dim dt As New DataTable
        Dim lista As List(Of be_rpt_despacho)
        dt = (New bl_Reportes).reporteDespacho_sanicenter_subcontratado(True, Me.txt3.Text, Me.txt4.Text)
        reporte.SetDataSource(dt)

        Select Case button.CommandName.ToLower()
            Case "pdf"
                formatType = ExportFormatType.PortableDocFormat
                Exit Select
            Case "word"
                formatType = ExportFormatType.WordForWindows
                Exit Select
            Case "excel"
                formatType = ExportFormatType.Excel
                Exit Select
        End Select
        Response.Buffer = False
        Response.ClearContent()
        Response.ClearHeaders()
        reporte.ExportToHttpResponse(formatType, Response, True, "Despachos - Vehiculos Terceros : Sanicenter")
        Response.[End]()
    End Sub
End Class