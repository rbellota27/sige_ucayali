<%@ Page Language="vb" AutoEventWireup="false"   MasterPageFile="~/Principal.Master"  CodeBehind="FrmReporteadorKardex.aspx.vb" Inherits="APPWEB.FrmReporteadorKardex" 
    title="Página sin título" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 550px">
                <tr>
                    <td style="background-color: background; text-align: center">
                        <asp:Label ID="Label7" runat="server" Font-Bold="True" ForeColor="White" Text="KÁRDEX VALORIZADO"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Empresa:" Width="64px"></asp:Label>
                        <asp:DropDownList ID="DropDownList1" runat="server" Width="173px">
                            <asp:ListItem>INDUSFERR E.I.R.L.</asp:ListItem>
                            <asp:ListItem>Los Pinos E.I.R.L.</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label ID="Label2" runat="server" Text="Almacén:" Width="64px"></asp:Label>
                        <asp:DropDownList ID="DropDownList2" runat="server" Width="189px">
                            <asp:ListItem>Todos</asp:ListItem>
                            <asp:ListItem>Almacen I</asp:ListItem>
                            <asp:ListItem>Almacen II</asp:ListItem>
                            <asp:ListItem>Almacen III</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="height: 9px">
                        <asp:Label ID="Label3" runat="server" Text="Reporte por:" Width="91px"></asp:Label>
                        <asp:DropDownList ID="DropDownList3" runat="server" Width="205px">
                            <asp:ListItem>Mes Actual</asp:ListItem>
                            <asp:ListItem>A&#241;o Actual</asp:ListItem>
                            <asp:ListItem>Rango de Fechas</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Fecha Inicio:" Width="91px"></asp:Label>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        <asp:Label ID="Label5" runat="server" Text="Fecha Fin:" Width="71px"></asp:Label>
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text="Producto:" Width="91px"></asp:Label>
                        <asp:TextBox ID="TextBox3" runat="server" Width="229px"></asp:TextBox>
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG" /></td>
                </tr>
                <tr>
                    <td style="height: 33px">
                        <asp:Button ID="Button1" runat="server" Text="Aceptar" Width="71px" />
                        <asp:Button ID="Button2" runat="server" Text="Cancelar" /></td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
