<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmGuiaRemisionT.aspx.vb" Inherits="APPWEB.FrmGuiaRemisionT" Title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <table class="style1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" ToolTip="Nuevo" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnEditar" runat="server" OnClientClick="return(  valOnClickEditar()  );"
                                Text="Editar" ToolTip="Editar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscar" runat="server" OnClientClick="return(  valOnClickBuscar()  );"
                                Text="Buscar" ToolTip="Buscar" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnAnular" runat="server" OnClientClick="return(  valOnClickAnular()   );"
                                Text="Anular" ToolTip="Anular" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" ToolTip="Guardar" OnClientClick=" return(     valSaveDocumento()       ); "
                                Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnImprimir" runat="server" OnClientClick="return(  valOnClick_btnImprimir()  );"
                                Text="Imprimir" ToolTip="Imprimir Documento" Width="80px" />
                        </td>
                        <td>
                            <asp:Button ID="btnBuscarDocumentoRef" runat="server" Text="Documento Ref." OnClientClick="  return(  onCapa('capaDocumentosReferencia')  );  "
                                ToolTip="Buscar Documento de Referencia" Width="120px" />
                        </td>
                        <td>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                GU�A DE REMISI�N - TRANSPORTISTA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                        <tr>
                            <td align="right" class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                                Serie:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboSerie" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCodigoDocumento" runat="server" CssClass="TextBox_ReadOnly" onKeypress="return( valOnKeyPressCodigoDoc(this,event)  );"
                                    ReadOnly="true" Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="btnBuscarDocumentoxCodigo" runat="server" ImageUrl="~/Caja/iconos/ok.gif"
                                    OnClientClick="return( valOnClickBuscarDocumentoxCodigo()   );" ToolTip="Buscar Documento por [ Serie - N�mero ]." />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnBusquedaAvanzado" runat="server" CssClass="Texto" Enabled="false"
                                    OnClientClick="return(  valOnClick_btnBusquedaAvanzado()  );" ToolTip="B�squeda Avanzada">Avanzado</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Fecha Emisi�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                    Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="txtFechaEmision_MaskedEditExtender0" runat="server" ClearMaskOnLostFocus="false"
                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaEmision">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="txtFechaEmision_CalendarExtender0" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="txtFechaEmision">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                Estado:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstado" runat="server" Enabled="false" Width="100%">
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="Texto">
                            </td>
                            <td>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Inicio Traslado:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaInicioTraslado" runat="server" CssClass="TextBox_Fecha"
                                    onblur="return(  valFecha(this) );" Width="100px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaInicioTraslado0" runat="server"
                                    ClearMaskOnLostFocus="false" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                    CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicioTraslado">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaInicioTraslado0" runat="server"
                                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaInicioTraslado">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="right" class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td align="right" class="Texto">
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="Texto">
                                Operaci�n:
                            </td>
                            <td colspan="5">
                                <asp:DropDownList ID="cboTipoOperacion" runat="server" Width="100%" Visible="true">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="imgDocumentoRef" runat="server" Width="15px" />&nbsp;DOCUMENTO DE
                            REFERENCIA
                            <cc1:CollapsiblePanelExtender ID="cpeDocumentoRef" runat="server" TargetControlID="Panel_DocumentoRef"
                                ImageControlID="imgDocumentoRef" ExpandedImage="~/Imagenes/Menos_B.JPG" CollapsedImage="~/Imagenes/Mas_B.JPG"
                                Collapsed="true" CollapseControlID="imgDocumentoRef" ExpandControlID="imgDocumentoRef">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_DocumentoRef" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_DocumentoRef" runat="server" AutoGenerateColumns="False" Width="100%">
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="NomTipoDocumento" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Red"
                                                        HeaderText="Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                                    <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblNroDocumento" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                                        <asp:HiddenField ID="hddIdDocumento" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                                        <asp:HiddenField ID="hddIdEmpresa" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />

                                                                        <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                                        <asp:HiddenField ID="hddIdPersona" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Empresa" HeaderText="Empresa" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="Tienda" HeaderText="Tienda" HeaderStyle-Height="25px"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                                </Columns>
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <RowStyle CssClass="GrillaRow" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <EditRowStyle CssClass="GrillaEditRow" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                REMITENTE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Remitente" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="right" style="text-align: left">
                                <table>
                                    <tr>
                                        <td class="Texto" align="right">
                                            Ap. y Nombres/Raz�n Social:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtNombre_Remitente" runat="server" Width="490px" ReadOnly="true"
                                                            CssClass="TextBoxReadOnly"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtIdRemitente" runat="server" CssClass="TextBoxReadOnly" onkeypress="return(false);"
                                                            Width="75px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarRemitente" runat="server" Text="Buscar" Width="80px" OnClientClick="return(  valOnClick_btnBuscarRemitente()  );" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" align="right">
                                            Almac�n:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="cboAlmacen" runat="server" Width="490px" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="Texto">
                                            D.N.I.:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDNI_Remitente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="183px"></asp:TextBox>
                                                    </td>
                                                    <td align="right" class="Texto">
                                                        R.U.C.:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtRUC_Remitente" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                            Width="202px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                PUNTO PARTIDA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_PuntoPartida" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Direcci�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDireccion_Partida" runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" Width="650px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                Depto.:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cboDepto_Partida" Enabled="true" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" align="right">
                                            Provincia:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboProvincia_Partida" Enabled="true" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" align="right">
                                            Distrito:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboDistrito_Partida" Enabled="true" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DESTINATARIO
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Destinatario" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="right" style="text-align: left">
                                <table>
                                    <tr>
                                        <td class="Texto" align="right">
                                            Ap. y Nombres/Raz�n Social:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDestinatario" runat="server" Width="490px" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtIdDestinatario" runat="server" CssClass="TextBoxReadOnly" onkeypress="return(false);"
                                                            Width="75px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarDestinatario" runat="server" Text="Buscar" Width="80px"
                                                            OnClientClick=" return(   valOnClick_btnBuscarDestinatario()   ); " />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Texto" align="right">
                                            Almac�n:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="cboAlmacen_Destinatario" runat="server" AutoPostBack="true"
                                                            Enabled="true" Width="490px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="Texto">
                                            D.N.I.:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtDni_Destinatario" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="183px"></asp:TextBox>
                                                    </td>
                                                    <td align="right" class="Texto">
                                                        R.U.C.:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtRuc_Destinatario" runat="server" CssClass="TextBoxReadOnly" ReadOnly="true"
                                                            Width="202px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="SubTituloCelda">
                PUNTO LLEGADA
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_PuntoLlegada" runat="server">
                    <table>
                        <tr>
                            <td class="Texto" align="right">
                                Direcci�n:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDireccion_Llegada" runat="server" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                    onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" Width="650px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="Texto" align="right">
                                Depto.:
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cboDepto_Llegada" Enabled="true" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" align="right">
                                            Provincia:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboProvincia_Llegada" Enabled="true" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Texto" align="right">
                                            Distrito:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboDistrito_Llegada" Enabled="true" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                DETALLE
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Detalle" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="btnBuscarProducto" runat="server" ImageUrl="~/Imagenes/BuscarProducto_b.JPG"
                                                OnClientClick="return(valOnClick_btnBuscarProducto());" onmouseout="this.src='../Imagenes/BuscarProducto_b.JPG';"
                                                onmouseover="this.src='../Imagenes/BuscarProducto_A.JPG';" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="btnLimpiarDetalleDocumento" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Detalle" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <Columns>
                                        <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="CodigoProducto" HeaderText="C�digo" HeaderStyle-Height="25px"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NomProducto" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:TextBox ID="txtCantidad" TabIndex="300" Width="70px" Font-Bold="true" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Cantidad","{0:F3}")%>' onKeyPress="return( validarNumeroPuntoPositivo('event') );"
                                                                onblur=" return( valBlur(event) ); " onFocus=" return( aceptarFoco(this) ); " onKeyUp=" return(  calcularPesoTotal('0',this)  ); "></asp:TextBox>

                                                            <asp:DropDownList ID="cboUnidadMedida" runat="server" DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaUM")%>'
                                                                DataValueField="IdUnidadMedida" DataTextField="NombreCortoUM" OnSelectedIndexChanged="valOnChange_cboUnidadMedida"
                                                                AutoPostBack="true" Width="70px">
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="hdd_Idproducto" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdProducto") %>' />

                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Peso" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:TextBox ID="txtPeso" TabIndex="310" Width="70px" Font-Bold="true" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"Peso","{0:F3}")%>' onKeyPress="return( validarNumeroPuntoPositivo('event') );"
                                                                onblur=" return( valBlur(event) ); " onFocus=" return( aceptarFoco(this) ); " onKeyUp=" return(  calcularPesoTotal('1',this)  ); "></asp:TextBox>

                                                            <asp:DropDownList ID="cboUnidadMedida_Peso" runat="server" Enabled="false" DataValueField="IdUnidadMedida"
                                                                DataTextField="NombreCortoUM">
                                                            </asp:DropDownList>

                                                            <asp:HiddenField ID="hddEquivalencia" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Equivalencia","{0:F6}")%>' />

                                                            <asp:HiddenField ID="hddEquivalencia1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Equivalencia1","{0:F6}")%>' />

                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table>
                                    <tr>
                                        <td class="Texto" style="font-weight: bold">
                                            Peso Total:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPesoTotal" Width="70px" CssClass="TextBox_ReadOnlyLeft" onKeyPress=" return( false ); "
                                                runat="server" Text="0"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cboUnidadMedida_PesoTotal" runat="server" Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="Image_Transportista" runat="server" Width="15px" />&nbsp;TRANSPORTISTA
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_Transportista" runat="server"
                                TargetControlID="Panel_Transportista" ImageControlID="Image_Transportista" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" Collapsed="true" CollapseControlID="Image_Transportista"
                                ExpandControlID="Image_Transportista">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Transportista" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" align="right">
                                                        Transportista:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTransportista" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="450px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarTransportista" runat="server" Text="Buscar" Width="80px"
                                                            OnClientClick="return(  valOnClick_btnBuscarTransportista()  );" />
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="Texto">
                                                        D.N.I.:
                                                    </td>
                                                    <td colspan="3">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtDni_Transportista" runat="server" CssClass="TextBoxReadOnly"
                                                                        ReadOnly="True" Width="150px"></asp:TextBox>
                                                                </td>
                                                                <td align="right" class="Texto">
                                                                    R.U.C.:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtRuc_Transportista" runat="server" CssClass="TextBoxReadOnly"
                                                                        ReadOnly="True" Width="150px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="Texto">
                                                        Direcci�n:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDireccion_Transportista" runat="server" CssClass="TextBoxReadOnly"
                                                            ReadOnly="True" Width="450px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="Image_Chofer" runat="server" Width="15px" />&nbsp;CHOFER
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_Chofer" runat="server"
                                TargetControlID="Panel_Chofer" ImageControlID="Image_Chofer" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" Collapsed="true" CollapseControlID="Image_Chofer"
                                ExpandControlID="Image_Chofer">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Chofer" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" align="right">
                                                        Chofer:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtChofer" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="520px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarChofer" runat="server" Text="Buscar" Width="80px" OnClientClick="return(  valOnClick_btnBuscarChofer()  );" />
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="Texto">
                                                        D.N.I.:
                                                    </td>
                                                    <td colspan="3">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtDni_Chofer" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                        Width="120px"></asp:TextBox>
                                                                </td>
                                                                <td align="right" class="Texto">
                                                                    R.U.C.:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtRuc_Chofer" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                        Width="120px"></asp:TextBox>
                                                                </td>
                                                                <td align="right" class="Texto">
                                                                    Nro. Licencia:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtNroLicencia" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                                        Width="120px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="TituloCeldaLeft">
                            <asp:Image ID="Image_Vehiculo" runat="server" Width="15px" />&nbsp;VEH�CULO
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender_Vehiculo" runat="server"
                                TargetControlID="Panel_Vehiculo" ImageControlID="Image_Vehiculo" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" Collapsed="true" CollapseControlID="Image_Vehiculo"
                                ExpandControlID="Image_Vehiculo">
                            </cc1:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Vehiculo" runat="server" Width="100%">
                                <table>
                                    <tr>
                                        <td align="right" class="Texto">
                                            Modelo / Marca:
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtModelo_Vehiculo" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="200px"></asp:TextBox>
                                                    </td>
                                                    <td align="right" class="Texto">
                                                        Nro. Placa:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPlaca_Vehiculo" runat="server" CssClass="TextBoxReadOnly" ReadOnly="True"
                                                            Width="120px"></asp:TextBox>
                                                    </td>
                                                    <td align="right" class="Texto">
                                                        Nro. Certificado MTC:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCertificado_Vehiculo" runat="server" CssClass="TextBoxReadOnly"
                                                            ReadOnly="True" Width="150px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnBuscarVehiculo" runat="server" Text="Buscar" Width="80px" OnClientClick=" return(   valOnClick_btnBuscarVehiculo()      ); " />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="TituloCelda">
                OBSERVACIONES
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel_Obs" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" Width="100%" runat="server"
                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos);"
                                    Height="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdRemitente" runat="server" />
                <asp:HiddenField ID="hddIdDestinatario" runat="server" />
                <asp:HiddenField ID="hddIdTransportista" runat="server" />
                <asp:HiddenField ID="hddIdChofer" runat="server" />
                <asp:HiddenField ID="hddIdVehiculo" runat="server" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <asp:HiddenField ID="hddFrmModo" runat="server" />
                <asp:HiddenField ID="hddCodigoDocumento" runat="server" />
                <asp:HiddenField ID="hddIndex_GV_Detalle" runat="server" />
                <asp:HiddenField ID="hddIdTipoDocumento" runat="server" Value="8" />
                <asp:HiddenField ID="hddOpcionBusquedaPersona" runat="server" Value="0" />
                <asp:HiddenField ID="hddNroFilasxDocumento" runat="server" Value="0" />
                <asp:HiddenField ID="hddConfigurarDatos" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    </ContentTemplate>    
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
    <div id="capaBuscarProducto_AddProd" style="border: 3px solid blue; padding: 8px;
        width: 900px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 2; display: none;">
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="Texto">
                                        TipoExistencia:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True"
                                            DataTextField="Descripcion" DataValueField="Id">
                                        </asp:DropDownList>
                                        <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                        <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre"
                                            DataValueField="Id" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"
                                                        onKeypress="return( valKeyPressDescripcionProd(this,event) );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"></asp:TextBox>
                                                </td>
                                                <td class="Texto">
                                                    C�d.:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCodigoProducto" Font-Bold="true" Width="100px" runat="server"
                                                        onKeypress="return( valKeyPressDescripcionProd(this,event) );" onBlur="return ( onBlurTextTransform(this,configurarDatos) );"
                                                        onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);" TabIndex="205"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                        TabIndex="207" onmouseout="this.src='../Imagenes/Buscar_b.JPG';" onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnAddProductos_AddProd" runat="server" ImageUrl="~/Imagenes/Agregar_B.JPG"
                                                        onmouseout="this.src='../Imagenes/Agregar_B.JPG';" onmouseover="this.src='../Imagenes/Agregar_A.JPG';"
                                                        OnClientClick="return(valAddProductos());" TabIndex="209" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG"
                                                        onmouseout="this.src='../Imagenes/Limpiar_B.JPG';" onmouseover="this.src='../Imagenes/Limpiar_A.JPG';"
                                                        OnClientClick="return(limpiarBuscarProducto());" TabIndex="208" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" TargetControlID="Panel_BusqAvanzadoProd"
                                CollapsedSize="0" ExpandedSize="0" Collapsed="true" ExpandControlID="Image21_11"
                                CollapseControlID="Image21_11" TextLabelID="Label21_11" ImageControlID="Image21_11"
                                CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                CollapsedText="B�squeda Avanzada" ExpandedText="B�squeda Avanzada" ExpandDirection="Vertical"
                                SuppressPostBack="true">
                            </cc1:CollapsiblePanelExtender>
                            <asp:Image ID="Image21_11" runat="server" Height="16px" />
                            <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                            <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                                <table width="100">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Texto" style="font-weight: bold">
                                                        Atributo:
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboTipoTabla" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False"
                                                Width="650px">
                                                <Columns>
                                                    <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px"
                                                        HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-Height="25px" />
                                                    <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label>
                                                            <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                                DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="GrillaRow" />
                                                <FooterStyle CssClass="GrillaFooter" />
                                                <PagerStyle CssClass="GrillaPager" />
                                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                <HeaderStyle CssClass="GrillaHeader" />
                                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="DGV_AddProd" runat="server" AutoGenerateColumns="False" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCantidad_AddProd" TabIndex="210" Width="65px" onblur="return( valBlurClear('0.00',event) );"
                                                onfocus="return(  aceptarFoco(this) );" onKeypress="return( valKeyPressCantidadAddProd(event)  );"
                                                Text="0" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="U.M." HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                                        <asp:DropDownList ID="cboUnidadMedida" runat="server" Width="80px" AutoPostBack="false"
                                                            DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaUM")%>' DataTextField="NombreCortoUM"
                                                            DataValueField="IdUnidadMedida">
                                                        </asp:DropDownList>

                                                        <asp:HiddenField ID="hddIdUnidadMedida_PR" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdUnidadMedida")%>' />

                                                        <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Codigo" HeaderText="C�digo" HeaderStyle-Height="25px"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center" NullDisplayText="---" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true"
                                Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                            <asp:Button TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true"
                                Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                            <asp:TextBox TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true"
                                CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                            <asp:Button TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px"
                                Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                            <asp:TextBox TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly"
                                runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
    </div>
    </ContentTemplate>    
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
    <ContentTemplate>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 231px; left: 21px; background-color: white;
        z-index: 2; display: none;">
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(offCapa('capaPersona'));" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnlBusquedaPersona" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td colspan="5">
                                                                                <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal"
                                                                                    AutoPostBack="false">
                                                                                    <asp:ListItem Value="N">Natural</asp:ListItem>
                                                                                    <asp:ListItem Selected="True" Value="J">Juridica</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="Texto">
                                                                                Razon Social / Nombres:
                                                                            </td>
                                                                            <td colspan="4">
                                                                                <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(this,event));" runat="server"
                                                                                    onBlur="return ( onBlurTextTransform(this,configurarDatos) );" onFocus="onFocusTextTransform(this,configurarDatos); aceptarFoco(this);"
                                                                                    Width="450px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="Texto">
                                                                                D.N.I.:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                                    MaxLength="8" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td class="Texto">
                                                                                Ruc:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                                    MaxLength="11"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false"
                                                                                    ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../Imagenes/Buscar_b.JPG';"
                                                                                    onmouseover="this.src='../Imagenes/Buscar_A.JPG';" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                        PageSize="20" Width="100%">
                                                                        <Columns>
                                                                            <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true"
                                                                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px"
                                                                                ItemStyle-Height="25px" />
                                                                            <asp:BoundField DataField="idpersona" HeaderText="C�digo" NullDisplayText="0" HeaderStyle-HorizontalAlign="Center"
                                                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px" />
                                                                            <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---"
                                                                                HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-Height="25px"
                                                                                ItemStyle-Height="25px"></asp:BoundField>
                                                                            <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---" HeaderStyle-HorizontalAlign="Center"
                                                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---" HeaderStyle-HorizontalAlign="Center"
                                                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                        <EditRowStyle CssClass="GrillaEditRow" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                        ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                                    <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                        ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                                    <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                        runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                            Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                                    <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                        onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
    </div>
    </ContentTemplate>    
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
    <ContentTemplate>
    <div id="capaGlosa" style="border: 3px solid blue; padding: 8px; width: auto; height: auto;
        position: absolute; top: 300px; left: 250px; background-color: white; z-index: 2;
        display: none;">
        <table>
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton7" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaGlosa'));" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table>
                        <tr>
                            <td class="Texto">
                                Glosa:
                            </td>
                            <td>
                                <asp:TextBox ID="txtGlosa_CapaGlosa" runat="server" Width="350px" TextMode="MultiLine"
                                    Height="50px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIndexGlosa_Gv_Detalle" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:ImageButton ID="btnAddGlosa_CapaGlosa" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                        onmouseout="this.src='../Imagenes/Aceptar_B.JPG';" OnClientClick="return(     valOnClickAddGlosa()       );"
                        onmouseover="this.src='../Imagenes/Aceptar_B.JPG';" />
                </td>
            </tr>
        </table>
    </div>
    </ContentTemplate>    
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
    <ContentTemplate>
    <div id="capaBusquedaAvanzado" style="border: 3px solid blue; padding: 8px; width: 850px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 142px; left: 22px;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="btnCerrar_capaDocumentosReferencia" runat="server" ToolTip="Cerrar"
                        ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaBusquedaAvanzado')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel_BusquedaAvanzado" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="Texto">
                                                Fecha Inicio:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaI" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaI">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaI">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td class="Texto">
                                                Fecha Fin:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFechaF" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                    Width="100px"></asp:TextBox>
                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" ClearMaskOnLostFocus="false"
                                                    CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                    CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                    CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaF">
                                                </cc1:MaskedEditExtender>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtFechaF">
                                                </cc1:CalendarExtender>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnAceptarBusquedaAvanzado" Width="70px" ToolTip="Buscar Documentos"
                                                    runat="server" Text="Buscar" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_BusquedaAvanzado" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="50px" HeaderStyle-Height="25px">
                                <HeaderStyle Height="25px" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CommandField>
                            <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                HeaderText="Tipo"></asp:BoundField>
                            <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroDocumento_BusquedaAvanzado" ForeColor="Red" Font-Bold="true"
                                                    runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdDocumento_BusquedaAvanzado" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fec. Emisi�n"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                            <asp:BoundField DataField="DescripcionPersona" HeaderText="Destinatario" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="NomEstadoDocumento" HeaderText="Estado" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="LabelRojo" style="font-weight: bold">
                    *** El proceso de b�squeda realiza un filtrado de acuerdo al destinatario seleccionado.
                </td>
            </tr>
        </table>
    </div>
    </ContentTemplate>    
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
    <ContentTemplate>
    <div id="capaDocumentosReferencia" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; background-color: white; z-index: 2; display: none;
        top: 100px; left: 30px;">
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="ImageButton6" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                                OnClientClick="return(  offCapa('capaDocumentosReferencia')   );" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="Texto">
                                        Tipo Documento:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="CboTipoDocumento" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="text-align: left">
                            <asp:RadioButtonList ID="rdbBuscarDocRef" runat="server" RepeatDirection="Horizontal"
                                CssClass="Texto" AutoPostBack="true">
                                <asp:ListItem Value="0" Selected="True">Por Fecha de Emisi�n</asp:ListItem>
                                <asp:ListItem Value="1">Por Nro. Documento</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Panel ID="Panel_BuscarDocRefxFecha" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td class="Texto">
                                                        Fecha Inicio:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtFechaInicio_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                            Width="100px"></asp:TextBox>
                                                        <cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" ClearMaskOnLostFocus="false"
                                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaInicio_DocRef">
                                                        </cc1:MaskedEditExtender>
                                                        <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                            TargetControlID="txtFechaInicio_DocRef">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                    <td class="Texto">
                                                        Fecha Fin:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtFechaFin_DocRef" runat="server" CssClass="TextBox_Fecha" onblur="return(  valFecha(this) );"
                                                            Width="100px"></asp:TextBox>
                                                        <cc1:MaskedEditExtender ID="MaskedEditExtender5" runat="server" ClearMaskOnLostFocus="false"
                                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                            CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" TargetControlID="txtFechaFin_DocRef">
                                                        </cc1:MaskedEditExtender>
                                                        <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                            TargetControlID="txtFechaFin_DocRef">
                                                        </cc1:CalendarExtender>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAceptarBuscarDocRef" Width="70px" ToolTip="Buscar Documentos" CssClass="btnBuscar"
                                                            runat="server" Text="Buscar" OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="Panel_BuscarDocRefxCodigo" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="Texto">
                                                        Nro. Serie:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtSerie_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                            Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td class="Texto">
                                                        Nro. C�digo:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCodigo_BuscarDocRef" onFocus="return(  aceptarFoco(this)  );"
                                                            Width="80px" onKeypress="return(   onKeyPressEsNumero('event')  );" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnAceptarBuscarDocRefxCodigo" OnClientClick="return(  valOnClick_btnAceptarBuscarDocRefxCodigo()  );"
                                                            runat="server" Text="Buscar" Width="70px" ToolTip="Buscar Documentos" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel_DocRef_Find" runat="server" ScrollBars="Horizontal" Width="100%">
                                <asp:GridView ID="GV_DocumentosReferencia_Find" runat="server" AutoGenerateColumns="false"
                                    Width="100%">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" EditText="OK" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-Width="50px" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="NomTipoDocumento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderText="Tipo" />
                                        <asp:TemplateField HeaderText="Nro. Documento" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                            <asp:Label ID="lblNroDocumento_Find" ForeColor="Red" Font-Bold="true" runat="server"
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"NroDocumento")%>'></asp:Label>

                                                            <asp:HiddenField ID="hddIdDocumentoReferencia_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />

                                                            <asp:HiddenField ID="hddIdEmpresa_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdEmpresa")%>' />

                                                            <asp:HiddenField ID="hddIdAlmacen_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                            <asp:HiddenField ID="hddIdPersona_Find" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdPersona")%>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FechaEmision" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Emisi�n"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="DescripcionPersona" HeaderText="Descripci�n" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="RUC" HeaderText="R.U.C." HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="DNI" HeaderText="D.N.I." HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="NomAlmacen" HeaderText="Almac�n" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Doc. Ref." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddl_TipoDocumentoRef" DataSource='<%#DataBinder.Eval(Container.DataItem,"getTipoDocumentoRef")%>'
                                                    DataTextField="DescripcionCorto" DataValueField="Id" runat="server">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
    </div>
    </ContentTemplate>    
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
    <ContentTemplate>
    <div id="capaVehiculo" style="border: 3px solid blue; padding: 8px; width: 560px;
        height: auto; position: absolute; top: 800px; left: 87px; background-color: white;
        z-index: 2; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton5" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(offCapa('capaVehiculo'));" />
                </td>
            </tr>
            <tr class="Label">
                <td align="left">
                    Nro. Placa:
                    <asp:TextBox ID="txtNroPlaca_BuscarVehiculo" runat="server" Width="120px"></asp:TextBox>
                    <asp:Button ID="btnBuscarVehiculo_Grilla" runat="server" Text="Buscar" Width="80px" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_Vehiculo" runat="server" AutoGenerateColumns="false" Width="100%">
                        <Columns>
                            <asp:CommandField SelectText="Seleccionar" ShowSelectButton="true" ButtonType="Link"
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Nro. Placa" HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblNroPlaca" Font-Bold="true" ForeColor="Red" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Placa")%>'></asp:Label>

                                                <asp:HiddenField ID="hddIdVehiculo" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdProducto")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Modelo" HeaderText="Modelo" NullDisplayText="---" HeaderStyle-Height="25px"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="ConstanciaIns" HeaderText="Const. Inscripci�n" NullDisplayText="---"
                                HeaderStyle-Height="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </ContentTemplate>    
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
    <ContentTemplate>
    <div id="capaConsultarStockPrecioxProducto" style="border: 3px solid blue; padding: 8px;
        width: 450px; height: auto; position: absolute; top: 237px; left: 32px; background-color: white;
        z-index: 6; display: none;">
        <table style="width: 100%;">
            <tr>
                <td align="right">
                    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar.GIF"
                        OnClientClick="return(  offCapa2('capaConsultarStockPrecioxProducto')   );" />
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="LabelRojo" style="font-weight: bold">
                                Producto:
                            </td>
                            <td>
                                <asp:Label ID="lblProductoConsultarStockPrecioxProd" runat="server" Text="-" CssClass="Texto"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GV_ConsultarStockPrecioxProd" runat="server" AutoGenerateColumns="false"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Almac�n" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:HiddenField ID="hddIdAlmacen" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdAlmacen")%>' />

                                                <asp:Label ID="lblAlmacen" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Almacen")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Stock Disponible" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                                <asp:Label ID="lblStock" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StockDisponibleN","{0:F3}")%>'></asp:Label>

                                                <asp:Label ID="lblUM" ForeColor="Red" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cadenaUM")%>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cantidad" Visible="false" HeaderStyle-Height="25px"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCantidad" Text="0" runat="server" Width="80px" Font-Bold="true"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="GrillaHeader" />
                        <FooterStyle CssClass="GrillaFooter" />
                        <PagerStyle CssClass="GrillaPager" />
                        <RowStyle CssClass="GrillaRow" />
                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        <EditRowStyle CssClass="GrillaEditRow" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnAddProducto_ConsultarStockPrecio" runat="server" Text="Aceptar"
                        Width="90px" OnClientClick="return(  offCapa2('capaConsultarStockPrecioxProducto')   );" />
                </td>
            </tr>
        </table>
    </div>
    </ContentTemplate>    
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }

        function valKeyPressCodigoSL(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(obj,e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) { // Enter          
                onBlurTextTransform(obj, configurarDatos);
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valAddProductos() {

            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            //        var txtIdCliente = 
            //        if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
            //            alert('Debe seleccionar un Cliente.');
            //            return false;
            //        }

            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('No se seleccionaron productos.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                var txtCantidad = rowElem.cells[0].children[0];
                if (parseFloat(txtCantidad.value) > 0) {
                    // if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
                    //                        alert('La cantidad ingresada excede a la cantidad disponible.');
                    //                        txtCantidad.select();
                    //                        txtCantidad.focus();
                    //     return false;
                    //  } else if (parseFloat(txtCantidad.value) > 0) {
                    cont = cont + 1;
                    //  }
                }
            }

            if (cont == 0) {
                alert('No se seleccionaron productos.');
                return false;
            }

            /*
            //************* Validamos que no agregue de otro almacen
            
            if (grillaDetalle != null) {

                if (grillaDetalle.rows.length > 1) {

                    //************ valido el almacen
                    
            var hddIdAlmacen = 
                    

                    if (parseFloat(cboAlmacen.value) != parseFloat(hddIdAlmacen.value)) {
                        
            return false;
            }
            }
            }
            */
            return true;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        function valStockDisponible_AddProd() {
            /*var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
            if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].id == event.srcElement.id) {
            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
            alert('La cantidad ingresada excede a la cantidad disponible.');
            rowElem.cells[0].children[0].select();
            rowElem.cells[0].children[0].focus();
            return false;
            }
            return false;
            }

                }
            }*/
            return false;
        }
        function valKeyPressCantidadAddProd(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                document.getElementById('<%=btnAddProductos_AddProd.ClientID%>').focus();
            } else {
                return validarNumeroPuntoPositivo('event');
            }
            return true;
        }
        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }

        //***************************************************** BUSQUEDA PERSONA

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(obj,e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;            
            if (key == 13) {
                onBlurTextTransform(obj, configurarDatos);
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }
        function valOnClick_btnBuscarRemitente() {
            //        document.getElementById('<%=hddOpcionBusquedaPersona.ClientID%>').value = '0';
            //        mostrarCapaPersona();
            //        return false;
            return true;
        }
        function valOnClick_btnBuscarDestinatario() {
            /*
            document.getElementById('<%=hddOpcionBusquedaPersona.ClientID%>').value = '1';
            mostrarCapaPersona();
            return false;
            */
            return true;
        }
        function valOnClick_btnBuscarTransportista() {
            /*
            document.getElementById('<%=hddOpcionBusquedaPersona.ClientID%>').value = '2';
            mostrarCapaPersona();
            return false;
            */
            return true;
        }
        function valOnClick_btnBuscarChofer() {
            /*
            document.getElementById('<%=hddOpcionBusquedaPersona.ClientID%>').value = '3';
            mostrarCapaPersona();
            return false;
            */
            return true;
        }
        function valOnClick_btnBuscarVehiculo() {
            onCapa('capaVehiculo');
            document.getElementById('<%=txtNroPlaca_BuscarVehiculo.ClientID%>').select();
            document.getElementById('<%=txtNroPlaca_BuscarVehiculo.ClientID%>').focus();
            return false;
        }
        function valSaveDocumento() {


            var cboEmpresa = document.getElementById('<%=cboEmpresa.ClientID%>');
            if (isNaN(parseInt(cboEmpresa.value)) || cboEmpresa.value.length <= 0) {
                alert('Debe seleccionar una Empresa');
                return false;
            }
            var cboTienda = document.getElementById('<%=cboTienda.ClientID%>');
            if (isNaN(parseInt(cboTienda.value)) || cboTienda.value.length <= 0) {
                alert('Debe seleccionar una Tienda');
                return false;
            }

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0) {
                alert('Debe seleccionar una Serie de Documento.');
                return false;
            }

            var cboTipoOperacion = document.getElementById('<%=cboTipoOperacion.ClientID%>');
            if (isNaN(parseInt(cboTipoOperacion.value)) || cboTipoOperacion.value.length <= 0 || parseInt(cboTipoOperacion.value) <= 0) {
                alert('Debe seleccionar un Tipo de Operaci�n.');
                return false;
            }

            var cboEstadoDocumento = document.getElementById('<%=cboEstado.ClientID%>');
            if (isNaN(parseInt(cboEstadoDocumento.value)) || cboEstadoDocumento.value.length <= 0) {
                alert('Debe seleccionar un Estado del Documento.');
                return false;
            }

            var hddIdDestinatario = document.getElementById('<%=hddIdDestinatario.ClientID%>');
            if (isNaN(parseInt(hddIdDestinatario.value)) || hddIdDestinatario.value.length <= 0) {
                alert('Debe seleccionar un Destinatario.');
                return false;
            }

            var hddIdRemitente = document.getElementById('<%=hddIdRemitente.ClientID%>');
            if (isNaN(parseInt(hddIdRemitente.value)) || hddIdRemitente.value.length <= 0) {
                alert('Debe seleccionar un Remitente.');
                return false;
            }

            //************* Validando cantidades y precios
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                var cantidad = 0;
                var txtCantidad;
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    txtCantidad = rowElem.cells[3].children[0].cells[0].children[0];
                    cantidad = parseFloat(txtCantidad.value);
                    if (isNaN(cantidad)) { cantidad = 0; }
                    if (cantidad <= 0) {
                        alert('Ingrese una cantidad mayor a cero.');
                        txtCantidad.select();
                        txtCantidad.focus();
                        return false;
                    }
                }
            } else {
                alert('No se han ingresado productos.');
                return false;
            }
            return confirm('Desea continuar con la Operaci�n ?');
        }

        //***********************************
        function valOnClickBuscar() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            var panel_Cab = document.getElementById('<%=Panel_Cab.ClientID %>');
            var btnBuscarDocumentoxCodigo = document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>');
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                panel_Cab.disabled = false;
                btnBuscarDocumentoxCodigo.disabled = false;
                btnBusquedaAvanzado.disabled = false;
                txtCodigoDocumento.readOnly = false;
                txtCodigoDocumento.disabled = false;
                txtCodigoDocumento.style.backgroundColor = 'Yellow';
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            return true;
        }
        function valOnKeyPressCodigoDoc(txtCodigoDocumento,event) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) {
                document.getElementById('<%=btnBuscarDocumentoxCodigo.ClientID %>').focus();
            } else {
                return onKeyPressEsNumero('event');
            }

        }

        function valOnClickBuscarDocumentoxCodigo() {
            var txtCodigoDocumento = document.getElementById('<%=txtCodigoDocumento.ClientID %>');
            if (txtCodigoDocumento.readOnly == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }
            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            if (isNaN(parseInt(txtCodigoDocumento.value)) || txtCodigoDocumento.value.length <= 0) {
                alert('Debe ingresar un valor v�lido.');
                txtCodigoDocumento.select();
                txtCodigoDocumento.focus();
                return false;
            }
            document.getElementById('<%=hddCodigoDocumento.ClientID %>').value = txtCodigoDocumento.value;
            return true;
        }
        function valOnClickEditar() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para Edici�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento est� ANULADO. No se permite su edici�n.');
                return false;
            }
            return true;
        }
        function valOnClickAnular() {
            var hddIdDocumento = document.getElementById('<%=hddIdDocumento.ClientID%>');
            if (isNaN(parseInt(hddIdDocumento.value)) || hddIdDocumento.value.length <= 0) {
                alert('No se tiene ning�n Documento para su Anulaci�n.');
                return false;
            }
            var cboEstado = document.getElementById('<%=cboEstado.ClientID%>');
            if (parseInt(cboEstado.value) == 2) {
                alert('El Documento ya est� ANULADO. No se permite su anulaci�n.');
                return false;
            }
            return confirm('Desea continuar con el Proceso de ANULACI�N del Documento ?');
        }

        function valOnClick_btnBusquedaAvanzado() {

            var cboSerie = document.getElementById('<%=cboSerie.ClientID%>');
            if (isNaN(parseInt(cboSerie.value)) || cboSerie.value.length <= 0 || parseInt(cboSerie.value) <= 0) {
                alert('Debe seleccionar una Serie.');
                return false;
            }
            var btnBusquedaAvanzado = document.getElementById('<%=btnBusquedaAvanzado.ClientID %>');
            if (btnBusquedaAvanzado.disabled == true) {  //**** Se habilita la B�squeda
                alert('Debe habilitar la B�squeda del Documento haciendo clic en el bot�n [ Buscar ]');
                return false;
            }

            onCapa('capaBusquedaAvanzado');
            return false;
        }

        function valOnClick_btnAceptarBuscarDocRefxCodigo() {
            var txtSerie = document.getElementById('<%=txtSerie_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtSerie.value)) || txtSerie.value.length <= 0) {
                alert('Ingrese un Nro. de Serie para la b�squeda.');
                txtSerie.select();
                txtSerie.focus();
                return false;
            }
            var txtCodigo = document.getElementById('<%=txtCodigo_BuscarDocRef.ClientID%>');
            if (isNaN(parseInt(txtCodigo.value)) || txtCodigo.value.length <= 0) {
                alert('Ingrese un Nro. de Documento para la b�squeda.');
                txtCodigo.select();
                txtCodigo.focus();
                return false;
            }
            return true;
        }


        //**********************************    CALCULO DE EQUIVALENCIA PRODUCTO
        function valOnClick_btnEquivalencia_PR(boton) {
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];
                    if (boton.id == rowElem.cells[6].children[0].cells[0].children[0].id) {
                        //onCapa('capaEquivalenciaProducto');                                                
                        document.getElementById('<%=hddIndex_GV_Detalle.ClientID%>').value = (i - 1);

                        return true;
                    }
                }
            }
            alert('Problemas en la ubicaci�n de la Fila.');
            return false;
        }



        //**********************************    FIN  CALCULO DE EQUIVALENCIA PRODUCTO

        function valOnClick_btnImprimir() {
            var hdd = document.getElementById('<%= hddIdDocumento.ClientID%>');
            if (hdd.value.length == 0 || hdd.value == '' || parseFloat(hdd.value) == 0) {
                alert('No se ha registrado ninguna Gu�a de Remisi�n para enviar a impresi�n.');
                return false;
            }
            //if (confirm('Desea mandar la impresi�n de la Gu�a de Remisi�n?')) {
            // mostrarVentana('../../Reportes/visor.aspx?iReporte=10&IdDocumento=' + hdd.value, 'Orden de Despacho', 500, 500);
            window.parent.open('../../Reportes/visor.aspx?iReporte=10&IdDocumento=' + hdd.value, 'Reporte', 'resizable=yes', null);
            //}
            return false;
        }
        function valOnKeyPress_txtDescripcionProd_AddProd(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) { // Enter
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valOnKeyPress_txtCodSubLinea_BuscarProd(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) { // Enter
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
                return true;
            }
            return (onKeyPressEsNumero('event'));
        }
        function valOnKeyPress_txtCantidad_AddProd(e) {
            var evt = e ? e : event;
            var key = window.Event ? evt.which : evt.keyCode;
            if (key == 13) { // Enter
                document.getElementById('<%=btnAddProductos_AddProd.ClientID%>').focus();
                return true;
            }
            return (validarNumeroPuntoPositivo('event'));
        }
        function calcularPesoTotal(opcion, txtCaja) {
            /*
            opcion  0: Cantidad 1: Peso
            */
            var txtPesoTotal = document.getElementById('<%=txtPesoTotal.ClientID%>');
            var grilla = document.getElementById('<%=GV_Detalle.ClientID%>');
            var pesoTotal = 0;
            var cantidad;
            var peso;
            var equivalencia;
            var equivalencia1;
            var txtCantidad;
            var txtPeso;
            var hddEquivalencia;
            var hddEquivalencia1;

            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {  //*********** VERIFICAR PORCENTAJES MAX DCTO
                    var rowElem = grilla.rows[i];

                    //**************  OBTENEMOS LOS DATOS
                    txtCantidad = rowElem.cells[3].children[0].cells[0].children[0];
                    cantidad = parseFloat(txtCantidad.value);
                    if (isNaN(cantidad) || txtCantidad.value.length <= 0) { cantidad = 0; }
                    txtPeso = rowElem.cells[4].children[0].cells[0].children[0];
                    peso = parseFloat(txtPeso.value);
                    if (isNaN(peso) || txtPeso.value.length <= 0) { peso = 0; }
                    hddEquivalencia = rowElem.cells[4].children[0].cells[2].children[0];
                    equivalencia = parseFloat(hddEquivalencia.value);
                    if (isNaN(equivalencia) || hddEquivalencia.value.length <= 0) { equivalencia = 0; }
                    hddEquivalencia1 = rowElem.cells[4].children[0].cells[3].children[0];
                    equivalencia1 = parseFloat(hddEquivalencia1.value);
                    if (isNaN(equivalencia1) || hddEquivalencia1.value.length <= 0) { equivalencia1 = 0; }

                    switch (opcion) {
                        case '0':
                            if (txtCantidad.id == txtCaja.id) {

                                peso = cantidad * equivalencia * equivalencia1;
                                txtPeso.value = redondear(peso, 3);

                            }
                            break;
                    }

                    pesoTotal = pesoTotal + peso;

                }
            }

            txtPesoTotal.value = redondear(pesoTotal, 3);
            return false;

        }
    </script>

    <script language="javascript" type="text/javascript">
        //////////////////////////////////////////
        var configurarDatos = document.getElementById('<%=hddConfigurarDatos.ClientID %>').value;
        /////////////////////////////////////////
    </script>

</asp:Content>
