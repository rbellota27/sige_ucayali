﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmInventariosxMxA.aspx.vb" Inherits="APPWEB.FrmInventariosxMxA" 
    title="Página sin título" %>
    
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>   
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<table class="style1">
   <tr style="background-color: Yellow">
            <td style="width: 350px">
                <asp:LinkButton CssClass="Label" Font-Bold="true" ID="lkb_CapaConfigFrm" OnClientClick="return(onCapa('capaConfiguracionFrm'));"
                    runat="server">Config. Formulario</asp:LinkButton>
            </td>
    </tr>
    <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnNuevo" runat="server" Text="Nueva Búsqueda" ToolTip="Nueva Busqueda" Width="120px" />
                        </td>
                       
                        <td>
                            <asp:Button ID="btnBuscar" runat="server"  Text="Buscar" ToolTip="Buscar" Width="80px"
                            OnClientClick="this.disabled=true;this.value='Procesando...'" UseSubmitBehavior="false"  />
                        </td>
                       
                       
                        <td>                            
                            <input id="btnExport" type="button" onclick="getPrint('divAreaImprimir')"; value="Exportar a Excel" />
                        </td>
                          <td>
                          <asp:Button  ID="btnreporte" runat ="server" Text ="Visualizar Reporte" ToolTip="Visualizar Documento"/>
                        </td>
                        <td align="right" style="width: 100%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
           <tr>
            <td class="TituloCelda">
                REPORTE DE ROTACION DE INVENTARIO DE LOS INDICADORES [NDI] - [IRI]
            </td>
        </tr>
        <tr>
        <td>
            <asp:Panel ID="Panel_Cab" runat="server">
                    <table>
                                        <tr>
                                                <td align="right" class="Texto">
                                                Mes:
                                                </td>
                                                
                                                <td >
                                                  <asp:DropDownList ID="cboMes" runat="server" Enabled="true" >
                                                         <asp:ListItem Text="Enero" Value="1" ></asp:ListItem>
                                                         <asp:ListItem Text="Febrero" Value="2" ></asp:ListItem>
                                                         <asp:ListItem Text="Marzo" Value="3" ></asp:ListItem>
                                                         <asp:ListItem Text="Abril" Value="4" ></asp:ListItem>
                                                         <asp:ListItem Text="Mayo" Value="5" ></asp:ListItem>
                                                         <asp:ListItem Text="Junio" Value="6" ></asp:ListItem>
                                                         <asp:ListItem Text="Julio" Value="7" ></asp:ListItem>
                                                         <asp:ListItem Text="Agosto" Value="8" ></asp:ListItem>
                                                         <asp:ListItem Text="Septiembre" Value="9" ></asp:ListItem>
                                                         <asp:ListItem Text="Octubre" Value="10" ></asp:ListItem>
                                                         <asp:ListItem Text="Noviembre" Value="11" ></asp:ListItem>
                                                         <asp:ListItem Text="Diciembre" Value="12" ></asp:ListItem>      
                                                </asp:DropDownList>
                                                </td>
                                              
                                              
                                                  <td> &nbsp;</td>
                                                  <td>
                                              &nbsp;
                                                  </td>
                                                  <td align="right" class="Texto">
                                                Año:
                                                </td>
                                                <td>
                                                <asp:DropDownList ID="cboanio" runat="server" Enabled="true" >
                                                     <asp:ListItem Text="2010" Value="1" ></asp:ListItem>
                                                     <asp:ListItem Text="2011" Value="2" ></asp:ListItem>
                                                     <asp:ListItem Text="2012" Value="3" ></asp:ListItem>
                                                     <asp:ListItem Text="2013" Value="4" ></asp:ListItem>
                                                     <asp:ListItem Text="2014" Value="5" ></asp:ListItem>
                                                     <asp:ListItem Text="2015" Value="6" ></asp:ListItem>
                                                     <asp:ListItem Text="2016" Value="7" ></asp:ListItem>
                                                     <asp:ListItem Text="2017" Value="8" ></asp:ListItem>
                                                     <asp:ListItem Text="2018" Value="9" ></asp:ListItem>
                                                     <asp:ListItem Text="2019" Value="10" ></asp:ListItem>
                                                      <asp:ListItem Text="2020" Value="11" ></asp:ListItem>
                                                     <asp:ListItem Text="2021" Value="12" ></asp:ListItem>                           
                                                </asp:DropDownList>
                                                </td>
                                                <td> &nbsp;</td>
                                        </tr>
                                    <tr>
                                      <td align="right" class="Texto">
                                                Tipo Almacen:
                                                </td>
                                                
                                                <td >
                                                <asp:DropDownList ID="cbotipoalmacen" runat="server" Enabled="true" >
                                                </asp:DropDownList>
                                                </td>
                                    
                                    </tr>
                                    <tr>
                                    <td></td>
                                    <td> 
                                                <asp:Button ID="add" Text="Agregar Almacen" runat="server" OnClientClick=" return ( Almacen_add() ); " ToolTip="Replicar almacenes"  />
                                                </td>
                                    </tr>
                                    <tr>
                                    <td></td>
                                    </tr>
                                        <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                        <asp:GridView ID="GV_TipoAlmacen" runat="server" AutoGenerateColumns="False" 
                                                Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" 
                                                BorderWidth="1px" CellPadding="3" >
                                <Columns>
                                    <asp:TemplateField HeaderText="Opciones">
                                        <ItemTemplate>
                                                        <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                        <asp:LinkButton ID="lkbAlmacenRemove" runat="server" OnClick="lkbAlmacenRemove_Click">Quitar</asp:LinkButton>
                                                        <asp:HiddenField ID="hddIdTipoAlmacen" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"IdtipoAlmacen") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField HeaderText="Tipo Almacen" DataField="Tipoalm_Nombre" HeaderStyle-Height="25px"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                    </asp:BoundField>--%>
                                   <asp:TemplateField HeaderText="Tipo Almacen">
                                   <ItemTemplate>
                                         <asp:Label ID="lbltipoalmacen"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Tipoalm_Nombre")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                    
                                </Columns>
                                <RowStyle CssClass="GrillaRow" ForeColor="#000066" />
                                <FooterStyle CssClass="GrillaFooter" BackColor="White" ForeColor="#000066" />
                                <PagerStyle CssClass="GrillaPager" BackColor="White" ForeColor="#000066" 
                                                HorizontalAlign="Left" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" BackColor="#669999" Font-Bold="True" 
                                                ForeColor="White" />
                                <HeaderStyle CssClass="GrillaHeader" BackColor="#006699" Font-Bold="True" 
                                                ForeColor="White" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            </asp:GridView>
                                        </td>
                                          <td>&nbsp;
                                                </td>
                                         </tr>                            
                                </table>
                                <table>
                                <tr>
                                <td></td>
                                <td>
                                <asp:Label  ID="lblmensaje" runat ="server" Text="" style="color:Red;font-family:Comic Sans MS ;font-size:small   "></asp:Label>
                                </td>
                                </tr>
                                <tr>
                                <td><%--OnSelectedIndexChanged="OnSelectedIndexChanged"--%></td>
                                <td>  
                                <div id="divAreaImprimir">                                                                        
                                <asp:GridView ID="GV_Inventrio" runat="server" AutoGenerateColumns="False" 
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None" 
                                        BorderWidth="1px" CellPadding="3" GridLines="Vertical"  ShowFooter="true" >
                                <Columns>
                                 <%--   <asp:ButtonField Text = "Seleccionar" CommandName = "Select"  />--%>
                                   <asp:TemplateField HeaderText="Tienda">
                                   <ItemTemplate>
                                         <asp:Label ID="lbltienda"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"tienda")%>'></asp:Label>
                                   </ItemTemplate>
                                      <FooterTemplate>
                                    <asp:Label ID="lblTotales" runat="server" Text="Totales :"></asp:Label>
                                    </FooterTemplate>
                                   </asp:TemplateField>
                                                        
                                    <asp:TemplateField HeaderText="Stock Inicial">
                                   <ItemTemplate>
                                         <asp:Label ID="lblstockini"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"stockinicial","{0:c}")%>'></asp:Label>
                                   </ItemTemplate>
                                  <FooterTemplate>
                                    <asp:Label ID="lblTotal" runat="server" style="background-color:Yellow"></asp:Label>
                                    </FooterTemplate>
                                     
                                   </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Stock Final">
                                   <ItemTemplate>
                                         <asp:Label ID="lblstockfinal"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"stockfinal","{0:c}")%>'></asp:Label>
                                   </ItemTemplate>
                                      <FooterTemplate>
                                    <asp:Label ID="lblTotal1" runat="server" style="background-color:Yellow"></asp:Label>
                                    </FooterTemplate>
                                   </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Compras">
                                   <ItemTemplate>
                                         <asp:Label ID="lblcompras"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"compras","{0:c}")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Transferencia">
                                   <ItemTemplate>
                                         <asp:Label ID="lbltransferencia"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"transferencias","{0:c}")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Transito">
                                   <ItemTemplate>
                                         <asp:Label ID="lbltransito"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"transito","{0:c}")%>'></asp:Label>
                                   </ItemTemplate>
                                   </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Total Compras">
                                   <ItemTemplate>
                                         <asp:Label ID="lbltotalcompras"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"totalcompras","{0:c}")%>'></asp:Label>
                                   </ItemTemplate>
                                   
                                      <FooterTemplate>
                                    <asp:Label ID="lblTotal2" runat="server" style="background-color:Yellow"></asp:Label>
                                    </FooterTemplate>
                                    
                                   </asp:TemplateField>
                                   
                                        <asp:TemplateField HeaderText="Iri">
                                   <ItemTemplate>
                                         <asp:Label ID="lbliri"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Iri")%>'></asp:Label>
                                   </ItemTemplate>
                                   
                                   </asp:TemplateField>
                                   
                                        <asp:TemplateField HeaderText="Ndi">
                                   <ItemTemplate>
                                         <asp:Label ID="lblndi"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Ndi")%>'></asp:Label>
                                   </ItemTemplate>
                                     
                                   </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Escala">
                                   <ItemTemplate>
                                         <asp:Label ID="lblescala"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"Escala")%>'></asp:Label>
                                   </ItemTemplate>
                                     
                                   </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="Objetivo">
                                   <ItemTemplate>
                                         <asp:Label ID="lblobjetivo"  runat="server"
                                           Text='<%#DataBinder.Eval(Container.DataItem,"ObjetivoxTienda")%>'></asp:Label>
                                   </ItemTemplate>
                                     
                                   </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="GrillaRow" BackColor="#EEEEEE" ForeColor="Black" />
                                <FooterStyle CssClass="GrillaFooter" BackColor="#CCCCCC" ForeColor="Black" />
                                <PagerStyle CssClass="GrillaPager" BackColor="#999999" ForeColor="Black" 
                                        HorizontalAlign="Center" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" BackColor="#008A8C" Font-Bold="True" 
                                        ForeColor="White" />
                                <HeaderStyle CssClass="GrillaHeader" BackColor="#000084" Font-Bold="True" 
                                        ForeColor="White" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" BackColor="#DCDCDC" />
                            </asp:GridView>                            
                                </div>
                                </td>
                                </tr>
                                </table>
                                <table>
                                 <tr>
                                <td>
                                 <asp:Label ID="valoriri" runat="server" Text="Total Compañia IRI :" style="color:#000066;font-family:Comic Sans MS ;font-size:small;font-weight:bold " ></asp:Label>
                                 </td>
                                <td>
                              <asp:Label ID="textoiri" runat="server" Text=" " style="font-family:Comic Sans MS ;font-size:small ; background-color:Yellow" ></asp:Label>
                                </td>
                                </tr>
                                <tr> 
                                <td>
                                <asp:Label ID="valorndi" runat="server" Text="Total Compañia NDI :" style="color:#000066;font-family:Comic Sans MS ;font-size:small;font-weight:bold" ></asp:Label>
                                </td>
                                <td>
                                <asp:Label ID="textondi" runat="server" Text=" " style="font-family:Comic Sans MS ;font-size:small; background-color:Yellow" ></asp:Label>
                                </td>
                                </tr>
                                <tr>
                                <td></td>
                                <td>
                                  <asp:Label ID="idtexto" runat="server"  Text=" " style="color:Red;font-family:Berlin Sans FB Demi;font-size:medium   " >
                                </asp:Label>
                                </td>
                                </tr>
                                <tr>
                                <td></td> 
                                <td></td>
                                </tr>
                                <tr> 
                                <td></td><td></td>
                                </tr>
                                <tr>
                                <td></td>
                                
                                <td><asp:GridView ID="gvEscala" runat="server" CellPadding="4" ForeColor="#333333" 
                                        GridLines="None"  Visible="false" >
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                             
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                                </td>
                                </tr>
                                </table>
             </asp:Panel>        
        </td>                
        </tr>
              
</table>


 <script language="javascript" type="text/javascript">
function Almacen_add() {

            var cbotipoalmacen = document.getElementById('<%=cbotipoalmacen.ClientID %>');
            var GV_TipoAlmacen = document.getElementById('<%=GV_TipoAlmacen.ClientID %>');

            if (GV_TipoAlmacen != null) {

                for (var i = 1; i < GV_TipoAlmacen.rows.length; i++) {
                    var rowElem = GV_TipoAlmacen.rows[i];

                    if (rowElem.cells[0].children[2].value == cbotipoalmacen.value) {

                        alert('LA LISTA CONTIENE EL TIPO ALMACEN [ ' + cbotipoalmacen.options[cbotipoalmacen.selectedIndex].text + ' ] SELECCIONADO. \n NO SE PERMITE LA OPERACIÓN.');
                        return false;
                    }

                }

            }

            return true;
        }
  </script>

</asp:Content>
