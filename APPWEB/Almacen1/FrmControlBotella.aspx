<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmControlBotella.aspx.vb" Inherits="APPWEB.FrmControlBotella" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td class="TituloCelda">
                CONTROL DE BOTELLAS</td>
        </tr>
        <tr>
        <td>
        <table>
        <tr>
        <td  class="Texto" style="font-weight:bold" >
            Empresa:
        </td>
        <td>
            <asp:DropDownList ID="cboEmpresa" runat="server" Width="300px"  AutoPostBack="true" >
            </asp:DropDownList>
        </td>
        <td class="Texto" style="font-weight:bold" >Tienda:</td>
        <td>
            <asp:DropDownList ID="cboTienda" runat="server" Width="300px">
            </asp:DropDownList>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
            <td>
                <cc1:TabContainer ID="TabContainer_Botella" runat="server" ActiveTabIndex="1" >
                    <cc1:TabPanel runat="server" HeaderText="NUEVO INGRESO DE BOTELLAS" ID="TabPanel_1">
                    <ContentTemplate><table width="100%"><tr><td><table><tr><td>
                    
                    <asp:Button ID="btnGuardar_1" OnClientClick=" return(  valOnClick_btnGuardar_1() ); " runat="server" Text="Guardar" Width="70px" ToolTip="Guardar" /></td><td></td></tr></table></td></tr><tr><td><table><tr><td style="font-weight:bold;text-align:right" >Propietario:</td><td><table><tr><td><asp:TextBox ID="txtPropietario_1"  style="background-color:#FFFFCC" Width="350px" Font-Bold="True" 
                                    ReadOnly="True"  runat="server"></asp:TextBox></td><td>
                                    <asp:Button ID="btnBuscarPropietario_1" OnClientClick=" return( valOnClick_btnBuscarPropietario_1()  ); " runat="server" Text="Buscar" Width="70px" ToolTip="Buscar" /></td></tr></table></td></tr><tr><td style="font-weight:bold;text-align:right" >Documento:</td><td><table><tr><td>
                                    
                                    <asp:DropDownList ID="cboTipoDocumento_1" runat="server">
                                    <asp:ListItem Value="0" >---</asp:ListItem>
                                    <asp:ListItem Value="6" >GU�A REMISI�N</asp:ListItem>
                                    </asp:DropDownList>
                                    
                                    
                                    </td><td style="font-weight:bold;text-align:right" >Nro.:</td><td><asp:TextBox ID="txtNroDocumento_1" Width="120px" Font-Bold="True" 
                                      runat="server"></asp:TextBox></td></tr></table></td></tr><tr><td  style="font-weight:bold;text-align:right" >Fecha:</td><td><table><tr><td>
                            <asp:TextBox ID="txtFechaIngreso_1" runat="server" Font-Bold="True" 
                                onblur="return(  valFecha(this) );" Width="87px"></asp:TextBox><cc1:MaskedEditExtender ID="txtFechaIngreso_1_MaskedEditExtender" runat="server" 
                                ClearMaskOnLostFocus="False" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaIngreso_1"></cc1:MaskedEditExtender><cc1:CalendarExtender ID="txtFechaIngreso_1_CalendarExtender" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaIngreso_1" ></cc1:CalendarExtender></td><td style="font-weight:bold;text-align:right" >Vigencia: </td><td>
                                
                                <asp:TextBox ID="txtVigencia_1" runat="server" Width="50px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); "  onBlur=" return( valBlur(event)  ); " onFocus=" return( aceptarFoco(this)  ); " Text="0" ></asp:TextBox>
                                
                                </td><td style="font-weight:bold">d�as</td></tr></table></td></tr></table></td></tr><tr><td><asp:Button ID="btnAgregar_1" runat="server" Text="Agregar" Width="70px" ToolTip="Agregar Envase" /></td></tr><tr><td>
                                <asp:GridView ID="GV_Envase_1" runat="server" AutoGenerateColumns="False" 
                            Width="100%"><HeaderStyle CssClass="GrillaHeader" /><AlternatingRowStyle CssClass="GrillaRowAlternating" /><Columns><asp:CommandField SelectText="Quitar" ShowSelectButton="True" ><HeaderStyle Height="25px" HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></asp:CommandField><asp:TemplateField HeaderText="Nro. Botella"><ItemTemplate><table><tr><td>
                                        <asp:TextBox ID="txtNroBotella" runat="server" 
                                            onFocus=" return( aceptarFoco(this)  ); " TabIndex="200" 
                                            Text='<%#DataBinder.Eval(Container.DataItem,"NroBotella")%>' Width="100px"></asp:TextBox>
                                        </td><td></td></tr></table></ItemTemplate><HeaderStyle Height="25px" HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></asp:TemplateField><asp:TemplateField HeaderText="Capacidad"><ItemTemplate><table><tr><td>
                                        <asp:TextBox ID="txtCapacidad" runat="server" onBlur=" return( valBlur(event)  ); " 
                                            onFocus=" return( aceptarFoco(this)  ); " 
                                            onKeyPress=" return(  validarNumeroPuntoPositivo('event')  ); " TabIndex="210" 
                                            Text='<%#DataBinder.Eval(Container.DataItem,"Capacidad","{0:F3}")%>' 
                                            Width="60px"></asp:TextBox>
                                        </td><td>
                                            <asp:DropDownList ID="cboUnidadMedida" runat="server" 
                                                DataSource='<%#DataBinder.Eval(Container.DataItem,"ListaMagnitudUnidad")%>' 
                                                DataTextField="NombreCortoUM" DataValueField="idUnidadMedida" Font-Bold="true" 
                                                TabIndex="220" Width="60px">
                                            </asp:DropDownList>
                                        </td></tr></table></ItemTemplate><HeaderStyle Height="25px" HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Contenido">
                                                <ItemTemplate>
                                                <table><tr><td>
                                                    <asp:Label ID="lblContenido" runat="server" Font-Bold="true" 
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"Contenido")%>'></asp:Label>
                                                    </td><td>
                                                        <asp:ImageButton ID="btnBuscarContenido" runat="server" 
                                                            ImageUrl="~/Caja/iconos/ok.gif" 
                                                            OnClientClick=" return( valOnClick_btnBuscarContenido_1(this) ); " 
                                                            ToolTip="Buscar contenido." />
                                                    </td><td></td></tr></table></ItemTemplate><HeaderStyle Height="25px" HorizontalAlign="Center" /><ItemStyle HorizontalAlign="Center" /></asp:TemplateField>
                                                
                                                
                                                <asp:TemplateField HeaderText="Obs.">
                                                <ItemTemplate>
                                                <table>
                                                <tr>
                                                <td>
                                                
                                                    <asp:TextBox ID="txtObservacion" TabIndex="240" TextMode="MultiLine" Width="250px" Height="75px" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Observacion")%>'  ></asp:TextBox>
                                                
                                                </td>
                                                </tr>
                                                </table>
                                                </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                
                                                </Columns><EditRowStyle CssClass="GrillaEditRow" /><FooterStyle CssClass="GrillaFooter" /><PagerStyle CssClass="GrillaPager" /><RowStyle CssClass="GrillaRow" /><SelectedRowStyle CssClass="GrillaSelectedRow" /></asp:GridView></td></tr>
                        <tr>
                            <td>
                            
                            
                            
                                <asp:HiddenField ID="hddIdPersona_1" runat="server" />
                                <asp:HiddenField ID="hddIndexGrilla_1" runat="server" />
                            
                            
                            
                            </td>
                        </tr>
                        </table>
                    </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel  runat="server" HeaderText="INGRESO POR DEVOLUCI�N" ID="TabPanel_2" >
                    <ContentTemplate>
                    
                    <table width="100%">
                    <tr>
                    <td>
                    
                    <table>
                    <tr>
                    <td>
                        <asp:Button ID="btnGuardar_2" runat="server" Text="Guardar" Width="70px" ToolTip="Guardar" />
                    </td>
                    </tr>
                    </table>
                    
                    </td>
                    </tr>
                    <tr>
                    <td>
                    
                           <fieldset class="FieldSetPanel">
                                        <legend>Datos Adicionales</legend>
                                        <table>
                                        <tr>
                                        <td  style="text-align:right;font-weight:bold;" >
                                            Documento:
                                        </td>
                                        <td>
                                        <table>
                                        <tr>
                                        <td>
                                            <asp:DropDownList ID="cboTipoDocumento_2" runat="server">
                                            <asp:ListItem Selected="True" Value="0" >-----</asp:ListItem>
                                            <asp:ListItem  Value="6" >Gu�a de Remisi�n</asp:ListItem>                                            
                                            </asp:DropDownList>
                                        </td>
                                        <td style="text-align:right;font-weight:bold;" >Nro.:</td>
                                        <td>
                                            <asp:TextBox ID="txtNroDocumento_2" runat="server"></asp:TextBox> 
                                        </td>
                                        </tr>
                                        </table>
                                        </td>                                        
                                        </tr>
                                        <tr>
                                        <td style="text-align:right;font-weight:bold;" >Fecha:</td>
                                        <td>
                                                <table>
                                                <tr>
                                                <td>
                                                <asp:TextBox ID="txtFechaMov_2" runat="server" Font-Bold="True" 
                                onblur="return(  valFecha(this) );" Width="87px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaMov_2" runat="server" 
                                ClearMaskOnLostFocus="False" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaMov_2">
                                </cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaMov_2" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaMov_2" ></cc1:CalendarExtender>
                                                </td>
                                                <td></td>
                                                </tr>
                                                </table>
                                        </td>
                                        </tr>
                                        </table>
                            </fieldset>
                    
                    </td>
                    </tr>
                    <tr>
                    <td>
                    
                    
                        <fieldset class="FieldSetPanel">
                                        <legend>Filtro</legend>
                                        <table>
                                        <tr>
                                        <td style="font-weight:bold; text-align: right;" >Cliente:</td>
                                        <td>
                                        <table>
                                        <tr>
                                        <td>
                                        <asp:TextBox ID="txtCliente_2" runat="server" Font-Bold="True" ReadOnly="True" 
                                                 style="background-color:#FFFFCC" Width="350px"></asp:TextBox>
                                        </td>
                                        <td>
                                        <asp:Button ID="btnBuscarCliente_2" runat="server" 
                                                 OnClientClick=" return(  valOnClick_btnBuscarCliente_2()  ); " Text="Buscar" 
                                                 ToolTip="Buscar" Width="70px" />
                                        </td>
                                        <td></td>
                                        </tr>
                                        </table>
                                        </td>
                                        </tr>
                                        <tr>
                                        <td  style="font-weight:bold; text-align: right;" >
                                        Nro. Botella:
                                        </td>
                                        <td>
                                        <asp:TextBox ID="txtNroBotella_2" runat="server"></asp:TextBox>
                                        </td>
                                        </tr>
                                        </table>
                    </fieldset>
                    </td>
                    </tr>
                    <tr>
                    <td>
                            
                                <table>
                                                 <tr>
                                                     <td>
                                                         <asp:Button ID="btnBuscarEnvase_2" runat="server" Text="Buscar" 
                                                             ToolTip="Buscar" Width="70px" />
                                                     </td>
                                                     <td>
                                                         <asp:Button ID="btnLimpiar_2" runat="server" 
                                                             OnClientClick=" return( valOnClick_btnLimpiar_2()  ); " Text="Limpiar" 
                                                             ToolTip="Limpiar" Width="70px" />
                                                     </td>
                                                 </tr>
                                             </table>
                            
                            
                    </td>
                    </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="GV_Botella_2" runat="server" AutoGenerateColumns="False" 
                                    Width="100%">
                                    <HeaderStyle CssClass="GrillaHeader" />
                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Nro. Botella">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblNroBotella" runat="server" 
                                                                Text='<%#DataBinder.Eval(Container.DataItem,"NroBotella")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:HiddenField ID="hddIdBotella" runat="server" 
                                                                Value='<%#DataBinder.Eval(Container.DataItem,"IdBotella")%>' />
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FechaSalida" DataFormatString="{0:dd/MM/yyyy}" 
                                            HeaderText="Fec. Salida">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Vigencia" DataFormatString="{0:F0}" 
                                            HeaderText="Vigencia (D�as)">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NroDiasTranscurridos" DataFormatString="{0:F0}" 
                                            HeaderText="Transcurrido">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NroDiasMora_Final" DataFormatString="{0:F0}" 
                                            HeaderText="Mora">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Ubicacion" HeaderText="Cliente">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Propietario" HeaderText="Propietario">
                                            <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chb_Estado" runat="server" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <HeaderStyle BackColor="Yellow" Height="25px" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EditRowStyle CssClass="GrillaEditRow" />
                                    <FooterStyle CssClass="GrillaFooter" />
                                    <PagerStyle CssClass="GrillaPager" />
                                    <RowStyle CssClass="GrillaRow" />
                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hddIdCliente_2" runat="server" />
                            </td>
                        </tr>
                    </table>
                    
                    </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="SALIDA POR PR�STAMO" ID="TabPanel_3" >
                    <ContentTemplate>
                    <table  width="100%" >
                    <tr>
                    <td>
                    <table>
                    <tr>
                    <td>
                        <asp:Button ID="btnGuardar_3" runat="server" Text="Guardar" Width="70px" ToolTip="Guardar" OnClientClick=" return(  valOnClick_btnGuardar_3() ); " />
                    </td>
                    <td></td>
                    <td></td>
                    </tr>
                    </table>
                    </td>
                    </tr>                    
                    <tr>
                    <td>
                    <fieldset class="FieldSetPanel" >
                    <legend>Datos</legend>
                    <table>
                    <tr>
                    <td style="font-weight:bold;text-align:right">
                        Destinatario:</td>
                    <td >
                    <table>
                    <tr>
                    <td>
                            <asp:TextBox ID="txtDestinatario_3" runat="server" Font-Bold="True" ReadOnly="True" 
                            style="background-color:#FFFFCC" Width="350px" ></asp:TextBox>                            
                    </td>
                    <td>
                            <asp:Button ID="btnBuscarPersona_3" runat="server" OnClientClick=" return(  valOnClick_btnBuscarPersona_3() ); " Text="Buscar" Width="70px" ToolTip="Buscar" />
                    </td>
                    </tr>
                    </table>
                        </td>
                    </tr>
                    <tr>
                    <td style="font-weight:bold;text-align:right">
                        Documento:
                    </td>
                    <td >
                        <table>
                        <tr>
                        <td>
                            <asp:DropDownList ID="cboTipoDocumento_3" runat="server">
                            <asp:ListItem Value="0"  Selected="True" >-----</asp:ListItem>
                            <asp:ListItem Value="6" >Gu�a de Remisi�n</asp:ListItem>
                            <asp:ListItem Value="1" >Factura</asp:ListItem>
                            <asp:ListItem Value="3" >Boleta</asp:ListItem>
                            <asp:ListItem Value="1" >Liquidaci�n</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="font-weight:bold;text-align:right" >Nro.:</td>
                        <td>
                            <asp:TextBox ID="txtNroDocumento_3" runat="server"></asp:TextBox>
                        </td>
                        </tr>
                        </table>
                    </td>
                    </tr>
                    <tr>
                    <td style="font-weight:bold;text-align:right">
                        Fecha:</td>
                    <td >
                        <table>
                        <tr>
                        <td>
                        
                        <asp:TextBox ID="txtFechaMov_3" runat="server" Font-Bold="True" 
                                onblur="return(  valFecha(this) );" Width="87px"></asp:TextBox>
                                <cc1:MaskedEditExtender ID="MaskedEditExtender_txtFechaMov_3" runat="server" 
                                ClearMaskOnLostFocus="False" CultureAMPMPlaceholder="" 
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                Mask="99/99/9999" TargetControlID="txtFechaMov_3"></cc1:MaskedEditExtender>
                                <cc1:CalendarExtender ID="CalendarExtender_txtFechaMov_3" runat="server" 
                                Enabled="True" Format="dd/MM/yyyy" TargetControlID="txtFechaMov_3" >
                                </cc1:CalendarExtender>
                        
                        </td>
                        <td style="font-weight:bold;text-align:right" >Vigencia:</td>
                        <td>
                            <asp:TextBox ID="txtVigencia_3" runat="server" Width="50px" onKeyPress=" return( validarNumeroPuntoPositivo('event') ); "  onBlur=" return( valBlur(event)  ); " onFocus=" return( aceptarFoco(this)  ); " Text="0" ></asp:TextBox>
                        </td>
                        <td></td>
                        </tr>
                        </table>  
                        </td>
                    </tr>
                    </table>
                    </fieldset>
                    </td>
                    </tr>                    
                    
                    <tr>
                    <td>
                    <fieldset class="FieldSetPanel" >
                    <legend>Filtro</legend>
                        <table>
                                       <tr>
                                           <td style="text-align:right;font-weight:bold">
                                               Contenido:</td>
                                           <td>
                                               <asp:DropDownList ID="cboContenido_3" runat="server"  DataTextField="Producto" DataValueField="IdContenido" >
                                               </asp:DropDownList>
                                           </td>
                                           <td>
                                               <asp:Button ID="btnBuscar_3" runat="server" Text="Buscar" ToolTip="Buscar" 
                                                   Width="70px" />
                                           </td>
                                       </tr>
                                   </table>
                    </fieldset>
                    </td>
                    </tr>
                    
                    
                    <tr>
                    <td>
                               
                        <asp:GridView ID="GV_Botella_3" runat="server" AutoGenerateColumns="False" 
                            Width="100%">
                            <HeaderStyle CssClass="GrillaHeader" />
                            <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                            <Columns>
                                <asp:TemplateField HeaderText="Nro. Botella">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblNroBotella" runat="server" 
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"NroBotella")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:HiddenField ID="hddIdBotella" runat="server" 
                                                        Value='<%#DataBinder.Eval(Container.DataItem,"IdBotella")%>' />
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Contenido" 
                                    HeaderText="Contenido">
                                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                
                                <asp:TemplateField HeaderText="Capacidad" >
                                <ItemTemplate>
                                <table>
                                <tr>
                                <td>
                                    <asp:Label ID="lblCapacidad" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Capacidad","{0:F3}")%>' ></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblUnidadMedida" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UnidadMedida_Capacidad")%>' ></asp:Label>
                                </td>
                                <td></td>
                                </tr>
                                </table>
                                </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                
                                <asp:BoundField DataField="Propietario" 
                                    HeaderText="Propietario">
                                    <HeaderStyle Height="25px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                               
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chb_Estado" runat="server" />
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <HeaderStyle BackColor="Yellow" Height="25px" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle CssClass="GrillaEditRow" />
                            <FooterStyle CssClass="GrillaFooter" />
                            <PagerStyle CssClass="GrillaPager" />
                            <RowStyle CssClass="GrillaRow" />
                            <SelectedRowStyle CssClass="GrillaSelectedRow" />
                        </asp:GridView>
                               
                    </td>
                    </tr>                    
                        <tr>
                            <td>
                                <asp:HiddenField ID="hddIdDestinatario_3"  runat="server" />
                            </td>
                        </tr>
                    </table>
                    </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddOpcion_BuscarPersona" runat="server" />    
             </td>
        </tr>
    </table>
    <div  id="capaPersona"         
        style="border: 3px solid blue; padding: 8px; width:900px; height:auto; position:absolute; top:231px; left:21px; background-color:white; z-index:2; display :none; ">                                
        
        <asp:UpdatePanel ID="UpdatePanel_capaPersona" runat="server" UpdateMode="Conditional" >
        <ContentTemplate>
        <table style="width: 100%;">                                    
                                <tr><td align="right">
                                       <asp:ImageButton ID="btnCerrar_Capa" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar.GIF"                                                
                                        OnClientClick="return(offCapa('capaPersona'));" />   
                                </td>
                                </tr>   
                                <tr>
                                <td>
                                        <asp:Panel ID="pnlBusquedaPersona" runat="server">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table width="100%">                                            
                                            <tr>
                                                <td>
                                                    <table width="100%">                                              
                                                        <tr>
                                                            <td>
                                                                <table >
                                                                <tr>
                                                                <td colspan="5">
                                                                  <asp:RadioButtonList ID="rdbTipoPersona" runat="server" CssClass="LabelTab" RepeatDirection="Horizontal" AutoPostBack="false">
                                                                <asp:ListItem  Value="N">Natural</asp:ListItem>
                                                                <asp:ListItem  Selected="True"  Value="J">Juridica</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                                </td>
                                                                </tr>
                                                                    <tr>
                                                                        <td class="Texto">
                                                                            Razon Social / Nombres:                                                                            
                                                                        </td>
                                                                        <td colspan="4" >
                                                                        <asp:TextBox ID="tbRazonApe" onKeyPress="return(validarCajaBusqueda(event));" runat="server"
                                                                                Width="450px"></asp:TextBox>
                                                                        </td>                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="Texto">                                                                                                                                                    
                                                                        D.N.I.:
                                                                        </td>
                                                                        <td >                                                                            
                                                                            <asp:TextBox ID="tbbuscarDni" onKeyPress="return(validarCajaBusquedaNumero(event));" onFocus="javascript:validarbusqueda();"
                                                                                MaxLength="8" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td class="Texto" >
                                                                          Ruc:                                                                            
                                                                        </td>
                                                                        <td>
                                                                        <asp:TextBox ID="tbbuscarRuc" runat="server" onKeyPress="return(validarCajaBusquedaNumero(event));"
                                                                                MaxLength="11"></asp:TextBox>                                                                            
                                                                        </td>
                                                                        <td>
                                                                        <asp:ImageButton ID="btBuscarPersonaGrilla" runat="server" CausesValidation="false" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                                                onmouseout="this.src='/Imagenes/Buscar_b.JPG';" onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="gvBuscar" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                    PageSize="20" Width="100%">
                                                                    <Columns>
                                                                        <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                                                        <asp:BoundField DataField="idpersona" HeaderText="Cod." NullDisplayText="0" />
                                                                        <asp:BoundField DataField="Nombre" HeaderText="Nombre o Razon Social" NullDisplayText="---">
                                                                            <ItemStyle HorizontalAlign="Left" Width="500px" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="RUC" HeaderText="RUC" NullDisplayText="---">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DNI" HeaderText="DNI" NullDisplayText="---">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>                                                                            
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="GrillaHeader" />
                                                                    <FooterStyle CssClass="GrillaFooter" />
                                                                    <PagerStyle CssClass="GrillaPager" />
                                                                    <RowStyle CssClass="GrillaRow" />
                                                                    <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                    <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                    <EditRowStyle CssClass="GrillaEditRow" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btAnterior" runat="server" Font-Bold="true" Width="50px" Text="<"
                                                                    ToolTip="P�gina Anterior" OnClientClick="return(valNavegacion('0'));" />
                                                                <asp:Button ID="btSiguiente" runat="server" Font-Bold="true" Width="50px" Text=">"
                                                                    ToolTip="P�gina Posterior" OnClientClick="return(valNavegacion('1'));" />
                                                                <asp:TextBox ID="tbPageIndex" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                                                    runat="server"></asp:TextBox><asp:Button ID="btIr" runat="server" Font-Bold="true"
                                                                        Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacion('2'));" />
                                                                <asp:TextBox ID="tbPageIndexGO" Width="50px" CssClass="TextBox_ReadOnly" runat="server"
                                                                    onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>                                
                            </table>
                        </asp:Panel>
                                </td>
                                </tr>                                                                 
                 </table>      
        </ContentTemplate>
        <Triggers>
        <asp:PostBackTrigger ControlID="gvBuscar" />
        </Triggers>
        </asp:UpdatePanel>
        
                        
                 
                 
                        </div>       
      <div  id="capaBuscarProducto_AddProd"         
        style="border: 3px solid blue; padding: 8px; width:900px; height:auto; position:absolute; top:70px; left:32px; background-color:white; z-index:2; display :block;">                        
          <asp:UpdatePanel ID="UpdatePanel_capaBuscarProducto_AddProd" runat="server"  UpdateMode="Conditional" >
          <ContentTemplate>
          <table style="width: 100%;">                                    
                                <tr><td align="right">                                                                    
                                        <asp:ImageButton ID="btnCerrar_capaAddProd" runat="server"  ToolTip="Cerrar"
                                                ImageUrl="~/Imagenes/Cerrar.GIF" OnClientClick="return(  offCapa('capaBuscarProducto_AddProd')   );" />   
                                        
                                </td>
                                </tr>
                                    <tr>
                                        <td>
                                            <table >
                                            <tr>
                                            <td class ="Texto">
                                            TipoExistencia:
                                            </td>
                                            
                                            <td>
                                                <asp:DropDownList ID="CboTipoExistencia" runat="server" AutoPostBack ="true" >
                                                </asp:DropDownList>
                                            </td>
                                            </tr>
                                                <tr>
                                                    <td align="right">
                                                    <asp:Label ID="Label17" runat="server" CssClass="Label" Text="L�nea:"></asp:Label>
                                                    </td>
                                                    <td>
                                                    <asp:DropDownList TabIndex="201" ID="cmbLinea_AddProd" runat="server" AutoPostBack="True" DataTextField="Descripcion" DataValueField="Id">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="Label16" runat="server" CssClass="Label" Text="Sub L�nea:"></asp:Label>
                                                    <asp:DropDownList TabIndex="202" ID="cmbSubLinea_AddProd" runat="server" DataTextField="Nombre" AutoPostBack="true" DataValueField="Id" >
                                                    </asp:DropDownList>                                                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right">
                                                    <asp:Label ID="Label19" runat="server" CssClass="Label" Text="Descripci�n:"></asp:Label>
                                                    </td>
                                                    <td>
                                                    
                                                    <table>
                                                    <tr>
                                                    <td><asp:TextBox TabIndex="204" ID="txtDescripcionProd_AddProd" runat="server" Width="300px"  onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );" ></asp:TextBox>                                                    </td>
                                                    <td class="Texto" style="text-align:right">C�d.:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtCodigoProducto" Font-Bold="true"  Width="100px" runat="server" onFocus="return( aceptarFoco(this)  );" onKeypress="return( valKeyPressDescripcionProd(event) );" TabIndex="205" ></asp:TextBox>
                                                    </td>
                                                    <td>    <asp:ImageButton ID="btnBuscarGrilla_AddProd" runat="server" 
                                                        ImageUrl="~/Imagenes/Buscar_b.JPG" TabIndex="207"
                                                        onmouseout="this.src='/Imagenes/Buscar_b.JPG';" 
                                                        onmouseover="this.src='/Imagenes/Buscar_A.JPG';" />                                                    </td>
                                                    <td>
                                                    <asp:ImageButton ID="btnLimpiar_AddProd" runat="server" ImageUrl="~/Imagenes/Limpiar_B.JPG" 
                                                        onmouseout="this.src='/Imagenes/Limpiar_B.JPG';" onmouseover="this.src='/Imagenes/Limpiar_A.JPG';"
                                                        OnClientClick="return(limpiarBuscarProducto());" TabIndex="209" />
                                                    </td>
                                                    </tr>
                                                    </table>
                                                    
                                                    
                                                    
                                                        
                                                    </td>
                                                </tr>
                                                </table>
                                        </td>
                                    </tr>
                                
                                <tr>
                                <td>
                                
                                  <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender15" runat="server" 
                                                TargetControlID="Panel_BusqAvanzadoProd" 
                                                CollapsedSize="0" 
                                                ExpandedSize="0" 
                                                Collapsed="true"
                                                ExpandControlID="Image21_11"
                                                CollapseControlID="Image21_11" 
                                                TextLabelID="Label21_11" 
                                                ImageControlID="Image21_11"
                                                CollapsedImage="~/Imagenes/Mas_B.JPG"
                                              ExpandedImage="~/Imagenes/Menos_B.JPG"
                                                CollapsedText="B�squeda Avanzada" 
                                                ExpandedText="B�squeda Avanzada"                               
                                                ExpandDirection="Vertical" SuppressPostBack="true">
                                                </cc1:CollapsiblePanelExtender>                                                                           
                                            <asp:Image ID="Image21_11" runat="server" Height="16px" />                                                             
                                            <asp:Label ID="Label21_11" runat="server" Text="B�squeda Avanzada" CssClass="Label"></asp:Label>
                                <asp:Panel ID="Panel_BusqAvanzadoProd" runat="server">
                                
                                 <table width="100">
                                <tr>
                                <td>
                                <table>
                                    <tr>
                                <td class="Texto" style="font-weight:bold">Atributo:</td>
                                <td>
                                    <asp:DropDownList ID="cboTipoTabla" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Button ID="btnAddTipoTabla" runat="server" Text="Agregar" Width="80px" OnClientClick="return( valAddTabla() );" /></td>
                                <td>
                                    <asp:Button ID="btnLimpiar_BA" runat="server" Text="Limpiar" Width="80px" />
                                </td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                <tr>
                                <td>
                                       <asp:GridView ID="GV_FiltroTipoTabla" runat="server" AutoGenerateColumns="False" Width="650px">
                                                                            <Columns>
                                                                                <asp:CommandField SelectText="Quitar" ShowSelectButton="True" HeaderStyle-Width="75px" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="25px" />
                                                                                <asp:TemplateField HeaderText="Atributo" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblNomTipoTabla" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Nombre")%>'></asp:Label> 
                                                                                        <asp:HiddenField ID="hddIdTipoTabla" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"IdTipoTabla")%>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Valor" HeaderStyle-Height="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>                                                                                        
                                                                                        <asp:DropDownList ID="cboTTV" runat="server" DataValueField="IdTipoTablaValor" DataTextField="Nombre"
                                                                                        DataSource='<%# DataBinder.Eval(Container.DataItem,"objTipoTabla") %>' Width="200px"></asp:DropDownList>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        <RowStyle CssClass="GrillaRow" />
                                                                        <FooterStyle CssClass="GrillaFooter" />
                                                                        <PagerStyle CssClass="GrillaPager" />
                                                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                                                        <HeaderStyle CssClass="GrillaHeader" />
                                                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                                                        </asp:GridView>       
                                </td>
                                </tr>                            
                                </table>
                                
                                </asp:Panel>
                                
                                
                                </td>                                                                
                                </tr>                                
                                <tr>
                                <td>
                                    
                                    <asp:GridView ID="DGV_AddProd" runat="server"
                                        AutoGenerateColumns="False" Width="100%">
                                        <Columns>                                        
                                        
                                        <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" HeaderStyle-Width="85px" ItemStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" ItemStyle-Height="25px" />
                                        
                                        <asp:TemplateField HeaderText="C�digo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate >
                                        <table>
                                        <tr>
                                        <td>
                                            <asp:HiddenField ID="hddIdProducto" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"Id")%>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCodigo" Font-Bold="true" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Codigo")%>' ></asp:Label></td>
                                        </tr>
                                        </table>
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                            
                                            <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" HeaderStyle-Height="25px"  HeaderStyle-HorizontalAlign="Center" 
                                                NullDisplayText="---" />                                                                                                                                   
                                                
                                            <asp:BoundField DataField="UnidadMedida" ItemStyle-HorizontalAlign="Center" HeaderText="U.M." HeaderStyle-Height="25px"  HeaderStyle-HorizontalAlign="Center" 
                                                NullDisplayText="---" />
                                                
                                      
                                                
                                            
                                                
                                        </Columns>
                                        <HeaderStyle CssClass="GrillaHeader" />
                                        <FooterStyle CssClass="GrillaFooter" />
                                        <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                        <EditRowStyle CssClass="GrillaEditRow" />
                                        <PagerStyle CssClass="GrillaPager" />
                                        <RowStyle CssClass="GrillaRow" />
                                        <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                    </asp:GridView>
                                    
                                    </td>
                                </tr>
                                <tr>
                                <td>
                                <asp:Button  TabIndex="212" ID="btnAnterior_Productos" runat="server" Font-Bold="true" Width="50px" Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionProductos('0'));" />
                                        <asp:Button  TabIndex="213" ID="btnPosterior_Productos" runat="server" Font-Bold="true" Width="50px" Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionProductos('1'));" />
                                        <asp:TextBox  TabIndex="214" ID="txtPageIndex_Productos" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right" runat="server"></asp:TextBox>
                                        <asp:Button  TabIndex="215" ID="btnIr_Productos" runat="server" Font-Bold="true" Width="50px" Text="Ir" ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionProductos('2'));" />
                                        <asp:TextBox  TabIndex="216" ID="txtPageIndexGO_Productos" Width="50px" CssClass="TextBox_ReadOnly" runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                                </td>
                                </tr>
                                                                    </table>     
          </ContentTemplate>
          <Triggers>
          <asp:PostBackTrigger ControlID="DGV_AddProd" />
          </Triggers>
          </asp:UpdatePanel>
                                                       
                                                             
    </div>     
    <script  language="javascript" type="text/javascript" >

        function validarbusqueda() {
            var tb = document.getElementById('<%=tbRazonApe.ClientID %>');
            var radio = document.getElementById('<%=rdbTipoPersona.ClientID %>');
            var control = radio.getElementsByTagName('input');
            if (control[1].checked == true) {
                tb.select();
                tb.focus();
                return false;
            }
            return true;
        }
        function validarCajaBusquedaNumero(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);            
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
            if (caracter == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }
        function validarCajaBusqueda(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) {
                var boton = document.getElementById('<%=btBuscarPersonaGrilla.ClientID %>');
                boton.focus();
                return true;
            }
        }
        function valNavegacion(tipoMov) {

            var index = parseInt(document.getElementById('<%=tbPageIndex.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=tbRazonApe.ClientID%>').select();
                document.getElementById('<%=tbRazonApe.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=tbPageIndexGO.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').select();
                        document.getElementById('<%=tbPageIndexGO.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }
        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=tbRazonApe.ClientID%>').select();
            document.getElementById('<%=tbRazonApe.ClientID%>').focus();
            return false;
        }
        function valOnClick_btnBuscarPropietario_1() {
            var hddOpcion_BuscarPersona = document.getElementById('<%=hddOpcion_BuscarPersona.ClientID%>');
            hddOpcion_BuscarPersona.value = '0';
            mostrarCapaPersona();            
            return false;
        }
        //*************************************** PRODUCTO
        function valKeyPressCodigoSL(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valKeyPressDescripcionProd(elEvento) {
            //obtener caracter pulsado en todos los exploradores
            var evento = elEvento || window.event;
            var caracter = evento.charCode || evento.keyCode;
            //alert("El car�cter pulsado es: " + caracter);
            if (caracter == 13) { // Enter                                    
                document.getElementById('<%=btnBuscarGrilla_AddProd.ClientID%>').focus();
            }
            return true;
        }
        function valNavegacionProductos(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Productos.ClientID%>').value);
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            if (isNaN(index) || index == null || index.length == 0 || index <= 0 || grilla == null) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
                document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Productos.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }

        function valAddProductos() {

            //*************** VALIDAMOS QUE SE HAYA SELECCIONADO UN CLIENTE
            //        var txtIdCliente = 
            //        if (isNaN(parseInt(txtIdCliente.value)) || txtIdCliente.value.length <= 0) {
            //            alert('Debe seleccionar un Cliente.');
            //            return false;
            //        }

            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            var cont = 0;
            if (grilla == null) {
                alert('No se seleccionaron productos.');
                return false;
            }
            for (var i = 1; i < grilla.rows.length; i++) {//comienzo en 1 xq 0 es la cabecera
                var rowElem = grilla.rows[i];
                var txtCantidad = rowElem.cells[0].children[0];
                if (parseFloat(txtCantidad.value) > 0) {
                    // if (parseFloat(txtCantidad.value) > parseFloat(rowElem.cells[7].innerHTML)) {
                    //                        alert('La cantidad ingresada excede a la cantidad disponible.');
                    //                        txtCantidad.select();
                    //                        txtCantidad.focus();
                    //     return false;
                    //  } else if (parseFloat(txtCantidad.value) > 0) {
                    cont = cont + 1;
                    //  }
                }
            }

            if (cont == 0) {
                alert('No se seleccionaron productos.');
                return false;
            }

            /*
            //************* Validamos que no agregue de otro almacen
            
            if (grillaDetalle != null) {

                if (grillaDetalle.rows.length > 1) {

                    //************ valido el almacen
                    
            var hddIdAlmacen = 
                    

                    if (parseFloat(cboAlmacen.value) != parseFloat(hddIdAlmacen.value)) {
                        
            return false;
            }
            }
            }
            */
            return true;
        }
        function limpiarBuscarProducto() {
            //************* limpiamos los controles
            //    var cboLinea = document.getElementById('');
            var cboLinea = document.getElementById('<%=cmbLinea_AddProd.ClientID%>');
            var cboSubLinea = document.getElementById('<%=cmbSubLinea_AddProd.ClientID%>');
            var txtDescripcion = document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>');
            var txtCodigoProducto = document.getElementById('<%=txtCodigoProducto.ClientID%>');
            var grilla = document.getElementById('<%=DGV_AddProd.ClientID%>');
            //************** Limpiamos la caja de la descripcion
            txtDescripcion.value = '';
            txtCodigoProducto.value = '';

            //*********** Limpiamos la linea
            cboLinea.selectedIndex = 0;
            //********** Eliminamos todos los items menos el primero
            for (var i = (cboSubLinea.length - 1); i > 0; i--) {
                cboSubLinea[i] = null;
            }

            //********* FILTRO AVANZADO
            txtDescripcion.select();
            txtDescripcion.focus();
            return false;
        }

        function valStockDisponible_AddProd() {
            /*var grilla = document.getElementById('<%=DGV_AddProd.ClientID %>');
            if (grilla != null) {
            for (var i = 1; i < grilla.rows.length; i++) {
            var rowElem = grilla.rows[i];
            if (rowElem.cells[0].children[0].id == event.srcElement.id) {
            if (parseFloat(rowElem.cells[0].children[0].value) > parseFloat(rowElem.cells[7].innerHTML)) {
            alert('La cantidad ingresada excede a la cantidad disponible.');
            rowElem.cells[0].children[0].select();
            rowElem.cells[0].children[0].focus();
            return false;
            }
            return false;
            }

                }
            }*/
            return false;
        }
        
        function valOnClick_btnBuscarProducto() {
            onCapa('capaBuscarProducto_AddProd');
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').select();
            document.getElementById('<%=txtDescripcionProd_AddProd.ClientID%>').focus();
            return false;
        }
        function valAddTabla() {
            var cbotabla = document.getElementById('<%=cboTipoTabla.ClientID %>');
            if (isNaN(parseInt(cbotabla.value)) || cbotabla.value.length <= 0) {
                alert('DEBE SELECCIONAR UN ATRIBUTO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            var grilla = document.getElementById('<%=GV_FiltroTipoTabla.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[1].children[1].value == cbotabla.value && rowElem.cells[1].children[1].value == cbotabla.value) {
                        alert('El Tipo Tabla seleccionado ya ha sido ingresado.');
                        return false;
                    }
                }
            }
            return true;
        }
        //************************** END PRODUCTO

        function valOnClick_btnBuscarContenido_1(boton) {
            var grilla = document.getElementById('<%=GV_Envase_1.ClientID %>');
            var hddIndexGrilla_1 = document.getElementById('<%=hddIndexGrilla_1.ClientID %>');
            if (grilla != null) {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    if (rowElem.cells[3].children[0].cells[1].children[0].id == boton.id) {
                        hddIndexGrilla_1.value = (i-1);
                        valOnClick_btnBuscarProducto();
                        return false;
                    }
                }
            }
            alert('NO SE HALLARON REGISTROS.');
            return false;
        }
        function valOnClick_btnGuardar_1() {

            var grilla = document.getElementById('<%=GV_Envase_1.ClientID %>');
            if (grilla == null) {
                alert('NO SE HA REGISTRADO BOTELLAS. NO SE PERMITE LA OPERACI�N.');
                return false;
            } else {
                for (var i = 1; i < grilla.rows.length; i++) {
                    var rowElem = grilla.rows[i];
                    var txtNroBotella = rowElem.cells[1].children[0].cells[0].children[0];
                    if ( txtNroBotella.value.length<=0 ) {
                        alert('INGRESE UN NRO BOTELLA. NO SE PERMITE LA OPERACI�N.');
                        txtNroBotella.select();
                        txtNroBotella.focus();
                        return false;
                    }
                }
            }
            var hddIdPersona_1 = document.getElementById('<%=hddIdPersona_1.ClientID %>');
            if (isNaN(parseInt(hddIdPersona_1.value))) {            
                alert('DEBE SELECCIONAR UN PROPIETARIO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            return confirm('Desea continuar con la Operaci�n ?');
        }
        function valOnClick_btnBuscarCliente_2() {

            var hddOpcion_BuscarPersona = document.getElementById('<%=hddOpcion_BuscarPersona.ClientID%>');
            hddOpcion_BuscarPersona.value = '1';
            mostrarCapaPersona();
            return false;
        }
        
        function valOnClick_btnLimpiar_2() {
            document.getElementById('<%=txtCliente_2.ClientID %>').value = '';
            document.getElementById('<%=txtNroBotella_2.ClientID %>').value = '';
            document.getElementById('<%=txtNroDocumento_2.ClientID %>').value = '';
            document.getElementById('<%=hddIdCliente_2.ClientID %>').value = '';
            return false;
        }

        function valOnClick_btnBuscarPersona_3() {
            var hddOpcion_BuscarPersona = document.getElementById('<%=hddOpcion_BuscarPersona.ClientID%>');
            hddOpcion_BuscarPersona.value = '2';
            mostrarCapaPersona();
            return false;
        }
        function valOnClick_btnGuardar_3() {
            var hddIdDestinatario_3 = document.getElementById('<%=hddIdDestinatario_3.ClientID%>');
            if (isNaN(parseInt(hddIdDestinatario_3.value)) || hddIdDestinatario_3.value.length <= 0) {
                alert('DEBE SELECCIONAR UN DESTINATARIO. NO SE PERMITE LA OPERACI�N.');
                return false;
            }
            return confirm('Desea continuar con la Operaci�n ?');
        }
    </script>
    
</asp:Content>
