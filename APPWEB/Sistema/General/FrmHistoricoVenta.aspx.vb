﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.


Partial Public Class FrmHistoricoVenta
    Inherits System.Web.UI.Page

    Private objScript As New ScriptManagerClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Me.IsPostBack) Then
            inicializarFrm()
            validarPermisos()
        End If
    End Sub
    Private Sub validarPermisos()

        Dim listaPermisos() As Integer = (New Negocio.Util).ValidarPermisosxIdUsuario(CInt(Session("IdUsuario")), New Integer() {207, 216})

        If listaPermisos(0) > 0 Then  '********* REGISTRAR
            Me.btnGuardar.Enabled = True
        Else
            Me.btnGuardar.Enabled = False
        End If

        If listaPermisos(1) > 0 Then  '********* FECHA
            Me.cboAnio.Enabled = True
            Me.cboMes.Enabled = True
        Else
            Me.cboAnio.Enabled = False
            Me.cboMes.Enabled = False
        End If

    End Sub
    Private Sub inicializarFrm()
        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), True)
                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), True)

                .LlenarCboLinea(Me.cboLinea, True)
                .LlenarCboSubLineaxIdLinea(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), True)

            End With

            Dim fechaActual As Date = (New Negocio.FechaActual).SelectFechaActual
            cargar_cboAnio(fechaActual.Year)

            '************ COLOCAMOS LOS VALORES ACTUALES
            If (Me.cboAnio.Items.FindByValue(CStr(fechaActual.Year)) IsNot Nothing) Then
                Me.cboAnio.SelectedValue = CStr(fechaActual.Year)
            End If

            If (Me.cboMes.Items.FindByValue(CStr(fechaActual.Month)) IsNot Nothing) Then
                Me.cboMes.SelectedValue = CStr(fechaActual.Month)
            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargar_cboAnio(ByVal anio As Integer)

        For i As Integer = 2000 To (anio + 5)

            Dim item As New ListItem(i.ToString, i.ToString)
            Me.cboAnio.Items.Add(item)

        Next

    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            With objCbo


                .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), True)

            End With

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            With objCbo

                .LlenarCboSubLineaxIdLinea(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), True)

            End With


        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGuardar.Click
        registrarHV()
    End Sub
    Private Sub registrarHV()
        Try

            If ((New Negocio.HistoricoVenta).HistoricoVenta_GenerarCargaDatosxAnioMes(CInt(Me.cboAnio.SelectedValue), CInt(Me.cboMes.SelectedValue), CInt(Me.cboEmpresa.SelectedValue), CInt(Me.cboTienda.SelectedValue), CInt(Me.cboLinea.SelectedValue), CInt(Me.cboSubLinea.SelectedValue))) Then

                objScript.mostrarMsjAlerta(Me, "La Operación se realizó con éxito.")

            Else

                Throw New Exception("PROBLEMAS EN LA OPERACIÓN.")

            End If

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
End Class