﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="FrmDetalleComision.aspx.vb" Inherits="APPWEB.FrmDetalleComision" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td class="TituloCelda">
                Detalle Ventas por Vendedor - Comisiones Extraordinarias
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend><span class="Texto"></span></legend>
                    <table>
                        <tr>
                            <td class="Texto">
                                Empresa:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Tienda:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTienda" runat="server" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="Texto">
                                Comisión Extraordinaria:
                            </td>
                            <td>
                                <asp:DropDownList ID="cboComisionExt" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend><span class="Texto">Fecha</span></legend>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="Texto">
                                Incio:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaIni" runat="server" CssClass="TextBox_Fecha" Width="80px"
                                    onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender_FechaIni" runat="server" TargetControlID="txtFechaIni"
                                    Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="Texto">
                                Fin:
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaFin" runat="server" CssClass="TextBox_Fecha" Width="80px"
                                    onBlur=" return ( valFecha_Blank(this) ); "></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender_FechaFin" runat="server" TargetControlID="txtFechaFin"
                                    Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                            </td>
                            <td>
                                <asp:HiddenField ID="hddIdVendedor" runat="server" Value="0" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ImageButton ID="imgAceptar" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                    OnClientClick=" return ( Aceptar() ); " />
            </td>
        </tr>
        <tr>
            <td>
                <iframe id="iReportFrame" name="iReportFrame" width="100%" height="2000px" scrolling="no">
                </iframe>
            </td>
        </tr>
    </table>
    
    <script language="javascript" type="text/javascript">

        function Aceptar() {

            var FechaIni = document.getElementById('<%=txtFechaIni.ClientID %>').value;
            if (!valFecha_Blank(document.getElementById('<%=txtFechaIni.ClientID %>'))) {
                alert('INGRESE UNA FECHA VALIDA.\n NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var FechaFin = document.getElementById('<%=txtFechaFin.ClientID %>').value;
            if (!valFecha_Blank(document.getElementById('<%=txtFechaFin.ClientID %>'))) {
                alert('INGRESE UNA FECHA VALIDA.\n NO SE PERMITE LA OPERACIÓN.');
                return false;
            }

            var IdEmpresa = parseInt(document.getElementById('<%=cboEmpresa.ClientID %>').value);
            var IdTienda = parseInt(document.getElementById('<%=cboTienda.ClientID %>').value);
            var IdComisionCab = parseInt(document.getElementById('<%=cboComisionExt.ClientID %>').value);
            var IdVendedor = parseInt(document.getElementById('<%=hddIdVendedor.ClientID %>').value);

            iReportFrame.location.href = 'visorVentas.aspx?iReporte=31&IdEmpresa=' + IdEmpresa + '&IdTienda=' + IdTienda + '&IdComisionCab=' + IdComisionCab + '&IdVendedor=' + IdVendedor + '&FechaIni=' + FechaIni + '&FechaFin=' + FechaFin;

            return false;
         }
    
    
    </script>
               
    
</asp:Content>
