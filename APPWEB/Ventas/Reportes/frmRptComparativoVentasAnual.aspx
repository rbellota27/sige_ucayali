<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master"
    CodeBehind="frmRptComparativoVentasAnual.aspx.vb" Inherits="APPWEB.frmRptComparativoVentasAnual" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%" width="100%">
        <tr>
            <td class="TituloCelda">
                Comparativo Anual de Ventas
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="Label">
                            Empresa:
                            <asp:DropDownList ID="cmbEmpresa" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="Label">
                            A�o:<asp:TextBox ID="txtano" runat="server" size="10"></asp:TextBox>
                        </td>
                        <td class="Label">
                            Moneda:
                            <asp:DropDownList ID="CmbMoneda" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rbtipo" onClick="return(validartipo(this));" runat="server"
                                CssClass="Label" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="dc_Cantidad">Cantidad</asp:ListItem>
                                <asp:ListItem Value="dc_Importe">Valor</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel_Cliente" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td class="TituloCeldaLeft">
                                    <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server" TargetControlID="PnlCliente"
                                        CollapsedSize="0" ExpandedSize="80" Collapsed="true" ExpandControlID="ImgCliente"
                                        CollapseControlID="ImgCliente" TextLabelID="lblCliente" ImageControlID="ImgCliente"
                                        CollapsedImage="~/Imagenes/Mas_B.JPG" ExpandedImage="~/Imagenes/Menos_B.JPG"
                                        CollapsedText="Buscar Cliente" ExpandedText="Buscar Cliente" ExpandDirection="Vertical"
                                        SuppressPostBack="True" />
                                    <asp:Image ID="ImgCliente" runat="server" Width="16px" />
                                    <asp:Label ID="lblCliente" runat="server" Text="Buscar Cliente" CssClass="LabelBlanco"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="PnlCliente" runat="server">
                                        <table cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Imagenes/Limpiar_b.JPG"
                                                        onmouseover="this.src='../../Imagenes/Limpiar_A.JPG';" onmouseout="this.src='../../Imagenes/Limpiar_b.JPG';"
                                                        OnClientClick="return(LimpiarControlesCliente());" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnBuscarCliente" runat="server" ImageUrl="~/Imagenes/Buscar_b.JPG"
                                                        onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                                        OnClientClick="return(mostrarCapaPersona());" />
                                                    <asp:Label ID="Label12024" runat="server" CssClass="Label" Text="Tipo de Persona"></asp:Label>
                                                    <asp:DropDownList ID="cboTipoPersona" runat="server" Width="116px" Font-Bold="True"
                                                        Enabled="False">
                                                        <asp:ListItem>---------</asp:ListItem>
                                                        <asp:ListItem>Natural</asp:ListItem>
                                                        <asp:ListItem>Juridica</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 139px; text-align: right;">
                                                    <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Nombre o Raz�n Social"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRazonSocial" CssClass="TextBoxReadOnly" runat="server" Width="521px"
                                                        MaxLength="50" ReadOnly="True"></asp:TextBox>
                                                    <asp:TextBox ID="txtCodigoCliente" CssClass="TextBoxReadOnly" runat="server" Width="99px"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; width: 139px">
                                                    <asp:Label ID="Label3" runat="server" Text="Dni" CssClass="Label"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDni" runat="server" Width="70px" CssClass="TextBoxReadOnly" Style="margin-left: 0px;
                                                        margin-bottom: 0px" MaxLength="8" ReadOnly="True"></asp:TextBox>
                                                    <asp:Label ID="Label4" runat="server" Text="Ruc" CssClass="Label"></asp:Label>
                                                    <asp:TextBox ID="txtRuc" runat="server" Width="90px" MaxLength="11" CssClass="TextBoxReadOnly"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DGV_BuscarPersona" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                            onmouseout="this.src='../../Imagenes/Aceptar_B.JPG';" onmouseover="this.src='../../Imagenes/Aceptar_A.JPG';"
                            OnClientClick="return(MostrarReporte());" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px">
                </iframe>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hddIdPersona" runat="server" />
                <asp:HiddenField ID="hddFechaActual" runat="server" />
                <asp:HiddenField ID="hddIdDocumento" runat="server" />
                <br />
            </td>
        </tr>
    </table>
    <div id="capaPersona" style="border: 3px solid blue; padding: 8px; width: 900px;
        height: auto; position: absolute; top: 239px; left: 27px; background-color: white;
        z-index: 2; display: none;">
        <asp:UpdatePanel ID="UpdatePanel_Persona" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="btnCerrar_Capa" runat="server" ToolTip="Cerrar" ImageUrl="~/Imagenes/Cerrar_B.JPG"
                                onmouseout="this.src='../../Imagenes/Cerrar_B.JPG';" onmouseover="this.src='../../Imagenes/Cerrar_A.JPG';"
                                OnClientClick="return(offCapa('capaPersona'));" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <table>
                                <tr>
                                    <td class="Label">
                                        Persona
                                    </td>
                                    <td class="Label">
                                        <asp:RadioButtonList ID="rblpersona" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="0" Text="Juridica" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Natural"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="LabelLeft">                                        
                                        RUC:
                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnBuscarPersona_Grilla">                                        
                                        <asp:TextBox ID="txtBPruc" runat="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                    <td class="LabelLeft">
                                        DNI:
                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnBuscarPersona_Grilla">                                        
                                        <asp:TextBox ID="txtBPDni" runat="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                    <td class="LabelLeft">
                                        Razon Social/Nombre:
                                        <asp:Panel ID="Panel3" runat="server" DefaultButton="btnBuscarPersona_Grilla">                                        
                                        <asp:TextBox ID="txtBuscarPersona_Grilla" runat="server" onKeypress="return(validarCajaBusqueda());"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                    <td class="LabelLeft">
                                        <asp:ImageButton ID="btnBuscarPersona_Grilla" runat="server" CausesValidation="false"
                                            ImageUrl="~/Imagenes/Buscar_b.JPG" onmouseout="this.src='../../Imagenes/Buscar_b.JPG';"
                                            onmouseover="this.src='../../Imagenes/Buscar_A.JPG';" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:GridView ID="DGV_BuscarPersona" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                PageSize="20" Width="100%">
                                <Columns>
                                    <asp:CommandField ButtonType="Link" SelectText="Seleccionar" ShowSelectButton="true" />
                                    <asp:BoundField DataField="IdPersona" HeaderText="Id" NullDisplayText="0" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Descripci�n" NullDisplayText="---" />
                                    <asp:BoundField DataField="Dni" HeaderText="D.N.I." NullDisplayText="---" />
                                    <asp:BoundField DataField="Ruc" HeaderText="R.U.C." NullDisplayText="---" />
                                    <asp:BoundField DataField="Direccion" HeaderText="Direcci�n" NullDisplayText="---" />
                                </Columns>
                                <HeaderStyle CssClass="GrillaHeader" />
                                <FooterStyle CssClass="GrillaFooter" />
                                <PagerStyle CssClass="GrillaPager" />
                                <RowStyle CssClass="GrillaRow" />
                                <AlternatingRowStyle CssClass="GrillaRowAlternating" />
                                <SelectedRowStyle CssClass="GrillaSelectedRow" />
                                <EditRowStyle CssClass="GrillaEditRow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnAnterior_Persona" runat="server" Font-Bold="true" Width="50px"
                                Text="<" ToolTip="P�gina Anterior" OnClientClick="return(valNavegacionPersona('0'));" />
                            <asp:Button ID="btnPosterior_Persona" runat="server" Font-Bold="true" Width="50px"
                                Text=">" ToolTip="P�gina Posterior" OnClientClick="return(valNavegacionPersona('1'));" />
                            <asp:TextBox ID="txtPageIndex_Persona" Width="50px" ReadOnly="true" CssClass="TextBoxReadOnly_Right"
                                runat="server"></asp:TextBox>
                            <asp:Button ID="btnIr_Persona" runat="server" Font-Bold="true" Width="50px" Text="Ir"
                                ToolTip="Ir a la P�gina" OnClientClick="return(valNavegacionPersona('2'));" />
                            <asp:TextBox ID="txtPageIndexGO_Persona" Width="50px" CssClass="TextBox_ReadOnly"
                                runat="server" onKeyPress="return(onKeyPressEsNumero('event'));"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script language="javascript" type="text/javascript">

        function validartipo(obj) {
            var control = obj.getElementsByTagName('input');
            var cbo = document.getElementById('<%=CmbMoneda.ClientID %>');
            if (control[0].checked == true && cbo.value != '0') {
                cbo.selectedIndex = 0;
            }
            return true;
        }

        function onCapaCliente(s) {
            LimpiarControlesCliente();
            onCapa(s);
            document.getElementById('<%= txtRazonSocial.ClientID%>').focus();
            return false;
        }
        void function LimpiarControlesCliente() {
            document.getElementById('<%= txtRazonSocial.ClientID%>').value = "";
            document.getElementById('<%= txtDNI.ClientID%>').value = "";
            document.getElementById('<%= txtRUC.ClientID%>').value = "";
            document.getElementById('<%= txtCodigoCliente.ClientID %>').value = "";
            return false;
        }



        function validarNumeroPunto(event) {
            var key = event.keyCode;
            if (key == 46) {
                return true;
            } else {
                if (!onKeyPressEsNumero('event')) {
                    return false;
                }
            }
        }



        //Buscar SubLinea
        function ValidarEnteroSinEnter() {
            if (onKeyPressEsNumero('event') == false) {
                alert('Caracter no v�lido. Solo se permiten n�meros Enteros');
                return false;
            }
            return true;
        }


        //End Buscar SubLinea
        //Buscar Cliente

        function BuscarCliente() {

            return true;
        }


        function ValidarEnter() {
            return (!esEnter(event));
        }
        function ValidaEnter() {
            if (esEnter(event) == true) {
                alert('Tecla No permitida');
                caja.focus();
                return false;
            }
            return true;
        }



        function mostrarCapaPersona() {
            onCapa('capaPersona');
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
            document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();

            return false;
        }
        function addPersona_Venta() {
            offCapa('capaPersona');

            return false;
        }


        function aceptarFoco(caja) {
            caja.select();
            caja.focus();
            return true;
        }




        function valNavegacionPersona(tipoMov) {
            var index = parseInt(document.getElementById('<%=txtPageIndex_Persona.ClientID%>').value);
            if (isNaN(index) || index == null || index.length == 0 || index <= 0) {
                alert('No puede utilizar los controles de Navegaci�n. Realice una B�squeda.');
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').select();
                document.getElementById('<%=txtBuscarPersona_Grilla.ClientID%>').focus();
                return false;
            }
            switch (tipoMov) {
                case '0':   //************ anterior
                    if (index <= 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        return false;
                    }
                    break;
                case '1':
                    break;
                case '2': //************ ir
                    index = parseInt(document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').value);
                    if (isNaN(index) || index == null || index.length == 0) {
                        alert('Ingrese una P�gina de navegaci�n.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    if (index < 1) {
                        alert('No existen p�ginas con �ndice menor a uno.');
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').select();
                        document.getElementById('<%=txtPageIndexGO_Persona.ClientID%>').focus();
                        return false;
                    }
                    break;
            }
            return true;
        }


        function validarCajaBusqueda() {
            var key = event.keyCode;
            if (key == 13) {
                var boton = document.getElementById('<%=btnBuscarPersona_Grilla.ClientID %>');
                boton.focus();
                return true;
            }
        }



        function MostrarReporte() {
            var fechaInicio;
            var fechaFin;
            var idempresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
            var idMoneda = document.getElementById('<%=CmbMoneda.ClientID%>').value;
            if (idMoneda ==0){
            idMoneda =1;
            }else{
            idMoneda =idMoneda;
            }
             
            var rb = document.getElementById('<%=rbtipo.ClientID %>');
            var control = rb.getElementsByTagName('input');
            
            var idtipo = '';
            for (var i = 0; i < control.length; i++) {
                if (control[i].checked == true) {
                    idtipo = control[i].value;
                }
            }

            var tipo = document.getElementById('<%=rbtipo.ClientID %>');
            var cbo = document.getElementById('<%=CmbMoneda.ClientID %>');
            var control = tipo.getElementsByTagName('input');
            if (control[1].checked == true && cbo.value == '0') {
                alert('Selecione una moneda.');
                return false;
            }

            var tipo = document.getElementById('<%=rbtipo.ClientID %>');
            var cbo = document.getElementById('<%=CmbMoneda.ClientID %>');
            var control = tipo.getElementsByTagName('input');
            if (control[0].checked == true && cbo.value != '0') {
                alert('NO selecionar una moneda.');
                return false;
            }

            var idpersona = document.getElementById('<%=txtCodigoCliente.ClientID%>').value;
            var persona;
            if (idpersona == '') {
                persona = 0;
            } else {
                persona = idpersona;
            }

            var ano = document.getElementById('<%=txtano.ClientID%>').value;
            if (ano == '') {
                ano = 0;
            } else {
                ano = ano;
            }

            frame1.location.href = 'VisorGrillaVentas.aspx?iReporte=1&IdEmpresa=' + idempresa + '&idMoneda=' + idMoneda + '&persona=' + persona + '&ano=' + ano + '&idtipo=' + idtipo;
            return false;

        }
   
    
    </script>

</asp:Content>
