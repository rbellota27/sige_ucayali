﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Principal.Master" CodeBehind="FrmDocMediosPago.aspx.vb" Inherits="APPWEB.FrmDocMediosPago" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table style="width: 100%">
<tr><td class ="TituloCelda" > Documentos por Medio de Pago </td> </tr>
   <tr>
            <td>
                <table>
                    <tr>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td class="Label">
                                                        Empresa:<asp:DropDownList ID="cmbEmpresa" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="Label">
                                                        Tienda:<asp:DropDownList ID="cmbtienda" runat="server" Enabled="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                   <td class="Label">
                                                       Fecha Inicio:
                                                   </td>
                                                   <td>
                                                       <asp:TextBox ID="txtFechaInicio" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                       <cc1:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                                           TargetControlID="txtFechaInicio">
                                                       </cc1:CalendarExtender>
                                                   </td>
                                                   <td class="Label">
                                                       Fecha Fin:
                                                   </td>
                                                   <td>
                                                       <asp:TextBox ID="txtFechaFin" runat="server" ReadOnly="true" CssClass="TextBoxReadOnly"></asp:TextBox>
                                                       <cc1:CalendarExtender ID="txtFechaFin_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                                           TargetControlID="txtFechaFin">
                                                       </cc1:CalendarExtender>
                                                   </td> 
                                                                                                      
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="btnMostrarReporte" runat="server" ImageUrl="~/Imagenes/Aceptar_B.JPG"
                                                                        onmouseout="this.src='/Imagenes/Aceptar_B.JPG';" onmouseover="this.src='/Imagenes/Aceptar_A.JPG';"
                                                                        OnClientClick="return(MostrarReporte());" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                     
                                                </tr>
                                        </td>
                                    </tr>
                                </table>
                                </td>
                            </tr>
                            <%--taable--%>
                                                          
                            </table>    
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    
                    </td>
                    </tr>



                </table>
            </td>
        </tr>
        
        <tr>
            <td>
            <iframe name="frame1" id="frame1" width="100%" scrolling="yes" style="height: 1130px"></iframe>
            </td>
        </tr>
        <tr>
            <td>
                                               
                <asp:HiddenField ID="hddFechaActual" runat="server" />
                <asp:HiddenField ID="hddRbtTipoReporte" runat="server" />
                <br />
            </td>
        </tr>
</table>
<script type ="text/javascript" language ="javascript" >

    function MostrarReporte() {
        var idempresa = document.getElementById('<%= cmbEmpresa.ClientID%>').value;
        var idtienda = document.getElementById('<%=cmbtienda.ClientID%>').value;
        
        if (document.getElementById('<%= txtFechaInicio.ClientID%>') == null) {
            fechaInicio = document.getElementById('<%= hddFechaActual.ClientID%>').value
            fechaFin = document.getElementById('<%= hddFechaActual.ClientID%>').value
        } else {
            fechaInicio = document.getElementById('<%= txtFechaInicio.ClientID%>').value;
            fechaFin = document.getElementById('<%= txtFechaFin.ClientID%>').value;
        }
        frame1.location.href = 'visorVentas.aspx?iReporte=7&FechaInicio=' + fechaInicio + '&FechaFin=' + fechaFin + '&IdEmpresa=' + idempresa + '&IdTienda=' + idtienda; 
        return false;

    }
</script>
</asp:Content>
