﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmComisionxVendedor
    Inherits System.Web.UI.Page

    Dim objScript As New ScriptManagerClass
    Dim cbo As Combo
    Private Const _IdTipoComision As Integer = 2


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            OnLoad_DetalleComision()
        End If

    End Sub

    Private Sub OnLoad_DetalleComision()

        cbo = New Combo
        With cbo
            .LlenarCboEmpresaxIdUsuario(Me.cboEmpresa, CInt(Session("IdUsuario")), False)
            .LlenarCboTiendaxIdEmpresaxIdUsuario(Me.cboTienda, CInt(Me.cboEmpresa.SelectedValue), CInt(Session("IdUsuario")), True)
            .llenarCboComsionCab(Me.cboComisionExt, _IdTipoComision, True)

            .llenarCboTipoExistencia(Me.cboTipoExistencia)
            .llenarCboLineaxTipoExistencia(Me.cboLinea, CInt(Me.cboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)


            .llenarCboTipoExistencia(Me.CboTipoExistencia_AddProd)
            .llenarCboLineaxTipoExistencia(Me.cmbLinea_AddProd, CInt(Me.CboTipoExistencia_AddProd.SelectedValue), True)
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)

            .LlenarCboPropietario(Me.cboEmpresa_BuscarPersona, False)
            .llenarCboTiendaxIdEmpresa(Me.cboTienda_BuscarPersona, CInt(Me.cboEmpresa_BuscarPersona.SelectedValue), False)
            .LlenarCboPerfil(Me.cboPerfil_BuscarPersona, False)

        End With

        Me.txtFechaIni.Text = Format((New Negocio.FechaActual).SelectFechaActual, "dd/MM/yyyy").Trim
        Me.txtFechaFin.Text = Me.txtFechaIni.Text.Trim

        actualizarControles_BuscarPersona(CInt(Me.rdbTipoBusqueda_Persona.SelectedValue))

        S_FiltroTablaValor = New List(Of Entidades.TipoTablaValor)
        S_TipoTablaValor = New List(Of Entidades.TipoTablaValor)


    End Sub


    Private Sub cboTipoExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoExistencia.SelectedIndexChanged
        cbo = New Combo
        With cbo
            .llenarCboLineaxTipoExistencia(Me.cboLinea, CInt(Me.cboTipoExistencia.SelectedValue), True)
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)
        End With
    End Sub

    Private Sub cboLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLinea.SelectedIndexChanged
        cbo = New Combo
        With cbo
            .LlenarCboSubLineaxIdLineaxIdTipoExistencia(Me.cboSubLinea, CInt(Me.cboLinea.SelectedValue), CInt(Me.cboTipoExistencia.SelectedValue), True)
        End With
    End Sub



#Region "************************** BUSQUEDA PRODUCTO"

    Private listaProductoView As List(Of Entidades.ProductoView)

    Private Sub btnBuscarGrilla_AddProd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscarGrilla_AddProd.Click
        BuscarProductoCatalogo(0)
    End Sub

    Protected Sub BuscarProductoCatalogo(ByVal opcion As Integer)
        Try

            Select Case opcion
                Case 0
                    '*************** cuando se presiona el boton buscar

                    ViewState.Add("nombre_BuscarProducto", txtDescripcionProd_AddProd.Text)
                    ViewState.Add("IdLinea_BuscarProducto", Me.cmbLinea_AddProd.SelectedValue)
                    ViewState.Add("IdSubLinea_BuscarProducto", Me.cmbSubLinea_AddProd.SelectedValue)
                    ViewState.Add("IdAlmacen_BuscarProducto", 0)
                    ViewState.Add("IdEmpresa_BuscarProducto", Me.cboEmpresa.SelectedValue)
                    ViewState.Add("CodigoSubLinea_BuscarProducto", "")
                    ViewState.Add("IdTipoExistencia", Me.CboTipoExistencia_AddProd.SelectedValue)

            End Select

            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            CInt(ViewState("IdAlmacen_BuscarProducto")), 0, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub cargarDatosProductoGrilla(ByVal grilla As GridView, ByVal IdLinea As Integer, ByVal IdSubLinea As Integer, ByVal codigoSubLinea As String, ByVal nomProducto As String, ByVal IdAlmacen As Integer, _
                                          ByVal tipoMov As Integer, ByVal IdEmpresa As Integer, ByVal IdTipoExistencia As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '**************** INICIO
                index = 0
            Case 1 '**************** Anterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) - 1
            Case 2 '**************** Posterior
                index = (CInt(txtPageIndex_Productos.Text) - 1) + 1
            Case 3 '**************** IR
                index = (CInt(txtPageIndexGO_Productos.Text) - 1)
        End Select

        '************Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Dim codigoProducto As String = Me.txtCodigoProducto.Text
        Dim tableTipoTabla As DataTable = obtenerDataTable_TipoTablaValor()
        'Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, CInt(Me.cboPais.SelectedValue), CInt(Me.cboFabricante.SelectedValue), CInt(Me.cboMarca.SelectedValue), CInt(Me.cboModelo.SelectedValue), CInt(Me.cboFormato.SelectedValue), CInt(Me.cboProveedor.SelectedValue), CInt(Me.cboEstilo.SelectedValue), CInt(Me.cboTransito.SelectedValue), CInt(Me.cboCalidad.SelectedValue))
        Me.listaProductoView = (New Negocio.Producto).Producto_ListarProductosxParams_V2(IdLinea, IdSubLinea, codigoSubLinea, nomProducto, IdEmpresa, IdAlmacen, grilla.PageSize, index, codigoProducto, IdTipoExistencia, tableTipoTabla)


        If listaProductoView.Count = 0 Then

            grilla.DataSource = Nothing
            grilla.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');    alert('No se hallaron registros.');        ", True)
            Return

        Else

            grilla.DataSource = listaProductoView
            grilla.DataBind()
            txtPageIndex_Productos.Text = CStr(index + 1)

            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "  onCapa('capaBuscarProducto_AddProd');           ", True)

        End If

        setListaProductoView(Me.listaProductoView)

    End Sub
    Private Sub btnAnterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            0, 1, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnPosterior_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosterior_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            0, 2, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btnIr_Productos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIr_Productos.Click
        Try
            cargarDatosProductoGrilla(Me.DGV_AddProd, CInt(ViewState("IdLinea_BuscarProducto")), CInt(ViewState("IdSubLinea_BuscarProducto")), CStr(ViewState("CodigoSubLinea_BuscarProducto")), CStr(ViewState("nombre_BuscarProducto")), _
            0, 3, CInt(ViewState("IdEmpresa_BuscarProducto")), CInt(ViewState("IdTipoExistencia")))
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub



    Private Sub CboTipoExistencia_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboTipoExistencia_AddProd.SelectedIndexChanged

        Dim objCbo As New Combo
        objCbo.llenarCboLineaxTipoExistencia(Me.cmbLinea_AddProd, CInt(Me.cboTipoExistencia.SelectedValue))
        objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
        objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)


        objScript.onCapa(Me, "capaBuscarProducto_AddProd")


    End Sub

    Private Sub cmbLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbLinea_AddProd.SelectedIndexChanged
        Try
            Dim objCbo As New Combo
            objCbo.LlenarCboSubLineaxIdLinea(Me.cmbSubLinea_AddProd, CInt(Me.cmbLinea_AddProd.SelectedValue), True)
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)


            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub


    Private Sub cmbSubLinea_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Dim objCbo As New Combo
        objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

        objScript.onCapa(Me, "capaBuscarProducto_AddProd")
    End Sub

    Private Function getListaProductoView() As List(Of Entidades.ProductoView)
        Return CType(Session.Item("listaProductoView"), List(Of Entidades.ProductoView))
    End Function

    Private Sub setListaProductoView(ByVal lista As List(Of Entidades.ProductoView))
        Session.Remove("listaProductoView")
        Session.Add("listaProductoView", lista)
    End Sub


    Private Sub DGV_AddProd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_AddProd.SelectedIndexChanged

        cargarProducto(Me.DGV_AddProd.SelectedIndex)


    End Sub

    Private Sub cargarProducto(ByVal Index As Integer)

        Try

            Me.listaProductoView = getListaProductoView()

            Dim lista As New List(Of Entidades.ProductoView)
            lista.Add(Me.listaProductoView(Index))

            Me.GV_Producto.DataSource = lista
            Me.GV_Producto.DataBind()

            Me.hddIdProducto.Value = CStr(Me.listaProductoView(Index).Id)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub



    Private Sub btnLimpiarDetalleDocumento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarDetalleDocumento.Click
        Me.GV_Producto.DataBind()
        Me.hddIdProducto.Value = "0"
    End Sub



#End Region

#Region "************************** BUSQUEDA AVANZADA PRODUCTOS"

    Private ListaSLTT As List(Of Entidades.SubLinea_TipoTabla)
    Private objSLTT As Entidades.SubLinea_TipoTabla
    Private ListaTTV As List(Of Entidades.TipoTablaValor)
    Private ObjTTV As Entidades.TipoTablaValor

    Private Sub LLenarGrillaTipoTablaValor()
        ListaSLTT = getListaTipoTablaValor()
        objSLTT = New Entidades.SubLinea_TipoTabla
        With objSLTT
            .IdTipoTablaValor = 0
            .objTipoTabla = (New Negocio.SubLinea_TipoTablaValor).SelectxIdLineaxIdSubLineaxIdTipoTabla(CInt(Me.cmbLinea_AddProd.SelectedValue), CInt(Me.cmbSubLinea_AddProd.SelectedValue), CInt(Me.cboTipoTabla.SelectedValue))
            .Nombre = CStr(Me.cboTipoTabla.SelectedItem.Text)
            .IdTipoTabla = CInt(Me.cboTipoTabla.SelectedValue)
        End With
        ListaSLTT.Add(objSLTT)
        Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
        Me.GV_FiltroTipoTabla.DataBind()
    End Sub
    Protected Sub btnAddTipoTabla_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTipoTabla.Click
        Try
            LLenarGrillaTipoTablaValor()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_FiltroTipoTabla_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_FiltroTipoTabla.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim cbo As DropDownList = CType(e.Row.Cells(2).FindControl("cboTTV"), DropDownList)
                If ListaSLTT(e.Row.RowIndex).IdTipoTablaValor <> 0 Then
                    cbo.SelectedValue = CStr(ListaSLTT(e.Row.RowIndex).IdTipoTablaValor)
                End If
            End If
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Protected Sub GV_FiltroTipoTabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GV_FiltroTipoTabla.SelectedIndexChanged
        Try
            ListaSLTT = getListaTipoTablaValor()
            ListaSLTT.RemoveAt(Me.GV_FiltroTipoTabla.SelectedIndex)
            Me.GV_FiltroTipoTabla.DataSource = ListaSLTT
            Me.GV_FiltroTipoTabla.DataBind()

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerListaTTVFrmGrilla() As List(Of Entidades.ProductoTipoTablaValor)
        Dim lista As New List(Of Entidades.ProductoTipoTablaValor)
        For i As Integer = 0 To Me.GV_FiltroTipoTabla.Rows.Count - 1
            With Me.GV_FiltroTipoTabla.Rows(i)
                Dim obj As New Entidades.ProductoTipoTablaValor(CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value), CInt(CType(Me.GV_FiltroTipoTabla.Rows(i).Cells(2).FindControl("cboTTV"), DropDownList).SelectedValue))
                lista.Add(obj)
            End With
        Next
        Return lista
    End Function
    Protected Sub M_B_cmbSubLinea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbSubLinea_AddProd.SelectedIndexChanged
        Try

            Dim objCbo As New Combo
            objCbo.LlenarCboTipoTablaxIdSubLinea(Me.cboTipoTabla, CInt(Me.cmbSubLinea_AddProd.SelectedValue), False)

            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Function getListaTipoTablaValor() As List(Of Entidades.SubLinea_TipoTabla)
        ListaSLTT = New List(Of Entidades.SubLinea_TipoTabla)
        For Each fila As GridViewRow In Me.GV_FiltroTipoTabla.Rows
            objSLTT = New Entidades.SubLinea_TipoTabla
            With objSLTT
                .IdTipoTabla = CInt(CType(fila.Cells(1).FindControl("hddIdTipoTabla"), HiddenField).Value)
                .Nombre = HttpUtility.HtmlDecode(CType(fila.Cells(1).FindControl("lblNomTipoTabla"), Label).Text).Trim
                Dim cboArea As DropDownList = CType(fila.Cells(2).FindControl("cboTTV"), DropDownList)
                .IdTipoTablaValor = CInt(cboArea.SelectedValue)
                ListaTTV = New List(Of Entidades.TipoTablaValor)
                For x As Integer = 0 To cboArea.Items.Count - 1
                    ObjTTV = New Entidades.TipoTablaValor
                    With ObjTTV
                        .IdTipoTablaValor = CInt(cboArea.Items(x).Value)
                        .Nombre = cboArea.Items(x).Text
                    End With
                    ListaTTV.Add(ObjTTV)
                Next
                .objTipoTabla = ListaTTV
            End With
            ListaSLTT.Add(objSLTT)
        Next
        Return ListaSLTT
    End Function

    Private Sub btnLimpiar_BA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiar_BA.Click
        limpiar_BA()
    End Sub
    Private Sub limpiar_BA()
        Try

            Me.GV_FiltroTipoTabla.DataSource = Nothing
            Me.GV_FiltroTipoTabla.DataBind()
            objScript.onCapa(Me, "capaBuscarProducto_AddProd")
        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Function obtenerDataTable_TipoTablaValor() As DataTable

        Dim dt As New DataTable
        dt.Columns.Add("Columna1")
        dt.Columns.Add("Columna2")

        Dim listaProductoTipoTablaValor As List(Of Entidades.ProductoTipoTablaValor) = obtenerListaTTVFrmGrilla()

        For i As Integer = 0 To listaProductoTipoTablaValor.Count - 1

            dt.Rows.Add(listaProductoTipoTablaValor(i).IdTipoTabla, listaProductoTipoTablaValor(i).IdTipoTablaValor)

        Next

        Return dt

    End Function
#End Region


#Region "****************Buscar Personas Mantenimiento"

    Private Sub Buscar(ByVal gv As GridView, ByVal dni As String, ByVal ruc As String, _
                       ByVal RazonApe As String, ByVal tipo As Integer, ByVal idrol As Integer, _
                       ByVal estado As Integer, ByVal tipoMov As Integer)

        Dim index As Integer = 0
        Select Case tipoMov
            Case 0 '********************************************************* INICIO
                index = 0
            Case 1 '********************************************************* Anterior
                index = (CInt(Me.tbPageIndex.Text) - 1) - 1
            Case 2 '********************************************************* Posterior
                index = (CInt(Me.tbPageIndex.Text) - 1) + 1
            Case 3 '********************************************************* IR
                index = (CInt(Me.tbPageIndexGO.Text) - 1)
        End Select

        Dim listaPersona As New List(Of Entidades.PersonaView)
        listaPersona = (New Negocio.Persona).listarxPersonaNaturalxPersonaJuridica(dni, ruc, RazonApe, tipo, index, gv.PageSize, idrol, estado)

        If listaPersona.Count > 0 Then
            gv.DataSource = listaPersona
            gv.DataBind()
            tbPageIndex.Text = CStr(index + 1)
            objScript.onCapa(Me, "capaPersona")
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "onLoad", "      onCapa('capaPersona');   alert('No se hallaron registros.');    ", True)
        End If

    End Sub
    Private Sub btAnterior_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAnterior.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 1)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btSiguiente.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 2)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub btIr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btIr.Click
        Try
            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 3)
        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub
    Private Sub GV_BuscarPersona_Usuario_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_BuscarPersona_Usuario.SelectedIndexChanged

        Try

            cargarPersona(CInt(Me.GV_BuscarPersona_Usuario.SelectedRow.Cells(1).Text))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub
    Private Sub cargarPersona(ByVal IdPersona As Integer)

        Dim objPersona As Entidades.PersonaView = (New Negocio.PersonaView).SelectxIdPersona(IdPersona)

        If (objPersona Is Nothing) Then Throw New Exception("NO SE HALLARON REGISTROS.")


        With objPersona

            Me.txtDescripcionPersona.Text = .Descripcion
            Me.txtDni.Text = .Dni
            Me.txtRuc.Text = .Ruc
            Me.hddIdVendedor.Value = CStr(.IdPersona)

        End With

        objScript.offCapa(Me, "capaPersona")

    End Sub

    Private Sub cboEmpresa_BuscarPersona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpresa_BuscarPersona.SelectedIndexChanged
        valOnChange_cboEmpresa_BuscarPersona()
    End Sub
    Private Sub valOnChange_cboEmpresa_BuscarPersona()
        Try

            cbo.llenarCboTiendaxIdEmpresa(Me.cboTienda_BuscarPersona, CInt(Me.cboEmpresa_BuscarPersona.SelectedValue), False)

        Catch ex As Exception

            objScript.mostrarMsjAlerta(Me, ex.Message)

        End Try
    End Sub

    Private Sub btnBuscarPersona_Usuario_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscarPersona_Usuario.Click
        buscarPersona_EmpresaTienda()
    End Sub
    Private Sub buscarPersona_EmpresaTienda()
        Try

            Me.GV_BuscarPersona_Usuario.DataSource = (New Negocio.Usuario).Usuario_SelectxIdEmpresaxIdTiendaxIdPerfil_Distinct(CInt(Me.cboEmpresa_BuscarPersona.SelectedValue), CInt(Me.cboTienda_BuscarPersona.SelectedValue), CInt(Me.cboPerfil_BuscarPersona.SelectedValue))
            Me.GV_BuscarPersona_Usuario.DataBind()

            If (Me.GV_BuscarPersona_Usuario.Rows.Count <= 0) Then
                objScript.mostrarMsjAlerta(Me, "NO SE HALLARON REGISTROS")
            End If

            objScript.onCapa(Me, "capaPersona")

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub rdbTipoBusqueda_Persona_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbTipoBusqueda_Persona.SelectedIndexChanged
        valOnChange_rdbTipoBusqueda_Persona()
    End Sub
    Private Sub valOnChange_rdbTipoBusqueda_Persona()
        Try

            actualizarControles_BuscarPersona(CInt(Me.rdbTipoBusqueda_Persona.SelectedValue))

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub actualizarControles_BuscarPersona(ByVal opcionBuscarPersona As Integer)
        Me.pnlBusquedaPersona.Visible = False
        Me.Panel_BusquedaPersona_Usuario.Visible = False

        Select Case opcionBuscarPersona
            Case 0 '****************** GENERAL
                Me.pnlBusquedaPersona.Visible = True
            Case 1 '****************** EMPRESA / TIENDA
                Me.Panel_BusquedaPersona_Usuario.Visible = True
        End Select


    End Sub

    Private Sub btnBuscar_Persona_General_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar_Persona_General.Click
        valOnClick_btnBuscar_Persona_General()
    End Sub
    Private Sub valOnClick_btnBuscar_Persona_General()
        Try

            ViewState.Add("dni", tbbuscarDni.Text.Trim)
            ViewState.Add("ruc", tbbuscarRuc.Text.Trim)
            ViewState.Add("pat", tbRazonApe.Text.Trim)
            ViewState.Add("tipo", CInt(IIf(Me.rdbTipoPersona.SelectedValue = "N", "1", "2")))
            ViewState.Add("estado", 1) '******************* ACTIVO
            ViewState.Add("rol", Me.cboRol.SelectedValue)

            Buscar(gvBuscar, CStr(ViewState("dni")), CStr(ViewState("ruc")), CStr(ViewState("pat")), CInt(ViewState("tipo")), CInt(ViewState("rol")), CInt(ViewState("estado")), 0)

        Catch ex As Exception
            objScript = New ScriptManagerClass
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

#End Region



#Region " **************** Buscar Tabla *** "
    Private ListaTipoTabla As List(Of Entidades.TipoTabla)

    Private Sub ImgBuscarTabla_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBuscarTabla.Click
        Buscar_Tabla()
    End Sub
    Private Sub Buscar_Tabla()

        Try

            Me.ViewState.Add("BuscarTabla", Me.txtBuscarTabla.Text.Trim)
            Me.Find_Tabla(0)

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try

    End Sub
    Private Sub Find_Tabla(ByVal TipoMov As Integer)

        Dim Index As Integer

        Select Case TipoMov
            Case 0
                Index = 0
            Case 1
                Index = (CInt(Me.txtPageIndexTabla.Text) - 1) - 1 ' ******* ANTERIOR
            Case 2
                Index = (CInt(Me.txtPageIndexTabla.Text) - 1) + 1 ' ******* SIGUIENTE
            Case 3
                Index = (CInt(Me.txtPageIndexGoTabla.Text) - 1) - 1 ' ******* IR
        End Select

        Dim lista As List(Of Entidades.TipoTabla) = (New Negocio.TipoTabla).TipoTablaSelectAllxEstado_Paginado(CStr(Me.ViewState("BuscarTabla")), 1, Index, Me.GV_Tabla.PageSize)

        If Not IsNothing(lista) Then

            If lista.Count > 0 Then

                Me.txtPageIndexTabla.Text = CStr(Index + 1)

                Me.GV_Tabla.DataSource = lista
                Me.GV_Tabla.DataBind()
                Me.GV_Tabla.SelectedIndex = -1

            Else
                objScript.mostrarMsjAlerta(Me, "No se hallaron Registros.")
            End If

        End If

        Me.ModalPopup_Atributo.Show()

    End Sub

    Private Sub btnAnteriorTabla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnteriorTabla.Click
        Find_Tabla(1)
    End Sub

    Private Sub btnSiguienteTabla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguienteTabla.Click
        Find_Tabla(2)
    End Sub

    Private Sub btnIrTabla_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIrTabla.Click
        Find_Tabla(3)
    End Sub


    Private Sub GV_Tabla_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_Tabla.SelectedIndexChanged

        Me.lblNomTabla.Text = HttpUtility.HtmlDecode(CType(Me.GV_Tabla.Rows(Me.GV_Tabla.SelectedRow.RowIndex).FindControl("lblTabla"), Label).Text).Trim

        Me.ModalPopup_Atributo.Show()
        Me.ModalPopup_TablaValor.Show()

    End Sub

#End Region

#Region " **************** Buscar Tabla Valor *** "

    Private ListaTipoTablaValor As List(Of Entidades.TipoTablaValor)
    Private ListaFiltroTablaValor As List(Of Entidades.TipoTablaValor)

    Private Property S_TipoTablaValor() As List(Of Entidades.TipoTablaValor)
        Get
            Return CType(Session.Item("ListaTipoTablaValor2"), List(Of Entidades.TipoTablaValor))
        End Get
        Set(ByVal value As List(Of Entidades.TipoTablaValor))
            Session.Remove("ListaTipoTablaValor2")
            Session.Add("ListaTipoTablaValor2", value)
        End Set
    End Property
    Private Property S_FiltroTablaValor() As List(Of Entidades.TipoTablaValor)
        Get
            Return CType(Session.Item("ListaFiltroTablaValor2"), List(Of Entidades.TipoTablaValor))
        End Get
        Set(ByVal value As List(Of Entidades.TipoTablaValor))
            Session.Remove("ListaFiltroTablaValor2")
            Session.Add("ListaFiltroTablaValor2", value)
        End Set
    End Property


    Private Sub ImgBuscarTablaValor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBuscarTablaValor.Click
        Buscar_TablaValor()
    End Sub
    Private Sub Buscar_TablaValor()

        Dim Index As Integer = Me.GV_Tabla.SelectedRow.RowIndex

        If Index < 0 Then
            objScript.mostrarMsjAlerta(Me, "Seleccione un tipo de tabla")
            ModalPopup_Atributo.Show()
            Exit Sub

        End If


        Me.ViewState.Add("BuscarTablaValor", Me.txtBuscarTablaValor.Text.Trim)
        Me.ViewState.Add("IdTipoTabla", CType(Me.GV_Tabla.Rows(Index).FindControl("hddIdTipoTabla"), HiddenField).Value)
        Find_TablaValor(0)

    End Sub
    Private Sub Find_TablaValor(ByVal TipoMov As Integer)
        Try
            Dim Index As Integer

            Select Case TipoMov
                Case 0
                    Index = 0
                Case 1
                    Index = (CInt(Me.txtPageIndexTablaValor.Text) - 1) - 1 ' ******* ANTERIOR
                Case 2
                    Index = (CInt(Me.txtPageIndexTablaValor.Text) - 1) + 1 ' ******* SIGUIENTE
                Case 3
                    Index = (CInt(Me.txtPageIndexGoTablaValor.Text) - 1) - 1 ' ******* IR
            End Select

            ListaTipoTablaValor = (New Negocio.TipoTablaValor).TipoTablaValorSelectAllxIdTipoTablaxEstado_Paginado(CInt(Me.ViewState("IdTipoTabla")), CStr(Me.ViewState("BuscarTablaValor")), 1, Index, Me.GV_TablaValor.PageSize)

            If Not IsNothing(ListaTipoTablaValor) Then

                If ListaTipoTablaValor.Count > 0 Then

                    S_TipoTablaValor = ListaTipoTablaValor

                    Me.txtPageIndexTablaValor.Text = CStr(Index + 1)

                    Me.GV_TablaValor.DataSource = ListaTipoTablaValor
                    Me.GV_TablaValor.DataBind()


                Else
                    objScript.mostrarMsjAlerta(Me, "No se hallaron Registros.")
                End If

            End If

            Me.ModalPopup_Atributo.Show()
            Me.ModalPopup_TablaValor.Show()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub btnAnteriorTablaValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAnteriorTablaValor.Click
        Find_TablaValor(1)
    End Sub

    Private Sub btnSiguienteTablaValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSiguienteTablaValor.Click
        Find_TablaValor(2)
    End Sub

    Private Sub btnIrTablaValor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIrTablaValor.Click
        Find_TablaValor(3)
    End Sub

    Private Sub ImgAgregarTablaValor_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgAgregarTablaValor.Click
        Agregar_TablaValor()
    End Sub
    Private Sub Agregar_TablaValor()
        Try
            ListaFiltroTablaValor = S_FiltroTablaValor
            ListaTipoTablaValor = S_TipoTablaValor

            Dim Lista As New List(Of Entidades.TipoTablaValor)

            For i As Integer = 0 To Me.GV_TablaValor.Rows.Count - 1
                If CType(Me.GV_TablaValor.Rows(i).FindControl("ckTablaValor"), CheckBox).Checked Then

                    ListaFiltroTablaValor.Add(ListaTipoTablaValor(i))

                End If
            Next

            Me.S_FiltroTablaValor = ListaFiltroTablaValor
            Me.GV_FiltroTipoTablaValor.DataSource = ListaFiltroTablaValor
            Me.GV_FiltroTipoTablaValor.DataBind()

            Me.ModalPopup_Atributo.Show()
            Me.ModalPopup_TablaValor.Show()

        Catch ex As Exception
            objScript.mostrarMsjAlerta(Me, ex.Message)
        End Try
    End Sub

    Private Sub GV_FiltroTipoTablaValor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GV_FiltroTipoTablaValor.SelectedIndexChanged

        Me.ListaFiltroTablaValor = Me.S_FiltroTablaValor
        Me.ListaFiltroTablaValor.RemoveAt(Me.GV_FiltroTipoTablaValor.SelectedRow.RowIndex)
        Me.S_FiltroTablaValor = ListaFiltroTablaValor

        Me.GV_FiltroTipoTablaValor.DataSource = ListaFiltroTablaValor
        Me.GV_FiltroTipoTablaValor.DataBind()

    End Sub

#End Region



End Class