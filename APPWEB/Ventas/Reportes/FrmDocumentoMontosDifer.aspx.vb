﻿'                            ADVERTENCIA
'Esta es una copia  del producto autorizada de DIGRAFIC  SRL.,  a SANICENTER SAC.,  en virtud al 
'contrato celebrado por ambas empresas, a fin de que el personal de  Sanicenter pueda efectuar las 
'modificaciones UNICAMENTE para los procesos de SANICENTER.
'Está prohibida su reproducción total o parcialmente.
'Si Ud. Tiene una copia de estos programas fuentes y quiere usarlos para otros fines que no sean 
'los descritos en el primer párrafo,  va a ser detectado y enjuiciado por la Empresa y DIGRAFIC  SRL. 
'De acuerdo a la ley de protección de los Derechos de Autor y la propiedad intelectual.

Partial Public Class FrmDocumentoMontosDifer
    Inherits System.Web.UI.Page
    Dim objcombo As New Combo
    Private objNegUsu As New Negocio.UsuarioView

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            inicializarForm()
        End If
    End Sub
    Private Sub inicializarForm()

        objcombo.LlenarCboPropietario(Me.cmbEmpresa, True)
        If objNegUsu.FunVerTodasTiendas(CInt(Session("IdUsuario")), 22) > 0 Then
            objcombo.LlenarCboTiendaxIdUsuario(Me.cmbtienda, CInt(Session("IdUsuario")), True)
        Else
            objcombo.LlenarCboTiendaxIdUsuario(Me.cmbtienda, CInt(Session("IdUsuario")), False)
        End If
        Dim objFechaActual As New Negocio.FechaActual
        txtFechaInicio.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
        txtFechaFin.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
        Me.hddFechaActual.Value = CStr(Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy"))

        'Me.TextBox1.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")
        'Me.TextBox2.Text = Format(objFechaActual.SelectFechaActual, "dd/MM/yyyy")

    End Sub

End Class